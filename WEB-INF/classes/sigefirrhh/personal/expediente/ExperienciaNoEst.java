package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ExperienciaNoEst
  implements Serializable, PersistenceCapable
{
  private long idExperienciaNoEst;
  private String nombreInstitucion;
  private Date fechaIngreso;
  private Date fechaEgreso;
  private String cargoIngreso;
  private String cargoEgreso;
  private String jefe;
  private String telefono;
  private String causaRetiro;
  private double ultimoSueldo;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargoEgreso", "cargoIngreso", "causaRetiro", "fechaEgreso", "fechaIngreso", "idExperienciaNoEst", "idSitp", "jefe", "nombreInstitucion", "observaciones", "personal", "telefono", "tiempoSitp", "ultimoSueldo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombreInstitucion(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaIngreso(this)) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaEgreso(this));
  }

  public String getCargoEgreso()
  {
    return jdoGetcargoEgreso(this);
  }

  public String getCargoIngreso()
  {
    return jdoGetcargoIngreso(this);
  }

  public String getCausaRetiro()
  {
    return jdoGetcausaRetiro(this);
  }

  public Date getFechaEgreso()
  {
    return jdoGetfechaEgreso(this);
  }

  public Date getFechaIngreso()
  {
    return jdoGetfechaIngreso(this);
  }

  public long getIdExperienciaNoEst()
  {
    return jdoGetidExperienciaNoEst(this);
  }

  public String getJefe()
  {
    return jdoGetjefe(this);
  }

  public String getNombreInstitucion()
  {
    return jdoGetnombreInstitucion(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public String getTelefono()
  {
    return jdoGettelefono(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public double getUltimoSueldo()
  {
    return jdoGetultimoSueldo(this);
  }

  public void setCargoEgreso(String string)
  {
    jdoSetcargoEgreso(this, string);
  }

  public void setCargoIngreso(String string)
  {
    jdoSetcargoIngreso(this, string);
  }

  public void setCausaRetiro(String string)
  {
    jdoSetcausaRetiro(this, string);
  }

  public void setFechaEgreso(Date date)
  {
    jdoSetfechaEgreso(this, date);
  }

  public void setFechaIngreso(Date date)
  {
    jdoSetfechaIngreso(this, date);
  }

  public void setIdExperienciaNoEst(long l)
  {
    jdoSetidExperienciaNoEst(this, l);
  }

  public void setJefe(String string)
  {
    jdoSetjefe(this, string);
  }

  public void setNombreInstitucion(String string)
  {
    jdoSetnombreInstitucion(this, string);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTelefono(String string)
  {
    jdoSettelefono(this, string);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setUltimoSueldo(double d)
  {
    jdoSetultimoSueldo(this, d);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.ExperienciaNoEst"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ExperienciaNoEst());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ExperienciaNoEst localExperienciaNoEst = new ExperienciaNoEst();
    localExperienciaNoEst.jdoFlags = 1;
    localExperienciaNoEst.jdoStateManager = paramStateManager;
    return localExperienciaNoEst;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ExperienciaNoEst localExperienciaNoEst = new ExperienciaNoEst();
    localExperienciaNoEst.jdoCopyKeyFieldsFromObjectId(paramObject);
    localExperienciaNoEst.jdoFlags = 1;
    localExperienciaNoEst.jdoStateManager = paramStateManager;
    return localExperienciaNoEst;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoEgreso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoIngreso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.causaRetiro);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEgreso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idExperienciaNoEst);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.jefe);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInstitucion);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ultimoSueldo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaRetiro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEgreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idExperienciaNoEst = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jefe = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ultimoSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ExperienciaNoEst paramExperienciaNoEst, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.cargoEgreso = paramExperienciaNoEst.cargoEgreso;
      return;
    case 1:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.cargoIngreso = paramExperienciaNoEst.cargoIngreso;
      return;
    case 2:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.causaRetiro = paramExperienciaNoEst.causaRetiro;
      return;
    case 3:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEgreso = paramExperienciaNoEst.fechaEgreso;
      return;
    case 4:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramExperienciaNoEst.fechaIngreso;
      return;
    case 5:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.idExperienciaNoEst = paramExperienciaNoEst.idExperienciaNoEst;
      return;
    case 6:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramExperienciaNoEst.idSitp;
      return;
    case 7:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.jefe = paramExperienciaNoEst.jefe;
      return;
    case 8:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInstitucion = paramExperienciaNoEst.nombreInstitucion;
      return;
    case 9:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramExperienciaNoEst.observaciones;
      return;
    case 10:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramExperienciaNoEst.personal;
      return;
    case 11:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.telefono = paramExperienciaNoEst.telefono;
      return;
    case 12:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramExperienciaNoEst.tiempoSitp;
      return;
    case 13:
      if (paramExperienciaNoEst == null)
        throw new IllegalArgumentException("arg1");
      this.ultimoSueldo = paramExperienciaNoEst.ultimoSueldo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ExperienciaNoEst))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ExperienciaNoEst localExperienciaNoEst = (ExperienciaNoEst)paramObject;
    if (localExperienciaNoEst.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localExperienciaNoEst, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ExperienciaNoEstPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ExperienciaNoEstPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExperienciaNoEstPK))
      throw new IllegalArgumentException("arg1");
    ExperienciaNoEstPK localExperienciaNoEstPK = (ExperienciaNoEstPK)paramObject;
    localExperienciaNoEstPK.idExperienciaNoEst = this.idExperienciaNoEst;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExperienciaNoEstPK))
      throw new IllegalArgumentException("arg1");
    ExperienciaNoEstPK localExperienciaNoEstPK = (ExperienciaNoEstPK)paramObject;
    this.idExperienciaNoEst = localExperienciaNoEstPK.idExperienciaNoEst;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExperienciaNoEstPK))
      throw new IllegalArgumentException("arg2");
    ExperienciaNoEstPK localExperienciaNoEstPK = (ExperienciaNoEstPK)paramObject;
    localExperienciaNoEstPK.idExperienciaNoEst = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExperienciaNoEstPK))
      throw new IllegalArgumentException("arg2");
    ExperienciaNoEstPK localExperienciaNoEstPK = (ExperienciaNoEstPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localExperienciaNoEstPK.idExperienciaNoEst);
  }

  private static final String jdoGetcargoEgreso(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.cargoEgreso;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.cargoEgreso;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 0))
      return paramExperienciaNoEst.cargoEgreso;
    return localStateManager.getStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 0, paramExperienciaNoEst.cargoEgreso);
  }

  private static final void jdoSetcargoEgreso(ExperienciaNoEst paramExperienciaNoEst, String paramString)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.cargoEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.cargoEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 0, paramExperienciaNoEst.cargoEgreso, paramString);
  }

  private static final String jdoGetcargoIngreso(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.cargoIngreso;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.cargoIngreso;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 1))
      return paramExperienciaNoEst.cargoIngreso;
    return localStateManager.getStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 1, paramExperienciaNoEst.cargoIngreso);
  }

  private static final void jdoSetcargoIngreso(ExperienciaNoEst paramExperienciaNoEst, String paramString)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.cargoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.cargoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 1, paramExperienciaNoEst.cargoIngreso, paramString);
  }

  private static final String jdoGetcausaRetiro(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.causaRetiro;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.causaRetiro;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 2))
      return paramExperienciaNoEst.causaRetiro;
    return localStateManager.getStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 2, paramExperienciaNoEst.causaRetiro);
  }

  private static final void jdoSetcausaRetiro(ExperienciaNoEst paramExperienciaNoEst, String paramString)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.causaRetiro = paramString;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.causaRetiro = paramString;
      return;
    }
    localStateManager.setStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 2, paramExperienciaNoEst.causaRetiro, paramString);
  }

  private static final Date jdoGetfechaEgreso(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.fechaEgreso;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.fechaEgreso;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 3))
      return paramExperienciaNoEst.fechaEgreso;
    return (Date)localStateManager.getObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 3, paramExperienciaNoEst.fechaEgreso);
  }

  private static final void jdoSetfechaEgreso(ExperienciaNoEst paramExperienciaNoEst, Date paramDate)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.fechaEgreso = paramDate;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.fechaEgreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 3, paramExperienciaNoEst.fechaEgreso, paramDate);
  }

  private static final Date jdoGetfechaIngreso(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.fechaIngreso;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.fechaIngreso;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 4))
      return paramExperienciaNoEst.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 4, paramExperienciaNoEst.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(ExperienciaNoEst paramExperienciaNoEst, Date paramDate)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 4, paramExperienciaNoEst.fechaIngreso, paramDate);
  }

  private static final long jdoGetidExperienciaNoEst(ExperienciaNoEst paramExperienciaNoEst)
  {
    return paramExperienciaNoEst.idExperienciaNoEst;
  }

  private static final void jdoSetidExperienciaNoEst(ExperienciaNoEst paramExperienciaNoEst, long paramLong)
  {
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.idExperienciaNoEst = paramLong;
      return;
    }
    localStateManager.setLongField(paramExperienciaNoEst, jdoInheritedFieldCount + 5, paramExperienciaNoEst.idExperienciaNoEst, paramLong);
  }

  private static final int jdoGetidSitp(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.idSitp;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.idSitp;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 6))
      return paramExperienciaNoEst.idSitp;
    return localStateManager.getIntField(paramExperienciaNoEst, jdoInheritedFieldCount + 6, paramExperienciaNoEst.idSitp);
  }

  private static final void jdoSetidSitp(ExperienciaNoEst paramExperienciaNoEst, int paramInt)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramExperienciaNoEst, jdoInheritedFieldCount + 6, paramExperienciaNoEst.idSitp, paramInt);
  }

  private static final String jdoGetjefe(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.jefe;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.jefe;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 7))
      return paramExperienciaNoEst.jefe;
    return localStateManager.getStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 7, paramExperienciaNoEst.jefe);
  }

  private static final void jdoSetjefe(ExperienciaNoEst paramExperienciaNoEst, String paramString)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.jefe = paramString;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.jefe = paramString;
      return;
    }
    localStateManager.setStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 7, paramExperienciaNoEst.jefe, paramString);
  }

  private static final String jdoGetnombreInstitucion(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.nombreInstitucion;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.nombreInstitucion;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 8))
      return paramExperienciaNoEst.nombreInstitucion;
    return localStateManager.getStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 8, paramExperienciaNoEst.nombreInstitucion);
  }

  private static final void jdoSetnombreInstitucion(ExperienciaNoEst paramExperienciaNoEst, String paramString)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.nombreInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.nombreInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 8, paramExperienciaNoEst.nombreInstitucion, paramString);
  }

  private static final String jdoGetobservaciones(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.observaciones;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.observaciones;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 9))
      return paramExperienciaNoEst.observaciones;
    return localStateManager.getStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 9, paramExperienciaNoEst.observaciones);
  }

  private static final void jdoSetobservaciones(ExperienciaNoEst paramExperienciaNoEst, String paramString)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 9, paramExperienciaNoEst.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(ExperienciaNoEst paramExperienciaNoEst)
  {
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.personal;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 10))
      return paramExperienciaNoEst.personal;
    return (Personal)localStateManager.getObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 10, paramExperienciaNoEst.personal);
  }

  private static final void jdoSetpersonal(ExperienciaNoEst paramExperienciaNoEst, Personal paramPersonal)
  {
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 10, paramExperienciaNoEst.personal, paramPersonal);
  }

  private static final String jdoGettelefono(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.telefono;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.telefono;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 11))
      return paramExperienciaNoEst.telefono;
    return localStateManager.getStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 11, paramExperienciaNoEst.telefono);
  }

  private static final void jdoSettelefono(ExperienciaNoEst paramExperienciaNoEst, String paramString)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.telefono = paramString;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.telefono = paramString;
      return;
    }
    localStateManager.setStringField(paramExperienciaNoEst, jdoInheritedFieldCount + 11, paramExperienciaNoEst.telefono, paramString);
  }

  private static final Date jdoGettiempoSitp(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.tiempoSitp;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.tiempoSitp;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 12))
      return paramExperienciaNoEst.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 12, paramExperienciaNoEst.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ExperienciaNoEst paramExperienciaNoEst, Date paramDate)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramExperienciaNoEst, jdoInheritedFieldCount + 12, paramExperienciaNoEst.tiempoSitp, paramDate);
  }

  private static final double jdoGetultimoSueldo(ExperienciaNoEst paramExperienciaNoEst)
  {
    if (paramExperienciaNoEst.jdoFlags <= 0)
      return paramExperienciaNoEst.ultimoSueldo;
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaNoEst.ultimoSueldo;
    if (localStateManager.isLoaded(paramExperienciaNoEst, jdoInheritedFieldCount + 13))
      return paramExperienciaNoEst.ultimoSueldo;
    return localStateManager.getDoubleField(paramExperienciaNoEst, jdoInheritedFieldCount + 13, paramExperienciaNoEst.ultimoSueldo);
  }

  private static final void jdoSetultimoSueldo(ExperienciaNoEst paramExperienciaNoEst, double paramDouble)
  {
    if (paramExperienciaNoEst.jdoFlags == 0)
    {
      paramExperienciaNoEst.ultimoSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramExperienciaNoEst.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaNoEst.ultimoSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramExperienciaNoEst, jdoInheritedFieldCount + 13, paramExperienciaNoEst.ultimoSueldo, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}