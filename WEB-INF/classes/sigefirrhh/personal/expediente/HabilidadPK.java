package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class HabilidadPK
  implements Serializable
{
  public long idHabilidad;

  public HabilidadPK()
  {
  }

  public HabilidadPK(long idHabilidad)
  {
    this.idHabilidad = idHabilidad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HabilidadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HabilidadPK thatPK)
  {
    return 
      this.idHabilidad == thatPK.idHabilidad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHabilidad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHabilidad);
  }
}