package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoIdioma;
import sigefirrhh.base.personal.TipoIdiomaBeanBusiness;

public class IdiomaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addIdioma(Idioma idioma)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Idioma idiomaNew = 
      (Idioma)BeanUtils.cloneBean(
      idioma);

    TipoIdiomaBeanBusiness tipoIdiomaBeanBusiness = new TipoIdiomaBeanBusiness();

    if (idiomaNew.getTipoIdioma() != null) {
      idiomaNew.setTipoIdioma(
        tipoIdiomaBeanBusiness.findTipoIdiomaById(
        idiomaNew.getTipoIdioma().getIdTipoIdioma()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (idiomaNew.getPersonal() != null) {
      idiomaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        idiomaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(idiomaNew);
  }

  public void updateIdioma(Idioma idioma) throws Exception
  {
    Idioma idiomaModify = 
      findIdiomaById(idioma.getIdIdioma());

    TipoIdiomaBeanBusiness tipoIdiomaBeanBusiness = new TipoIdiomaBeanBusiness();

    if (idioma.getTipoIdioma() != null) {
      idioma.setTipoIdioma(
        tipoIdiomaBeanBusiness.findTipoIdiomaById(
        idioma.getTipoIdioma().getIdTipoIdioma()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (idioma.getPersonal() != null) {
      idioma.setPersonal(
        personalBeanBusiness.findPersonalById(
        idioma.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(idiomaModify, idioma);
  }

  public void deleteIdioma(Idioma idioma) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Idioma idiomaDelete = 
      findIdiomaById(idioma.getIdIdioma());
    pm.deletePersistent(idiomaDelete);
  }

  public Idioma findIdiomaById(long idIdioma) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idIdioma == pIdIdioma";
    Query query = pm.newQuery(Idioma.class, filter);

    query.declareParameters("long pIdIdioma");

    parameters.put("pIdIdioma", new Long(idIdioma));

    Collection colIdioma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colIdioma.iterator();
    return (Idioma)iterator.next();
  }

  public Collection findIdiomaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent idiomaExtent = pm.getExtent(
      Idioma.class, true);
    Query query = pm.newQuery(idiomaExtent);
    query.setOrdering("tipoIdioma.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Idioma.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("tipoIdioma.descripcion ascending");

    Collection colIdioma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colIdioma);

    return colIdioma;
  }
}