package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Carrera;

public class ActividadDocente
  implements Serializable, PersistenceCapable
{
  private long idActividadDocente;
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SECTOR;
  private String nivelEducativo;
  private int anioInicio;
  private int anioFin;
  private String estatus;
  private String sector;
  private String nombreEntidad;
  private String asignatura;
  private String relacionLaboral;
  private Carrera carrera;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anioFin", "anioInicio", "asignatura", "carrera", "estatus", "idActividadDocente", "idSitp", "nivelEducativo", "nombreEntidad", "observaciones", "personal", "relacionLaboral", "sector", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.Carrera"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 21, 24, 21, 21, 21, 21, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.ActividadDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ActividadDocente());

    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_SECTOR = 
      new LinkedHashMap();

    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("N", "NINGUNO");
    LISTA_ESTATUS.put("S", "SI");
    LISTA_ESTATUS.put("N", "NO");
    LISTA_SECTOR.put("P", "PUBLICO");
    LISTA_SECTOR.put("U", "PRIVADO");
    LISTA_SECTOR.put("E", "EXTERIOR");
  }

  public ActividadDocente()
  {
    jdoSetnivelEducativo(this, "N");

    jdoSetestatus(this, "N");

    jdoSetsector(this, "P");
  }

  public String toString()
  {
    return jdoGetnombreEntidad(this) + " " + 
      jdoGetanioInicio(this);
  }

  public int getAnioFin()
  {
    return jdoGetanioFin(this);
  }

  public int getAnioInicio()
  {
    return jdoGetanioInicio(this);
  }

  public Carrera getCarrera()
  {
    return jdoGetcarrera(this);
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public long getIdActividadDocente()
  {
    return jdoGetidActividadDocente(this);
  }

  public String getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setAnioFin(int i)
  {
    jdoSetanioFin(this, i);
  }

  public void setAnioInicio(int i)
  {
    jdoSetanioInicio(this, i);
  }

  public void setCarrera(Carrera carrera)
  {
    jdoSetcarrera(this, carrera);
  }

  public void setNombreEntidad(String string)
  {
    jdoSetnombreEntidad(this, string);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setIdActividadDocente(long l)
  {
    jdoSetidActividadDocente(this, l);
  }

  public void setNivelEducativo(String string)
  {
    jdoSetnivelEducativo(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public String getSector() {
    return jdoGetsector(this);
  }

  public String getRelacionLaboral() {
    return jdoGetrelacionLaboral(this);
  }

  public void setSector(String string) {
    jdoSetsector(this, string);
  }

  public void setRelacionLaboral(String string) {
    jdoSetrelacionLaboral(this, string);
  }

  public String getAsignatura()
  {
    return jdoGetasignatura(this);
  }

  public void setAsignatura(String string)
  {
    jdoSetasignatura(this, string);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ActividadDocente localActividadDocente = new ActividadDocente();
    localActividadDocente.jdoFlags = 1;
    localActividadDocente.jdoStateManager = paramStateManager;
    return localActividadDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ActividadDocente localActividadDocente = new ActividadDocente();
    localActividadDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localActividadDocente.jdoFlags = 1;
    localActividadDocente.jdoStateManager = paramStateManager;
    return localActividadDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioFin);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioInicio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.asignatura);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.carrera);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idActividadDocente);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.relacionLaboral);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sector);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioFin = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioInicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.carrera = ((Carrera)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idActividadDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.relacionLaboral = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ActividadDocente paramActividadDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.anioFin = paramActividadDocente.anioFin;
      return;
    case 1:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.anioInicio = paramActividadDocente.anioInicio;
      return;
    case 2:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.asignatura = paramActividadDocente.asignatura;
      return;
    case 3:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.carrera = paramActividadDocente.carrera;
      return;
    case 4:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramActividadDocente.estatus;
      return;
    case 5:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idActividadDocente = paramActividadDocente.idActividadDocente;
      return;
    case 6:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramActividadDocente.idSitp;
      return;
    case 7:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramActividadDocente.nivelEducativo;
      return;
    case 8:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramActividadDocente.nombreEntidad;
      return;
    case 9:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramActividadDocente.observaciones;
      return;
    case 10:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramActividadDocente.personal;
      return;
    case 11:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.relacionLaboral = paramActividadDocente.relacionLaboral;
      return;
    case 12:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.sector = paramActividadDocente.sector;
      return;
    case 13:
      if (paramActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramActividadDocente.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ActividadDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ActividadDocente localActividadDocente = (ActividadDocente)paramObject;
    if (localActividadDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localActividadDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ActividadDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ActividadDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ActividadDocentePK))
      throw new IllegalArgumentException("arg1");
    ActividadDocentePK localActividadDocentePK = (ActividadDocentePK)paramObject;
    localActividadDocentePK.idActividadDocente = this.idActividadDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ActividadDocentePK))
      throw new IllegalArgumentException("arg1");
    ActividadDocentePK localActividadDocentePK = (ActividadDocentePK)paramObject;
    this.idActividadDocente = localActividadDocentePK.idActividadDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ActividadDocentePK))
      throw new IllegalArgumentException("arg2");
    ActividadDocentePK localActividadDocentePK = (ActividadDocentePK)paramObject;
    localActividadDocentePK.idActividadDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ActividadDocentePK))
      throw new IllegalArgumentException("arg2");
    ActividadDocentePK localActividadDocentePK = (ActividadDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localActividadDocentePK.idActividadDocente);
  }

  private static final int jdoGetanioFin(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.anioFin;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.anioFin;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 0))
      return paramActividadDocente.anioFin;
    return localStateManager.getIntField(paramActividadDocente, jdoInheritedFieldCount + 0, paramActividadDocente.anioFin);
  }

  private static final void jdoSetanioFin(ActividadDocente paramActividadDocente, int paramInt)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.anioFin = paramInt;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.anioFin = paramInt;
      return;
    }
    localStateManager.setIntField(paramActividadDocente, jdoInheritedFieldCount + 0, paramActividadDocente.anioFin, paramInt);
  }

  private static final int jdoGetanioInicio(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.anioInicio;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.anioInicio;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 1))
      return paramActividadDocente.anioInicio;
    return localStateManager.getIntField(paramActividadDocente, jdoInheritedFieldCount + 1, paramActividadDocente.anioInicio);
  }

  private static final void jdoSetanioInicio(ActividadDocente paramActividadDocente, int paramInt)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.anioInicio = paramInt;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.anioInicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramActividadDocente, jdoInheritedFieldCount + 1, paramActividadDocente.anioInicio, paramInt);
  }

  private static final String jdoGetasignatura(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.asignatura;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.asignatura;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 2))
      return paramActividadDocente.asignatura;
    return localStateManager.getStringField(paramActividadDocente, jdoInheritedFieldCount + 2, paramActividadDocente.asignatura);
  }

  private static final void jdoSetasignatura(ActividadDocente paramActividadDocente, String paramString)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.asignatura = paramString;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.asignatura = paramString;
      return;
    }
    localStateManager.setStringField(paramActividadDocente, jdoInheritedFieldCount + 2, paramActividadDocente.asignatura, paramString);
  }

  private static final Carrera jdoGetcarrera(ActividadDocente paramActividadDocente)
  {
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.carrera;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 3))
      return paramActividadDocente.carrera;
    return (Carrera)localStateManager.getObjectField(paramActividadDocente, jdoInheritedFieldCount + 3, paramActividadDocente.carrera);
  }

  private static final void jdoSetcarrera(ActividadDocente paramActividadDocente, Carrera paramCarrera)
  {
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.carrera = paramCarrera;
      return;
    }
    localStateManager.setObjectField(paramActividadDocente, jdoInheritedFieldCount + 3, paramActividadDocente.carrera, paramCarrera);
  }

  private static final String jdoGetestatus(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.estatus;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.estatus;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 4))
      return paramActividadDocente.estatus;
    return localStateManager.getStringField(paramActividadDocente, jdoInheritedFieldCount + 4, paramActividadDocente.estatus);
  }

  private static final void jdoSetestatus(ActividadDocente paramActividadDocente, String paramString)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramActividadDocente, jdoInheritedFieldCount + 4, paramActividadDocente.estatus, paramString);
  }

  private static final long jdoGetidActividadDocente(ActividadDocente paramActividadDocente)
  {
    return paramActividadDocente.idActividadDocente;
  }

  private static final void jdoSetidActividadDocente(ActividadDocente paramActividadDocente, long paramLong)
  {
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.idActividadDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramActividadDocente, jdoInheritedFieldCount + 5, paramActividadDocente.idActividadDocente, paramLong);
  }

  private static final int jdoGetidSitp(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.idSitp;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.idSitp;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 6))
      return paramActividadDocente.idSitp;
    return localStateManager.getIntField(paramActividadDocente, jdoInheritedFieldCount + 6, paramActividadDocente.idSitp);
  }

  private static final void jdoSetidSitp(ActividadDocente paramActividadDocente, int paramInt)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramActividadDocente, jdoInheritedFieldCount + 6, paramActividadDocente.idSitp, paramInt);
  }

  private static final String jdoGetnivelEducativo(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.nivelEducativo;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.nivelEducativo;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 7))
      return paramActividadDocente.nivelEducativo;
    return localStateManager.getStringField(paramActividadDocente, jdoInheritedFieldCount + 7, paramActividadDocente.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(ActividadDocente paramActividadDocente, String paramString)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramActividadDocente, jdoInheritedFieldCount + 7, paramActividadDocente.nivelEducativo, paramString);
  }

  private static final String jdoGetnombreEntidad(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.nombreEntidad;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.nombreEntidad;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 8))
      return paramActividadDocente.nombreEntidad;
    return localStateManager.getStringField(paramActividadDocente, jdoInheritedFieldCount + 8, paramActividadDocente.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(ActividadDocente paramActividadDocente, String paramString)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramActividadDocente, jdoInheritedFieldCount + 8, paramActividadDocente.nombreEntidad, paramString);
  }

  private static final String jdoGetobservaciones(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.observaciones;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.observaciones;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 9))
      return paramActividadDocente.observaciones;
    return localStateManager.getStringField(paramActividadDocente, jdoInheritedFieldCount + 9, paramActividadDocente.observaciones);
  }

  private static final void jdoSetobservaciones(ActividadDocente paramActividadDocente, String paramString)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramActividadDocente, jdoInheritedFieldCount + 9, paramActividadDocente.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(ActividadDocente paramActividadDocente)
  {
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.personal;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 10))
      return paramActividadDocente.personal;
    return (Personal)localStateManager.getObjectField(paramActividadDocente, jdoInheritedFieldCount + 10, paramActividadDocente.personal);
  }

  private static final void jdoSetpersonal(ActividadDocente paramActividadDocente, Personal paramPersonal)
  {
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramActividadDocente, jdoInheritedFieldCount + 10, paramActividadDocente.personal, paramPersonal);
  }

  private static final String jdoGetrelacionLaboral(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.relacionLaboral;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.relacionLaboral;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 11))
      return paramActividadDocente.relacionLaboral;
    return localStateManager.getStringField(paramActividadDocente, jdoInheritedFieldCount + 11, paramActividadDocente.relacionLaboral);
  }

  private static final void jdoSetrelacionLaboral(ActividadDocente paramActividadDocente, String paramString)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.relacionLaboral = paramString;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.relacionLaboral = paramString;
      return;
    }
    localStateManager.setStringField(paramActividadDocente, jdoInheritedFieldCount + 11, paramActividadDocente.relacionLaboral, paramString);
  }

  private static final String jdoGetsector(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.sector;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.sector;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 12))
      return paramActividadDocente.sector;
    return localStateManager.getStringField(paramActividadDocente, jdoInheritedFieldCount + 12, paramActividadDocente.sector);
  }

  private static final void jdoSetsector(ActividadDocente paramActividadDocente, String paramString)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.sector = paramString;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.sector = paramString;
      return;
    }
    localStateManager.setStringField(paramActividadDocente, jdoInheritedFieldCount + 12, paramActividadDocente.sector, paramString);
  }

  private static final Date jdoGettiempoSitp(ActividadDocente paramActividadDocente)
  {
    if (paramActividadDocente.jdoFlags <= 0)
      return paramActividadDocente.tiempoSitp;
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramActividadDocente.tiempoSitp;
    if (localStateManager.isLoaded(paramActividadDocente, jdoInheritedFieldCount + 13))
      return paramActividadDocente.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramActividadDocente, jdoInheritedFieldCount + 13, paramActividadDocente.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ActividadDocente paramActividadDocente, Date paramDate)
  {
    if (paramActividadDocente.jdoFlags == 0)
    {
      paramActividadDocente.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividadDocente.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramActividadDocente, jdoInheritedFieldCount + 13, paramActividadDocente.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}