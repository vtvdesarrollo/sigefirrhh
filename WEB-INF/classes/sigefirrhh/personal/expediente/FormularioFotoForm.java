package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.myfaces.custom.fileupload.UploadedFile;

public class FormularioFotoForm extends Form
{
  private UploadedFile docDocumento;
  private String descripcion;
  private String contentType;

  public String getContentType()
  {
    return this.contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public String getDescripcion() {
    return this.descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public UploadedFile getDocDocumento() {
    return this.docDocumento;
  }

  public void setDocDocumento(UploadedFile docDocumento) {
    this.docDocumento = docDocumento;
  }

  public String upload()
  {
    try
    {
      FacesContext facesContext = FacesContext.getCurrentInstance();
      facesContext.getExternalContext().getApplicationMap().put(
        "fileupload_bytes", this.docDocumento.getBytes());
      facesContext.getExternalContext().getApplicationMap().put(
        "fileupload_type", this.docDocumento.getContentType());
      facesContext.getExternalContext().getApplicationMap().put(
        "fileupload_name", this.docDocumento.getName());
      return "ver_documento";
    } catch (Exception x) {
      FacesMessage message = new FacesMessage(
        FacesMessage.SEVERITY_FATAL, x.getClass().getName(), x
        .getMessage());
      FacesContext.getCurrentInstance().addMessage(null, message);
    }return null;
  }
}