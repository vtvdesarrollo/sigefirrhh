package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class OtraActividadPK
  implements Serializable
{
  public long idOtraActividad;

  public OtraActividadPK()
  {
  }

  public OtraActividadPK(long idOtraActividad)
  {
    this.idOtraActividad = idOtraActividad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((OtraActividadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(OtraActividadPK thatPK)
  {
    return 
      this.idOtraActividad == thatPK.idOtraActividad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idOtraActividad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idOtraActividad);
  }
}