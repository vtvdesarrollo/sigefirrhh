package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ProfesionTrabajadorPK
  implements Serializable
{
  public long idProfesionTrabajador;

  public ProfesionTrabajadorPK()
  {
  }

  public ProfesionTrabajadorPK(long idProfesionTrabajador)
  {
    this.idProfesionTrabajador = idProfesionTrabajador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProfesionTrabajadorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProfesionTrabajadorPK thatPK)
  {
    return 
      this.idProfesionTrabajador == thatPK.idProfesionTrabajador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProfesionTrabajador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProfesionTrabajador);
  }
}