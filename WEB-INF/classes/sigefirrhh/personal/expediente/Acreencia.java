package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoAcreencia;

public class Acreencia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idAcreencia;
  private TipoAcreencia tipoAcreencia;
  private double montoAcreencia;
  private String estatus;
  private Date fechaRegistro;
  private String observaciones;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "estatus", "fechaRegistro", "idAcreencia", "montoAcreencia", "observaciones", "personal", "tipoAcreencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.personal.TipoAcreencia") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Acreencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Acreencia());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("P", "PROCESO");
    LISTA_ESTATUS.put("R", "RECLAMO");
    LISTA_ESTATUS.put("C", "CERRADO");
  }

  public Acreencia()
  {
    jdoSetestatus(this, "P");
  }

  public String toString()
  {
    return jdoGettipoAcreencia(this).getDescripcion() + " " + 
      jdoGetmontoAcreencia(this) + " " + 
      jdoGetestatus(this);
  }

  public String getEstatusAcreencia()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public long getIdAcreencia()
  {
    return jdoGetidAcreencia(this);
  }

  public double getMontoAcreencia()
  {
    return jdoGetmontoAcreencia(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoAcreencia getTipoAcreencia()
  {
    return jdoGettipoAcreencia(this);
  }

  public void setEstatusAcreencia(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setIdAcreencia(long l)
  {
    jdoSetidAcreencia(this, l);
  }

  public void setMontoAcreencia(double d)
  {
    jdoSetmontoAcreencia(this, d);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoAcreencia(TipoAcreencia acreencia)
  {
    jdoSettipoAcreencia(this, acreencia);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Acreencia localAcreencia = new Acreencia();
    localAcreencia.jdoFlags = 1;
    localAcreencia.jdoStateManager = paramStateManager;
    return localAcreencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Acreencia localAcreencia = new Acreencia();
    localAcreencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAcreencia.jdoFlags = 1;
    localAcreencia.jdoStateManager = paramStateManager;
    return localAcreencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAcreencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAcreencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoAcreencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAcreencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAcreencia = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoAcreencia = ((TipoAcreencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Acreencia paramAcreencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramAcreencia.estatus;
      return;
    case 1:
      if (paramAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramAcreencia.fechaRegistro;
      return;
    case 2:
      if (paramAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.idAcreencia = paramAcreencia.idAcreencia;
      return;
    case 3:
      if (paramAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.montoAcreencia = paramAcreencia.montoAcreencia;
      return;
    case 4:
      if (paramAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramAcreencia.observaciones;
      return;
    case 5:
      if (paramAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramAcreencia.personal;
      return;
    case 6:
      if (paramAcreencia == null)
        throw new IllegalArgumentException("arg1");
      this.tipoAcreencia = paramAcreencia.tipoAcreencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Acreencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Acreencia localAcreencia = (Acreencia)paramObject;
    if (localAcreencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAcreencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AcreenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AcreenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AcreenciaPK))
      throw new IllegalArgumentException("arg1");
    AcreenciaPK localAcreenciaPK = (AcreenciaPK)paramObject;
    localAcreenciaPK.idAcreencia = this.idAcreencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AcreenciaPK))
      throw new IllegalArgumentException("arg1");
    AcreenciaPK localAcreenciaPK = (AcreenciaPK)paramObject;
    this.idAcreencia = localAcreenciaPK.idAcreencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AcreenciaPK))
      throw new IllegalArgumentException("arg2");
    AcreenciaPK localAcreenciaPK = (AcreenciaPK)paramObject;
    localAcreenciaPK.idAcreencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AcreenciaPK))
      throw new IllegalArgumentException("arg2");
    AcreenciaPK localAcreenciaPK = (AcreenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAcreenciaPK.idAcreencia);
  }

  private static final String jdoGetestatus(Acreencia paramAcreencia)
  {
    if (paramAcreencia.jdoFlags <= 0)
      return paramAcreencia.estatus;
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramAcreencia.estatus;
    if (localStateManager.isLoaded(paramAcreencia, jdoInheritedFieldCount + 0))
      return paramAcreencia.estatus;
    return localStateManager.getStringField(paramAcreencia, jdoInheritedFieldCount + 0, paramAcreencia.estatus);
  }

  private static final void jdoSetestatus(Acreencia paramAcreencia, String paramString)
  {
    if (paramAcreencia.jdoFlags == 0)
    {
      paramAcreencia.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAcreencia.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramAcreencia, jdoInheritedFieldCount + 0, paramAcreencia.estatus, paramString);
  }

  private static final Date jdoGetfechaRegistro(Acreencia paramAcreencia)
  {
    if (paramAcreencia.jdoFlags <= 0)
      return paramAcreencia.fechaRegistro;
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramAcreencia.fechaRegistro;
    if (localStateManager.isLoaded(paramAcreencia, jdoInheritedFieldCount + 1))
      return paramAcreencia.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramAcreencia, jdoInheritedFieldCount + 1, paramAcreencia.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(Acreencia paramAcreencia, Date paramDate)
  {
    if (paramAcreencia.jdoFlags == 0)
    {
      paramAcreencia.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAcreencia.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAcreencia, jdoInheritedFieldCount + 1, paramAcreencia.fechaRegistro, paramDate);
  }

  private static final long jdoGetidAcreencia(Acreencia paramAcreencia)
  {
    return paramAcreencia.idAcreencia;
  }

  private static final void jdoSetidAcreencia(Acreencia paramAcreencia, long paramLong)
  {
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAcreencia.idAcreencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramAcreencia, jdoInheritedFieldCount + 2, paramAcreencia.idAcreencia, paramLong);
  }

  private static final double jdoGetmontoAcreencia(Acreencia paramAcreencia)
  {
    if (paramAcreencia.jdoFlags <= 0)
      return paramAcreencia.montoAcreencia;
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramAcreencia.montoAcreencia;
    if (localStateManager.isLoaded(paramAcreencia, jdoInheritedFieldCount + 3))
      return paramAcreencia.montoAcreencia;
    return localStateManager.getDoubleField(paramAcreencia, jdoInheritedFieldCount + 3, paramAcreencia.montoAcreencia);
  }

  private static final void jdoSetmontoAcreencia(Acreencia paramAcreencia, double paramDouble)
  {
    if (paramAcreencia.jdoFlags == 0)
    {
      paramAcreencia.montoAcreencia = paramDouble;
      return;
    }
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAcreencia.montoAcreencia = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAcreencia, jdoInheritedFieldCount + 3, paramAcreencia.montoAcreencia, paramDouble);
  }

  private static final String jdoGetobservaciones(Acreencia paramAcreencia)
  {
    if (paramAcreencia.jdoFlags <= 0)
      return paramAcreencia.observaciones;
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramAcreencia.observaciones;
    if (localStateManager.isLoaded(paramAcreencia, jdoInheritedFieldCount + 4))
      return paramAcreencia.observaciones;
    return localStateManager.getStringField(paramAcreencia, jdoInheritedFieldCount + 4, paramAcreencia.observaciones);
  }

  private static final void jdoSetobservaciones(Acreencia paramAcreencia, String paramString)
  {
    if (paramAcreencia.jdoFlags == 0)
    {
      paramAcreencia.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAcreencia.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramAcreencia, jdoInheritedFieldCount + 4, paramAcreencia.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Acreencia paramAcreencia)
  {
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramAcreencia.personal;
    if (localStateManager.isLoaded(paramAcreencia, jdoInheritedFieldCount + 5))
      return paramAcreencia.personal;
    return (Personal)localStateManager.getObjectField(paramAcreencia, jdoInheritedFieldCount + 5, paramAcreencia.personal);
  }

  private static final void jdoSetpersonal(Acreencia paramAcreencia, Personal paramPersonal)
  {
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAcreencia.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramAcreencia, jdoInheritedFieldCount + 5, paramAcreencia.personal, paramPersonal);
  }

  private static final TipoAcreencia jdoGettipoAcreencia(Acreencia paramAcreencia)
  {
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
      return paramAcreencia.tipoAcreencia;
    if (localStateManager.isLoaded(paramAcreencia, jdoInheritedFieldCount + 6))
      return paramAcreencia.tipoAcreencia;
    return (TipoAcreencia)localStateManager.getObjectField(paramAcreencia, jdoInheritedFieldCount + 6, paramAcreencia.tipoAcreencia);
  }

  private static final void jdoSettipoAcreencia(Acreencia paramAcreencia, TipoAcreencia paramTipoAcreencia)
  {
    StateManager localStateManager = paramAcreencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAcreencia.tipoAcreencia = paramTipoAcreencia;
      return;
    }
    localStateManager.setObjectField(paramAcreencia, jdoInheritedFieldCount + 6, paramAcreencia.tipoAcreencia, paramTipoAcreencia);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}