package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoAusencia;

public class Ausencia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_HORAS_DIAS;
  protected static final Map LISTA_CESTA_TICKET;
  protected static final Map LISTA_DESCUENTO_STATUS;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_REMUNERADO;
  public static final Map LISTA_CLASE;
  private long idAusencia;
  private TipoAusencia tipoAusencia;
  private Date fechaInicio;
  private Date fechaFin;
  private String horasDias;
  private int diasContinuos;
  private int diasHabiles;
  private String cestaTicket;
  private int mes;
  private int anio;
  private String nombreJefe;
  private String nombreSupervisor;
  private String nombreDirector;
  private String estatus;
  private String descuentoEstatus;
  private String remunerado;
  private String clase;
  private String observaciones;
  private Personal personal;
  private int cedulaJefe;
  private int cedulaSupervisor;
  private int cedulaDirector;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "cedulaDirector", "cedulaJefe", "cedulaSupervisor", "cestaTicket", "clase", "descuentoEstatus", "diasContinuos", "diasHabiles", "estatus", "fechaFin", "fechaInicio", "horasDias", "idAusencia", "mes", "nombreDirector", "nombreJefe", "nombreSupervisor", "observaciones", "personal", "remunerado", "tipoAusencia" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.TipoAusencia") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Ausencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Ausencia());

    LISTA_HORAS_DIAS = 
      new LinkedHashMap();
    LISTA_CESTA_TICKET = 
      new LinkedHashMap();
    LISTA_DESCUENTO_STATUS = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_REMUNERADO = 
      new LinkedHashMap();
    LISTA_CLASE = 
      new LinkedHashMap();

    LISTA_HORAS_DIAS.put("H", "HORAS");
    LISTA_HORAS_DIAS.put("D", "DIAS");
    LISTA_HORAS_DIAS.put("M", "MESES");
    LISTA_HORAS_DIAS.put("A", "AÑOS");
    LISTA_CESTA_TICKET.put("S", "SI");
    LISTA_CESTA_TICKET.put("N", "NO");

    LISTA_ESTATUS.put("P", "PROCESO");
    LISTA_ESTATUS.put("A", "APROBADO");
    LISTA_ESTATUS.put("R", "RECHAZADO");
    LISTA_ESTATUS.put("G", "REGISTRADO");
    LISTA_ESTATUS.put("O", "OTRO");

    LISTA_REMUNERADO.put("S", "SI");
    LISTA_REMUNERADO.put("N", "NO");

    LISTA_CLASE.put("A", "AUSENCIA");
    LISTA_CLASE.put("P", "PERMISO");
    LISTA_CLASE.put("R", "REPOSO");

    LISTA_DESCUENTO_STATUS.put("D", "NO APLICA");
    LISTA_DESCUENTO_STATUS.put("P", "PENDIENTE");
    LISTA_DESCUENTO_STATUS.put("A", "APLICADO");
  }

  public Ausencia()
  {
    jdoSethorasDias(this, "D");

    jdoSetcestaTicket(this, "N");

    jdoSetestatus(this, "A");

    jdoSetdescuentoEstatus(this, "D");

    jdoSetremunerado(this, "N");

    jdoSetclase(this, "A");
  }

  public String toString()
  {
    return jdoGettipoAusencia(this).getDescripcion() + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this));
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public int getCedulaDirector() {
    return jdoGetcedulaDirector(this);
  }

  public int getCedulaJefe() {
    return jdoGetcedulaJefe(this);
  }

  public int getCedulaSupervisor() {
    return jdoGetcedulaSupervisor(this);
  }

  public String getCestaTicket() {
    return jdoGetcestaTicket(this);
  }

  public int getDiasContinuos() {
    return jdoGetdiasContinuos(this);
  }

  public int getDiasHabiles() {
    return jdoGetdiasHabiles(this);
  }

  public String getEstatus() {
    return jdoGetestatus(this);
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public String getHorasDias() {
    return jdoGethorasDias(this);
  }

  public long getIdAusencia() {
    return jdoGetidAusencia(this);
  }

  public int getMes() {
    return jdoGetmes(this);
  }

  public String getNombreDirector() {
    return jdoGetnombreDirector(this);
  }

  public String getNombreJefe() {
    return jdoGetnombreJefe(this);
  }

  public String getNombreSupervisor() {
    return jdoGetnombreSupervisor(this);
  }

  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public TipoAusencia getTipoAusencia() {
    return jdoGettipoAusencia(this);
  }

  public void setAnio(int i) {
    jdoSetanio(this, i);
  }

  public void setCedulaDirector(int i) {
    jdoSetcedulaDirector(this, i);
  }

  public void setCedulaJefe(int i) {
    jdoSetcedulaJefe(this, i);
  }

  public void setCedulaSupervisor(int i) {
    jdoSetcedulaSupervisor(this, i);
  }

  public void setCestaTicket(String string) {
    jdoSetcestaTicket(this, string);
  }

  public void setDiasContinuos(int i) {
    jdoSetdiasContinuos(this, i);
  }

  public void setDiasHabiles(int i) {
    jdoSetdiasHabiles(this, i);
  }

  public void setEstatus(String string) {
    jdoSetestatus(this, string);
  }

  public String getDescuentoEstatus() {
    return jdoGetdescuentoEstatus(this);
  }

  public void setDescuentoEstatus(String descuentoEstatus) {
    jdoSetdescuentoEstatus(this, descuentoEstatus);
  }

  public void setFechaFin(Date date) {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date) {
    jdoSetfechaInicio(this, date);
  }

  public void setHorasDias(String string) {
    jdoSethorasDias(this, string);
  }

  public void setIdAusencia(long l) {
    jdoSetidAusencia(this, l);
  }

  public void setMes(int i) {
    jdoSetmes(this, i);
  }

  public void setNombreDirector(String string) {
    jdoSetnombreDirector(this, string);
  }

  public void setNombreJefe(String string) {
    jdoSetnombreJefe(this, string);
  }

  public void setNombreSupervisor(String string) {
    jdoSetnombreSupervisor(this, string);
  }

  public void setObservaciones(String string) {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public void setTipoAusencia(TipoAusencia ausencia) {
    jdoSettipoAusencia(this, ausencia);
  }

  public String getRemunerado() {
    return jdoGetremunerado(this);
  }

  public void setRemunerado(String string) {
    jdoSetremunerado(this, string);
  }

  public String getClase()
  {
    return jdoGetclase(this);
  }

  public void setClase(String string)
  {
    jdoSetclase(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 22;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Ausencia localAusencia = new Ausencia();
    localAusencia.jdoFlags = 1;
    localAusencia.jdoStateManager = paramStateManager;
    return localAusencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Ausencia localAusencia = new Ausencia();
    localAusencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAusencia.jdoFlags = 1;
    localAusencia.jdoStateManager = paramStateManager;
    return localAusencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaDirector);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaJefe);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaSupervisor);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cestaTicket);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.clase);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descuentoEstatus);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasContinuos);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasHabiles);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.horasDias);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAusencia);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDirector);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreJefe);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSupervisor);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.remunerado);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoAusencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaDirector = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaJefe = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaSupervisor = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cestaTicket = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clase = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descuentoEstatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasContinuos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasHabiles = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horasDias = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAusencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDirector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreJefe = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSupervisor = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remunerado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoAusencia = ((TipoAusencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Ausencia paramAusencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramAusencia.anio;
      return;
    case 1:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaDirector = paramAusencia.cedulaDirector;
      return;
    case 2:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaJefe = paramAusencia.cedulaJefe;
      return;
    case 3:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaSupervisor = paramAusencia.cedulaSupervisor;
      return;
    case 4:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.cestaTicket = paramAusencia.cestaTicket;
      return;
    case 5:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.clase = paramAusencia.clase;
      return;
    case 6:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.descuentoEstatus = paramAusencia.descuentoEstatus;
      return;
    case 7:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.diasContinuos = paramAusencia.diasContinuos;
      return;
    case 8:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.diasHabiles = paramAusencia.diasHabiles;
      return;
    case 9:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramAusencia.estatus;
      return;
    case 10:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramAusencia.fechaFin;
      return;
    case 11:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramAusencia.fechaInicio;
      return;
    case 12:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.horasDias = paramAusencia.horasDias;
      return;
    case 13:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.idAusencia = paramAusencia.idAusencia;
      return;
    case 14:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramAusencia.mes;
      return;
    case 15:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDirector = paramAusencia.nombreDirector;
      return;
    case 16:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombreJefe = paramAusencia.nombreJefe;
      return;
    case 17:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSupervisor = paramAusencia.nombreSupervisor;
      return;
    case 18:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramAusencia.observaciones;
      return;
    case 19:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramAusencia.personal;
      return;
    case 20:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.remunerado = paramAusencia.remunerado;
      return;
    case 21:
      if (paramAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.tipoAusencia = paramAusencia.tipoAusencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Ausencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Ausencia localAusencia = (Ausencia)paramObject;
    if (localAusencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAusencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AusenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AusenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AusenciaPK))
      throw new IllegalArgumentException("arg1");
    AusenciaPK localAusenciaPK = (AusenciaPK)paramObject;
    localAusenciaPK.idAusencia = this.idAusencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AusenciaPK))
      throw new IllegalArgumentException("arg1");
    AusenciaPK localAusenciaPK = (AusenciaPK)paramObject;
    this.idAusencia = localAusenciaPK.idAusencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AusenciaPK))
      throw new IllegalArgumentException("arg2");
    AusenciaPK localAusenciaPK = (AusenciaPK)paramObject;
    localAusenciaPK.idAusencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 13);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AusenciaPK))
      throw new IllegalArgumentException("arg2");
    AusenciaPK localAusenciaPK = (AusenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 13, localAusenciaPK.idAusencia);
  }

  private static final int jdoGetanio(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.anio;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.anio;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 0))
      return paramAusencia.anio;
    return localStateManager.getIntField(paramAusencia, jdoInheritedFieldCount + 0, paramAusencia.anio);
  }

  private static final void jdoSetanio(Ausencia paramAusencia, int paramInt)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramAusencia, jdoInheritedFieldCount + 0, paramAusencia.anio, paramInt);
  }

  private static final int jdoGetcedulaDirector(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.cedulaDirector;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.cedulaDirector;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 1))
      return paramAusencia.cedulaDirector;
    return localStateManager.getIntField(paramAusencia, jdoInheritedFieldCount + 1, paramAusencia.cedulaDirector);
  }

  private static final void jdoSetcedulaDirector(Ausencia paramAusencia, int paramInt)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.cedulaDirector = paramInt;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.cedulaDirector = paramInt;
      return;
    }
    localStateManager.setIntField(paramAusencia, jdoInheritedFieldCount + 1, paramAusencia.cedulaDirector, paramInt);
  }

  private static final int jdoGetcedulaJefe(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.cedulaJefe;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.cedulaJefe;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 2))
      return paramAusencia.cedulaJefe;
    return localStateManager.getIntField(paramAusencia, jdoInheritedFieldCount + 2, paramAusencia.cedulaJefe);
  }

  private static final void jdoSetcedulaJefe(Ausencia paramAusencia, int paramInt)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.cedulaJefe = paramInt;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.cedulaJefe = paramInt;
      return;
    }
    localStateManager.setIntField(paramAusencia, jdoInheritedFieldCount + 2, paramAusencia.cedulaJefe, paramInt);
  }

  private static final int jdoGetcedulaSupervisor(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.cedulaSupervisor;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.cedulaSupervisor;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 3))
      return paramAusencia.cedulaSupervisor;
    return localStateManager.getIntField(paramAusencia, jdoInheritedFieldCount + 3, paramAusencia.cedulaSupervisor);
  }

  private static final void jdoSetcedulaSupervisor(Ausencia paramAusencia, int paramInt)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.cedulaSupervisor = paramInt;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.cedulaSupervisor = paramInt;
      return;
    }
    localStateManager.setIntField(paramAusencia, jdoInheritedFieldCount + 3, paramAusencia.cedulaSupervisor, paramInt);
  }

  private static final String jdoGetcestaTicket(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.cestaTicket;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.cestaTicket;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 4))
      return paramAusencia.cestaTicket;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 4, paramAusencia.cestaTicket);
  }

  private static final void jdoSetcestaTicket(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.cestaTicket = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.cestaTicket = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 4, paramAusencia.cestaTicket, paramString);
  }

  private static final String jdoGetclase(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.clase;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.clase;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 5))
      return paramAusencia.clase;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 5, paramAusencia.clase);
  }

  private static final void jdoSetclase(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.clase = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.clase = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 5, paramAusencia.clase, paramString);
  }

  private static final String jdoGetdescuentoEstatus(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.descuentoEstatus;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.descuentoEstatus;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 6))
      return paramAusencia.descuentoEstatus;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 6, paramAusencia.descuentoEstatus);
  }

  private static final void jdoSetdescuentoEstatus(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.descuentoEstatus = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.descuentoEstatus = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 6, paramAusencia.descuentoEstatus, paramString);
  }

  private static final int jdoGetdiasContinuos(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.diasContinuos;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.diasContinuos;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 7))
      return paramAusencia.diasContinuos;
    return localStateManager.getIntField(paramAusencia, jdoInheritedFieldCount + 7, paramAusencia.diasContinuos);
  }

  private static final void jdoSetdiasContinuos(Ausencia paramAusencia, int paramInt)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.diasContinuos = paramInt;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.diasContinuos = paramInt;
      return;
    }
    localStateManager.setIntField(paramAusencia, jdoInheritedFieldCount + 7, paramAusencia.diasContinuos, paramInt);
  }

  private static final int jdoGetdiasHabiles(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.diasHabiles;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.diasHabiles;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 8))
      return paramAusencia.diasHabiles;
    return localStateManager.getIntField(paramAusencia, jdoInheritedFieldCount + 8, paramAusencia.diasHabiles);
  }

  private static final void jdoSetdiasHabiles(Ausencia paramAusencia, int paramInt)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.diasHabiles = paramInt;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.diasHabiles = paramInt;
      return;
    }
    localStateManager.setIntField(paramAusencia, jdoInheritedFieldCount + 8, paramAusencia.diasHabiles, paramInt);
  }

  private static final String jdoGetestatus(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.estatus;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.estatus;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 9))
      return paramAusencia.estatus;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 9, paramAusencia.estatus);
  }

  private static final void jdoSetestatus(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 9, paramAusencia.estatus, paramString);
  }

  private static final Date jdoGetfechaFin(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.fechaFin;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.fechaFin;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 10))
      return paramAusencia.fechaFin;
    return (Date)localStateManager.getObjectField(paramAusencia, jdoInheritedFieldCount + 10, paramAusencia.fechaFin);
  }

  private static final void jdoSetfechaFin(Ausencia paramAusencia, Date paramDate)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAusencia, jdoInheritedFieldCount + 10, paramAusencia.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.fechaInicio;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.fechaInicio;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 11))
      return paramAusencia.fechaInicio;
    return (Date)localStateManager.getObjectField(paramAusencia, jdoInheritedFieldCount + 11, paramAusencia.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Ausencia paramAusencia, Date paramDate)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAusencia, jdoInheritedFieldCount + 11, paramAusencia.fechaInicio, paramDate);
  }

  private static final String jdoGethorasDias(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.horasDias;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.horasDias;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 12))
      return paramAusencia.horasDias;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 12, paramAusencia.horasDias);
  }

  private static final void jdoSethorasDias(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.horasDias = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.horasDias = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 12, paramAusencia.horasDias, paramString);
  }

  private static final long jdoGetidAusencia(Ausencia paramAusencia)
  {
    return paramAusencia.idAusencia;
  }

  private static final void jdoSetidAusencia(Ausencia paramAusencia, long paramLong)
  {
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.idAusencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramAusencia, jdoInheritedFieldCount + 13, paramAusencia.idAusencia, paramLong);
  }

  private static final int jdoGetmes(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.mes;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.mes;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 14))
      return paramAusencia.mes;
    return localStateManager.getIntField(paramAusencia, jdoInheritedFieldCount + 14, paramAusencia.mes);
  }

  private static final void jdoSetmes(Ausencia paramAusencia, int paramInt)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramAusencia, jdoInheritedFieldCount + 14, paramAusencia.mes, paramInt);
  }

  private static final String jdoGetnombreDirector(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.nombreDirector;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.nombreDirector;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 15))
      return paramAusencia.nombreDirector;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 15, paramAusencia.nombreDirector);
  }

  private static final void jdoSetnombreDirector(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.nombreDirector = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.nombreDirector = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 15, paramAusencia.nombreDirector, paramString);
  }

  private static final String jdoGetnombreJefe(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.nombreJefe;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.nombreJefe;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 16))
      return paramAusencia.nombreJefe;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 16, paramAusencia.nombreJefe);
  }

  private static final void jdoSetnombreJefe(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.nombreJefe = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.nombreJefe = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 16, paramAusencia.nombreJefe, paramString);
  }

  private static final String jdoGetnombreSupervisor(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.nombreSupervisor;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.nombreSupervisor;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 17))
      return paramAusencia.nombreSupervisor;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 17, paramAusencia.nombreSupervisor);
  }

  private static final void jdoSetnombreSupervisor(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.nombreSupervisor = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.nombreSupervisor = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 17, paramAusencia.nombreSupervisor, paramString);
  }

  private static final String jdoGetobservaciones(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.observaciones;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.observaciones;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 18))
      return paramAusencia.observaciones;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 18, paramAusencia.observaciones);
  }

  private static final void jdoSetobservaciones(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 18, paramAusencia.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Ausencia paramAusencia)
  {
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.personal;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 19))
      return paramAusencia.personal;
    return (Personal)localStateManager.getObjectField(paramAusencia, jdoInheritedFieldCount + 19, paramAusencia.personal);
  }

  private static final void jdoSetpersonal(Ausencia paramAusencia, Personal paramPersonal)
  {
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramAusencia, jdoInheritedFieldCount + 19, paramAusencia.personal, paramPersonal);
  }

  private static final String jdoGetremunerado(Ausencia paramAusencia)
  {
    if (paramAusencia.jdoFlags <= 0)
      return paramAusencia.remunerado;
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.remunerado;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 20))
      return paramAusencia.remunerado;
    return localStateManager.getStringField(paramAusencia, jdoInheritedFieldCount + 20, paramAusencia.remunerado);
  }

  private static final void jdoSetremunerado(Ausencia paramAusencia, String paramString)
  {
    if (paramAusencia.jdoFlags == 0)
    {
      paramAusencia.remunerado = paramString;
      return;
    }
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.remunerado = paramString;
      return;
    }
    localStateManager.setStringField(paramAusencia, jdoInheritedFieldCount + 20, paramAusencia.remunerado, paramString);
  }

  private static final TipoAusencia jdoGettipoAusencia(Ausencia paramAusencia)
  {
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramAusencia.tipoAusencia;
    if (localStateManager.isLoaded(paramAusencia, jdoInheritedFieldCount + 21))
      return paramAusencia.tipoAusencia;
    return (TipoAusencia)localStateManager.getObjectField(paramAusencia, jdoInheritedFieldCount + 21, paramAusencia.tipoAusencia);
  }

  private static final void jdoSettipoAusencia(Ausencia paramAusencia, TipoAusencia paramTipoAusencia)
  {
    StateManager localStateManager = paramAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusencia.tipoAusencia = paramTipoAusencia;
      return;
    }
    localStateManager.setObjectField(paramAusencia, jdoInheritedFieldCount + 21, paramAusencia.tipoAusencia, paramTipoAusencia);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}