package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

public class TrayectoriaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(TrayectoriaBeanBusiness.class.getName());

  public void addTrayectoria(Trayectoria trayectoria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Trayectoria trayectoriaNew = 
      (Trayectoria)BeanUtils.cloneBean(
      trayectoria);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (trayectoriaNew.getPersonal() != null) {
      trayectoriaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        trayectoriaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(trayectoriaNew);
  }

  public void updateTrayectoria(Trayectoria trayectoria) throws Exception
  {
    Trayectoria trayectoriaModify = 
      findTrayectoriaById(trayectoria.getIdTrayectoria());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (trayectoria.getPersonal() != null) {
      trayectoria.setPersonal(
        personalBeanBusiness.findPersonalById(
        trayectoria.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(trayectoriaModify, trayectoria);
  }

  public void deleteTrayectoria(Trayectoria trayectoria) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Trayectoria trayectoriaDelete = 
      findTrayectoriaById(trayectoria.getIdTrayectoria());
    pm.deletePersistent(trayectoriaDelete);
  }

  public Trayectoria findTrayectoriaById(long idTrayectoria) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTrayectoria == pIdTrayectoria";
    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("long pIdTrayectoria");

    parameters.put("pIdTrayectoria", new Long(idTrayectoria));

    Collection colTrayectoria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTrayectoria.iterator();
    return (Trayectoria)iterator.next();
  }

  public Collection findTrayectoriaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent trayectoriaExtent = pm.getExtent(
      Trayectoria.class, true);
    Query query = pm.newQuery(trayectoriaExtent);
    query.setOrdering("fechaVigencia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaVigencia ascending");

    Collection colTrayectoria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrayectoria);

    return colTrayectoria;
  }

  public Trayectoria findTrayectoriaByIdPersonal(long idPersonal) throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.idPersonal == pIdPersonal";
    Query query = pm.newQuery(Trayectoria.class, filter);

    query.declareParameters("long pIdPersonal");

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPersonal.iterator();
    if (iterator.hasNext())
      return (Trayectoria)iterator.next();
    this.log.error("Advertencia: No se encontro Trayectoria para el idPersonal = " + idPersonal);
    return null;
  }
}