package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class SancionPK
  implements Serializable
{
  public long idSancion;

  public SancionPK()
  {
  }

  public SancionPK(long idSancion)
  {
    this.idSancion = idSancion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SancionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SancionPK thatPK)
  {
    return 
      this.idSancion == thatPK.idSancion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSancion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSancion);
  }
}