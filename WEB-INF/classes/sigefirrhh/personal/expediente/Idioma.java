package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoIdioma;

public class Idioma
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_IDIOMA;
  private long idIdioma;
  private TipoIdioma tipoIdioma;
  private String habla;
  private String lee;
  private String escribe;
  private String nombreEntidad;
  private String examenSuficiencia;
  private String entidadSuficiencia;
  private Date fechaSuficiencia;
  private String examenAcademico;
  private String entidadAcademica;
  private Date fechaAcademica;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "entidadAcademica", "entidadSuficiencia", "escribe", "examenAcademico", "examenSuficiencia", "fechaAcademica", "fechaSuficiencia", "habla", "idIdioma", "idSitp", "lee", "nombreEntidad", "personal", "tiempoSitp", "tipoIdioma" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.TipoIdioma") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Idioma"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Idioma());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_IDIOMA = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_IDIOMA.put("B", "BASICO");
    LISTA_IDIOMA.put("I", "INTERMEDIO");
    LISTA_IDIOMA.put("A", "AVANZADO");
    LISTA_IDIOMA.put("N", "NO");
  }

  public Idioma()
  {
    jdoSethabla(this, "N");

    jdoSetlee(this, "N");

    jdoSetescribe(this, "N");

    jdoSetexamenSuficiencia(this, "N");

    jdoSetexamenAcademico(this, "N");
  }

  public String toString()
  {
    return jdoGettipoIdioma(this).getDescripcion();
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public String getEscribe()
  {
    return jdoGetescribe(this);
  }

  public String getExamenAcademico()
  {
    return jdoGetexamenAcademico(this);
  }

  public String getExamenSuficiencia()
  {
    return jdoGetexamenSuficiencia(this);
  }

  public String getHabla()
  {
    return jdoGethabla(this);
  }

  public long getIdIdioma()
  {
    return jdoGetidIdioma(this);
  }

  public String getLee()
  {
    return jdoGetlee(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoIdioma getTipoIdioma()
  {
    return jdoGettipoIdioma(this);
  }

  public void setNombreEntidad(String string)
  {
    jdoSetnombreEntidad(this, string);
  }

  public void setEscribe(String string)
  {
    jdoSetescribe(this, string);
  }

  public void setExamenAcademico(String string)
  {
    jdoSetexamenAcademico(this, string);
  }

  public void setExamenSuficiencia(String string)
  {
    jdoSetexamenSuficiencia(this, string);
  }

  public void setHabla(String string)
  {
    jdoSethabla(this, string);
  }

  public void setIdIdioma(long l)
  {
    jdoSetidIdioma(this, l);
  }

  public void setLee(String string)
  {
    jdoSetlee(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoIdioma(TipoIdioma idioma)
  {
    jdoSettipoIdioma(this, idioma);
  }

  public String getEntidadSuficiencia() {
    return jdoGetentidadSuficiencia(this);
  }

  public Date getFechaSuficiencia() {
    return jdoGetfechaSuficiencia(this);
  }

  public void setEntidadSuficiencia(String string) {
    jdoSetentidadSuficiencia(this, string);
  }

  public void setFechaSuficiencia(Date date) {
    jdoSetfechaSuficiencia(this, date);
  }

  public String getEntidadAcademica() {
    return jdoGetentidadAcademica(this);
  }

  public void setEntidadAcademica(String string) {
    jdoSetentidadAcademica(this, string);
  }

  public Date getFechaAcademica() {
    return jdoGetfechaAcademica(this);
  }

  public void setFechaAcademica(Date date) {
    jdoSetfechaAcademica(this, date);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Idioma localIdioma = new Idioma();
    localIdioma.jdoFlags = 1;
    localIdioma.jdoStateManager = paramStateManager;
    return localIdioma;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Idioma localIdioma = new Idioma();
    localIdioma.jdoCopyKeyFieldsFromObjectId(paramObject);
    localIdioma.jdoFlags = 1;
    localIdioma.jdoStateManager = paramStateManager;
    return localIdioma;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.entidadAcademica);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.entidadSuficiencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.escribe);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.examenAcademico);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.examenSuficiencia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaAcademica);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaSuficiencia);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.habla);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idIdioma);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.lee);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoIdioma);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entidadAcademica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entidadSuficiencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.escribe = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.examenAcademico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.examenSuficiencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaAcademica = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaSuficiencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.habla = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idIdioma = localStateManager.replacingLongField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lee = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoIdioma = ((TipoIdioma)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Idioma paramIdioma, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.entidadAcademica = paramIdioma.entidadAcademica;
      return;
    case 1:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.entidadSuficiencia = paramIdioma.entidadSuficiencia;
      return;
    case 2:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.escribe = paramIdioma.escribe;
      return;
    case 3:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.examenAcademico = paramIdioma.examenAcademico;
      return;
    case 4:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.examenSuficiencia = paramIdioma.examenSuficiencia;
      return;
    case 5:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.fechaAcademica = paramIdioma.fechaAcademica;
      return;
    case 6:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.fechaSuficiencia = paramIdioma.fechaSuficiencia;
      return;
    case 7:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.habla = paramIdioma.habla;
      return;
    case 8:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.idIdioma = paramIdioma.idIdioma;
      return;
    case 9:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramIdioma.idSitp;
      return;
    case 10:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.lee = paramIdioma.lee;
      return;
    case 11:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramIdioma.nombreEntidad;
      return;
    case 12:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramIdioma.personal;
      return;
    case 13:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramIdioma.tiempoSitp;
      return;
    case 14:
      if (paramIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.tipoIdioma = paramIdioma.tipoIdioma;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Idioma))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Idioma localIdioma = (Idioma)paramObject;
    if (localIdioma.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localIdioma, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new IdiomaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new IdiomaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof IdiomaPK))
      throw new IllegalArgumentException("arg1");
    IdiomaPK localIdiomaPK = (IdiomaPK)paramObject;
    localIdiomaPK.idIdioma = this.idIdioma;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof IdiomaPK))
      throw new IllegalArgumentException("arg1");
    IdiomaPK localIdiomaPK = (IdiomaPK)paramObject;
    this.idIdioma = localIdiomaPK.idIdioma;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof IdiomaPK))
      throw new IllegalArgumentException("arg2");
    IdiomaPK localIdiomaPK = (IdiomaPK)paramObject;
    localIdiomaPK.idIdioma = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 8);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof IdiomaPK))
      throw new IllegalArgumentException("arg2");
    IdiomaPK localIdiomaPK = (IdiomaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 8, localIdiomaPK.idIdioma);
  }

  private static final String jdoGetentidadAcademica(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.entidadAcademica;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.entidadAcademica;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 0))
      return paramIdioma.entidadAcademica;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 0, paramIdioma.entidadAcademica);
  }

  private static final void jdoSetentidadAcademica(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.entidadAcademica = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.entidadAcademica = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 0, paramIdioma.entidadAcademica, paramString);
  }

  private static final String jdoGetentidadSuficiencia(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.entidadSuficiencia;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.entidadSuficiencia;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 1))
      return paramIdioma.entidadSuficiencia;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 1, paramIdioma.entidadSuficiencia);
  }

  private static final void jdoSetentidadSuficiencia(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.entidadSuficiencia = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.entidadSuficiencia = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 1, paramIdioma.entidadSuficiencia, paramString);
  }

  private static final String jdoGetescribe(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.escribe;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.escribe;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 2))
      return paramIdioma.escribe;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 2, paramIdioma.escribe);
  }

  private static final void jdoSetescribe(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.escribe = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.escribe = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 2, paramIdioma.escribe, paramString);
  }

  private static final String jdoGetexamenAcademico(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.examenAcademico;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.examenAcademico;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 3))
      return paramIdioma.examenAcademico;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 3, paramIdioma.examenAcademico);
  }

  private static final void jdoSetexamenAcademico(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.examenAcademico = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.examenAcademico = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 3, paramIdioma.examenAcademico, paramString);
  }

  private static final String jdoGetexamenSuficiencia(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.examenSuficiencia;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.examenSuficiencia;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 4))
      return paramIdioma.examenSuficiencia;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 4, paramIdioma.examenSuficiencia);
  }

  private static final void jdoSetexamenSuficiencia(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.examenSuficiencia = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.examenSuficiencia = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 4, paramIdioma.examenSuficiencia, paramString);
  }

  private static final Date jdoGetfechaAcademica(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.fechaAcademica;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.fechaAcademica;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 5))
      return paramIdioma.fechaAcademica;
    return (Date)localStateManager.getObjectField(paramIdioma, jdoInheritedFieldCount + 5, paramIdioma.fechaAcademica);
  }

  private static final void jdoSetfechaAcademica(Idioma paramIdioma, Date paramDate)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.fechaAcademica = paramDate;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.fechaAcademica = paramDate;
      return;
    }
    localStateManager.setObjectField(paramIdioma, jdoInheritedFieldCount + 5, paramIdioma.fechaAcademica, paramDate);
  }

  private static final Date jdoGetfechaSuficiencia(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.fechaSuficiencia;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.fechaSuficiencia;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 6))
      return paramIdioma.fechaSuficiencia;
    return (Date)localStateManager.getObjectField(paramIdioma, jdoInheritedFieldCount + 6, paramIdioma.fechaSuficiencia);
  }

  private static final void jdoSetfechaSuficiencia(Idioma paramIdioma, Date paramDate)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.fechaSuficiencia = paramDate;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.fechaSuficiencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramIdioma, jdoInheritedFieldCount + 6, paramIdioma.fechaSuficiencia, paramDate);
  }

  private static final String jdoGethabla(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.habla;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.habla;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 7))
      return paramIdioma.habla;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 7, paramIdioma.habla);
  }

  private static final void jdoSethabla(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.habla = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.habla = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 7, paramIdioma.habla, paramString);
  }

  private static final long jdoGetidIdioma(Idioma paramIdioma)
  {
    return paramIdioma.idIdioma;
  }

  private static final void jdoSetidIdioma(Idioma paramIdioma, long paramLong)
  {
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.idIdioma = paramLong;
      return;
    }
    localStateManager.setLongField(paramIdioma, jdoInheritedFieldCount + 8, paramIdioma.idIdioma, paramLong);
  }

  private static final int jdoGetidSitp(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.idSitp;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.idSitp;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 9))
      return paramIdioma.idSitp;
    return localStateManager.getIntField(paramIdioma, jdoInheritedFieldCount + 9, paramIdioma.idSitp);
  }

  private static final void jdoSetidSitp(Idioma paramIdioma, int paramInt)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramIdioma, jdoInheritedFieldCount + 9, paramIdioma.idSitp, paramInt);
  }

  private static final String jdoGetlee(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.lee;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.lee;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 10))
      return paramIdioma.lee;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 10, paramIdioma.lee);
  }

  private static final void jdoSetlee(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.lee = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.lee = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 10, paramIdioma.lee, paramString);
  }

  private static final String jdoGetnombreEntidad(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.nombreEntidad;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.nombreEntidad;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 11))
      return paramIdioma.nombreEntidad;
    return localStateManager.getStringField(paramIdioma, jdoInheritedFieldCount + 11, paramIdioma.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(Idioma paramIdioma, String paramString)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramIdioma, jdoInheritedFieldCount + 11, paramIdioma.nombreEntidad, paramString);
  }

  private static final Personal jdoGetpersonal(Idioma paramIdioma)
  {
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.personal;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 12))
      return paramIdioma.personal;
    return (Personal)localStateManager.getObjectField(paramIdioma, jdoInheritedFieldCount + 12, paramIdioma.personal);
  }

  private static final void jdoSetpersonal(Idioma paramIdioma, Personal paramPersonal)
  {
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramIdioma, jdoInheritedFieldCount + 12, paramIdioma.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Idioma paramIdioma)
  {
    if (paramIdioma.jdoFlags <= 0)
      return paramIdioma.tiempoSitp;
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.tiempoSitp;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 13))
      return paramIdioma.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramIdioma, jdoInheritedFieldCount + 13, paramIdioma.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Idioma paramIdioma, Date paramDate)
  {
    if (paramIdioma.jdoFlags == 0)
    {
      paramIdioma.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramIdioma, jdoInheritedFieldCount + 13, paramIdioma.tiempoSitp, paramDate);
  }

  private static final TipoIdioma jdoGettipoIdioma(Idioma paramIdioma)
  {
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramIdioma.tipoIdioma;
    if (localStateManager.isLoaded(paramIdioma, jdoInheritedFieldCount + 14))
      return paramIdioma.tipoIdioma;
    return (TipoIdioma)localStateManager.getObjectField(paramIdioma, jdoInheritedFieldCount + 14, paramIdioma.tipoIdioma);
  }

  private static final void jdoSettipoIdioma(Idioma paramIdioma, TipoIdioma paramTipoIdioma)
  {
    StateManager localStateManager = paramIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramIdioma.tipoIdioma = paramTipoIdioma;
      return;
    }
    localStateManager.setObjectField(paramIdioma, jdoInheritedFieldCount + 14, paramIdioma.tipoIdioma, paramTipoIdioma);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}