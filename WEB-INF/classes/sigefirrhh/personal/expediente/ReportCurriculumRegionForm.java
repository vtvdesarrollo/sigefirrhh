package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportCurriculumRegionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportCurriculumRegionForm.class.getName());
  private int reportId;
  private long idRegion;
  private String reportName;
  private String tipo = "1";
  private Collection listRegion;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;

  public ReportCurriculumRegionForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.reportName = "curriculumregion";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportCurriculumRegionForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRegion = this.estructuraFacade.findAllRegion();
    } catch (Exception e) {
      this.listRegion = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      log.error("paso 0 tipo--" + this.tipo);
      if (this.tipo != null)
        if (this.tipo.equals("1")) {
          this.reportName = "curriculumregion";
          log.error("paso 1 tipo-> " + this.tipo);
        } else if (this.tipo.equals("2")) {
          this.reportName = "curriculumregionsintra";
          log.error("paso 1 tipo-> " + this.tipo);
        }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      if (this.tipo != null) {
        if (this.tipo.equals("1"))
          this.reportName = "curriculumregion";
        else if (this.tipo.equals("2")) {
          this.reportName = "curriculumregionsintra";
        }
      }

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/expediente");
      parameters.put("id_region", new Long(this.idRegion));

      JasperForWeb report = new JasperForWeb();
      log.error("paso 2  ");
      report.setReportName(this.reportName);
      log.error("paso 3 ");
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/expediente");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListRegion()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public long getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(long l) {
    this.idRegion = l;
  }

  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
}