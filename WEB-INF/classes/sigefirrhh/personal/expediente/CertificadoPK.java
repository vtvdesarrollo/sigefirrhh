package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class CertificadoPK
  implements Serializable
{
  public long idCertificado;

  public CertificadoPK()
  {
  }

  public CertificadoPK(long idCertificado)
  {
    this.idCertificado = idCertificado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CertificadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CertificadoPK thatPK)
  {
    return 
      this.idCertificado == thatPK.idCertificado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCertificado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCertificado);
  }
}