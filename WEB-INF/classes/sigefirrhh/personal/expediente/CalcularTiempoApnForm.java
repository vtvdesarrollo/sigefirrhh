package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIData;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;

public class CalcularTiempoApnForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularTiempoApnForm.class.getName());
  private Collection result;
  private Collection resultAntecedentes;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteNoGenFacade expedienteFacade = new ExpedienteNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private DefinicionesFacade defincionesFacade = new DefinicionesFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showResultAntecedentes;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private int totalAnios = 0;
  private int totalMeses = 0;
  private int totalDias = 0;
  private UIData tableAntecedentes;
  private Object stateResultPersonal = null;

  private Object stateResultTrayectoriaByPersonal = null;

  public Collection getResult()
  {
    return this.result;
  }

  public CalcularTiempoApnForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public void refresh()
  {
  }

  public void findAntecedentesApn()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.resultAntecedentes = null;
      this.showResult = false;

      this.resultAntecedentes = 
        this.expedienteFacade.findAntecedentesApn(this.personal.getIdPersonal());
      this.showResultAntecedentes = 
        ((this.resultAntecedentes != null) && (!this.resultAntecedentes.isEmpty()));

      if (!this.showResultAntecedentes)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se encontraron registros de antecedentes", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findTrayectoriaByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findTrayectoriaByPersonal(
          this.personal.getIdPersonal(), "4", "S");
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);

      findAntecedentesApn();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  public String calcular() {
    try {
      AntecedenteAux antecedenteAux = this.expedienteFacade.calcularTiempoApn(this.resultAntecedentes);
      this.totalAnios = antecedenteAux.getAnios();
      this.totalMeses = antecedenteAux.getMeses();
      this.totalDias = antecedenteAux.getDias();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }
  public String delete() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      AntecedenteAux antecedente = (AntecedenteAux)this.tableAntecedentes.getRowData();
      this.resultAntecedentes.remove(antecedente);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }
  public String save() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.personal.setAniosServicioApn(this.totalAnios);
      this.personal.setMesesServicioApn(this.totalMeses);
      this.personal.setDiasServicioApn(this.totalDias);
      this.expedienteFacade.updatePersonal(this.personal);

      context.addMessage("success_modify", new FacesMessage("Se actualizó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al actualizar" + e.toString(), ""));
    }
    return null;
  }
  private void resetResult() {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;

    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;

    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }
  public boolean isDeleting() {
    return this.deleting;
  }
  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return this.selectedPersonal;
  }
  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public Collection getResultAntecedentes() {
    return this.resultAntecedentes;
  }

  public boolean isShowResultAntecedentes() {
    return this.showResultAntecedentes;
  }

  public int getTotalAnios() {
    return this.totalAnios;
  }

  public int getTotalDias() {
    return this.totalDias;
  }

  public int getTotalMeses() {
    return this.totalMeses;
  }

  public UIData getTableAntecedentes() {
    return this.tableAntecedentes;
  }

  public void setTableAntecedentes(UIData tableAntecedentes) {
    this.tableAntecedentes = tableAntecedentes;
  }
}