package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportMadresPadresForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportMadresPadresForm.class.getName());
  private int reportId;
  private long idTipoPersonal;
  private long idRegion;
  private String reportName;
  private String sexo = "F";
  private String orden = "A";
  private Collection listTipoPersonal;
  private Collection listRegion;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private String agrupado = "G";

  public ReportMadresPadresForm() { FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.reportName = "madrespadresalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportMadresPadresForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listRegion = this.estructuraFacade.findAllRegion();
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
      this.listRegion = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try {
      this.reportName = "madrespadres";
      log.error("orden= " + this.orden);
      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }
      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }
      if (this.agrupado.equals("D"))
        this.reportName += "dep";
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      cambiarNombreAReporte();

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }
      parameters.put("sexo", this.sexo);
      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/expediente");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int i) {
    this.reportId = i;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getSexo()
  {
    return this.sexo;
  }

  public void setSexo(String sexo)
  {
    this.sexo = sexo;
  }

  public long getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(long idRegion)
  {
    this.idRegion = idRegion;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setOrden(String orden)
  {
    this.orden = orden;
  }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getAgrupado()
  {
    return this.agrupado;
  }

  public void setAgrupado(String agrupado)
  {
    this.agrupado = agrupado;
  }
}