package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoHabilidad;

public class Habilidad
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_NIVEL;
  private long idHabilidad;
  private TipoHabilidad tipoHabilidad;
  private String nivel;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "idHabilidad", "idSitp", "nivel", "personal", "tiempoSitp", "tipoHabilidad" }; private static final Class[] jdoFieldTypes = { Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.TipoHabilidad") }; private static final byte[] jdoFieldFlags = { 24, 21, 21, 26, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Habilidad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Habilidad());

    LISTA_NIVEL = 
      new LinkedHashMap();
    LISTA_NIVEL.put("A", "ALTO");
    LISTA_NIVEL.put("M", "MEDIO");
    LISTA_NIVEL.put("B", "BAJO");
  }

  public Habilidad()
  {
    jdoSetnivel(this, "A");
  }

  public String toString()
  {
    return jdoGettipoHabilidad(this).getDescripcion();
  }

  public long getIdHabilidad()
  {
    return jdoGetidHabilidad(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoHabilidad getTipoHabilidad()
  {
    return jdoGettipoHabilidad(this);
  }

  public void setIdHabilidad(long l)
  {
    jdoSetidHabilidad(this, l);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoHabilidad(TipoHabilidad habilidad)
  {
    jdoSettipoHabilidad(this, habilidad);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public String getNivel()
  {
    return jdoGetnivel(this);
  }

  public void setNivel(String string)
  {
    jdoSetnivel(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Habilidad localHabilidad = new Habilidad();
    localHabilidad.jdoFlags = 1;
    localHabilidad.jdoStateManager = paramStateManager;
    return localHabilidad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Habilidad localHabilidad = new Habilidad();
    localHabilidad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHabilidad.jdoFlags = 1;
    localHabilidad.jdoStateManager = paramStateManager;
    return localHabilidad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHabilidad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivel);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoHabilidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHabilidad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoHabilidad = ((TipoHabilidad)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Habilidad paramHabilidad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.idHabilidad = paramHabilidad.idHabilidad;
      return;
    case 1:
      if (paramHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramHabilidad.idSitp;
      return;
    case 2:
      if (paramHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramHabilidad.nivel;
      return;
    case 3:
      if (paramHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramHabilidad.personal;
      return;
    case 4:
      if (paramHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramHabilidad.tiempoSitp;
      return;
    case 5:
      if (paramHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.tipoHabilidad = paramHabilidad.tipoHabilidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Habilidad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Habilidad localHabilidad = (Habilidad)paramObject;
    if (localHabilidad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHabilidad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HabilidadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HabilidadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HabilidadPK))
      throw new IllegalArgumentException("arg1");
    HabilidadPK localHabilidadPK = (HabilidadPK)paramObject;
    localHabilidadPK.idHabilidad = this.idHabilidad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HabilidadPK))
      throw new IllegalArgumentException("arg1");
    HabilidadPK localHabilidadPK = (HabilidadPK)paramObject;
    this.idHabilidad = localHabilidadPK.idHabilidad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HabilidadPK))
      throw new IllegalArgumentException("arg2");
    HabilidadPK localHabilidadPK = (HabilidadPK)paramObject;
    localHabilidadPK.idHabilidad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HabilidadPK))
      throw new IllegalArgumentException("arg2");
    HabilidadPK localHabilidadPK = (HabilidadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localHabilidadPK.idHabilidad);
  }

  private static final long jdoGetidHabilidad(Habilidad paramHabilidad)
  {
    return paramHabilidad.idHabilidad;
  }

  private static final void jdoSetidHabilidad(Habilidad paramHabilidad, long paramLong)
  {
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidad.idHabilidad = paramLong;
      return;
    }
    localStateManager.setLongField(paramHabilidad, jdoInheritedFieldCount + 0, paramHabilidad.idHabilidad, paramLong);
  }

  private static final int jdoGetidSitp(Habilidad paramHabilidad)
  {
    if (paramHabilidad.jdoFlags <= 0)
      return paramHabilidad.idSitp;
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidad.idSitp;
    if (localStateManager.isLoaded(paramHabilidad, jdoInheritedFieldCount + 1))
      return paramHabilidad.idSitp;
    return localStateManager.getIntField(paramHabilidad, jdoInheritedFieldCount + 1, paramHabilidad.idSitp);
  }

  private static final void jdoSetidSitp(Habilidad paramHabilidad, int paramInt)
  {
    if (paramHabilidad.jdoFlags == 0)
    {
      paramHabilidad.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidad.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramHabilidad, jdoInheritedFieldCount + 1, paramHabilidad.idSitp, paramInt);
  }

  private static final String jdoGetnivel(Habilidad paramHabilidad)
  {
    if (paramHabilidad.jdoFlags <= 0)
      return paramHabilidad.nivel;
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidad.nivel;
    if (localStateManager.isLoaded(paramHabilidad, jdoInheritedFieldCount + 2))
      return paramHabilidad.nivel;
    return localStateManager.getStringField(paramHabilidad, jdoInheritedFieldCount + 2, paramHabilidad.nivel);
  }

  private static final void jdoSetnivel(Habilidad paramHabilidad, String paramString)
  {
    if (paramHabilidad.jdoFlags == 0)
    {
      paramHabilidad.nivel = paramString;
      return;
    }
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidad.nivel = paramString;
      return;
    }
    localStateManager.setStringField(paramHabilidad, jdoInheritedFieldCount + 2, paramHabilidad.nivel, paramString);
  }

  private static final Personal jdoGetpersonal(Habilidad paramHabilidad)
  {
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidad.personal;
    if (localStateManager.isLoaded(paramHabilidad, jdoInheritedFieldCount + 3))
      return paramHabilidad.personal;
    return (Personal)localStateManager.getObjectField(paramHabilidad, jdoInheritedFieldCount + 3, paramHabilidad.personal);
  }

  private static final void jdoSetpersonal(Habilidad paramHabilidad, Personal paramPersonal)
  {
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidad.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramHabilidad, jdoInheritedFieldCount + 3, paramHabilidad.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Habilidad paramHabilidad)
  {
    if (paramHabilidad.jdoFlags <= 0)
      return paramHabilidad.tiempoSitp;
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidad.tiempoSitp;
    if (localStateManager.isLoaded(paramHabilidad, jdoInheritedFieldCount + 4))
      return paramHabilidad.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramHabilidad, jdoInheritedFieldCount + 4, paramHabilidad.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Habilidad paramHabilidad, Date paramDate)
  {
    if (paramHabilidad.jdoFlags == 0)
    {
      paramHabilidad.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidad.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHabilidad, jdoInheritedFieldCount + 4, paramHabilidad.tiempoSitp, paramDate);
  }

  private static final TipoHabilidad jdoGettipoHabilidad(Habilidad paramHabilidad)
  {
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidad.tipoHabilidad;
    if (localStateManager.isLoaded(paramHabilidad, jdoInheritedFieldCount + 5))
      return paramHabilidad.tipoHabilidad;
    return (TipoHabilidad)localStateManager.getObjectField(paramHabilidad, jdoInheritedFieldCount + 5, paramHabilidad.tipoHabilidad);
  }

  private static final void jdoSettipoHabilidad(Habilidad paramHabilidad, TipoHabilidad paramTipoHabilidad)
  {
    StateManager localStateManager = paramHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidad.tipoHabilidad = paramTipoHabilidad;
      return;
    }
    localStateManager.setObjectField(paramHabilidad, jdoInheritedFieldCount + 5, paramHabilidad.tipoHabilidad, paramTipoHabilidad);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}