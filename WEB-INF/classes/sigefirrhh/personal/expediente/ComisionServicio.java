package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.mre.SedeDiplomatica;

public class ComisionServicio
  implements Serializable, PersistenceCapable
{
  private long idComisionServicio;
  private String nombreInstitucion;
  private Date fechaInicio;
  private Date fechaFin;
  private SedeDiplomatica sedeDiplomatica;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fechaFin", "fechaInicio", "idComisionServicio", "idSitp", "nombreInstitucion", "observaciones", "personal", "sedeDiplomatica", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.mre.SedeDiplomatica"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombreInstitucion(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this));
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public long getIdComisionServicio() {
    return jdoGetidComisionServicio(this);
  }

  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public SedeDiplomatica getSedeDiplomatica() {
    return jdoGetsedeDiplomatica(this);
  }

  public void setFechaFin(Date date) {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date) {
    jdoSetfechaInicio(this, date);
  }

  public void setIdComisionServicio(long l) {
    jdoSetidComisionServicio(this, l);
  }

  public void setObservaciones(String string) {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public void setSedeDiplomatica(SedeDiplomatica diplomatica) {
    jdoSetsedeDiplomatica(this, diplomatica);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public String getNombreInstitucion()
  {
    return jdoGetnombreInstitucion(this);
  }

  public void setNombreInstitucion(String string)
  {
    jdoSetnombreInstitucion(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.ComisionServicio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ComisionServicio());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ComisionServicio localComisionServicio = new ComisionServicio();
    localComisionServicio.jdoFlags = 1;
    localComisionServicio.jdoStateManager = paramStateManager;
    return localComisionServicio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ComisionServicio localComisionServicio = new ComisionServicio();
    localComisionServicio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localComisionServicio.jdoFlags = 1;
    localComisionServicio.jdoStateManager = paramStateManager;
    return localComisionServicio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idComisionServicio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInstitucion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sedeDiplomatica);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idComisionServicio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sedeDiplomatica = ((SedeDiplomatica)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ComisionServicio paramComisionServicio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramComisionServicio.fechaFin;
      return;
    case 1:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramComisionServicio.fechaInicio;
      return;
    case 2:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.idComisionServicio = paramComisionServicio.idComisionServicio;
      return;
    case 3:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramComisionServicio.idSitp;
      return;
    case 4:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInstitucion = paramComisionServicio.nombreInstitucion;
      return;
    case 5:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramComisionServicio.observaciones;
      return;
    case 6:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramComisionServicio.personal;
      return;
    case 7:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.sedeDiplomatica = paramComisionServicio.sedeDiplomatica;
      return;
    case 8:
      if (paramComisionServicio == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramComisionServicio.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ComisionServicio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ComisionServicio localComisionServicio = (ComisionServicio)paramObject;
    if (localComisionServicio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localComisionServicio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ComisionServicioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ComisionServicioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ComisionServicioPK))
      throw new IllegalArgumentException("arg1");
    ComisionServicioPK localComisionServicioPK = (ComisionServicioPK)paramObject;
    localComisionServicioPK.idComisionServicio = this.idComisionServicio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ComisionServicioPK))
      throw new IllegalArgumentException("arg1");
    ComisionServicioPK localComisionServicioPK = (ComisionServicioPK)paramObject;
    this.idComisionServicio = localComisionServicioPK.idComisionServicio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ComisionServicioPK))
      throw new IllegalArgumentException("arg2");
    ComisionServicioPK localComisionServicioPK = (ComisionServicioPK)paramObject;
    localComisionServicioPK.idComisionServicio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ComisionServicioPK))
      throw new IllegalArgumentException("arg2");
    ComisionServicioPK localComisionServicioPK = (ComisionServicioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localComisionServicioPK.idComisionServicio);
  }

  private static final Date jdoGetfechaFin(ComisionServicio paramComisionServicio)
  {
    if (paramComisionServicio.jdoFlags <= 0)
      return paramComisionServicio.fechaFin;
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.fechaFin;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 0))
      return paramComisionServicio.fechaFin;
    return (Date)localStateManager.getObjectField(paramComisionServicio, jdoInheritedFieldCount + 0, paramComisionServicio.fechaFin);
  }

  private static final void jdoSetfechaFin(ComisionServicio paramComisionServicio, Date paramDate)
  {
    if (paramComisionServicio.jdoFlags == 0)
    {
      paramComisionServicio.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComisionServicio, jdoInheritedFieldCount + 0, paramComisionServicio.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(ComisionServicio paramComisionServicio)
  {
    if (paramComisionServicio.jdoFlags <= 0)
      return paramComisionServicio.fechaInicio;
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.fechaInicio;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 1))
      return paramComisionServicio.fechaInicio;
    return (Date)localStateManager.getObjectField(paramComisionServicio, jdoInheritedFieldCount + 1, paramComisionServicio.fechaInicio);
  }

  private static final void jdoSetfechaInicio(ComisionServicio paramComisionServicio, Date paramDate)
  {
    if (paramComisionServicio.jdoFlags == 0)
    {
      paramComisionServicio.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComisionServicio, jdoInheritedFieldCount + 1, paramComisionServicio.fechaInicio, paramDate);
  }

  private static final long jdoGetidComisionServicio(ComisionServicio paramComisionServicio)
  {
    return paramComisionServicio.idComisionServicio;
  }

  private static final void jdoSetidComisionServicio(ComisionServicio paramComisionServicio, long paramLong)
  {
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.idComisionServicio = paramLong;
      return;
    }
    localStateManager.setLongField(paramComisionServicio, jdoInheritedFieldCount + 2, paramComisionServicio.idComisionServicio, paramLong);
  }

  private static final int jdoGetidSitp(ComisionServicio paramComisionServicio)
  {
    if (paramComisionServicio.jdoFlags <= 0)
      return paramComisionServicio.idSitp;
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.idSitp;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 3))
      return paramComisionServicio.idSitp;
    return localStateManager.getIntField(paramComisionServicio, jdoInheritedFieldCount + 3, paramComisionServicio.idSitp);
  }

  private static final void jdoSetidSitp(ComisionServicio paramComisionServicio, int paramInt)
  {
    if (paramComisionServicio.jdoFlags == 0)
    {
      paramComisionServicio.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramComisionServicio, jdoInheritedFieldCount + 3, paramComisionServicio.idSitp, paramInt);
  }

  private static final String jdoGetnombreInstitucion(ComisionServicio paramComisionServicio)
  {
    if (paramComisionServicio.jdoFlags <= 0)
      return paramComisionServicio.nombreInstitucion;
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.nombreInstitucion;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 4))
      return paramComisionServicio.nombreInstitucion;
    return localStateManager.getStringField(paramComisionServicio, jdoInheritedFieldCount + 4, paramComisionServicio.nombreInstitucion);
  }

  private static final void jdoSetnombreInstitucion(ComisionServicio paramComisionServicio, String paramString)
  {
    if (paramComisionServicio.jdoFlags == 0)
    {
      paramComisionServicio.nombreInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.nombreInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramComisionServicio, jdoInheritedFieldCount + 4, paramComisionServicio.nombreInstitucion, paramString);
  }

  private static final String jdoGetobservaciones(ComisionServicio paramComisionServicio)
  {
    if (paramComisionServicio.jdoFlags <= 0)
      return paramComisionServicio.observaciones;
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.observaciones;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 5))
      return paramComisionServicio.observaciones;
    return localStateManager.getStringField(paramComisionServicio, jdoInheritedFieldCount + 5, paramComisionServicio.observaciones);
  }

  private static final void jdoSetobservaciones(ComisionServicio paramComisionServicio, String paramString)
  {
    if (paramComisionServicio.jdoFlags == 0)
    {
      paramComisionServicio.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramComisionServicio, jdoInheritedFieldCount + 5, paramComisionServicio.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(ComisionServicio paramComisionServicio)
  {
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.personal;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 6))
      return paramComisionServicio.personal;
    return (Personal)localStateManager.getObjectField(paramComisionServicio, jdoInheritedFieldCount + 6, paramComisionServicio.personal);
  }

  private static final void jdoSetpersonal(ComisionServicio paramComisionServicio, Personal paramPersonal)
  {
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramComisionServicio, jdoInheritedFieldCount + 6, paramComisionServicio.personal, paramPersonal);
  }

  private static final SedeDiplomatica jdoGetsedeDiplomatica(ComisionServicio paramComisionServicio)
  {
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.sedeDiplomatica;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 7))
      return paramComisionServicio.sedeDiplomatica;
    return (SedeDiplomatica)localStateManager.getObjectField(paramComisionServicio, jdoInheritedFieldCount + 7, paramComisionServicio.sedeDiplomatica);
  }

  private static final void jdoSetsedeDiplomatica(ComisionServicio paramComisionServicio, SedeDiplomatica paramSedeDiplomatica)
  {
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.sedeDiplomatica = paramSedeDiplomatica;
      return;
    }
    localStateManager.setObjectField(paramComisionServicio, jdoInheritedFieldCount + 7, paramComisionServicio.sedeDiplomatica, paramSedeDiplomatica);
  }

  private static final Date jdoGettiempoSitp(ComisionServicio paramComisionServicio)
  {
    if (paramComisionServicio.jdoFlags <= 0)
      return paramComisionServicio.tiempoSitp;
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicio.tiempoSitp;
    if (localStateManager.isLoaded(paramComisionServicio, jdoInheritedFieldCount + 8))
      return paramComisionServicio.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramComisionServicio, jdoInheritedFieldCount + 8, paramComisionServicio.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ComisionServicio paramComisionServicio, Date paramDate)
  {
    if (paramComisionServicio.jdoFlags == 0)
    {
      paramComisionServicio.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramComisionServicio.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicio.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComisionServicio, jdoInheritedFieldCount + 8, paramComisionServicio.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}