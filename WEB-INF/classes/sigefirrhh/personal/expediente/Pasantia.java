package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Pasantia
  implements Serializable, PersistenceCapable
{
  private long idPasantia;
  private Date fechaInicio;
  private Date fechaFin;
  private String instituto;
  private String proyecto;
  private String institucion;
  private String dependencia;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "dependencia", "fechaFin", "fechaInicio", "idPasantia", "idSitp", "institucion", "instituto", "observaciones", "personal", "proyecto", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 21, 26, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdependencia(this) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this));
  }

  public String getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setDependencia(String string)
  {
    jdoSetdependencia(this, string);
  }

  public void setFechaFin(Date date) {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date) {
    jdoSetfechaInicio(this, date);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public long getIdPasantia()
  {
    return jdoGetidPasantia(this);
  }

  public void setIdPasantia(long l)
  {
    jdoSetidPasantia(this, l);
  }

  public String getInstituto()
  {
    return jdoGetinstituto(this);
  }

  public String getProyecto()
  {
    return jdoGetproyecto(this);
  }

  public void setInstituto(String string)
  {
    jdoSetinstituto(this, string);
  }

  public void setProyecto(String string)
  {
    jdoSetproyecto(this, string);
  }

  public String getInstitucion()
  {
    return jdoGetinstitucion(this);
  }

  public void setInstitucion(String string)
  {
    jdoSetinstitucion(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Pasantia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Pasantia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Pasantia localPasantia = new Pasantia();
    localPasantia.jdoFlags = 1;
    localPasantia.jdoStateManager = paramStateManager;
    return localPasantia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Pasantia localPasantia = new Pasantia();
    localPasantia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPasantia.jdoFlags = 1;
    localPasantia.jdoStateManager = paramStateManager;
    return localPasantia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.dependencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPasantia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.institucion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.instituto);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.proyecto);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPasantia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.institucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.instituto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proyecto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Pasantia paramPasantia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramPasantia.dependencia;
      return;
    case 1:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramPasantia.fechaFin;
      return;
    case 2:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramPasantia.fechaInicio;
      return;
    case 3:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.idPasantia = paramPasantia.idPasantia;
      return;
    case 4:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPasantia.idSitp;
      return;
    case 5:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.institucion = paramPasantia.institucion;
      return;
    case 6:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.instituto = paramPasantia.instituto;
      return;
    case 7:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramPasantia.observaciones;
      return;
    case 8:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramPasantia.personal;
      return;
    case 9:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.proyecto = paramPasantia.proyecto;
      return;
    case 10:
      if (paramPasantia == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPasantia.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Pasantia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Pasantia localPasantia = (Pasantia)paramObject;
    if (localPasantia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPasantia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PasantiaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PasantiaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg1");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    localPasantiaPK.idPasantia = this.idPasantia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg1");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    this.idPasantia = localPasantiaPK.idPasantia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg2");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    localPasantiaPK.idPasantia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PasantiaPK))
      throw new IllegalArgumentException("arg2");
    PasantiaPK localPasantiaPK = (PasantiaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localPasantiaPK.idPasantia);
  }

  private static final String jdoGetdependencia(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.dependencia;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.dependencia;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 0))
      return paramPasantia.dependencia;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 0, paramPasantia.dependencia);
  }

  private static final void jdoSetdependencia(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.dependencia = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.dependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 0, paramPasantia.dependencia, paramString);
  }

  private static final Date jdoGetfechaFin(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.fechaFin;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.fechaFin;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 1))
      return paramPasantia.fechaFin;
    return (Date)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 1, paramPasantia.fechaFin);
  }

  private static final void jdoSetfechaFin(Pasantia paramPasantia, Date paramDate)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 1, paramPasantia.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.fechaInicio;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.fechaInicio;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 2))
      return paramPasantia.fechaInicio;
    return (Date)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 2, paramPasantia.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Pasantia paramPasantia, Date paramDate)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 2, paramPasantia.fechaInicio, paramDate);
  }

  private static final long jdoGetidPasantia(Pasantia paramPasantia)
  {
    return paramPasantia.idPasantia;
  }

  private static final void jdoSetidPasantia(Pasantia paramPasantia, long paramLong)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.idPasantia = paramLong;
      return;
    }
    localStateManager.setLongField(paramPasantia, jdoInheritedFieldCount + 3, paramPasantia.idPasantia, paramLong);
  }

  private static final int jdoGetidSitp(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.idSitp;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.idSitp;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 4))
      return paramPasantia.idSitp;
    return localStateManager.getIntField(paramPasantia, jdoInheritedFieldCount + 4, paramPasantia.idSitp);
  }

  private static final void jdoSetidSitp(Pasantia paramPasantia, int paramInt)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPasantia, jdoInheritedFieldCount + 4, paramPasantia.idSitp, paramInt);
  }

  private static final String jdoGetinstitucion(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.institucion;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.institucion;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 5))
      return paramPasantia.institucion;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 5, paramPasantia.institucion);
  }

  private static final void jdoSetinstitucion(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.institucion = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.institucion = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 5, paramPasantia.institucion, paramString);
  }

  private static final String jdoGetinstituto(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.instituto;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.instituto;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 6))
      return paramPasantia.instituto;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 6, paramPasantia.instituto);
  }

  private static final void jdoSetinstituto(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.instituto = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.instituto = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 6, paramPasantia.instituto, paramString);
  }

  private static final String jdoGetobservaciones(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.observaciones;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.observaciones;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 7))
      return paramPasantia.observaciones;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 7, paramPasantia.observaciones);
  }

  private static final void jdoSetobservaciones(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 7, paramPasantia.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Pasantia paramPasantia)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.personal;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 8))
      return paramPasantia.personal;
    return (Personal)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 8, paramPasantia.personal);
  }

  private static final void jdoSetpersonal(Pasantia paramPasantia, Personal paramPersonal)
  {
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 8, paramPasantia.personal, paramPersonal);
  }

  private static final String jdoGetproyecto(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.proyecto;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.proyecto;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 9))
      return paramPasantia.proyecto;
    return localStateManager.getStringField(paramPasantia, jdoInheritedFieldCount + 9, paramPasantia.proyecto);
  }

  private static final void jdoSetproyecto(Pasantia paramPasantia, String paramString)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.proyecto = paramString;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.proyecto = paramString;
      return;
    }
    localStateManager.setStringField(paramPasantia, jdoInheritedFieldCount + 9, paramPasantia.proyecto, paramString);
  }

  private static final Date jdoGettiempoSitp(Pasantia paramPasantia)
  {
    if (paramPasantia.jdoFlags <= 0)
      return paramPasantia.tiempoSitp;
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
      return paramPasantia.tiempoSitp;
    if (localStateManager.isLoaded(paramPasantia, jdoInheritedFieldCount + 10))
      return paramPasantia.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPasantia, jdoInheritedFieldCount + 10, paramPasantia.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Pasantia paramPasantia, Date paramDate)
  {
    if (paramPasantia.jdoFlags == 0)
    {
      paramPasantia.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPasantia.jdoStateManager;
    if (localStateManager == null)
    {
      paramPasantia.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPasantia, jdoInheritedFieldCount + 10, paramPasantia.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}