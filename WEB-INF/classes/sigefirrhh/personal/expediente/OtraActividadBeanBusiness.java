package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoOtraActividad;
import sigefirrhh.base.personal.TipoOtraActividadBeanBusiness;

public class OtraActividadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addOtraActividad(OtraActividad otraActividad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    OtraActividad otraActividadNew = 
      (OtraActividad)BeanUtils.cloneBean(
      otraActividad);

    TipoOtraActividadBeanBusiness tipoOtraActividadBeanBusiness = new TipoOtraActividadBeanBusiness();

    if (otraActividadNew.getTipoOtraActividad() != null) {
      otraActividadNew.setTipoOtraActividad(
        tipoOtraActividadBeanBusiness.findTipoOtraActividadById(
        otraActividadNew.getTipoOtraActividad().getIdTipoOtraActividad()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (otraActividadNew.getPersonal() != null) {
      otraActividadNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        otraActividadNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(otraActividadNew);
  }

  public void updateOtraActividad(OtraActividad otraActividad) throws Exception
  {
    OtraActividad otraActividadModify = 
      findOtraActividadById(otraActividad.getIdOtraActividad());

    TipoOtraActividadBeanBusiness tipoOtraActividadBeanBusiness = new TipoOtraActividadBeanBusiness();

    if (otraActividad.getTipoOtraActividad() != null) {
      otraActividad.setTipoOtraActividad(
        tipoOtraActividadBeanBusiness.findTipoOtraActividadById(
        otraActividad.getTipoOtraActividad().getIdTipoOtraActividad()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (otraActividad.getPersonal() != null) {
      otraActividad.setPersonal(
        personalBeanBusiness.findPersonalById(
        otraActividad.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(otraActividadModify, otraActividad);
  }

  public void deleteOtraActividad(OtraActividad otraActividad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    OtraActividad otraActividadDelete = 
      findOtraActividadById(otraActividad.getIdOtraActividad());
    pm.deletePersistent(otraActividadDelete);
  }

  public OtraActividad findOtraActividadById(long idOtraActividad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idOtraActividad == pIdOtraActividad";
    Query query = pm.newQuery(OtraActividad.class, filter);

    query.declareParameters("long pIdOtraActividad");

    parameters.put("pIdOtraActividad", new Long(idOtraActividad));

    Collection colOtraActividad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colOtraActividad.iterator();
    return (OtraActividad)iterator.next();
  }

  public Collection findOtraActividadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent otraActividadExtent = pm.getExtent(
      OtraActividad.class, true);
    Query query = pm.newQuery(otraActividadExtent);
    query.setOrdering("tipoOtraActividad.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(OtraActividad.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("tipoOtraActividad.descripcion ascending");

    Collection colOtraActividad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colOtraActividad);

    return colOtraActividad;
  }
}