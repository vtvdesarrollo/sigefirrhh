package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PasantiaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPasantia(Pasantia pasantia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Pasantia pasantiaNew = 
      (Pasantia)BeanUtils.cloneBean(
      pasantia);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (pasantiaNew.getPersonal() != null) {
      pasantiaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        pasantiaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(pasantiaNew);
  }

  public void updatePasantia(Pasantia pasantia) throws Exception
  {
    Pasantia pasantiaModify = 
      findPasantiaById(pasantia.getIdPasantia());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (pasantia.getPersonal() != null) {
      pasantia.setPersonal(
        personalBeanBusiness.findPersonalById(
        pasantia.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(pasantiaModify, pasantia);
  }

  public void deletePasantia(Pasantia pasantia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Pasantia pasantiaDelete = 
      findPasantiaById(pasantia.getIdPasantia());
    pm.deletePersistent(pasantiaDelete);
  }

  public Pasantia findPasantiaById(long idPasantia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPasantia == pIdPasantia";
    Query query = pm.newQuery(Pasantia.class, filter);

    query.declareParameters("long pIdPasantia");

    parameters.put("pIdPasantia", new Long(idPasantia));

    Collection colPasantia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPasantia.iterator();
    return (Pasantia)iterator.next();
  }

  public Collection findPasantiaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent pasantiaExtent = pm.getExtent(
      Pasantia.class, true);
    Query query = pm.newQuery(pasantiaExtent);
    query.setOrdering("fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Pasantia.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaInicio ascending");

    Collection colPasantia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPasantia);

    return colPasantia;
  }
}