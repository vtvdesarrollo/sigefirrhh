package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.RelacionRecaudo;
import sigefirrhh.base.personal.RelacionRecaudoBeanBusiness;

public class PersonalRecaudoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPersonalRecaudo(PersonalRecaudo personalRecaudo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PersonalRecaudo personalRecaudoNew = 
      (PersonalRecaudo)BeanUtils.cloneBean(
      personalRecaudo);

    RelacionRecaudoBeanBusiness relacionRecaudoBeanBusiness = new RelacionRecaudoBeanBusiness();

    if (personalRecaudoNew.getRelacionRecaudo() != null) {
      personalRecaudoNew.setRelacionRecaudo(
        relacionRecaudoBeanBusiness.findRelacionRecaudoById(
        personalRecaudoNew.getRelacionRecaudo().getIdRelacionRecaudo()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (personalRecaudoNew.getPersonal() != null) {
      personalRecaudoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        personalRecaudoNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(personalRecaudoNew);
  }

  public void updatePersonalRecaudo(PersonalRecaudo personalRecaudo) throws Exception
  {
    PersonalRecaudo personalRecaudoModify = 
      findPersonalRecaudoById(personalRecaudo.getIdPersonalRecaudo());

    RelacionRecaudoBeanBusiness relacionRecaudoBeanBusiness = new RelacionRecaudoBeanBusiness();

    if (personalRecaudo.getRelacionRecaudo() != null) {
      personalRecaudo.setRelacionRecaudo(
        relacionRecaudoBeanBusiness.findRelacionRecaudoById(
        personalRecaudo.getRelacionRecaudo().getIdRelacionRecaudo()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (personalRecaudo.getPersonal() != null) {
      personalRecaudo.setPersonal(
        personalBeanBusiness.findPersonalById(
        personalRecaudo.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(personalRecaudoModify, personalRecaudo);
  }

  public void deletePersonalRecaudo(PersonalRecaudo personalRecaudo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PersonalRecaudo personalRecaudoDelete = 
      findPersonalRecaudoById(personalRecaudo.getIdPersonalRecaudo());
    pm.deletePersistent(personalRecaudoDelete);
  }

  public PersonalRecaudo findPersonalRecaudoById(long idPersonalRecaudo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPersonalRecaudo == pIdPersonalRecaudo";
    Query query = pm.newQuery(PersonalRecaudo.class, filter);

    query.declareParameters("long pIdPersonalRecaudo");

    parameters.put("pIdPersonalRecaudo", new Long(idPersonalRecaudo));

    Collection colPersonalRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPersonalRecaudo.iterator();
    return (PersonalRecaudo)iterator.next();
  }

  public Collection findPersonalRecaudoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent personalRecaudoExtent = pm.getExtent(
      PersonalRecaudo.class, true);
    Query query = pm.newQuery(personalRecaudoExtent);
    query.setOrdering("relacionRecaudo.recaudo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(PersonalRecaudo.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("relacionRecaudo.recaudo ascending");

    Collection colPersonalRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonalRecaudo);

    return colPersonalRecaudo;
  }
}