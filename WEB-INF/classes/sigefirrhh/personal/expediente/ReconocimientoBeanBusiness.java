package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.personal.TipoReconocimiento;
import sigefirrhh.base.personal.TipoReconocimientoBeanBusiness;

public class ReconocimientoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addReconocimiento(Reconocimiento reconocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Reconocimiento reconocimientoNew = 
      (Reconocimiento)BeanUtils.cloneBean(
      reconocimiento);

    TipoReconocimientoBeanBusiness tipoReconocimientoBeanBusiness = new TipoReconocimientoBeanBusiness();

    if (reconocimientoNew.getTipoReconocimiento() != null) {
      reconocimientoNew.setTipoReconocimiento(
        tipoReconocimientoBeanBusiness.findTipoReconocimientoById(
        reconocimientoNew.getTipoReconocimiento().getIdTipoReconocimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (reconocimientoNew.getPersonal() != null) {
      reconocimientoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        reconocimientoNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (reconocimientoNew.getOrganismo() != null) {
      reconocimientoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        reconocimientoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(reconocimientoNew);
  }

  public void updateReconocimiento(Reconocimiento reconocimiento) throws Exception
  {
    Reconocimiento reconocimientoModify = 
      findReconocimientoById(reconocimiento.getIdReconocimiento());

    TipoReconocimientoBeanBusiness tipoReconocimientoBeanBusiness = new TipoReconocimientoBeanBusiness();

    if (reconocimiento.getTipoReconocimiento() != null) {
      reconocimiento.setTipoReconocimiento(
        tipoReconocimientoBeanBusiness.findTipoReconocimientoById(
        reconocimiento.getTipoReconocimiento().getIdTipoReconocimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (reconocimiento.getPersonal() != null) {
      reconocimiento.setPersonal(
        personalBeanBusiness.findPersonalById(
        reconocimiento.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (reconocimiento.getOrganismo() != null) {
      reconocimiento.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        reconocimiento.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(reconocimientoModify, reconocimiento);
  }

  public void deleteReconocimiento(Reconocimiento reconocimiento) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Reconocimiento reconocimientoDelete = 
      findReconocimientoById(reconocimiento.getIdReconocimiento());
    pm.deletePersistent(reconocimientoDelete);
  }

  public Reconocimiento findReconocimientoById(long idReconocimiento) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idReconocimiento == pIdReconocimiento";
    Query query = pm.newQuery(Reconocimiento.class, filter);

    query.declareParameters("long pIdReconocimiento");

    parameters.put("pIdReconocimiento", new Long(idReconocimiento));

    Collection colReconocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colReconocimiento.iterator();
    return (Reconocimiento)iterator.next();
  }

  public Collection findReconocimientoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent reconocimientoExtent = pm.getExtent(
      Reconocimiento.class, true);
    Query query = pm.newQuery(reconocimientoExtent);
    query.setOrdering("tipoReconocimiento.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Reconocimiento.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("tipoReconocimiento.descripcion ascending");

    Collection colReconocimiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colReconocimiento);

    return colReconocimiento;
  }
}