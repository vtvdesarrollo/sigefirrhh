package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoHabilidad;
import sigefirrhh.base.personal.TipoHabilidadBeanBusiness;

public class HabilidadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHabilidad(Habilidad habilidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Habilidad habilidadNew = 
      (Habilidad)BeanUtils.cloneBean(
      habilidad);

    TipoHabilidadBeanBusiness tipoHabilidadBeanBusiness = new TipoHabilidadBeanBusiness();

    if (habilidadNew.getTipoHabilidad() != null) {
      habilidadNew.setTipoHabilidad(
        tipoHabilidadBeanBusiness.findTipoHabilidadById(
        habilidadNew.getTipoHabilidad().getIdTipoHabilidad()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (habilidadNew.getPersonal() != null) {
      habilidadNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        habilidadNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(habilidadNew);
  }

  public void updateHabilidad(Habilidad habilidad) throws Exception
  {
    Habilidad habilidadModify = 
      findHabilidadById(habilidad.getIdHabilidad());

    TipoHabilidadBeanBusiness tipoHabilidadBeanBusiness = new TipoHabilidadBeanBusiness();

    if (habilidad.getTipoHabilidad() != null) {
      habilidad.setTipoHabilidad(
        tipoHabilidadBeanBusiness.findTipoHabilidadById(
        habilidad.getTipoHabilidad().getIdTipoHabilidad()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (habilidad.getPersonal() != null) {
      habilidad.setPersonal(
        personalBeanBusiness.findPersonalById(
        habilidad.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(habilidadModify, habilidad);
  }

  public void deleteHabilidad(Habilidad habilidad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Habilidad habilidadDelete = 
      findHabilidadById(habilidad.getIdHabilidad());
    pm.deletePersistent(habilidadDelete);
  }

  public Habilidad findHabilidadById(long idHabilidad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHabilidad == pIdHabilidad";
    Query query = pm.newQuery(Habilidad.class, filter);

    query.declareParameters("long pIdHabilidad");

    parameters.put("pIdHabilidad", new Long(idHabilidad));

    Collection colHabilidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHabilidad.iterator();
    return (Habilidad)iterator.next();
  }

  public Collection findHabilidadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent habilidadExtent = pm.getExtent(
      Habilidad.class, true);
    Query query = pm.newQuery(habilidadExtent);
    query.setOrdering("tipoHabilidad.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Habilidad.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("tipoHabilidad.descripcion ascending");

    Collection colHabilidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHabilidad);

    return colHabilidad;
  }
}