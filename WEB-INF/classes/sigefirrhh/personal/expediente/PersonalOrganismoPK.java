package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class PersonalOrganismoPK
  implements Serializable
{
  public long idPersonalOrganismo;

  public PersonalOrganismoPK()
  {
  }

  public PersonalOrganismoPK(long idPersonalOrganismo)
  {
    this.idPersonalOrganismo = idPersonalOrganismo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PersonalOrganismoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PersonalOrganismoPK thatPK)
  {
    return 
      this.idPersonalOrganismo == thatPK.idPersonalOrganismo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPersonalOrganismo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPersonalOrganismo);
  }
}