package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.Parroquia;

public class Personal
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTADO_CIVIL;
  protected static final Map LISTA_SEXO;
  protected static final Map LISTA_SECTOR;
  protected static final Map LISTA_TIPO_VIVIENDA;
  protected static final Map LISTA_TENENCIA_VIVIENDA;
  protected static final Map LISTA_DIESTRALIDAD;
  protected static final Map LISTA_NACIONALIDAD;
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_GRUPO_SANGUINEO;
  protected static final Map LISTA_TIPO_DISCAPACIDAD;
  protected static final Map LISTA_PUEBLO_INDIGENA;
  private long idPersonal;
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private String sexo;
  private Date fechaNacimiento;
  private Ciudad ciudadNacimiento;
  private String estadoCivil;
  private String nivelEducativo;
  private String nacionalidad;
  private String dobleNacionalidad;
  private String discapacidad;
  private String Tipodiscapacidad;
  private String Puebloindigena;
  private String fotoTrabajador;
  private Pais paisNacionalidad;
  private String nacionalizado;
  private Date fechaNacionalizacion;
  private String gacetaNacionalizacion;
  private String otraNormativaNac;
  private String direccionResidencia;
  private String zonaPostalResidencia;
  private Ciudad ciudadResidencia;
  private Parroquia parroquia;
  private String telefonoResidencia;
  private String telefonoCelular;
  private String telefonoOficina;
  private String email;
  private int cedulaConyugue;
  private String nombreConyugue;
  private String sectorTrabajoConyugue;
  private String mismoOrganismoConyugue;
  private String tieneHijos;
  private String tipoVivienda;
  private String tenenciaVivienda;
  private String maneja;
  private int gradoLicencia;
  private String tieneVehiculo;
  private String marcaVehiculo;
  private String modeloVehiculo;
  private String placaVehiculo;
  private String numeroSso;
  private String numeroRif;
  private String numeroLibretaMilitar;
  private double estatura;
  private double peso;
  private String diestralidad;
  private String grupoSanguineo;
  private String madrePadre;
  private int aniosServicioApn;
  private int mesesServicioApn;
  private int diasServicioApn;
  private String reingresable;
  private Date fechaFallecimiento;
  private String password;
  private EstablecimientoSalud establecimientoSalud;
  private int idSitp;
  private Date tiempoSitp;
  private String credencial;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "Puebloindigena", "Tipodiscapacidad", "aniosServicioApn", "cedula", "cedulaConyugue", "ciudadNacimiento", "ciudadResidencia", "credencial", "diasServicioApn", "diestralidad", "direccionResidencia", "discapacidad", "dobleNacionalidad", "email", "establecimientoSalud", "estadoCivil", "estatura", "fechaFallecimiento", "fechaNacimiento", "fechaNacionalizacion", "fotoTrabajador", "gacetaNacionalizacion", "gradoLicencia", "grupoSanguineo", "idPersonal", "idSitp", "madrePadre", "maneja", "marcaVehiculo", "mesesServicioApn", "mismoOrganismoConyugue", "modeloVehiculo", "nacionalidad", "nacionalizado", "nivelEducativo", "nombreConyugue", "numeroLibretaMilitar", "numeroRif", "numeroSso", "otraNormativaNac", "paisNacionalidad", "parroquia", "password", "peso", "placaVehiculo", "primerApellido", "primerNombre", "reingresable", "sectorTrabajoConyugue", "segundoApellido", "segundoNombre", "sexo", "telefonoCelular", "telefonoOficina", "telefonoResidencia", "tenenciaVivienda", "tiempoSitp", "tieneHijos", "tieneVehiculo", "tipoVivienda", "zonaPostalResidencia" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.EstablecimientoSalud"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Pais"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Parroquia"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 26, 26, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Personal());

    LISTA_ESTADO_CIVIL = 
      new LinkedHashMap();
    LISTA_SEXO = 
      new LinkedHashMap();
    LISTA_SECTOR = 
      new LinkedHashMap();
    LISTA_TIPO_VIVIENDA = 
      new LinkedHashMap();
    LISTA_TENENCIA_VIVIENDA = 
      new LinkedHashMap();
    LISTA_DIESTRALIDAD = 
      new LinkedHashMap();
    LISTA_NACIONALIDAD = 
      new LinkedHashMap();
    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_GRUPO_SANGUINEO = 
      new LinkedHashMap();
    LISTA_TIPO_DISCAPACIDAD = 
      new LinkedHashMap();
    LISTA_PUEBLO_INDIGENA = 
      new LinkedHashMap();

    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
    LISTA_ESTADO_CIVIL.put("D", "DIVORCIADO(A)");
    LISTA_ESTADO_CIVIL.put("V", "VIUDO(A)");
    LISTA_ESTADO_CIVIL.put("U", "CONCUBINO(A)");
    LISTA_ESTADO_CIVIL.put("O", "OTRO");
    LISTA_SEXO.put("F", "FEMENINO");
    LISTA_SEXO.put("M", "MASCULINO");
    LISTA_SECTOR.put("P", "PRIVADO");
    LISTA_SECTOR.put("U", "PUBLICO");
    LISTA_SECTOR.put("N", "NINGUNO");
    LISTA_NACIONALIDAD.put("V", "VENEZOLANO(A)");
    LISTA_NACIONALIDAD.put("E", "EXTRANJERO(A)");
    LISTA_DIESTRALIDAD.put("D", "DERECHO(A)");
    LISTA_DIESTRALIDAD.put("Z", "ZURDO(A)");
    LISTA_DIESTRALIDAD.put("A", "AMBIDIESTRO");
    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("N", "NINGUNO");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_GRUPO_SANGUINEO.put("A+", "A+");
    LISTA_GRUPO_SANGUINEO.put("B+", "B+");
    LISTA_GRUPO_SANGUINEO.put("O+", "O+");
    LISTA_GRUPO_SANGUINEO.put("A-", "A-");
    LISTA_GRUPO_SANGUINEO.put("B-", "B-");
    LISTA_GRUPO_SANGUINEO.put("O-", "O-");
    LISTA_GRUPO_SANGUINEO.put("AB+", "AB+");
    LISTA_GRUPO_SANGUINEO.put("AB-", "AB-");
    LISTA_GRUPO_SANGUINEO.put("NO", "NO");
    LISTA_TIPO_VIVIENDA.put("C", "CASA");
    LISTA_TIPO_VIVIENDA.put("A", "APARTAMENTO");
    LISTA_TIPO_VIVIENDA.put("B", "HABITACION");
    LISTA_TIPO_VIVIENDA.put("H", "HOTEL");
    LISTA_TIPO_VIVIENDA.put("N", "NO APLICA");
    LISTA_TENENCIA_VIVIENDA.put("A", "ALQUILADA");
    LISTA_TENENCIA_VIVIENDA.put("P", "PROPIA");
    LISTA_TENENCIA_VIVIENDA.put("H", "PAGANDO");
    LISTA_TENENCIA_VIVIENDA.put("F", "FAMILIAR");
    LISTA_TENENCIA_VIVIENDA.put("N", "NO APLICA");
    LISTA_TIPO_DISCAPACIDAD.put("S", "Sensorial");
    LISTA_TIPO_DISCAPACIDAD.put("M", "Motrices");
    LISTA_TIPO_DISCAPACIDAD.put("I", "Intelectual");
    LISTA_PUEBLO_INDIGENA.put("I1", "Baniva");
    LISTA_PUEBLO_INDIGENA.put("I2", "Baré");
    LISTA_PUEBLO_INDIGENA.put("I3", "Kubeo");
    LISTA_PUEBLO_INDIGENA.put("I4", "Jiwi");
    LISTA_PUEBLO_INDIGENA.put("I5", "Hoti");
    LISTA_PUEBLO_INDIGENA.put("I6", "Curripako");
    LISTA_PUEBLO_INDIGENA.put("I7", "Piapoco");
    LISTA_PUEBLO_INDIGENA.put("I8", "Purinave");
    LISTA_PUEBLO_INDIGENA.put("I9", "Sáliva");
    LISTA_PUEBLO_INDIGENA.put("I10", "Sanemá/shirianá");
    LISTA_PUEBLO_INDIGENA.put("I11", "Wotjuja");
    LISTA_PUEBLO_INDIGENA.put("I12", "Yanomami");
    LISTA_PUEBLO_INDIGENA.put("I13", "Warekena");
    LISTA_PUEBLO_INDIGENA.put("I14", "Yabarana/Mako");
    LISTA_PUEBLO_INDIGENA.put("I15", "Ñengatu/Yeral");
    LISTA_PUEBLO_INDIGENA.put("I16", "Kariña");
    LISTA_PUEBLO_INDIGENA.put("I17", "Cumanagoto");
    LISTA_PUEBLO_INDIGENA.put("I18", "Pumé");
    LISTA_PUEBLO_INDIGENA.put("I19", "Kumba");
    LISTA_PUEBLO_INDIGENA.put("I20", "Uruak/Arrutan");
    LISTA_PUEBLO_INDIGENA.put("I21", "Akawayo");
    LISTA_PUEBLO_INDIGENA.put("I22", "Arawak");
    LISTA_PUEBLO_INDIGENA.put("I23", "Eñepá");
    LISTA_PUEBLO_INDIGENA.put("I24", "Pemón");
    LISTA_PUEBLO_INDIGENA.put("I25", "Sape");
    LISTA_PUEBLO_INDIGENA.put("I26", "Wanai/Mapoyo");
    LISTA_PUEBLO_INDIGENA.put("I27", "Warao");
    LISTA_PUEBLO_INDIGENA.put("I28", "Chaima");
    LISTA_PUEBLO_INDIGENA.put("I29", "Wayuu");
    LISTA_PUEBLO_INDIGENA.put("I30", "Añu");
    LISTA_PUEBLO_INDIGENA.put("I31", "Bari");
    LISTA_PUEBLO_INDIGENA.put("I32", "Yukpa");
    LISTA_PUEBLO_INDIGENA.put("I33", "Japreira");
    LISTA_PUEBLO_INDIGENA.put("I34", "Ayamán");
    LISTA_PUEBLO_INDIGENA.put("I35", "Amorua");
    LISTA_PUEBLO_INDIGENA.put("I36", "Inga");
    LISTA_PUEBLO_INDIGENA.put("I37", "Yekwana");
    LISTA_PUEBLO_INDIGENA.put("I38", "Quinaroe");
    LISTA_PUEBLO_INDIGENA.put("I39", "Guazabara");
    LISTA_PUEBLO_INDIGENA.put("I40", "Gayón");
    LISTA_PUEBLO_INDIGENA.put("I41", "Camentza");
    LISTA_PUEBLO_INDIGENA.put("I42", "Guanomo");
    LISTA_PUEBLO_INDIGENA.put("I43", "Timotes");
    LISTA_PUEBLO_INDIGENA.put("I44", "Mako");
  }

  public Personal()
  {
    jdoSetestadoCivil(this, "S");

    jdoSetnivelEducativo(this, "N");

    jdoSetnacionalidad(this, "V");

    jdoSetdobleNacionalidad(this, "N");

    jdoSetdiscapacidad(this, "N");

    jdoSetTipodiscapacidad(this, "N");

    jdoSetPuebloindigena(this, "0");

    jdoSetfotoTrabajador(this, "");

    jdoSetnacionalizado(this, "N");

    jdoSetsectorTrabajoConyugue(this, "N");

    jdoSetmismoOrganismoConyugue(this, "N");

    jdoSettieneHijos(this, "N");

    jdoSettipoVivienda(this, "C");

    jdoSettenenciaVivienda(this, "P");

    jdoSetmaneja(this, "N");

    jdoSettieneVehiculo(this, "N");

    jdoSetdiestralidad(this, "D");

    jdoSetgrupoSanguineo(this, "NO");

    jdoSetmadrePadre(this, "N");

    jdoSetaniosServicioApn(this, 0);

    jdoSetmesesServicioApn(this, 0);

    jdoSetdiasServicioApn(this, 0);

    jdoSetreingresable(this, "S");
  }

  public String toString()
  {
    return 
      jdoGetcedula(this) + "  -  " + 
      jdoGetprimerApellido(this) + " " + (
      jdoGetsegundoApellido(this) == null ? "" : jdoGetsegundoApellido(this)) + ", " + 
      jdoGetprimerNombre(this) + " " + (
      jdoGetsegundoNombre(this) == null ? "" : jdoGetsegundoNombre(this));
  }

  public String getCredencial()
  {
    return jdoGetcredencial(this);
  }

  public void setCredencial(String credencial) {
    jdoSetcredencial(this, credencial);
  }

  public int getAniosServicioApn() {
    return jdoGetaniosServicioApn(this);
  }

  public int getCedula() {
    return jdoGetcedula(this);
  }

  public int getCedulaConyugue() {
    return jdoGetcedulaConyugue(this);
  }

  public Ciudad getCiudadNacimiento() {
    return jdoGetciudadNacimiento(this);
  }
  public Ciudad getCiudadResidencia() {
    return jdoGetciudadResidencia(this);
  }

  public String getDiestralidad() {
    return jdoGetdiestralidad(this);
  }

  public String getDireccionResidencia() {
    return jdoGetdireccionResidencia(this);
  }

  public String getDobleNacionalidad() {
    return jdoGetdobleNacionalidad(this);
  }
  public String getDiscapacidad() {
    return jdoGetdiscapacidad(this);
  }
  public String getTipodiscapacidad() {
    return jdoGetTipodiscapacidad(this);
  }
  public String getPuebloindigena() {
    return jdoGetPuebloindigena(this);
  }

  public String getEmail() {
    return jdoGetemail(this);
  }

  public EstablecimientoSalud getEstablecimientoSalud() {
    return jdoGetestablecimientoSalud(this);
  }

  public String getEstadoCivil() {
    return jdoGetestadoCivil(this);
  }

  public double getEstatura() {
    return jdoGetestatura(this);
  }

  public Date getFechaNacimiento() {
    return jdoGetfechaNacimiento(this);
  }

  public Date getFechaNacionalizacion() {
    return jdoGetfechaNacionalizacion(this);
  }

  public String getGacetaNacionalizacion() {
    return jdoGetgacetaNacionalizacion(this);
  }

  public int getGradoLicencia() {
    return jdoGetgradoLicencia(this);
  }

  public String getGrupoSanguineo() {
    return jdoGetgrupoSanguineo(this);
  }

  public long getIdPersonal() {
    return jdoGetidPersonal(this);
  }

  public String getManeja() {
    return jdoGetmaneja(this);
  }

  public String getMarcaVehiculo() {
    return jdoGetmarcaVehiculo(this);
  }

  public String getMismoOrganismoConyugue() {
    return jdoGetmismoOrganismoConyugue(this);
  }

  public String getModeloVehiculo() {
    return jdoGetmodeloVehiculo(this);
  }

  public String getNacionalidad() {
    return jdoGetnacionalidad(this);
  }

  public String getNacionalizado() {
    return jdoGetnacionalizado(this);
  }

  public String getNivelEducativo() {
    return jdoGetnivelEducativo(this);
  }

  public String getNombreConyugue() {
    return jdoGetnombreConyugue(this);
  }

  public String getNumeroLibretaMilitar() {
    return jdoGetnumeroLibretaMilitar(this);
  }

  public String getNumeroRif() {
    return jdoGetnumeroRif(this);
  }

  public String getNumeroSso() {
    return jdoGetnumeroSso(this);
  }

  public String getOtraNormativaNac()
  {
    return jdoGetotraNormativaNac(this);
  }

  public Pais getPaisNacionalidad() {
    return jdoGetpaisNacionalidad(this);
  }

  public Parroquia getParroquia() {
    return jdoGetparroquia(this);
  }

  public double getPeso() {
    return jdoGetpeso(this);
  }

  public String getPlacaVehiculo() {
    return jdoGetplacaVehiculo(this);
  }

  public String getPrimerApellido() {
    return jdoGetprimerApellido(this);
  }

  public String getPrimerNombre() {
    return jdoGetprimerNombre(this);
  }

  public String getReingresable() {
    return jdoGetreingresable(this);
  }

  public String getSectorTrabajoConyugue() {
    return jdoGetsectorTrabajoConyugue(this);
  }

  public String getSegundoApellido() {
    return jdoGetsegundoApellido(this);
  }

  public String getSegundoNombre() {
    return jdoGetsegundoNombre(this);
  }

  public String getSexo() {
    return jdoGetsexo(this);
  }

  public String getTelefonoCelular() {
    return jdoGettelefonoCelular(this);
  }

  public String getTelefonoOficina() {
    return jdoGettelefonoOficina(this);
  }

  public String getTelefonoResidencia() {
    return jdoGettelefonoResidencia(this);
  }

  public String getTenenciaVivienda() {
    return jdoGettenenciaVivienda(this);
  }

  public String getTieneHijos() {
    return jdoGettieneHijos(this);
  }

  public String getTieneVehiculo() {
    return jdoGettieneVehiculo(this);
  }

  public String getTipoVivienda() {
    return jdoGettipoVivienda(this);
  }

  public String getZonaPostalResidencia() {
    return jdoGetzonaPostalResidencia(this);
  }

  public void setAniosServicioApn(int i) {
    jdoSetaniosServicioApn(this, i);
  }

  public void setCedula(int i) {
    jdoSetcedula(this, i);
  }

  public void setCedulaConyugue(int i) {
    jdoSetcedulaConyugue(this, i);
  }

  public void setCiudadNacimiento(Ciudad ciudad) {
    jdoSetciudadNacimiento(this, ciudad);
  }

  public void setCiudadResidencia(Ciudad ciudad) {
    jdoSetciudadResidencia(this, ciudad);
  }

  public void setDiestralidad(String string) {
    jdoSetdiestralidad(this, string);
  }

  public void setDireccionResidencia(String string) {
    jdoSetdireccionResidencia(this, string);
  }

  public void setDobleNacionalidad(String string) {
    jdoSetdobleNacionalidad(this, string);
  }
  public void setDiscapacidad(String string) {
    jdoSetdiscapacidad(this, string);
  }
  public void setTipodiscapacidad(String string) {
    jdoSetTipodiscapacidad(this, string);
  }
  public void setPuebloindigena(String string) {
    jdoSetPuebloindigena(this, string);
  }

  public void setEmail(String string) {
    jdoSetemail(this, string);
  }

  public void setEstablecimientoSalud(EstablecimientoSalud salud) {
    jdoSetestablecimientoSalud(this, salud);
  }

  public void setEstadoCivil(String string) {
    jdoSetestadoCivil(this, string);
  }

  public void setEstatura(double d) {
    jdoSetestatura(this, d);
  }

  public void setFechaNacimiento(Date date) {
    jdoSetfechaNacimiento(this, date);
  }

  public void setFechaNacionalizacion(Date date) {
    jdoSetfechaNacionalizacion(this, date);
  }

  public void setGacetaNacionalizacion(String string) {
    jdoSetgacetaNacionalizacion(this, string);
  }

  public void setGradoLicencia(int i) {
    jdoSetgradoLicencia(this, i);
  }

  public void setGrupoSanguineo(String string) {
    jdoSetgrupoSanguineo(this, string);
  }

  public void setIdPersonal(long l) {
    jdoSetidPersonal(this, l);
  }

  public void setManeja(String string) {
    jdoSetmaneja(this, string);
  }

  public void setMarcaVehiculo(String string) {
    jdoSetmarcaVehiculo(this, string);
  }

  public void setMismoOrganismoConyugue(String string) {
    jdoSetmismoOrganismoConyugue(this, string);
  }

  public void setModeloVehiculo(String string) {
    jdoSetmodeloVehiculo(this, string);
  }

  public void setNacionalidad(String string) {
    jdoSetnacionalidad(this, string);
  }

  public void setNacionalizado(String string) {
    jdoSetnacionalizado(this, string);
  }

  public void setNivelEducativo(String string) {
    jdoSetnivelEducativo(this, string);
  }

  public void setNombreConyugue(String string) {
    jdoSetnombreConyugue(this, string);
  }

  public void setNumeroLibretaMilitar(String string) {
    jdoSetnumeroLibretaMilitar(this, string);
  }

  public void setNumeroRif(String string) {
    jdoSetnumeroRif(this, string);
  }

  public void setNumeroSso(String string) {
    jdoSetnumeroSso(this, string);
  }

  public void setOtraNormativaNac(String string)
  {
    jdoSetotraNormativaNac(this, string);
  }

  public void setPaisNacionalidad(Pais pais) {
    jdoSetpaisNacionalidad(this, pais);
  }

  public void setParroquia(Parroquia parroquia) {
    jdoSetparroquia(this, parroquia);
  }

  public void setPeso(double d) {
    jdoSetpeso(this, d);
  }

  public void setPlacaVehiculo(String string) {
    jdoSetplacaVehiculo(this, string);
  }

  public void setPrimerApellido(String string) {
    jdoSetprimerApellido(this, string);
  }

  public void setPrimerNombre(String string) {
    jdoSetprimerNombre(this, string);
  }

  public void setReingresable(String string) {
    jdoSetreingresable(this, string);
  }

  public void setSectorTrabajoConyugue(String string) {
    jdoSetsectorTrabajoConyugue(this, string);
  }

  public void setSegundoApellido(String string) {
    jdoSetsegundoApellido(this, string);
  }

  public void setSegundoNombre(String string) {
    jdoSetsegundoNombre(this, string);
  }

  public void setSexo(String string) {
    jdoSetsexo(this, string);
  }

  public void setTelefonoCelular(String string) {
    jdoSettelefonoCelular(this, string);
  }

  public void setTelefonoOficina(String string) {
    jdoSettelefonoOficina(this, string);
  }

  public void setTelefonoResidencia(String string) {
    jdoSettelefonoResidencia(this, string);
  }

  public void setTenenciaVivienda(String string) {
    jdoSettenenciaVivienda(this, string);
  }

  public void setTieneHijos(String string) {
    jdoSettieneHijos(this, string);
  }

  public void setTieneVehiculo(String string) {
    jdoSettieneVehiculo(this, string);
  }

  public void setTipoVivienda(String string) {
    jdoSettipoVivienda(this, string);
  }

  public void setZonaPostalResidencia(String string) {
    jdoSetzonaPostalResidencia(this, string);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public String getPassword() {
    return jdoGetpassword(this);
  }

  public void setPassword(String string) {
    jdoSetpassword(this, string);
  }

  public Date getFechaFallecimiento() {
    return jdoGetfechaFallecimiento(this);
  }

  public void setFechaFallecimiento(Date fechaFallecimiento) {
    jdoSetfechaFallecimiento(this, fechaFallecimiento);
  }

  public String getMadrePadre() {
    return jdoGetmadrePadre(this);
  }

  public void setMadrePadre(String madrePadre) {
    jdoSetmadrePadre(this, madrePadre);
  }

  public int getDiasServicioApn() {
    return jdoGetdiasServicioApn(this);
  }

  public void setDiasServicioApn(int diasServicioApn) {
    jdoSetdiasServicioApn(this, diasServicioApn);
  }

  public int getMesesServicioApn() {
    return jdoGetmesesServicioApn(this);
  }

  public void setMesesServicioApn(int mesesServicioApn) {
    jdoSetmesesServicioApn(this, mesesServicioApn);
  }

  public void setFotoTrabajador(String fotoTrabajador)
  {
    jdoSetfotoTrabajador(this, fotoTrabajador);
  }

  public String getFotoTrabajador()
  {
    return jdoGetfotoTrabajador(this);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 61;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Personal localPersonal = new Personal();
    localPersonal.jdoFlags = 1;
    localPersonal.jdoStateManager = paramStateManager;
    return localPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Personal localPersonal = new Personal();
    localPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPersonal.jdoFlags = 1;
    localPersonal.jdoStateManager = paramStateManager;
    return localPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.Puebloindigena);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.Tipodiscapacidad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicioApn);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaConyugue);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudadNacimiento);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudadResidencia);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.credencial);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasServicioApn);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.diestralidad);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccionResidencia);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.discapacidad);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.dobleNacionalidad);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.email);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.establecimientoSalud);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estadoCivil);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.estatura);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFallecimiento);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaNacimiento);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaNacionalizacion);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.fotoTrabajador);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gacetaNacionalizacion);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.gradoLicencia);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.grupoSanguineo);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPersonal);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.madrePadre);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.maneja);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.marcaVehiculo);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesServicioApn);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mismoOrganismoConyugue);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.modeloVehiculo);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nacionalidad);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nacionalizado);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreConyugue);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroLibretaMilitar);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroRif);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroSso);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.otraNormativaNac);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.paisNacionalidad);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.parroquia);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.password);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.peso);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.placaVehiculo);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.reingresable);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sectorTrabajoConyugue);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sexo);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoCelular);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoOficina);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoResidencia);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tenenciaVivienda);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tieneHijos);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tieneVehiculo);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoVivienda);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.zonaPostalResidencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.Puebloindigena = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.Tipodiscapacidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicioApn = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaConyugue = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudadNacimiento = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudadResidencia = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.credencial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasServicioApn = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diestralidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccionResidencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.discapacidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dobleNacionalidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.email = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.establecimientoSalud = ((EstablecimientoSalud)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estadoCivil = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatura = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFallecimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaNacimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaNacionalizacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fotoTrabajador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gacetaNacionalizacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gradoLicencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoSanguineo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.madrePadre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maneja = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.marcaVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesServicioApn = localStateManager.replacingIntField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mismoOrganismoConyugue = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.modeloVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nacionalidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nacionalizado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreConyugue = localStateManager.replacingStringField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroLibretaMilitar = localStateManager.replacingStringField(this, paramInt);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroRif = localStateManager.replacingStringField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroSso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otraNormativaNac = localStateManager.replacingStringField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paisNacionalidad = ((Pais)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parroquia = ((Parroquia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.password = localStateManager.replacingStringField(this, paramInt);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.peso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.placaVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reingresable = localStateManager.replacingStringField(this, paramInt);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sectorTrabajoConyugue = localStateManager.replacingStringField(this, paramInt);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sexo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoCelular = localStateManager.replacingStringField(this, paramInt);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoOficina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoResidencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tenenciaVivienda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tieneHijos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tieneVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoVivienda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.zonaPostalResidencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Personal paramPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.Puebloindigena = paramPersonal.Puebloindigena;
      return;
    case 1:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.Tipodiscapacidad = paramPersonal.Tipodiscapacidad;
      return;
    case 2:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicioApn = paramPersonal.aniosServicioApn;
      return;
    case 3:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramPersonal.cedula;
      return;
    case 4:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaConyugue = paramPersonal.cedulaConyugue;
      return;
    case 5:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.ciudadNacimiento = paramPersonal.ciudadNacimiento;
      return;
    case 6:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.ciudadResidencia = paramPersonal.ciudadResidencia;
      return;
    case 7:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.credencial = paramPersonal.credencial;
      return;
    case 8:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.diasServicioApn = paramPersonal.diasServicioApn;
      return;
    case 9:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.diestralidad = paramPersonal.diestralidad;
      return;
    case 10:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.direccionResidencia = paramPersonal.direccionResidencia;
      return;
    case 11:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.discapacidad = paramPersonal.discapacidad;
      return;
    case 12:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.dobleNacionalidad = paramPersonal.dobleNacionalidad;
      return;
    case 13:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.email = paramPersonal.email;
      return;
    case 14:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.establecimientoSalud = paramPersonal.establecimientoSalud;
      return;
    case 15:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.estadoCivil = paramPersonal.estadoCivil;
      return;
    case 16:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.estatura = paramPersonal.estatura;
      return;
    case 17:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFallecimiento = paramPersonal.fechaFallecimiento;
      return;
    case 18:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.fechaNacimiento = paramPersonal.fechaNacimiento;
      return;
    case 19:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.fechaNacionalizacion = paramPersonal.fechaNacionalizacion;
      return;
    case 20:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.fotoTrabajador = paramPersonal.fotoTrabajador;
      return;
    case 21:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.gacetaNacionalizacion = paramPersonal.gacetaNacionalizacion;
      return;
    case 22:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.gradoLicencia = paramPersonal.gradoLicencia;
      return;
    case 23:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.grupoSanguineo = paramPersonal.grupoSanguineo;
      return;
    case 24:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idPersonal = paramPersonal.idPersonal;
      return;
    case 25:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPersonal.idSitp;
      return;
    case 26:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.madrePadre = paramPersonal.madrePadre;
      return;
    case 27:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.maneja = paramPersonal.maneja;
      return;
    case 28:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.marcaVehiculo = paramPersonal.marcaVehiculo;
      return;
    case 29:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.mesesServicioApn = paramPersonal.mesesServicioApn;
      return;
    case 30:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.mismoOrganismoConyugue = paramPersonal.mismoOrganismoConyugue;
      return;
    case 31:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.modeloVehiculo = paramPersonal.modeloVehiculo;
      return;
    case 32:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.nacionalidad = paramPersonal.nacionalidad;
      return;
    case 33:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.nacionalizado = paramPersonal.nacionalizado;
      return;
    case 34:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramPersonal.nivelEducativo;
      return;
    case 35:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.nombreConyugue = paramPersonal.nombreConyugue;
      return;
    case 36:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.numeroLibretaMilitar = paramPersonal.numeroLibretaMilitar;
      return;
    case 37:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.numeroRif = paramPersonal.numeroRif;
      return;
    case 38:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.numeroSso = paramPersonal.numeroSso;
      return;
    case 39:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.otraNormativaNac = paramPersonal.otraNormativaNac;
      return;
    case 40:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.paisNacionalidad = paramPersonal.paisNacionalidad;
      return;
    case 41:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.parroquia = paramPersonal.parroquia;
      return;
    case 42:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.password = paramPersonal.password;
      return;
    case 43:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.peso = paramPersonal.peso;
      return;
    case 44:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.placaVehiculo = paramPersonal.placaVehiculo;
      return;
    case 45:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramPersonal.primerApellido;
      return;
    case 46:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramPersonal.primerNombre;
      return;
    case 47:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.reingresable = paramPersonal.reingresable;
      return;
    case 48:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.sectorTrabajoConyugue = paramPersonal.sectorTrabajoConyugue;
      return;
    case 49:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramPersonal.segundoApellido;
      return;
    case 50:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramPersonal.segundoNombre;
      return;
    case 51:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.sexo = paramPersonal.sexo;
      return;
    case 52:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoCelular = paramPersonal.telefonoCelular;
      return;
    case 53:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoOficina = paramPersonal.telefonoOficina;
      return;
    case 54:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoResidencia = paramPersonal.telefonoResidencia;
      return;
    case 55:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tenenciaVivienda = paramPersonal.tenenciaVivienda;
      return;
    case 56:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPersonal.tiempoSitp;
      return;
    case 57:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tieneHijos = paramPersonal.tieneHijos;
      return;
    case 58:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tieneVehiculo = paramPersonal.tieneVehiculo;
      return;
    case 59:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tipoVivienda = paramPersonal.tipoVivienda;
      return;
    case 60:
      if (paramPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.zonaPostalResidencia = paramPersonal.zonaPostalResidencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Personal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Personal localPersonal = (Personal)paramObject;
    if (localPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg1");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    localPersonalPK.idPersonal = this.idPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg1");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    this.idPersonal = localPersonalPK.idPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg2");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    localPersonalPK.idPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 24);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg2");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 24, localPersonalPK.idPersonal);
  }

  private static final String jdoGetPuebloindigena(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.Puebloindigena;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.Puebloindigena;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 0))
      return paramPersonal.Puebloindigena;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 0, paramPersonal.Puebloindigena);
  }

  private static final void jdoSetPuebloindigena(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.Puebloindigena = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.Puebloindigena = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 0, paramPersonal.Puebloindigena, paramString);
  }

  private static final String jdoGetTipodiscapacidad(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.Tipodiscapacidad;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.Tipodiscapacidad;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 1))
      return paramPersonal.Tipodiscapacidad;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 1, paramPersonal.Tipodiscapacidad);
  }

  private static final void jdoSetTipodiscapacidad(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.Tipodiscapacidad = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.Tipodiscapacidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 1, paramPersonal.Tipodiscapacidad, paramString);
  }

  private static final int jdoGetaniosServicioApn(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.aniosServicioApn;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.aniosServicioApn;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 2))
      return paramPersonal.aniosServicioApn;
    return localStateManager.getIntField(paramPersonal, jdoInheritedFieldCount + 2, paramPersonal.aniosServicioApn);
  }

  private static final void jdoSetaniosServicioApn(Personal paramPersonal, int paramInt)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.aniosServicioApn = paramInt;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.aniosServicioApn = paramInt;
      return;
    }
    localStateManager.setIntField(paramPersonal, jdoInheritedFieldCount + 2, paramPersonal.aniosServicioApn, paramInt);
  }

  private static final int jdoGetcedula(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.cedula;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.cedula;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 3))
      return paramPersonal.cedula;
    return localStateManager.getIntField(paramPersonal, jdoInheritedFieldCount + 3, paramPersonal.cedula);
  }

  private static final void jdoSetcedula(Personal paramPersonal, int paramInt)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramPersonal, jdoInheritedFieldCount + 3, paramPersonal.cedula, paramInt);
  }

  private static final int jdoGetcedulaConyugue(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.cedulaConyugue;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.cedulaConyugue;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 4))
      return paramPersonal.cedulaConyugue;
    return localStateManager.getIntField(paramPersonal, jdoInheritedFieldCount + 4, paramPersonal.cedulaConyugue);
  }

  private static final void jdoSetcedulaConyugue(Personal paramPersonal, int paramInt)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.cedulaConyugue = paramInt;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.cedulaConyugue = paramInt;
      return;
    }
    localStateManager.setIntField(paramPersonal, jdoInheritedFieldCount + 4, paramPersonal.cedulaConyugue, paramInt);
  }

  private static final Ciudad jdoGetciudadNacimiento(Personal paramPersonal)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.ciudadNacimiento;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 5))
      return paramPersonal.ciudadNacimiento;
    return (Ciudad)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 5, paramPersonal.ciudadNacimiento);
  }

  private static final void jdoSetciudadNacimiento(Personal paramPersonal, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.ciudadNacimiento = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 5, paramPersonal.ciudadNacimiento, paramCiudad);
  }

  private static final Ciudad jdoGetciudadResidencia(Personal paramPersonal)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.ciudadResidencia;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 6))
      return paramPersonal.ciudadResidencia;
    return (Ciudad)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 6, paramPersonal.ciudadResidencia);
  }

  private static final void jdoSetciudadResidencia(Personal paramPersonal, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.ciudadResidencia = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 6, paramPersonal.ciudadResidencia, paramCiudad);
  }

  private static final String jdoGetcredencial(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.credencial;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.credencial;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 7))
      return paramPersonal.credencial;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 7, paramPersonal.credencial);
  }

  private static final void jdoSetcredencial(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.credencial = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.credencial = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 7, paramPersonal.credencial, paramString);
  }

  private static final int jdoGetdiasServicioApn(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.diasServicioApn;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.diasServicioApn;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 8))
      return paramPersonal.diasServicioApn;
    return localStateManager.getIntField(paramPersonal, jdoInheritedFieldCount + 8, paramPersonal.diasServicioApn);
  }

  private static final void jdoSetdiasServicioApn(Personal paramPersonal, int paramInt)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.diasServicioApn = paramInt;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.diasServicioApn = paramInt;
      return;
    }
    localStateManager.setIntField(paramPersonal, jdoInheritedFieldCount + 8, paramPersonal.diasServicioApn, paramInt);
  }

  private static final String jdoGetdiestralidad(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.diestralidad;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.diestralidad;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 9))
      return paramPersonal.diestralidad;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 9, paramPersonal.diestralidad);
  }

  private static final void jdoSetdiestralidad(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.diestralidad = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.diestralidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 9, paramPersonal.diestralidad, paramString);
  }

  private static final String jdoGetdireccionResidencia(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.direccionResidencia;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.direccionResidencia;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 10))
      return paramPersonal.direccionResidencia;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 10, paramPersonal.direccionResidencia);
  }

  private static final void jdoSetdireccionResidencia(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.direccionResidencia = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.direccionResidencia = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 10, paramPersonal.direccionResidencia, paramString);
  }

  private static final String jdoGetdiscapacidad(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.discapacidad;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.discapacidad;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 11))
      return paramPersonal.discapacidad;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 11, paramPersonal.discapacidad);
  }

  private static final void jdoSetdiscapacidad(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.discapacidad = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.discapacidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 11, paramPersonal.discapacidad, paramString);
  }

  private static final String jdoGetdobleNacionalidad(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.dobleNacionalidad;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.dobleNacionalidad;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 12))
      return paramPersonal.dobleNacionalidad;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 12, paramPersonal.dobleNacionalidad);
  }

  private static final void jdoSetdobleNacionalidad(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.dobleNacionalidad = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.dobleNacionalidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 12, paramPersonal.dobleNacionalidad, paramString);
  }

  private static final String jdoGetemail(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.email;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.email;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 13))
      return paramPersonal.email;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 13, paramPersonal.email);
  }

  private static final void jdoSetemail(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.email = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.email = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 13, paramPersonal.email, paramString);
  }

  private static final EstablecimientoSalud jdoGetestablecimientoSalud(Personal paramPersonal)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.establecimientoSalud;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 14))
      return paramPersonal.establecimientoSalud;
    return (EstablecimientoSalud)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 14, paramPersonal.establecimientoSalud);
  }

  private static final void jdoSetestablecimientoSalud(Personal paramPersonal, EstablecimientoSalud paramEstablecimientoSalud)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.establecimientoSalud = paramEstablecimientoSalud;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 14, paramPersonal.establecimientoSalud, paramEstablecimientoSalud);
  }

  private static final String jdoGetestadoCivil(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.estadoCivil;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.estadoCivil;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 15))
      return paramPersonal.estadoCivil;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 15, paramPersonal.estadoCivil);
  }

  private static final void jdoSetestadoCivil(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.estadoCivil = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.estadoCivil = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 15, paramPersonal.estadoCivil, paramString);
  }

  private static final double jdoGetestatura(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.estatura;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.estatura;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 16))
      return paramPersonal.estatura;
    return localStateManager.getDoubleField(paramPersonal, jdoInheritedFieldCount + 16, paramPersonal.estatura);
  }

  private static final void jdoSetestatura(Personal paramPersonal, double paramDouble)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.estatura = paramDouble;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.estatura = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPersonal, jdoInheritedFieldCount + 16, paramPersonal.estatura, paramDouble);
  }

  private static final Date jdoGetfechaFallecimiento(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.fechaFallecimiento;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.fechaFallecimiento;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 17))
      return paramPersonal.fechaFallecimiento;
    return (Date)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 17, paramPersonal.fechaFallecimiento);
  }

  private static final void jdoSetfechaFallecimiento(Personal paramPersonal, Date paramDate)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.fechaFallecimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.fechaFallecimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 17, paramPersonal.fechaFallecimiento, paramDate);
  }

  private static final Date jdoGetfechaNacimiento(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.fechaNacimiento;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.fechaNacimiento;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 18))
      return paramPersonal.fechaNacimiento;
    return (Date)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 18, paramPersonal.fechaNacimiento);
  }

  private static final void jdoSetfechaNacimiento(Personal paramPersonal, Date paramDate)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.fechaNacimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.fechaNacimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 18, paramPersonal.fechaNacimiento, paramDate);
  }

  private static final Date jdoGetfechaNacionalizacion(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.fechaNacionalizacion;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.fechaNacionalizacion;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 19))
      return paramPersonal.fechaNacionalizacion;
    return (Date)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 19, paramPersonal.fechaNacionalizacion);
  }

  private static final void jdoSetfechaNacionalizacion(Personal paramPersonal, Date paramDate)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.fechaNacionalizacion = paramDate;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.fechaNacionalizacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 19, paramPersonal.fechaNacionalizacion, paramDate);
  }

  private static final String jdoGetfotoTrabajador(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.fotoTrabajador;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.fotoTrabajador;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 20))
      return paramPersonal.fotoTrabajador;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 20, paramPersonal.fotoTrabajador);
  }

  private static final void jdoSetfotoTrabajador(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.fotoTrabajador = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.fotoTrabajador = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 20, paramPersonal.fotoTrabajador, paramString);
  }

  private static final String jdoGetgacetaNacionalizacion(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.gacetaNacionalizacion;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.gacetaNacionalizacion;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 21))
      return paramPersonal.gacetaNacionalizacion;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 21, paramPersonal.gacetaNacionalizacion);
  }

  private static final void jdoSetgacetaNacionalizacion(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.gacetaNacionalizacion = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.gacetaNacionalizacion = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 21, paramPersonal.gacetaNacionalizacion, paramString);
  }

  private static final int jdoGetgradoLicencia(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.gradoLicencia;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.gradoLicencia;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 22))
      return paramPersonal.gradoLicencia;
    return localStateManager.getIntField(paramPersonal, jdoInheritedFieldCount + 22, paramPersonal.gradoLicencia);
  }

  private static final void jdoSetgradoLicencia(Personal paramPersonal, int paramInt)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.gradoLicencia = paramInt;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.gradoLicencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramPersonal, jdoInheritedFieldCount + 22, paramPersonal.gradoLicencia, paramInt);
  }

  private static final String jdoGetgrupoSanguineo(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.grupoSanguineo;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.grupoSanguineo;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 23))
      return paramPersonal.grupoSanguineo;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 23, paramPersonal.grupoSanguineo);
  }

  private static final void jdoSetgrupoSanguineo(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.grupoSanguineo = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.grupoSanguineo = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 23, paramPersonal.grupoSanguineo, paramString);
  }

  private static final long jdoGetidPersonal(Personal paramPersonal)
  {
    return paramPersonal.idPersonal;
  }

  private static final void jdoSetidPersonal(Personal paramPersonal, long paramLong)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.idPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramPersonal, jdoInheritedFieldCount + 24, paramPersonal.idPersonal, paramLong);
  }

  private static final int jdoGetidSitp(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.idSitp;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.idSitp;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 25))
      return paramPersonal.idSitp;
    return localStateManager.getIntField(paramPersonal, jdoInheritedFieldCount + 25, paramPersonal.idSitp);
  }

  private static final void jdoSetidSitp(Personal paramPersonal, int paramInt)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPersonal, jdoInheritedFieldCount + 25, paramPersonal.idSitp, paramInt);
  }

  private static final String jdoGetmadrePadre(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.madrePadre;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.madrePadre;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 26))
      return paramPersonal.madrePadre;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 26, paramPersonal.madrePadre);
  }

  private static final void jdoSetmadrePadre(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.madrePadre = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.madrePadre = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 26, paramPersonal.madrePadre, paramString);
  }

  private static final String jdoGetmaneja(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.maneja;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.maneja;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 27))
      return paramPersonal.maneja;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 27, paramPersonal.maneja);
  }

  private static final void jdoSetmaneja(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.maneja = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.maneja = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 27, paramPersonal.maneja, paramString);
  }

  private static final String jdoGetmarcaVehiculo(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.marcaVehiculo;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.marcaVehiculo;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 28))
      return paramPersonal.marcaVehiculo;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 28, paramPersonal.marcaVehiculo);
  }

  private static final void jdoSetmarcaVehiculo(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.marcaVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.marcaVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 28, paramPersonal.marcaVehiculo, paramString);
  }

  private static final int jdoGetmesesServicioApn(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.mesesServicioApn;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.mesesServicioApn;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 29))
      return paramPersonal.mesesServicioApn;
    return localStateManager.getIntField(paramPersonal, jdoInheritedFieldCount + 29, paramPersonal.mesesServicioApn);
  }

  private static final void jdoSetmesesServicioApn(Personal paramPersonal, int paramInt)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.mesesServicioApn = paramInt;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.mesesServicioApn = paramInt;
      return;
    }
    localStateManager.setIntField(paramPersonal, jdoInheritedFieldCount + 29, paramPersonal.mesesServicioApn, paramInt);
  }

  private static final String jdoGetmismoOrganismoConyugue(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.mismoOrganismoConyugue;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.mismoOrganismoConyugue;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 30))
      return paramPersonal.mismoOrganismoConyugue;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 30, paramPersonal.mismoOrganismoConyugue);
  }

  private static final void jdoSetmismoOrganismoConyugue(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.mismoOrganismoConyugue = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.mismoOrganismoConyugue = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 30, paramPersonal.mismoOrganismoConyugue, paramString);
  }

  private static final String jdoGetmodeloVehiculo(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.modeloVehiculo;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.modeloVehiculo;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 31))
      return paramPersonal.modeloVehiculo;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 31, paramPersonal.modeloVehiculo);
  }

  private static final void jdoSetmodeloVehiculo(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.modeloVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.modeloVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 31, paramPersonal.modeloVehiculo, paramString);
  }

  private static final String jdoGetnacionalidad(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.nacionalidad;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.nacionalidad;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 32))
      return paramPersonal.nacionalidad;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 32, paramPersonal.nacionalidad);
  }

  private static final void jdoSetnacionalidad(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.nacionalidad = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.nacionalidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 32, paramPersonal.nacionalidad, paramString);
  }

  private static final String jdoGetnacionalizado(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.nacionalizado;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.nacionalizado;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 33))
      return paramPersonal.nacionalizado;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 33, paramPersonal.nacionalizado);
  }

  private static final void jdoSetnacionalizado(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.nacionalizado = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.nacionalizado = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 33, paramPersonal.nacionalizado, paramString);
  }

  private static final String jdoGetnivelEducativo(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.nivelEducativo;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.nivelEducativo;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 34))
      return paramPersonal.nivelEducativo;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 34, paramPersonal.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 34, paramPersonal.nivelEducativo, paramString);
  }

  private static final String jdoGetnombreConyugue(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.nombreConyugue;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.nombreConyugue;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 35))
      return paramPersonal.nombreConyugue;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 35, paramPersonal.nombreConyugue);
  }

  private static final void jdoSetnombreConyugue(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.nombreConyugue = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.nombreConyugue = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 35, paramPersonal.nombreConyugue, paramString);
  }

  private static final String jdoGetnumeroLibretaMilitar(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.numeroLibretaMilitar;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.numeroLibretaMilitar;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 36))
      return paramPersonal.numeroLibretaMilitar;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 36, paramPersonal.numeroLibretaMilitar);
  }

  private static final void jdoSetnumeroLibretaMilitar(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.numeroLibretaMilitar = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.numeroLibretaMilitar = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 36, paramPersonal.numeroLibretaMilitar, paramString);
  }

  private static final String jdoGetnumeroRif(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.numeroRif;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.numeroRif;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 37))
      return paramPersonal.numeroRif;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 37, paramPersonal.numeroRif);
  }

  private static final void jdoSetnumeroRif(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.numeroRif = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.numeroRif = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 37, paramPersonal.numeroRif, paramString);
  }

  private static final String jdoGetnumeroSso(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.numeroSso;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.numeroSso;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 38))
      return paramPersonal.numeroSso;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 38, paramPersonal.numeroSso);
  }

  private static final void jdoSetnumeroSso(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.numeroSso = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.numeroSso = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 38, paramPersonal.numeroSso, paramString);
  }

  private static final String jdoGetotraNormativaNac(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.otraNormativaNac;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.otraNormativaNac;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 39))
      return paramPersonal.otraNormativaNac;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 39, paramPersonal.otraNormativaNac);
  }

  private static final void jdoSetotraNormativaNac(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.otraNormativaNac = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.otraNormativaNac = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 39, paramPersonal.otraNormativaNac, paramString);
  }

  private static final Pais jdoGetpaisNacionalidad(Personal paramPersonal)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.paisNacionalidad;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 40))
      return paramPersonal.paisNacionalidad;
    return (Pais)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 40, paramPersonal.paisNacionalidad);
  }

  private static final void jdoSetpaisNacionalidad(Personal paramPersonal, Pais paramPais)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.paisNacionalidad = paramPais;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 40, paramPersonal.paisNacionalidad, paramPais);
  }

  private static final Parroquia jdoGetparroquia(Personal paramPersonal)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.parroquia;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 41))
      return paramPersonal.parroquia;
    return (Parroquia)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 41, paramPersonal.parroquia);
  }

  private static final void jdoSetparroquia(Personal paramPersonal, Parroquia paramParroquia)
  {
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.parroquia = paramParroquia;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 41, paramPersonal.parroquia, paramParroquia);
  }

  private static final String jdoGetpassword(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.password;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.password;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 42))
      return paramPersonal.password;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 42, paramPersonal.password);
  }

  private static final void jdoSetpassword(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.password = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.password = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 42, paramPersonal.password, paramString);
  }

  private static final double jdoGetpeso(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.peso;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.peso;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 43))
      return paramPersonal.peso;
    return localStateManager.getDoubleField(paramPersonal, jdoInheritedFieldCount + 43, paramPersonal.peso);
  }

  private static final void jdoSetpeso(Personal paramPersonal, double paramDouble)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.peso = paramDouble;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.peso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPersonal, jdoInheritedFieldCount + 43, paramPersonal.peso, paramDouble);
  }

  private static final String jdoGetplacaVehiculo(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.placaVehiculo;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.placaVehiculo;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 44))
      return paramPersonal.placaVehiculo;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 44, paramPersonal.placaVehiculo);
  }

  private static final void jdoSetplacaVehiculo(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.placaVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.placaVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 44, paramPersonal.placaVehiculo, paramString);
  }

  private static final String jdoGetprimerApellido(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.primerApellido;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.primerApellido;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 45))
      return paramPersonal.primerApellido;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 45, paramPersonal.primerApellido);
  }

  private static final void jdoSetprimerApellido(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 45, paramPersonal.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.primerNombre;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.primerNombre;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 46))
      return paramPersonal.primerNombre;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 46, paramPersonal.primerNombre);
  }

  private static final void jdoSetprimerNombre(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 46, paramPersonal.primerNombre, paramString);
  }

  private static final String jdoGetreingresable(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.reingresable;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.reingresable;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 47))
      return paramPersonal.reingresable;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 47, paramPersonal.reingresable);
  }

  private static final void jdoSetreingresable(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.reingresable = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.reingresable = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 47, paramPersonal.reingresable, paramString);
  }

  private static final String jdoGetsectorTrabajoConyugue(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.sectorTrabajoConyugue;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.sectorTrabajoConyugue;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 48))
      return paramPersonal.sectorTrabajoConyugue;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 48, paramPersonal.sectorTrabajoConyugue);
  }

  private static final void jdoSetsectorTrabajoConyugue(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.sectorTrabajoConyugue = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.sectorTrabajoConyugue = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 48, paramPersonal.sectorTrabajoConyugue, paramString);
  }

  private static final String jdoGetsegundoApellido(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.segundoApellido;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.segundoApellido;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 49))
      return paramPersonal.segundoApellido;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 49, paramPersonal.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 49, paramPersonal.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.segundoNombre;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.segundoNombre;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 50))
      return paramPersonal.segundoNombre;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 50, paramPersonal.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 50, paramPersonal.segundoNombre, paramString);
  }

  private static final String jdoGetsexo(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.sexo;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.sexo;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 51))
      return paramPersonal.sexo;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 51, paramPersonal.sexo);
  }

  private static final void jdoSetsexo(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.sexo = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.sexo = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 51, paramPersonal.sexo, paramString);
  }

  private static final String jdoGettelefonoCelular(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.telefonoCelular;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.telefonoCelular;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 52))
      return paramPersonal.telefonoCelular;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 52, paramPersonal.telefonoCelular);
  }

  private static final void jdoSettelefonoCelular(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.telefonoCelular = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.telefonoCelular = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 52, paramPersonal.telefonoCelular, paramString);
  }

  private static final String jdoGettelefonoOficina(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.telefonoOficina;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.telefonoOficina;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 53))
      return paramPersonal.telefonoOficina;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 53, paramPersonal.telefonoOficina);
  }

  private static final void jdoSettelefonoOficina(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.telefonoOficina = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.telefonoOficina = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 53, paramPersonal.telefonoOficina, paramString);
  }

  private static final String jdoGettelefonoResidencia(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.telefonoResidencia;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.telefonoResidencia;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 54))
      return paramPersonal.telefonoResidencia;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 54, paramPersonal.telefonoResidencia);
  }

  private static final void jdoSettelefonoResidencia(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.telefonoResidencia = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.telefonoResidencia = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 54, paramPersonal.telefonoResidencia, paramString);
  }

  private static final String jdoGettenenciaVivienda(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.tenenciaVivienda;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.tenenciaVivienda;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 55))
      return paramPersonal.tenenciaVivienda;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 55, paramPersonal.tenenciaVivienda);
  }

  private static final void jdoSettenenciaVivienda(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.tenenciaVivienda = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.tenenciaVivienda = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 55, paramPersonal.tenenciaVivienda, paramString);
  }

  private static final Date jdoGettiempoSitp(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.tiempoSitp;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.tiempoSitp;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 56))
      return paramPersonal.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPersonal, jdoInheritedFieldCount + 56, paramPersonal.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Personal paramPersonal, Date paramDate)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPersonal, jdoInheritedFieldCount + 56, paramPersonal.tiempoSitp, paramDate);
  }

  private static final String jdoGettieneHijos(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.tieneHijos;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.tieneHijos;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 57))
      return paramPersonal.tieneHijos;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 57, paramPersonal.tieneHijos);
  }

  private static final void jdoSettieneHijos(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.tieneHijos = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.tieneHijos = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 57, paramPersonal.tieneHijos, paramString);
  }

  private static final String jdoGettieneVehiculo(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.tieneVehiculo;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.tieneVehiculo;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 58))
      return paramPersonal.tieneVehiculo;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 58, paramPersonal.tieneVehiculo);
  }

  private static final void jdoSettieneVehiculo(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.tieneVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.tieneVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 58, paramPersonal.tieneVehiculo, paramString);
  }

  private static final String jdoGettipoVivienda(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.tipoVivienda;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.tipoVivienda;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 59))
      return paramPersonal.tipoVivienda;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 59, paramPersonal.tipoVivienda);
  }

  private static final void jdoSettipoVivienda(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.tipoVivienda = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.tipoVivienda = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 59, paramPersonal.tipoVivienda, paramString);
  }

  private static final String jdoGetzonaPostalResidencia(Personal paramPersonal)
  {
    if (paramPersonal.jdoFlags <= 0)
      return paramPersonal.zonaPostalResidencia;
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPersonal.zonaPostalResidencia;
    if (localStateManager.isLoaded(paramPersonal, jdoInheritedFieldCount + 60))
      return paramPersonal.zonaPostalResidencia;
    return localStateManager.getStringField(paramPersonal, jdoInheritedFieldCount + 60, paramPersonal.zonaPostalResidencia);
  }

  private static final void jdoSetzonaPostalResidencia(Personal paramPersonal, String paramString)
  {
    if (paramPersonal.jdoFlags == 0)
    {
      paramPersonal.zonaPostalResidencia = paramString;
      return;
    }
    StateManager localStateManager = paramPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonal.zonaPostalResidencia = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonal, jdoInheritedFieldCount + 60, paramPersonal.zonaPostalResidencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}