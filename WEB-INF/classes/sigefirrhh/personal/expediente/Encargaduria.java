package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Encargaduria
  implements Serializable, PersistenceCapable
{
  private long idEncargaduria;
  private Date fechaInicio;
  private Date fechaFin;
  private String cargo;
  private String dependencia;
  private String observaciones;
  private Personal personal;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargo", "dependencia", "fechaFin", "fechaInicio", "idEncargaduria", "idSitp", "observaciones", "organismo", "personal", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcargo(this) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this));
  }

  public String getCargo() {
    return jdoGetcargo(this);
  }

  public String getDependencia() {
    return jdoGetdependencia(this);
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public long getIdEncargaduria() {
    return jdoGetidEncargaduria(this);
  }

  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setCargo(String string) {
    jdoSetcargo(this, string);
  }

  public void setDependencia(String string) {
    jdoSetdependencia(this, string);
  }

  public void setFechaFin(Date date) {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date) {
    jdoSetfechaInicio(this, date);
  }

  public void setIdEncargaduria(long l) {
    jdoSetidEncargaduria(this, l);
  }

  public void setObservaciones(String string) {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Encargaduria"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Encargaduria());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Encargaduria localEncargaduria = new Encargaduria();
    localEncargaduria.jdoFlags = 1;
    localEncargaduria.jdoStateManager = paramStateManager;
    return localEncargaduria;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Encargaduria localEncargaduria = new Encargaduria();
    localEncargaduria.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEncargaduria.jdoFlags = 1;
    localEncargaduria.jdoStateManager = paramStateManager;
    return localEncargaduria;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.dependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEncargaduria);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEncargaduria = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Encargaduria paramEncargaduria, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramEncargaduria.cargo;
      return;
    case 1:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramEncargaduria.dependencia;
      return;
    case 2:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramEncargaduria.fechaFin;
      return;
    case 3:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramEncargaduria.fechaInicio;
      return;
    case 4:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.idEncargaduria = paramEncargaduria.idEncargaduria;
      return;
    case 5:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramEncargaduria.idSitp;
      return;
    case 6:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramEncargaduria.observaciones;
      return;
    case 7:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramEncargaduria.organismo;
      return;
    case 8:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramEncargaduria.personal;
      return;
    case 9:
      if (paramEncargaduria == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramEncargaduria.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Encargaduria))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Encargaduria localEncargaduria = (Encargaduria)paramObject;
    if (localEncargaduria.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEncargaduria, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EncargaduriaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EncargaduriaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EncargaduriaPK))
      throw new IllegalArgumentException("arg1");
    EncargaduriaPK localEncargaduriaPK = (EncargaduriaPK)paramObject;
    localEncargaduriaPK.idEncargaduria = this.idEncargaduria;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EncargaduriaPK))
      throw new IllegalArgumentException("arg1");
    EncargaduriaPK localEncargaduriaPK = (EncargaduriaPK)paramObject;
    this.idEncargaduria = localEncargaduriaPK.idEncargaduria;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EncargaduriaPK))
      throw new IllegalArgumentException("arg2");
    EncargaduriaPK localEncargaduriaPK = (EncargaduriaPK)paramObject;
    localEncargaduriaPK.idEncargaduria = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EncargaduriaPK))
      throw new IllegalArgumentException("arg2");
    EncargaduriaPK localEncargaduriaPK = (EncargaduriaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localEncargaduriaPK.idEncargaduria);
  }

  private static final String jdoGetcargo(Encargaduria paramEncargaduria)
  {
    if (paramEncargaduria.jdoFlags <= 0)
      return paramEncargaduria.cargo;
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.cargo;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 0))
      return paramEncargaduria.cargo;
    return localStateManager.getStringField(paramEncargaduria, jdoInheritedFieldCount + 0, paramEncargaduria.cargo);
  }

  private static final void jdoSetcargo(Encargaduria paramEncargaduria, String paramString)
  {
    if (paramEncargaduria.jdoFlags == 0)
    {
      paramEncargaduria.cargo = paramString;
      return;
    }
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.cargo = paramString;
      return;
    }
    localStateManager.setStringField(paramEncargaduria, jdoInheritedFieldCount + 0, paramEncargaduria.cargo, paramString);
  }

  private static final String jdoGetdependencia(Encargaduria paramEncargaduria)
  {
    if (paramEncargaduria.jdoFlags <= 0)
      return paramEncargaduria.dependencia;
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.dependencia;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 1))
      return paramEncargaduria.dependencia;
    return localStateManager.getStringField(paramEncargaduria, jdoInheritedFieldCount + 1, paramEncargaduria.dependencia);
  }

  private static final void jdoSetdependencia(Encargaduria paramEncargaduria, String paramString)
  {
    if (paramEncargaduria.jdoFlags == 0)
    {
      paramEncargaduria.dependencia = paramString;
      return;
    }
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.dependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramEncargaduria, jdoInheritedFieldCount + 1, paramEncargaduria.dependencia, paramString);
  }

  private static final Date jdoGetfechaFin(Encargaduria paramEncargaduria)
  {
    if (paramEncargaduria.jdoFlags <= 0)
      return paramEncargaduria.fechaFin;
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.fechaFin;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 2))
      return paramEncargaduria.fechaFin;
    return (Date)localStateManager.getObjectField(paramEncargaduria, jdoInheritedFieldCount + 2, paramEncargaduria.fechaFin);
  }

  private static final void jdoSetfechaFin(Encargaduria paramEncargaduria, Date paramDate)
  {
    if (paramEncargaduria.jdoFlags == 0)
    {
      paramEncargaduria.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEncargaduria, jdoInheritedFieldCount + 2, paramEncargaduria.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Encargaduria paramEncargaduria)
  {
    if (paramEncargaduria.jdoFlags <= 0)
      return paramEncargaduria.fechaInicio;
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.fechaInicio;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 3))
      return paramEncargaduria.fechaInicio;
    return (Date)localStateManager.getObjectField(paramEncargaduria, jdoInheritedFieldCount + 3, paramEncargaduria.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Encargaduria paramEncargaduria, Date paramDate)
  {
    if (paramEncargaduria.jdoFlags == 0)
    {
      paramEncargaduria.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEncargaduria, jdoInheritedFieldCount + 3, paramEncargaduria.fechaInicio, paramDate);
  }

  private static final long jdoGetidEncargaduria(Encargaduria paramEncargaduria)
  {
    return paramEncargaduria.idEncargaduria;
  }

  private static final void jdoSetidEncargaduria(Encargaduria paramEncargaduria, long paramLong)
  {
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.idEncargaduria = paramLong;
      return;
    }
    localStateManager.setLongField(paramEncargaduria, jdoInheritedFieldCount + 4, paramEncargaduria.idEncargaduria, paramLong);
  }

  private static final int jdoGetidSitp(Encargaduria paramEncargaduria)
  {
    if (paramEncargaduria.jdoFlags <= 0)
      return paramEncargaduria.idSitp;
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.idSitp;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 5))
      return paramEncargaduria.idSitp;
    return localStateManager.getIntField(paramEncargaduria, jdoInheritedFieldCount + 5, paramEncargaduria.idSitp);
  }

  private static final void jdoSetidSitp(Encargaduria paramEncargaduria, int paramInt)
  {
    if (paramEncargaduria.jdoFlags == 0)
    {
      paramEncargaduria.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramEncargaduria, jdoInheritedFieldCount + 5, paramEncargaduria.idSitp, paramInt);
  }

  private static final String jdoGetobservaciones(Encargaduria paramEncargaduria)
  {
    if (paramEncargaduria.jdoFlags <= 0)
      return paramEncargaduria.observaciones;
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.observaciones;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 6))
      return paramEncargaduria.observaciones;
    return localStateManager.getStringField(paramEncargaduria, jdoInheritedFieldCount + 6, paramEncargaduria.observaciones);
  }

  private static final void jdoSetobservaciones(Encargaduria paramEncargaduria, String paramString)
  {
    if (paramEncargaduria.jdoFlags == 0)
    {
      paramEncargaduria.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramEncargaduria, jdoInheritedFieldCount + 6, paramEncargaduria.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(Encargaduria paramEncargaduria)
  {
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.organismo;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 7))
      return paramEncargaduria.organismo;
    return (Organismo)localStateManager.getObjectField(paramEncargaduria, jdoInheritedFieldCount + 7, paramEncargaduria.organismo);
  }

  private static final void jdoSetorganismo(Encargaduria paramEncargaduria, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramEncargaduria, jdoInheritedFieldCount + 7, paramEncargaduria.organismo, paramOrganismo);
  }

  private static final Personal jdoGetpersonal(Encargaduria paramEncargaduria)
  {
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.personal;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 8))
      return paramEncargaduria.personal;
    return (Personal)localStateManager.getObjectField(paramEncargaduria, jdoInheritedFieldCount + 8, paramEncargaduria.personal);
  }

  private static final void jdoSetpersonal(Encargaduria paramEncargaduria, Personal paramPersonal)
  {
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramEncargaduria, jdoInheritedFieldCount + 8, paramEncargaduria.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Encargaduria paramEncargaduria)
  {
    if (paramEncargaduria.jdoFlags <= 0)
      return paramEncargaduria.tiempoSitp;
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
      return paramEncargaduria.tiempoSitp;
    if (localStateManager.isLoaded(paramEncargaduria, jdoInheritedFieldCount + 9))
      return paramEncargaduria.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramEncargaduria, jdoInheritedFieldCount + 9, paramEncargaduria.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Encargaduria paramEncargaduria, Date paramDate)
  {
    if (paramEncargaduria.jdoFlags == 0)
    {
      paramEncargaduria.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramEncargaduria.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncargaduria.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEncargaduria, jdoInheritedFieldCount + 9, paramEncargaduria.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}