package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.TipoReconocimiento;

public class Reconocimiento
  implements Serializable, PersistenceCapable
{
  private long idReconocimiento;
  private TipoReconocimiento tipoReconocimiento;
  private Date fecha;
  private String observaciones;
  private Personal personal;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fecha", "idReconocimiento", "idSitp", "observaciones", "organismo", "personal", "tiempoSitp", "tipoReconocimiento" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.TipoReconocimiento") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 26, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoReconocimiento(this).getDescripcion() + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfecha(this));
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public long getIdReconocimiento()
  {
    return jdoGetidReconocimiento(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoReconocimiento getTipoReconocimiento()
  {
    return jdoGettipoReconocimiento(this);
  }

  public void setFecha(Date date)
  {
    jdoSetfecha(this, date);
  }

  public void setIdReconocimiento(long l)
  {
    jdoSetidReconocimiento(this, l);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoReconocimiento(TipoReconocimiento reconocimiento)
  {
    jdoSettipoReconocimiento(this, reconocimiento);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Reconocimiento"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Reconocimiento());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Reconocimiento localReconocimiento = new Reconocimiento();
    localReconocimiento.jdoFlags = 1;
    localReconocimiento.jdoStateManager = paramStateManager;
    return localReconocimiento;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Reconocimiento localReconocimiento = new Reconocimiento();
    localReconocimiento.jdoCopyKeyFieldsFromObjectId(paramObject);
    localReconocimiento.jdoFlags = 1;
    localReconocimiento.jdoStateManager = paramStateManager;
    return localReconocimiento;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idReconocimiento);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoReconocimiento);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idReconocimiento = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoReconocimiento = ((TipoReconocimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Reconocimiento paramReconocimiento, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramReconocimiento.fecha;
      return;
    case 1:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.idReconocimiento = paramReconocimiento.idReconocimiento;
      return;
    case 2:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramReconocimiento.idSitp;
      return;
    case 3:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramReconocimiento.observaciones;
      return;
    case 4:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramReconocimiento.organismo;
      return;
    case 5:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramReconocimiento.personal;
      return;
    case 6:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramReconocimiento.tiempoSitp;
      return;
    case 7:
      if (paramReconocimiento == null)
        throw new IllegalArgumentException("arg1");
      this.tipoReconocimiento = paramReconocimiento.tipoReconocimiento;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Reconocimiento))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Reconocimiento localReconocimiento = (Reconocimiento)paramObject;
    if (localReconocimiento.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localReconocimiento, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ReconocimientoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ReconocimientoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ReconocimientoPK))
      throw new IllegalArgumentException("arg1");
    ReconocimientoPK localReconocimientoPK = (ReconocimientoPK)paramObject;
    localReconocimientoPK.idReconocimiento = this.idReconocimiento;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ReconocimientoPK))
      throw new IllegalArgumentException("arg1");
    ReconocimientoPK localReconocimientoPK = (ReconocimientoPK)paramObject;
    this.idReconocimiento = localReconocimientoPK.idReconocimiento;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ReconocimientoPK))
      throw new IllegalArgumentException("arg2");
    ReconocimientoPK localReconocimientoPK = (ReconocimientoPK)paramObject;
    localReconocimientoPK.idReconocimiento = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ReconocimientoPK))
      throw new IllegalArgumentException("arg2");
    ReconocimientoPK localReconocimientoPK = (ReconocimientoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localReconocimientoPK.idReconocimiento);
  }

  private static final Date jdoGetfecha(Reconocimiento paramReconocimiento)
  {
    if (paramReconocimiento.jdoFlags <= 0)
      return paramReconocimiento.fecha;
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramReconocimiento.fecha;
    if (localStateManager.isLoaded(paramReconocimiento, jdoInheritedFieldCount + 0))
      return paramReconocimiento.fecha;
    return (Date)localStateManager.getObjectField(paramReconocimiento, jdoInheritedFieldCount + 0, paramReconocimiento.fecha);
  }

  private static final void jdoSetfecha(Reconocimiento paramReconocimiento, Date paramDate)
  {
    if (paramReconocimiento.jdoFlags == 0)
    {
      paramReconocimiento.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramReconocimiento, jdoInheritedFieldCount + 0, paramReconocimiento.fecha, paramDate);
  }

  private static final long jdoGetidReconocimiento(Reconocimiento paramReconocimiento)
  {
    return paramReconocimiento.idReconocimiento;
  }

  private static final void jdoSetidReconocimiento(Reconocimiento paramReconocimiento, long paramLong)
  {
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.idReconocimiento = paramLong;
      return;
    }
    localStateManager.setLongField(paramReconocimiento, jdoInheritedFieldCount + 1, paramReconocimiento.idReconocimiento, paramLong);
  }

  private static final int jdoGetidSitp(Reconocimiento paramReconocimiento)
  {
    if (paramReconocimiento.jdoFlags <= 0)
      return paramReconocimiento.idSitp;
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramReconocimiento.idSitp;
    if (localStateManager.isLoaded(paramReconocimiento, jdoInheritedFieldCount + 2))
      return paramReconocimiento.idSitp;
    return localStateManager.getIntField(paramReconocimiento, jdoInheritedFieldCount + 2, paramReconocimiento.idSitp);
  }

  private static final void jdoSetidSitp(Reconocimiento paramReconocimiento, int paramInt)
  {
    if (paramReconocimiento.jdoFlags == 0)
    {
      paramReconocimiento.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramReconocimiento, jdoInheritedFieldCount + 2, paramReconocimiento.idSitp, paramInt);
  }

  private static final String jdoGetobservaciones(Reconocimiento paramReconocimiento)
  {
    if (paramReconocimiento.jdoFlags <= 0)
      return paramReconocimiento.observaciones;
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramReconocimiento.observaciones;
    if (localStateManager.isLoaded(paramReconocimiento, jdoInheritedFieldCount + 3))
      return paramReconocimiento.observaciones;
    return localStateManager.getStringField(paramReconocimiento, jdoInheritedFieldCount + 3, paramReconocimiento.observaciones);
  }

  private static final void jdoSetobservaciones(Reconocimiento paramReconocimiento, String paramString)
  {
    if (paramReconocimiento.jdoFlags == 0)
    {
      paramReconocimiento.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramReconocimiento, jdoInheritedFieldCount + 3, paramReconocimiento.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(Reconocimiento paramReconocimiento)
  {
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramReconocimiento.organismo;
    if (localStateManager.isLoaded(paramReconocimiento, jdoInheritedFieldCount + 4))
      return paramReconocimiento.organismo;
    return (Organismo)localStateManager.getObjectField(paramReconocimiento, jdoInheritedFieldCount + 4, paramReconocimiento.organismo);
  }

  private static final void jdoSetorganismo(Reconocimiento paramReconocimiento, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramReconocimiento, jdoInheritedFieldCount + 4, paramReconocimiento.organismo, paramOrganismo);
  }

  private static final Personal jdoGetpersonal(Reconocimiento paramReconocimiento)
  {
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramReconocimiento.personal;
    if (localStateManager.isLoaded(paramReconocimiento, jdoInheritedFieldCount + 5))
      return paramReconocimiento.personal;
    return (Personal)localStateManager.getObjectField(paramReconocimiento, jdoInheritedFieldCount + 5, paramReconocimiento.personal);
  }

  private static final void jdoSetpersonal(Reconocimiento paramReconocimiento, Personal paramPersonal)
  {
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramReconocimiento, jdoInheritedFieldCount + 5, paramReconocimiento.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Reconocimiento paramReconocimiento)
  {
    if (paramReconocimiento.jdoFlags <= 0)
      return paramReconocimiento.tiempoSitp;
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramReconocimiento.tiempoSitp;
    if (localStateManager.isLoaded(paramReconocimiento, jdoInheritedFieldCount + 6))
      return paramReconocimiento.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramReconocimiento, jdoInheritedFieldCount + 6, paramReconocimiento.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Reconocimiento paramReconocimiento, Date paramDate)
  {
    if (paramReconocimiento.jdoFlags == 0)
    {
      paramReconocimiento.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramReconocimiento, jdoInheritedFieldCount + 6, paramReconocimiento.tiempoSitp, paramDate);
  }

  private static final TipoReconocimiento jdoGettipoReconocimiento(Reconocimiento paramReconocimiento)
  {
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
      return paramReconocimiento.tipoReconocimiento;
    if (localStateManager.isLoaded(paramReconocimiento, jdoInheritedFieldCount + 7))
      return paramReconocimiento.tipoReconocimiento;
    return (TipoReconocimiento)localStateManager.getObjectField(paramReconocimiento, jdoInheritedFieldCount + 7, paramReconocimiento.tipoReconocimiento);
  }

  private static final void jdoSettipoReconocimiento(Reconocimiento paramReconocimiento, TipoReconocimiento paramTipoReconocimiento)
  {
    StateManager localStateManager = paramReconocimiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramReconocimiento.tipoReconocimiento = paramTipoReconocimiento;
      return;
    }
    localStateManager.setObjectField(paramReconocimiento, jdoInheritedFieldCount + 7, paramReconocimiento.tipoReconocimiento, paramTipoReconocimiento);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}