package sigefirrhh.personal.expediente;

import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class FamiliarNoGenBeanBusiness extends FamiliarBeanBusiness
{
  Logger log = Logger.getLogger(FamiliarNoGenBeanBusiness.class.getName());

  public Collection findByPersonalAndParentesco(long idPersonal, String parentesco)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && parentesco == pParentesco";

    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdPersonal, String pParentesco");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pParentesco", parentesco);

    query.setOrdering("primerApellido ascending, primerNombre ascending");

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliar);

    return colFamiliar;
  }

  public Collection findByPersonalParentescoEdadMaxima(long idPersonal, String parentesco, int edadMaxima) throws Exception
  {
    Calendar fechaTope = Calendar.getInstance();
    fechaTope.add(1, -edadMaxima);
    fechaTope.add(5, 1);

    this.log.error("fecha_tope.........................." + fechaTope.getTime());
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && parentesco == pParentesco && fechaNacimiento >= pFechaTope";

    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdPersonal, String pParentesco, java.util.Date pFechaTope");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pParentesco", parentesco);
    parameters.put("pFechaTope", fechaTope.getTime());

    query.setOrdering("primerApellido ascending, primerNombre ascending");

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliar);

    return colFamiliar;
  }

  public Collection findByPersonalParentescoPensionado(long idPersonal, String parentescoH, String parentescoC, int edadMaximaH, int edadMaximaF, int edadMaximaM)
    throws Exception
  {
    Calendar fechaTopeH = Calendar.getInstance();
    fechaTopeH.add(1, -edadMaximaH);
    fechaTopeH.add(5, 1);
    Calendar fechaTopeF = Calendar.getInstance();
    fechaTopeF.add(1, -edadMaximaF);
    fechaTopeF.add(5, 1);
    Calendar fechaTopeM = Calendar.getInstance();
    fechaTopeM.add(1, -edadMaximaM);
    fechaTopeM.add(5, 1);

    this.log.error("fecha_tope.........................." + fechaTopeH.getTime());
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && ((parentesco == pParentescoH && fechaNacimiento >= pFechaTopeH)|| (parentesco == pParentescoC && fechaNacimiento <= pFechaTopeF && sexo ==pSexoF)|| (parentesco == pParentescoC && fechaNacimiento <= pFechaTopeM && sexo ==pSexoM))";

    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdPersonal, String pParentescoH, String pParentescoC ,java.util.Date pFechaTopeH,java.util.Date pFechaTopeF,java.util.Date pFechaTopeM, String pSexoF, String pSexoM ");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pParentescoH", parentescoH);
    parameters.put("pFechaTopeH", fechaTopeH.getTime());
    parameters.put("pParentescoC", parentescoC);
    parameters.put("pFechaTopeF", fechaTopeF.getTime());
    parameters.put("pFechaTopeM", fechaTopeM.getTime());
    parameters.put("pSexoF", "F");
    parameters.put("pSexoM", "M");

    query.setOrdering("primerApellido ascending, primerNombre ascending");

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliar);

    return colFamiliar;
  }
}