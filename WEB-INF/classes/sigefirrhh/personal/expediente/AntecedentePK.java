package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class AntecedentePK
  implements Serializable
{
  public long idAntecedente;

  public AntecedentePK()
  {
  }

  public AntecedentePK(long idAntecedente)
  {
    this.idAntecedente = idAntecedente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AntecedentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AntecedentePK thatPK)
  {
    return 
      this.idAntecedente == thatPK.idAntecedente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAntecedente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAntecedente);
  }
}