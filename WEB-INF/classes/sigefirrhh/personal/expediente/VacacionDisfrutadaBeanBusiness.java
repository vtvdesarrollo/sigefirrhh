package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class VacacionDisfrutadaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    VacacionDisfrutada vacacionDisfrutadaNew = 
      (VacacionDisfrutada)BeanUtils.cloneBean(
      vacacionDisfrutada);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionDisfrutadaNew.getTipoPersonal() != null) {
      vacacionDisfrutadaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionDisfrutadaNew.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (vacacionDisfrutadaNew.getPersonal() != null) {
      vacacionDisfrutadaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        vacacionDisfrutadaNew.getPersonal().getIdPersonal()));
    }

    VacacionBeanBusiness vacacionBeanBusiness = new VacacionBeanBusiness();

    if (vacacionDisfrutadaNew.getVacacion() != null) {
      vacacionDisfrutadaNew.setVacacion(
        vacacionBeanBusiness.findVacacionById(
        vacacionDisfrutadaNew.getVacacion().getIdVacacion()));
    }
    vacacionDisfrutadaNew.getVacacion().setDiasDisfrute(vacacionDisfrutadaNew.getVacacion().getDiasDisfrute() + vacacionDisfrutadaNew.getDiasDisfrute());
    vacacionBeanBusiness.updateVacacion(vacacionDisfrutadaNew.getVacacion());

    pm.makePersistent(vacacionDisfrutadaNew);
  }

  public void updateVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada) throws Exception
  {
    VacacionDisfrutada vacacionDisfrutadaModify = 
      findVacacionDisfrutadaById(vacacionDisfrutada.getIdVacacionDisfrutada());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionDisfrutada.getTipoPersonal() != null) {
      vacacionDisfrutada.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionDisfrutada.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (vacacionDisfrutada.getPersonal() != null) {
      vacacionDisfrutada.setPersonal(
        personalBeanBusiness.findPersonalById(
        vacacionDisfrutada.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(vacacionDisfrutadaModify, vacacionDisfrutada);
  }

  public void deleteVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada) throws Exception {
    PersistenceManager pm = PMThread.getPM();

    VacacionBeanBusiness vacacionBeanBusiness = new VacacionBeanBusiness();
    VacacionDisfrutada vacacionDisfrutadaDelete = 
      findVacacionDisfrutadaById(vacacionDisfrutada.getIdVacacionDisfrutada());

    vacacionDisfrutadaDelete.getVacacion().setDiasDisfrute(vacacionDisfrutadaDelete.getVacacion().getDiasDisfrute() - vacacionDisfrutadaDelete.getDiasDisfrute());
    vacacionBeanBusiness.updateVacacion(vacacionDisfrutadaDelete.getVacacion());

    pm.deletePersistent(vacacionDisfrutadaDelete);
  }

  public VacacionDisfrutada findVacacionDisfrutadaById(long idVacacionDisfrutada) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idVacacionDisfrutada == pIdVacacionDisfrutada";
    Query query = pm.newQuery(VacacionDisfrutada.class, filter);

    query.declareParameters("long pIdVacacionDisfrutada");

    parameters.put("pIdVacacionDisfrutada", new Long(idVacacionDisfrutada));

    Collection colVacacionDisfrutada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colVacacionDisfrutada.iterator();
    return (VacacionDisfrutada)iterator.next();
  }

  public Collection findVacacionDisfrutadaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent vacacionDisfrutadaExtent = pm.getExtent(
      VacacionDisfrutada.class, true);
    Query query = pm.newQuery(vacacionDisfrutadaExtent);
    query.setOrdering("anio ascending, fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(VacacionDisfrutada.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, fechaInicio ascending");

    Collection colVacacionDisfrutada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVacacionDisfrutada);

    return colVacacionDisfrutada;
  }
}