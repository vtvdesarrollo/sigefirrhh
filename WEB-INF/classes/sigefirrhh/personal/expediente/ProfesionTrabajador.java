package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Profesion;

public class ProfesionTrabajador
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idProfesionTrabajador;
  private Profesion profesion;
  private Personal personal;
  private String actualmente;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "actualmente", "idProfesionTrabajador", "idSitp", "personal", "profesion", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.personal.Profesion"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.ProfesionTrabajador"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ProfesionTrabajador());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ProfesionTrabajador()
  {
    jdoSetactualmente(this, "N");
  }

  public String toString()
  {
    return jdoGetprofesion(this).getNombre();
  }

  public long getIdProfesionTrabajador()
  {
    return jdoGetidProfesionTrabajador(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public Profesion getProfesion()
  {
    return jdoGetprofesion(this);
  }

  public void setIdProfesionTrabajador(long l)
  {
    jdoSetidProfesionTrabajador(this, l);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setProfesion(Profesion profesion)
  {
    jdoSetprofesion(this, profesion);
  }

  public String getActualmente()
  {
    return jdoGetactualmente(this);
  }

  public void setActualmente(String string)
  {
    jdoSetactualmente(this, string);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ProfesionTrabajador localProfesionTrabajador = new ProfesionTrabajador();
    localProfesionTrabajador.jdoFlags = 1;
    localProfesionTrabajador.jdoStateManager = paramStateManager;
    return localProfesionTrabajador;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ProfesionTrabajador localProfesionTrabajador = new ProfesionTrabajador();
    localProfesionTrabajador.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProfesionTrabajador.jdoFlags = 1;
    localProfesionTrabajador.jdoStateManager = paramStateManager;
    return localProfesionTrabajador;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.actualmente);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProfesionTrabajador);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.profesion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.actualmente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProfesionTrabajador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.profesion = ((Profesion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ProfesionTrabajador paramProfesionTrabajador, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProfesionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.actualmente = paramProfesionTrabajador.actualmente;
      return;
    case 1:
      if (paramProfesionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.idProfesionTrabajador = paramProfesionTrabajador.idProfesionTrabajador;
      return;
    case 2:
      if (paramProfesionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramProfesionTrabajador.idSitp;
      return;
    case 3:
      if (paramProfesionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramProfesionTrabajador.personal;
      return;
    case 4:
      if (paramProfesionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.profesion = paramProfesionTrabajador.profesion;
      return;
    case 5:
      if (paramProfesionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramProfesionTrabajador.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ProfesionTrabajador))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ProfesionTrabajador localProfesionTrabajador = (ProfesionTrabajador)paramObject;
    if (localProfesionTrabajador.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProfesionTrabajador, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProfesionTrabajadorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProfesionTrabajadorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProfesionTrabajadorPK))
      throw new IllegalArgumentException("arg1");
    ProfesionTrabajadorPK localProfesionTrabajadorPK = (ProfesionTrabajadorPK)paramObject;
    localProfesionTrabajadorPK.idProfesionTrabajador = this.idProfesionTrabajador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProfesionTrabajadorPK))
      throw new IllegalArgumentException("arg1");
    ProfesionTrabajadorPK localProfesionTrabajadorPK = (ProfesionTrabajadorPK)paramObject;
    this.idProfesionTrabajador = localProfesionTrabajadorPK.idProfesionTrabajador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProfesionTrabajadorPK))
      throw new IllegalArgumentException("arg2");
    ProfesionTrabajadorPK localProfesionTrabajadorPK = (ProfesionTrabajadorPK)paramObject;
    localProfesionTrabajadorPK.idProfesionTrabajador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProfesionTrabajadorPK))
      throw new IllegalArgumentException("arg2");
    ProfesionTrabajadorPK localProfesionTrabajadorPK = (ProfesionTrabajadorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localProfesionTrabajadorPK.idProfesionTrabajador);
  }

  private static final String jdoGetactualmente(ProfesionTrabajador paramProfesionTrabajador)
  {
    if (paramProfesionTrabajador.jdoFlags <= 0)
      return paramProfesionTrabajador.actualmente;
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionTrabajador.actualmente;
    if (localStateManager.isLoaded(paramProfesionTrabajador, jdoInheritedFieldCount + 0))
      return paramProfesionTrabajador.actualmente;
    return localStateManager.getStringField(paramProfesionTrabajador, jdoInheritedFieldCount + 0, paramProfesionTrabajador.actualmente);
  }

  private static final void jdoSetactualmente(ProfesionTrabajador paramProfesionTrabajador, String paramString)
  {
    if (paramProfesionTrabajador.jdoFlags == 0)
    {
      paramProfesionTrabajador.actualmente = paramString;
      return;
    }
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionTrabajador.actualmente = paramString;
      return;
    }
    localStateManager.setStringField(paramProfesionTrabajador, jdoInheritedFieldCount + 0, paramProfesionTrabajador.actualmente, paramString);
  }

  private static final long jdoGetidProfesionTrabajador(ProfesionTrabajador paramProfesionTrabajador)
  {
    return paramProfesionTrabajador.idProfesionTrabajador;
  }

  private static final void jdoSetidProfesionTrabajador(ProfesionTrabajador paramProfesionTrabajador, long paramLong)
  {
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionTrabajador.idProfesionTrabajador = paramLong;
      return;
    }
    localStateManager.setLongField(paramProfesionTrabajador, jdoInheritedFieldCount + 1, paramProfesionTrabajador.idProfesionTrabajador, paramLong);
  }

  private static final int jdoGetidSitp(ProfesionTrabajador paramProfesionTrabajador)
  {
    if (paramProfesionTrabajador.jdoFlags <= 0)
      return paramProfesionTrabajador.idSitp;
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionTrabajador.idSitp;
    if (localStateManager.isLoaded(paramProfesionTrabajador, jdoInheritedFieldCount + 2))
      return paramProfesionTrabajador.idSitp;
    return localStateManager.getIntField(paramProfesionTrabajador, jdoInheritedFieldCount + 2, paramProfesionTrabajador.idSitp);
  }

  private static final void jdoSetidSitp(ProfesionTrabajador paramProfesionTrabajador, int paramInt)
  {
    if (paramProfesionTrabajador.jdoFlags == 0)
    {
      paramProfesionTrabajador.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionTrabajador.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramProfesionTrabajador, jdoInheritedFieldCount + 2, paramProfesionTrabajador.idSitp, paramInt);
  }

  private static final Personal jdoGetpersonal(ProfesionTrabajador paramProfesionTrabajador)
  {
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionTrabajador.personal;
    if (localStateManager.isLoaded(paramProfesionTrabajador, jdoInheritedFieldCount + 3))
      return paramProfesionTrabajador.personal;
    return (Personal)localStateManager.getObjectField(paramProfesionTrabajador, jdoInheritedFieldCount + 3, paramProfesionTrabajador.personal);
  }

  private static final void jdoSetpersonal(ProfesionTrabajador paramProfesionTrabajador, Personal paramPersonal)
  {
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionTrabajador.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramProfesionTrabajador, jdoInheritedFieldCount + 3, paramProfesionTrabajador.personal, paramPersonal);
  }

  private static final Profesion jdoGetprofesion(ProfesionTrabajador paramProfesionTrabajador)
  {
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionTrabajador.profesion;
    if (localStateManager.isLoaded(paramProfesionTrabajador, jdoInheritedFieldCount + 4))
      return paramProfesionTrabajador.profesion;
    return (Profesion)localStateManager.getObjectField(paramProfesionTrabajador, jdoInheritedFieldCount + 4, paramProfesionTrabajador.profesion);
  }

  private static final void jdoSetprofesion(ProfesionTrabajador paramProfesionTrabajador, Profesion paramProfesion)
  {
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionTrabajador.profesion = paramProfesion;
      return;
    }
    localStateManager.setObjectField(paramProfesionTrabajador, jdoInheritedFieldCount + 4, paramProfesionTrabajador.profesion, paramProfesion);
  }

  private static final Date jdoGettiempoSitp(ProfesionTrabajador paramProfesionTrabajador)
  {
    if (paramProfesionTrabajador.jdoFlags <= 0)
      return paramProfesionTrabajador.tiempoSitp;
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionTrabajador.tiempoSitp;
    if (localStateManager.isLoaded(paramProfesionTrabajador, jdoInheritedFieldCount + 5))
      return paramProfesionTrabajador.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramProfesionTrabajador, jdoInheritedFieldCount + 5, paramProfesionTrabajador.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ProfesionTrabajador paramProfesionTrabajador, Date paramDate)
  {
    if (paramProfesionTrabajador.jdoFlags == 0)
    {
      paramProfesionTrabajador.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramProfesionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionTrabajador.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProfesionTrabajador, jdoInheritedFieldCount + 5, paramProfesionTrabajador.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}