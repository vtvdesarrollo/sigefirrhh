package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ComisionServicioExtPK
  implements Serializable
{
  public long idComisionServicioExt;

  public ComisionServicioExtPK()
  {
  }

  public ComisionServicioExtPK(long idComisionServicioExt)
  {
    this.idComisionServicioExt = idComisionServicioExt;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ComisionServicioExtPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ComisionServicioExtPK thatPK)
  {
    return 
      this.idComisionServicioExt == thatPK.idComisionServicioExt;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idComisionServicioExt)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idComisionServicioExt);
  }
}