package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;

public class HistorialOrganismo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_LOCALIDAD;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_TIPO;
  private long idHistorialOrganismo;
  private int cedula;
  private String apellidosNombres;
  private ClasificacionPersonal clasificacionPersonal;
  private String tipoPersonal;
  private CausaMovimiento causaMovimiento;
  private String codCausaMovimiento;
  private Date fechaMovimiento;
  private Date fechaRegistro;
  private int codManualCargo;
  private String codCargo;
  private String codTabulador;
  private String descripcionCargo;
  private int codigoNomina;
  private String codSede;
  private String nombreSede;
  private String codDependencia;
  private String nombreDependencia;
  private double sueldo;
  private double compensacion;
  private double primasCargo;
  private double primasTrabajador;
  private int grado;
  private int paso;
  private String remesa;
  private int numeroMovimiento;
  private String afectaSueldo;
  private String localidad;
  private String estatus;
  private String aprobacionMpd;
  private String documentoSoporte;
  private Personal personal;
  private Organismo organismo;
  private String codOrganismo;
  private String nombreOrganismo;
  private String codRegion;
  private String nombreRegion;
  private String origenMovimiento;
  private String puntoCuenta;
  private Date fechaPuntoCuenta;
  private String codConcurso;
  private String observaciones;
  private String usuario;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "afectaSueldo", "apellidosNombres", "aprobacionMpd", "causaMovimiento", "cedula", "clasificacionPersonal", "codCargo", "codCausaMovimiento", "codConcurso", "codDependencia", "codManualCargo", "codOrganismo", "codRegion", "codSede", "codTabulador", "codigoNomina", "compensacion", "descripcionCargo", "documentoSoporte", "estatus", "fechaMovimiento", "fechaPuntoCuenta", "fechaRegistro", "grado", "idHistorialOrganismo", "idSitp", "localidad", "nombreDependencia", "nombreOrganismo", "nombreRegion", "nombreSede", "numeroMovimiento", "observaciones", "organismo", "origenMovimiento", "paso", "personal", "primasCargo", "primasTrabajador", "puntoCuenta", "remesa", "sueldo", "tiempoSitp", "tipoPersonal", "usuario" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ClasificacionPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.HistorialOrganismo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistorialOrganismo());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_LOCALIDAD = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_LOCALIDAD.put("C", "NIVEL CENTRAL");
    LISTA_LOCALIDAD.put("R", "NIVEL REGIONAL");
    LISTA_ESTATUS.put("0", "SIN REMESA");
    LISTA_ESTATUS.put("1", "CON REMESA");
    LISTA_ESTATUS.put("2", "ENVIADO MPD");
    LISTA_ESTATUS.put("3", "ANALISIS");
    LISTA_ESTATUS.put("4", "APROBADO");
    LISTA_ESTATUS.put("5", "DEVUELTO");
    LISTA_TIPO.put("0", "ALTO NIVEL");
    LISTA_TIPO.put("1", "ADMINISTRATIVO");
    LISTA_TIPO.put("2", "PROFESIONAL Y TECNICO");
    LISTA_TIPO.put("3", "NO CLASIFICADO");
    LISTA_TIPO.put("4", "OBRERO");
  }

  public HistorialOrganismo()
  {
    jdoSetcedula(this, 0);

    jdoSettipoPersonal(this, "1");

    jdoSetcodigoNomina(this, 0);

    jdoSetsueldo(this, 0.0D);

    jdoSetcompensacion(this, 0.0D);

    jdoSetprimasCargo(this, 0.0D);

    jdoSetprimasTrabajador(this, 0.0D);

    jdoSetgrado(this, 1);

    jdoSetpaso(this, 1);

    jdoSetafectaSueldo(this, "S");

    jdoSetlocalidad(this, "C");

    jdoSetestatus(this, "0");

    jdoSetaprobacionMpd(this, "S");

    jdoSetorigenMovimiento(this, "S");
  }

  public String toString()
  {
    return 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaMovimiento(this)) + " - " + 
      jdoGetcausaMovimiento(this).getDescripcion();
  }

  public String getAfectaSueldo()
  {
    return jdoGetafectaSueldo(this);
  }

  public String getApellidosNombres()
  {
    return jdoGetapellidosNombres(this);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public CausaMovimiento getCausaMovimiento()
  {
    return jdoGetcausaMovimiento(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }

  public ClasificacionPersonal getClasificacionPersonal()
  {
    return jdoGetclasificacionPersonal(this);
  }

  public String getCodCargo()
  {
    return jdoGetcodCargo(this);
  }

  public String getCodCausaMovimiento()
  {
    return jdoGetcodCausaMovimiento(this);
  }

  public String getCodDependencia()
  {
    return jdoGetcodDependencia(this);
  }

  public int getCodigoNomina()
  {
    return jdoGetcodigoNomina(this);
  }

  public int getCodManualCargo()
  {
    return jdoGetcodManualCargo(this);
  }

  public String getCodOrganismo()
  {
    return jdoGetcodOrganismo(this);
  }

  public String getCodRegion()
  {
    return jdoGetcodRegion(this);
  }

  public String getCodSede()
  {
    return jdoGetcodSede(this);
  }

  public String getCodTabulador()
  {
    return jdoGetcodTabulador(this);
  }

  public double getCompensacion()
  {
    return jdoGetcompensacion(this);
  }

  public String getDescripcionCargo()
  {
    return jdoGetdescripcionCargo(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public Date getFechaMovimiento()
  {
    return jdoGetfechaMovimiento(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public int getGrado()
  {
    return jdoGetgrado(this);
  }

  public long getIdHistorialOrganismo()
  {
    return jdoGetidHistorialOrganismo(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public String getLocalidad()
  {
    return jdoGetlocalidad(this);
  }

  public String getNombreDependencia()
  {
    return jdoGetnombreDependencia(this);
  }

  public String getNombreOrganismo()
  {
    return jdoGetnombreOrganismo(this);
  }

  public String getNombreRegion()
  {
    return jdoGetnombreRegion(this);
  }

  public String getNombreSede()
  {
    return jdoGetnombreSede(this);
  }

  public int getNumeroMovimiento()
  {
    return jdoGetnumeroMovimiento(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public int getPaso()
  {
    return jdoGetpaso(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public double getPrimasCargo()
  {
    return jdoGetprimasCargo(this);
  }

  public double getPrimasTrabajador()
  {
    return jdoGetprimasTrabajador(this);
  }

  public String getRemesa()
  {
    return jdoGetremesa(this);
  }

  public double getSueldo()
  {
    return jdoGetsueldo(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public String getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setAfectaSueldo(String string)
  {
    jdoSetafectaSueldo(this, string);
  }

  public void setApellidosNombres(String string)
  {
    jdoSetapellidosNombres(this, string);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setCausaMovimiento(CausaMovimiento movimiento)
  {
    jdoSetcausaMovimiento(this, movimiento);
  }

  public void setCedula(int i)
  {
    jdoSetcedula(this, i);
  }

  public void setClasificacionPersonal(ClasificacionPersonal personal)
  {
    jdoSetclasificacionPersonal(this, personal);
  }

  public void setCodCargo(String string)
  {
    jdoSetcodCargo(this, string);
  }

  public void setCodCausaMovimiento(String string)
  {
    jdoSetcodCausaMovimiento(this, string);
  }

  public void setCodDependencia(String string)
  {
    jdoSetcodDependencia(this, string);
  }

  public void setCodigoNomina(int i)
  {
    jdoSetcodigoNomina(this, i);
  }

  public void setCodManualCargo(int i)
  {
    jdoSetcodManualCargo(this, i);
  }

  public void setCodOrganismo(String string)
  {
    jdoSetcodOrganismo(this, string);
  }

  public void setCodRegion(String string)
  {
    jdoSetcodRegion(this, string);
  }

  public void setCodSede(String string)
  {
    jdoSetcodSede(this, string);
  }

  public void setCodTabulador(String string)
  {
    jdoSetcodTabulador(this, string);
  }

  public void setCompensacion(double d)
  {
    jdoSetcompensacion(this, d);
  }

  public void setDescripcionCargo(String string)
  {
    jdoSetdescripcionCargo(this, string);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFechaMovimiento(Date date)
  {
    jdoSetfechaMovimiento(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setGrado(int i)
  {
    jdoSetgrado(this, i);
  }

  public void setIdHistorialOrganismo(long l)
  {
    jdoSetidHistorialOrganismo(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setLocalidad(String string)
  {
    jdoSetlocalidad(this, string);
  }

  public void setNombreDependencia(String string)
  {
    jdoSetnombreDependencia(this, string);
  }

  public void setNombreOrganismo(String string)
  {
    jdoSetnombreOrganismo(this, string);
  }

  public void setNombreRegion(String string)
  {
    jdoSetnombreRegion(this, string);
  }

  public void setNombreSede(String string)
  {
    jdoSetnombreSede(this, string);
  }

  public void setNumeroMovimiento(int i)
  {
    jdoSetnumeroMovimiento(this, i);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setPaso(int i)
  {
    jdoSetpaso(this, i);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPrimasCargo(double d)
  {
    jdoSetprimasCargo(this, d);
  }

  public void setPrimasTrabajador(double d)
  {
    jdoSetprimasTrabajador(this, d);
  }

  public void setRemesa(String string)
  {
    jdoSetremesa(this, string);
  }

  public void setSueldo(double d)
  {
    jdoSetsueldo(this, d);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setTipoPersonal(String string)
  {
    jdoSettipoPersonal(this, string);
  }

  public void setUsuario(String string)
  {
    jdoSetusuario(this, string);
  }

  public String getOrigenMovimiento()
  {
    return jdoGetorigenMovimiento(this);
  }

  public void setOrigenMovimiento(String string)
  {
    jdoSetorigenMovimiento(this, string);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public Date getFechaPuntoCuenta()
  {
    return jdoGetfechaPuntoCuenta(this);
  }

  public String getPuntoCuenta()
  {
    return jdoGetpuntoCuenta(this);
  }

  public void setFechaPuntoCuenta(Date date)
  {
    jdoSetfechaPuntoCuenta(this, date);
  }

  public void setPuntoCuenta(String string)
  {
    jdoSetpuntoCuenta(this, string);
  }

  public String getCodConcurso()
  {
    return jdoGetcodConcurso(this);
  }

  public void setCodConcurso(String string)
  {
    jdoSetcodConcurso(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 45;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistorialOrganismo localHistorialOrganismo = new HistorialOrganismo();
    localHistorialOrganismo.jdoFlags = 1;
    localHistorialOrganismo.jdoStateManager = paramStateManager;
    return localHistorialOrganismo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistorialOrganismo localHistorialOrganismo = new HistorialOrganismo();
    localHistorialOrganismo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistorialOrganismo.jdoFlags = 1;
    localHistorialOrganismo.jdoStateManager = paramStateManager;
    return localHistorialOrganismo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.afectaSueldo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.apellidosNombres);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacionPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaMovimiento);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcurso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codManualCargo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegion);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSede);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTabulador);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacion);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionCargo);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaMovimiento);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPuntoCuenta);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistorialOrganismo);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.localidad);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDependencia);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreOrganismo);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreRegion);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSede);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMovimiento);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origenMovimiento);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasCargo);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasTrabajador);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.puntoCuenta);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.remesa);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldo);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoPersonal);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.afectaSueldo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.apellidosNombres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacionPersonal = ((ClasificacionPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codManualCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTabulador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaMovimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPuntoCuenta = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistorialOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.localidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMovimiento = localStateManager.replacingIntField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origenMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.puntoCuenta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remesa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.afectaSueldo = paramHistorialOrganismo.afectaSueldo;
      return;
    case 1:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.apellidosNombres = paramHistorialOrganismo.apellidosNombres;
      return;
    case 2:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramHistorialOrganismo.aprobacionMpd;
      return;
    case 3:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramHistorialOrganismo.causaMovimiento;
      return;
    case 4:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramHistorialOrganismo.cedula;
      return;
    case 5:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacionPersonal = paramHistorialOrganismo.clasificacionPersonal;
      return;
    case 6:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramHistorialOrganismo.codCargo;
      return;
    case 7:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaMovimiento = paramHistorialOrganismo.codCausaMovimiento;
      return;
    case 8:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codConcurso = paramHistorialOrganismo.codConcurso;
      return;
    case 9:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramHistorialOrganismo.codDependencia;
      return;
    case 10:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codManualCargo = paramHistorialOrganismo.codManualCargo;
      return;
    case 11:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismo = paramHistorialOrganismo.codOrganismo;
      return;
    case 12:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codRegion = paramHistorialOrganismo.codRegion;
      return;
    case 13:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codSede = paramHistorialOrganismo.codSede;
      return;
    case 14:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codTabulador = paramHistorialOrganismo.codTabulador;
      return;
    case 15:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramHistorialOrganismo.codigoNomina;
      return;
    case 16:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramHistorialOrganismo.compensacion;
      return;
    case 17:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionCargo = paramHistorialOrganismo.descripcionCargo;
      return;
    case 18:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramHistorialOrganismo.documentoSoporte;
      return;
    case 19:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramHistorialOrganismo.estatus;
      return;
    case 20:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaMovimiento = paramHistorialOrganismo.fechaMovimiento;
      return;
    case 21:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPuntoCuenta = paramHistorialOrganismo.fechaPuntoCuenta;
      return;
    case 22:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramHistorialOrganismo.fechaRegistro;
      return;
    case 23:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramHistorialOrganismo.grado;
      return;
    case 24:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idHistorialOrganismo = paramHistorialOrganismo.idHistorialOrganismo;
      return;
    case 25:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramHistorialOrganismo.idSitp;
      return;
    case 26:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.localidad = paramHistorialOrganismo.localidad;
      return;
    case 27:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDependencia = paramHistorialOrganismo.nombreDependencia;
      return;
    case 28:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreOrganismo = paramHistorialOrganismo.nombreOrganismo;
      return;
    case 29:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreRegion = paramHistorialOrganismo.nombreRegion;
      return;
    case 30:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSede = paramHistorialOrganismo.nombreSede;
      return;
    case 31:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMovimiento = paramHistorialOrganismo.numeroMovimiento;
      return;
    case 32:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramHistorialOrganismo.observaciones;
      return;
    case 33:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramHistorialOrganismo.organismo;
      return;
    case 34:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.origenMovimiento = paramHistorialOrganismo.origenMovimiento;
      return;
    case 35:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramHistorialOrganismo.paso;
      return;
    case 36:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramHistorialOrganismo.personal;
      return;
    case 37:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.primasCargo = paramHistorialOrganismo.primasCargo;
      return;
    case 38:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.primasTrabajador = paramHistorialOrganismo.primasTrabajador;
      return;
    case 39:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.puntoCuenta = paramHistorialOrganismo.puntoCuenta;
      return;
    case 40:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.remesa = paramHistorialOrganismo.remesa;
      return;
    case 41:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.sueldo = paramHistorialOrganismo.sueldo;
      return;
    case 42:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramHistorialOrganismo.tiempoSitp;
      return;
    case 43:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramHistorialOrganismo.tipoPersonal;
      return;
    case 44:
      if (paramHistorialOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramHistorialOrganismo.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistorialOrganismo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistorialOrganismo localHistorialOrganismo = (HistorialOrganismo)paramObject;
    if (localHistorialOrganismo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistorialOrganismo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistorialOrganismoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistorialOrganismoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistorialOrganismoPK))
      throw new IllegalArgumentException("arg1");
    HistorialOrganismoPK localHistorialOrganismoPK = (HistorialOrganismoPK)paramObject;
    localHistorialOrganismoPK.idHistorialOrganismo = this.idHistorialOrganismo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistorialOrganismoPK))
      throw new IllegalArgumentException("arg1");
    HistorialOrganismoPK localHistorialOrganismoPK = (HistorialOrganismoPK)paramObject;
    this.idHistorialOrganismo = localHistorialOrganismoPK.idHistorialOrganismo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistorialOrganismoPK))
      throw new IllegalArgumentException("arg2");
    HistorialOrganismoPK localHistorialOrganismoPK = (HistorialOrganismoPK)paramObject;
    localHistorialOrganismoPK.idHistorialOrganismo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 24);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistorialOrganismoPK))
      throw new IllegalArgumentException("arg2");
    HistorialOrganismoPK localHistorialOrganismoPK = (HistorialOrganismoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 24, localHistorialOrganismoPK.idHistorialOrganismo);
  }

  private static final String jdoGetafectaSueldo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.afectaSueldo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.afectaSueldo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 0))
      return paramHistorialOrganismo.afectaSueldo;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 0, paramHistorialOrganismo.afectaSueldo);
  }

  private static final void jdoSetafectaSueldo(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.afectaSueldo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.afectaSueldo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 0, paramHistorialOrganismo.afectaSueldo, paramString);
  }

  private static final String jdoGetapellidosNombres(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.apellidosNombres;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.apellidosNombres;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 1))
      return paramHistorialOrganismo.apellidosNombres;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 1, paramHistorialOrganismo.apellidosNombres);
  }

  private static final void jdoSetapellidosNombres(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.apellidosNombres = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.apellidosNombres = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 1, paramHistorialOrganismo.apellidosNombres, paramString);
  }

  private static final String jdoGetaprobacionMpd(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.aprobacionMpd;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.aprobacionMpd;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 2))
      return paramHistorialOrganismo.aprobacionMpd;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 2, paramHistorialOrganismo.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 2, paramHistorialOrganismo.aprobacionMpd, paramString);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(HistorialOrganismo paramHistorialOrganismo)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.causaMovimiento;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 3))
      return paramHistorialOrganismo.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 3, paramHistorialOrganismo.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(HistorialOrganismo paramHistorialOrganismo, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 3, paramHistorialOrganismo.causaMovimiento, paramCausaMovimiento);
  }

  private static final int jdoGetcedula(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.cedula;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.cedula;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 4))
      return paramHistorialOrganismo.cedula;
    return localStateManager.getIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 4, paramHistorialOrganismo.cedula);
  }

  private static final void jdoSetcedula(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 4, paramHistorialOrganismo.cedula, paramInt);
  }

  private static final ClasificacionPersonal jdoGetclasificacionPersonal(HistorialOrganismo paramHistorialOrganismo)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.clasificacionPersonal;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 5))
      return paramHistorialOrganismo.clasificacionPersonal;
    return (ClasificacionPersonal)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 5, paramHistorialOrganismo.clasificacionPersonal);
  }

  private static final void jdoSetclasificacionPersonal(HistorialOrganismo paramHistorialOrganismo, ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.clasificacionPersonal = paramClasificacionPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 5, paramHistorialOrganismo.clasificacionPersonal, paramClasificacionPersonal);
  }

  private static final String jdoGetcodCargo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codCargo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codCargo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 6))
      return paramHistorialOrganismo.codCargo;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 6, paramHistorialOrganismo.codCargo);
  }

  private static final void jdoSetcodCargo(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 6, paramHistorialOrganismo.codCargo, paramString);
  }

  private static final String jdoGetcodCausaMovimiento(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codCausaMovimiento;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codCausaMovimiento;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 7))
      return paramHistorialOrganismo.codCausaMovimiento;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 7, paramHistorialOrganismo.codCausaMovimiento);
  }

  private static final void jdoSetcodCausaMovimiento(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codCausaMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codCausaMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 7, paramHistorialOrganismo.codCausaMovimiento, paramString);
  }

  private static final String jdoGetcodConcurso(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codConcurso;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codConcurso;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 8))
      return paramHistorialOrganismo.codConcurso;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 8, paramHistorialOrganismo.codConcurso);
  }

  private static final void jdoSetcodConcurso(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codConcurso = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codConcurso = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 8, paramHistorialOrganismo.codConcurso, paramString);
  }

  private static final String jdoGetcodDependencia(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codDependencia;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codDependencia;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 9))
      return paramHistorialOrganismo.codDependencia;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 9, paramHistorialOrganismo.codDependencia);
  }

  private static final void jdoSetcodDependencia(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 9, paramHistorialOrganismo.codDependencia, paramString);
  }

  private static final int jdoGetcodManualCargo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codManualCargo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codManualCargo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 10))
      return paramHistorialOrganismo.codManualCargo;
    return localStateManager.getIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 10, paramHistorialOrganismo.codManualCargo);
  }

  private static final void jdoSetcodManualCargo(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codManualCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codManualCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 10, paramHistorialOrganismo.codManualCargo, paramInt);
  }

  private static final String jdoGetcodOrganismo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codOrganismo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codOrganismo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 11))
      return paramHistorialOrganismo.codOrganismo;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 11, paramHistorialOrganismo.codOrganismo);
  }

  private static final void jdoSetcodOrganismo(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 11, paramHistorialOrganismo.codOrganismo, paramString);
  }

  private static final String jdoGetcodRegion(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codRegion;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codRegion;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 12))
      return paramHistorialOrganismo.codRegion;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 12, paramHistorialOrganismo.codRegion);
  }

  private static final void jdoSetcodRegion(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codRegion = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 12, paramHistorialOrganismo.codRegion, paramString);
  }

  private static final String jdoGetcodSede(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codSede;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codSede;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 13))
      return paramHistorialOrganismo.codSede;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 13, paramHistorialOrganismo.codSede);
  }

  private static final void jdoSetcodSede(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codSede = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codSede = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 13, paramHistorialOrganismo.codSede, paramString);
  }

  private static final String jdoGetcodTabulador(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codTabulador;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codTabulador;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 14))
      return paramHistorialOrganismo.codTabulador;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 14, paramHistorialOrganismo.codTabulador);
  }

  private static final void jdoSetcodTabulador(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codTabulador = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codTabulador = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 14, paramHistorialOrganismo.codTabulador, paramString);
  }

  private static final int jdoGetcodigoNomina(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.codigoNomina;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.codigoNomina;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 15))
      return paramHistorialOrganismo.codigoNomina;
    return localStateManager.getIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 15, paramHistorialOrganismo.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 15, paramHistorialOrganismo.codigoNomina, paramInt);
  }

  private static final double jdoGetcompensacion(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.compensacion;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.compensacion;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 16))
      return paramHistorialOrganismo.compensacion;
    return localStateManager.getDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 16, paramHistorialOrganismo.compensacion);
  }

  private static final void jdoSetcompensacion(HistorialOrganismo paramHistorialOrganismo, double paramDouble)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.compensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.compensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 16, paramHistorialOrganismo.compensacion, paramDouble);
  }

  private static final String jdoGetdescripcionCargo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.descripcionCargo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.descripcionCargo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 17))
      return paramHistorialOrganismo.descripcionCargo;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 17, paramHistorialOrganismo.descripcionCargo);
  }

  private static final void jdoSetdescripcionCargo(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.descripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.descripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 17, paramHistorialOrganismo.descripcionCargo, paramString);
  }

  private static final String jdoGetdocumentoSoporte(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.documentoSoporte;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.documentoSoporte;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 18))
      return paramHistorialOrganismo.documentoSoporte;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 18, paramHistorialOrganismo.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 18, paramHistorialOrganismo.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.estatus;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.estatus;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 19))
      return paramHistorialOrganismo.estatus;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 19, paramHistorialOrganismo.estatus);
  }

  private static final void jdoSetestatus(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 19, paramHistorialOrganismo.estatus, paramString);
  }

  private static final Date jdoGetfechaMovimiento(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.fechaMovimiento;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.fechaMovimiento;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 20))
      return paramHistorialOrganismo.fechaMovimiento;
    return (Date)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 20, paramHistorialOrganismo.fechaMovimiento);
  }

  private static final void jdoSetfechaMovimiento(HistorialOrganismo paramHistorialOrganismo, Date paramDate)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.fechaMovimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.fechaMovimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 20, paramHistorialOrganismo.fechaMovimiento, paramDate);
  }

  private static final Date jdoGetfechaPuntoCuenta(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.fechaPuntoCuenta;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.fechaPuntoCuenta;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 21))
      return paramHistorialOrganismo.fechaPuntoCuenta;
    return (Date)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 21, paramHistorialOrganismo.fechaPuntoCuenta);
  }

  private static final void jdoSetfechaPuntoCuenta(HistorialOrganismo paramHistorialOrganismo, Date paramDate)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.fechaPuntoCuenta = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.fechaPuntoCuenta = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 21, paramHistorialOrganismo.fechaPuntoCuenta, paramDate);
  }

  private static final Date jdoGetfechaRegistro(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.fechaRegistro;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.fechaRegistro;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 22))
      return paramHistorialOrganismo.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 22, paramHistorialOrganismo.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(HistorialOrganismo paramHistorialOrganismo, Date paramDate)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 22, paramHistorialOrganismo.fechaRegistro, paramDate);
  }

  private static final int jdoGetgrado(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.grado;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.grado;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 23))
      return paramHistorialOrganismo.grado;
    return localStateManager.getIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 23, paramHistorialOrganismo.grado);
  }

  private static final void jdoSetgrado(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 23, paramHistorialOrganismo.grado, paramInt);
  }

  private static final long jdoGetidHistorialOrganismo(HistorialOrganismo paramHistorialOrganismo)
  {
    return paramHistorialOrganismo.idHistorialOrganismo;
  }

  private static final void jdoSetidHistorialOrganismo(HistorialOrganismo paramHistorialOrganismo, long paramLong)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.idHistorialOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistorialOrganismo, jdoInheritedFieldCount + 24, paramHistorialOrganismo.idHistorialOrganismo, paramLong);
  }

  private static final int jdoGetidSitp(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.idSitp;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.idSitp;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 25))
      return paramHistorialOrganismo.idSitp;
    return localStateManager.getIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 25, paramHistorialOrganismo.idSitp);
  }

  private static final void jdoSetidSitp(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 25, paramHistorialOrganismo.idSitp, paramInt);
  }

  private static final String jdoGetlocalidad(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.localidad;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.localidad;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 26))
      return paramHistorialOrganismo.localidad;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 26, paramHistorialOrganismo.localidad);
  }

  private static final void jdoSetlocalidad(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.localidad = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.localidad = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 26, paramHistorialOrganismo.localidad, paramString);
  }

  private static final String jdoGetnombreDependencia(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.nombreDependencia;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.nombreDependencia;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 27))
      return paramHistorialOrganismo.nombreDependencia;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 27, paramHistorialOrganismo.nombreDependencia);
  }

  private static final void jdoSetnombreDependencia(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.nombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.nombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 27, paramHistorialOrganismo.nombreDependencia, paramString);
  }

  private static final String jdoGetnombreOrganismo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.nombreOrganismo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.nombreOrganismo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 28))
      return paramHistorialOrganismo.nombreOrganismo;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 28, paramHistorialOrganismo.nombreOrganismo);
  }

  private static final void jdoSetnombreOrganismo(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.nombreOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.nombreOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 28, paramHistorialOrganismo.nombreOrganismo, paramString);
  }

  private static final String jdoGetnombreRegion(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.nombreRegion;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.nombreRegion;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 29))
      return paramHistorialOrganismo.nombreRegion;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 29, paramHistorialOrganismo.nombreRegion);
  }

  private static final void jdoSetnombreRegion(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.nombreRegion = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.nombreRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 29, paramHistorialOrganismo.nombreRegion, paramString);
  }

  private static final String jdoGetnombreSede(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.nombreSede;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.nombreSede;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 30))
      return paramHistorialOrganismo.nombreSede;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 30, paramHistorialOrganismo.nombreSede);
  }

  private static final void jdoSetnombreSede(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.nombreSede = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.nombreSede = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 30, paramHistorialOrganismo.nombreSede, paramString);
  }

  private static final int jdoGetnumeroMovimiento(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.numeroMovimiento;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.numeroMovimiento;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 31))
      return paramHistorialOrganismo.numeroMovimiento;
    return localStateManager.getIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 31, paramHistorialOrganismo.numeroMovimiento);
  }

  private static final void jdoSetnumeroMovimiento(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.numeroMovimiento = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.numeroMovimiento = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 31, paramHistorialOrganismo.numeroMovimiento, paramInt);
  }

  private static final String jdoGetobservaciones(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.observaciones;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.observaciones;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 32))
      return paramHistorialOrganismo.observaciones;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 32, paramHistorialOrganismo.observaciones);
  }

  private static final void jdoSetobservaciones(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 32, paramHistorialOrganismo.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(HistorialOrganismo paramHistorialOrganismo)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.organismo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 33))
      return paramHistorialOrganismo.organismo;
    return (Organismo)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 33, paramHistorialOrganismo.organismo);
  }

  private static final void jdoSetorganismo(HistorialOrganismo paramHistorialOrganismo, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 33, paramHistorialOrganismo.organismo, paramOrganismo);
  }

  private static final String jdoGetorigenMovimiento(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.origenMovimiento;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.origenMovimiento;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 34))
      return paramHistorialOrganismo.origenMovimiento;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 34, paramHistorialOrganismo.origenMovimiento);
  }

  private static final void jdoSetorigenMovimiento(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.origenMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.origenMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 34, paramHistorialOrganismo.origenMovimiento, paramString);
  }

  private static final int jdoGetpaso(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.paso;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.paso;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 35))
      return paramHistorialOrganismo.paso;
    return localStateManager.getIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 35, paramHistorialOrganismo.paso);
  }

  private static final void jdoSetpaso(HistorialOrganismo paramHistorialOrganismo, int paramInt)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialOrganismo, jdoInheritedFieldCount + 35, paramHistorialOrganismo.paso, paramInt);
  }

  private static final Personal jdoGetpersonal(HistorialOrganismo paramHistorialOrganismo)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.personal;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 36))
      return paramHistorialOrganismo.personal;
    return (Personal)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 36, paramHistorialOrganismo.personal);
  }

  private static final void jdoSetpersonal(HistorialOrganismo paramHistorialOrganismo, Personal paramPersonal)
  {
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 36, paramHistorialOrganismo.personal, paramPersonal);
  }

  private static final double jdoGetprimasCargo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.primasCargo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.primasCargo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 37))
      return paramHistorialOrganismo.primasCargo;
    return localStateManager.getDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 37, paramHistorialOrganismo.primasCargo);
  }

  private static final void jdoSetprimasCargo(HistorialOrganismo paramHistorialOrganismo, double paramDouble)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.primasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.primasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 37, paramHistorialOrganismo.primasCargo, paramDouble);
  }

  private static final double jdoGetprimasTrabajador(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.primasTrabajador;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.primasTrabajador;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 38))
      return paramHistorialOrganismo.primasTrabajador;
    return localStateManager.getDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 38, paramHistorialOrganismo.primasTrabajador);
  }

  private static final void jdoSetprimasTrabajador(HistorialOrganismo paramHistorialOrganismo, double paramDouble)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.primasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.primasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 38, paramHistorialOrganismo.primasTrabajador, paramDouble);
  }

  private static final String jdoGetpuntoCuenta(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.puntoCuenta;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.puntoCuenta;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 39))
      return paramHistorialOrganismo.puntoCuenta;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 39, paramHistorialOrganismo.puntoCuenta);
  }

  private static final void jdoSetpuntoCuenta(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.puntoCuenta = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.puntoCuenta = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 39, paramHistorialOrganismo.puntoCuenta, paramString);
  }

  private static final String jdoGetremesa(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.remesa;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.remesa;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 40))
      return paramHistorialOrganismo.remesa;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 40, paramHistorialOrganismo.remesa);
  }

  private static final void jdoSetremesa(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.remesa = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.remesa = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 40, paramHistorialOrganismo.remesa, paramString);
  }

  private static final double jdoGetsueldo(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.sueldo;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.sueldo;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 41))
      return paramHistorialOrganismo.sueldo;
    return localStateManager.getDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 41, paramHistorialOrganismo.sueldo);
  }

  private static final void jdoSetsueldo(HistorialOrganismo paramHistorialOrganismo, double paramDouble)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.sueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.sueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialOrganismo, jdoInheritedFieldCount + 41, paramHistorialOrganismo.sueldo, paramDouble);
  }

  private static final Date jdoGettiempoSitp(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.tiempoSitp;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.tiempoSitp;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 42))
      return paramHistorialOrganismo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 42, paramHistorialOrganismo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(HistorialOrganismo paramHistorialOrganismo, Date paramDate)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialOrganismo, jdoInheritedFieldCount + 42, paramHistorialOrganismo.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoPersonal(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.tipoPersonal;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.tipoPersonal;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 43))
      return paramHistorialOrganismo.tipoPersonal;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 43, paramHistorialOrganismo.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.tipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.tipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 43, paramHistorialOrganismo.tipoPersonal, paramString);
  }

  private static final String jdoGetusuario(HistorialOrganismo paramHistorialOrganismo)
  {
    if (paramHistorialOrganismo.jdoFlags <= 0)
      return paramHistorialOrganismo.usuario;
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialOrganismo.usuario;
    if (localStateManager.isLoaded(paramHistorialOrganismo, jdoInheritedFieldCount + 44))
      return paramHistorialOrganismo.usuario;
    return localStateManager.getStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 44, paramHistorialOrganismo.usuario);
  }

  private static final void jdoSetusuario(HistorialOrganismo paramHistorialOrganismo, String paramString)
  {
    if (paramHistorialOrganismo.jdoFlags == 0)
    {
      paramHistorialOrganismo.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialOrganismo.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialOrganismo, jdoInheritedFieldCount + 44, paramHistorialOrganismo.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}