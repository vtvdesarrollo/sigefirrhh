package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AveriguacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAveriguacion(Averiguacion averiguacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Averiguacion averiguacionNew = 
      (Averiguacion)BeanUtils.cloneBean(
      averiguacion);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (averiguacionNew.getPersonal() != null) {
      averiguacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        averiguacionNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(averiguacionNew);
  }

  public void updateAveriguacion(Averiguacion averiguacion) throws Exception
  {
    Averiguacion averiguacionModify = 
      findAveriguacionById(averiguacion.getIdAveriguacion());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (averiguacion.getPersonal() != null) {
      averiguacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        averiguacion.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(averiguacionModify, averiguacion);
  }

  public void deleteAveriguacion(Averiguacion averiguacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Averiguacion averiguacionDelete = 
      findAveriguacionById(averiguacion.getIdAveriguacion());
    pm.deletePersistent(averiguacionDelete);
  }

  public Averiguacion findAveriguacionById(long idAveriguacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAveriguacion == pIdAveriguacion";
    Query query = pm.newQuery(Averiguacion.class, filter);

    query.declareParameters("long pIdAveriguacion");

    parameters.put("pIdAveriguacion", new Long(idAveriguacion));

    Collection colAveriguacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAveriguacion.iterator();
    return (Averiguacion)iterator.next();
  }

  public Collection findAveriguacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent averiguacionExtent = pm.getExtent(
      Averiguacion.class, true);
    Query query = pm.newQuery(averiguacionExtent);
    query.setOrdering("fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Averiguacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaInicio ascending");

    Collection colAveriguacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAveriguacion);

    return colAveriguacion;
  }
}