package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.adiestramiento.AreaConocimiento;

public class Certificacion
  implements Serializable, PersistenceCapable
{
  private long idCertificacion;
  private AreaConocimiento areaConocimiento;
  private String nombreCertificacion;
  private String nombreEntidad;
  private Date fechaCertificacion;
  private String vigencia;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "areaConocimiento", "fechaCertificacion", "idCertificacion", "idSitp", "nombreCertificacion", "nombreEntidad", "personal", "tiempoSitp", "vigencia" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.adiestramiento.AreaConocimiento"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 21, 21, 21, 26, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaCertificacion(this)) + " - " + 
      jdoGetnombreCertificacion(this);
  }

  public Date getFechaCertificacion()
  {
    return jdoGetfechaCertificacion(this);
  }

  public long getIdCertificacion() {
    return jdoGetidCertificacion(this);
  }

  public String getNombreCertificacion() {
    return jdoGetnombreCertificacion(this);
  }

  public String getNombreEntidad() {
    return jdoGetnombreEntidad(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public String getVigencia() {
    return jdoGetvigencia(this);
  }

  public void setFechaCertificacion(Date date)
  {
    jdoSetfechaCertificacion(this, date);
  }

  public void setIdCertificacion(long l) {
    jdoSetidCertificacion(this, l);
  }

  public void setNombreCertificacion(String string) {
    jdoSetnombreCertificacion(this, string);
  }

  public void setNombreEntidad(String string) {
    jdoSetnombreEntidad(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public void setVigencia(String string) {
    jdoSetvigencia(this, string);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public AreaConocimiento getAreaConocimiento()
  {
    return jdoGetareaConocimiento(this);
  }

  public void setAreaConocimiento(AreaConocimiento conocimiento)
  {
    jdoSetareaConocimiento(this, conocimiento);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Certificacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Certificacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Certificacion localCertificacion = new Certificacion();
    localCertificacion.jdoFlags = 1;
    localCertificacion.jdoStateManager = paramStateManager;
    return localCertificacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Certificacion localCertificacion = new Certificacion();
    localCertificacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCertificacion.jdoFlags = 1;
    localCertificacion.jdoStateManager = paramStateManager;
    return localCertificacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaConocimiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCertificacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCertificacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCertificacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaConocimiento = ((AreaConocimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCertificacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCertificacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCertificacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Certificacion paramCertificacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.areaConocimiento = paramCertificacion.areaConocimiento;
      return;
    case 1:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCertificacion = paramCertificacion.fechaCertificacion;
      return;
    case 2:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.idCertificacion = paramCertificacion.idCertificacion;
      return;
    case 3:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramCertificacion.idSitp;
      return;
    case 4:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCertificacion = paramCertificacion.nombreCertificacion;
      return;
    case 5:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramCertificacion.nombreEntidad;
      return;
    case 6:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramCertificacion.personal;
      return;
    case 7:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramCertificacion.tiempoSitp;
      return;
    case 8:
      if (paramCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramCertificacion.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Certificacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Certificacion localCertificacion = (Certificacion)paramObject;
    if (localCertificacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCertificacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CertificacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CertificacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CertificacionPK))
      throw new IllegalArgumentException("arg1");
    CertificacionPK localCertificacionPK = (CertificacionPK)paramObject;
    localCertificacionPK.idCertificacion = this.idCertificacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CertificacionPK))
      throw new IllegalArgumentException("arg1");
    CertificacionPK localCertificacionPK = (CertificacionPK)paramObject;
    this.idCertificacion = localCertificacionPK.idCertificacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CertificacionPK))
      throw new IllegalArgumentException("arg2");
    CertificacionPK localCertificacionPK = (CertificacionPK)paramObject;
    localCertificacionPK.idCertificacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CertificacionPK))
      throw new IllegalArgumentException("arg2");
    CertificacionPK localCertificacionPK = (CertificacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCertificacionPK.idCertificacion);
  }

  private static final AreaConocimiento jdoGetareaConocimiento(Certificacion paramCertificacion)
  {
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.areaConocimiento;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 0))
      return paramCertificacion.areaConocimiento;
    return (AreaConocimiento)localStateManager.getObjectField(paramCertificacion, jdoInheritedFieldCount + 0, paramCertificacion.areaConocimiento);
  }

  private static final void jdoSetareaConocimiento(Certificacion paramCertificacion, AreaConocimiento paramAreaConocimiento)
  {
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.areaConocimiento = paramAreaConocimiento;
      return;
    }
    localStateManager.setObjectField(paramCertificacion, jdoInheritedFieldCount + 0, paramCertificacion.areaConocimiento, paramAreaConocimiento);
  }

  private static final Date jdoGetfechaCertificacion(Certificacion paramCertificacion)
  {
    if (paramCertificacion.jdoFlags <= 0)
      return paramCertificacion.fechaCertificacion;
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.fechaCertificacion;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 1))
      return paramCertificacion.fechaCertificacion;
    return (Date)localStateManager.getObjectField(paramCertificacion, jdoInheritedFieldCount + 1, paramCertificacion.fechaCertificacion);
  }

  private static final void jdoSetfechaCertificacion(Certificacion paramCertificacion, Date paramDate)
  {
    if (paramCertificacion.jdoFlags == 0)
    {
      paramCertificacion.fechaCertificacion = paramDate;
      return;
    }
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.fechaCertificacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCertificacion, jdoInheritedFieldCount + 1, paramCertificacion.fechaCertificacion, paramDate);
  }

  private static final long jdoGetidCertificacion(Certificacion paramCertificacion)
  {
    return paramCertificacion.idCertificacion;
  }

  private static final void jdoSetidCertificacion(Certificacion paramCertificacion, long paramLong)
  {
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.idCertificacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramCertificacion, jdoInheritedFieldCount + 2, paramCertificacion.idCertificacion, paramLong);
  }

  private static final int jdoGetidSitp(Certificacion paramCertificacion)
  {
    if (paramCertificacion.jdoFlags <= 0)
      return paramCertificacion.idSitp;
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.idSitp;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 3))
      return paramCertificacion.idSitp;
    return localStateManager.getIntField(paramCertificacion, jdoInheritedFieldCount + 3, paramCertificacion.idSitp);
  }

  private static final void jdoSetidSitp(Certificacion paramCertificacion, int paramInt)
  {
    if (paramCertificacion.jdoFlags == 0)
    {
      paramCertificacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramCertificacion, jdoInheritedFieldCount + 3, paramCertificacion.idSitp, paramInt);
  }

  private static final String jdoGetnombreCertificacion(Certificacion paramCertificacion)
  {
    if (paramCertificacion.jdoFlags <= 0)
      return paramCertificacion.nombreCertificacion;
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.nombreCertificacion;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 4))
      return paramCertificacion.nombreCertificacion;
    return localStateManager.getStringField(paramCertificacion, jdoInheritedFieldCount + 4, paramCertificacion.nombreCertificacion);
  }

  private static final void jdoSetnombreCertificacion(Certificacion paramCertificacion, String paramString)
  {
    if (paramCertificacion.jdoFlags == 0)
    {
      paramCertificacion.nombreCertificacion = paramString;
      return;
    }
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.nombreCertificacion = paramString;
      return;
    }
    localStateManager.setStringField(paramCertificacion, jdoInheritedFieldCount + 4, paramCertificacion.nombreCertificacion, paramString);
  }

  private static final String jdoGetnombreEntidad(Certificacion paramCertificacion)
  {
    if (paramCertificacion.jdoFlags <= 0)
      return paramCertificacion.nombreEntidad;
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.nombreEntidad;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 5))
      return paramCertificacion.nombreEntidad;
    return localStateManager.getStringField(paramCertificacion, jdoInheritedFieldCount + 5, paramCertificacion.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(Certificacion paramCertificacion, String paramString)
  {
    if (paramCertificacion.jdoFlags == 0)
    {
      paramCertificacion.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramCertificacion, jdoInheritedFieldCount + 5, paramCertificacion.nombreEntidad, paramString);
  }

  private static final Personal jdoGetpersonal(Certificacion paramCertificacion)
  {
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.personal;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 6))
      return paramCertificacion.personal;
    return (Personal)localStateManager.getObjectField(paramCertificacion, jdoInheritedFieldCount + 6, paramCertificacion.personal);
  }

  private static final void jdoSetpersonal(Certificacion paramCertificacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramCertificacion, jdoInheritedFieldCount + 6, paramCertificacion.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Certificacion paramCertificacion)
  {
    if (paramCertificacion.jdoFlags <= 0)
      return paramCertificacion.tiempoSitp;
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.tiempoSitp;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 7))
      return paramCertificacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramCertificacion, jdoInheritedFieldCount + 7, paramCertificacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Certificacion paramCertificacion, Date paramDate)
  {
    if (paramCertificacion.jdoFlags == 0)
    {
      paramCertificacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCertificacion, jdoInheritedFieldCount + 7, paramCertificacion.tiempoSitp, paramDate);
  }

  private static final String jdoGetvigencia(Certificacion paramCertificacion)
  {
    if (paramCertificacion.jdoFlags <= 0)
      return paramCertificacion.vigencia;
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramCertificacion.vigencia;
    if (localStateManager.isLoaded(paramCertificacion, jdoInheritedFieldCount + 8))
      return paramCertificacion.vigencia;
    return localStateManager.getStringField(paramCertificacion, jdoInheritedFieldCount + 8, paramCertificacion.vigencia);
  }

  private static final void jdoSetvigencia(Certificacion paramCertificacion, String paramString)
  {
    if (paramCertificacion.jdoFlags == 0)
    {
      paramCertificacion.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramCertificacion.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramCertificacion, jdoInheritedFieldCount + 8, paramCertificacion.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}