package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ExperienciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addExperiencia(Experiencia experiencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Experiencia experienciaNew = 
      (Experiencia)BeanUtils.cloneBean(
      experiencia);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (experienciaNew.getPersonal() != null) {
      experienciaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        experienciaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(experienciaNew);
  }

  public void updateExperiencia(Experiencia experiencia) throws Exception
  {
    Experiencia experienciaModify = 
      findExperienciaById(experiencia.getIdExperiencia());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (experiencia.getPersonal() != null) {
      experiencia.setPersonal(
        personalBeanBusiness.findPersonalById(
        experiencia.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(experienciaModify, experiencia);
  }

  public void deleteExperiencia(Experiencia experiencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Experiencia experienciaDelete = 
      findExperienciaById(experiencia.getIdExperiencia());
    pm.deletePersistent(experienciaDelete);
  }

  public Experiencia findExperienciaById(long idExperiencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idExperiencia == pIdExperiencia";
    Query query = pm.newQuery(Experiencia.class, filter);

    query.declareParameters("long pIdExperiencia");

    parameters.put("pIdExperiencia", new Long(idExperiencia));

    Collection colExperiencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colExperiencia.iterator();
    return (Experiencia)iterator.next();
  }

  public Collection findExperienciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent experienciaExtent = pm.getExtent(
      Experiencia.class, true);
    Query query = pm.newQuery(experienciaExtent);
    query.setOrdering("fechaIngreso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Experiencia.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaIngreso ascending");

    Collection colExperiencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExperiencia);

    return colExperiencia;
  }
}