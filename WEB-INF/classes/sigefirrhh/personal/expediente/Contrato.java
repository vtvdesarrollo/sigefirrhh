package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoContrato;
import sigefirrhh.base.definiciones.TipoPersonal;

public class Contrato
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_MODALIDAD;
  protected static final Map LISTA_SINO;
  private long idContrato;
  private TipoPersonal tipoPersonal;
  private TipoContrato tipoContrato;
  private Date fechaInicio;
  private Date fechaFin;
  private Date fechaRegistro;
  private String institucion;
  private String estatus;
  private Date fechaRescision;
  private String modalidad;
  private String prorroga;
  private double montoUnico;
  private double montoMensual;
  private String objetoContrato;
  private String tareasContrato;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "estatus", "fechaFin", "fechaInicio", "fechaRegistro", "fechaRescision", "idContrato", "idSitp", "institucion", "modalidad", "montoMensual", "montoUnico", "objetoContrato", "observaciones", "personal", "prorroga", "tareasContrato", "tiempoSitp", "tipoContrato", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoContrato"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Contrato"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Contrato());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_MODALIDAD = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("V", "VIGENTE");
    LISTA_ESTATUS.put("F", "FINALIZADO");
    LISTA_ESTATUS.put("R", "RESCINDIDO");
    LISTA_MODALIDAD.put("M", "MENSUAL");
    LISTA_MODALIDAD.put("A", "ANUAL");
    LISTA_MODALIDAD.put("P", "PRODUCTOS");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public Contrato()
  {
    jdoSetestatus(this, "V");

    jdoSetmodalidad(this, "M");

    jdoSetprorroga(this, "S");

    jdoSetmontoUnico(this, 0.0D);

    jdoSetmontoMensual(this, 0.0D);
  }

  public String toString()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this)) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaFin(this)) + " - " + 
      LISTA_ESTATUS.get(jdoGetestatus(this));
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public Date getFechaRescision()
  {
    return jdoGetfechaRescision(this);
  }

  public long getIdContrato()
  {
    return jdoGetidContrato(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public String getInstitucion()
  {
    return jdoGetinstitucion(this);
  }

  public String getModalidad()
  {
    return jdoGetmodalidad(this);
  }

  public double getMontoMensual()
  {
    return jdoGetmontoMensual(this);
  }

  public double getMontoUnico()
  {
    return jdoGetmontoUnico(this);
  }

  public String getObjetoContrato()
  {
    return jdoGetobjetoContrato(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public String getProrroga()
  {
    return jdoGetprorroga(this);
  }

  public String getTareasContrato()
  {
    return jdoGettareasContrato(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public TipoContrato getTipoContrato()
  {
    return jdoGettipoContrato(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setFechaRescision(Date date)
  {
    jdoSetfechaRescision(this, date);
  }

  public void setIdContrato(long l)
  {
    jdoSetidContrato(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setInstitucion(String string)
  {
    jdoSetinstitucion(this, string);
  }

  public void setModalidad(String string)
  {
    jdoSetmodalidad(this, string);
  }

  public void setMontoMensual(double d)
  {
    jdoSetmontoMensual(this, d);
  }

  public void setMontoUnico(double d)
  {
    jdoSetmontoUnico(this, d);
  }

  public void setObjetoContrato(String string)
  {
    jdoSetobjetoContrato(this, string);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setProrroga(String string)
  {
    jdoSetprorroga(this, string);
  }

  public void setTareasContrato(String string)
  {
    jdoSettareasContrato(this, string);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setTipoContrato(TipoContrato contrato)
  {
    jdoSettipoContrato(this, contrato);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 19;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Contrato localContrato = new Contrato();
    localContrato.jdoFlags = 1;
    localContrato.jdoStateManager = paramStateManager;
    return localContrato;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Contrato localContrato = new Contrato();
    localContrato.jdoCopyKeyFieldsFromObjectId(paramObject);
    localContrato.jdoFlags = 1;
    localContrato.jdoStateManager = paramStateManager;
    return localContrato;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRescision);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idContrato);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.institucion);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.modalidad);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoMensual);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoUnico);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.objetoContrato);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.prorroga);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tareasContrato);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoContrato);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRescision = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idContrato = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.institucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.modalidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoUnico = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.objetoContrato = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.prorroga = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tareasContrato = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoContrato = ((TipoContrato)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Contrato paramContrato, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramContrato.estatus;
      return;
    case 1:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramContrato.fechaFin;
      return;
    case 2:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramContrato.fechaInicio;
      return;
    case 3:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramContrato.fechaRegistro;
      return;
    case 4:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRescision = paramContrato.fechaRescision;
      return;
    case 5:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.idContrato = paramContrato.idContrato;
      return;
    case 6:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramContrato.idSitp;
      return;
    case 7:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.institucion = paramContrato.institucion;
      return;
    case 8:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.modalidad = paramContrato.modalidad;
      return;
    case 9:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.montoMensual = paramContrato.montoMensual;
      return;
    case 10:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.montoUnico = paramContrato.montoUnico;
      return;
    case 11:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.objetoContrato = paramContrato.objetoContrato;
      return;
    case 12:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramContrato.observaciones;
      return;
    case 13:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramContrato.personal;
      return;
    case 14:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.prorroga = paramContrato.prorroga;
      return;
    case 15:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.tareasContrato = paramContrato.tareasContrato;
      return;
    case 16:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramContrato.tiempoSitp;
      return;
    case 17:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.tipoContrato = paramContrato.tipoContrato;
      return;
    case 18:
      if (paramContrato == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramContrato.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Contrato))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Contrato localContrato = (Contrato)paramObject;
    if (localContrato.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localContrato, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ContratoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ContratoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContratoPK))
      throw new IllegalArgumentException("arg1");
    ContratoPK localContratoPK = (ContratoPK)paramObject;
    localContratoPK.idContrato = this.idContrato;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContratoPK))
      throw new IllegalArgumentException("arg1");
    ContratoPK localContratoPK = (ContratoPK)paramObject;
    this.idContrato = localContratoPK.idContrato;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContratoPK))
      throw new IllegalArgumentException("arg2");
    ContratoPK localContratoPK = (ContratoPK)paramObject;
    localContratoPK.idContrato = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContratoPK))
      throw new IllegalArgumentException("arg2");
    ContratoPK localContratoPK = (ContratoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localContratoPK.idContrato);
  }

  private static final String jdoGetestatus(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.estatus;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.estatus;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 0))
      return paramContrato.estatus;
    return localStateManager.getStringField(paramContrato, jdoInheritedFieldCount + 0, paramContrato.estatus);
  }

  private static final void jdoSetestatus(Contrato paramContrato, String paramString)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramContrato, jdoInheritedFieldCount + 0, paramContrato.estatus, paramString);
  }

  private static final Date jdoGetfechaFin(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.fechaFin;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.fechaFin;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 1))
      return paramContrato.fechaFin;
    return (Date)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 1, paramContrato.fechaFin);
  }

  private static final void jdoSetfechaFin(Contrato paramContrato, Date paramDate)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 1, paramContrato.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.fechaInicio;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.fechaInicio;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 2))
      return paramContrato.fechaInicio;
    return (Date)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 2, paramContrato.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Contrato paramContrato, Date paramDate)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 2, paramContrato.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaRegistro(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.fechaRegistro;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.fechaRegistro;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 3))
      return paramContrato.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 3, paramContrato.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(Contrato paramContrato, Date paramDate)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 3, paramContrato.fechaRegistro, paramDate);
  }

  private static final Date jdoGetfechaRescision(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.fechaRescision;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.fechaRescision;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 4))
      return paramContrato.fechaRescision;
    return (Date)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 4, paramContrato.fechaRescision);
  }

  private static final void jdoSetfechaRescision(Contrato paramContrato, Date paramDate)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.fechaRescision = paramDate;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.fechaRescision = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 4, paramContrato.fechaRescision, paramDate);
  }

  private static final long jdoGetidContrato(Contrato paramContrato)
  {
    return paramContrato.idContrato;
  }

  private static final void jdoSetidContrato(Contrato paramContrato, long paramLong)
  {
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.idContrato = paramLong;
      return;
    }
    localStateManager.setLongField(paramContrato, jdoInheritedFieldCount + 5, paramContrato.idContrato, paramLong);
  }

  private static final int jdoGetidSitp(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.idSitp;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.idSitp;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 6))
      return paramContrato.idSitp;
    return localStateManager.getIntField(paramContrato, jdoInheritedFieldCount + 6, paramContrato.idSitp);
  }

  private static final void jdoSetidSitp(Contrato paramContrato, int paramInt)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramContrato, jdoInheritedFieldCount + 6, paramContrato.idSitp, paramInt);
  }

  private static final String jdoGetinstitucion(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.institucion;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.institucion;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 7))
      return paramContrato.institucion;
    return localStateManager.getStringField(paramContrato, jdoInheritedFieldCount + 7, paramContrato.institucion);
  }

  private static final void jdoSetinstitucion(Contrato paramContrato, String paramString)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.institucion = paramString;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.institucion = paramString;
      return;
    }
    localStateManager.setStringField(paramContrato, jdoInheritedFieldCount + 7, paramContrato.institucion, paramString);
  }

  private static final String jdoGetmodalidad(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.modalidad;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.modalidad;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 8))
      return paramContrato.modalidad;
    return localStateManager.getStringField(paramContrato, jdoInheritedFieldCount + 8, paramContrato.modalidad);
  }

  private static final void jdoSetmodalidad(Contrato paramContrato, String paramString)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.modalidad = paramString;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.modalidad = paramString;
      return;
    }
    localStateManager.setStringField(paramContrato, jdoInheritedFieldCount + 8, paramContrato.modalidad, paramString);
  }

  private static final double jdoGetmontoMensual(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.montoMensual;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.montoMensual;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 9))
      return paramContrato.montoMensual;
    return localStateManager.getDoubleField(paramContrato, jdoInheritedFieldCount + 9, paramContrato.montoMensual);
  }

  private static final void jdoSetmontoMensual(Contrato paramContrato, double paramDouble)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.montoMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.montoMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramContrato, jdoInheritedFieldCount + 9, paramContrato.montoMensual, paramDouble);
  }

  private static final double jdoGetmontoUnico(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.montoUnico;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.montoUnico;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 10))
      return paramContrato.montoUnico;
    return localStateManager.getDoubleField(paramContrato, jdoInheritedFieldCount + 10, paramContrato.montoUnico);
  }

  private static final void jdoSetmontoUnico(Contrato paramContrato, double paramDouble)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.montoUnico = paramDouble;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.montoUnico = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramContrato, jdoInheritedFieldCount + 10, paramContrato.montoUnico, paramDouble);
  }

  private static final String jdoGetobjetoContrato(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.objetoContrato;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.objetoContrato;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 11))
      return paramContrato.objetoContrato;
    return localStateManager.getStringField(paramContrato, jdoInheritedFieldCount + 11, paramContrato.objetoContrato);
  }

  private static final void jdoSetobjetoContrato(Contrato paramContrato, String paramString)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.objetoContrato = paramString;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.objetoContrato = paramString;
      return;
    }
    localStateManager.setStringField(paramContrato, jdoInheritedFieldCount + 11, paramContrato.objetoContrato, paramString);
  }

  private static final String jdoGetobservaciones(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.observaciones;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.observaciones;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 12))
      return paramContrato.observaciones;
    return localStateManager.getStringField(paramContrato, jdoInheritedFieldCount + 12, paramContrato.observaciones);
  }

  private static final void jdoSetobservaciones(Contrato paramContrato, String paramString)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramContrato, jdoInheritedFieldCount + 12, paramContrato.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Contrato paramContrato)
  {
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.personal;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 13))
      return paramContrato.personal;
    return (Personal)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 13, paramContrato.personal);
  }

  private static final void jdoSetpersonal(Contrato paramContrato, Personal paramPersonal)
  {
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 13, paramContrato.personal, paramPersonal);
  }

  private static final String jdoGetprorroga(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.prorroga;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.prorroga;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 14))
      return paramContrato.prorroga;
    return localStateManager.getStringField(paramContrato, jdoInheritedFieldCount + 14, paramContrato.prorroga);
  }

  private static final void jdoSetprorroga(Contrato paramContrato, String paramString)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.prorroga = paramString;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.prorroga = paramString;
      return;
    }
    localStateManager.setStringField(paramContrato, jdoInheritedFieldCount + 14, paramContrato.prorroga, paramString);
  }

  private static final String jdoGettareasContrato(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.tareasContrato;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.tareasContrato;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 15))
      return paramContrato.tareasContrato;
    return localStateManager.getStringField(paramContrato, jdoInheritedFieldCount + 15, paramContrato.tareasContrato);
  }

  private static final void jdoSettareasContrato(Contrato paramContrato, String paramString)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.tareasContrato = paramString;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.tareasContrato = paramString;
      return;
    }
    localStateManager.setStringField(paramContrato, jdoInheritedFieldCount + 15, paramContrato.tareasContrato, paramString);
  }

  private static final Date jdoGettiempoSitp(Contrato paramContrato)
  {
    if (paramContrato.jdoFlags <= 0)
      return paramContrato.tiempoSitp;
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.tiempoSitp;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 16))
      return paramContrato.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 16, paramContrato.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Contrato paramContrato, Date paramDate)
  {
    if (paramContrato.jdoFlags == 0)
    {
      paramContrato.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 16, paramContrato.tiempoSitp, paramDate);
  }

  private static final TipoContrato jdoGettipoContrato(Contrato paramContrato)
  {
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.tipoContrato;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 17))
      return paramContrato.tipoContrato;
    return (TipoContrato)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 17, paramContrato.tipoContrato);
  }

  private static final void jdoSettipoContrato(Contrato paramContrato, TipoContrato paramTipoContrato)
  {
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.tipoContrato = paramTipoContrato;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 17, paramContrato.tipoContrato, paramTipoContrato);
  }

  private static final TipoPersonal jdoGettipoPersonal(Contrato paramContrato)
  {
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
      return paramContrato.tipoPersonal;
    if (localStateManager.isLoaded(paramContrato, jdoInheritedFieldCount + 18))
      return paramContrato.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramContrato, jdoInheritedFieldCount + 18, paramContrato.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Contrato paramContrato, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramContrato.jdoStateManager;
    if (localStateManager == null)
    {
      paramContrato.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramContrato, jdoInheritedFieldCount + 18, paramContrato.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}