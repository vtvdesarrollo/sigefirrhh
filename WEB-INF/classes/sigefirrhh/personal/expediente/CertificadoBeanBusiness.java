package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class CertificadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCertificado(Certificado certificado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Certificado certificadoNew = 
      (Certificado)BeanUtils.cloneBean(
      certificado);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (certificadoNew.getPersonal() != null) {
      certificadoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        certificadoNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(certificadoNew);
  }

  public void updateCertificado(Certificado certificado) throws Exception
  {
    Certificado certificadoModify = 
      findCertificadoById(certificado.getIdCertificado());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (certificado.getPersonal() != null) {
      certificado.setPersonal(
        personalBeanBusiness.findPersonalById(
        certificado.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(certificadoModify, certificado);
  }

  public void deleteCertificado(Certificado certificado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Certificado certificadoDelete = 
      findCertificadoById(certificado.getIdCertificado());
    pm.deletePersistent(certificadoDelete);
  }

  public Certificado findCertificadoById(long idCertificado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCertificado == pIdCertificado";
    Query query = pm.newQuery(Certificado.class, filter);

    query.declareParameters("long pIdCertificado");

    parameters.put("pIdCertificado", new Long(idCertificado));

    Collection colCertificado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCertificado.iterator();
    return (Certificado)iterator.next();
  }

  public Collection findCertificadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent certificadoExtent = pm.getExtent(
      Certificado.class, true);
    Query query = pm.newQuery(certificadoExtent);
    query.setOrdering("fechaEmision ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Certificado.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaEmision ascending");

    Collection colCertificado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCertificado);

    return colCertificado;
  }
}