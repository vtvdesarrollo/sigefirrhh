package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class DeclaracionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDeclaracion(Declaracion declaracion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Declaracion declaracionNew = 
      (Declaracion)BeanUtils.cloneBean(
      declaracion);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (declaracionNew.getPersonal() != null) {
      declaracionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        declaracionNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (declaracionNew.getOrganismo() != null) {
      declaracionNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        declaracionNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(declaracionNew);
  }

  public void updateDeclaracion(Declaracion declaracion) throws Exception
  {
    Declaracion declaracionModify = 
      findDeclaracionById(declaracion.getIdDeclaracion());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (declaracion.getPersonal() != null) {
      declaracion.setPersonal(
        personalBeanBusiness.findPersonalById(
        declaracion.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (declaracion.getOrganismo() != null) {
      declaracion.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        declaracion.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(declaracionModify, declaracion);
  }

  public void deleteDeclaracion(Declaracion declaracion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Declaracion declaracionDelete = 
      findDeclaracionById(declaracion.getIdDeclaracion());
    pm.deletePersistent(declaracionDelete);
  }

  public Declaracion findDeclaracionById(long idDeclaracion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDeclaracion == pIdDeclaracion";
    Query query = pm.newQuery(Declaracion.class, filter);

    query.declareParameters("long pIdDeclaracion");

    parameters.put("pIdDeclaracion", new Long(idDeclaracion));

    Collection colDeclaracion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDeclaracion.iterator();
    return (Declaracion)iterator.next();
  }

  public Collection findDeclaracionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent declaracionExtent = pm.getExtent(
      Declaracion.class, true);
    Query query = pm.newQuery(declaracionExtent);
    query.setOrdering("fechaRegistro ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Declaracion.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaRegistro ascending");

    Collection colDeclaracion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDeclaracion);

    return colDeclaracion;
  }
}