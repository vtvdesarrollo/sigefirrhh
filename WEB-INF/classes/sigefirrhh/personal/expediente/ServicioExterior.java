package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.mre.SedeDiplomatica;

public class ServicioExterior
  implements Serializable, PersistenceCapable
{
  private long idServicioExterior;
  private SedeDiplomatica sedeDiplomatica;
  private Date fechaInicio;
  private Date fechaFin;
  private Cargo cargo;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargo", "fechaFin", "fechaInicio", "idServicioExterior", "idSitp", "personal", "sedeDiplomatica", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.mre.SedeDiplomatica"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 24, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetsedeDiplomatica(this).getNombre() + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this));
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public long getIdServicioExterior()
  {
    return jdoGetidServicioExterior(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public SedeDiplomatica getSedeDiplomatica()
  {
    return jdoGetsedeDiplomatica(this);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setIdServicioExterior(long i)
  {
    jdoSetidServicioExterior(this, i);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setSedeDiplomatica(SedeDiplomatica diplomatica)
  {
    jdoSetsedeDiplomatica(this, diplomatica);
  }

  public Cargo getCargo() {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.ServicioExterior"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ServicioExterior());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ServicioExterior localServicioExterior = new ServicioExterior();
    localServicioExterior.jdoFlags = 1;
    localServicioExterior.jdoStateManager = paramStateManager;
    return localServicioExterior;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ServicioExterior localServicioExterior = new ServicioExterior();
    localServicioExterior.jdoCopyKeyFieldsFromObjectId(paramObject);
    localServicioExterior.jdoFlags = 1;
    localServicioExterior.jdoStateManager = paramStateManager;
    return localServicioExterior;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idServicioExterior);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sedeDiplomatica);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idServicioExterior = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sedeDiplomatica = ((SedeDiplomatica)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ServicioExterior paramServicioExterior, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramServicioExterior.cargo;
      return;
    case 1:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramServicioExterior.fechaFin;
      return;
    case 2:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramServicioExterior.fechaInicio;
      return;
    case 3:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.idServicioExterior = paramServicioExterior.idServicioExterior;
      return;
    case 4:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramServicioExterior.idSitp;
      return;
    case 5:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramServicioExterior.personal;
      return;
    case 6:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.sedeDiplomatica = paramServicioExterior.sedeDiplomatica;
      return;
    case 7:
      if (paramServicioExterior == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramServicioExterior.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ServicioExterior))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ServicioExterior localServicioExterior = (ServicioExterior)paramObject;
    if (localServicioExterior.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localServicioExterior, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ServicioExteriorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ServicioExteriorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ServicioExteriorPK))
      throw new IllegalArgumentException("arg1");
    ServicioExteriorPK localServicioExteriorPK = (ServicioExteriorPK)paramObject;
    localServicioExteriorPK.idServicioExterior = this.idServicioExterior;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ServicioExteriorPK))
      throw new IllegalArgumentException("arg1");
    ServicioExteriorPK localServicioExteriorPK = (ServicioExteriorPK)paramObject;
    this.idServicioExterior = localServicioExteriorPK.idServicioExterior;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ServicioExteriorPK))
      throw new IllegalArgumentException("arg2");
    ServicioExteriorPK localServicioExteriorPK = (ServicioExteriorPK)paramObject;
    localServicioExteriorPK.idServicioExterior = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ServicioExteriorPK))
      throw new IllegalArgumentException("arg2");
    ServicioExteriorPK localServicioExteriorPK = (ServicioExteriorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localServicioExteriorPK.idServicioExterior);
  }

  private static final Cargo jdoGetcargo(ServicioExterior paramServicioExterior)
  {
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
      return paramServicioExterior.cargo;
    if (localStateManager.isLoaded(paramServicioExterior, jdoInheritedFieldCount + 0))
      return paramServicioExterior.cargo;
    return (Cargo)localStateManager.getObjectField(paramServicioExterior, jdoInheritedFieldCount + 0, paramServicioExterior.cargo);
  }

  private static final void jdoSetcargo(ServicioExterior paramServicioExterior, Cargo paramCargo)
  {
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramServicioExterior, jdoInheritedFieldCount + 0, paramServicioExterior.cargo, paramCargo);
  }

  private static final Date jdoGetfechaFin(ServicioExterior paramServicioExterior)
  {
    if (paramServicioExterior.jdoFlags <= 0)
      return paramServicioExterior.fechaFin;
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
      return paramServicioExterior.fechaFin;
    if (localStateManager.isLoaded(paramServicioExterior, jdoInheritedFieldCount + 1))
      return paramServicioExterior.fechaFin;
    return (Date)localStateManager.getObjectField(paramServicioExterior, jdoInheritedFieldCount + 1, paramServicioExterior.fechaFin);
  }

  private static final void jdoSetfechaFin(ServicioExterior paramServicioExterior, Date paramDate)
  {
    if (paramServicioExterior.jdoFlags == 0)
    {
      paramServicioExterior.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramServicioExterior, jdoInheritedFieldCount + 1, paramServicioExterior.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(ServicioExterior paramServicioExterior)
  {
    if (paramServicioExterior.jdoFlags <= 0)
      return paramServicioExterior.fechaInicio;
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
      return paramServicioExterior.fechaInicio;
    if (localStateManager.isLoaded(paramServicioExterior, jdoInheritedFieldCount + 2))
      return paramServicioExterior.fechaInicio;
    return (Date)localStateManager.getObjectField(paramServicioExterior, jdoInheritedFieldCount + 2, paramServicioExterior.fechaInicio);
  }

  private static final void jdoSetfechaInicio(ServicioExterior paramServicioExterior, Date paramDate)
  {
    if (paramServicioExterior.jdoFlags == 0)
    {
      paramServicioExterior.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramServicioExterior, jdoInheritedFieldCount + 2, paramServicioExterior.fechaInicio, paramDate);
  }

  private static final long jdoGetidServicioExterior(ServicioExterior paramServicioExterior)
  {
    return paramServicioExterior.idServicioExterior;
  }

  private static final void jdoSetidServicioExterior(ServicioExterior paramServicioExterior, long paramLong)
  {
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.idServicioExterior = paramLong;
      return;
    }
    localStateManager.setLongField(paramServicioExterior, jdoInheritedFieldCount + 3, paramServicioExterior.idServicioExterior, paramLong);
  }

  private static final int jdoGetidSitp(ServicioExterior paramServicioExterior)
  {
    if (paramServicioExterior.jdoFlags <= 0)
      return paramServicioExterior.idSitp;
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
      return paramServicioExterior.idSitp;
    if (localStateManager.isLoaded(paramServicioExterior, jdoInheritedFieldCount + 4))
      return paramServicioExterior.idSitp;
    return localStateManager.getIntField(paramServicioExterior, jdoInheritedFieldCount + 4, paramServicioExterior.idSitp);
  }

  private static final void jdoSetidSitp(ServicioExterior paramServicioExterior, int paramInt)
  {
    if (paramServicioExterior.jdoFlags == 0)
    {
      paramServicioExterior.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramServicioExterior, jdoInheritedFieldCount + 4, paramServicioExterior.idSitp, paramInt);
  }

  private static final Personal jdoGetpersonal(ServicioExterior paramServicioExterior)
  {
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
      return paramServicioExterior.personal;
    if (localStateManager.isLoaded(paramServicioExterior, jdoInheritedFieldCount + 5))
      return paramServicioExterior.personal;
    return (Personal)localStateManager.getObjectField(paramServicioExterior, jdoInheritedFieldCount + 5, paramServicioExterior.personal);
  }

  private static final void jdoSetpersonal(ServicioExterior paramServicioExterior, Personal paramPersonal)
  {
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramServicioExterior, jdoInheritedFieldCount + 5, paramServicioExterior.personal, paramPersonal);
  }

  private static final SedeDiplomatica jdoGetsedeDiplomatica(ServicioExterior paramServicioExterior)
  {
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
      return paramServicioExterior.sedeDiplomatica;
    if (localStateManager.isLoaded(paramServicioExterior, jdoInheritedFieldCount + 6))
      return paramServicioExterior.sedeDiplomatica;
    return (SedeDiplomatica)localStateManager.getObjectField(paramServicioExterior, jdoInheritedFieldCount + 6, paramServicioExterior.sedeDiplomatica);
  }

  private static final void jdoSetsedeDiplomatica(ServicioExterior paramServicioExterior, SedeDiplomatica paramSedeDiplomatica)
  {
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.sedeDiplomatica = paramSedeDiplomatica;
      return;
    }
    localStateManager.setObjectField(paramServicioExterior, jdoInheritedFieldCount + 6, paramServicioExterior.sedeDiplomatica, paramSedeDiplomatica);
  }

  private static final Date jdoGettiempoSitp(ServicioExterior paramServicioExterior)
  {
    if (paramServicioExterior.jdoFlags <= 0)
      return paramServicioExterior.tiempoSitp;
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
      return paramServicioExterior.tiempoSitp;
    if (localStateManager.isLoaded(paramServicioExterior, jdoInheritedFieldCount + 7))
      return paramServicioExterior.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramServicioExterior, jdoInheritedFieldCount + 7, paramServicioExterior.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ServicioExterior paramServicioExterior, Date paramDate)
  {
    if (paramServicioExterior.jdoFlags == 0)
    {
      paramServicioExterior.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramServicioExterior.jdoStateManager;
    if (localStateManager == null)
    {
      paramServicioExterior.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramServicioExterior, jdoInheritedFieldCount + 7, paramServicioExterior.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}