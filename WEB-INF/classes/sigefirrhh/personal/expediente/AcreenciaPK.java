package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class AcreenciaPK
  implements Serializable
{
  public long idAcreencia;

  public AcreenciaPK()
  {
  }

  public AcreenciaPK(long idAcreencia)
  {
    this.idAcreencia = idAcreencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AcreenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AcreenciaPK thatPK)
  {
    return 
      this.idAcreencia == thatPK.idAcreencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAcreencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAcreencia);
  }
}