package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ExperienciaNoEstPK
  implements Serializable
{
  public long idExperienciaNoEst;

  public ExperienciaNoEstPK()
  {
  }

  public ExperienciaNoEstPK(long idExperienciaNoEst)
  {
    this.idExperienciaNoEst = idExperienciaNoEst;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ExperienciaNoEstPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ExperienciaNoEstPK thatPK)
  {
    return 
      this.idExperienciaNoEst == thatPK.idExperienciaNoEst;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idExperienciaNoEst)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idExperienciaNoEst);
  }
}