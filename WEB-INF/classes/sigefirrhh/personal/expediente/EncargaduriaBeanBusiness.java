package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class EncargaduriaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEncargaduria(Encargaduria encargaduria)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Encargaduria encargaduriaNew = 
      (Encargaduria)BeanUtils.cloneBean(
      encargaduria);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (encargaduriaNew.getPersonal() != null) {
      encargaduriaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        encargaduriaNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (encargaduriaNew.getOrganismo() != null) {
      encargaduriaNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        encargaduriaNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(encargaduriaNew);
  }

  public void updateEncargaduria(Encargaduria encargaduria) throws Exception
  {
    Encargaduria encargaduriaModify = 
      findEncargaduriaById(encargaduria.getIdEncargaduria());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (encargaduria.getPersonal() != null) {
      encargaduria.setPersonal(
        personalBeanBusiness.findPersonalById(
        encargaduria.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (encargaduria.getOrganismo() != null) {
      encargaduria.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        encargaduria.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(encargaduriaModify, encargaduria);
  }

  public void deleteEncargaduria(Encargaduria encargaduria) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Encargaduria encargaduriaDelete = 
      findEncargaduriaById(encargaduria.getIdEncargaduria());
    pm.deletePersistent(encargaduriaDelete);
  }

  public Encargaduria findEncargaduriaById(long idEncargaduria) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEncargaduria == pIdEncargaduria";
    Query query = pm.newQuery(Encargaduria.class, filter);

    query.declareParameters("long pIdEncargaduria");

    parameters.put("pIdEncargaduria", new Long(idEncargaduria));

    Collection colEncargaduria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEncargaduria.iterator();
    return (Encargaduria)iterator.next();
  }

  public Collection findEncargaduriaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent encargaduriaExtent = pm.getExtent(
      Encargaduria.class, true);
    Query query = pm.newQuery(encargaduriaExtent);
    query.setOrdering("fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Encargaduria.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaInicio ascending");

    Collection colEncargaduria = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEncargaduria);

    return colEncargaduria;
  }
}