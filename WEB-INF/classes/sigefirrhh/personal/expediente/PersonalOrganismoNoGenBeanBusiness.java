package sigefirrhh.personal.expediente;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class PersonalOrganismoNoGenBeanBusiness
  implements Serializable
{
  public PersonalOrganismo findPersonalOrganismoByPersonalAndOrganismo(long idPersonal, long idOrganismo)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.idPersonal == pIdPersonal && organismo.idOrganismo == pIdOrganismo";
    Query query = pm.newQuery(PersonalOrganismo.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPersonalOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPersonalOrganismo.iterator();

    return (PersonalOrganismo)iterator.next();
  }
}