package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.mre.SedeDiplomatica;
import sigefirrhh.base.mre.SedeDiplomaticaBeanBusiness;

public class ComisionServicioExtBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addComisionServicioExt(ComisionServicioExt comisionServicioExt)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ComisionServicioExt comisionServicioExtNew = 
      (ComisionServicioExt)BeanUtils.cloneBean(
      comisionServicioExt);

    SedeDiplomaticaBeanBusiness sedeDiplomaticaBeanBusiness = new SedeDiplomaticaBeanBusiness();

    if (comisionServicioExtNew.getSedeDiplomatica() != null) {
      comisionServicioExtNew.setSedeDiplomatica(
        sedeDiplomaticaBeanBusiness.findSedeDiplomaticaById(
        comisionServicioExtNew.getSedeDiplomatica().getIdSedeDiplomatica()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (comisionServicioExtNew.getPersonal() != null) {
      comisionServicioExtNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        comisionServicioExtNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(comisionServicioExtNew);
  }

  public void updateComisionServicioExt(ComisionServicioExt comisionServicioExt) throws Exception
  {
    ComisionServicioExt comisionServicioExtModify = 
      findComisionServicioExtById(comisionServicioExt.getIdComisionServicioExt());

    SedeDiplomaticaBeanBusiness sedeDiplomaticaBeanBusiness = new SedeDiplomaticaBeanBusiness();

    if (comisionServicioExt.getSedeDiplomatica() != null) {
      comisionServicioExt.setSedeDiplomatica(
        sedeDiplomaticaBeanBusiness.findSedeDiplomaticaById(
        comisionServicioExt.getSedeDiplomatica().getIdSedeDiplomatica()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (comisionServicioExt.getPersonal() != null) {
      comisionServicioExt.setPersonal(
        personalBeanBusiness.findPersonalById(
        comisionServicioExt.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(comisionServicioExtModify, comisionServicioExt);
  }

  public void deleteComisionServicioExt(ComisionServicioExt comisionServicioExt) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ComisionServicioExt comisionServicioExtDelete = 
      findComisionServicioExtById(comisionServicioExt.getIdComisionServicioExt());
    pm.deletePersistent(comisionServicioExtDelete);
  }

  public ComisionServicioExt findComisionServicioExtById(long idComisionServicioExt) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idComisionServicioExt == pIdComisionServicioExt";
    Query query = pm.newQuery(ComisionServicioExt.class, filter);

    query.declareParameters("long pIdComisionServicioExt");

    parameters.put("pIdComisionServicioExt", new Long(idComisionServicioExt));

    Collection colComisionServicioExt = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colComisionServicioExt.iterator();
    return (ComisionServicioExt)iterator.next();
  }

  public Collection findComisionServicioExtAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent comisionServicioExtExtent = pm.getExtent(
      ComisionServicioExt.class, true);
    Query query = pm.newQuery(comisionServicioExtExtent);
    query.setOrdering("nombreInstitucion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ComisionServicioExt.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("nombreInstitucion ascending");

    Collection colComisionServicioExt = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colComisionServicioExt);

    return colComisionServicioExt;
  }
}