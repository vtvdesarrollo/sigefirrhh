package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.mre.SedeDiplomatica;

public class ComisionServicioExt
  implements Serializable, PersistenceCapable
{
  private long idComisionServicioExt;
  private String nombreInstitucion;
  private Date fechaInicio;
  private Date fechaFin;
  private String observaciones;
  private SedeDiplomatica sedeDiplomatica;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fechaFin", "fechaInicio", "idComisionServicioExt", "idSitp", "nombreInstitucion", "observaciones", "personal", "sedeDiplomatica", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.mre.SedeDiplomatica"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombreInstitucion(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this));
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public long getIdComisionServicioExt() {
    return jdoGetidComisionServicioExt(this);
  }

  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public SedeDiplomatica getSedeDiplomatica() {
    return jdoGetsedeDiplomatica(this);
  }

  public void setFechaFin(Date date) {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date) {
    jdoSetfechaInicio(this, date);
  }

  public void setIdComisionServicioExt(long l) {
    jdoSetidComisionServicioExt(this, l);
  }

  public void setObservaciones(String string) {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public void setSedeDiplomatica(SedeDiplomatica diplomatica) {
    jdoSetsedeDiplomatica(this, diplomatica);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public String getNombreInstitucion()
  {
    return jdoGetnombreInstitucion(this);
  }

  public void setNombreInstitucion(String string)
  {
    jdoSetnombreInstitucion(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.ComisionServicioExt"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ComisionServicioExt());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ComisionServicioExt localComisionServicioExt = new ComisionServicioExt();
    localComisionServicioExt.jdoFlags = 1;
    localComisionServicioExt.jdoStateManager = paramStateManager;
    return localComisionServicioExt;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ComisionServicioExt localComisionServicioExt = new ComisionServicioExt();
    localComisionServicioExt.jdoCopyKeyFieldsFromObjectId(paramObject);
    localComisionServicioExt.jdoFlags = 1;
    localComisionServicioExt.jdoStateManager = paramStateManager;
    return localComisionServicioExt;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idComisionServicioExt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInstitucion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sedeDiplomatica);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idComisionServicioExt = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sedeDiplomatica = ((SedeDiplomatica)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ComisionServicioExt paramComisionServicioExt, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramComisionServicioExt.fechaFin;
      return;
    case 1:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramComisionServicioExt.fechaInicio;
      return;
    case 2:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.idComisionServicioExt = paramComisionServicioExt.idComisionServicioExt;
      return;
    case 3:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramComisionServicioExt.idSitp;
      return;
    case 4:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInstitucion = paramComisionServicioExt.nombreInstitucion;
      return;
    case 5:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramComisionServicioExt.observaciones;
      return;
    case 6:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramComisionServicioExt.personal;
      return;
    case 7:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.sedeDiplomatica = paramComisionServicioExt.sedeDiplomatica;
      return;
    case 8:
      if (paramComisionServicioExt == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramComisionServicioExt.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ComisionServicioExt))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ComisionServicioExt localComisionServicioExt = (ComisionServicioExt)paramObject;
    if (localComisionServicioExt.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localComisionServicioExt, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ComisionServicioExtPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ComisionServicioExtPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ComisionServicioExtPK))
      throw new IllegalArgumentException("arg1");
    ComisionServicioExtPK localComisionServicioExtPK = (ComisionServicioExtPK)paramObject;
    localComisionServicioExtPK.idComisionServicioExt = this.idComisionServicioExt;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ComisionServicioExtPK))
      throw new IllegalArgumentException("arg1");
    ComisionServicioExtPK localComisionServicioExtPK = (ComisionServicioExtPK)paramObject;
    this.idComisionServicioExt = localComisionServicioExtPK.idComisionServicioExt;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ComisionServicioExtPK))
      throw new IllegalArgumentException("arg2");
    ComisionServicioExtPK localComisionServicioExtPK = (ComisionServicioExtPK)paramObject;
    localComisionServicioExtPK.idComisionServicioExt = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ComisionServicioExtPK))
      throw new IllegalArgumentException("arg2");
    ComisionServicioExtPK localComisionServicioExtPK = (ComisionServicioExtPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localComisionServicioExtPK.idComisionServicioExt);
  }

  private static final Date jdoGetfechaFin(ComisionServicioExt paramComisionServicioExt)
  {
    if (paramComisionServicioExt.jdoFlags <= 0)
      return paramComisionServicioExt.fechaFin;
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.fechaFin;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 0))
      return paramComisionServicioExt.fechaFin;
    return (Date)localStateManager.getObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 0, paramComisionServicioExt.fechaFin);
  }

  private static final void jdoSetfechaFin(ComisionServicioExt paramComisionServicioExt, Date paramDate)
  {
    if (paramComisionServicioExt.jdoFlags == 0)
    {
      paramComisionServicioExt.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 0, paramComisionServicioExt.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(ComisionServicioExt paramComisionServicioExt)
  {
    if (paramComisionServicioExt.jdoFlags <= 0)
      return paramComisionServicioExt.fechaInicio;
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.fechaInicio;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 1))
      return paramComisionServicioExt.fechaInicio;
    return (Date)localStateManager.getObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 1, paramComisionServicioExt.fechaInicio);
  }

  private static final void jdoSetfechaInicio(ComisionServicioExt paramComisionServicioExt, Date paramDate)
  {
    if (paramComisionServicioExt.jdoFlags == 0)
    {
      paramComisionServicioExt.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 1, paramComisionServicioExt.fechaInicio, paramDate);
  }

  private static final long jdoGetidComisionServicioExt(ComisionServicioExt paramComisionServicioExt)
  {
    return paramComisionServicioExt.idComisionServicioExt;
  }

  private static final void jdoSetidComisionServicioExt(ComisionServicioExt paramComisionServicioExt, long paramLong)
  {
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.idComisionServicioExt = paramLong;
      return;
    }
    localStateManager.setLongField(paramComisionServicioExt, jdoInheritedFieldCount + 2, paramComisionServicioExt.idComisionServicioExt, paramLong);
  }

  private static final int jdoGetidSitp(ComisionServicioExt paramComisionServicioExt)
  {
    if (paramComisionServicioExt.jdoFlags <= 0)
      return paramComisionServicioExt.idSitp;
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.idSitp;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 3))
      return paramComisionServicioExt.idSitp;
    return localStateManager.getIntField(paramComisionServicioExt, jdoInheritedFieldCount + 3, paramComisionServicioExt.idSitp);
  }

  private static final void jdoSetidSitp(ComisionServicioExt paramComisionServicioExt, int paramInt)
  {
    if (paramComisionServicioExt.jdoFlags == 0)
    {
      paramComisionServicioExt.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramComisionServicioExt, jdoInheritedFieldCount + 3, paramComisionServicioExt.idSitp, paramInt);
  }

  private static final String jdoGetnombreInstitucion(ComisionServicioExt paramComisionServicioExt)
  {
    if (paramComisionServicioExt.jdoFlags <= 0)
      return paramComisionServicioExt.nombreInstitucion;
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.nombreInstitucion;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 4))
      return paramComisionServicioExt.nombreInstitucion;
    return localStateManager.getStringField(paramComisionServicioExt, jdoInheritedFieldCount + 4, paramComisionServicioExt.nombreInstitucion);
  }

  private static final void jdoSetnombreInstitucion(ComisionServicioExt paramComisionServicioExt, String paramString)
  {
    if (paramComisionServicioExt.jdoFlags == 0)
    {
      paramComisionServicioExt.nombreInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.nombreInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramComisionServicioExt, jdoInheritedFieldCount + 4, paramComisionServicioExt.nombreInstitucion, paramString);
  }

  private static final String jdoGetobservaciones(ComisionServicioExt paramComisionServicioExt)
  {
    if (paramComisionServicioExt.jdoFlags <= 0)
      return paramComisionServicioExt.observaciones;
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.observaciones;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 5))
      return paramComisionServicioExt.observaciones;
    return localStateManager.getStringField(paramComisionServicioExt, jdoInheritedFieldCount + 5, paramComisionServicioExt.observaciones);
  }

  private static final void jdoSetobservaciones(ComisionServicioExt paramComisionServicioExt, String paramString)
  {
    if (paramComisionServicioExt.jdoFlags == 0)
    {
      paramComisionServicioExt.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramComisionServicioExt, jdoInheritedFieldCount + 5, paramComisionServicioExt.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(ComisionServicioExt paramComisionServicioExt)
  {
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.personal;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 6))
      return paramComisionServicioExt.personal;
    return (Personal)localStateManager.getObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 6, paramComisionServicioExt.personal);
  }

  private static final void jdoSetpersonal(ComisionServicioExt paramComisionServicioExt, Personal paramPersonal)
  {
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 6, paramComisionServicioExt.personal, paramPersonal);
  }

  private static final SedeDiplomatica jdoGetsedeDiplomatica(ComisionServicioExt paramComisionServicioExt)
  {
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.sedeDiplomatica;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 7))
      return paramComisionServicioExt.sedeDiplomatica;
    return (SedeDiplomatica)localStateManager.getObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 7, paramComisionServicioExt.sedeDiplomatica);
  }

  private static final void jdoSetsedeDiplomatica(ComisionServicioExt paramComisionServicioExt, SedeDiplomatica paramSedeDiplomatica)
  {
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.sedeDiplomatica = paramSedeDiplomatica;
      return;
    }
    localStateManager.setObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 7, paramComisionServicioExt.sedeDiplomatica, paramSedeDiplomatica);
  }

  private static final Date jdoGettiempoSitp(ComisionServicioExt paramComisionServicioExt)
  {
    if (paramComisionServicioExt.jdoFlags <= 0)
      return paramComisionServicioExt.tiempoSitp;
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
      return paramComisionServicioExt.tiempoSitp;
    if (localStateManager.isLoaded(paramComisionServicioExt, jdoInheritedFieldCount + 8))
      return paramComisionServicioExt.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 8, paramComisionServicioExt.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ComisionServicioExt paramComisionServicioExt, Date paramDate)
  {
    if (paramComisionServicioExt.jdoFlags == 0)
    {
      paramComisionServicioExt.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramComisionServicioExt.jdoStateManager;
    if (localStateManager == null)
    {
      paramComisionServicioExt.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComisionServicioExt, jdoInheritedFieldCount + 8, paramComisionServicioExt.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}