package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Suplencia
  implements Serializable, PersistenceCapable
{
  private long idSuplencia;
  private Date fechaInicio;
  private Date fechaFin;
  private String personaSuplencia;
  private String motivoSuplencia;
  private String cargo;
  private String dependencia;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargo", "dependencia", "fechaFin", "fechaInicio", "idSitp", "idSuplencia", "motivoSuplencia", "observaciones", "personaSuplencia", "personal", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcargo(this) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaInicio(this));
  }

  public String getCargo() {
    return jdoGetcargo(this);
  }

  public String getDependencia() {
    return jdoGetdependencia(this);
  }

  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setCargo(String string) {
    jdoSetcargo(this, string);
  }

  public void setDependencia(String string) {
    jdoSetdependencia(this, string);
  }

  public void setFechaFin(Date date) {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date) {
    jdoSetfechaInicio(this, date);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public long getIdSuplencia()
  {
    return jdoGetidSuplencia(this);
  }

  public String getMotivoSuplencia()
  {
    return jdoGetmotivoSuplencia(this);
  }

  public String getPersonaSuplencia()
  {
    return jdoGetpersonaSuplencia(this);
  }

  public void setIdSuplencia(long l)
  {
    jdoSetidSuplencia(this, l);
  }

  public void setMotivoSuplencia(String string)
  {
    jdoSetmotivoSuplencia(this, string);
  }

  public void setPersonaSuplencia(String string)
  {
    jdoSetpersonaSuplencia(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Suplencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Suplencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Suplencia localSuplencia = new Suplencia();
    localSuplencia.jdoFlags = 1;
    localSuplencia.jdoStateManager = paramStateManager;
    return localSuplencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Suplencia localSuplencia = new Suplencia();
    localSuplencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSuplencia.jdoFlags = 1;
    localSuplencia.jdoStateManager = paramStateManager;
    return localSuplencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.dependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSuplencia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.motivoSuplencia);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personaSuplencia);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSuplencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.motivoSuplencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personaSuplencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Suplencia paramSuplencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramSuplencia.cargo;
      return;
    case 1:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramSuplencia.dependencia;
      return;
    case 2:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramSuplencia.fechaFin;
      return;
    case 3:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramSuplencia.fechaInicio;
      return;
    case 4:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramSuplencia.idSitp;
      return;
    case 5:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.idSuplencia = paramSuplencia.idSuplencia;
      return;
    case 6:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.motivoSuplencia = paramSuplencia.motivoSuplencia;
      return;
    case 7:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramSuplencia.observaciones;
      return;
    case 8:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.personaSuplencia = paramSuplencia.personaSuplencia;
      return;
    case 9:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramSuplencia.personal;
      return;
    case 10:
      if (paramSuplencia == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramSuplencia.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Suplencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Suplencia localSuplencia = (Suplencia)paramObject;
    if (localSuplencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSuplencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SuplenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SuplenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SuplenciaPK))
      throw new IllegalArgumentException("arg1");
    SuplenciaPK localSuplenciaPK = (SuplenciaPK)paramObject;
    localSuplenciaPK.idSuplencia = this.idSuplencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SuplenciaPK))
      throw new IllegalArgumentException("arg1");
    SuplenciaPK localSuplenciaPK = (SuplenciaPK)paramObject;
    this.idSuplencia = localSuplenciaPK.idSuplencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SuplenciaPK))
      throw new IllegalArgumentException("arg2");
    SuplenciaPK localSuplenciaPK = (SuplenciaPK)paramObject;
    localSuplenciaPK.idSuplencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SuplenciaPK))
      throw new IllegalArgumentException("arg2");
    SuplenciaPK localSuplenciaPK = (SuplenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localSuplenciaPK.idSuplencia);
  }

  private static final String jdoGetcargo(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.cargo;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.cargo;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 0))
      return paramSuplencia.cargo;
    return localStateManager.getStringField(paramSuplencia, jdoInheritedFieldCount + 0, paramSuplencia.cargo);
  }

  private static final void jdoSetcargo(Suplencia paramSuplencia, String paramString)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.cargo = paramString;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.cargo = paramString;
      return;
    }
    localStateManager.setStringField(paramSuplencia, jdoInheritedFieldCount + 0, paramSuplencia.cargo, paramString);
  }

  private static final String jdoGetdependencia(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.dependencia;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.dependencia;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 1))
      return paramSuplencia.dependencia;
    return localStateManager.getStringField(paramSuplencia, jdoInheritedFieldCount + 1, paramSuplencia.dependencia);
  }

  private static final void jdoSetdependencia(Suplencia paramSuplencia, String paramString)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.dependencia = paramString;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.dependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramSuplencia, jdoInheritedFieldCount + 1, paramSuplencia.dependencia, paramString);
  }

  private static final Date jdoGetfechaFin(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.fechaFin;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.fechaFin;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 2))
      return paramSuplencia.fechaFin;
    return (Date)localStateManager.getObjectField(paramSuplencia, jdoInheritedFieldCount + 2, paramSuplencia.fechaFin);
  }

  private static final void jdoSetfechaFin(Suplencia paramSuplencia, Date paramDate)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSuplencia, jdoInheritedFieldCount + 2, paramSuplencia.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.fechaInicio;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.fechaInicio;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 3))
      return paramSuplencia.fechaInicio;
    return (Date)localStateManager.getObjectField(paramSuplencia, jdoInheritedFieldCount + 3, paramSuplencia.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Suplencia paramSuplencia, Date paramDate)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSuplencia, jdoInheritedFieldCount + 3, paramSuplencia.fechaInicio, paramDate);
  }

  private static final int jdoGetidSitp(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.idSitp;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.idSitp;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 4))
      return paramSuplencia.idSitp;
    return localStateManager.getIntField(paramSuplencia, jdoInheritedFieldCount + 4, paramSuplencia.idSitp);
  }

  private static final void jdoSetidSitp(Suplencia paramSuplencia, int paramInt)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramSuplencia, jdoInheritedFieldCount + 4, paramSuplencia.idSitp, paramInt);
  }

  private static final long jdoGetidSuplencia(Suplencia paramSuplencia)
  {
    return paramSuplencia.idSuplencia;
  }

  private static final void jdoSetidSuplencia(Suplencia paramSuplencia, long paramLong)
  {
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.idSuplencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramSuplencia, jdoInheritedFieldCount + 5, paramSuplencia.idSuplencia, paramLong);
  }

  private static final String jdoGetmotivoSuplencia(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.motivoSuplencia;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.motivoSuplencia;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 6))
      return paramSuplencia.motivoSuplencia;
    return localStateManager.getStringField(paramSuplencia, jdoInheritedFieldCount + 6, paramSuplencia.motivoSuplencia);
  }

  private static final void jdoSetmotivoSuplencia(Suplencia paramSuplencia, String paramString)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.motivoSuplencia = paramString;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.motivoSuplencia = paramString;
      return;
    }
    localStateManager.setStringField(paramSuplencia, jdoInheritedFieldCount + 6, paramSuplencia.motivoSuplencia, paramString);
  }

  private static final String jdoGetobservaciones(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.observaciones;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.observaciones;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 7))
      return paramSuplencia.observaciones;
    return localStateManager.getStringField(paramSuplencia, jdoInheritedFieldCount + 7, paramSuplencia.observaciones);
  }

  private static final void jdoSetobservaciones(Suplencia paramSuplencia, String paramString)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramSuplencia, jdoInheritedFieldCount + 7, paramSuplencia.observaciones, paramString);
  }

  private static final String jdoGetpersonaSuplencia(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.personaSuplencia;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.personaSuplencia;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 8))
      return paramSuplencia.personaSuplencia;
    return localStateManager.getStringField(paramSuplencia, jdoInheritedFieldCount + 8, paramSuplencia.personaSuplencia);
  }

  private static final void jdoSetpersonaSuplencia(Suplencia paramSuplencia, String paramString)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.personaSuplencia = paramString;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.personaSuplencia = paramString;
      return;
    }
    localStateManager.setStringField(paramSuplencia, jdoInheritedFieldCount + 8, paramSuplencia.personaSuplencia, paramString);
  }

  private static final Personal jdoGetpersonal(Suplencia paramSuplencia)
  {
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.personal;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 9))
      return paramSuplencia.personal;
    return (Personal)localStateManager.getObjectField(paramSuplencia, jdoInheritedFieldCount + 9, paramSuplencia.personal);
  }

  private static final void jdoSetpersonal(Suplencia paramSuplencia, Personal paramPersonal)
  {
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramSuplencia, jdoInheritedFieldCount + 9, paramSuplencia.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Suplencia paramSuplencia)
  {
    if (paramSuplencia.jdoFlags <= 0)
      return paramSuplencia.tiempoSitp;
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
      return paramSuplencia.tiempoSitp;
    if (localStateManager.isLoaded(paramSuplencia, jdoInheritedFieldCount + 10))
      return paramSuplencia.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramSuplencia, jdoInheritedFieldCount + 10, paramSuplencia.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Suplencia paramSuplencia, Date paramDate)
  {
    if (paramSuplencia.jdoFlags == 0)
    {
      paramSuplencia.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramSuplencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSuplencia.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSuplencia, jdoInheritedFieldCount + 10, paramSuplencia.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}