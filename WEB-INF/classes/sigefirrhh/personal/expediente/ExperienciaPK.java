package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ExperienciaPK
  implements Serializable
{
  public long idExperiencia;

  public ExperienciaPK()
  {
  }

  public ExperienciaPK(long idExperiencia)
  {
    this.idExperiencia = idExperiencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ExperienciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ExperienciaPK thatPK)
  {
    return 
      this.idExperiencia == thatPK.idExperiencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idExperiencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idExperiencia);
  }
}