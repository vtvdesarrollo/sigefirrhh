package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PublicacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPublicacion(Publicacion publicacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Publicacion publicacionNew = 
      (Publicacion)BeanUtils.cloneBean(
      publicacion);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (publicacionNew.getPersonal() != null) {
      publicacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        publicacionNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(publicacionNew);
  }

  public void updatePublicacion(Publicacion publicacion) throws Exception
  {
    Publicacion publicacionModify = 
      findPublicacionById(publicacion.getIdPublicacion());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (publicacion.getPersonal() != null) {
      publicacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        publicacion.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(publicacionModify, publicacion);
  }

  public void deletePublicacion(Publicacion publicacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Publicacion publicacionDelete = 
      findPublicacionById(publicacion.getIdPublicacion());
    pm.deletePersistent(publicacionDelete);
  }

  public Publicacion findPublicacionById(long idPublicacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPublicacion == pIdPublicacion";
    Query query = pm.newQuery(Publicacion.class, filter);

    query.declareParameters("long pIdPublicacion");

    parameters.put("pIdPublicacion", new Long(idPublicacion));

    Collection colPublicacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPublicacion.iterator();
    return (Publicacion)iterator.next();
  }

  public Collection findPublicacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent publicacionExtent = pm.getExtent(
      Publicacion.class, true);
    Query query = pm.newQuery(publicacionExtent);
    query.setOrdering("anioPublicacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Publicacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anioPublicacion ascending");

    Collection colPublicacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPublicacion);

    return colPublicacion;
  }
}