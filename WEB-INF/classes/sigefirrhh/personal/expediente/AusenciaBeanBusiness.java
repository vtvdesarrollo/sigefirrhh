package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoAusencia;
import sigefirrhh.base.personal.TipoAusenciaBeanBusiness;

public class AusenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAusencia(Ausencia ausencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Ausencia ausenciaNew = 
      (Ausencia)BeanUtils.cloneBean(
      ausencia);

    TipoAusenciaBeanBusiness tipoAusenciaBeanBusiness = new TipoAusenciaBeanBusiness();

    if (ausenciaNew.getTipoAusencia() != null) {
      ausenciaNew.setTipoAusencia(
        tipoAusenciaBeanBusiness.findTipoAusenciaById(
        ausenciaNew.getTipoAusencia().getIdTipoAusencia()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (ausenciaNew.getPersonal() != null) {
      ausenciaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        ausenciaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(ausenciaNew);
  }

  public void updateAusencia(Ausencia ausencia) throws Exception
  {
    Ausencia ausenciaModify = 
      findAusenciaById(ausencia.getIdAusencia());

    TipoAusenciaBeanBusiness tipoAusenciaBeanBusiness = new TipoAusenciaBeanBusiness();

    if (ausencia.getTipoAusencia() != null) {
      ausencia.setTipoAusencia(
        tipoAusenciaBeanBusiness.findTipoAusenciaById(
        ausencia.getTipoAusencia().getIdTipoAusencia()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (ausencia.getPersonal() != null) {
      ausencia.setPersonal(
        personalBeanBusiness.findPersonalById(
        ausencia.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(ausenciaModify, ausencia);
  }

  public void deleteAusencia(Ausencia ausencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Ausencia ausenciaDelete = 
      findAusenciaById(ausencia.getIdAusencia());
    pm.deletePersistent(ausenciaDelete);
  }

  public Ausencia findAusenciaById(long idAusencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAusencia == pIdAusencia";
    Query query = pm.newQuery(Ausencia.class, filter);

    query.declareParameters("long pIdAusencia");

    parameters.put("pIdAusencia", new Long(idAusencia));

    Collection colAusencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAusencia.iterator();
    return (Ausencia)iterator.next();
  }

  public Collection findAusenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent ausenciaExtent = pm.getExtent(
      Ausencia.class, true);
    Query query = pm.newQuery(ausenciaExtent);
    query.setOrdering("tipoAusencia.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Ausencia.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("tipoAusencia.descripcion ascending");

    Collection colAusencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAusencia);

    return colAusencia;
  }

  public Collection findByCedulaDirector(int cedulaDirector)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cedulaDirector == pCedulaDirector";

    Query query = pm.newQuery(Ausencia.class, filter);

    query.declareParameters("int pCedulaDirector");
    HashMap parameters = new HashMap();

    parameters.put("pCedulaDirector", new Integer(cedulaDirector));

    query.setOrdering("tipoAusencia.descripcion ascending");

    Collection colAusencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAusencia);

    return colAusencia;
  }
}