package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class AveriguacionPK
  implements Serializable
{
  public long idAveriguacion;

  public AveriguacionPK()
  {
  }

  public AveriguacionPK(long idAveriguacion)
  {
    this.idAveriguacion = idAveriguacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AveriguacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AveriguacionPK thatPK)
  {
    return 
      this.idAveriguacion == thatPK.idAveriguacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAveriguacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAveriguacion);
  }
}