package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class PersonalOrganismo
  implements Serializable, PersistenceCapable
{
  private long idPersonalOrganismo;
  private Personal personal;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idPersonalOrganismo", "organismo", "personal" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public long getIdPersonalOrganismo()
  {
    return jdoGetidPersonalOrganismo(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setIdPersonalOrganismo(long l)
  {
    jdoSetidPersonalOrganismo(this, l);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.PersonalOrganismo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PersonalOrganismo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PersonalOrganismo localPersonalOrganismo = new PersonalOrganismo();
    localPersonalOrganismo.jdoFlags = 1;
    localPersonalOrganismo.jdoStateManager = paramStateManager;
    return localPersonalOrganismo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PersonalOrganismo localPersonalOrganismo = new PersonalOrganismo();
    localPersonalOrganismo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPersonalOrganismo.jdoFlags = 1;
    localPersonalOrganismo.jdoStateManager = paramStateManager;
    return localPersonalOrganismo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPersonalOrganismo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPersonalOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PersonalOrganismo paramPersonalOrganismo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPersonalOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idPersonalOrganismo = paramPersonalOrganismo.idPersonalOrganismo;
      return;
    case 1:
      if (paramPersonalOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramPersonalOrganismo.organismo;
      return;
    case 2:
      if (paramPersonalOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramPersonalOrganismo.personal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PersonalOrganismo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PersonalOrganismo localPersonalOrganismo = (PersonalOrganismo)paramObject;
    if (localPersonalOrganismo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPersonalOrganismo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PersonalOrganismoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PersonalOrganismoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalOrganismoPK))
      throw new IllegalArgumentException("arg1");
    PersonalOrganismoPK localPersonalOrganismoPK = (PersonalOrganismoPK)paramObject;
    localPersonalOrganismoPK.idPersonalOrganismo = this.idPersonalOrganismo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalOrganismoPK))
      throw new IllegalArgumentException("arg1");
    PersonalOrganismoPK localPersonalOrganismoPK = (PersonalOrganismoPK)paramObject;
    this.idPersonalOrganismo = localPersonalOrganismoPK.idPersonalOrganismo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalOrganismoPK))
      throw new IllegalArgumentException("arg2");
    PersonalOrganismoPK localPersonalOrganismoPK = (PersonalOrganismoPK)paramObject;
    localPersonalOrganismoPK.idPersonalOrganismo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalOrganismoPK))
      throw new IllegalArgumentException("arg2");
    PersonalOrganismoPK localPersonalOrganismoPK = (PersonalOrganismoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localPersonalOrganismoPK.idPersonalOrganismo);
  }

  private static final long jdoGetidPersonalOrganismo(PersonalOrganismo paramPersonalOrganismo)
  {
    return paramPersonalOrganismo.idPersonalOrganismo;
  }

  private static final void jdoSetidPersonalOrganismo(PersonalOrganismo paramPersonalOrganismo, long paramLong)
  {
    StateManager localStateManager = paramPersonalOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalOrganismo.idPersonalOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramPersonalOrganismo, jdoInheritedFieldCount + 0, paramPersonalOrganismo.idPersonalOrganismo, paramLong);
  }

  private static final Organismo jdoGetorganismo(PersonalOrganismo paramPersonalOrganismo)
  {
    StateManager localStateManager = paramPersonalOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramPersonalOrganismo.organismo;
    if (localStateManager.isLoaded(paramPersonalOrganismo, jdoInheritedFieldCount + 1))
      return paramPersonalOrganismo.organismo;
    return (Organismo)localStateManager.getObjectField(paramPersonalOrganismo, jdoInheritedFieldCount + 1, paramPersonalOrganismo.organismo);
  }

  private static final void jdoSetorganismo(PersonalOrganismo paramPersonalOrganismo, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramPersonalOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalOrganismo.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramPersonalOrganismo, jdoInheritedFieldCount + 1, paramPersonalOrganismo.organismo, paramOrganismo);
  }

  private static final Personal jdoGetpersonal(PersonalOrganismo paramPersonalOrganismo)
  {
    StateManager localStateManager = paramPersonalOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramPersonalOrganismo.personal;
    if (localStateManager.isLoaded(paramPersonalOrganismo, jdoInheritedFieldCount + 2))
      return paramPersonalOrganismo.personal;
    return (Personal)localStateManager.getObjectField(paramPersonalOrganismo, jdoInheritedFieldCount + 2, paramPersonalOrganismo.personal);
  }

  private static final void jdoSetpersonal(PersonalOrganismo paramPersonalOrganismo, Personal paramPersonal)
  {
    StateManager localStateManager = paramPersonalOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalOrganismo.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramPersonalOrganismo, jdoInheritedFieldCount + 2, paramPersonalOrganismo.personal, paramPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}