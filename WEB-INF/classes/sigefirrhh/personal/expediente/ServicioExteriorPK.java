package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ServicioExteriorPK
  implements Serializable
{
  public long idServicioExterior;

  public ServicioExteriorPK()
  {
  }

  public ServicioExteriorPK(long idServicioExterior)
  {
    this.idServicioExterior = idServicioExterior;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ServicioExteriorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ServicioExteriorPK thatPK)
  {
    return 
      this.idServicioExterior == thatPK.idServicioExterior;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idServicioExterior)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idServicioExterior);
  }
}