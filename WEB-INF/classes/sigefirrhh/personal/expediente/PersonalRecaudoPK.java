package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class PersonalRecaudoPK
  implements Serializable
{
  public long idPersonalRecaudo;

  public PersonalRecaudoPK()
  {
  }

  public PersonalRecaudoPK(long idPersonalRecaudo)
  {
    this.idPersonalRecaudo = idPersonalRecaudo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PersonalRecaudoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PersonalRecaudoPK thatPK)
  {
    return 
      this.idPersonalRecaudo == thatPK.idPersonalRecaudo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPersonalRecaudo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPersonalRecaudo);
  }
}