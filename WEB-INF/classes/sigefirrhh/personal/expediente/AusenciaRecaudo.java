package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.RelacionRecaudo;

public class AusenciaRecaudo
  implements Serializable, PersistenceCapable
{
  private long idAusenciaRecaudo;
  private Date fechaRecaudo;
  private RelacionRecaudo relacionRecaudo;
  private Ausencia ausencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ausencia", "fechaRecaudo", "idAusenciaRecaudo", "relacionRecaudo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.expediente.Ausencia"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.RelacionRecaudo") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetausencia(this).getTipoAusencia() + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetausencia(this).getFechaInicio()) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetausencia(this).getFechaFin()) + " " + 
      jdoGetrelacionRecaudo(this).getRecaudo() + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaRecaudo(this));
  }

  public Ausencia getAusencia()
  {
    return jdoGetausencia(this);
  }

  public Date getFechaRecaudo()
  {
    return jdoGetfechaRecaudo(this);
  }

  public long getIdAusenciaRecaudo()
  {
    return jdoGetidAusenciaRecaudo(this);
  }

  public RelacionRecaudo getRelacionRecaudo()
  {
    return jdoGetrelacionRecaudo(this);
  }

  public void setAusencia(Ausencia ausencia)
  {
    jdoSetausencia(this, ausencia);
  }

  public void setFechaRecaudo(Date date)
  {
    jdoSetfechaRecaudo(this, date);
  }

  public void setIdAusenciaRecaudo(long l)
  {
    jdoSetidAusenciaRecaudo(this, l);
  }

  public void setRelacionRecaudo(RelacionRecaudo recaudo)
  {
    jdoSetrelacionRecaudo(this, recaudo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.AusenciaRecaudo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AusenciaRecaudo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AusenciaRecaudo localAusenciaRecaudo = new AusenciaRecaudo();
    localAusenciaRecaudo.jdoFlags = 1;
    localAusenciaRecaudo.jdoStateManager = paramStateManager;
    return localAusenciaRecaudo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AusenciaRecaudo localAusenciaRecaudo = new AusenciaRecaudo();
    localAusenciaRecaudo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAusenciaRecaudo.jdoFlags = 1;
    localAusenciaRecaudo.jdoStateManager = paramStateManager;
    return localAusenciaRecaudo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ausencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRecaudo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAusenciaRecaudo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.relacionRecaudo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ausencia = ((Ausencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRecaudo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAusenciaRecaudo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.relacionRecaudo = ((RelacionRecaudo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AusenciaRecaudo paramAusenciaRecaudo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAusenciaRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.ausencia = paramAusenciaRecaudo.ausencia;
      return;
    case 1:
      if (paramAusenciaRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRecaudo = paramAusenciaRecaudo.fechaRecaudo;
      return;
    case 2:
      if (paramAusenciaRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.idAusenciaRecaudo = paramAusenciaRecaudo.idAusenciaRecaudo;
      return;
    case 3:
      if (paramAusenciaRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.relacionRecaudo = paramAusenciaRecaudo.relacionRecaudo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AusenciaRecaudo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AusenciaRecaudo localAusenciaRecaudo = (AusenciaRecaudo)paramObject;
    if (localAusenciaRecaudo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAusenciaRecaudo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AusenciaRecaudoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AusenciaRecaudoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AusenciaRecaudoPK))
      throw new IllegalArgumentException("arg1");
    AusenciaRecaudoPK localAusenciaRecaudoPK = (AusenciaRecaudoPK)paramObject;
    localAusenciaRecaudoPK.idAusenciaRecaudo = this.idAusenciaRecaudo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AusenciaRecaudoPK))
      throw new IllegalArgumentException("arg1");
    AusenciaRecaudoPK localAusenciaRecaudoPK = (AusenciaRecaudoPK)paramObject;
    this.idAusenciaRecaudo = localAusenciaRecaudoPK.idAusenciaRecaudo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AusenciaRecaudoPK))
      throw new IllegalArgumentException("arg2");
    AusenciaRecaudoPK localAusenciaRecaudoPK = (AusenciaRecaudoPK)paramObject;
    localAusenciaRecaudoPK.idAusenciaRecaudo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AusenciaRecaudoPK))
      throw new IllegalArgumentException("arg2");
    AusenciaRecaudoPK localAusenciaRecaudoPK = (AusenciaRecaudoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAusenciaRecaudoPK.idAusenciaRecaudo);
  }

  private static final Ausencia jdoGetausencia(AusenciaRecaudo paramAusenciaRecaudo)
  {
    StateManager localStateManager = paramAusenciaRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramAusenciaRecaudo.ausencia;
    if (localStateManager.isLoaded(paramAusenciaRecaudo, jdoInheritedFieldCount + 0))
      return paramAusenciaRecaudo.ausencia;
    return (Ausencia)localStateManager.getObjectField(paramAusenciaRecaudo, jdoInheritedFieldCount + 0, paramAusenciaRecaudo.ausencia);
  }

  private static final void jdoSetausencia(AusenciaRecaudo paramAusenciaRecaudo, Ausencia paramAusencia)
  {
    StateManager localStateManager = paramAusenciaRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusenciaRecaudo.ausencia = paramAusencia;
      return;
    }
    localStateManager.setObjectField(paramAusenciaRecaudo, jdoInheritedFieldCount + 0, paramAusenciaRecaudo.ausencia, paramAusencia);
  }

  private static final Date jdoGetfechaRecaudo(AusenciaRecaudo paramAusenciaRecaudo)
  {
    if (paramAusenciaRecaudo.jdoFlags <= 0)
      return paramAusenciaRecaudo.fechaRecaudo;
    StateManager localStateManager = paramAusenciaRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramAusenciaRecaudo.fechaRecaudo;
    if (localStateManager.isLoaded(paramAusenciaRecaudo, jdoInheritedFieldCount + 1))
      return paramAusenciaRecaudo.fechaRecaudo;
    return (Date)localStateManager.getObjectField(paramAusenciaRecaudo, jdoInheritedFieldCount + 1, paramAusenciaRecaudo.fechaRecaudo);
  }

  private static final void jdoSetfechaRecaudo(AusenciaRecaudo paramAusenciaRecaudo, Date paramDate)
  {
    if (paramAusenciaRecaudo.jdoFlags == 0)
    {
      paramAusenciaRecaudo.fechaRecaudo = paramDate;
      return;
    }
    StateManager localStateManager = paramAusenciaRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusenciaRecaudo.fechaRecaudo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAusenciaRecaudo, jdoInheritedFieldCount + 1, paramAusenciaRecaudo.fechaRecaudo, paramDate);
  }

  private static final long jdoGetidAusenciaRecaudo(AusenciaRecaudo paramAusenciaRecaudo)
  {
    return paramAusenciaRecaudo.idAusenciaRecaudo;
  }

  private static final void jdoSetidAusenciaRecaudo(AusenciaRecaudo paramAusenciaRecaudo, long paramLong)
  {
    StateManager localStateManager = paramAusenciaRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusenciaRecaudo.idAusenciaRecaudo = paramLong;
      return;
    }
    localStateManager.setLongField(paramAusenciaRecaudo, jdoInheritedFieldCount + 2, paramAusenciaRecaudo.idAusenciaRecaudo, paramLong);
  }

  private static final RelacionRecaudo jdoGetrelacionRecaudo(AusenciaRecaudo paramAusenciaRecaudo)
  {
    StateManager localStateManager = paramAusenciaRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramAusenciaRecaudo.relacionRecaudo;
    if (localStateManager.isLoaded(paramAusenciaRecaudo, jdoInheritedFieldCount + 3))
      return paramAusenciaRecaudo.relacionRecaudo;
    return (RelacionRecaudo)localStateManager.getObjectField(paramAusenciaRecaudo, jdoInheritedFieldCount + 3, paramAusenciaRecaudo.relacionRecaudo);
  }

  private static final void jdoSetrelacionRecaudo(AusenciaRecaudo paramAusenciaRecaudo, RelacionRecaudo paramRelacionRecaudo)
  {
    StateManager localStateManager = paramAusenciaRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAusenciaRecaudo.relacionRecaudo = paramRelacionRecaudo;
      return;
    }
    localStateManager.setObjectField(paramAusenciaRecaudo, jdoInheritedFieldCount + 3, paramAusenciaRecaudo.relacionRecaudo, paramRelacionRecaudo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}