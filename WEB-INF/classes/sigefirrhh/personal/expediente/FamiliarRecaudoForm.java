package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.RelacionRecaudo;
import sigefirrhh.login.LoginSession;

public class FamiliarRecaudoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(FamiliarRecaudoForm.class.getName());
  private FamiliarRecaudo familiarRecaudo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private PersonalFacade personalFacade = new PersonalFacade();
  private boolean showFamiliarRecaudoByFamiliar;
  private String findSelectFamiliar;
  private Collection findColFamiliar;
  private Collection colRelacionRecaudo;
  private Collection colFamiliar;
  private String selectRelacionRecaudo;
  private String selectFamiliar;
  private Object stateResultFamiliarRecaudoByFamiliar = null;

  public String getFindSelectFamiliar()
  {
    return this.findSelectFamiliar;
  }
  public void setFindSelectFamiliar(String valFamiliar) {
    this.findSelectFamiliar = valFamiliar;
  }

  public Collection getFindColFamiliar() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColFamiliar.iterator();
    Familiar familiar = null;
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      col.add(new SelectItem(
        String.valueOf(familiar.getIdFamiliar()), 
        familiar.toString()));
    }
    return col;
  }

  public String getSelectRelacionRecaudo()
  {
    return this.selectRelacionRecaudo;
  }
  public void setSelectRelacionRecaudo(String valRelacionRecaudo) {
    Iterator iterator = this.colRelacionRecaudo.iterator();
    RelacionRecaudo relacionRecaudo = null;
    this.familiarRecaudo.setRelacionRecaudo(null);
    while (iterator.hasNext()) {
      relacionRecaudo = (RelacionRecaudo)iterator.next();
      if (String.valueOf(relacionRecaudo.getIdRelacionRecaudo()).equals(
        valRelacionRecaudo)) {
        this.familiarRecaudo.setRelacionRecaudo(
          relacionRecaudo);
        break;
      }
    }
    this.selectRelacionRecaudo = valRelacionRecaudo;
  }
  public String getSelectFamiliar() {
    return this.selectFamiliar;
  }
  public void setSelectFamiliar(String valFamiliar) {
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    this.familiarRecaudo.setFamiliar(null);
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      if (String.valueOf(familiar.getIdFamiliar()).equals(
        valFamiliar)) {
        this.familiarRecaudo.setFamiliar(
          familiar);
        break;
      }
    }
    this.selectFamiliar = valFamiliar;
  }
  public Collection getResult() {
    return this.result;
  }

  public FamiliarRecaudo getFamiliarRecaudo() {
    if (this.familiarRecaudo == null) {
      this.familiarRecaudo = new FamiliarRecaudo();
    }
    return this.familiarRecaudo;
  }

  public FamiliarRecaudoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRelacionRecaudo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRelacionRecaudo.iterator();
    RelacionRecaudo relacionRecaudo = null;
    while (iterator.hasNext()) {
      relacionRecaudo = (RelacionRecaudo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(relacionRecaudo.getIdRelacionRecaudo()), 
        relacionRecaudo.toString()));
    }
    return col;
  }

  public Collection getColFamiliar()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      col.add(new SelectItem(
        String.valueOf(familiar.getIdFamiliar()), 
        familiar.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColFamiliar = 
        this.expedienteFacade.findAllFamiliar();

      this.colRelacionRecaudo = 
        this.personalFacade.findAllRelacionRecaudo();
      this.colFamiliar = 
        this.expedienteFacade.findAllFamiliar();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findFamiliarRecaudoByFamiliar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.expedienteFacade.findFamiliarRecaudoByFamiliar(Long.valueOf(this.findSelectFamiliar).longValue());
      this.showFamiliarRecaudoByFamiliar = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showFamiliarRecaudoByFamiliar)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectFamiliar = null;

    return null;
  }

  public boolean isShowFamiliarRecaudoByFamiliar() {
    return this.showFamiliarRecaudoByFamiliar;
  }

  public String selectFamiliarRecaudo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRelacionRecaudo = null;
    this.selectFamiliar = null;

    long idFamiliarRecaudo = 
      Long.parseLong((String)requestParameterMap.get("idFamiliarRecaudo"));
    try
    {
      this.familiarRecaudo = 
        this.expedienteFacade.findFamiliarRecaudoById(
        idFamiliarRecaudo);
      if (this.familiarRecaudo.getRelacionRecaudo() != null) {
        this.selectRelacionRecaudo = 
          String.valueOf(this.familiarRecaudo.getRelacionRecaudo().getIdRelacionRecaudo());
      }
      if (this.familiarRecaudo.getFamiliar() != null) {
        this.selectFamiliar = 
          String.valueOf(this.familiarRecaudo.getFamiliar().getIdFamiliar());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.familiarRecaudo = null;
    this.showFamiliarRecaudoByFamiliar = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.familiarRecaudo.getFechaRecaudo() != null) && 
      (this.familiarRecaudo.getFechaRecaudo().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.expedienteFacade.addFamiliarRecaudo(
          this.familiarRecaudo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateFamiliarRecaudo(
          this.familiarRecaudo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteFamiliarRecaudo(
        this.familiarRecaudo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.familiarRecaudo = new FamiliarRecaudo();

    this.selectRelacionRecaudo = null;

    this.selectFamiliar = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.familiarRecaudo.setIdFamiliarRecaudo(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.FamiliarRecaudo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.familiarRecaudo = new FamiliarRecaudo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}