package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Gremio;

public class Afiliacion
  implements Serializable, PersistenceCapable
{
  private long idAfiliacion;
  private Gremio gremio;
  private Date fecha;
  private String numeroAfiliacion;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fecha", "gremio", "idAfiliacion", "idSitp", "numeroAfiliacion", "personal", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.Gremio"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetgremio(this).getNombre() + " " + 
      jdoGetnumeroAfiliacion(this);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public Gremio getGremio()
  {
    return jdoGetgremio(this);
  }

  public long getIdAfiliacion()
  {
    return jdoGetidAfiliacion(this);
  }

  public String getNumeroAfiliacion()
  {
    return jdoGetnumeroAfiliacion(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setFecha(Date date)
  {
    jdoSetfecha(this, date);
  }

  public void setGremio(Gremio gremio)
  {
    jdoSetgremio(this, gremio);
  }

  public void setIdAfiliacion(long l)
  {
    jdoSetidAfiliacion(this, l);
  }

  public void setNumeroAfiliacion(String string)
  {
    jdoSetnumeroAfiliacion(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Afiliacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Afiliacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Afiliacion localAfiliacion = new Afiliacion();
    localAfiliacion.jdoFlags = 1;
    localAfiliacion.jdoStateManager = paramStateManager;
    return localAfiliacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Afiliacion localAfiliacion = new Afiliacion();
    localAfiliacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAfiliacion.jdoFlags = 1;
    localAfiliacion.jdoStateManager = paramStateManager;
    return localAfiliacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.gremio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAfiliacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroAfiliacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gremio = ((Gremio)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAfiliacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroAfiliacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Afiliacion paramAfiliacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramAfiliacion.fecha;
      return;
    case 1:
      if (paramAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.gremio = paramAfiliacion.gremio;
      return;
    case 2:
      if (paramAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.idAfiliacion = paramAfiliacion.idAfiliacion;
      return;
    case 3:
      if (paramAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramAfiliacion.idSitp;
      return;
    case 4:
      if (paramAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.numeroAfiliacion = paramAfiliacion.numeroAfiliacion;
      return;
    case 5:
      if (paramAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramAfiliacion.personal;
      return;
    case 6:
      if (paramAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramAfiliacion.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Afiliacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Afiliacion localAfiliacion = (Afiliacion)paramObject;
    if (localAfiliacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAfiliacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AfiliacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AfiliacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AfiliacionPK))
      throw new IllegalArgumentException("arg1");
    AfiliacionPK localAfiliacionPK = (AfiliacionPK)paramObject;
    localAfiliacionPK.idAfiliacion = this.idAfiliacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AfiliacionPK))
      throw new IllegalArgumentException("arg1");
    AfiliacionPK localAfiliacionPK = (AfiliacionPK)paramObject;
    this.idAfiliacion = localAfiliacionPK.idAfiliacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AfiliacionPK))
      throw new IllegalArgumentException("arg2");
    AfiliacionPK localAfiliacionPK = (AfiliacionPK)paramObject;
    localAfiliacionPK.idAfiliacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AfiliacionPK))
      throw new IllegalArgumentException("arg2");
    AfiliacionPK localAfiliacionPK = (AfiliacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAfiliacionPK.idAfiliacion);
  }

  private static final Date jdoGetfecha(Afiliacion paramAfiliacion)
  {
    if (paramAfiliacion.jdoFlags <= 0)
      return paramAfiliacion.fecha;
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramAfiliacion.fecha;
    if (localStateManager.isLoaded(paramAfiliacion, jdoInheritedFieldCount + 0))
      return paramAfiliacion.fecha;
    return (Date)localStateManager.getObjectField(paramAfiliacion, jdoInheritedFieldCount + 0, paramAfiliacion.fecha);
  }

  private static final void jdoSetfecha(Afiliacion paramAfiliacion, Date paramDate)
  {
    if (paramAfiliacion.jdoFlags == 0)
    {
      paramAfiliacion.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAfiliacion.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAfiliacion, jdoInheritedFieldCount + 0, paramAfiliacion.fecha, paramDate);
  }

  private static final Gremio jdoGetgremio(Afiliacion paramAfiliacion)
  {
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramAfiliacion.gremio;
    if (localStateManager.isLoaded(paramAfiliacion, jdoInheritedFieldCount + 1))
      return paramAfiliacion.gremio;
    return (Gremio)localStateManager.getObjectField(paramAfiliacion, jdoInheritedFieldCount + 1, paramAfiliacion.gremio);
  }

  private static final void jdoSetgremio(Afiliacion paramAfiliacion, Gremio paramGremio)
  {
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAfiliacion.gremio = paramGremio;
      return;
    }
    localStateManager.setObjectField(paramAfiliacion, jdoInheritedFieldCount + 1, paramAfiliacion.gremio, paramGremio);
  }

  private static final long jdoGetidAfiliacion(Afiliacion paramAfiliacion)
  {
    return paramAfiliacion.idAfiliacion;
  }

  private static final void jdoSetidAfiliacion(Afiliacion paramAfiliacion, long paramLong)
  {
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAfiliacion.idAfiliacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramAfiliacion, jdoInheritedFieldCount + 2, paramAfiliacion.idAfiliacion, paramLong);
  }

  private static final int jdoGetidSitp(Afiliacion paramAfiliacion)
  {
    if (paramAfiliacion.jdoFlags <= 0)
      return paramAfiliacion.idSitp;
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramAfiliacion.idSitp;
    if (localStateManager.isLoaded(paramAfiliacion, jdoInheritedFieldCount + 3))
      return paramAfiliacion.idSitp;
    return localStateManager.getIntField(paramAfiliacion, jdoInheritedFieldCount + 3, paramAfiliacion.idSitp);
  }

  private static final void jdoSetidSitp(Afiliacion paramAfiliacion, int paramInt)
  {
    if (paramAfiliacion.jdoFlags == 0)
    {
      paramAfiliacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAfiliacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramAfiliacion, jdoInheritedFieldCount + 3, paramAfiliacion.idSitp, paramInt);
  }

  private static final String jdoGetnumeroAfiliacion(Afiliacion paramAfiliacion)
  {
    if (paramAfiliacion.jdoFlags <= 0)
      return paramAfiliacion.numeroAfiliacion;
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramAfiliacion.numeroAfiliacion;
    if (localStateManager.isLoaded(paramAfiliacion, jdoInheritedFieldCount + 4))
      return paramAfiliacion.numeroAfiliacion;
    return localStateManager.getStringField(paramAfiliacion, jdoInheritedFieldCount + 4, paramAfiliacion.numeroAfiliacion);
  }

  private static final void jdoSetnumeroAfiliacion(Afiliacion paramAfiliacion, String paramString)
  {
    if (paramAfiliacion.jdoFlags == 0)
    {
      paramAfiliacion.numeroAfiliacion = paramString;
      return;
    }
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAfiliacion.numeroAfiliacion = paramString;
      return;
    }
    localStateManager.setStringField(paramAfiliacion, jdoInheritedFieldCount + 4, paramAfiliacion.numeroAfiliacion, paramString);
  }

  private static final Personal jdoGetpersonal(Afiliacion paramAfiliacion)
  {
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramAfiliacion.personal;
    if (localStateManager.isLoaded(paramAfiliacion, jdoInheritedFieldCount + 5))
      return paramAfiliacion.personal;
    return (Personal)localStateManager.getObjectField(paramAfiliacion, jdoInheritedFieldCount + 5, paramAfiliacion.personal);
  }

  private static final void jdoSetpersonal(Afiliacion paramAfiliacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAfiliacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramAfiliacion, jdoInheritedFieldCount + 5, paramAfiliacion.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Afiliacion paramAfiliacion)
  {
    if (paramAfiliacion.jdoFlags <= 0)
      return paramAfiliacion.tiempoSitp;
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramAfiliacion.tiempoSitp;
    if (localStateManager.isLoaded(paramAfiliacion, jdoInheritedFieldCount + 6))
      return paramAfiliacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramAfiliacion, jdoInheritedFieldCount + 6, paramAfiliacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Afiliacion paramAfiliacion, Date paramDate)
  {
    if (paramAfiliacion.jdoFlags == 0)
    {
      paramAfiliacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAfiliacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAfiliacion, jdoInheritedFieldCount + 6, paramAfiliacion.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}