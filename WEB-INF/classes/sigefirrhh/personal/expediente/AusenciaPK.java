package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class AusenciaPK
  implements Serializable
{
  public long idAusencia;

  public AusenciaPK()
  {
  }

  public AusenciaPK(long idAusencia)
  {
    this.idAusencia = idAusencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AusenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AusenciaPK thatPK)
  {
    return 
      this.idAusencia == thatPK.idAusencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAusencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAusencia);
  }
}