package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.SubtipoCredencial;
import sigefirrhh.base.bienestar.SubtipoCredencialBeanBusiness;

public class CredencialBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCredencial(Credencial credencial)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Credencial credencialNew = 
      (Credencial)BeanUtils.cloneBean(
      credencial);

    SubtipoCredencialBeanBusiness subtipoCredencialBeanBusiness = new SubtipoCredencialBeanBusiness();

    if (credencialNew.getSubTipoCredencial() != null) {
      credencialNew.setSubTipoCredencial(
        subtipoCredencialBeanBusiness.findSubtipoCredencialById(
        credencialNew.getSubTipoCredencial().getIdSubtipoCredencial()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (credencialNew.getPersonal() != null) {
      credencialNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        credencialNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(credencialNew);
  }

  public void updateCredencial(Credencial credencial) throws Exception
  {
    Credencial credencialModify = 
      findCredencialById(credencial.getIdCredencial());

    SubtipoCredencialBeanBusiness subtipoCredencialBeanBusiness = new SubtipoCredencialBeanBusiness();

    if (credencial.getSubTipoCredencial() != null) {
      credencial.setSubTipoCredencial(
        subtipoCredencialBeanBusiness.findSubtipoCredencialById(
        credencial.getSubTipoCredencial().getIdSubtipoCredencial()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (credencial.getPersonal() != null) {
      credencial.setPersonal(
        personalBeanBusiness.findPersonalById(
        credencial.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(credencialModify, credencial);
  }

  public void deleteCredencial(Credencial credencial) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Credencial credencialDelete = 
      findCredencialById(credencial.getIdCredencial());
    pm.deletePersistent(credencialDelete);
  }

  public Credencial findCredencialById(long idCredencial) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCredencial == pIdCredencial";
    Query query = pm.newQuery(Credencial.class, filter);

    query.declareParameters("long pIdCredencial");

    parameters.put("pIdCredencial", new Long(idCredencial));

    Collection colCredencial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCredencial.iterator();
    return (Credencial)iterator.next();
  }

  public Collection findCredencialAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent credencialExtent = pm.getExtent(
      Credencial.class, true);
    Query query = pm.newQuery(credencialExtent);
    query.setOrdering("tipoCredencial.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Credencial.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaEntrega ascending");

    Collection colCredencial = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCredencial);

    return colCredencial;
  }
}