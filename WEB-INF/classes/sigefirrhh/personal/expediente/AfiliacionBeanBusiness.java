package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Gremio;
import sigefirrhh.base.personal.GremioBeanBusiness;

public class AfiliacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAfiliacion(Afiliacion afiliacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Afiliacion afiliacionNew = 
      (Afiliacion)BeanUtils.cloneBean(
      afiliacion);

    GremioBeanBusiness gremioBeanBusiness = new GremioBeanBusiness();

    if (afiliacionNew.getGremio() != null) {
      afiliacionNew.setGremio(
        gremioBeanBusiness.findGremioById(
        afiliacionNew.getGremio().getIdGremio()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (afiliacionNew.getPersonal() != null) {
      afiliacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        afiliacionNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(afiliacionNew);
  }

  public void updateAfiliacion(Afiliacion afiliacion) throws Exception
  {
    Afiliacion afiliacionModify = 
      findAfiliacionById(afiliacion.getIdAfiliacion());

    GremioBeanBusiness gremioBeanBusiness = new GremioBeanBusiness();

    if (afiliacion.getGremio() != null) {
      afiliacion.setGremio(
        gremioBeanBusiness.findGremioById(
        afiliacion.getGremio().getIdGremio()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (afiliacion.getPersonal() != null) {
      afiliacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        afiliacion.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(afiliacionModify, afiliacion);
  }

  public void deleteAfiliacion(Afiliacion afiliacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Afiliacion afiliacionDelete = 
      findAfiliacionById(afiliacion.getIdAfiliacion());
    pm.deletePersistent(afiliacionDelete);
  }

  public Afiliacion findAfiliacionById(long idAfiliacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAfiliacion == pIdAfiliacion";
    Query query = pm.newQuery(Afiliacion.class, filter);

    query.declareParameters("long pIdAfiliacion");

    parameters.put("pIdAfiliacion", new Long(idAfiliacion));

    Collection colAfiliacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAfiliacion.iterator();
    return (Afiliacion)iterator.next();
  }

  public Collection findAfiliacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent afiliacionExtent = pm.getExtent(
      Afiliacion.class, true);
    Query query = pm.newQuery(afiliacionExtent);
    query.setOrdering("gremio.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Afiliacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("gremio.nombre ascending");

    Collection colAfiliacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAfiliacion);

    return colAfiliacion;
  }
}