package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class DeclaracionPK
  implements Serializable
{
  public long idDeclaracion;

  public DeclaracionPK()
  {
  }

  public DeclaracionPK(long idDeclaracion)
  {
    this.idDeclaracion = idDeclaracion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DeclaracionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DeclaracionPK thatPK)
  {
    return 
      this.idDeclaracion == thatPK.idDeclaracion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDeclaracion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDeclaracion);
  }
}