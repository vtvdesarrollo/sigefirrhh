package sigefirrhh.personal.expediente;

import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AntecedenteAux
  implements PersistenceCapable
{
  private long id;
  private String organismo;
  private Date fechaDesde;
  private Date fechaHasta;
  private int anios;
  private int meses;
  private int dias;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anios", "dias", "fechaDesde", "fechaHasta", "id", "meses", "organismo" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public long getId()
  {
    return jdoGetid(this);
  }
  public void setId(long id) {
    jdoSetid(this, id);
  }
  public Date getFechaDesde() {
    return jdoGetfechaDesde(this);
  }
  public void setFechaDesde(Date fechaDesde) {
    jdoSetfechaDesde(this, fechaDesde);
  }
  public Date getFechaHasta() {
    return jdoGetfechaHasta(this);
  }
  public void setFechaHasta(Date fechaHasta) {
    jdoSetfechaHasta(this, fechaHasta);
  }
  public String getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(String organismo) {
    jdoSetorganismo(this, organismo);
  }
  public int getAnios() {
    return jdoGetanios(this);
  }
  public void setAnios(int anios) {
    jdoSetanios(this, anios);
  }
  public int getDias() {
    return jdoGetdias(this);
  }
  public void setDias(int dias) {
    jdoSetdias(this, dias);
  }
  public int getMeses() {
    return jdoGetmeses(this);
  }
  public void setMeses(int meses) {
    jdoSetmeses(this, meses);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.AntecedenteAux"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AntecedenteAux());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AntecedenteAux localAntecedenteAux = new AntecedenteAux();
    localAntecedenteAux.jdoFlags = 1;
    localAntecedenteAux.jdoStateManager = paramStateManager;
    return localAntecedenteAux;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AntecedenteAux localAntecedenteAux = new AntecedenteAux();
    localAntecedenteAux.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAntecedenteAux.jdoFlags = 1;
    localAntecedenteAux.jdoStateManager = paramStateManager;
    return localAntecedenteAux;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anios);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.dias);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaDesde);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaHasta);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.id);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.meses);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anios = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dias = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaDesde = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaHasta = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.id = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.meses = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AntecedenteAux paramAntecedenteAux, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAntecedenteAux == null)
        throw new IllegalArgumentException("arg1");
      this.anios = paramAntecedenteAux.anios;
      return;
    case 1:
      if (paramAntecedenteAux == null)
        throw new IllegalArgumentException("arg1");
      this.dias = paramAntecedenteAux.dias;
      return;
    case 2:
      if (paramAntecedenteAux == null)
        throw new IllegalArgumentException("arg1");
      this.fechaDesde = paramAntecedenteAux.fechaDesde;
      return;
    case 3:
      if (paramAntecedenteAux == null)
        throw new IllegalArgumentException("arg1");
      this.fechaHasta = paramAntecedenteAux.fechaHasta;
      return;
    case 4:
      if (paramAntecedenteAux == null)
        throw new IllegalArgumentException("arg1");
      this.id = paramAntecedenteAux.id;
      return;
    case 5:
      if (paramAntecedenteAux == null)
        throw new IllegalArgumentException("arg1");
      this.meses = paramAntecedenteAux.meses;
      return;
    case 6:
      if (paramAntecedenteAux == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramAntecedenteAux.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AntecedenteAux))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AntecedenteAux localAntecedenteAux = (AntecedenteAux)paramObject;
    if (localAntecedenteAux.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAntecedenteAux, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AntecedentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AntecedentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg1");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    localAntecedentePK.id = this.id;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg1");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    this.id = localAntecedentePK.id;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg2");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    localAntecedentePK.id = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AntecedentePK))
      throw new IllegalArgumentException("arg2");
    AntecedentePK localAntecedentePK = (AntecedentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localAntecedentePK.id);
  }

  private static final int jdoGetanios(AntecedenteAux paramAntecedenteAux)
  {
    if (paramAntecedenteAux.jdoFlags <= 0)
      return paramAntecedenteAux.anios;
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedenteAux.anios;
    if (localStateManager.isLoaded(paramAntecedenteAux, jdoInheritedFieldCount + 0))
      return paramAntecedenteAux.anios;
    return localStateManager.getIntField(paramAntecedenteAux, jdoInheritedFieldCount + 0, paramAntecedenteAux.anios);
  }

  private static final void jdoSetanios(AntecedenteAux paramAntecedenteAux, int paramInt)
  {
    if (paramAntecedenteAux.jdoFlags == 0)
    {
      paramAntecedenteAux.anios = paramInt;
      return;
    }
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedenteAux.anios = paramInt;
      return;
    }
    localStateManager.setIntField(paramAntecedenteAux, jdoInheritedFieldCount + 0, paramAntecedenteAux.anios, paramInt);
  }

  private static final int jdoGetdias(AntecedenteAux paramAntecedenteAux)
  {
    if (paramAntecedenteAux.jdoFlags <= 0)
      return paramAntecedenteAux.dias;
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedenteAux.dias;
    if (localStateManager.isLoaded(paramAntecedenteAux, jdoInheritedFieldCount + 1))
      return paramAntecedenteAux.dias;
    return localStateManager.getIntField(paramAntecedenteAux, jdoInheritedFieldCount + 1, paramAntecedenteAux.dias);
  }

  private static final void jdoSetdias(AntecedenteAux paramAntecedenteAux, int paramInt)
  {
    if (paramAntecedenteAux.jdoFlags == 0)
    {
      paramAntecedenteAux.dias = paramInt;
      return;
    }
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedenteAux.dias = paramInt;
      return;
    }
    localStateManager.setIntField(paramAntecedenteAux, jdoInheritedFieldCount + 1, paramAntecedenteAux.dias, paramInt);
  }

  private static final Date jdoGetfechaDesde(AntecedenteAux paramAntecedenteAux)
  {
    if (paramAntecedenteAux.jdoFlags <= 0)
      return paramAntecedenteAux.fechaDesde;
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedenteAux.fechaDesde;
    if (localStateManager.isLoaded(paramAntecedenteAux, jdoInheritedFieldCount + 2))
      return paramAntecedenteAux.fechaDesde;
    return (Date)localStateManager.getObjectField(paramAntecedenteAux, jdoInheritedFieldCount + 2, paramAntecedenteAux.fechaDesde);
  }

  private static final void jdoSetfechaDesde(AntecedenteAux paramAntecedenteAux, Date paramDate)
  {
    if (paramAntecedenteAux.jdoFlags == 0)
    {
      paramAntecedenteAux.fechaDesde = paramDate;
      return;
    }
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedenteAux.fechaDesde = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAntecedenteAux, jdoInheritedFieldCount + 2, paramAntecedenteAux.fechaDesde, paramDate);
  }

  private static final Date jdoGetfechaHasta(AntecedenteAux paramAntecedenteAux)
  {
    if (paramAntecedenteAux.jdoFlags <= 0)
      return paramAntecedenteAux.fechaHasta;
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedenteAux.fechaHasta;
    if (localStateManager.isLoaded(paramAntecedenteAux, jdoInheritedFieldCount + 3))
      return paramAntecedenteAux.fechaHasta;
    return (Date)localStateManager.getObjectField(paramAntecedenteAux, jdoInheritedFieldCount + 3, paramAntecedenteAux.fechaHasta);
  }

  private static final void jdoSetfechaHasta(AntecedenteAux paramAntecedenteAux, Date paramDate)
  {
    if (paramAntecedenteAux.jdoFlags == 0)
    {
      paramAntecedenteAux.fechaHasta = paramDate;
      return;
    }
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedenteAux.fechaHasta = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAntecedenteAux, jdoInheritedFieldCount + 3, paramAntecedenteAux.fechaHasta, paramDate);
  }

  private static final long jdoGetid(AntecedenteAux paramAntecedenteAux)
  {
    return paramAntecedenteAux.id;
  }

  private static final void jdoSetid(AntecedenteAux paramAntecedenteAux, long paramLong)
  {
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedenteAux.id = paramLong;
      return;
    }
    localStateManager.setLongField(paramAntecedenteAux, jdoInheritedFieldCount + 4, paramAntecedenteAux.id, paramLong);
  }

  private static final int jdoGetmeses(AntecedenteAux paramAntecedenteAux)
  {
    if (paramAntecedenteAux.jdoFlags <= 0)
      return paramAntecedenteAux.meses;
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedenteAux.meses;
    if (localStateManager.isLoaded(paramAntecedenteAux, jdoInheritedFieldCount + 5))
      return paramAntecedenteAux.meses;
    return localStateManager.getIntField(paramAntecedenteAux, jdoInheritedFieldCount + 5, paramAntecedenteAux.meses);
  }

  private static final void jdoSetmeses(AntecedenteAux paramAntecedenteAux, int paramInt)
  {
    if (paramAntecedenteAux.jdoFlags == 0)
    {
      paramAntecedenteAux.meses = paramInt;
      return;
    }
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedenteAux.meses = paramInt;
      return;
    }
    localStateManager.setIntField(paramAntecedenteAux, jdoInheritedFieldCount + 5, paramAntecedenteAux.meses, paramInt);
  }

  private static final String jdoGetorganismo(AntecedenteAux paramAntecedenteAux)
  {
    if (paramAntecedenteAux.jdoFlags <= 0)
      return paramAntecedenteAux.organismo;
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
      return paramAntecedenteAux.organismo;
    if (localStateManager.isLoaded(paramAntecedenteAux, jdoInheritedFieldCount + 6))
      return paramAntecedenteAux.organismo;
    return localStateManager.getStringField(paramAntecedenteAux, jdoInheritedFieldCount + 6, paramAntecedenteAux.organismo);
  }

  private static final void jdoSetorganismo(AntecedenteAux paramAntecedenteAux, String paramString)
  {
    if (paramAntecedenteAux.jdoFlags == 0)
    {
      paramAntecedenteAux.organismo = paramString;
      return;
    }
    StateManager localStateManager = paramAntecedenteAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramAntecedenteAux.organismo = paramString;
      return;
    }
    localStateManager.setStringField(paramAntecedenteAux, jdoInheritedFieldCount + 6, paramAntecedenteAux.organismo, paramString);
  }
}