package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class PersonalAuxPK
  implements Serializable
{
  public long id;

  public PersonalAuxPK()
  {
  }

  public PersonalAuxPK(long id)
  {
    this.id = id;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PersonalAuxPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PersonalAuxPK thatPK)
  {
    return 
      this.id == thatPK.id;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.id)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.id);
  }
}