package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class HistorialOrganismoPK
  implements Serializable
{
  public long idHistorialOrganismo;

  public HistorialOrganismoPK()
  {
  }

  public HistorialOrganismoPK(long idHistorialOrganismo)
  {
    this.idHistorialOrganismo = idHistorialOrganismo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistorialOrganismoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistorialOrganismoPK thatPK)
  {
    return 
      this.idHistorialOrganismo == thatPK.idHistorialOrganismo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistorialOrganismo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistorialOrganismo);
  }
}