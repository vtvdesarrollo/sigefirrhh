package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class Vacacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO_VACACION;
  protected static final Map LISTA_SI_NO;
  private long idVacacion;
  private TipoPersonal tipoPersonal;
  private String tipoVacacion;
  private int anio;
  private Date fechaInicio;
  private Date fechaFin;
  private Date fechaReintegro;
  private int diasDisfrute;
  private int diasPendientes;
  private String observaciones;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "diasDisfrute", "diasPendientes", "fechaFin", "fechaInicio", "fechaReintegro", "idVacacion", "observaciones", "personal", "tipoPersonal", "tipoVacacion" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 24, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Vacacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Vacacion());

    LISTA_TIPO_VACACION = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_TIPO_VACACION.put("P", "PENDIENTE(DERECHO)");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public Vacacion()
  {
    jdoSettipoVacacion(this, "P");

    jdoSetanio(this, 0);

    jdoSetdiasDisfrute(this, 0);

    jdoSetdiasPendientes(this, 0);
  }

  public String toString()
  {
    return jdoGetanio(this) + " " + 
      LISTA_TIPO_VACACION.get(jdoGettipoVacacion(this)) + " " + (getDiasPendientes() - getDiasDisfrute());
  }

  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public int getDiasDisfrute() {
    return jdoGetdiasDisfrute(this);
  }
  public void setDiasDisfrute(int diasDisfrute) {
    jdoSetdiasDisfrute(this, diasDisfrute);
  }
  public int getDiasPendientes() {
    return jdoGetdiasPendientes(this);
  }
  public void setDiasPendientes(int diasPendientes) {
    jdoSetdiasPendientes(this, diasPendientes);
  }
  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }
  public void setFechaFin(Date fechaFin) {
    jdoSetfechaFin(this, fechaFin);
  }
  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }
  public void setFechaInicio(Date fechaInicio) {
    jdoSetfechaInicio(this, fechaInicio);
  }
  public Date getFechaReintegro() {
    return jdoGetfechaReintegro(this);
  }
  public void setFechaReintegro(Date fechaReintegro) {
    jdoSetfechaReintegro(this, fechaReintegro);
  }

  public long getIdVacacion()
  {
    return jdoGetidVacacion(this);
  }
  public void setIdVacacion(long idVacacion) {
    jdoSetidVacacion(this, idVacacion);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public String getTipoVacacion() {
    return jdoGettipoVacacion(this);
  }
  public void setTipoVacacion(String tipoVacacion) {
    jdoSettipoVacacion(this, tipoVacacion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Vacacion localVacacion = new Vacacion();
    localVacacion.jdoFlags = 1;
    localVacacion.jdoStateManager = paramStateManager;
    return localVacacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Vacacion localVacacion = new Vacacion();
    localVacacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localVacacion.jdoFlags = 1;
    localVacacion.jdoStateManager = paramStateManager;
    return localVacacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasDisfrute);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasPendientes);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaReintegro);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idVacacion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoVacacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasDisfrute = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasPendientes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaReintegro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idVacacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoVacacion = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Vacacion paramVacacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramVacacion.anio;
      return;
    case 1:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.diasDisfrute = paramVacacion.diasDisfrute;
      return;
    case 2:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.diasPendientes = paramVacacion.diasPendientes;
      return;
    case 3:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramVacacion.fechaFin;
      return;
    case 4:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramVacacion.fechaInicio;
      return;
    case 5:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaReintegro = paramVacacion.fechaReintegro;
      return;
    case 6:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.idVacacion = paramVacacion.idVacacion;
      return;
    case 7:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramVacacion.observaciones;
      return;
    case 8:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramVacacion.personal;
      return;
    case 9:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramVacacion.tipoPersonal;
      return;
    case 10:
      if (paramVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoVacacion = paramVacacion.tipoVacacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Vacacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Vacacion localVacacion = (Vacacion)paramObject;
    if (localVacacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localVacacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new VacacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new VacacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionPK))
      throw new IllegalArgumentException("arg1");
    VacacionPK localVacacionPK = (VacacionPK)paramObject;
    localVacacionPK.idVacacion = this.idVacacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VacacionPK))
      throw new IllegalArgumentException("arg1");
    VacacionPK localVacacionPK = (VacacionPK)paramObject;
    this.idVacacion = localVacacionPK.idVacacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionPK))
      throw new IllegalArgumentException("arg2");
    VacacionPK localVacacionPK = (VacacionPK)paramObject;
    localVacacionPK.idVacacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VacacionPK))
      throw new IllegalArgumentException("arg2");
    VacacionPK localVacacionPK = (VacacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localVacacionPK.idVacacion);
  }

  private static final int jdoGetanio(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.anio;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.anio;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 0))
      return paramVacacion.anio;
    return localStateManager.getIntField(paramVacacion, jdoInheritedFieldCount + 0, paramVacacion.anio);
  }

  private static final void jdoSetanio(Vacacion paramVacacion, int paramInt)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacion, jdoInheritedFieldCount + 0, paramVacacion.anio, paramInt);
  }

  private static final int jdoGetdiasDisfrute(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.diasDisfrute;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.diasDisfrute;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 1))
      return paramVacacion.diasDisfrute;
    return localStateManager.getIntField(paramVacacion, jdoInheritedFieldCount + 1, paramVacacion.diasDisfrute);
  }

  private static final void jdoSetdiasDisfrute(Vacacion paramVacacion, int paramInt)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.diasDisfrute = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.diasDisfrute = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacion, jdoInheritedFieldCount + 1, paramVacacion.diasDisfrute, paramInt);
  }

  private static final int jdoGetdiasPendientes(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.diasPendientes;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.diasPendientes;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 2))
      return paramVacacion.diasPendientes;
    return localStateManager.getIntField(paramVacacion, jdoInheritedFieldCount + 2, paramVacacion.diasPendientes);
  }

  private static final void jdoSetdiasPendientes(Vacacion paramVacacion, int paramInt)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.diasPendientes = paramInt;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.diasPendientes = paramInt;
      return;
    }
    localStateManager.setIntField(paramVacacion, jdoInheritedFieldCount + 2, paramVacacion.diasPendientes, paramInt);
  }

  private static final Date jdoGetfechaFin(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.fechaFin;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.fechaFin;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 3))
      return paramVacacion.fechaFin;
    return (Date)localStateManager.getObjectField(paramVacacion, jdoInheritedFieldCount + 3, paramVacacion.fechaFin);
  }

  private static final void jdoSetfechaFin(Vacacion paramVacacion, Date paramDate)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacion, jdoInheritedFieldCount + 3, paramVacacion.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.fechaInicio;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.fechaInicio;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 4))
      return paramVacacion.fechaInicio;
    return (Date)localStateManager.getObjectField(paramVacacion, jdoInheritedFieldCount + 4, paramVacacion.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Vacacion paramVacacion, Date paramDate)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacion, jdoInheritedFieldCount + 4, paramVacacion.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaReintegro(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.fechaReintegro;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.fechaReintegro;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 5))
      return paramVacacion.fechaReintegro;
    return (Date)localStateManager.getObjectField(paramVacacion, jdoInheritedFieldCount + 5, paramVacacion.fechaReintegro);
  }

  private static final void jdoSetfechaReintegro(Vacacion paramVacacion, Date paramDate)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.fechaReintegro = paramDate;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.fechaReintegro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramVacacion, jdoInheritedFieldCount + 5, paramVacacion.fechaReintegro, paramDate);
  }

  private static final long jdoGetidVacacion(Vacacion paramVacacion)
  {
    return paramVacacion.idVacacion;
  }

  private static final void jdoSetidVacacion(Vacacion paramVacacion, long paramLong)
  {
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.idVacacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramVacacion, jdoInheritedFieldCount + 6, paramVacacion.idVacacion, paramLong);
  }

  private static final String jdoGetobservaciones(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.observaciones;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.observaciones;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 7))
      return paramVacacion.observaciones;
    return localStateManager.getStringField(paramVacacion, jdoInheritedFieldCount + 7, paramVacacion.observaciones);
  }

  private static final void jdoSetobservaciones(Vacacion paramVacacion, String paramString)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramVacacion, jdoInheritedFieldCount + 7, paramVacacion.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Vacacion paramVacacion)
  {
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.personal;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 8))
      return paramVacacion.personal;
    return (Personal)localStateManager.getObjectField(paramVacacion, jdoInheritedFieldCount + 8, paramVacacion.personal);
  }

  private static final void jdoSetpersonal(Vacacion paramVacacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacion, jdoInheritedFieldCount + 8, paramVacacion.personal, paramPersonal);
  }

  private static final TipoPersonal jdoGettipoPersonal(Vacacion paramVacacion)
  {
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.tipoPersonal;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 9))
      return paramVacacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramVacacion, jdoInheritedFieldCount + 9, paramVacacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Vacacion paramVacacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramVacacion, jdoInheritedFieldCount + 9, paramVacacion.tipoPersonal, paramTipoPersonal);
  }

  private static final String jdoGettipoVacacion(Vacacion paramVacacion)
  {
    if (paramVacacion.jdoFlags <= 0)
      return paramVacacion.tipoVacacion;
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramVacacion.tipoVacacion;
    if (localStateManager.isLoaded(paramVacacion, jdoInheritedFieldCount + 10))
      return paramVacacion.tipoVacacion;
    return localStateManager.getStringField(paramVacacion, jdoInheritedFieldCount + 10, paramVacacion.tipoVacacion);
  }

  private static final void jdoSettipoVacacion(Vacacion paramVacacion, String paramString)
  {
    if (paramVacacion.jdoFlags == 0)
    {
      paramVacacion.tipoVacacion = paramString;
      return;
    }
    StateManager localStateManager = paramVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramVacacion.tipoVacacion = paramString;
      return;
    }
    localStateManager.setStringField(paramVacacion, jdoInheritedFieldCount + 10, paramVacacion.tipoVacacion, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}