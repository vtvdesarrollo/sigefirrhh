package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ContratoPK
  implements Serializable
{
  public long idContrato;

  public ContratoPK()
  {
  }

  public ContratoPK(long idContrato)
  {
    this.idContrato = idContrato;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ContratoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ContratoPK thatPK)
  {
    return 
      this.idContrato == thatPK.idContrato;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idContrato)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idContrato);
  }
}