package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AntecedenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAntecedente(Antecedente antecedente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Antecedente antecedenteNew = 
      (Antecedente)BeanUtils.cloneBean(
      antecedente);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (antecedenteNew.getPersonal() != null) {
      antecedenteNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        antecedenteNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(antecedenteNew);
  }

  public void updateAntecedente(Antecedente antecedente) throws Exception
  {
    Antecedente antecedenteModify = 
      findAntecedenteById(antecedente.getIdAntecedente());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (antecedente.getPersonal() != null) {
      antecedente.setPersonal(
        personalBeanBusiness.findPersonalById(
        antecedente.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(antecedenteModify, antecedente);
  }

  public void deleteAntecedente(Antecedente antecedente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Antecedente antecedenteDelete = 
      findAntecedenteById(antecedente.getIdAntecedente());
    pm.deletePersistent(antecedenteDelete);
  }

  public Antecedente findAntecedenteById(long idAntecedente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAntecedente == pIdAntecedente";
    Query query = pm.newQuery(Antecedente.class, filter);

    query.declareParameters("long pIdAntecedente");

    parameters.put("pIdAntecedente", new Long(idAntecedente));

    Collection colAntecedente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAntecedente.iterator();
    return (Antecedente)iterator.next();
  }

  public Collection findAntecedenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent antecedenteExtent = pm.getExtent(
      Antecedente.class, true);
    Query query = pm.newQuery(antecedenteExtent);
    query.setOrdering("nombreInstitucion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Antecedente.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("nombreInstitucion ascending");

    Collection colAntecedente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAntecedente);

    return colAntecedente;
  }
}