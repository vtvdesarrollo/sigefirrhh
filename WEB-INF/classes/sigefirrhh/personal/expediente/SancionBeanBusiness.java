package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.personal.TipoAmonestacion;
import sigefirrhh.base.personal.TipoAmonestacionBeanBusiness;

public class SancionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSancion(Sancion sancion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Sancion sancionNew = 
      (Sancion)BeanUtils.cloneBean(
      sancion);

    TipoAmonestacionBeanBusiness tipoAmonestacionBeanBusiness = new TipoAmonestacionBeanBusiness();

    if (sancionNew.getTipoAmonestacion() != null) {
      sancionNew.setTipoAmonestacion(
        tipoAmonestacionBeanBusiness.findTipoAmonestacionById(
        sancionNew.getTipoAmonestacion().getIdTipoAmonestacion()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (sancionNew.getPersonal() != null) {
      sancionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        sancionNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (sancionNew.getOrganismo() != null) {
      sancionNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        sancionNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(sancionNew);
  }

  public void updateSancion(Sancion sancion) throws Exception
  {
    Sancion sancionModify = 
      findSancionById(sancion.getIdSancion());

    TipoAmonestacionBeanBusiness tipoAmonestacionBeanBusiness = new TipoAmonestacionBeanBusiness();

    if (sancion.getTipoAmonestacion() != null) {
      sancion.setTipoAmonestacion(
        tipoAmonestacionBeanBusiness.findTipoAmonestacionById(
        sancion.getTipoAmonestacion().getIdTipoAmonestacion()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (sancion.getPersonal() != null) {
      sancion.setPersonal(
        personalBeanBusiness.findPersonalById(
        sancion.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (sancion.getOrganismo() != null) {
      sancion.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        sancion.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(sancionModify, sancion);
  }

  public void deleteSancion(Sancion sancion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Sancion sancionDelete = 
      findSancionById(sancion.getIdSancion());
    pm.deletePersistent(sancionDelete);
  }

  public Sancion findSancionById(long idSancion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSancion == pIdSancion";
    Query query = pm.newQuery(Sancion.class, filter);

    query.declareParameters("long pIdSancion");

    parameters.put("pIdSancion", new Long(idSancion));

    Collection colSancion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSancion.iterator();
    return (Sancion)iterator.next();
  }

  public Collection findSancionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent sancionExtent = pm.getExtent(
      Sancion.class, true);
    Query query = pm.newQuery(sancionExtent);
    query.setOrdering("tipoAmonestacion.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Sancion.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("tipoAmonestacion.descripcion ascending");

    Collection colSancion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSancion);

    return colSancion;
  }
}