package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoAcreencia;
import sigefirrhh.base.personal.TipoAcreenciaBeanBusiness;

public class AcreenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAcreencia(Acreencia acreencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Acreencia acreenciaNew = 
      (Acreencia)BeanUtils.cloneBean(
      acreencia);

    TipoAcreenciaBeanBusiness tipoAcreenciaBeanBusiness = new TipoAcreenciaBeanBusiness();

    if (acreenciaNew.getTipoAcreencia() != null) {
      acreenciaNew.setTipoAcreencia(
        tipoAcreenciaBeanBusiness.findTipoAcreenciaById(
        acreenciaNew.getTipoAcreencia().getIdTipoAcreencia()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (acreenciaNew.getPersonal() != null) {
      acreenciaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        acreenciaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(acreenciaNew);
  }

  public void updateAcreencia(Acreencia acreencia) throws Exception
  {
    Acreencia acreenciaModify = 
      findAcreenciaById(acreencia.getIdAcreencia());

    TipoAcreenciaBeanBusiness tipoAcreenciaBeanBusiness = new TipoAcreenciaBeanBusiness();

    if (acreencia.getTipoAcreencia() != null) {
      acreencia.setTipoAcreencia(
        tipoAcreenciaBeanBusiness.findTipoAcreenciaById(
        acreencia.getTipoAcreencia().getIdTipoAcreencia()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (acreencia.getPersonal() != null) {
      acreencia.setPersonal(
        personalBeanBusiness.findPersonalById(
        acreencia.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(acreenciaModify, acreencia);
  }

  public void deleteAcreencia(Acreencia acreencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Acreencia acreenciaDelete = 
      findAcreenciaById(acreencia.getIdAcreencia());
    pm.deletePersistent(acreenciaDelete);
  }

  public Acreencia findAcreenciaById(long idAcreencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAcreencia == pIdAcreencia";
    Query query = pm.newQuery(Acreencia.class, filter);

    query.declareParameters("long pIdAcreencia");

    parameters.put("pIdAcreencia", new Long(idAcreencia));

    Collection colAcreencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAcreencia.iterator();
    return (Acreencia)iterator.next();
  }

  public Collection findAcreenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent acreenciaExtent = pm.getExtent(
      Acreencia.class, true);
    Query query = pm.newQuery(acreenciaExtent);
    query.setOrdering("tipoAcreencia.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoAcreencia(long idTipoAcreencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoAcreencia.idTipoAcreencia == pIdTipoAcreencia";

    Query query = pm.newQuery(Acreencia.class, filter);

    query.declareParameters("long pIdTipoAcreencia");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoAcreencia", new Long(idTipoAcreencia));

    query.setOrdering("tipoAcreencia.descripcion ascending");

    Collection colAcreencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAcreencia);

    return colAcreencia;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Acreencia.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("tipoAcreencia.descripcion ascending");

    Collection colAcreencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAcreencia);

    return colAcreencia;
  }
}