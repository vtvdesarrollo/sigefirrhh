package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Experiencia
  implements Serializable, PersistenceCapable
{
  private long idExperiencia;
  private String nombreInstitucion;
  private Date fechaIngreso;
  private Date fechaEgreso;
  private String cargoIngreso;
  private String cargoEgreso;
  private String jefe;
  private String telefono;
  private String causaRetiro;
  private double ultimoSueldo;
  private String observaciones;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargoEgreso", "cargoIngreso", "causaRetiro", "fechaEgreso", "fechaIngreso", "idExperiencia", "idSitp", "jefe", "nombreInstitucion", "observaciones", "personal", "telefono", "tiempoSitp", "ultimoSueldo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombreInstitucion(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaIngreso(this)) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaEgreso(this));
  }

  public String getCausaRetiro()
  {
    return jdoGetcausaRetiro(this);
  }

  public Date getFechaEgreso()
  {
    return jdoGetfechaEgreso(this);
  }

  public Date getFechaIngreso()
  {
    return jdoGetfechaIngreso(this);
  }

  public long getIdExperiencia()
  {
    return jdoGetidExperiencia(this);
  }

  public String getJefe()
  {
    return jdoGetjefe(this);
  }

  public String getNombreInstitucion()
  {
    return jdoGetnombreInstitucion(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public String getTelefono()
  {
    return jdoGettelefono(this);
  }

  public double getUltimoSueldo()
  {
    return jdoGetultimoSueldo(this);
  }

  public void setCausaRetiro(String string)
  {
    jdoSetcausaRetiro(this, string);
  }

  public void setFechaEgreso(Date date)
  {
    jdoSetfechaEgreso(this, date);
  }

  public void setFechaIngreso(Date date)
  {
    jdoSetfechaIngreso(this, date);
  }

  public void setIdExperiencia(long l)
  {
    jdoSetidExperiencia(this, l);
  }

  public void setJefe(String string)
  {
    jdoSetjefe(this, string);
  }

  public void setNombreInstitucion(String string)
  {
    jdoSetnombreInstitucion(this, string);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTelefono(String string)
  {
    jdoSettelefono(this, string);
  }

  public void setUltimoSueldo(double d)
  {
    jdoSetultimoSueldo(this, d);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public String getCargoEgreso()
  {
    return jdoGetcargoEgreso(this);
  }

  public String getCargoIngreso()
  {
    return jdoGetcargoIngreso(this);
  }

  public void setCargoEgreso(String string)
  {
    jdoSetcargoEgreso(this, string);
  }

  public void setCargoIngreso(String string)
  {
    jdoSetcargoIngreso(this, string);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Experiencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Experiencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Experiencia localExperiencia = new Experiencia();
    localExperiencia.jdoFlags = 1;
    localExperiencia.jdoStateManager = paramStateManager;
    return localExperiencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Experiencia localExperiencia = new Experiencia();
    localExperiencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localExperiencia.jdoFlags = 1;
    localExperiencia.jdoStateManager = paramStateManager;
    return localExperiencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoEgreso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoIngreso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.causaRetiro);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEgreso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idExperiencia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.jefe);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInstitucion);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ultimoSueldo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaRetiro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEgreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idExperiencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jefe = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ultimoSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Experiencia paramExperiencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.cargoEgreso = paramExperiencia.cargoEgreso;
      return;
    case 1:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.cargoIngreso = paramExperiencia.cargoIngreso;
      return;
    case 2:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.causaRetiro = paramExperiencia.causaRetiro;
      return;
    case 3:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEgreso = paramExperiencia.fechaEgreso;
      return;
    case 4:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramExperiencia.fechaIngreso;
      return;
    case 5:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.idExperiencia = paramExperiencia.idExperiencia;
      return;
    case 6:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramExperiencia.idSitp;
      return;
    case 7:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.jefe = paramExperiencia.jefe;
      return;
    case 8:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInstitucion = paramExperiencia.nombreInstitucion;
      return;
    case 9:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramExperiencia.observaciones;
      return;
    case 10:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramExperiencia.personal;
      return;
    case 11:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.telefono = paramExperiencia.telefono;
      return;
    case 12:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramExperiencia.tiempoSitp;
      return;
    case 13:
      if (paramExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.ultimoSueldo = paramExperiencia.ultimoSueldo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Experiencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Experiencia localExperiencia = (Experiencia)paramObject;
    if (localExperiencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localExperiencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ExperienciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ExperienciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExperienciaPK))
      throw new IllegalArgumentException("arg1");
    ExperienciaPK localExperienciaPK = (ExperienciaPK)paramObject;
    localExperienciaPK.idExperiencia = this.idExperiencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExperienciaPK))
      throw new IllegalArgumentException("arg1");
    ExperienciaPK localExperienciaPK = (ExperienciaPK)paramObject;
    this.idExperiencia = localExperienciaPK.idExperiencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExperienciaPK))
      throw new IllegalArgumentException("arg2");
    ExperienciaPK localExperienciaPK = (ExperienciaPK)paramObject;
    localExperienciaPK.idExperiencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExperienciaPK))
      throw new IllegalArgumentException("arg2");
    ExperienciaPK localExperienciaPK = (ExperienciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localExperienciaPK.idExperiencia);
  }

  private static final String jdoGetcargoEgreso(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.cargoEgreso;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.cargoEgreso;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 0))
      return paramExperiencia.cargoEgreso;
    return localStateManager.getStringField(paramExperiencia, jdoInheritedFieldCount + 0, paramExperiencia.cargoEgreso);
  }

  private static final void jdoSetcargoEgreso(Experiencia paramExperiencia, String paramString)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.cargoEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.cargoEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramExperiencia, jdoInheritedFieldCount + 0, paramExperiencia.cargoEgreso, paramString);
  }

  private static final String jdoGetcargoIngreso(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.cargoIngreso;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.cargoIngreso;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 1))
      return paramExperiencia.cargoIngreso;
    return localStateManager.getStringField(paramExperiencia, jdoInheritedFieldCount + 1, paramExperiencia.cargoIngreso);
  }

  private static final void jdoSetcargoIngreso(Experiencia paramExperiencia, String paramString)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.cargoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.cargoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramExperiencia, jdoInheritedFieldCount + 1, paramExperiencia.cargoIngreso, paramString);
  }

  private static final String jdoGetcausaRetiro(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.causaRetiro;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.causaRetiro;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 2))
      return paramExperiencia.causaRetiro;
    return localStateManager.getStringField(paramExperiencia, jdoInheritedFieldCount + 2, paramExperiencia.causaRetiro);
  }

  private static final void jdoSetcausaRetiro(Experiencia paramExperiencia, String paramString)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.causaRetiro = paramString;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.causaRetiro = paramString;
      return;
    }
    localStateManager.setStringField(paramExperiencia, jdoInheritedFieldCount + 2, paramExperiencia.causaRetiro, paramString);
  }

  private static final Date jdoGetfechaEgreso(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.fechaEgreso;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.fechaEgreso;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 3))
      return paramExperiencia.fechaEgreso;
    return (Date)localStateManager.getObjectField(paramExperiencia, jdoInheritedFieldCount + 3, paramExperiencia.fechaEgreso);
  }

  private static final void jdoSetfechaEgreso(Experiencia paramExperiencia, Date paramDate)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.fechaEgreso = paramDate;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.fechaEgreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramExperiencia, jdoInheritedFieldCount + 3, paramExperiencia.fechaEgreso, paramDate);
  }

  private static final Date jdoGetfechaIngreso(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.fechaIngreso;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.fechaIngreso;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 4))
      return paramExperiencia.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramExperiencia, jdoInheritedFieldCount + 4, paramExperiencia.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(Experiencia paramExperiencia, Date paramDate)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramExperiencia, jdoInheritedFieldCount + 4, paramExperiencia.fechaIngreso, paramDate);
  }

  private static final long jdoGetidExperiencia(Experiencia paramExperiencia)
  {
    return paramExperiencia.idExperiencia;
  }

  private static final void jdoSetidExperiencia(Experiencia paramExperiencia, long paramLong)
  {
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.idExperiencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramExperiencia, jdoInheritedFieldCount + 5, paramExperiencia.idExperiencia, paramLong);
  }

  private static final int jdoGetidSitp(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.idSitp;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.idSitp;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 6))
      return paramExperiencia.idSitp;
    return localStateManager.getIntField(paramExperiencia, jdoInheritedFieldCount + 6, paramExperiencia.idSitp);
  }

  private static final void jdoSetidSitp(Experiencia paramExperiencia, int paramInt)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramExperiencia, jdoInheritedFieldCount + 6, paramExperiencia.idSitp, paramInt);
  }

  private static final String jdoGetjefe(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.jefe;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.jefe;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 7))
      return paramExperiencia.jefe;
    return localStateManager.getStringField(paramExperiencia, jdoInheritedFieldCount + 7, paramExperiencia.jefe);
  }

  private static final void jdoSetjefe(Experiencia paramExperiencia, String paramString)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.jefe = paramString;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.jefe = paramString;
      return;
    }
    localStateManager.setStringField(paramExperiencia, jdoInheritedFieldCount + 7, paramExperiencia.jefe, paramString);
  }

  private static final String jdoGetnombreInstitucion(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.nombreInstitucion;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.nombreInstitucion;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 8))
      return paramExperiencia.nombreInstitucion;
    return localStateManager.getStringField(paramExperiencia, jdoInheritedFieldCount + 8, paramExperiencia.nombreInstitucion);
  }

  private static final void jdoSetnombreInstitucion(Experiencia paramExperiencia, String paramString)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.nombreInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.nombreInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramExperiencia, jdoInheritedFieldCount + 8, paramExperiencia.nombreInstitucion, paramString);
  }

  private static final String jdoGetobservaciones(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.observaciones;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.observaciones;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 9))
      return paramExperiencia.observaciones;
    return localStateManager.getStringField(paramExperiencia, jdoInheritedFieldCount + 9, paramExperiencia.observaciones);
  }

  private static final void jdoSetobservaciones(Experiencia paramExperiencia, String paramString)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramExperiencia, jdoInheritedFieldCount + 9, paramExperiencia.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Experiencia paramExperiencia)
  {
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.personal;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 10))
      return paramExperiencia.personal;
    return (Personal)localStateManager.getObjectField(paramExperiencia, jdoInheritedFieldCount + 10, paramExperiencia.personal);
  }

  private static final void jdoSetpersonal(Experiencia paramExperiencia, Personal paramPersonal)
  {
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramExperiencia, jdoInheritedFieldCount + 10, paramExperiencia.personal, paramPersonal);
  }

  private static final String jdoGettelefono(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.telefono;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.telefono;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 11))
      return paramExperiencia.telefono;
    return localStateManager.getStringField(paramExperiencia, jdoInheritedFieldCount + 11, paramExperiencia.telefono);
  }

  private static final void jdoSettelefono(Experiencia paramExperiencia, String paramString)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.telefono = paramString;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.telefono = paramString;
      return;
    }
    localStateManager.setStringField(paramExperiencia, jdoInheritedFieldCount + 11, paramExperiencia.telefono, paramString);
  }

  private static final Date jdoGettiempoSitp(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.tiempoSitp;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.tiempoSitp;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 12))
      return paramExperiencia.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramExperiencia, jdoInheritedFieldCount + 12, paramExperiencia.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Experiencia paramExperiencia, Date paramDate)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramExperiencia, jdoInheritedFieldCount + 12, paramExperiencia.tiempoSitp, paramDate);
  }

  private static final double jdoGetultimoSueldo(Experiencia paramExperiencia)
  {
    if (paramExperiencia.jdoFlags <= 0)
      return paramExperiencia.ultimoSueldo;
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramExperiencia.ultimoSueldo;
    if (localStateManager.isLoaded(paramExperiencia, jdoInheritedFieldCount + 13))
      return paramExperiencia.ultimoSueldo;
    return localStateManager.getDoubleField(paramExperiencia, jdoInheritedFieldCount + 13, paramExperiencia.ultimoSueldo);
  }

  private static final void jdoSetultimoSueldo(Experiencia paramExperiencia, double paramDouble)
  {
    if (paramExperiencia.jdoFlags == 0)
    {
      paramExperiencia.ultimoSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperiencia.ultimoSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramExperiencia, jdoInheritedFieldCount + 13, paramExperiencia.ultimoSueldo, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}