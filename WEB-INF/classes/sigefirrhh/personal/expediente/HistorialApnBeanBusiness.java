package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonalBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaMovimientoBeanBusiness;

public class HistorialApnBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHistorialApn(HistorialApn historialApn)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HistorialApn historialApnNew = 
      (HistorialApn)BeanUtils.cloneBean(
      historialApn);

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (historialApnNew.getClasificacionPersonal() != null) {
      historialApnNew.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        historialApnNew.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (historialApnNew.getCausaMovimiento() != null) {
      historialApnNew.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        historialApnNew.getCausaMovimiento().getIdCausaMovimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (historialApnNew.getPersonal() != null) {
      historialApnNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        historialApnNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (historialApnNew.getOrganismo() != null) {
      historialApnNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        historialApnNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(historialApnNew);
  }

  public void updateHistorialApn(HistorialApn historialApn) throws Exception
  {
    HistorialApn historialApnModify = 
      findHistorialApnById(historialApn.getIdHistorialApn());

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (historialApn.getClasificacionPersonal() != null) {
      historialApn.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        historialApn.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (historialApn.getCausaMovimiento() != null) {
      historialApn.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        historialApn.getCausaMovimiento().getIdCausaMovimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (historialApn.getPersonal() != null) {
      historialApn.setPersonal(
        personalBeanBusiness.findPersonalById(
        historialApn.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (historialApn.getOrganismo() != null) {
      historialApn.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        historialApn.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(historialApnModify, historialApn);
  }

  public void deleteHistorialApn(HistorialApn historialApn) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistorialApn historialApnDelete = 
      findHistorialApnById(historialApn.getIdHistorialApn());
    pm.deletePersistent(historialApnDelete);
  }

  public HistorialApn findHistorialApnById(long idHistorialApn) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistorialApn == pIdHistorialApn";
    Query query = pm.newQuery(HistorialApn.class, filter);

    query.declareParameters("long pIdHistorialApn");

    parameters.put("pIdHistorialApn", new Long(idHistorialApn));

    Collection colHistorialApn = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistorialApn.iterator();
    return (HistorialApn)iterator.next();
  }

  public Collection findHistorialApnAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historialApnExtent = pm.getExtent(
      HistorialApn.class, true);
    Query query = pm.newQuery(historialApnExtent);
    query.setOrdering("fechaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(HistorialApn.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colHistorialApn = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistorialApn);

    return colHistorialApn;
  }
}