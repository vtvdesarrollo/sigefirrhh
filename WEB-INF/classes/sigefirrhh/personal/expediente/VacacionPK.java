package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class VacacionPK
  implements Serializable
{
  public long idVacacion;

  public VacacionPK()
  {
  }

  public VacacionPK(long idVacacion)
  {
    this.idVacacion = idVacacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((VacacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(VacacionPK thatPK)
  {
    return 
      this.idVacacion == thatPK.idVacacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idVacacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idVacacion);
  }
}