package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Profesion;
import sigefirrhh.base.personal.ProfesionBeanBusiness;

public class ProfesionTrabajadorBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addProfesionTrabajador(ProfesionTrabajador profesionTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ProfesionTrabajador profesionTrabajadorNew = 
      (ProfesionTrabajador)BeanUtils.cloneBean(
      profesionTrabajador);

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (profesionTrabajadorNew.getProfesion() != null) {
      profesionTrabajadorNew.setProfesion(
        profesionBeanBusiness.findProfesionById(
        profesionTrabajadorNew.getProfesion().getIdProfesion()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (profesionTrabajadorNew.getPersonal() != null) {
      profesionTrabajadorNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        profesionTrabajadorNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(profesionTrabajadorNew);
  }

  public void updateProfesionTrabajador(ProfesionTrabajador profesionTrabajador) throws Exception
  {
    ProfesionTrabajador profesionTrabajadorModify = 
      findProfesionTrabajadorById(profesionTrabajador.getIdProfesionTrabajador());

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (profesionTrabajador.getProfesion() != null) {
      profesionTrabajador.setProfesion(
        profesionBeanBusiness.findProfesionById(
        profesionTrabajador.getProfesion().getIdProfesion()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (profesionTrabajador.getPersonal() != null) {
      profesionTrabajador.setPersonal(
        personalBeanBusiness.findPersonalById(
        profesionTrabajador.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(profesionTrabajadorModify, profesionTrabajador);
  }

  public void deleteProfesionTrabajador(ProfesionTrabajador profesionTrabajador) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ProfesionTrabajador profesionTrabajadorDelete = 
      findProfesionTrabajadorById(profesionTrabajador.getIdProfesionTrabajador());
    pm.deletePersistent(profesionTrabajadorDelete);
  }

  public ProfesionTrabajador findProfesionTrabajadorById(long idProfesionTrabajador) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idProfesionTrabajador == pIdProfesionTrabajador";
    Query query = pm.newQuery(ProfesionTrabajador.class, filter);

    query.declareParameters("long pIdProfesionTrabajador");

    parameters.put("pIdProfesionTrabajador", new Long(idProfesionTrabajador));

    Collection colProfesionTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colProfesionTrabajador.iterator();
    return (ProfesionTrabajador)iterator.next();
  }

  public Collection findProfesionTrabajadorAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent profesionTrabajadorExtent = pm.getExtent(
      ProfesionTrabajador.class, true);
    Query query = pm.newQuery(profesionTrabajadorExtent);
    query.setOrdering("profesion.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ProfesionTrabajador.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("profesion.nombre ascending");

    Collection colProfesionTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProfesionTrabajador);

    return colProfesionTrabajador;
  }
}