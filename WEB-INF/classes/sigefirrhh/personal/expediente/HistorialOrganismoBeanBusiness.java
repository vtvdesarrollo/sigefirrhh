package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonalBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaMovimientoBeanBusiness;

public class HistorialOrganismoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHistorialOrganismo(HistorialOrganismo historialOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HistorialOrganismo historialOrganismoNew = 
      (HistorialOrganismo)BeanUtils.cloneBean(
      historialOrganismo);

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (historialOrganismoNew.getClasificacionPersonal() != null) {
      historialOrganismoNew.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        historialOrganismoNew.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (historialOrganismoNew.getCausaMovimiento() != null) {
      historialOrganismoNew.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        historialOrganismoNew.getCausaMovimiento().getIdCausaMovimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (historialOrganismoNew.getPersonal() != null) {
      historialOrganismoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        historialOrganismoNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (historialOrganismoNew.getOrganismo() != null) {
      historialOrganismoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        historialOrganismoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(historialOrganismoNew);
  }

  public void updateHistorialOrganismo(HistorialOrganismo historialOrganismo) throws Exception
  {
    HistorialOrganismo historialOrganismoModify = 
      findHistorialOrganismoById(historialOrganismo.getIdHistorialOrganismo());

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (historialOrganismo.getClasificacionPersonal() != null) {
      historialOrganismo.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        historialOrganismo.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (historialOrganismo.getCausaMovimiento() != null) {
      historialOrganismo.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        historialOrganismo.getCausaMovimiento().getIdCausaMovimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (historialOrganismo.getPersonal() != null) {
      historialOrganismo.setPersonal(
        personalBeanBusiness.findPersonalById(
        historialOrganismo.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (historialOrganismo.getOrganismo() != null) {
      historialOrganismo.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        historialOrganismo.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(historialOrganismoModify, historialOrganismo);
  }

  public void deleteHistorialOrganismo(HistorialOrganismo historialOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistorialOrganismo historialOrganismoDelete = 
      findHistorialOrganismoById(historialOrganismo.getIdHistorialOrganismo());
    pm.deletePersistent(historialOrganismoDelete);
  }

  public HistorialOrganismo findHistorialOrganismoById(long idHistorialOrganismo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistorialOrganismo == pIdHistorialOrganismo";
    Query query = pm.newQuery(HistorialOrganismo.class, filter);

    query.declareParameters("long pIdHistorialOrganismo");

    parameters.put("pIdHistorialOrganismo", new Long(idHistorialOrganismo));

    Collection colHistorialOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistorialOrganismo.iterator();
    return (HistorialOrganismo)iterator.next();
  }

  public Collection findHistorialOrganismoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historialOrganismoExtent = pm.getExtent(
      HistorialOrganismo.class, true);
    Query query = pm.newQuery(historialOrganismoExtent);
    query.setOrdering("fechaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(HistorialOrganismo.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colHistorialOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistorialOrganismo);

    return colHistorialOrganismo;
  }
}