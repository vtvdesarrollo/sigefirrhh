package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.SubtipoCredencial;
import sigefirrhh.base.bienestar.TipoCredencial;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CredencialForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CredencialForm.class.getName());
  private Credencial credencial;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colTipoCredencialForSubTipoCredencial;
  private Collection colSubTipoCredencial;
  private Collection colPersonal;
  private String selectTipoCredencialForSubTipoCredencial;
  private String selectSubTipoCredencial;
  private String selectPersonal;
  private Object stateResultPersonal = null;

  private Object stateResultCredencialByPersonal = null;

  public String getSelectTipoCredencialForSubTipoCredencial()
  {
    return this.selectTipoCredencialForSubTipoCredencial;
  }
  public void setSelectTipoCredencialForSubTipoCredencial(String valTipoCredencialForSubTipoCredencial) {
    this.selectTipoCredencialForSubTipoCredencial = valTipoCredencialForSubTipoCredencial;
  }
  public void changeTipoCredencialForSubTipoCredencial(ValueChangeEvent event) {
    long idTipoCredencial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colSubTipoCredencial = null;
      if (idTipoCredencial > 0L)
        this.colSubTipoCredencial = 
          this.bienestarFacade.findSubtipoCredencialByTipoCredencial(
          idTipoCredencial);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowTipoCredencialForSubTipoCredencial() { return this.colTipoCredencialForSubTipoCredencial != null; }

  public String getSelectSubTipoCredencial() {
    return this.selectSubTipoCredencial;
  }
  public void setSelectSubTipoCredencial(String valSubTipoCredencial) {
    Iterator iterator = this.colSubTipoCredencial.iterator();
    SubtipoCredencial subTipoCredencial = null;
    this.credencial.setSubTipoCredencial(null);
    while (iterator.hasNext()) {
      subTipoCredencial = (SubtipoCredencial)iterator.next();
      if (String.valueOf(subTipoCredencial.getIdSubtipoCredencial()).equals(
        valSubTipoCredencial)) {
        this.credencial.setSubTipoCredencial(subTipoCredencial);
        break;
      }
    }
    this.selectSubTipoCredencial = valSubTipoCredencial;
  }
  public boolean isShowSubTipoCredencial() {
    return this.colSubTipoCredencial != null;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.credencial.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.credencial.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public Credencial getCredencial() {
    if (this.credencial == null) {
      this.credencial = new Credencial();
    }
    return this.credencial;
  }

  public CredencialForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColTipoCredencialForSubTipoCredencial() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCredencialForSubTipoCredencial.iterator();
    TipoCredencial tipoCredencialForSubTipoCredencial = null;
    while (iterator.hasNext()) {
      tipoCredencialForSubTipoCredencial = (TipoCredencial)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCredencialForSubTipoCredencial.getIdTipoCredencial()), 
        tipoCredencialForSubTipoCredencial.toString()));
    }
    return col;
  }
  public Collection getColSubTipoCredencial() {
    Collection col = new ArrayList();
    Iterator iterator = this.colSubTipoCredencial.iterator();
    SubtipoCredencial subTipoCredencial = null;
    while (iterator.hasNext()) {
      subTipoCredencial = (SubtipoCredencial)iterator.next();
      col.add(new SelectItem(
        String.valueOf(subTipoCredencial.getIdSubtipoCredencial()), 
        subTipoCredencial.toString()));
    }
    return col;
  }

  public Collection getColPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }
  public Collection getListMotivo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Credencial.LISTA_MOTIVO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colTipoCredencialForSubTipoCredencial = this.bienestarFacade.findAllTipoCredencial();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findCredencialByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findCredencialByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectCredencial()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectSubTipoCredencial = null;
    this.selectTipoCredencialForSubTipoCredencial = null;
    this.selectPersonal = null;

    long idCredencial = 
      Long.parseLong((String)requestParameterMap.get("idCredencial"));
    try
    {
      this.credencial = 
        this.expedienteFacade.findCredencialById(
        idCredencial);

      if (this.credencial.getSubTipoCredencial() != null) {
        this.selectSubTipoCredencial = 
          String.valueOf(this.credencial.getSubTipoCredencial().getIdSubtipoCredencial());
      }
      if (this.credencial.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.credencial.getPersonal().getIdPersonal());
      }
      SubtipoCredencial subTipoCredencial = null;
      TipoCredencial tipoCredencialForSubTipoCredencial = null;

      if (this.credencial.getSubTipoCredencial() != null) {
        long idSubTipoCredencial = this.credencial.getSubTipoCredencial().getIdSubtipoCredencial();
        this.selectSubTipoCredencial = String.valueOf(idSubTipoCredencial);
        subTipoCredencial = this.bienestarFacade.findSubtipoCredencialById(idSubTipoCredencial);
        this.colSubTipoCredencial = this.bienestarFacade.findSubtipoCredencialByTipoCredencial(
          subTipoCredencial.getTipoCredencial().getIdTipoCredencial());

        long idTipoCredencialForSubTipoCredencial = 
          this.credencial.getSubTipoCredencial().getTipoCredencial().getIdTipoCredencial();
        this.selectTipoCredencialForSubTipoCredencial = String.valueOf(idTipoCredencialForSubTipoCredencial);
        tipoCredencialForSubTipoCredencial = this.bienestarFacade.findTipoCredencialById(idTipoCredencialForSubTipoCredencial);
        this.colTipoCredencialForSubTipoCredencial = this.bienestarFacade.findAllTipoCredencial();
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = this.expedienteFacade.findPersonalById(idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.credencial.getFechaEntrega() != null) && 
      (this.credencial.getFechaEntrega().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha entrega no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.credencial.getFechaRetiro() != null) && 
      (this.credencial.getFechaRetiro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha retiro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.credencial.setPersonal(this.personal);
        log.error("******************************va a grabar credencial****" + this.credencial);
        log.error("******************************color****" + this.credencial.getColor());
        this.expedienteFacade.addCredencial(this.credencial);
        this.personal.setCredencial(this.credencial.getNumero());
        this.expedienteFacade.updatePersonal(this.personal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateCredencial(this.credencial);
        this.personal.setCredencial(this.credencial.getNumero());
        this.expedienteFacade.updatePersonal(this.personal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteCredencial(
        this.credencial);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;
    this.selectSubTipoCredencial = null;
    this.selectTipoCredencialForSubTipoCredencial = null;
    this.selectPersonal = null;

    this.credencial = new Credencial();

    this.credencial.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.credencial.setIdCredencial(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Credencial"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.credencial = new Credencial();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.credencial = new Credencial();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }
  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}