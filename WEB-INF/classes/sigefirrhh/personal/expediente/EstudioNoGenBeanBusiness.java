package sigefirrhh.personal.expediente;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class EstudioNoGenBeanBusiness extends EstudioBeanBusiness
  implements Serializable
{
  public Collection findByPersonalAndTipoCurso(long idPersonal, String codTipoCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && tipoCurso.codTipoCurso == pCodTipoCurso";

    Query query = pm.newQuery(Estudio.class, filter);

    query.declareParameters("long pIdPersonal, String pCodTipoCurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pCodTipoCurso", codTipoCurso);

    query.setOrdering("anio ascending");

    Collection colEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstudio);

    return colEstudio;
  }
}