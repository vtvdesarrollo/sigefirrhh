package sigefirrhh.personal.expediente;

import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PersonalAux
  implements PersistenceCapable
{
  private long id;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "id", "nombre" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public long getId()
  {
    return jdoGetid(this);
  }
  public void setId(long id) {
    jdoSetid(this, id);
  }
  public String getNombre() {
    return jdoGetnombre(this);
  }
  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 2;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.PersonalAux"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PersonalAux());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PersonalAux localPersonalAux = new PersonalAux();
    localPersonalAux.jdoFlags = 1;
    localPersonalAux.jdoStateManager = paramStateManager;
    return localPersonalAux;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PersonalAux localPersonalAux = new PersonalAux();
    localPersonalAux.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPersonalAux.jdoFlags = 1;
    localPersonalAux.jdoStateManager = paramStateManager;
    return localPersonalAux;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.id);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.id = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PersonalAux paramPersonalAux, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPersonalAux == null)
        throw new IllegalArgumentException("arg1");
      this.id = paramPersonalAux.id;
      return;
    case 1:
      if (paramPersonalAux == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramPersonalAux.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PersonalAux))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PersonalAux localPersonalAux = (PersonalAux)paramObject;
    if (localPersonalAux.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPersonalAux, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg1");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    localPersonalPK.id = this.id;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg1");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    this.id = localPersonalPK.id;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg2");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    localPersonalPK.id = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PersonalPK))
      throw new IllegalArgumentException("arg2");
    PersonalPK localPersonalPK = (PersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localPersonalPK.id);
  }

  private static final long jdoGetid(PersonalAux paramPersonalAux)
  {
    return paramPersonalAux.id;
  }

  private static final void jdoSetid(PersonalAux paramPersonalAux, long paramLong)
  {
    StateManager localStateManager = paramPersonalAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalAux.id = paramLong;
      return;
    }
    localStateManager.setLongField(paramPersonalAux, jdoInheritedFieldCount + 0, paramPersonalAux.id, paramLong);
  }

  private static final String jdoGetnombre(PersonalAux paramPersonalAux)
  {
    if (paramPersonalAux.jdoFlags <= 0)
      return paramPersonalAux.nombre;
    StateManager localStateManager = paramPersonalAux.jdoStateManager;
    if (localStateManager == null)
      return paramPersonalAux.nombre;
    if (localStateManager.isLoaded(paramPersonalAux, jdoInheritedFieldCount + 1))
      return paramPersonalAux.nombre;
    return localStateManager.getStringField(paramPersonalAux, jdoInheritedFieldCount + 1, paramPersonalAux.nombre);
  }

  private static final void jdoSetnombre(PersonalAux paramPersonalAux, String paramString)
  {
    if (paramPersonalAux.jdoFlags == 0)
    {
      paramPersonalAux.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramPersonalAux.jdoStateManager;
    if (localStateManager == null)
    {
      paramPersonalAux.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPersonalAux, jdoInheritedFieldCount + 1, paramPersonalAux.nombre, paramString);
  }
}