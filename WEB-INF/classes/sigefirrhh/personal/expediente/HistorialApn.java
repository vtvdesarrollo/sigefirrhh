package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;

public class HistorialApn
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_LOCALIDAD;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_TIPO;
  private long idHistorialApn;
  private int cedula;
  private String apellidosNombres;
  private ClasificacionPersonal clasificacionPersonal;
  private String tipoPersonal;
  private CausaMovimiento causaMovimiento;
  private String codCausaMovimiento;
  private Date fechaMovimiento;
  private Date fechaRegistro;
  private int codManualCargo;
  private String codCargo;
  private String codTabulador;
  private String descripcionCargo;
  private int codigoNomina;
  private String codSede;
  private String nombreSede;
  private String codDependencia;
  private String nombreDependencia;
  private double sueldo;
  private double compensacion;
  private double primasCargo;
  private double primasTrabajador;
  private int grado;
  private int paso;
  private String remesa;
  private int numeroMovimiento;
  private String afectaSueldo;
  private String localidad;
  private String estatus;
  private String aprobacionMpd;
  private String documentoSoporte;
  private Personal personal;
  private Organismo organismo;
  private String codOrganismo;
  private String nombreOrganismo;
  private String codRegion;
  private String nombreRegion;
  private String origenMovimiento;
  private String puntoCuenta;
  private Date fechaPuntoCuenta;
  private String codConcurso;
  private String observaciones;
  private String usuario;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "afectaSueldo", "apellidosNombres", "aprobacionMpd", "causaMovimiento", "cedula", "clasificacionPersonal", "codCargo", "codCausaMovimiento", "codConcurso", "codDependencia", "codManualCargo", "codOrganismo", "codRegion", "codSede", "codTabulador", "codigoNomina", "compensacion", "descripcionCargo", "documentoSoporte", "estatus", "fechaMovimiento", "fechaPuntoCuenta", "fechaRegistro", "grado", "idHistorialApn", "idSitp", "localidad", "nombreDependencia", "nombreOrganismo", "nombreRegion", "nombreSede", "numeroMovimiento", "observaciones", "organismo", "origenMovimiento", "paso", "personal", "primasCargo", "primasTrabajador", "puntoCuenta", "remesa", "sueldo", "tiempoSitp", "tipoPersonal", "usuario" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ClasificacionPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.HistorialApn"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistorialApn());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_LOCALIDAD = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_LOCALIDAD.put("C", "NIVEL CENTRAL");
    LISTA_LOCALIDAD.put("R", "NIVEL REGIONAL");
    LISTA_ESTATUS.put("0", "SIN REMESA");
    LISTA_ESTATUS.put("1", "CON REMESA");
    LISTA_ESTATUS.put("2", "ENVIADO MPD");
    LISTA_ESTATUS.put("3", "ANALISIS");
    LISTA_ESTATUS.put("4", "APROBADO");
    LISTA_ESTATUS.put("5", "DEVUELTO");
    LISTA_TIPO.put("0", "ALTO NIVEL");
    LISTA_TIPO.put("1", "ADMINISTRATIVO");
    LISTA_TIPO.put("2", "PROFESIONAL Y TECNICO");
    LISTA_TIPO.put("3", "NO CLASIFICADO");
    LISTA_TIPO.put("4", "OBRERO");
  }

  public HistorialApn()
  {
    jdoSetcedula(this, 0);

    jdoSettipoPersonal(this, "1");

    jdoSetcodigoNomina(this, 0);

    jdoSetsueldo(this, 0.0D);

    jdoSetcompensacion(this, 0.0D);

    jdoSetprimasCargo(this, 0.0D);

    jdoSetprimasTrabajador(this, 0.0D);

    jdoSetgrado(this, 1);

    jdoSetpaso(this, 1);

    jdoSetafectaSueldo(this, "S");

    jdoSetlocalidad(this, "C");

    jdoSetestatus(this, "0");

    jdoSetaprobacionMpd(this, "S");

    jdoSetorigenMovimiento(this, "S");
  }

  public String toString()
  {
    return 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaMovimiento(this)) + " - " + 
      jdoGetcausaMovimiento(this).getDescripcion();
  }

  public String getAfectaSueldo()
  {
    return jdoGetafectaSueldo(this);
  }

  public String getApellidosNombres()
  {
    return jdoGetapellidosNombres(this);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public CausaMovimiento getCausaMovimiento()
  {
    return jdoGetcausaMovimiento(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }

  public ClasificacionPersonal getClasificacionPersonal()
  {
    return jdoGetclasificacionPersonal(this);
  }

  public String getCodCargo()
  {
    return jdoGetcodCargo(this);
  }

  public String getCodCausaMovimiento()
  {
    return jdoGetcodCausaMovimiento(this);
  }

  public String getCodDependencia()
  {
    return jdoGetcodDependencia(this);
  }

  public int getCodigoNomina()
  {
    return jdoGetcodigoNomina(this);
  }

  public int getCodManualCargo()
  {
    return jdoGetcodManualCargo(this);
  }

  public String getCodOrganismo()
  {
    return jdoGetcodOrganismo(this);
  }

  public String getCodRegion()
  {
    return jdoGetcodRegion(this);
  }

  public String getCodSede()
  {
    return jdoGetcodSede(this);
  }

  public String getCodTabulador()
  {
    return jdoGetcodTabulador(this);
  }

  public double getCompensacion()
  {
    return jdoGetcompensacion(this);
  }

  public String getDescripcionCargo()
  {
    return jdoGetdescripcionCargo(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public Date getFechaMovimiento()
  {
    return jdoGetfechaMovimiento(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public int getGrado()
  {
    return jdoGetgrado(this);
  }

  public long getIdHistorialApn()
  {
    return jdoGetidHistorialApn(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public String getLocalidad()
  {
    return jdoGetlocalidad(this);
  }

  public String getNombreDependencia()
  {
    return jdoGetnombreDependencia(this);
  }

  public String getNombreOrganismo()
  {
    return jdoGetnombreOrganismo(this);
  }

  public String getNombreRegion()
  {
    return jdoGetnombreRegion(this);
  }

  public String getNombreSede()
  {
    return jdoGetnombreSede(this);
  }

  public int getNumeroMovimiento()
  {
    return jdoGetnumeroMovimiento(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public int getPaso()
  {
    return jdoGetpaso(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public double getPrimasCargo()
  {
    return jdoGetprimasCargo(this);
  }

  public double getPrimasTrabajador()
  {
    return jdoGetprimasTrabajador(this);
  }

  public String getRemesa()
  {
    return jdoGetremesa(this);
  }

  public double getSueldo()
  {
    return jdoGetsueldo(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public String getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setAfectaSueldo(String string)
  {
    jdoSetafectaSueldo(this, string);
  }

  public void setApellidosNombres(String string)
  {
    jdoSetapellidosNombres(this, string);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setCausaMovimiento(CausaMovimiento movimiento)
  {
    jdoSetcausaMovimiento(this, movimiento);
  }

  public void setCedula(int i)
  {
    jdoSetcedula(this, i);
  }

  public void setClasificacionPersonal(ClasificacionPersonal personal)
  {
    jdoSetclasificacionPersonal(this, personal);
  }

  public void setCodCargo(String string)
  {
    jdoSetcodCargo(this, string);
  }

  public void setCodCausaMovimiento(String string)
  {
    jdoSetcodCausaMovimiento(this, string);
  }

  public void setCodDependencia(String string)
  {
    jdoSetcodDependencia(this, string);
  }

  public void setCodigoNomina(int i)
  {
    jdoSetcodigoNomina(this, i);
  }

  public void setCodManualCargo(int i)
  {
    jdoSetcodManualCargo(this, i);
  }

  public void setCodOrganismo(String string)
  {
    jdoSetcodOrganismo(this, string);
  }

  public void setCodRegion(String string)
  {
    jdoSetcodRegion(this, string);
  }

  public void setCodSede(String string)
  {
    jdoSetcodSede(this, string);
  }

  public void setCodTabulador(String string)
  {
    jdoSetcodTabulador(this, string);
  }

  public void setCompensacion(double d)
  {
    jdoSetcompensacion(this, d);
  }

  public void setDescripcionCargo(String string)
  {
    jdoSetdescripcionCargo(this, string);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setFechaMovimiento(Date date)
  {
    jdoSetfechaMovimiento(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setGrado(int i)
  {
    jdoSetgrado(this, i);
  }

  public void setIdHistorialApn(long l)
  {
    jdoSetidHistorialApn(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setLocalidad(String string)
  {
    jdoSetlocalidad(this, string);
  }

  public void setNombreDependencia(String string)
  {
    jdoSetnombreDependencia(this, string);
  }

  public void setNombreOrganismo(String string)
  {
    jdoSetnombreOrganismo(this, string);
  }

  public void setNombreRegion(String string)
  {
    jdoSetnombreRegion(this, string);
  }

  public void setNombreSede(String string)
  {
    jdoSetnombreSede(this, string);
  }

  public void setNumeroMovimiento(int i)
  {
    jdoSetnumeroMovimiento(this, i);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setPaso(int i)
  {
    jdoSetpaso(this, i);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPrimasCargo(double d)
  {
    jdoSetprimasCargo(this, d);
  }

  public void setPrimasTrabajador(double d)
  {
    jdoSetprimasTrabajador(this, d);
  }

  public void setRemesa(String string)
  {
    jdoSetremesa(this, string);
  }

  public void setSueldo(double d)
  {
    jdoSetsueldo(this, d);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setTipoPersonal(String string)
  {
    jdoSettipoPersonal(this, string);
  }

  public void setUsuario(String string)
  {
    jdoSetusuario(this, string);
  }

  public String getOrigenMovimiento()
  {
    return jdoGetorigenMovimiento(this);
  }

  public void setOrigenMovimiento(String string)
  {
    jdoSetorigenMovimiento(this, string);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public Date getFechaPuntoCuenta()
  {
    return jdoGetfechaPuntoCuenta(this);
  }

  public String getPuntoCuenta()
  {
    return jdoGetpuntoCuenta(this);
  }

  public void setFechaPuntoCuenta(Date date)
  {
    jdoSetfechaPuntoCuenta(this, date);
  }

  public void setPuntoCuenta(String string)
  {
    jdoSetpuntoCuenta(this, string);
  }

  public String getCodConcurso()
  {
    return jdoGetcodConcurso(this);
  }

  public void setCodConcurso(String string)
  {
    jdoSetcodConcurso(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 45;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistorialApn localHistorialApn = new HistorialApn();
    localHistorialApn.jdoFlags = 1;
    localHistorialApn.jdoStateManager = paramStateManager;
    return localHistorialApn;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistorialApn localHistorialApn = new HistorialApn();
    localHistorialApn.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistorialApn.jdoFlags = 1;
    localHistorialApn.jdoStateManager = paramStateManager;
    return localHistorialApn;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.afectaSueldo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.apellidosNombres);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacionPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaMovimiento);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcurso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codManualCargo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegion);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSede);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTabulador);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacion);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionCargo);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaMovimiento);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPuntoCuenta);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistorialApn);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.localidad);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDependencia);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreOrganismo);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreRegion);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSede);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMovimiento);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origenMovimiento);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasCargo);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasTrabajador);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.puntoCuenta);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.remesa);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldo);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoPersonal);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.afectaSueldo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.apellidosNombres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacionPersonal = ((ClasificacionPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codManualCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTabulador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaMovimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPuntoCuenta = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistorialApn = localStateManager.replacingLongField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.localidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMovimiento = localStateManager.replacingIntField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origenMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.puntoCuenta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remesa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistorialApn paramHistorialApn, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.afectaSueldo = paramHistorialApn.afectaSueldo;
      return;
    case 1:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.apellidosNombres = paramHistorialApn.apellidosNombres;
      return;
    case 2:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramHistorialApn.aprobacionMpd;
      return;
    case 3:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramHistorialApn.causaMovimiento;
      return;
    case 4:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramHistorialApn.cedula;
      return;
    case 5:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacionPersonal = paramHistorialApn.clasificacionPersonal;
      return;
    case 6:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramHistorialApn.codCargo;
      return;
    case 7:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaMovimiento = paramHistorialApn.codCausaMovimiento;
      return;
    case 8:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codConcurso = paramHistorialApn.codConcurso;
      return;
    case 9:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramHistorialApn.codDependencia;
      return;
    case 10:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codManualCargo = paramHistorialApn.codManualCargo;
      return;
    case 11:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismo = paramHistorialApn.codOrganismo;
      return;
    case 12:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codRegion = paramHistorialApn.codRegion;
      return;
    case 13:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codSede = paramHistorialApn.codSede;
      return;
    case 14:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codTabulador = paramHistorialApn.codTabulador;
      return;
    case 15:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramHistorialApn.codigoNomina;
      return;
    case 16:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramHistorialApn.compensacion;
      return;
    case 17:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionCargo = paramHistorialApn.descripcionCargo;
      return;
    case 18:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramHistorialApn.documentoSoporte;
      return;
    case 19:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramHistorialApn.estatus;
      return;
    case 20:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.fechaMovimiento = paramHistorialApn.fechaMovimiento;
      return;
    case 21:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPuntoCuenta = paramHistorialApn.fechaPuntoCuenta;
      return;
    case 22:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramHistorialApn.fechaRegistro;
      return;
    case 23:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramHistorialApn.grado;
      return;
    case 24:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.idHistorialApn = paramHistorialApn.idHistorialApn;
      return;
    case 25:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramHistorialApn.idSitp;
      return;
    case 26:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.localidad = paramHistorialApn.localidad;
      return;
    case 27:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDependencia = paramHistorialApn.nombreDependencia;
      return;
    case 28:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.nombreOrganismo = paramHistorialApn.nombreOrganismo;
      return;
    case 29:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.nombreRegion = paramHistorialApn.nombreRegion;
      return;
    case 30:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSede = paramHistorialApn.nombreSede;
      return;
    case 31:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMovimiento = paramHistorialApn.numeroMovimiento;
      return;
    case 32:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramHistorialApn.observaciones;
      return;
    case 33:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramHistorialApn.organismo;
      return;
    case 34:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.origenMovimiento = paramHistorialApn.origenMovimiento;
      return;
    case 35:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramHistorialApn.paso;
      return;
    case 36:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramHistorialApn.personal;
      return;
    case 37:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.primasCargo = paramHistorialApn.primasCargo;
      return;
    case 38:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.primasTrabajador = paramHistorialApn.primasTrabajador;
      return;
    case 39:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.puntoCuenta = paramHistorialApn.puntoCuenta;
      return;
    case 40:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.remesa = paramHistorialApn.remesa;
      return;
    case 41:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.sueldo = paramHistorialApn.sueldo;
      return;
    case 42:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramHistorialApn.tiempoSitp;
      return;
    case 43:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramHistorialApn.tipoPersonal;
      return;
    case 44:
      if (paramHistorialApn == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramHistorialApn.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistorialApn))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistorialApn localHistorialApn = (HistorialApn)paramObject;
    if (localHistorialApn.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistorialApn, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistorialApnPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistorialApnPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistorialApnPK))
      throw new IllegalArgumentException("arg1");
    HistorialApnPK localHistorialApnPK = (HistorialApnPK)paramObject;
    localHistorialApnPK.idHistorialApn = this.idHistorialApn;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistorialApnPK))
      throw new IllegalArgumentException("arg1");
    HistorialApnPK localHistorialApnPK = (HistorialApnPK)paramObject;
    this.idHistorialApn = localHistorialApnPK.idHistorialApn;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistorialApnPK))
      throw new IllegalArgumentException("arg2");
    HistorialApnPK localHistorialApnPK = (HistorialApnPK)paramObject;
    localHistorialApnPK.idHistorialApn = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 24);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistorialApnPK))
      throw new IllegalArgumentException("arg2");
    HistorialApnPK localHistorialApnPK = (HistorialApnPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 24, localHistorialApnPK.idHistorialApn);
  }

  private static final String jdoGetafectaSueldo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.afectaSueldo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.afectaSueldo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 0))
      return paramHistorialApn.afectaSueldo;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 0, paramHistorialApn.afectaSueldo);
  }

  private static final void jdoSetafectaSueldo(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.afectaSueldo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.afectaSueldo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 0, paramHistorialApn.afectaSueldo, paramString);
  }

  private static final String jdoGetapellidosNombres(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.apellidosNombres;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.apellidosNombres;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 1))
      return paramHistorialApn.apellidosNombres;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 1, paramHistorialApn.apellidosNombres);
  }

  private static final void jdoSetapellidosNombres(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.apellidosNombres = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.apellidosNombres = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 1, paramHistorialApn.apellidosNombres, paramString);
  }

  private static final String jdoGetaprobacionMpd(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.aprobacionMpd;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.aprobacionMpd;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 2))
      return paramHistorialApn.aprobacionMpd;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 2, paramHistorialApn.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 2, paramHistorialApn.aprobacionMpd, paramString);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(HistorialApn paramHistorialApn)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.causaMovimiento;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 3))
      return paramHistorialApn.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 3, paramHistorialApn.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(HistorialApn paramHistorialApn, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 3, paramHistorialApn.causaMovimiento, paramCausaMovimiento);
  }

  private static final int jdoGetcedula(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.cedula;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.cedula;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 4))
      return paramHistorialApn.cedula;
    return localStateManager.getIntField(paramHistorialApn, jdoInheritedFieldCount + 4, paramHistorialApn.cedula);
  }

  private static final void jdoSetcedula(HistorialApn paramHistorialApn, int paramInt)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialApn, jdoInheritedFieldCount + 4, paramHistorialApn.cedula, paramInt);
  }

  private static final ClasificacionPersonal jdoGetclasificacionPersonal(HistorialApn paramHistorialApn)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.clasificacionPersonal;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 5))
      return paramHistorialApn.clasificacionPersonal;
    return (ClasificacionPersonal)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 5, paramHistorialApn.clasificacionPersonal);
  }

  private static final void jdoSetclasificacionPersonal(HistorialApn paramHistorialApn, ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.clasificacionPersonal = paramClasificacionPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 5, paramHistorialApn.clasificacionPersonal, paramClasificacionPersonal);
  }

  private static final String jdoGetcodCargo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codCargo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codCargo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 6))
      return paramHistorialApn.codCargo;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 6, paramHistorialApn.codCargo);
  }

  private static final void jdoSetcodCargo(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 6, paramHistorialApn.codCargo, paramString);
  }

  private static final String jdoGetcodCausaMovimiento(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codCausaMovimiento;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codCausaMovimiento;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 7))
      return paramHistorialApn.codCausaMovimiento;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 7, paramHistorialApn.codCausaMovimiento);
  }

  private static final void jdoSetcodCausaMovimiento(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codCausaMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codCausaMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 7, paramHistorialApn.codCausaMovimiento, paramString);
  }

  private static final String jdoGetcodConcurso(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codConcurso;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codConcurso;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 8))
      return paramHistorialApn.codConcurso;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 8, paramHistorialApn.codConcurso);
  }

  private static final void jdoSetcodConcurso(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codConcurso = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codConcurso = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 8, paramHistorialApn.codConcurso, paramString);
  }

  private static final String jdoGetcodDependencia(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codDependencia;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codDependencia;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 9))
      return paramHistorialApn.codDependencia;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 9, paramHistorialApn.codDependencia);
  }

  private static final void jdoSetcodDependencia(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 9, paramHistorialApn.codDependencia, paramString);
  }

  private static final int jdoGetcodManualCargo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codManualCargo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codManualCargo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 10))
      return paramHistorialApn.codManualCargo;
    return localStateManager.getIntField(paramHistorialApn, jdoInheritedFieldCount + 10, paramHistorialApn.codManualCargo);
  }

  private static final void jdoSetcodManualCargo(HistorialApn paramHistorialApn, int paramInt)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codManualCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codManualCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialApn, jdoInheritedFieldCount + 10, paramHistorialApn.codManualCargo, paramInt);
  }

  private static final String jdoGetcodOrganismo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codOrganismo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codOrganismo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 11))
      return paramHistorialApn.codOrganismo;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 11, paramHistorialApn.codOrganismo);
  }

  private static final void jdoSetcodOrganismo(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 11, paramHistorialApn.codOrganismo, paramString);
  }

  private static final String jdoGetcodRegion(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codRegion;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codRegion;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 12))
      return paramHistorialApn.codRegion;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 12, paramHistorialApn.codRegion);
  }

  private static final void jdoSetcodRegion(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codRegion = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 12, paramHistorialApn.codRegion, paramString);
  }

  private static final String jdoGetcodSede(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codSede;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codSede;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 13))
      return paramHistorialApn.codSede;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 13, paramHistorialApn.codSede);
  }

  private static final void jdoSetcodSede(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codSede = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codSede = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 13, paramHistorialApn.codSede, paramString);
  }

  private static final String jdoGetcodTabulador(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codTabulador;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codTabulador;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 14))
      return paramHistorialApn.codTabulador;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 14, paramHistorialApn.codTabulador);
  }

  private static final void jdoSetcodTabulador(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codTabulador = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codTabulador = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 14, paramHistorialApn.codTabulador, paramString);
  }

  private static final int jdoGetcodigoNomina(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.codigoNomina;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.codigoNomina;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 15))
      return paramHistorialApn.codigoNomina;
    return localStateManager.getIntField(paramHistorialApn, jdoInheritedFieldCount + 15, paramHistorialApn.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(HistorialApn paramHistorialApn, int paramInt)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialApn, jdoInheritedFieldCount + 15, paramHistorialApn.codigoNomina, paramInt);
  }

  private static final double jdoGetcompensacion(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.compensacion;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.compensacion;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 16))
      return paramHistorialApn.compensacion;
    return localStateManager.getDoubleField(paramHistorialApn, jdoInheritedFieldCount + 16, paramHistorialApn.compensacion);
  }

  private static final void jdoSetcompensacion(HistorialApn paramHistorialApn, double paramDouble)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.compensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.compensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialApn, jdoInheritedFieldCount + 16, paramHistorialApn.compensacion, paramDouble);
  }

  private static final String jdoGetdescripcionCargo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.descripcionCargo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.descripcionCargo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 17))
      return paramHistorialApn.descripcionCargo;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 17, paramHistorialApn.descripcionCargo);
  }

  private static final void jdoSetdescripcionCargo(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.descripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.descripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 17, paramHistorialApn.descripcionCargo, paramString);
  }

  private static final String jdoGetdocumentoSoporte(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.documentoSoporte;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.documentoSoporte;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 18))
      return paramHistorialApn.documentoSoporte;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 18, paramHistorialApn.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 18, paramHistorialApn.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.estatus;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.estatus;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 19))
      return paramHistorialApn.estatus;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 19, paramHistorialApn.estatus);
  }

  private static final void jdoSetestatus(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 19, paramHistorialApn.estatus, paramString);
  }

  private static final Date jdoGetfechaMovimiento(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.fechaMovimiento;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.fechaMovimiento;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 20))
      return paramHistorialApn.fechaMovimiento;
    return (Date)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 20, paramHistorialApn.fechaMovimiento);
  }

  private static final void jdoSetfechaMovimiento(HistorialApn paramHistorialApn, Date paramDate)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.fechaMovimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.fechaMovimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 20, paramHistorialApn.fechaMovimiento, paramDate);
  }

  private static final Date jdoGetfechaPuntoCuenta(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.fechaPuntoCuenta;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.fechaPuntoCuenta;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 21))
      return paramHistorialApn.fechaPuntoCuenta;
    return (Date)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 21, paramHistorialApn.fechaPuntoCuenta);
  }

  private static final void jdoSetfechaPuntoCuenta(HistorialApn paramHistorialApn, Date paramDate)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.fechaPuntoCuenta = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.fechaPuntoCuenta = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 21, paramHistorialApn.fechaPuntoCuenta, paramDate);
  }

  private static final Date jdoGetfechaRegistro(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.fechaRegistro;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.fechaRegistro;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 22))
      return paramHistorialApn.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 22, paramHistorialApn.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(HistorialApn paramHistorialApn, Date paramDate)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 22, paramHistorialApn.fechaRegistro, paramDate);
  }

  private static final int jdoGetgrado(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.grado;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.grado;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 23))
      return paramHistorialApn.grado;
    return localStateManager.getIntField(paramHistorialApn, jdoInheritedFieldCount + 23, paramHistorialApn.grado);
  }

  private static final void jdoSetgrado(HistorialApn paramHistorialApn, int paramInt)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialApn, jdoInheritedFieldCount + 23, paramHistorialApn.grado, paramInt);
  }

  private static final long jdoGetidHistorialApn(HistorialApn paramHistorialApn)
  {
    return paramHistorialApn.idHistorialApn;
  }

  private static final void jdoSetidHistorialApn(HistorialApn paramHistorialApn, long paramLong)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.idHistorialApn = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistorialApn, jdoInheritedFieldCount + 24, paramHistorialApn.idHistorialApn, paramLong);
  }

  private static final int jdoGetidSitp(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.idSitp;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.idSitp;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 25))
      return paramHistorialApn.idSitp;
    return localStateManager.getIntField(paramHistorialApn, jdoInheritedFieldCount + 25, paramHistorialApn.idSitp);
  }

  private static final void jdoSetidSitp(HistorialApn paramHistorialApn, int paramInt)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialApn, jdoInheritedFieldCount + 25, paramHistorialApn.idSitp, paramInt);
  }

  private static final String jdoGetlocalidad(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.localidad;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.localidad;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 26))
      return paramHistorialApn.localidad;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 26, paramHistorialApn.localidad);
  }

  private static final void jdoSetlocalidad(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.localidad = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.localidad = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 26, paramHistorialApn.localidad, paramString);
  }

  private static final String jdoGetnombreDependencia(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.nombreDependencia;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.nombreDependencia;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 27))
      return paramHistorialApn.nombreDependencia;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 27, paramHistorialApn.nombreDependencia);
  }

  private static final void jdoSetnombreDependencia(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.nombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.nombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 27, paramHistorialApn.nombreDependencia, paramString);
  }

  private static final String jdoGetnombreOrganismo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.nombreOrganismo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.nombreOrganismo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 28))
      return paramHistorialApn.nombreOrganismo;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 28, paramHistorialApn.nombreOrganismo);
  }

  private static final void jdoSetnombreOrganismo(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.nombreOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.nombreOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 28, paramHistorialApn.nombreOrganismo, paramString);
  }

  private static final String jdoGetnombreRegion(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.nombreRegion;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.nombreRegion;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 29))
      return paramHistorialApn.nombreRegion;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 29, paramHistorialApn.nombreRegion);
  }

  private static final void jdoSetnombreRegion(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.nombreRegion = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.nombreRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 29, paramHistorialApn.nombreRegion, paramString);
  }

  private static final String jdoGetnombreSede(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.nombreSede;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.nombreSede;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 30))
      return paramHistorialApn.nombreSede;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 30, paramHistorialApn.nombreSede);
  }

  private static final void jdoSetnombreSede(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.nombreSede = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.nombreSede = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 30, paramHistorialApn.nombreSede, paramString);
  }

  private static final int jdoGetnumeroMovimiento(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.numeroMovimiento;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.numeroMovimiento;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 31))
      return paramHistorialApn.numeroMovimiento;
    return localStateManager.getIntField(paramHistorialApn, jdoInheritedFieldCount + 31, paramHistorialApn.numeroMovimiento);
  }

  private static final void jdoSetnumeroMovimiento(HistorialApn paramHistorialApn, int paramInt)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.numeroMovimiento = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.numeroMovimiento = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialApn, jdoInheritedFieldCount + 31, paramHistorialApn.numeroMovimiento, paramInt);
  }

  private static final String jdoGetobservaciones(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.observaciones;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.observaciones;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 32))
      return paramHistorialApn.observaciones;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 32, paramHistorialApn.observaciones);
  }

  private static final void jdoSetobservaciones(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 32, paramHistorialApn.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(HistorialApn paramHistorialApn)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.organismo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 33))
      return paramHistorialApn.organismo;
    return (Organismo)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 33, paramHistorialApn.organismo);
  }

  private static final void jdoSetorganismo(HistorialApn paramHistorialApn, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 33, paramHistorialApn.organismo, paramOrganismo);
  }

  private static final String jdoGetorigenMovimiento(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.origenMovimiento;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.origenMovimiento;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 34))
      return paramHistorialApn.origenMovimiento;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 34, paramHistorialApn.origenMovimiento);
  }

  private static final void jdoSetorigenMovimiento(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.origenMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.origenMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 34, paramHistorialApn.origenMovimiento, paramString);
  }

  private static final int jdoGetpaso(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.paso;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.paso;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 35))
      return paramHistorialApn.paso;
    return localStateManager.getIntField(paramHistorialApn, jdoInheritedFieldCount + 35, paramHistorialApn.paso);
  }

  private static final void jdoSetpaso(HistorialApn paramHistorialApn, int paramInt)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistorialApn, jdoInheritedFieldCount + 35, paramHistorialApn.paso, paramInt);
  }

  private static final Personal jdoGetpersonal(HistorialApn paramHistorialApn)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.personal;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 36))
      return paramHistorialApn.personal;
    return (Personal)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 36, paramHistorialApn.personal);
  }

  private static final void jdoSetpersonal(HistorialApn paramHistorialApn, Personal paramPersonal)
  {
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 36, paramHistorialApn.personal, paramPersonal);
  }

  private static final double jdoGetprimasCargo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.primasCargo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.primasCargo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 37))
      return paramHistorialApn.primasCargo;
    return localStateManager.getDoubleField(paramHistorialApn, jdoInheritedFieldCount + 37, paramHistorialApn.primasCargo);
  }

  private static final void jdoSetprimasCargo(HistorialApn paramHistorialApn, double paramDouble)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.primasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.primasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialApn, jdoInheritedFieldCount + 37, paramHistorialApn.primasCargo, paramDouble);
  }

  private static final double jdoGetprimasTrabajador(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.primasTrabajador;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.primasTrabajador;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 38))
      return paramHistorialApn.primasTrabajador;
    return localStateManager.getDoubleField(paramHistorialApn, jdoInheritedFieldCount + 38, paramHistorialApn.primasTrabajador);
  }

  private static final void jdoSetprimasTrabajador(HistorialApn paramHistorialApn, double paramDouble)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.primasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.primasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialApn, jdoInheritedFieldCount + 38, paramHistorialApn.primasTrabajador, paramDouble);
  }

  private static final String jdoGetpuntoCuenta(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.puntoCuenta;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.puntoCuenta;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 39))
      return paramHistorialApn.puntoCuenta;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 39, paramHistorialApn.puntoCuenta);
  }

  private static final void jdoSetpuntoCuenta(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.puntoCuenta = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.puntoCuenta = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 39, paramHistorialApn.puntoCuenta, paramString);
  }

  private static final String jdoGetremesa(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.remesa;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.remesa;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 40))
      return paramHistorialApn.remesa;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 40, paramHistorialApn.remesa);
  }

  private static final void jdoSetremesa(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.remesa = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.remesa = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 40, paramHistorialApn.remesa, paramString);
  }

  private static final double jdoGetsueldo(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.sueldo;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.sueldo;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 41))
      return paramHistorialApn.sueldo;
    return localStateManager.getDoubleField(paramHistorialApn, jdoInheritedFieldCount + 41, paramHistorialApn.sueldo);
  }

  private static final void jdoSetsueldo(HistorialApn paramHistorialApn, double paramDouble)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.sueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.sueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistorialApn, jdoInheritedFieldCount + 41, paramHistorialApn.sueldo, paramDouble);
  }

  private static final Date jdoGettiempoSitp(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.tiempoSitp;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.tiempoSitp;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 42))
      return paramHistorialApn.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramHistorialApn, jdoInheritedFieldCount + 42, paramHistorialApn.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(HistorialApn paramHistorialApn, Date paramDate)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistorialApn, jdoInheritedFieldCount + 42, paramHistorialApn.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoPersonal(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.tipoPersonal;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.tipoPersonal;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 43))
      return paramHistorialApn.tipoPersonal;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 43, paramHistorialApn.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.tipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.tipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 43, paramHistorialApn.tipoPersonal, paramString);
  }

  private static final String jdoGetusuario(HistorialApn paramHistorialApn)
  {
    if (paramHistorialApn.jdoFlags <= 0)
      return paramHistorialApn.usuario;
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
      return paramHistorialApn.usuario;
    if (localStateManager.isLoaded(paramHistorialApn, jdoInheritedFieldCount + 44))
      return paramHistorialApn.usuario;
    return localStateManager.getStringField(paramHistorialApn, jdoInheritedFieldCount + 44, paramHistorialApn.usuario);
  }

  private static final void jdoSetusuario(HistorialApn paramHistorialApn, String paramString)
  {
    if (paramHistorialApn.jdoFlags == 0)
    {
      paramHistorialApn.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramHistorialApn.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistorialApn.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramHistorialApn, jdoInheritedFieldCount + 44, paramHistorialApn.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}