package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ExperienciaNoEstBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addExperienciaNoEst(ExperienciaNoEst experienciaNoEst)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ExperienciaNoEst experienciaNoEstNew = 
      (ExperienciaNoEst)BeanUtils.cloneBean(
      experienciaNoEst);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (experienciaNoEstNew.getPersonal() != null) {
      experienciaNoEstNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        experienciaNoEstNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(experienciaNoEstNew);
  }

  public void updateExperienciaNoEst(ExperienciaNoEst experienciaNoEst) throws Exception
  {
    ExperienciaNoEst experienciaNoEstModify = 
      findExperienciaNoEstById(experienciaNoEst.getIdExperienciaNoEst());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (experienciaNoEst.getPersonal() != null) {
      experienciaNoEst.setPersonal(
        personalBeanBusiness.findPersonalById(
        experienciaNoEst.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(experienciaNoEstModify, experienciaNoEst);
  }

  public void deleteExperienciaNoEst(ExperienciaNoEst experienciaNoEst) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ExperienciaNoEst experienciaNoEstDelete = 
      findExperienciaNoEstById(experienciaNoEst.getIdExperienciaNoEst());
    pm.deletePersistent(experienciaNoEstDelete);
  }

  public ExperienciaNoEst findExperienciaNoEstById(long idExperienciaNoEst) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idExperienciaNoEst == pIdExperienciaNoEst";
    Query query = pm.newQuery(ExperienciaNoEst.class, filter);

    query.declareParameters("long pIdExperienciaNoEst");

    parameters.put("pIdExperienciaNoEst", new Long(idExperienciaNoEst));

    Collection colExperienciaNoEst = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colExperienciaNoEst.iterator();
    return (ExperienciaNoEst)iterator.next();
  }

  public Collection findExperienciaNoEstAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent experienciaNoEstExtent = pm.getExtent(
      ExperienciaNoEst.class, true);
    Query query = pm.newQuery(experienciaNoEstExtent);
    query.setOrdering("fechaIngreso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ExperienciaNoEst.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaIngreso ascending");

    Collection colExperienciaNoEst = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExperienciaNoEst);

    return colExperienciaNoEst;
  }
}