package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class IdiomaPK
  implements Serializable
{
  public long idIdioma;

  public IdiomaPK()
  {
  }

  public IdiomaPK(long idIdioma)
  {
    this.idIdioma = idIdioma;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((IdiomaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(IdiomaPK thatPK)
  {
    return 
      this.idIdioma == thatPK.idIdioma;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idIdioma)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idIdioma);
  }
}