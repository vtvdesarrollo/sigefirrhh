package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Averiguacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_ORIGEN;
  protected static final Map LISTA_INHABILITACION;
  protected static final Map LISTA_SUSPENSION_SUELDO;
  private long idAveriguacion;
  private Date fechaProceso;
  private Date fechaFinProceso;
  private String estatus;
  private String origen;
  private String resolucion;
  private String observaciones;
  private String inhabilitado;
  private Date fechaInicio;
  private Date fechaFin;
  private String suspensionSueldo;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "estatus", "fechaFin", "fechaFinProceso", "fechaInicio", "fechaProceso", "idAveriguacion", "idSitp", "inhabilitado", "observaciones", "origen", "personal", "resolucion", "suspensionSueldo", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Averiguacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Averiguacion());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_ORIGEN = 
      new LinkedHashMap();
    LISTA_INHABILITACION = 
      new LinkedHashMap();
    LISTA_SUSPENSION_SUELDO = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("P", "PROCESO");
    LISTA_ESTATUS.put("C", "CERRADO");
    LISTA_ESTATUS.put("R", "RECHAZADO");
    LISTA_ORIGEN.put("I", "INTERNO");
    LISTA_ORIGEN.put("O", "OTRO");
    LISTA_ORIGEN.put("C", "CGR");
    LISTA_INHABILITACION.put("S", "SI");
    LISTA_INHABILITACION.put("N", "NO");
    LISTA_SUSPENSION_SUELDO.put("S", "SI");
    LISTA_SUSPENSION_SUELDO.put("N", "NO");
  }

  public Averiguacion()
  {
    jdoSetestatus(this, "P");

    jdoSetorigen(this, "C");

    jdoSetinhabilitado(this, "N");

    jdoSetsuspensionSueldo(this, "N");
  }

  public String toString()
  {
    return 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaProceso(this)) + " " + 
      LISTA_ESTATUS.get(jdoGetestatus(this));
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public long getIdAveriguacion()
  {
    return jdoGetidAveriguacion(this);
  }

  public String getInhabilitado()
  {
    return jdoGetinhabilitado(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public String getOrigen()
  {
    return jdoGetorigen(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public String getResolucion()
  {
    return jdoGetresolucion(this);
  }

  public String getSuspensionSueldo()
  {
    return jdoGetsuspensionSueldo(this);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setIdAveriguacion(long l)
  {
    jdoSetidAveriguacion(this, l);
  }

  public void setInhabilitado(String string)
  {
    jdoSetinhabilitado(this, string);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setOrigen(String string)
  {
    jdoSetorigen(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setResolucion(String string)
  {
    jdoSetresolucion(this, string);
  }

  public void setSuspensionSueldo(String string)
  {
    jdoSetsuspensionSueldo(this, string);
  }

  public Date getFechaFinProceso() {
    return jdoGetfechaFinProceso(this);
  }

  public Date getFechaProceso() {
    return jdoGetfechaProceso(this);
  }

  public void setFechaFinProceso(Date date) {
    jdoSetfechaFinProceso(this, date);
  }

  public void setFechaProceso(Date date) {
    jdoSetfechaProceso(this, date);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Averiguacion localAveriguacion = new Averiguacion();
    localAveriguacion.jdoFlags = 1;
    localAveriguacion.jdoStateManager = paramStateManager;
    return localAveriguacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Averiguacion localAveriguacion = new Averiguacion();
    localAveriguacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAveriguacion.jdoFlags = 1;
    localAveriguacion.jdoStateManager = paramStateManager;
    return localAveriguacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFinProceso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAveriguacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.inhabilitado);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.resolucion);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.suspensionSueldo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFinProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAveriguacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inhabilitado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resolucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.suspensionSueldo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Averiguacion paramAveriguacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramAveriguacion.estatus;
      return;
    case 1:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramAveriguacion.fechaFin;
      return;
    case 2:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFinProceso = paramAveriguacion.fechaFinProceso;
      return;
    case 3:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramAveriguacion.fechaInicio;
      return;
    case 4:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramAveriguacion.fechaProceso;
      return;
    case 5:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.idAveriguacion = paramAveriguacion.idAveriguacion;
      return;
    case 6:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramAveriguacion.idSitp;
      return;
    case 7:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.inhabilitado = paramAveriguacion.inhabilitado;
      return;
    case 8:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramAveriguacion.observaciones;
      return;
    case 9:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramAveriguacion.origen;
      return;
    case 10:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramAveriguacion.personal;
      return;
    case 11:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.resolucion = paramAveriguacion.resolucion;
      return;
    case 12:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.suspensionSueldo = paramAveriguacion.suspensionSueldo;
      return;
    case 13:
      if (paramAveriguacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramAveriguacion.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Averiguacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Averiguacion localAveriguacion = (Averiguacion)paramObject;
    if (localAveriguacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAveriguacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AveriguacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AveriguacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AveriguacionPK))
      throw new IllegalArgumentException("arg1");
    AveriguacionPK localAveriguacionPK = (AveriguacionPK)paramObject;
    localAveriguacionPK.idAveriguacion = this.idAveriguacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AveriguacionPK))
      throw new IllegalArgumentException("arg1");
    AveriguacionPK localAveriguacionPK = (AveriguacionPK)paramObject;
    this.idAveriguacion = localAveriguacionPK.idAveriguacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AveriguacionPK))
      throw new IllegalArgumentException("arg2");
    AveriguacionPK localAveriguacionPK = (AveriguacionPK)paramObject;
    localAveriguacionPK.idAveriguacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AveriguacionPK))
      throw new IllegalArgumentException("arg2");
    AveriguacionPK localAveriguacionPK = (AveriguacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localAveriguacionPK.idAveriguacion);
  }

  private static final String jdoGetestatus(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.estatus;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.estatus;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 0))
      return paramAveriguacion.estatus;
    return localStateManager.getStringField(paramAveriguacion, jdoInheritedFieldCount + 0, paramAveriguacion.estatus);
  }

  private static final void jdoSetestatus(Averiguacion paramAveriguacion, String paramString)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramAveriguacion, jdoInheritedFieldCount + 0, paramAveriguacion.estatus, paramString);
  }

  private static final Date jdoGetfechaFin(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.fechaFin;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.fechaFin;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 1))
      return paramAveriguacion.fechaFin;
    return (Date)localStateManager.getObjectField(paramAveriguacion, jdoInheritedFieldCount + 1, paramAveriguacion.fechaFin);
  }

  private static final void jdoSetfechaFin(Averiguacion paramAveriguacion, Date paramDate)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAveriguacion, jdoInheritedFieldCount + 1, paramAveriguacion.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaFinProceso(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.fechaFinProceso;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.fechaFinProceso;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 2))
      return paramAveriguacion.fechaFinProceso;
    return (Date)localStateManager.getObjectField(paramAveriguacion, jdoInheritedFieldCount + 2, paramAveriguacion.fechaFinProceso);
  }

  private static final void jdoSetfechaFinProceso(Averiguacion paramAveriguacion, Date paramDate)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.fechaFinProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.fechaFinProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAveriguacion, jdoInheritedFieldCount + 2, paramAveriguacion.fechaFinProceso, paramDate);
  }

  private static final Date jdoGetfechaInicio(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.fechaInicio;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.fechaInicio;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 3))
      return paramAveriguacion.fechaInicio;
    return (Date)localStateManager.getObjectField(paramAveriguacion, jdoInheritedFieldCount + 3, paramAveriguacion.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Averiguacion paramAveriguacion, Date paramDate)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAveriguacion, jdoInheritedFieldCount + 3, paramAveriguacion.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaProceso(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.fechaProceso;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.fechaProceso;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 4))
      return paramAveriguacion.fechaProceso;
    return (Date)localStateManager.getObjectField(paramAveriguacion, jdoInheritedFieldCount + 4, paramAveriguacion.fechaProceso);
  }

  private static final void jdoSetfechaProceso(Averiguacion paramAveriguacion, Date paramDate)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAveriguacion, jdoInheritedFieldCount + 4, paramAveriguacion.fechaProceso, paramDate);
  }

  private static final long jdoGetidAveriguacion(Averiguacion paramAveriguacion)
  {
    return paramAveriguacion.idAveriguacion;
  }

  private static final void jdoSetidAveriguacion(Averiguacion paramAveriguacion, long paramLong)
  {
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.idAveriguacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramAveriguacion, jdoInheritedFieldCount + 5, paramAveriguacion.idAveriguacion, paramLong);
  }

  private static final int jdoGetidSitp(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.idSitp;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.idSitp;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 6))
      return paramAveriguacion.idSitp;
    return localStateManager.getIntField(paramAveriguacion, jdoInheritedFieldCount + 6, paramAveriguacion.idSitp);
  }

  private static final void jdoSetidSitp(Averiguacion paramAveriguacion, int paramInt)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramAveriguacion, jdoInheritedFieldCount + 6, paramAveriguacion.idSitp, paramInt);
  }

  private static final String jdoGetinhabilitado(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.inhabilitado;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.inhabilitado;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 7))
      return paramAveriguacion.inhabilitado;
    return localStateManager.getStringField(paramAveriguacion, jdoInheritedFieldCount + 7, paramAveriguacion.inhabilitado);
  }

  private static final void jdoSetinhabilitado(Averiguacion paramAveriguacion, String paramString)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.inhabilitado = paramString;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.inhabilitado = paramString;
      return;
    }
    localStateManager.setStringField(paramAveriguacion, jdoInheritedFieldCount + 7, paramAveriguacion.inhabilitado, paramString);
  }

  private static final String jdoGetobservaciones(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.observaciones;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.observaciones;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 8))
      return paramAveriguacion.observaciones;
    return localStateManager.getStringField(paramAveriguacion, jdoInheritedFieldCount + 8, paramAveriguacion.observaciones);
  }

  private static final void jdoSetobservaciones(Averiguacion paramAveriguacion, String paramString)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramAveriguacion, jdoInheritedFieldCount + 8, paramAveriguacion.observaciones, paramString);
  }

  private static final String jdoGetorigen(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.origen;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.origen;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 9))
      return paramAveriguacion.origen;
    return localStateManager.getStringField(paramAveriguacion, jdoInheritedFieldCount + 9, paramAveriguacion.origen);
  }

  private static final void jdoSetorigen(Averiguacion paramAveriguacion, String paramString)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.origen = paramString;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramAveriguacion, jdoInheritedFieldCount + 9, paramAveriguacion.origen, paramString);
  }

  private static final Personal jdoGetpersonal(Averiguacion paramAveriguacion)
  {
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.personal;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 10))
      return paramAveriguacion.personal;
    return (Personal)localStateManager.getObjectField(paramAveriguacion, jdoInheritedFieldCount + 10, paramAveriguacion.personal);
  }

  private static final void jdoSetpersonal(Averiguacion paramAveriguacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramAveriguacion, jdoInheritedFieldCount + 10, paramAveriguacion.personal, paramPersonal);
  }

  private static final String jdoGetresolucion(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.resolucion;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.resolucion;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 11))
      return paramAveriguacion.resolucion;
    return localStateManager.getStringField(paramAveriguacion, jdoInheritedFieldCount + 11, paramAveriguacion.resolucion);
  }

  private static final void jdoSetresolucion(Averiguacion paramAveriguacion, String paramString)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.resolucion = paramString;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.resolucion = paramString;
      return;
    }
    localStateManager.setStringField(paramAveriguacion, jdoInheritedFieldCount + 11, paramAveriguacion.resolucion, paramString);
  }

  private static final String jdoGetsuspensionSueldo(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.suspensionSueldo;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.suspensionSueldo;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 12))
      return paramAveriguacion.suspensionSueldo;
    return localStateManager.getStringField(paramAveriguacion, jdoInheritedFieldCount + 12, paramAveriguacion.suspensionSueldo);
  }

  private static final void jdoSetsuspensionSueldo(Averiguacion paramAveriguacion, String paramString)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.suspensionSueldo = paramString;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.suspensionSueldo = paramString;
      return;
    }
    localStateManager.setStringField(paramAveriguacion, jdoInheritedFieldCount + 12, paramAveriguacion.suspensionSueldo, paramString);
  }

  private static final Date jdoGettiempoSitp(Averiguacion paramAveriguacion)
  {
    if (paramAveriguacion.jdoFlags <= 0)
      return paramAveriguacion.tiempoSitp;
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
      return paramAveriguacion.tiempoSitp;
    if (localStateManager.isLoaded(paramAveriguacion, jdoInheritedFieldCount + 13))
      return paramAveriguacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramAveriguacion, jdoInheritedFieldCount + 13, paramAveriguacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Averiguacion paramAveriguacion, Date paramDate)
  {
    if (paramAveriguacion.jdoFlags == 0)
    {
      paramAveriguacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramAveriguacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAveriguacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAveriguacion, jdoInheritedFieldCount + 13, paramAveriguacion.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}