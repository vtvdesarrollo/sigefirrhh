package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.CategoriaPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.RelacionPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ActualizarTrayectoriaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarTrayectoriaForm.class.getName());
  private Trayectoria trayectoria;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteNoGenFacade expedienteFacade = new ExpedienteNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private DefinicionesFacade defincionesFacade = new DefinicionesFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private Collection listRegion;
  private Collection listDependencia;
  private long idRegion;
  private String selectRegion;
  private long idDependencia;
  private Collection listMovimientoPersonal;
  private Collection listCausaMovimiento;
  private long idMovimientoPersonal;
  private String selectMovimientoPersonal;
  private long idCausaMovimiento;
  private Collection listManualCargo;
  private Collection listCargo;
  private long idManualCargo;
  private String selectManualCargo;
  private long idCargo;
  private Collection listClasificacionPersonal;
  private long idClasificacionPersonal;
  private Object stateResultPersonal = null;

  private Object stateResultTrayectoriaByPersonal = null;

  public Collection getResult()
  {
    return this.result;
  }

  public Trayectoria getTrayectoria() {
    if (this.trayectoria == null) {
      this.trayectoria = new Trayectoria();
    }
    return this.trayectoria;
  }

  public ActualizarTrayectoriaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public void refresh() {
    try {
      this.listRegion = this.estructuraFacade.findAllRegion();
      this.listMovimientoPersonal = this.registroFacade.findAllMovimientoPersonal();
      this.listManualCargo = this.cargoFacade.findAllManualCargo();
      this.listClasificacionPersonal = this.defincionesFacade.findAllClasificacionPersonal();
    }
    catch (Exception e) {
      this.listRegion = new ArrayList();
      this.listMovimientoPersonal = new ArrayList();
      this.listManualCargo = new ArrayList();
      this.listClasificacionPersonal = new ArrayList();
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findTrayectoriaByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findTrayectoriaByPersonal(
          this.personal.getIdPersonal(), "4", "S");
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrayectoria()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    String id = (String)requestParameterMap.get("idTrayectoria");
    log.error("id************************" + id);

    long idTrayectoria = 
      Long.parseLong((String)requestParameterMap.get("idTrayectoria"));
    try
    {
      this.trayectoria = 
        this.expedienteFacade.findTrayectoriaById(
        idTrayectoria);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.trayectoria.getFechaPreparacion() != null) && 
      (this.trayectoria.getFechaPreparacion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Preparación no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trayectoria.getFechaEstatus() != null) && 
      (this.trayectoria.getFechaEstatus().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Estatus no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.trayectoria.getFechaVigencia() != null) && 
      (this.trayectoria.getFechaVigencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Vigencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.trayectoria.setPersonal(
          this.personal);
        if (this.idRegion != 0L) {
          Region region = this.estructuraFacade.findRegionById(this.idRegion);
          this.trayectoria.setCodRegion(region.getCodRegion());
          this.trayectoria.setNombreRegion(region.getNombre());
        }
        if (this.idDependencia != 0L) {
          Dependencia dependencia = this.estructuraFacade.findDependenciaById(this.idDependencia);
          this.trayectoria.setCodDependencia(dependencia.getCodDependencia());
          this.trayectoria.setNombreDependencia(dependencia.getNombre());
        }
        if (this.idManualCargo != 0L) {
          ManualCargo manualCargo = this.cargoFacade.findManualCargoById(this.idManualCargo);
          this.trayectoria.setCodManualCargo(String.valueOf(manualCargo.getCodManualCargo()));
        }
        if (this.idCargo != 0L) {
          Cargo cargo = this.cargoFacade.findCargoById(this.idCargo);
          this.trayectoria.setCodCargo(cargo.getCodCargo());
          this.trayectoria.setDescripcionCargo(cargo.getDescripcionCargo());
        }
        if (this.idCausaMovimiento != 0L) {
          CausaMovimiento causaMovimiento = this.registroFacade.findCausaMovimientoById(this.idCausaMovimiento);
          this.trayectoria.setCodCausaMovimiento(causaMovimiento.getCodCausaMovimiento());
          this.trayectoria.setDescripcionMovimiento(causaMovimiento.getDescripcion());
        }
        if (this.idClasificacionPersonal != 0L) {
          ClasificacionPersonal clasificacionPersonal = this.defincionesFacade.findClasificacionPersonalById(this.idClasificacionPersonal);
          this.trayectoria.setCodCategoria(clasificacionPersonal.getCategoriaPersonal().getCodCategoria());
          this.trayectoria.setDescCategoria(clasificacionPersonal.getCategoriaPersonal().getDescCategoria());
          this.trayectoria.setCodRelacion(clasificacionPersonal.getRelacionPersonal().getCodRelacion());
          this.trayectoria.setDescRelacion(clasificacionPersonal.getRelacionPersonal().getDescRelacion());
        }

        this.trayectoria.setCedula(this.personal.getCedula());
        this.trayectoria.setPrimerApellido(this.personal.getPrimerApellido());
        this.trayectoria.setSegundoApellido(this.personal.getSegundoApellido());
        this.trayectoria.setPrimerNombre(this.personal.getPrimerNombre());
        this.trayectoria.setSegundoNombre(this.personal.getSegundoNombre());
        this.trayectoria.setOrigen("S");
        this.trayectoria.setUsuario(this.login.getUsuario());
        this.trayectoria.setEstatus("4");
        this.trayectoria.setCodOrganismo(this.login.getOrganismo().getCodOrganismo());
        this.trayectoria.setNombreCorto(this.login.getOrganismo().getNombreCorto());
        this.trayectoria.setNombreOrganismo(this.login.getOrganismo().getNombreOrganismo());

        this.expedienteFacade.addTrayectoria(
          this.trayectoria);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.trayectoria, this.personal);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateTrayectoria(
          this.trayectoria);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.trayectoria, this.personal);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteTrayectoria(
        this.trayectoria);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.trayectoria, this.personal);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;
    this.idCargo = 0L;
    this.idCausaMovimiento = 0L;
    this.idClasificacionPersonal = 0L;
    this.idDependencia = 0L;
    this.idManualCargo = 0L;
    this.idMovimientoPersonal = 0L;
    this.idRegion = 0L;

    this.selectManualCargo = null;
    this.selectMovimientoPersonal = null;
    this.selectRegion = null;

    this.trayectoria = new Trayectoria();
    this.trayectoria.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.trayectoria.setIdTrayectoria(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Trayectoria"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.trayectoria = new Trayectoria();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.trayectoria = new Trayectoria();
    return "cancel";
  }
  public boolean isShowDependencia() {
    return (this.listDependencia != null) && (!this.listDependencia.isEmpty());
  }

  public void changeRegion(ValueChangeEvent event) {
    this.idRegion = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.listDependencia = null;
    try {
      if (this.idRegion != 0L)
        this.listDependencia = this.estructuraFacade.findDependenciaByRegion(this.idRegion, this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowCausaMovimiento() { return (this.listCausaMovimiento != null) && (!this.listCausaMovimiento.isEmpty()); }

  public void changeMovimientoPersonal(ValueChangeEvent event)
  {
    this.idMovimientoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.listCausaMovimiento = null;
    try {
      if (this.idMovimientoPersonal != 0L)
        this.listCausaMovimiento = this.registroFacade.findCausaMovimientoByMovimientoPersonal(this.idMovimientoPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowCargo() { return (this.listCargo != null) && (!this.listCargo.isEmpty()); }

  public void changeManualCargo(ValueChangeEvent event)
  {
    this.idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.listCargo = null;
    try {
      if (this.idManualCargo != 0L)
        this.listCargo = this.cargoFacade.findCargoByManualCargo(this.idManualCargo);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getListRegion() { return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region"); }

  public Collection getListDependencia() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listDependencia, "sigefirrhh.base.estructura.Dependencia");
  }
  public Collection getListMovimientoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listMovimientoPersonal, "sigefirrhh.base.registro.MovimientoPersonal");
  }
  public Collection getListCausaMovimiento() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCausaMovimiento, "sigefirrhh.base.registro.CausaMovimiento");
  }
  public Collection getListManualCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listManualCargo, "sigefirrhh.base.cargo.ManualCargo");
  }
  public Collection getListCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCargo, "sigefirrhh.base.cargo.Cargo");
  }
  public Collection getListClasificacionPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listClasificacionPersonal, "sigefirrhh.base.definiciones.ClasificacionPersonal");
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }

  public LoginSession getLogin() {
    return this.login;
  }
  public long getIdDependencia() {
    return this.idDependencia;
  }
  public void setIdDependencia(long idDependencia) {
    this.idDependencia = idDependencia;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String selectRegion) {
    this.selectRegion = selectRegion;
  }

  public long getIdCausaMovimiento() {
    return this.idCausaMovimiento;
  }

  public void setIdCausaMovimiento(long idCausaMovimiento) {
    this.idCausaMovimiento = idCausaMovimiento;
  }

  public String getSelectMovimientoPersonal() {
    return this.selectMovimientoPersonal;
  }

  public void setSelectMovimientoPersonal(String selectMovimientoPersonal) {
    this.selectMovimientoPersonal = selectMovimientoPersonal;
  }

  public long getIdCargo() {
    return this.idCargo;
  }

  public void setIdCargo(long idCargo) {
    this.idCargo = idCargo;
  }

  public String getSelectManualCargo() {
    return this.selectManualCargo;
  }

  public void setSelectManualCargo(String selectManualCargo) {
    this.selectManualCargo = selectManualCargo;
  }

  public long getIdClasificacionPersonal() {
    return this.idClasificacionPersonal;
  }

  public void setIdClasificacionPersonal(long idClasificacionPersonal) {
    this.idClasificacionPersonal = idClasificacionPersonal;
  }
}