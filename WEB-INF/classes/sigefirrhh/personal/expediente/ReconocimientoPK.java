package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ReconocimientoPK
  implements Serializable
{
  public long idReconocimiento;

  public ReconocimientoPK()
  {
  }

  public ReconocimientoPK(long idReconocimiento)
  {
    this.idReconocimiento = idReconocimiento;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ReconocimientoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ReconocimientoPK thatPK)
  {
    return 
      this.idReconocimiento == thatPK.idReconocimiento;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idReconocimiento)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idReconocimiento);
  }
}