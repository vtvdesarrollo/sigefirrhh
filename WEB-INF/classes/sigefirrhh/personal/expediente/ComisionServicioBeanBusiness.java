package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.mre.SedeDiplomatica;
import sigefirrhh.base.mre.SedeDiplomaticaBeanBusiness;

public class ComisionServicioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addComisionServicio(ComisionServicio comisionServicio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ComisionServicio comisionServicioNew = 
      (ComisionServicio)BeanUtils.cloneBean(
      comisionServicio);

    SedeDiplomaticaBeanBusiness sedeDiplomaticaBeanBusiness = new SedeDiplomaticaBeanBusiness();

    if (comisionServicioNew.getSedeDiplomatica() != null) {
      comisionServicioNew.setSedeDiplomatica(
        sedeDiplomaticaBeanBusiness.findSedeDiplomaticaById(
        comisionServicioNew.getSedeDiplomatica().getIdSedeDiplomatica()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (comisionServicioNew.getPersonal() != null) {
      comisionServicioNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        comisionServicioNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(comisionServicioNew);
  }

  public void updateComisionServicio(ComisionServicio comisionServicio) throws Exception
  {
    ComisionServicio comisionServicioModify = 
      findComisionServicioById(comisionServicio.getIdComisionServicio());

    SedeDiplomaticaBeanBusiness sedeDiplomaticaBeanBusiness = new SedeDiplomaticaBeanBusiness();

    if (comisionServicio.getSedeDiplomatica() != null) {
      comisionServicio.setSedeDiplomatica(
        sedeDiplomaticaBeanBusiness.findSedeDiplomaticaById(
        comisionServicio.getSedeDiplomatica().getIdSedeDiplomatica()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (comisionServicio.getPersonal() != null) {
      comisionServicio.setPersonal(
        personalBeanBusiness.findPersonalById(
        comisionServicio.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(comisionServicioModify, comisionServicio);
  }

  public void deleteComisionServicio(ComisionServicio comisionServicio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ComisionServicio comisionServicioDelete = 
      findComisionServicioById(comisionServicio.getIdComisionServicio());
    pm.deletePersistent(comisionServicioDelete);
  }

  public ComisionServicio findComisionServicioById(long idComisionServicio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idComisionServicio == pIdComisionServicio";
    Query query = pm.newQuery(ComisionServicio.class, filter);

    query.declareParameters("long pIdComisionServicio");

    parameters.put("pIdComisionServicio", new Long(idComisionServicio));

    Collection colComisionServicio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colComisionServicio.iterator();
    return (ComisionServicio)iterator.next();
  }

  public Collection findComisionServicioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent comisionServicioExtent = pm.getExtent(
      ComisionServicio.class, true);
    Query query = pm.newQuery(comisionServicioExtent);
    query.setOrdering("nombreInstitucion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ComisionServicio.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("nombreInstitucion ascending");

    Collection colComisionServicio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colComisionServicio);

    return colComisionServicio;
  }
}