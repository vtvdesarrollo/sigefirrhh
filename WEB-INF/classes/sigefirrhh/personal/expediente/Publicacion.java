package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Publicacion
  implements Serializable, PersistenceCapable
{
  private long idPublicacion;
  private int anioPublicacion;
  private String titulo;
  private String editorial;
  private String propiedadIntelectual;
  private Personal personal;
  private String observaciones;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anioPublicacion", "editorial", "idPublicacion", "idSitp", "observaciones", "personal", "propiedadIntelectual", "tiempoSitp", "titulo" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 26, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return String.valueOf(jdoGetanioPublicacion(this));
  }

  public int getAnioPublicacion()
  {
    return jdoGetanioPublicacion(this);
  }

  public String getEditorial()
  {
    return jdoGeteditorial(this);
  }

  public long getIdPublicacion()
  {
    return jdoGetidPublicacion(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public String getTitulo()
  {
    return jdoGettitulo(this);
  }

  public void setAnioPublicacion(int i)
  {
    jdoSetanioPublicacion(this, i);
  }

  public void setEditorial(String string)
  {
    jdoSeteditorial(this, string);
  }

  public void setIdPublicacion(long l)
  {
    jdoSetidPublicacion(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setTitulo(String string)
  {
    jdoSettitulo(this, string);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public String getPropiedadIntelectual()
  {
    return jdoGetpropiedadIntelectual(this);
  }

  public void setPropiedadIntelectual(String string)
  {
    jdoSetpropiedadIntelectual(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Publicacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Publicacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Publicacion localPublicacion = new Publicacion();
    localPublicacion.jdoFlags = 1;
    localPublicacion.jdoStateManager = paramStateManager;
    return localPublicacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Publicacion localPublicacion = new Publicacion();
    localPublicacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPublicacion.jdoFlags = 1;
    localPublicacion.jdoStateManager = paramStateManager;
    return localPublicacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioPublicacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.editorial);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPublicacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.propiedadIntelectual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.titulo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioPublicacion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.editorial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPublicacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.propiedadIntelectual = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.titulo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Publicacion paramPublicacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.anioPublicacion = paramPublicacion.anioPublicacion;
      return;
    case 1:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.editorial = paramPublicacion.editorial;
      return;
    case 2:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.idPublicacion = paramPublicacion.idPublicacion;
      return;
    case 3:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPublicacion.idSitp;
      return;
    case 4:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramPublicacion.observaciones;
      return;
    case 5:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramPublicacion.personal;
      return;
    case 6:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.propiedadIntelectual = paramPublicacion.propiedadIntelectual;
      return;
    case 7:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPublicacion.tiempoSitp;
      return;
    case 8:
      if (paramPublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.titulo = paramPublicacion.titulo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Publicacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Publicacion localPublicacion = (Publicacion)paramObject;
    if (localPublicacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPublicacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PublicacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PublicacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PublicacionPK))
      throw new IllegalArgumentException("arg1");
    PublicacionPK localPublicacionPK = (PublicacionPK)paramObject;
    localPublicacionPK.idPublicacion = this.idPublicacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PublicacionPK))
      throw new IllegalArgumentException("arg1");
    PublicacionPK localPublicacionPK = (PublicacionPK)paramObject;
    this.idPublicacion = localPublicacionPK.idPublicacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PublicacionPK))
      throw new IllegalArgumentException("arg2");
    PublicacionPK localPublicacionPK = (PublicacionPK)paramObject;
    localPublicacionPK.idPublicacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PublicacionPK))
      throw new IllegalArgumentException("arg2");
    PublicacionPK localPublicacionPK = (PublicacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localPublicacionPK.idPublicacion);
  }

  private static final int jdoGetanioPublicacion(Publicacion paramPublicacion)
  {
    if (paramPublicacion.jdoFlags <= 0)
      return paramPublicacion.anioPublicacion;
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.anioPublicacion;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 0))
      return paramPublicacion.anioPublicacion;
    return localStateManager.getIntField(paramPublicacion, jdoInheritedFieldCount + 0, paramPublicacion.anioPublicacion);
  }

  private static final void jdoSetanioPublicacion(Publicacion paramPublicacion, int paramInt)
  {
    if (paramPublicacion.jdoFlags == 0)
    {
      paramPublicacion.anioPublicacion = paramInt;
      return;
    }
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.anioPublicacion = paramInt;
      return;
    }
    localStateManager.setIntField(paramPublicacion, jdoInheritedFieldCount + 0, paramPublicacion.anioPublicacion, paramInt);
  }

  private static final String jdoGeteditorial(Publicacion paramPublicacion)
  {
    if (paramPublicacion.jdoFlags <= 0)
      return paramPublicacion.editorial;
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.editorial;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 1))
      return paramPublicacion.editorial;
    return localStateManager.getStringField(paramPublicacion, jdoInheritedFieldCount + 1, paramPublicacion.editorial);
  }

  private static final void jdoSeteditorial(Publicacion paramPublicacion, String paramString)
  {
    if (paramPublicacion.jdoFlags == 0)
    {
      paramPublicacion.editorial = paramString;
      return;
    }
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.editorial = paramString;
      return;
    }
    localStateManager.setStringField(paramPublicacion, jdoInheritedFieldCount + 1, paramPublicacion.editorial, paramString);
  }

  private static final long jdoGetidPublicacion(Publicacion paramPublicacion)
  {
    return paramPublicacion.idPublicacion;
  }

  private static final void jdoSetidPublicacion(Publicacion paramPublicacion, long paramLong)
  {
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.idPublicacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramPublicacion, jdoInheritedFieldCount + 2, paramPublicacion.idPublicacion, paramLong);
  }

  private static final int jdoGetidSitp(Publicacion paramPublicacion)
  {
    if (paramPublicacion.jdoFlags <= 0)
      return paramPublicacion.idSitp;
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.idSitp;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 3))
      return paramPublicacion.idSitp;
    return localStateManager.getIntField(paramPublicacion, jdoInheritedFieldCount + 3, paramPublicacion.idSitp);
  }

  private static final void jdoSetidSitp(Publicacion paramPublicacion, int paramInt)
  {
    if (paramPublicacion.jdoFlags == 0)
    {
      paramPublicacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPublicacion, jdoInheritedFieldCount + 3, paramPublicacion.idSitp, paramInt);
  }

  private static final String jdoGetobservaciones(Publicacion paramPublicacion)
  {
    if (paramPublicacion.jdoFlags <= 0)
      return paramPublicacion.observaciones;
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.observaciones;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 4))
      return paramPublicacion.observaciones;
    return localStateManager.getStringField(paramPublicacion, jdoInheritedFieldCount + 4, paramPublicacion.observaciones);
  }

  private static final void jdoSetobservaciones(Publicacion paramPublicacion, String paramString)
  {
    if (paramPublicacion.jdoFlags == 0)
    {
      paramPublicacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramPublicacion, jdoInheritedFieldCount + 4, paramPublicacion.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Publicacion paramPublicacion)
  {
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.personal;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 5))
      return paramPublicacion.personal;
    return (Personal)localStateManager.getObjectField(paramPublicacion, jdoInheritedFieldCount + 5, paramPublicacion.personal);
  }

  private static final void jdoSetpersonal(Publicacion paramPublicacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramPublicacion, jdoInheritedFieldCount + 5, paramPublicacion.personal, paramPersonal);
  }

  private static final String jdoGetpropiedadIntelectual(Publicacion paramPublicacion)
  {
    if (paramPublicacion.jdoFlags <= 0)
      return paramPublicacion.propiedadIntelectual;
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.propiedadIntelectual;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 6))
      return paramPublicacion.propiedadIntelectual;
    return localStateManager.getStringField(paramPublicacion, jdoInheritedFieldCount + 6, paramPublicacion.propiedadIntelectual);
  }

  private static final void jdoSetpropiedadIntelectual(Publicacion paramPublicacion, String paramString)
  {
    if (paramPublicacion.jdoFlags == 0)
    {
      paramPublicacion.propiedadIntelectual = paramString;
      return;
    }
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.propiedadIntelectual = paramString;
      return;
    }
    localStateManager.setStringField(paramPublicacion, jdoInheritedFieldCount + 6, paramPublicacion.propiedadIntelectual, paramString);
  }

  private static final Date jdoGettiempoSitp(Publicacion paramPublicacion)
  {
    if (paramPublicacion.jdoFlags <= 0)
      return paramPublicacion.tiempoSitp;
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.tiempoSitp;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 7))
      return paramPublicacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPublicacion, jdoInheritedFieldCount + 7, paramPublicacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Publicacion paramPublicacion, Date paramDate)
  {
    if (paramPublicacion.jdoFlags == 0)
    {
      paramPublicacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPublicacion, jdoInheritedFieldCount + 7, paramPublicacion.tiempoSitp, paramDate);
  }

  private static final String jdoGettitulo(Publicacion paramPublicacion)
  {
    if (paramPublicacion.jdoFlags <= 0)
      return paramPublicacion.titulo;
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramPublicacion.titulo;
    if (localStateManager.isLoaded(paramPublicacion, jdoInheritedFieldCount + 8))
      return paramPublicacion.titulo;
    return localStateManager.getStringField(paramPublicacion, jdoInheritedFieldCount + 8, paramPublicacion.titulo);
  }

  private static final void jdoSettitulo(Publicacion paramPublicacion, String paramString)
  {
    if (paramPublicacion.jdoFlags == 0)
    {
      paramPublicacion.titulo = paramString;
      return;
    }
    StateManager localStateManager = paramPublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPublicacion.titulo = paramString;
      return;
    }
    localStateManager.setStringField(paramPublicacion, jdoInheritedFieldCount + 8, paramPublicacion.titulo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}