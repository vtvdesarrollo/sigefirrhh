package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class PublicacionPK
  implements Serializable
{
  public long idPublicacion;

  public PublicacionPK()
  {
  }

  public PublicacionPK(long idPublicacion)
  {
    this.idPublicacion = idPublicacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PublicacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PublicacionPK thatPK)
  {
    return 
      this.idPublicacion == thatPK.idPublicacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPublicacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPublicacion);
  }
}