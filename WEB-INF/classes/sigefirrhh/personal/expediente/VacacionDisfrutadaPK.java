package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class VacacionDisfrutadaPK
  implements Serializable
{
  public long idVacacionDisfrutada;

  public VacacionDisfrutadaPK()
  {
  }

  public VacacionDisfrutadaPK(long idVacacionDisfrutada)
  {
    this.idVacacionDisfrutada = idVacacionDisfrutada;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((VacacionDisfrutadaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(VacacionDisfrutadaPK thatPK)
  {
    return 
      this.idVacacionDisfrutada == thatPK.idVacacionDisfrutada;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idVacacionDisfrutada)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idVacacionDisfrutada);
  }
}