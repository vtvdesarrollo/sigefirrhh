package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.AreaConocimientoBeanBusiness;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.adiestramiento.TipoCursoBeanBusiness;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.PaisBeanBusiness;

public class EstudioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEstudio(Estudio estudio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Estudio estudioNew = 
      (Estudio)BeanUtils.cloneBean(
      estudio);

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (estudioNew.getTipoCurso() != null) {
      estudioNew.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        estudioNew.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (estudioNew.getAreaConocimiento() != null) {
      estudioNew.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        estudioNew.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PaisBeanBusiness paisBeanBusiness = new PaisBeanBusiness();

    if (estudioNew.getPais() != null) {
      estudioNew.setPais(
        paisBeanBusiness.findPaisById(
        estudioNew.getPais().getIdPais()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (estudioNew.getPersonal() != null) {
      estudioNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        estudioNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(estudioNew);
  }

  public void updateEstudio(Estudio estudio) throws Exception
  {
    Estudio estudioModify = 
      findEstudioById(estudio.getIdEstudio());

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (estudio.getTipoCurso() != null) {
      estudio.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        estudio.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (estudio.getAreaConocimiento() != null) {
      estudio.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        estudio.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PaisBeanBusiness paisBeanBusiness = new PaisBeanBusiness();

    if (estudio.getPais() != null) {
      estudio.setPais(
        paisBeanBusiness.findPaisById(
        estudio.getPais().getIdPais()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (estudio.getPersonal() != null) {
      estudio.setPersonal(
        personalBeanBusiness.findPersonalById(
        estudio.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(estudioModify, estudio);
  }

  public void deleteEstudio(Estudio estudio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Estudio estudioDelete = 
      findEstudioById(estudio.getIdEstudio());
    pm.deletePersistent(estudioDelete);
  }

  public Estudio findEstudioById(long idEstudio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEstudio == pIdEstudio";
    Query query = pm.newQuery(Estudio.class, filter);

    query.declareParameters("long pIdEstudio");

    parameters.put("pIdEstudio", new Long(idEstudio));

    Collection colEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEstudio.iterator();
    return (Estudio)iterator.next();
  }

  public Collection findEstudioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent estudioExtent = pm.getExtent(
      Estudio.class, true);
    Query query = pm.newQuery(estudioExtent);
    query.setOrdering("anio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoCurso(long idTipoCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoCurso.idTipoCurso == pIdTipoCurso";

    Query query = pm.newQuery(Estudio.class, filter);

    query.declareParameters("long pIdTipoCurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoCurso", new Long(idTipoCurso));

    query.setOrdering("anio ascending");

    Collection colEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstudio);

    return colEstudio;
  }

  public Collection findByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "areaConocimiento.idAreaConocimiento == pIdAreaConocimiento";

    Query query = pm.newQuery(Estudio.class, filter);

    query.declareParameters("long pIdAreaConocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdAreaConocimiento", new Long(idAreaConocimiento));

    query.setOrdering("anio ascending");

    Collection colEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstudio);

    return colEstudio;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Estudio.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending");

    Collection colEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEstudio);

    return colEstudio;
  }
}