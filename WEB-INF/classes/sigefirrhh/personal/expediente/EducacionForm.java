package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.Titulo;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class EducacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EducacionForm.class.getName());
  private Educacion educacion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private PersonalFacade personalFacade = new PersonalFacade();
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colNivelEducativo;
  private Collection colCarrera;
  private Collection colNivelEducativoForTitulo;
  private Collection colTitulo;
  private Collection colPaisForCiudad;
  private Collection colEstadoForCiudad;
  private Collection colCiudad;
  private Collection colPersonal;
  private String selectNivelEducativo;
  private String selectCarrera;
  private String selectNivelEducativoForTitulo;
  private String selectTitulo;
  private String selectPaisForCiudad;
  private String selectEstadoForCiudad;
  private String selectCiudad;
  private String selectPersonal;
  private Object stateScrollPersonal = null;
  private Object stateResultPersonal = null;

  private Object stateScrollEducacionByPersonal = null;
  private Object stateResultEducacionByPersonal = null;

  public String getSelectNivelEducativo()
  {
    return this.selectNivelEducativo;
  }
  public void setSelectNivelEducativo(String valNivelEducativo) {
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    this.educacion.setNivelEducativo(null);
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      if (String.valueOf(nivelEducativo.getIdNivelEducativo()).equals(
        valNivelEducativo)) {
        this.educacion.setNivelEducativo(
          nivelEducativo);
      }
    }
    this.selectNivelEducativo = valNivelEducativo;
  }
  public String getSelectCarrera() {
    return this.selectCarrera;
  }
  public void setSelectCarrera(String valCarrera) {
    Iterator iterator = this.colCarrera.iterator();
    Carrera carrera = null;
    this.educacion.setCarrera(null);
    while (iterator.hasNext()) {
      carrera = (Carrera)iterator.next();
      if (String.valueOf(carrera.getIdCarrera()).equals(
        valCarrera)) {
        this.educacion.setCarrera(
          carrera);
      }
    }
    this.selectCarrera = valCarrera;
  }
  public String getSelectNivelEducativoForTitulo() {
    return this.selectNivelEducativoForTitulo;
  }
  public void setSelectNivelEducativoForTitulo(String valNivelEducativoForTitulo) {
    this.selectNivelEducativoForTitulo = valNivelEducativoForTitulo;
  }
  public void changeNivelEducativoForTitulo(ValueChangeEvent event) {
    long idNivelEducativo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colTitulo = null;
      if (idNivelEducativo > 0L)
        this.colTitulo = 
          this.personalFacade.findTituloByNivelEducativo(
          idNivelEducativo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowNivelEducativoForTitulo() { return this.colNivelEducativoForTitulo != null; }

  public String getSelectTitulo() {
    return this.selectTitulo;
  }
  public void setSelectTitulo(String valTitulo) {
    Iterator iterator = this.colTitulo.iterator();
    Titulo titulo = null;
    this.educacion.setTitulo(null);
    while (iterator.hasNext()) {
      titulo = (Titulo)iterator.next();
      if (String.valueOf(titulo.getIdTitulo()).equals(
        valTitulo)) {
        this.educacion.setTitulo(
          titulo);
      }
    }
    this.selectTitulo = valTitulo;
  }
  public boolean isShowTitulo() {
    return this.colTitulo != null;
  }
  public String getSelectPaisForCiudad() {
    return this.selectPaisForCiudad;
  }
  public void setSelectPaisForCiudad(String valPaisForCiudad) {
    this.selectPaisForCiudad = valPaisForCiudad;
  }
  public void changePaisForCiudad(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      this.colEstadoForCiudad = null;
      if (idPais > 0L)
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowPaisForCiudad() { return this.colPaisForCiudad != null; }

  public String getSelectEstadoForCiudad() {
    return this.selectEstadoForCiudad;
  }
  public void setSelectEstadoForCiudad(String valEstadoForCiudad) {
    this.selectEstadoForCiudad = valEstadoForCiudad;
  }
  public void changeEstadoForCiudad(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      if (idEstado > 0L)
        this.colCiudad = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowEstadoForCiudad() { return this.colEstadoForCiudad != null; }

  public String getSelectCiudad() {
    return this.selectCiudad;
  }
  public void setSelectCiudad(String valCiudad) {
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    this.educacion.setCiudad(null);
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      if (String.valueOf(ciudad.getIdCiudad()).equals(
        valCiudad)) {
        this.educacion.setCiudad(
          ciudad);
      }
    }
    this.selectCiudad = valCiudad;
  }
  public boolean isShowCiudad() {
    return this.colCiudad != null;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.educacion.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.educacion.setPersonal(
          personal);
      }
    }
    this.selectPersonal = valPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public Educacion getEducacion() {
    if (this.educacion == null) {
      this.educacion = new Educacion();
    }
    return this.educacion;
  }

  public EducacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColNivelEducativo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativo.getIdNivelEducativo()), 
        nivelEducativo.toString()));
    }
    return col;
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Educacion.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColCarrera()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCarrera.iterator();
    Carrera carrera = null;
    while (iterator.hasNext()) {
      carrera = (Carrera)iterator.next();
      col.add(new SelectItem(
        String.valueOf(carrera.getIdCarrera()), 
        carrera.toString()));
    }
    return col;
  }

  public Collection getColNivelEducativoForTitulo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativoForTitulo.iterator();
    NivelEducativo nivelEducativoForTitulo = null;
    while (iterator.hasNext()) {
      nivelEducativoForTitulo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativoForTitulo.getIdNivelEducativo()), 
        nivelEducativoForTitulo.toString()));
    }
    return col;
  }

  public Collection getColTitulo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTitulo.iterator();
    Titulo titulo = null;
    while (iterator.hasNext()) {
      titulo = (Titulo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(titulo.getIdTitulo()), 
        titulo.toString()));
    }
    return col;
  }

  public Collection getListRegistroTitulo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Educacion.LISTA_REGISTRO_TITULO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudad.iterator();
    Pais paisForCiudad = null;
    while (iterator.hasNext()) {
      paisForCiudad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudad.getIdPais()), 
        paisForCiudad.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudad.iterator();
    Estado estadoForCiudad = null;
    while (iterator.hasNext()) {
      estadoForCiudad = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudad.getIdEstado()), 
        estadoForCiudad.toString()));
    }
    return col;
  }

  public Collection getColCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudad.getIdCiudad()), 
        ciudad.toString()));
    }
    return col;
  }

  public Collection getListSector() {
    Collection col = new ArrayList();

    Iterator iterEntry = Educacion.LISTA_SECTOR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListBecado() {
    Collection col = new ArrayList();

    Iterator iterEntry = Educacion.LISTA_BECADO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListReembolso()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Educacion.LISTA_REEMBOLSO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colNivelEducativo = 
        this.personalFacade.findAllNivelEducativo();
      this.colCarrera = 
        this.personalFacade.findAllCarrera();
      this.colNivelEducativoForTitulo = 
        this.personalFacade.findAllNivelEducativo();
      this.colPaisForCiudad = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findEducacionByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findEducacionByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectEducacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectNivelEducativo = null;
    this.selectCarrera = null;
    this.selectTitulo = null;
    this.selectNivelEducativoForTitulo = null;

    this.selectCiudad = null;
    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    this.selectPersonal = null;

    long idEducacion = 
      Long.parseLong((String)requestParameterMap.get("idEducacion"));
    try
    {
      this.educacion = 
        this.expedienteFacade.findEducacionById(
        idEducacion);

      if (this.educacion.getNivelEducativo() != null) {
        this.selectNivelEducativo = 
          String.valueOf(this.educacion.getNivelEducativo().getIdNivelEducativo());
      }
      if (this.educacion.getCarrera() != null) {
        this.selectCarrera = 
          String.valueOf(this.educacion.getCarrera().getIdCarrera());
      }
      if (this.educacion.getTitulo() != null) {
        this.selectTitulo = 
          String.valueOf(this.educacion.getTitulo().getIdTitulo());
      }
      if (this.educacion.getCiudad() != null) {
        this.selectCiudad = 
          String.valueOf(this.educacion.getCiudad().getIdCiudad());
      }
      if (this.educacion.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.educacion.getPersonal().getIdPersonal());
      }

      Titulo titulo = null;
      NivelEducativo nivelEducativoForTitulo = null;
      Ciudad ciudad = null;
      Estado estadoForCiudad = null;
      Pais paisForCiudad = null;

      if (this.educacion.getTitulo() != null) {
        long idTitulo = 
          this.educacion.getTitulo().getIdTitulo();
        this.selectTitulo = String.valueOf(idTitulo);
        titulo = this.personalFacade.findTituloById(
          idTitulo);
        this.colTitulo = this.personalFacade.findTituloByNivelEducativo(
          titulo.getNivelEducativo().getIdNivelEducativo());

        long idNivelEducativoForTitulo = 
          this.educacion.getTitulo().getNivelEducativo().getIdNivelEducativo();
        this.selectNivelEducativoForTitulo = String.valueOf(idNivelEducativoForTitulo);
        nivelEducativoForTitulo = 
          this.personalFacade.findNivelEducativoById(
          idNivelEducativoForTitulo);
        this.colNivelEducativoForTitulo = 
          this.personalFacade.findAllNivelEducativo();
      }
      if (this.educacion.getCiudad() != null) {
        long idCiudad = 
          this.educacion.getCiudad().getIdCiudad();
        this.selectCiudad = String.valueOf(idCiudad);
        ciudad = this.ubicacionFacade.findCiudadById(
          idCiudad);
        this.colCiudad = this.ubicacionFacade.findCiudadByEstado(
          ciudad.getEstado().getIdEstado());

        long idEstadoForCiudad = 
          this.educacion.getCiudad().getEstado().getIdEstado();
        this.selectEstadoForCiudad = String.valueOf(idEstadoForCiudad);
        estadoForCiudad = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudad);
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudad.getPais().getIdPais());
        long idPaisForCiudad = 
          estadoForCiudad.getPais().getIdPais();
        this.selectPaisForCiudad = String.valueOf(idPaisForCiudad);
        paisForCiudad = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudad);
        this.colPaisForCiudad = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    if ((!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("T")) && (!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("U")) && (!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("S"))) {
      this.educacion.setCarrera(null);
    }
    if ((!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("E")) && (!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("M")) && (!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("C")) && (!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("R")) && (!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("G")) && (!this.educacion.getNivelEducativo().getCodNivelEducativo().equals("L"))) {
      this.educacion.setNombrePostgrado(null);
    }
    if ((this.educacion.getNivelEducativo().getCodNivelEducativo().equals("I")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("B")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("P"))) {
      this.educacion.setRegistroTitulo("N");
      this.educacion.setFechaRegistro(null);
    }

    if ((this.educacion.getFechaRegistro() != null) && 
      (this.educacion.getFechaRegistro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Registro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.educacion.getTiempoSitp() != null) && 
      (this.educacion.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    try
    {
      if (this.educacion.getNivelEducativo().getIdNivelEducativo() != 16L)
      {
        if (this.educacion.getAnioInicio() <= 0) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año Inicio tiene un valor no valido", ""));
          error = true;
        }
        if (this.educacion.getAnioInicio() > new Date().getYear() + 1900) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año Inicio tiene un valor no valido", ""));
          error = true;
        }
        if ((this.educacion.getAnioFin() != 0) && ((this.educacion.getAnioInicio() >= this.educacion.getAnioFin()) || (this.educacion.getAnioFin() > new Date().getYear() + 1900))) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año Fin tiene un valor no valido", ""));
          error = true;
        }
        if ((this.educacion.getFechaRegistro() != null) && (this.educacion.getFechaRegistro().getYear() + 1900 < this.educacion.getAnioFin())) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha Registro tiene un valor no valido", ""));
          error = true;
        }
      }
    }
    catch (Exception localException1) {
    }
    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.educacion.setPersonal(
          this.personal);
        this.expedienteFacade.addEducacion(
          this.educacion);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.educacion, this.personal);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateEducacion(
          this.educacion);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.educacion, this.personal);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }

      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteEducacion(
        this.educacion);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.educacion, this.personal);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectNivelEducativo = null;

    this.selectCarrera = null;

    this.selectTitulo = null;

    this.selectNivelEducativoForTitulo = null;

    this.selectCiudad = null;

    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    this.selectPersonal = null;

    this.educacion = new Educacion();

    this.educacion.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.educacion.setIdEducacion(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Educacion"));

    return null;
  }

  public boolean isShowCarreraAux() {
    try {
      return (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("T")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("S")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTituloAux() {
    try {
      return (this.educacion.getEstatus().equals("F")) && ((this.educacion.getNivelEducativo().getCodNivelEducativo().equals("T")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("S")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("U")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("D")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("H"))); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowRegistroTituloAux() {
    try {
      return this.educacion.getEstatus().equals("F"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFechaRegistroAux() {
    try {
      return this.educacion.getRegistroTitulo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNombrePostgradoAux() {
    try {
      return (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("E")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("M")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("C")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("R")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("G")) || (this.educacion.getNivelEducativo().getCodNivelEducativo().equals("L")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowOrganizacionBecariaAux() {
    try {
      return this.educacion.getBecado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.educacion = new Educacion();
    return "cancel";
  }
  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.educacion = new Educacion();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }
}