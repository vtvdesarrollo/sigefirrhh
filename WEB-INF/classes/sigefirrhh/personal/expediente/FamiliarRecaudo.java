package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.RelacionRecaudo;

public class FamiliarRecaudo
  implements Serializable, PersistenceCapable
{
  private long idFamiliarRecaudo;
  private Date fechaRecaudo;
  private RelacionRecaudo relacionRecaudo;
  private Familiar familiar;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "familiar", "fechaRecaudo", "idFamiliarRecaudo", "relacionRecaudo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.RelacionRecaudo") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetfamiliar(this).getPrimerApellido() + " " + 
      jdoGetfamiliar(this).getPrimerNombre() + " " + 
      jdoGetfamiliar(this).getParentesco() + " " + 
      jdoGetrelacionRecaudo(this).getRecaudo() + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaRecaudo(this));
  }

  public Familiar getFamiliar()
  {
    return jdoGetfamiliar(this);
  }

  public Date getFechaRecaudo()
  {
    return jdoGetfechaRecaudo(this);
  }

  public long getIdFamiliarRecaudo()
  {
    return jdoGetidFamiliarRecaudo(this);
  }

  public RelacionRecaudo getRelacionRecaudo()
  {
    return jdoGetrelacionRecaudo(this);
  }

  public void setFamiliar(Familiar familiar)
  {
    jdoSetfamiliar(this, familiar);
  }

  public void setFechaRecaudo(Date date)
  {
    jdoSetfechaRecaudo(this, date);
  }

  public void setIdFamiliarRecaudo(long l)
  {
    jdoSetidFamiliarRecaudo(this, l);
  }

  public void setRelacionRecaudo(RelacionRecaudo recaudo)
  {
    jdoSetrelacionRecaudo(this, recaudo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.FamiliarRecaudo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new FamiliarRecaudo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    FamiliarRecaudo localFamiliarRecaudo = new FamiliarRecaudo();
    localFamiliarRecaudo.jdoFlags = 1;
    localFamiliarRecaudo.jdoStateManager = paramStateManager;
    return localFamiliarRecaudo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    FamiliarRecaudo localFamiliarRecaudo = new FamiliarRecaudo();
    localFamiliarRecaudo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFamiliarRecaudo.jdoFlags = 1;
    localFamiliarRecaudo.jdoStateManager = paramStateManager;
    return localFamiliarRecaudo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRecaudo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFamiliarRecaudo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.relacionRecaudo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRecaudo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFamiliarRecaudo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.relacionRecaudo = ((RelacionRecaudo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(FamiliarRecaudo paramFamiliarRecaudo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFamiliarRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramFamiliarRecaudo.familiar;
      return;
    case 1:
      if (paramFamiliarRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRecaudo = paramFamiliarRecaudo.fechaRecaudo;
      return;
    case 2:
      if (paramFamiliarRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.idFamiliarRecaudo = paramFamiliarRecaudo.idFamiliarRecaudo;
      return;
    case 3:
      if (paramFamiliarRecaudo == null)
        throw new IllegalArgumentException("arg1");
      this.relacionRecaudo = paramFamiliarRecaudo.relacionRecaudo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof FamiliarRecaudo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    FamiliarRecaudo localFamiliarRecaudo = (FamiliarRecaudo)paramObject;
    if (localFamiliarRecaudo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFamiliarRecaudo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FamiliarRecaudoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FamiliarRecaudoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FamiliarRecaudoPK))
      throw new IllegalArgumentException("arg1");
    FamiliarRecaudoPK localFamiliarRecaudoPK = (FamiliarRecaudoPK)paramObject;
    localFamiliarRecaudoPK.idFamiliarRecaudo = this.idFamiliarRecaudo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FamiliarRecaudoPK))
      throw new IllegalArgumentException("arg1");
    FamiliarRecaudoPK localFamiliarRecaudoPK = (FamiliarRecaudoPK)paramObject;
    this.idFamiliarRecaudo = localFamiliarRecaudoPK.idFamiliarRecaudo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FamiliarRecaudoPK))
      throw new IllegalArgumentException("arg2");
    FamiliarRecaudoPK localFamiliarRecaudoPK = (FamiliarRecaudoPK)paramObject;
    localFamiliarRecaudoPK.idFamiliarRecaudo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FamiliarRecaudoPK))
      throw new IllegalArgumentException("arg2");
    FamiliarRecaudoPK localFamiliarRecaudoPK = (FamiliarRecaudoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localFamiliarRecaudoPK.idFamiliarRecaudo);
  }

  private static final Familiar jdoGetfamiliar(FamiliarRecaudo paramFamiliarRecaudo)
  {
    StateManager localStateManager = paramFamiliarRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliarRecaudo.familiar;
    if (localStateManager.isLoaded(paramFamiliarRecaudo, jdoInheritedFieldCount + 0))
      return paramFamiliarRecaudo.familiar;
    return (Familiar)localStateManager.getObjectField(paramFamiliarRecaudo, jdoInheritedFieldCount + 0, paramFamiliarRecaudo.familiar);
  }

  private static final void jdoSetfamiliar(FamiliarRecaudo paramFamiliarRecaudo, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramFamiliarRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliarRecaudo.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramFamiliarRecaudo, jdoInheritedFieldCount + 0, paramFamiliarRecaudo.familiar, paramFamiliar);
  }

  private static final Date jdoGetfechaRecaudo(FamiliarRecaudo paramFamiliarRecaudo)
  {
    if (paramFamiliarRecaudo.jdoFlags <= 0)
      return paramFamiliarRecaudo.fechaRecaudo;
    StateManager localStateManager = paramFamiliarRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliarRecaudo.fechaRecaudo;
    if (localStateManager.isLoaded(paramFamiliarRecaudo, jdoInheritedFieldCount + 1))
      return paramFamiliarRecaudo.fechaRecaudo;
    return (Date)localStateManager.getObjectField(paramFamiliarRecaudo, jdoInheritedFieldCount + 1, paramFamiliarRecaudo.fechaRecaudo);
  }

  private static final void jdoSetfechaRecaudo(FamiliarRecaudo paramFamiliarRecaudo, Date paramDate)
  {
    if (paramFamiliarRecaudo.jdoFlags == 0)
    {
      paramFamiliarRecaudo.fechaRecaudo = paramDate;
      return;
    }
    StateManager localStateManager = paramFamiliarRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliarRecaudo.fechaRecaudo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramFamiliarRecaudo, jdoInheritedFieldCount + 1, paramFamiliarRecaudo.fechaRecaudo, paramDate);
  }

  private static final long jdoGetidFamiliarRecaudo(FamiliarRecaudo paramFamiliarRecaudo)
  {
    return paramFamiliarRecaudo.idFamiliarRecaudo;
  }

  private static final void jdoSetidFamiliarRecaudo(FamiliarRecaudo paramFamiliarRecaudo, long paramLong)
  {
    StateManager localStateManager = paramFamiliarRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliarRecaudo.idFamiliarRecaudo = paramLong;
      return;
    }
    localStateManager.setLongField(paramFamiliarRecaudo, jdoInheritedFieldCount + 2, paramFamiliarRecaudo.idFamiliarRecaudo, paramLong);
  }

  private static final RelacionRecaudo jdoGetrelacionRecaudo(FamiliarRecaudo paramFamiliarRecaudo)
  {
    StateManager localStateManager = paramFamiliarRecaudo.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliarRecaudo.relacionRecaudo;
    if (localStateManager.isLoaded(paramFamiliarRecaudo, jdoInheritedFieldCount + 3))
      return paramFamiliarRecaudo.relacionRecaudo;
    return (RelacionRecaudo)localStateManager.getObjectField(paramFamiliarRecaudo, jdoInheritedFieldCount + 3, paramFamiliarRecaudo.relacionRecaudo);
  }

  private static final void jdoSetrelacionRecaudo(FamiliarRecaudo paramFamiliarRecaudo, RelacionRecaudo paramRelacionRecaudo)
  {
    StateManager localStateManager = paramFamiliarRecaudo.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliarRecaudo.relacionRecaudo = paramRelacionRecaudo;
      return;
    }
    localStateManager.setObjectField(paramFamiliarRecaudo, jdoInheritedFieldCount + 3, paramFamiliarRecaudo.relacionRecaudo, paramRelacionRecaudo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}