package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.RelacionRecaudo;
import sigefirrhh.base.personal.RelacionRecaudoBeanBusiness;

public class FamiliarRecaudoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addFamiliarRecaudo(FamiliarRecaudo familiarRecaudo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    FamiliarRecaudo familiarRecaudoNew = 
      (FamiliarRecaudo)BeanUtils.cloneBean(
      familiarRecaudo);

    RelacionRecaudoBeanBusiness relacionRecaudoBeanBusiness = new RelacionRecaudoBeanBusiness();

    if (familiarRecaudoNew.getRelacionRecaudo() != null) {
      familiarRecaudoNew.setRelacionRecaudo(
        relacionRecaudoBeanBusiness.findRelacionRecaudoById(
        familiarRecaudoNew.getRelacionRecaudo().getIdRelacionRecaudo()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (familiarRecaudoNew.getFamiliar() != null) {
      familiarRecaudoNew.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        familiarRecaudoNew.getFamiliar().getIdFamiliar()));
    }
    pm.makePersistent(familiarRecaudoNew);
  }

  public void updateFamiliarRecaudo(FamiliarRecaudo familiarRecaudo) throws Exception
  {
    FamiliarRecaudo familiarRecaudoModify = 
      findFamiliarRecaudoById(familiarRecaudo.getIdFamiliarRecaudo());

    RelacionRecaudoBeanBusiness relacionRecaudoBeanBusiness = new RelacionRecaudoBeanBusiness();

    if (familiarRecaudo.getRelacionRecaudo() != null) {
      familiarRecaudo.setRelacionRecaudo(
        relacionRecaudoBeanBusiness.findRelacionRecaudoById(
        familiarRecaudo.getRelacionRecaudo().getIdRelacionRecaudo()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (familiarRecaudo.getFamiliar() != null) {
      familiarRecaudo.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        familiarRecaudo.getFamiliar().getIdFamiliar()));
    }

    BeanUtils.copyProperties(familiarRecaudoModify, familiarRecaudo);
  }

  public void deleteFamiliarRecaudo(FamiliarRecaudo familiarRecaudo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    FamiliarRecaudo familiarRecaudoDelete = 
      findFamiliarRecaudoById(familiarRecaudo.getIdFamiliarRecaudo());
    pm.deletePersistent(familiarRecaudoDelete);
  }

  public FamiliarRecaudo findFamiliarRecaudoById(long idFamiliarRecaudo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFamiliarRecaudo == pIdFamiliarRecaudo";
    Query query = pm.newQuery(FamiliarRecaudo.class, filter);

    query.declareParameters("long pIdFamiliarRecaudo");

    parameters.put("pIdFamiliarRecaudo", new Long(idFamiliarRecaudo));

    Collection colFamiliarRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFamiliarRecaudo.iterator();
    return (FamiliarRecaudo)iterator.next();
  }

  public Collection findFamiliarRecaudoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent familiarRecaudoExtent = pm.getExtent(
      FamiliarRecaudo.class, true);
    Query query = pm.newQuery(familiarRecaudoExtent);
    query.setOrdering("familiar.primerApellido ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByFamiliar(long idFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "familiar.idFamiliar == pIdFamiliar";

    Query query = pm.newQuery(FamiliarRecaudo.class, filter);

    query.declareParameters("long pIdFamiliar");
    HashMap parameters = new HashMap();

    parameters.put("pIdFamiliar", new Long(idFamiliar));

    query.setOrdering("familiar.primerApellido ascending");

    Collection colFamiliarRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliarRecaudo);

    return colFamiliarRecaudo;
  }
}