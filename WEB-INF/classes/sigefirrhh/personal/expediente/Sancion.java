package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.TipoAmonestacion;

public class Sancion
  implements Serializable, PersistenceCapable
{
  private long idSancion;
  private TipoAmonestacion tipoAmonestacion;
  private Date fecha;
  private String acta;
  private String nombreSupervisor;
  private String cargoSupervisor;
  private String cargoTrabajador;
  private String observaciones;
  private Personal personal;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "acta", "cargoSupervisor", "cargoTrabajador", "fecha", "idSancion", "idSitp", "nombreSupervisor", "observaciones", "organismo", "personal", "tiempoSitp", "tipoAmonestacion" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.TipoAmonestacion") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 21, 26, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoAmonestacion(this).getDescripcion() + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfecha(this));
  }

  public String getActa()
  {
    return jdoGetacta(this);
  }

  public String getCargoSupervisor()
  {
    return jdoGetcargoSupervisor(this);
  }

  public String getCargoTrabajador()
  {
    return jdoGetcargoTrabajador(this);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public String getNombreSupervisor()
  {
    return jdoGetnombreSupervisor(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoAmonestacion getTipoAmonestacion()
  {
    return jdoGettipoAmonestacion(this);
  }

  public void setActa(String string)
  {
    jdoSetacta(this, string);
  }

  public void setCargoSupervisor(String string)
  {
    jdoSetcargoSupervisor(this, string);
  }

  public void setCargoTrabajador(String string)
  {
    jdoSetcargoTrabajador(this, string);
  }

  public void setFecha(Date date)
  {
    jdoSetfecha(this, date);
  }

  public void setNombreSupervisor(String string)
  {
    jdoSetnombreSupervisor(this, string);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoAmonestacion(TipoAmonestacion amonestacion)
  {
    jdoSettipoAmonestacion(this, amonestacion);
  }

  public long getIdSancion() {
    return jdoGetidSancion(this);
  }

  public void setIdSancion(long l) {
    jdoSetidSancion(this, l);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 12;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Sancion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Sancion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Sancion localSancion = new Sancion();
    localSancion.jdoFlags = 1;
    localSancion.jdoStateManager = paramStateManager;
    return localSancion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Sancion localSancion = new Sancion();
    localSancion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSancion.jdoFlags = 1;
    localSancion.jdoStateManager = paramStateManager;
    return localSancion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.acta);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoSupervisor);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoTrabajador);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSancion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSupervisor);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoAmonestacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.acta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoSupervisor = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoTrabajador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSancion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSupervisor = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoAmonestacion = ((TipoAmonestacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Sancion paramSancion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.acta = paramSancion.acta;
      return;
    case 1:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.cargoSupervisor = paramSancion.cargoSupervisor;
      return;
    case 2:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.cargoTrabajador = paramSancion.cargoTrabajador;
      return;
    case 3:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramSancion.fecha;
      return;
    case 4:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.idSancion = paramSancion.idSancion;
      return;
    case 5:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramSancion.idSitp;
      return;
    case 6:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSupervisor = paramSancion.nombreSupervisor;
      return;
    case 7:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramSancion.observaciones;
      return;
    case 8:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramSancion.organismo;
      return;
    case 9:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramSancion.personal;
      return;
    case 10:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramSancion.tiempoSitp;
      return;
    case 11:
      if (paramSancion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoAmonestacion = paramSancion.tipoAmonestacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Sancion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Sancion localSancion = (Sancion)paramObject;
    if (localSancion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSancion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SancionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SancionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SancionPK))
      throw new IllegalArgumentException("arg1");
    SancionPK localSancionPK = (SancionPK)paramObject;
    localSancionPK.idSancion = this.idSancion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SancionPK))
      throw new IllegalArgumentException("arg1");
    SancionPK localSancionPK = (SancionPK)paramObject;
    this.idSancion = localSancionPK.idSancion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SancionPK))
      throw new IllegalArgumentException("arg2");
    SancionPK localSancionPK = (SancionPK)paramObject;
    localSancionPK.idSancion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SancionPK))
      throw new IllegalArgumentException("arg2");
    SancionPK localSancionPK = (SancionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localSancionPK.idSancion);
  }

  private static final String jdoGetacta(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.acta;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.acta;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 0))
      return paramSancion.acta;
    return localStateManager.getStringField(paramSancion, jdoInheritedFieldCount + 0, paramSancion.acta);
  }

  private static final void jdoSetacta(Sancion paramSancion, String paramString)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.acta = paramString;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.acta = paramString;
      return;
    }
    localStateManager.setStringField(paramSancion, jdoInheritedFieldCount + 0, paramSancion.acta, paramString);
  }

  private static final String jdoGetcargoSupervisor(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.cargoSupervisor;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.cargoSupervisor;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 1))
      return paramSancion.cargoSupervisor;
    return localStateManager.getStringField(paramSancion, jdoInheritedFieldCount + 1, paramSancion.cargoSupervisor);
  }

  private static final void jdoSetcargoSupervisor(Sancion paramSancion, String paramString)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.cargoSupervisor = paramString;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.cargoSupervisor = paramString;
      return;
    }
    localStateManager.setStringField(paramSancion, jdoInheritedFieldCount + 1, paramSancion.cargoSupervisor, paramString);
  }

  private static final String jdoGetcargoTrabajador(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.cargoTrabajador;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.cargoTrabajador;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 2))
      return paramSancion.cargoTrabajador;
    return localStateManager.getStringField(paramSancion, jdoInheritedFieldCount + 2, paramSancion.cargoTrabajador);
  }

  private static final void jdoSetcargoTrabajador(Sancion paramSancion, String paramString)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.cargoTrabajador = paramString;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.cargoTrabajador = paramString;
      return;
    }
    localStateManager.setStringField(paramSancion, jdoInheritedFieldCount + 2, paramSancion.cargoTrabajador, paramString);
  }

  private static final Date jdoGetfecha(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.fecha;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.fecha;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 3))
      return paramSancion.fecha;
    return (Date)localStateManager.getObjectField(paramSancion, jdoInheritedFieldCount + 3, paramSancion.fecha);
  }

  private static final void jdoSetfecha(Sancion paramSancion, Date paramDate)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSancion, jdoInheritedFieldCount + 3, paramSancion.fecha, paramDate);
  }

  private static final long jdoGetidSancion(Sancion paramSancion)
  {
    return paramSancion.idSancion;
  }

  private static final void jdoSetidSancion(Sancion paramSancion, long paramLong)
  {
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.idSancion = paramLong;
      return;
    }
    localStateManager.setLongField(paramSancion, jdoInheritedFieldCount + 4, paramSancion.idSancion, paramLong);
  }

  private static final int jdoGetidSitp(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.idSitp;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.idSitp;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 5))
      return paramSancion.idSitp;
    return localStateManager.getIntField(paramSancion, jdoInheritedFieldCount + 5, paramSancion.idSitp);
  }

  private static final void jdoSetidSitp(Sancion paramSancion, int paramInt)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramSancion, jdoInheritedFieldCount + 5, paramSancion.idSitp, paramInt);
  }

  private static final String jdoGetnombreSupervisor(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.nombreSupervisor;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.nombreSupervisor;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 6))
      return paramSancion.nombreSupervisor;
    return localStateManager.getStringField(paramSancion, jdoInheritedFieldCount + 6, paramSancion.nombreSupervisor);
  }

  private static final void jdoSetnombreSupervisor(Sancion paramSancion, String paramString)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.nombreSupervisor = paramString;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.nombreSupervisor = paramString;
      return;
    }
    localStateManager.setStringField(paramSancion, jdoInheritedFieldCount + 6, paramSancion.nombreSupervisor, paramString);
  }

  private static final String jdoGetobservaciones(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.observaciones;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.observaciones;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 7))
      return paramSancion.observaciones;
    return localStateManager.getStringField(paramSancion, jdoInheritedFieldCount + 7, paramSancion.observaciones);
  }

  private static final void jdoSetobservaciones(Sancion paramSancion, String paramString)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramSancion, jdoInheritedFieldCount + 7, paramSancion.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(Sancion paramSancion)
  {
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.organismo;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 8))
      return paramSancion.organismo;
    return (Organismo)localStateManager.getObjectField(paramSancion, jdoInheritedFieldCount + 8, paramSancion.organismo);
  }

  private static final void jdoSetorganismo(Sancion paramSancion, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramSancion, jdoInheritedFieldCount + 8, paramSancion.organismo, paramOrganismo);
  }

  private static final Personal jdoGetpersonal(Sancion paramSancion)
  {
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.personal;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 9))
      return paramSancion.personal;
    return (Personal)localStateManager.getObjectField(paramSancion, jdoInheritedFieldCount + 9, paramSancion.personal);
  }

  private static final void jdoSetpersonal(Sancion paramSancion, Personal paramPersonal)
  {
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramSancion, jdoInheritedFieldCount + 9, paramSancion.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(Sancion paramSancion)
  {
    if (paramSancion.jdoFlags <= 0)
      return paramSancion.tiempoSitp;
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.tiempoSitp;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 10))
      return paramSancion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramSancion, jdoInheritedFieldCount + 10, paramSancion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Sancion paramSancion, Date paramDate)
  {
    if (paramSancion.jdoFlags == 0)
    {
      paramSancion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSancion, jdoInheritedFieldCount + 10, paramSancion.tiempoSitp, paramDate);
  }

  private static final TipoAmonestacion jdoGettipoAmonestacion(Sancion paramSancion)
  {
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
      return paramSancion.tipoAmonestacion;
    if (localStateManager.isLoaded(paramSancion, jdoInheritedFieldCount + 11))
      return paramSancion.tipoAmonestacion;
    return (TipoAmonestacion)localStateManager.getObjectField(paramSancion, jdoInheritedFieldCount + 11, paramSancion.tipoAmonestacion);
  }

  private static final void jdoSettipoAmonestacion(Sancion paramSancion, TipoAmonestacion paramTipoAmonestacion)
  {
    StateManager localStateManager = paramSancion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSancion.tipoAmonestacion = paramTipoAmonestacion;
      return;
    }
    localStateManager.setObjectField(paramSancion, jdoInheritedFieldCount + 11, paramSancion.tipoAmonestacion, paramTipoAmonestacion);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}