package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.mre.SedeDiplomatica;
import sigefirrhh.base.mre.SedeDiplomaticaBeanBusiness;

public class ServicioExteriorBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addServicioExterior(ServicioExterior servicioExterior)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ServicioExterior servicioExteriorNew = 
      (ServicioExterior)BeanUtils.cloneBean(
      servicioExterior);

    SedeDiplomaticaBeanBusiness sedeDiplomaticaBeanBusiness = new SedeDiplomaticaBeanBusiness();

    if (servicioExteriorNew.getSedeDiplomatica() != null) {
      servicioExteriorNew.setSedeDiplomatica(
        sedeDiplomaticaBeanBusiness.findSedeDiplomaticaById(
        servicioExteriorNew.getSedeDiplomatica().getIdSedeDiplomatica()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (servicioExteriorNew.getCargo() != null) {
      servicioExteriorNew.setCargo(
        cargoBeanBusiness.findCargoById(
        servicioExteriorNew.getCargo().getIdCargo()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (servicioExteriorNew.getPersonal() != null) {
      servicioExteriorNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        servicioExteriorNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(servicioExteriorNew);
  }

  public void updateServicioExterior(ServicioExterior servicioExterior) throws Exception
  {
    ServicioExterior servicioExteriorModify = 
      findServicioExteriorById(servicioExterior.getIdServicioExterior());

    SedeDiplomaticaBeanBusiness sedeDiplomaticaBeanBusiness = new SedeDiplomaticaBeanBusiness();

    if (servicioExterior.getSedeDiplomatica() != null) {
      servicioExterior.setSedeDiplomatica(
        sedeDiplomaticaBeanBusiness.findSedeDiplomaticaById(
        servicioExterior.getSedeDiplomatica().getIdSedeDiplomatica()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (servicioExterior.getCargo() != null) {
      servicioExterior.setCargo(
        cargoBeanBusiness.findCargoById(
        servicioExterior.getCargo().getIdCargo()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (servicioExterior.getPersonal() != null) {
      servicioExterior.setPersonal(
        personalBeanBusiness.findPersonalById(
        servicioExterior.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(servicioExteriorModify, servicioExterior);
  }

  public void deleteServicioExterior(ServicioExterior servicioExterior) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ServicioExterior servicioExteriorDelete = 
      findServicioExteriorById(servicioExterior.getIdServicioExterior());
    pm.deletePersistent(servicioExteriorDelete);
  }

  public ServicioExterior findServicioExteriorById(long idServicioExterior) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idServicioExterior == pIdServicioExterior";
    Query query = pm.newQuery(ServicioExterior.class, filter);

    query.declareParameters("long pIdServicioExterior");

    parameters.put("pIdServicioExterior", new Long(idServicioExterior));

    Collection colServicioExterior = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colServicioExterior.iterator();
    return (ServicioExterior)iterator.next();
  }

  public Collection findServicioExteriorAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent servicioExteriorExtent = pm.getExtent(
      ServicioExterior.class, true);
    Query query = pm.newQuery(servicioExteriorExtent);
    query.setOrdering("sedeDiplomatica.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ServicioExterior.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("sedeDiplomatica.nombre ascending");

    Collection colServicioExterior = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colServicioExterior);

    return colServicioExterior;
  }
}