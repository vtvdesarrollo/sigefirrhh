package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.ubicacion.Pais;

public class Estudio
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_UNIDAD_TIEMPO;
  protected static final Map LISTA_ORIGEN_CURSO;
  protected static final Map LISTA_PARTICIPACION;
  protected static final Map LISTA_CERTIFICO;
  protected static final Map LISTA_DIPLOMA;
  protected static final Map LISTA_BECADO;
  protected static final Map LISTA_FINANCIAMIENTO;
  private long idEstudio;
  private TipoCurso tipoCurso;
  private AreaConocimiento areaConocimiento;
  private String nombreCurso;
  private int anio;
  private String nombreEntidad;
  private int duracion;
  private String unidadTiempo;
  private String origenCurso;
  private String participacion;
  private String certifico;
  private String escala;
  private String calificacion;
  private Pais pais;
  private String becado;
  private String financiamiento;
  private int aniosExperiencia;
  private int mesesExperiencia;
  private String observaciones;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "aniosExperiencia", "areaConocimiento", "becado", "calificacion", "certifico", "duracion", "escala", "financiamiento", "idEstudio", "mesesExperiencia", "nombreCurso", "nombreEntidad", "observaciones", "origenCurso", "pais", "participacion", "personal", "tipoCurso", "unidadTiempo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.adiestramiento.AreaConocimiento"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Pais"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.adiestramiento.TipoCurso"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 26, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Estudio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Estudio());

    LISTA_UNIDAD_TIEMPO = 
      new LinkedHashMap();
    LISTA_ORIGEN_CURSO = 
      new LinkedHashMap();
    LISTA_PARTICIPACION = 
      new LinkedHashMap();
    LISTA_CERTIFICO = 
      new LinkedHashMap();
    LISTA_DIPLOMA = 
      new LinkedHashMap();
    LISTA_BECADO = 
      new LinkedHashMap();
    LISTA_FINANCIAMIENTO = 
      new LinkedHashMap();

    LISTA_UNIDAD_TIEMPO.put("H", "HORAS");
    LISTA_UNIDAD_TIEMPO.put("D", "DIAS");
    LISTA_UNIDAD_TIEMPO.put("S", "SEMANAS");
    LISTA_UNIDAD_TIEMPO.put("M", "MESES");
    LISTA_UNIDAD_TIEMPO.put("A", "AÑOS");
    LISTA_ORIGEN_CURSO.put("I", "INICIATIVA PROPIA");
    LISTA_ORIGEN_CURSO.put("O", "ORGANISMO");
    LISTA_PARTICIPACION.put("P", "PARTICIPANTE");
    LISTA_PARTICIPACION.put("I", "INSTRUCTOR");
    LISTA_PARTICIPACION.put("O", "OYENTE");
    LISTA_PARTICIPACION.put("N", "PONENTE");
    LISTA_PARTICIPACION.put("C", "COORDINADOR");
    LISTA_CERTIFICO.put("S", "SI");
    LISTA_CERTIFICO.put("N", "NO");
    LISTA_DIPLOMA.put("S", "SI");
    LISTA_DIPLOMA.put("N", "NO");
    LISTA_BECADO.put("S", "SI");
    LISTA_BECADO.put("N", "NO");
    LISTA_FINANCIAMIENTO.put("O", "ORGANISMO");
    LISTA_FINANCIAMIENTO.put("E", "FINANCIAMIENTO EXTERNO");
    LISTA_FINANCIAMIENTO.put("P", "FINANCIAMIENTO PARCIAL");
    LISTA_FINANCIAMIENTO.put("N", "NINGUNO");
  }

  public Estudio()
  {
    jdoSetanio(this, 0);

    jdoSetduracion(this, 0);

    jdoSetunidadTiempo(this, "H");

    jdoSetorigenCurso(this, "I");

    jdoSetparticipacion(this, "P");

    jdoSetcertifico(this, "N");

    jdoSetbecado(this, "N");

    jdoSetfinanciamiento(this, "N");
  }

  public String toString()
  {
    return jdoGetnombreCurso(this) + "  - " + 
      jdoGetanio(this);
  }

  public int getAniosExperiencia()
  {
    return jdoGetaniosExperiencia(this);
  }

  public String getBecado()
  {
    return jdoGetbecado(this);
  }

  public String getCertifico()
  {
    return jdoGetcertifico(this);
  }

  public int getDuracion()
  {
    return jdoGetduracion(this);
  }

  public String getFinanciamiento()
  {
    return jdoGetfinanciamiento(this);
  }

  public long getIdEstudio()
  {
    return jdoGetidEstudio(this);
  }

  public int getMesesExperiencia()
  {
    return jdoGetmesesExperiencia(this);
  }

  public String getNombreCurso()
  {
    return jdoGetnombreCurso(this);
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public String getOrigenCurso()
  {
    return jdoGetorigenCurso(this);
  }

  public Pais getPais()
  {
    return jdoGetpais(this);
  }

  public String getParticipacion()
  {
    return jdoGetparticipacion(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public TipoCurso getTipoCurso()
  {
    return jdoGettipoCurso(this);
  }

  public String getUnidadTiempo()
  {
    return jdoGetunidadTiempo(this);
  }

  public void setAniosExperiencia(int i)
  {
    jdoSetaniosExperiencia(this, i);
  }

  public void setBecado(String string)
  {
    jdoSetbecado(this, string);
  }

  public void setCertifico(String string)
  {
    jdoSetcertifico(this, string);
  }

  public void setDuracion(int i)
  {
    jdoSetduracion(this, i);
  }

  public void setFinanciamiento(String string)
  {
    jdoSetfinanciamiento(this, string);
  }

  public void setIdEstudio(long l)
  {
    jdoSetidEstudio(this, l);
  }

  public void setMesesExperiencia(int i)
  {
    jdoSetmesesExperiencia(this, i);
  }

  public void setNombreCurso(String string)
  {
    jdoSetnombreCurso(this, string);
  }

  public void setNombreEntidad(String string)
  {
    jdoSetnombreEntidad(this, string);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setOrigenCurso(String string)
  {
    jdoSetorigenCurso(this, string);
  }

  public void setPais(Pais pais)
  {
    jdoSetpais(this, pais);
  }

  public void setParticipacion(String string)
  {
    jdoSetparticipacion(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setTipoCurso(TipoCurso curso)
  {
    jdoSettipoCurso(this, curso);
  }

  public void setUnidadTiempo(String string)
  {
    jdoSetunidadTiempo(this, string);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int i) {
    jdoSetanio(this, i);
  }

  public AreaConocimiento getAreaConocimiento()
  {
    return jdoGetareaConocimiento(this);
  }

  public void setAreaConocimiento(AreaConocimiento conocimiento)
  {
    jdoSetareaConocimiento(this, conocimiento);
  }

  public String getCalificacion()
  {
    return jdoGetcalificacion(this);
  }

  public String getEscala()
  {
    return jdoGetescala(this);
  }

  public void setCalificacion(String string)
  {
    jdoSetcalificacion(this, string);
  }

  public void setEscala(String string)
  {
    jdoSetescala(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 20;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Estudio localEstudio = new Estudio();
    localEstudio.jdoFlags = 1;
    localEstudio.jdoStateManager = paramStateManager;
    return localEstudio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Estudio localEstudio = new Estudio();
    localEstudio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEstudio.jdoFlags = 1;
    localEstudio.jdoStateManager = paramStateManager;
    return localEstudio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosExperiencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaConocimiento);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.becado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.calificacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.certifico);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.duracion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.escala);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.financiamiento);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEstudio);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesExperiencia);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCurso);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origenCurso);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.pais);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.participacion);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoCurso);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.unidadTiempo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaConocimiento = ((AreaConocimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.becado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.calificacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.certifico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.duracion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.escala = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.financiamiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEstudio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origenCurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pais = ((Pais)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.participacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCurso = ((TipoCurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadTiempo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Estudio paramEstudio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramEstudio.anio;
      return;
    case 1:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.aniosExperiencia = paramEstudio.aniosExperiencia;
      return;
    case 2:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.areaConocimiento = paramEstudio.areaConocimiento;
      return;
    case 3:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.becado = paramEstudio.becado;
      return;
    case 4:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.calificacion = paramEstudio.calificacion;
      return;
    case 5:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.certifico = paramEstudio.certifico;
      return;
    case 6:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.duracion = paramEstudio.duracion;
      return;
    case 7:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.escala = paramEstudio.escala;
      return;
    case 8:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.financiamiento = paramEstudio.financiamiento;
      return;
    case 9:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.idEstudio = paramEstudio.idEstudio;
      return;
    case 10:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.mesesExperiencia = paramEstudio.mesesExperiencia;
      return;
    case 11:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCurso = paramEstudio.nombreCurso;
      return;
    case 12:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramEstudio.nombreEntidad;
      return;
    case 13:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramEstudio.observaciones;
      return;
    case 14:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.origenCurso = paramEstudio.origenCurso;
      return;
    case 15:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.pais = paramEstudio.pais;
      return;
    case 16:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.participacion = paramEstudio.participacion;
      return;
    case 17:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramEstudio.personal;
      return;
    case 18:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCurso = paramEstudio.tipoCurso;
      return;
    case 19:
      if (paramEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.unidadTiempo = paramEstudio.unidadTiempo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Estudio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Estudio localEstudio = (Estudio)paramObject;
    if (localEstudio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEstudio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EstudioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EstudioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstudioPK))
      throw new IllegalArgumentException("arg1");
    EstudioPK localEstudioPK = (EstudioPK)paramObject;
    localEstudioPK.idEstudio = this.idEstudio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EstudioPK))
      throw new IllegalArgumentException("arg1");
    EstudioPK localEstudioPK = (EstudioPK)paramObject;
    this.idEstudio = localEstudioPK.idEstudio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstudioPK))
      throw new IllegalArgumentException("arg2");
    EstudioPK localEstudioPK = (EstudioPK)paramObject;
    localEstudioPK.idEstudio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 9);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EstudioPK))
      throw new IllegalArgumentException("arg2");
    EstudioPK localEstudioPK = (EstudioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 9, localEstudioPK.idEstudio);
  }

  private static final int jdoGetanio(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.anio;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.anio;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 0))
      return paramEstudio.anio;
    return localStateManager.getIntField(paramEstudio, jdoInheritedFieldCount + 0, paramEstudio.anio);
  }

  private static final void jdoSetanio(Estudio paramEstudio, int paramInt)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramEstudio, jdoInheritedFieldCount + 0, paramEstudio.anio, paramInt);
  }

  private static final int jdoGetaniosExperiencia(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.aniosExperiencia;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.aniosExperiencia;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 1))
      return paramEstudio.aniosExperiencia;
    return localStateManager.getIntField(paramEstudio, jdoInheritedFieldCount + 1, paramEstudio.aniosExperiencia);
  }

  private static final void jdoSetaniosExperiencia(Estudio paramEstudio, int paramInt)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.aniosExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.aniosExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramEstudio, jdoInheritedFieldCount + 1, paramEstudio.aniosExperiencia, paramInt);
  }

  private static final AreaConocimiento jdoGetareaConocimiento(Estudio paramEstudio)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.areaConocimiento;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 2))
      return paramEstudio.areaConocimiento;
    return (AreaConocimiento)localStateManager.getObjectField(paramEstudio, jdoInheritedFieldCount + 2, paramEstudio.areaConocimiento);
  }

  private static final void jdoSetareaConocimiento(Estudio paramEstudio, AreaConocimiento paramAreaConocimiento)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.areaConocimiento = paramAreaConocimiento;
      return;
    }
    localStateManager.setObjectField(paramEstudio, jdoInheritedFieldCount + 2, paramEstudio.areaConocimiento, paramAreaConocimiento);
  }

  private static final String jdoGetbecado(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.becado;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.becado;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 3))
      return paramEstudio.becado;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 3, paramEstudio.becado);
  }

  private static final void jdoSetbecado(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.becado = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.becado = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 3, paramEstudio.becado, paramString);
  }

  private static final String jdoGetcalificacion(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.calificacion;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.calificacion;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 4))
      return paramEstudio.calificacion;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 4, paramEstudio.calificacion);
  }

  private static final void jdoSetcalificacion(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.calificacion = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.calificacion = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 4, paramEstudio.calificacion, paramString);
  }

  private static final String jdoGetcertifico(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.certifico;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.certifico;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 5))
      return paramEstudio.certifico;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 5, paramEstudio.certifico);
  }

  private static final void jdoSetcertifico(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.certifico = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.certifico = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 5, paramEstudio.certifico, paramString);
  }

  private static final int jdoGetduracion(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.duracion;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.duracion;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 6))
      return paramEstudio.duracion;
    return localStateManager.getIntField(paramEstudio, jdoInheritedFieldCount + 6, paramEstudio.duracion);
  }

  private static final void jdoSetduracion(Estudio paramEstudio, int paramInt)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.duracion = paramInt;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.duracion = paramInt;
      return;
    }
    localStateManager.setIntField(paramEstudio, jdoInheritedFieldCount + 6, paramEstudio.duracion, paramInt);
  }

  private static final String jdoGetescala(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.escala;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.escala;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 7))
      return paramEstudio.escala;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 7, paramEstudio.escala);
  }

  private static final void jdoSetescala(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.escala = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.escala = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 7, paramEstudio.escala, paramString);
  }

  private static final String jdoGetfinanciamiento(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.financiamiento;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.financiamiento;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 8))
      return paramEstudio.financiamiento;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 8, paramEstudio.financiamiento);
  }

  private static final void jdoSetfinanciamiento(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.financiamiento = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.financiamiento = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 8, paramEstudio.financiamiento, paramString);
  }

  private static final long jdoGetidEstudio(Estudio paramEstudio)
  {
    return paramEstudio.idEstudio;
  }

  private static final void jdoSetidEstudio(Estudio paramEstudio, long paramLong)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.idEstudio = paramLong;
      return;
    }
    localStateManager.setLongField(paramEstudio, jdoInheritedFieldCount + 9, paramEstudio.idEstudio, paramLong);
  }

  private static final int jdoGetmesesExperiencia(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.mesesExperiencia;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.mesesExperiencia;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 10))
      return paramEstudio.mesesExperiencia;
    return localStateManager.getIntField(paramEstudio, jdoInheritedFieldCount + 10, paramEstudio.mesesExperiencia);
  }

  private static final void jdoSetmesesExperiencia(Estudio paramEstudio, int paramInt)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.mesesExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.mesesExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramEstudio, jdoInheritedFieldCount + 10, paramEstudio.mesesExperiencia, paramInt);
  }

  private static final String jdoGetnombreCurso(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.nombreCurso;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.nombreCurso;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 11))
      return paramEstudio.nombreCurso;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 11, paramEstudio.nombreCurso);
  }

  private static final void jdoSetnombreCurso(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.nombreCurso = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.nombreCurso = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 11, paramEstudio.nombreCurso, paramString);
  }

  private static final String jdoGetnombreEntidad(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.nombreEntidad;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.nombreEntidad;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 12))
      return paramEstudio.nombreEntidad;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 12, paramEstudio.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 12, paramEstudio.nombreEntidad, paramString);
  }

  private static final String jdoGetobservaciones(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.observaciones;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.observaciones;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 13))
      return paramEstudio.observaciones;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 13, paramEstudio.observaciones);
  }

  private static final void jdoSetobservaciones(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 13, paramEstudio.observaciones, paramString);
  }

  private static final String jdoGetorigenCurso(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.origenCurso;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.origenCurso;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 14))
      return paramEstudio.origenCurso;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 14, paramEstudio.origenCurso);
  }

  private static final void jdoSetorigenCurso(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.origenCurso = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.origenCurso = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 14, paramEstudio.origenCurso, paramString);
  }

  private static final Pais jdoGetpais(Estudio paramEstudio)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.pais;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 15))
      return paramEstudio.pais;
    return (Pais)localStateManager.getObjectField(paramEstudio, jdoInheritedFieldCount + 15, paramEstudio.pais);
  }

  private static final void jdoSetpais(Estudio paramEstudio, Pais paramPais)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.pais = paramPais;
      return;
    }
    localStateManager.setObjectField(paramEstudio, jdoInheritedFieldCount + 15, paramEstudio.pais, paramPais);
  }

  private static final String jdoGetparticipacion(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.participacion;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.participacion;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 16))
      return paramEstudio.participacion;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 16, paramEstudio.participacion);
  }

  private static final void jdoSetparticipacion(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.participacion = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.participacion = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 16, paramEstudio.participacion, paramString);
  }

  private static final Personal jdoGetpersonal(Estudio paramEstudio)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.personal;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 17))
      return paramEstudio.personal;
    return (Personal)localStateManager.getObjectField(paramEstudio, jdoInheritedFieldCount + 17, paramEstudio.personal);
  }

  private static final void jdoSetpersonal(Estudio paramEstudio, Personal paramPersonal)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramEstudio, jdoInheritedFieldCount + 17, paramEstudio.personal, paramPersonal);
  }

  private static final TipoCurso jdoGettipoCurso(Estudio paramEstudio)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.tipoCurso;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 18))
      return paramEstudio.tipoCurso;
    return (TipoCurso)localStateManager.getObjectField(paramEstudio, jdoInheritedFieldCount + 18, paramEstudio.tipoCurso);
  }

  private static final void jdoSettipoCurso(Estudio paramEstudio, TipoCurso paramTipoCurso)
  {
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.tipoCurso = paramTipoCurso;
      return;
    }
    localStateManager.setObjectField(paramEstudio, jdoInheritedFieldCount + 18, paramEstudio.tipoCurso, paramTipoCurso);
  }

  private static final String jdoGetunidadTiempo(Estudio paramEstudio)
  {
    if (paramEstudio.jdoFlags <= 0)
      return paramEstudio.unidadTiempo;
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramEstudio.unidadTiempo;
    if (localStateManager.isLoaded(paramEstudio, jdoInheritedFieldCount + 19))
      return paramEstudio.unidadTiempo;
    return localStateManager.getStringField(paramEstudio, jdoInheritedFieldCount + 19, paramEstudio.unidadTiempo);
  }

  private static final void jdoSetunidadTiempo(Estudio paramEstudio, String paramString)
  {
    if (paramEstudio.jdoFlags == 0)
    {
      paramEstudio.unidadTiempo = paramString;
      return;
    }
    StateManager localStateManager = paramEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramEstudio.unidadTiempo = paramString;
      return;
    }
    localStateManager.setStringField(paramEstudio, jdoInheritedFieldCount + 19, paramEstudio.unidadTiempo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}