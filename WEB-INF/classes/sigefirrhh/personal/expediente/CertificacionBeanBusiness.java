package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.AreaConocimientoBeanBusiness;

public class CertificacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCertificacion(Certificacion certificacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Certificacion certificacionNew = 
      (Certificacion)BeanUtils.cloneBean(
      certificacion);

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (certificacionNew.getAreaConocimiento() != null) {
      certificacionNew.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        certificacionNew.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (certificacionNew.getPersonal() != null) {
      certificacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        certificacionNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(certificacionNew);
  }

  public void updateCertificacion(Certificacion certificacion) throws Exception
  {
    Certificacion certificacionModify = 
      findCertificacionById(certificacion.getIdCertificacion());

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (certificacion.getAreaConocimiento() != null) {
      certificacion.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        certificacion.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (certificacion.getPersonal() != null) {
      certificacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        certificacion.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(certificacionModify, certificacion);
  }

  public void deleteCertificacion(Certificacion certificacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Certificacion certificacionDelete = 
      findCertificacionById(certificacion.getIdCertificacion());
    pm.deletePersistent(certificacionDelete);
  }

  public Certificacion findCertificacionById(long idCertificacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCertificacion == pIdCertificacion";
    Query query = pm.newQuery(Certificacion.class, filter);

    query.declareParameters("long pIdCertificacion");

    parameters.put("pIdCertificacion", new Long(idCertificacion));

    Collection colCertificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCertificacion.iterator();
    return (Certificacion)iterator.next();
  }

  public Collection findCertificacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent certificacionExtent = pm.getExtent(
      Certificacion.class, true);
    Query query = pm.newQuery(certificacionExtent);
    query.setOrdering("fechaCertificacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "areaConocimiento.idAreaConocimiento == pIdAreaConocimiento";

    Query query = pm.newQuery(Certificacion.class, filter);

    query.declareParameters("long pIdAreaConocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdAreaConocimiento", new Long(idAreaConocimiento));

    query.setOrdering("fechaCertificacion ascending");

    Collection colCertificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCertificacion);

    return colCertificacion;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Certificacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaCertificacion ascending");

    Collection colCertificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCertificacion);

    return colCertificacion;
  }
}