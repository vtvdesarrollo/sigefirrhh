package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class ExpedienteBusiness extends AbstractBusiness
  implements Serializable
{
  private VacacionProgramadaBeanBusiness vacacionProgramadaBeanBusiness = new VacacionProgramadaBeanBusiness();

  private VacacionDisfrutadaBeanBusiness vacacionDisfrutadaBeanBusiness = new VacacionDisfrutadaBeanBusiness();

  private TrayectoriaBeanBusiness trayectoriaBeanBusiness = new TrayectoriaBeanBusiness();

  private AcreenciaBeanBusiness acreenciaBeanBusiness = new AcreenciaBeanBusiness();

  private ActividadDocenteBeanBusiness actividadDocenteBeanBusiness = new ActividadDocenteBeanBusiness();

  private AfiliacionBeanBusiness afiliacionBeanBusiness = new AfiliacionBeanBusiness();

  private AntecedenteBeanBusiness antecedenteBeanBusiness = new AntecedenteBeanBusiness();

  private AusenciaBeanBusiness ausenciaBeanBusiness = new AusenciaBeanBusiness();

  private AusenciaRecaudoBeanBusiness ausenciaRecaudoBeanBusiness = new AusenciaRecaudoBeanBusiness();

  private AveriguacionBeanBusiness averiguacionBeanBusiness = new AveriguacionBeanBusiness();

  private CertificacionBeanBusiness certificacionBeanBusiness = new CertificacionBeanBusiness();

  private CertificadoBeanBusiness certificadoBeanBusiness = new CertificadoBeanBusiness();

  private ComisionServicioBeanBusiness comisionServicioBeanBusiness = new ComisionServicioBeanBusiness();

  private ComisionServicioExtBeanBusiness comisionServicioExtBeanBusiness = new ComisionServicioExtBeanBusiness();

  private ContratoBeanBusiness contratoBeanBusiness = new ContratoBeanBusiness();

  private CredencialBeanBusiness credencialBeanBusiness = new CredencialBeanBusiness();

  private DeclaracionBeanBusiness declaracionBeanBusiness = new DeclaracionBeanBusiness();

  private EducacionBeanBusiness educacionBeanBusiness = new EducacionBeanBusiness();

  private EncargaduriaBeanBusiness encargaduriaBeanBusiness = new EncargaduriaBeanBusiness();

  private EstudioBeanBusiness estudioBeanBusiness = new EstudioBeanBusiness();

  private ExperienciaBeanBusiness experienciaBeanBusiness = new ExperienciaBeanBusiness();

  private ExperienciaNoEstBeanBusiness experienciaNoEstBeanBusiness = new ExperienciaNoEstBeanBusiness();

  private FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

  private FamiliarRecaudoBeanBusiness familiarRecaudoBeanBusiness = new FamiliarRecaudoBeanBusiness();

  private HabilidadBeanBusiness habilidadBeanBusiness = new HabilidadBeanBusiness();

  private HistorialApnBeanBusiness historialApnBeanBusiness = new HistorialApnBeanBusiness();

  private HistorialOrganismoBeanBusiness historialOrganismoBeanBusiness = new HistorialOrganismoBeanBusiness();

  private IdiomaBeanBusiness idiomaBeanBusiness = new IdiomaBeanBusiness();

  private OtraActividadBeanBusiness otraActividadBeanBusiness = new OtraActividadBeanBusiness();

  private PasantiaBeanBusiness pasantiaBeanBusiness = new PasantiaBeanBusiness();

  private PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

  private PersonalOrganismoBeanBusiness personalOrganismoBeanBusiness = new PersonalOrganismoBeanBusiness();

  private PersonalRecaudoBeanBusiness personalRecaudoBeanBusiness = new PersonalRecaudoBeanBusiness();

  private ProfesionTrabajadorBeanBusiness profesionTrabajadorBeanBusiness = new ProfesionTrabajadorBeanBusiness();

  private PublicacionBeanBusiness publicacionBeanBusiness = new PublicacionBeanBusiness();

  private ReconocimientoBeanBusiness reconocimientoBeanBusiness = new ReconocimientoBeanBusiness();

  private SancionBeanBusiness sancionBeanBusiness = new SancionBeanBusiness();

  private ServicioExteriorBeanBusiness servicioExteriorBeanBusiness = new ServicioExteriorBeanBusiness();

  private SuplenciaBeanBusiness suplenciaBeanBusiness = new SuplenciaBeanBusiness();

  private VacacionBeanBusiness vacacionBeanBusiness = new VacacionBeanBusiness();

  private TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

  public void addVacacionProgramada(VacacionProgramada vacacionProgramada)
    throws Exception
  {
    this.vacacionProgramadaBeanBusiness.addVacacionProgramada(vacacionProgramada);
  }

  public void updateVacacionProgramada(VacacionProgramada vacacionProgramada) throws Exception {
    this.vacacionProgramadaBeanBusiness.updateVacacionProgramada(vacacionProgramada);
  }

  public void deleteVacacionProgramada(VacacionProgramada vacacionProgramada) throws Exception {
    this.vacacionProgramadaBeanBusiness.deleteVacacionProgramada(vacacionProgramada);
  }

  public VacacionProgramada findVacacionProgramadaById(long vacacionProgramadaId) throws Exception {
    return this.vacacionProgramadaBeanBusiness.findVacacionProgramadaById(vacacionProgramadaId);
  }

  public Collection findAllVacacionProgramada() throws Exception {
    return this.vacacionProgramadaBeanBusiness.findVacacionProgramadaAll();
  }

  public Collection findVacacionProgramadaByPersonal(long idPersonal)
    throws Exception
  {
    return this.vacacionProgramadaBeanBusiness.findByPersonal(idPersonal);
  }

  public void addVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada)
    throws Exception
  {
    this.vacacionDisfrutadaBeanBusiness.addVacacionDisfrutada(vacacionDisfrutada);
  }

  public void updateVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada) throws Exception {
    this.vacacionDisfrutadaBeanBusiness.updateVacacionDisfrutada(vacacionDisfrutada);
  }

  public void deleteVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada) throws Exception {
    this.vacacionDisfrutadaBeanBusiness.deleteVacacionDisfrutada(vacacionDisfrutada);
  }

  public VacacionDisfrutada findVacacionDisfrutadaById(long vacacionDisfrutadaId) throws Exception {
    return this.vacacionDisfrutadaBeanBusiness.findVacacionDisfrutadaById(vacacionDisfrutadaId);
  }

  public Collection findAllVacacionDisfrutada() throws Exception {
    return this.vacacionDisfrutadaBeanBusiness.findVacacionDisfrutadaAll();
  }

  public Collection findVacacionDisfrutadaByPersonal(long idPersonal)
    throws Exception
  {
    return this.vacacionDisfrutadaBeanBusiness.findByPersonal(idPersonal);
  }

  public void addTrayectoria(Trayectoria trayectoria)
    throws Exception
  {
    this.trayectoriaBeanBusiness.addTrayectoria(trayectoria);
  }

  public void updateTrayectoria(Trayectoria trayectoria) throws Exception {
    this.trayectoriaBeanBusiness.updateTrayectoria(trayectoria);
  }

  public void deleteTrayectoria(Trayectoria trayectoria) throws Exception {
    this.trayectoriaBeanBusiness.deleteTrayectoria(trayectoria);
  }

  public Trayectoria findTrayectoriaById(long trayectoriaId) throws Exception {
    return this.trayectoriaBeanBusiness.findTrayectoriaById(trayectoriaId);
  }

  public Collection findAllTrayectoria() throws Exception {
    return this.trayectoriaBeanBusiness.findTrayectoriaAll();
  }

  public Collection findTrayectoriaByPersonal(long idPersonal)
    throws Exception
  {
    return this.trayectoriaBeanBusiness.findByPersonal(idPersonal);
  }

  public void addAcreencia(Acreencia acreencia)
    throws Exception
  {
    this.acreenciaBeanBusiness.addAcreencia(acreencia);
  }

  public void updateAcreencia(Acreencia acreencia) throws Exception {
    this.acreenciaBeanBusiness.updateAcreencia(acreencia);
  }

  public void deleteAcreencia(Acreencia acreencia) throws Exception {
    this.acreenciaBeanBusiness.deleteAcreencia(acreencia);
  }

  public Acreencia findAcreenciaById(long acreenciaId) throws Exception {
    return this.acreenciaBeanBusiness.findAcreenciaById(acreenciaId);
  }

  public Collection findAllAcreencia() throws Exception {
    return this.acreenciaBeanBusiness.findAcreenciaAll();
  }

  public Collection findAcreenciaByTipoAcreencia(long idTipoAcreencia)
    throws Exception
  {
    return this.acreenciaBeanBusiness.findByTipoAcreencia(idTipoAcreencia);
  }

  public Collection findAcreenciaByPersonal(long idPersonal)
    throws Exception
  {
    return this.acreenciaBeanBusiness.findByPersonal(idPersonal);
  }

  public void addActividadDocente(ActividadDocente actividadDocente)
    throws Exception
  {
    this.actividadDocenteBeanBusiness.addActividadDocente(actividadDocente);
  }

  public void updateActividadDocente(ActividadDocente actividadDocente) throws Exception {
    this.actividadDocenteBeanBusiness.updateActividadDocente(actividadDocente);
  }

  public void deleteActividadDocente(ActividadDocente actividadDocente) throws Exception {
    this.actividadDocenteBeanBusiness.deleteActividadDocente(actividadDocente);
  }

  public ActividadDocente findActividadDocenteById(long actividadDocenteId) throws Exception {
    return this.actividadDocenteBeanBusiness.findActividadDocenteById(actividadDocenteId);
  }

  public Collection findAllActividadDocente() throws Exception {
    return this.actividadDocenteBeanBusiness.findActividadDocenteAll();
  }

  public Collection findActividadDocenteByPersonal(long idPersonal)
    throws Exception
  {
    return this.actividadDocenteBeanBusiness.findByPersonal(idPersonal);
  }

  public void addAfiliacion(Afiliacion afiliacion)
    throws Exception
  {
    this.afiliacionBeanBusiness.addAfiliacion(afiliacion);
  }

  public void updateAfiliacion(Afiliacion afiliacion) throws Exception {
    this.afiliacionBeanBusiness.updateAfiliacion(afiliacion);
  }

  public void deleteAfiliacion(Afiliacion afiliacion) throws Exception {
    this.afiliacionBeanBusiness.deleteAfiliacion(afiliacion);
  }

  public Afiliacion findAfiliacionById(long afiliacionId) throws Exception {
    return this.afiliacionBeanBusiness.findAfiliacionById(afiliacionId);
  }

  public Collection findAllAfiliacion() throws Exception {
    return this.afiliacionBeanBusiness.findAfiliacionAll();
  }

  public Collection findAfiliacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.afiliacionBeanBusiness.findByPersonal(idPersonal);
  }

  public void addAntecedente(Antecedente antecedente)
    throws Exception
  {
    this.antecedenteBeanBusiness.addAntecedente(antecedente);
  }

  public void updateAntecedente(Antecedente antecedente) throws Exception {
    this.antecedenteBeanBusiness.updateAntecedente(antecedente);
  }

  public void deleteAntecedente(Antecedente antecedente) throws Exception {
    this.antecedenteBeanBusiness.deleteAntecedente(antecedente);
  }

  public Antecedente findAntecedenteById(long antecedenteId) throws Exception {
    return this.antecedenteBeanBusiness.findAntecedenteById(antecedenteId);
  }

  public Collection findAllAntecedente() throws Exception {
    return this.antecedenteBeanBusiness.findAntecedenteAll();
  }

  public Collection findAntecedenteByPersonal(long idPersonal)
    throws Exception
  {
    return this.antecedenteBeanBusiness.findByPersonal(idPersonal);
  }

  public void addAusencia(Ausencia ausencia)
    throws Exception
  {
    this.ausenciaBeanBusiness.addAusencia(ausencia);
  }

  public void updateAusencia(Ausencia ausencia) throws Exception {
    this.ausenciaBeanBusiness.updateAusencia(ausencia);
  }

  public void deleteAusencia(Ausencia ausencia) throws Exception {
    this.ausenciaBeanBusiness.deleteAusencia(ausencia);
  }

  public Ausencia findAusenciaById(long ausenciaId) throws Exception {
    return this.ausenciaBeanBusiness.findAusenciaById(ausenciaId);
  }

  public Collection findAllAusencia() throws Exception {
    return this.ausenciaBeanBusiness.findAusenciaAll();
  }

  public Collection findAusenciaByPersonal(long idPersonal)
    throws Exception
  {
    return this.ausenciaBeanBusiness.findByPersonal(idPersonal);
  }

  public Collection findAusenciaByCedulaDirector(int cedulaDirector)
    throws Exception
  {
    return this.ausenciaBeanBusiness.findByCedulaDirector(cedulaDirector);
  }

  public void addAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo)
    throws Exception
  {
    this.ausenciaRecaudoBeanBusiness.addAusenciaRecaudo(ausenciaRecaudo);
  }

  public void updateAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo) throws Exception {
    this.ausenciaRecaudoBeanBusiness.updateAusenciaRecaudo(ausenciaRecaudo);
  }

  public void deleteAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo) throws Exception {
    this.ausenciaRecaudoBeanBusiness.deleteAusenciaRecaudo(ausenciaRecaudo);
  }

  public AusenciaRecaudo findAusenciaRecaudoById(long ausenciaRecaudoId) throws Exception {
    return this.ausenciaRecaudoBeanBusiness.findAusenciaRecaudoById(ausenciaRecaudoId);
  }

  public Collection findAllAusenciaRecaudo() throws Exception {
    return this.ausenciaRecaudoBeanBusiness.findAusenciaRecaudoAll();
  }

  public void addAveriguacion(Averiguacion averiguacion)
    throws Exception
  {
    this.averiguacionBeanBusiness.addAveriguacion(averiguacion);
  }

  public void updateAveriguacion(Averiguacion averiguacion) throws Exception {
    this.averiguacionBeanBusiness.updateAveriguacion(averiguacion);
  }

  public void deleteAveriguacion(Averiguacion averiguacion) throws Exception {
    this.averiguacionBeanBusiness.deleteAveriguacion(averiguacion);
  }

  public Averiguacion findAveriguacionById(long averiguacionId) throws Exception {
    return this.averiguacionBeanBusiness.findAveriguacionById(averiguacionId);
  }

  public Collection findAllAveriguacion() throws Exception {
    return this.averiguacionBeanBusiness.findAveriguacionAll();
  }

  public Collection findAveriguacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.averiguacionBeanBusiness.findByPersonal(idPersonal);
  }

  public void addCertificacion(Certificacion certificacion)
    throws Exception
  {
    this.certificacionBeanBusiness.addCertificacion(certificacion);
  }

  public void updateCertificacion(Certificacion certificacion) throws Exception {
    this.certificacionBeanBusiness.updateCertificacion(certificacion);
  }

  public void deleteCertificacion(Certificacion certificacion) throws Exception {
    this.certificacionBeanBusiness.deleteCertificacion(certificacion);
  }

  public Certificacion findCertificacionById(long certificacionId) throws Exception {
    return this.certificacionBeanBusiness.findCertificacionById(certificacionId);
  }

  public Collection findAllCertificacion() throws Exception {
    return this.certificacionBeanBusiness.findCertificacionAll();
  }

  public Collection findCertificacionByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    return this.certificacionBeanBusiness.findByAreaConocimiento(idAreaConocimiento);
  }

  public Collection findCertificacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.certificacionBeanBusiness.findByPersonal(idPersonal);
  }

  public void addCertificado(Certificado certificado)
    throws Exception
  {
    this.certificadoBeanBusiness.addCertificado(certificado);
  }

  public void updateCertificado(Certificado certificado) throws Exception {
    this.certificadoBeanBusiness.updateCertificado(certificado);
  }

  public void deleteCertificado(Certificado certificado) throws Exception {
    this.certificadoBeanBusiness.deleteCertificado(certificado);
  }

  public Certificado findCertificadoById(long certificadoId) throws Exception {
    return this.certificadoBeanBusiness.findCertificadoById(certificadoId);
  }

  public Collection findAllCertificado() throws Exception {
    return this.certificadoBeanBusiness.findCertificadoAll();
  }

  public Collection findCertificadoByPersonal(long idPersonal)
    throws Exception
  {
    return this.certificadoBeanBusiness.findByPersonal(idPersonal);
  }

  public void addComisionServicio(ComisionServicio comisionServicio)
    throws Exception
  {
    this.comisionServicioBeanBusiness.addComisionServicio(comisionServicio);
  }

  public void updateComisionServicio(ComisionServicio comisionServicio) throws Exception {
    this.comisionServicioBeanBusiness.updateComisionServicio(comisionServicio);
  }

  public void deleteComisionServicio(ComisionServicio comisionServicio) throws Exception {
    this.comisionServicioBeanBusiness.deleteComisionServicio(comisionServicio);
  }

  public ComisionServicio findComisionServicioById(long comisionServicioId) throws Exception {
    return this.comisionServicioBeanBusiness.findComisionServicioById(comisionServicioId);
  }

  public Collection findAllComisionServicio() throws Exception {
    return this.comisionServicioBeanBusiness.findComisionServicioAll();
  }

  public Collection findComisionServicioByPersonal(long idPersonal)
    throws Exception
  {
    return this.comisionServicioBeanBusiness.findByPersonal(idPersonal);
  }

  public void addComisionServicioExt(ComisionServicioExt comisionServicioExt)
    throws Exception
  {
    this.comisionServicioExtBeanBusiness.addComisionServicioExt(comisionServicioExt);
  }

  public void updateComisionServicioExt(ComisionServicioExt comisionServicioExt) throws Exception {
    this.comisionServicioExtBeanBusiness.updateComisionServicioExt(comisionServicioExt);
  }

  public void deleteComisionServicioExt(ComisionServicioExt comisionServicioExt) throws Exception {
    this.comisionServicioExtBeanBusiness.deleteComisionServicioExt(comisionServicioExt);
  }

  public ComisionServicioExt findComisionServicioExtById(long comisionServicioExtId) throws Exception {
    return this.comisionServicioExtBeanBusiness.findComisionServicioExtById(comisionServicioExtId);
  }

  public Collection findAllComisionServicioExt() throws Exception {
    return this.comisionServicioExtBeanBusiness.findComisionServicioExtAll();
  }

  public Collection findComisionServicioExtByPersonal(long idPersonal)
    throws Exception
  {
    return this.comisionServicioExtBeanBusiness.findByPersonal(idPersonal);
  }

  public void addContrato(Contrato contrato)
    throws Exception
  {
    this.contratoBeanBusiness.addContrato(contrato);
  }

  public void updateContrato(Contrato contrato) throws Exception {
    this.contratoBeanBusiness.updateContrato(contrato);
  }

  public void deleteContrato(Contrato contrato) throws Exception {
    this.contratoBeanBusiness.deleteContrato(contrato);
  }

  public Contrato findContratoById(long contratoId) throws Exception {
    return this.contratoBeanBusiness.findContratoById(contratoId);
  }

  public Collection findAllContrato() throws Exception {
    return this.contratoBeanBusiness.findContratoAll();
  }

  public Collection findContratoByPersonal(long idPersonal)
    throws Exception
  {
    return this.contratoBeanBusiness.findByPersonal(idPersonal);
  }

  public void addDeclaracion(Declaracion declaracion)
    throws Exception
  {
    this.declaracionBeanBusiness.addDeclaracion(declaracion);
  }

  public void updateDeclaracion(Declaracion declaracion) throws Exception {
    this.declaracionBeanBusiness.updateDeclaracion(declaracion);
  }

  public void deleteDeclaracion(Declaracion declaracion) throws Exception {
    this.declaracionBeanBusiness.deleteDeclaracion(declaracion);
  }

  public Declaracion findDeclaracionById(long declaracionId) throws Exception {
    return this.declaracionBeanBusiness.findDeclaracionById(declaracionId);
  }

  public Collection findAllDeclaracion() throws Exception {
    return this.declaracionBeanBusiness.findDeclaracionAll();
  }

  public Collection findDeclaracionByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.declaracionBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addEducacion(Educacion educacion)
    throws Exception
  {
    this.educacionBeanBusiness.addEducacion(educacion);
  }

  public void updateEducacion(Educacion educacion) throws Exception {
    this.educacionBeanBusiness.updateEducacion(educacion);
  }

  public void deleteEducacion(Educacion educacion) throws Exception {
    this.educacionBeanBusiness.deleteEducacion(educacion);
  }

  public Educacion findEducacionById(long educacionId) throws Exception {
    return this.educacionBeanBusiness.findEducacionById(educacionId);
  }

  public Collection findAllEducacion() throws Exception {
    return this.educacionBeanBusiness.findEducacionAll();
  }

  public Collection findEducacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.educacionBeanBusiness.findByPersonal(idPersonal);
  }

  public void addEncargaduria(Encargaduria encargaduria)
    throws Exception
  {
    this.encargaduriaBeanBusiness.addEncargaduria(encargaduria);
  }

  public void updateEncargaduria(Encargaduria encargaduria) throws Exception {
    this.encargaduriaBeanBusiness.updateEncargaduria(encargaduria);
  }

  public void deleteEncargaduria(Encargaduria encargaduria) throws Exception {
    this.encargaduriaBeanBusiness.deleteEncargaduria(encargaduria);
  }

  public Encargaduria findEncargaduriaById(long encargaduriaId) throws Exception {
    return this.encargaduriaBeanBusiness.findEncargaduriaById(encargaduriaId);
  }

  public Collection findAllEncargaduria() throws Exception {
    return this.encargaduriaBeanBusiness.findEncargaduriaAll();
  }

  public Collection findEncargaduriaByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.encargaduriaBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addEstudio(Estudio estudio)
    throws Exception
  {
    this.estudioBeanBusiness.addEstudio(estudio);
  }

  public void updateEstudio(Estudio estudio) throws Exception {
    this.estudioBeanBusiness.updateEstudio(estudio);
  }

  public void deleteEstudio(Estudio estudio) throws Exception {
    this.estudioBeanBusiness.deleteEstudio(estudio);
  }

  public Estudio findEstudioById(long estudioId) throws Exception {
    return this.estudioBeanBusiness.findEstudioById(estudioId);
  }

  public Collection findAllEstudio() throws Exception {
    return this.estudioBeanBusiness.findEstudioAll();
  }

  public Collection findEstudioByTipoCurso(long idTipoCurso)
    throws Exception
  {
    return this.estudioBeanBusiness.findByTipoCurso(idTipoCurso);
  }

  public Collection findEstudioByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    return this.estudioBeanBusiness.findByAreaConocimiento(idAreaConocimiento);
  }

  public Collection findEstudioByPersonal(long idPersonal)
    throws Exception
  {
    return this.estudioBeanBusiness.findByPersonal(idPersonal);
  }

  public void addExperiencia(Experiencia experiencia)
    throws Exception
  {
    this.experienciaBeanBusiness.addExperiencia(experiencia);
  }

  public void updateExperiencia(Experiencia experiencia) throws Exception {
    this.experienciaBeanBusiness.updateExperiencia(experiencia);
  }

  public void deleteExperiencia(Experiencia experiencia) throws Exception {
    this.experienciaBeanBusiness.deleteExperiencia(experiencia);
  }

  public Experiencia findExperienciaById(long experienciaId) throws Exception {
    return this.experienciaBeanBusiness.findExperienciaById(experienciaId);
  }

  public Collection findAllExperiencia() throws Exception {
    return this.experienciaBeanBusiness.findExperienciaAll();
  }

  public Collection findExperienciaByPersonal(long idPersonal)
    throws Exception
  {
    return this.experienciaBeanBusiness.findByPersonal(idPersonal);
  }

  public void addExperienciaNoEst(ExperienciaNoEst experienciaNoEst)
    throws Exception
  {
    this.experienciaNoEstBeanBusiness.addExperienciaNoEst(experienciaNoEst);
  }

  public void updateExperienciaNoEst(ExperienciaNoEst experienciaNoEst) throws Exception {
    this.experienciaNoEstBeanBusiness.updateExperienciaNoEst(experienciaNoEst);
  }

  public void deleteExperienciaNoEst(ExperienciaNoEst experienciaNoEst) throws Exception {
    this.experienciaNoEstBeanBusiness.deleteExperienciaNoEst(experienciaNoEst);
  }

  public ExperienciaNoEst findExperienciaNoEstById(long experienciaNoEstId) throws Exception {
    return this.experienciaNoEstBeanBusiness.findExperienciaNoEstById(experienciaNoEstId);
  }

  public Collection findAllExperienciaNoEst() throws Exception {
    return this.experienciaNoEstBeanBusiness.findExperienciaNoEstAll();
  }

  public Collection findExperienciaNoEstByPersonal(long idPersonal)
    throws Exception
  {
    return this.experienciaNoEstBeanBusiness.findByPersonal(idPersonal);
  }

  public void addFamiliar(Familiar familiar)
    throws Exception
  {
    this.familiarBeanBusiness.addFamiliar(familiar);
  }

  public void updateFamiliar(Familiar familiar) throws Exception {
    this.familiarBeanBusiness.updateFamiliar(familiar);
  }

  public void deleteFamiliar(Familiar familiar) throws Exception {
    this.familiarBeanBusiness.deleteFamiliar(familiar);
  }

  public Familiar findFamiliarById(long familiarId) throws Exception {
    return this.familiarBeanBusiness.findFamiliarById(familiarId);
  }

  public Collection findAllFamiliar() throws Exception {
    return this.familiarBeanBusiness.findFamiliarAll();
  }

  public Collection findFamiliarByPersonal(long idPersonal)
    throws Exception
  {
    return this.familiarBeanBusiness.findByPersonal(idPersonal);
  }

  public void addFamiliarRecaudo(FamiliarRecaudo familiarRecaudo)
    throws Exception
  {
    this.familiarRecaudoBeanBusiness.addFamiliarRecaudo(familiarRecaudo);
  }

  public void updateFamiliarRecaudo(FamiliarRecaudo familiarRecaudo) throws Exception {
    this.familiarRecaudoBeanBusiness.updateFamiliarRecaudo(familiarRecaudo);
  }

  public void deleteFamiliarRecaudo(FamiliarRecaudo familiarRecaudo) throws Exception {
    this.familiarRecaudoBeanBusiness.deleteFamiliarRecaudo(familiarRecaudo);
  }

  public FamiliarRecaudo findFamiliarRecaudoById(long familiarRecaudoId) throws Exception {
    return this.familiarRecaudoBeanBusiness.findFamiliarRecaudoById(familiarRecaudoId);
  }

  public Collection findAllFamiliarRecaudo() throws Exception {
    return this.familiarRecaudoBeanBusiness.findFamiliarRecaudoAll();
  }

  public Collection findFamiliarRecaudoByFamiliar(long idFamiliar)
    throws Exception
  {
    return this.familiarRecaudoBeanBusiness.findByFamiliar(idFamiliar);
  }

  public void addHabilidad(Habilidad habilidad)
    throws Exception
  {
    this.habilidadBeanBusiness.addHabilidad(habilidad);
  }

  public void updateHabilidad(Habilidad habilidad) throws Exception {
    this.habilidadBeanBusiness.updateHabilidad(habilidad);
  }

  public void deleteHabilidad(Habilidad habilidad) throws Exception {
    this.habilidadBeanBusiness.deleteHabilidad(habilidad);
  }

  public Habilidad findHabilidadById(long habilidadId) throws Exception {
    return this.habilidadBeanBusiness.findHabilidadById(habilidadId);
  }

  public Collection findAllHabilidad() throws Exception {
    return this.habilidadBeanBusiness.findHabilidadAll();
  }

  public Collection findHabilidadByPersonal(long idPersonal)
    throws Exception
  {
    return this.habilidadBeanBusiness.findByPersonal(idPersonal);
  }

  public void addHistorialApn(HistorialApn historialApn)
    throws Exception
  {
    this.historialApnBeanBusiness.addHistorialApn(historialApn);
  }

  public void updateHistorialApn(HistorialApn historialApn) throws Exception {
    this.historialApnBeanBusiness.updateHistorialApn(historialApn);
  }

  public void deleteHistorialApn(HistorialApn historialApn) throws Exception {
    this.historialApnBeanBusiness.deleteHistorialApn(historialApn);
  }

  public HistorialApn findHistorialApnById(long historialApnId) throws Exception {
    return this.historialApnBeanBusiness.findHistorialApnById(historialApnId);
  }

  public Collection findAllHistorialApn() throws Exception {
    return this.historialApnBeanBusiness.findHistorialApnAll();
  }

  public Collection findHistorialApnByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.historialApnBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addHistorialOrganismo(HistorialOrganismo historialOrganismo)
    throws Exception
  {
    this.historialOrganismoBeanBusiness.addHistorialOrganismo(historialOrganismo);
  }

  public void updateHistorialOrganismo(HistorialOrganismo historialOrganismo) throws Exception {
    this.historialOrganismoBeanBusiness.updateHistorialOrganismo(historialOrganismo);
  }

  public void deleteHistorialOrganismo(HistorialOrganismo historialOrganismo) throws Exception {
    this.historialOrganismoBeanBusiness.deleteHistorialOrganismo(historialOrganismo);
  }

  public HistorialOrganismo findHistorialOrganismoById(long historialOrganismoId) throws Exception {
    return this.historialOrganismoBeanBusiness.findHistorialOrganismoById(historialOrganismoId);
  }

  public Collection findAllHistorialOrganismo() throws Exception {
    return this.historialOrganismoBeanBusiness.findHistorialOrganismoAll();
  }

  public Collection findHistorialOrganismoByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.historialOrganismoBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addIdioma(Idioma idioma)
    throws Exception
  {
    this.idiomaBeanBusiness.addIdioma(idioma);
  }

  public void updateIdioma(Idioma idioma) throws Exception {
    this.idiomaBeanBusiness.updateIdioma(idioma);
  }

  public void deleteIdioma(Idioma idioma) throws Exception {
    this.idiomaBeanBusiness.deleteIdioma(idioma);
  }

  public Idioma findIdiomaById(long idiomaId) throws Exception {
    return this.idiomaBeanBusiness.findIdiomaById(idiomaId);
  }

  public Collection findAllIdioma() throws Exception {
    return this.idiomaBeanBusiness.findIdiomaAll();
  }

  public Collection findIdiomaByPersonal(long idPersonal)
    throws Exception
  {
    return this.idiomaBeanBusiness.findByPersonal(idPersonal);
  }

  public void addOtraActividad(OtraActividad otraActividad)
    throws Exception
  {
    this.otraActividadBeanBusiness.addOtraActividad(otraActividad);
  }

  public void updateOtraActividad(OtraActividad otraActividad) throws Exception {
    this.otraActividadBeanBusiness.updateOtraActividad(otraActividad);
  }

  public void deleteOtraActividad(OtraActividad otraActividad) throws Exception {
    this.otraActividadBeanBusiness.deleteOtraActividad(otraActividad);
  }

  public OtraActividad findOtraActividadById(long otraActividadId) throws Exception {
    return this.otraActividadBeanBusiness.findOtraActividadById(otraActividadId);
  }

  public Collection findAllOtraActividad() throws Exception {
    return this.otraActividadBeanBusiness.findOtraActividadAll();
  }

  public Collection findOtraActividadByPersonal(long idPersonal)
    throws Exception
  {
    return this.otraActividadBeanBusiness.findByPersonal(idPersonal);
  }

  public void addPasantia(Pasantia pasantia)
    throws Exception
  {
    this.pasantiaBeanBusiness.addPasantia(pasantia);
  }

  public void updatePasantia(Pasantia pasantia) throws Exception {
    this.pasantiaBeanBusiness.updatePasantia(pasantia);
  }

  public void deletePasantia(Pasantia pasantia) throws Exception {
    this.pasantiaBeanBusiness.deletePasantia(pasantia);
  }

  public Pasantia findPasantiaById(long pasantiaId) throws Exception {
    return this.pasantiaBeanBusiness.findPasantiaById(pasantiaId);
  }

  public Collection findAllPasantia() throws Exception {
    return this.pasantiaBeanBusiness.findPasantiaAll();
  }

  public Collection findPasantiaByPersonal(long idPersonal)
    throws Exception
  {
    return this.pasantiaBeanBusiness.findByPersonal(idPersonal);
  }

  public void updatePersonal(Personal personal)
    throws Exception
  {
    this.personalBeanBusiness.updatePersonal(personal);
  }

  public Personal findPersonalById(long personalId)
    throws Exception
  {
    return this.personalBeanBusiness.findPersonalById(personalId);
  }

  public Collection findAllPersonal() throws Exception {
    return this.personalBeanBusiness.findPersonalAll();
  }

  public Collection findPersonalByCedula(int cedula)
    throws Exception
  {
    return this.personalBeanBusiness.findByCedula(cedula);
  }

  public Collection findPersonalByPrimerApellido(String primerApellido)
    throws Exception
  {
    return this.personalBeanBusiness.findByPrimerApellido(primerApellido);
  }

  public Collection findPersonalByPrimerNombre(String primerNombre)
    throws Exception
  {
    return this.personalBeanBusiness.findByPrimerNombre(primerNombre);
  }

  public Collection findPersonalByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    return this.personalBeanBusiness.findByNombresApellidos(
      primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo);
  }

  public Collection findPersonalByCedula(int cedula, long idOrganismo) throws Exception
  {
    return this.personalBeanBusiness.findByCedula(cedula, idOrganismo);
  }

  public void addPersonal(Personal personal, long idOrganismo) throws Exception
  {
    this.personalBeanBusiness.addPersonal(personal, idOrganismo);
  }

  public void deletePersonal(Personal personal, long idOrganismo) throws Exception {
    this.personalBeanBusiness.deletePersonal(personal, idOrganismo);
  }

  public void addPersonalOrganismo(PersonalOrganismo personalOrganismo)
    throws Exception
  {
    this.personalOrganismoBeanBusiness.addPersonalOrganismo(personalOrganismo);
  }

  public void updatePersonalOrganismo(PersonalOrganismo personalOrganismo) throws Exception {
    this.personalOrganismoBeanBusiness.updatePersonalOrganismo(personalOrganismo);
  }

  public void deletePersonalOrganismo(PersonalOrganismo personalOrganismo) throws Exception {
    this.personalOrganismoBeanBusiness.deletePersonalOrganismo(personalOrganismo);
  }

  public PersonalOrganismo findPersonalOrganismoById(long personalOrganismoId) throws Exception {
    return this.personalOrganismoBeanBusiness.findPersonalOrganismoById(personalOrganismoId);
  }

  public Collection findAllPersonalOrganismo() throws Exception {
    return this.personalOrganismoBeanBusiness.findPersonalOrganismoAll();
  }

  public void addPersonalRecaudo(PersonalRecaudo personalRecaudo)
    throws Exception
  {
    this.personalRecaudoBeanBusiness.addPersonalRecaudo(personalRecaudo);
  }

  public void updatePersonalRecaudo(PersonalRecaudo personalRecaudo) throws Exception {
    this.personalRecaudoBeanBusiness.updatePersonalRecaudo(personalRecaudo);
  }

  public void deletePersonalRecaudo(PersonalRecaudo personalRecaudo) throws Exception {
    this.personalRecaudoBeanBusiness.deletePersonalRecaudo(personalRecaudo);
  }

  public PersonalRecaudo findPersonalRecaudoById(long personalRecaudoId) throws Exception {
    return this.personalRecaudoBeanBusiness.findPersonalRecaudoById(personalRecaudoId);
  }

  public Collection findAllPersonalRecaudo() throws Exception {
    return this.personalRecaudoBeanBusiness.findPersonalRecaudoAll();
  }

  public Collection findPersonalRecaudoByPersonal(long idPersonal)
    throws Exception
  {
    return this.personalRecaudoBeanBusiness.findByPersonal(idPersonal);
  }

  public void addProfesionTrabajador(ProfesionTrabajador profesionTrabajador)
    throws Exception
  {
    this.profesionTrabajadorBeanBusiness.addProfesionTrabajador(profesionTrabajador);
  }

  public void updateProfesionTrabajador(ProfesionTrabajador profesionTrabajador) throws Exception {
    this.profesionTrabajadorBeanBusiness.updateProfesionTrabajador(profesionTrabajador);
  }

  public void deleteProfesionTrabajador(ProfesionTrabajador profesionTrabajador) throws Exception {
    this.profesionTrabajadorBeanBusiness.deleteProfesionTrabajador(profesionTrabajador);
  }

  public ProfesionTrabajador findProfesionTrabajadorById(long profesionTrabajadorId) throws Exception {
    return this.profesionTrabajadorBeanBusiness.findProfesionTrabajadorById(profesionTrabajadorId);
  }

  public Collection findAllProfesionTrabajador() throws Exception {
    return this.profesionTrabajadorBeanBusiness.findProfesionTrabajadorAll();
  }

  public Collection findProfesionTrabajadorByPersonal(long idPersonal)
    throws Exception
  {
    return this.profesionTrabajadorBeanBusiness.findByPersonal(idPersonal);
  }

  public void addPublicacion(Publicacion publicacion)
    throws Exception
  {
    this.publicacionBeanBusiness.addPublicacion(publicacion);
  }

  public void updatePublicacion(Publicacion publicacion) throws Exception {
    this.publicacionBeanBusiness.updatePublicacion(publicacion);
  }

  public void deletePublicacion(Publicacion publicacion) throws Exception {
    this.publicacionBeanBusiness.deletePublicacion(publicacion);
  }

  public Publicacion findPublicacionById(long publicacionId) throws Exception {
    return this.publicacionBeanBusiness.findPublicacionById(publicacionId);
  }

  public Collection findAllPublicacion() throws Exception {
    return this.publicacionBeanBusiness.findPublicacionAll();
  }

  public Collection findPublicacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.publicacionBeanBusiness.findByPersonal(idPersonal);
  }

  public void addReconocimiento(Reconocimiento reconocimiento)
    throws Exception
  {
    this.reconocimientoBeanBusiness.addReconocimiento(reconocimiento);
  }

  public void updateReconocimiento(Reconocimiento reconocimiento) throws Exception {
    this.reconocimientoBeanBusiness.updateReconocimiento(reconocimiento);
  }

  public void deleteReconocimiento(Reconocimiento reconocimiento) throws Exception {
    this.reconocimientoBeanBusiness.deleteReconocimiento(reconocimiento);
  }

  public Reconocimiento findReconocimientoById(long reconocimientoId) throws Exception {
    return this.reconocimientoBeanBusiness.findReconocimientoById(reconocimientoId);
  }

  public Collection findAllReconocimiento() throws Exception {
    return this.reconocimientoBeanBusiness.findReconocimientoAll();
  }

  public Collection findReconocimientoByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.reconocimientoBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addSancion(Sancion sancion)
    throws Exception
  {
    this.sancionBeanBusiness.addSancion(sancion);
  }

  public void updateSancion(Sancion sancion) throws Exception {
    this.sancionBeanBusiness.updateSancion(sancion);
  }

  public void deleteSancion(Sancion sancion) throws Exception {
    this.sancionBeanBusiness.deleteSancion(sancion);
  }

  public Sancion findSancionById(long sancionId) throws Exception {
    return this.sancionBeanBusiness.findSancionById(sancionId);
  }

  public Collection findAllSancion() throws Exception {
    return this.sancionBeanBusiness.findSancionAll();
  }

  public Collection findSancionByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.sancionBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addServicioExterior(ServicioExterior servicioExterior)
    throws Exception
  {
    this.servicioExteriorBeanBusiness.addServicioExterior(servicioExterior);
  }

  public void updateServicioExterior(ServicioExterior servicioExterior) throws Exception {
    this.servicioExteriorBeanBusiness.updateServicioExterior(servicioExterior);
  }

  public void deleteServicioExterior(ServicioExterior servicioExterior) throws Exception {
    this.servicioExteriorBeanBusiness.deleteServicioExterior(servicioExterior);
  }

  public ServicioExterior findServicioExteriorById(long servicioExteriorId) throws Exception {
    return this.servicioExteriorBeanBusiness.findServicioExteriorById(servicioExteriorId);
  }

  public Collection findAllServicioExterior() throws Exception {
    return this.servicioExteriorBeanBusiness.findServicioExteriorAll();
  }

  public Collection findServicioExteriorByPersonal(long idPersonal)
    throws Exception
  {
    return this.servicioExteriorBeanBusiness.findByPersonal(idPersonal);
  }

  public void addSuplencia(Suplencia suplencia)
    throws Exception
  {
    this.suplenciaBeanBusiness.addSuplencia(suplencia);
  }

  public void updateSuplencia(Suplencia suplencia) throws Exception {
    this.suplenciaBeanBusiness.updateSuplencia(suplencia);
  }

  public void deleteSuplencia(Suplencia suplencia) throws Exception {
    this.suplenciaBeanBusiness.deleteSuplencia(suplencia);
  }

  public Suplencia findSuplenciaById(long suplenciaId) throws Exception {
    return this.suplenciaBeanBusiness.findSuplenciaById(suplenciaId);
  }

  public Collection findAllSuplencia() throws Exception {
    return this.suplenciaBeanBusiness.findSuplenciaAll();
  }

  public Collection findSuplenciaByPersonal(long idPersonal)
    throws Exception
  {
    return this.suplenciaBeanBusiness.findByPersonal(idPersonal);
  }

  public void addVacacion(Vacacion vacacion)
    throws Exception
  {
    this.vacacionBeanBusiness.addVacacion(vacacion);
  }

  public void updateVacacion(Vacacion vacacion) throws Exception {
    this.vacacionBeanBusiness.updateVacacion(vacacion);
  }

  public void deleteVacacion(Vacacion vacacion) throws Exception {
    this.vacacionBeanBusiness.deleteVacacion(vacacion);
  }

  public Vacacion findVacacionById(long vacacionId) throws Exception {
    return this.vacacionBeanBusiness.findVacacionById(vacacionId);
  }

  public Collection findAllVacacion() throws Exception {
    return this.vacacionBeanBusiness.findVacacionAll();
  }

  public Collection findVacacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.vacacionBeanBusiness.findByPersonal(idPersonal);
  }

  public Collection findVacacionByPersonalPendiente(long idPersonal) throws Exception
  {
    return this.vacacionBeanBusiness.findByPersonalPendiente(idPersonal);
  }

  public Collection findPersonalBySeguriodadUnidadFuncional(int cedula, long idOrganismo, long idUsuario, String administrador)
    throws Exception
  {
    return this.personalBeanBusiness.findBySeguriodadUnidadFuncional(cedula, idOrganismo, idUsuario, administrador);
  }

  public Collection findPersonalBySeguriodadUnidadFuncional(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    return this.personalBeanBusiness.findBySeguriodadUnidadFuncional(primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo, idUsuario, administrador);
  }

  public Collection findTrabajadorByCedulaAndEstatus(int cedula, long idOrganismo, long idUsuario, String administrador, String estatus)
    throws Exception
  {
    return this.trabajadorBeanBusiness.findByCedula(cedula, idOrganismo, idUsuario, administrador, estatus);
  }

  public void addCredencial(Credencial credencial) throws Exception
  {
    this.credencialBeanBusiness.addCredencial(credencial);
  }

  public void updateCredencial(Credencial credencial) throws Exception {
    this.credencialBeanBusiness.updateCredencial(credencial);
  }

  public void deleteCredencial(Credencial credencial) throws Exception {
    this.credencialBeanBusiness.deleteCredencial(credencial);
  }

  public Credencial findCredencialById(long vacacionId) throws Exception {
    return this.credencialBeanBusiness.findCredencialById(vacacionId);
  }

  public Collection findAllCredencial() throws Exception {
    return this.credencialBeanBusiness.findCredencialAll();
  }

  public Collection findCredencialByPersonal(long idPersonal)
    throws Exception
  {
    return this.credencialBeanBusiness.findByPersonal(idPersonal);
  }
}