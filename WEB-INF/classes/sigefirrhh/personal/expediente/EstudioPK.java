package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class EstudioPK
  implements Serializable
{
  public long idEstudio;

  public EstudioPK()
  {
  }

  public EstudioPK(long idEstudio)
  {
    this.idEstudio = idEstudio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EstudioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EstudioPK thatPK)
  {
    return 
      this.idEstudio == thatPK.idEstudio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEstudio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEstudio);
  }
}