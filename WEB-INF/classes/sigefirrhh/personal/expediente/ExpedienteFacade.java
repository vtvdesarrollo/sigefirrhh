package sigefirrhh.personal.expediente;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class ExpedienteFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ExpedienteBusiness expedienteBusiness = new ExpedienteBusiness();

  public void addVacacionProgramada(VacacionProgramada vacacionProgramada)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addVacacionProgramada(vacacionProgramada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateVacacionProgramada(VacacionProgramada vacacionProgramada) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateVacacionProgramada(vacacionProgramada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteVacacionProgramada(VacacionProgramada vacacionProgramada) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteVacacionProgramada(vacacionProgramada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public VacacionProgramada findVacacionProgramadaById(long vacacionProgramadaId) throws Exception
  {
    try { this.txn.open();
      VacacionProgramada vacacionProgramada = 
        this.expedienteBusiness.findVacacionProgramadaById(vacacionProgramadaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(vacacionProgramada);
      return vacacionProgramada;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllVacacionProgramada() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllVacacionProgramada();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findVacacionProgramadaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findVacacionProgramadaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addVacacionDisfrutada(vacacionDisfrutada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateVacacionDisfrutada(vacacionDisfrutada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteVacacionDisfrutada(VacacionDisfrutada vacacionDisfrutada) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteVacacionDisfrutada(vacacionDisfrutada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public VacacionDisfrutada findVacacionDisfrutadaById(long vacacionDisfrutadaId) throws Exception
  {
    try { this.txn.open();
      VacacionDisfrutada vacacionDisfrutada = 
        this.expedienteBusiness.findVacacionDisfrutadaById(vacacionDisfrutadaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(vacacionDisfrutada);
      return vacacionDisfrutada;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllVacacionDisfrutada() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllVacacionDisfrutada();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findVacacionDisfrutadaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findVacacionDisfrutadaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTrayectoria(Trayectoria trayectoria)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addTrayectoria(trayectoria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTrayectoria(Trayectoria trayectoria) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateTrayectoria(trayectoria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTrayectoria(Trayectoria trayectoria) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteTrayectoria(trayectoria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Trayectoria findTrayectoriaById(long trayectoriaId) throws Exception
  {
    try { this.txn.open();
      Trayectoria trayectoria = 
        this.expedienteBusiness.findTrayectoriaById(trayectoriaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(trayectoria);
      return trayectoria;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTrayectoria() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllTrayectoria();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findTrayectoriaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAcreencia(Acreencia acreencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addAcreencia(acreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAcreencia(Acreencia acreencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateAcreencia(acreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAcreencia(Acreencia acreencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteAcreencia(acreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Acreencia findAcreenciaById(long acreenciaId) throws Exception
  {
    try { this.txn.open();
      Acreencia acreencia = 
        this.expedienteBusiness.findAcreenciaById(acreenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(acreencia);
      return acreencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAcreencia() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllAcreencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAcreenciaByTipoAcreencia(long idTipoAcreencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findAcreenciaByTipoAcreencia(idTipoAcreencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAcreenciaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findAcreenciaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addActividadDocente(ActividadDocente actividadDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addActividadDocente(actividadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateActividadDocente(ActividadDocente actividadDocente) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateActividadDocente(actividadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteActividadDocente(ActividadDocente actividadDocente) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteActividadDocente(actividadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ActividadDocente findActividadDocenteById(long actividadDocenteId) throws Exception
  {
    try { this.txn.open();
      ActividadDocente actividadDocente = 
        this.expedienteBusiness.findActividadDocenteById(actividadDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(actividadDocente);
      return actividadDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllActividadDocente() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllActividadDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findActividadDocenteByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findActividadDocenteByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAfiliacion(Afiliacion afiliacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addAfiliacion(afiliacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAfiliacion(Afiliacion afiliacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateAfiliacion(afiliacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAfiliacion(Afiliacion afiliacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteAfiliacion(afiliacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Afiliacion findAfiliacionById(long afiliacionId) throws Exception
  {
    try { this.txn.open();
      Afiliacion afiliacion = 
        this.expedienteBusiness.findAfiliacionById(afiliacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(afiliacion);
      return afiliacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAfiliacion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllAfiliacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAfiliacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findAfiliacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAntecedente(Antecedente antecedente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addAntecedente(antecedente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAntecedente(Antecedente antecedente) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateAntecedente(antecedente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAntecedente(Antecedente antecedente) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteAntecedente(antecedente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Antecedente findAntecedenteById(long antecedenteId) throws Exception
  {
    try { this.txn.open();
      Antecedente antecedente = 
        this.expedienteBusiness.findAntecedenteById(antecedenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(antecedente);
      return antecedente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAntecedente() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllAntecedente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAntecedenteByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findAntecedenteByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAusencia(Ausencia ausencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addAusencia(ausencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAusencia(Ausencia ausencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateAusencia(ausencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAusencia(Ausencia ausencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteAusencia(ausencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Ausencia findAusenciaById(long ausenciaId) throws Exception
  {
    try { this.txn.open();
      Ausencia ausencia = 
        this.expedienteBusiness.findAusenciaById(ausenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(ausencia);
      return ausencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAusencia() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllAusencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAusenciaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findAusenciaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAusenciaByCedulaDirector(int cedulaDirector)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findAusenciaByCedulaDirector(cedulaDirector);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addAusenciaRecaudo(ausenciaRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateAusenciaRecaudo(ausenciaRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteAusenciaRecaudo(ausenciaRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AusenciaRecaudo findAusenciaRecaudoById(long ausenciaRecaudoId) throws Exception
  {
    try { this.txn.open();
      AusenciaRecaudo ausenciaRecaudo = 
        this.expedienteBusiness.findAusenciaRecaudoById(ausenciaRecaudoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(ausenciaRecaudo);
      return ausenciaRecaudo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAusenciaRecaudo() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllAusenciaRecaudo();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAveriguacion(Averiguacion averiguacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addAveriguacion(averiguacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAveriguacion(Averiguacion averiguacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateAveriguacion(averiguacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAveriguacion(Averiguacion averiguacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteAveriguacion(averiguacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Averiguacion findAveriguacionById(long averiguacionId) throws Exception
  {
    try { this.txn.open();
      Averiguacion averiguacion = 
        this.expedienteBusiness.findAveriguacionById(averiguacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(averiguacion);
      return averiguacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAveriguacion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllAveriguacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAveriguacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findAveriguacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCertificacion(Certificacion certificacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addCertificacion(certificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCertificacion(Certificacion certificacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateCertificacion(certificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCertificacion(Certificacion certificacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteCertificacion(certificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Certificacion findCertificacionById(long certificacionId) throws Exception
  {
    try { this.txn.open();
      Certificacion certificacion = 
        this.expedienteBusiness.findCertificacionById(certificacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(certificacion);
      return certificacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCertificacion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllCertificacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCertificacionByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findCertificacionByAreaConocimiento(idAreaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCertificacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findCertificacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCertificado(Certificado certificado)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addCertificado(certificado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCertificado(Certificado certificado) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateCertificado(certificado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCertificado(Certificado certificado) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteCertificado(certificado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Certificado findCertificadoById(long certificadoId) throws Exception
  {
    try { this.txn.open();
      Certificado certificado = 
        this.expedienteBusiness.findCertificadoById(certificadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(certificado);
      return certificado;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCertificado() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllCertificado();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCertificadoByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findCertificadoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addComisionServicio(ComisionServicio comisionServicio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addComisionServicio(comisionServicio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateComisionServicio(ComisionServicio comisionServicio) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateComisionServicio(comisionServicio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteComisionServicio(ComisionServicio comisionServicio) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteComisionServicio(comisionServicio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ComisionServicio findComisionServicioById(long comisionServicioId) throws Exception
  {
    try { this.txn.open();
      ComisionServicio comisionServicio = 
        this.expedienteBusiness.findComisionServicioById(comisionServicioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(comisionServicio);
      return comisionServicio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllComisionServicio() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllComisionServicio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findComisionServicioByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findComisionServicioByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addComisionServicioExt(ComisionServicioExt comisionServicioExt)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addComisionServicioExt(comisionServicioExt);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateComisionServicioExt(ComisionServicioExt comisionServicioExt) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateComisionServicioExt(comisionServicioExt);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteComisionServicioExt(ComisionServicioExt comisionServicioExt) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteComisionServicioExt(comisionServicioExt);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ComisionServicioExt findComisionServicioExtById(long comisionServicioExtId) throws Exception
  {
    try { this.txn.open();
      ComisionServicioExt comisionServicioExt = 
        this.expedienteBusiness.findComisionServicioExtById(comisionServicioExtId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(comisionServicioExt);
      return comisionServicioExt;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllComisionServicioExt() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllComisionServicioExt();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findComisionServicioExtByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findComisionServicioExtByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addContrato(Contrato contrato)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addContrato(contrato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateContrato(Contrato contrato) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateContrato(contrato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteContrato(Contrato contrato) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteContrato(contrato);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Contrato findContratoById(long contratoId) throws Exception
  {
    try { this.txn.open();
      Contrato contrato = 
        this.expedienteBusiness.findContratoById(contratoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(contrato);
      return contrato;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllContrato() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllContrato();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findContratoByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findContratoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDeclaracion(Declaracion declaracion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addDeclaracion(declaracion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDeclaracion(Declaracion declaracion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateDeclaracion(declaracion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDeclaracion(Declaracion declaracion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteDeclaracion(declaracion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Declaracion findDeclaracionById(long declaracionId) throws Exception
  {
    try { this.txn.open();
      Declaracion declaracion = 
        this.expedienteBusiness.findDeclaracionById(declaracionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(declaracion);
      return declaracion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDeclaracion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllDeclaracion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDeclaracionByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findDeclaracionByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEducacion(Educacion educacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addEducacion(educacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEducacion(Educacion educacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateEducacion(educacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEducacion(Educacion educacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteEducacion(educacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Educacion findEducacionById(long educacionId) throws Exception
  {
    try { this.txn.open();
      Educacion educacion = 
        this.expedienteBusiness.findEducacionById(educacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(educacion);
      return educacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEducacion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllEducacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEducacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findEducacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEncargaduria(Encargaduria encargaduria)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addEncargaduria(encargaduria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEncargaduria(Encargaduria encargaduria) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateEncargaduria(encargaduria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEncargaduria(Encargaduria encargaduria) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteEncargaduria(encargaduria);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Encargaduria findEncargaduriaById(long encargaduriaId) throws Exception
  {
    try { this.txn.open();
      Encargaduria encargaduria = 
        this.expedienteBusiness.findEncargaduriaById(encargaduriaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(encargaduria);
      return encargaduria;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEncargaduria() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllEncargaduria();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEncargaduriaByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findEncargaduriaByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEstudio(Estudio estudio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addEstudio(estudio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEstudio(Estudio estudio) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateEstudio(estudio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEstudio(Estudio estudio) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteEstudio(estudio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Estudio findEstudioById(long estudioId) throws Exception
  {
    try { this.txn.open();
      Estudio estudio = 
        this.expedienteBusiness.findEstudioById(estudioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(estudio);
      return estudio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEstudio() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllEstudio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstudioByTipoCurso(long idTipoCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findEstudioByTipoCurso(idTipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstudioByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findEstudioByAreaConocimiento(idAreaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEstudioByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findEstudioByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addExperiencia(Experiencia experiencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addExperiencia(experiencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateExperiencia(Experiencia experiencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateExperiencia(experiencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteExperiencia(Experiencia experiencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteExperiencia(experiencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Experiencia findExperienciaById(long experienciaId) throws Exception
  {
    try { this.txn.open();
      Experiencia experiencia = 
        this.expedienteBusiness.findExperienciaById(experienciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(experiencia);
      return experiencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllExperiencia() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllExperiencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExperienciaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findExperienciaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addExperienciaNoEst(ExperienciaNoEst experienciaNoEst)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addExperienciaNoEst(experienciaNoEst);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateExperienciaNoEst(ExperienciaNoEst experienciaNoEst) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateExperienciaNoEst(experienciaNoEst);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteExperienciaNoEst(ExperienciaNoEst experienciaNoEst) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteExperienciaNoEst(experienciaNoEst);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ExperienciaNoEst findExperienciaNoEstById(long experienciaNoEstId) throws Exception
  {
    try { this.txn.open();
      ExperienciaNoEst experienciaNoEst = 
        this.expedienteBusiness.findExperienciaNoEstById(experienciaNoEstId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(experienciaNoEst);
      return experienciaNoEst;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllExperienciaNoEst() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllExperienciaNoEst();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExperienciaNoEstByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findExperienciaNoEstByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addFamiliar(Familiar familiar)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addFamiliar(familiar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFamiliar(Familiar familiar) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateFamiliar(familiar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFamiliar(Familiar familiar) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteFamiliar(familiar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Familiar findFamiliarById(long familiarId) throws Exception
  {
    try { this.txn.open();
      Familiar familiar = 
        this.expedienteBusiness.findFamiliarById(familiarId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(familiar);
      return familiar;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFamiliar() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllFamiliar();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFamiliarByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findFamiliarByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addFamiliarRecaudo(FamiliarRecaudo familiarRecaudo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addFamiliarRecaudo(familiarRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFamiliarRecaudo(FamiliarRecaudo familiarRecaudo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateFamiliarRecaudo(familiarRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFamiliarRecaudo(FamiliarRecaudo familiarRecaudo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteFamiliarRecaudo(familiarRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FamiliarRecaudo findFamiliarRecaudoById(long familiarRecaudoId) throws Exception
  {
    try { this.txn.open();
      FamiliarRecaudo familiarRecaudo = 
        this.expedienteBusiness.findFamiliarRecaudoById(familiarRecaudoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(familiarRecaudo);
      return familiarRecaudo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFamiliarRecaudo() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllFamiliarRecaudo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFamiliarRecaudoByFamiliar(long idFamiliar)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findFamiliarRecaudoByFamiliar(idFamiliar);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHabilidad(Habilidad habilidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addHabilidad(habilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHabilidad(Habilidad habilidad) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateHabilidad(habilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHabilidad(Habilidad habilidad) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteHabilidad(habilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Habilidad findHabilidadById(long habilidadId) throws Exception
  {
    try { this.txn.open();
      Habilidad habilidad = 
        this.expedienteBusiness.findHabilidadById(habilidadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(habilidad);
      return habilidad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHabilidad() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllHabilidad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHabilidadByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findHabilidadByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHistorialApn(HistorialApn historialApn)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addHistorialApn(historialApn);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistorialApn(HistorialApn historialApn) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateHistorialApn(historialApn);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistorialApn(HistorialApn historialApn) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteHistorialApn(historialApn);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistorialApn findHistorialApnById(long historialApnId) throws Exception
  {
    try { this.txn.open();
      HistorialApn historialApn = 
        this.expedienteBusiness.findHistorialApnById(historialApnId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historialApn);
      return historialApn;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistorialApn() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllHistorialApn();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistorialApnByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findHistorialApnByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHistorialOrganismo(HistorialOrganismo historialOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addHistorialOrganismo(historialOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistorialOrganismo(HistorialOrganismo historialOrganismo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateHistorialOrganismo(historialOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistorialOrganismo(HistorialOrganismo historialOrganismo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteHistorialOrganismo(historialOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistorialOrganismo findHistorialOrganismoById(long historialOrganismoId) throws Exception
  {
    try { this.txn.open();
      HistorialOrganismo historialOrganismo = 
        this.expedienteBusiness.findHistorialOrganismoById(historialOrganismoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historialOrganismo);
      return historialOrganismo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistorialOrganismo() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllHistorialOrganismo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistorialOrganismoByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findHistorialOrganismoByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addIdioma(Idioma idioma)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addIdioma(idioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateIdioma(Idioma idioma) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateIdioma(idioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteIdioma(Idioma idioma) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteIdioma(idioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Idioma findIdiomaById(long idiomaId) throws Exception
  {
    try { this.txn.open();
      Idioma idioma = 
        this.expedienteBusiness.findIdiomaById(idiomaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(idioma);
      return idioma;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllIdioma() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllIdioma();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findIdiomaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findIdiomaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addOtraActividad(OtraActividad otraActividad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addOtraActividad(otraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateOtraActividad(OtraActividad otraActividad) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateOtraActividad(otraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteOtraActividad(OtraActividad otraActividad) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteOtraActividad(otraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public OtraActividad findOtraActividadById(long otraActividadId) throws Exception
  {
    try { this.txn.open();
      OtraActividad otraActividad = 
        this.expedienteBusiness.findOtraActividadById(otraActividadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(otraActividad);
      return otraActividad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllOtraActividad() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllOtraActividad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findOtraActividadByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findOtraActividadByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPasantia(Pasantia pasantia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addPasantia(pasantia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePasantia(Pasantia pasantia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updatePasantia(pasantia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePasantia(Pasantia pasantia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deletePasantia(pasantia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Pasantia findPasantiaById(long pasantiaId) throws Exception
  {
    try { this.txn.open();
      Pasantia pasantia = 
        this.expedienteBusiness.findPasantiaById(pasantiaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(pasantia);
      return pasantia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPasantia() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllPasantia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPasantiaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findPasantiaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void updatePersonal(Personal personal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.updatePersonal(personal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Personal findPersonalById(long personalId)
    throws Exception
  {
    try
    {
      this.txn.open();
      Personal personal = 
        this.expedienteBusiness.findPersonalById(personalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(personal);
      return personal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPersonal() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPersonalByCedula(int cedula)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findPersonalByCedula(cedula);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPersonalByPrimerApellido(String primerApellido)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findPersonalByPrimerApellido(primerApellido);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPersonalByPrimerNombre(String primerNombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findPersonalByPrimerNombre(primerNombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findPersonalByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findPersonalByNombresApellidos(
        primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPersonalByCedula(int cedula, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      return this.expedienteBusiness.findPersonalByCedula(cedula, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addPersonal(Personal personal, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      this.expedienteBusiness.addPersonal(personal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePersonal(Personal personal, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      this.expedienteBusiness.deletePersonal(personal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPersonalOrganismo(PersonalOrganismo personalOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addPersonalOrganismo(personalOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePersonalOrganismo(PersonalOrganismo personalOrganismo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updatePersonalOrganismo(personalOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePersonalOrganismo(PersonalOrganismo personalOrganismo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deletePersonalOrganismo(personalOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PersonalOrganismo findPersonalOrganismoById(long personalOrganismoId) throws Exception
  {
    try { this.txn.open();
      PersonalOrganismo personalOrganismo = 
        this.expedienteBusiness.findPersonalOrganismoById(personalOrganismoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(personalOrganismo);
      return personalOrganismo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPersonalOrganismo() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllPersonalOrganismo();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPersonalRecaudo(PersonalRecaudo personalRecaudo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addPersonalRecaudo(personalRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePersonalRecaudo(PersonalRecaudo personalRecaudo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updatePersonalRecaudo(personalRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePersonalRecaudo(PersonalRecaudo personalRecaudo) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deletePersonalRecaudo(personalRecaudo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PersonalRecaudo findPersonalRecaudoById(long personalRecaudoId) throws Exception
  {
    try { this.txn.open();
      PersonalRecaudo personalRecaudo = 
        this.expedienteBusiness.findPersonalRecaudoById(personalRecaudoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(personalRecaudo);
      return personalRecaudo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPersonalRecaudo() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllPersonalRecaudo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPersonalRecaudoByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findPersonalRecaudoByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addProfesionTrabajador(ProfesionTrabajador profesionTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addProfesionTrabajador(profesionTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateProfesionTrabajador(ProfesionTrabajador profesionTrabajador) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateProfesionTrabajador(profesionTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteProfesionTrabajador(ProfesionTrabajador profesionTrabajador) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteProfesionTrabajador(profesionTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ProfesionTrabajador findProfesionTrabajadorById(long profesionTrabajadorId) throws Exception
  {
    try { this.txn.open();
      ProfesionTrabajador profesionTrabajador = 
        this.expedienteBusiness.findProfesionTrabajadorById(profesionTrabajadorId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(profesionTrabajador);
      return profesionTrabajador;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllProfesionTrabajador() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllProfesionTrabajador();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProfesionTrabajadorByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findProfesionTrabajadorByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPublicacion(Publicacion publicacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addPublicacion(publicacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePublicacion(Publicacion publicacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updatePublicacion(publicacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePublicacion(Publicacion publicacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deletePublicacion(publicacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Publicacion findPublicacionById(long publicacionId) throws Exception
  {
    try { this.txn.open();
      Publicacion publicacion = 
        this.expedienteBusiness.findPublicacionById(publicacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(publicacion);
      return publicacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPublicacion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllPublicacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPublicacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findPublicacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addReconocimiento(Reconocimiento reconocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addReconocimiento(reconocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateReconocimiento(Reconocimiento reconocimiento) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateReconocimiento(reconocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteReconocimiento(Reconocimiento reconocimiento) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteReconocimiento(reconocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Reconocimiento findReconocimientoById(long reconocimientoId) throws Exception
  {
    try { this.txn.open();
      Reconocimiento reconocimiento = 
        this.expedienteBusiness.findReconocimientoById(reconocimientoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(reconocimiento);
      return reconocimiento;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllReconocimiento() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllReconocimiento();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findReconocimientoByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findReconocimientoByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSancion(Sancion sancion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addSancion(sancion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSancion(Sancion sancion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateSancion(sancion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSancion(Sancion sancion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteSancion(sancion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Sancion findSancionById(long sancionId) throws Exception
  {
    try { this.txn.open();
      Sancion sancion = 
        this.expedienteBusiness.findSancionById(sancionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(sancion);
      return sancion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSancion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllSancion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSancionByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findSancionByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addServicioExterior(ServicioExterior servicioExterior)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addServicioExterior(servicioExterior);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateServicioExterior(ServicioExterior servicioExterior) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateServicioExterior(servicioExterior);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteServicioExterior(ServicioExterior servicioExterior) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteServicioExterior(servicioExterior);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ServicioExterior findServicioExteriorById(long servicioExteriorId) throws Exception
  {
    try { this.txn.open();
      ServicioExterior servicioExterior = 
        this.expedienteBusiness.findServicioExteriorById(servicioExteriorId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(servicioExterior);
      return servicioExterior;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllServicioExterior() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllServicioExterior();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findServicioExteriorByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findServicioExteriorByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSuplencia(Suplencia suplencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addSuplencia(suplencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSuplencia(Suplencia suplencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateSuplencia(suplencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSuplencia(Suplencia suplencia) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteSuplencia(suplencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Suplencia findSuplenciaById(long suplenciaId) throws Exception
  {
    try { this.txn.open();
      Suplencia suplencia = 
        this.expedienteBusiness.findSuplenciaById(suplenciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(suplencia);
      return suplencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSuplencia() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllSuplencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSuplenciaByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findSuplenciaByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addVacacion(Vacacion vacacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.expedienteBusiness.addVacacion(vacacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateVacacion(Vacacion vacacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateVacacion(vacacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteVacacion(Vacacion vacacion) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteVacacion(vacacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Vacacion findVacacionById(long vacacionId) throws Exception
  {
    try { this.txn.open();
      Vacacion vacacion = 
        this.expedienteBusiness.findVacacionById(vacacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(vacacion);
      return vacacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllVacacion() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllVacacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findVacacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findVacacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findVacacionByPersonalPendiente(long idPersonal) throws Exception
  {
    try {
      this.txn.open();
      return this.expedienteBusiness.findVacacionByPersonalPendiente(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPersonalBySeguriodadUnidadFuncional(int cedula, long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    try {
      this.txn.open();
      return this.expedienteBusiness.findPersonalBySeguriodadUnidadFuncional(cedula, idOrganismo, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPersonalBySeguriodadUnidadFuncional(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    try {
      this.txn.open();
      return this.expedienteBusiness.findPersonalBySeguriodadUnidadFuncional(primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo, idUsuario, administrador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorByCedulaAndEstatus(int cedula, long idOrganismo, long idUsuario, String administrador, String estatus)
    throws Exception
  {
    return this.expedienteBusiness.findTrabajadorByCedulaAndEstatus(cedula, idOrganismo, idUsuario, administrador, estatus);
  }

  public void addCredencial(Credencial credencial) throws Exception {
    try {
      this.txn.open();
      this.expedienteBusiness.addCredencial(credencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCredencial(Credencial credencial) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.updateCredencial(credencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCredencial(Credencial credencial) throws Exception
  {
    try { this.txn.open();
      this.expedienteBusiness.deleteCredencial(credencial);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Credencial findCredencialById(long credencialId) throws Exception
  {
    try { this.txn.open();
      Credencial credencial = 
        this.expedienteBusiness.findCredencialById(credencialId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(credencial);
      return credencial;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCredencial() throws Exception
  {
    try { this.txn.open();
      return this.expedienteBusiness.findAllCredencial();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCredencialByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.expedienteBusiness.findCredencialByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}