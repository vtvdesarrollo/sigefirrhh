package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Municipio;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.Parroquia;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class PersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PersonalForm.class.getName());
  private Personal personal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private boolean showPersonalByCedula;
  private boolean showPersonalByPrimerApellido;
  private boolean showPersonalByPrimerNombre;
  private int findCedula;
  private String findPrimerApellido;
  private String findPrimerNombre;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private Collection colPaisForCiudadNacimiento;
  private Collection colEstadoForCiudadNacimiento;
  private Collection colCiudadNacimiento;
  private Collection colPaisNacionalidad;
  private Collection colPaisForCiudadResidencia;
  private Collection colEstadoForCiudadResidencia;
  private Collection colCiudadResidencia;
  private Collection colPaisForParroquia;
  private Collection colEstadoForParroquia;
  private Collection colMunicipioForParroquia;
  private Collection colParroquia;
  private Collection colEstablecimientoSalud;
  private String selectPaisForCiudadNacimiento;
  private String selectEstadoForCiudadNacimiento;
  private String selectCiudadNacimiento;
  private String selectPaisNacionalidad;
  private String selectPaisForCiudadResidencia;
  private String selectEstadoForCiudadResidencia;
  private String selectCiudadResidencia;
  private String selectPaisForParroquia;
  private String selectEstadoForParroquia;
  private String selectMunicipioForParroquia;
  private String selectParroquia;
  private String selectEstablecimientoSalud;
  private Object stateScrollPersonalByPrimerApellido = null;
  private Object stateResultPersonalByPrimerApellido = null;

  private Object stateScrollPersonalByPrimerNombre = null;
  private Object stateResultPersonalByPrimerNombre = null;

  private Object stateScrollPersonal = null;
  private Object stateResultPersonal = null;
  private boolean showFotoTrabajador;

  public int getFindCedula()
  {
    return this.findCedula;
  }

  public void setFindCedula(int findCedula) {
    this.findCedula = findCedula;
  }

  public String getFindPrimerApellido() {
    return this.findPrimerApellido;
  }

  public void setFindPrimerApellido(String findPrimerApellido) {
    this.findPrimerApellido = findPrimerApellido;
  }

  public String getFindPrimerNombre() {
    return this.findPrimerNombre;
  }

  public void setFindPrimerNombre(String findPrimerNombre)
  {
    this.findPrimerNombre = findPrimerNombre;
  }

  public String getSelectPaisForCiudadNacimiento()
  {
    return this.selectPaisForCiudadNacimiento;
  }

  public void setSelectPaisForCiudadNacimiento(String valPaisForCiudadNacimiento)
  {
    this.selectPaisForCiudadNacimiento = valPaisForCiudadNacimiento;
  }

  public void changePaisForCiudadNacimiento(ValueChangeEvent event) {
    long idPais = Long.valueOf((String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadNacimiento = null;
      this.colEstadoForCiudadNacimiento = null;
      if (idPais > 0L) {
        this.colEstadoForCiudadNacimiento = this.ubicacionFacade
          .findEstadoByPais(idPais);
      } else {
        this.selectCiudadNacimiento = null;
        this.personal.setCiudadNacimiento(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadNacimiento = null;
      this.personal.setCiudadNacimiento(null);
    }
  }

  public boolean isShowPaisForCiudadNacimiento() {
    return this.colPaisForCiudadNacimiento != null;
  }

  public String getSelectEstadoForCiudadNacimiento() {
    return this.selectEstadoForCiudadNacimiento;
  }

  public void setSelectEstadoForCiudadNacimiento(String valEstadoForCiudadNacimiento)
  {
    this.selectEstadoForCiudadNacimiento = valEstadoForCiudadNacimiento;
  }

  public void changeEstadoForCiudadNacimiento(ValueChangeEvent event) {
    long idEstado = Long.valueOf((String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadNacimiento = null;
      if (idEstado > 0L) {
        this.colCiudadNacimiento = this.ubicacionFacade
          .findCiudadByEstado(idEstado);
      } else {
        this.selectCiudadNacimiento = null;
        this.personal.setCiudadNacimiento(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadNacimiento = null;
      this.personal.setCiudadNacimiento(null);
    }
  }

  public boolean isShowEstadoForCiudadNacimiento() {
    return this.colEstadoForCiudadNacimiento != null;
  }

  public String getSelectCiudadNacimiento() {
    return this.selectCiudadNacimiento;
  }

  public void setSelectCiudadNacimiento(String valCiudadNacimiento) {
    Iterator iterator = this.colCiudadNacimiento.iterator();
    Ciudad ciudadNacimiento = null;
    this.personal.setCiudadNacimiento(null);
    while (iterator.hasNext()) {
      ciudadNacimiento = (Ciudad)iterator.next();
      if (String.valueOf(ciudadNacimiento.getIdCiudad()).equals(
        valCiudadNacimiento)) {
        this.personal.setCiudadNacimiento(ciudadNacimiento);
      }
    }
    this.selectCiudadNacimiento = valCiudadNacimiento;
  }

  public boolean isShowCiudadNacimiento() {
    return this.colCiudadNacimiento != null;
  }

  public String getSelectPaisNacionalidad() {
    return this.selectPaisNacionalidad;
  }

  public void setSelectPaisNacionalidad(String valPaisNacionalidad) {
    Iterator iterator = this.colPaisNacionalidad.iterator();
    Pais paisNacionalidad = null;
    this.personal.setPaisNacionalidad(null);
    while (iterator.hasNext()) {
      paisNacionalidad = (Pais)iterator.next();
      if (String.valueOf(paisNacionalidad.getIdPais()).equals(
        valPaisNacionalidad)) {
        this.personal.setPaisNacionalidad(paisNacionalidad);
      }
    }
    this.selectPaisNacionalidad = valPaisNacionalidad;
  }

  public String getSelectPaisForCiudadResidencia() {
    return this.selectPaisForCiudadResidencia;
  }

  public void setSelectPaisForCiudadResidencia(String valPaisForCiudadResidencia)
  {
    this.selectPaisForCiudadResidencia = valPaisForCiudadResidencia;
  }

  public void changePaisForCiudadResidencia(ValueChangeEvent event) {
    long idPais = Long.valueOf((String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadResidencia = null;
      this.colEstadoForCiudadResidencia = null;
      this.colMunicipioForParroquia = null;
      if (idPais > 0L) {
        this.colEstadoForCiudadResidencia = this.ubicacionFacade
          .findEstadoByPais(idPais);
      } else {
        this.selectCiudadResidencia = null;
        this.selectMunicipioForParroquia = null;
        this.selectParroquia = null;
        this.personal.setParroquia(null);
        this.personal.setCiudadResidencia(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadResidencia = null;
      this.selectMunicipioForParroquia = null;
      this.selectParroquia = null;
      this.personal.setParroquia(null);
      this.personal.setCiudadResidencia(null);
    }
  }

  public boolean isShowPaisForCiudadResidencia() {
    return this.colPaisForCiudadResidencia != null;
  }

  public String getSelectEstadoForCiudadResidencia() {
    return this.selectEstadoForCiudadResidencia;
  }

  public void setSelectEstadoForCiudadResidencia(String valEstadoForCiudadResidencia)
  {
    this.selectEstadoForCiudadResidencia = valEstadoForCiudadResidencia;
  }

  public void changeEstadoForCiudadResidencia(ValueChangeEvent event) {
    long idEstado = Long.valueOf((String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadResidencia = null;
      this.colMunicipioForParroquia = null;
      if (idEstado > 0L) {
        this.colCiudadResidencia = this.ubicacionFacade
          .findCiudadByEstado(idEstado);
        this.colMunicipioForParroquia = this.ubicacionFacade
          .findMunicipioByEstado(idEstado);
      } else {
        this.selectCiudadResidencia = null;
        this.selectParroquia = null;
        this.personal.setParroquia(null);
        this.personal.setCiudadResidencia(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadResidencia = null;
      this.selectParroquia = null;
      this.personal.setParroquia(null);
      this.personal.setCiudadResidencia(null);
    }
  }

  public boolean isShowEstadoForCiudadResidencia() {
    return this.colEstadoForCiudadResidencia != null;
  }

  public String getSelectCiudadResidencia() {
    return this.selectCiudadResidencia;
  }

  public void setSelectCiudadResidencia(String valCiudadResidencia) {
    Iterator iterator = this.colCiudadResidencia.iterator();
    Ciudad ciudadResidencia = null;
    this.personal.setCiudadResidencia(null);
    while (iterator.hasNext()) {
      ciudadResidencia = (Ciudad)iterator.next();
      if (String.valueOf(ciudadResidencia.getIdCiudad()).equals(
        valCiudadResidencia)) {
        this.personal.setCiudadResidencia(ciudadResidencia);
      }
    }
    this.selectCiudadResidencia = valCiudadResidencia;
  }

  public boolean isShowCiudadResidencia() {
    return this.colCiudadResidencia != null;
  }

  public String getSelectPaisForParroquia() {
    return this.selectPaisForParroquia;
  }

  public void setSelectPaisForParroquia(String valPaisForParroquia) {
    this.selectPaisForParroquia = valPaisForParroquia;
  }

  public void changePaisForParroquia(ValueChangeEvent event) {
    long idPais = Long.valueOf((String)event.getNewValue()).longValue();
    try
    {
      this.colParroquia = null;
      this.colMunicipioForParroquia = null;
      this.colEstadoForParroquia = null;
      if (idPais > 0L) {
        this.colEstadoForParroquia = this.ubicacionFacade
          .findEstadoByPais(idPais);
      } else {
        this.selectParroquia = null;
        this.personal.setParroquia(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectParroquia = null;
      this.personal.setParroquia(null);
    }
  }

  public boolean isShowPaisForParroquia() {
    return this.colPaisForParroquia != null;
  }

  public String getSelectEstadoForParroquia() {
    return this.selectEstadoForParroquia;
  }

  public void setSelectEstadoForParroquia(String valEstadoForParroquia) {
    this.selectEstadoForParroquia = valEstadoForParroquia;
  }

  public void changeEstadoForParroquia(ValueChangeEvent event) {
    long idEstado = Long.valueOf((String)event.getNewValue()).longValue();
    try
    {
      this.colParroquia = null;
      this.colMunicipioForParroquia = null;
      if (idEstado > 0L) {
        this.colMunicipioForParroquia = this.ubicacionFacade
          .findMunicipioByEstado(idEstado);
      } else {
        this.selectParroquia = null;
        this.personal.setParroquia(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectParroquia = null;
      this.personal.setParroquia(null);
    }
  }

  public boolean isShowEstadoForParroquia() {
    return this.colEstadoForParroquia != null;
  }

  public String getSelectMunicipioForParroquia() {
    return this.selectMunicipioForParroquia;
  }

  public void setSelectMunicipioForParroquia(String valMunicipioForParroquia) {
    this.selectMunicipioForParroquia = valMunicipioForParroquia;
  }

  public void changeMunicipioForParroquia(ValueChangeEvent event) {
    long idMunicipio = Long.valueOf((String)event.getNewValue())
      .longValue();
    try
    {
      this.colParroquia = null;
      if (idMunicipio > 0L) {
        this.colParroquia = this.ubicacionFacade
          .findParroquiaByMunicipio(idMunicipio);
      } else {
        this.selectParroquia = null;
        this.personal.setParroquia(null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectParroquia = null;
      this.personal.setParroquia(null);
    }
  }

  public boolean isShowMunicipioForParroquia() {
    return this.colMunicipioForParroquia != null;
  }

  public String getSelectParroquia() {
    return this.selectParroquia;
  }

  public void setSelectParroquia(String valParroquia) {
    Iterator iterator = this.colParroquia.iterator();
    Parroquia parroquia = null;
    this.personal.setParroquia(null);
    while (iterator.hasNext()) {
      parroquia = (Parroquia)iterator.next();
      if (String.valueOf(parroquia.getIdParroquia()).equals(valParroquia)) {
        this.personal.setParroquia(parroquia);
      }
    }
    this.selectParroquia = valParroquia;
  }

  public boolean isShowParroquia() {
    return this.colParroquia != null;
  }

  public String getSelectEstablecimientoSalud() {
    return this.selectEstablecimientoSalud;
  }

  public void setSelectEstablecimientoSalud(String valEstablecimientoSalud) {
    Iterator iterator = this.colEstablecimientoSalud.iterator();
    EstablecimientoSalud establecimientoSalud = null;
    this.personal.setEstablecimientoSalud(null);
    while (iterator.hasNext()) {
      establecimientoSalud = (EstablecimientoSalud)iterator.next();

      if (String.valueOf(establecimientoSalud.getIdEstablecimientoSalud())
        .equals(valEstablecimientoSalud)) {
        this.personal.setEstablecimientoSalud(establecimientoSalud);
      }
    }
    this.selectEstablecimientoSalud = valEstablecimientoSalud;
  }

  public Collection getResult() {
    return this.result;
  }

  public Personal getPersonal() {
    if (this.personal == null) {
      this.personal = new Personal();
    }
    return this.personal;
  }

  public PersonalForm() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication()
      .getVariableResolver().resolveVariable(context, "loginSession"));
    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getListSexo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SEXO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudadNacimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudadNacimiento.iterator();
    Pais paisForCiudadNacimiento = null;
    while (iterator.hasNext()) {
      paisForCiudadNacimiento = (Pais)iterator.next();
      col.add(new SelectItem(String.valueOf(paisForCiudadNacimiento
        .getIdPais()), paisForCiudadNacimiento.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudadNacimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudadNacimiento.iterator();
    Estado estadoForCiudadNacimiento = null;
    while (iterator.hasNext()) {
      estadoForCiudadNacimiento = (Estado)iterator.next();
      col.add(new SelectItem(String.valueOf(estadoForCiudadNacimiento
        .getIdEstado()), estadoForCiudadNacimiento.toString()));
    }
    return col;
  }

  public Collection getColCiudadNacimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudadNacimiento.iterator();
    Ciudad ciudadNacimiento = null;
    while (iterator.hasNext()) {
      ciudadNacimiento = (Ciudad)iterator.next();
      col.add(new SelectItem(String.valueOf(ciudadNacimiento
        .getIdCiudad()), ciudadNacimiento.toString()));
    }
    return col;
  }

  public Collection getListEstadoCivil() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_ESTADO_CIVIL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListNivelEducativo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_NIVEL_EDUCATIVO.entrySet()
      .iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListNacionalidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_NACIONALIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListDobleNacionalidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListdiscapacidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListtipodiscapacidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_TIPO_DISCAPACIDAD.entrySet()
      .iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListpuebloindigena() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_PUEBLO_INDIGENA.entrySet()
      .iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getColPaisNacionalidad() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisNacionalidad.iterator();
    Pais paisNacionalidad = null;
    while (iterator.hasNext()) {
      paisNacionalidad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisNacionalidad.getIdPais()), 
        paisNacionalidad.toString()));
    }
    return col;
  }

  public Collection getListNacionalizado() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudadResidencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudadResidencia.iterator();
    Pais paisForCiudadResidencia = null;
    while (iterator.hasNext()) {
      paisForCiudadResidencia = (Pais)iterator.next();
      col.add(new SelectItem(String.valueOf(paisForCiudadResidencia
        .getIdPais()), paisForCiudadResidencia.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudadResidencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudadResidencia.iterator();
    Estado estadoForCiudadResidencia = null;
    while (iterator.hasNext()) {
      estadoForCiudadResidencia = (Estado)iterator.next();
      col.add(new SelectItem(String.valueOf(estadoForCiudadResidencia
        .getIdEstado()), estadoForCiudadResidencia.toString()));
    }
    return col;
  }

  public Collection getColCiudadResidencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudadResidencia.iterator();
    Ciudad ciudadResidencia = null;
    while (iterator.hasNext()) {
      ciudadResidencia = (Ciudad)iterator.next();
      col.add(new SelectItem(String.valueOf(ciudadResidencia
        .getIdCiudad()), ciudadResidencia.toString()));
    }
    return col;
  }

  public Collection getColPaisForParroquia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForParroquia.iterator();
    Pais paisForParroquia = null;
    while (iterator.hasNext()) {
      paisForParroquia = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForParroquia.getIdPais()), 
        paisForParroquia.toString()));
    }
    return col;
  }

  public Collection getColEstadoForParroquia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForParroquia.iterator();
    Estado estadoForParroquia = null;
    while (iterator.hasNext()) {
      estadoForParroquia = (Estado)iterator.next();
      col.add(new SelectItem(String.valueOf(estadoForParroquia
        .getIdEstado()), estadoForParroquia.toString()));
    }
    return col;
  }

  public Collection getColMunicipioForParroquia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colMunicipioForParroquia.iterator();
    Municipio municipioForParroquia = null;
    while (iterator.hasNext()) {
      municipioForParroquia = (Municipio)iterator.next();
      col.add(new SelectItem(String.valueOf(municipioForParroquia
        .getIdMunicipio()), municipioForParroquia.toString()));
    }
    return col;
  }

  public Collection getColParroquia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colParroquia.iterator();
    Parroquia parroquia = null;
    while (iterator.hasNext()) {
      parroquia = (Parroquia)iterator.next();
      col.add(new SelectItem(String.valueOf(parroquia.getIdParroquia()), 
        parroquia.toString()));
    }
    return col;
  }

  public Collection getListSectorTrabajoConyugue() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SECTOR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListMismoOrganismoConyugue() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListMadrePadre() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListTieneHijos() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListTipoVivienda() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_TIPO_VIVIENDA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListTenenciaVivienda() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_TENENCIA_VIVIENDA.entrySet()
      .iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListManeja() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListTieneVehiculo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListDiestralidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_DIESTRALIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getListGrupoSanguineo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Personal.LISTA_GRUPO_SANGUINEO.entrySet()
      .iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(entry.getKey().toString(), entry.getValue()
        .toString()));
    }
    return col;
  }

  public Collection getColEstablecimientoSalud() {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstablecimientoSalud.iterator();
    EstablecimientoSalud establecimientoSalud = null;
    while (iterator.hasNext()) {
      establecimientoSalud = (EstablecimientoSalud)iterator.next();
      col.add(new SelectItem(String.valueOf(establecimientoSalud
        .getIdEstablecimientoSalud()), establecimientoSalud
        .toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colPaisForCiudadNacimiento = this.ubicacionFacade
        .findAllPais();

      this.colPaisNacionalidad = this.ubicacionFacade.findAllPais();

      this.colPaisForCiudadResidencia = this.ubicacionFacade
        .findAllPais();

      this.colPaisForParroquia = this.ubicacionFacade.findAllPais();

      this.colEstablecimientoSalud = this.bienestarFacade
        .findAllEstablecimientoSalud();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowPersonalByCedula()
  {
    return this.showPersonalByCedula;
  }

  public boolean isShowPersonalByPrimerApellido() {
    return this.showPersonalByPrimerApellido;
  }

  public boolean isShowPersonalByPrimerNombre() {
    return this.showPersonalByPrimerNombre;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = context.getExternalContext()
      .getRequestParameterMap();

    this.selectCiudadNacimiento = null;
    this.selectPaisForCiudadNacimiento = null;

    this.selectEstadoForCiudadNacimiento = null;

    this.selectPaisNacionalidad = null;
    this.selectCiudadResidencia = null;
    this.selectPaisForCiudadResidencia = null;

    this.selectEstadoForCiudadResidencia = null;

    this.selectParroquia = null;
    this.selectPaisForParroquia = null;

    this.selectEstadoForParroquia = null;

    this.selectMunicipioForParroquia = null;

    this.selectEstablecimientoSalud = null;

    long idPersonal = Long.parseLong((String)requestParameterMap
      .get("idPersonal"));
    try
    {
      this.personal = this.expedienteFacade.findPersonalById(idPersonal);
      if (this.personal.getCiudadNacimiento() != null) {
        this.selectCiudadNacimiento = String.valueOf(this.personal
          .getCiudadNacimiento().getIdCiudad());
      }
      if (this.personal.getPaisNacionalidad() != null) {
        this.selectPaisNacionalidad = String.valueOf(this.personal
          .getPaisNacionalidad().getIdPais());
      }
      if (this.personal.getCiudadResidencia() != null) {
        this.selectCiudadResidencia = String.valueOf(this.personal
          .getCiudadResidencia().getIdCiudad());
      }
      if (this.personal.getParroquia() != null) {
        this.selectParroquia = String.valueOf(this.personal
          .getParroquia().getIdParroquia());
      }
      if (this.personal.getEstablecimientoSalud() != null) {
        this.selectEstablecimientoSalud = String.valueOf(this.personal
          .getEstablecimientoSalud().getIdEstablecimientoSalud());
      }

      if ((this.personal.getFotoTrabajador() != null) && (!this.personal.getFotoTrabajador().equals(""))) {
        this.showFotoTrabajador = true;
      }
      else {
        this.showFotoTrabajador = false;
      }

      Ciudad ciudadNacimiento = null;
      Estado estadoForCiudadNacimiento = null;
      Pais paisForCiudadNacimiento = null;
      Ciudad ciudadResidencia = null;
      Estado estadoForCiudadResidencia = null;
      Pais paisForCiudadResidencia = null;
      Parroquia parroquia = null;
      Municipio municipioForParroquia = null;
      Estado estadoForParroquia = null;
      Pais paisForParroquia = null;

      if (this.personal.getCiudadNacimiento() != null) {
        long idCiudadNacimiento = this.personal.getCiudadNacimiento()
          .getIdCiudad();
        this.selectCiudadNacimiento = 
          String.valueOf(idCiudadNacimiento);
        ciudadNacimiento = this.ubicacionFacade
          .findCiudadById(idCiudadNacimiento);
        this.colCiudadNacimiento = this.ubicacionFacade
          .findCiudadByEstado(ciudadNacimiento.getEstado()
          .getIdEstado());

        long idEstadoForCiudadNacimiento = this.personal
          .getCiudadNacimiento().getEstado().getIdEstado();
        this.selectEstadoForCiudadNacimiento = 
          String.valueOf(idEstadoForCiudadNacimiento);
        estadoForCiudadNacimiento = this.ubicacionFacade
          .findEstadoById(idEstadoForCiudadNacimiento);
        this.colEstadoForCiudadNacimiento = this.ubicacionFacade
          .findEstadoByPais(estadoForCiudadNacimiento.getPais()
          .getIdPais());

        long idPaisForCiudadNacimiento = estadoForCiudadNacimiento
          .getPais().getIdPais();
        this.selectPaisForCiudadNacimiento = 
          String.valueOf(idPaisForCiudadNacimiento);
        paisForCiudadNacimiento = this.ubicacionFacade
          .findPaisById(idPaisForCiudadNacimiento);
        this.colPaisForCiudadNacimiento = this.ubicacionFacade.findAllPais();
      }
      if (this.personal.getCiudadResidencia() != null) {
        long idCiudadResidencia = this.personal.getCiudadResidencia()
          .getIdCiudad();
        this.selectCiudadResidencia = 
          String.valueOf(idCiudadResidencia);
        ciudadResidencia = this.ubicacionFacade
          .findCiudadById(idCiudadResidencia);
        this.colCiudadResidencia = this.ubicacionFacade
          .findCiudadByEstado(ciudadResidencia.getEstado()
          .getIdEstado());

        long idEstadoForCiudadResidencia = this.personal
          .getCiudadResidencia().getEstado().getIdEstado();
        this.selectEstadoForCiudadResidencia = 
          String.valueOf(idEstadoForCiudadResidencia);
        estadoForCiudadResidencia = this.ubicacionFacade
          .findEstadoById(idEstadoForCiudadResidencia);
        this.colEstadoForCiudadResidencia = this.ubicacionFacade
          .findEstadoByPais(estadoForCiudadResidencia.getPais()
          .getIdPais());

        long idPaisForCiudadResidencia = estadoForCiudadResidencia
          .getPais().getIdPais();
        this.selectPaisForCiudadResidencia = 
          String.valueOf(idPaisForCiudadResidencia);
        paisForCiudadResidencia = this.ubicacionFacade
          .findPaisById(idPaisForCiudadResidencia);
        this.colPaisForCiudadResidencia = this.ubicacionFacade.findAllPais();
      }
      if (this.personal.getParroquia() != null) {
        long idParroquia = this.personal.getParroquia()
          .getIdParroquia();
        this.selectParroquia = String.valueOf(idParroquia);
        parroquia = this.ubicacionFacade.findParroquiaById(idParroquia);
        this.colParroquia = this.ubicacionFacade
          .findParroquiaByMunicipio(parroquia.getMunicipio()
          .getIdMunicipio());

        long idMunicipioForParroquia = this.personal.getParroquia()
          .getMunicipio().getIdMunicipio();
        this.selectMunicipioForParroquia = 
          String.valueOf(idMunicipioForParroquia);
        municipioForParroquia = this.ubicacionFacade
          .findMunicipioById(idMunicipioForParroquia);
        this.colMunicipioForParroquia = this.ubicacionFacade
          .findMunicipioByEstado(municipioForParroquia
          .getEstado().getIdEstado());

        long idEstadoForParroquia = municipioForParroquia.getEstado()
          .getIdEstado();
        this.selectEstadoForParroquia = 
          String.valueOf(idEstadoForParroquia);
        estadoForParroquia = this.ubicacionFacade
          .findEstadoById(idEstadoForParroquia);
        this.colEstadoForParroquia = this.ubicacionFacade
          .findEstadoByPais(estadoForParroquia.getPais()
          .getIdPais());

        long idPaisForParroquia = estadoForParroquia.getPais()
          .getIdPais();
        this.selectPaisForParroquia = 
          String.valueOf(idPaisForParroquia);
        paisForParroquia = this.ubicacionFacade
          .findPaisById(idPaisForParroquia);
        this.colPaisForParroquia = this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.personal = null;
    this.showPersonalByCedula = false;
    this.showPersonalByPrimerApellido = false;
    this.showPersonalByPrimerNombre = false;
  }

  public String edit() {
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    return null;
  }

  public String save() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    if ((!this.personal.getEstadoCivil().equals("C")) && 
      (!this.personal
      .getEstadoCivil().equals("U"))) {
      this.personal.setCedulaConyugue(0);
      this.personal.setNombreConyugue(null);
      this.personal.setSectorTrabajoConyugue("N");
      this.personal.setMismoOrganismoConyugue("N");
    }
    if (this.personal.getNacionalizado().equals("N")) {
      this.personal.setFechaNacionalizacion(null);
      this.personal.setGacetaNacionalizacion(null);
      this.personal.setOtraNormativaNac(null);
    }
    if (this.personal.getDobleNacionalidad().equals("N")) {
      this.personal.setPaisNacionalidad(null);
    }
    if (this.personal.getDiscapacidad().equals("N")) {
      this.personal.setTipodiscapacidad("0");
    }
    this.personal.getTipodiscapacidad().equals("S");

    if (this.personal.getTieneVehiculo().equals("N")) {
      this.personal.setMarcaVehiculo(null);
      this.personal.setPlacaVehiculo(null);
    }
    if ((this.personal.getFechaNacimiento() != null) && 
      (this.personal.getFechaNacimiento().compareTo(new Date()) > 0)) {
      context
        .addMessage(
        "error_data", 
        new FacesMessage(
        FacesMessage.SEVERITY_ERROR, 
        "La fecha Fecha Nacimiento no puede ser mayor a la fecha de hoy", 
        ""));
      error = true;
    }
    if (this.personal.getFechaNacionalizacion() != null)
    {
      if (this.personal.getFechaNacionalizacion()
        .compareTo(new Date()) > 0) {
        context
          .addMessage(
          "error_data", 
          new FacesMessage(
          FacesMessage.SEVERITY_ERROR, 
          "La fecha Fecha Nacionalización no puede ser mayor a la fecha de hoy", 
          ""));
        error = true;
      }
    }
    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.expedienteFacade.addPersonal(this.personal, this.login
          .getOrganismo().getIdOrganismo());
        RegistrarAuditoria.registrar(context, this.login
          .getUsuarioObject(), 'A', 
          this.personal, this.personal);
        context.addMessage("success_add", new FacesMessage(
          "Se agregó con éxito"));
      } else {
        this.expedienteFacade.updatePersonal(this.personal);
        RegistrarAuditoria.registrar(context, this.login
          .getUsuarioObject(), 'M', 
          this.personal, this.personal);
        context.addMessage("success_modify", new FacesMessage(
          "Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
    } catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(
          FacesMessage.SEVERITY_ERROR, 
          "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context
          .addMessage(
          "error_data", 
          new FacesMessage(
          FacesMessage.SEVERITY_ERROR, 
          "Hubo un error de inconsistencia de datos al modificar ", 
          ""));
      }
    }
    return "cancel";
  }

  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deletePersonal(this.personal, this.login
        .getOrganismo().getIdOrganismo());
      RegistrarAuditoria.registrar(context, 
        this.login.getUsuarioObject(), 'E', 
        this.personal, this.personal);

      context.addMessage("success_delete", new FacesMessage(
        "Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context
        .addMessage(
        "error_data", 
        new FacesMessage(
        FacesMessage.SEVERITY_ERROR, 
        "No se puede eliminar por tener información asociada ", 
        ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    this.personal = new Personal();

    setShowFotoTrabajador(false);

    this.selectCiudadNacimiento = null;

    this.selectPaisForCiudadNacimiento = null;

    this.selectEstadoForCiudadNacimiento = null;

    this.selectPaisNacionalidad = null;

    this.selectCiudadResidencia = null;

    this.selectPaisForCiudadResidencia = null;

    this.selectEstadoForCiudadResidencia = null;

    this.selectParroquia = null;

    this.selectPaisForParroquia = null;

    this.selectEstadoForParroquia = null;

    this.selectMunicipioForParroquia = null;

    this.selectEstablecimientoSalud = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.personal
      .setIdPersonal(identityGenerator
      .getNextSequenceNumber("sigefirrhh.personal.expediente.Personal"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.scrollx = 0;
    this.scrolly = 0;
    resetResult();
    this.personal = new Personal();
    return "cancel";
  }

  public String abortUpdate()
  {
    this.showPersonalByCedula = true;
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.personal = new Personal();
    return "cancel";
  }

  public boolean isShowPaisNacionalidadAux() {
    try {
      return this.personal.getDobleNacionalidad().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFechaNacionalizacionAux()
  {
    try {
      return this.personal.getNacionalizado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowGacetaNacionalizacionAux()
  {
    try {
      return this.personal.getNacionalizado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowOtraNormativaNacAux()
  {
    try {
      return this.personal.getNacionalizado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCedulaConyugueAux()
  {
    try
    {
      return (this.personal.getEstadoCivil().equals("C")) || 
        (this.personal.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNombreConyugueAux()
  {
    try
    {
      return (this.personal.getEstadoCivil().equals("C")) || 
        (this.personal.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowSectorTrabajoConyugueAux()
  {
    try
    {
      return (this.personal.getEstadoCivil().equals("C")) || 
        (this.personal.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowMismoOrganismoConyugueAux()
  {
    try
    {
      return (this.personal.getEstadoCivil().equals("C")) || 
        (this.personal.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowMarcaVehiculoAux()
  {
    try {
      return this.personal.getTieneVehiculo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowModeloVehiculoAux()
  {
    try {
      return this.personal.getTieneVehiculo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowPlacaVehiculoAux()
  {
    try {
      return this.personal.getTieneVehiculo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }

  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public int getScrollx() {
    return this.scrollx;
  }

  public int getScrolly() {
    return this.scrolly;
  }

  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }

  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      resetResult();

      this.result = null;
      this.showPersonalByCedula = false;

      this.result = this.expedienteFacade.findPersonalByCedula(
        this.findPersonalCedula, this.login.getOrganismo()
        .getIdOrganismo());
      this.showPersonalByCedula = ((this.result != null) && 
        (!this.result
        .isEmpty()));

      if (!this.showPersonalByCedula)
        context.addMessage("error_data", new FacesMessage(
          FacesMessage.SEVERITY_ERROR, 
          "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      this.result = null;
      this.showPersonalByCedula = false;

      if (((this.findPersonalPrimerNombre == null) || 
        (this.findPersonalPrimerNombre
        .equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || 
        (this.findPersonalSegundoNombre
        .equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || 
        (this.findPersonalPrimerApellido
        .equals(""))) && (
        (this.findPersonalSegundoApellido == null) || 
        (this.findPersonalSegundoApellido
        .equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(
          FacesMessage.SEVERITY_ERROR, 
          "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.result = this.expedienteFacade
          .findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, this.login
          .getOrganismo().getIdOrganismo());
        this.showPersonalByCedula = ((this.result != null) && 
          (!this.result
          .isEmpty()));
        if (!this.showPersonalByCedula)
          context
            .addMessage(
            "error_data", 
            new FacesMessage(
            FacesMessage.SEVERITY_ERROR, 
            "No hay información con el criterio solicitado", 
            ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }

  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }

  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }

  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }

  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }

  public void setFindPersonalCedula(int i) {
    this.findPersonalCedula = i;
  }

  public void setFindPersonalPrimerApellido(String string) {
    this.findPersonalPrimerApellido = string;
  }

  public void setFindPersonalPrimerNombre(String string) {
    this.findPersonalPrimerNombre = string;
  }

  public void setFindPersonalSegundoApellido(String string) {
    this.findPersonalSegundoApellido = string;
  }

  public void setFindPersonalSegundoNombre(String string) {
    this.findPersonalSegundoNombre = string;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public void setShowFotoTrabajador(boolean showFotoTrabajador)
  {
    this.showFotoTrabajador = showFotoTrabajador;
  }

  public boolean isShowFotoTrabajador()
  {
    return this.showFotoTrabajador;
  }
}