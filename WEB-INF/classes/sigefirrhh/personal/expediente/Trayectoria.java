package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Trayectoria
  implements Serializable, PersistenceCapable
{
  private long idTrayectoria;
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private int anioPreparacion;
  private Date fechaPreparacion;
  private String codigoAnteriorMpd;
  private String numeroRemesa;
  private int numeroMovimiento;
  private int correlativoMpd;
  private String codOrganismo;
  private String nombreCorto;
  private String nombreOrganismo;
  private String estatus;
  private Date fechaEstatus;
  private Date fechaVigencia;
  private Date fechaCulminacion;
  private String codUbiGeografico;
  private String estado;
  private String ciudad;
  private String municipio;
  private String codRegion;
  private String nombreRegion;
  private String codDependencia;
  private String nombreDependencia;
  private String codCausaMovimiento;
  private String descripcionMovimiento;
  private String codGrupoOrganismo;
  private String nombreCortoGrupo;
  private String nombreLargoGrupo;
  private String caucion;
  private String codManualCargo;
  private String codCargo;
  private String descripcionCargo;
  private String codRelacion;
  private String descRelacion;
  private String codCategoria;
  private String descCategoria;
  private String nombramiento;
  private int grado;
  private int paso;
  private int codigoNomina;
  private double montoJubilacion;
  private double porcJubilacion;
  private double sueldoPromedio;
  private double montoJubilacionSobrev;
  private double porcPensionSobrev;
  private double montoPensionSobrev;
  private double montoPensionInvalid;
  private double porcPensionInvalid;
  private double invalidezSact;
  private double sueldoBasico;
  private double compensacion;
  private double primaJerarquia;
  private double primaServicio;
  private double ajusteSueldo;
  private double otrosPagos;
  private double otrosNoVicepladin;
  private double primasCargo;
  private double primasTrabajador;
  private String puntoCuenta;
  private Date fechaPuntoCuenta;
  private String codConcurso;
  private String observaciones;
  private String usuario;
  private String origen;
  private Personal personal;
  private double horas;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ajusteSueldo", "anioPreparacion", "caucion", "cedula", "ciudad", "codCargo", "codCategoria", "codCausaMovimiento", "codConcurso", "codDependencia", "codGrupoOrganismo", "codManualCargo", "codOrganismo", "codRegion", "codRelacion", "codUbiGeografico", "codigoAnteriorMpd", "codigoNomina", "compensacion", "correlativoMpd", "descCategoria", "descRelacion", "descripcionCargo", "descripcionMovimiento", "estado", "estatus", "fechaCulminacion", "fechaEstatus", "fechaPreparacion", "fechaPuntoCuenta", "fechaVigencia", "grado", "horas", "idTrayectoria", "invalidezSact", "montoJubilacion", "montoJubilacionSobrev", "montoPensionInvalid", "montoPensionSobrev", "municipio", "nombramiento", "nombreCorto", "nombreCortoGrupo", "nombreDependencia", "nombreLargoGrupo", "nombreOrganismo", "nombreRegion", "numeroMovimiento", "numeroRemesa", "observaciones", "origen", "otrosNoVicepladin", "otrosPagos", "paso", "personal", "porcJubilacion", "porcPensionInvalid", "porcPensionSobrev", "primaJerarquia", "primaServicio", "primasCargo", "primasTrabajador", "primerApellido", "primerNombre", "puntoCuenta", "segundoApellido", "segundoNombre", "sueldoBasico", "sueldoPromedio", "usuario" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Double.TYPE, Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Trayectoria()
  {
    jdoSetcedula(this, 0);

    jdoSetnumeroMovimiento(this, 0);

    jdoSetcorrelativoMpd(this, 0);

    jdoSetpaso(this, 1);

    jdoSetmontoJubilacion(this, 0.0D);

    jdoSetporcJubilacion(this, 0.0D);

    jdoSetsueldoPromedio(this, 0.0D);

    jdoSetmontoJubilacionSobrev(this, 0.0D);

    jdoSetporcPensionSobrev(this, 0.0D);

    jdoSetmontoPensionSobrev(this, 0.0D);

    jdoSetmontoPensionInvalid(this, 0.0D);

    jdoSetporcPensionInvalid(this, 0.0D);

    jdoSetinvalidezSact(this, 0.0D);

    jdoSetsueldoBasico(this, 0.0D);

    jdoSetcompensacion(this, 0.0D);

    jdoSetprimaJerarquia(this, 0.0D);

    jdoSetprimaServicio(this, 0.0D);

    jdoSetajusteSueldo(this, 0.0D);

    jdoSetotrosPagos(this, 0.0D);

    jdoSetotrosNoVicepladin(this, 0.0D);

    jdoSetprimasCargo(this, 0.0D);

    jdoSetprimasTrabajador(this, 0.0D);

    jdoSetorigen(this, "S");

    jdoSethoras(this, 0.0D);
  }

  public String toString() {
    return 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaVigencia(this)) + " - " + 
      jdoGetdescripcionMovimiento(this) + " - " + jdoGetdescripcionCargo(this) + " - " + jdoGetnombreCorto(this);
  }

  public double getHoras() {
    return jdoGethoras(this);
  }
  public void setHoras(double horas) {
    jdoSethoras(this, horas);
  }
  public String getNombramiento() {
    return jdoGetnombramiento(this);
  }
  public void setNombramiento(String nombramiento) {
    jdoSetnombramiento(this, nombramiento);
  }
  public double getAjusteSueldo() {
    return jdoGetajusteSueldo(this);
  }
  public void setAjusteSueldo(double ajusteSueldo) {
    jdoSetajusteSueldo(this, ajusteSueldo);
  }
  public int getAnioPreparacion() {
    return jdoGetanioPreparacion(this);
  }
  public void setAnioPreparacion(int anioPreparacion) {
    jdoSetanioPreparacion(this, anioPreparacion);
  }
  public String getCaucion() {
    return jdoGetcaucion(this);
  }
  public void setCaucion(String caucion) {
    jdoSetcaucion(this, caucion);
  }
  public int getCedula() {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }
  public String getCiudad() {
    return jdoGetciudad(this);
  }
  public void setCiudad(String ciudad) {
    jdoSetciudad(this, ciudad);
  }
  public String getCodCargo() {
    return jdoGetcodCargo(this);
  }
  public void setCodCargo(String codCargo) {
    jdoSetcodCargo(this, codCargo);
  }
  public String getCodCausaMovimiento() {
    return jdoGetcodCausaMovimiento(this);
  }
  public void setCodCausaMovimiento(String codCausaMovimiento) {
    jdoSetcodCausaMovimiento(this, codCausaMovimiento);
  }
  public String getCodDependencia() {
    return jdoGetcodDependencia(this);
  }
  public void setCodDependencia(String codDependencia) {
    jdoSetcodDependencia(this, codDependencia);
  }
  public String getCodigoAnteriorMpd() {
    return jdoGetcodigoAnteriorMpd(this);
  }
  public void setCodigoAnteriorMpd(String codigoAnteriorMpd) {
    jdoSetcodigoAnteriorMpd(this, codigoAnteriorMpd);
  }
  public int getCodigoNomina() {
    return jdoGetcodigoNomina(this);
  }
  public void setCodigoNomina(int codigoNomina) {
    jdoSetcodigoNomina(this, codigoNomina);
  }
  public String getCodManualCargo() {
    return jdoGetcodManualCargo(this);
  }
  public void setCodManualCargo(String codManualCargo) {
    jdoSetcodManualCargo(this, codManualCargo);
  }
  public String getCodUbiGeografico() {
    return jdoGetcodUbiGeografico(this);
  }
  public void setCodUbiGeografico(String codUbiGeografico) {
    jdoSetcodUbiGeografico(this, codUbiGeografico);
  }
  public double getCompensacion() {
    return jdoGetcompensacion(this);
  }
  public void setCompensacion(double compensacion) {
    jdoSetcompensacion(this, compensacion);
  }
  public int getCorrelativoMpd() {
    return jdoGetcorrelativoMpd(this);
  }
  public void setCorrelativoMpd(int correlativoMpd) {
    jdoSetcorrelativoMpd(this, correlativoMpd);
  }
  public String getDescripcionCargo() {
    return jdoGetdescripcionCargo(this);
  }
  public void setDescripcionCargo(String descripcionCargo) {
    jdoSetdescripcionCargo(this, descripcionCargo);
  }
  public String getDescripcionMovimiento() {
    return jdoGetdescripcionMovimiento(this);
  }
  public void setDescripcionMovimiento(String descripcionMovimiento) {
    jdoSetdescripcionMovimiento(this, descripcionMovimiento);
  }
  public String getEstado() {
    return jdoGetestado(this);
  }
  public void setEstado(String estado) {
    jdoSetestado(this, estado);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public Date getFechaEstatus() {
    return jdoGetfechaEstatus(this);
  }
  public void setFechaEstatus(Date fechaEstatus) {
    jdoSetfechaEstatus(this, fechaEstatus);
  }
  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }
  public void setFechaVigencia(Date fechaVigencia) {
    jdoSetfechaVigencia(this, fechaVigencia);
  }
  public int getGrado() {
    return jdoGetgrado(this);
  }
  public void setGrado(int grado) {
    jdoSetgrado(this, grado);
  }
  public long getIdTrayectoria() {
    return jdoGetidTrayectoria(this);
  }
  public void setIdTrayectoria(long idTrayectoria) {
    jdoSetidTrayectoria(this, idTrayectoria);
  }
  public double getInvalidezSact() {
    return jdoGetinvalidezSact(this);
  }
  public void setInvalidezSact(double invalidezSact) {
    jdoSetinvalidezSact(this, invalidezSact);
  }
  public double getMontoJubilacion() {
    return jdoGetmontoJubilacion(this);
  }
  public void setMontoJubilacion(double montoJubilacion) {
    jdoSetmontoJubilacion(this, montoJubilacion);
  }
  public double getMontoJubilacionSobrev() {
    return jdoGetmontoJubilacionSobrev(this);
  }
  public void setMontoJubilacionSobrev(double montoJubilacionSobrev) {
    jdoSetmontoJubilacionSobrev(this, montoJubilacionSobrev);
  }
  public double getMontoPensionInvalid() {
    return jdoGetmontoPensionInvalid(this);
  }
  public void setMontoPensionInvalid(double montoPensionInvalid) {
    jdoSetmontoPensionInvalid(this, montoPensionInvalid);
  }
  public double getMontoPensionSobrev() {
    return jdoGetmontoPensionSobrev(this);
  }
  public void setMontoPensionSobrev(double montoPensionSobrev) {
    jdoSetmontoPensionSobrev(this, montoPensionSobrev);
  }
  public String getMunicipio() {
    return jdoGetmunicipio(this);
  }
  public void setMunicipio(String municipio) {
    jdoSetmunicipio(this, municipio);
  }
  public String getNombreCorto() {
    return jdoGetnombreCorto(this);
  }
  public void setNombreCorto(String nombreCorto) {
    jdoSetnombreCorto(this, nombreCorto);
  }
  public String getNombreCortoGrupo() {
    return jdoGetnombreCortoGrupo(this);
  }
  public void setNombreCortoGrupo(String nombreCortoGrupo) {
    jdoSetnombreCortoGrupo(this, nombreCortoGrupo);
  }
  public String getNombreDependencia() {
    return jdoGetnombreDependencia(this);
  }
  public void setNombreDependencia(String nombreDependencia) {
    jdoSetnombreDependencia(this, nombreDependencia);
  }
  public String getNombreLargoGrupo() {
    return jdoGetnombreLargoGrupo(this);
  }
  public void setNombreLargoGrupo(String nombreLargoGrupo) {
    jdoSetnombreLargoGrupo(this, nombreLargoGrupo);
  }
  public String getNombreOrganismo() {
    return jdoGetnombreOrganismo(this);
  }
  public void setNombreOrganismo(String nombreOrganismo) {
    jdoSetnombreOrganismo(this, nombreOrganismo);
  }
  public int getNumeroMovimiento() {
    return jdoGetnumeroMovimiento(this);
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    jdoSetnumeroMovimiento(this, numeroMovimiento);
  }
  public String getNumeroRemesa() {
    return jdoGetnumeroRemesa(this);
  }
  public void setNumeroRemesa(String numeroRemesa) {
    jdoSetnumeroRemesa(this, numeroRemesa);
  }
  public String getOrigen() {
    return jdoGetorigen(this);
  }
  public void setOrigen(String origen) {
    jdoSetorigen(this, origen);
  }
  public double getOtrosNoVicepladin() {
    return jdoGetotrosNoVicepladin(this);
  }
  public void setOtrosNoVicepladin(double otrosNoVicepladin) {
    jdoSetotrosNoVicepladin(this, otrosNoVicepladin);
  }
  public double getOtrosPagos() {
    return jdoGetotrosPagos(this);
  }
  public void setOtrosPagos(double otrosPagos) {
    jdoSetotrosPagos(this, otrosPagos);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public double getPorcJubilacion() {
    return jdoGetporcJubilacion(this);
  }
  public void setPorcJubilacion(double porcJubilacion) {
    jdoSetporcJubilacion(this, porcJubilacion);
  }
  public double getPorcPensionInvalid() {
    return jdoGetporcPensionInvalid(this);
  }
  public void setPorcPensionInvalid(double porcPensionInvalid) {
    jdoSetporcPensionInvalid(this, porcPensionInvalid);
  }
  public double getPorcPensionSobrev() {
    return jdoGetporcPensionSobrev(this);
  }
  public void setPorcPensionSobrev(double porcPensionSobrev) {
    jdoSetporcPensionSobrev(this, porcPensionSobrev);
  }
  public double getPrimaJerarquia() {
    return jdoGetprimaJerarquia(this);
  }
  public void setPrimaJerarquia(double primaJerarquia) {
    jdoSetprimaJerarquia(this, primaJerarquia);
  }
  public double getPrimasCargo() {
    return jdoGetprimasCargo(this);
  }
  public void setPrimasCargo(double primasCargo) {
    jdoSetprimasCargo(this, primasCargo);
  }
  public double getPrimaServicio() {
    return jdoGetprimaServicio(this);
  }
  public void setPrimaServicio(double primaServicio) {
    jdoSetprimaServicio(this, primaServicio);
  }
  public double getPrimasTrabajador() {
    return jdoGetprimasTrabajador(this);
  }
  public void setPrimasTrabajador(double primasTrabajador) {
    jdoSetprimasTrabajador(this, primasTrabajador);
  }
  public String getPrimerApellido() {
    return jdoGetprimerApellido(this);
  }
  public void setPrimerApellido(String primerApellido) {
    jdoSetprimerApellido(this, primerApellido);
  }
  public String getPrimerNombre() {
    return jdoGetprimerNombre(this);
  }
  public void setPrimerNombre(String primerNombre) {
    jdoSetprimerNombre(this, primerNombre);
  }
  public String getSegundoApellido() {
    return jdoGetsegundoApellido(this);
  }
  public void setSegundoApellido(String segundoApellido) {
    jdoSetsegundoApellido(this, segundoApellido);
  }
  public String getSegundoNombre() {
    return jdoGetsegundoNombre(this);
  }
  public void setSegundoNombre(String segundoNombre) {
    jdoSetsegundoNombre(this, segundoNombre);
  }
  public double getSueldoBasico() {
    return jdoGetsueldoBasico(this);
  }
  public void setSueldoBasico(double sueldoBasico) {
    jdoSetsueldoBasico(this, sueldoBasico);
  }
  public double getSueldoPromedio() {
    return jdoGetsueldoPromedio(this);
  }
  public void setSueldoPromedio(double sueldoPromedio) {
    jdoSetsueldoPromedio(this, sueldoPromedio);
  }
  public String getCodConcurso() {
    return jdoGetcodConcurso(this);
  }
  public void setCodConcurso(String codConcurso) {
    jdoSetcodConcurso(this, codConcurso);
  }
  public Date getFechaPuntoCuenta() {
    return jdoGetfechaPuntoCuenta(this);
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    jdoSetfechaPuntoCuenta(this, fechaPuntoCuenta);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public int getPaso() {
    return jdoGetpaso(this);
  }
  public void setPaso(int paso) {
    jdoSetpaso(this, paso);
  }
  public String getPuntoCuenta() {
    return jdoGetpuntoCuenta(this);
  }
  public void setPuntoCuenta(String puntoCuenta) {
    jdoSetpuntoCuenta(this, puntoCuenta);
  }
  public String getUsuario() {
    return jdoGetusuario(this);
  }
  public void setUsuario(String usuario) {
    jdoSetusuario(this, usuario);
  }
  public String getCodRegion() {
    return jdoGetcodRegion(this);
  }
  public void setCodRegion(String codRegion) {
    jdoSetcodRegion(this, codRegion);
  }
  public String getNombreRegion() {
    return jdoGetnombreRegion(this);
  }
  public void setNombreRegion(String nombreRegion) {
    jdoSetnombreRegion(this, nombreRegion);
  }
  public String getCodCategoria() {
    return jdoGetcodCategoria(this);
  }
  public void setCodCategoria(String codCategoria) {
    jdoSetcodCategoria(this, codCategoria);
  }
  public String getCodRelacion() {
    return jdoGetcodRelacion(this);
  }
  public void setCodRelacion(String codRelacion) {
    jdoSetcodRelacion(this, codRelacion);
  }
  public String getDescCategoria() {
    return jdoGetdescCategoria(this);
  }
  public void setDescCategoria(String descCategoria) {
    jdoSetdescCategoria(this, descCategoria);
  }
  public String getDescRelacion() {
    return jdoGetdescRelacion(this);
  }
  public void setDescRelacion(String descRelacion) {
    jdoSetdescRelacion(this, descRelacion);
  }
  public String getCodGrupoOrganismo() {
    return jdoGetcodGrupoOrganismo(this);
  }
  public void setCodGrupoOrganismo(String codGrupoOrganismo) {
    jdoSetcodGrupoOrganismo(this, codGrupoOrganismo);
  }
  public Date getFechaPreparacion() {
    return jdoGetfechaPreparacion(this);
  }
  public void setFechaPreparacion(Date fechaPreparacion) {
    jdoSetfechaPreparacion(this, fechaPreparacion);
  }

  public String getCodOrganismo() {
    return jdoGetcodOrganismo(this);
  }

  public void setCodOrganismo(String codOrganismo) {
    jdoSetcodOrganismo(this, codOrganismo);
  }

  public Date getFechaCulminacion() {
    return jdoGetfechaCulminacion(this);
  }

  public void setFechaCulminacion(Date fechaCulminacion) {
    jdoSetfechaCulminacion(this, fechaCulminacion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 70;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Trayectoria"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Trayectoria());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Trayectoria localTrayectoria = new Trayectoria();
    localTrayectoria.jdoFlags = 1;
    localTrayectoria.jdoStateManager = paramStateManager;
    return localTrayectoria;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Trayectoria localTrayectoria = new Trayectoria();
    localTrayectoria.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTrayectoria.jdoFlags = 1;
    localTrayectoria.jdoStateManager = paramStateManager;
    return localTrayectoria;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ajusteSueldo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioPreparacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.caucion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ciudad);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCategoria);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaMovimiento);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcurso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codGrupoOrganismo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codManualCargo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegion);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRelacion);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUbiGeografico);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoAnteriorMpd);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacion);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.correlativoMpd);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descCategoria);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descRelacion);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionCargo);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionMovimiento);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estado);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCulminacion);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEstatus);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPreparacion);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPuntoCuenta);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horas);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTrayectoria);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.invalidezSact);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoJubilacion);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoJubilacionSobrev);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPensionInvalid);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPensionSobrev);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.municipio);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombramiento);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCorto);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCortoGrupo);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDependencia);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreLargoGrupo);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreOrganismo);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreRegion);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMovimiento);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroRemesa);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.otrosNoVicepladin);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.otrosPagos);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcJubilacion);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcPensionInvalid);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcPensionSobrev);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaJerarquia);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaServicio);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasCargo);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasTrabajador);
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.puntoCuenta);
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoBasico);
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoPromedio);
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ajusteSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioPreparacion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.caucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCategoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGrupoOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codManualCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRelacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUbiGeografico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoAnteriorMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.correlativoMpd = localStateManager.replacingIntField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descCategoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descRelacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCulminacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEstatus = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPreparacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPuntoCuenta = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTrayectoria = localStateManager.replacingLongField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.invalidezSact = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoJubilacionSobrev = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPensionInvalid = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPensionSobrev = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.municipio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombramiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCorto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCortoGrupo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreLargoGrupo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMovimiento = localStateManager.replacingIntField(this, paramInt);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroRemesa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otrosNoVicepladin = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otrosPagos = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcPensionInvalid = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcPensionSobrev = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaJerarquia = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaServicio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.puntoCuenta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoBasico = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoPromedio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Trayectoria paramTrayectoria, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.ajusteSueldo = paramTrayectoria.ajusteSueldo;
      return;
    case 1:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.anioPreparacion = paramTrayectoria.anioPreparacion;
      return;
    case 2:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.caucion = paramTrayectoria.caucion;
      return;
    case 3:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramTrayectoria.cedula;
      return;
    case 4:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramTrayectoria.ciudad;
      return;
    case 5:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramTrayectoria.codCargo;
      return;
    case 6:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codCategoria = paramTrayectoria.codCategoria;
      return;
    case 7:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaMovimiento = paramTrayectoria.codCausaMovimiento;
      return;
    case 8:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codConcurso = paramTrayectoria.codConcurso;
      return;
    case 9:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramTrayectoria.codDependencia;
      return;
    case 10:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codGrupoOrganismo = paramTrayectoria.codGrupoOrganismo;
      return;
    case 11:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codManualCargo = paramTrayectoria.codManualCargo;
      return;
    case 12:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismo = paramTrayectoria.codOrganismo;
      return;
    case 13:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codRegion = paramTrayectoria.codRegion;
      return;
    case 14:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codRelacion = paramTrayectoria.codRelacion;
      return;
    case 15:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codUbiGeografico = paramTrayectoria.codUbiGeografico;
      return;
    case 16:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codigoAnteriorMpd = paramTrayectoria.codigoAnteriorMpd;
      return;
    case 17:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramTrayectoria.codigoNomina;
      return;
    case 18:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramTrayectoria.compensacion;
      return;
    case 19:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.correlativoMpd = paramTrayectoria.correlativoMpd;
      return;
    case 20:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.descCategoria = paramTrayectoria.descCategoria;
      return;
    case 21:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.descRelacion = paramTrayectoria.descRelacion;
      return;
    case 22:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionCargo = paramTrayectoria.descripcionCargo;
      return;
    case 23:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionMovimiento = paramTrayectoria.descripcionMovimiento;
      return;
    case 24:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.estado = paramTrayectoria.estado;
      return;
    case 25:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramTrayectoria.estatus;
      return;
    case 26:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCulminacion = paramTrayectoria.fechaCulminacion;
      return;
    case 27:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEstatus = paramTrayectoria.fechaEstatus;
      return;
    case 28:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPreparacion = paramTrayectoria.fechaPreparacion;
      return;
    case 29:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPuntoCuenta = paramTrayectoria.fechaPuntoCuenta;
      return;
    case 30:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramTrayectoria.fechaVigencia;
      return;
    case 31:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramTrayectoria.grado;
      return;
    case 32:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramTrayectoria.horas;
      return;
    case 33:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.idTrayectoria = paramTrayectoria.idTrayectoria;
      return;
    case 34:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.invalidezSact = paramTrayectoria.invalidezSact;
      return;
    case 35:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.montoJubilacion = paramTrayectoria.montoJubilacion;
      return;
    case 36:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.montoJubilacionSobrev = paramTrayectoria.montoJubilacionSobrev;
      return;
    case 37:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.montoPensionInvalid = paramTrayectoria.montoPensionInvalid;
      return;
    case 38:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.montoPensionSobrev = paramTrayectoria.montoPensionSobrev;
      return;
    case 39:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.municipio = paramTrayectoria.municipio;
      return;
    case 40:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombramiento = paramTrayectoria.nombramiento;
      return;
    case 41:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCorto = paramTrayectoria.nombreCorto;
      return;
    case 42:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCortoGrupo = paramTrayectoria.nombreCortoGrupo;
      return;
    case 43:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDependencia = paramTrayectoria.nombreDependencia;
      return;
    case 44:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombreLargoGrupo = paramTrayectoria.nombreLargoGrupo;
      return;
    case 45:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombreOrganismo = paramTrayectoria.nombreOrganismo;
      return;
    case 46:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombreRegion = paramTrayectoria.nombreRegion;
      return;
    case 47:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMovimiento = paramTrayectoria.numeroMovimiento;
      return;
    case 48:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.numeroRemesa = paramTrayectoria.numeroRemesa;
      return;
    case 49:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramTrayectoria.observaciones;
      return;
    case 50:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramTrayectoria.origen;
      return;
    case 51:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.otrosNoVicepladin = paramTrayectoria.otrosNoVicepladin;
      return;
    case 52:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.otrosPagos = paramTrayectoria.otrosPagos;
      return;
    case 53:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramTrayectoria.paso;
      return;
    case 54:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramTrayectoria.personal;
      return;
    case 55:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.porcJubilacion = paramTrayectoria.porcJubilacion;
      return;
    case 56:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.porcPensionInvalid = paramTrayectoria.porcPensionInvalid;
      return;
    case 57:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.porcPensionSobrev = paramTrayectoria.porcPensionSobrev;
      return;
    case 58:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.primaJerarquia = paramTrayectoria.primaJerarquia;
      return;
    case 59:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.primaServicio = paramTrayectoria.primaServicio;
      return;
    case 60:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.primasCargo = paramTrayectoria.primasCargo;
      return;
    case 61:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.primasTrabajador = paramTrayectoria.primasTrabajador;
      return;
    case 62:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramTrayectoria.primerApellido;
      return;
    case 63:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramTrayectoria.primerNombre;
      return;
    case 64:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.puntoCuenta = paramTrayectoria.puntoCuenta;
      return;
    case 65:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramTrayectoria.segundoApellido;
      return;
    case 66:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramTrayectoria.segundoNombre;
      return;
    case 67:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoBasico = paramTrayectoria.sueldoBasico;
      return;
    case 68:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoPromedio = paramTrayectoria.sueldoPromedio;
      return;
    case 69:
      if (paramTrayectoria == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramTrayectoria.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Trayectoria))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Trayectoria localTrayectoria = (Trayectoria)paramObject;
    if (localTrayectoria.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTrayectoria, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TrayectoriaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TrayectoriaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrayectoriaPK))
      throw new IllegalArgumentException("arg1");
    TrayectoriaPK localTrayectoriaPK = (TrayectoriaPK)paramObject;
    localTrayectoriaPK.idTrayectoria = this.idTrayectoria;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrayectoriaPK))
      throw new IllegalArgumentException("arg1");
    TrayectoriaPK localTrayectoriaPK = (TrayectoriaPK)paramObject;
    this.idTrayectoria = localTrayectoriaPK.idTrayectoria;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrayectoriaPK))
      throw new IllegalArgumentException("arg2");
    TrayectoriaPK localTrayectoriaPK = (TrayectoriaPK)paramObject;
    localTrayectoriaPK.idTrayectoria = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 33);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrayectoriaPK))
      throw new IllegalArgumentException("arg2");
    TrayectoriaPK localTrayectoriaPK = (TrayectoriaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 33, localTrayectoriaPK.idTrayectoria);
  }

  private static final double jdoGetajusteSueldo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.ajusteSueldo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.ajusteSueldo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 0))
      return paramTrayectoria.ajusteSueldo;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 0, paramTrayectoria.ajusteSueldo);
  }

  private static final void jdoSetajusteSueldo(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.ajusteSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.ajusteSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 0, paramTrayectoria.ajusteSueldo, paramDouble);
  }

  private static final int jdoGetanioPreparacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.anioPreparacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.anioPreparacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 1))
      return paramTrayectoria.anioPreparacion;
    return localStateManager.getIntField(paramTrayectoria, jdoInheritedFieldCount + 1, paramTrayectoria.anioPreparacion);
  }

  private static final void jdoSetanioPreparacion(Trayectoria paramTrayectoria, int paramInt)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.anioPreparacion = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.anioPreparacion = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoria, jdoInheritedFieldCount + 1, paramTrayectoria.anioPreparacion, paramInt);
  }

  private static final String jdoGetcaucion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.caucion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.caucion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 2))
      return paramTrayectoria.caucion;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 2, paramTrayectoria.caucion);
  }

  private static final void jdoSetcaucion(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.caucion = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.caucion = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 2, paramTrayectoria.caucion, paramString);
  }

  private static final int jdoGetcedula(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.cedula;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.cedula;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 3))
      return paramTrayectoria.cedula;
    return localStateManager.getIntField(paramTrayectoria, jdoInheritedFieldCount + 3, paramTrayectoria.cedula);
  }

  private static final void jdoSetcedula(Trayectoria paramTrayectoria, int paramInt)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoria, jdoInheritedFieldCount + 3, paramTrayectoria.cedula, paramInt);
  }

  private static final String jdoGetciudad(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.ciudad;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.ciudad;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 4))
      return paramTrayectoria.ciudad;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 4, paramTrayectoria.ciudad);
  }

  private static final void jdoSetciudad(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.ciudad = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.ciudad = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 4, paramTrayectoria.ciudad, paramString);
  }

  private static final String jdoGetcodCargo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codCargo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codCargo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 5))
      return paramTrayectoria.codCargo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 5, paramTrayectoria.codCargo);
  }

  private static final void jdoSetcodCargo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 5, paramTrayectoria.codCargo, paramString);
  }

  private static final String jdoGetcodCategoria(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codCategoria;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codCategoria;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 6))
      return paramTrayectoria.codCategoria;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 6, paramTrayectoria.codCategoria);
  }

  private static final void jdoSetcodCategoria(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codCategoria = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codCategoria = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 6, paramTrayectoria.codCategoria, paramString);
  }

  private static final String jdoGetcodCausaMovimiento(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codCausaMovimiento;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codCausaMovimiento;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 7))
      return paramTrayectoria.codCausaMovimiento;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 7, paramTrayectoria.codCausaMovimiento);
  }

  private static final void jdoSetcodCausaMovimiento(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codCausaMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codCausaMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 7, paramTrayectoria.codCausaMovimiento, paramString);
  }

  private static final String jdoGetcodConcurso(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codConcurso;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codConcurso;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 8))
      return paramTrayectoria.codConcurso;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 8, paramTrayectoria.codConcurso);
  }

  private static final void jdoSetcodConcurso(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codConcurso = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codConcurso = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 8, paramTrayectoria.codConcurso, paramString);
  }

  private static final String jdoGetcodDependencia(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codDependencia;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codDependencia;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 9))
      return paramTrayectoria.codDependencia;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 9, paramTrayectoria.codDependencia);
  }

  private static final void jdoSetcodDependencia(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 9, paramTrayectoria.codDependencia, paramString);
  }

  private static final String jdoGetcodGrupoOrganismo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codGrupoOrganismo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codGrupoOrganismo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 10))
      return paramTrayectoria.codGrupoOrganismo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 10, paramTrayectoria.codGrupoOrganismo);
  }

  private static final void jdoSetcodGrupoOrganismo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codGrupoOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codGrupoOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 10, paramTrayectoria.codGrupoOrganismo, paramString);
  }

  private static final String jdoGetcodManualCargo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codManualCargo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codManualCargo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 11))
      return paramTrayectoria.codManualCargo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 11, paramTrayectoria.codManualCargo);
  }

  private static final void jdoSetcodManualCargo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codManualCargo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codManualCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 11, paramTrayectoria.codManualCargo, paramString);
  }

  private static final String jdoGetcodOrganismo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codOrganismo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codOrganismo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 12))
      return paramTrayectoria.codOrganismo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 12, paramTrayectoria.codOrganismo);
  }

  private static final void jdoSetcodOrganismo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 12, paramTrayectoria.codOrganismo, paramString);
  }

  private static final String jdoGetcodRegion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codRegion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codRegion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 13))
      return paramTrayectoria.codRegion;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 13, paramTrayectoria.codRegion);
  }

  private static final void jdoSetcodRegion(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codRegion = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 13, paramTrayectoria.codRegion, paramString);
  }

  private static final String jdoGetcodRelacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codRelacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codRelacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 14))
      return paramTrayectoria.codRelacion;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 14, paramTrayectoria.codRelacion);
  }

  private static final void jdoSetcodRelacion(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codRelacion = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codRelacion = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 14, paramTrayectoria.codRelacion, paramString);
  }

  private static final String jdoGetcodUbiGeografico(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codUbiGeografico;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codUbiGeografico;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 15))
      return paramTrayectoria.codUbiGeografico;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 15, paramTrayectoria.codUbiGeografico);
  }

  private static final void jdoSetcodUbiGeografico(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codUbiGeografico = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codUbiGeografico = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 15, paramTrayectoria.codUbiGeografico, paramString);
  }

  private static final String jdoGetcodigoAnteriorMpd(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codigoAnteriorMpd;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codigoAnteriorMpd;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 16))
      return paramTrayectoria.codigoAnteriorMpd;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 16, paramTrayectoria.codigoAnteriorMpd);
  }

  private static final void jdoSetcodigoAnteriorMpd(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codigoAnteriorMpd = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codigoAnteriorMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 16, paramTrayectoria.codigoAnteriorMpd, paramString);
  }

  private static final int jdoGetcodigoNomina(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.codigoNomina;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.codigoNomina;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 17))
      return paramTrayectoria.codigoNomina;
    return localStateManager.getIntField(paramTrayectoria, jdoInheritedFieldCount + 17, paramTrayectoria.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(Trayectoria paramTrayectoria, int paramInt)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoria, jdoInheritedFieldCount + 17, paramTrayectoria.codigoNomina, paramInt);
  }

  private static final double jdoGetcompensacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.compensacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.compensacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 18))
      return paramTrayectoria.compensacion;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 18, paramTrayectoria.compensacion);
  }

  private static final void jdoSetcompensacion(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.compensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.compensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 18, paramTrayectoria.compensacion, paramDouble);
  }

  private static final int jdoGetcorrelativoMpd(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.correlativoMpd;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.correlativoMpd;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 19))
      return paramTrayectoria.correlativoMpd;
    return localStateManager.getIntField(paramTrayectoria, jdoInheritedFieldCount + 19, paramTrayectoria.correlativoMpd);
  }

  private static final void jdoSetcorrelativoMpd(Trayectoria paramTrayectoria, int paramInt)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.correlativoMpd = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.correlativoMpd = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoria, jdoInheritedFieldCount + 19, paramTrayectoria.correlativoMpd, paramInt);
  }

  private static final String jdoGetdescCategoria(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.descCategoria;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.descCategoria;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 20))
      return paramTrayectoria.descCategoria;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 20, paramTrayectoria.descCategoria);
  }

  private static final void jdoSetdescCategoria(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.descCategoria = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.descCategoria = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 20, paramTrayectoria.descCategoria, paramString);
  }

  private static final String jdoGetdescRelacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.descRelacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.descRelacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 21))
      return paramTrayectoria.descRelacion;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 21, paramTrayectoria.descRelacion);
  }

  private static final void jdoSetdescRelacion(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.descRelacion = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.descRelacion = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 21, paramTrayectoria.descRelacion, paramString);
  }

  private static final String jdoGetdescripcionCargo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.descripcionCargo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.descripcionCargo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 22))
      return paramTrayectoria.descripcionCargo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 22, paramTrayectoria.descripcionCargo);
  }

  private static final void jdoSetdescripcionCargo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.descripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.descripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 22, paramTrayectoria.descripcionCargo, paramString);
  }

  private static final String jdoGetdescripcionMovimiento(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.descripcionMovimiento;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.descripcionMovimiento;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 23))
      return paramTrayectoria.descripcionMovimiento;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 23, paramTrayectoria.descripcionMovimiento);
  }

  private static final void jdoSetdescripcionMovimiento(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.descripcionMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.descripcionMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 23, paramTrayectoria.descripcionMovimiento, paramString);
  }

  private static final String jdoGetestado(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.estado;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.estado;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 24))
      return paramTrayectoria.estado;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 24, paramTrayectoria.estado);
  }

  private static final void jdoSetestado(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.estado = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.estado = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 24, paramTrayectoria.estado, paramString);
  }

  private static final String jdoGetestatus(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.estatus;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.estatus;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 25))
      return paramTrayectoria.estatus;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 25, paramTrayectoria.estatus);
  }

  private static final void jdoSetestatus(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 25, paramTrayectoria.estatus, paramString);
  }

  private static final Date jdoGetfechaCulminacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.fechaCulminacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.fechaCulminacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 26))
      return paramTrayectoria.fechaCulminacion;
    return (Date)localStateManager.getObjectField(paramTrayectoria, jdoInheritedFieldCount + 26, paramTrayectoria.fechaCulminacion);
  }

  private static final void jdoSetfechaCulminacion(Trayectoria paramTrayectoria, Date paramDate)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.fechaCulminacion = paramDate;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.fechaCulminacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrayectoria, jdoInheritedFieldCount + 26, paramTrayectoria.fechaCulminacion, paramDate);
  }

  private static final Date jdoGetfechaEstatus(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.fechaEstatus;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.fechaEstatus;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 27))
      return paramTrayectoria.fechaEstatus;
    return (Date)localStateManager.getObjectField(paramTrayectoria, jdoInheritedFieldCount + 27, paramTrayectoria.fechaEstatus);
  }

  private static final void jdoSetfechaEstatus(Trayectoria paramTrayectoria, Date paramDate)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.fechaEstatus = paramDate;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.fechaEstatus = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrayectoria, jdoInheritedFieldCount + 27, paramTrayectoria.fechaEstatus, paramDate);
  }

  private static final Date jdoGetfechaPreparacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.fechaPreparacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.fechaPreparacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 28))
      return paramTrayectoria.fechaPreparacion;
    return (Date)localStateManager.getObjectField(paramTrayectoria, jdoInheritedFieldCount + 28, paramTrayectoria.fechaPreparacion);
  }

  private static final void jdoSetfechaPreparacion(Trayectoria paramTrayectoria, Date paramDate)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.fechaPreparacion = paramDate;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.fechaPreparacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrayectoria, jdoInheritedFieldCount + 28, paramTrayectoria.fechaPreparacion, paramDate);
  }

  private static final Date jdoGetfechaPuntoCuenta(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.fechaPuntoCuenta;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.fechaPuntoCuenta;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 29))
      return paramTrayectoria.fechaPuntoCuenta;
    return (Date)localStateManager.getObjectField(paramTrayectoria, jdoInheritedFieldCount + 29, paramTrayectoria.fechaPuntoCuenta);
  }

  private static final void jdoSetfechaPuntoCuenta(Trayectoria paramTrayectoria, Date paramDate)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.fechaPuntoCuenta = paramDate;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.fechaPuntoCuenta = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrayectoria, jdoInheritedFieldCount + 29, paramTrayectoria.fechaPuntoCuenta, paramDate);
  }

  private static final Date jdoGetfechaVigencia(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.fechaVigencia;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.fechaVigencia;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 30))
      return paramTrayectoria.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramTrayectoria, jdoInheritedFieldCount + 30, paramTrayectoria.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(Trayectoria paramTrayectoria, Date paramDate)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTrayectoria, jdoInheritedFieldCount + 30, paramTrayectoria.fechaVigencia, paramDate);
  }

  private static final int jdoGetgrado(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.grado;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.grado;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 31))
      return paramTrayectoria.grado;
    return localStateManager.getIntField(paramTrayectoria, jdoInheritedFieldCount + 31, paramTrayectoria.grado);
  }

  private static final void jdoSetgrado(Trayectoria paramTrayectoria, int paramInt)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoria, jdoInheritedFieldCount + 31, paramTrayectoria.grado, paramInt);
  }

  private static final double jdoGethoras(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.horas;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.horas;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 32))
      return paramTrayectoria.horas;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 32, paramTrayectoria.horas);
  }

  private static final void jdoSethoras(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.horas = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.horas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 32, paramTrayectoria.horas, paramDouble);
  }

  private static final long jdoGetidTrayectoria(Trayectoria paramTrayectoria)
  {
    return paramTrayectoria.idTrayectoria;
  }

  private static final void jdoSetidTrayectoria(Trayectoria paramTrayectoria, long paramLong)
  {
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.idTrayectoria = paramLong;
      return;
    }
    localStateManager.setLongField(paramTrayectoria, jdoInheritedFieldCount + 33, paramTrayectoria.idTrayectoria, paramLong);
  }

  private static final double jdoGetinvalidezSact(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.invalidezSact;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.invalidezSact;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 34))
      return paramTrayectoria.invalidezSact;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 34, paramTrayectoria.invalidezSact);
  }

  private static final void jdoSetinvalidezSact(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.invalidezSact = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.invalidezSact = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 34, paramTrayectoria.invalidezSact, paramDouble);
  }

  private static final double jdoGetmontoJubilacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.montoJubilacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.montoJubilacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 35))
      return paramTrayectoria.montoJubilacion;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 35, paramTrayectoria.montoJubilacion);
  }

  private static final void jdoSetmontoJubilacion(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.montoJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.montoJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 35, paramTrayectoria.montoJubilacion, paramDouble);
  }

  private static final double jdoGetmontoJubilacionSobrev(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.montoJubilacionSobrev;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.montoJubilacionSobrev;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 36))
      return paramTrayectoria.montoJubilacionSobrev;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 36, paramTrayectoria.montoJubilacionSobrev);
  }

  private static final void jdoSetmontoJubilacionSobrev(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.montoJubilacionSobrev = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.montoJubilacionSobrev = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 36, paramTrayectoria.montoJubilacionSobrev, paramDouble);
  }

  private static final double jdoGetmontoPensionInvalid(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.montoPensionInvalid;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.montoPensionInvalid;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 37))
      return paramTrayectoria.montoPensionInvalid;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 37, paramTrayectoria.montoPensionInvalid);
  }

  private static final void jdoSetmontoPensionInvalid(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.montoPensionInvalid = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.montoPensionInvalid = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 37, paramTrayectoria.montoPensionInvalid, paramDouble);
  }

  private static final double jdoGetmontoPensionSobrev(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.montoPensionSobrev;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.montoPensionSobrev;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 38))
      return paramTrayectoria.montoPensionSobrev;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 38, paramTrayectoria.montoPensionSobrev);
  }

  private static final void jdoSetmontoPensionSobrev(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.montoPensionSobrev = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.montoPensionSobrev = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 38, paramTrayectoria.montoPensionSobrev, paramDouble);
  }

  private static final String jdoGetmunicipio(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.municipio;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.municipio;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 39))
      return paramTrayectoria.municipio;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 39, paramTrayectoria.municipio);
  }

  private static final void jdoSetmunicipio(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.municipio = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.municipio = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 39, paramTrayectoria.municipio, paramString);
  }

  private static final String jdoGetnombramiento(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.nombramiento;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.nombramiento;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 40))
      return paramTrayectoria.nombramiento;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 40, paramTrayectoria.nombramiento);
  }

  private static final void jdoSetnombramiento(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.nombramiento = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.nombramiento = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 40, paramTrayectoria.nombramiento, paramString);
  }

  private static final String jdoGetnombreCorto(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.nombreCorto;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.nombreCorto;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 41))
      return paramTrayectoria.nombreCorto;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 41, paramTrayectoria.nombreCorto);
  }

  private static final void jdoSetnombreCorto(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.nombreCorto = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.nombreCorto = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 41, paramTrayectoria.nombreCorto, paramString);
  }

  private static final String jdoGetnombreCortoGrupo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.nombreCortoGrupo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.nombreCortoGrupo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 42))
      return paramTrayectoria.nombreCortoGrupo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 42, paramTrayectoria.nombreCortoGrupo);
  }

  private static final void jdoSetnombreCortoGrupo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.nombreCortoGrupo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.nombreCortoGrupo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 42, paramTrayectoria.nombreCortoGrupo, paramString);
  }

  private static final String jdoGetnombreDependencia(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.nombreDependencia;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.nombreDependencia;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 43))
      return paramTrayectoria.nombreDependencia;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 43, paramTrayectoria.nombreDependencia);
  }

  private static final void jdoSetnombreDependencia(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.nombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.nombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 43, paramTrayectoria.nombreDependencia, paramString);
  }

  private static final String jdoGetnombreLargoGrupo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.nombreLargoGrupo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.nombreLargoGrupo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 44))
      return paramTrayectoria.nombreLargoGrupo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 44, paramTrayectoria.nombreLargoGrupo);
  }

  private static final void jdoSetnombreLargoGrupo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.nombreLargoGrupo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.nombreLargoGrupo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 44, paramTrayectoria.nombreLargoGrupo, paramString);
  }

  private static final String jdoGetnombreOrganismo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.nombreOrganismo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.nombreOrganismo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 45))
      return paramTrayectoria.nombreOrganismo;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 45, paramTrayectoria.nombreOrganismo);
  }

  private static final void jdoSetnombreOrganismo(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.nombreOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.nombreOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 45, paramTrayectoria.nombreOrganismo, paramString);
  }

  private static final String jdoGetnombreRegion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.nombreRegion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.nombreRegion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 46))
      return paramTrayectoria.nombreRegion;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 46, paramTrayectoria.nombreRegion);
  }

  private static final void jdoSetnombreRegion(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.nombreRegion = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.nombreRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 46, paramTrayectoria.nombreRegion, paramString);
  }

  private static final int jdoGetnumeroMovimiento(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.numeroMovimiento;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.numeroMovimiento;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 47))
      return paramTrayectoria.numeroMovimiento;
    return localStateManager.getIntField(paramTrayectoria, jdoInheritedFieldCount + 47, paramTrayectoria.numeroMovimiento);
  }

  private static final void jdoSetnumeroMovimiento(Trayectoria paramTrayectoria, int paramInt)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.numeroMovimiento = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.numeroMovimiento = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoria, jdoInheritedFieldCount + 47, paramTrayectoria.numeroMovimiento, paramInt);
  }

  private static final String jdoGetnumeroRemesa(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.numeroRemesa;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.numeroRemesa;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 48))
      return paramTrayectoria.numeroRemesa;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 48, paramTrayectoria.numeroRemesa);
  }

  private static final void jdoSetnumeroRemesa(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.numeroRemesa = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.numeroRemesa = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 48, paramTrayectoria.numeroRemesa, paramString);
  }

  private static final String jdoGetobservaciones(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.observaciones;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.observaciones;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 49))
      return paramTrayectoria.observaciones;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 49, paramTrayectoria.observaciones);
  }

  private static final void jdoSetobservaciones(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 49, paramTrayectoria.observaciones, paramString);
  }

  private static final String jdoGetorigen(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.origen;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.origen;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 50))
      return paramTrayectoria.origen;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 50, paramTrayectoria.origen);
  }

  private static final void jdoSetorigen(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.origen = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 50, paramTrayectoria.origen, paramString);
  }

  private static final double jdoGetotrosNoVicepladin(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.otrosNoVicepladin;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.otrosNoVicepladin;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 51))
      return paramTrayectoria.otrosNoVicepladin;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 51, paramTrayectoria.otrosNoVicepladin);
  }

  private static final void jdoSetotrosNoVicepladin(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.otrosNoVicepladin = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.otrosNoVicepladin = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 51, paramTrayectoria.otrosNoVicepladin, paramDouble);
  }

  private static final double jdoGetotrosPagos(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.otrosPagos;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.otrosPagos;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 52))
      return paramTrayectoria.otrosPagos;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 52, paramTrayectoria.otrosPagos);
  }

  private static final void jdoSetotrosPagos(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.otrosPagos = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.otrosPagos = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 52, paramTrayectoria.otrosPagos, paramDouble);
  }

  private static final int jdoGetpaso(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.paso;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.paso;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 53))
      return paramTrayectoria.paso;
    return localStateManager.getIntField(paramTrayectoria, jdoInheritedFieldCount + 53, paramTrayectoria.paso);
  }

  private static final void jdoSetpaso(Trayectoria paramTrayectoria, int paramInt)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrayectoria, jdoInheritedFieldCount + 53, paramTrayectoria.paso, paramInt);
  }

  private static final Personal jdoGetpersonal(Trayectoria paramTrayectoria)
  {
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.personal;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 54))
      return paramTrayectoria.personal;
    return (Personal)localStateManager.getObjectField(paramTrayectoria, jdoInheritedFieldCount + 54, paramTrayectoria.personal);
  }

  private static final void jdoSetpersonal(Trayectoria paramTrayectoria, Personal paramPersonal)
  {
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramTrayectoria, jdoInheritedFieldCount + 54, paramTrayectoria.personal, paramPersonal);
  }

  private static final double jdoGetporcJubilacion(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.porcJubilacion;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.porcJubilacion;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 55))
      return paramTrayectoria.porcJubilacion;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 55, paramTrayectoria.porcJubilacion);
  }

  private static final void jdoSetporcJubilacion(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.porcJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.porcJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 55, paramTrayectoria.porcJubilacion, paramDouble);
  }

  private static final double jdoGetporcPensionInvalid(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.porcPensionInvalid;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.porcPensionInvalid;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 56))
      return paramTrayectoria.porcPensionInvalid;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 56, paramTrayectoria.porcPensionInvalid);
  }

  private static final void jdoSetporcPensionInvalid(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.porcPensionInvalid = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.porcPensionInvalid = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 56, paramTrayectoria.porcPensionInvalid, paramDouble);
  }

  private static final double jdoGetporcPensionSobrev(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.porcPensionSobrev;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.porcPensionSobrev;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 57))
      return paramTrayectoria.porcPensionSobrev;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 57, paramTrayectoria.porcPensionSobrev);
  }

  private static final void jdoSetporcPensionSobrev(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.porcPensionSobrev = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.porcPensionSobrev = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 57, paramTrayectoria.porcPensionSobrev, paramDouble);
  }

  private static final double jdoGetprimaJerarquia(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.primaJerarquia;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.primaJerarquia;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 58))
      return paramTrayectoria.primaJerarquia;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 58, paramTrayectoria.primaJerarquia);
  }

  private static final void jdoSetprimaJerarquia(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.primaJerarquia = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.primaJerarquia = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 58, paramTrayectoria.primaJerarquia, paramDouble);
  }

  private static final double jdoGetprimaServicio(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.primaServicio;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.primaServicio;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 59))
      return paramTrayectoria.primaServicio;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 59, paramTrayectoria.primaServicio);
  }

  private static final void jdoSetprimaServicio(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.primaServicio = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.primaServicio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 59, paramTrayectoria.primaServicio, paramDouble);
  }

  private static final double jdoGetprimasCargo(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.primasCargo;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.primasCargo;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 60))
      return paramTrayectoria.primasCargo;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 60, paramTrayectoria.primasCargo);
  }

  private static final void jdoSetprimasCargo(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.primasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.primasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 60, paramTrayectoria.primasCargo, paramDouble);
  }

  private static final double jdoGetprimasTrabajador(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.primasTrabajador;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.primasTrabajador;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 61))
      return paramTrayectoria.primasTrabajador;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 61, paramTrayectoria.primasTrabajador);
  }

  private static final void jdoSetprimasTrabajador(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.primasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.primasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 61, paramTrayectoria.primasTrabajador, paramDouble);
  }

  private static final String jdoGetprimerApellido(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.primerApellido;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.primerApellido;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 62))
      return paramTrayectoria.primerApellido;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 62, paramTrayectoria.primerApellido);
  }

  private static final void jdoSetprimerApellido(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 62, paramTrayectoria.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.primerNombre;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.primerNombre;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 63))
      return paramTrayectoria.primerNombre;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 63, paramTrayectoria.primerNombre);
  }

  private static final void jdoSetprimerNombre(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 63, paramTrayectoria.primerNombre, paramString);
  }

  private static final String jdoGetpuntoCuenta(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.puntoCuenta;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.puntoCuenta;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 64))
      return paramTrayectoria.puntoCuenta;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 64, paramTrayectoria.puntoCuenta);
  }

  private static final void jdoSetpuntoCuenta(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.puntoCuenta = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.puntoCuenta = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 64, paramTrayectoria.puntoCuenta, paramString);
  }

  private static final String jdoGetsegundoApellido(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.segundoApellido;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.segundoApellido;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 65))
      return paramTrayectoria.segundoApellido;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 65, paramTrayectoria.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 65, paramTrayectoria.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.segundoNombre;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.segundoNombre;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 66))
      return paramTrayectoria.segundoNombre;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 66, paramTrayectoria.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 66, paramTrayectoria.segundoNombre, paramString);
  }

  private static final double jdoGetsueldoBasico(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.sueldoBasico;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.sueldoBasico;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 67))
      return paramTrayectoria.sueldoBasico;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 67, paramTrayectoria.sueldoBasico);
  }

  private static final void jdoSetsueldoBasico(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.sueldoBasico = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.sueldoBasico = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 67, paramTrayectoria.sueldoBasico, paramDouble);
  }

  private static final double jdoGetsueldoPromedio(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.sueldoPromedio;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.sueldoPromedio;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 68))
      return paramTrayectoria.sueldoPromedio;
    return localStateManager.getDoubleField(paramTrayectoria, jdoInheritedFieldCount + 68, paramTrayectoria.sueldoPromedio);
  }

  private static final void jdoSetsueldoPromedio(Trayectoria paramTrayectoria, double paramDouble)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.sueldoPromedio = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.sueldoPromedio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrayectoria, jdoInheritedFieldCount + 68, paramTrayectoria.sueldoPromedio, paramDouble);
  }

  private static final String jdoGetusuario(Trayectoria paramTrayectoria)
  {
    if (paramTrayectoria.jdoFlags <= 0)
      return paramTrayectoria.usuario;
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
      return paramTrayectoria.usuario;
    if (localStateManager.isLoaded(paramTrayectoria, jdoInheritedFieldCount + 69))
      return paramTrayectoria.usuario;
    return localStateManager.getStringField(paramTrayectoria, jdoInheritedFieldCount + 69, paramTrayectoria.usuario);
  }

  private static final void jdoSetusuario(Trayectoria paramTrayectoria, String paramString)
  {
    if (paramTrayectoria.jdoFlags == 0)
    {
      paramTrayectoria.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramTrayectoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrayectoria.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramTrayectoria, jdoInheritedFieldCount + 69, paramTrayectoria.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}