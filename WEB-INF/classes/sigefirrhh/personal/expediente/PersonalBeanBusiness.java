package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.bienestar.EstablecimientoSaludBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.PaisBeanBusiness;
import sigefirrhh.base.ubicacion.Parroquia;
import sigefirrhh.base.ubicacion.ParroquiaBeanBusiness;

public class PersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(PersonalBeanBusiness.class.getName());

  public void addPersonal(Personal personal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Personal personalNew = 
      (Personal)BeanUtils.cloneBean(
      personal);

    CiudadBeanBusiness ciudadNacimientoBeanBusiness = new CiudadBeanBusiness();

    if (personalNew.getCiudadNacimiento() != null) {
      personalNew.setCiudadNacimiento(
        ciudadNacimientoBeanBusiness.findCiudadById(
        personalNew.getCiudadNacimiento().getIdCiudad()));
    }

    PaisBeanBusiness paisNacionalidadBeanBusiness = new PaisBeanBusiness();

    if (personalNew.getPaisNacionalidad() != null) {
      personalNew.setPaisNacionalidad(
        paisNacionalidadBeanBusiness.findPaisById(
        personalNew.getPaisNacionalidad().getIdPais()));
    }

    CiudadBeanBusiness ciudadResidenciaBeanBusiness = new CiudadBeanBusiness();

    if (personalNew.getCiudadResidencia() != null) {
      personalNew.setCiudadResidencia(
        ciudadResidenciaBeanBusiness.findCiudadById(
        personalNew.getCiudadResidencia().getIdCiudad()));
    }

    ParroquiaBeanBusiness parroquiaBeanBusiness = new ParroquiaBeanBusiness();

    if (personalNew.getParroquia() != null) {
      personalNew.setParroquia(
        parroquiaBeanBusiness.findParroquiaById(
        personalNew.getParroquia().getIdParroquia()));
    }

    EstablecimientoSaludBeanBusiness establecimientoSaludBeanBusiness = new EstablecimientoSaludBeanBusiness();

    if (personalNew.getEstablecimientoSalud() != null) {
      personalNew.setEstablecimientoSalud(
        establecimientoSaludBeanBusiness.findEstablecimientoSaludById(
        personalNew.getEstablecimientoSalud().getIdEstablecimientoSalud()));
    }
    if (personalNew.getSegundoApellido() == null) {
      personalNew.setSegundoApellido("");
    }
    if (personalNew.getSegundoNombre() == null) {
      personalNew.setSegundoNombre("");
    }
    pm.makePersistent(personalNew);

    PersonalOrganismo personalOrganismo = new PersonalOrganismo();

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();
    Organismo organismonew = organismoBeanBusiness.findOrganismoById(idOrganismo);

    personalOrganismo.setPersonal(personalNew);
    personalOrganismo.setOrganismo(organismonew);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    personalOrganismo.setIdPersonalOrganismo(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.PersonalOrganismo"));

    pm.makePersistent(personalOrganismo);
  }

  public void updatePersonal(Personal personal) throws Exception
  {
    Personal personalModify = 
      findPersonalById(personal.getIdPersonal());

    CiudadBeanBusiness ciudadNacimientoBeanBusiness = new CiudadBeanBusiness();

    if (personal.getCiudadNacimiento() != null) {
      personal.setCiudadNacimiento(
        ciudadNacimientoBeanBusiness.findCiudadById(
        personal.getCiudadNacimiento().getIdCiudad()));
    }

    PaisBeanBusiness paisNacionalidadBeanBusiness = new PaisBeanBusiness();

    if (personal.getPaisNacionalidad() != null) {
      personal.setPaisNacionalidad(
        paisNacionalidadBeanBusiness.findPaisById(
        personal.getPaisNacionalidad().getIdPais()));
    }

    CiudadBeanBusiness ciudadResidenciaBeanBusiness = new CiudadBeanBusiness();

    if (personal.getCiudadResidencia() != null) {
      personal.setCiudadResidencia(
        ciudadResidenciaBeanBusiness.findCiudadById(
        personal.getCiudadResidencia().getIdCiudad()));
    }

    ParroquiaBeanBusiness parroquiaBeanBusiness = new ParroquiaBeanBusiness();

    if (personal.getParroquia() != null) {
      personal.setParroquia(
        parroquiaBeanBusiness.findParroquiaById(
        personal.getParroquia().getIdParroquia()));
    }

    EstablecimientoSaludBeanBusiness establecimientoSaludBeanBusiness = new EstablecimientoSaludBeanBusiness();

    if (personal.getEstablecimientoSalud() != null) {
      personal.setEstablecimientoSalud(
        establecimientoSaludBeanBusiness.findEstablecimientoSaludById(
        personal.getEstablecimientoSalud().getIdEstablecimientoSalud()));
    }

    BeanUtils.copyProperties(personalModify, personal);
  }

  public void deletePersonal(Personal personal, long idOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Personal personalDelete = 
      findPersonalById(personal.getIdPersonal());
    PersonalOrganismoNoGenBeanBusiness personalOrganismoNoGenBeanBusiness = new PersonalOrganismoNoGenBeanBusiness();
    PersonalOrganismo personalOrganismo = personalOrganismoNoGenBeanBusiness.findPersonalOrganismoByPersonalAndOrganismo(personal.getIdPersonal(), idOrganismo);
    pm.deletePersistent(personalOrganismo);
    pm.deletePersistent(personalDelete);
  }

  public Personal findPersonalById(long idPersonal)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPersonal == pIdPersonal";
    Query query = pm.newQuery(Personal.class, filter);

    query.declareParameters("long pIdPersonal");

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPersonal.iterator();
    return (Personal)iterator.next();
  }

  public Collection findPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent personalExtent = pm.getExtent(
      Personal.class, true);
    Query query = pm.newQuery(personalExtent);
    query.setOrdering("primerApellido ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCedula(int cedula)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cedula == pCedula";

    Query query = pm.newQuery(Personal.class, filter);

    query.declareParameters("int pCedula");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));

    query.setOrdering("primerApellido ascending");

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findByPrimerApellido(String primerApellido)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primerApellido.startsWith(pPrimerApellido)";

    Query query = pm.newQuery(Personal.class, filter);

    query.declareParameters("java.lang.String pPrimerApellido");
    HashMap parameters = new HashMap();

    parameters.put("pPrimerApellido", new String(primerApellido));

    query.setOrdering("primerApellido ascending");

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findByPrimerNombre(String primerNombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primerNombre.startsWith(pPrimerNombre)";

    Query query = pm.newQuery(Personal.class, filter);

    query.declareParameters("java.lang.String pPrimerNombre");
    HashMap parameters = new HashMap();

    parameters.put("pPrimerNombre", new String(primerNombre));

    query.setOrdering("primerApellido ascending");

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    StringBuffer filter = new StringBuffer();
    if (primerNombre != null) {
      filter.append("primerNombre.startsWith(pPrimerNombre) && ");
    }
    if (segundoNombre != null) {
      filter.append("segundoNombre.startsWith(pSegundoNombre) && ");
    }
    if (primerApellido != null) {
      filter.append("primerApellido.startsWith(pPrimerApellido) && ");
    }
    if (segundoApellido != null) {
      filter.append("segundoApellido.startsWith(pSegundoApellido) && ");
    }
    filter.append("po.personal.idPersonal == idPersonal && po.organismo.idOrganismo == pIdOrganismo");

    Query query = pm.newQuery(Personal.class, filter.toString());

    StringBuffer declare = new StringBuffer();

    if (primerNombre != null) {
      declare.append("String pPrimerNombre, ");
    }
    if (segundoNombre != null) {
      declare.append("String pSegundoNombre, ");
    }
    if (primerApellido != null) {
      declare.append("String pPrimerApellido, ");
    }
    if (segundoApellido != null) {
      declare.append("String pSegundoApellido, ");
    }
    declare.append("long pIdOrganismo");

    query.declareImports("import sigefirrhh.personal.expediente.PersonalOrganismo");
    query.declareVariables("PersonalOrganismo po");

    query.declareParameters(declare.toString());
    query.setOrdering("primerApellido ascending, primerNombre ascending");
    HashMap parameters = new HashMap();

    if (primerNombre != null) {
      parameters.put("pPrimerNombre", new String(primerNombre));
    }
    if (segundoNombre != null) {
      parameters.put("pSegundoNombre", new String(segundoNombre));
    }
    if (primerApellido != null) {
      parameters.put("pPrimerApellido", new String(primerApellido));
    }
    if (segundoApellido != null) {
      parameters.put("pSegundoApellido", new String(segundoApellido));
    }
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findByCedula(int cedula, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "cedula == pCedula && po.personal.idPersonal == idPersonal && po.organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Personal.class, filter);
    query.declareImports("import sigefirrhh.personal.expediente.PersonalOrganismo");
    query.declareVariables("PersonalOrganismo po");

    query.declareParameters("int pCedula, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("primerApellido ascending");

    Collection colPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPersonal);

    return colPersonal;
  }

  public Collection findBySeguriodadUnidadFuncional(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo, long idUsuario, String administrador) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    int numero = 0;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      if (administrador.equals("N")) {
        numero = 1;
        sql.append("select distinct p.id_personal, (p.primer_apellido  || ' ' || p.primer_nombre)  as nombre  ");
        sql.append(" from personal p, trabajador t, usuariounidadfuncional uf, dependencia d");
        sql.append(" where uf.id_usuario = ?");
        sql.append(" and p.id_personal = t.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_unidad_funcional = uf.id_unidad_funcional");
        sql.append(" and t.estatus = 'A'");
        if ((primerApellido != null) && (!primerApellido.equals(""))) {
          sql.append(" and p.primer_apellido like ? ");
        }
        if ((segundoApellido != null) && (!segundoApellido.equals(""))) {
          sql.append(" and p.segundo_apellido like ? ");
        }
        if ((primerNombre != null) && (!primerNombre.equals(""))) {
          sql.append(" and p.primer_nombre like ? ");
        }
        if ((segundoNombre != null) && (!segundoNombre.equals(""))) {
          sql.append(" and p.segundo_nombre like ? ");
        }
        sql.append(" and t.id_organismo = ? ");
        sql.append(" order by p.primer_apellido, p.primer_nombre");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        if ((primerApellido != null) && (!primerApellido.equals(""))) {
          numero++;
          stRegistros.setString(numero, primerApellido);
        }
        if ((segundoApellido != null) && (!segundoApellido.equals(""))) {
          numero++;
          stRegistros.setString(numero, segundoApellido);
        }
        if ((primerNombre != null) && (!primerNombre.equals(""))) {
          numero++;
          stRegistros.setString(numero, primerNombre);
        }
        if ((segundoNombre != null) && (!segundoNombre.equals(""))) {
          numero++;
          stRegistros.setString(numero, segundoNombre);
        }
        numero++;
        stRegistros.setLong(numero, idOrganismo);
      }
      else {
        sql.append("select p.id_personal, (p.primer_apellido  || ' ' || p.primer_nombre)  as nombre  ");
        sql.append(" from personal p, trabajador t");
        sql.append(" where p.id_personal = t.id_personal");
        sql.append(" and t.estatus = 'A'");
        if ((primerApellido != null) && (!primerApellido.equals(""))) {
          sql.append(" and p.primer_apellido like ? ");
        }
        if ((segundoApellido != null) && (!segundoApellido.equals(""))) {
          sql.append(" and p.segundo_apellido like ? ");
        }
        if ((primerNombre != null) && (!primerNombre.equals(""))) {
          sql.append(" and p.primer_nombre like ? ");
        }
        if ((segundoNombre != null) && (!segundoNombre.equals(""))) {
          sql.append(" and p.segundo_nombre like ? ");
        }
        sql.append(" and t.id_organismo = ? ");
        sql.append(" order by p.primer_apellido, p.primer_nombre");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        if ((primerApellido != null) && (!primerApellido.equals(""))) {
          numero++;
          stRegistros.setString(numero, primerApellido);
        }
        if ((segundoApellido != null) && (!segundoApellido.equals(""))) {
          numero++;
          stRegistros.setString(numero, segundoApellido);
        }
        if ((primerNombre != null) && (!primerNombre.equals(""))) {
          numero++;
          stRegistros.setString(numero, primerNombre);
        }
        if ((segundoNombre != null) && (!segundoNombre.equals(""))) {
          numero++;
          stRegistros.setString(numero, segundoNombre);
        }
        numero++;
        stRegistros.setLong(numero, idOrganismo);
      }

      this.log.error("sql......." + sql.toString());
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        PersonalAux personalAux = new PersonalAux();
        personalAux.setId(rsRegistros.getLong("id_personal"));
        personalAux.setNombre(rsRegistros.getString("nombre"));
        col.add(personalAux);
      }
      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public Collection findBySeguriodadUnidadFuncional(int cedula, long idOrganismo, long idUsuario, String administrador) throws Exception { Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select distinct p.id_personal, (p.primer_apellido  || ' ' || p.primer_nombre)  as nombre  ");
        sql.append(" from personal p, trabajador t, usuariounidadfuncional uf, dependencia d");
        sql.append(" where uf.id_usuario = ?");
        sql.append(" and p.id_personal = t.id_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_unidad_funcional = uf.id_unidad_funcional");
        sql.append(" and t.estatus = 'A'");
        sql.append(" and p.cedula = ? ");
        sql.append(" and t.id_organismo = ? ");
        sql.append(" order by p.primer_apellido, p.primer_nombre");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setInt(2, cedula);
        stRegistros.setLong(3, idOrganismo);
      }
      else {
        sql.append("select p.id_personal, (p.primer_apellido  || ' ' || p.primer_nombre)  as nombre  ");
        sql.append(" from personal p, trabajador t");
        sql.append(" where p.id_personal = t.id_personal");
        sql.append(" and t.estatus = 'A'");
        sql.append(" and p.cedula = ? ");
        sql.append(" and t.id_organismo = ? ");
        sql.append(" order by p.primer_apellido, p.primer_nombre");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setInt(1, cedula);
        stRegistros.setLong(2, idOrganismo);
      }

      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        PersonalAux personalAux = new PersonalAux();
        personalAux.setId(rsRegistros.getLong("id_personal"));
        personalAux.setNombre(rsRegistros.getString("nombre"));
        col.add(personalAux);
      }
      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}