package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ComisionServicioPK
  implements Serializable
{
  public long idComisionServicio;

  public ComisionServicioPK()
  {
  }

  public ComisionServicioPK(long idComisionServicio)
  {
    this.idComisionServicio = idComisionServicio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ComisionServicioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ComisionServicioPK thatPK)
  {
    return 
      this.idComisionServicio == thatPK.idComisionServicio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idComisionServicio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idComisionServicio);
  }
}