package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.adiestramiento.AdiestramientoFacade;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class EstudioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EstudioForm.class.getName());
  private Estudio estudio;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colTipoCurso;
  private Collection colAreaConocimiento;
  private Collection colPais;
  private Collection colPersonal;
  private String selectTipoCurso;
  private String selectAreaConocimiento;
  private String selectPais;
  private String selectPersonal;
  private Object stateResultPersonal = null;

  private Object stateResultEstudioByPersonal = null;

  public String getSelectTipoCurso()
  {
    return this.selectTipoCurso;
  }
  public void setSelectTipoCurso(String valTipoCurso) {
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    this.estudio.setTipoCurso(null);
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      if (String.valueOf(tipoCurso.getIdTipoCurso()).equals(
        valTipoCurso)) {
        this.estudio.setTipoCurso(
          tipoCurso);
        break;
      }
    }
    this.selectTipoCurso = valTipoCurso;
  }
  public String getSelectAreaConocimiento() {
    return this.selectAreaConocimiento;
  }
  public void setSelectAreaConocimiento(String valAreaConocimiento) {
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    this.estudio.setAreaConocimiento(null);
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      if (String.valueOf(areaConocimiento.getIdAreaConocimiento()).equals(
        valAreaConocimiento)) {
        this.estudio.setAreaConocimiento(
          areaConocimiento);
        break;
      }
    }
    this.selectAreaConocimiento = valAreaConocimiento;
  }
  public String getSelectPais() {
    return this.selectPais;
  }
  public void setSelectPais(String valPais) {
    Iterator iterator = this.colPais.iterator();
    Pais pais = null;
    this.estudio.setPais(null);
    while (iterator.hasNext()) {
      pais = (Pais)iterator.next();
      if (String.valueOf(pais.getIdPais()).equals(
        valPais)) {
        this.estudio.setPais(
          pais);
        break;
      }
    }
    this.selectPais = valPais;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.estudio.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.estudio.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public Estudio getEstudio() {
    if (this.estudio == null) {
      this.estudio = new Estudio();
    }
    return this.estudio;
  }

  public EstudioForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColTipoCurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCurso.getIdTipoCurso()), 
        tipoCurso.toString()));
    }
    return col;
  }

  public Collection getColAreaConocimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public Collection getListUnidadTiempo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Estudio.LISTA_UNIDAD_TIEMPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListOrigenCurso() {
    Collection col = new ArrayList();

    Iterator iterEntry = Estudio.LISTA_ORIGEN_CURSO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListParticipacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = Estudio.LISTA_PARTICIPACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCertifico() {
    Collection col = new ArrayList();

    Iterator iterEntry = Estudio.LISTA_CERTIFICO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPais()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPais.iterator();
    Pais pais = null;
    while (iterator.hasNext()) {
      pais = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(pais.getIdPais()), 
        pais.toString()));
    }
    return col;
  }

  public Collection getListBecado() {
    Collection col = new ArrayList();

    Iterator iterEntry = Estudio.LISTA_BECADO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListFinanciamiento() {
    Collection col = new ArrayList();

    Iterator iterEntry = Estudio.LISTA_FINANCIAMIENTO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colTipoCurso = 
        this.adiestramientoFacade.findAllTipoCurso();
      this.colAreaConocimiento = 
        this.adiestramientoFacade.findAllAreaConocimiento();
      this.colPais = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findEstudioByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findEstudioByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectEstudio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoCurso = null;
    this.selectAreaConocimiento = null;
    this.selectPais = null;
    this.selectPersonal = null;

    long idEstudio = 
      Long.parseLong((String)requestParameterMap.get("idEstudio"));
    try
    {
      this.estudio = 
        this.expedienteFacade.findEstudioById(
        idEstudio);

      if (this.estudio.getTipoCurso() != null) {
        this.selectTipoCurso = 
          String.valueOf(this.estudio.getTipoCurso().getIdTipoCurso());
      }
      if (this.estudio.getAreaConocimiento() != null) {
        this.selectAreaConocimiento = 
          String.valueOf(this.estudio.getAreaConocimiento().getIdAreaConocimiento());
      }
      if (this.estudio.getPais() != null) {
        this.selectPais = 
          String.valueOf(this.estudio.getPais().getIdPais());
      }
      if (this.estudio.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.estudio.getPersonal().getIdPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try
    {
      if ((this.estudio.getAnio() == 0) || (this.estudio.getAnio() > new Date().getYear() + 1900)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año tiene un valor no valido", ""));
        error = true;
      }
    }
    catch (Exception localException1) {
    }
    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.estudio.setPersonal(
          this.personal);
        this.expedienteFacade.addEstudio(
          this.estudio);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateEstudio(
          this.estudio);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteEstudio(
        this.estudio);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectTipoCurso = null;

    this.selectAreaConocimiento = null;

    this.selectPais = null;

    this.selectPersonal = null;

    this.estudio = new Estudio();

    this.estudio.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.estudio.setIdEstudio(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Estudio"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.estudio = new Estudio();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.estudio = new Estudio();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }
}