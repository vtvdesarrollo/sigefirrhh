package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class VacacionProgramadaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addVacacionProgramada(VacacionProgramada vacacionProgramada)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    VacacionProgramada vacacionProgramadaNew = 
      (VacacionProgramada)BeanUtils.cloneBean(
      vacacionProgramada);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionProgramadaNew.getTipoPersonal() != null) {
      vacacionProgramadaNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionProgramadaNew.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (vacacionProgramadaNew.getPersonal() != null) {
      vacacionProgramadaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        vacacionProgramadaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(vacacionProgramadaNew);
  }

  public void updateVacacionProgramada(VacacionProgramada vacacionProgramada) throws Exception
  {
    VacacionProgramada vacacionProgramadaModify = 
      findVacacionProgramadaById(vacacionProgramada.getIdVacacionProgramada());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionProgramada.getTipoPersonal() != null) {
      vacacionProgramada.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionProgramada.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (vacacionProgramada.getPersonal() != null) {
      vacacionProgramada.setPersonal(
        personalBeanBusiness.findPersonalById(
        vacacionProgramada.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(vacacionProgramadaModify, vacacionProgramada);
  }

  public void deleteVacacionProgramada(VacacionProgramada vacacionProgramada) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    VacacionProgramada vacacionProgramadaDelete = 
      findVacacionProgramadaById(vacacionProgramada.getIdVacacionProgramada());
    pm.deletePersistent(vacacionProgramadaDelete);
  }

  public VacacionProgramada findVacacionProgramadaById(long idVacacionProgramada) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idVacacionProgramada == pIdVacacionProgramada";
    Query query = pm.newQuery(VacacionProgramada.class, filter);

    query.declareParameters("long pIdVacacionProgramada");

    parameters.put("pIdVacacionProgramada", new Long(idVacacionProgramada));

    Collection colVacacionProgramada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colVacacionProgramada.iterator();
    return (VacacionProgramada)iterator.next();
  }

  public Collection findVacacionProgramadaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent vacacionProgramadaExtent = pm.getExtent(
      VacacionProgramada.class, true);
    Query query = pm.newQuery(vacacionProgramadaExtent);
    query.setOrdering("anio ascending, fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(VacacionProgramada.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, fechaInicio ascending");

    Collection colVacacionProgramada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVacacionProgramada);

    return colVacacionProgramada;
  }
}