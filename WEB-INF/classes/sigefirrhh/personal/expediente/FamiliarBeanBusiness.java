package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class FamiliarBeanBusiness extends AbstractBeanBusiness
{
  public void addFamiliar(Familiar familiar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Familiar familiarNew = 
      (Familiar)BeanUtils.cloneBean(
      familiar);

    if (((familiarNew.getParentesco().equals("P")) || (familiarNew.getParentesco().equals("M")) || (familiarNew.getParentesco().equals("C"))) && (countParentesco(familiarNew.getPersonal().getIdPersonal(), familiarNew.getParentesco(), 0L) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo personal");
      throw error;
    }

    if ((familiarNew.getParentesco().equals("S")) && (countParentesco(familiarNew.getPersonal().getIdPersonal(), familiarNew.getParentesco(), familiarNew.getSexo(), 0L) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo personal");
      throw error;
    }

    if ((familiarNew.getCedulaFamiliar() != 0) && (searchCedulaFamiliar(familiarNew.getPersonal().getIdPersonal(), familiarNew.getCedulaFamiliar(), 0L) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este cédula ya esta registrado en otro familiar para el mismo trabajador");
      throw error;
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (familiarNew.getPersonal() != null) {
      familiarNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        familiarNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(familiarNew);
  }

  public void updateFamiliar(Familiar familiar) throws Exception
  {
    Familiar familiarModify = 
      findFamiliarById(familiar.getIdFamiliar());

    if (((familiar.getParentesco().equals("P")) || (familiar.getParentesco().equals("M")) || (familiar.getParentesco().equals("C"))) && (countParentesco(familiar.getPersonal().getIdPersonal(), familiar.getParentesco(), familiarModify.getIdFamiliar()) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo personal");
      throw error;
    }

    if (((familiar.getParentesco().equals("A")) || (familiar.getParentesco().equals("S"))) && (countParentesco(familiar.getPersonal().getIdPersonal(), familiar.getParentesco(), familiar.getSexo(), familiarModify.getIdFamiliar()) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo personal");
      throw error;
    }

    if ((familiar.getCedulaFamiliar() != 0) && (searchCedulaFamiliar(familiar.getPersonal().getIdPersonal(), familiar.getCedulaFamiliar(), familiarModify.getIdFamiliar()) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este cédula ya esta registrado en otro familiar para el mismo trabajador");
      throw error;
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (familiar.getPersonal() != null) {
      familiar.setPersonal(
        personalBeanBusiness.findPersonalById(
        familiar.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(familiarModify, familiar);
  }

  public void deleteFamiliar(Familiar familiar) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Familiar familiarDelete = 
      findFamiliarById(familiar.getIdFamiliar());
    pm.deletePersistent(familiarDelete);
  }

  public Familiar findFamiliarById(long idFamiliar) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFamiliar == pIdFamiliar";
    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdFamiliar");

    parameters.put("pIdFamiliar", new Long(idFamiliar));

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFamiliar.iterator();
    return (Familiar)iterator.next();
  }

  public Collection findFamiliarAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent familiarExtent = pm.getExtent(
      Familiar.class, true);
    Query query = pm.newQuery(familiarExtent);
    query.setOrdering("primerApellido ascending, primerNombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("primerApellido ascending, primerNombre ascending");

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliar);

    return colFamiliar;
  }

  public int countParentesco(long idPersonal, String parentesco, long idFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && parentesco == pParentesco && idFamiliar != pIdFamiliar";

    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdPersonal, String pParentesco, long pIdFamiliar");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pParentesco", new String(parentesco));
    parameters.put("pIdFamiliar", new Long(idFamiliar));

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliar);

    return colFamiliar.size();
  }

  public int countParentesco(long idPersonal, String parentesco, String sexo, long idFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && parentesco == pParentesco && sexo == pSexo && idFamiliar != pIdFamiliar";

    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdPersonal, String pParentesco, String pSexo, long pIdFamiliar");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pParentesco", new String(parentesco));
    parameters.put("pSexo", new String(sexo));
    parameters.put("pIdFamiliar", new Long(idFamiliar));

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliar);

    return colFamiliar.size();
  }

  public int searchCedulaFamiliar(long idPersonal, int cedulaFamiliar, long idFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && cedulaFamiliar == pCedulaFamiliar && idFamiliar != pIdFamiliar";

    Query query = pm.newQuery(Familiar.class, filter);

    query.declareParameters("long pIdPersonal, int pCedulaFamiliar, long pIdFamiliar");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pCedulaFamiliar", new Integer(cedulaFamiliar));
    parameters.put("pIdFamiliar", new Long(idFamiliar));

    Collection colFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFamiliar);

    return colFamiliar.size();
  }
}