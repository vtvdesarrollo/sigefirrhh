package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class ActividadDocentePK
  implements Serializable
{
  public long idActividadDocente;

  public ActividadDocentePK()
  {
  }

  public ActividadDocentePK(long idActividadDocente)
  {
    this.idActividadDocente = idActividadDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ActividadDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ActividadDocentePK thatPK)
  {
    return 
      this.idActividadDocente == thatPK.idActividadDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idActividadDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idActividadDocente);
  }
}