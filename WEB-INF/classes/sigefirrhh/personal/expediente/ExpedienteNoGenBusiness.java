package sigefirrhh.personal.expediente;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

public class ExpedienteNoGenBusiness extends ExpedienteBusiness
  implements Serializable
{
  private TrayectoriaNoGenBeanBusiness trayectoriaNoGenBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  private FamiliarNoGenBeanBusiness familiarNoGenBeanBusiness = new FamiliarNoGenBeanBusiness();

  private EstudioNoGenBeanBusiness estudioNoGenBeanBusiness = new EstudioNoGenBeanBusiness();

  private EducacionNoGenBeanBusiness educacionNoGenBeanBusiness = new EducacionNoGenBeanBusiness();

  public Collection findTrayectoriaByPersonal(long idPersonal, String estatus, String origen)
    throws Exception
  {
    return this.trayectoriaNoGenBeanBusiness.findByPersonal(idPersonal, estatus, origen);
  }

  public Collection findTrayectoriaByPersonal(long idPersonal, String estatus) throws Exception {
    return this.trayectoriaNoGenBeanBusiness.findByPersonal(idPersonal, estatus);
  }

  public Collection findTrayectoriaAprobadoByPersonal(long idPersonal) throws Exception {
    return this.trayectoriaNoGenBeanBusiness.findAprobadoByPersonal(idPersonal);
  }

  public Collection findTrayectoriaDevueltoByPersonal(long idPersonal) throws Exception {
    return this.trayectoriaNoGenBeanBusiness.findDevueltoByPersonal(idPersonal);
  }

  public Collection findTrayectoriaByAnioAndNumeroMovimiento(int anio, int numeroMovimiento) throws Exception {
    return this.trayectoriaNoGenBeanBusiness.findByAnioAndNumeroMovimiento(anio, numeroMovimiento);
  }

  public Collection findFamiliarByPersonalAndParentesco(long idPersonal, String parentesco) throws Exception {
    return this.familiarNoGenBeanBusiness.findByPersonalAndParentesco(idPersonal, parentesco);
  }

  public Collection findFamiliarByPersonalParentescoEdadMaxima(long idPersonal, String parentesco, int edadMaxima) throws Exception {
    return this.familiarNoGenBeanBusiness.findByPersonalParentescoEdadMaxima(idPersonal, parentesco, edadMaxima);
  }

  public Collection findFamiliarByPersonalParentescoPensionado(long idPersonal, String parentescoH, String parentescoC, int edadMaximaH, int edadMaximaF, int edadMaximaM) throws Exception {
    return this.familiarNoGenBeanBusiness.findByPersonalParentescoPensionado(idPersonal, parentescoH, parentescoC, edadMaximaH, edadMaximaF, edadMaximaM);
  }

  public Collection findEstudioByPersonalAndTipoCurso(long idPersonal, String tipoCurso) throws Exception {
    return this.estudioNoGenBeanBusiness.findByPersonalAndTipoCurso(idPersonal, tipoCurso);
  }

  public Collection findEducacionByClasificacionDocentePostgrado(long idPersonal) throws Exception {
    return this.educacionNoGenBeanBusiness.findByClasificacionDocentePostgrado(idPersonal);
  }

  public Collection findEducacionByClasificacionDocenteDoctorado(long idPersonal) throws Exception {
    return this.educacionNoGenBeanBusiness.findByClasificacionDocenteDoctorado(idPersonal);
  }

  public Collection findAntecedentesApn(long idPersonal) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append(" select nombre_organismo, fecha_desde, fecha_hasta from(  ");
      sql.append("    \tselect nombre_institucion as nombre_organismo, ");
      sql.append("    \tfecha_ingreso as fecha_desde, fecha_egreso as fecha_hasta ");
      sql.append("\t\tfrom antecedente where id_personal = ?");
      sql.append(" union ");
      sql.append(" \tselect nombre_institucion as nombre_organismo,");
      sql.append(" \tfecha_ingreso as fecha_desde, fecha_egreso as fecha_hasta");
      sql.append(" \tfrom experiencianoest where id_personal = ?) ");
      sql.append(" as antecedente order by fecha_desde ");
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idPersonal);
      stRegistros.setLong(2, idPersonal);
      rsRegistros = stRegistros.executeQuery();

      int id = 0;
      while (rsRegistros.next()) {
        id++;
        AntecedenteAux antecedenteAux = new AntecedenteAux();
        antecedenteAux.setId(id);
        antecedenteAux.setOrganismo(rsRegistros.getString("nombre_organismo"));
        antecedenteAux.setFechaDesde(rsRegistros.getDate("fecha_desde"));
        antecedenteAux.setFechaHasta(rsRegistros.getDate("fecha_hasta"));
        col.add(antecedenteAux);
      }
      connection.close(); connection = null;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }

  public AntecedenteAux calcularTiempoApn(Collection colAntecedentes)
    throws Exception
  {
    Calendar calDesde = Calendar.getInstance();
    Calendar calHasta = Calendar.getInstance();
    Iterator iterAntecedentes = colAntecedentes.iterator();

    int anios = 0;
    int meses = 0;
    int dias = 0;
    int anioDesde = 0;
    int mesDesde = 0;
    int diaDesde = 0;
    int anioHasta = 0;
    int mesHasta = 0;
    int diaHasta = 0;
    int totalAnios = 0;
    int totalMeses = 0;
    int totalDias = 0;

    int mesesAdicionales = 0;
    int diasAdicionales = 0;
    int aniosAdicionales = 0;

    boolean bisiestoDesde = false;
    boolean BisiestoHasta = false;

    while (iterAntecedentes.hasNext()) {
      AntecedenteAux antecedente = (AntecedenteAux)iterAntecedentes.next();
      calDesde.setTime(antecedente.getFechaDesde());
      calHasta.setTime(antecedente.getFechaHasta());

      anioHasta = calHasta.get(1);
      mesHasta = calHasta.get(2);
      diaHasta = calHasta.get(5);

      anioDesde = calDesde.get(1);
      mesDesde = calDesde.get(2);
      diaDesde = calDesde.get(5);

      if (((mesHasta == 2) && (diaHasta >= 28)) || ((mesHasta != 2) && (diaHasta > 30))) {
        diaHasta = 30;
      }
      diaHasta = 30;

      anios = anioHasta - anioDesde;
      meses = mesHasta - mesDesde;
      dias = diaHasta + 1 - diaDesde;

      if (meses < 0) {
        anios--;
        meses = 12 - -meses;
      }
      if (dias < 0) {
        meses--;
        dias = 30 - -dias;
      }
      totalAnios += anios;
      totalMeses += meses;
      totalDias += dias;
    }

    if (totalDias >= 30) {
      totalMeses += totalDias / 30;
      totalDias -= totalDias / 30 * 30;
    }
    if (totalMeses >= 12) {
      totalAnios += totalMeses / 12;
      totalMeses -= totalMeses / 12 * 12;
    }
    AntecedenteAux antecedente = new AntecedenteAux();
    antecedente.setAnios(totalAnios);
    antecedente.setMeses(totalMeses);
    antecedente.setDias(totalDias);

    return antecedente;
  }
}