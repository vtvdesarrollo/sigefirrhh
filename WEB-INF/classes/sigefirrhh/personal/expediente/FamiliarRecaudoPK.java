package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class FamiliarRecaudoPK
  implements Serializable
{
  public long idFamiliarRecaudo;

  public FamiliarRecaudoPK()
  {
  }

  public FamiliarRecaudoPK(long idFamiliarRecaudo)
  {
    this.idFamiliarRecaudo = idFamiliarRecaudo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FamiliarRecaudoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FamiliarRecaudoPK thatPK)
  {
    return 
      this.idFamiliarRecaudo == thatPK.idFamiliarRecaudo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFamiliarRecaudo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFamiliarRecaudo);
  }
}