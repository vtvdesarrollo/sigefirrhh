package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class VacacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addVacacion(Vacacion vacacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Vacacion vacacionNew = 
      (Vacacion)BeanUtils.cloneBean(
      vacacion);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacionNew.getTipoPersonal() != null) {
      vacacionNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacionNew.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (vacacionNew.getPersonal() != null) {
      vacacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        vacacionNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(vacacionNew);
  }

  public void updateVacacion(Vacacion vacacion) throws Exception
  {
    Vacacion vacacionModify = 
      findVacacionById(vacacion.getIdVacacion());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (vacacion.getTipoPersonal() != null) {
      vacacion.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        vacacion.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (vacacion.getPersonal() != null) {
      vacacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        vacacion.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(vacacionModify, vacacion);
  }

  public void deleteVacacion(Vacacion vacacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Vacacion vacacionDelete = 
      findVacacionById(vacacion.getIdVacacion());
    pm.deletePersistent(vacacionDelete);
  }

  public Vacacion findVacacionById(long idVacacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idVacacion == pIdVacacion";
    Query query = pm.newQuery(Vacacion.class, filter);

    query.declareParameters("long pIdVacacion");

    parameters.put("pIdVacacion", new Long(idVacacion));

    Collection colVacacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colVacacion.iterator();
    return (Vacacion)iterator.next();
  }

  public Collection findVacacionAll()
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent vacacionExtent = pm.getExtent(
      Vacacion.class, true);
    Query query = pm.newQuery(vacacionExtent);
    query.setOrdering("anioInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Vacacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, tipoVacacion descending");

    Collection colVacacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVacacion);

    return colVacacion;
  }

  public Collection findByPersonalPendiente(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal && (diasPendientes - diasDisfrute) > 0";

    Query query = pm.newQuery(Vacacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, tipoVacacion descending");

    Collection colVacacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVacacion);

    return colVacacion;
  }
}