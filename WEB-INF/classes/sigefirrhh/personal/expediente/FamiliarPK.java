package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class FamiliarPK
  implements Serializable
{
  public long idFamiliar;

  public FamiliarPK()
  {
  }

  public FamiliarPK(long idFamiliar)
  {
    this.idFamiliar = idFamiliar;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FamiliarPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FamiliarPK thatPK)
  {
    return 
      this.idFamiliar == thatPK.idFamiliar;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFamiliar)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFamiliar);
  }
}