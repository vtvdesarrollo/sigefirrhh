package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class HistorialApnPK
  implements Serializable
{
  public long idHistorialApn;

  public HistorialApnPK()
  {
  }

  public HistorialApnPK(long idHistorialApn)
  {
    this.idHistorialApn = idHistorialApn;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistorialApnPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistorialApnPK thatPK)
  {
    return 
      this.idHistorialApn == thatPK.idHistorialApn;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistorialApn)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistorialApn);
  }
}