package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class CertificacionPK
  implements Serializable
{
  public long idCertificacion;

  public CertificacionPK()
  {
  }

  public CertificacionPK(long idCertificacion)
  {
    this.idCertificacion = idCertificacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CertificacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CertificacionPK thatPK)
  {
    return 
      this.idCertificacion == thatPK.idCertificacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCertificacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCertificacion);
  }
}