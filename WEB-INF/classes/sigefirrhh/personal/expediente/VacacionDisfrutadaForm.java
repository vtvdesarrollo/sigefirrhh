package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class VacacionDisfrutadaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(VacacionDisfrutadaForm.class.getName());
  private VacacionDisfrutada vacacionDisfrutada;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private Collection resultPersonal;
  private Personal personal;
  private Vacacion vacacion;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colTipoPersonal;
  private Collection colVacacion;
  private Collection colPersonal;
  private String selectTipoPersonal;
  private String selectVacacion;
  private String selectPersonal;
  private Object stateResultPersonal = null;

  private Object stateResultVacacionDisfrutadaByPersonal = null;

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.vacacionDisfrutada.setTipoPersonal(null);

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valTipoPersonal)) {
        try {
          this.vacacionDisfrutada.setTipoPersonal(this.definicionesFacade.findTipoPersonalById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }

  public String getSelectVacacion() {
    return this.selectVacacion;
  }

  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectVacacion(String valVacacion) {
    Iterator iterator = this.colVacacion.iterator();
    Vacacion vacacion = null;
    this.vacacionDisfrutada.setVacacion(null);
    while (iterator.hasNext()) {
      vacacion = (Vacacion)iterator.next();
      if (String.valueOf(vacacion.getIdVacacion()).equals(
        valVacacion)) {
        try {
          this.vacacionDisfrutada.setVacacion(vacacion);
          this.vacacionDisfrutada.setAnio(vacacion.getAnio());
        }
        catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }
    this.selectVacacion = valVacacion;
  }

  public void changeSelectVacacion(ValueChangeEvent event) {
    long valVacacion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    Iterator iterator = this.colVacacion.iterator();
    Vacacion vacacion = null;
    this.vacacionDisfrutada.setVacacion(null);
    if (valVacacion != 0L) {
      while (iterator.hasNext()) {
        vacacion = (Vacacion)iterator.next();
        if (vacacion.getIdVacacion() == valVacacion)
          try {
            this.vacacionDisfrutada.setVacacion(vacacion);
            this.vacacionDisfrutada.setAnio(vacacion.getAnio());
          }
          catch (Exception e) {
            log.error("Excepcion controlada:", e);
          }
      }
    }
    this.selectVacacion = String.valueOf(valVacacion);
  }

  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.vacacionDisfrutada.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.vacacionDisfrutada.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public VacacionDisfrutada getVacacionDisfrutada() {
    if (this.vacacionDisfrutada == null) {
      this.vacacionDisfrutada = new VacacionDisfrutada();
    }
    return this.vacacionDisfrutada;
  }

  public VacacionDisfrutadaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getColPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public Collection getColVacacion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colVacacion.iterator();
    Vacacion vacacion = null;
    while (iterator.hasNext()) {
      vacacion = (Vacacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(vacacion.getIdVacacion()), 
        vacacion.toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findVacacionDisfrutadaByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findVacacionDisfrutadaByPersonal(
          this.personal.getIdPersonal());
        if (this.personal != null)
          this.colVacacion = this.expedienteFacade.findVacacionByPersonalPendiente(this.personal.getIdPersonal());
        this.showResult = ((this.result != null) && (!this.result.isEmpty()));
        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectVacacionDisfrutada()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectPersonal = null;

    long idVacacionDisfrutada = 
      Long.parseLong((String)requestParameterMap.get("idVacacionDisfrutada"));
    try
    {
      this.vacacionDisfrutada = 
        this.expedienteFacade.findVacacionDisfrutadaById(
        idVacacionDisfrutada);

      if (this.vacacionDisfrutada.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.vacacionDisfrutada.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.vacacionDisfrutada.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.vacacionDisfrutada.getPersonal().getIdPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
      if (this.personal != null) {
        this.colTipoPersonal = this.definicionesFacade.findTipoPersonalByPersonal(this.personal.getIdPersonal(), "A");
        this.colVacacion = this.expedienteFacade.findVacacionByPersonalPendiente(this.personal.getIdPersonal());
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    log.error("paso 1");
    boolean error = false;
    try
    {
      log.error("paso 2");
      if ((this.vacacionDisfrutada.getFechaInicio() == null) || (this.vacacionDisfrutada.getFechaFin() == null)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Los campos Fecha Inicio y Fecha Fin son requeridos", ""));
        error = true;
      } else {
        if (this.vacacionDisfrutada.getFechaInicio().compareTo(this.vacacionDisfrutada.getFechaFin()) >= 0) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha Fin tiene un valor no valido", ""));
          error = true;
        }
        if ((this.vacacionDisfrutada.getFechaReintegro() != null) && 
          (this.vacacionDisfrutada.getFechaFin().compareTo(this.vacacionDisfrutada.getFechaReintegro()) >= 0)) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha Reintegro tiene un valor no valido", ""));
          error = true;
        }

      }

      log.error("paso 3");

      if (this.vacacionDisfrutada.getDiasDisfrute() > this.vacacionDisfrutada.getVacacion().getDiasPendientes() - this.vacacionDisfrutada.getVacacion().getDiasDisfrute()) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Los dias a disfrutar no pueden ser mayor que los dias pendientes.", ""));
        error = true;
      }
      if (this.vacacionDisfrutada.getDiasDisfrute() > this.vacacionDisfrutada.getVacacion().getDiasPendientes() - this.vacacionDisfrutada.getVacacion().getDiasDisfrute()) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Los dias a disfrutar no pueden ser mayor que los dias pendientes.", ""));
        error = true;
      }
      log.error("paso 4");
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      log.error("Excepcion controlada:", e);
    }

    if (error) {
      return null;
    }

    log.error("paso 5");
    try {
      log.error("paso 6");
      this.vacacionDisfrutada.setAnio(this.vacacionDisfrutada.getVacacion().getAnio());

      log.error("paso 7");
      if (this.adding) {
        this.vacacionDisfrutada.setPersonal(
          this.personal);
        this.expedienteFacade.addVacacionDisfrutada(
          this.vacacionDisfrutada);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateVacacionDisfrutada(
          this.vacacionDisfrutada);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
      log.error("paso 8");
    } catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
      log.error("Excepcion controlada:", e);
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteVacacionDisfrutada(
        this.vacacionDisfrutada);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectTipoPersonal = null;
    this.selectVacacion = null;

    this.selectPersonal = null;

    this.vacacionDisfrutada = new VacacionDisfrutada();
    this.vacacionDisfrutada.setAnio(Calendar.getInstance().getTime().getYear() + 1900);

    this.vacacionDisfrutada.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.vacacionDisfrutada.setIdVacacionDisfrutada(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.VacacionDisfrutada"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.vacacionDisfrutada = new VacacionDisfrutada();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.vacacionDisfrutada = new VacacionDisfrutada();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public void setColVacacion(Collection colVacacion) {
    this.colVacacion = colVacacion;
  }
  public Vacacion getVacacion() {
    return this.vacacion;
  }
  public void setVacacion(Vacacion vacacion) {
    this.vacacion = vacacion;
  }
}