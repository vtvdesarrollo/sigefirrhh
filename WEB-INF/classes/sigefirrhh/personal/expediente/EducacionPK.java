package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class EducacionPK
  implements Serializable
{
  public long idEducacion;

  public EducacionPK()
  {
  }

  public EducacionPK(long idEducacion)
  {
    this.idEducacion = idEducacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EducacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EducacionPK thatPK)
  {
    return 
      this.idEducacion == thatPK.idEducacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEducacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEducacion);
  }
}