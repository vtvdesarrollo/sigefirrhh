package sigefirrhh.personal.expediente;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Familiar
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTADO_CIVIL;
  protected static final Map LISTA_SEXO;
  protected static final Map LISTA_PARENTESCO;
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_GRUPO_SANGUINEO;
  private long idFamiliar;
  private String primerNombre;
  private String segundoNombre;
  private String primerApellido;
  private String segundoApellido;
  private String estadoCivil;
  private String sexo;
  private int cedulaFamiliar;
  private Date fechaNacimiento;
  private String parentesco;
  private String nivelEducativo;
  private String grado;
  private String promedioNota;
  private String mismoOrganismo;
  private String ninoExcepcional;
  private String tallaFranela;
  private String tallaPantalon;
  private String tallaGorra;
  private String gozaPrimaPorHijo;
  private String gozaUtiles;
  private String gozaSeguro;
  private String gozaBeca;
  private String grupoSanguineo;
  private String alergico;
  private String alergias;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "alergias", "alergico", "cedulaFamiliar", "estadoCivil", "fechaNacimiento", "gozaBeca", "gozaPrimaPorHijo", "gozaSeguro", "gozaUtiles", "grado", "grupoSanguineo", "idFamiliar", "idSitp", "mismoOrganismo", "ninoExcepcional", "nivelEducativo", "parentesco", "personal", "primerApellido", "primerNombre", "promedioNota", "segundoApellido", "segundoNombre", "sexo", "tallaFranela", "tallaGorra", "tallaPantalon", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Familiar());

    LISTA_ESTADO_CIVIL = 
      new LinkedHashMap();
    LISTA_SEXO = 
      new LinkedHashMap();
    LISTA_PARENTESCO = 
      new LinkedHashMap();
    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_GRUPO_SANGUINEO = 
      new LinkedHashMap();

    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
    LISTA_ESTADO_CIVIL.put("D", "DIVORCIADO(A)");
    LISTA_ESTADO_CIVIL.put("V", "VIUDO(A)");
    LISTA_ESTADO_CIVIL.put("U", "UNIDO(A)");
    LISTA_ESTADO_CIVIL.put("O", "OTRO");
    LISTA_SEXO.put("F", "FEMENINO");
    LISTA_SEXO.put("M", "MASCULINO");
    LISTA_PARENTESCO.put("C", "CONYUGUE");
    LISTA_PARENTESCO.put("M", "MADRE");
    LISTA_PARENTESCO.put("P", "PADRE");
    LISTA_PARENTESCO.put("H", "HIJO(A)");
    LISTA_PARENTESCO.put("E", "HERMANO(A)");
    LISTA_PARENTESCO.put("S", "SUEGRO(A)");
    LISTA_PARENTESCO.put("A", "ABUELO(A)");
    LISTA_PARENTESCO.put("B", "SOBRINO(A)");
    LISTA_PARENTESCO.put("I", "TIO(A)");
    LISTA_PARENTESCO.put("U", "TUTELADO(A)");
    LISTA_PARENTESCO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("I", "PRIMARIA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("H", "BACHILLERATO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("E", "ESPECIALIZACION");
    LISTA_NIVEL_EDUCATIVO.put("M", "MAESTRIA");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("R", "POST DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("L", "DIPLOMADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("N", "NINGUNO");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_GRUPO_SANGUINEO.put("A+", "A+");
    LISTA_GRUPO_SANGUINEO.put("B+", "B+");
    LISTA_GRUPO_SANGUINEO.put("O+", "O+");
    LISTA_GRUPO_SANGUINEO.put("A-", "A-");
    LISTA_GRUPO_SANGUINEO.put("B-", "B-");
    LISTA_GRUPO_SANGUINEO.put("O-", "O-");
    LISTA_GRUPO_SANGUINEO.put("AB+", "AB+");
    LISTA_GRUPO_SANGUINEO.put("AB-", "AB-");
    LISTA_GRUPO_SANGUINEO.put("NO", "NO");
  }

  public Familiar()
  {
    jdoSetcedulaFamiliar(this, 0);

    jdoSetnivelEducativo(this, "N");

    jdoSetmismoOrganismo(this, "N");

    jdoSetninoExcepcional(this, "N");

    jdoSetgozaPrimaPorHijo(this, "N");

    jdoSetgozaUtiles(this, "N");

    jdoSetgozaSeguro(this, "N");

    jdoSetgozaBeca(this, "N");

    jdoSetgrupoSanguineo(this, "NO");

    jdoSetalergico(this, "N");
  }

  public String toString()
  {
    return jdoGetprimerApellido(this) + " " + 
      jdoGetprimerNombre(this) + " - " + 
      LISTA_PARENTESCO.get(jdoGetparentesco(this)) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaNacimiento(this));
  }

  public String getAlergias() {
    return jdoGetalergias(this);
  }

  public String getAlergico() {
    return jdoGetalergico(this);
  }

  public int getCedulaFamiliar() {
    return jdoGetcedulaFamiliar(this);
  }

  public String getEstadoCivil() {
    return jdoGetestadoCivil(this);
  }

  public Date getFechaNacimiento() {
    return jdoGetfechaNacimiento(this);
  }

  public String getGozaBeca() {
    return jdoGetgozaBeca(this);
  }

  public String getGozaPrimaPorHijo() {
    return jdoGetgozaPrimaPorHijo(this);
  }

  public String getGozaSeguro() {
    return jdoGetgozaSeguro(this);
  }

  public String getGozaUtiles() {
    return jdoGetgozaUtiles(this);
  }

  public String getGrado() {
    return jdoGetgrado(this);
  }

  public String getGrupoSanguineo() {
    return jdoGetgrupoSanguineo(this);
  }

  public long getIdFamiliar() {
    return jdoGetidFamiliar(this);
  }

  public String getMismoOrganismo() {
    return jdoGetmismoOrganismo(this);
  }

  public String getNinoExcepcional() {
    return jdoGetninoExcepcional(this);
  }

  public String getNivelEducativo() {
    return jdoGetnivelEducativo(this);
  }

  public String getParentesco() {
    return jdoGetparentesco(this);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public String getPrimerApellido() {
    return jdoGetprimerApellido(this);
  }

  public String getPrimerNombre() {
    return jdoGetprimerNombre(this);
  }

  public String getPromedioNota() {
    return jdoGetpromedioNota(this);
  }

  public String getSegundoApellido() {
    return jdoGetsegundoApellido(this);
  }

  public String getSegundoNombre() {
    return jdoGetsegundoNombre(this);
  }

  public String getSexo() {
    return jdoGetsexo(this);
  }

  public String getTallaFranela() {
    return jdoGettallaFranela(this);
  }

  public String getTallaGorra() {
    return jdoGettallaGorra(this);
  }

  public String getTallaPantalon() {
    return jdoGettallaPantalon(this);
  }

  public void setAlergias(String string) {
    jdoSetalergias(this, string);
  }

  public void setAlergico(String string) {
    jdoSetalergico(this, string);
  }

  public void setCedulaFamiliar(int i) {
    jdoSetcedulaFamiliar(this, i);
  }

  public void setEstadoCivil(String string) {
    jdoSetestadoCivil(this, string);
  }

  public void setFechaNacimiento(Date date) {
    jdoSetfechaNacimiento(this, date);
  }

  public void setGozaBeca(String string) {
    jdoSetgozaBeca(this, string);
  }

  public void setGozaPrimaPorHijo(String string) {
    jdoSetgozaPrimaPorHijo(this, string);
  }

  public void setGozaSeguro(String string) {
    jdoSetgozaSeguro(this, string);
  }

  public void setGozaUtiles(String string) {
    jdoSetgozaUtiles(this, string);
  }

  public void setGrado(String string) {
    jdoSetgrado(this, string);
  }

  public void setGrupoSanguineo(String string) {
    jdoSetgrupoSanguineo(this, string);
  }

  public void setIdFamiliar(long l) {
    jdoSetidFamiliar(this, l);
  }

  public void setMismoOrganismo(String string) {
    jdoSetmismoOrganismo(this, string);
  }

  public void setNinoExcepcional(String string) {
    jdoSetninoExcepcional(this, string);
  }

  public void setNivelEducativo(String string) {
    jdoSetnivelEducativo(this, string);
  }

  public void setParentesco(String string) {
    jdoSetparentesco(this, string);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public void setPrimerApellido(String string) {
    jdoSetprimerApellido(this, string);
  }

  public void setPrimerNombre(String string) {
    jdoSetprimerNombre(this, string);
  }

  public void setPromedioNota(String string) {
    jdoSetpromedioNota(this, string);
  }

  public void setSegundoApellido(String string) {
    jdoSetsegundoApellido(this, string);
  }

  public void setSegundoNombre(String string) {
    jdoSetsegundoNombre(this, string);
  }

  public void setSexo(String string) {
    jdoSetsexo(this, string);
  }

  public void setTallaFranela(String string) {
    jdoSettallaFranela(this, string);
  }

  public void setTallaGorra(String string) {
    jdoSettallaGorra(this, string);
  }

  public void setTallaPantalon(String string) {
    jdoSettallaPantalon(this, string);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 28;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Familiar localFamiliar = new Familiar();
    localFamiliar.jdoFlags = 1;
    localFamiliar.jdoStateManager = paramStateManager;
    return localFamiliar;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Familiar localFamiliar = new Familiar();
    localFamiliar.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFamiliar.jdoFlags = 1;
    localFamiliar.jdoStateManager = paramStateManager;
    return localFamiliar;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alergias);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alergico);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaFamiliar);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estadoCivil);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaNacimiento);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaBeca);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaPrimaPorHijo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaSeguro);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaUtiles);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.grado);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.grupoSanguineo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFamiliar);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mismoOrganismo);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ninoExcepcional);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.parentesco);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.promedioNota);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sexo);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tallaFranela);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tallaGorra);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tallaPantalon);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alergias = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alergico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaFamiliar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estadoCivil = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaNacimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaBeca = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaPrimaPorHijo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaSeguro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaUtiles = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoSanguineo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFamiliar = localStateManager.replacingLongField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mismoOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ninoExcepcional = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parentesco = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioNota = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sexo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tallaFranela = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tallaGorra = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tallaPantalon = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Familiar paramFamiliar, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.alergias = paramFamiliar.alergias;
      return;
    case 1:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.alergico = paramFamiliar.alergico;
      return;
    case 2:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaFamiliar = paramFamiliar.cedulaFamiliar;
      return;
    case 3:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.estadoCivil = paramFamiliar.estadoCivil;
      return;
    case 4:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.fechaNacimiento = paramFamiliar.fechaNacimiento;
      return;
    case 5:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaBeca = paramFamiliar.gozaBeca;
      return;
    case 6:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaPrimaPorHijo = paramFamiliar.gozaPrimaPorHijo;
      return;
    case 7:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaSeguro = paramFamiliar.gozaSeguro;
      return;
    case 8:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaUtiles = paramFamiliar.gozaUtiles;
      return;
    case 9:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramFamiliar.grado;
      return;
    case 10:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.grupoSanguineo = paramFamiliar.grupoSanguineo;
      return;
    case 11:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.idFamiliar = paramFamiliar.idFamiliar;
      return;
    case 12:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramFamiliar.idSitp;
      return;
    case 13:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.mismoOrganismo = paramFamiliar.mismoOrganismo;
      return;
    case 14:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.ninoExcepcional = paramFamiliar.ninoExcepcional;
      return;
    case 15:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramFamiliar.nivelEducativo;
      return;
    case 16:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.parentesco = paramFamiliar.parentesco;
      return;
    case 17:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramFamiliar.personal;
      return;
    case 18:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramFamiliar.primerApellido;
      return;
    case 19:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramFamiliar.primerNombre;
      return;
    case 20:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.promedioNota = paramFamiliar.promedioNota;
      return;
    case 21:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramFamiliar.segundoApellido;
      return;
    case 22:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramFamiliar.segundoNombre;
      return;
    case 23:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.sexo = paramFamiliar.sexo;
      return;
    case 24:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tallaFranela = paramFamiliar.tallaFranela;
      return;
    case 25:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tallaGorra = paramFamiliar.tallaGorra;
      return;
    case 26:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tallaPantalon = paramFamiliar.tallaPantalon;
      return;
    case 27:
      if (paramFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramFamiliar.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Familiar))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Familiar localFamiliar = (Familiar)paramObject;
    if (localFamiliar.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFamiliar, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FamiliarPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FamiliarPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FamiliarPK))
      throw new IllegalArgumentException("arg1");
    FamiliarPK localFamiliarPK = (FamiliarPK)paramObject;
    localFamiliarPK.idFamiliar = this.idFamiliar;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FamiliarPK))
      throw new IllegalArgumentException("arg1");
    FamiliarPK localFamiliarPK = (FamiliarPK)paramObject;
    this.idFamiliar = localFamiliarPK.idFamiliar;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FamiliarPK))
      throw new IllegalArgumentException("arg2");
    FamiliarPK localFamiliarPK = (FamiliarPK)paramObject;
    localFamiliarPK.idFamiliar = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 11);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FamiliarPK))
      throw new IllegalArgumentException("arg2");
    FamiliarPK localFamiliarPK = (FamiliarPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 11, localFamiliarPK.idFamiliar);
  }

  private static final String jdoGetalergias(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.alergias;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.alergias;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 0))
      return paramFamiliar.alergias;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 0, paramFamiliar.alergias);
  }

  private static final void jdoSetalergias(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.alergias = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.alergias = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 0, paramFamiliar.alergias, paramString);
  }

  private static final String jdoGetalergico(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.alergico;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.alergico;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 1))
      return paramFamiliar.alergico;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 1, paramFamiliar.alergico);
  }

  private static final void jdoSetalergico(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.alergico = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.alergico = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 1, paramFamiliar.alergico, paramString);
  }

  private static final int jdoGetcedulaFamiliar(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.cedulaFamiliar;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.cedulaFamiliar;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 2))
      return paramFamiliar.cedulaFamiliar;
    return localStateManager.getIntField(paramFamiliar, jdoInheritedFieldCount + 2, paramFamiliar.cedulaFamiliar);
  }

  private static final void jdoSetcedulaFamiliar(Familiar paramFamiliar, int paramInt)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.cedulaFamiliar = paramInt;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.cedulaFamiliar = paramInt;
      return;
    }
    localStateManager.setIntField(paramFamiliar, jdoInheritedFieldCount + 2, paramFamiliar.cedulaFamiliar, paramInt);
  }

  private static final String jdoGetestadoCivil(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.estadoCivil;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.estadoCivil;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 3))
      return paramFamiliar.estadoCivil;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 3, paramFamiliar.estadoCivil);
  }

  private static final void jdoSetestadoCivil(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.estadoCivil = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.estadoCivil = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 3, paramFamiliar.estadoCivil, paramString);
  }

  private static final Date jdoGetfechaNacimiento(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.fechaNacimiento;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.fechaNacimiento;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 4))
      return paramFamiliar.fechaNacimiento;
    return (Date)localStateManager.getObjectField(paramFamiliar, jdoInheritedFieldCount + 4, paramFamiliar.fechaNacimiento);
  }

  private static final void jdoSetfechaNacimiento(Familiar paramFamiliar, Date paramDate)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.fechaNacimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.fechaNacimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramFamiliar, jdoInheritedFieldCount + 4, paramFamiliar.fechaNacimiento, paramDate);
  }

  private static final String jdoGetgozaBeca(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.gozaBeca;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.gozaBeca;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 5))
      return paramFamiliar.gozaBeca;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 5, paramFamiliar.gozaBeca);
  }

  private static final void jdoSetgozaBeca(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.gozaBeca = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.gozaBeca = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 5, paramFamiliar.gozaBeca, paramString);
  }

  private static final String jdoGetgozaPrimaPorHijo(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.gozaPrimaPorHijo;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.gozaPrimaPorHijo;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 6))
      return paramFamiliar.gozaPrimaPorHijo;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 6, paramFamiliar.gozaPrimaPorHijo);
  }

  private static final void jdoSetgozaPrimaPorHijo(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.gozaPrimaPorHijo = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.gozaPrimaPorHijo = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 6, paramFamiliar.gozaPrimaPorHijo, paramString);
  }

  private static final String jdoGetgozaSeguro(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.gozaSeguro;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.gozaSeguro;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 7))
      return paramFamiliar.gozaSeguro;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 7, paramFamiliar.gozaSeguro);
  }

  private static final void jdoSetgozaSeguro(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.gozaSeguro = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.gozaSeguro = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 7, paramFamiliar.gozaSeguro, paramString);
  }

  private static final String jdoGetgozaUtiles(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.gozaUtiles;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.gozaUtiles;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 8))
      return paramFamiliar.gozaUtiles;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 8, paramFamiliar.gozaUtiles);
  }

  private static final void jdoSetgozaUtiles(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.gozaUtiles = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.gozaUtiles = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 8, paramFamiliar.gozaUtiles, paramString);
  }

  private static final String jdoGetgrado(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.grado;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.grado;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 9))
      return paramFamiliar.grado;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 9, paramFamiliar.grado);
  }

  private static final void jdoSetgrado(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.grado = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.grado = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 9, paramFamiliar.grado, paramString);
  }

  private static final String jdoGetgrupoSanguineo(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.grupoSanguineo;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.grupoSanguineo;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 10))
      return paramFamiliar.grupoSanguineo;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 10, paramFamiliar.grupoSanguineo);
  }

  private static final void jdoSetgrupoSanguineo(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.grupoSanguineo = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.grupoSanguineo = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 10, paramFamiliar.grupoSanguineo, paramString);
  }

  private static final long jdoGetidFamiliar(Familiar paramFamiliar)
  {
    return paramFamiliar.idFamiliar;
  }

  private static final void jdoSetidFamiliar(Familiar paramFamiliar, long paramLong)
  {
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.idFamiliar = paramLong;
      return;
    }
    localStateManager.setLongField(paramFamiliar, jdoInheritedFieldCount + 11, paramFamiliar.idFamiliar, paramLong);
  }

  private static final int jdoGetidSitp(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.idSitp;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.idSitp;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 12))
      return paramFamiliar.idSitp;
    return localStateManager.getIntField(paramFamiliar, jdoInheritedFieldCount + 12, paramFamiliar.idSitp);
  }

  private static final void jdoSetidSitp(Familiar paramFamiliar, int paramInt)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramFamiliar, jdoInheritedFieldCount + 12, paramFamiliar.idSitp, paramInt);
  }

  private static final String jdoGetmismoOrganismo(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.mismoOrganismo;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.mismoOrganismo;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 13))
      return paramFamiliar.mismoOrganismo;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 13, paramFamiliar.mismoOrganismo);
  }

  private static final void jdoSetmismoOrganismo(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.mismoOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.mismoOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 13, paramFamiliar.mismoOrganismo, paramString);
  }

  private static final String jdoGetninoExcepcional(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.ninoExcepcional;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.ninoExcepcional;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 14))
      return paramFamiliar.ninoExcepcional;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 14, paramFamiliar.ninoExcepcional);
  }

  private static final void jdoSetninoExcepcional(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.ninoExcepcional = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.ninoExcepcional = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 14, paramFamiliar.ninoExcepcional, paramString);
  }

  private static final String jdoGetnivelEducativo(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.nivelEducativo;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.nivelEducativo;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 15))
      return paramFamiliar.nivelEducativo;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 15, paramFamiliar.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 15, paramFamiliar.nivelEducativo, paramString);
  }

  private static final String jdoGetparentesco(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.parentesco;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.parentesco;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 16))
      return paramFamiliar.parentesco;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 16, paramFamiliar.parentesco);
  }

  private static final void jdoSetparentesco(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.parentesco = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.parentesco = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 16, paramFamiliar.parentesco, paramString);
  }

  private static final Personal jdoGetpersonal(Familiar paramFamiliar)
  {
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.personal;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 17))
      return paramFamiliar.personal;
    return (Personal)localStateManager.getObjectField(paramFamiliar, jdoInheritedFieldCount + 17, paramFamiliar.personal);
  }

  private static final void jdoSetpersonal(Familiar paramFamiliar, Personal paramPersonal)
  {
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramFamiliar, jdoInheritedFieldCount + 17, paramFamiliar.personal, paramPersonal);
  }

  private static final String jdoGetprimerApellido(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.primerApellido;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.primerApellido;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 18))
      return paramFamiliar.primerApellido;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 18, paramFamiliar.primerApellido);
  }

  private static final void jdoSetprimerApellido(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 18, paramFamiliar.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.primerNombre;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.primerNombre;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 19))
      return paramFamiliar.primerNombre;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 19, paramFamiliar.primerNombre);
  }

  private static final void jdoSetprimerNombre(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 19, paramFamiliar.primerNombre, paramString);
  }

  private static final String jdoGetpromedioNota(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.promedioNota;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.promedioNota;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 20))
      return paramFamiliar.promedioNota;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 20, paramFamiliar.promedioNota);
  }

  private static final void jdoSetpromedioNota(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.promedioNota = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.promedioNota = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 20, paramFamiliar.promedioNota, paramString);
  }

  private static final String jdoGetsegundoApellido(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.segundoApellido;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.segundoApellido;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 21))
      return paramFamiliar.segundoApellido;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 21, paramFamiliar.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 21, paramFamiliar.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.segundoNombre;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.segundoNombre;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 22))
      return paramFamiliar.segundoNombre;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 22, paramFamiliar.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 22, paramFamiliar.segundoNombre, paramString);
  }

  private static final String jdoGetsexo(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.sexo;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.sexo;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 23))
      return paramFamiliar.sexo;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 23, paramFamiliar.sexo);
  }

  private static final void jdoSetsexo(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.sexo = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.sexo = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 23, paramFamiliar.sexo, paramString);
  }

  private static final String jdoGettallaFranela(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.tallaFranela;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.tallaFranela;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 24))
      return paramFamiliar.tallaFranela;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 24, paramFamiliar.tallaFranela);
  }

  private static final void jdoSettallaFranela(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.tallaFranela = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.tallaFranela = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 24, paramFamiliar.tallaFranela, paramString);
  }

  private static final String jdoGettallaGorra(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.tallaGorra;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.tallaGorra;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 25))
      return paramFamiliar.tallaGorra;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 25, paramFamiliar.tallaGorra);
  }

  private static final void jdoSettallaGorra(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.tallaGorra = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.tallaGorra = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 25, paramFamiliar.tallaGorra, paramString);
  }

  private static final String jdoGettallaPantalon(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.tallaPantalon;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.tallaPantalon;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 26))
      return paramFamiliar.tallaPantalon;
    return localStateManager.getStringField(paramFamiliar, jdoInheritedFieldCount + 26, paramFamiliar.tallaPantalon);
  }

  private static final void jdoSettallaPantalon(Familiar paramFamiliar, String paramString)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.tallaPantalon = paramString;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.tallaPantalon = paramString;
      return;
    }
    localStateManager.setStringField(paramFamiliar, jdoInheritedFieldCount + 26, paramFamiliar.tallaPantalon, paramString);
  }

  private static final Date jdoGettiempoSitp(Familiar paramFamiliar)
  {
    if (paramFamiliar.jdoFlags <= 0)
      return paramFamiliar.tiempoSitp;
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramFamiliar.tiempoSitp;
    if (localStateManager.isLoaded(paramFamiliar, jdoInheritedFieldCount + 27))
      return paramFamiliar.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramFamiliar, jdoInheritedFieldCount + 27, paramFamiliar.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Familiar paramFamiliar, Date paramDate)
  {
    if (paramFamiliar.jdoFlags == 0)
    {
      paramFamiliar.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramFamiliar.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramFamiliar, jdoInheritedFieldCount + 27, paramFamiliar.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}