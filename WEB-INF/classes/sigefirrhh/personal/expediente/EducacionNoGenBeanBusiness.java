package sigefirrhh.personal.expediente;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class EducacionNoGenBeanBusiness extends EducacionBeanBusiness
  implements Serializable
{
  public Collection findByClasificacionDocentePostgrado(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String nivel1 = "E";
    String nivel2 = "M";
    String filter = "personal.idPersonal == pIdPersonal && (nivelEducativo.codNivelEducativo == pNivel1 || nivelEducativo.codNivelEducativo == pNivel2)";

    Query query = pm.newQuery(Educacion.class, filter);

    query.declareParameters("long pIdPersonal, String pNivel1, String pNivel2");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pNivel1", nivel1);
    parameters.put("pNivel2", nivel2);

    query.setOrdering("anioInicio ascending");

    Collection colEducacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEducacion);

    return colEducacion;
  }

  public Collection findByClasificacionDocenteDoctorado(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String nivel = "C";
    String filter = "personal.idPersonal == pIdPersonal && nivelEducativo.codNivelEducativo == pNivel";

    Query query = pm.newQuery(Educacion.class, filter);

    query.declareParameters("long pIdPersonal, String pNivel");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pNivel", nivel);

    query.setOrdering("anioInicio ascending");

    Collection colEducacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEducacion);

    return colEducacion;
  }
}