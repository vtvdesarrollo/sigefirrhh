package sigefirrhh.personal.expediente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.TipoIdioma;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class IdiomaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(IdiomaForm.class.getName());
  private Idioma idioma;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private PersonalFacade personalFacade = new PersonalFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colTipoIdioma;
  private Collection colPersonal;
  private String selectTipoIdioma;
  private String selectPersonal;
  private Object stateScrollPersonal = null;
  private Object stateResultPersonal = null;

  private Object stateScrollIdiomaByPersonal = null;
  private Object stateResultIdiomaByPersonal = null;

  public String getSelectTipoIdioma()
  {
    return this.selectTipoIdioma;
  }
  public void setSelectTipoIdioma(String valTipoIdioma) {
    Iterator iterator = this.colTipoIdioma.iterator();
    TipoIdioma tipoIdioma = null;
    this.idioma.setTipoIdioma(null);
    while (iterator.hasNext()) {
      tipoIdioma = (TipoIdioma)iterator.next();
      if (String.valueOf(tipoIdioma.getIdTipoIdioma()).equals(
        valTipoIdioma)) {
        this.idioma.setTipoIdioma(
          tipoIdioma);
      }
    }
    this.selectTipoIdioma = valTipoIdioma;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.idioma.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.idioma.setPersonal(
          personal);
      }
    }
    this.selectPersonal = valPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public Idioma getIdioma() {
    if (this.idioma == null) {
      this.idioma = new Idioma();
    }
    return this.idioma;
  }

  public IdiomaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColTipoIdioma()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoIdioma.iterator();
    TipoIdioma tipoIdioma = null;
    while (iterator.hasNext()) {
      tipoIdioma = (TipoIdioma)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoIdioma.getIdTipoIdioma()), 
        tipoIdioma.toString()));
    }
    return col;
  }

  public Collection getListHabla() {
    Collection col = new ArrayList();

    Iterator iterEntry = Idioma.LISTA_IDIOMA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListLee() {
    Collection col = new ArrayList();

    Iterator iterEntry = Idioma.LISTA_IDIOMA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEscribe() {
    Collection col = new ArrayList();

    Iterator iterEntry = Idioma.LISTA_IDIOMA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListExamenSuficiencia()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Idioma.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListExamenAcademico()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Idioma.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colTipoIdioma = 
        this.personalFacade.findAllTipoIdioma();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findIdiomaByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.expedienteFacade.findIdiomaByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectIdioma()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoIdioma = null;
    this.selectPersonal = null;

    long idIdioma = 
      Long.parseLong((String)requestParameterMap.get("idIdioma"));
    try
    {
      this.idioma = 
        this.expedienteFacade.findIdiomaById(
        idIdioma);

      if (this.idioma.getTipoIdioma() != null) {
        this.selectTipoIdioma = 
          String.valueOf(this.idioma.getTipoIdioma().getIdTipoIdioma());
      }
      if (this.idioma.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.idioma.getPersonal().getIdPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    if (this.idioma.getExamenSuficiencia().equals("N")) {
      this.idioma.setEntidadSuficiencia(null);
      this.idioma.setFechaSuficiencia(null);
    }
    if (this.idioma.getExamenAcademico().equals("N")) {
      this.idioma.setEntidadAcademica(null);
      this.idioma.setFechaAcademica(null);
    }

    if ((this.idioma.getFechaSuficiencia() != null) && 
      (this.idioma.getFechaSuficiencia().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Examen Suficiencia no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.idioma.getFechaAcademica() != null) && 
      (this.idioma.getFechaAcademica().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La  Fecha Examen Académico no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.idioma.getTiempoSitp() != null) && 
      (this.idioma.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.idioma.setPersonal(
          this.personal);
        this.expedienteFacade.addIdioma(
          this.idioma);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.idioma, this.personal);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.expedienteFacade.updateIdioma(
          this.idioma);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.idioma, this.personal);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Registro ya existe\n", ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.expedienteFacade.deleteIdioma(
        this.idioma);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.idioma, this.personal);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));

      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    abortUpdate();
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectTipoIdioma = null;

    this.selectPersonal = null;

    this.idioma = new Idioma();

    this.idioma.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.idioma.setIdIdioma(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Idioma"));

    return null;
  }

  public boolean isShowEntidadSuficienciaAux() {
    try {
      return this.idioma.getExamenSuficiencia().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFechaSuficienciaAux() {
    try {
      return this.idioma.getExamenSuficiencia().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowEntidadAcademicaAux() {
    try {
      return this.idioma.getExamenAcademico().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFechaAcademicaAux() {
    try {
      return this.idioma.getExamenAcademico().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.idioma = new Idioma();
    return "cancel";
  }
  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.idioma = new Idioma();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }
}