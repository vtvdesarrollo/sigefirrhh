package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class AfiliacionPK
  implements Serializable
{
  public long idAfiliacion;

  public AfiliacionPK()
  {
  }

  public AfiliacionPK(long idAfiliacion)
  {
    this.idAfiliacion = idAfiliacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AfiliacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AfiliacionPK thatPK)
  {
    return 
      this.idAfiliacion == thatPK.idAfiliacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAfiliacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAfiliacion);
  }
}