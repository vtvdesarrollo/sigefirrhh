package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.RelacionRecaudo;
import sigefirrhh.base.personal.RelacionRecaudoBeanBusiness;

public class AusenciaRecaudoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AusenciaRecaudo ausenciaRecaudoNew = 
      (AusenciaRecaudo)BeanUtils.cloneBean(
      ausenciaRecaudo);

    RelacionRecaudoBeanBusiness relacionRecaudoBeanBusiness = new RelacionRecaudoBeanBusiness();

    if (ausenciaRecaudoNew.getRelacionRecaudo() != null) {
      ausenciaRecaudoNew.setRelacionRecaudo(
        relacionRecaudoBeanBusiness.findRelacionRecaudoById(
        ausenciaRecaudoNew.getRelacionRecaudo().getIdRelacionRecaudo()));
    }

    AusenciaBeanBusiness ausenciaBeanBusiness = new AusenciaBeanBusiness();

    if (ausenciaRecaudoNew.getAusencia() != null) {
      ausenciaRecaudoNew.setAusencia(
        ausenciaBeanBusiness.findAusenciaById(
        ausenciaRecaudoNew.getAusencia().getIdAusencia()));
    }
    pm.makePersistent(ausenciaRecaudoNew);
  }

  public void updateAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo) throws Exception
  {
    AusenciaRecaudo ausenciaRecaudoModify = 
      findAusenciaRecaudoById(ausenciaRecaudo.getIdAusenciaRecaudo());

    RelacionRecaudoBeanBusiness relacionRecaudoBeanBusiness = new RelacionRecaudoBeanBusiness();

    if (ausenciaRecaudo.getRelacionRecaudo() != null) {
      ausenciaRecaudo.setRelacionRecaudo(
        relacionRecaudoBeanBusiness.findRelacionRecaudoById(
        ausenciaRecaudo.getRelacionRecaudo().getIdRelacionRecaudo()));
    }

    AusenciaBeanBusiness ausenciaBeanBusiness = new AusenciaBeanBusiness();

    if (ausenciaRecaudo.getAusencia() != null) {
      ausenciaRecaudo.setAusencia(
        ausenciaBeanBusiness.findAusenciaById(
        ausenciaRecaudo.getAusencia().getIdAusencia()));
    }

    BeanUtils.copyProperties(ausenciaRecaudoModify, ausenciaRecaudo);
  }

  public void deleteAusenciaRecaudo(AusenciaRecaudo ausenciaRecaudo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AusenciaRecaudo ausenciaRecaudoDelete = 
      findAusenciaRecaudoById(ausenciaRecaudo.getIdAusenciaRecaudo());
    pm.deletePersistent(ausenciaRecaudoDelete);
  }

  public AusenciaRecaudo findAusenciaRecaudoById(long idAusenciaRecaudo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAusenciaRecaudo == pIdAusenciaRecaudo";
    Query query = pm.newQuery(AusenciaRecaudo.class, filter);

    query.declareParameters("long pIdAusenciaRecaudo");

    parameters.put("pIdAusenciaRecaudo", new Long(idAusenciaRecaudo));

    Collection colAusenciaRecaudo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAusenciaRecaudo.iterator();
    return (AusenciaRecaudo)iterator.next();
  }

  public Collection findAusenciaRecaudoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent ausenciaRecaudoExtent = pm.getExtent(
      AusenciaRecaudo.class, true);
    Query query = pm.newQuery(ausenciaRecaudoExtent);
    query.setOrdering("Ausencia.tipoAusencia ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}