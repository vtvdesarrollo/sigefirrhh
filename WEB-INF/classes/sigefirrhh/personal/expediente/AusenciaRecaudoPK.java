package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class AusenciaRecaudoPK
  implements Serializable
{
  public long idAusenciaRecaudo;

  public AusenciaRecaudoPK()
  {
  }

  public AusenciaRecaudoPK(long idAusenciaRecaudo)
  {
    this.idAusenciaRecaudo = idAusenciaRecaudo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AusenciaRecaudoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AusenciaRecaudoPK thatPK)
  {
    return 
      this.idAusenciaRecaudo == thatPK.idAusenciaRecaudo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAusenciaRecaudo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAusenciaRecaudo);
  }
}