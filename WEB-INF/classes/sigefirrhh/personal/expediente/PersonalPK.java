package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class PersonalPK
  implements Serializable
{
  public long idPersonal;

  public PersonalPK()
  {
  }

  public PersonalPK(long idPersonal)
  {
    this.idPersonal = idPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PersonalPK thatPK)
  {
    return 
      this.idPersonal == thatPK.idPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPersonal);
  }
}