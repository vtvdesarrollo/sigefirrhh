package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SuplenciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSuplencia(Suplencia suplencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Suplencia suplenciaNew = 
      (Suplencia)BeanUtils.cloneBean(
      suplencia);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (suplenciaNew.getPersonal() != null) {
      suplenciaNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        suplenciaNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(suplenciaNew);
  }

  public void updateSuplencia(Suplencia suplencia) throws Exception
  {
    Suplencia suplenciaModify = 
      findSuplenciaById(suplencia.getIdSuplencia());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (suplencia.getPersonal() != null) {
      suplencia.setPersonal(
        personalBeanBusiness.findPersonalById(
        suplencia.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(suplenciaModify, suplencia);
  }

  public void deleteSuplencia(Suplencia suplencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Suplencia suplenciaDelete = 
      findSuplenciaById(suplencia.getIdSuplencia());
    pm.deletePersistent(suplenciaDelete);
  }

  public Suplencia findSuplenciaById(long idSuplencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSuplencia == pIdSuplencia";
    Query query = pm.newQuery(Suplencia.class, filter);

    query.declareParameters("long pIdSuplencia");

    parameters.put("pIdSuplencia", new Long(idSuplencia));

    Collection colSuplencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSuplencia.iterator();
    return (Suplencia)iterator.next();
  }

  public Collection findSuplenciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent suplenciaExtent = pm.getExtent(
      Suplencia.class, true);
    Query query = pm.newQuery(suplenciaExtent);
    query.setOrdering("fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Suplencia.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaInicio ascending");

    Collection colSuplencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSuplencia);

    return colSuplencia;
  }
}