package sigefirrhh.personal.expediente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoContrato;
import sigefirrhh.base.definiciones.TipoContratoBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ContratoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addContrato(Contrato contrato)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Contrato contratoNew = 
      (Contrato)BeanUtils.cloneBean(
      contrato);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (contratoNew.getTipoPersonal() != null) {
      contratoNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        contratoNew.getTipoPersonal().getIdTipoPersonal()));
    }

    TipoContratoBeanBusiness tipoContratoBeanBusiness = new TipoContratoBeanBusiness();

    if (contratoNew.getTipoContrato() != null) {
      contratoNew.setTipoContrato(
        tipoContratoBeanBusiness.findTipoContratoById(
        contratoNew.getTipoContrato().getIdTipoContrato()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (contratoNew.getPersonal() != null) {
      contratoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        contratoNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(contratoNew);
  }

  public void updateContrato(Contrato contrato) throws Exception
  {
    Contrato contratoModify = 
      findContratoById(contrato.getIdContrato());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (contrato.getTipoPersonal() != null) {
      contrato.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        contrato.getTipoPersonal().getIdTipoPersonal()));
    }

    TipoContratoBeanBusiness tipoContratoBeanBusiness = new TipoContratoBeanBusiness();

    if (contrato.getTipoContrato() != null) {
      contrato.setTipoContrato(
        tipoContratoBeanBusiness.findTipoContratoById(
        contrato.getTipoContrato().getIdTipoContrato()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (contrato.getPersonal() != null) {
      contrato.setPersonal(
        personalBeanBusiness.findPersonalById(
        contrato.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(contratoModify, contrato);
  }

  public void deleteContrato(Contrato contrato) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Contrato contratoDelete = 
      findContratoById(contrato.getIdContrato());
    pm.deletePersistent(contratoDelete);
  }

  public Contrato findContratoById(long idContrato) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idContrato == pIdContrato";
    Query query = pm.newQuery(Contrato.class, filter);

    query.declareParameters("long pIdContrato");

    parameters.put("pIdContrato", new Long(idContrato));

    Collection colContrato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colContrato.iterator();
    return (Contrato)iterator.next();
  }

  public Collection findContratoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent contratoExtent = pm.getExtent(
      Contrato.class, true);
    Query query = pm.newQuery(contratoExtent);
    query.setOrdering("fechaInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Contrato.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("fechaInicio ascending");

    Collection colContrato = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colContrato);

    return colContrato;
  }
}