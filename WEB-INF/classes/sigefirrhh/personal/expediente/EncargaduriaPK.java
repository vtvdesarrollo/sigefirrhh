package sigefirrhh.personal.expediente;

import java.io.Serializable;

public class EncargaduriaPK
  implements Serializable
{
  public long idEncargaduria;

  public EncargaduriaPK()
  {
  }

  public EncargaduriaPK(long idEncargaduria)
  {
    this.idEncargaduria = idEncargaduria;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EncargaduriaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EncargaduriaPK thatPK)
  {
    return 
      this.idEncargaduria == thatPK.idEncargaduria;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEncargaduria)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEncargaduria);
  }
}