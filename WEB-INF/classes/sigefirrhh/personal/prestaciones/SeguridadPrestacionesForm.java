package sigefirrhh.personal.prestaciones;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class SeguridadPrestacionesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeguridadPrestacionesForm.class.getName());
  private SeguridadPrestaciones seguridadPrestaciones;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private PrestacionesFacade prestacionesFacade = new PrestacionesFacade();
  private boolean showSeguridadPrestacionesByTipoPersonal;
  private boolean showSeguridadPrestacionesByAnio;
  private String findSelectTipoPersonal;
  private int findAnio;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private String selectTipoPersonal;
  private Object stateResultSeguridadPrestacionesByTipoPersonal = null;

  private Object stateResultSeguridadPrestacionesByAnio = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.seguridadPrestaciones.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.seguridadPrestaciones.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public SeguridadPrestaciones getSeguridadPrestaciones() {
    if (this.seguridadPrestaciones == null) {
      this.seguridadPrestaciones = getNewSeguridadPrestaciones();
    }
    return this.seguridadPrestaciones;
  }

  public SeguridadPrestacionesForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSeguridadPrestacionesByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.prestacionesFacade.findSeguridadPrestacionesByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showSeguridadPrestacionesByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadPrestacionesByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findAnio = 0;

    return null;
  }

  public String findSeguridadPrestacionesByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.prestacionesFacade.findSeguridadPrestacionesByAnio(this.findAnio);
      this.showSeguridadPrestacionesByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadPrestacionesByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowSeguridadPrestacionesByTipoPersonal() {
    return this.showSeguridadPrestacionesByTipoPersonal;
  }
  public boolean isShowSeguridadPrestacionesByAnio() {
    return this.showSeguridadPrestacionesByAnio;
  }

  public String selectSeguridadPrestaciones()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;

    long idSeguridadPrestaciones = 
      Long.parseLong((String)requestParameterMap.get("idSeguridadPrestaciones"));
    try
    {
      this.seguridadPrestaciones = 
        this.prestacionesFacade.findSeguridadPrestacionesById(
        idSeguridadPrestaciones);
      if (this.seguridadPrestaciones.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.seguridadPrestaciones.getTipoPersonal().getIdTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.seguridadPrestaciones = null;
    this.showSeguridadPrestacionesByTipoPersonal = false;
    this.showSeguridadPrestacionesByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.seguridadPrestaciones.getFechaProceso() != null) && 
      (this.seguridadPrestaciones.getFechaProceso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha de Proceso no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }
    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.prestacionesFacade.addSeguridadPrestaciones(
          this.seguridadPrestaciones);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.prestacionesFacade.updateSeguridadPrestaciones(
          this.seguridadPrestaciones);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.prestacionesFacade.deleteSeguridadPrestaciones(
        this.seguridadPrestaciones);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.seguridadPrestaciones = getNewSeguridadPrestaciones();

    this.selectTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.seguridadPrestaciones.setIdSeguridadPrestaciones(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.prestaciones.SeguridadPrestaciones"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.seguridadPrestaciones = getNewSeguridadPrestaciones();
    return "cancel";
  }

  private SeguridadPrestaciones getNewSeguridadPrestaciones()
  {
    SeguridadPrestaciones sgpNew = new SeguridadPrestaciones();
    sgpNew.setAnio(0);
    sgpNew.setFechaProceso(null);
    sgpNew.setIdSeguridadPrestaciones(0L);
    sgpNew.setMes(0);
    sgpNew.setTipoPersonal(null);
    sgpNew.setUsuario(this.login.getUsuario());
    return sgpNew;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}