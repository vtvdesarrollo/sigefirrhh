package sigefirrhh.personal.prestaciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class Fideicomiso
  implements Serializable, PersistenceCapable
{
  private long idFideicomiso;
  private Date fecha;
  private int anio;
  private int mes;
  private double montoFideicomiso;
  private Banco banco;
  private TipoPersonal tipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "banco", "fecha", "idFideicomiso", "mes", "montoFideicomiso", "tipoPersonal", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.Banco"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 24, 21, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Fideicomiso()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetmontoFideicomiso(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoFideicomiso(this));

    return jdoGetanio(this) + " - " + jdoGetmes(this) + " - Monto: " + a;
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public Banco getBanco() {
    return jdoGetbanco(this);
  }
  public void setBanco(Banco banco) {
    jdoSetbanco(this, banco);
  }
  public Date getFecha() {
    return jdoGetfecha(this);
  }
  public void setFecha(Date fecha) {
    jdoSetfecha(this, fecha);
  }
  public long getIdFideicomiso() {
    return jdoGetidFideicomiso(this);
  }
  public void setIdFideicomiso(long idFideicomiso) {
    jdoSetidFideicomiso(this, idFideicomiso);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public double getMontoFideicomiso() {
    return jdoGetmontoFideicomiso(this);
  }
  public void setMontoFideicomiso(double montoFideicomiso) {
    jdoSetmontoFideicomiso(this, montoFideicomiso);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.prestaciones.Fideicomiso"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Fideicomiso());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Fideicomiso localFideicomiso = new Fideicomiso();
    localFideicomiso.jdoFlags = 1;
    localFideicomiso.jdoStateManager = paramStateManager;
    return localFideicomiso;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Fideicomiso localFideicomiso = new Fideicomiso();
    localFideicomiso.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFideicomiso.jdoFlags = 1;
    localFideicomiso.jdoStateManager = paramStateManager;
    return localFideicomiso;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.banco);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFideicomiso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoFideicomiso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.banco = ((Banco)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFideicomiso = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoFideicomiso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Fideicomiso paramFideicomiso, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramFideicomiso.anio;
      return;
    case 1:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.banco = paramFideicomiso.banco;
      return;
    case 2:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramFideicomiso.fecha;
      return;
    case 3:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.idFideicomiso = paramFideicomiso.idFideicomiso;
      return;
    case 4:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramFideicomiso.mes;
      return;
    case 5:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.montoFideicomiso = paramFideicomiso.montoFideicomiso;
      return;
    case 6:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramFideicomiso.tipoPersonal;
      return;
    case 7:
      if (paramFideicomiso == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramFideicomiso.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Fideicomiso))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Fideicomiso localFideicomiso = (Fideicomiso)paramObject;
    if (localFideicomiso.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFideicomiso, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FideicomisoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FideicomisoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FideicomisoPK))
      throw new IllegalArgumentException("arg1");
    FideicomisoPK localFideicomisoPK = (FideicomisoPK)paramObject;
    localFideicomisoPK.idFideicomiso = this.idFideicomiso;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FideicomisoPK))
      throw new IllegalArgumentException("arg1");
    FideicomisoPK localFideicomisoPK = (FideicomisoPK)paramObject;
    this.idFideicomiso = localFideicomisoPK.idFideicomiso;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FideicomisoPK))
      throw new IllegalArgumentException("arg2");
    FideicomisoPK localFideicomisoPK = (FideicomisoPK)paramObject;
    localFideicomisoPK.idFideicomiso = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FideicomisoPK))
      throw new IllegalArgumentException("arg2");
    FideicomisoPK localFideicomisoPK = (FideicomisoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localFideicomisoPK.idFideicomiso);
  }

  private static final int jdoGetanio(Fideicomiso paramFideicomiso)
  {
    if (paramFideicomiso.jdoFlags <= 0)
      return paramFideicomiso.anio;
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
      return paramFideicomiso.anio;
    if (localStateManager.isLoaded(paramFideicomiso, jdoInheritedFieldCount + 0))
      return paramFideicomiso.anio;
    return localStateManager.getIntField(paramFideicomiso, jdoInheritedFieldCount + 0, paramFideicomiso.anio);
  }

  private static final void jdoSetanio(Fideicomiso paramFideicomiso, int paramInt)
  {
    if (paramFideicomiso.jdoFlags == 0)
    {
      paramFideicomiso.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramFideicomiso, jdoInheritedFieldCount + 0, paramFideicomiso.anio, paramInt);
  }

  private static final Banco jdoGetbanco(Fideicomiso paramFideicomiso)
  {
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
      return paramFideicomiso.banco;
    if (localStateManager.isLoaded(paramFideicomiso, jdoInheritedFieldCount + 1))
      return paramFideicomiso.banco;
    return (Banco)localStateManager.getObjectField(paramFideicomiso, jdoInheritedFieldCount + 1, paramFideicomiso.banco);
  }

  private static final void jdoSetbanco(Fideicomiso paramFideicomiso, Banco paramBanco)
  {
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.banco = paramBanco;
      return;
    }
    localStateManager.setObjectField(paramFideicomiso, jdoInheritedFieldCount + 1, paramFideicomiso.banco, paramBanco);
  }

  private static final Date jdoGetfecha(Fideicomiso paramFideicomiso)
  {
    if (paramFideicomiso.jdoFlags <= 0)
      return paramFideicomiso.fecha;
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
      return paramFideicomiso.fecha;
    if (localStateManager.isLoaded(paramFideicomiso, jdoInheritedFieldCount + 2))
      return paramFideicomiso.fecha;
    return (Date)localStateManager.getObjectField(paramFideicomiso, jdoInheritedFieldCount + 2, paramFideicomiso.fecha);
  }

  private static final void jdoSetfecha(Fideicomiso paramFideicomiso, Date paramDate)
  {
    if (paramFideicomiso.jdoFlags == 0)
    {
      paramFideicomiso.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramFideicomiso, jdoInheritedFieldCount + 2, paramFideicomiso.fecha, paramDate);
  }

  private static final long jdoGetidFideicomiso(Fideicomiso paramFideicomiso)
  {
    return paramFideicomiso.idFideicomiso;
  }

  private static final void jdoSetidFideicomiso(Fideicomiso paramFideicomiso, long paramLong)
  {
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.idFideicomiso = paramLong;
      return;
    }
    localStateManager.setLongField(paramFideicomiso, jdoInheritedFieldCount + 3, paramFideicomiso.idFideicomiso, paramLong);
  }

  private static final int jdoGetmes(Fideicomiso paramFideicomiso)
  {
    if (paramFideicomiso.jdoFlags <= 0)
      return paramFideicomiso.mes;
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
      return paramFideicomiso.mes;
    if (localStateManager.isLoaded(paramFideicomiso, jdoInheritedFieldCount + 4))
      return paramFideicomiso.mes;
    return localStateManager.getIntField(paramFideicomiso, jdoInheritedFieldCount + 4, paramFideicomiso.mes);
  }

  private static final void jdoSetmes(Fideicomiso paramFideicomiso, int paramInt)
  {
    if (paramFideicomiso.jdoFlags == 0)
    {
      paramFideicomiso.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramFideicomiso, jdoInheritedFieldCount + 4, paramFideicomiso.mes, paramInt);
  }

  private static final double jdoGetmontoFideicomiso(Fideicomiso paramFideicomiso)
  {
    if (paramFideicomiso.jdoFlags <= 0)
      return paramFideicomiso.montoFideicomiso;
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
      return paramFideicomiso.montoFideicomiso;
    if (localStateManager.isLoaded(paramFideicomiso, jdoInheritedFieldCount + 5))
      return paramFideicomiso.montoFideicomiso;
    return localStateManager.getDoubleField(paramFideicomiso, jdoInheritedFieldCount + 5, paramFideicomiso.montoFideicomiso);
  }

  private static final void jdoSetmontoFideicomiso(Fideicomiso paramFideicomiso, double paramDouble)
  {
    if (paramFideicomiso.jdoFlags == 0)
    {
      paramFideicomiso.montoFideicomiso = paramDouble;
      return;
    }
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.montoFideicomiso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramFideicomiso, jdoInheritedFieldCount + 5, paramFideicomiso.montoFideicomiso, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(Fideicomiso paramFideicomiso)
  {
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
      return paramFideicomiso.tipoPersonal;
    if (localStateManager.isLoaded(paramFideicomiso, jdoInheritedFieldCount + 6))
      return paramFideicomiso.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramFideicomiso, jdoInheritedFieldCount + 6, paramFideicomiso.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Fideicomiso paramFideicomiso, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramFideicomiso, jdoInheritedFieldCount + 6, paramFideicomiso.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(Fideicomiso paramFideicomiso)
  {
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
      return paramFideicomiso.trabajador;
    if (localStateManager.isLoaded(paramFideicomiso, jdoInheritedFieldCount + 7))
      return paramFideicomiso.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramFideicomiso, jdoInheritedFieldCount + 7, paramFideicomiso.trabajador);
  }

  private static final void jdoSettrabajador(Fideicomiso paramFideicomiso, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramFideicomiso.jdoStateManager;
    if (localStateManager == null)
    {
      paramFideicomiso.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramFideicomiso, jdoInheritedFieldCount + 7, paramFideicomiso.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}