package sigefirrhh.personal.prestaciones;

import java.io.Serializable;

public class PrestacionesOnaprePK
  implements Serializable
{
  public long idPrestacionesOnapre;

  public PrestacionesOnaprePK()
  {
  }

  public PrestacionesOnaprePK(long idPrestacionesOnapre)
  {
    this.idPrestacionesOnapre = idPrestacionesOnapre;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrestacionesOnaprePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrestacionesOnaprePK thatPK)
  {
    return 
      this.idPrestacionesOnapre == thatPK.idPrestacionesOnapre;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrestacionesOnapre)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrestacionesOnapre);
  }
}