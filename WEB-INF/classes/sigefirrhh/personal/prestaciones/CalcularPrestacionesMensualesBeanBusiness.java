package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesBusiness;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.ParametroVarios;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.VacacionesPorAnio;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;
import sigefirrhh.personal.historico.HistoricoNoGenBusiness;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;

public class CalcularPrestacionesMensualesBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(CalcularPrestacionesMensualesBeanBusiness.class.getName());

  private DefinicionesBusiness definicionesBusiness = new DefinicionesBusiness();
  private TrabajadorNoGenBusiness trabajadorNoGenBusiness = new TrabajadorNoGenBusiness();
  private HistoricoNoGenBusiness historicoNoGenBusiness = new HistoricoNoGenBusiness();
  private DefinicionesNoGenBusiness definicionesNoGenBusiness = new DefinicionesNoGenBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean calcular(long idTipoPersonal, int mes, int anio, long idSeguridadPrestaciones, String proceso, String usuario)
    throws Exception
  {
    this.log.error("CalcularPrestacionesMensualesBeanBusiness/calcular()-> idTipoPersonal= " + idTipoPersonal);
    this.log.error("CalcularPrestacionesMensualesBeanBusiness/calcular()-> mes= " + mes);
    this.log.error("CalcularPrestacionesMensualesBeanBusiness/calcular()-> anio= " + anio);
    this.log.error("CalcularPrestacionesMensualesBeanBusiness/calcular()-> proceso= " + proceso);

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    ParametroVarios parametroVarios = new ParametroVarios();
    Collection colParametroVarios = this.definicionesBusiness.findParametroVariosByTipoPersonal(idTipoPersonal);
    Iterator iteratorParametroVarios = colParametroVarios.iterator();
    parametroVarios = (ParametroVarios)iteratorParametroVarios.next();

    TipoPersonal tipoPersonal = new TipoPersonal();
    tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    double montoA = 0.0D;
    double montoB = 0.0D;
    double montoC = 0.0D;
    double montoD = 0.0D;
    double montoE = 0.0D;
    double totalMontoA = 0.0D;
    double totalMontoB = 0.0D;
    double totalMontoC = 0.0D;
    double totalMontoE = 0.0D;

    double baseMensual = 0.0D;
    double baseAdicional = 0.0D;

    double montoPrestaciones = 0.0D;
    double montoAdicional = 0.0D;
    int diasAdicionales = 0;
    int aniosCumple = 0;

    double sueldoBasicoOnapre = 0.0D;
    double compensacionOnapre = 0.0D;
    double primasOnapre = 0.0D;
    double bonoFinAnioOnapre = 0.0D;
    double bonoVacacionalOnapre = 0.0D;
    double otrosOnapre = 0.0D;

    int dias_laborados = 0;

    ResultSet rsSemana = null;
    PreparedStatement stSemana = null;
    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;
    ResultSet rsHistorico = null;
    PreparedStatement stHistorico = null;
    ResultSet rsDiasAdicionalesPersonal = null;
    PreparedStatement stDiasAdicionalesPersonal = null;
    ResultSet rsTrabajadorPersonal = null;
    PreparedStatement stTrabajadorPersonal = null;
    Statement stCerrar = null;
    Statement stActualizarFecha = null;
    Statement stBorrar = null;
    ResultSet rsConceptoAlicuotaBfa = null;
    ResultSet rsConceptoPrestaciones = null;
    PreparedStatement stConceptoPrestaciones = null;
    PreparedStatement stConceptoAlicuotaBfa = null;
    ResultSet rsConceptoAlicuotaBvac = null;
    PreparedStatement stConceptoAlicuotaBvac = null;
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;
    Statement stInsert = null;

    Connection connection = null;

    double fraccion = 0.0D;
    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      stInsert = connection.createStatement();

      if (proceso.equals("2"))
      {
        stCerrar = connection.createStatement();
        sql = new StringBuffer();

        sql.append("update seguridadprestaciones set mes = " + mes + " , anio = " + anio + " where id_tipo_personal = " + idTipoPersonal);
        stCerrar.addBatch(sql.toString());
        stCerrar.executeBatch();
        connection.commit();
        return true;
      }

      stActualizarFecha = connection.createStatement();
      sql = new StringBuffer();
      sql.append("update trabajador set fecha_prestaciones = '1997-06-19', anio_prestaciones = 1997, mes_prestaciones = 6, dia_prestaciones = 19 where fecha_prestaciones < '1997-06-19' and id_tipo_personal = " + idTipoPersonal);
      stActualizarFecha.addBatch(sql.toString());
      stActualizarFecha.executeBatch();

      sql = new StringBuffer();
      sql.append("select cf.monto, cf.unidades, fp.cod_frecuencia_pago ");
      sql.append(" from conceptofijo cf, frecuenciatipopersonal ftp, frecuenciapago fp ");
      sql.append(" where cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append("  and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
      sql.append("  and cf.id_concepto_tipo_personal = ? ");
      sql.append(" and id_trabajador = ? ");

      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        sql.append("select sum(hq.monto_asigna) as monto, max(fp.cod_frecuencia_pago) as cod_frecuencia_pago, ");
        sql.append("max(hq.origen) as origen ");
        sql.append(" from historicoquincena hq, frecuenciatipopersonal ftp, frecuenciapago fp  ");
        sql.append(" where hq.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
        sql.append(" and hq.id_concepto_tipo_personal = ?");
        sql.append(" and id_trabajador = ?");
        sql.append(" and anio = ?");
        sql.append(" and mes = ?");
        sql.append(" group by id_trabajador");
      }
      else {
        sql.append("select sum(hs.monto_asigna) as monto, max(fp.cod_frecuencia_pago) as cod_frecuencia_pago, ");
        sql.append("max(hs.origen) as origen ");
        sql.append(" from historicosemana hs, frecuenciatipopersonal ftp, frecuenciapago fp  ");
        sql.append(" where hs.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
        sql.append("  and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
        sql.append(" and hs.id_concepto_tipo_personal = ?");
        sql.append(" and id_trabajador = ?");
        sql.append(" and anio = ?");
        sql.append(" and mes = ?");
        sql.append(" group by id_trabajador");
      }

      stHistorico = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select sum(monto_prestaciones) as base_adicional ");
      sql.append("from prestacionesmensuales pm ");
      sql.append("where (((pm.anio)*100) + pm.mes) >= ? ");
      sql.append("and (((pm.anio)*100)+pm.mes) <= ? ");
      sql.append("and pm.id_personal = ?");
      stDiasAdicionalesPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select tr.id_personal, count(*) ");
      sql.append("from trabajador tr, tipopersonal tp, clasificacionpersonal cla, relacionpersonal rp ");
      sql.append("where tr.estatus = 'A' ");
      sql.append("and tr.id_tipo_personal = tp.id_tipo_personal ");
      sql.append("and tp.id_clasificacion_personal = cla.id_clasificacion_personal ");
      sql.append("and cla.id_relacion_personal = rp.id_relacion_personal ");
      sql.append("and rp.cod_relacion in ('1', '9') ");
      sql.append("and tr.id_personal = ? ");
      sql.append("group by tr.id_personal having count(*) > 1 ");
      stTrabajadorPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stBorrar = connection.createStatement();
      sql = new StringBuffer();
      sql.append("delete from prestacionesmensuales where id_tipo_personal = " + idTipoPersonal + " and mes = " + mes + " and anio = " + anio);
      stBorrar.addBatch(sql.toString());
      stBorrar.executeBatch();

      sql = new StringBuffer();
      sql.append("delete from prestacionesonapre where id_tipo_personal = " + idTipoPersonal + " and mes = " + mes + " and anio = " + anio);
      stBorrar.addBatch(sql.toString());
      stBorrar.executeBatch();

      sql = new StringBuffer();
      sql.append("select id_concepto_tipo_personal, tipo, posicion_onapre ");
      sql.append(" from conceptoprestaciones ");
      sql.append("  where id_tipo_personal = ? ");

      stConceptoPrestaciones = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoPrestaciones.setLong(1, idTipoPersonal);

      if (parametroVarios.getAlicuotaBfaPrestac().equals("S"))
      {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1601'");
        sql.append("    and ctp.id_concepto = c.id_concepto");

        stConceptoAlicuotaBfa = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoAlicuotaBfa.setLong(1, idTipoPersonal);
        rsConceptoAlicuotaBfa = stConceptoAlicuotaBfa.executeQuery();
        rsConceptoAlicuotaBfa.next();
      }

      if (parametroVarios.getAlicuotaBvacPrestac().equals("S"))
      {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1501'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoAlicuotaBvac = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoAlicuotaBvac.setLong(1, idTipoPersonal);
        rsConceptoAlicuotaBvac = stConceptoAlicuotaBvac.executeQuery();
        rsConceptoAlicuotaBvac.next();
      }

      if (tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        sql = new StringBuffer();
        sql.append(" select max(semana_mes) as max_semana from semana where anio = ? and mes = ? ");
        stSemana = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stSemana.setInt(1, anio);
        stSemana.setInt(2, mes);
        rsSemana = stSemana.executeQuery();
        rsSemana.next();
      }

      Calendar fechaTope = Calendar.getInstance();

      fechaTope.set(anio, mes - 1, 1);

      this.log.error("calcular()-> fechaTope: INICIAL = " + fechaTope.getTime());

      int inicioDiasAdicionales = (anio - 1) * 100 + (mes + 1);

      int finDiasAdicionales = anio * 100 + mes;

      if (mes == 12) {
        inicioDiasAdicionales = anio * 100 + 1;
      }

      this.log.error("calcular()-> inicioDiasAdicionales: " + inicioDiasAdicionales);
      this.log.error("calcular()-> finDiasAdicionales = " + finDiasAdicionales);

      java.sql.Date fechaTopeSql = new java.sql.Date(fechaTope.getTime().getYear(), fechaTope.getTime().getMonth(), fechaTope.getTime().getDate());

      this.log.error("calcular()-> fechaTopeSql = " + fechaTopeSql.getTime());

      Calendar fechaFinMes = Calendar.getInstance();
      fechaFinMes.setTime(new java.util.Date());

      fechaFinMes.set(anio, mes, 1);

      this.log.error("calcular()-> 1- fechaFinMes= " + fechaFinMes.getTime());

      fechaFinMes.add(5, -1);
      this.log.error("calcular()-> 2- fechaFinMes= " + fechaFinMes.getTime());

      java.sql.Date fechaFinMesSql = new java.sql.Date(fechaFinMes.getTime().getYear(), fechaFinMes.getTime().getMonth(), fechaFinMes.getTime().getDate());

      this.log.error("calcular()-> FECHAS QUE SE USAN EN LOS QUERYS: fechaFinMesSql = " + fechaFinMesSql.getTime() + "   -  fechaTopeSql =" + fechaTope.getTime());

      sql = new StringBuffer();
      sql.append("select DISTINCT t.id_trabajador, t.mes_prestaciones, t.anio_prestaciones, t.dia_prestaciones, t.fecha_prestaciones, t.id_cargo, ");
      sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.estatus, t.id_personal, rp.cod_relacion ");
      sql.append(" from trabajador t, turno tu, tipopersonal tp, clasificacionpersonal cla, relacionpersonal rp ");
      sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and tp.id_clasificacion_personal = cla.id_clasificacion_personal");
      sql.append(" and cla.id_relacion_personal = rp.id_relacion_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and t.id_tipo_personal = ?");
      sql.append(" and t.estatus = 'A'");
      sql.append(" and t.fecha_prestaciones <= ?");

      sql.append(" union");

      sql.append(" select DISTINCT t.id_trabajador, t.mes_egreso, t.anio_egreso, t.dia_egreso, t.fecha_egreso, t.id_cargo, ");
      sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.estatus, t.id_personal, rp.cod_relacion ");
      sql.append(" from trabajador t, turno tu, tipopersonal tp, clasificacionpersonal cla, relacionpersonal rp ");
      sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and tp.id_clasificacion_personal = cla.id_clasificacion_personal");
      sql.append(" and cla.id_relacion_personal = rp.id_relacion_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and t.id_tipo_personal = ?");
      sql.append(" and t.estatus = 'E' ");
      sql.append(" and t.fecha_egreso >= ?");

      sql.append(" order by anio_prestaciones, mes_prestaciones,  dia_prestaciones");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, idTipoPersonal);
      stTrabajadores.setDate(2, fechaFinMesSql);
      stTrabajadores.setLong(3, idTipoPersonal);
      stTrabajadores.setDate(4, fechaTopeSql);
      rsTrabajadores = stTrabajadores.executeQuery();
      this.log.error("calcular()-> Leyo trabajadores********* ");

      while (rsTrabajadores.next())
      {
        baseMensual = 0.0D;
        baseAdicional = 0.0D;
        montoPrestaciones = 0.0D;
        montoAdicional = 0.0D;
        diasAdicionales = 0;
        totalMontoA = 0.0D;
        totalMontoB = 0.0D;
        totalMontoC = 0.0D;
        totalMontoE = 0.0D;
        aniosCumple = 0;
        sueldoBasicoOnapre = 0.0D;
        compensacionOnapre = 0.0D;
        primasOnapre = 0.0D;
        bonoFinAnioOnapre = 0.0D;
        bonoVacacionalOnapre = 0.0D;
        otrosOnapre = 0.0D;
        dias_laborados = 0;

        aniosCumple = anio - rsTrabajadores.getInt("anio_prestaciones");
        VacacionesPorAnio vacacionesPorAnio = new VacacionesPorAnio();

        rsTrabajadores.getInt("anio_prestaciones");

        if (rsTrabajadores.getString("estatus").equals("A"))
        {
          if ((rsTrabajadores.getInt("mes_prestaciones") == mes) && (rsTrabajadores.getInt("anio_prestaciones") == anio))
          {
            if (mes == 2) {
              dias_laborados = 28 - rsTrabajadores.getInt("dia_prestaciones") + 1 + 2;
            }
            else {
              dias_laborados = 30 - rsTrabajadores.getInt("dia_prestaciones") + 1;
            }

          }
          else if ((rsTrabajadores.getInt("mes_prestaciones") < mes) && (rsTrabajadores.getInt("anio_prestaciones") == anio))
          {
            dias_laborados = 30;
          }
          else if (rsTrabajadores.getInt("anio_prestaciones") < anio)
          {
            dias_laborados = 30;
          }
          else {
            dias_laborados = 30;
          }

        }
        else if (mes == 2)
          dias_laborados = rsTrabajadores.getInt("dia_prestaciones") + 2;
        else {
          dias_laborados = rsTrabajadores.getInt("dia_prestaciones");
        }

        if (parametroVarios.getAlicuotaBvacPrestac().equals("S")) {
          aniosCumple++;
          vacacionesPorAnio = this.definicionesNoGenBusiness.findVacacionesPorAnioForAniosServicio(idTipoPersonal, aniosCumple);
          aniosCumple--;
        }

        rsConceptoPrestaciones = stConceptoPrestaciones.executeQuery();

        while (rsConceptoPrestaciones.next())
        {
          montoA = 0.0D;
          montoB = 0.0D;
          montoC = 0.0D;
          montoD = 0.0D;
          montoE = 0.0D;

          if ((rsConceptoPrestaciones.getString("tipo").equals("B")) || 
            (rsTrabajadores.getString("estatus").equals("P")))
          {
            stConceptos.setLong(1, rsConceptoPrestaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));

            rsConceptos = stConceptos.executeQuery();

            while (rsConceptos.next())
            {
              if (rsConceptos.getInt("cod_frecuencia_pago") == 3)
                montoA += rsConceptos.getDouble("monto") * 2.0D;
              else if ((rsConceptos.getInt("cod_frecuencia_pago") == 4) || 
                (rsConceptos.getInt("cod_frecuencia_pago") == 10))
                montoA += rsConceptos.getDouble("monto") / 7.0D * 30.0D;
              else if ((rsConceptos.getInt("cod_frecuencia_pago") == 1) || 
                (rsConceptos.getInt("cod_frecuencia_pago") == 2) || (
                (rsConceptos.getInt("cod_frecuencia_pago") > 4) && 
                (rsConceptos.getInt("cod_frecuencia_pago") != 10)))
              {
                montoA += rsConceptos.getDouble("monto");
              }
            }

            totalMontoA += montoA;
          }
          else if ((rsConceptoPrestaciones.getString("tipo").equals("D")) && 
            (!rsTrabajadores.getString("estatus").equals("P")))
          {
            stHistorico.setLong(1, rsConceptoPrestaciones.getLong("id_concepto_tipo_personal"));
            stHistorico.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            stHistorico.setInt(3, anio);
            stHistorico.setInt(4, mes);
            rsHistorico = stHistorico.executeQuery();

            if (rsHistorico.next()) {
              if (((rsHistorico.getInt("cod_frecuencia_pago") != 4) && 
                (rsHistorico.getInt("cod_frecuencia_pago") != 10)) || (
                (rsHistorico.getInt("cod_frecuencia_pago") == 4) && 
                (rsHistorico.getString("origen").equalsIgnoreCase("V"))))
                montoB += rsHistorico.getDouble("monto");
              else if (rsHistorico.getInt("cod_frecuencia_pago") == 4)
                montoC += rsHistorico.getDouble("monto");
              else if (rsHistorico.getInt("cod_frecuencia_pago") == 10) {
                montoE += rsHistorico.getDouble("monto");
              }
              totalMontoB += montoB;
              totalMontoC += montoC;
              totalMontoE += montoE;

              if (rsHistorico.getInt("cod_frecuencia_pago") == 4) {
                montoD = montoC / (rsSemana.getInt("max_semana") * 7) * 30.0D;
              }

            }

          }

          switch (Integer.valueOf(rsConceptoPrestaciones.getString("posicion_onapre")).intValue()) {
          case 0:
            otrosOnapre = otrosOnapre + montoA + montoB + montoD + montoE;
            break;
          case 1:
            sueldoBasicoOnapre = sueldoBasicoOnapre + montoA + montoB + montoD + montoE;
            break;
          case 2:
            compensacionOnapre = compensacionOnapre + montoA + montoB + montoD + montoE;
            break;
          case 3:
            primasOnapre = primasOnapre + montoA + montoB + montoD + montoE;
            break;
          case 4:
            bonoFinAnioOnapre = bonoFinAnioOnapre + montoA + montoB + montoD + montoE;
            break;
          case 5:
            bonoVacacionalOnapre = bonoVacacionalOnapre + montoA + montoB + montoD + montoE;
          }

        }

        double montoAlicuotaBfa = 0.0D;
        double nuevoAlicuotaBfa = 0.0D;
        this.log.error("ID TRABAJADOR ..." + rsTrabajadores.getLong("id_trabajador"));
        this.log.error("MONTO A  ......" + totalMontoA);
        this.log.error("MONTO B  ......" + totalMontoB);
        if (parametroVarios.getAlicuotaBfaPrestac().equals("S")) {
          if (parametroVarios.getCalculoAlicuotaBfa().equals("F")) {
            montoAlicuotaBfa = calcularConceptoBeanBusiness.calcular(rsConceptoAlicuotaBfa.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), rsConceptoAlicuotaBfa.getDouble("unidades"), rsConceptoAlicuotaBfa.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoAlicuotaBfa.getDouble("valor"), rsConceptoAlicuotaBfa.getDouble("tope_minimo"), rsConceptoAlicuotaBfa.getDouble("tope_maximo"));
            montoAlicuotaBfa /= parametroVarios.getDiasAnio();
            this.log.error("Entre a 1 ......" + montoAlicuotaBfa);
            this.log.error("id_concepto_tipo_personal ......" + rsConceptoAlicuotaBfa.getLong("id_concepto_tipo_personal"));
          }
          else {
            montoAlicuotaBfa = (totalMontoA + totalMontoB) * rsConceptoAlicuotaBfa.getDouble("valor") / 100.0D / 30.0D;
            this.log.error("Entre a 2 ......" + montoAlicuotaBfa);
          }
        }

        double montoAlicuotaBvac = 0.0D;
        double nuevoAlicuotaBvac = 0.0D;
        if (parametroVarios.getAlicuotaBvacPrestac().equals("S")) {
          montoAlicuotaBvac = (totalMontoA + totalMontoB) / 30.0D;
          montoAlicuotaBvac = montoAlicuotaBvac * vacacionesPorAnio.getDiasBono() / parametroVarios.getDiasAnio();
          if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
            nuevoAlicuotaBvac = (totalMontoA + totalMontoB) / 30.0D + montoAlicuotaBfa;
            nuevoAlicuotaBvac = nuevoAlicuotaBvac * vacacionesPorAnio.getDiasBono() / parametroVarios.getDiasAnio();
          }

        }

        if ((parametroVarios.getAlicuotaBfaPrestac().equals("S")) && 
          (parametroVarios.getAlicuotaBvacBfa().equals("S"))) {
          nuevoAlicuotaBfa = (totalMontoA + totalMontoB + montoAlicuotaBvac * 30.0D) * rsConceptoAlicuotaBfa.getDouble("valor") / 100.0D / 30.0D;
        }

        if (nuevoAlicuotaBvac > 0.0D) {
          montoAlicuotaBvac = nuevoAlicuotaBvac;
        }
        if (nuevoAlicuotaBfa > 0.0D) {
          montoAlicuotaBfa = nuevoAlicuotaBfa;
        }

        baseMensual = totalMontoA + totalMontoB + totalMontoE + (montoAlicuotaBfa + montoAlicuotaBvac) * 30.0D;

        if (tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
          totalMontoC = totalMontoC / (rsSemana.getInt("max_semana") * 7) * 30.0D;
          baseMensual += totalMontoC;
        }

        if (dias_laborados == 30) {
          montoPrestaciones = baseMensual / 30.0D * 5.0D;
        }
        else if (dias_laborados < 30)
        {
          fraccion = 0.0D;

          fraccion = dias_laborados * 5 / 30.0D;

          montoPrestaciones = baseMensual / 30.0D * fraccion;
        }

        stTrabajadorPersonal.setLong(1, rsTrabajadores.getLong("id_personal"));
        rsTrabajadorPersonal = stTrabajadorPersonal.executeQuery();

        boolean mayorQueUno = rsTrabajadorPersonal.next();

        if ((rsTrabajadores.getInt("mes_prestaciones") == mes) && (aniosCumple > 1) && (
          (!rsTrabajadores.getString("cod_relacion").equals("9")) || (!mayorQueUno))) {
          stDiasAdicionalesPersonal.setInt(1, inicioDiasAdicionales);
          stDiasAdicionalesPersonal.setInt(2, finDiasAdicionales);
          stDiasAdicionalesPersonal.setLong(3, rsTrabajadores.getLong("id_personal"));
          rsDiasAdicionalesPersonal = stDiasAdicionalesPersonal.executeQuery();

          if (rsDiasAdicionalesPersonal.next()) {
            diasAdicionales = (aniosCumple - 1) * 2;
            if (diasAdicionales > 30) {
              diasAdicionales = 30;
            }
            baseAdicional = rsDiasAdicionalesPersonal.getDouble("base_adicional") + montoPrestaciones;
            montoAdicional = baseAdicional / 60.0D * diasAdicionales;
          }

        }

        if (baseMensual != 0.0D) {
          sql = new StringBuffer();
          sql.append("insert into prestacionesmensuales (id_tipo_personal, id_trabajador, id_personal, ");
          sql.append("anio, mes, dias_mensuales, dias_adicionales, base_mensual, ");
          sql.append("base_adicional, monto_prestaciones, monto_adicional, id_prestaciones_mensuales) values(");
          sql.append(idTipoPersonal + ", " + rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsTrabajadores.getLong("id_personal") + ", ");
          sql.append(anio + ", " + mes + "," + NumberTools.twoDecimal(fraccion) + "," + diasAdicionales + ", ");
          sql.append(NumberTools.twoDecimal(baseMensual) + ", " + NumberTools.twoDecimal(baseAdicional) + ", ");
          sql.append(NumberTools.twoDecimal(montoPrestaciones) + ", " + NumberTools.twoDecimal(montoAdicional) + ", ");
          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.prestaciones.PrestacionesMensuales") + ")");
          stInsert.addBatch(sql.toString());

          sql = new StringBuffer();
          sql.append("insert into prestacionesonapre (id_tipo_personal, id_trabajador, ");
          sql.append("anio, mes, fecha_prestaciones, sueldo_basico, compensacion, primas, ");
          sql.append("bono_fin_anio, bono_vacacional,  ");
          sql.append("otros, sueldo_integral, dias_mensuales, ");
          sql.append("dias_adicionales, abono_mensual, base_adicional, abono_adicional, ");
          sql.append("id_prestaciones_onapre) values(");
          sql.append(idTipoPersonal + ", " + rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(anio + ", " + mes + ", '" + rsTrabajadores.getDate("fecha_prestaciones") + "', " + NumberTools.twoDecimal(sueldoBasicoOnapre) + ", ");
          sql.append(NumberTools.twoDecimal(compensacionOnapre) + ", " + NumberTools.twoDecimal(primasOnapre) + ", " + NumberTools.twoDecimal(bonoFinAnioOnapre) + ", ");
          sql.append(NumberTools.twoDecimal(bonoVacacionalOnapre) + ", " + NumberTools.twoDecimal(otrosOnapre) + ", ");
          sql.append(NumberTools.twoDecimal(baseMensual) + "," + NumberTools.twoDecimal(fraccion) + "," + diasAdicionales + ", " + NumberTools.twoDecimal(montoPrestaciones) + ", " + NumberTools.twoDecimal(baseAdicional) + ", ");
          sql.append(NumberTools.twoDecimal(montoAdicional) + ", ");
          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.prestaciones.PrestacionesOnapre") + ")");
          stInsert.addBatch(sql.toString());
        }

      }

      stInsert.executeBatch();
      connection.commit();
    } finally {
      if (rsSemana != null) try {
          rsSemana.close();
        } catch (Exception localException19) {
        } if (rsHistorico != null) try {
          rsHistorico.close();
        } catch (Exception localException20) {
        } if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException21) {
        } if (rsDiasAdicionalesPersonal != null) try {
          rsDiasAdicionalesPersonal.close();
        } catch (Exception localException22) {
        } if (rsConceptoAlicuotaBfa != null) try {
          rsConceptoAlicuotaBfa.close();
        } catch (Exception localException23) {
        } if (rsConceptoAlicuotaBvac != null) try {
          rsConceptoAlicuotaBvac.close();
        } catch (Exception localException24) {
        } if (rsConceptoPrestaciones != null) try {
          rsConceptoPrestaciones.close();
        } catch (Exception localException25) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException26) {
        } if (stSemana != null) try {
          stSemana.close();
        } catch (Exception localException27) {
        } if (stHistorico != null) try {
          stHistorico.close();
        } catch (Exception localException28) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException29) {
        } if (stDiasAdicionalesPersonal != null) try {
          stDiasAdicionalesPersonal.close();
        } catch (Exception localException30) {
        } if (stCerrar != null) try {
          stCerrar.close();
        } catch (Exception localException31) {
        } if (stActualizarFecha != null) try {
          stActualizarFecha.close();
        } catch (Exception localException32) {
        } if (stBorrar != null) try {
          stBorrar.close();
        } catch (Exception localException33) {
        } if (stConceptoAlicuotaBvac != null) try {
          stConceptoAlicuotaBvac.close();
        } catch (Exception localException34) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException35) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException36) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException37)
        {
        }
    }
    if (rsSemana != null) try {
        rsSemana.close();
      } catch (Exception localException38) {
      } if (rsHistorico != null) try {
        rsHistorico.close();
      } catch (Exception localException39) {
      } if (rsConceptos != null) try {
        rsConceptos.close();
      } catch (Exception localException40) {
      } if (rsDiasAdicionalesPersonal != null) try {
        rsDiasAdicionalesPersonal.close();
      } catch (Exception localException41) {
      } if (rsConceptoAlicuotaBfa != null) try {
        rsConceptoAlicuotaBfa.close();
      } catch (Exception localException42) {
      } if (rsConceptoAlicuotaBvac != null) try {
        rsConceptoAlicuotaBvac.close();
      } catch (Exception localException43) {
      } if (rsConceptoPrestaciones != null) try {
        rsConceptoPrestaciones.close();
      } catch (Exception localException44) {
      } if (rsTrabajadores != null) try {
        rsTrabajadores.close();
      } catch (Exception localException45) {
      } if (stSemana != null) try {
        stSemana.close();
      } catch (Exception localException46) {
      } if (stHistorico != null) try {
        stHistorico.close();
      } catch (Exception localException47) {
      } if (stConceptos != null) try {
        stConceptos.close();
      } catch (Exception localException48) {
      } if (stDiasAdicionalesPersonal != null) try {
        stDiasAdicionalesPersonal.close();
      } catch (Exception localException49) {
      } if (stCerrar != null) try {
        stCerrar.close();
      } catch (Exception localException50) {
      } if (stActualizarFecha != null) try {
        stActualizarFecha.close();
      } catch (Exception localException51) {
      } if (stBorrar != null) try {
        stBorrar.close();
      } catch (Exception localException52) {
      } if (stConceptoAlicuotaBvac != null) try {
        stConceptoAlicuotaBvac.close();
      } catch (Exception localException53) {
      } if (stTrabajadores != null) try {
        stTrabajadores.close();
      } catch (Exception localException54) {
      } if (stInsert != null) try {
        stInsert.close();
      } catch (Exception localException55) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException56)
      {
      } return true;
  }
}