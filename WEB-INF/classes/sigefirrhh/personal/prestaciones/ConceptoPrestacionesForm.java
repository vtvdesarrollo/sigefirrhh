package sigefirrhh.personal.prestaciones;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoPrestacionesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoPrestacionesForm.class.getName());
  private ConceptoPrestaciones conceptoPrestaciones;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private String reportName = "ConceptoPrestaciones";

  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private PrestacionesFacade prestacionesFacade = new PrestacionesFacade();
  private boolean showConceptoPrestacionesByTipoPersonal;
  private String findSelectTipoPersonal;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private String selectTipoPersonal;
  private String selectConceptoTipoPersonal;
  private Object stateResultConceptoPrestacionesByTipoPersonal = null;

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));

    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName(this.reportName);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/personal/prestaciones");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      this.reportName + this.reportId, report);
    newReportId();
    return null;
  }
  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }

  public boolean isShowConcepto() {
    return (this.colConceptoTipoPersonal != null) && 
      (!this.colConceptoTipoPersonal.isEmpty());
  }
  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.colConceptoTipoPersonal = null;
    try
    {
      if (idTipoPersonal > 0L)
      {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.conceptoPrestaciones.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.conceptoPrestaciones.setTipoPersonal(
          tipoPersonal);
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoPrestaciones.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoPrestaciones.setConceptoTipoPersonal(
          conceptoTipoPersonal);
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoPrestaciones getConceptoPrestaciones() {
    if (this.conceptoPrestaciones == null) {
      this.conceptoPrestaciones = new ConceptoPrestaciones();
    }
    return this.conceptoPrestaciones;
  }

  public ConceptoPrestacionesForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.reportName = "ConceptoPrestaciones";
    newReportId();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getListTipo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoPrestaciones.LISTA_TIPO_PAGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListPosicionOnapre() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoPrestaciones.LISTA_ONAPRE.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
      this.colConceptoTipoPersonal = 
        this.definicionesFacade.findAllConceptoTipoPersonal();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoPrestacionesByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.prestacionesFacade.findConceptoPrestacionesByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showConceptoPrestacionesByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoPrestacionesByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowConceptoPrestacionesByTipoPersonal() {
    return this.showConceptoPrestacionesByTipoPersonal;
  }

  public String selectConceptoPrestaciones()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectConceptoTipoPersonal = null;

    long idConceptoPrestaciones = 
      Long.parseLong((String)requestParameterMap.get("idConceptoPrestaciones"));
    try
    {
      this.conceptoPrestaciones = 
        this.prestacionesFacade.findConceptoPrestacionesById(
        idConceptoPrestaciones);
      if (this.conceptoPrestaciones.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.conceptoPrestaciones.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.conceptoPrestaciones.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoPrestaciones.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoPrestaciones = null;
    this.showConceptoPrestacionesByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.conceptoPrestaciones.getTiempoSitp() != null) && 
      (this.conceptoPrestaciones.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.prestacionesFacade.addConceptoPrestaciones(
          this.conceptoPrestaciones);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoPrestaciones);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.prestacionesFacade.updateConceptoPrestaciones(
          this.conceptoPrestaciones);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoPrestaciones);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.prestacionesFacade.deleteConceptoPrestaciones(
        this.conceptoPrestaciones);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoPrestaciones);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoPrestaciones = new ConceptoPrestaciones();

    this.selectTipoPersonal = null;

    this.selectConceptoTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoPrestaciones.setIdConceptoPrestaciones(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.prestaciones.ConceptoPrestaciones"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoPrestaciones = new ConceptoPrestaciones();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
}