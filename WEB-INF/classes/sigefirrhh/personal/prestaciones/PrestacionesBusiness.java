package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;
import org.apache.log4j.Logger;

public class PrestacionesBusiness extends AbstractBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(PrestacionesBusiness.class.getName());

  private ConceptoPrestacionesBeanBusiness conceptoPrestacionesBeanBusiness = new ConceptoPrestacionesBeanBusiness();

  private FideicomisoBeanBusiness fideicomisoBeanBusiness = new FideicomisoBeanBusiness();

  private PrestacionesMensualesBeanBusiness prestacionesMensualesBeanBusiness = new PrestacionesMensualesBeanBusiness();

  private PrestacionesOnapreBeanBusiness prestacionesOnapreBeanBusiness = new PrestacionesOnapreBeanBusiness();

  private SeguridadPrestacionesBeanBusiness seguridadPrestacionesBeanBusiness = new SeguridadPrestacionesBeanBusiness();

  public void addConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones)
    throws Exception
  {
    this.conceptoPrestacionesBeanBusiness.addConceptoPrestaciones(conceptoPrestaciones);
  }

  public void updateConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones) throws Exception {
    this.conceptoPrestacionesBeanBusiness.updateConceptoPrestaciones(conceptoPrestaciones);
  }

  public void deleteConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones) throws Exception {
    this.conceptoPrestacionesBeanBusiness.deleteConceptoPrestaciones(conceptoPrestaciones);
  }

  public ConceptoPrestaciones findConceptoPrestacionesById(long conceptoPrestacionesId) throws Exception {
    return this.conceptoPrestacionesBeanBusiness.findConceptoPrestacionesById(conceptoPrestacionesId);
  }

  public Collection findAllConceptoPrestaciones() throws Exception {
    return this.conceptoPrestacionesBeanBusiness.findConceptoPrestacionesAll();
  }

  public Collection findConceptoPrestacionesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.conceptoPrestacionesBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addFideicomiso(Fideicomiso fideicomiso)
    throws Exception
  {
    this.fideicomisoBeanBusiness.addFideicomiso(fideicomiso);
  }

  public void updateFideicomiso(Fideicomiso fideicomiso) throws Exception {
    this.fideicomisoBeanBusiness.updateFideicomiso(fideicomiso);
  }

  public void deleteFideicomiso(Fideicomiso fideicomiso) throws Exception {
    this.fideicomisoBeanBusiness.deleteFideicomiso(fideicomiso);
  }

  public Fideicomiso findFideicomisoById(long fideicomisoId) throws Exception {
    return this.fideicomisoBeanBusiness.findFideicomisoById(fideicomisoId);
  }

  public Collection findAllFideicomiso() throws Exception {
    return this.fideicomisoBeanBusiness.findFideicomisoAll();
  }

  public Collection findFideicomisoByBanco(long idBanco)
    throws Exception
  {
    return this.fideicomisoBeanBusiness.findByBanco(idBanco);
  }

  public Collection findFideicomisoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.fideicomisoBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findFideicomisoByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.fideicomisoBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addPrestacionesMensuales(PrestacionesMensuales prestacionesMensuales)
    throws Exception
  {
    this.prestacionesMensualesBeanBusiness.addPrestacionesMensuales(prestacionesMensuales);
  }

  public void updatePrestacionesMensuales(PrestacionesMensuales prestacionesMensuales) throws Exception {
    this.log.error("update 20");
    this.prestacionesMensualesBeanBusiness.updatePrestacionesMensuales(prestacionesMensuales);
    this.log.error("update 21");
  }

  public void deletePrestacionesMensuales(PrestacionesMensuales prestacionesMensuales) throws Exception {
    this.prestacionesMensualesBeanBusiness.deletePrestacionesMensuales(prestacionesMensuales);
  }

  public PrestacionesMensuales findPrestacionesMensualesById(long prestacionesMensualesId) throws Exception {
    return this.prestacionesMensualesBeanBusiness.findPrestacionesMensualesById(prestacionesMensualesId);
  }

  public Collection findAllPrestacionesMensuales() throws Exception {
    return this.prestacionesMensualesBeanBusiness.findPrestacionesMensualesAll();
  }

  public Collection findPrestacionesMensualesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.prestacionesMensualesBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findPrestacionesMensualesByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.prestacionesMensualesBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addPrestacionesOnapre(PrestacionesOnapre prestacionesOnapre)
    throws Exception
  {
    this.prestacionesOnapreBeanBusiness.addPrestacionesOnapre(prestacionesOnapre);
  }

  public void updatePrestacionesOnapre(PrestacionesOnapre prestacionesOnapre) throws Exception {
    this.prestacionesOnapreBeanBusiness.updatePrestacionesOnapre(prestacionesOnapre);
  }

  public void deletePrestacionesOnapre(PrestacionesOnapre prestacionesOnapre) throws Exception {
    this.prestacionesOnapreBeanBusiness.deletePrestacionesOnapre(prestacionesOnapre);
  }

  public PrestacionesOnapre findPrestacionesOnapreById(long prestacionesOnapreId) throws Exception {
    return this.prestacionesOnapreBeanBusiness.findPrestacionesOnapreById(prestacionesOnapreId);
  }

  public Collection findAllPrestacionesOnapre() throws Exception {
    return this.prestacionesOnapreBeanBusiness.findPrestacionesOnapreAll();
  }

  public Collection findPrestacionesOnapreByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.prestacionesOnapreBeanBusiness.findByTrabajador(idTrabajador);
  }

  public Collection findPrestacionesOnapreByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.prestacionesOnapreBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones)
    throws Exception
  {
    this.seguridadPrestacionesBeanBusiness.addSeguridadPrestaciones(seguridadPrestaciones);
  }

  public void updateSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones) throws Exception {
    this.seguridadPrestacionesBeanBusiness.updateSeguridadPrestaciones(seguridadPrestaciones);
  }

  public void deleteSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones) throws Exception {
    this.seguridadPrestacionesBeanBusiness.deleteSeguridadPrestaciones(seguridadPrestaciones);
  }

  public SeguridadPrestaciones findSeguridadPrestacionesById(long seguridadPrestacionesId) throws Exception {
    return this.seguridadPrestacionesBeanBusiness.findSeguridadPrestacionesById(seguridadPrestacionesId);
  }

  public Collection findAllSeguridadPrestaciones() throws Exception {
    return this.seguridadPrestacionesBeanBusiness.findSeguridadPrestacionesAll();
  }

  public Collection findSeguridadPrestacionesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.seguridadPrestacionesBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findSeguridadPrestacionesByAnio(int anio)
    throws Exception
  {
    return this.seguridadPrestacionesBeanBusiness.findByAnio(anio);
  }
}