package sigefirrhh.personal.prestaciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class PrestacionesOnapre
  implements Serializable, PersistenceCapable
{
  private long idPrestacionesOnapre;
  private int anio;
  private int mes;
  private Date fechaPrestaciones;
  private double sueldoBasico;
  private double compensacion;
  private double primas;
  private double bonoFinAnio;
  private double bonoVacacional;
  private double otros;
  private double sueldoIntegral;
  private double abonoMensual;
  private double baseAdicional;
  private double abonoAdicional;
  private double diasMensuales;
  private int diasAdicionales;
  private Trabajador trabajador;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "abonoAdicional", "abonoMensual", "anio", "baseAdicional", "bonoFinAnio", "bonoVacacional", "compensacion", "diasAdicionales", "diasMensuales", "fechaPrestaciones", "idPrestacionesOnapre", "mes", "otros", "primas", "sueldoBasico", "sueldoIntegral", "tipoPersonal", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public PrestacionesOnapre()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetsueldoBasico(this, 0.0D);

    jdoSetcompensacion(this, 0.0D);

    jdoSetprimas(this, 0.0D);

    jdoSetbonoFinAnio(this, 0.0D);

    jdoSetbonoVacacional(this, 0.0D);

    jdoSetotros(this, 0.0D);

    jdoSetsueldoIntegral(this, 0.0D);

    jdoSetabonoMensual(this, 0.0D);

    jdoSetbaseAdicional(this, 0.0D);

    jdoSetabonoAdicional(this, 0.0D);

    jdoSetdiasMensuales(this, 5.0D);

    jdoSetdiasAdicionales(this, 0);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetabonoMensual(this));

    return jdoGetanio(this) + " - " + jdoGetmes(this) + " - " + a;
  }

  public double getAbonoAdicional() {
    return jdoGetabonoAdicional(this);
  }

  public void setAbonoAdicional(double abonoAdicional) {
    jdoSetabonoAdicional(this, abonoAdicional);
  }

  public double getAbonoMensual() {
    return jdoGetabonoMensual(this);
  }

  public void setAbonoMensual(double abonoMensual) {
    jdoSetabonoMensual(this, abonoMensual);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public double getBaseAdicional() {
    return jdoGetbaseAdicional(this);
  }

  public void setBaseAdicional(double baseAdicional) {
    jdoSetbaseAdicional(this, baseAdicional);
  }

  public double getBonoFinAnio() {
    return jdoGetbonoFinAnio(this);
  }

  public void setBonoFinAnio(double bonoFinAnio) {
    jdoSetbonoFinAnio(this, bonoFinAnio);
  }

  public double getBonoVacacional() {
    return jdoGetbonoVacacional(this);
  }

  public void setBonoVacacional(double bonoVacacional) {
    jdoSetbonoVacacional(this, bonoVacacional);
  }

  public double getCompensacion() {
    return jdoGetcompensacion(this);
  }

  public void setCompensacion(double compensacion) {
    jdoSetcompensacion(this, compensacion);
  }

  public int getDiasAdicionales() {
    return jdoGetdiasAdicionales(this);
  }

  public void setDiasAdicionales(int diasAdicionales) {
    jdoSetdiasAdicionales(this, diasAdicionales);
  }

  public double getDiasMensuales() {
    return jdoGetdiasMensuales(this);
  }

  public void setDiasMensuales(double diasMensuales) {
    jdoSetdiasMensuales(this, diasMensuales);
  }

  public Date getFechaPrestaciones() {
    return jdoGetfechaPrestaciones(this);
  }

  public void setFechaPrestaciones(Date fechaPrestaciones) {
    jdoSetfechaPrestaciones(this, fechaPrestaciones);
  }

  public long getIdPrestacionesOnapre() {
    return jdoGetidPrestacionesOnapre(this);
  }

  public void setIdPrestacionesOnapre(long idPrestacionesOnapre) {
    jdoSetidPrestacionesOnapre(this, idPrestacionesOnapre);
  }

  public int getMes() {
    return jdoGetmes(this);
  }

  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }

  public double getOtros() {
    return jdoGetotros(this);
  }

  public void setOtros(double otros) {
    jdoSetotros(this, otros);
  }

  public double getPrimas() {
    return jdoGetprimas(this);
  }

  public void setPrimas(double primas) {
    jdoSetprimas(this, primas);
  }

  public double getSueldoBasico() {
    return jdoGetsueldoBasico(this);
  }

  public void setSueldoBasico(double sueldoBasico) {
    jdoSetsueldoBasico(this, sueldoBasico);
  }

  public double getSueldoIntegral() {
    return jdoGetsueldoIntegral(this);
  }

  public void setSueldoIntegral(double sueldoIntegral) {
    jdoSetsueldoIntegral(this, sueldoIntegral);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }

  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 18;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.prestaciones.PrestacionesOnapre"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrestacionesOnapre());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrestacionesOnapre localPrestacionesOnapre = new PrestacionesOnapre();
    localPrestacionesOnapre.jdoFlags = 1;
    localPrestacionesOnapre.jdoStateManager = paramStateManager;
    return localPrestacionesOnapre;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrestacionesOnapre localPrestacionesOnapre = new PrestacionesOnapre();
    localPrestacionesOnapre.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrestacionesOnapre.jdoFlags = 1;
    localPrestacionesOnapre.jdoStateManager = paramStateManager;
    return localPrestacionesOnapre;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.abonoAdicional);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.abonoMensual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseAdicional);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.bonoFinAnio);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.bonoVacacional);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasAdicionales);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasMensuales);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPrestaciones);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrestacionesOnapre);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.otros);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primas);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoBasico);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoIntegral);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abonoAdicional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.abonoMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseAdicional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bonoFinAnio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bonoVacacional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasAdicionales = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasMensuales = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPrestaciones = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrestacionesOnapre = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otros = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoBasico = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoIntegral = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrestacionesOnapre paramPrestacionesOnapre, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.abonoAdicional = paramPrestacionesOnapre.abonoAdicional;
      return;
    case 1:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.abonoMensual = paramPrestacionesOnapre.abonoMensual;
      return;
    case 2:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPrestacionesOnapre.anio;
      return;
    case 3:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.baseAdicional = paramPrestacionesOnapre.baseAdicional;
      return;
    case 4:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.bonoFinAnio = paramPrestacionesOnapre.bonoFinAnio;
      return;
    case 5:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.bonoVacacional = paramPrestacionesOnapre.bonoVacacional;
      return;
    case 6:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramPrestacionesOnapre.compensacion;
      return;
    case 7:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.diasAdicionales = paramPrestacionesOnapre.diasAdicionales;
      return;
    case 8:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.diasMensuales = paramPrestacionesOnapre.diasMensuales;
      return;
    case 9:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPrestaciones = paramPrestacionesOnapre.fechaPrestaciones;
      return;
    case 10:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.idPrestacionesOnapre = paramPrestacionesOnapre.idPrestacionesOnapre;
      return;
    case 11:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramPrestacionesOnapre.mes;
      return;
    case 12:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.otros = paramPrestacionesOnapre.otros;
      return;
    case 13:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.primas = paramPrestacionesOnapre.primas;
      return;
    case 14:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoBasico = paramPrestacionesOnapre.sueldoBasico;
      return;
    case 15:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoIntegral = paramPrestacionesOnapre.sueldoIntegral;
      return;
    case 16:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPrestacionesOnapre.tipoPersonal;
      return;
    case 17:
      if (paramPrestacionesOnapre == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPrestacionesOnapre.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrestacionesOnapre))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrestacionesOnapre localPrestacionesOnapre = (PrestacionesOnapre)paramObject;
    if (localPrestacionesOnapre.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrestacionesOnapre, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrestacionesOnaprePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrestacionesOnaprePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesOnaprePK))
      throw new IllegalArgumentException("arg1");
    PrestacionesOnaprePK localPrestacionesOnaprePK = (PrestacionesOnaprePK)paramObject;
    localPrestacionesOnaprePK.idPrestacionesOnapre = this.idPrestacionesOnapre;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesOnaprePK))
      throw new IllegalArgumentException("arg1");
    PrestacionesOnaprePK localPrestacionesOnaprePK = (PrestacionesOnaprePK)paramObject;
    this.idPrestacionesOnapre = localPrestacionesOnaprePK.idPrestacionesOnapre;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesOnaprePK))
      throw new IllegalArgumentException("arg2");
    PrestacionesOnaprePK localPrestacionesOnaprePK = (PrestacionesOnaprePK)paramObject;
    localPrestacionesOnaprePK.idPrestacionesOnapre = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesOnaprePK))
      throw new IllegalArgumentException("arg2");
    PrestacionesOnaprePK localPrestacionesOnaprePK = (PrestacionesOnaprePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localPrestacionesOnaprePK.idPrestacionesOnapre);
  }

  private static final double jdoGetabonoAdicional(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.abonoAdicional;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.abonoAdicional;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 0))
      return paramPrestacionesOnapre.abonoAdicional;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 0, paramPrestacionesOnapre.abonoAdicional);
  }

  private static final void jdoSetabonoAdicional(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.abonoAdicional = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.abonoAdicional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 0, paramPrestacionesOnapre.abonoAdicional, paramDouble);
  }

  private static final double jdoGetabonoMensual(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.abonoMensual;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.abonoMensual;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 1))
      return paramPrestacionesOnapre.abonoMensual;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 1, paramPrestacionesOnapre.abonoMensual);
  }

  private static final void jdoSetabonoMensual(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.abonoMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.abonoMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 1, paramPrestacionesOnapre.abonoMensual, paramDouble);
  }

  private static final int jdoGetanio(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.anio;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.anio;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 2))
      return paramPrestacionesOnapre.anio;
    return localStateManager.getIntField(paramPrestacionesOnapre, jdoInheritedFieldCount + 2, paramPrestacionesOnapre.anio);
  }

  private static final void jdoSetanio(PrestacionesOnapre paramPrestacionesOnapre, int paramInt)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesOnapre, jdoInheritedFieldCount + 2, paramPrestacionesOnapre.anio, paramInt);
  }

  private static final double jdoGetbaseAdicional(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.baseAdicional;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.baseAdicional;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 3))
      return paramPrestacionesOnapre.baseAdicional;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 3, paramPrestacionesOnapre.baseAdicional);
  }

  private static final void jdoSetbaseAdicional(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.baseAdicional = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.baseAdicional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 3, paramPrestacionesOnapre.baseAdicional, paramDouble);
  }

  private static final double jdoGetbonoFinAnio(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.bonoFinAnio;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.bonoFinAnio;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 4))
      return paramPrestacionesOnapre.bonoFinAnio;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 4, paramPrestacionesOnapre.bonoFinAnio);
  }

  private static final void jdoSetbonoFinAnio(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.bonoFinAnio = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.bonoFinAnio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 4, paramPrestacionesOnapre.bonoFinAnio, paramDouble);
  }

  private static final double jdoGetbonoVacacional(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.bonoVacacional;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.bonoVacacional;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 5))
      return paramPrestacionesOnapre.bonoVacacional;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 5, paramPrestacionesOnapre.bonoVacacional);
  }

  private static final void jdoSetbonoVacacional(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.bonoVacacional = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.bonoVacacional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 5, paramPrestacionesOnapre.bonoVacacional, paramDouble);
  }

  private static final double jdoGetcompensacion(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.compensacion;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.compensacion;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 6))
      return paramPrestacionesOnapre.compensacion;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 6, paramPrestacionesOnapre.compensacion);
  }

  private static final void jdoSetcompensacion(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.compensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.compensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 6, paramPrestacionesOnapre.compensacion, paramDouble);
  }

  private static final int jdoGetdiasAdicionales(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.diasAdicionales;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.diasAdicionales;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 7))
      return paramPrestacionesOnapre.diasAdicionales;
    return localStateManager.getIntField(paramPrestacionesOnapre, jdoInheritedFieldCount + 7, paramPrestacionesOnapre.diasAdicionales);
  }

  private static final void jdoSetdiasAdicionales(PrestacionesOnapre paramPrestacionesOnapre, int paramInt)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.diasAdicionales = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.diasAdicionales = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesOnapre, jdoInheritedFieldCount + 7, paramPrestacionesOnapre.diasAdicionales, paramInt);
  }

  private static final double jdoGetdiasMensuales(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.diasMensuales;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.diasMensuales;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 8))
      return paramPrestacionesOnapre.diasMensuales;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 8, paramPrestacionesOnapre.diasMensuales);
  }

  private static final void jdoSetdiasMensuales(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.diasMensuales = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.diasMensuales = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 8, paramPrestacionesOnapre.diasMensuales, paramDouble);
  }

  private static final Date jdoGetfechaPrestaciones(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.fechaPrestaciones;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.fechaPrestaciones;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 9))
      return paramPrestacionesOnapre.fechaPrestaciones;
    return (Date)localStateManager.getObjectField(paramPrestacionesOnapre, jdoInheritedFieldCount + 9, paramPrestacionesOnapre.fechaPrestaciones);
  }

  private static final void jdoSetfechaPrestaciones(PrestacionesOnapre paramPrestacionesOnapre, Date paramDate)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.fechaPrestaciones = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.fechaPrestaciones = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesOnapre, jdoInheritedFieldCount + 9, paramPrestacionesOnapre.fechaPrestaciones, paramDate);
  }

  private static final long jdoGetidPrestacionesOnapre(PrestacionesOnapre paramPrestacionesOnapre)
  {
    return paramPrestacionesOnapre.idPrestacionesOnapre;
  }

  private static final void jdoSetidPrestacionesOnapre(PrestacionesOnapre paramPrestacionesOnapre, long paramLong)
  {
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.idPrestacionesOnapre = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrestacionesOnapre, jdoInheritedFieldCount + 10, paramPrestacionesOnapre.idPrestacionesOnapre, paramLong);
  }

  private static final int jdoGetmes(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.mes;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.mes;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 11))
      return paramPrestacionesOnapre.mes;
    return localStateManager.getIntField(paramPrestacionesOnapre, jdoInheritedFieldCount + 11, paramPrestacionesOnapre.mes);
  }

  private static final void jdoSetmes(PrestacionesOnapre paramPrestacionesOnapre, int paramInt)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesOnapre, jdoInheritedFieldCount + 11, paramPrestacionesOnapre.mes, paramInt);
  }

  private static final double jdoGetotros(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.otros;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.otros;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 12))
      return paramPrestacionesOnapre.otros;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 12, paramPrestacionesOnapre.otros);
  }

  private static final void jdoSetotros(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.otros = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.otros = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 12, paramPrestacionesOnapre.otros, paramDouble);
  }

  private static final double jdoGetprimas(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.primas;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.primas;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 13))
      return paramPrestacionesOnapre.primas;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 13, paramPrestacionesOnapre.primas);
  }

  private static final void jdoSetprimas(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.primas = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.primas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 13, paramPrestacionesOnapre.primas, paramDouble);
  }

  private static final double jdoGetsueldoBasico(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.sueldoBasico;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.sueldoBasico;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 14))
      return paramPrestacionesOnapre.sueldoBasico;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 14, paramPrestacionesOnapre.sueldoBasico);
  }

  private static final void jdoSetsueldoBasico(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.sueldoBasico = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.sueldoBasico = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 14, paramPrestacionesOnapre.sueldoBasico, paramDouble);
  }

  private static final double jdoGetsueldoIntegral(PrestacionesOnapre paramPrestacionesOnapre)
  {
    if (paramPrestacionesOnapre.jdoFlags <= 0)
      return paramPrestacionesOnapre.sueldoIntegral;
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.sueldoIntegral;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 15))
      return paramPrestacionesOnapre.sueldoIntegral;
    return localStateManager.getDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 15, paramPrestacionesOnapre.sueldoIntegral);
  }

  private static final void jdoSetsueldoIntegral(PrestacionesOnapre paramPrestacionesOnapre, double paramDouble)
  {
    if (paramPrestacionesOnapre.jdoFlags == 0)
    {
      paramPrestacionesOnapre.sueldoIntegral = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.sueldoIntegral = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesOnapre, jdoInheritedFieldCount + 15, paramPrestacionesOnapre.sueldoIntegral, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(PrestacionesOnapre paramPrestacionesOnapre)
  {
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.tipoPersonal;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 16))
      return paramPrestacionesOnapre.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPrestacionesOnapre, jdoInheritedFieldCount + 16, paramPrestacionesOnapre.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PrestacionesOnapre paramPrestacionesOnapre, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesOnapre, jdoInheritedFieldCount + 16, paramPrestacionesOnapre.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(PrestacionesOnapre paramPrestacionesOnapre)
  {
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesOnapre.trabajador;
    if (localStateManager.isLoaded(paramPrestacionesOnapre, jdoInheritedFieldCount + 17))
      return paramPrestacionesOnapre.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPrestacionesOnapre, jdoInheritedFieldCount + 17, paramPrestacionesOnapre.trabajador);
  }

  private static final void jdoSettrabajador(PrestacionesOnapre paramPrestacionesOnapre, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPrestacionesOnapre.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesOnapre.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesOnapre, jdoInheritedFieldCount + 17, paramPrestacionesOnapre.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}