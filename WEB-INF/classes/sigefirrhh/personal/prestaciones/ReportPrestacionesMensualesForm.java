package sigefirrhh.personal.prestaciones;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FirmasReportes;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPrestacionesMensualesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPrestacionesMensualesForm.class.getName());
  private int reportId;
  private long idRegion;
  private long idTipoPersonal;
  private String reportName;
  private String orden;
  private int mes;
  private int anio;
  private String formato = "1";
  private String tipo = "G";
  private Collection listRegion;
  private Collection listTipoPersonal;
  private EstructuraFacade estructuraFacade;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private long firma1;
  private Collection listFirma1;
  private long firma2;
  private Collection listFirma2;
  private long firma3;
  private Collection listFirma3;

  public ReportPrestacionesMensualesForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "prestmensualesalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPrestacionesMensualesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRegion = this.estructuraFacade.findAllRegion();

      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());

      this.listFirma1 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma2 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
      this.listFirma3 = this.definicionesFacade.findFirmasReportesByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e) {
      this.listRegion = new ArrayList();
      this.listTipoPersonal = new ArrayList();
      this.listFirma1 = new ArrayList();
      this.listFirma2 = new ArrayList();
      this.listFirma3 = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      this.reportName = "";

      if (this.tipo.equals("D"))
        this.reportName = "detalle";
      else if (this.tipo.equals("R")) {
        this.reportName = "resumen";
      }
      this.reportName += "prestmensuales";
      if (this.idTipoPersonal != 0L) {
        this.reportName += "tp";
      }
      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }
      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";

      if (this.tipo.equals("D"))
        this.reportName = "detalle";
      else if (this.tipo.equals("R")) {
        this.reportName = "resumen";
      }
      this.reportName += "prestmensuales";
      if (this.idTipoPersonal != 0L) {
        this.reportName += "tp";
      }
      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }
      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }

      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      FacesContext context = FacesContext.getCurrentInstance();

      if ((this.mes < 1) || (this.mes > 12)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));
      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }
      if (this.idTipoPersonal != 0L) {
        parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      }

      if (this.firma1 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma1);
        parameters.put("nombre_uno", firma.getNombre());
        parameters.put("cargo_uno", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_uno", "");
        else {
          parameters.put("nombramiento_uno", firma.getNombramiento());
        }
      }
      if (this.firma2 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma2);
        parameters.put("nombre_dos", firma.getNombre());
        parameters.put("cargo_dos", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_dos", "");
        else {
          parameters.put("nombramiento_dos", firma.getNombramiento());
        }
      }
      if (this.firma3 != 0L) {
        FirmasReportes firma = this.definicionesFacade.findFirmasReportesById(this.firma3);
        parameters.put("nombre_tres", firma.getNombre());
        parameters.put("cargo_tres", firma.getCargo());
        if (firma.getNombramiento() == null)
          parameters.put("nombramiento_tres", "");
        else {
          parameters.put("nombramiento_tres", firma.getNombramiento());
        }
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/prestaciones");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListFirma1() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma1, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma2() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma2, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListFirma3() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFirma3, "sigefirrhh.base.definiciones.FirmasReportes");
  }
  public Collection getListTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }

  public long getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(long l) {
    this.idRegion = l;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public long getFirma1()
  {
    return this.firma1;
  }

  public void setFirma1(long firma1)
  {
    this.firma1 = firma1;
  }

  public long getFirma2()
  {
    return this.firma2;
  }

  public void setFirma2(long firma2)
  {
    this.firma2 = firma2;
  }

  public long getFirma3()
  {
    return this.firma3;
  }

  public void setFirma3(long firma3)
  {
    this.firma3 = firma3;
  }
}