package sigefirrhh.personal.prestaciones;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPrestacionesPresupuestoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPrestacionesPresupuestoForm.class.getName());
  private int reportId;
  private long idTipoPersonal;
  private String reportName;
  private String orden;
  private int mes;
  private int anio;
  private String tipo = "1";
  private String clasificado = "1";
  private Collection listTipoPersonal;
  private EstructuraFacade estructuraFacade;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private String formato = "1";

  public ReportPrestacionesPresupuestoForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "prestmensualestpalfcat";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPrestacionesPresupuestoForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e)
    {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      if (this.tipo.equals("1"))
        this.reportName = "prestmensualestp";
      else {
        this.reportName = "cincodiastp";
      }
      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }
      if (this.clasificado.equals("1"))
        this.reportName += "cat";
      else if (this.clasificado.equals("2")) {
        this.reportName += "catuel";
      }
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      if (this.tipo.equals("1"))
        this.reportName = "prestmensualestp";
      else {
        this.reportName = "cincodiastp";
      }
      if (this.orden.equals("A"))
        this.reportName += "alf";
      else if (this.orden.equals("C"))
        this.reportName += "ced";
      else {
        this.reportName += "cod";
      }
      if (this.clasificado.equals("1"))
        this.reportName += "cat";
      else if (this.clasificado.equals("2")) {
        this.reportName += "catuel";
      }

      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      FacesContext context = FacesContext.getCurrentInstance();

      if ((this.mes < 1) || (this.mes > 12)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));

      if (this.idTipoPersonal != 0L) {
        parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      }

      JasperForWeb report = new JasperForWeb();

      if (this.formato.equals("2")) {
        report.setType(3);
      }

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/prestaciones");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public String getClasificado()
  {
    return this.clasificado;
  }

  public void setClasificado(String clasificado)
  {
    this.clasificado = clasificado;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String formato)
  {
    this.formato = formato;
  }
}