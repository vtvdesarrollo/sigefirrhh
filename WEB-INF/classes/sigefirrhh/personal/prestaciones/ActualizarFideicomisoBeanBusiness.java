package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class ActualizarFideicomisoBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(ActualizarFideicomisoBeanBusiness.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean actualizar(long idTipoPersonal, int mes, int anio, java.util.Date fecha)
    throws Exception
  {
    int mesAbono = fecha.getMonth() + 1;
    int anioAbono = fecha.getYear() + 1900;

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

    this.log.error("idTipoPersonal= " + idTipoPersonal);
    this.log.error("mes= " + mes);
    this.log.error("anio= " + anio);

    Statement stInsert = null;

    ResultSet rsPrestacionesMensuales = null;
    PreparedStatement stPrestacionesMensuales = null;

    Connection connection = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      stInsert = connection.createStatement();

      StringBuffer sql = new StringBuffer();

      if (idTipoPersonal != 0L) {
        sql = new StringBuffer();
        sql.append(" select id_prestaciones_mensuales, min(id_tipo_personal) as id_tipo_personal, min(id_trabajador) as id_trabajador, min(id_banco_fid) as id_banco, sum(monto) as monto from (");
        sql.append(" select pt.id_prestaciones_mensuales, pt.id_tipo_personal, t.id_trabajador, t.id_banco_fid, pt.monto_prestaciones as monto");
        sql.append(" from prestacionesmensuales pt, trabajador t");
        sql.append(" where pt.id_tipo_personal = ?");
        sql.append(" and pt.id_trabajador = t.id_trabajador");
        sql.append(" and pt.anio = ?");
        sql.append(" and pt.mes = ?");
        sql.append(" and fideicomiso = 'N'");
        sql.append(" union");
        sql.append(" select pt.id_prestaciones_mensuales, pt.id_tipo_personal, t.id_trabajador, t.id_banco_fid, pt.monto_adicional as monto");
        sql.append(" from prestacionesmensuales pt, trabajador t");
        sql.append(" where pt.id_tipo_personal = ?");
        sql.append(" and pt.id_trabajador = t.id_trabajador");
        sql.append(" and pt.anio = ?");
        sql.append(" and pt.mes = ?");
        sql.append(" and pt.dias_adicionales > 0");
        sql.append(" and pt.dias_cancelados = 'N') as prestaciones group by id_prestaciones_mensuales");

        stPrestacionesMensuales = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stPrestacionesMensuales.setLong(1, idTipoPersonal);
        stPrestacionesMensuales.setLong(2, anio);
        stPrestacionesMensuales.setLong(3, mes);
        stPrestacionesMensuales.setLong(4, idTipoPersonal);
        stPrestacionesMensuales.setLong(5, anio);
        stPrestacionesMensuales.setLong(6, mes);
        rsPrestacionesMensuales = stPrestacionesMensuales.executeQuery();
      } else {
        sql = new StringBuffer();
        sql.append(" select id_prestaciones_mensuales, min(id_tipo_personal), min(id_trabajador), min(id_banco_fid) as id_banco, sum(monto) as monto from (");
        sql.append(" select pt.id_prestaciones_mensuales, pt.id_tipo_personal, tid_trabajador, t.id_banco_fid, pt.monto_prestaciones as monto");
        sql.append(" from prestacionesmensuales pt, trabajador t ");
        sql.append(" where ");
        sql.append(" pt.anio = ?");
        sql.append(" pt.and mes = ?");
        sql.append(" and pt.fideicomiso = 'N'");
        sql.append(" union");
        sql.append(" select pt.id_prestaciones_mensuales, pt.id_tipo_personal, t.id_trabajador, t.id_banco_fid, pt.monto_adicional as monto");
        sql.append(" from prestacionesmensuales");
        sql.append(" where ");
        sql.append(" pt.anio = ?");
        sql.append(" and pt.mes = ?");
        sql.append(" and pt.dias_adicionales > 0");
        sql.append(" and pt.dias_cancelados = 'N') as prestaciones group by id_prestaciones_mensuales");

        stPrestacionesMensuales = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stPrestacionesMensuales.setLong(1, anio);
        stPrestacionesMensuales.setLong(2, mes);
        stPrestacionesMensuales.setLong(3, anio);
        stPrestacionesMensuales.setLong(4, mes);
        rsPrestacionesMensuales = stPrestacionesMensuales.executeQuery();
      }

      while (rsPrestacionesMensuales.next())
      {
        sql = new StringBuffer();
        sql.append("insert into fideicomiso (id_trabajador, id_tipo_personal, ");
        sql.append(" anio, mes, monto_fideicomiso, fecha, mes_abono, anio_abono, id_banco,");
        sql.append(" id_fideicomiso) values(");
        sql.append(rsPrestacionesMensuales.getLong("id_trabajador") + ", ");
        sql.append(rsPrestacionesMensuales.getLong("id_tipo_personal") + ", ");
        sql.append(anio + ", ");
        sql.append(mes + ",");
        sql.append(NumberTools.twoDecimal(rsPrestacionesMensuales.getDouble("monto")) + ",");
        sql.append("'" + fechaActualSql + "',");
        sql.append(mesAbono + ", " + anioAbono + ",");
        if (rsPrestacionesMensuales.getLong("id_banco") != 0L)
          sql.append(rsPrestacionesMensuales.getLong("id_banco") + ", ");
        else {
          sql.append("null, ");
        }
        sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.prestaciones.Fideicomiso") + ")");

        stInsert.addBatch(sql.toString());

        sql = new StringBuffer();
        sql.append("update prestacionesmensuales set fideicomiso = 'S' where id_prestaciones_mensuales =  " + rsPrestacionesMensuales.getLong("id_prestaciones_mensuales"));
        stInsert.addBatch(sql.toString());
      }

      stInsert.executeBatch();
      connection.commit();
    }
    finally {
      if (rsPrestacionesMensuales != null) try {
          rsPrestacionesMensuales.close();
        } catch (Exception localException) {
        } if (stPrestacionesMensuales != null) try {
          stPrestacionesMensuales.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return true;
  }
}