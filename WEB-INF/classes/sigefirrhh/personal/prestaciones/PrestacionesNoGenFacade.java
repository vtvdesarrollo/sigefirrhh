package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;

public class PrestacionesNoGenFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PrestacionesNoGenBusiness prestacionesNoGenBusiness = new PrestacionesNoGenBusiness();

  public SeguridadPrestaciones findUltimasPrestaciones(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      SeguridadPrestaciones seguridadPrestaciones = 
        this.prestacionesNoGenBusiness.findUltimasPrestaciones(idTipoPersonal);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadPrestaciones);
      return seguridadPrestaciones;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean calcularPrestacionesMensuales(long idTipoPersonal, int mes, int anio, long idSeguridadAniversario, String proceso, String usuario) throws Exception
  {
    try {
      this.txn.open();
      return this.prestacionesNoGenBusiness.calcularPrestacionesMensuales(idTipoPersonal, mes, anio, idSeguridadAniversario, proceso, usuario);
    }
    finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean generarPagoDiasAdicionales(long idTipoPersonal, int mes, int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesNoGenBusiness.generarPagoDiasAdicionales(idTipoPersonal, mes, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean actualizarFideicomiso(long idTipoPersonal, int mes, int anio, Date fecha)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesNoGenBusiness.actualizarFideicomiso(idTipoPersonal, mes, anio, fecha);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}