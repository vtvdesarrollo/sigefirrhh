package sigefirrhh.personal.prestaciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class SeguridadPrestaciones
  implements Serializable, PersistenceCapable
{
  private long idSeguridadPrestaciones;
  private TipoPersonal tipoPersonal;
  private int anio;
  private int mes;
  private Date fechaProceso;
  private String usuario;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "fechaProceso", "idSeguridadPrestaciones", "mes", "tipoPersonal", "usuario" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre() + "  -  " + 
      jdoGetanio(this) + "  -  " + 
      jdoGetmes(this) + "  -  " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaProceso(this));
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public long getIdSeguridadPrestaciones()
  {
    return jdoGetidSeguridadPrestaciones(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setFechaProceso(Date date)
  {
    jdoSetfechaProceso(this, date);
  }

  public void setIdSeguridadPrestaciones(long l)
  {
    jdoSetidSeguridadPrestaciones(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setUsuario(String usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.prestaciones.SeguridadPrestaciones"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadPrestaciones());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadPrestaciones localSeguridadPrestaciones = new SeguridadPrestaciones();
    localSeguridadPrestaciones.jdoFlags = 1;
    localSeguridadPrestaciones.jdoStateManager = paramStateManager;
    return localSeguridadPrestaciones;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadPrestaciones localSeguridadPrestaciones = new SeguridadPrestaciones();
    localSeguridadPrestaciones.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadPrestaciones.jdoFlags = 1;
    localSeguridadPrestaciones.jdoStateManager = paramStateManager;
    return localSeguridadPrestaciones;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadPrestaciones);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadPrestaciones = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadPrestaciones paramSeguridadPrestaciones, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadPrestaciones.anio;
      return;
    case 1:
      if (paramSeguridadPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadPrestaciones.fechaProceso;
      return;
    case 2:
      if (paramSeguridadPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadPrestaciones = paramSeguridadPrestaciones.idSeguridadPrestaciones;
      return;
    case 3:
      if (paramSeguridadPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadPrestaciones.mes;
      return;
    case 4:
      if (paramSeguridadPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramSeguridadPrestaciones.tipoPersonal;
      return;
    case 5:
      if (paramSeguridadPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadPrestaciones.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadPrestaciones))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadPrestaciones localSeguridadPrestaciones = (SeguridadPrestaciones)paramObject;
    if (localSeguridadPrestaciones.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadPrestaciones, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadPrestacionesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadPrestacionesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadPrestacionesPK))
      throw new IllegalArgumentException("arg1");
    SeguridadPrestacionesPK localSeguridadPrestacionesPK = (SeguridadPrestacionesPK)paramObject;
    localSeguridadPrestacionesPK.idSeguridadPrestaciones = this.idSeguridadPrestaciones;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadPrestacionesPK))
      throw new IllegalArgumentException("arg1");
    SeguridadPrestacionesPK localSeguridadPrestacionesPK = (SeguridadPrestacionesPK)paramObject;
    this.idSeguridadPrestaciones = localSeguridadPrestacionesPK.idSeguridadPrestaciones;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadPrestacionesPK))
      throw new IllegalArgumentException("arg2");
    SeguridadPrestacionesPK localSeguridadPrestacionesPK = (SeguridadPrestacionesPK)paramObject;
    localSeguridadPrestacionesPK.idSeguridadPrestaciones = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadPrestacionesPK))
      throw new IllegalArgumentException("arg2");
    SeguridadPrestacionesPK localSeguridadPrestacionesPK = (SeguridadPrestacionesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSeguridadPrestacionesPK.idSeguridadPrestaciones);
  }

  private static final int jdoGetanio(SeguridadPrestaciones paramSeguridadPrestaciones)
  {
    if (paramSeguridadPrestaciones.jdoFlags <= 0)
      return paramSeguridadPrestaciones.anio;
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPrestaciones.anio;
    if (localStateManager.isLoaded(paramSeguridadPrestaciones, jdoInheritedFieldCount + 0))
      return paramSeguridadPrestaciones.anio;
    return localStateManager.getIntField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 0, paramSeguridadPrestaciones.anio);
  }

  private static final void jdoSetanio(SeguridadPrestaciones paramSeguridadPrestaciones, int paramInt)
  {
    if (paramSeguridadPrestaciones.jdoFlags == 0)
    {
      paramSeguridadPrestaciones.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPrestaciones.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 0, paramSeguridadPrestaciones.anio, paramInt);
  }

  private static final Date jdoGetfechaProceso(SeguridadPrestaciones paramSeguridadPrestaciones)
  {
    if (paramSeguridadPrestaciones.jdoFlags <= 0)
      return paramSeguridadPrestaciones.fechaProceso;
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPrestaciones.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadPrestaciones, jdoInheritedFieldCount + 1))
      return paramSeguridadPrestaciones.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 1, paramSeguridadPrestaciones.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadPrestaciones paramSeguridadPrestaciones, Date paramDate)
  {
    if (paramSeguridadPrestaciones.jdoFlags == 0)
    {
      paramSeguridadPrestaciones.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPrestaciones.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 1, paramSeguridadPrestaciones.fechaProceso, paramDate);
  }

  private static final long jdoGetidSeguridadPrestaciones(SeguridadPrestaciones paramSeguridadPrestaciones)
  {
    return paramSeguridadPrestaciones.idSeguridadPrestaciones;
  }

  private static final void jdoSetidSeguridadPrestaciones(SeguridadPrestaciones paramSeguridadPrestaciones, long paramLong)
  {
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPrestaciones.idSeguridadPrestaciones = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 2, paramSeguridadPrestaciones.idSeguridadPrestaciones, paramLong);
  }

  private static final int jdoGetmes(SeguridadPrestaciones paramSeguridadPrestaciones)
  {
    if (paramSeguridadPrestaciones.jdoFlags <= 0)
      return paramSeguridadPrestaciones.mes;
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPrestaciones.mes;
    if (localStateManager.isLoaded(paramSeguridadPrestaciones, jdoInheritedFieldCount + 3))
      return paramSeguridadPrestaciones.mes;
    return localStateManager.getIntField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 3, paramSeguridadPrestaciones.mes);
  }

  private static final void jdoSetmes(SeguridadPrestaciones paramSeguridadPrestaciones, int paramInt)
  {
    if (paramSeguridadPrestaciones.jdoFlags == 0)
    {
      paramSeguridadPrestaciones.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPrestaciones.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 3, paramSeguridadPrestaciones.mes, paramInt);
  }

  private static final TipoPersonal jdoGettipoPersonal(SeguridadPrestaciones paramSeguridadPrestaciones)
  {
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPrestaciones.tipoPersonal;
    if (localStateManager.isLoaded(paramSeguridadPrestaciones, jdoInheritedFieldCount + 4))
      return paramSeguridadPrestaciones.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 4, paramSeguridadPrestaciones.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(SeguridadPrestaciones paramSeguridadPrestaciones, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPrestaciones.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 4, paramSeguridadPrestaciones.tipoPersonal, paramTipoPersonal);
  }

  private static final String jdoGetusuario(SeguridadPrestaciones paramSeguridadPrestaciones)
  {
    if (paramSeguridadPrestaciones.jdoFlags <= 0)
      return paramSeguridadPrestaciones.usuario;
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPrestaciones.usuario;
    if (localStateManager.isLoaded(paramSeguridadPrestaciones, jdoInheritedFieldCount + 5))
      return paramSeguridadPrestaciones.usuario;
    return localStateManager.getStringField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 5, paramSeguridadPrestaciones.usuario);
  }

  private static final void jdoSetusuario(SeguridadPrestaciones paramSeguridadPrestaciones, String paramString)
  {
    if (paramSeguridadPrestaciones.jdoFlags == 0)
    {
      paramSeguridadPrestaciones.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPrestaciones.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadPrestaciones, jdoInheritedFieldCount + 5, paramSeguridadPrestaciones.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}