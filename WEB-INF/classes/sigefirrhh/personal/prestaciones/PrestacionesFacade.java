package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;

public class PrestacionesFacade extends AbstractFacade
  implements Serializable
{
  static Logger log = Logger.getLogger(PrestacionesFacade.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PrestacionesBusiness prestacionesBusiness = new PrestacionesBusiness();

  public void addConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.prestacionesBusiness.addConceptoPrestaciones(conceptoPrestaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.updateConceptoPrestaciones(conceptoPrestaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.deleteConceptoPrestaciones(conceptoPrestaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoPrestaciones findConceptoPrestacionesById(long conceptoPrestacionesId) throws Exception
  {
    try { this.txn.open();
      ConceptoPrestaciones conceptoPrestaciones = 
        this.prestacionesBusiness.findConceptoPrestacionesById(conceptoPrestacionesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoPrestaciones);
      return conceptoPrestaciones;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoPrestaciones() throws Exception
  {
    try { this.txn.open();
      return this.prestacionesBusiness.findAllConceptoPrestaciones();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoPrestacionesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findConceptoPrestacionesByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addFideicomiso(Fideicomiso fideicomiso)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.prestacionesBusiness.addFideicomiso(fideicomiso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFideicomiso(Fideicomiso fideicomiso) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.updateFideicomiso(fideicomiso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFideicomiso(Fideicomiso fideicomiso) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.deleteFideicomiso(fideicomiso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Fideicomiso findFideicomisoById(long fideicomisoId) throws Exception
  {
    try { this.txn.open();
      Fideicomiso fideicomiso = 
        this.prestacionesBusiness.findFideicomisoById(fideicomisoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(fideicomiso);
      return fideicomiso;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFideicomiso() throws Exception
  {
    try { this.txn.open();
      return this.prestacionesBusiness.findAllFideicomiso();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFideicomisoByBanco(long idBanco)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findFideicomisoByBanco(idBanco);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFideicomisoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findFideicomisoByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFideicomisoByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findFideicomisoByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrestacionesMensuales(PrestacionesMensuales prestacionesMensuales)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.prestacionesBusiness.addPrestacionesMensuales(prestacionesMensuales);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrestacionesMensuales(PrestacionesMensuales prestacionesMensuales) throws Exception
  {
    try { this.txn.open();
      log.error("update 10");
      this.prestacionesBusiness.updatePrestacionesMensuales(prestacionesMensuales);
      log.error("update 11");
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrestacionesMensuales(PrestacionesMensuales prestacionesMensuales) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.deletePrestacionesMensuales(prestacionesMensuales);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PrestacionesMensuales findPrestacionesMensualesById(long prestacionesMensualesId) throws Exception
  {
    try { this.txn.open();
      PrestacionesMensuales prestacionesMensuales = 
        this.prestacionesBusiness.findPrestacionesMensualesById(prestacionesMensualesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(prestacionesMensuales);
      return prestacionesMensuales;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrestacionesMensuales() throws Exception
  {
    try { this.txn.open();
      return this.prestacionesBusiness.findAllPrestacionesMensuales();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrestacionesMensualesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findPrestacionesMensualesByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrestacionesMensualesByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findPrestacionesMensualesByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrestacionesOnapre(PrestacionesOnapre prestacionesOnapre)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.prestacionesBusiness.addPrestacionesOnapre(prestacionesOnapre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrestacionesOnapre(PrestacionesOnapre prestacionesOnapre) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.updatePrestacionesOnapre(prestacionesOnapre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrestacionesOnapre(PrestacionesOnapre prestacionesOnapre) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.deletePrestacionesOnapre(prestacionesOnapre);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PrestacionesOnapre findPrestacionesOnapreById(long prestacionesOnapreId) throws Exception
  {
    try { this.txn.open();
      PrestacionesOnapre prestacionesOnapre = 
        this.prestacionesBusiness.findPrestacionesOnapreById(prestacionesOnapreId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(prestacionesOnapre);
      return prestacionesOnapre;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrestacionesOnapre() throws Exception
  {
    try { this.txn.open();
      return this.prestacionesBusiness.findAllPrestacionesOnapre();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrestacionesOnapreByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findPrestacionesOnapreByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrestacionesOnapreByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findPrestacionesOnapreByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.prestacionesBusiness.addSeguridadPrestaciones(seguridadPrestaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.updateSeguridadPrestaciones(seguridadPrestaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones) throws Exception
  {
    try { this.txn.open();
      this.prestacionesBusiness.deleteSeguridadPrestaciones(seguridadPrestaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadPrestaciones findSeguridadPrestacionesById(long seguridadPrestacionesId) throws Exception
  {
    try { this.txn.open();
      SeguridadPrestaciones seguridadPrestaciones = 
        this.prestacionesBusiness.findSeguridadPrestacionesById(seguridadPrestacionesId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadPrestaciones);
      return seguridadPrestaciones;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadPrestaciones() throws Exception
  {
    try { this.txn.open();
      return this.prestacionesBusiness.findAllSeguridadPrestaciones();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadPrestacionesByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findSeguridadPrestacionesByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadPrestacionesByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.prestacionesBusiness.findSeguridadPrestacionesByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}