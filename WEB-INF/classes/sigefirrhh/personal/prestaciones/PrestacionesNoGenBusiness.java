package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBusiness;
import java.util.Date;

public class PrestacionesNoGenBusiness extends AbstractBusiness
{
  SeguridadPrestacionesNoGenBeanBusiness seguridadPrestacionesNoGenBeanBusiness = new SeguridadPrestacionesNoGenBeanBusiness();

  CalcularPrestacionesMensualesBeanBusiness calcularPrestacionesMensualesBeanBusiness = new CalcularPrestacionesMensualesBeanBusiness();

  GenerarPagoDiasAdicionalesBeanBusiness generarPagoDiasAdicionalesBeanBusiness = new GenerarPagoDiasAdicionalesBeanBusiness();

  ActualizarFideicomisoBeanBusiness actualizarFideicomisoBeanBusiness = new ActualizarFideicomisoBeanBusiness();

  public SeguridadPrestaciones findUltimasPrestaciones(long idTipoPersonal)
    throws Exception
  {
    return this.seguridadPrestacionesNoGenBeanBusiness.findUltimasPrestaciones(idTipoPersonal);
  }

  public boolean calcularPrestacionesMensuales(long idTipoPersonal, int mes, int anio, long idSeguridadAniversario, String periodicidad, String usuario) throws Exception
  {
    return this.calcularPrestacionesMensualesBeanBusiness.calcular(idTipoPersonal, mes, anio, idSeguridadAniversario, periodicidad, usuario);
  }

  public boolean generarPagoDiasAdicionales(long idTipoPersonal, int mes, int anio) throws Exception
  {
    return this.generarPagoDiasAdicionalesBeanBusiness.calcular(idTipoPersonal, mes, anio);
  }

  public boolean actualizarFideicomiso(long idTipoPersonal, int mes, int anio, Date fecha) throws Exception
  {
    return this.actualizarFideicomisoBeanBusiness.actualizar(idTipoPersonal, mes, anio, fecha);
  }
}