package sigefirrhh.personal.prestaciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ConceptoPrestaciones
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO_PAGO;
  protected static final Map LISTA_ONAPRE;
  private long idConceptoPrestaciones;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private String tipo;
  private String posicionOnapre;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "idConceptoPrestaciones", "idSitp", "posicionOnapre", "tiempoSitp", "tipo", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 26, 24, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.prestaciones.ConceptoPrestaciones"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoPrestaciones());

    LISTA_TIPO_PAGO = 
      new LinkedHashMap();
    LISTA_ONAPRE = 
      new LinkedHashMap();
    LISTA_TIPO_PAGO.put("B", "BASICO");
    LISTA_TIPO_PAGO.put("D", "DEVENGADO");
    LISTA_ONAPRE.put("1", "SUELDO BASICO");
    LISTA_ONAPRE.put("2", "COMPENSACION");
    LISTA_ONAPRE.put("3", "PRIMAS");
    LISTA_ONAPRE.put("4", "BONO FIN DE AÑO");
    LISTA_ONAPRE.put("5", "BONO VACACIONAL");
    LISTA_ONAPRE.put("0", "OTROS");
  }

  public ConceptoPrestaciones()
  {
    jdoSettipo(this, "B");

    jdoSetposicionOnapre(this, "B");

    jdoSetidSitp(this, 0);
  }

  public String toString()
  {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + jdoGettipo(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal)
  {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public long getIdConceptoPrestaciones()
  {
    return jdoGetidConceptoPrestaciones(this);
  }

  public void setIdConceptoPrestaciones(long idConceptoPrestaciones)
  {
    jdoSetidConceptoPrestaciones(this, idConceptoPrestaciones);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public String getTipo()
  {
    return jdoGettipo(this);
  }

  public void setTipo(String tipo)
  {
    jdoSettipo(this, tipo);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal)
  {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public String getPosicionOnapre()
  {
    return jdoGetposicionOnapre(this);
  }

  public void setPosicionOnapre(String string)
  {
    jdoSetposicionOnapre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoPrestaciones localConceptoPrestaciones = new ConceptoPrestaciones();
    localConceptoPrestaciones.jdoFlags = 1;
    localConceptoPrestaciones.jdoStateManager = paramStateManager;
    return localConceptoPrestaciones;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoPrestaciones localConceptoPrestaciones = new ConceptoPrestaciones();
    localConceptoPrestaciones.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoPrestaciones.jdoFlags = 1;
    localConceptoPrestaciones.jdoStateManager = paramStateManager;
    return localConceptoPrestaciones;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoPrestaciones);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.posicionOnapre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoPrestaciones = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.posicionOnapre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoPrestaciones paramConceptoPrestaciones, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoPrestaciones.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoPrestaciones = paramConceptoPrestaciones.idConceptoPrestaciones;
      return;
    case 2:
      if (paramConceptoPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramConceptoPrestaciones.idSitp;
      return;
    case 3:
      if (paramConceptoPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.posicionOnapre = paramConceptoPrestaciones.posicionOnapre;
      return;
    case 4:
      if (paramConceptoPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramConceptoPrestaciones.tiempoSitp;
      return;
    case 5:
      if (paramConceptoPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramConceptoPrestaciones.tipo;
      return;
    case 6:
      if (paramConceptoPrestaciones == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramConceptoPrestaciones.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoPrestaciones))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoPrestaciones localConceptoPrestaciones = (ConceptoPrestaciones)paramObject;
    if (localConceptoPrestaciones.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoPrestaciones, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoPrestacionesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoPrestacionesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoPrestacionesPK))
      throw new IllegalArgumentException("arg1");
    ConceptoPrestacionesPK localConceptoPrestacionesPK = (ConceptoPrestacionesPK)paramObject;
    localConceptoPrestacionesPK.idConceptoPrestaciones = this.idConceptoPrestaciones;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoPrestacionesPK))
      throw new IllegalArgumentException("arg1");
    ConceptoPrestacionesPK localConceptoPrestacionesPK = (ConceptoPrestacionesPK)paramObject;
    this.idConceptoPrestaciones = localConceptoPrestacionesPK.idConceptoPrestaciones;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoPrestacionesPK))
      throw new IllegalArgumentException("arg2");
    ConceptoPrestacionesPK localConceptoPrestacionesPK = (ConceptoPrestacionesPK)paramObject;
    localConceptoPrestacionesPK.idConceptoPrestaciones = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoPrestacionesPK))
      throw new IllegalArgumentException("arg2");
    ConceptoPrestacionesPK localConceptoPrestacionesPK = (ConceptoPrestacionesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localConceptoPrestacionesPK.idConceptoPrestaciones);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoPrestaciones paramConceptoPrestaciones)
  {
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoPrestaciones.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoPrestaciones, jdoInheritedFieldCount + 0))
      return paramConceptoPrestaciones.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoPrestaciones, jdoInheritedFieldCount + 0, paramConceptoPrestaciones.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoPrestaciones paramConceptoPrestaciones, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoPrestaciones.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoPrestaciones, jdoInheritedFieldCount + 0, paramConceptoPrestaciones.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidConceptoPrestaciones(ConceptoPrestaciones paramConceptoPrestaciones)
  {
    return paramConceptoPrestaciones.idConceptoPrestaciones;
  }

  private static final void jdoSetidConceptoPrestaciones(ConceptoPrestaciones paramConceptoPrestaciones, long paramLong)
  {
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoPrestaciones.idConceptoPrestaciones = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoPrestaciones, jdoInheritedFieldCount + 1, paramConceptoPrestaciones.idConceptoPrestaciones, paramLong);
  }

  private static final int jdoGetidSitp(ConceptoPrestaciones paramConceptoPrestaciones)
  {
    if (paramConceptoPrestaciones.jdoFlags <= 0)
      return paramConceptoPrestaciones.idSitp;
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoPrestaciones.idSitp;
    if (localStateManager.isLoaded(paramConceptoPrestaciones, jdoInheritedFieldCount + 2))
      return paramConceptoPrestaciones.idSitp;
    return localStateManager.getIntField(paramConceptoPrestaciones, jdoInheritedFieldCount + 2, paramConceptoPrestaciones.idSitp);
  }

  private static final void jdoSetidSitp(ConceptoPrestaciones paramConceptoPrestaciones, int paramInt)
  {
    if (paramConceptoPrestaciones.jdoFlags == 0)
    {
      paramConceptoPrestaciones.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoPrestaciones.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoPrestaciones, jdoInheritedFieldCount + 2, paramConceptoPrestaciones.idSitp, paramInt);
  }

  private static final String jdoGetposicionOnapre(ConceptoPrestaciones paramConceptoPrestaciones)
  {
    if (paramConceptoPrestaciones.jdoFlags <= 0)
      return paramConceptoPrestaciones.posicionOnapre;
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoPrestaciones.posicionOnapre;
    if (localStateManager.isLoaded(paramConceptoPrestaciones, jdoInheritedFieldCount + 3))
      return paramConceptoPrestaciones.posicionOnapre;
    return localStateManager.getStringField(paramConceptoPrestaciones, jdoInheritedFieldCount + 3, paramConceptoPrestaciones.posicionOnapre);
  }

  private static final void jdoSetposicionOnapre(ConceptoPrestaciones paramConceptoPrestaciones, String paramString)
  {
    if (paramConceptoPrestaciones.jdoFlags == 0)
    {
      paramConceptoPrestaciones.posicionOnapre = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoPrestaciones.posicionOnapre = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoPrestaciones, jdoInheritedFieldCount + 3, paramConceptoPrestaciones.posicionOnapre, paramString);
  }

  private static final Date jdoGettiempoSitp(ConceptoPrestaciones paramConceptoPrestaciones)
  {
    if (paramConceptoPrestaciones.jdoFlags <= 0)
      return paramConceptoPrestaciones.tiempoSitp;
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoPrestaciones.tiempoSitp;
    if (localStateManager.isLoaded(paramConceptoPrestaciones, jdoInheritedFieldCount + 4))
      return paramConceptoPrestaciones.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramConceptoPrestaciones, jdoInheritedFieldCount + 4, paramConceptoPrestaciones.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ConceptoPrestaciones paramConceptoPrestaciones, Date paramDate)
  {
    if (paramConceptoPrestaciones.jdoFlags == 0)
    {
      paramConceptoPrestaciones.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoPrestaciones.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConceptoPrestaciones, jdoInheritedFieldCount + 4, paramConceptoPrestaciones.tiempoSitp, paramDate);
  }

  private static final String jdoGettipo(ConceptoPrestaciones paramConceptoPrestaciones)
  {
    if (paramConceptoPrestaciones.jdoFlags <= 0)
      return paramConceptoPrestaciones.tipo;
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoPrestaciones.tipo;
    if (localStateManager.isLoaded(paramConceptoPrestaciones, jdoInheritedFieldCount + 5))
      return paramConceptoPrestaciones.tipo;
    return localStateManager.getStringField(paramConceptoPrestaciones, jdoInheritedFieldCount + 5, paramConceptoPrestaciones.tipo);
  }

  private static final void jdoSettipo(ConceptoPrestaciones paramConceptoPrestaciones, String paramString)
  {
    if (paramConceptoPrestaciones.jdoFlags == 0)
    {
      paramConceptoPrestaciones.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoPrestaciones.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoPrestaciones, jdoInheritedFieldCount + 5, paramConceptoPrestaciones.tipo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(ConceptoPrestaciones paramConceptoPrestaciones)
  {
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoPrestaciones.tipoPersonal;
    if (localStateManager.isLoaded(paramConceptoPrestaciones, jdoInheritedFieldCount + 6))
      return paramConceptoPrestaciones.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramConceptoPrestaciones, jdoInheritedFieldCount + 6, paramConceptoPrestaciones.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ConceptoPrestaciones paramConceptoPrestaciones, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramConceptoPrestaciones.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoPrestaciones.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoPrestaciones, jdoInheritedFieldCount + 6, paramConceptoPrestaciones.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}