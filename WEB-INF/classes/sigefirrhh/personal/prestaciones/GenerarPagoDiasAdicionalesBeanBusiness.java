package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class GenerarPagoDiasAdicionalesBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(GenerarPagoDiasAdicionalesBeanBusiness.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean calcular(long idTipoPersonal, int mes, int anio)
    throws Exception
  {
    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());

    this.log.error("idTipoPersonal= " + idTipoPersonal);
    this.log.error("mes= " + mes);
    this.log.error("anio= " + anio);

    Statement stInsert = null;

    ResultSet rsPrestacionesMensuales = null;
    PreparedStatement stPrestacionesMensuales = null;

    ResultSet rsConcepto = null;
    PreparedStatement stConcepto = null;

    Connection connection = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      stInsert = connection.createStatement();

      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal, ctp.id_frecuencia_tipo_personal ");
      sql.append(" from conceptotipopersonal ctp, concepto c ");
      sql.append(" where ctp.id_concepto = c.id_concepto");
      sql.append("  and ctp.id_tipo_personal = ?");
      sql.append("  and c.cod_concepto = '1805' ");

      stConcepto = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConcepto.setLong(1, idTipoPersonal);
      rsConcepto = stConcepto.executeQuery();

      if (!rsConcepto.next())
      {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("El Concepto 1805 no esta asociado al tipo de personal seleccionado");
        throw error;
      }

      sql = new StringBuffer();

      sql.append("select id_prestaciones_mensuales, id_trabajador, monto_adicional, dias_adicionales ");
      sql.append(" from prestacionesmensuales  ");
      sql.append(" where id_tipo_personal = ?");
      sql.append(" and anio = ?");
      sql.append(" and mes = ?");
      sql.append(" and dias_adicionales > 0");
      sql.append(" and dias_cancelados = 'N'");

      stPrestacionesMensuales = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stPrestacionesMensuales.setLong(1, idTipoPersonal);
      stPrestacionesMensuales.setLong(2, anio);
      stPrestacionesMensuales.setLong(3, mes);
      rsPrestacionesMensuales = stPrestacionesMensuales.executeQuery();

      while (rsPrestacionesMensuales.next())
      {
        sql = new StringBuffer();
        sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
        sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
        sql.append(" estatus, id_concepto_variable) values(");
        sql.append(rsPrestacionesMensuales.getLong("id_trabajador") + ", ");
        sql.append(rsConcepto.getLong("id_concepto_tipo_personal") + ", ");
        sql.append(rsConcepto.getLong("id_frecuencia_tipo_personal") + ", ");
        sql.append(rsPrestacionesMensuales.getDouble("dias_adicionales") + ",");
        sql.append(rsPrestacionesMensuales.getDouble("monto_adicional") + ",");
        sql.append("'" + fechaActualSql + "',");
        sql.append("'A', ");
        sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

        stInsert.addBatch(sql.toString());

        sql = new StringBuffer();
        sql.append("update prestacionesmensuales set dias_cancelados = 'S' where id_prestaciones_mensuales =  " + rsPrestacionesMensuales.getLong("id_prestaciones_mensuales"));
        stInsert.addBatch(sql.toString());
      }

      stInsert.executeBatch();
      connection.commit();
    } finally {
      if (rsPrestacionesMensuales != null) try {
          rsPrestacionesMensuales.close();
        } catch (Exception localException) {
        } if (rsConcepto != null) try {
          rsConcepto.close();
        }
        catch (Exception localException1) {
        } if (stPrestacionesMensuales != null) try {
          stPrestacionesMensuales.close();
        } catch (Exception localException2) {
        } if (stConcepto != null) try {
          stConcepto.close();
        }
        catch (Exception localException3) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException4) {  }
 
    }
    return true;
  }
}