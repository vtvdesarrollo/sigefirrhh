package sigefirrhh.personal.prestaciones;

import java.io.Serializable;

public class PrestacionesMensualesPK
  implements Serializable
{
  public long idPrestacionesMensuales;

  public PrestacionesMensualesPK()
  {
  }

  public PrestacionesMensualesPK(long idPrestacionesMensuales)
  {
    this.idPrestacionesMensuales = idPrestacionesMensuales;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrestacionesMensualesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrestacionesMensualesPK thatPK)
  {
    return 
      this.idPrestacionesMensuales == thatPK.idPrestacionesMensuales;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrestacionesMensuales)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrestacionesMensuales);
  }
}