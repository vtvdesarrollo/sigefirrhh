package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ConceptoPrestacionesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoPrestaciones conceptoPrestacionesNew = 
      (ConceptoPrestaciones)BeanUtils.cloneBean(
      conceptoPrestaciones);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoPrestacionesNew.getTipoPersonal() != null) {
      conceptoPrestacionesNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoPrestacionesNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoPrestacionesNew.getConceptoTipoPersonal() != null) {
      conceptoPrestacionesNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoPrestacionesNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(conceptoPrestacionesNew);
  }

  public void updateConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones) throws Exception
  {
    ConceptoPrestaciones conceptoPrestacionesModify = 
      findConceptoPrestacionesById(conceptoPrestaciones.getIdConceptoPrestaciones());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoPrestaciones.getTipoPersonal() != null) {
      conceptoPrestaciones.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoPrestaciones.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoPrestaciones.getConceptoTipoPersonal() != null) {
      conceptoPrestaciones.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoPrestaciones.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(conceptoPrestacionesModify, conceptoPrestaciones);
  }

  public void deleteConceptoPrestaciones(ConceptoPrestaciones conceptoPrestaciones) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoPrestaciones conceptoPrestacionesDelete = 
      findConceptoPrestacionesById(conceptoPrestaciones.getIdConceptoPrestaciones());
    pm.deletePersistent(conceptoPrestacionesDelete);
  }

  public ConceptoPrestaciones findConceptoPrestacionesById(long idConceptoPrestaciones) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoPrestaciones == pIdConceptoPrestaciones";
    Query query = pm.newQuery(ConceptoPrestaciones.class, filter);

    query.declareParameters("long pIdConceptoPrestaciones");

    parameters.put("pIdConceptoPrestaciones", new Long(idConceptoPrestaciones));

    Collection colConceptoPrestaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoPrestaciones.iterator();
    return (ConceptoPrestaciones)iterator.next();
  }

  public Collection findConceptoPrestacionesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoPrestacionesExtent = pm.getExtent(
      ConceptoPrestaciones.class, true);
    Query query = pm.newQuery(conceptoPrestacionesExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ConceptoPrestaciones.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoPrestaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoPrestaciones);

    return colConceptoPrestaciones;
  }
}