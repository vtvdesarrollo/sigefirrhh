package sigefirrhh.personal.prestaciones;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPrestacionesTrabajadorGeneralForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPrestacionesTrabajadorGeneralForm.class.getName());
  private int reportId;
  private long idRegion;
  private long idTipoPersonal;
  private String reportName;
  private String orden;
  private int mes;
  private int anio;
  private String formato = "1";
  private Collection listRegion;
  private Collection listTipoPersonal;
  private EstructuraFacade estructuraFacade;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;

  public ReportPrestacionesTrabajadorGeneralForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "prestmensualesalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPrestacionesTrabajadorGeneralForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRegion = this.estructuraFacade.findAllRegion();

      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listRegion = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try {
      if ((this.idTipoPersonal == 0L) && (this.idRegion == 0L))
        this.reportName = "estadocuentaregtp";
      else if ((this.idTipoPersonal != 0L) && (this.idRegion == 0L))
        this.reportName = "estadocuentaregtp1";
      else if ((this.idTipoPersonal == 0L) && (this.idRegion != 0L))
        this.reportName = "estadocuentareg1tp";
      else if ((this.idTipoPersonal != 0L) && (this.idRegion != 0L))
        this.reportName = "estadocuentareg1tp1";
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      if ((this.idTipoPersonal == 0L) && (this.idRegion == 0L))
        this.reportName = "estadocuentaregtp";
      else if ((this.idTipoPersonal != 0L) && (this.idRegion == 0L))
        this.reportName = "estadocuentaregtp1";
      else if ((this.idTipoPersonal == 0L) && (this.idRegion != 0L))
        this.reportName = "estadocuentareg1tp";
      else if ((this.idTipoPersonal != 0L) && (this.idRegion != 0L)) {
        this.reportName = "estadocuentareg1tp1";
      }

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));

      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }
      if (this.idTipoPersonal != 0L) {
        parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      }

      JasperForWeb report = new JasperForWeb();

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/prestaciones");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }

  public long getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(long l) {
    this.idRegion = l;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
}