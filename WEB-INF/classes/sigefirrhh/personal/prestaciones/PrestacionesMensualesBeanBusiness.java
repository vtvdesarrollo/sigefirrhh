package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class PrestacionesMensualesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrestacionesMensuales(PrestacionesMensuales prestacionesMensuales)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PrestacionesMensuales prestacionesMensualesNew = 
      (PrestacionesMensuales)BeanUtils.cloneBean(
      prestacionesMensuales);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (prestacionesMensualesNew.getTrabajador() != null)
    {
      prestacionesMensualesNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        prestacionesMensualesNew.getTrabajador().getTipoPersonal().getIdTipoPersonal()));

      prestacionesMensualesNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        prestacionesMensualesNew.getTrabajador().getIdTrabajador()));

      prestacionesMensualesNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        prestacionesMensualesNew.getTrabajador().getPersonal().getIdPersonal()));
    }

    pm.makePersistent(prestacionesMensualesNew);
  }

  public void updatePrestacionesMensuales(PrestacionesMensuales prestacionesMensuales)
    throws Exception
  {
    PrestacionesMensuales prestacionesMensualesModify = 
      findPrestacionesMensualesById(prestacionesMensuales.getIdPrestacionesMensuales());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (prestacionesMensuales.getTrabajador() != null)
    {
      prestacionesMensuales.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        prestacionesMensuales.getTipoPersonal().getIdTipoPersonal()));
      prestacionesMensuales.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        prestacionesMensuales.getTrabajador().getIdTrabajador()));
      prestacionesMensuales.setPersonal(
        personalBeanBusiness.findPersonalById(
        prestacionesMensuales.getTrabajador().getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(prestacionesMensualesModify, prestacionesMensuales);
  }

  public void deletePrestacionesMensuales(PrestacionesMensuales prestacionesMensuales) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    PrestacionesMensuales prestacionesMensualesDelete = 
      findPrestacionesMensualesById(prestacionesMensuales.getIdPrestacionesMensuales());
    pm.deletePersistent(prestacionesMensualesDelete);
  }

  public PrestacionesMensuales findPrestacionesMensualesById(long idPrestacionesMensuales) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrestacionesMensuales == pIdPrestacionesMensuales";
    Query query = pm.newQuery(PrestacionesMensuales.class, filter);

    query.declareParameters("long pIdPrestacionesMensuales");

    parameters.put("pIdPrestacionesMensuales", new Long(idPrestacionesMensuales));

    Collection colPrestacionesMensuales = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrestacionesMensuales.iterator();
    return (PrestacionesMensuales)iterator.next();
  }

  public Collection findPrestacionesMensualesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent prestacionesMensualesExtent = pm.getExtent(
      PrestacionesMensuales.class, true);
    Query query = pm.newQuery(prestacionesMensualesExtent);
    query.setOrdering("anio descending, mes descending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(PrestacionesMensuales.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio descending, mes descending");

    Collection colPrestacionesMensuales = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrestacionesMensuales);

    return colPrestacionesMensuales;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(PrestacionesMensuales.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("anio descending, mes descending");

    Collection colPrestacionesMensuales = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrestacionesMensuales);

    return colPrestacionesMensuales;
  }
}