package sigefirrhh.personal.prestaciones;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.trabajador.Trabajador;

public class PrestacionesMensuales
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_FIDEICOMISO;
  protected static final Map LISTA_CANCELADO;
  private long idPrestacionesMensuales;
  private int anio;
  private int mes;
  private double diasMensuales;
  private int diasAdicionales;
  private double baseMensual;
  private double baseAdicional;
  private double montoPrestaciones;
  private double montoAdicional;
  private String fideicomiso;
  private String diasCancelados;
  private TipoPersonal tipoPersonal;
  private Trabajador trabajador;
  private Personal personal;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "baseAdicional", "baseMensual", "diasAdicionales", "diasCancelados", "diasMensuales", "fideicomiso", "idPrestacionesMensuales", "idSitp", "mes", "montoAdicional", "montoPrestaciones", "personal", "tiempoSitp", "tipoPersonal", "trabajador" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.prestaciones.PrestacionesMensuales"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrestacionesMensuales());

    LISTA_FIDEICOMISO = 
      new LinkedHashMap();
    LISTA_CANCELADO = 
      new LinkedHashMap();

    LISTA_FIDEICOMISO.put("S", "SI");
    LISTA_FIDEICOMISO.put("N", "NO");
    LISTA_CANCELADO.put("S", "SI");
    LISTA_CANCELADO.put("N", "NO");
  }

  public PrestacionesMensuales()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetdiasMensuales(this, 5.0D);

    jdoSetdiasAdicionales(this, 0);

    jdoSetbaseMensual(this, 0.0D);

    jdoSetbaseAdicional(this, 0.0D);

    jdoSetmontoPrestaciones(this, 0.0D);

    jdoSetmontoAdicional(this, 0.0D);

    jdoSetfideicomiso(this, "N");

    jdoSetdiasCancelados(this, "N");

    jdoSetidSitp(this, 0);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoPrestaciones(this));
    String c = b.format(jdoGetmontoAdicional(this));

    return jdoGetanio(this) + " - " + jdoGetmes(this) + " - Abono Mensual: " + a + " - Monto Dias Adicionales: " + c;
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public double getBaseAdicional()
  {
    return jdoGetbaseAdicional(this);
  }

  public double getBaseMensual()
  {
    return jdoGetbaseMensual(this);
  }

  public double getDiasAdicionales()
  {
    return jdoGetdiasAdicionales(this);
  }

  public double getDiasMensuales()
  {
    return jdoGetdiasMensuales(this);
  }

  public long getIdPrestacionesMensuales()
  {
    return jdoGetidPrestacionesMensuales(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public double getMontoAdicional()
  {
    return jdoGetmontoAdicional(this);
  }

  public double getMontoPrestaciones()
  {
    return jdoGetmontoPrestaciones(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setBaseAdicional(double d)
  {
    jdoSetbaseAdicional(this, d);
  }

  public void setBaseMensual(double d)
  {
    jdoSetbaseMensual(this, d);
  }

  public void setDiasAdicionales(int i)
  {
    jdoSetdiasAdicionales(this, i);
  }

  public void setDiasMensuales(double i)
  {
    jdoSetdiasMensuales(this, i);
  }

  public void setIdPrestacionesMensuales(long l)
  {
    jdoSetidPrestacionesMensuales(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setMontoAdicional(double d)
  {
    jdoSetmontoAdicional(this, d);
  }

  public void setMontoPrestaciones(double d)
  {
    jdoSetmontoPrestaciones(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public String getDiasCancelados()
  {
    return jdoGetdiasCancelados(this);
  }

  public void setDiasCancelados(String diasCancelados)
  {
    jdoSetdiasCancelados(this, diasCancelados);
  }

  public String getFideicomiso()
  {
    return jdoGetfideicomiso(this);
  }

  public void setFideicomiso(String fideicomiso)
  {
    jdoSetfideicomiso(this, fideicomiso);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 16;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrestacionesMensuales localPrestacionesMensuales = new PrestacionesMensuales();
    localPrestacionesMensuales.jdoFlags = 1;
    localPrestacionesMensuales.jdoStateManager = paramStateManager;
    return localPrestacionesMensuales;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrestacionesMensuales localPrestacionesMensuales = new PrestacionesMensuales();
    localPrestacionesMensuales.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrestacionesMensuales.jdoFlags = 1;
    localPrestacionesMensuales.jdoStateManager = paramStateManager;
    return localPrestacionesMensuales;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseAdicional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseMensual);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasAdicionales);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.diasCancelados);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.diasMensuales);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.fideicomiso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrestacionesMensuales);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAdicional);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPrestaciones);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseAdicional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasAdicionales = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasCancelados = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasMensuales = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fideicomiso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrestacionesMensuales = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAdicional = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPrestaciones = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrestacionesMensuales paramPrestacionesMensuales, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPrestacionesMensuales.anio;
      return;
    case 1:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.baseAdicional = paramPrestacionesMensuales.baseAdicional;
      return;
    case 2:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.baseMensual = paramPrestacionesMensuales.baseMensual;
      return;
    case 3:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.diasAdicionales = paramPrestacionesMensuales.diasAdicionales;
      return;
    case 4:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.diasCancelados = paramPrestacionesMensuales.diasCancelados;
      return;
    case 5:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.diasMensuales = paramPrestacionesMensuales.diasMensuales;
      return;
    case 6:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.fideicomiso = paramPrestacionesMensuales.fideicomiso;
      return;
    case 7:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.idPrestacionesMensuales = paramPrestacionesMensuales.idPrestacionesMensuales;
      return;
    case 8:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPrestacionesMensuales.idSitp;
      return;
    case 9:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramPrestacionesMensuales.mes;
      return;
    case 10:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.montoAdicional = paramPrestacionesMensuales.montoAdicional;
      return;
    case 11:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.montoPrestaciones = paramPrestacionesMensuales.montoPrestaciones;
      return;
    case 12:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramPrestacionesMensuales.personal;
      return;
    case 13:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPrestacionesMensuales.tiempoSitp;
      return;
    case 14:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPrestacionesMensuales.tipoPersonal;
      return;
    case 15:
      if (paramPrestacionesMensuales == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramPrestacionesMensuales.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrestacionesMensuales))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrestacionesMensuales localPrestacionesMensuales = (PrestacionesMensuales)paramObject;
    if (localPrestacionesMensuales.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrestacionesMensuales, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrestacionesMensualesPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrestacionesMensualesPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesMensualesPK))
      throw new IllegalArgumentException("arg1");
    PrestacionesMensualesPK localPrestacionesMensualesPK = (PrestacionesMensualesPK)paramObject;
    localPrestacionesMensualesPK.idPrestacionesMensuales = this.idPrestacionesMensuales;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrestacionesMensualesPK))
      throw new IllegalArgumentException("arg1");
    PrestacionesMensualesPK localPrestacionesMensualesPK = (PrestacionesMensualesPK)paramObject;
    this.idPrestacionesMensuales = localPrestacionesMensualesPK.idPrestacionesMensuales;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesMensualesPK))
      throw new IllegalArgumentException("arg2");
    PrestacionesMensualesPK localPrestacionesMensualesPK = (PrestacionesMensualesPK)paramObject;
    localPrestacionesMensualesPK.idPrestacionesMensuales = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrestacionesMensualesPK))
      throw new IllegalArgumentException("arg2");
    PrestacionesMensualesPK localPrestacionesMensualesPK = (PrestacionesMensualesPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localPrestacionesMensualesPK.idPrestacionesMensuales);
  }

  private static final int jdoGetanio(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.anio;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.anio;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 0))
      return paramPrestacionesMensuales.anio;
    return localStateManager.getIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 0, paramPrestacionesMensuales.anio);
  }

  private static final void jdoSetanio(PrestacionesMensuales paramPrestacionesMensuales, int paramInt)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 0, paramPrestacionesMensuales.anio, paramInt);
  }

  private static final double jdoGetbaseAdicional(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.baseAdicional;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.baseAdicional;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 1))
      return paramPrestacionesMensuales.baseAdicional;
    return localStateManager.getDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 1, paramPrestacionesMensuales.baseAdicional);
  }

  private static final void jdoSetbaseAdicional(PrestacionesMensuales paramPrestacionesMensuales, double paramDouble)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.baseAdicional = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.baseAdicional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 1, paramPrestacionesMensuales.baseAdicional, paramDouble);
  }

  private static final double jdoGetbaseMensual(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.baseMensual;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.baseMensual;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 2))
      return paramPrestacionesMensuales.baseMensual;
    return localStateManager.getDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 2, paramPrestacionesMensuales.baseMensual);
  }

  private static final void jdoSetbaseMensual(PrestacionesMensuales paramPrestacionesMensuales, double paramDouble)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.baseMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.baseMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 2, paramPrestacionesMensuales.baseMensual, paramDouble);
  }

  private static final int jdoGetdiasAdicionales(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.diasAdicionales;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.diasAdicionales;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 3))
      return paramPrestacionesMensuales.diasAdicionales;
    return localStateManager.getIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 3, paramPrestacionesMensuales.diasAdicionales);
  }

  private static final void jdoSetdiasAdicionales(PrestacionesMensuales paramPrestacionesMensuales, int paramInt)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.diasAdicionales = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.diasAdicionales = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 3, paramPrestacionesMensuales.diasAdicionales, paramInt);
  }

  private static final String jdoGetdiasCancelados(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.diasCancelados;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.diasCancelados;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 4))
      return paramPrestacionesMensuales.diasCancelados;
    return localStateManager.getStringField(paramPrestacionesMensuales, jdoInheritedFieldCount + 4, paramPrestacionesMensuales.diasCancelados);
  }

  private static final void jdoSetdiasCancelados(PrestacionesMensuales paramPrestacionesMensuales, String paramString)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.diasCancelados = paramString;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.diasCancelados = paramString;
      return;
    }
    localStateManager.setStringField(paramPrestacionesMensuales, jdoInheritedFieldCount + 4, paramPrestacionesMensuales.diasCancelados, paramString);
  }

  private static final double jdoGetdiasMensuales(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.diasMensuales;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.diasMensuales;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 5))
      return paramPrestacionesMensuales.diasMensuales;
    return localStateManager.getDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 5, paramPrestacionesMensuales.diasMensuales);
  }

  private static final void jdoSetdiasMensuales(PrestacionesMensuales paramPrestacionesMensuales, double paramDouble)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.diasMensuales = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.diasMensuales = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 5, paramPrestacionesMensuales.diasMensuales, paramDouble);
  }

  private static final String jdoGetfideicomiso(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.fideicomiso;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.fideicomiso;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 6))
      return paramPrestacionesMensuales.fideicomiso;
    return localStateManager.getStringField(paramPrestacionesMensuales, jdoInheritedFieldCount + 6, paramPrestacionesMensuales.fideicomiso);
  }

  private static final void jdoSetfideicomiso(PrestacionesMensuales paramPrestacionesMensuales, String paramString)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.fideicomiso = paramString;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.fideicomiso = paramString;
      return;
    }
    localStateManager.setStringField(paramPrestacionesMensuales, jdoInheritedFieldCount + 6, paramPrestacionesMensuales.fideicomiso, paramString);
  }

  private static final long jdoGetidPrestacionesMensuales(PrestacionesMensuales paramPrestacionesMensuales)
  {
    return paramPrestacionesMensuales.idPrestacionesMensuales;
  }

  private static final void jdoSetidPrestacionesMensuales(PrestacionesMensuales paramPrestacionesMensuales, long paramLong)
  {
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.idPrestacionesMensuales = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrestacionesMensuales, jdoInheritedFieldCount + 7, paramPrestacionesMensuales.idPrestacionesMensuales, paramLong);
  }

  private static final int jdoGetidSitp(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.idSitp;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.idSitp;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 8))
      return paramPrestacionesMensuales.idSitp;
    return localStateManager.getIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 8, paramPrestacionesMensuales.idSitp);
  }

  private static final void jdoSetidSitp(PrestacionesMensuales paramPrestacionesMensuales, int paramInt)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 8, paramPrestacionesMensuales.idSitp, paramInt);
  }

  private static final int jdoGetmes(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.mes;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.mes;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 9))
      return paramPrestacionesMensuales.mes;
    return localStateManager.getIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 9, paramPrestacionesMensuales.mes);
  }

  private static final void jdoSetmes(PrestacionesMensuales paramPrestacionesMensuales, int paramInt)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramPrestacionesMensuales, jdoInheritedFieldCount + 9, paramPrestacionesMensuales.mes, paramInt);
  }

  private static final double jdoGetmontoAdicional(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.montoAdicional;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.montoAdicional;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 10))
      return paramPrestacionesMensuales.montoAdicional;
    return localStateManager.getDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 10, paramPrestacionesMensuales.montoAdicional);
  }

  private static final void jdoSetmontoAdicional(PrestacionesMensuales paramPrestacionesMensuales, double paramDouble)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.montoAdicional = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.montoAdicional = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 10, paramPrestacionesMensuales.montoAdicional, paramDouble);
  }

  private static final double jdoGetmontoPrestaciones(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.montoPrestaciones;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.montoPrestaciones;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 11))
      return paramPrestacionesMensuales.montoPrestaciones;
    return localStateManager.getDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 11, paramPrestacionesMensuales.montoPrestaciones);
  }

  private static final void jdoSetmontoPrestaciones(PrestacionesMensuales paramPrestacionesMensuales, double paramDouble)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.montoPrestaciones = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.montoPrestaciones = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrestacionesMensuales, jdoInheritedFieldCount + 11, paramPrestacionesMensuales.montoPrestaciones, paramDouble);
  }

  private static final Personal jdoGetpersonal(PrestacionesMensuales paramPrestacionesMensuales)
  {
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.personal;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 12))
      return paramPrestacionesMensuales.personal;
    return (Personal)localStateManager.getObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 12, paramPrestacionesMensuales.personal);
  }

  private static final void jdoSetpersonal(PrestacionesMensuales paramPrestacionesMensuales, Personal paramPersonal)
  {
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 12, paramPrestacionesMensuales.personal, paramPersonal);
  }

  private static final Date jdoGettiempoSitp(PrestacionesMensuales paramPrestacionesMensuales)
  {
    if (paramPrestacionesMensuales.jdoFlags <= 0)
      return paramPrestacionesMensuales.tiempoSitp;
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.tiempoSitp;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 13))
      return paramPrestacionesMensuales.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 13, paramPrestacionesMensuales.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(PrestacionesMensuales paramPrestacionesMensuales, Date paramDate)
  {
    if (paramPrestacionesMensuales.jdoFlags == 0)
    {
      paramPrestacionesMensuales.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 13, paramPrestacionesMensuales.tiempoSitp, paramDate);
  }

  private static final TipoPersonal jdoGettipoPersonal(PrestacionesMensuales paramPrestacionesMensuales)
  {
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.tipoPersonal;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 14))
      return paramPrestacionesMensuales.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 14, paramPrestacionesMensuales.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PrestacionesMensuales paramPrestacionesMensuales, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 14, paramPrestacionesMensuales.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(PrestacionesMensuales paramPrestacionesMensuales)
  {
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
      return paramPrestacionesMensuales.trabajador;
    if (localStateManager.isLoaded(paramPrestacionesMensuales, jdoInheritedFieldCount + 15))
      return paramPrestacionesMensuales.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 15, paramPrestacionesMensuales.trabajador);
  }

  private static final void jdoSettrabajador(PrestacionesMensuales paramPrestacionesMensuales, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramPrestacionesMensuales.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrestacionesMensuales.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramPrestacionesMensuales, jdoInheritedFieldCount + 15, paramPrestacionesMensuales.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}