package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SeguridadPrestacionesNoGenBeanBusiness extends AbstractBeanBusiness
{
  public SeguridadPrestaciones findUltimasPrestaciones(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal ";

    Query query = pm.newQuery(SeguridadPrestaciones.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio descending, mes descending");

    Collection colSeguridadPrestaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadPrestaciones);
    Iterator iterSeguridadPrestaciones = colSeguridadPrestaciones.iterator();

    return (SeguridadPrestaciones)iterSeguridadPrestaciones.next();
  }
}