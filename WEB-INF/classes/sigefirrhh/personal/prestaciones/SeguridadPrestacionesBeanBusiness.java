package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class SeguridadPrestacionesBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadPrestaciones seguridadPrestacionesNew = 
      (SeguridadPrestaciones)BeanUtils.cloneBean(
      seguridadPrestaciones);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadPrestacionesNew.getTipoPersonal() != null) {
      seguridadPrestacionesNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadPrestacionesNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(seguridadPrestacionesNew);
  }

  public void updateSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones) throws Exception
  {
    SeguridadPrestaciones seguridadPrestacionesModify = 
      findSeguridadPrestacionesById(seguridadPrestaciones.getIdSeguridadPrestaciones());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadPrestaciones.getTipoPersonal() != null) {
      seguridadPrestaciones.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadPrestaciones.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(seguridadPrestacionesModify, seguridadPrestaciones);
  }

  public void deleteSeguridadPrestaciones(SeguridadPrestaciones seguridadPrestaciones) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadPrestaciones seguridadPrestacionesDelete = 
      findSeguridadPrestacionesById(seguridadPrestaciones.getIdSeguridadPrestaciones());
    pm.deletePersistent(seguridadPrestacionesDelete);
  }

  public SeguridadPrestaciones findSeguridadPrestacionesById(long idSeguridadPrestaciones) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadPrestaciones == pIdSeguridadPrestaciones";
    Query query = pm.newQuery(SeguridadPrestaciones.class, filter);

    query.declareParameters("long pIdSeguridadPrestaciones");

    parameters.put("pIdSeguridadPrestaciones", new Long(idSeguridadPrestaciones));

    Collection colSeguridadPrestaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadPrestaciones.iterator();
    return (SeguridadPrestaciones)iterator.next();
  }

  public Collection findSeguridadPrestacionesAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadPrestacionesExtent = pm.getExtent(
      SeguridadPrestaciones.class, true);
    Query query = pm.newQuery(seguridadPrestacionesExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(SeguridadPrestaciones.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colSeguridadPrestaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadPrestaciones);

    return colSeguridadPrestaciones;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(SeguridadPrestaciones.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, mes ascending");

    Collection colSeguridadPrestaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadPrestaciones);

    return colSeguridadPrestaciones;
  }
}