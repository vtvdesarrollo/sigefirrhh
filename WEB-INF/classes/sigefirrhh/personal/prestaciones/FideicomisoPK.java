package sigefirrhh.personal.prestaciones;

import java.io.Serializable;

public class FideicomisoPK
  implements Serializable
{
  public long idFideicomiso;

  public FideicomisoPK()
  {
  }

  public FideicomisoPK(long idFideicomiso)
  {
    this.idFideicomiso = idFideicomiso;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FideicomisoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FideicomisoPK thatPK)
  {
    return 
      this.idFideicomiso == thatPK.idFideicomiso;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFideicomiso)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFideicomiso);
  }
}