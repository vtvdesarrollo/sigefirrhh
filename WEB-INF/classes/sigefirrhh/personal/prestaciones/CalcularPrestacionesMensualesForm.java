package sigefirrhh.personal.prestaciones;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CalcularPrestacionesMensualesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularPrestacionesMensualesForm.class.getName());
  private String selectProceso;
  private String selectTipoPersonal;
  private int mes;
  private int anio;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private PrestacionesNoGenFacade prestacionesNoGenFacade = new PrestacionesNoGenFacade();
  private boolean show;
  private boolean auxShow;
  private SeguridadPrestaciones seguridadPrestaciones = new SeguridadPrestaciones();

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.seguridadPrestaciones = this.prestacionesNoGenFacade.findUltimasPrestaciones(this.idTipoPersonal);
      if (this.seguridadPrestaciones.getMes() == 12) {
        this.anio = (this.seguridadPrestaciones.getAnio() + 1);
        this.mes = 1;
      } else {
        this.mes = (this.seguridadPrestaciones.getMes() + 1);
        this.anio = this.seguridadPrestaciones.getAnio();
      }

      this.auxShow = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public CalcularPrestacionesMensualesForm() {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if ((this.mes < 1) || (this.mes > 12)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }
      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

      boolean estado = this.prestacionesNoGenFacade.calcularPrestacionesMensuales(this.idTipoPersonal, this.mes, this.anio, this.seguridadPrestaciones.getIdSeguridadPrestaciones(), this.selectProceso, this.login.getUsuario());

      if ((estado = 1) != 0)
      {
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

        context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
      } else {
        context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }
}