package sigefirrhh.personal.prestaciones;

import java.io.Serializable;

public class ConceptoPrestacionesPK
  implements Serializable
{
  public long idConceptoPrestaciones;

  public ConceptoPrestacionesPK()
  {
  }

  public ConceptoPrestacionesPK(long idConceptoPrestaciones)
  {
    this.idConceptoPrestaciones = idConceptoPrestaciones;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoPrestacionesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoPrestacionesPK thatPK)
  {
    return 
      this.idConceptoPrestaciones == thatPK.idConceptoPrestaciones;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoPrestaciones)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoPrestaciones);
  }
}