package sigefirrhh.personal.prestaciones;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ActualizarFideicomisoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarFideicomisoForm.class.getName());
  private String selectTipoPersonal;
  private int mes;
  private int anio;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private PrestacionesNoGenFacade prestacionesNoGenFacade = new PrestacionesNoGenFacade();
  private boolean show;
  private boolean auxShow;
  private SeguridadPrestaciones seguridadPrestaciones = new SeguridadPrestaciones();
  private Date fecha;

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.auxShow = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public ActualizarFideicomisoForm() {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if ((this.mes < 1) || (this.mes > 12)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }

      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

      boolean estado = this.prestacionesNoGenFacade.actualizarFideicomiso(this.idTipoPersonal, this.mes, this.anio, this.fecha);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
    }

    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }
  public int getAnio() {
    return this.anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setAnio(int i) {
    this.anio = i;
  }
  public void setMes(int i) {
    this.mes = i;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public Date getFecha() {
    return this.fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }
}