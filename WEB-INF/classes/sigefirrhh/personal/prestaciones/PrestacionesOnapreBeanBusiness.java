package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class PrestacionesOnapreBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrestacionesOnapre(PrestacionesOnapre prestacionesOnapre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PrestacionesOnapre prestacionesOnapreNew = 
      (PrestacionesOnapre)BeanUtils.cloneBean(
      prestacionesOnapre);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (prestacionesOnapreNew.getTrabajador() != null) {
      prestacionesOnapreNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        prestacionesOnapreNew.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (prestacionesOnapreNew.getTipoPersonal() != null) {
      prestacionesOnapreNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        prestacionesOnapreNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(prestacionesOnapreNew);
  }

  public void updatePrestacionesOnapre(PrestacionesOnapre prestacionesOnapre) throws Exception
  {
    PrestacionesOnapre prestacionesOnapreModify = 
      findPrestacionesOnapreById(prestacionesOnapre.getIdPrestacionesOnapre());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (prestacionesOnapre.getTrabajador() != null) {
      prestacionesOnapre.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        prestacionesOnapre.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (prestacionesOnapre.getTipoPersonal() != null) {
      prestacionesOnapre.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        prestacionesOnapre.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(prestacionesOnapreModify, prestacionesOnapre);
  }

  public void deletePrestacionesOnapre(PrestacionesOnapre prestacionesOnapre) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PrestacionesOnapre prestacionesOnapreDelete = 
      findPrestacionesOnapreById(prestacionesOnapre.getIdPrestacionesOnapre());
    pm.deletePersistent(prestacionesOnapreDelete);
  }

  public PrestacionesOnapre findPrestacionesOnapreById(long idPrestacionesOnapre) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrestacionesOnapre == pIdPrestacionesOnapre";
    Query query = pm.newQuery(PrestacionesOnapre.class, filter);

    query.declareParameters("long pIdPrestacionesOnapre");

    parameters.put("pIdPrestacionesOnapre", new Long(idPrestacionesOnapre));

    Collection colPrestacionesOnapre = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrestacionesOnapre.iterator();
    return (PrestacionesOnapre)iterator.next();
  }

  public Collection findPrestacionesOnapreAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent prestacionesOnapreExtent = pm.getExtent(
      PrestacionesOnapre.class, true);
    Query query = pm.newQuery(prestacionesOnapreExtent);
    query.setOrdering("anio descending, mes descending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(PrestacionesOnapre.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("anio descending, mes descending");

    Collection colPrestacionesOnapre = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrestacionesOnapre);

    return colPrestacionesOnapre;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(PrestacionesOnapre.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio descending, mes descending");

    Collection colPrestacionesOnapre = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrestacionesOnapre);

    return colPrestacionesOnapre;
  }
}