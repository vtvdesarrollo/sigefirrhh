package sigefirrhh.personal.prestaciones;

import java.io.Serializable;

public class SeguridadPrestacionesPK
  implements Serializable
{
  public long idSeguridadPrestaciones;

  public SeguridadPrestacionesPK()
  {
  }

  public SeguridadPrestacionesPK(long idSeguridadPrestaciones)
  {
    this.idSeguridadPrestaciones = idSeguridadPrestaciones;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadPrestacionesPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadPrestacionesPK thatPK)
  {
    return 
      this.idSeguridadPrestaciones == thatPK.idSeguridadPrestaciones;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadPrestaciones)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadPrestaciones);
  }
}