package sigefirrhh.personal.prestaciones;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.BancoBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class FideicomisoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addFideicomiso(Fideicomiso fideicomiso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Fideicomiso fideicomisoNew = 
      (Fideicomiso)BeanUtils.cloneBean(
      fideicomiso);

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (fideicomisoNew.getBanco() != null) {
      fideicomisoNew.setBanco(
        bancoBeanBusiness.findBancoById(
        fideicomisoNew.getBanco().getIdBanco()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (fideicomisoNew.getTipoPersonal() != null) {
      fideicomisoNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        fideicomisoNew.getTipoPersonal().getIdTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (fideicomisoNew.getTrabajador() != null) {
      fideicomisoNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        fideicomisoNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(fideicomisoNew);
  }

  public void updateFideicomiso(Fideicomiso fideicomiso) throws Exception
  {
    Fideicomiso fideicomisoModify = 
      findFideicomisoById(fideicomiso.getIdFideicomiso());

    BancoBeanBusiness bancoBeanBusiness = new BancoBeanBusiness();

    if (fideicomiso.getBanco() != null) {
      fideicomiso.setBanco(
        bancoBeanBusiness.findBancoById(
        fideicomiso.getBanco().getIdBanco()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (fideicomiso.getTipoPersonal() != null) {
      fideicomiso.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        fideicomiso.getTipoPersonal().getIdTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (fideicomiso.getTrabajador() != null) {
      fideicomiso.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        fideicomiso.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(fideicomisoModify, fideicomiso);
  }

  public void deleteFideicomiso(Fideicomiso fideicomiso) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Fideicomiso fideicomisoDelete = 
      findFideicomisoById(fideicomiso.getIdFideicomiso());
    pm.deletePersistent(fideicomisoDelete);
  }

  public Fideicomiso findFideicomisoById(long idFideicomiso) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFideicomiso == pIdFideicomiso";
    Query query = pm.newQuery(Fideicomiso.class, filter);

    query.declareParameters("long pIdFideicomiso");

    parameters.put("pIdFideicomiso", new Long(idFideicomiso));

    Collection colFideicomiso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFideicomiso.iterator();
    return (Fideicomiso)iterator.next();
  }

  public Collection findFideicomisoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent fideicomisoExtent = pm.getExtent(
      Fideicomiso.class, true);
    Query query = pm.newQuery(fideicomisoExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByBanco(long idBanco)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "banco.idBanco == pIdBanco";

    Query query = pm.newQuery(Fideicomiso.class, filter);

    query.declareParameters("long pIdBanco");
    HashMap parameters = new HashMap();

    parameters.put("pIdBanco", new Long(idBanco));

    query.setOrdering("anio ascending, mes ascending");

    Collection colFideicomiso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFideicomiso);

    return colFideicomiso;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(Fideicomiso.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colFideicomiso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFideicomiso);

    return colFideicomiso;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(Fideicomiso.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("anio ascending, mes ascending");

    Collection colFideicomiso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFideicomiso);

    return colFideicomiso;
  }
}