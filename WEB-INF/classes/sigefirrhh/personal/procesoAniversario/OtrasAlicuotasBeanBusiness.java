package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class OtrasAlicuotasBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    OtrasAlicuotas otrasAlicuotasNew = 
      (OtrasAlicuotas)BeanUtils.cloneBean(
      otrasAlicuotas);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (otrasAlicuotasNew.getTrabajador() != null) {
      otrasAlicuotasNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        otrasAlicuotasNew.getTrabajador().getIdTrabajador()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoAlicuotaBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (otrasAlicuotasNew.getConceptoAlicuota() != null) {
      otrasAlicuotasNew.setConceptoAlicuota(
        conceptoAlicuotaBeanBusiness.findConceptoTipoPersonalById(
        otrasAlicuotasNew.getConceptoAlicuota().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(otrasAlicuotasNew);
  }

  public void updateOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas) throws Exception
  {
    OtrasAlicuotas otrasAlicuotasModify = 
      findOtrasAlicuotasById(otrasAlicuotas.getIdOtrasAlicuotas());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (otrasAlicuotas.getTrabajador() != null) {
      otrasAlicuotas.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        otrasAlicuotas.getTrabajador().getIdTrabajador()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoAlicuotaBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (otrasAlicuotas.getConceptoAlicuota() != null) {
      otrasAlicuotas.setConceptoAlicuota(
        conceptoAlicuotaBeanBusiness.findConceptoTipoPersonalById(
        otrasAlicuotas.getConceptoAlicuota().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(otrasAlicuotasModify, otrasAlicuotas);
  }

  public void deleteOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    OtrasAlicuotas otrasAlicuotasDelete = 
      findOtrasAlicuotasById(otrasAlicuotas.getIdOtrasAlicuotas());
    pm.deletePersistent(otrasAlicuotasDelete);
  }

  public OtrasAlicuotas findOtrasAlicuotasById(long idOtrasAlicuotas) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idOtrasAlicuotas == pIdOtrasAlicuotas";
    Query query = pm.newQuery(OtrasAlicuotas.class, filter);

    query.declareParameters("long pIdOtrasAlicuotas");

    parameters.put("pIdOtrasAlicuotas", new Long(idOtrasAlicuotas));

    Collection colOtrasAlicuotas = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colOtrasAlicuotas.iterator();
    return (OtrasAlicuotas)iterator.next();
  }

  public Collection findOtrasAlicuotasAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent otrasAlicuotasExtent = pm.getExtent(
      OtrasAlicuotas.class, true);
    Query query = pm.newQuery(otrasAlicuotasExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}