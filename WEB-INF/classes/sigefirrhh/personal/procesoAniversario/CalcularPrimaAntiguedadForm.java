package sigefirrhh.personal.procesoAniversario;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CalcularPrimaAntiguedadForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularPrimaAntiguedadForm.class.getName());

  private int tipoReporte = 1;
  private String reportName;
  private int reportId;
  private String selectProceso;
  private String selectTipoPersonal;
  private Date inicio;
  private Date fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private ProcesoAniversarioNoGenFacade procesoAniversarioNoGenFacade = new ProcesoAniversarioNoGenFacade();
  private boolean show;
  private boolean auxShow;
  private boolean showGenerate;
  private SeguridadAniversario seguridadAniversario = new SeguridadAniversario();

  public CalcularPrimaAntiguedadForm() {
    this.reportName = "priant";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.inicio = null;
    this.fin = null;
    this.inicioAux = Calendar.getInstance();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    setShowGenerate(false);
    refresh();
    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        CalcularPrimaAntiguedadForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    if (this.tipoReporte == 1)
      this.reportName = "priant";
    else if (this.tipoReporte == 2)
      this.reportName = "priantanual";
    else if (this.tipoReporte == 3) {
      this.reportName = "priantuel";
    }
    log.error("NOMBRE DE REPORTE" + this.reportName);
    log.error("id DE REPORTE" + this.tipoReporte);
  }

  public String runReport() {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
    Date fec_ini = new Date(this.inicioAux.getTime().getYear(), this.inicioAux.getTime().getMonth(), this.inicioAux.getTime().getDate());
    Date fec_fin = new Date(this.fin.getYear(), this.fin.getMonth(), this.fin.getDate());
    parameters.put("fec_ini", fec_ini);
    parameters.put("fec_fin", fec_fin);

    if (this.tipoReporte == 1)
      this.reportName = "priant";
    else if (this.tipoReporte == 2)
      this.reportName = "priantanual";
    else if (this.tipoReporte == 3) {
      this.reportName = "priantuel";
    }

    JasperForWeb report = new JasperForWeb();

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/procesoAniversario");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.seguridadAniversario = this.procesoAniversarioNoGenFacade.findSeguridadAniversarioForCalculo(this.idTipoPersonal, "0500");

      Calendar fecha = Calendar.getInstance();
      fecha.setTime(this.seguridadAniversario.getFechaUltimo());
      fecha.add(5, 1);
      this.inicioAux.set(fecha.getTime().getYear() + 1900, fecha.getTime().getMonth(), fecha.getTime().getDate());
      this.auxShow = true;
      setShowGenerate(true);
    }
    catch (Exception e) {
      this.auxShow = false;
      setShowGenerate(false);
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh() {
    try {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

      this.procesoAniversarioNoGenFacade.calcularPrimaAntiguedad(this.idTipoPersonal, this.inicioAux.getTime(), this.fin, this.selectProceso, this.seguridadAniversario.getIdSeguridadAniversario(), this.login.getUsuario());
      if (getSelectProceso().equalsIgnoreCase("2")) {
        setShowGenerate(false);
      }
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
    }

    return null;
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string)
  {
    this.selectTipoPersonal = string;
  }

  public Date getFin()
  {
    return this.fin;
  }

  public Date getInicio()
  {
    return this.inicioAux.getTime();
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public String getSelectProceso()
  {
    return this.selectProceso;
  }

  public void setSelectProceso(String string)
  {
    this.selectProceso = string;
  }

  public void setFin(Date date)
  {
    this.fin = date;
  }

  public void setInicio(Date date)
  {
    this.inicio = date;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }
  public boolean isShowGenerate() {
    return this.showGenerate;
  }
  public void setShowGenerate(boolean showGenerate) {
    this.showGenerate = showGenerate;
  }
}