package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesBusiness;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.ParametroVarios;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.VacacionesPorAnio;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;

public class GenerarDerechoVacacionBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(GenerarDerechoVacacionBeanBusiness.class.getName());

  private DefinicionesBusiness definicionesBusiness = new DefinicionesBusiness();
  private TrabajadorNoGenBusiness trabajadorNoGenBusiness = new TrabajadorNoGenBusiness();
  private DefinicionesNoGenBusiness definicionesNoGenBusiness = new DefinicionesNoGenBusiness();

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean generarderecho(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, String proceso, long idSeguridadVacacion, String usuario) throws Exception
  {
    Calendar fin = Calendar.getInstance();
    Calendar inicio = Calendar.getInstance();

    ParametroVarios parametroVarios = new ParametroVarios();
    Collection colParametroVarios = this.definicionesBusiness.findParametroVariosByTipoPersonal(idTipoPersonal);
    Iterator iteratorParametroVarios = colParametroVarios.iterator();
    parametroVarios = (ParametroVarios)iteratorParametroVarios.next();

    TipoPersonal tipoPersonal = new TipoPersonal();
    tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    int anioInicio = fechaInicio.getYear();
    int diaInicio = fechaInicio.getDate();
    int mesInicio = fechaInicio.getMonth() + 1;
    int diaFin = fechaFin.getDate();
    int mesFin = fechaFin.getMonth() + 1;
    int diasDisfrute = 0;
    String tipoVacacion = "P";
    String observacion = "Generado automaticamente";

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());
    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());
    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());

    int aniosCumple = 0;
    int mes = 0;
    int anioDeFechaFin = fechaFin.getYear() + 1900;
    int anioDeFechaInicio = fechaInicio.getYear() + 1900;

    Connection connection = null;
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;
    Statement stInsert = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      stInsert = connection.createStatement();

      sql = new StringBuffer();
      sql.append("select t.id_personal, t.anio_vacaciones,  t.id_cargo, ");
      sql.append(" tp.formula_integral, tp.formula_semanal, p.anios_servicio_apn,t.mes_vacaciones,  t.dia_vacaciones ");
      sql.append(" from trabajador t,personal p , tipopersonal tp where t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and tp.id_tipo_personal = ? and t.estatus = 'A'");
      sql.append("  and (t.mes_vacaciones*100)+t.dia_vacaciones >= ? ");
      sql.append("  and (t.mes_vacaciones)*100+t.dia_vacaciones <= ? ");
      sql.append(" and t.anio_vacaciones < ?");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, idTipoPersonal);
      stTrabajadores.setInt(2, mesInicio * 100 + diaInicio);
      stTrabajadores.setInt(3, mesFin * 100 + diaFin);
      stTrabajadores.setInt(4, fechaInicio.getYear() + 1900);
      rsTrabajadores = stTrabajadores.executeQuery();

      while (rsTrabajadores.next()) {
        aniosCumple = 0;
        aniosCumple = fechaFin.getYear() + 1900 - rsTrabajadores.getInt("anio_vacaciones");
        if ((parametroVarios.getSumoApn().equals("S")) && (rsTrabajadores.getInt("anios_servicio_apn") > 0)) {
          aniosCumple += rsTrabajadores.getInt("anios_servicio_apn");
        }
        this.log.error("aniosCumple: " + aniosCumple);

        java.sql.Date fechaDerechoSql = new java.sql.Date(fechaInicio.getYear(), rsTrabajadores.getInt("mes_vacaciones") - 1, rsTrabajadores.getInt("dia_vacaciones"));

        VacacionesPorAnio vacacionesPorAnio = this.definicionesNoGenBusiness.findVacacionesPorAnioForAniosServicio(idTipoPersonal, aniosCumple);

        sql = new StringBuffer();
        sql.append("insert into vacacion (id_tipo_personal,id_personal,anio,fecha_inicio,dias_pendientes,dias_disfrute,observaciones, ");
        sql.append(" tipo_vacacion,id_vacacion) values(");
        sql.append(tipoPersonal.getIdTipoPersonal() + ", ");
        sql.append(rsTrabajadores.getLong("id_personal") + ", ");
        sql.append(anioDeFechaInicio + ",");
        sql.append("'" + fechaDerechoSql + "',");
        sql.append(vacacionesPorAnio.getDiasDisfrutar() + ",");
        sql.append(diasDisfrute + ",");
        sql.append("'" + observacion + "',");
        sql.append("'" + tipoVacacion + "',");
        sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Vacacion") + ")");
        stInsert.addBatch(sql.toString());
      }

      sql = new StringBuffer();
      sql.append("update seguridadvacacion set fecha_ultimo= '" + fechaFinSql + "', ");
      sql.append("fecha_proceso = '" + fechaActualSql + "', usuario = '" + usuario + "' where id_seguridad_vacacion = " + idSeguridadVacacion);

      stInsert.addBatch(sql.toString());
      stInsert.executeBatch();
      connection.commit();
    }
    finally {
      if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException1) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException2) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException3) {  }
 
    }
    return true;
  }
}