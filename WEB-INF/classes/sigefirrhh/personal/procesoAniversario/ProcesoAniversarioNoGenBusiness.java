package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBusiness;
import java.util.Date;

public class ProcesoAniversarioNoGenBusiness extends AbstractBusiness
{
  SeguridadAniversarioNoGenBeanBusiness seguridadAniversarioNoGenBeanBusiness = new SeguridadAniversarioNoGenBeanBusiness();

  SeguridadVacacionNoGenBeanBusiness seguridadVacacionNoGenBeanBusiness = new SeguridadVacacionNoGenBeanBusiness();

  CalcularPrimaAntiguedadBeanBusiness calcularPrimaAntiguedadBeanBusiness = new CalcularPrimaAntiguedadBeanBusiness();

  CalcularBonoVacacionalBeanBusiness calcularBonoVacacionalBeanBusiness = new CalcularBonoVacacionalBeanBusiness();

  GenerarDerechoVacacionBeanBusiness generarDerechoVacacionBeanBusiness = new GenerarDerechoVacacionBeanBusiness();

  public SeguridadAniversario findSeguridadAniversarioForCalculo(long idTipoPersonal, String codConcepto)
    throws Exception
  {
    return this.seguridadAniversarioNoGenBeanBusiness.findForCalculo(idTipoPersonal, codConcepto);
  }

  public SeguridadVacacion findSeguridadVacacionForDerecho(long idTipoPersonal) throws Exception
  {
    return this.seguridadVacacionNoGenBeanBusiness.findForCalculo(idTipoPersonal);
  }

  public void calcularPrimaAntiguedad(long idTipoPersonal, Date fechaInicio, Date fechaFin, String proceso, long idSeguridadAniversario, String usuario)
    throws Exception
  {
    this.calcularPrimaAntiguedadBeanBusiness.calcular(idTipoPersonal, fechaInicio, fechaFin, proceso, idSeguridadAniversario, usuario);
  }

  public long proyectarPrimaAntiguedad(long idTipoPersonal, Date fechaInicio, Date fechaFin, long idUnidadAdministradora, int anioProceso, int mesProceso, long id) throws Exception {
    return this.calcularPrimaAntiguedadBeanBusiness.proyectar(idTipoPersonal, fechaInicio, fechaFin, idUnidadAdministradora, anioProceso, mesProceso, id);
  }

  public boolean calcularBonoVacacional(long idTipoPersonal, Date fechaInicio, Date fechaFin, String proceso, long idSeguridadAniversario, String usuario) throws Exception {
    return this.calcularBonoVacacionalBeanBusiness.calcular(idTipoPersonal, fechaInicio, fechaFin, proceso, idSeguridadAniversario, usuario);
  }

  public boolean calcularBonoVacacionalLiquidacion(long idTipoPersonal, long idTrabajador, String usuario) throws Exception
  {
    return this.calcularBonoVacacionalBeanBusiness.calcularbonovacliquidacion(idTipoPersonal, idTrabajador, usuario);
  }

  public boolean generarderechovacacion(long idTipoPersonal, Date fechaInicio, Date fechaFin, String proceso, long idSeguridadAniversario, String usuario) throws Exception
  {
    return this.generarDerechoVacacionBeanBusiness.generarderecho(idTipoPersonal, fechaInicio, fechaFin, proceso, idSeguridadAniversario, usuario);
  }

  public long proyectarBonoVacacional(long idTipoPersonal, Date fechaInicio, Date fechaFin, long idUnidadAdministradora, int anioProceso, int mesProceso, long id) throws Exception
  {
    return this.calcularBonoVacacionalBeanBusiness.proyectar(idTipoPersonal, fechaInicio, fechaFin, idUnidadAdministradora, anioProceso, mesProceso, id);
  }
}