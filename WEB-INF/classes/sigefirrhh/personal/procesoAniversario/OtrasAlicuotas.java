package sigefirrhh.personal.procesoAniversario;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class OtrasAlicuotas
  implements Serializable, PersistenceCapable
{
  private long idOtrasAlicuotas;
  private Trabajador trabajador;
  private ConceptoTipoPersonal conceptoAlicuota;
  private double monto;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoAlicuota", "idOtrasAlicuotas", "monto", "trabajador" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public OtrasAlicuotas()
  {
    jdoSetmonto(this, 0.0D);
  }

  public String toString() {
    return jdoGetconceptoAlicuota(this).getConcepto().getDescripcion() + " - " + jdoGetmonto(this);
  }

  public ConceptoTipoPersonal getConceptoAlicuota()
  {
    return jdoGetconceptoAlicuota(this);
  }

  public long getIdOtrasAlicuotas()
  {
    return jdoGetidOtrasAlicuotas(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setConceptoAlicuota(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoAlicuota(this, personal);
  }

  public void setIdOtrasAlicuotas(long l)
  {
    jdoSetidOtrasAlicuotas(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoAniversario.OtrasAlicuotas"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new OtrasAlicuotas());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    OtrasAlicuotas localOtrasAlicuotas = new OtrasAlicuotas();
    localOtrasAlicuotas.jdoFlags = 1;
    localOtrasAlicuotas.jdoStateManager = paramStateManager;
    return localOtrasAlicuotas;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    OtrasAlicuotas localOtrasAlicuotas = new OtrasAlicuotas();
    localOtrasAlicuotas.jdoCopyKeyFieldsFromObjectId(paramObject);
    localOtrasAlicuotas.jdoFlags = 1;
    localOtrasAlicuotas.jdoStateManager = paramStateManager;
    return localOtrasAlicuotas;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoAlicuota);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idOtrasAlicuotas);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoAlicuota = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idOtrasAlicuotas = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(OtrasAlicuotas paramOtrasAlicuotas, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramOtrasAlicuotas == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoAlicuota = paramOtrasAlicuotas.conceptoAlicuota;
      return;
    case 1:
      if (paramOtrasAlicuotas == null)
        throw new IllegalArgumentException("arg1");
      this.idOtrasAlicuotas = paramOtrasAlicuotas.idOtrasAlicuotas;
      return;
    case 2:
      if (paramOtrasAlicuotas == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramOtrasAlicuotas.monto;
      return;
    case 3:
      if (paramOtrasAlicuotas == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramOtrasAlicuotas.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof OtrasAlicuotas))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    OtrasAlicuotas localOtrasAlicuotas = (OtrasAlicuotas)paramObject;
    if (localOtrasAlicuotas.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localOtrasAlicuotas, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new OtrasAlicuotasPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new OtrasAlicuotasPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OtrasAlicuotasPK))
      throw new IllegalArgumentException("arg1");
    OtrasAlicuotasPK localOtrasAlicuotasPK = (OtrasAlicuotasPK)paramObject;
    localOtrasAlicuotasPK.idOtrasAlicuotas = this.idOtrasAlicuotas;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OtrasAlicuotasPK))
      throw new IllegalArgumentException("arg1");
    OtrasAlicuotasPK localOtrasAlicuotasPK = (OtrasAlicuotasPK)paramObject;
    this.idOtrasAlicuotas = localOtrasAlicuotasPK.idOtrasAlicuotas;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OtrasAlicuotasPK))
      throw new IllegalArgumentException("arg2");
    OtrasAlicuotasPK localOtrasAlicuotasPK = (OtrasAlicuotasPK)paramObject;
    localOtrasAlicuotasPK.idOtrasAlicuotas = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OtrasAlicuotasPK))
      throw new IllegalArgumentException("arg2");
    OtrasAlicuotasPK localOtrasAlicuotasPK = (OtrasAlicuotasPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localOtrasAlicuotasPK.idOtrasAlicuotas);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoAlicuota(OtrasAlicuotas paramOtrasAlicuotas)
  {
    StateManager localStateManager = paramOtrasAlicuotas.jdoStateManager;
    if (localStateManager == null)
      return paramOtrasAlicuotas.conceptoAlicuota;
    if (localStateManager.isLoaded(paramOtrasAlicuotas, jdoInheritedFieldCount + 0))
      return paramOtrasAlicuotas.conceptoAlicuota;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramOtrasAlicuotas, jdoInheritedFieldCount + 0, paramOtrasAlicuotas.conceptoAlicuota);
  }

  private static final void jdoSetconceptoAlicuota(OtrasAlicuotas paramOtrasAlicuotas, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramOtrasAlicuotas.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtrasAlicuotas.conceptoAlicuota = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramOtrasAlicuotas, jdoInheritedFieldCount + 0, paramOtrasAlicuotas.conceptoAlicuota, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidOtrasAlicuotas(OtrasAlicuotas paramOtrasAlicuotas)
  {
    return paramOtrasAlicuotas.idOtrasAlicuotas;
  }

  private static final void jdoSetidOtrasAlicuotas(OtrasAlicuotas paramOtrasAlicuotas, long paramLong)
  {
    StateManager localStateManager = paramOtrasAlicuotas.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtrasAlicuotas.idOtrasAlicuotas = paramLong;
      return;
    }
    localStateManager.setLongField(paramOtrasAlicuotas, jdoInheritedFieldCount + 1, paramOtrasAlicuotas.idOtrasAlicuotas, paramLong);
  }

  private static final double jdoGetmonto(OtrasAlicuotas paramOtrasAlicuotas)
  {
    if (paramOtrasAlicuotas.jdoFlags <= 0)
      return paramOtrasAlicuotas.monto;
    StateManager localStateManager = paramOtrasAlicuotas.jdoStateManager;
    if (localStateManager == null)
      return paramOtrasAlicuotas.monto;
    if (localStateManager.isLoaded(paramOtrasAlicuotas, jdoInheritedFieldCount + 2))
      return paramOtrasAlicuotas.monto;
    return localStateManager.getDoubleField(paramOtrasAlicuotas, jdoInheritedFieldCount + 2, paramOtrasAlicuotas.monto);
  }

  private static final void jdoSetmonto(OtrasAlicuotas paramOtrasAlicuotas, double paramDouble)
  {
    if (paramOtrasAlicuotas.jdoFlags == 0)
    {
      paramOtrasAlicuotas.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramOtrasAlicuotas.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtrasAlicuotas.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramOtrasAlicuotas, jdoInheritedFieldCount + 2, paramOtrasAlicuotas.monto, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(OtrasAlicuotas paramOtrasAlicuotas)
  {
    StateManager localStateManager = paramOtrasAlicuotas.jdoStateManager;
    if (localStateManager == null)
      return paramOtrasAlicuotas.trabajador;
    if (localStateManager.isLoaded(paramOtrasAlicuotas, jdoInheritedFieldCount + 3))
      return paramOtrasAlicuotas.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramOtrasAlicuotas, jdoInheritedFieldCount + 3, paramOtrasAlicuotas.trabajador);
  }

  private static final void jdoSettrabajador(OtrasAlicuotas paramOtrasAlicuotas, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramOtrasAlicuotas.jdoStateManager;
    if (localStateManager == null)
    {
      paramOtrasAlicuotas.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramOtrasAlicuotas, jdoInheritedFieldCount + 3, paramOtrasAlicuotas.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}