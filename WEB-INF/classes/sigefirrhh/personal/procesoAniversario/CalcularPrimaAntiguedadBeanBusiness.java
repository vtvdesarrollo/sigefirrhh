package sigefirrhh.personal.procesoAniversario;

import eforserver.common.Resource;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CalcularPrimaAntiguedadBeanBusiness
{
  Logger log = Logger.getLogger(CalcularPrimaAntiguedadBeanBusiness.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public void calcular(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, String proceso, long idSeguridadAniversario, String usuario)
    throws Exception
  {
    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    int diaInicio = fechaInicio.getDate();
    int mesInicio = fechaInicio.getMonth() + 1;
    int diaFin = fechaFin.getDate();
    int mesFin = fechaFin.getMonth() + 1;

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());

    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    Connection connection = null;
    Statement stInsert = null;
    ResultSet rsConceptosFijos = null;
    PreparedStatement stConceptosFijos = null;
    ResultSet rsPrimaAntiguedad = null;
    PreparedStatement stPrimaAntiguedad = null;
    ResultSet rsMinPrimaAntiguedad = null;
    PreparedStatement stMinPrimaAntiguedad = null;
    ResultSet rsConceptoTipoPersonal = null;
    PreparedStatement stConceptoTipoPersonal = null;
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    Statement stBorrarCalculoAntiguedad = null;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      stInsert = connection.createStatement();

      StringBuffer sql = new StringBuffer();

      sql = new StringBuffer();
      sql.append("select cf.monto, fp.cod_frecuencia_pago ");
      sql.append(" from conceptofijo cf, frecuenciatipopersonal ftp, frecuenciapago fp ");
      sql.append(" where cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and cf.id_concepto_tipo_personal = ?");
      sql.append(" and cf.id_trabajador = ?");
      sql.append(" and cf.estatus = 'A' ");
      stConceptosFijos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select pa.monto, pa.tipo, pa.operacion, pa.porcentaje ");
      sql.append(" from primaantiguedad pa ");
      sql.append(" where pa.id_tipo_personal = ?");
      sql.append(" and pa.anios_servicio <= ?");
      sql.append(" order by pa.anios_servicio desc limit 1");

      stPrimaAntiguedad = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();

      stBorrarCalculoAntiguedad = connection.createStatement();
      sql.append("delete from calculoantiguedad where id_tipo_personal = " + idTipoPersonal);
      stBorrarCalculoAntiguedad.addBatch(sql.toString());
      stBorrarCalculoAntiguedad.executeBatch();

      sql = new StringBuffer();
      sql.append("select pm.anios_servicio ");
      sql.append(" from primaantiguedad pm ");
      sql.append(" where pm.id_tipo_personal = ?");
      sql.append(" order by pm.anios_servicio asc limit 1");

      stMinPrimaAntiguedad = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stMinPrimaAntiguedad.setLong(1, idTipoPersonal);
      rsMinPrimaAntiguedad = stMinPrimaAntiguedad.executeQuery();

      int minimo = 0;
      if (rsMinPrimaAntiguedad.next())
        minimo = rsMinPrimaAntiguedad.getInt("anios_servicio");
      if (rsMinPrimaAntiguedad != null) try {
          rsMinPrimaAntiguedad.close();
        }
        catch (Exception localException1)
        {
        }
      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal, ftp.id_frecuencia_tipo_personal, fp.cod_frecuencia_pago, ctp.tope_minimo, ctp.tope_maximo ");
      sql.append(" from conceptotipopersonal ctp, concepto c, frecuenciatipopersonal ftp, frecuenciapago fp ");
      sql.append(" where ctp.id_concepto = c.id_concepto ");
      sql.append(" and ctp.id_frecuencia_tipo_personal =  ftp.id_frecuencia_tipo_personal ");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.id_tipo_personal = ?");
      sql.append(" and c.cod_concepto = '0500'");

      stConceptoTipoPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoTipoPersonal.setLong(1, idTipoPersonal);
      rsConceptoTipoPersonal = stConceptoTipoPersonal.executeQuery();

      if (rsConceptoTipoPersonal.next()) {
        int idConceptoTipoPersonal = rsConceptoTipoPersonal.getInt("id_concepto_tipo_personal");
        int codFrecuenciaPago = rsConceptoTipoPersonal.getInt("cod_frecuencia_pago");
        int idFrecuenciaTipoPersonal = rsConceptoTipoPersonal.getInt("id_frecuencia_tipo_personal");

        sql = new StringBuffer();
        sql.append("select t.id_trabajador, t.mes_antiguedad, t.anio_antiguedad, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo");
        sql.append(" from trabajador t, tipopersonal tp, turno tu ");
        sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and t.id_tipo_personal = ? and t.estatus = 'A'");
        sql.append(" and (t.mes_antiguedad*100)+t.dia_antiguedad >= ? ");
        sql.append(" and (t.mes_antiguedad*100)+t.dia_antiguedad <= ? ");
        sql.append(" and t.anio_antiguedad < ?");
        sql.append(" and t.id_cargo not in (select id_cargo from conceptocargo where id_concepto_tipo_personal = ? and excluir = 'S' )");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, idTipoPersonal);
        stTrabajadores.setInt(2, mesInicio * 100 + diaInicio);
        stTrabajadores.setInt(3, mesFin * 100 + diaFin);
        stTrabajadores.setInt(4, fechaInicio.getYear() + 1900);
        stTrabajadores.setInt(5, idConceptoTipoPersonal);
        rsTrabajadores = stTrabajadores.executeQuery();

        while (rsTrabajadores.next()) {
          double montoAnterior = 0.0D;
          double montoActual = 0.0D;
          long aniosCumple = fechaActual.getYear() + 1900 - rsTrabajadores.getLong("anio_antiguedad");

          if (aniosCumple > minimo) {
            stConceptosFijos.setLong(1, idConceptoTipoPersonal);
            stConceptosFijos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            rsConceptosFijos = stConceptosFijos.executeQuery();

            if (rsConceptosFijos.next())
            {
              if (rsConceptosFijos.getInt("cod_frecuencia_pago") == 3)
                montoAnterior = NumberTools.twoDecimal(rsConceptosFijos.getDouble("monto") * 2.0D);
              else
                montoAnterior = NumberTools.twoDecimal(rsConceptosFijos.getDouble("monto"));
            }
          }
          else if (aniosCumple == minimo) {
            montoAnterior = 0.0D;
          }
          stPrimaAntiguedad.setLong(1, idTipoPersonal);
          stPrimaAntiguedad.setLong(2, aniosCumple);
          rsPrimaAntiguedad = stPrimaAntiguedad.executeQuery();
          if (rsPrimaAntiguedad.next()) {
            if (rsPrimaAntiguedad.getString("tipo").equals("F")) {
              if (rsPrimaAntiguedad.getString("operacion").equals("M")) {
                montoActual = NumberTools.twoDecimal(aniosCumple * rsPrimaAntiguedad.getDouble("monto"));
              }
              else if (rsPrimaAntiguedad.getString("operacion").equals("I")) {
                montoActual = NumberTools.twoDecimal(montoAnterior + rsPrimaAntiguedad.getDouble("monto"));
              }
              else
                montoActual = NumberTools.twoDecimal(rsPrimaAntiguedad.getDouble("monto"));
            }
            else
            {
              montoActual = calcularConceptoBeanBusiness.calcular(rsConceptoTipoPersonal.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), 1.0D, "P", 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsPrimaAntiguedad.getDouble("porcentaje"), rsConceptoTipoPersonal.getDouble("tope_minimo"), rsConceptoTipoPersonal.getDouble("tope_maximo"));

              if (rsPrimaAntiguedad.getString("operacion").equals("M")) {
                montoActual = NumberTools.twoDecimal(aniosCumple * montoActual);
                this.log.error("monto Actual*AniosCumple " + montoActual);
              }
              else if (rsPrimaAntiguedad.getString("operacion").equals("I")) {
                montoActual = NumberTools.twoDecimal(montoAnterior + montoActual);
              }
              else {
                montoActual = NumberTools.twoDecimal(montoActual);
              }

            }

            sql = new StringBuffer();
            sql.append("insert into calculoantiguedad (anios_servicio, id_tipo_personal, monto,");
            sql.append(" monto_anterior, id_trabajador, mes, id_calculo_antiguedad) values(");
            sql.append(aniosCumple + ", " + idTipoPersonal + ", " + montoActual + ", ");
            sql.append(montoAnterior + ", " + rsTrabajadores.getLong("id_trabajador") + ", ");
            sql.append(rsTrabajadores.getInt("mes_antiguedad") + ", ");
            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.procesoAniversario.CalculoAntiguedad") + ")");
            stInsert.addBatch(sql.toString());
            if (proceso.equals("2")) {
              sql = new StringBuffer();
              if (aniosCumple > minimo) {
                if (codFrecuenciaPago == 3) {
                  sql.append("update conceptofijo set monto = " + NumberTools.twoDecimal(montoActual / 2.0D));
                  if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F"))) {
                    sql.append(" , unidades = " + rsPrimaAntiguedad.getDouble("porcentaje"));
                  }
                  sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);
                  sql.append(" and id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
                }
                else {
                  sql.append("update conceptofijo set monto = " + montoActual);
                  if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F"))) {
                    sql.append(" , unidades = " + rsPrimaAntiguedad.getDouble("porcentaje"));
                  }
                  sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);
                  sql.append(" and id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
                }
              }
              else if (aniosCumple == minimo)
              {
                sql.append("delete from conceptofijo ");
                sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);
                sql.append(" and id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
                stInsert.addBatch(sql.toString());
                sql = new StringBuffer();

                if (codFrecuenciaPago == 3)
                {
                  sql.append("insert into conceptofijo (id_trabajador, id_concepto_tipo_personal, ");
                  sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, estatus, ");
                  sql.append(" restituir, id_concepto_fijo) values(");
                  sql.append(rsTrabajadores.getLong("id_trabajador") + ", " + idConceptoTipoPersonal + ", " + idFrecuenciaTipoPersonal + ", ");
                  if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F")))
                    sql.append(rsPrimaAntiguedad.getDouble("porcentaje") + ",");
                  else {
                    sql.append("0,");
                  }
                  sql.append(NumberTools.twoDecimal(montoActual / 2.0D) + ", '" + fechaActualSql + "', 'A', 'N', ");
                  sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");
                }
                else
                {
                  sql.append("insert into conceptofijo (id_trabajador, id_concepto_tipo_personal, ");
                  sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, estatus, ");
                  sql.append(" restituir, id_concepto_fijo) values(");
                  sql.append(rsTrabajadores.getLong("id_trabajador") + ", " + idConceptoTipoPersonal + ", " + idFrecuenciaTipoPersonal + ", ");
                  if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F")))
                    sql.append(rsPrimaAntiguedad.getDouble("porcentaje") + ",");
                  else {
                    sql.append("0,");
                  }
                  sql.append(montoActual + ", '" + fechaActualSql + "', 'A', 'N', ");

                  sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");
                }

              }

              stInsert.addBatch(sql.toString());
            }
          }

        }

        try
        {
          if (proceso.equals("2")) {
            sql = new StringBuffer();
            sql.append("update seguridadaniversario set fecha_proceso = '" + fechaActualSql + "', ");
            sql.append(" fecha_ultimo = '" + fechaFinSql + "', usuario = '" + usuario + "'");
            sql.append(" where id_seguridad_aniversario = " + idSeguridadAniversario);
            stInsert.addBatch(sql.toString());
          }
          stInsert.executeBatch();
          connection.commit();
        } catch (Exception e) {
          this.log.error("Excepcion controlada:", e);
          ErrorSistema error = new ErrorSistema();
          error.setDescription("Ocurrieron errores al tratar de insertar en la tabla CalculoAntiguedad");
          throw error;
        }
      }
      else {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("El Concepto 0500 para este Tipo de Personal no esta registrado en la base de datos. Debe registrar este concepto para poder ejecutar este proceso");
        throw error;
      }
    } finally {
      if (rsConceptosFijos != null) try {
          rsConceptosFijos.close();
        } catch (Exception localException2) {
        } if (rsPrimaAntiguedad != null) try {
          rsPrimaAntiguedad.close();
        }
        catch (Exception localException3) {
        } if (rsConceptoTipoPersonal != null) try {
          rsConceptoTipoPersonal.close();
        } catch (Exception localException4) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException5) {
        } if (stConceptosFijos != null) try {
          stConceptosFijos.close();
        } catch (Exception localException6) {
        } if (stPrimaAntiguedad != null) try {
          stPrimaAntiguedad.close();
        } catch (Exception localException7) {
        } if (stConceptoTipoPersonal != null) try {
          stConceptoTipoPersonal.close();
        } catch (Exception localException8) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException9) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException10) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException11)
        {
        }
    }
    if (rsConceptosFijos != null) try {
        rsConceptosFijos.close();
      } catch (Exception localException12) {
      } if (rsPrimaAntiguedad != null) try {
        rsPrimaAntiguedad.close();
      }
      catch (Exception localException13) {
      } if (rsConceptoTipoPersonal != null) try {
        rsConceptoTipoPersonal.close();
      } catch (Exception localException14) {
      } if (rsTrabajadores != null) try {
        rsTrabajadores.close();
      } catch (Exception localException15) {
      } if (stConceptosFijos != null) try {
        stConceptosFijos.close();
      } catch (Exception localException16) {
      } if (stPrimaAntiguedad != null) try {
        stPrimaAntiguedad.close();
      } catch (Exception localException17) {
      } if (stConceptoTipoPersonal != null) try {
        stConceptoTipoPersonal.close();
      } catch (Exception localException18) {
      } if (stTrabajadores != null) try {
        stTrabajadores.close();
      } catch (Exception localException19) {
      } if (stInsert != null) try {
        stInsert.close();
      } catch (Exception localException20) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException21)
      {
      } 
  }

  public long proyectar(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, long idUnidadAdministradora, int anioProceso, int mesProceso, long id) throws Exception {
    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    int diaInicio = fechaInicio.getDate();
    int mesInicio = fechaInicio.getMonth() + 1;
    int diaFin = fechaFin.getDate();
    int mesFin = fechaFin.getMonth() + 1;

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());

    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    Connection connection = null;
    Statement stInsert = null;
    ResultSet rsConceptosFijos = null;
    PreparedStatement stConceptosFijos = null;
    ResultSet rsConceptoResumen = null;
    PreparedStatement stConceptoResumen = null;
    ResultSet rsPrimaAntiguedad = null;
    PreparedStatement stPrimaAntiguedad = null;
    ResultSet rsConceptoTipoPersonal = null;
    PreparedStatement stConceptoTipoPersonal = null;
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      StringBuffer sql = new StringBuffer();

      stInsert = connection.createStatement();

      sql = new StringBuffer();
      sql.append("select cf.monto, fp.cod_frecuencia_pago ");
      sql.append(" from conceptofijo cf, frecuenciatipopersonal ftp, frecuenciapago fp ");
      sql.append(" where cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and cf.id_concepto_tipo_personal = ?");
      sql.append(" and cf.id_trabajador = ?");
      sql.append(" and cf.estatus = 'A' ");
      stConceptosFijos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select id_concepto_resumen ");
      sql.append(" from conceptoresumen ");
      sql.append(" where ");
      sql.append(" id_concepto_tipo_personal = ?");
      sql.append(" and id_trabajador = ?");

      stConceptoResumen = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select pa.monto, pa.tipo, pa.operacion, pa.porcentaje ");
      sql.append(" from primaantiguedad pa ");
      sql.append(" where pa.id_tipo_personal = ?");
      sql.append(" and anios_servicio <= ?");
      sql.append(" order by anios_servicio desc limit 1");

      stPrimaAntiguedad = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal, ftp.id_frecuencia_tipo_personal, fp.cod_frecuencia_pago, ctp.tope_minimo, ctp.tope_maximo ");
      sql.append(" from conceptotipopersonal ctp, concepto c, frecuenciatipopersonal ftp, frecuenciapago fp ");
      sql.append(" where ctp.id_concepto = c.id_concepto ");
      sql.append(" and ctp.id_frecuencia_tipo_personal =  ftp.id_frecuencia_tipo_personal ");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.id_tipo_personal = ?");
      sql.append(" and c.cod_concepto = '0500'");

      stConceptoTipoPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoTipoPersonal.setLong(1, idTipoPersonal);
      rsConceptoTipoPersonal = stConceptoTipoPersonal.executeQuery();

      if (rsConceptoTipoPersonal.next()) {
        int idConceptoTipoPersonal = rsConceptoTipoPersonal.getInt("id_concepto_tipo_personal");
        int codFrecuenciaPago = rsConceptoTipoPersonal.getInt("cod_frecuencia_pago");
        int idFrecuenciaTipoPersonal = rsConceptoTipoPersonal.getInt("id_frecuencia_tipo_personal");

        sql = new StringBuffer();
        sql.append("select t.id_trabajador, t.mes_antiguedad, t.anio_antiguedad, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo");
        sql.append(" from trabajador t, tipopersonal tp, turno tu, dependencia d, administradorauel auel ");
        sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_administradora_uel = auel.id_administradora_uel");
        sql.append(" and auel.id_unidad_administradora = ? ");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and t.id_tipo_personal = ? and t.estatus = 'A'");
        sql.append(" and (t.mes_antiguedad*100)+t.dia_antiguedad >= ? ");
        sql.append(" and (t.mes_antiguedad*100)+t.dia_antiguedad <= ? ");
        sql.append(" and t.anio_antiguedad < ?");
        sql.append(" and t.id_cargo not in (select id_cargo from conceptocargo where id_concepto_tipo_personal = ? and excluir = 'S' )");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, idUnidadAdministradora);
        stTrabajadores.setLong(2, idTipoPersonal);
        stTrabajadores.setInt(3, mesInicio * 100 + diaInicio);
        stTrabajadores.setInt(4, mesFin * 100 + diaFin);
        stTrabajadores.setInt(5, fechaInicio.getYear() + 1900);
        stTrabajadores.setInt(6, idConceptoTipoPersonal);
        rsTrabajadores = stTrabajadores.executeQuery();

        while (rsTrabajadores.next()) {
          double montoAnterior = 0.0D;
          double montoActual = 0.0D;
          long aniosCumple = fechaActual.getYear() + 1900 - rsTrabajadores.getLong("anio_antiguedad");
          id += 1L;

          if (aniosCumple > 1L) {
            stConceptosFijos.setLong(1, idConceptoTipoPersonal);
            stConceptosFijos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            rsConceptosFijos = stConceptosFijos.executeQuery();
            if (rsConceptosFijos.next())
            {
              if (rsConceptosFijos.getInt("cod_frecuencia_pago") == 3)
                montoAnterior = NumberTools.twoDecimal(rsConceptosFijos.getDouble("monto") * 2.0D);
              else
                montoAnterior = NumberTools.twoDecimal(rsConceptosFijos.getDouble("monto"));
            }
          }
          else if (aniosCumple == 1L) {
            montoAnterior = 0.0D;
          }
          stPrimaAntiguedad.setLong(1, idTipoPersonal);
          stPrimaAntiguedad.setLong(2, aniosCumple);
          rsPrimaAntiguedad = stPrimaAntiguedad.executeQuery();
          if (rsPrimaAntiguedad.next()) {
            if (rsPrimaAntiguedad.getString("tipo").equals("F")) {
              if (rsPrimaAntiguedad.getString("operacion").equals("M")) {
                montoActual = NumberTools.twoDecimal(aniosCumple * rsPrimaAntiguedad.getDouble("monto"));
              }
              else if (rsPrimaAntiguedad.getString("operacion").equals("I")) {
                montoActual = NumberTools.twoDecimal(montoAnterior + rsPrimaAntiguedad.getDouble("monto"));
              }
              else
                montoActual = NumberTools.twoDecimal(rsPrimaAntiguedad.getDouble("monto"));
            }
            else
            {
              montoActual = calcularConceptoBeanBusiness.calcular(rsConceptoTipoPersonal.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), 1.0D, "P", 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsPrimaAntiguedad.getDouble("porcentaje"), rsConceptoTipoPersonal.getDouble("tope_minimo"), rsConceptoTipoPersonal.getDouble("tope_maximo"));

              if (rsPrimaAntiguedad.getString("operacion").equals("M")) {
                montoActual = NumberTools.twoDecimal(aniosCumple * montoActual);
                this.log.error("monto Actual*AniosCumple " + montoActual);
              }
              else if (rsPrimaAntiguedad.getString("operacion").equals("I")) {
                montoActual = NumberTools.twoDecimal(montoAnterior + montoActual);
              }
              else {
                montoActual = NumberTools.twoDecimal(montoActual);
              }
            }
          } else {
            this.log.error("no hay prima antiguedad");
            ErrorSistema error = new ErrorSistema();
            error.setDescription("No existe el registro de Prima Antiguedad para los años de servicio");
            throw error;
          }

          this.log.error("salio prima antiguedad");

          sql = new StringBuffer();
          if (aniosCumple > 1L) {
            if (codFrecuenciaPago == 3) {
              sql.append("update conceptoresumen set monto = " + NumberTools.twoDecimal(montoActual / 2.0D));
              if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F"))) {
                sql.append(" , unidades = " + rsPrimaAntiguedad.getDouble("porcentaje"));
              }
              sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);
              sql.append(" and id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
            }
            else {
              sql.append("update conceptoresumen set monto = " + montoActual);
              if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F"))) {
                sql.append(" , unidades = " + rsPrimaAntiguedad.getDouble("porcentaje"));
              }
              sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);
              sql.append(" and id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
            }
          }
          else if (aniosCumple == 1L)
          {
            stConceptoResumen.setLong(1, idConceptoTipoPersonal);
            stConceptoResumen.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            rsConceptoResumen = stConceptoResumen.executeQuery();
            if (rsConceptoResumen.next()) {
              if (codFrecuenciaPago == 3) {
                sql.append("update conceptoresumen set monto = " + NumberTools.twoDecimal(montoActual / 2.0D));
                if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F"))) {
                  sql.append(" , unidades = " + rsPrimaAntiguedad.getDouble("porcentaje"));
                }
                sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);
                sql.append(" and id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
              }
              else {
                sql.append("update conceptoresumen set monto = " + montoActual);
                if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F"))) {
                  sql.append(" , unidades = " + rsPrimaAntiguedad.getDouble("porcentaje"));
                }
                sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);
                sql.append(" and id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
              }
            }
            else if (codFrecuenciaPago == 3)
            {
              sql.append("insert into conceptoresumen (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto, ");
              sql.append(" id_unidad_administradora,  anio, mes, numero_nomina, id_concepto_resumen) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", " + idConceptoTipoPersonal + ", " + idFrecuenciaTipoPersonal + ", ");
              if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F")))
                sql.append(rsPrimaAntiguedad.getDouble("porcentaje") + ",");
              else {
                sql.append("0,");
              }
              sql.append(idUnidadAdministradora + ", " + anioProceso + ", " + mesProceso + ", 0, ");
              sql.append(NumberTools.twoDecimal(montoActual / 2.0D) + ", ");
              sql.append(id + ")");
              this.log.error("SQL 1 -------------  " + sql.toString());
            }
            else {
              sql.append("insert into conceptoresumen (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto, ");
              sql.append(" id_unidad_administradora, anio, mes, numero_nomina, id_concepto_resumen) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", " + idConceptoTipoPersonal + ", " + idFrecuenciaTipoPersonal + ", ");
              if ((rsPrimaAntiguedad.getString("tipo").equals("P")) && (rsPrimaAntiguedad.getString("operacion").equals("F")))
                sql.append(rsPrimaAntiguedad.getDouble("porcentaje") + ",");
              else {
                sql.append("0,");
              }
              sql.append(montoActual + ",  ");
              sql.append(idUnidadAdministradora + ", " + anioProceso + ", " + mesProceso + ", 0, ");
              sql.append(id + ")");
              this.log.error("SQL 2 -------  " + sql.toString());
            }

            stInsert.addBatch(sql.toString());
          }
        }
        try
        {
          stInsert.executeBatch();
          connection.commit();
        } catch (Exception e) {
          this.log.error("Excepcion controlada:", e);
          ErrorSistema error = new ErrorSistema();
          error.setDescription("Ocurrieron errores al tratar de insertar en la tabla CalculoAntiguedad");
          throw error;
        }
      }
    }
    finally {
      if (rsConceptosFijos != null) try {
          rsConceptosFijos.close();
        } catch (Exception localException1) {
        } if (rsPrimaAntiguedad != null) try {
          rsPrimaAntiguedad.close();
        } catch (Exception localException2) {
        } if (rsConceptoTipoPersonal != null) try {
          rsConceptoTipoPersonal.close();
        } catch (Exception localException3) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException4) {
        } if (stConceptosFijos != null) try {
          stConceptosFijos.close();
        } catch (Exception localException5) {
        } if (stPrimaAntiguedad != null) try {
          stPrimaAntiguedad.close();
        } catch (Exception localException6) {
        } if (stConceptoTipoPersonal != null) try {
          stConceptoTipoPersonal.close();
        } catch (Exception localException7) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException8) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException9) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException10)
        {
        }
    }
    if (rsConceptosFijos != null) try {
        rsConceptosFijos.close();
      } catch (Exception localException11) {
      } if (rsPrimaAntiguedad != null) try {
        rsPrimaAntiguedad.close();
      } catch (Exception localException12) {
      } if (rsConceptoTipoPersonal != null) try {
        rsConceptoTipoPersonal.close();
      } catch (Exception localException13) {
      } if (rsTrabajadores != null) try {
        rsTrabajadores.close();
      } catch (Exception localException14) {
      } if (stConceptosFijos != null) try {
        stConceptosFijos.close();
      } catch (Exception localException15) {
      } if (stPrimaAntiguedad != null) try {
        stPrimaAntiguedad.close();
      } catch (Exception localException16) {
      } if (stConceptoTipoPersonal != null) try {
        stConceptoTipoPersonal.close();
      } catch (Exception localException17) {
      } if (stTrabajadores != null) try {
        stTrabajadores.close();
      } catch (Exception localException18) {
      } if (stInsert != null) try {
        stInsert.close();
      } catch (Exception localException19) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException20) {
      } return id;
  }
}