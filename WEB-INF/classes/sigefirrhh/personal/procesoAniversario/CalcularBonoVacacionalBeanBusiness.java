package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesBusiness;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.ParametroVarios;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.VacacionesPorAnio;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;
import sigefirrhh.personal.historico.HistoricoNoGenBusiness;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;

public class CalcularBonoVacacionalBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(CalcularBonoVacacionalBeanBusiness.class.getName());

  private DefinicionesBusiness definicionesBusiness = new DefinicionesBusiness();
  private TrabajadorNoGenBusiness trabajadorNoGenBusiness = new TrabajadorNoGenBusiness();
  private HistoricoNoGenBusiness historicoNoGenBusiness = new HistoricoNoGenBusiness();
  private DefinicionesNoGenBusiness definicionesNoGenBusiness = new DefinicionesNoGenBusiness();

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean calcular(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, String proceso, long idSeguridadAniversario, String usuario)
    throws Exception
  {
    Calendar fin = Calendar.getInstance();
    Calendar inicio = Calendar.getInstance();

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    ParametroVarios parametroVarios = new ParametroVarios();
    Collection colParametroVarios = this.definicionesBusiness.findParametroVariosByTipoPersonal(idTipoPersonal);
    Iterator iteratorParametroVarios = colParametroVarios.iterator();
    parametroVarios = (ParametroVarios)iteratorParametroVarios.next();

    TipoPersonal tipoPersonal = new TipoPersonal();
    tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    int diaInicio = fechaInicio.getDate();
    int mesInicio = fechaInicio.getMonth() + 1;
    int diaFin = fechaFin.getDate();
    int mesFin = fechaFin.getMonth() + 1;

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());
    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());

    double montoA = 0.0D;
    double montoB = 0.0D;
    double montoC = 0.0D;
    double montoD = 0.0D;
    double totalMontoA = 0.0D;
    double totalMontoB = 0.0D;
    double totalMontoC = 0.0D;
    double totalMontoD = 0.0D;
    double totalUnidades = 0.0D;
    int aniosCumple = 0;
    long diasDividir = 0L;
    double diasBono = 0.0D;
    double otrasAlicuotas = 0.0D;
    double bonoSinAlicuota = 0.0D;
    double totalBono = 0.0D;
    double montoVariable = 0.0D;
    double bonoExtra = 0.0D;
    int mes = 0;

    int anioDeFechaFin = fechaFin.getYear() + 1900;

    Connection connection = null;

    ResultSet rsSemana = null;
    PreparedStatement stSemana = null;

    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;

    ResultSet rsConceptoVacaciones = null;
    PreparedStatement stConceptoVacaciones = null;

    ResultSet rsConceptoAlicuota = null;
    PreparedStatement stConceptoAlicuota = null;

    ResultSet rsConceptoPetrolero = null;
    PreparedStatement stConceptoPetrolero = null;

    ResultSet rsConceptoBonoExtra = null;
    PreparedStatement stConceptoBonoExtra = null;

    ResultSet rsConceptoBonoVacacional = null;
    PreparedStatement stConceptoBonoVacacional = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    ResultSet rsOtrasAlicuotas = null;
    PreparedStatement stOtrasAlicuotas = null;

    Statement stBorrarOtrasAlicuotas = null;
    Statement stBorrarCalculoVacacional = null;
    Statement stInsert = null;
    Statement stInsert2 = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      stInsert = connection.createStatement();
      stInsert2 = connection.createStatement();

      stBorrarCalculoVacacional = connection.createStatement();
      sql.append("delete from calculovacacional where id_tipo_personal = " + idTipoPersonal);
      stBorrarCalculoVacacional.addBatch(sql.toString());
      stBorrarCalculoVacacional.executeBatch();

      sql = new StringBuffer();
      sql.append("select id_concepto_tipo_personal, tipo, tope_unidades, ");
      sql.append(" tope_monto, numero_meses, mes_cerrado, alicuota_vacacional, id_concepto_alicuota, mes30 ");
      sql.append(" from conceptovacaciones ");
      sql.append("  where id_tipo_personal = ? ");

      stConceptoVacaciones = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoVacaciones.setLong(1, idTipoPersonal);

      if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1601'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoAlicuota = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoAlicuota.setLong(1, idTipoPersonal);
        rsConceptoAlicuota = stConceptoAlicuota.executeQuery();
        rsConceptoAlicuota.next();
      }

      if (parametroVarios.getAlicuotaBonoPetrolero().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1701'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoPetrolero = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoPetrolero.setLong(1, idTipoPersonal);
        rsConceptoPetrolero = stConceptoPetrolero.executeQuery();
        rsConceptoPetrolero.next();
      }

      if (parametroVarios.getBonoExtra().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1502'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoBonoExtra = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoBonoExtra.setLong(1, idTipoPersonal);
        rsConceptoBonoExtra = stConceptoBonoExtra.executeQuery();
        rsConceptoBonoExtra.next();
      }

      sql = new StringBuffer();
      sql.append("select id_concepto_tipo_personal, id_frecuencia_tipo_personal  ");
      sql.append(" from conceptotipopersonal, concepto  ");
      sql.append(" where conceptotipopersonal.id_concepto = concepto.id_concepto");
      sql.append("  and id_tipo_personal = ? and concepto.cod_concepto = '1500'");

      stConceptoBonoVacacional = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoBonoVacacional.setLong(1, idTipoPersonal);
      rsConceptoBonoVacacional = stConceptoBonoVacacional.executeQuery();
      rsConceptoBonoVacacional.next();

      if (tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        sql = new StringBuffer();
        sql.append(" select anio, mes from semana where fecha_inicio = ? ");
        this.log.error(sql);
        stSemana = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stSemana.setDate(1, fechaInicioSql);
        rsSemana = stSemana.executeQuery();
        rsSemana.next();
      }

      sql = new StringBuffer();
      sql.append("select t.id_trabajador, t.anio_vacaciones, tu.jornada_diaria, t.id_cargo, ");
      sql.append(" tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, p.anios_servicio_apn");
      sql.append(" from trabajador t, personal p, turno tu, tipopersonal tp ");
      sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and tp.id_tipo_personal = ? and t.estatus = 'A'");
      sql.append("  and (t.mes_vacaciones*100)+t.dia_vacaciones >= ? ");
      sql.append("  and (t.mes_vacaciones)*100+t.dia_vacaciones <= ? ");
      sql.append(" and t.anio_vacaciones < ?");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, idTipoPersonal);
      stTrabajadores.setInt(2, mesInicio * 100 + diaInicio);
      stTrabajadores.setInt(3, mesFin * 100 + diaFin);
      stTrabajadores.setInt(4, fechaInicio.getYear() + 1900);
      rsTrabajadores = stTrabajadores.executeQuery();

      while (rsTrabajadores.next())
      {
        stBorrarOtrasAlicuotas = connection.createStatement();
        sql = new StringBuffer();
        sql.append("delete from otrasalicuotas where id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
        stBorrarOtrasAlicuotas.addBatch(sql.toString());
        stBorrarOtrasAlicuotas.executeBatch();

        bonoExtra = 0.0D;
        totalUnidades = 0.0D;
        totalMontoA = 0.0D;
        totalMontoB = 0.0D;
        totalMontoC = 0.0D;
        totalMontoD = 0.0D;
        aniosCumple = 0;
        aniosCumple = fechaFin.getYear() + 1900 - rsTrabajadores.getInt("anio_vacaciones");
        if ((parametroVarios.getSumoApn().equals("S")) && (rsTrabajadores.getInt("anios_servicio_apn") > 0)) {
          aniosCumple += rsTrabajadores.getInt("anios_servicio_apn");
        }
        this.log.error("aniosCumple: " + aniosCumple);

        VacacionesPorAnio vacacionesPorAnio = this.definicionesNoGenBusiness.findVacacionesPorAnioForAniosServicio(idTipoPersonal, aniosCumple);
        diasBono = 0.0D;
        otrasAlicuotas = 0.0D;
        rsConceptoVacaciones = stConceptoVacaciones.executeQuery();

        while (rsConceptoVacaciones.next()) {
          montoA = 0.0D;
          montoB = 0.0D;
          montoC = 0.0D;
          montoD = 0.0D;

          if (rsConceptoVacaciones.getString("tipo").equals("B")) {
            rsConceptos = null;
            stConceptos = null;

            sql = new StringBuffer();
            sql.append("select cf.monto, cf.unidades, fp.cod_frecuencia_pago ");
            sql.append(" from conceptofijo cf, frecuenciatipopersonal ftp, frecuenciapago fp ");
            sql.append(" where cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
            sql.append("  and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
            sql.append("  and cf.id_concepto_tipo_personal = ? ");
            sql.append("  and cf.id_trabajador = ? ");
            sql.append("  and cf.estatus = 'A' ");

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));

            rsConceptos = stConceptos.executeQuery();

            while (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              if (rsConceptos.getInt("cod_frecuencia_pago") == 3)
                montoA += rsConceptos.getDouble("monto") * 2.0D;
              else if (rsConceptos.getInt("cod_frecuencia_pago") == 4)
                montoA += rsConceptos.getDouble("monto") / 7.0D * 30.0D;
              else if (rsConceptos.getInt("cod_frecuencia_pago") == 10)
                montoA += rsConceptos.getDouble("monto") * 4.0D;
              else if ((rsConceptos.getInt("cod_frecuencia_pago") == 1) || 
                (rsConceptos.getInt("cod_frecuencia_pago") == 2) || 
                (rsConceptos.getInt("cod_frecuencia_pago") > 10))
              {
                montoA += rsConceptos.getDouble("monto");
              } else if ((rsConceptos.getInt("cod_frecuencia_pago") > 4) && 
                (rsConceptos.getInt("cod_frecuencia_pago") < 10)) {
                if (!rsTrabajadores.getString("formula_integral").equals("4"))
                  montoA += rsConceptos.getDouble("monto");
                else if (rsTrabajadores.getString("formula_integral").equals("4")) {
                  montoA += rsConceptos.getDouble("monto") * 12.0D / 52.0D / 7.0D * 30.0D;
                }
              }

            }

            if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
              montoA = montoA / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
            }
            if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoA)) {
              montoA = rsConceptoVacaciones.getDouble("tope_monto");
            }

            montoA /= 30.0D;
            totalMontoA += montoA;
          }
          else if (rsConceptoVacaciones.getString("tipo").equals("D"))
          {
            rsConceptos = null;
            stConceptos = null;

            sql = new StringBuffer();
            if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
              sql.append("select sum(hq.monto_asigna) as monto, sum(hq.unidades) as unidades ");
              sql.append(" from historicoquincena hq ");
              sql.append(" where hq.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio = ?");
              sql.append(" group by id_trabajador");
            } else {
              sql.append("select sum(hs.monto_asigna) as monto , sum(hs.unidades) as unidades ");
              sql.append(" from historicosemana hs ");
              sql.append(" where hs.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio = ?");
              sql.append(" group by id_trabajador");
            }

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            stConceptos.setInt(3, anioDeFechaFin);

            rsConceptos = stConceptos.executeQuery();

            if (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              montoB += rsConceptos.getDouble("monto");
            }

            if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
              montoB = montoB / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
            }
            if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoB)) {
              montoB = rsConceptoVacaciones.getDouble("tope_monto");
            }

            montoB /= parametroVarios.getDiasAnio();
            totalMontoB += montoB;
          }
          else if (rsConceptoVacaciones.getString("tipo").equals("P"))
          {
            diasDividir = 0L;
            int periodoFin;
            int periodoInicio;
            if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
            {
              if (rsConceptoVacaciones.getString("mes_cerrado").equals("S")) {
                fin.setTime(fechaInicio);
                this.log.error("1 - fin " + fin.getTime());
                if (fechaInicio.getDate() == 1) {
                  fin.add(5, -1);
                  this.log.error("2 - fin " + fin.getTime());
                } else if (fechaInicio.getDate() == 16) {
                  fin.add(5, -16);
                }
              } else {
                fin.setTime(fechaFin);
              }
              this.log.error("3 - fin " + fin.getTime());
              inicio.setTime(fin.getTime());
              this.log.error("0 - INICIO " + inicio.getTime());
              this.log.error("4 - fin " + fin.getTime());
              inicio.add(5, 1);
              this.log.error("5 - fin " + fin.getTime());
              this.log.error("1 - INICIO " + inicio.getTime());
              inicio.add(2, -rsConceptoVacaciones.getInt("numero_meses"));
              this.log.error("inicio " + inicio.getTime());
              this.log.error("fin " + fin.getTime());
              if (rsConceptoVacaciones.getString("mes30").equals("S")) {
                diasDividir = 30 * rsConceptoVacaciones.getInt("numero_meses");
              }
              else {
                long dias = fin.getTimeInMillis() - inicio.getTimeInMillis();
                diasDividir = dias / 86400000L;
              }

              int periodoInicio = (inicio.getTime().getYear() + 1900) * 100 + inicio.getTime().getMonth() + 1;
              int periodoFin = (fin.getTime().getYear() + 1900) * 100 + fin.getTime().getMonth() + 1;
              this.log.error("* periodo inicio " + periodoInicio);
              this.log.error("* periodo fin " + periodoFin);
            } else {
              if (rsConceptoVacaciones.getString("mes_cerrado").equals("S")) {
                int periodoFin = Integer.valueOf(rsSemana.getString("mes")).intValue();
                if (periodoFin == 1) {
                  periodoFin = 12;
                  periodoFin = (rsSemana.getInt("anio") - 1) * 100 + periodoFin;
                } else {
                  periodoFin--;
                  periodoFin = rsSemana.getInt("anio") * 100 + periodoFin;
                }
                int periodoInicio = Integer.valueOf(rsSemana.getString("mes")).intValue() - rsConceptoVacaciones.getInt("numero_meses");
                if (periodoInicio > 0)
                  periodoInicio = rsSemana.getInt("anio") * 100 + periodoInicio;
                else
                  periodoInicio = (rsSemana.getInt("anio") - 1) * 100 + (12 + periodoInicio);
              }
              else {
                periodoFin = Integer.valueOf(rsSemana.getString("mes")).intValue();
                periodoFin = rsSemana.getInt("anio") * 100 + periodoFin;
                periodoInicio = Integer.valueOf(rsSemana.getString("mes")).intValue() - rsConceptoVacaciones.getInt("numero_meses");
                if (periodoInicio > 0)
                  periodoInicio = rsSemana.getInt("anio") * 100 + periodoInicio;
                else {
                  periodoInicio = (rsSemana.getInt("anio") - 1) * 100 + (12 + periodoInicio);
                }
              }
              if (rsConceptoVacaciones.getString("mes30").equals("S")) {
                diasDividir = 30 * rsConceptoVacaciones.getInt("numero_meses");
              }
              else {
                long dias = fin.getTimeInMillis() - inicio.getTimeInMillis();
                diasDividir = dias / 86400000L;
              }

              this.log.error("0 - INICIO " + inicio.getTime());
              this.log.error("1 - fin " + fin.getTime());
            }

            rsConceptos = null;
            stConceptos = null;

            sql = new StringBuffer();
            if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
              sql.append("select sum(hq.monto_asigna) as monto, sum(hq.unidades) as unidades ");
              sql.append(" from historicoquincena hq ");
              sql.append(" where hq.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio*100+mes >= ? and  anio*100+mes <= ? ");
              sql.append(" group by id_trabajador");
            } else {
              sql.append("select sum(hs.monto_asigna) as monto , sum(hs.unidades) as unidades ");
              sql.append(" from historicosemana hs ");
              sql.append(" where hs.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio*100+mes >= ? and  anio*100+mes <= ? ");
              sql.append(" group by id_trabajador");
            }

            this.log.error("antes de ejecutar ");

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            stConceptos.setInt(3, periodoInicio);
            stConceptos.setInt(4, periodoFin);

            this.log.error("antes de ejecutar  2");

            rsConceptos = stConceptos.executeQuery();
            this.log.error("periodo inicio " + periodoInicio);
            this.log.error("periodo fin " + periodoFin);
            this.log.error("despues  de ejecutar ");

            if (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              montoC += rsConceptos.getDouble("monto");
            }
            this.log.error("antes de ejecutar 2");

            montoC /= diasDividir;
            totalMontoC += montoC;
          }
          else if (rsConceptoVacaciones.getString("tipo").equals("Y"))
          {
            rsConceptos = null;
            stConceptos = null;

            sql = new StringBuffer();

            sql.append("select sum(cf.monto) as monto, sum(cf.unidades) as unidades ");
            sql.append(" from conceptofijo cf ");
            sql.append(" where cf.id_concepto_tipo_personal = ?");
            sql.append(" and cf.estatus = 'Y'");
            sql.append("  and id_trabajador = ?");
            sql.append(" group by id_trabajador");

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));

            rsConceptos = stConceptos.executeQuery();

            if (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              montoD += rsConceptos.getDouble("monto");
            }

            if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
              montoD = montoD / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
            }
            if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoD)) {
              montoD = rsConceptoVacaciones.getDouble("tope_monto");
            }

            montoD /= parametroVarios.getDiasAnio();

            if (rsConceptoVacaciones.getString("alicuota_vacacional").equals("S")) {
              sql = new StringBuffer();
              montoD = NumberTools.twoDecimal(montoD);
              otrasAlicuotas += montoD * vacacionesPorAnio.getDiasBono();
              sql.append("insert into otrasalicuotas (id_trabajador, id_concepto_alicuota, monto, id_otras_alicuotas) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(rsConceptoVacaciones.getLong("id_concepto_alicuota") + ",");
              sql.append(montoD * vacacionesPorAnio.getDiasBono() + ", ");
              sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.procesoAniversario.OtrasAlicuotas") + ")");
              stInsert2.addBatch(sql.toString());
            }

            totalMontoD += montoD;
          }
        }

        stInsert2.executeBatch();
        double montoAlicuota;
        if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
          double montoAlicuota = calcularConceptoBeanBusiness.calcular(rsConceptoAlicuota.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), rsConceptoAlicuota.getDouble("unidades"), rsConceptoAlicuota.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoAlicuota.getDouble("valor"), rsConceptoAlicuota.getDouble("tope_minimo"), rsConceptoAlicuota.getDouble("tope_maximo"));
          montoAlicuota /= parametroVarios.getDiasAnio();
          montoAlicuota *= vacacionesPorAnio.getDiasBono();
        } else {
          montoAlicuota = 0.0D;
        }
        double montoPetrolero;
        if (parametroVarios.getAlicuotaBonoPetrolero().equals("S")) {
          double montoPetrolero = calcularConceptoBeanBusiness.calcular(rsConceptoPetrolero.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), 1.0D, rsConceptoPetrolero.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoPetrolero.getDouble("valor"), rsConceptoPetrolero.getDouble("tope_minimo"), rsConceptoPetrolero.getDouble("tope_maximo"));
          montoPetrolero = (parametroVarios.getConstantePetroleroA() + 
            parametroVarios.getConstantePetroleroB() * vacacionesPorAnio.getDiasBono()) * 
            montoPetrolero;
          montoPetrolero /= parametroVarios.getConstantePetroleroC();
          montoPetrolero /= parametroVarios.getDiasAnio();
          montoPetrolero *= vacacionesPorAnio.getDiasBono();
        } else {
          montoPetrolero = 0.0D;
        }

        mes = fechaInicio.getMonth() + 1;
        bonoSinAlicuota = (totalMontoA + totalMontoB + totalMontoC + totalMontoD) * vacacionesPorAnio.getDiasBono();
        bonoExtra = (totalMontoA + totalMontoB + totalMontoC + totalMontoD) * vacacionesPorAnio.getDiasExtra();
        totalBono = bonoSinAlicuota + montoAlicuota + montoPetrolero;

        sql = new StringBuffer();
        sql.append("insert into calculovacacional (base_fijo, base_devengado, base_promedio, ");
        sql.append(" base_proyectado,  bono_sin_alicuota, anios_servicio, momento_pago, ");
        sql.append(" mes, dias, monto_alicuota, monto_petrolero, bono_extra, total_bono, id_trabajador, id_tipo_personal, id_calculo_vacacional) values(");
        sql.append(NumberTools.twoDecimal(totalMontoA) + ", " + NumberTools.twoDecimal(totalMontoB) + ", " + NumberTools.twoDecimal(totalMontoC) + ", " + NumberTools.twoDecimal(totalMontoD) + ", " + NumberTools.twoDecimal(bonoSinAlicuota) + ", ");
        sql.append(aniosCumple + ", '" + parametroVarios.getAniversarioDisfrute() + "', " + mes + ", ");
        sql.append(vacacionesPorAnio.getDiasBono() + ", " + NumberTools.twoDecimal(montoAlicuota) + ", " + NumberTools.twoDecimal(montoPetrolero) + ", " + NumberTools.twoDecimal(bonoExtra) + ", " + NumberTools.twoDecimal(totalBono) + ", " + rsTrabajadores.getLong("id_trabajador") + ", ");
        sql.append(tipoPersonal.getIdTipoPersonal() + ", ");
        sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.procesoAniversario.CalculoVacacional") + ")");

        stInsert.addBatch(sql.toString());

        if (proceso.equals("2"))
        {
          sql = new StringBuffer();
          montoVariable = bonoSinAlicuota - otrasAlicuotas;
          sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, id_concepto_variable) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsConceptoBonoVacacional.getLong("id_concepto_tipo_personal") + ", ");
          sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
          sql.append(vacacionesPorAnio.getDiasBono() + ",");
          sql.append(NumberTools.twoDecimal(montoVariable) + ",");
          sql.append("'" + fechaActualSql + "',");

          sql.append("'A', ");

          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

          stInsert.addBatch(sql.toString());

          if (parametroVarios.getAlicuotaBfaBvac().equals("S"))
          {
            sql = new StringBuffer();
            sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
            sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
            sql.append(" estatus, id_concepto_variable) values(");
            sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
            sql.append(rsConceptoAlicuota.getLong("id_concepto_tipo_personal") + ", ");
            sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
            sql.append("0,");
            sql.append(NumberTools.twoDecimal(montoAlicuota) + ",");
            sql.append("'" + fechaActualSql + "',");
            sql.append("'A', ");

            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

            stInsert.addBatch(sql.toString());
          }

          if (parametroVarios.getAlicuotaBonoPetrolero().equals("S"))
          {
            sql = new StringBuffer();
            sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
            sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
            sql.append(" estatus, id_concepto_variable) values(");
            sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
            sql.append(rsConceptoPetrolero.getLong("id_concepto_tipo_personal") + ", ");
            sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
            sql.append("0,");
            sql.append(NumberTools.twoDecimal(montoPetrolero) + ",");
            sql.append("'" + fechaActualSql + "',");
            sql.append("'A', ");

            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

            stInsert.addBatch(sql.toString());
          }

          if (bonoExtra != 0.0D)
          {
            sql = new StringBuffer();
            sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
            sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
            sql.append(" estatus, id_concepto_variable) values(");
            sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
            sql.append(rsConceptoBonoExtra.getLong("id_concepto_tipo_personal") + ", ");
            sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
            sql.append("0,");
            sql.append(NumberTools.twoDecimal(bonoExtra) + ",");
            sql.append("'" + fechaActualSql + "',");
            sql.append("'A', ");

            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

            stInsert.addBatch(sql.toString());
          }

          if (otrasAlicuotas != 0.0D)
          {
            sql = new StringBuffer();
            sql.append("select id_concepto_alicuota, monto ");
            sql.append(" from otrasalicuotas ");
            sql.append("  where id_trabajador = ? ");

            stOtrasAlicuotas = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stOtrasAlicuotas.setLong(1, rsTrabajadores.getLong("id_trabajador"));
            rsOtrasAlicuotas = stOtrasAlicuotas.executeQuery();
            while (rsOtrasAlicuotas.next()) {
              sql = new StringBuffer();
              sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
              sql.append(" estatus, id_concepto_variable) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(rsOtrasAlicuotas.getLong("id_concepto_alicuota") + ", ");
              sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
              sql.append("0,");
              sql.append(rsOtrasAlicuotas.getDouble("monto") + ",");
              sql.append("'" + fechaActualSql + "',");
              sql.append("'A', ");
              sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

              stInsert.addBatch(sql.toString());
            }

          }

        }

      }

      if (proceso.equals("2")) {
        sql = new StringBuffer();
        sql.append("update seguridadaniversario set fecha_ultimo= '" + fechaFinSql + "', ");
        sql.append("fecha_proceso = '" + fechaActualSql + "', usuario = '" + usuario + "' where id_seguridad_aniversario = " + idSeguridadAniversario);
        stInsert.addBatch(sql.toString());
      }

      stInsert.executeBatch();
      connection.commit();
    } finally {
      if (rsSemana != null) try {
          rsSemana.close();
        } catch (Exception localException) {
        } if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException1) {
        } if (rsConceptoVacaciones != null) try {
          rsConceptoVacaciones.close();
        } catch (Exception localException2) {
        } if (rsConceptoAlicuota != null) try {
          rsConceptoAlicuota.close();
        } catch (Exception localException3) {
        } if (rsConceptoPetrolero != null) try {
          rsConceptoPetrolero.close();
        } catch (Exception localException4) {
        } if (rsConceptoBonoExtra != null) try {
          rsConceptoBonoExtra.close();
        } catch (Exception localException5) {
        } if (rsConceptoBonoVacacional != null) try {
          rsConceptoBonoVacacional.close();
        } catch (Exception localException6) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException7) {
        } if (rsOtrasAlicuotas != null) try {
          rsOtrasAlicuotas.close();
        } catch (Exception localException8) {
        } if (stSemana != null) try {
          stSemana.close();
        } catch (Exception localException9) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException10) {
        } if (stConceptoVacaciones != null) try {
          stConceptoVacaciones.close();
        } catch (Exception localException11) {
        } if (stConceptoAlicuota != null) try {
          stConceptoAlicuota.close();
        } catch (Exception localException12) {
        } if (stConceptoPetrolero != null) try {
          stConceptoPetrolero.close();
        } catch (Exception localException13) {
        } if (stConceptoBonoExtra != null) try {
          stConceptoBonoExtra.close();
        } catch (Exception localException14) {
        } if (stConceptoBonoVacacional != null) try {
          stConceptoBonoVacacional.close();
        } catch (Exception localException15) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException16) {
        } if (stOtrasAlicuotas != null) try {
          stOtrasAlicuotas.close();
        } catch (Exception localException17) {
        } if (stBorrarOtrasAlicuotas != null) try {
          stBorrarOtrasAlicuotas.close();
        } catch (Exception localException18) {
        } if (stBorrarCalculoVacacional != null) try {
          stBorrarCalculoVacacional.close();
        } catch (Exception localException19) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException20) {
        } if (stInsert2 != null) try {
          stInsert2.close();
        } catch (Exception localException21) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException22) {  }
 
    }
    return true;
  }

  public long proyectar(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, long idUnidadAdministradora, int anioProceso, int mesProceso, long id)
    throws Exception
  {
    Connection connection = null;

    ResultSet rsSemana = null;
    PreparedStatement stSemana = null;

    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;

    ResultSet rsConceptoVacaciones = null;
    PreparedStatement stConceptoVacaciones = null;

    ResultSet rsConceptoAlicuota = null;
    PreparedStatement stConceptoAlicuota = null;

    ResultSet rsConceptoPetrolero = null;
    PreparedStatement stConceptoPetrolero = null;

    ResultSet rsConceptoBonoExtra = null;
    PreparedStatement stConceptoBonoExtra = null;

    ResultSet rsConceptoBonoVacacional = null;
    PreparedStatement stConceptoBonoVacacional = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    ResultSet rsOtrasAlicuotas = null;
    PreparedStatement stOtrasAlicuotas = null;

    ResultSet rsFrecuencia = null;
    PreparedStatement stFrecuencia = null;

    Statement stInsert = null;
    Statement stInsert2 = null;

    Calendar fin = Calendar.getInstance();
    Calendar inicio = Calendar.getInstance();

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();
    try
    {
      this.txn.open();
      ParametroVarios parametroVarios = new ParametroVarios();
      Collection colParametroVarios = this.definicionesBusiness.findParametroVariosByTipoPersonal(idTipoPersonal);
      Iterator iteratorParametroVarios = colParametroVarios.iterator();
      parametroVarios = (ParametroVarios)iteratorParametroVarios.next();

      TipoPersonal tipoPersonal = new TipoPersonal();
      tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

      int diaInicio = fechaInicio.getDate();
      int mesInicio = fechaInicio.getMonth() + 1;
      int diaFin = fechaFin.getDate();
      int mesFin = fechaFin.getMonth() + 1;

      java.util.Date fechaActual = new java.util.Date();
      java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());
      java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

      java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());

      double montoA = 0.0D;
      double montoB = 0.0D;
      double montoC = 0.0D;
      double montoD = 0.0D;
      double totalMontoA = 0.0D;
      double totalMontoB = 0.0D;
      double totalMontoC = 0.0D;
      double totalMontoD = 0.0D;
      double totalUnidades = 0.0D;
      int aniosCumple = 0;
      long diasDividir = 0L;
      double diasBono = 0.0D;
      double otrasAlicuotas = 0.0D;
      double bonoSinAlicuota = 0.0D;
      double totalBono = 0.0D;
      double montoVariable = 0.0D;
      double bonoExtra = 0.0D;
      int mes = 0;

      int anioDeFechaFin = fechaFin.getYear() + 1900;

      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      stInsert = connection.createStatement();
      stInsert2 = connection.createStatement();
      StringBuffer sql = new StringBuffer();

      sql = new StringBuffer();
      sql.append("select ftp.id_frecuencia_tipo_personal ");
      sql.append(" from frecuenciatipopersonal ftp, frecuenciapago fp ");
      sql.append(" where id_tipo_personal = ? ");
      sql.append(" and fp.cod_frecuencia_pago = ?");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      stFrecuencia = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select id_concepto_tipo_personal, tipo, tope_unidades, ");
      sql.append(" tope_monto, numero_meses, mes_cerrado, alicuota_vacacional, id_concepto_alicuota, mes30 ");
      sql.append(" from conceptovacaciones ");
      sql.append("  where id_tipo_personal = ? ");

      stConceptoVacaciones = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoVacaciones.setLong(1, idTipoPersonal);

      if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1601'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoAlicuota = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoAlicuota.setLong(1, idTipoPersonal);
        rsConceptoAlicuota = stConceptoAlicuota.executeQuery();
        rsConceptoAlicuota.next();
      }

      if (parametroVarios.getAlicuotaBonoPetrolero().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1701'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoPetrolero = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoPetrolero.setLong(1, idTipoPersonal);
        rsConceptoPetrolero = stConceptoPetrolero.executeQuery();
        rsConceptoPetrolero.next();
      }

      if (parametroVarios.getBonoExtra().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1502'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoBonoExtra = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoBonoExtra.setLong(1, idTipoPersonal);
        rsConceptoBonoExtra = stConceptoBonoExtra.executeQuery();
        rsConceptoBonoExtra.next();
      }

      sql = new StringBuffer();
      sql.append("select id_concepto_tipo_personal, id_frecuencia_tipo_personal  ");
      sql.append(" from conceptotipopersonal, concepto  ");
      sql.append(" where conceptotipopersonal.id_concepto = concepto.id_concepto");
      sql.append("  and id_tipo_personal = ? and concepto.cod_concepto = '1500'");

      stConceptoBonoVacacional = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoBonoVacacional.setLong(1, idTipoPersonal);
      rsConceptoBonoVacacional = stConceptoBonoVacacional.executeQuery();

      if (tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        sql = new StringBuffer();
        sql.append(" select anio, mes from semana where fecha_inicio = ? ");
        stSemana = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stSemana.setDate(1, fechaInicioSql);
        rsSemana = stSemana.executeQuery();
        rsSemana.next();
      }

      sql = new StringBuffer();
      sql.append("select t.id_trabajador, t.anio_vacaciones, t.dia_vacaciones, tu.jornada_diaria, t.id_cargo, ");
      sql.append(" tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, p.anios_servicio_apn");
      sql.append(" from trabajador t, personal p, turno tu, tipopersonal tp, dependencia d, administradorauel auel ");
      sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_dependencia = d.id_dependencia");
      sql.append(" and d.id_administradora_uel = auel.id_administradora_uel");
      sql.append(" and auel.id_unidad_administradora = ? ");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and tp.id_tipo_personal = ? and t.estatus = 'A'");
      sql.append("  and (t.mes_vacaciones*100)+t.dia_vacaciones >= ? ");
      sql.append("  and (t.mes_vacaciones)*100+t.dia_vacaciones <= ? ");
      sql.append(" and t.anio_vacaciones < ?");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, idUnidadAdministradora);
      stTrabajadores.setLong(2, idTipoPersonal);
      stTrabajadores.setInt(3, mesInicio * 100 + diaInicio);
      stTrabajadores.setInt(4, mesFin * 100 + diaFin);
      stTrabajadores.setInt(5, fechaInicio.getYear() + 1900);
      rsTrabajadores = stTrabajadores.executeQuery();

      if (rsConceptoBonoVacacional.next()) {
        while (rsTrabajadores.next())
        {
          id += 1L;

          bonoExtra = 0.0D;
          totalUnidades = 0.0D;
          totalMontoA = 0.0D;
          totalMontoB = 0.0D;
          totalMontoC = 0.0D;
          totalMontoD = 0.0D;
          aniosCumple = 0;
          aniosCumple = fechaFin.getYear() + 1900 - rsTrabajadores.getInt("anio_vacaciones");
          if ((parametroVarios.getSumoApn().equals("S")) && (rsTrabajadores.getInt("anios_servicio_apn") > 0)) {
            aniosCumple += rsTrabajadores.getInt("anios_servicio_apn");
          }
          this.log.error("aniosCumple: " + aniosCumple);

          VacacionesPorAnio vacacionesPorAnio = this.definicionesNoGenBusiness.findVacacionesPorAnioForAniosServicio(idTipoPersonal, aniosCumple);
          diasBono = 0.0D;
          otrasAlicuotas = 0.0D;
          rsConceptoVacaciones = stConceptoVacaciones.executeQuery();

          while (rsConceptoVacaciones.next()) {
            montoA = 0.0D;
            montoB = 0.0D;
            montoC = 0.0D;
            montoD = 0.0D;

            if (rsConceptoVacaciones.getString("tipo").equals("B")) {
              rsConceptos = null;
              stConceptos = null;

              sql = new StringBuffer();
              sql.append("select cf.monto, cf.unidades, fp.cod_frecuencia_pago ");
              sql.append(" from conceptofijo cf, frecuenciatipopersonal ftp, frecuenciapago fp ");
              sql.append(" where cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
              sql.append("  and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
              sql.append("  and cf.id_concepto_tipo_personal = ? ");
              sql.append("  and cf.id_trabajador = ? ");
              sql.append("  and cf.estatus = 'A' ");

              stConceptos = connection.prepareStatement(
                sql.toString(), 
                1003, 
                1007);
              stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
              stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));

              rsConceptos = stConceptos.executeQuery();

              while (rsConceptos.next()) {
                totalUnidades += rsConceptos.getDouble("unidades");
                if (rsConceptos.getInt("cod_frecuencia_pago") == 3)
                  montoA += rsConceptos.getDouble("monto") * 2.0D;
                else if (rsConceptos.getInt("cod_frecuencia_pago") == 4)
                  montoA += rsConceptos.getDouble("monto") / 7.0D * 30.0D;
                else if (rsConceptos.getInt("cod_frecuencia_pago") == 10)
                  montoA += rsConceptos.getDouble("monto") * 4.0D;
                else if ((rsConceptos.getInt("cod_frecuencia_pago") == 1) || 
                  (rsConceptos.getInt("cod_frecuencia_pago") == 2) || 
                  (rsConceptos.getInt("cod_frecuencia_pago") > 10))
                {
                  montoA += rsConceptos.getDouble("monto");
                } else if ((rsConceptos.getInt("cod_frecuencia_pago") > 4) && 
                  (rsConceptos.getInt("cod_frecuencia_pago") < 10)) {
                  if (!rsTrabajadores.getString("formula_integral").equals("4"))
                    montoA += rsConceptos.getDouble("monto");
                  else if (rsTrabajadores.getString("formula_integral").equals("4")) {
                    montoA += rsConceptos.getDouble("monto") * 12.0D / 52.0D / 7.0D * 30.0D;
                  }
                }

              }

              if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
                montoA = montoA / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
              }
              if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoA)) {
                montoA = rsConceptoVacaciones.getDouble("tope_monto");
              }

              montoA /= 30.0D;
              totalMontoA += montoA;
            }
            else if (rsConceptoVacaciones.getString("tipo").equals("D"))
            {
              rsConceptos = null;
              stConceptos = null;

              sql = new StringBuffer();
              if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
                sql.append("select sum(hq.monto_asigna) as monto, sum(hq.unidades) as unidades ");
                sql.append(" from historicoquincena hq ");
                sql.append(" where hq.id_concepto_tipo_personal = ?");
                sql.append("  and id_trabajador = ?");
                sql.append(" and anio = ?");
                sql.append(" group by id_trabajador");
              } else {
                sql.append("select sum(hs.monto_asigna) as monto , sum(hs.unidades) as unidades ");
                sql.append(" from historicosemana hs ");
                sql.append(" where hs.id_concepto_tipo_personal = ?");
                sql.append("  and id_trabajador = ?");
                sql.append(" and anio = ?");
                sql.append(" group by id_trabajador");
              }

              stConceptos = connection.prepareStatement(
                sql.toString(), 
                1003, 
                1007);
              stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
              stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
              stConceptos.setInt(3, anioDeFechaFin);

              rsConceptos = stConceptos.executeQuery();

              if (rsConceptos.next()) {
                totalUnidades += rsConceptos.getDouble("unidades");
                montoB += rsConceptos.getDouble("monto");
              }

              if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
                montoB = montoB / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
              }
              if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoB)) {
                montoB = rsConceptoVacaciones.getDouble("tope_monto");
              }

              montoB /= parametroVarios.getDiasAnio();
              totalMontoB += montoB;
            }
            else if (rsConceptoVacaciones.getString("tipo").equals("P"))
            {
              diasDividir = 0L;

              this.log.error("en promedio ");
              int periodoFin;
              int periodoInicio;
              if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
              {
                if (rsConceptoVacaciones.getString("mes_cerrado").equals("S")) {
                  fin.setTime(fechaInicio);
                  this.log.error("1 - fin " + fin.getTime());
                  if (fechaInicio.getDate() == 1) {
                    fin.add(5, -1);
                    this.log.error("2 - fin " + fin.getTime());
                  } else if (fechaInicio.getDate() == 16) {
                    fin.add(5, -17);
                  }
                } else {
                  fin.setTime(fechaFin);
                }
                this.log.error("3 - fin " + fin.getTime());
                inicio.setTime(fin.getTime());
                this.log.error("0 - INICIO " + inicio.getTime());
                this.log.error("4 - fin " + fin.getTime());
                inicio.add(5, 1);
                this.log.error("5 - fin " + fin.getTime());
                this.log.error("1 - INICIO " + inicio.getTime());
                inicio.add(2, -rsConceptoVacaciones.getInt("numero_meses"));
                this.log.error("inicio " + inicio.getTime());
                this.log.error("fin " + fin.getTime());
                if (rsConceptoVacaciones.getString("mes30").equals("S")) {
                  diasDividir = 30 * rsConceptoVacaciones.getInt("numero_meses");
                }
                else {
                  long dias = fin.getTimeInMillis() - inicio.getTimeInMillis();
                  diasDividir = dias / 86400000L;
                }

                int periodoInicio = (inicio.getTime().getYear() + 1900) * 100 + inicio.getTime().getMonth() + 1;
                int periodoFin = (fin.getTime().getYear() + 1900) * 100 + fin.getTime().getMonth() + 1;
                this.log.error("* periodo inicio " + periodoInicio);
                this.log.error("* periodo fin " + periodoFin);
              } else {
                if (rsConceptoVacaciones.getString("mes_cerrado").equals("S")) {
                  int periodoFin = Integer.valueOf(rsSemana.getString("mes")).intValue();
                  if (periodoFin == 1) {
                    periodoFin = 12;
                    periodoFin = (rsSemana.getInt("anio") - 1) * 100 + periodoFin;
                  } else {
                    periodoFin--;
                    periodoFin = rsSemana.getInt("anio") * 100 + periodoFin;
                  }
                  int periodoInicio = Integer.valueOf(rsSemana.getString("mes")).intValue() - rsConceptoVacaciones.getInt("numero_meses");
                  if (periodoInicio > 0)
                    periodoInicio = rsSemana.getInt("anio") * 100 + periodoInicio;
                  else
                    periodoInicio = (rsSemana.getInt("anio") - 1) * 100 + (12 + periodoInicio);
                }
                else {
                  periodoFin = Integer.valueOf(rsSemana.getString("mes")).intValue();
                  periodoFin = rsSemana.getInt("anio") * 100 + periodoFin;
                  periodoInicio = Integer.valueOf(rsSemana.getString("mes")).intValue() - rsConceptoVacaciones.getInt("numero_meses");
                  if (periodoInicio > 0)
                    periodoInicio = rsSemana.getInt("anio") * 100 + periodoInicio;
                  else {
                    periodoInicio = (rsSemana.getInt("anio") - 1) * 100 + (12 + periodoInicio);
                  }
                }
                if (rsConceptoVacaciones.getString("mes30").equals("S")) {
                  diasDividir = 30 * rsConceptoVacaciones.getInt("numero_meses");
                }
                else {
                  long dias = fin.getTimeInMillis() - inicio.getTimeInMillis();
                  diasDividir = dias / 86400000L;
                }
              }

              rsConceptos = null;
              stConceptos = null;

              sql = new StringBuffer();
              if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
                sql.append("select sum(hq.monto_asigna) as monto, sum(hq.unidades) as unidades ");
                sql.append(" from historicoquincena hq ");
                sql.append(" where hq.id_concepto_tipo_personal = ?");
                sql.append("  and id_trabajador = ?");
                sql.append(" and anio*100+mes >= ? and  anio*100+mes <= ? ");
                sql.append(" group by id_trabajador");
              } else {
                sql.append("select sum(hs.monto_asigna) as monto , sum(hs.unidades) as unidades ");
                sql.append(" from historicosemana hs ");
                sql.append(" where hs.id_concepto_tipo_personal = ?");
                sql.append("  and id_trabajador = ?");
                sql.append(" and anio*100+mes >= ? and  anio*100+mes <= ? ");
                sql.append(" group by id_trabajador");
              }

              stConceptos = connection.prepareStatement(
                sql.toString(), 
                1003, 
                1007);
              stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
              stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
              stConceptos.setInt(3, periodoInicio);
              stConceptos.setInt(4, periodoFin);

              rsConceptos = stConceptos.executeQuery();
              this.log.error("periodo inicio " + periodoInicio);
              this.log.error("periodo fin " + periodoFin);
              if (rsConceptos.next()) {
                totalUnidades += rsConceptos.getDouble("unidades");
                montoC += rsConceptos.getDouble("monto");
              }
              montoC /= diasDividir;
              totalMontoC += montoC;
            }
            else if (rsConceptoVacaciones.getString("tipo").equals("Y"))
            {
              rsConceptos = null;
              stConceptos = null;

              sql = new StringBuffer();

              sql.append("select sum(cf.monto) as monto, sum(cf.unidades) as unidades ");
              sql.append(" from conceptofijo cf ");
              sql.append(" where cf.id_concepto_tipo_personal = ?");
              sql.append(" and cf.estatus = 'Y'");
              sql.append("  and id_trabajador = ?");
              sql.append(" group by id_trabajador");

              stConceptos = connection.prepareStatement(
                sql.toString(), 
                1003, 
                1007);
              stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
              stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));

              rsConceptos = stConceptos.executeQuery();

              if (rsConceptos.next()) {
                totalUnidades += rsConceptos.getDouble("unidades");
                montoD += rsConceptos.getDouble("monto");
              }

              if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
                montoD = montoD / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
              }
              if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoD)) {
                montoD = rsConceptoVacaciones.getDouble("tope_monto");
              }

              montoD /= parametroVarios.getDiasAnio();

              totalMontoD += montoD;
            }
          }

          stInsert2.executeBatch();
          double montoAlicuota;
          if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
            double montoAlicuota = calcularConceptoBeanBusiness.calcular(rsConceptoAlicuota.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), rsConceptoAlicuota.getDouble("unidades"), rsConceptoAlicuota.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoAlicuota.getDouble("valor"), rsConceptoAlicuota.getDouble("tope_minimo"), rsConceptoAlicuota.getDouble("tope_maximo"));
            montoAlicuota /= parametroVarios.getDiasAnio();
            montoAlicuota *= vacacionesPorAnio.getDiasBono();
          } else {
            montoAlicuota = 0.0D;
          }
          double montoPetrolero;
          if (parametroVarios.getAlicuotaBonoPetrolero().equals("S")) {
            double montoPetrolero = calcularConceptoBeanBusiness.calcular(rsConceptoPetrolero.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), 1.0D, rsConceptoPetrolero.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoPetrolero.getDouble("valor"), rsConceptoPetrolero.getDouble("tope_minimo"), rsConceptoPetrolero.getDouble("tope_maximo"));
            montoPetrolero = (parametroVarios.getConstantePetroleroA() + parametroVarios.getConstantePetroleroB() * vacacionesPorAnio.getDiasBono()) * montoPetrolero;
            montoPetrolero /= parametroVarios.getConstantePetroleroC();
            montoPetrolero /= parametroVarios.getDiasAnio();
            montoPetrolero *= vacacionesPorAnio.getDiasBono();
          } else {
            montoPetrolero = 0.0D;
          }

          mes = fechaInicio.getMonth() + 1;
          bonoSinAlicuota = (totalMontoA + totalMontoB + totalMontoC + totalMontoD) * vacacionesPorAnio.getDiasBono();
          bonoExtra = (totalMontoA + totalMontoB + totalMontoC + totalMontoD) * vacacionesPorAnio.getDiasExtra();
          totalBono = bonoSinAlicuota + montoAlicuota + montoPetrolero;

          stFrecuencia.setLong(1, idTipoPersonal);
          if (rsTrabajadores.getInt("dia_vacaciones") < 15)
            stFrecuencia.setInt(2, 1);
          else {
            stFrecuencia.setInt(2, 2);
          }
          rsFrecuencia = stFrecuencia.executeQuery();

          if (rsFrecuencia.next())
          {
            sql = new StringBuffer();
            montoVariable = bonoSinAlicuota - otrasAlicuotas;
            sql.append("insert into conceptoresumen (id_trabajador, id_concepto_tipo_personal, ");
            sql.append(" id_frecuencia_tipo_personal, unidades, monto, ");
            sql.append(" tipo, id_unidad_administradora,  anio, mes, numero_nomina, id_concepto_resumen) values(");
            sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
            sql.append(rsConceptoBonoVacacional.getLong("id_concepto_tipo_personal") + ", ");
            sql.append(rsFrecuencia.getLong("id_frecuencia_tipo_personal") + ", ");
            sql.append(vacacionesPorAnio.getDiasBono() + ",");
            sql.append(NumberTools.twoDecimal(montoVariable) + ",");
            sql.append("'V', ");
            sql.append(idUnidadAdministradora + ", " + anioProceso + ", " + mesProceso + ", 0, ");
            sql.append(id + ")");

            stInsert.addBatch(sql.toString());

            this.log.error(sql.toString());

            if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
              id += 1L;
              sql = new StringBuffer();
              sql.append("insert into conceptoresumen (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto, ");
              sql.append(" tipo, id_unidad_administradora,  anio, mes, numero_nomina, id_concepto_resumen) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(rsConceptoAlicuota.getLong("id_concepto_tipo_personal") + ", ");
              sql.append(rsFrecuencia.getLong("id_frecuencia_tipo_personal") + ", ");
              sql.append("0,");
              sql.append(NumberTools.twoDecimal(montoAlicuota) + ",");
              sql.append("'V', ");
              sql.append(idUnidadAdministradora + ", " + anioProceso + ", " + mesProceso + ", 0, ");
              sql.append(id + ")");

              stInsert.addBatch(sql.toString());

              this.log.error(sql.toString());
            }

            if (parametroVarios.getAlicuotaBonoPetrolero().equals("S")) {
              id += 1L;
              sql = new StringBuffer();
              sql.append("insert into conceptoresumen (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto, ");
              sql.append(" tipo, id_unidad_administradora,  anio, mes, numero_nomina, id_concepto_resumen) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(rsConceptoPetrolero.getLong("id_concepto_tipo_personal") + ", ");
              sql.append(rsFrecuencia.getLong("id_frecuencia_tipo_personal") + ", ");
              sql.append("0,");
              sql.append(NumberTools.twoDecimal(montoPetrolero) + ",");
              sql.append("'V', ");
              sql.append(idUnidadAdministradora + ", " + anioProceso + ", " + mesProceso + ", 0, ");
              sql.append(id + ")");

              stInsert.addBatch(sql.toString());

              this.log.error(sql.toString());
            }
            if (bonoExtra != 0.0D)
            {
              id += 1L;
              sql = new StringBuffer();
              sql.append("insert into conceptoresumen (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto,  ");
              sql.append(" tipo, id_unidad_administradora,  anio, mes, numero_nomina, id_concepto_resumen) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(rsConceptoBonoExtra.getLong("id_concepto_tipo_personal") + ", ");
              sql.append(rsFrecuencia.getLong("id_frecuencia_tipo_personal") + ", ");
              sql.append("0,");
              sql.append("'V', ");
              sql.append(idUnidadAdministradora + ", " + anioProceso + ", " + mesProceso + ", 0, ");
              sql.append(id + ")");

              stInsert.addBatch(sql.toString());

              this.log.error(sql.toString());
            }

            if (otrasAlicuotas != 0.0D)
            {
              sql = new StringBuffer();
              sql.append("select id_concepto_alicuota, monto ");
              sql.append(" from otrasalicuotas ");
              sql.append("  where id_trabajador = ? ");

              stOtrasAlicuotas = connection.prepareStatement(
                sql.toString(), 
                1003, 
                1007);
              stOtrasAlicuotas.setLong(1, rsTrabajadores.getLong("id_trabajador"));
              rsOtrasAlicuotas = stOtrasAlicuotas.executeQuery();
              while (rsOtrasAlicuotas.next()) {
                id += 1L;
                sql = new StringBuffer();
                sql.append("insert into conceptoresumen (id_trabajador, id_concepto_tipo_personal, ");
                sql.append(" id_frecuencia_tipo_personal, unidades, monto, ");
                sql.append(" tipo, id_unidad_administradora,  anio, mes, numero_nomina, id_concepto_resumen) values(");
                sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
                sql.append(rsOtrasAlicuotas.getLong("id_concepto_alicuota") + ", ");
                sql.append(rsFrecuencia.getLong("id_frecuencia_tipo_personal") + ", ");
                sql.append("0,");
                sql.append(rsOtrasAlicuotas.getDouble("monto") + ",");
                sql.append("'V', ");
                sql.append(idUnidadAdministradora + ", " + anioProceso + ", " + mesProceso + ", 0, ");
                sql.append(id + ")");

                stInsert.addBatch(sql.toString());
                this.log.error(sql.toString());
              }
            }
          }

        }

        stInsert.executeBatch();
        connection.commit();
      }
    }
    finally {
      if (this.txn != null) this.txn.close();
      if (rsSemana != null) try {
          rsSemana.close();
        } catch (Exception localException) {
        } if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException1) {
        } if (rsConceptoVacaciones != null) try {
          rsConceptoVacaciones.close();
        } catch (Exception localException2) {
        } if (rsConceptoAlicuota != null) try {
          rsConceptoAlicuota.close();
        } catch (Exception localException3) {
        } if (rsConceptoPetrolero != null) try {
          rsConceptoPetrolero.close();
        } catch (Exception localException4) {
        } if (rsConceptoBonoExtra != null) try {
          rsConceptoBonoExtra.close();
        } catch (Exception localException5) {
        } if (rsConceptoBonoVacacional != null) try {
          rsConceptoBonoVacacional.close();
        } catch (Exception localException6) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException7) {
        } if (rsOtrasAlicuotas != null) try {
          rsOtrasAlicuotas.close();
        } catch (Exception localException8) {
        } if (rsFrecuencia != null) try {
          rsFrecuencia.close();
        } catch (Exception localException9) {
        } if (stSemana != null) try {
          stSemana.close();
        } catch (Exception localException10) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException11) {
        } if (stConceptoVacaciones != null) try {
          stConceptoVacaciones.close();
        } catch (Exception localException12) {
        } if (stConceptoAlicuota != null) try {
          stConceptoAlicuota.close();
        } catch (Exception localException13) {
        } if (stConceptoPetrolero != null) try {
          stConceptoPetrolero.close();
        } catch (Exception localException14) {
        } if (stConceptoBonoExtra != null) try {
          stConceptoBonoExtra.close();
        } catch (Exception localException15) {
        } if (stConceptoBonoVacacional != null) try {
          stConceptoBonoVacacional.close();
        } catch (Exception localException16) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException17) {
        } if (stOtrasAlicuotas != null) try {
          stOtrasAlicuotas.close();
        } catch (Exception localException18) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException19) {
        } if (stInsert2 != null) try {
          stInsert2.close();
        } catch (Exception localException20) {
        } if (stFrecuencia != null) try {
          stFrecuencia.close();
        } catch (Exception localException21) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException22) {  }
 
    }
    return id;
  }

  public boolean calcularbonovacliquidacion(long idTipoPersonal, long idTrabajador, String usuario)
    throws Exception
  {
    Calendar fin = Calendar.getInstance();
    Calendar inicio = Calendar.getInstance();

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    ParametroVarios parametroVarios = new ParametroVarios();
    Collection colParametroVarios = this.definicionesBusiness.findParametroVariosByTipoPersonal(idTipoPersonal);
    Iterator iteratorParametroVarios = colParametroVarios.iterator();
    parametroVarios = (ParametroVarios)iteratorParametroVarios.next();

    TipoPersonal tipoPersonal = new TipoPersonal();
    tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    java.util.Date fechaActual = new java.util.Date();
    java.util.Date fechaFin = new java.util.Date();

    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());

    double montoA = 0.0D;
    double montoB = 0.0D;
    double montoC = 0.0D;
    double montoD = 0.0D;
    double totalMontoA = 0.0D;
    double totalMontoB = 0.0D;
    double totalMontoC = 0.0D;
    double totalMontoD = 0.0D;
    double totalUnidades = 0.0D;
    int aniosCumple = 0;
    long diasDividir = 0L;
    double diasBono = 0.0D;
    double otrasAlicuotas = 0.0D;
    double bonoSinAlicuota = 0.0D;
    double totalBono = 0.0D;
    double montoVariable = 0.0D;
    double bonoExtra = 0.0D;
    int mes = 0;
    int anioDesde = 0;
    int mesDesde = 0;
    int diaDesde = 0;
    int anioHasta = 0;
    int mesHasta = 0;
    int diaHasta = 0;
    int totalMeses = 0;
    double montoDiarioVacacionesPendientes = 0.0D;
    double montoVacacionPendiente = 0.0D;
    double factor = 0.0D;

    Calendar calDesde = Calendar.getInstance();
    Calendar calHasta = Calendar.getInstance();

    Connection connection = null;

    ResultSet rsSemana = null;
    PreparedStatement stSemana = null;

    ResultSet rsConceptos = null;
    PreparedStatement stConceptos = null;

    ResultSet rsConceptoVacacionesPendientes = null;
    PreparedStatement stConceptoVacacionesPendientes = null;

    ResultSet rsConceptoVacacionesPendientesF = null;
    PreparedStatement stConceptoVacacionesPendientesF = null;

    ResultSet rsConceptoVacaciones = null;
    PreparedStatement stConceptoVacaciones = null;

    ResultSet rsConceptoAlicuota = null;
    PreparedStatement stConceptoAlicuota = null;

    ResultSet rsConceptoPetrolero = null;
    PreparedStatement stConceptoPetrolero = null;

    ResultSet rsConceptoBonoExtra = null;
    PreparedStatement stConceptoBonoExtra = null;

    ResultSet rsConceptoBonoVacacional = null;
    PreparedStatement stConceptoBonoVacacional = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    ResultSet rsOtrasAlicuotas = null;
    PreparedStatement stOtrasAlicuotas = null;

    ResultSet rsVacacionesPendientes = null;
    PreparedStatement stVacacionesPendientes = null;

    Statement stBorrarOtrasAlicuotas = null;
    Statement stBorrarConceptoLiquidacion = null;
    Statement stBorrarCalculoVacacional = null;
    Statement stInsert = null;
    Statement stInsert2 = null;

    this.log.error("Paso 1");

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      stInsert = connection.createStatement();
      stInsert2 = connection.createStatement();

      this.log.error("Paso 2");

      sql = new StringBuffer();
      sql.append("select id_concepto_tipo_personal, tipo, tope_unidades, ");
      sql.append(" tope_monto, numero_meses, mes_cerrado, alicuota_vacacional, id_concepto_alicuota, mes30 ");
      sql.append(" from conceptovacaciones ");
      sql.append("  where id_tipo_personal = ? ");

      stConceptoVacaciones = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoVacaciones.setLong(1, idTipoPersonal);

      this.log.error("Paso 3");
      if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1601'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoAlicuota = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoAlicuota.setLong(1, idTipoPersonal);
        rsConceptoAlicuota = stConceptoAlicuota.executeQuery();
        rsConceptoAlicuota.next();
      }

      this.log.error("Paso 4");
      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal,ctp.id_frecuencia_tipo_personal  ");
      sql.append(" from conceptotipopersonal ctp, concepto c ");
      sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1811'");
      sql.append(" and ctp.id_concepto = c.id_concepto");

      stConceptoVacacionesPendientes = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoVacacionesPendientes.setLong(1, idTipoPersonal);
      rsConceptoVacacionesPendientes = stConceptoVacacionesPendientes.executeQuery();
      rsConceptoVacacionesPendientes.next();

      this.log.error("Paso 5");

      this.log.error("Paso 4 F");
      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal,ctp.id_frecuencia_tipo_personal  ");
      sql.append(" from conceptotipopersonal ctp, concepto c ");
      sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1812'");
      sql.append(" and ctp.id_concepto = c.id_concepto");

      stConceptoVacacionesPendientesF = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoVacacionesPendientesF.setLong(1, idTipoPersonal);
      rsConceptoVacacionesPendientesF = stConceptoVacacionesPendientesF.executeQuery();
      rsConceptoVacacionesPendientesF.next();

      this.log.error("Paso 5 F");

      if (parametroVarios.getAlicuotaBonoPetrolero().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1701'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoPetrolero = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoPetrolero.setLong(1, idTipoPersonal);
        rsConceptoPetrolero = stConceptoPetrolero.executeQuery();
        rsConceptoPetrolero.next();
      }

      if (parametroVarios.getBonoExtra().equals("S")) {
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades ");
        sql.append(" from conceptotipopersonal ctp, concepto c ");
        sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1502'");
        sql.append(" and ctp.id_concepto = c.id_concepto");

        stConceptoBonoExtra = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConceptoBonoExtra.setLong(1, idTipoPersonal);
        rsConceptoBonoExtra = stConceptoBonoExtra.executeQuery();
        rsConceptoBonoExtra.next();
      }
      this.log.error("Paso 6");

      sql = new StringBuffer();
      sql.append("select id_concepto_tipo_personal, id_frecuencia_tipo_personal  ");
      sql.append(" from conceptotipopersonal, concepto  ");
      sql.append(" where conceptotipopersonal.id_concepto = concepto.id_concepto");
      sql.append("  and id_tipo_personal = ? and concepto.cod_concepto = '1505'");

      stConceptoBonoVacacional = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoBonoVacacional.setLong(1, idTipoPersonal);
      rsConceptoBonoVacacional = stConceptoBonoVacacional.executeQuery();
      rsConceptoBonoVacacional.next();

      if (tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        sql = new StringBuffer();
        sql.append(" select anio, mes from semana where fecha_inicio = ? ");
        stSemana = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stSemana.setDate(1, fechaActualSql);
        rsSemana = stSemana.executeQuery();
        rsSemana.next();
      }

      this.log.error("Paso 7");

      sql = new StringBuffer();
      sql.append("select t.id_trabajador, t.anio_vacaciones, tu.jornada_diaria, t.id_cargo, ");
      sql.append(" tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, p.anios_servicio_apn, ");
      sql.append(" t.fecha_egreso, t.anio_egreso, t.mes_egreso, t.dia_egreso, ");
      sql.append(" t.fecha_vacaciones,  t.mes_vacaciones, t.dia_vacaciones,t.id_personal  ");
      sql.append(" from trabajador t, personal p, turno tu, tipopersonal tp ");
      sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and tp.id_tipo_personal = ? and t.estatus = 'E' and t.id_trabajador = ? ");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, idTipoPersonal);
      stTrabajadores.setLong(2, idTrabajador);

      rsTrabajadores = stTrabajadores.executeQuery();

      this.log.error("Paso 8");
      while (rsTrabajadores.next())
      {
        fechaFin = rsTrabajadores.getDate("fecha_egreso");
        stBorrarOtrasAlicuotas = connection.createStatement();
        sql = new StringBuffer();
        sql.append("delete from otrasalicuotas where id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
        stBorrarOtrasAlicuotas.addBatch(sql.toString());
        stBorrarOtrasAlicuotas.executeBatch();

        stBorrarConceptoLiquidacion = connection.createStatement();
        sql = new StringBuffer();
        sql.append("delete from conceptoliquidacion where id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
        stBorrarConceptoLiquidacion.addBatch(sql.toString());
        stBorrarConceptoLiquidacion.executeBatch();

        this.log.error("Paso 9");

        calDesde.setTime(rsTrabajadores.getDate("fecha_vacaciones"));
        calHasta.setTime(rsTrabajadores.getDate("fecha_egreso"));

        anioHasta = calHasta.get(1);
        mesHasta = calHasta.get(2);
        diaHasta = calHasta.get(5);

        anioDesde = calDesde.get(1);
        mesDesde = calDesde.get(2);
        diaDesde = calDesde.get(5);

        if (mesHasta > mesDesde) {
          totalMeses = mesHasta - mesDesde;
          if (diaHasta < diaDesde) {
            totalMeses--;
          }

        }

        if (mesHasta < mesDesde) {
          totalMeses = 12 - mesDesde + mesHasta;
          if (diaHasta < diaDesde) {
            totalMeses--;
          }
        }

        this.log.error("Paso 10");
        bonoExtra = 0.0D;
        totalUnidades = 0.0D;
        totalMontoA = 0.0D;
        totalMontoB = 0.0D;
        totalMontoC = 0.0D;
        totalMontoD = 0.0D;
        aniosCumple = 0;
        aniosCumple = rsTrabajadores.getInt("anio_egreso") - rsTrabajadores.getInt("anio_vacaciones");
        if ((parametroVarios.getSumoApn().equals("S")) && (rsTrabajadores.getInt("anios_servicio_apn") > 0)) {
          aniosCumple += rsTrabajadores.getInt("anios_servicio_apn");
        }
        this.log.error("aniosCumple: " + aniosCumple);

        VacacionesPorAnio vacacionesPorAnio = this.definicionesNoGenBusiness.findVacacionesPorAnioForAniosServicio(idTipoPersonal, aniosCumple);
        diasBono = 0.0D;
        otrasAlicuotas = 0.0D;
        rsConceptoVacaciones = stConceptoVacaciones.executeQuery();

        this.log.error("Paso 11");
        while (rsConceptoVacaciones.next()) {
          montoA = 0.0D;
          montoB = 0.0D;
          montoC = 0.0D;
          montoD = 0.0D;

          if (rsConceptoVacaciones.getString("tipo").equals("B")) {
            rsConceptos = null;
            stConceptos = null;

            this.log.error("Paso 12");

            sql = new StringBuffer();
            sql.append("select cf.monto, cf.unidades, fp.cod_frecuencia_pago ");
            sql.append(" from conceptofijo cf, frecuenciatipopersonal ftp, frecuenciapago fp ");
            sql.append(" where cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
            sql.append("  and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
            sql.append("  and cf.id_concepto_tipo_personal = ? ");
            sql.append("  and cf.id_trabajador = ? ");
            sql.append("  and cf.estatus = 'A' ");

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));

            rsConceptos = stConceptos.executeQuery();

            while (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              if (rsConceptos.getInt("cod_frecuencia_pago") == 3)
                montoA += rsConceptos.getDouble("monto") * 2.0D;
              else if (rsConceptos.getInt("cod_frecuencia_pago") == 4)
                montoA += rsConceptos.getDouble("monto") / 7.0D * 30.0D;
              else if (rsConceptos.getInt("cod_frecuencia_pago") == 10)
                montoA += rsConceptos.getDouble("monto") * 4.0D;
              else if ((rsConceptos.getInt("cod_frecuencia_pago") == 1) || 
                (rsConceptos.getInt("cod_frecuencia_pago") == 2) || 
                (rsConceptos.getInt("cod_frecuencia_pago") > 10))
              {
                montoA += rsConceptos.getDouble("monto");
              } else if ((rsConceptos.getInt("cod_frecuencia_pago") > 4) && 
                (rsConceptos.getInt("cod_frecuencia_pago") < 10)) {
                if (!rsTrabajadores.getString("formula_integral").equals("4"))
                  montoA += rsConceptos.getDouble("monto");
                else if (rsTrabajadores.getString("formula_integral").equals("4")) {
                  montoA += rsConceptos.getDouble("monto") * 12.0D / 52.0D / 7.0D * 30.0D;
                }
              }

            }

            if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
              montoA = montoA / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
            }
            if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoA)) {
              montoA = rsConceptoVacaciones.getDouble("tope_monto");
            }

            montoA /= 30.0D;
            totalMontoA += montoA;
          }
          else if (rsConceptoVacaciones.getString("tipo").equals("D"))
          {
            rsConceptos = null;
            stConceptos = null;

            sql = new StringBuffer();
            if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
              sql.append("select sum(hq.monto_asigna) as monto, sum(hq.unidades) as unidades ");
              sql.append(" from historicoquincena hq ");
              sql.append(" where hq.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio = ?");
              sql.append(" group by id_trabajador");
            } else {
              sql.append("select sum(hs.monto_asigna) as monto , sum(hs.unidades) as unidades ");
              sql.append(" from historicosemana hs ");
              sql.append(" where hs.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio = ?");
              sql.append(" group by id_trabajador");
            }

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            stConceptos.setInt(3, rsTrabajadores.getInt("anio_egreso"));

            rsConceptos = stConceptos.executeQuery();

            if (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              montoB += rsConceptos.getDouble("monto");
            }

            if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
              montoB = montoB / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
            }
            if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoB)) {
              montoB = rsConceptoVacaciones.getDouble("tope_monto");
            }

            montoB /= parametroVarios.getDiasAnio();
            totalMontoB += montoB;
          }
          else if (rsConceptoVacaciones.getString("tipo").equals("P"))
          {
            diasDividir = 0L;
            int periodoFin;
            int periodoInicio;
            if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S"))
            {
              if (rsConceptoVacaciones.getString("mes_cerrado").equals("S")) {
                fin.setTime(rsTrabajadores.getDate("fecha_egreso"));
                this.log.error("1 - fin " + fin.getTime());
                if (fechaFin.getDate() == 1) {
                  fin.add(5, -1);
                  this.log.error("2 - fin " + fin.getTime());
                } else if (fechaFin.getDate() == 16) {
                  fin.add(5, -16);
                }
              } else {
                fin.setTime(fechaFin);
              }
              this.log.error("3 - fin " + fin.getTime());
              inicio.setTime(fin.getTime());
              this.log.error("0 - INICIO " + inicio.getTime());
              this.log.error("4 - fin " + fin.getTime());
              inicio.add(5, 1);
              this.log.error("5 - fin " + fin.getTime());
              this.log.error("1 - INICIO " + inicio.getTime());
              inicio.add(2, -rsConceptoVacaciones.getInt("numero_meses"));
              this.log.error("inicio " + inicio.getTime());
              this.log.error("fin " + fin.getTime());
              if (rsConceptoVacaciones.getString("mes30").equals("S")) {
                diasDividir = 30 * rsConceptoVacaciones.getInt("numero_meses");
              }
              else {
                long dias = fin.getTimeInMillis() - inicio.getTimeInMillis();
                diasDividir = dias / 86400000L;
              }

              int periodoInicio = (inicio.getTime().getYear() + 1900) * 100 + inicio.getTime().getMonth() + 1;
              int periodoFin = (fin.getTime().getYear() + 1900) * 100 + fin.getTime().getMonth() + 1;
              this.log.error("* periodo inicio " + periodoInicio);
              this.log.error("* periodo fin " + periodoFin);
            } else {
              if (rsConceptoVacaciones.getString("mes_cerrado").equals("S")) {
                int periodoFin = Integer.valueOf(rsSemana.getString("mes")).intValue();
                if (periodoFin == 1) {
                  periodoFin = 12;
                  periodoFin = (rsSemana.getInt("anio") - 1) * 100 + periodoFin;
                } else {
                  periodoFin--;
                  periodoFin = rsSemana.getInt("anio") * 100 + periodoFin;
                }
                int periodoInicio = Integer.valueOf(rsSemana.getString("mes")).intValue() - rsConceptoVacaciones.getInt("numero_meses");
                if (periodoInicio > 0)
                  periodoInicio = rsSemana.getInt("anio") * 100 + periodoInicio;
                else
                  periodoInicio = (rsSemana.getInt("anio") - 1) * 100 + (12 + periodoInicio);
              }
              else {
                periodoFin = Integer.valueOf(rsSemana.getString("mes")).intValue();
                periodoFin = rsSemana.getInt("anio") * 100 + periodoFin;
                periodoInicio = Integer.valueOf(rsSemana.getString("mes")).intValue() - rsConceptoVacaciones.getInt("numero_meses");
                if (periodoInicio > 0)
                  periodoInicio = rsSemana.getInt("anio") * 100 + periodoInicio;
                else {
                  periodoInicio = (rsSemana.getInt("anio") - 1) * 100 + (12 + periodoInicio);
                }
              }
              if (rsConceptoVacaciones.getString("mes30").equals("S")) {
                diasDividir = 30 * rsConceptoVacaciones.getInt("numero_meses");
              }
              else {
                long dias = fin.getTimeInMillis() - inicio.getTimeInMillis();
                diasDividir = dias / 86400000L;
              }
            }

            rsConceptos = null;
            stConceptos = null;

            sql = new StringBuffer();
            if (!tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
              sql.append("select sum(hq.monto_asigna) as monto, sum(hq.unidades) as unidades ");
              sql.append(" from historicoquincena hq ");
              sql.append(" where hq.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio*100+mes >= ? and  anio*100+mes <= ? ");
              sql.append(" group by id_trabajador");
            } else {
              sql.append("select sum(hs.monto_asigna) as monto , sum(hs.unidades) as unidades ");
              sql.append(" from historicosemana hs ");
              sql.append(" where hs.id_concepto_tipo_personal = ?");
              sql.append("  and id_trabajador = ?");
              sql.append(" and anio*100+mes >= ? and  anio*100+mes <= ? ");
              sql.append(" group by id_trabajador");
            }

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));
            stConceptos.setInt(3, periodoInicio);
            stConceptos.setInt(4, periodoFin);

            rsConceptos = stConceptos.executeQuery();
            this.log.error("periodo inicio " + periodoInicio);
            this.log.error("periodo fin " + periodoFin);
            if (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              montoC += rsConceptos.getDouble("monto");
            }
            montoC /= diasDividir;
            totalMontoC += montoC;
          }
          else if (rsConceptoVacaciones.getString("tipo").equals("Y"))
          {
            rsConceptos = null;
            stConceptos = null;

            sql = new StringBuffer();

            sql.append("select sum(cf.monto) as monto, sum(cf.unidades) as unidades ");
            sql.append(" from conceptofijo cf ");
            sql.append(" where cf.id_concepto_tipo_personal = ?");
            sql.append(" and cf.estatus = 'Y'");
            sql.append("  and id_trabajador = ?");
            sql.append(" group by id_trabajador");

            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            stConceptos.setLong(1, rsConceptoVacaciones.getLong("id_concepto_tipo_personal"));
            stConceptos.setLong(2, rsTrabajadores.getLong("id_trabajador"));

            rsConceptos = stConceptos.executeQuery();

            if (rsConceptos.next()) {
              totalUnidades += rsConceptos.getDouble("unidades");
              montoD += rsConceptos.getDouble("monto");
            }

            if ((rsConceptoVacaciones.getDouble("tope_unidades") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_unidades") < totalUnidades)) {
              montoD = montoD / totalUnidades * rsConceptoVacaciones.getDouble("tope_unidades");
            }
            if ((rsConceptoVacaciones.getDouble("tope_monto") != 0.0D) && (rsConceptoVacaciones.getDouble("tope_monto") < montoD)) {
              montoD = rsConceptoVacaciones.getDouble("tope_monto");
            }

            montoD /= parametroVarios.getDiasAnio();

            if (rsConceptoVacaciones.getString("alicuota_vacacional").equals("S")) {
              sql = new StringBuffer();
              montoD = NumberTools.twoDecimal(montoD);
              otrasAlicuotas += montoD * vacacionesPorAnio.getDiasBono();
              sql.append("insert into otrasalicuotas (id_trabajador, id_concepto_alicuota, monto, id_otras_alicuotas) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(rsConceptoVacaciones.getLong("id_concepto_alicuota") + ",");
              sql.append(montoD * vacacionesPorAnio.getDiasBono() + ", ");
              sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.procesoAniversario.OtrasAlicuotas") + ")");
              stInsert2.addBatch(sql.toString());
            }

            totalMontoD += montoD;
          }

        }

        this.log.error("Paso 14");
        stInsert2.executeBatch();

        if (parametroVarios.getAlicuotaBfaBvac().equals("S")) {
          double montoAlicuota = calcularConceptoBeanBusiness.calcular(rsConceptoAlicuota.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), rsConceptoAlicuota.getDouble("unidades"), rsConceptoAlicuota.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoAlicuota.getDouble("valor"), rsConceptoAlicuota.getDouble("tope_minimo"), rsConceptoAlicuota.getDouble("tope_maximo"));
          montoAlicuota /= parametroVarios.getDiasAnio();
          montoAlicuota *= vacacionesPorAnio.getDiasBono();
        } else {
          montoAlicuota = 0.0D;
        }

        if (parametroVarios.getAlicuotaBonoPetrolero().equals("S")) {
          double montoPetrolero = calcularConceptoBeanBusiness.calcular(rsConceptoPetrolero.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), 1.0D, rsConceptoPetrolero.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoPetrolero.getDouble("valor"), rsConceptoPetrolero.getDouble("tope_minimo"), rsConceptoPetrolero.getDouble("tope_maximo"));
          montoPetrolero = (parametroVarios.getConstantePetroleroA() + parametroVarios.getConstantePetroleroB() * vacacionesPorAnio.getDiasBono()) * montoPetrolero;
          montoPetrolero /= parametroVarios.getConstantePetroleroC();
          montoPetrolero /= parametroVarios.getDiasAnio();
          montoPetrolero *= vacacionesPorAnio.getDiasBono();
        } else {
          montoPetrolero = 0.0D;
        }

        this.log.error("Paso 15");
        mes = fechaFin.getMonth() + 1;

        bonoSinAlicuota = (totalMontoA + totalMontoB + totalMontoC + totalMontoD) * vacacionesPorAnio.getDiasBono();
        bonoExtra = (totalMontoA + totalMontoB + totalMontoC + totalMontoD) * vacacionesPorAnio.getDiasExtra();
        totalBono = bonoSinAlicuota + montoAlicuota + montoPetrolero;

        montoDiarioVacacionesPendientes = totalMontoA + totalMontoB + totalMontoC + totalMontoD;

        sql = new StringBuffer();
        montoVariable = bonoSinAlicuota - otrasAlicuotas;

        montoVariable = montoVariable / 12.0D * totalMeses;
        double montoAlicuota = montoAlicuota / 12.0D * totalMeses;
        double montoPetrolero = montoPetrolero / 12.0D * totalMeses;
        bonoExtra = bonoExtra / 12.0D * totalMeses;
        otrasAlicuotas = otrasAlicuotas / 12.0D * totalMeses;
        this.log.error("factor bono-1 : " + factor);
        factor = vacacionesPorAnio.getDiasBono();
        this.log.error("factor bono-2 : " + factor);
        factor /= 12.0D;
        this.log.error("factor bono-3 : " + factor);
        factor *= totalMeses;
        this.log.error("factor bono-4 : " + factor);

        if (montoVariable != 0.0D) {
          sql.append("insert into conceptoliquidacion (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, documento_soporte, id_concepto_liquidacion) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsConceptoBonoVacacional.getLong("id_concepto_tipo_personal") + ", ");
          sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
          sql.append(factor + ",");

          sql.append(NumberTools.twoDecimal(montoVariable) + ",");
          sql.append("'" + fechaActualSql + "',");

          sql.append("'A', ");
          sql.append("'F. : " + rsTrabajadores.getLong("anio_egreso") + "',");
          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion") + ")");

          stInsert.addBatch(sql.toString());

          this.log.error("Paso 16");
        }

        if (parametroVarios.getAlicuotaBfaBvac().equals("S"))
        {
          sql = new StringBuffer();
          sql.append("insert into conceptoliquidacion (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, id_concepto_liquidacion) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsConceptoAlicuota.getLong("id_concepto_tipo_personal") + ", ");
          sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
          sql.append("0,");
          sql.append(NumberTools.twoDecimal(montoAlicuota) + ",");
          sql.append("'" + fechaActualSql + "',");
          sql.append("'A', ");

          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion") + ")");

          stInsert.addBatch(sql.toString());
        }

        this.log.error("Paso 17");
        if ((parametroVarios.getAlicuotaBonoPetrolero().equals("S")) && (montoPetrolero != 0.0D))
        {
          sql = new StringBuffer();
          sql.append("insert into conceptoliquidacion (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, id_concepto_liquidacion) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsConceptoPetrolero.getLong("id_concepto_tipo_personal") + ", ");
          sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
          sql.append("0,");
          sql.append(NumberTools.twoDecimal(montoPetrolero) + ",");
          sql.append("'" + fechaActualSql + "',");
          sql.append("'A', ");

          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion") + ")");

          stInsert.addBatch(sql.toString());
        }

        this.log.error("Paso 18");
        if (bonoExtra != 0.0D)
        {
          sql = new StringBuffer();
          sql.append("insert into conceptoliquidacion (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, id_concepto_liquidacion) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsConceptoBonoExtra.getLong("id_concepto_tipo_personal") + ", ");
          sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
          sql.append("0,");
          sql.append(NumberTools.twoDecimal(bonoExtra) + ",");
          sql.append("'" + fechaActualSql + "',");
          sql.append("'A', ");

          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion") + ")");

          stInsert.addBatch(sql.toString());
        }

        this.log.error("Paso 19");
        if (otrasAlicuotas != 0.0D)
        {
          this.log.error("Paso 20");
          sql = new StringBuffer();
          sql.append("select id_concepto_alicuota, monto ");
          sql.append(" from otrasalicuotas ");
          sql.append("  where id_trabajador = ? ");

          stOtrasAlicuotas = connection.prepareStatement(
            sql.toString(), 
            1003, 
            1007);
          stOtrasAlicuotas.setLong(1, rsTrabajadores.getLong("id_trabajador"));
          rsOtrasAlicuotas = stOtrasAlicuotas.executeQuery();
          while (rsOtrasAlicuotas.next()) {
            sql = new StringBuffer();
            sql.append("insert into conceptoliquidacion (id_trabajador, id_concepto_tipo_personal, ");
            sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
            sql.append(" estatus, id_concepto_liquidacion) values(");
            sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
            sql.append(rsOtrasAlicuotas.getLong("id_concepto_alicuota") + ", ");
            sql.append(rsConceptoBonoVacacional.getLong("id_frecuencia_tipo_personal") + ", ");
            sql.append("0,");
            sql.append(rsOtrasAlicuotas.getDouble("monto") + ",");
            sql.append("'" + fechaActualSql + "',");
            sql.append("'A', ");
            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion") + ")");

            stInsert.addBatch(sql.toString());
          }

        }

        this.log.error("Paso 21");

        sql = new StringBuffer();
        sql.append("select anio , dias_pendientes - dias_disfrute as dias");
        sql.append(" from vacacion v ");
        sql.append("   where v.id_tipo_personal = ? and v.id_personal = ? ");
        sql.append("   and  (v.dias_pendientes - v.dias_disfrute) > 0 ");

        stVacacionesPendientes = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stVacacionesPendientes.setLong(1, idTipoPersonal);
        stVacacionesPendientes.setLong(2, rsTrabajadores.getLong("id_personal"));
        rsVacacionesPendientes = stVacacionesPendientes.executeQuery();

        this.log.error("Paso 22");

        while (rsVacacionesPendientes.next())
        {
          this.log.error("Paso 23");

          montoVacacionPendiente = rsVacacionesPendientes.getDouble("dias") * montoDiarioVacacionesPendientes;
          sql = new StringBuffer();
          sql.append("insert into conceptoliquidacion (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, documento_soporte, id_concepto_liquidacion) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsConceptoVacacionesPendientes.getLong("id_concepto_tipo_personal") + ", ");
          sql.append(rsConceptoVacacionesPendientes.getLong("id_frecuencia_tipo_personal") + ", ");
          sql.append(rsVacacionesPendientes.getDouble("dias") + ",");
          sql.append(montoVacacionPendiente + ",");
          sql.append("'" + fechaActualSql + "',");
          sql.append("'A', ");
          sql.append(rsVacacionesPendientes.getInt("anio") + ",");
          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion") + ")");
          stInsert.addBatch(sql.toString());
        }

        if (totalMeses > 0)
        {
          this.log.error("Paso 28");

          this.log.error("factor-1: " + factor);
          factor = vacacionesPorAnio.getDiasDisfrutar();
          this.log.error("factor-2: " + factor);

          factor /= 12.0D;
          this.log.error("factor-3: " + factor);
          this.log.error("totalmeses: " + totalMeses);
          factor *= totalMeses;
          this.log.error("factor-4: " + factor);

          montoVacacionPendiente = factor * montoDiarioVacacionesPendientes;

          sql = new StringBuffer();
          sql.append("insert into conceptoliquidacion (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, documento_soporte, id_concepto_liquidacion) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");

          this.log.error(sql);

          sql.append(rsConceptoVacacionesPendientesF.getLong("id_concepto_tipo_personal") + ", ");

          this.log.error(sql);

          sql.append(rsConceptoVacacionesPendientesF.getLong("id_frecuencia_tipo_personal") + ", ");

          this.log.error(sql);

          sql.append(factor + ",");
          this.log.error(sql);
          sql.append(montoVacacionPendiente + ",");
          this.log.error(sql);
          sql.append("'" + fechaActualSql + "',");
          this.log.error(sql);

          sql.append("'A', ");
          this.log.error(sql);
          sql.append("'F. : " + rsTrabajadores.getLong("anio_egreso") + "', ");
          this.log.error(sql);
          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoLiquidacion") + ")");
          this.log.error(sql);
          stInsert.addBatch(sql.toString());
        }

      }

      stInsert.executeBatch();

      this.log.error("Paso 29");

      connection.commit();

      this.log.error("Paso 30");
    }
    finally
    {
      if (rsSemana != null) try {
          rsSemana.close();
        } catch (Exception localException) {
        } if (rsConceptos != null) try {
          rsConceptos.close();
        } catch (Exception localException1) {
        } if (rsConceptoVacaciones != null) try {
          rsConceptoVacaciones.close();
        } catch (Exception localException2) {
        } if (rsConceptoAlicuota != null) try {
          rsConceptoAlicuota.close();
        } catch (Exception localException3) {
        } if (rsConceptoVacacionesPendientes != null) try {
          rsConceptoVacacionesPendientes.close();
        } catch (Exception localException4) {
        } if (rsConceptoPetrolero != null) try {
          rsConceptoPetrolero.close();
        } catch (Exception localException5) {
        } if (rsConceptoBonoExtra != null) try {
          rsConceptoBonoExtra.close();
        } catch (Exception localException6) {
        } if (rsConceptoBonoVacacional != null) try {
          rsConceptoBonoVacacional.close();
        } catch (Exception localException7) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException8) {
        } if (rsOtrasAlicuotas != null) try {
          rsOtrasAlicuotas.close();
        } catch (Exception localException9) {
        } if (stSemana != null) try {
          stSemana.close();
        } catch (Exception localException10) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException11) {
        } if (stConceptoVacaciones != null) try {
          stConceptoVacaciones.close();
        } catch (Exception localException12) {
        } if (stConceptoAlicuota != null) try {
          stConceptoAlicuota.close();
        } catch (Exception localException13) {
        } if (stConceptoPetrolero != null) try {
          stConceptoPetrolero.close();
        } catch (Exception localException14) {
        } if (stConceptoBonoExtra != null) try {
          stConceptoBonoExtra.close();
        } catch (Exception localException15) {
        } if (stConceptoBonoVacacional != null) try {
          stConceptoBonoVacacional.close();
        } catch (Exception localException16) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException17) {
        } if (stOtrasAlicuotas != null) try {
          stOtrasAlicuotas.close();
        } catch (Exception localException18) {
        } if (stBorrarOtrasAlicuotas != null) try {
          stBorrarOtrasAlicuotas.close();
        } catch (Exception localException19) {
        } if (stBorrarCalculoVacacional != null) try {
          stBorrarCalculoVacacional.close();
        } catch (Exception localException20) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException21) {
        } if (stInsert2 != null) try {
          stInsert2.close();
        } catch (Exception localException22) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException23) {  }
 
    }
    return true;
  }
}