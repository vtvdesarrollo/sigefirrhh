package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class SeguridadAniversarioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadAniversario(SeguridadAniversario seguridadAniversario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadAniversario seguridadAniversarioNew = 
      (SeguridadAniversario)BeanUtils.cloneBean(
      seguridadAniversario);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadAniversarioNew.getTipoPersonal() != null) {
      seguridadAniversarioNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadAniversarioNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (seguridadAniversarioNew.getConcepto() != null) {
      seguridadAniversarioNew.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        seguridadAniversarioNew.getConcepto().getIdConcepto()));
    }
    pm.makePersistent(seguridadAniversarioNew);
  }

  public void updateSeguridadAniversario(SeguridadAniversario seguridadAniversario) throws Exception
  {
    SeguridadAniversario seguridadAniversarioModify = 
      findSeguridadAniversarioById(seguridadAniversario.getIdSeguridadAniversario());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadAniversario.getTipoPersonal() != null) {
      seguridadAniversario.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadAniversario.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoBeanBusiness conceptoBeanBusiness = new ConceptoBeanBusiness();

    if (seguridadAniversario.getConcepto() != null) {
      seguridadAniversario.setConcepto(
        conceptoBeanBusiness.findConceptoById(
        seguridadAniversario.getConcepto().getIdConcepto()));
    }

    BeanUtils.copyProperties(seguridadAniversarioModify, seguridadAniversario);
  }

  public void deleteSeguridadAniversario(SeguridadAniversario seguridadAniversario) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadAniversario seguridadAniversarioDelete = 
      findSeguridadAniversarioById(seguridadAniversario.getIdSeguridadAniversario());
    pm.deletePersistent(seguridadAniversarioDelete);
  }

  public SeguridadAniversario findSeguridadAniversarioById(long idSeguridadAniversario) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadAniversario == pIdSeguridadAniversario";
    Query query = pm.newQuery(SeguridadAniversario.class, filter);

    query.declareParameters("long pIdSeguridadAniversario");

    parameters.put("pIdSeguridadAniversario", new Long(idSeguridadAniversario));

    Collection colSeguridadAniversario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadAniversario.iterator();
    return (SeguridadAniversario)iterator.next();
  }

  public Collection findSeguridadAniversarioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadAniversarioExtent = pm.getExtent(
      SeguridadAniversario.class, true);
    Query query = pm.newQuery(seguridadAniversarioExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(SeguridadAniversario.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colSeguridadAniversario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadAniversario);

    return colSeguridadAniversario;
  }
}