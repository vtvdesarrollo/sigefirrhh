package sigefirrhh.personal.procesoAniversario;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class CalculoAntiguedad
  implements Serializable, PersistenceCapable
{
  private long idCalculoAntiguedad;
  private double monto;
  private double montoAnterior;
  private int aniosServicio;
  private int mes;
  private Trabajador trabajador;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "idCalculoAntiguedad", "mes", "monto", "montoAnterior", "tipoPersonal", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public long getIdCalculoAntiguedad()
  {
    return jdoGetidCalculoAntiguedad(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public double getMontoAnterior()
  {
    return jdoGetmontoAnterior(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAniosServicio(int i)
  {
    jdoSetaniosServicio(this, i);
  }

  public void setIdCalculoAntiguedad(long l)
  {
    jdoSetidCalculoAntiguedad(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setMontoAnterior(double d)
  {
    jdoSetmontoAnterior(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoAniversario.CalculoAntiguedad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CalculoAntiguedad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CalculoAntiguedad localCalculoAntiguedad = new CalculoAntiguedad();
    localCalculoAntiguedad.jdoFlags = 1;
    localCalculoAntiguedad.jdoStateManager = paramStateManager;
    return localCalculoAntiguedad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CalculoAntiguedad localCalculoAntiguedad = new CalculoAntiguedad();
    localCalculoAntiguedad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCalculoAntiguedad.jdoFlags = 1;
    localCalculoAntiguedad.jdoStateManager = paramStateManager;
    return localCalculoAntiguedad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCalculoAntiguedad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAnterior);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCalculoAntiguedad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAnterior = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CalculoAntiguedad paramCalculoAntiguedad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCalculoAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramCalculoAntiguedad.aniosServicio;
      return;
    case 1:
      if (paramCalculoAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.idCalculoAntiguedad = paramCalculoAntiguedad.idCalculoAntiguedad;
      return;
    case 2:
      if (paramCalculoAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramCalculoAntiguedad.mes;
      return;
    case 3:
      if (paramCalculoAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramCalculoAntiguedad.monto;
      return;
    case 4:
      if (paramCalculoAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.montoAnterior = paramCalculoAntiguedad.montoAnterior;
      return;
    case 5:
      if (paramCalculoAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramCalculoAntiguedad.tipoPersonal;
      return;
    case 6:
      if (paramCalculoAntiguedad == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramCalculoAntiguedad.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CalculoAntiguedad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CalculoAntiguedad localCalculoAntiguedad = (CalculoAntiguedad)paramObject;
    if (localCalculoAntiguedad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCalculoAntiguedad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CalculoAntiguedadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CalculoAntiguedadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CalculoAntiguedadPK))
      throw new IllegalArgumentException("arg1");
    CalculoAntiguedadPK localCalculoAntiguedadPK = (CalculoAntiguedadPK)paramObject;
    localCalculoAntiguedadPK.idCalculoAntiguedad = this.idCalculoAntiguedad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CalculoAntiguedadPK))
      throw new IllegalArgumentException("arg1");
    CalculoAntiguedadPK localCalculoAntiguedadPK = (CalculoAntiguedadPK)paramObject;
    this.idCalculoAntiguedad = localCalculoAntiguedadPK.idCalculoAntiguedad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CalculoAntiguedadPK))
      throw new IllegalArgumentException("arg2");
    CalculoAntiguedadPK localCalculoAntiguedadPK = (CalculoAntiguedadPK)paramObject;
    localCalculoAntiguedadPK.idCalculoAntiguedad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CalculoAntiguedadPK))
      throw new IllegalArgumentException("arg2");
    CalculoAntiguedadPK localCalculoAntiguedadPK = (CalculoAntiguedadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localCalculoAntiguedadPK.idCalculoAntiguedad);
  }

  private static final int jdoGetaniosServicio(CalculoAntiguedad paramCalculoAntiguedad)
  {
    if (paramCalculoAntiguedad.jdoFlags <= 0)
      return paramCalculoAntiguedad.aniosServicio;
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoAntiguedad.aniosServicio;
    if (localStateManager.isLoaded(paramCalculoAntiguedad, jdoInheritedFieldCount + 0))
      return paramCalculoAntiguedad.aniosServicio;
    return localStateManager.getIntField(paramCalculoAntiguedad, jdoInheritedFieldCount + 0, paramCalculoAntiguedad.aniosServicio);
  }

  private static final void jdoSetaniosServicio(CalculoAntiguedad paramCalculoAntiguedad, int paramInt)
  {
    if (paramCalculoAntiguedad.jdoFlags == 0)
    {
      paramCalculoAntiguedad.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoAntiguedad.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramCalculoAntiguedad, jdoInheritedFieldCount + 0, paramCalculoAntiguedad.aniosServicio, paramInt);
  }

  private static final long jdoGetidCalculoAntiguedad(CalculoAntiguedad paramCalculoAntiguedad)
  {
    return paramCalculoAntiguedad.idCalculoAntiguedad;
  }

  private static final void jdoSetidCalculoAntiguedad(CalculoAntiguedad paramCalculoAntiguedad, long paramLong)
  {
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoAntiguedad.idCalculoAntiguedad = paramLong;
      return;
    }
    localStateManager.setLongField(paramCalculoAntiguedad, jdoInheritedFieldCount + 1, paramCalculoAntiguedad.idCalculoAntiguedad, paramLong);
  }

  private static final int jdoGetmes(CalculoAntiguedad paramCalculoAntiguedad)
  {
    if (paramCalculoAntiguedad.jdoFlags <= 0)
      return paramCalculoAntiguedad.mes;
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoAntiguedad.mes;
    if (localStateManager.isLoaded(paramCalculoAntiguedad, jdoInheritedFieldCount + 2))
      return paramCalculoAntiguedad.mes;
    return localStateManager.getIntField(paramCalculoAntiguedad, jdoInheritedFieldCount + 2, paramCalculoAntiguedad.mes);
  }

  private static final void jdoSetmes(CalculoAntiguedad paramCalculoAntiguedad, int paramInt)
  {
    if (paramCalculoAntiguedad.jdoFlags == 0)
    {
      paramCalculoAntiguedad.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoAntiguedad.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramCalculoAntiguedad, jdoInheritedFieldCount + 2, paramCalculoAntiguedad.mes, paramInt);
  }

  private static final double jdoGetmonto(CalculoAntiguedad paramCalculoAntiguedad)
  {
    if (paramCalculoAntiguedad.jdoFlags <= 0)
      return paramCalculoAntiguedad.monto;
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoAntiguedad.monto;
    if (localStateManager.isLoaded(paramCalculoAntiguedad, jdoInheritedFieldCount + 3))
      return paramCalculoAntiguedad.monto;
    return localStateManager.getDoubleField(paramCalculoAntiguedad, jdoInheritedFieldCount + 3, paramCalculoAntiguedad.monto);
  }

  private static final void jdoSetmonto(CalculoAntiguedad paramCalculoAntiguedad, double paramDouble)
  {
    if (paramCalculoAntiguedad.jdoFlags == 0)
    {
      paramCalculoAntiguedad.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoAntiguedad.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoAntiguedad, jdoInheritedFieldCount + 3, paramCalculoAntiguedad.monto, paramDouble);
  }

  private static final double jdoGetmontoAnterior(CalculoAntiguedad paramCalculoAntiguedad)
  {
    if (paramCalculoAntiguedad.jdoFlags <= 0)
      return paramCalculoAntiguedad.montoAnterior;
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoAntiguedad.montoAnterior;
    if (localStateManager.isLoaded(paramCalculoAntiguedad, jdoInheritedFieldCount + 4))
      return paramCalculoAntiguedad.montoAnterior;
    return localStateManager.getDoubleField(paramCalculoAntiguedad, jdoInheritedFieldCount + 4, paramCalculoAntiguedad.montoAnterior);
  }

  private static final void jdoSetmontoAnterior(CalculoAntiguedad paramCalculoAntiguedad, double paramDouble)
  {
    if (paramCalculoAntiguedad.jdoFlags == 0)
    {
      paramCalculoAntiguedad.montoAnterior = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoAntiguedad.montoAnterior = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoAntiguedad, jdoInheritedFieldCount + 4, paramCalculoAntiguedad.montoAnterior, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(CalculoAntiguedad paramCalculoAntiguedad)
  {
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoAntiguedad.tipoPersonal;
    if (localStateManager.isLoaded(paramCalculoAntiguedad, jdoInheritedFieldCount + 5))
      return paramCalculoAntiguedad.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramCalculoAntiguedad, jdoInheritedFieldCount + 5, paramCalculoAntiguedad.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(CalculoAntiguedad paramCalculoAntiguedad, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoAntiguedad.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramCalculoAntiguedad, jdoInheritedFieldCount + 5, paramCalculoAntiguedad.tipoPersonal, paramTipoPersonal);
  }

  private static final Trabajador jdoGettrabajador(CalculoAntiguedad paramCalculoAntiguedad)
  {
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoAntiguedad.trabajador;
    if (localStateManager.isLoaded(paramCalculoAntiguedad, jdoInheritedFieldCount + 6))
      return paramCalculoAntiguedad.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramCalculoAntiguedad, jdoInheritedFieldCount + 6, paramCalculoAntiguedad.trabajador);
  }

  private static final void jdoSettrabajador(CalculoAntiguedad paramCalculoAntiguedad, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramCalculoAntiguedad.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoAntiguedad.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramCalculoAntiguedad, jdoInheritedFieldCount + 6, paramCalculoAntiguedad.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}