package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SeguridadAniversarioNoGenBeanBusiness extends AbstractBeanBusiness
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public SeguridadAniversario findForCalculo(long idTipoPersonal, String codConcepto)
    throws Exception
  {
    HashMap parameters = new HashMap();
    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && concepto.codConcepto == pCodConcepto";
    Query query = pm.newQuery(SeguridadAniversario.class, filter);

    query.declareParameters("long pIdTipoPersonal, String pCodConcepto");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pCodConcepto", codConcepto);

    query.setOrdering("fechaUltimo descending");

    Collection colSeguridadAniversario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadAniversario.iterator();
    return (SeguridadAniversario)iterator.next();
  }
}