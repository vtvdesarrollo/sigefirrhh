package sigefirrhh.personal.procesoAniversario;

import java.io.Serializable;

public class CalculoVacacionalPK
  implements Serializable
{
  public long idCalculoVacacional;

  public CalculoVacacionalPK()
  {
  }

  public CalculoVacacionalPK(long idCalculoVacacional)
  {
    this.idCalculoVacacional = idCalculoVacacional;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CalculoVacacionalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CalculoVacacionalPK thatPK)
  {
    return 
      this.idCalculoVacacional == thatPK.idCalculoVacacional;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCalculoVacacional)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCalculoVacacional);
  }
}