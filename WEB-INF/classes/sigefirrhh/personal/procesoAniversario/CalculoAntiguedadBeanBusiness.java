package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class CalculoAntiguedadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CalculoAntiguedad calculoAntiguedadNew = 
      (CalculoAntiguedad)BeanUtils.cloneBean(
      calculoAntiguedad);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (calculoAntiguedadNew.getTrabajador() != null) {
      calculoAntiguedadNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        calculoAntiguedadNew.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (calculoAntiguedadNew.getTipoPersonal() != null) {
      calculoAntiguedadNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        calculoAntiguedadNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(calculoAntiguedadNew);
  }

  public void updateCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad) throws Exception
  {
    CalculoAntiguedad calculoAntiguedadModify = 
      findCalculoAntiguedadById(calculoAntiguedad.getIdCalculoAntiguedad());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (calculoAntiguedad.getTrabajador() != null) {
      calculoAntiguedad.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        calculoAntiguedad.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (calculoAntiguedad.getTipoPersonal() != null) {
      calculoAntiguedad.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        calculoAntiguedad.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(calculoAntiguedadModify, calculoAntiguedad);
  }

  public void deleteCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CalculoAntiguedad calculoAntiguedadDelete = 
      findCalculoAntiguedadById(calculoAntiguedad.getIdCalculoAntiguedad());
    pm.deletePersistent(calculoAntiguedadDelete);
  }

  public CalculoAntiguedad findCalculoAntiguedadById(long idCalculoAntiguedad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCalculoAntiguedad == pIdCalculoAntiguedad";
    Query query = pm.newQuery(CalculoAntiguedad.class, filter);

    query.declareParameters("long pIdCalculoAntiguedad");

    parameters.put("pIdCalculoAntiguedad", new Long(idCalculoAntiguedad));

    Collection colCalculoAntiguedad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCalculoAntiguedad.iterator();
    return (CalculoAntiguedad)iterator.next();
  }

  public Collection findCalculoAntiguedadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent calculoAntiguedadExtent = pm.getExtent(
      CalculoAntiguedad.class, true);
    Query query = pm.newQuery(calculoAntiguedadExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(CalculoAntiguedad.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colCalculoAntiguedad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCalculoAntiguedad);

    return colCalculoAntiguedad;
  }
}