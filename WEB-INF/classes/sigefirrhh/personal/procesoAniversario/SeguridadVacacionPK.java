package sigefirrhh.personal.procesoAniversario;

import java.io.Serializable;

public class SeguridadVacacionPK
  implements Serializable
{
  public long idSeguridadVacacion;

  public SeguridadVacacionPK()
  {
  }

  public SeguridadVacacionPK(long idSeguridadVacacion)
  {
    this.idSeguridadVacacion = idSeguridadVacacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadVacacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadVacacionPK thatPK)
  {
    return 
      this.idSeguridadVacacion == thatPK.idSeguridadVacacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadVacacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadVacacion);
  }
}