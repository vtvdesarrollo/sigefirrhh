package sigefirrhh.personal.procesoAniversario;

import java.io.Serializable;

public class CalculoAntiguedadPK
  implements Serializable
{
  public long idCalculoAntiguedad;

  public CalculoAntiguedadPK()
  {
  }

  public CalculoAntiguedadPK(long idCalculoAntiguedad)
  {
    this.idCalculoAntiguedad = idCalculoAntiguedad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CalculoAntiguedadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CalculoAntiguedadPK thatPK)
  {
    return 
      this.idCalculoAntiguedad == thatPK.idCalculoAntiguedad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCalculoAntiguedad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCalculoAntiguedad);
  }
}