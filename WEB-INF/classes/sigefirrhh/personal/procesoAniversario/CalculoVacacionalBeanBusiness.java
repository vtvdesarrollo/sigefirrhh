package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class CalculoVacacionalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCalculoVacacional(CalculoVacacional calculoVacacional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CalculoVacacional calculoVacacionalNew = 
      (CalculoVacacional)BeanUtils.cloneBean(
      calculoVacacional);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (calculoVacacionalNew.getTrabajador() != null) {
      calculoVacacionalNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        calculoVacacionalNew.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (calculoVacacionalNew.getTipoPersonal() != null) {
      calculoVacacionalNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        calculoVacacionalNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(calculoVacacionalNew);
  }

  public void updateCalculoVacacional(CalculoVacacional calculoVacacional) throws Exception
  {
    CalculoVacacional calculoVacacionalModify = 
      findCalculoVacacionalById(calculoVacacional.getIdCalculoVacacional());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (calculoVacacional.getTrabajador() != null) {
      calculoVacacional.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        calculoVacacional.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (calculoVacacional.getTipoPersonal() != null) {
      calculoVacacional.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        calculoVacacional.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(calculoVacacionalModify, calculoVacacional);
  }

  public void deleteCalculoVacacional(CalculoVacacional calculoVacacional) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CalculoVacacional calculoVacacionalDelete = 
      findCalculoVacacionalById(calculoVacacional.getIdCalculoVacacional());
    pm.deletePersistent(calculoVacacionalDelete);
  }

  public CalculoVacacional findCalculoVacacionalById(long idCalculoVacacional) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCalculoVacacional == pIdCalculoVacacional";
    Query query = pm.newQuery(CalculoVacacional.class, filter);

    query.declareParameters("long pIdCalculoVacacional");

    parameters.put("pIdCalculoVacacional", new Long(idCalculoVacacional));

    Collection colCalculoVacacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCalculoVacacional.iterator();
    return (CalculoVacacional)iterator.next();
  }

  public Collection findCalculoVacacionalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent calculoVacacionalExtent = pm.getExtent(
      CalculoVacacional.class, true);
    Query query = pm.newQuery(calculoVacacionalExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(CalculoVacacional.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colCalculoVacacional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCalculoVacacional);

    return colCalculoVacacional;
  }
}