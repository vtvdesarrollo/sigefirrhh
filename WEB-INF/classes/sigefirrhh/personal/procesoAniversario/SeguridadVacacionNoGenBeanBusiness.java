package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SeguridadVacacionNoGenBeanBusiness extends AbstractBeanBusiness
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public SeguridadVacacion findForCalculo(long idTipoPersonal)
    throws Exception
  {
    HashMap parameters = new HashMap();
    PersistenceManager pm = PMThread.getPM();
    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal ";
    Query query = pm.newQuery(SeguridadVacacion.class, filter);

    query.declareParameters("long pIdTipoPersonal");

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("fechaUltimo descending");

    Collection colSeguridadVacacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadVacacion.iterator();
    return (SeguridadVacacion)iterator.next();
  }
}