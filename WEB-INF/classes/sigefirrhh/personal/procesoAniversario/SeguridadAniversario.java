package sigefirrhh.personal.procesoAniversario;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.TipoPersonal;

public class SeguridadAniversario
  implements Serializable, PersistenceCapable
{
  private long idSeguridadAniversario;
  private TipoPersonal tipoPersonal;
  private Concepto concepto;
  private Date fechaUltimo;
  private Date fechaProceso;
  private String usuario;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "concepto", "fechaProceso", "fechaUltimo", "idSeguridadAniversario", "tipoPersonal", "usuario" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 24, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetconcepto(this).getDescripcion();
  }

  public Concepto getConcepto()
  {
    return jdoGetconcepto(this);
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public Date getFechaUltimo()
  {
    return jdoGetfechaUltimo(this);
  }

  public long getIdSeguridadAniversario()
  {
    return jdoGetidSeguridadAniversario(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setConcepto(Concepto concepto)
  {
    jdoSetconcepto(this, concepto);
  }

  public void setFechaProceso(Date date)
  {
    jdoSetfechaProceso(this, date);
  }

  public void setFechaUltimo(Date date)
  {
    jdoSetfechaUltimo(this, date);
  }

  public void setIdSeguridadAniversario(long l)
  {
    jdoSetidSeguridadAniversario(this, l);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setUsuario(String string)
  {
    jdoSetusuario(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoAniversario.SeguridadAniversario"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadAniversario());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadAniversario localSeguridadAniversario = new SeguridadAniversario();
    localSeguridadAniversario.jdoFlags = 1;
    localSeguridadAniversario.jdoStateManager = paramStateManager;
    return localSeguridadAniversario;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadAniversario localSeguridadAniversario = new SeguridadAniversario();
    localSeguridadAniversario.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadAniversario.jdoFlags = 1;
    localSeguridadAniversario.jdoStateManager = paramStateManager;
    return localSeguridadAniversario;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaUltimo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadAniversario);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaUltimo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadAniversario = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadAniversario paramSeguridadAniversario, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadAniversario == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramSeguridadAniversario.concepto;
      return;
    case 1:
      if (paramSeguridadAniversario == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadAniversario.fechaProceso;
      return;
    case 2:
      if (paramSeguridadAniversario == null)
        throw new IllegalArgumentException("arg1");
      this.fechaUltimo = paramSeguridadAniversario.fechaUltimo;
      return;
    case 3:
      if (paramSeguridadAniversario == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadAniversario = paramSeguridadAniversario.idSeguridadAniversario;
      return;
    case 4:
      if (paramSeguridadAniversario == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramSeguridadAniversario.tipoPersonal;
      return;
    case 5:
      if (paramSeguridadAniversario == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadAniversario.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadAniversario))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadAniversario localSeguridadAniversario = (SeguridadAniversario)paramObject;
    if (localSeguridadAniversario.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadAniversario, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadAniversarioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadAniversarioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadAniversarioPK))
      throw new IllegalArgumentException("arg1");
    SeguridadAniversarioPK localSeguridadAniversarioPK = (SeguridadAniversarioPK)paramObject;
    localSeguridadAniversarioPK.idSeguridadAniversario = this.idSeguridadAniversario;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadAniversarioPK))
      throw new IllegalArgumentException("arg1");
    SeguridadAniversarioPK localSeguridadAniversarioPK = (SeguridadAniversarioPK)paramObject;
    this.idSeguridadAniversario = localSeguridadAniversarioPK.idSeguridadAniversario;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadAniversarioPK))
      throw new IllegalArgumentException("arg2");
    SeguridadAniversarioPK localSeguridadAniversarioPK = (SeguridadAniversarioPK)paramObject;
    localSeguridadAniversarioPK.idSeguridadAniversario = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadAniversarioPK))
      throw new IllegalArgumentException("arg2");
    SeguridadAniversarioPK localSeguridadAniversarioPK = (SeguridadAniversarioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localSeguridadAniversarioPK.idSeguridadAniversario);
  }

  private static final Concepto jdoGetconcepto(SeguridadAniversario paramSeguridadAniversario)
  {
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAniversario.concepto;
    if (localStateManager.isLoaded(paramSeguridadAniversario, jdoInheritedFieldCount + 0))
      return paramSeguridadAniversario.concepto;
    return (Concepto)localStateManager.getObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 0, paramSeguridadAniversario.concepto);
  }

  private static final void jdoSetconcepto(SeguridadAniversario paramSeguridadAniversario, Concepto paramConcepto)
  {
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAniversario.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 0, paramSeguridadAniversario.concepto, paramConcepto);
  }

  private static final Date jdoGetfechaProceso(SeguridadAniversario paramSeguridadAniversario)
  {
    if (paramSeguridadAniversario.jdoFlags <= 0)
      return paramSeguridadAniversario.fechaProceso;
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAniversario.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadAniversario, jdoInheritedFieldCount + 1))
      return paramSeguridadAniversario.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 1, paramSeguridadAniversario.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadAniversario paramSeguridadAniversario, Date paramDate)
  {
    if (paramSeguridadAniversario.jdoFlags == 0)
    {
      paramSeguridadAniversario.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAniversario.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 1, paramSeguridadAniversario.fechaProceso, paramDate);
  }

  private static final Date jdoGetfechaUltimo(SeguridadAniversario paramSeguridadAniversario)
  {
    if (paramSeguridadAniversario.jdoFlags <= 0)
      return paramSeguridadAniversario.fechaUltimo;
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAniversario.fechaUltimo;
    if (localStateManager.isLoaded(paramSeguridadAniversario, jdoInheritedFieldCount + 2))
      return paramSeguridadAniversario.fechaUltimo;
    return (Date)localStateManager.getObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 2, paramSeguridadAniversario.fechaUltimo);
  }

  private static final void jdoSetfechaUltimo(SeguridadAniversario paramSeguridadAniversario, Date paramDate)
  {
    if (paramSeguridadAniversario.jdoFlags == 0)
    {
      paramSeguridadAniversario.fechaUltimo = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAniversario.fechaUltimo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 2, paramSeguridadAniversario.fechaUltimo, paramDate);
  }

  private static final long jdoGetidSeguridadAniversario(SeguridadAniversario paramSeguridadAniversario)
  {
    return paramSeguridadAniversario.idSeguridadAniversario;
  }

  private static final void jdoSetidSeguridadAniversario(SeguridadAniversario paramSeguridadAniversario, long paramLong)
  {
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAniversario.idSeguridadAniversario = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadAniversario, jdoInheritedFieldCount + 3, paramSeguridadAniversario.idSeguridadAniversario, paramLong);
  }

  private static final TipoPersonal jdoGettipoPersonal(SeguridadAniversario paramSeguridadAniversario)
  {
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAniversario.tipoPersonal;
    if (localStateManager.isLoaded(paramSeguridadAniversario, jdoInheritedFieldCount + 4))
      return paramSeguridadAniversario.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 4, paramSeguridadAniversario.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(SeguridadAniversario paramSeguridadAniversario, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAniversario.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramSeguridadAniversario, jdoInheritedFieldCount + 4, paramSeguridadAniversario.tipoPersonal, paramTipoPersonal);
  }

  private static final String jdoGetusuario(SeguridadAniversario paramSeguridadAniversario)
  {
    if (paramSeguridadAniversario.jdoFlags <= 0)
      return paramSeguridadAniversario.usuario;
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAniversario.usuario;
    if (localStateManager.isLoaded(paramSeguridadAniversario, jdoInheritedFieldCount + 5))
      return paramSeguridadAniversario.usuario;
    return localStateManager.getStringField(paramSeguridadAniversario, jdoInheritedFieldCount + 5, paramSeguridadAniversario.usuario);
  }

  private static final void jdoSetusuario(SeguridadAniversario paramSeguridadAniversario, String paramString)
  {
    if (paramSeguridadAniversario.jdoFlags == 0)
    {
      paramSeguridadAniversario.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadAniversario.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAniversario.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadAniversario, jdoInheritedFieldCount + 5, paramSeguridadAniversario.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}