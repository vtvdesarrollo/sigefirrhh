package sigefirrhh.personal.procesoAniversario;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarDerechoVacacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarDerechoVacacionForm.class.getName());
  private int tipoReporte;
  private String reportName;
  private int reportId;
  private String selectProceso;
  private String selectTipoPersonal;
  private Date inicio;
  private Date fin;
  private Calendar inicioAux;
  private Calendar finAux;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private ProcesoAniversarioNoGenFacade procesoAniversarioNoGenFacade = new ProcesoAniversarioNoGenFacade();
  private boolean show;
  private boolean auxShow;
  private boolean showGenerate;
  private SeguridadVacacion seguridadVacacion = new SeguridadVacacion();

  public GenerarDerechoVacacionForm() {
    this.reportName = "bonvac";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.inicio = null;
    this.fin = null;
    this.inicioAux = Calendar.getInstance();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    setShowGenerate(false);
    refresh();
    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.fin = null;
      this.seguridadVacacion = this.procesoAniversarioNoGenFacade.findSeguridadVacacionForDerecho(this.idTipoPersonal);
      Calendar fecha = Calendar.getInstance();
      fecha.setTime(this.seguridadVacacion.getFechaUltimo());
      fecha.add(5, 1);
      this.inicioAux.set(fecha.getTime().getYear() + 1900, fecha.getTime().getMonth(), fecha.getTime().getDate());

      this.auxShow = true;
      setShowGenerate(true);
    }
    catch (Exception e) {
      this.auxShow = false;
      setShowGenerate(false);
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh() {
    try {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if ((this.fin.getYear() != this.inicioAux.getTime().getYear()) && 
        (this.fin.getMonth() != this.inicioAux.getTime().getMonth())) {
        context.addMessage("error_data", new FacesMessage("La fecha hasta debe pertenecer al mismo año y mes de la fecha de inicio"));
      }

      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

      boolean estado = this.procesoAniversarioNoGenFacade.generarDerechoVacacion(this.idTipoPersonal, this.inicioAux.getTime(), this.fin, this.selectProceso, this.seguridadVacacion.getIdSeguridadVacacion(), this.login.getUsuario());

      setShowGenerate(false);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      if (estado)
        context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
      else
        context.addMessage("error_data", new FacesMessage("Error al calcular vacion por derecho"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string)
  {
    this.selectTipoPersonal = string;
  }

  public Date getFin()
  {
    return this.fin;
  }

  public Date getInicio()
  {
    return this.inicioAux.getTime();
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public String getSelectProceso()
  {
    return this.selectProceso;
  }

  public void setSelectProceso(String string)
  {
    this.selectProceso = string;
  }

  public void setFin(Date date)
  {
    this.fin = date;
  }

  public void setInicio(Date date)
  {
    this.inicio = date;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }
  public boolean isShowGenerate() {
    return this.showGenerate;
  }
  public void setShowGenerate(boolean showGenerate) {
    this.showGenerate = showGenerate;
  }
}