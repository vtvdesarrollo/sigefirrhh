package sigefirrhh.personal.procesoAniversario;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class SeguridadVacacion
  implements Serializable, PersistenceCapable
{
  private long idSeguridadVacacion;
  private TipoPersonal tipoPersonal;
  private Date fechaUltimo;
  private Date fechaProceso;
  private String usuario;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "fechaProceso", "fechaUltimo", "idSeguridadVacacion", "tipoPersonal", "usuario" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre() + "-" + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaUltimo(this));
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public Date getFechaUltimo()
  {
    return jdoGetfechaUltimo(this);
  }

  public long getIdSeguridadVacacion()
  {
    return jdoGetidSeguridadVacacion(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setFechaProceso(Date date)
  {
    jdoSetfechaProceso(this, date);
  }

  public void setFechaUltimo(Date date)
  {
    jdoSetfechaUltimo(this, date);
  }

  public void setIdSeguridadVacacion(long l)
  {
    jdoSetidSeguridadVacacion(this, l);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setUsuario(String string)
  {
    jdoSetusuario(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoAniversario.SeguridadVacacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadVacacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadVacacion localSeguridadVacacion = new SeguridadVacacion();
    localSeguridadVacacion.jdoFlags = 1;
    localSeguridadVacacion.jdoStateManager = paramStateManager;
    return localSeguridadVacacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadVacacion localSeguridadVacacion = new SeguridadVacacion();
    localSeguridadVacacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadVacacion.jdoFlags = 1;
    localSeguridadVacacion.jdoStateManager = paramStateManager;
    return localSeguridadVacacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaUltimo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadVacacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaUltimo = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadVacacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadVacacion paramSeguridadVacacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadVacacion.fechaProceso;
      return;
    case 1:
      if (paramSeguridadVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaUltimo = paramSeguridadVacacion.fechaUltimo;
      return;
    case 2:
      if (paramSeguridadVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadVacacion = paramSeguridadVacacion.idSeguridadVacacion;
      return;
    case 3:
      if (paramSeguridadVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramSeguridadVacacion.tipoPersonal;
      return;
    case 4:
      if (paramSeguridadVacacion == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadVacacion.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadVacacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadVacacion localSeguridadVacacion = (SeguridadVacacion)paramObject;
    if (localSeguridadVacacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadVacacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadVacacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadVacacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadVacacionPK))
      throw new IllegalArgumentException("arg1");
    SeguridadVacacionPK localSeguridadVacacionPK = (SeguridadVacacionPK)paramObject;
    localSeguridadVacacionPK.idSeguridadVacacion = this.idSeguridadVacacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadVacacionPK))
      throw new IllegalArgumentException("arg1");
    SeguridadVacacionPK localSeguridadVacacionPK = (SeguridadVacacionPK)paramObject;
    this.idSeguridadVacacion = localSeguridadVacacionPK.idSeguridadVacacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadVacacionPK))
      throw new IllegalArgumentException("arg2");
    SeguridadVacacionPK localSeguridadVacacionPK = (SeguridadVacacionPK)paramObject;
    localSeguridadVacacionPK.idSeguridadVacacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadVacacionPK))
      throw new IllegalArgumentException("arg2");
    SeguridadVacacionPK localSeguridadVacacionPK = (SeguridadVacacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSeguridadVacacionPK.idSeguridadVacacion);
  }

  private static final Date jdoGetfechaProceso(SeguridadVacacion paramSeguridadVacacion)
  {
    if (paramSeguridadVacacion.jdoFlags <= 0)
      return paramSeguridadVacacion.fechaProceso;
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadVacacion.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadVacacion, jdoInheritedFieldCount + 0))
      return paramSeguridadVacacion.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadVacacion, jdoInheritedFieldCount + 0, paramSeguridadVacacion.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadVacacion paramSeguridadVacacion, Date paramDate)
  {
    if (paramSeguridadVacacion.jdoFlags == 0)
    {
      paramSeguridadVacacion.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadVacacion.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadVacacion, jdoInheritedFieldCount + 0, paramSeguridadVacacion.fechaProceso, paramDate);
  }

  private static final Date jdoGetfechaUltimo(SeguridadVacacion paramSeguridadVacacion)
  {
    if (paramSeguridadVacacion.jdoFlags <= 0)
      return paramSeguridadVacacion.fechaUltimo;
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadVacacion.fechaUltimo;
    if (localStateManager.isLoaded(paramSeguridadVacacion, jdoInheritedFieldCount + 1))
      return paramSeguridadVacacion.fechaUltimo;
    return (Date)localStateManager.getObjectField(paramSeguridadVacacion, jdoInheritedFieldCount + 1, paramSeguridadVacacion.fechaUltimo);
  }

  private static final void jdoSetfechaUltimo(SeguridadVacacion paramSeguridadVacacion, Date paramDate)
  {
    if (paramSeguridadVacacion.jdoFlags == 0)
    {
      paramSeguridadVacacion.fechaUltimo = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadVacacion.fechaUltimo = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadVacacion, jdoInheritedFieldCount + 1, paramSeguridadVacacion.fechaUltimo, paramDate);
  }

  private static final long jdoGetidSeguridadVacacion(SeguridadVacacion paramSeguridadVacacion)
  {
    return paramSeguridadVacacion.idSeguridadVacacion;
  }

  private static final void jdoSetidSeguridadVacacion(SeguridadVacacion paramSeguridadVacacion, long paramLong)
  {
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadVacacion.idSeguridadVacacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadVacacion, jdoInheritedFieldCount + 2, paramSeguridadVacacion.idSeguridadVacacion, paramLong);
  }

  private static final TipoPersonal jdoGettipoPersonal(SeguridadVacacion paramSeguridadVacacion)
  {
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadVacacion.tipoPersonal;
    if (localStateManager.isLoaded(paramSeguridadVacacion, jdoInheritedFieldCount + 3))
      return paramSeguridadVacacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramSeguridadVacacion, jdoInheritedFieldCount + 3, paramSeguridadVacacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(SeguridadVacacion paramSeguridadVacacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadVacacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramSeguridadVacacion, jdoInheritedFieldCount + 3, paramSeguridadVacacion.tipoPersonal, paramTipoPersonal);
  }

  private static final String jdoGetusuario(SeguridadVacacion paramSeguridadVacacion)
  {
    if (paramSeguridadVacacion.jdoFlags <= 0)
      return paramSeguridadVacacion.usuario;
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadVacacion.usuario;
    if (localStateManager.isLoaded(paramSeguridadVacacion, jdoInheritedFieldCount + 4))
      return paramSeguridadVacacion.usuario;
    return localStateManager.getStringField(paramSeguridadVacacion, jdoInheritedFieldCount + 4, paramSeguridadVacacion.usuario);
  }

  private static final void jdoSetusuario(SeguridadVacacion paramSeguridadVacacion, String paramString)
  {
    if (paramSeguridadVacacion.jdoFlags == 0)
    {
      paramSeguridadVacacion.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadVacacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadVacacion.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadVacacion, jdoInheritedFieldCount + 4, paramSeguridadVacacion.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}