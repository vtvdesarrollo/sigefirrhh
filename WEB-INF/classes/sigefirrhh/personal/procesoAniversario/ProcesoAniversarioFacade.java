package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class ProcesoAniversarioFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ProcesoAniversarioBusiness procesoAniversarioBusiness = new ProcesoAniversarioBusiness();

  public void addCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoAniversarioBusiness.addCalculoAntiguedad(calculoAntiguedad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.updateCalculoAntiguedad(calculoAntiguedad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.deleteCalculoAntiguedad(calculoAntiguedad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CalculoAntiguedad findCalculoAntiguedadById(long calculoAntiguedadId) throws Exception
  {
    try { this.txn.open();
      CalculoAntiguedad calculoAntiguedad = 
        this.procesoAniversarioBusiness.findCalculoAntiguedadById(calculoAntiguedadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(calculoAntiguedad);
      return calculoAntiguedad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCalculoAntiguedad() throws Exception
  {
    try { this.txn.open();
      return this.procesoAniversarioBusiness.findAllCalculoAntiguedad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCalculoAntiguedadByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoAniversarioBusiness.findCalculoAntiguedadByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCalculoVacacional(CalculoVacacional calculoVacacional)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoAniversarioBusiness.addCalculoVacacional(calculoVacacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCalculoVacacional(CalculoVacacional calculoVacacional) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.updateCalculoVacacional(calculoVacacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCalculoVacacional(CalculoVacacional calculoVacacional) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.deleteCalculoVacacional(calculoVacacional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CalculoVacacional findCalculoVacacionalById(long calculoVacacionalId) throws Exception
  {
    try { this.txn.open();
      CalculoVacacional calculoVacacional = 
        this.procesoAniversarioBusiness.findCalculoVacacionalById(calculoVacacionalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(calculoVacacional);
      return calculoVacacional;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCalculoVacacional() throws Exception
  {
    try { this.txn.open();
      return this.procesoAniversarioBusiness.findAllCalculoVacacional();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCalculoVacacionalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoAniversarioBusiness.findCalculoVacacionalByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoAniversarioBusiness.addOtrasAlicuotas(otrasAlicuotas);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.updateOtrasAlicuotas(otrasAlicuotas);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.deleteOtrasAlicuotas(otrasAlicuotas);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public OtrasAlicuotas findOtrasAlicuotasById(long otrasAlicuotasId) throws Exception
  {
    try { this.txn.open();
      OtrasAlicuotas otrasAlicuotas = 
        this.procesoAniversarioBusiness.findOtrasAlicuotasById(otrasAlicuotasId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(otrasAlicuotas);
      return otrasAlicuotas;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllOtrasAlicuotas() throws Exception
  {
    try { this.txn.open();
      return this.procesoAniversarioBusiness.findAllOtrasAlicuotas();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadAniversario(SeguridadAniversario seguridadAniversario)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoAniversarioBusiness.addSeguridadAniversario(seguridadAniversario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadAniversario(SeguridadAniversario seguridadAniversario) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.updateSeguridadAniversario(seguridadAniversario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadAniversario(SeguridadAniversario seguridadAniversario) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.deleteSeguridadAniversario(seguridadAniversario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadAniversario findSeguridadAniversarioById(long seguridadAniversarioId) throws Exception
  {
    try { this.txn.open();
      SeguridadAniversario seguridadAniversario = 
        this.procesoAniversarioBusiness.findSeguridadAniversarioById(seguridadAniversarioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadAniversario);
      return seguridadAniversario;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadAniversario() throws Exception
  {
    try { this.txn.open();
      return this.procesoAniversarioBusiness.findAllSeguridadAniversario();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadAniversarioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoAniversarioBusiness.findSeguridadAniversarioByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.addSeguridadVacacion(seguridadVacacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.updateSeguridadVacacion(seguridadVacacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception
  {
    try { this.txn.open();
      this.procesoAniversarioBusiness.deleteSeguridadVacacion(seguridadVacacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadVacacion findSeguridadVacacionById(long seguridadVacacionId) throws Exception
  {
    try { this.txn.open();
      SeguridadVacacion seguridadVacacion = 
        this.procesoAniversarioBusiness.findSeguridadVacacionById(seguridadVacacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadVacacion);
      return seguridadVacacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadVacacion() throws Exception
  {
    try { this.txn.open();
      return this.procesoAniversarioBusiness.findAllSeguridadVacacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadVacacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoAniversarioBusiness.findSeguridadVacacionByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}