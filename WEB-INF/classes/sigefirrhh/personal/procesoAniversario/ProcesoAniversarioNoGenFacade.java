package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;

public class ProcesoAniversarioNoGenFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ProcesoAniversarioNoGenBusiness procesoAniversarioNoGenBusiness = new ProcesoAniversarioNoGenBusiness();

  public SeguridadAniversario findSeguridadAniversarioForCalculo(long idTipoPersonal, String codConcepto)
    throws Exception
  {
    try
    {
      this.txn.open();
      SeguridadAniversario seguridadAniversario = 
        this.procesoAniversarioNoGenBusiness.findSeguridadAniversarioForCalculo(idTipoPersonal, codConcepto);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadAniversario);
      return seguridadAniversario;
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public SeguridadVacacion findSeguridadVacacionForDerecho(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      SeguridadVacacion seguridadVacacion = 
        this.procesoAniversarioNoGenBusiness.findSeguridadVacacionForDerecho(idTipoPersonal);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadVacacion);
      return seguridadVacacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void calcularPrimaAntiguedad(long idTipoPersonal, Date fechaInicio, Date fechaFin, String proceso, long idSeguridadAniversario, String usuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.procesoAniversarioNoGenBusiness.calcularPrimaAntiguedad(idTipoPersonal, fechaInicio, fechaFin, proceso, idSeguridadAniversario, usuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean calcularBonoVacacional(long idTipoPersonal, Date fechaInicio, Date fechaFin, String proceso, long idSeguridadAniversario, String usuario) throws Exception
  {
    try {
      this.txn.open();
      return this.procesoAniversarioNoGenBusiness.calcularBonoVacacional(idTipoPersonal, fechaInicio, fechaFin, proceso, idSeguridadAniversario, usuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean calcularBonoVacacionalLiquidacion(long idTipoPersonal, long idTrabajador, String usuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoAniversarioNoGenBusiness.calcularBonoVacacionalLiquidacion(idTipoPersonal, idTrabajador, usuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean generarDerechoVacacion(long idTipoPersonal, Date fechaInicio, Date fechaFin, String proceso, long idSeguridadAniversario, String usuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.procesoAniversarioNoGenBusiness.generarderechovacacion(idTipoPersonal, fechaInicio, fechaFin, proceso, idSeguridadAniversario, usuario);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}