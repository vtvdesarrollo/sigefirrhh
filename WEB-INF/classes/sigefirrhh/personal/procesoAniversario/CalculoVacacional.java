package sigefirrhh.personal.procesoAniversario;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class CalculoVacacional
  implements Serializable, PersistenceCapable
{
  private long idCalculoVacacional;
  private double totalBono;
  private double montoAlicuota;
  private double bonoSinAlicuota;
  private double dias;
  private int mes;
  private double baseFijo;
  private double basePromedio;
  private double baseDevengado;
  private double baseAlicuota;
  private int aniosServicio;
  private String momentoPago;
  private Trabajador trabajador;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "baseAlicuota", "baseDevengado", "baseFijo", "basePromedio", "bonoSinAlicuota", "dias", "idCalculoVacacional", "mes", "momentoPago", "montoAlicuota", "tipoPersonal", "totalBono", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public long getIdCalculoVacacional()
  {
    return jdoGetidCalculoVacacional(this);
  }

  public String getMomentoPago()
  {
    return jdoGetmomentoPago(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAniosServicio(int i)
  {
    jdoSetaniosServicio(this, i);
  }

  public void setIdCalculoVacacional(long l)
  {
    jdoSetidCalculoVacacional(this, l);
  }

  public void setMomentoPago(String string)
  {
    jdoSetmomentoPago(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public double getBaseDevengado()
  {
    return jdoGetbaseDevengado(this);
  }

  public double getBaseFijo()
  {
    return jdoGetbaseFijo(this);
  }

  public double getBasePromedio()
  {
    return jdoGetbasePromedio(this);
  }

  public double getMontoAlicuota()
  {
    return jdoGetmontoAlicuota(this);
  }

  public void setBaseDevengado(double d)
  {
    jdoSetbaseDevengado(this, d);
  }

  public void setBaseFijo(double d)
  {
    jdoSetbaseFijo(this, d);
  }

  public void setBasePromedio(double d)
  {
    jdoSetbasePromedio(this, d);
  }

  public void setMontoAlicuota(double d)
  {
    jdoSetmontoAlicuota(this, d);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public double getDias()
  {
    return jdoGetdias(this);
  }

  public void setDias(double d)
  {
    jdoSetdias(this, d);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public double getBaseAlicuota()
  {
    return jdoGetbaseAlicuota(this);
  }

  public double getBonoSinAlicuota()
  {
    return jdoGetbonoSinAlicuota(this);
  }

  public double getTotalBono()
  {
    return jdoGettotalBono(this);
  }

  public void setBaseAlicuota(double d)
  {
    jdoSetbaseAlicuota(this, d);
  }

  public void setBonoSinAlicuota(double d)
  {
    jdoSetbonoSinAlicuota(this, d);
  }

  public void setTotalBono(double d)
  {
    jdoSettotalBono(this, d);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.procesoAniversario.CalculoVacacional"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CalculoVacacional());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CalculoVacacional localCalculoVacacional = new CalculoVacacional();
    localCalculoVacacional.jdoFlags = 1;
    localCalculoVacacional.jdoStateManager = paramStateManager;
    return localCalculoVacacional;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CalculoVacacional localCalculoVacacional = new CalculoVacacional();
    localCalculoVacacional.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCalculoVacacional.jdoFlags = 1;
    localCalculoVacacional.jdoStateManager = paramStateManager;
    return localCalculoVacacional;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseAlicuota);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseDevengado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseFijo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.basePromedio);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.bonoSinAlicuota);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.dias);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCalculoVacacional);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.momentoPago);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAlicuota);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalBono);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseAlicuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseDevengado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseFijo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.basePromedio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bonoSinAlicuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dias = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCalculoVacacional = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.momentoPago = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAlicuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalBono = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CalculoVacacional paramCalculoVacacional, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramCalculoVacacional.aniosServicio;
      return;
    case 1:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.baseAlicuota = paramCalculoVacacional.baseAlicuota;
      return;
    case 2:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.baseDevengado = paramCalculoVacacional.baseDevengado;
      return;
    case 3:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.baseFijo = paramCalculoVacacional.baseFijo;
      return;
    case 4:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.basePromedio = paramCalculoVacacional.basePromedio;
      return;
    case 5:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.bonoSinAlicuota = paramCalculoVacacional.bonoSinAlicuota;
      return;
    case 6:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.dias = paramCalculoVacacional.dias;
      return;
    case 7:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.idCalculoVacacional = paramCalculoVacacional.idCalculoVacacional;
      return;
    case 8:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramCalculoVacacional.mes;
      return;
    case 9:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.momentoPago = paramCalculoVacacional.momentoPago;
      return;
    case 10:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.montoAlicuota = paramCalculoVacacional.montoAlicuota;
      return;
    case 11:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramCalculoVacacional.tipoPersonal;
      return;
    case 12:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.totalBono = paramCalculoVacacional.totalBono;
      return;
    case 13:
      if (paramCalculoVacacional == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramCalculoVacacional.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CalculoVacacional))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CalculoVacacional localCalculoVacacional = (CalculoVacacional)paramObject;
    if (localCalculoVacacional.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCalculoVacacional, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CalculoVacacionalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CalculoVacacionalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CalculoVacacionalPK))
      throw new IllegalArgumentException("arg1");
    CalculoVacacionalPK localCalculoVacacionalPK = (CalculoVacacionalPK)paramObject;
    localCalculoVacacionalPK.idCalculoVacacional = this.idCalculoVacacional;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CalculoVacacionalPK))
      throw new IllegalArgumentException("arg1");
    CalculoVacacionalPK localCalculoVacacionalPK = (CalculoVacacionalPK)paramObject;
    this.idCalculoVacacional = localCalculoVacacionalPK.idCalculoVacacional;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CalculoVacacionalPK))
      throw new IllegalArgumentException("arg2");
    CalculoVacacionalPK localCalculoVacacionalPK = (CalculoVacacionalPK)paramObject;
    localCalculoVacacionalPK.idCalculoVacacional = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CalculoVacacionalPK))
      throw new IllegalArgumentException("arg2");
    CalculoVacacionalPK localCalculoVacacionalPK = (CalculoVacacionalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localCalculoVacacionalPK.idCalculoVacacional);
  }

  private static final int jdoGetaniosServicio(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.aniosServicio;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.aniosServicio;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 0))
      return paramCalculoVacacional.aniosServicio;
    return localStateManager.getIntField(paramCalculoVacacional, jdoInheritedFieldCount + 0, paramCalculoVacacional.aniosServicio);
  }

  private static final void jdoSetaniosServicio(CalculoVacacional paramCalculoVacacional, int paramInt)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramCalculoVacacional, jdoInheritedFieldCount + 0, paramCalculoVacacional.aniosServicio, paramInt);
  }

  private static final double jdoGetbaseAlicuota(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.baseAlicuota;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.baseAlicuota;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 1))
      return paramCalculoVacacional.baseAlicuota;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 1, paramCalculoVacacional.baseAlicuota);
  }

  private static final void jdoSetbaseAlicuota(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.baseAlicuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.baseAlicuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 1, paramCalculoVacacional.baseAlicuota, paramDouble);
  }

  private static final double jdoGetbaseDevengado(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.baseDevengado;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.baseDevengado;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 2))
      return paramCalculoVacacional.baseDevengado;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 2, paramCalculoVacacional.baseDevengado);
  }

  private static final void jdoSetbaseDevengado(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.baseDevengado = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.baseDevengado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 2, paramCalculoVacacional.baseDevengado, paramDouble);
  }

  private static final double jdoGetbaseFijo(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.baseFijo;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.baseFijo;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 3))
      return paramCalculoVacacional.baseFijo;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 3, paramCalculoVacacional.baseFijo);
  }

  private static final void jdoSetbaseFijo(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.baseFijo = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.baseFijo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 3, paramCalculoVacacional.baseFijo, paramDouble);
  }

  private static final double jdoGetbasePromedio(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.basePromedio;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.basePromedio;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 4))
      return paramCalculoVacacional.basePromedio;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 4, paramCalculoVacacional.basePromedio);
  }

  private static final void jdoSetbasePromedio(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.basePromedio = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.basePromedio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 4, paramCalculoVacacional.basePromedio, paramDouble);
  }

  private static final double jdoGetbonoSinAlicuota(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.bonoSinAlicuota;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.bonoSinAlicuota;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 5))
      return paramCalculoVacacional.bonoSinAlicuota;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 5, paramCalculoVacacional.bonoSinAlicuota);
  }

  private static final void jdoSetbonoSinAlicuota(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.bonoSinAlicuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.bonoSinAlicuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 5, paramCalculoVacacional.bonoSinAlicuota, paramDouble);
  }

  private static final double jdoGetdias(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.dias;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.dias;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 6))
      return paramCalculoVacacional.dias;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 6, paramCalculoVacacional.dias);
  }

  private static final void jdoSetdias(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.dias = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.dias = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 6, paramCalculoVacacional.dias, paramDouble);
  }

  private static final long jdoGetidCalculoVacacional(CalculoVacacional paramCalculoVacacional)
  {
    return paramCalculoVacacional.idCalculoVacacional;
  }

  private static final void jdoSetidCalculoVacacional(CalculoVacacional paramCalculoVacacional, long paramLong)
  {
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.idCalculoVacacional = paramLong;
      return;
    }
    localStateManager.setLongField(paramCalculoVacacional, jdoInheritedFieldCount + 7, paramCalculoVacacional.idCalculoVacacional, paramLong);
  }

  private static final int jdoGetmes(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.mes;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.mes;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 8))
      return paramCalculoVacacional.mes;
    return localStateManager.getIntField(paramCalculoVacacional, jdoInheritedFieldCount + 8, paramCalculoVacacional.mes);
  }

  private static final void jdoSetmes(CalculoVacacional paramCalculoVacacional, int paramInt)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramCalculoVacacional, jdoInheritedFieldCount + 8, paramCalculoVacacional.mes, paramInt);
  }

  private static final String jdoGetmomentoPago(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.momentoPago;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.momentoPago;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 9))
      return paramCalculoVacacional.momentoPago;
    return localStateManager.getStringField(paramCalculoVacacional, jdoInheritedFieldCount + 9, paramCalculoVacacional.momentoPago);
  }

  private static final void jdoSetmomentoPago(CalculoVacacional paramCalculoVacacional, String paramString)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.momentoPago = paramString;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.momentoPago = paramString;
      return;
    }
    localStateManager.setStringField(paramCalculoVacacional, jdoInheritedFieldCount + 9, paramCalculoVacacional.momentoPago, paramString);
  }

  private static final double jdoGetmontoAlicuota(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.montoAlicuota;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.montoAlicuota;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 10))
      return paramCalculoVacacional.montoAlicuota;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 10, paramCalculoVacacional.montoAlicuota);
  }

  private static final void jdoSetmontoAlicuota(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.montoAlicuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.montoAlicuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 10, paramCalculoVacacional.montoAlicuota, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(CalculoVacacional paramCalculoVacacional)
  {
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.tipoPersonal;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 11))
      return paramCalculoVacacional.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramCalculoVacacional, jdoInheritedFieldCount + 11, paramCalculoVacacional.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(CalculoVacacional paramCalculoVacacional, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramCalculoVacacional, jdoInheritedFieldCount + 11, paramCalculoVacacional.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGettotalBono(CalculoVacacional paramCalculoVacacional)
  {
    if (paramCalculoVacacional.jdoFlags <= 0)
      return paramCalculoVacacional.totalBono;
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.totalBono;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 12))
      return paramCalculoVacacional.totalBono;
    return localStateManager.getDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 12, paramCalculoVacacional.totalBono);
  }

  private static final void jdoSettotalBono(CalculoVacacional paramCalculoVacacional, double paramDouble)
  {
    if (paramCalculoVacacional.jdoFlags == 0)
    {
      paramCalculoVacacional.totalBono = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.totalBono = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoVacacional, jdoInheritedFieldCount + 12, paramCalculoVacacional.totalBono, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(CalculoVacacional paramCalculoVacacional)
  {
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoVacacional.trabajador;
    if (localStateManager.isLoaded(paramCalculoVacacional, jdoInheritedFieldCount + 13))
      return paramCalculoVacacional.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramCalculoVacacional, jdoInheritedFieldCount + 13, paramCalculoVacacional.trabajador);
  }

  private static final void jdoSettrabajador(CalculoVacacional paramCalculoVacacional, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramCalculoVacacional.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoVacacional.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramCalculoVacacional, jdoInheritedFieldCount + 13, paramCalculoVacacional.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}