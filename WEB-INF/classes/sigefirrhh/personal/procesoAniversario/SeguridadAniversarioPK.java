package sigefirrhh.personal.procesoAniversario;

import java.io.Serializable;

public class SeguridadAniversarioPK
  implements Serializable
{
  public long idSeguridadAniversario;

  public SeguridadAniversarioPK()
  {
  }

  public SeguridadAniversarioPK(long idSeguridadAniversario)
  {
    this.idSeguridadAniversario = idSeguridadAniversario;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadAniversarioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadAniversarioPK thatPK)
  {
    return 
      this.idSeguridadAniversario == thatPK.idSeguridadAniversario;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadAniversario)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadAniversario);
  }
}