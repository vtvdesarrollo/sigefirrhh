package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class SeguridadVacacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadVacacion(SeguridadVacacion seguridadVacacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadVacacion seguridadVacacionNew = 
      (SeguridadVacacion)BeanUtils.cloneBean(
      seguridadVacacion);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadVacacionNew.getTipoPersonal() != null) {
      seguridadVacacionNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadVacacionNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(seguridadVacacionNew);
  }

  public void updateSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception
  {
    SeguridadVacacion seguridadVacacionModify = 
      findSeguridadVacacionById(seguridadVacacion.getIdSeguridadVacacion());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadVacacion.getTipoPersonal() != null) {
      seguridadVacacion.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadVacacion.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(seguridadVacacionModify, seguridadVacacion);
  }

  public void deleteSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadVacacion seguridadVacacionDelete = 
      findSeguridadVacacionById(seguridadVacacion.getIdSeguridadVacacion());
    pm.deletePersistent(seguridadVacacionDelete);
  }

  public SeguridadVacacion findSeguridadVacacionById(long idSeguridadVacacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadVacacion == pIdSeguridadVacacion";
    Query query = pm.newQuery(SeguridadVacacion.class, filter);

    query.declareParameters("long pIdSeguridadVacacion");

    parameters.put("pIdSeguridadVacacion", new Long(idSeguridadVacacion));

    Collection colSeguridadVacacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadVacacion.iterator();
    return (SeguridadVacacion)iterator.next();
  }

  public Collection findSeguridadVacacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadVacacionExtent = pm.getExtent(
      SeguridadVacacion.class, true);
    Query query = pm.newQuery(seguridadVacacionExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(SeguridadVacacion.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colSeguridadVacacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadVacacion);

    return colSeguridadVacacion;
  }
}