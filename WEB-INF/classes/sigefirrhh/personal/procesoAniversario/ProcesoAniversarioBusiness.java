package sigefirrhh.personal.procesoAniversario;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class ProcesoAniversarioBusiness extends AbstractBusiness
  implements Serializable
{
  private CalculoAntiguedadBeanBusiness calculoAntiguedadBeanBusiness = new CalculoAntiguedadBeanBusiness();

  private CalculoVacacionalBeanBusiness calculoVacacionalBeanBusiness = new CalculoVacacionalBeanBusiness();

  private OtrasAlicuotasBeanBusiness otrasAlicuotasBeanBusiness = new OtrasAlicuotasBeanBusiness();

  private SeguridadAniversarioBeanBusiness seguridadAniversarioBeanBusiness = new SeguridadAniversarioBeanBusiness();

  private SeguridadVacacionBeanBusiness seguridadVacacionBeanBusiness = new SeguridadVacacionBeanBusiness();

  public void addCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad)
    throws Exception
  {
    this.calculoAntiguedadBeanBusiness.addCalculoAntiguedad(calculoAntiguedad);
  }

  public void updateCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad) throws Exception {
    this.calculoAntiguedadBeanBusiness.updateCalculoAntiguedad(calculoAntiguedad);
  }

  public void deleteCalculoAntiguedad(CalculoAntiguedad calculoAntiguedad) throws Exception {
    this.calculoAntiguedadBeanBusiness.deleteCalculoAntiguedad(calculoAntiguedad);
  }

  public CalculoAntiguedad findCalculoAntiguedadById(long calculoAntiguedadId) throws Exception {
    return this.calculoAntiguedadBeanBusiness.findCalculoAntiguedadById(calculoAntiguedadId);
  }

  public Collection findAllCalculoAntiguedad() throws Exception {
    return this.calculoAntiguedadBeanBusiness.findCalculoAntiguedadAll();
  }

  public Collection findCalculoAntiguedadByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.calculoAntiguedadBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addCalculoVacacional(CalculoVacacional calculoVacacional)
    throws Exception
  {
    this.calculoVacacionalBeanBusiness.addCalculoVacacional(calculoVacacional);
  }

  public void updateCalculoVacacional(CalculoVacacional calculoVacacional) throws Exception {
    this.calculoVacacionalBeanBusiness.updateCalculoVacacional(calculoVacacional);
  }

  public void deleteCalculoVacacional(CalculoVacacional calculoVacacional) throws Exception {
    this.calculoVacacionalBeanBusiness.deleteCalculoVacacional(calculoVacacional);
  }

  public CalculoVacacional findCalculoVacacionalById(long calculoVacacionalId) throws Exception {
    return this.calculoVacacionalBeanBusiness.findCalculoVacacionalById(calculoVacacionalId);
  }

  public Collection findAllCalculoVacacional() throws Exception {
    return this.calculoVacacionalBeanBusiness.findCalculoVacacionalAll();
  }

  public Collection findCalculoVacacionalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.calculoVacacionalBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas)
    throws Exception
  {
    this.otrasAlicuotasBeanBusiness.addOtrasAlicuotas(otrasAlicuotas);
  }

  public void updateOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas) throws Exception {
    this.otrasAlicuotasBeanBusiness.updateOtrasAlicuotas(otrasAlicuotas);
  }

  public void deleteOtrasAlicuotas(OtrasAlicuotas otrasAlicuotas) throws Exception {
    this.otrasAlicuotasBeanBusiness.deleteOtrasAlicuotas(otrasAlicuotas);
  }

  public OtrasAlicuotas findOtrasAlicuotasById(long otrasAlicuotasId) throws Exception {
    return this.otrasAlicuotasBeanBusiness.findOtrasAlicuotasById(otrasAlicuotasId);
  }

  public Collection findAllOtrasAlicuotas() throws Exception {
    return this.otrasAlicuotasBeanBusiness.findOtrasAlicuotasAll();
  }

  public void addSeguridadAniversario(SeguridadAniversario seguridadAniversario)
    throws Exception
  {
    this.seguridadAniversarioBeanBusiness.addSeguridadAniversario(seguridadAniversario);
  }

  public void updateSeguridadAniversario(SeguridadAniversario seguridadAniversario) throws Exception {
    this.seguridadAniversarioBeanBusiness.updateSeguridadAniversario(seguridadAniversario);
  }

  public void deleteSeguridadAniversario(SeguridadAniversario seguridadAniversario) throws Exception {
    this.seguridadAniversarioBeanBusiness.deleteSeguridadAniversario(seguridadAniversario);
  }

  public SeguridadAniversario findSeguridadAniversarioById(long seguridadAniversarioId) throws Exception {
    return this.seguridadAniversarioBeanBusiness.findSeguridadAniversarioById(seguridadAniversarioId);
  }

  public Collection findAllSeguridadAniversario() throws Exception {
    return this.seguridadAniversarioBeanBusiness.findSeguridadAniversarioAll();
  }

  public Collection findSeguridadAniversarioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.seguridadAniversarioBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception
  {
    this.seguridadVacacionBeanBusiness.addSeguridadVacacion(seguridadVacacion);
  }

  public void updateSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception {
    this.seguridadVacacionBeanBusiness.updateSeguridadVacacion(seguridadVacacion);
  }

  public void deleteSeguridadVacacion(SeguridadVacacion seguridadVacacion) throws Exception {
    this.seguridadVacacionBeanBusiness.deleteSeguridadVacacion(seguridadVacacion);
  }

  public SeguridadVacacion findSeguridadVacacionById(long seguridadVacacionId) throws Exception {
    return this.seguridadVacacionBeanBusiness.findSeguridadVacacionById(seguridadVacacionId);
  }

  public Collection findAllSeguridadVacacion() throws Exception {
    return this.seguridadVacacionBeanBusiness.findSeguridadVacacionAll();
  }

  public Collection findSeguridadVacacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.seguridadVacacionBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }
}