package sigefirrhh.personal.procesoAniversario;

import java.io.Serializable;

public class OtrasAlicuotasPK
  implements Serializable
{
  public long idOtrasAlicuotas;

  public OtrasAlicuotasPK()
  {
  }

  public OtrasAlicuotasPK(long idOtrasAlicuotas)
  {
    this.idOtrasAlicuotas = idOtrasAlicuotas;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((OtrasAlicuotasPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(OtrasAlicuotasPK thatPK)
  {
    return 
      this.idOtrasAlicuotas == thatPK.idOtrasAlicuotas;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idOtrasAlicuotas)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idOtrasAlicuotas);
  }
}