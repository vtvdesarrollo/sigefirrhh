package sigefirrhh.personal.jubilacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;

public class PensionInvalidez
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_BASE;
  protected static final Map LISTA_SINO;
  private long idPensionInvalidez;
  private TipoPersonal tipoPersonal;
  private double porcentajeMaximoPension;
  private String base;
  private double numeroMeses;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "base", "idPensionInvalidez", "numeroMeses", "organismo", "porcentajeMaximoPension", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.jubilacion.PensionInvalidez"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PensionInvalidez());

    LISTA_BASE = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_BASE.put("A", "SUELDO ACTUAL");
    LISTA_BASE.put("P", "SUELDO PROMEDIO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public PensionInvalidez()
  {
    jdoSetporcentajeMaximoPension(this, 0.0D);

    jdoSetbase(this, "P");

    jdoSetnumeroMeses(this, 0.0D);
  }

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre();
  }

  public String getBase()
  {
    return jdoGetbase(this);
  }

  public double getNumeroMeses()
  {
    return jdoGetnumeroMeses(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public double getPorcentajeMaximoPension()
  {
    return jdoGetporcentajeMaximoPension(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setBase(String string)
  {
    jdoSetbase(this, string);
  }

  public void setNumeroMeses(double d)
  {
    jdoSetnumeroMeses(this, d);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setPorcentajeMaximoPension(double d)
  {
    jdoSetporcentajeMaximoPension(this, d);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public long getIdPensionInvalidez()
  {
    return jdoGetidPensionInvalidez(this);
  }

  public void setIdPensionInvalidez(long l)
  {
    jdoSetidPensionInvalidez(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PensionInvalidez localPensionInvalidez = new PensionInvalidez();
    localPensionInvalidez.jdoFlags = 1;
    localPensionInvalidez.jdoStateManager = paramStateManager;
    return localPensionInvalidez;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PensionInvalidez localPensionInvalidez = new PensionInvalidez();
    localPensionInvalidez.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPensionInvalidez.jdoFlags = 1;
    localPensionInvalidez.jdoStateManager = paramStateManager;
    return localPensionInvalidez;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.base);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPensionInvalidez);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.numeroMeses);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeMaximoPension);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.base = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPensionInvalidez = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMeses = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeMaximoPension = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PensionInvalidez paramPensionInvalidez, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPensionInvalidez == null)
        throw new IllegalArgumentException("arg1");
      this.base = paramPensionInvalidez.base;
      return;
    case 1:
      if (paramPensionInvalidez == null)
        throw new IllegalArgumentException("arg1");
      this.idPensionInvalidez = paramPensionInvalidez.idPensionInvalidez;
      return;
    case 2:
      if (paramPensionInvalidez == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMeses = paramPensionInvalidez.numeroMeses;
      return;
    case 3:
      if (paramPensionInvalidez == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramPensionInvalidez.organismo;
      return;
    case 4:
      if (paramPensionInvalidez == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeMaximoPension = paramPensionInvalidez.porcentajeMaximoPension;
      return;
    case 5:
      if (paramPensionInvalidez == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPensionInvalidez.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PensionInvalidez))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PensionInvalidez localPensionInvalidez = (PensionInvalidez)paramObject;
    if (localPensionInvalidez.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPensionInvalidez, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PensionInvalidezPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PensionInvalidezPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PensionInvalidezPK))
      throw new IllegalArgumentException("arg1");
    PensionInvalidezPK localPensionInvalidezPK = (PensionInvalidezPK)paramObject;
    localPensionInvalidezPK.idPensionInvalidez = this.idPensionInvalidez;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PensionInvalidezPK))
      throw new IllegalArgumentException("arg1");
    PensionInvalidezPK localPensionInvalidezPK = (PensionInvalidezPK)paramObject;
    this.idPensionInvalidez = localPensionInvalidezPK.idPensionInvalidez;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PensionInvalidezPK))
      throw new IllegalArgumentException("arg2");
    PensionInvalidezPK localPensionInvalidezPK = (PensionInvalidezPK)paramObject;
    localPensionInvalidezPK.idPensionInvalidez = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PensionInvalidezPK))
      throw new IllegalArgumentException("arg2");
    PensionInvalidezPK localPensionInvalidezPK = (PensionInvalidezPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localPensionInvalidezPK.idPensionInvalidez);
  }

  private static final String jdoGetbase(PensionInvalidez paramPensionInvalidez)
  {
    if (paramPensionInvalidez.jdoFlags <= 0)
      return paramPensionInvalidez.base;
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
      return paramPensionInvalidez.base;
    if (localStateManager.isLoaded(paramPensionInvalidez, jdoInheritedFieldCount + 0))
      return paramPensionInvalidez.base;
    return localStateManager.getStringField(paramPensionInvalidez, jdoInheritedFieldCount + 0, paramPensionInvalidez.base);
  }

  private static final void jdoSetbase(PensionInvalidez paramPensionInvalidez, String paramString)
  {
    if (paramPensionInvalidez.jdoFlags == 0)
    {
      paramPensionInvalidez.base = paramString;
      return;
    }
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionInvalidez.base = paramString;
      return;
    }
    localStateManager.setStringField(paramPensionInvalidez, jdoInheritedFieldCount + 0, paramPensionInvalidez.base, paramString);
  }

  private static final long jdoGetidPensionInvalidez(PensionInvalidez paramPensionInvalidez)
  {
    return paramPensionInvalidez.idPensionInvalidez;
  }

  private static final void jdoSetidPensionInvalidez(PensionInvalidez paramPensionInvalidez, long paramLong)
  {
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionInvalidez.idPensionInvalidez = paramLong;
      return;
    }
    localStateManager.setLongField(paramPensionInvalidez, jdoInheritedFieldCount + 1, paramPensionInvalidez.idPensionInvalidez, paramLong);
  }

  private static final double jdoGetnumeroMeses(PensionInvalidez paramPensionInvalidez)
  {
    if (paramPensionInvalidez.jdoFlags <= 0)
      return paramPensionInvalidez.numeroMeses;
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
      return paramPensionInvalidez.numeroMeses;
    if (localStateManager.isLoaded(paramPensionInvalidez, jdoInheritedFieldCount + 2))
      return paramPensionInvalidez.numeroMeses;
    return localStateManager.getDoubleField(paramPensionInvalidez, jdoInheritedFieldCount + 2, paramPensionInvalidez.numeroMeses);
  }

  private static final void jdoSetnumeroMeses(PensionInvalidez paramPensionInvalidez, double paramDouble)
  {
    if (paramPensionInvalidez.jdoFlags == 0)
    {
      paramPensionInvalidez.numeroMeses = paramDouble;
      return;
    }
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionInvalidez.numeroMeses = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPensionInvalidez, jdoInheritedFieldCount + 2, paramPensionInvalidez.numeroMeses, paramDouble);
  }

  private static final Organismo jdoGetorganismo(PensionInvalidez paramPensionInvalidez)
  {
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
      return paramPensionInvalidez.organismo;
    if (localStateManager.isLoaded(paramPensionInvalidez, jdoInheritedFieldCount + 3))
      return paramPensionInvalidez.organismo;
    return (Organismo)localStateManager.getObjectField(paramPensionInvalidez, jdoInheritedFieldCount + 3, paramPensionInvalidez.organismo);
  }

  private static final void jdoSetorganismo(PensionInvalidez paramPensionInvalidez, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionInvalidez.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramPensionInvalidez, jdoInheritedFieldCount + 3, paramPensionInvalidez.organismo, paramOrganismo);
  }

  private static final double jdoGetporcentajeMaximoPension(PensionInvalidez paramPensionInvalidez)
  {
    if (paramPensionInvalidez.jdoFlags <= 0)
      return paramPensionInvalidez.porcentajeMaximoPension;
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
      return paramPensionInvalidez.porcentajeMaximoPension;
    if (localStateManager.isLoaded(paramPensionInvalidez, jdoInheritedFieldCount + 4))
      return paramPensionInvalidez.porcentajeMaximoPension;
    return localStateManager.getDoubleField(paramPensionInvalidez, jdoInheritedFieldCount + 4, paramPensionInvalidez.porcentajeMaximoPension);
  }

  private static final void jdoSetporcentajeMaximoPension(PensionInvalidez paramPensionInvalidez, double paramDouble)
  {
    if (paramPensionInvalidez.jdoFlags == 0)
    {
      paramPensionInvalidez.porcentajeMaximoPension = paramDouble;
      return;
    }
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionInvalidez.porcentajeMaximoPension = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPensionInvalidez, jdoInheritedFieldCount + 4, paramPensionInvalidez.porcentajeMaximoPension, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(PensionInvalidez paramPensionInvalidez)
  {
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
      return paramPensionInvalidez.tipoPersonal;
    if (localStateManager.isLoaded(paramPensionInvalidez, jdoInheritedFieldCount + 5))
      return paramPensionInvalidez.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPensionInvalidez, jdoInheritedFieldCount + 5, paramPensionInvalidez.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PensionInvalidez paramPensionInvalidez, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPensionInvalidez.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionInvalidez.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPensionInvalidez, jdoInheritedFieldCount + 5, paramPensionInvalidez.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}