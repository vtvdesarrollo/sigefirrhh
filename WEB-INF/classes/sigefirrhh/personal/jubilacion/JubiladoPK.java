package sigefirrhh.personal.jubilacion;

import java.io.Serializable;

public class JubiladoPK
  implements Serializable
{
  public long idJubilado;

  public JubiladoPK()
  {
  }

  public JubiladoPK(long idJubilado)
  {
    this.idJubilado = idJubilado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((JubiladoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(JubiladoPK thatPK)
  {
    return 
      this.idJubilado == thatPK.idJubilado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idJubilado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idJubilado);
  }
}