package sigefirrhh.personal.jubilacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Institucion;
import sigefirrhh.personal.trabajador.Trabajador;

public class Jubilado
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idJubilado;
  private Trabajador trabajador;
  private String tipoJubilacion;
  private Date fechaJubilacion;
  private String codCargo;
  private String descripcionCargo;
  private int grado;
  private double baseJubilacion;
  private double porcentajeJubilacion;
  private double montoJubilacion;
  private Institucion institucion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "baseJubilacion", "codCargo", "descripcionCargo", "fechaJubilacion", "grado", "idJubilado", "institucion", "montoJubilacion", "porcentajeJubilacion", "tipoJubilacion", "trabajador" }; private static final Class[] jdoFieldTypes = { Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.Institucion"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 26, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.jubilacion.Jubilado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Jubilado());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("0", "REGLAMENTARIA");
    LISTA_TIPO.put("1", "ESPECIAL");
  }

  public Jubilado()
  {
    jdoSettipoJubilacion(this, "1");

    jdoSetgrado(this, 1);

    jdoSetbaseJubilacion(this, 0.0D);

    jdoSetporcentajeJubilacion(this, 0.0D);

    jdoSetmontoJubilacion(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoJubilacion(this));
    return new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaJubilacion(this)) + a;
  }

  public double getBaseJubilacion()
  {
    return jdoGetbaseJubilacion(this);
  }
  public void setBaseJubilacion(double baseJubilacion) {
    jdoSetbaseJubilacion(this, baseJubilacion);
  }
  public String getCodCargo() {
    return jdoGetcodCargo(this);
  }
  public void setCodCargo(String codCargo) {
    jdoSetcodCargo(this, codCargo);
  }
  public String getDescripcionCargo() {
    return jdoGetdescripcionCargo(this);
  }
  public void setDescripcionCargo(String descripcionCargo) {
    jdoSetdescripcionCargo(this, descripcionCargo);
  }
  public Date getFechaJubilacion() {
    return jdoGetfechaJubilacion(this);
  }
  public void setFechaJubilacion(Date fechaJubilacion) {
    jdoSetfechaJubilacion(this, fechaJubilacion);
  }
  public int getGrado() {
    return jdoGetgrado(this);
  }
  public void setGrado(int grado) {
    jdoSetgrado(this, grado);
  }
  public long getIdJubilado() {
    return jdoGetidJubilado(this);
  }
  public void setIdJubilado(long idJubilado) {
    jdoSetidJubilado(this, idJubilado);
  }
  public double getMontoJubilacion() {
    return jdoGetmontoJubilacion(this);
  }
  public void setMontoJubilacion(double montoJubilacion) {
    jdoSetmontoJubilacion(this, montoJubilacion);
  }
  public double getPorcentajeJubilacion() {
    return jdoGetporcentajeJubilacion(this);
  }
  public void setPorcentajeJubilacion(double porcentajeJubilacion) {
    jdoSetporcentajeJubilacion(this, porcentajeJubilacion);
  }
  public String getTipoJubilacion() {
    return jdoGettipoJubilacion(this);
  }
  public void setTipoJubilacion(String tipoJubilacion) {
    jdoSettipoJubilacion(this, tipoJubilacion);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }
  public Institucion getInstitucion() {
    return jdoGetinstitucion(this);
  }
  public void setInstitucion(Institucion institucion) {
    jdoSetinstitucion(this, institucion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Jubilado localJubilado = new Jubilado();
    localJubilado.jdoFlags = 1;
    localJubilado.jdoStateManager = paramStateManager;
    return localJubilado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Jubilado localJubilado = new Jubilado();
    localJubilado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localJubilado.jdoFlags = 1;
    localJubilado.jdoStateManager = paramStateManager;
    return localJubilado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseJubilacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaJubilacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idJubilado);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.institucion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoJubilacion);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeJubilacion);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoJubilacion);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaJubilacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idJubilado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.institucion = ((Institucion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoJubilacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Jubilado paramJubilado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.baseJubilacion = paramJubilado.baseJubilacion;
      return;
    case 1:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramJubilado.codCargo;
      return;
    case 2:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionCargo = paramJubilado.descripcionCargo;
      return;
    case 3:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.fechaJubilacion = paramJubilado.fechaJubilacion;
      return;
    case 4:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramJubilado.grado;
      return;
    case 5:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.idJubilado = paramJubilado.idJubilado;
      return;
    case 6:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.institucion = paramJubilado.institucion;
      return;
    case 7:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.montoJubilacion = paramJubilado.montoJubilacion;
      return;
    case 8:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeJubilacion = paramJubilado.porcentajeJubilacion;
      return;
    case 9:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.tipoJubilacion = paramJubilado.tipoJubilacion;
      return;
    case 10:
      if (paramJubilado == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramJubilado.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Jubilado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Jubilado localJubilado = (Jubilado)paramObject;
    if (localJubilado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localJubilado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new JubiladoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new JubiladoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof JubiladoPK))
      throw new IllegalArgumentException("arg1");
    JubiladoPK localJubiladoPK = (JubiladoPK)paramObject;
    localJubiladoPK.idJubilado = this.idJubilado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof JubiladoPK))
      throw new IllegalArgumentException("arg1");
    JubiladoPK localJubiladoPK = (JubiladoPK)paramObject;
    this.idJubilado = localJubiladoPK.idJubilado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof JubiladoPK))
      throw new IllegalArgumentException("arg2");
    JubiladoPK localJubiladoPK = (JubiladoPK)paramObject;
    localJubiladoPK.idJubilado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof JubiladoPK))
      throw new IllegalArgumentException("arg2");
    JubiladoPK localJubiladoPK = (JubiladoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localJubiladoPK.idJubilado);
  }

  private static final double jdoGetbaseJubilacion(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.baseJubilacion;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.baseJubilacion;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 0))
      return paramJubilado.baseJubilacion;
    return localStateManager.getDoubleField(paramJubilado, jdoInheritedFieldCount + 0, paramJubilado.baseJubilacion);
  }

  private static final void jdoSetbaseJubilacion(Jubilado paramJubilado, double paramDouble)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.baseJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.baseJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramJubilado, jdoInheritedFieldCount + 0, paramJubilado.baseJubilacion, paramDouble);
  }

  private static final String jdoGetcodCargo(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.codCargo;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.codCargo;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 1))
      return paramJubilado.codCargo;
    return localStateManager.getStringField(paramJubilado, jdoInheritedFieldCount + 1, paramJubilado.codCargo);
  }

  private static final void jdoSetcodCargo(Jubilado paramJubilado, String paramString)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramJubilado, jdoInheritedFieldCount + 1, paramJubilado.codCargo, paramString);
  }

  private static final String jdoGetdescripcionCargo(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.descripcionCargo;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.descripcionCargo;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 2))
      return paramJubilado.descripcionCargo;
    return localStateManager.getStringField(paramJubilado, jdoInheritedFieldCount + 2, paramJubilado.descripcionCargo);
  }

  private static final void jdoSetdescripcionCargo(Jubilado paramJubilado, String paramString)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.descripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.descripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramJubilado, jdoInheritedFieldCount + 2, paramJubilado.descripcionCargo, paramString);
  }

  private static final Date jdoGetfechaJubilacion(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.fechaJubilacion;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.fechaJubilacion;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 3))
      return paramJubilado.fechaJubilacion;
    return (Date)localStateManager.getObjectField(paramJubilado, jdoInheritedFieldCount + 3, paramJubilado.fechaJubilacion);
  }

  private static final void jdoSetfechaJubilacion(Jubilado paramJubilado, Date paramDate)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.fechaJubilacion = paramDate;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.fechaJubilacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramJubilado, jdoInheritedFieldCount + 3, paramJubilado.fechaJubilacion, paramDate);
  }

  private static final int jdoGetgrado(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.grado;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.grado;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 4))
      return paramJubilado.grado;
    return localStateManager.getIntField(paramJubilado, jdoInheritedFieldCount + 4, paramJubilado.grado);
  }

  private static final void jdoSetgrado(Jubilado paramJubilado, int paramInt)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramJubilado, jdoInheritedFieldCount + 4, paramJubilado.grado, paramInt);
  }

  private static final long jdoGetidJubilado(Jubilado paramJubilado)
  {
    return paramJubilado.idJubilado;
  }

  private static final void jdoSetidJubilado(Jubilado paramJubilado, long paramLong)
  {
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.idJubilado = paramLong;
      return;
    }
    localStateManager.setLongField(paramJubilado, jdoInheritedFieldCount + 5, paramJubilado.idJubilado, paramLong);
  }

  private static final Institucion jdoGetinstitucion(Jubilado paramJubilado)
  {
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.institucion;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 6))
      return paramJubilado.institucion;
    return (Institucion)localStateManager.getObjectField(paramJubilado, jdoInheritedFieldCount + 6, paramJubilado.institucion);
  }

  private static final void jdoSetinstitucion(Jubilado paramJubilado, Institucion paramInstitucion)
  {
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.institucion = paramInstitucion;
      return;
    }
    localStateManager.setObjectField(paramJubilado, jdoInheritedFieldCount + 6, paramJubilado.institucion, paramInstitucion);
  }

  private static final double jdoGetmontoJubilacion(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.montoJubilacion;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.montoJubilacion;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 7))
      return paramJubilado.montoJubilacion;
    return localStateManager.getDoubleField(paramJubilado, jdoInheritedFieldCount + 7, paramJubilado.montoJubilacion);
  }

  private static final void jdoSetmontoJubilacion(Jubilado paramJubilado, double paramDouble)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.montoJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.montoJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramJubilado, jdoInheritedFieldCount + 7, paramJubilado.montoJubilacion, paramDouble);
  }

  private static final double jdoGetporcentajeJubilacion(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.porcentajeJubilacion;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.porcentajeJubilacion;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 8))
      return paramJubilado.porcentajeJubilacion;
    return localStateManager.getDoubleField(paramJubilado, jdoInheritedFieldCount + 8, paramJubilado.porcentajeJubilacion);
  }

  private static final void jdoSetporcentajeJubilacion(Jubilado paramJubilado, double paramDouble)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.porcentajeJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.porcentajeJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramJubilado, jdoInheritedFieldCount + 8, paramJubilado.porcentajeJubilacion, paramDouble);
  }

  private static final String jdoGettipoJubilacion(Jubilado paramJubilado)
  {
    if (paramJubilado.jdoFlags <= 0)
      return paramJubilado.tipoJubilacion;
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.tipoJubilacion;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 9))
      return paramJubilado.tipoJubilacion;
    return localStateManager.getStringField(paramJubilado, jdoInheritedFieldCount + 9, paramJubilado.tipoJubilacion);
  }

  private static final void jdoSettipoJubilacion(Jubilado paramJubilado, String paramString)
  {
    if (paramJubilado.jdoFlags == 0)
    {
      paramJubilado.tipoJubilacion = paramString;
      return;
    }
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.tipoJubilacion = paramString;
      return;
    }
    localStateManager.setStringField(paramJubilado, jdoInheritedFieldCount + 9, paramJubilado.tipoJubilacion, paramString);
  }

  private static final Trabajador jdoGettrabajador(Jubilado paramJubilado)
  {
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
      return paramJubilado.trabajador;
    if (localStateManager.isLoaded(paramJubilado, jdoInheritedFieldCount + 10))
      return paramJubilado.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramJubilado, jdoInheritedFieldCount + 10, paramJubilado.trabajador);
  }

  private static final void jdoSettrabajador(Jubilado paramJubilado, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramJubilado.jdoStateManager;
    if (localStateManager == null)
    {
      paramJubilado.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramJubilado, jdoInheritedFieldCount + 10, paramJubilado.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}