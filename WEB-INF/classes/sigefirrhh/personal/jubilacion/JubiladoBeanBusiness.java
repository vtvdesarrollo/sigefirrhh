package sigefirrhh.personal.jubilacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Institucion;
import sigefirrhh.base.personal.InstitucionBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class JubiladoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addJubilado(Jubilado jubilado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Jubilado jubiladoNew = 
      (Jubilado)BeanUtils.cloneBean(
      jubilado);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (jubiladoNew.getTrabajador() != null) {
      jubiladoNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        jubiladoNew.getTrabajador().getIdTrabajador()));
    }

    InstitucionBeanBusiness institucionBeanBusiness = new InstitucionBeanBusiness();

    if (jubiladoNew.getInstitucion() != null) {
      jubiladoNew.setInstitucion(
        institucionBeanBusiness.findInstitucionById(
        jubiladoNew.getInstitucion().getIdInstitucion()));
    }
    pm.makePersistent(jubiladoNew);
  }

  public void updateJubilado(Jubilado jubilado) throws Exception
  {
    Jubilado jubiladoModify = 
      findJubiladoById(jubilado.getIdJubilado());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (jubilado.getTrabajador() != null) {
      jubilado.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        jubilado.getTrabajador().getIdTrabajador()));
    }

    InstitucionBeanBusiness institucionBeanBusiness = new InstitucionBeanBusiness();

    if (jubilado.getInstitucion() != null) {
      jubilado.setInstitucion(
        institucionBeanBusiness.findInstitucionById(
        jubilado.getInstitucion().getIdInstitucion()));
    }

    BeanUtils.copyProperties(jubiladoModify, jubilado);
  }

  public void deleteJubilado(Jubilado jubilado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Jubilado jubiladoDelete = 
      findJubiladoById(jubilado.getIdJubilado());
    pm.deletePersistent(jubiladoDelete);
  }

  public Jubilado findJubiladoById(long idJubilado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idJubilado == pIdJubilado";
    Query query = pm.newQuery(Jubilado.class, filter);

    query.declareParameters("long pIdJubilado");

    parameters.put("pIdJubilado", new Long(idJubilado));

    Collection colJubilado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colJubilado.iterator();
    return (Jubilado)iterator.next();
  }

  public Collection findJubiladoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent jubiladoExtent = pm.getExtent(
      Jubilado.class, true);
    Query query = pm.newQuery(jubiladoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(Jubilado.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colJubilado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colJubilado);

    return colJubilado;
  }

  public Collection findByInstitucion(long idInstitucion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "institucion.idInstitucion == pIdInstitucion";

    Query query = pm.newQuery(Jubilado.class, filter);

    query.declareParameters("long pIdInstitucion");
    HashMap parameters = new HashMap();

    parameters.put("pIdInstitucion", new Long(idInstitucion));

    Collection colJubilado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colJubilado);

    return colJubilado;
  }
}