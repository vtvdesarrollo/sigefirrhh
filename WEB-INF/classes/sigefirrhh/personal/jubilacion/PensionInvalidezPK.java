package sigefirrhh.personal.jubilacion;

import java.io.Serializable;

public class PensionInvalidezPK
  implements Serializable
{
  public long idPensionInvalidez;

  public PensionInvalidezPK()
  {
  }

  public PensionInvalidezPK(long idPensionInvalidez)
  {
    this.idPensionInvalidez = idPensionInvalidez;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PensionInvalidezPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PensionInvalidezPK thatPK)
  {
    return 
      this.idPensionInvalidez == thatPK.idPensionInvalidez;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPensionInvalidez)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPensionInvalidez);
  }
}