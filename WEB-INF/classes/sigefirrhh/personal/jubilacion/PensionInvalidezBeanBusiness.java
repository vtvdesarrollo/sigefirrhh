package sigefirrhh.personal.jubilacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class PensionInvalidezBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPensionInvalidez(PensionInvalidez pensionInvalidez)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PensionInvalidez pensionInvalidezNew = 
      (PensionInvalidez)BeanUtils.cloneBean(
      pensionInvalidez);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (pensionInvalidezNew.getTipoPersonal() != null) {
      pensionInvalidezNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        pensionInvalidezNew.getTipoPersonal().getIdTipoPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (pensionInvalidezNew.getOrganismo() != null) {
      pensionInvalidezNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        pensionInvalidezNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(pensionInvalidezNew);
  }

  public void updatePensionInvalidez(PensionInvalidez pensionInvalidez) throws Exception
  {
    PensionInvalidez pensionInvalidezModify = 
      findPensionInvalidezById(pensionInvalidez.getIdPensionInvalidez());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (pensionInvalidez.getTipoPersonal() != null) {
      pensionInvalidez.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        pensionInvalidez.getTipoPersonal().getIdTipoPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (pensionInvalidez.getOrganismo() != null) {
      pensionInvalidez.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        pensionInvalidez.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(pensionInvalidezModify, pensionInvalidez);
  }

  public void deletePensionInvalidez(PensionInvalidez pensionInvalidez) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PensionInvalidez pensionInvalidezDelete = 
      findPensionInvalidezById(pensionInvalidez.getIdPensionInvalidez());
    pm.deletePersistent(pensionInvalidezDelete);
  }

  public PensionInvalidez findPensionInvalidezById(long idPensionInvalidez) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPensionInvalidez == pIdPensionInvalidez";
    Query query = pm.newQuery(PensionInvalidez.class, filter);

    query.declareParameters("long pIdPensionInvalidez");

    parameters.put("pIdPensionInvalidez", new Long(idPensionInvalidez));

    Collection colPensionInvalidez = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPensionInvalidez.iterator();
    return (PensionInvalidez)iterator.next();
  }

  public Collection findPensionInvalidezAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent pensionInvalidezExtent = pm.getExtent(
      PensionInvalidez.class, true);
    Query query = pm.newQuery(pensionInvalidezExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(PensionInvalidez.class, filter);

    query.declareParameters("long pIdTipoPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPensionInvalidez = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPensionInvalidez);

    return colPensionInvalidez;
  }
}