package sigefirrhh.personal.jubilacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.trabajador.Trabajador;

public class Sobreviviente
  implements Serializable, PersistenceCapable
{
  private long idSobreviviente;
  private Trabajador trabajador;
  private Familiar familiar;
  private String observacion;
  private String situacion;
  private Date fechaPension;
  private double montoPensionOriginal;
  private double montoPensionActual;
  private double porcentajePensionOriginal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "familiar", "fechaPension", "idSobreviviente", "montoPensionActual", "montoPensionOriginal", "observacion", "porcentajePensionOriginal", "situacion", "trabajador" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 26, 21, 24, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Sobreviviente()
  {
    jdoSetmontoPensionOriginal(this, 0.0D);

    jdoSetmontoPensionActual(this, 0.0D);

    jdoSetporcentajePensionOriginal(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoPensionActual(this));

    return jdoGetfamiliar(this).getPrimerApellido() + ", " + 
      jdoGetfamiliar(this).getPrimerNombre() + " - " + 
      jdoGetfamiliar(this).getParentesco() + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfamiliar(this).getFechaNacimiento()) + " - " + a;
  }

  public Date getFechaPension()
  {
    return jdoGetfechaPension(this);
  }
  public void setFechaPension(Date fechaPension) {
    jdoSetfechaPension(this, fechaPension);
  }
  public long getIdSobreviviente() {
    return jdoGetidSobreviviente(this);
  }
  public void setIdSobreviviente(long idSobreviviente) {
    jdoSetidSobreviviente(this, idSobreviviente);
  }
  public double getMontoPensionOriginal() {
    return jdoGetmontoPensionOriginal(this);
  }
  public void setMontoPensionOriginal(double montoPensionOriginal) {
    jdoSetmontoPensionOriginal(this, montoPensionOriginal);
  }

  public double getPorcentajePensionOriginal() {
    return jdoGetporcentajePensionOriginal(this);
  }
  public void setPorcentajePensionOriginal(double porcentajePensionOriginal) {
    jdoSetporcentajePensionOriginal(this, porcentajePensionOriginal);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }
  public String getSituacion() {
    return jdoGetsituacion(this);
  }
  public void setSituacion(String situacion) {
    jdoSetsituacion(this, situacion);
  }
  public String getObservacion() {
    return jdoGetobservacion(this);
  }
  public void setObservacion(String observacion) {
    jdoSetobservacion(this, observacion);
  }
  public Familiar getFamiliar() {
    return jdoGetfamiliar(this);
  }

  public void setFamiliar(Familiar familiar) {
    jdoSetfamiliar(this, familiar);
  }
  public double getMontoPensionActual() {
    return jdoGetmontoPensionActual(this);
  }
  public void setMontoPensionActual(double montoPensionActual) {
    jdoSetmontoPensionActual(this, montoPensionActual);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.jubilacion.Sobreviviente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Sobreviviente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Sobreviviente localSobreviviente = new Sobreviviente();
    localSobreviviente.jdoFlags = 1;
    localSobreviviente.jdoStateManager = paramStateManager;
    return localSobreviviente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Sobreviviente localSobreviviente = new Sobreviviente();
    localSobreviviente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSobreviviente.jdoFlags = 1;
    localSobreviviente.jdoStateManager = paramStateManager;
    return localSobreviviente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPension);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSobreviviente);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPensionActual);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPensionOriginal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajePensionOriginal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPension = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSobreviviente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPensionActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPensionOriginal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajePensionOriginal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Sobreviviente paramSobreviviente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramSobreviviente.familiar;
      return;
    case 1:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPension = paramSobreviviente.fechaPension;
      return;
    case 2:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.idSobreviviente = paramSobreviviente.idSobreviviente;
      return;
    case 3:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.montoPensionActual = paramSobreviviente.montoPensionActual;
      return;
    case 4:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.montoPensionOriginal = paramSobreviviente.montoPensionOriginal;
      return;
    case 5:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.observacion = paramSobreviviente.observacion;
      return;
    case 6:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajePensionOriginal = paramSobreviviente.porcentajePensionOriginal;
      return;
    case 7:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramSobreviviente.situacion;
      return;
    case 8:
      if (paramSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramSobreviviente.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Sobreviviente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Sobreviviente localSobreviviente = (Sobreviviente)paramObject;
    if (localSobreviviente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSobreviviente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SobrevivientePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SobrevivientePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SobrevivientePK))
      throw new IllegalArgumentException("arg1");
    SobrevivientePK localSobrevivientePK = (SobrevivientePK)paramObject;
    localSobrevivientePK.idSobreviviente = this.idSobreviviente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SobrevivientePK))
      throw new IllegalArgumentException("arg1");
    SobrevivientePK localSobrevivientePK = (SobrevivientePK)paramObject;
    this.idSobreviviente = localSobrevivientePK.idSobreviviente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SobrevivientePK))
      throw new IllegalArgumentException("arg2");
    SobrevivientePK localSobrevivientePK = (SobrevivientePK)paramObject;
    localSobrevivientePK.idSobreviviente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SobrevivientePK))
      throw new IllegalArgumentException("arg2");
    SobrevivientePK localSobrevivientePK = (SobrevivientePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSobrevivientePK.idSobreviviente);
  }

  private static final Familiar jdoGetfamiliar(Sobreviviente paramSobreviviente)
  {
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.familiar;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 0))
      return paramSobreviviente.familiar;
    return (Familiar)localStateManager.getObjectField(paramSobreviviente, jdoInheritedFieldCount + 0, paramSobreviviente.familiar);
  }

  private static final void jdoSetfamiliar(Sobreviviente paramSobreviviente, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramSobreviviente, jdoInheritedFieldCount + 0, paramSobreviviente.familiar, paramFamiliar);
  }

  private static final Date jdoGetfechaPension(Sobreviviente paramSobreviviente)
  {
    if (paramSobreviviente.jdoFlags <= 0)
      return paramSobreviviente.fechaPension;
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.fechaPension;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 1))
      return paramSobreviviente.fechaPension;
    return (Date)localStateManager.getObjectField(paramSobreviviente, jdoInheritedFieldCount + 1, paramSobreviviente.fechaPension);
  }

  private static final void jdoSetfechaPension(Sobreviviente paramSobreviviente, Date paramDate)
  {
    if (paramSobreviviente.jdoFlags == 0)
    {
      paramSobreviviente.fechaPension = paramDate;
      return;
    }
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.fechaPension = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSobreviviente, jdoInheritedFieldCount + 1, paramSobreviviente.fechaPension, paramDate);
  }

  private static final long jdoGetidSobreviviente(Sobreviviente paramSobreviviente)
  {
    return paramSobreviviente.idSobreviviente;
  }

  private static final void jdoSetidSobreviviente(Sobreviviente paramSobreviviente, long paramLong)
  {
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.idSobreviviente = paramLong;
      return;
    }
    localStateManager.setLongField(paramSobreviviente, jdoInheritedFieldCount + 2, paramSobreviviente.idSobreviviente, paramLong);
  }

  private static final double jdoGetmontoPensionActual(Sobreviviente paramSobreviviente)
  {
    if (paramSobreviviente.jdoFlags <= 0)
      return paramSobreviviente.montoPensionActual;
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.montoPensionActual;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 3))
      return paramSobreviviente.montoPensionActual;
    return localStateManager.getDoubleField(paramSobreviviente, jdoInheritedFieldCount + 3, paramSobreviviente.montoPensionActual);
  }

  private static final void jdoSetmontoPensionActual(Sobreviviente paramSobreviviente, double paramDouble)
  {
    if (paramSobreviviente.jdoFlags == 0)
    {
      paramSobreviviente.montoPensionActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.montoPensionActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSobreviviente, jdoInheritedFieldCount + 3, paramSobreviviente.montoPensionActual, paramDouble);
  }

  private static final double jdoGetmontoPensionOriginal(Sobreviviente paramSobreviviente)
  {
    if (paramSobreviviente.jdoFlags <= 0)
      return paramSobreviviente.montoPensionOriginal;
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.montoPensionOriginal;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 4))
      return paramSobreviviente.montoPensionOriginal;
    return localStateManager.getDoubleField(paramSobreviviente, jdoInheritedFieldCount + 4, paramSobreviviente.montoPensionOriginal);
  }

  private static final void jdoSetmontoPensionOriginal(Sobreviviente paramSobreviviente, double paramDouble)
  {
    if (paramSobreviviente.jdoFlags == 0)
    {
      paramSobreviviente.montoPensionOriginal = paramDouble;
      return;
    }
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.montoPensionOriginal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSobreviviente, jdoInheritedFieldCount + 4, paramSobreviviente.montoPensionOriginal, paramDouble);
  }

  private static final String jdoGetobservacion(Sobreviviente paramSobreviviente)
  {
    if (paramSobreviviente.jdoFlags <= 0)
      return paramSobreviviente.observacion;
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.observacion;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 5))
      return paramSobreviviente.observacion;
    return localStateManager.getStringField(paramSobreviviente, jdoInheritedFieldCount + 5, paramSobreviviente.observacion);
  }

  private static final void jdoSetobservacion(Sobreviviente paramSobreviviente, String paramString)
  {
    if (paramSobreviviente.jdoFlags == 0)
    {
      paramSobreviviente.observacion = paramString;
      return;
    }
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.observacion = paramString;
      return;
    }
    localStateManager.setStringField(paramSobreviviente, jdoInheritedFieldCount + 5, paramSobreviviente.observacion, paramString);
  }

  private static final double jdoGetporcentajePensionOriginal(Sobreviviente paramSobreviviente)
  {
    if (paramSobreviviente.jdoFlags <= 0)
      return paramSobreviviente.porcentajePensionOriginal;
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.porcentajePensionOriginal;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 6))
      return paramSobreviviente.porcentajePensionOriginal;
    return localStateManager.getDoubleField(paramSobreviviente, jdoInheritedFieldCount + 6, paramSobreviviente.porcentajePensionOriginal);
  }

  private static final void jdoSetporcentajePensionOriginal(Sobreviviente paramSobreviviente, double paramDouble)
  {
    if (paramSobreviviente.jdoFlags == 0)
    {
      paramSobreviviente.porcentajePensionOriginal = paramDouble;
      return;
    }
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.porcentajePensionOriginal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSobreviviente, jdoInheritedFieldCount + 6, paramSobreviviente.porcentajePensionOriginal, paramDouble);
  }

  private static final String jdoGetsituacion(Sobreviviente paramSobreviviente)
  {
    if (paramSobreviviente.jdoFlags <= 0)
      return paramSobreviviente.situacion;
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.situacion;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 7))
      return paramSobreviviente.situacion;
    return localStateManager.getStringField(paramSobreviviente, jdoInheritedFieldCount + 7, paramSobreviviente.situacion);
  }

  private static final void jdoSetsituacion(Sobreviviente paramSobreviviente, String paramString)
  {
    if (paramSobreviviente.jdoFlags == 0)
    {
      paramSobreviviente.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramSobreviviente, jdoInheritedFieldCount + 7, paramSobreviviente.situacion, paramString);
  }

  private static final Trabajador jdoGettrabajador(Sobreviviente paramSobreviviente)
  {
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramSobreviviente.trabajador;
    if (localStateManager.isLoaded(paramSobreviviente, jdoInheritedFieldCount + 8))
      return paramSobreviviente.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramSobreviviente, jdoInheritedFieldCount + 8, paramSobreviviente.trabajador);
  }

  private static final void jdoSettrabajador(Sobreviviente paramSobreviviente, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramSobreviviente.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramSobreviviente, jdoInheritedFieldCount + 8, paramSobreviviente.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}