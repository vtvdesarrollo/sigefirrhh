package sigefirrhh.personal.jubilacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class Tutor
  implements Serializable, PersistenceCapable
{
  private long idTutor;
  private Personal personal;
  private Familiar familiar;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "familiar", "idTutor", "personal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Familiar getFamiliar()
  {
    return jdoGetfamiliar(this);
  }

  public long getIdTutor()
  {
    return jdoGetidTutor(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setFamiliar(Familiar familiar)
  {
    jdoSetfamiliar(this, familiar);
  }

  public void setIdTutor(long l)
  {
    jdoSetidTutor(this, l);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.jubilacion.Tutor"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Tutor());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Tutor localTutor = new Tutor();
    localTutor.jdoFlags = 1;
    localTutor.jdoStateManager = paramStateManager;
    return localTutor;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Tutor localTutor = new Tutor();
    localTutor.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTutor.jdoFlags = 1;
    localTutor.jdoStateManager = paramStateManager;
    return localTutor;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTutor);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTutor = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Tutor paramTutor, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTutor == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramTutor.familiar;
      return;
    case 1:
      if (paramTutor == null)
        throw new IllegalArgumentException("arg1");
      this.idTutor = paramTutor.idTutor;
      return;
    case 2:
      if (paramTutor == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramTutor.personal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Tutor))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Tutor localTutor = (Tutor)paramObject;
    if (localTutor.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTutor, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TutorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TutorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TutorPK))
      throw new IllegalArgumentException("arg1");
    TutorPK localTutorPK = (TutorPK)paramObject;
    localTutorPK.idTutor = this.idTutor;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TutorPK))
      throw new IllegalArgumentException("arg1");
    TutorPK localTutorPK = (TutorPK)paramObject;
    this.idTutor = localTutorPK.idTutor;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TutorPK))
      throw new IllegalArgumentException("arg2");
    TutorPK localTutorPK = (TutorPK)paramObject;
    localTutorPK.idTutor = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TutorPK))
      throw new IllegalArgumentException("arg2");
    TutorPK localTutorPK = (TutorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTutorPK.idTutor);
  }

  private static final Familiar jdoGetfamiliar(Tutor paramTutor)
  {
    StateManager localStateManager = paramTutor.jdoStateManager;
    if (localStateManager == null)
      return paramTutor.familiar;
    if (localStateManager.isLoaded(paramTutor, jdoInheritedFieldCount + 0))
      return paramTutor.familiar;
    return (Familiar)localStateManager.getObjectField(paramTutor, jdoInheritedFieldCount + 0, paramTutor.familiar);
  }

  private static final void jdoSetfamiliar(Tutor paramTutor, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramTutor.jdoStateManager;
    if (localStateManager == null)
    {
      paramTutor.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramTutor, jdoInheritedFieldCount + 0, paramTutor.familiar, paramFamiliar);
  }

  private static final long jdoGetidTutor(Tutor paramTutor)
  {
    return paramTutor.idTutor;
  }

  private static final void jdoSetidTutor(Tutor paramTutor, long paramLong)
  {
    StateManager localStateManager = paramTutor.jdoStateManager;
    if (localStateManager == null)
    {
      paramTutor.idTutor = paramLong;
      return;
    }
    localStateManager.setLongField(paramTutor, jdoInheritedFieldCount + 1, paramTutor.idTutor, paramLong);
  }

  private static final Personal jdoGetpersonal(Tutor paramTutor)
  {
    StateManager localStateManager = paramTutor.jdoStateManager;
    if (localStateManager == null)
      return paramTutor.personal;
    if (localStateManager.isLoaded(paramTutor, jdoInheritedFieldCount + 2))
      return paramTutor.personal;
    return (Personal)localStateManager.getObjectField(paramTutor, jdoInheritedFieldCount + 2, paramTutor.personal);
  }

  private static final void jdoSetpersonal(Tutor paramTutor, Personal paramPersonal)
  {
    StateManager localStateManager = paramTutor.jdoStateManager;
    if (localStateManager == null)
    {
      paramTutor.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramTutor, jdoInheritedFieldCount + 2, paramTutor.personal, paramPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}