package sigefirrhh.personal.jubilacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class PensionSobrevivienteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPensionSobreviviente(PensionSobreviviente pensionSobreviviente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PensionSobreviviente pensionSobrevivienteNew = 
      (PensionSobreviviente)BeanUtils.cloneBean(
      pensionSobreviviente);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (pensionSobrevivienteNew.getTipoPersonal() != null) {
      pensionSobrevivienteNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        pensionSobrevivienteNew.getTipoPersonal().getIdTipoPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (pensionSobrevivienteNew.getOrganismo() != null) {
      pensionSobrevivienteNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        pensionSobrevivienteNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(pensionSobrevivienteNew);
  }

  public void updatePensionSobreviviente(PensionSobreviviente pensionSobreviviente) throws Exception
  {
    PensionSobreviviente pensionSobrevivienteModify = 
      findPensionSobrevivienteById(pensionSobreviviente.getIdPensionSobreviviente());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (pensionSobreviviente.getTipoPersonal() != null) {
      pensionSobreviviente.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        pensionSobreviviente.getTipoPersonal().getIdTipoPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (pensionSobreviviente.getOrganismo() != null) {
      pensionSobreviviente.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        pensionSobreviviente.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(pensionSobrevivienteModify, pensionSobreviviente);
  }

  public void deletePensionSobreviviente(PensionSobreviviente pensionSobreviviente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PensionSobreviviente pensionSobrevivienteDelete = 
      findPensionSobrevivienteById(pensionSobreviviente.getIdPensionSobreviviente());
    pm.deletePersistent(pensionSobrevivienteDelete);
  }

  public PensionSobreviviente findPensionSobrevivienteById(long idPensionSobreviviente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPensionSobreviviente == pIdPensionSobreviviente";
    Query query = pm.newQuery(PensionSobreviviente.class, filter);

    query.declareParameters("long pIdPensionSobreviviente");

    parameters.put("pIdPensionSobreviviente", new Long(idPensionSobreviviente));

    Collection colPensionSobreviviente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPensionSobreviviente.iterator();
    return (PensionSobreviviente)iterator.next();
  }

  public Collection findPensionSobrevivienteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent pensionSobrevivienteExtent = pm.getExtent(
      PensionSobreviviente.class, true);
    Query query = pm.newQuery(pensionSobrevivienteExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(PensionSobreviviente.class, filter);

    query.declareParameters("long pIdTipoPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colPensionSobreviviente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPensionSobreviviente);

    return colPensionSobreviviente;
  }
}