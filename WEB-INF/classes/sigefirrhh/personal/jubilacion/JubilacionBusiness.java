package sigefirrhh.personal.jubilacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class JubilacionBusiness extends AbstractBusiness
  implements Serializable
{
  private JubiladoBeanBusiness jubiladoBeanBusiness = new JubiladoBeanBusiness();

  private SobrevivienteBeanBusiness sobrevivienteBeanBusiness = new SobrevivienteBeanBusiness();

  private PensionInvalidezBeanBusiness pensionInvalidezBeanBusiness = new PensionInvalidezBeanBusiness();

  private PensionSobrevivienteBeanBusiness pensionSobrevivienteBeanBusiness = new PensionSobrevivienteBeanBusiness();

  public void addJubilado(Jubilado jubilado)
    throws Exception
  {
    this.jubiladoBeanBusiness.addJubilado(jubilado);
  }

  public void updateJubilado(Jubilado jubilado) throws Exception {
    this.jubiladoBeanBusiness.updateJubilado(jubilado);
  }

  public void deleteJubilado(Jubilado jubilado) throws Exception {
    this.jubiladoBeanBusiness.deleteJubilado(jubilado);
  }

  public Jubilado findJubiladoById(long jubiladoId) throws Exception {
    return this.jubiladoBeanBusiness.findJubiladoById(jubiladoId);
  }

  public Collection findAllJubilado() throws Exception {
    return this.jubiladoBeanBusiness.findJubiladoAll();
  }

  public Collection findJubiladoByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.jubiladoBeanBusiness.findByTrabajador(idTrabajador);
  }

  public Collection findJubiladoByInstitucion(long idInstitucion)
    throws Exception
  {
    return this.jubiladoBeanBusiness.findByInstitucion(idInstitucion);
  }

  public void addSobreviviente(Sobreviviente sobreviviente)
    throws Exception
  {
    this.sobrevivienteBeanBusiness.addSobreviviente(sobreviviente);
  }

  public void updateSobreviviente(Sobreviviente sobreviviente) throws Exception {
    this.sobrevivienteBeanBusiness.updateSobreviviente(sobreviviente);
  }

  public void deleteSobreviviente(Sobreviviente sobreviviente) throws Exception {
    this.sobrevivienteBeanBusiness.deleteSobreviviente(sobreviviente);
  }

  public Sobreviviente findSobrevivienteById(long sobrevivienteId) throws Exception {
    return this.sobrevivienteBeanBusiness.findSobrevivienteById(sobrevivienteId);
  }

  public Collection findAllSobreviviente() throws Exception {
    return this.sobrevivienteBeanBusiness.findSobrevivienteAll();
  }

  public Collection findSobrevivienteByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.sobrevivienteBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addPensionInvalidez(PensionInvalidez pensionInvalidez)
    throws Exception
  {
    this.pensionInvalidezBeanBusiness.addPensionInvalidez(pensionInvalidez);
  }

  public void updatePensionInvalidez(PensionInvalidez pensionInvalidez) throws Exception {
    this.pensionInvalidezBeanBusiness.updatePensionInvalidez(pensionInvalidez);
  }

  public void deletePensionInvalidez(PensionInvalidez pensionInvalidez) throws Exception {
    this.pensionInvalidezBeanBusiness.deletePensionInvalidez(pensionInvalidez);
  }

  public PensionInvalidez findPensionInvalidezById(long pensionInvalidezId) throws Exception {
    return this.pensionInvalidezBeanBusiness.findPensionInvalidezById(pensionInvalidezId);
  }

  public Collection findAllPensionInvalidez() throws Exception {
    return this.pensionInvalidezBeanBusiness.findPensionInvalidezAll();
  }

  public Collection findPensionInvalidezByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    return this.pensionInvalidezBeanBusiness.findByTipoPersonal(idTipoPersonal, idOrganismo);
  }

  public void addPensionSobreviviente(PensionSobreviviente pensionSobreviviente)
    throws Exception
  {
    this.pensionSobrevivienteBeanBusiness.addPensionSobreviviente(pensionSobreviviente);
  }

  public void updatePensionSobreviviente(PensionSobreviviente pensionSobreviviente) throws Exception {
    this.pensionSobrevivienteBeanBusiness.updatePensionSobreviviente(pensionSobreviviente);
  }

  public void deletePensionSobreviviente(PensionSobreviviente pensionSobreviviente) throws Exception {
    this.pensionSobrevivienteBeanBusiness.deletePensionSobreviviente(pensionSobreviviente);
  }

  public PensionSobreviviente findPensionSobrevivienteById(long pensionSobrevivienteId) throws Exception {
    return this.pensionSobrevivienteBeanBusiness.findPensionSobrevivienteById(pensionSobrevivienteId);
  }

  public Collection findAllPensionSobreviviente() throws Exception {
    return this.pensionSobrevivienteBeanBusiness.findPensionSobrevivienteAll();
  }

  public Collection findPensionSobrevivienteByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    return this.pensionSobrevivienteBeanBusiness.findByTipoPersonal(idTipoPersonal, idOrganismo);
  }
}