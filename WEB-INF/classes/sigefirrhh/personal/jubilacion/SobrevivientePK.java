package sigefirrhh.personal.jubilacion;

import java.io.Serializable;

public class SobrevivientePK
  implements Serializable
{
  public long idSobreviviente;

  public SobrevivientePK()
  {
  }

  public SobrevivientePK(long idSobreviviente)
  {
    this.idSobreviviente = idSobreviviente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SobrevivientePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SobrevivientePK thatPK)
  {
    return 
      this.idSobreviviente == thatPK.idSobreviviente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSobreviviente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSobreviviente);
  }
}