package sigefirrhh.personal.jubilacion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class JubilacionFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private JubilacionBusiness jubilacionBusiness = new JubilacionBusiness();

  public void addJubilado(Jubilado jubilado)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.jubilacionBusiness.addJubilado(jubilado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateJubilado(Jubilado jubilado) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.updateJubilado(jubilado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteJubilado(Jubilado jubilado) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.deleteJubilado(jubilado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Jubilado findJubiladoById(long jubiladoId) throws Exception
  {
    try { this.txn.open();
      Jubilado jubilado = 
        this.jubilacionBusiness.findJubiladoById(jubiladoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(jubilado);
      return jubilado;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllJubilado() throws Exception
  {
    try { this.txn.open();
      return this.jubilacionBusiness.findAllJubilado();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findJubiladoByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.jubilacionBusiness.findJubiladoByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findJubiladoByInstitucion(long idInstitucion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.jubilacionBusiness.findJubiladoByInstitucion(idInstitucion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addSobreviviente(Sobreviviente sobreviviente) throws Exception
  {
    try {
      this.txn.open();
      this.jubilacionBusiness.addSobreviviente(sobreviviente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSobreviviente(Sobreviviente sobreviviente) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.updateSobreviviente(sobreviviente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSobreviviente(Sobreviviente sobreviviente) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.deleteSobreviviente(sobreviviente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Sobreviviente findSobrevivienteById(long sobrevivienteId) throws Exception
  {
    try { this.txn.open();
      Sobreviviente sobreviviente = 
        this.jubilacionBusiness.findSobrevivienteById(sobrevivienteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(sobreviviente);
      return sobreviviente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSobreviviente() throws Exception
  {
    try { this.txn.open();
      return this.jubilacionBusiness.findAllSobreviviente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSobrevivienteByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.jubilacionBusiness.findSobrevivienteByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPensionInvalidez(PensionInvalidez pensionInvalidez)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.jubilacionBusiness.addPensionInvalidez(pensionInvalidez);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePensionInvalidez(PensionInvalidez pensionInvalidez) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.updatePensionInvalidez(pensionInvalidez);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePensionInvalidez(PensionInvalidez pensionInvalidez) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.deletePensionInvalidez(pensionInvalidez);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PensionInvalidez findPensionInvalidezById(long pensionInvalidezId) throws Exception
  {
    try { this.txn.open();
      PensionInvalidez pensionInvalidez = 
        this.jubilacionBusiness.findPensionInvalidezById(pensionInvalidezId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(pensionInvalidez);
      return pensionInvalidez;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPensionInvalidez() throws Exception
  {
    try { this.txn.open();
      return this.jubilacionBusiness.findAllPensionInvalidez();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPensionInvalidezByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.jubilacionBusiness.findPensionInvalidezByTipoPersonal(idTipoPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPensionSobreviviente(PensionSobreviviente pensionSobreviviente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.jubilacionBusiness.addPensionSobreviviente(pensionSobreviviente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePensionSobreviviente(PensionSobreviviente pensionSobreviviente) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.updatePensionSobreviviente(pensionSobreviviente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePensionSobreviviente(PensionSobreviviente pensionSobreviviente) throws Exception
  {
    try { this.txn.open();
      this.jubilacionBusiness.deletePensionSobreviviente(pensionSobreviviente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PensionSobreviviente findPensionSobrevivienteById(long pensionSobrevivienteId) throws Exception
  {
    try { this.txn.open();
      PensionSobreviviente pensionSobreviviente = 
        this.jubilacionBusiness.findPensionSobrevivienteById(pensionSobrevivienteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(pensionSobreviviente);
      return pensionSobreviviente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPensionSobreviviente() throws Exception
  {
    try { this.txn.open();
      return this.jubilacionBusiness.findAllPensionSobreviviente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPensionSobrevivienteByTipoPersonal(long idTipoPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.jubilacionBusiness.findPensionSobrevivienteByTipoPersonal(idTipoPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}