package sigefirrhh.personal.jubilacion;

import java.io.Serializable;

public class TutorPK
  implements Serializable
{
  public long idTutor;

  public TutorPK()
  {
  }

  public TutorPK(long idTutor)
  {
    this.idTutor = idTutor;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TutorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TutorPK thatPK)
  {
    return 
      this.idTutor == thatPK.idTutor;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTutor)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTutor);
  }
}