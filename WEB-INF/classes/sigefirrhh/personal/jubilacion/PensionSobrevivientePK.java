package sigefirrhh.personal.jubilacion;

import java.io.Serializable;

public class PensionSobrevivientePK
  implements Serializable
{
  public long idPensionSobreviviente;

  public PensionSobrevivientePK()
  {
  }

  public PensionSobrevivientePK(long idPensionSobreviviente)
  {
    this.idPensionSobreviviente = idPensionSobreviviente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PensionSobrevivientePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PensionSobrevivientePK thatPK)
  {
    return 
      this.idPensionSobreviviente == thatPK.idPensionSobreviviente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPensionSobreviviente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPensionSobreviviente);
  }
}