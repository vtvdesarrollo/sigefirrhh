package sigefirrhh.personal.jubilacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.FamiliarBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class SobrevivienteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSobreviviente(Sobreviviente sobreviviente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Sobreviviente sobrevivienteNew = 
      (Sobreviviente)BeanUtils.cloneBean(
      sobreviviente);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (sobrevivienteNew.getTrabajador() != null) {
      sobrevivienteNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        sobrevivienteNew.getTrabajador().getIdTrabajador()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (sobrevivienteNew.getFamiliar() != null) {
      sobrevivienteNew.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        sobrevivienteNew.getFamiliar().getIdFamiliar()));
    }

    pm.makePersistent(sobrevivienteNew);
  }

  public void updateSobreviviente(Sobreviviente sobreviviente) throws Exception
  {
    Sobreviviente sobrevivienteModify = 
      findSobrevivienteById(sobreviviente.getIdSobreviviente());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (sobreviviente.getTrabajador() != null) {
      sobreviviente.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        sobreviviente.getTrabajador().getIdTrabajador()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (sobreviviente.getFamiliar() != null) {
      sobreviviente.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        sobreviviente.getFamiliar().getIdFamiliar()));
    }

    BeanUtils.copyProperties(sobrevivienteModify, sobreviviente);
  }

  public void deleteSobreviviente(Sobreviviente sobreviviente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Sobreviviente sobrevivienteDelete = 
      findSobrevivienteById(sobreviviente.getIdSobreviviente());
    pm.deletePersistent(sobrevivienteDelete);
  }

  public Sobreviviente findSobrevivienteById(long idSobreviviente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSobreviviente == pIdSobreviviente";
    Query query = pm.newQuery(Sobreviviente.class, filter);

    query.declareParameters("long pIdSobreviviente");

    parameters.put("pIdSobreviviente", new Long(idSobreviviente));

    Collection colSobreviviente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSobreviviente.iterator();
    return (Sobreviviente)iterator.next();
  }

  public Collection findSobrevivienteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent sobrevivienteExtent = pm.getExtent(
      Sobreviviente.class, true);
    Query query = pm.newQuery(sobrevivienteExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(Sobreviviente.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colSobreviviente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSobreviviente);

    return colSobreviviente;
  }
}