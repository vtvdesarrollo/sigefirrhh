package sigefirrhh.personal.jubilacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;

public class PensionSobreviviente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_BASE;
  protected static final Map LISTA_SINO;
  private long idPensionSobreviviente;
  private TipoPersonal tipoPersonal;
  private double porcentajeMaximoPension;
  private String base;
  private double numeroMeses;
  private Organismo organismo;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "base", "idPensionSobreviviente", "idSitp", "numeroMeses", "organismo", "porcentajeMaximoPension", "tiempoSitp", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 26, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.jubilacion.PensionSobreviviente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PensionSobreviviente());

    LISTA_BASE = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_BASE.put("A", "MONTO JUBILACION ACTUAL");
    LISTA_BASE.put("P", "MONTO JUBILACION PROMEDIO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public PensionSobreviviente()
  {
    jdoSetporcentajeMaximoPension(this, 0.0D);

    jdoSetbase(this, "P");

    jdoSetnumeroMeses(this, 0.0D);

    jdoSetidSitp(this, 0);
  }

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre();
  }
  public String getBase() {
    return jdoGetbase(this);
  }
  public void setBase(String base) {
    jdoSetbase(this, base);
  }
  public long getIdPensionSobreviviente() {
    return jdoGetidPensionSobreviviente(this);
  }
  public void setIdPensionSobreviviente(long idPensionSobreviviente) {
    jdoSetidPensionSobreviviente(this, idPensionSobreviviente);
  }
  public int getIdSitp() {
    return jdoGetidSitp(this);
  }
  public void setIdSitp(int idSitp) {
    jdoSetidSitp(this, idSitp);
  }
  public double getNumeroMeses() {
    return jdoGetnumeroMeses(this);
  }
  public void setNumeroMeses(double numeroMeses) {
    jdoSetnumeroMeses(this, numeroMeses);
  }
  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }
  public double getPorcentajeMaximoPension() {
    return jdoGetporcentajeMaximoPension(this);
  }
  public void setPorcentajeMaximoPension(double porcentajeMaximoPension) {
    jdoSetporcentajeMaximoPension(this, porcentajeMaximoPension);
  }
  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }
  public void setTiempoSitp(Date tiempoSitp) {
    jdoSettiempoSitp(this, tiempoSitp);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PensionSobreviviente localPensionSobreviviente = new PensionSobreviviente();
    localPensionSobreviviente.jdoFlags = 1;
    localPensionSobreviviente.jdoStateManager = paramStateManager;
    return localPensionSobreviviente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PensionSobreviviente localPensionSobreviviente = new PensionSobreviviente();
    localPensionSobreviviente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPensionSobreviviente.jdoFlags = 1;
    localPensionSobreviviente.jdoStateManager = paramStateManager;
    return localPensionSobreviviente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.base);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPensionSobreviviente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.numeroMeses);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeMaximoPension);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.base = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPensionSobreviviente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMeses = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeMaximoPension = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PensionSobreviviente paramPensionSobreviviente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.base = paramPensionSobreviviente.base;
      return;
    case 1:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.idPensionSobreviviente = paramPensionSobreviviente.idPensionSobreviviente;
      return;
    case 2:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPensionSobreviviente.idSitp;
      return;
    case 3:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMeses = paramPensionSobreviviente.numeroMeses;
      return;
    case 4:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramPensionSobreviviente.organismo;
      return;
    case 5:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeMaximoPension = paramPensionSobreviviente.porcentajeMaximoPension;
      return;
    case 6:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPensionSobreviviente.tiempoSitp;
      return;
    case 7:
      if (paramPensionSobreviviente == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPensionSobreviviente.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PensionSobreviviente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PensionSobreviviente localPensionSobreviviente = (PensionSobreviviente)paramObject;
    if (localPensionSobreviviente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPensionSobreviviente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PensionSobrevivientePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PensionSobrevivientePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PensionSobrevivientePK))
      throw new IllegalArgumentException("arg1");
    PensionSobrevivientePK localPensionSobrevivientePK = (PensionSobrevivientePK)paramObject;
    localPensionSobrevivientePK.idPensionSobreviviente = this.idPensionSobreviviente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PensionSobrevivientePK))
      throw new IllegalArgumentException("arg1");
    PensionSobrevivientePK localPensionSobrevivientePK = (PensionSobrevivientePK)paramObject;
    this.idPensionSobreviviente = localPensionSobrevivientePK.idPensionSobreviviente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PensionSobrevivientePK))
      throw new IllegalArgumentException("arg2");
    PensionSobrevivientePK localPensionSobrevivientePK = (PensionSobrevivientePK)paramObject;
    localPensionSobrevivientePK.idPensionSobreviviente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PensionSobrevivientePK))
      throw new IllegalArgumentException("arg2");
    PensionSobrevivientePK localPensionSobrevivientePK = (PensionSobrevivientePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localPensionSobrevivientePK.idPensionSobreviviente);
  }

  private static final String jdoGetbase(PensionSobreviviente paramPensionSobreviviente)
  {
    if (paramPensionSobreviviente.jdoFlags <= 0)
      return paramPensionSobreviviente.base;
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramPensionSobreviviente.base;
    if (localStateManager.isLoaded(paramPensionSobreviviente, jdoInheritedFieldCount + 0))
      return paramPensionSobreviviente.base;
    return localStateManager.getStringField(paramPensionSobreviviente, jdoInheritedFieldCount + 0, paramPensionSobreviviente.base);
  }

  private static final void jdoSetbase(PensionSobreviviente paramPensionSobreviviente, String paramString)
  {
    if (paramPensionSobreviviente.jdoFlags == 0)
    {
      paramPensionSobreviviente.base = paramString;
      return;
    }
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.base = paramString;
      return;
    }
    localStateManager.setStringField(paramPensionSobreviviente, jdoInheritedFieldCount + 0, paramPensionSobreviviente.base, paramString);
  }

  private static final long jdoGetidPensionSobreviviente(PensionSobreviviente paramPensionSobreviviente)
  {
    return paramPensionSobreviviente.idPensionSobreviviente;
  }

  private static final void jdoSetidPensionSobreviviente(PensionSobreviviente paramPensionSobreviviente, long paramLong)
  {
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.idPensionSobreviviente = paramLong;
      return;
    }
    localStateManager.setLongField(paramPensionSobreviviente, jdoInheritedFieldCount + 1, paramPensionSobreviviente.idPensionSobreviviente, paramLong);
  }

  private static final int jdoGetidSitp(PensionSobreviviente paramPensionSobreviviente)
  {
    if (paramPensionSobreviviente.jdoFlags <= 0)
      return paramPensionSobreviviente.idSitp;
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramPensionSobreviviente.idSitp;
    if (localStateManager.isLoaded(paramPensionSobreviviente, jdoInheritedFieldCount + 2))
      return paramPensionSobreviviente.idSitp;
    return localStateManager.getIntField(paramPensionSobreviviente, jdoInheritedFieldCount + 2, paramPensionSobreviviente.idSitp);
  }

  private static final void jdoSetidSitp(PensionSobreviviente paramPensionSobreviviente, int paramInt)
  {
    if (paramPensionSobreviviente.jdoFlags == 0)
    {
      paramPensionSobreviviente.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPensionSobreviviente, jdoInheritedFieldCount + 2, paramPensionSobreviviente.idSitp, paramInt);
  }

  private static final double jdoGetnumeroMeses(PensionSobreviviente paramPensionSobreviviente)
  {
    if (paramPensionSobreviviente.jdoFlags <= 0)
      return paramPensionSobreviviente.numeroMeses;
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramPensionSobreviviente.numeroMeses;
    if (localStateManager.isLoaded(paramPensionSobreviviente, jdoInheritedFieldCount + 3))
      return paramPensionSobreviviente.numeroMeses;
    return localStateManager.getDoubleField(paramPensionSobreviviente, jdoInheritedFieldCount + 3, paramPensionSobreviviente.numeroMeses);
  }

  private static final void jdoSetnumeroMeses(PensionSobreviviente paramPensionSobreviviente, double paramDouble)
  {
    if (paramPensionSobreviviente.jdoFlags == 0)
    {
      paramPensionSobreviviente.numeroMeses = paramDouble;
      return;
    }
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.numeroMeses = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPensionSobreviviente, jdoInheritedFieldCount + 3, paramPensionSobreviviente.numeroMeses, paramDouble);
  }

  private static final Organismo jdoGetorganismo(PensionSobreviviente paramPensionSobreviviente)
  {
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramPensionSobreviviente.organismo;
    if (localStateManager.isLoaded(paramPensionSobreviviente, jdoInheritedFieldCount + 4))
      return paramPensionSobreviviente.organismo;
    return (Organismo)localStateManager.getObjectField(paramPensionSobreviviente, jdoInheritedFieldCount + 4, paramPensionSobreviviente.organismo);
  }

  private static final void jdoSetorganismo(PensionSobreviviente paramPensionSobreviviente, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramPensionSobreviviente, jdoInheritedFieldCount + 4, paramPensionSobreviviente.organismo, paramOrganismo);
  }

  private static final double jdoGetporcentajeMaximoPension(PensionSobreviviente paramPensionSobreviviente)
  {
    if (paramPensionSobreviviente.jdoFlags <= 0)
      return paramPensionSobreviviente.porcentajeMaximoPension;
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramPensionSobreviviente.porcentajeMaximoPension;
    if (localStateManager.isLoaded(paramPensionSobreviviente, jdoInheritedFieldCount + 5))
      return paramPensionSobreviviente.porcentajeMaximoPension;
    return localStateManager.getDoubleField(paramPensionSobreviviente, jdoInheritedFieldCount + 5, paramPensionSobreviviente.porcentajeMaximoPension);
  }

  private static final void jdoSetporcentajeMaximoPension(PensionSobreviviente paramPensionSobreviviente, double paramDouble)
  {
    if (paramPensionSobreviviente.jdoFlags == 0)
    {
      paramPensionSobreviviente.porcentajeMaximoPension = paramDouble;
      return;
    }
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.porcentajeMaximoPension = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPensionSobreviviente, jdoInheritedFieldCount + 5, paramPensionSobreviviente.porcentajeMaximoPension, paramDouble);
  }

  private static final Date jdoGettiempoSitp(PensionSobreviviente paramPensionSobreviviente)
  {
    if (paramPensionSobreviviente.jdoFlags <= 0)
      return paramPensionSobreviviente.tiempoSitp;
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramPensionSobreviviente.tiempoSitp;
    if (localStateManager.isLoaded(paramPensionSobreviviente, jdoInheritedFieldCount + 6))
      return paramPensionSobreviviente.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPensionSobreviviente, jdoInheritedFieldCount + 6, paramPensionSobreviviente.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(PensionSobreviviente paramPensionSobreviviente, Date paramDate)
  {
    if (paramPensionSobreviviente.jdoFlags == 0)
    {
      paramPensionSobreviviente.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPensionSobreviviente, jdoInheritedFieldCount + 6, paramPensionSobreviviente.tiempoSitp, paramDate);
  }

  private static final TipoPersonal jdoGettipoPersonal(PensionSobreviviente paramPensionSobreviviente)
  {
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
      return paramPensionSobreviviente.tipoPersonal;
    if (localStateManager.isLoaded(paramPensionSobreviviente, jdoInheritedFieldCount + 7))
      return paramPensionSobreviviente.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPensionSobreviviente, jdoInheritedFieldCount + 7, paramPensionSobreviviente.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PensionSobreviviente paramPensionSobreviviente, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPensionSobreviviente.jdoStateManager;
    if (localStateManager == null)
    {
      paramPensionSobreviviente.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPensionSobreviviente, jdoInheritedFieldCount + 7, paramPensionSobreviviente.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}