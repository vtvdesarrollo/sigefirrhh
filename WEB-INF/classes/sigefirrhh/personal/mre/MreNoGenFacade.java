package sigefirrhh.personal.mre;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class MreNoGenFacade extends MreFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private MreNoGenBusiness mreNoGenBusiness = new MreNoGenBusiness();

  public void generarNominaMre(long idGrupoNomina, int numeroNomina, long idNominaEspecial, int anio, int mes, String proceso)
    throws Exception
  {
    this.mreNoGenBusiness.generarNominaMre(idGrupoNomina, numeroNomina, idNominaEspecial, anio, mes, proceso);
  }

  public Collection findNominaDiplomaticoByTrabajador(long idTrabajador, int anio) throws Exception
  {
    try {
      this.txn.open();
      return this.mreNoGenBusiness.findNominaDiplomaticoByTrabajador(idTrabajador, anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}