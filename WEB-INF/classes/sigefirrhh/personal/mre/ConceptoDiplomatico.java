package sigefirrhh.personal.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class ConceptoDiplomatico
  implements Serializable, PersistenceCapable
{
  private long idConceptoDiplomatico;
  private double monto;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "idConceptoDiplomatico", "monto", "trabajador" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ConceptoDiplomatico()
  {
    jdoSetmonto(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      a;
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public long getIdConceptoDiplomatico() {
    return jdoGetidConceptoDiplomatico(this);
  }

  public void setIdConceptoDiplomatico(long idConceptoDiplomatico) {
    jdoSetidConceptoDiplomatico(this, idConceptoDiplomatico);
  }

  public double getMonto() {
    return jdoGetmonto(this);
  }

  public void setMonto(double monto) {
    jdoSetmonto(this, monto);
  }

  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }

  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.mre.ConceptoDiplomatico"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoDiplomatico());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoDiplomatico localConceptoDiplomatico = new ConceptoDiplomatico();
    localConceptoDiplomatico.jdoFlags = 1;
    localConceptoDiplomatico.jdoStateManager = paramStateManager;
    return localConceptoDiplomatico;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoDiplomatico localConceptoDiplomatico = new ConceptoDiplomatico();
    localConceptoDiplomatico.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoDiplomatico.jdoFlags = 1;
    localConceptoDiplomatico.jdoStateManager = paramStateManager;
    return localConceptoDiplomatico;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoDiplomatico);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoDiplomatico = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoDiplomatico paramConceptoDiplomatico, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoDiplomatico.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoDiplomatico = paramConceptoDiplomatico.idConceptoDiplomatico;
      return;
    case 2:
      if (paramConceptoDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoDiplomatico.monto;
      return;
    case 3:
      if (paramConceptoDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramConceptoDiplomatico.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoDiplomatico))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoDiplomatico localConceptoDiplomatico = (ConceptoDiplomatico)paramObject;
    if (localConceptoDiplomatico.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoDiplomatico, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoDiplomaticoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoDiplomaticoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoDiplomaticoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoDiplomaticoPK localConceptoDiplomaticoPK = (ConceptoDiplomaticoPK)paramObject;
    localConceptoDiplomaticoPK.idConceptoDiplomatico = this.idConceptoDiplomatico;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoDiplomaticoPK))
      throw new IllegalArgumentException("arg1");
    ConceptoDiplomaticoPK localConceptoDiplomaticoPK = (ConceptoDiplomaticoPK)paramObject;
    this.idConceptoDiplomatico = localConceptoDiplomaticoPK.idConceptoDiplomatico;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoDiplomaticoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoDiplomaticoPK localConceptoDiplomaticoPK = (ConceptoDiplomaticoPK)paramObject;
    localConceptoDiplomaticoPK.idConceptoDiplomatico = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoDiplomaticoPK))
      throw new IllegalArgumentException("arg2");
    ConceptoDiplomaticoPK localConceptoDiplomaticoPK = (ConceptoDiplomaticoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localConceptoDiplomaticoPK.idConceptoDiplomatico);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoDiplomatico paramConceptoDiplomatico)
  {
    StateManager localStateManager = paramConceptoDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDiplomatico.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoDiplomatico, jdoInheritedFieldCount + 0))
      return paramConceptoDiplomatico.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoDiplomatico, jdoInheritedFieldCount + 0, paramConceptoDiplomatico.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoDiplomatico paramConceptoDiplomatico, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDiplomatico.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoDiplomatico, jdoInheritedFieldCount + 0, paramConceptoDiplomatico.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidConceptoDiplomatico(ConceptoDiplomatico paramConceptoDiplomatico)
  {
    return paramConceptoDiplomatico.idConceptoDiplomatico;
  }

  private static final void jdoSetidConceptoDiplomatico(ConceptoDiplomatico paramConceptoDiplomatico, long paramLong)
  {
    StateManager localStateManager = paramConceptoDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDiplomatico.idConceptoDiplomatico = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoDiplomatico, jdoInheritedFieldCount + 1, paramConceptoDiplomatico.idConceptoDiplomatico, paramLong);
  }

  private static final double jdoGetmonto(ConceptoDiplomatico paramConceptoDiplomatico)
  {
    if (paramConceptoDiplomatico.jdoFlags <= 0)
      return paramConceptoDiplomatico.monto;
    StateManager localStateManager = paramConceptoDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDiplomatico.monto;
    if (localStateManager.isLoaded(paramConceptoDiplomatico, jdoInheritedFieldCount + 2))
      return paramConceptoDiplomatico.monto;
    return localStateManager.getDoubleField(paramConceptoDiplomatico, jdoInheritedFieldCount + 2, paramConceptoDiplomatico.monto);
  }

  private static final void jdoSetmonto(ConceptoDiplomatico paramConceptoDiplomatico, double paramDouble)
  {
    if (paramConceptoDiplomatico.jdoFlags == 0)
    {
      paramConceptoDiplomatico.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDiplomatico.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoDiplomatico, jdoInheritedFieldCount + 2, paramConceptoDiplomatico.monto, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(ConceptoDiplomatico paramConceptoDiplomatico)
  {
    StateManager localStateManager = paramConceptoDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoDiplomatico.trabajador;
    if (localStateManager.isLoaded(paramConceptoDiplomatico, jdoInheritedFieldCount + 3))
      return paramConceptoDiplomatico.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramConceptoDiplomatico, jdoInheritedFieldCount + 3, paramConceptoDiplomatico.trabajador);
  }

  private static final void jdoSettrabajador(ConceptoDiplomatico paramConceptoDiplomatico, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramConceptoDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoDiplomatico.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramConceptoDiplomatico, jdoInheritedFieldCount + 3, paramConceptoDiplomatico.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}