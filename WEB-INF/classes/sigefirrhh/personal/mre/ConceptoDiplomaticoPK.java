package sigefirrhh.personal.mre;

import java.io.Serializable;

public class ConceptoDiplomaticoPK
  implements Serializable
{
  public long idConceptoDiplomatico;

  public ConceptoDiplomaticoPK()
  {
  }

  public ConceptoDiplomaticoPK(long idConceptoDiplomatico)
  {
    this.idConceptoDiplomatico = idConceptoDiplomatico;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoDiplomaticoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoDiplomaticoPK thatPK)
  {
    return 
      this.idConceptoDiplomatico == thatPK.idConceptoDiplomatico;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoDiplomatico)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoDiplomatico);
  }
}