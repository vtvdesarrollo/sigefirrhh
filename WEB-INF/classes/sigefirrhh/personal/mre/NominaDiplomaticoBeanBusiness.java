package sigefirrhh.personal.mre;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class NominaDiplomaticoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addNominaDiplomatico(NominaDiplomatico nominaDiplomatico)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    NominaDiplomatico nominaDiplomaticoNew = 
      (NominaDiplomatico)BeanUtils.cloneBean(
      nominaDiplomatico);

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (nominaDiplomaticoNew.getDependencia() != null) {
      nominaDiplomaticoNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        nominaDiplomaticoNew.getDependencia().getIdDependencia()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (nominaDiplomaticoNew.getCargo() != null) {
      nominaDiplomaticoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        nominaDiplomaticoNew.getCargo().getIdCargo()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (nominaDiplomaticoNew.getTrabajador() != null) {
      nominaDiplomaticoNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        nominaDiplomaticoNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(nominaDiplomaticoNew);
  }

  public void updateNominaDiplomatico(NominaDiplomatico nominaDiplomatico) throws Exception
  {
    NominaDiplomatico nominaDiplomaticoModify = 
      findNominaDiplomaticoById(nominaDiplomatico.getIdNominaDiplomatico());

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (nominaDiplomatico.getDependencia() != null) {
      nominaDiplomatico.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        nominaDiplomatico.getDependencia().getIdDependencia()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (nominaDiplomatico.getCargo() != null) {
      nominaDiplomatico.setCargo(
        cargoBeanBusiness.findCargoById(
        nominaDiplomatico.getCargo().getIdCargo()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (nominaDiplomatico.getTrabajador() != null) {
      nominaDiplomatico.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        nominaDiplomatico.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(nominaDiplomaticoModify, nominaDiplomatico);
  }

  public void deleteNominaDiplomatico(NominaDiplomatico nominaDiplomatico) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    NominaDiplomatico nominaDiplomaticoDelete = 
      findNominaDiplomaticoById(nominaDiplomatico.getIdNominaDiplomatico());
    pm.deletePersistent(nominaDiplomaticoDelete);
  }

  public NominaDiplomatico findNominaDiplomaticoById(long idNominaDiplomatico) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idNominaDiplomatico == pIdNominaDiplomatico";
    Query query = pm.newQuery(NominaDiplomatico.class, filter);

    query.declareParameters("long pIdNominaDiplomatico");

    parameters.put("pIdNominaDiplomatico", new Long(idNominaDiplomatico));

    Collection colNominaDiplomatico = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colNominaDiplomatico.iterator();
    return (NominaDiplomatico)iterator.next();
  }

  public Collection findNominaDiplomaticoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent nominaDiplomaticoExtent = pm.getExtent(
      NominaDiplomatico.class, true);
    Query query = pm.newQuery(nominaDiplomaticoExtent);
    query.setOrdering("mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(NominaDiplomatico.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("mes ascending");

    Collection colNominaDiplomatico = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaDiplomatico);

    return colNominaDiplomatico;
  }
}