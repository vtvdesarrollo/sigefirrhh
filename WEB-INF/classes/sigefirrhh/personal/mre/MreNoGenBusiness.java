package sigefirrhh.personal.mre;

import eforserver.common.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import org.apache.log4j.Logger;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class MreNoGenBusiness extends MreBusiness
{
  Logger log = Logger.getLogger(MreNoGenBusiness.class.getName());

  private NominaDiplomaticoNoGenBeanBusiness nominaDiplomaticoNoGenBeanBusiness = new NominaDiplomaticoNoGenBeanBusiness();

  public boolean generarNominaMre(long idGrupoNomina, int numeroNomina, long idNominaEspecial, int anio, int mes, String proceso)
    throws Exception
  {
    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    sql.append("select generar_nominadiplomatico(?,?,?,?,?,?)");

    this.log.error("3- " + idGrupoNomina);
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idGrupoNomina);
      st.setInt(2, numeroNomina);
      st.setLong(3, idNominaEspecial);
      st.setInt(4, anio);
      st.setInt(5, mes);

      st.setString(6, proceso);

      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);
      connection.commit();

      this.log.error("ejecutó la nomina");

      if (valor == 10) {
        this.log.error("Error");
        return true;
      }
      int valor;
      if (valor == 11) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("Error");
        throw error;
      }
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
    if (rs != null) try {
        rs.close();
      } catch (Exception localException6) {
      } if (st != null) try {
        st.close();
      } catch (Exception localException7) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException8)
      {
      } return false;
  }

  public Collection findNominaDiplomaticoByTrabajador(long idTrabajador, int anio)
    throws Exception
  {
    return this.nominaDiplomaticoNoGenBeanBusiness.findByTrabajador(idTrabajador, anio);
  }
}