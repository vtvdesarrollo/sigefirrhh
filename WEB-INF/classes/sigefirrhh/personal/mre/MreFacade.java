package sigefirrhh.personal.mre;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class MreFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private MreBusiness mreBusiness = new MreBusiness();

  public void addNominaDiplomatico(NominaDiplomatico nominaDiplomatico)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.mreBusiness.addNominaDiplomatico(nominaDiplomatico);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateNominaDiplomatico(NominaDiplomatico nominaDiplomatico) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.updateNominaDiplomatico(nominaDiplomatico);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteNominaDiplomatico(NominaDiplomatico nominaDiplomatico) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.deleteNominaDiplomatico(nominaDiplomatico);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public NominaDiplomatico findNominaDiplomaticoById(long nominaDiplomaticoId) throws Exception
  {
    try { this.txn.open();
      NominaDiplomatico nominaDiplomatico = 
        this.mreBusiness.findNominaDiplomaticoById(nominaDiplomaticoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(nominaDiplomatico);
      return nominaDiplomatico;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllNominaDiplomatico() throws Exception
  {
    try { this.txn.open();
      return this.mreBusiness.findAllNominaDiplomatico();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNominaDiplomaticoByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findNominaDiplomaticoByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.mreBusiness.addConceptoDiplomatico(conceptoDiplomatico);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.updateConceptoDiplomatico(conceptoDiplomatico);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico) throws Exception
  {
    try { this.txn.open();
      this.mreBusiness.deleteConceptoDiplomatico(conceptoDiplomatico);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoDiplomatico findConceptoDiplomaticoById(long conceptoDiplomaticoId) throws Exception
  {
    try { this.txn.open();
      ConceptoDiplomatico conceptoDiplomatico = 
        this.mreBusiness.findConceptoDiplomaticoById(conceptoDiplomaticoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoDiplomatico);
      return conceptoDiplomatico;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoDiplomatico() throws Exception
  {
    try { this.txn.open();
      return this.mreBusiness.findAllConceptoDiplomatico();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoDiplomaticoByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.mreBusiness.findConceptoDiplomaticoByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}