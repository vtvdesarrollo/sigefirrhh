package sigefirrhh.personal.mre;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class NominaDiplomaticoNoGenBeanBusiness extends NominaDiplomaticoBeanBusiness
  implements Serializable
{
  public Collection findByTrabajador(long idTrabajador, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && anio == pAnio";

    Query query = pm.newQuery(NominaDiplomatico.class, filter);

    query.declareParameters("long pIdTrabajador, int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("mes ascending");

    Collection colNominaDiplomatico = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNominaDiplomatico);

    return colNominaDiplomatico;
  }
}