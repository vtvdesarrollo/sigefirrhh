package sigefirrhh.personal.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.personal.trabajador.Trabajador;

public class NominaDiplomatico
  implements Serializable, PersistenceCapable
{
  private long idNominaDiplomatico;
  private int anio;
  private int mes;
  private double asignacionBs;
  private double asignacionOtraMoneda;
  private double deduccionLeyBs;
  private double deduccionLeyOtraMoneda;
  private double deduccionPersonalBs;
  private double deduccionPersonalOtraMoneda;
  private double asignacionMre;
  private double otrasAsignaciones;
  private double fluctuacionMre;
  private double asignacionAnualOnu;
  private double asignacionMensualOnu;
  private double ajusteAnualOnu;
  private double primaDestinoMre;
  private double primaDestinoOnu;
  private double multiplicador;
  private Dependencia dependencia;
  private Cargo cargo;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ajusteAnualOnu", "anio", "asignacionAnualOnu", "asignacionBs", "asignacionMensualOnu", "asignacionMre", "asignacionOtraMoneda", "cargo", "deduccionLeyBs", "deduccionLeyOtraMoneda", "deduccionPersonalBs", "deduccionPersonalOtraMoneda", "dependencia", "fluctuacionMre", "idNominaDiplomatico", "mes", "multiplicador", "otrasAsignaciones", "primaDestinoMre", "primaDestinoOnu", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Double.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 26, 21, 24, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public NominaDiplomatico()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetasignacionBs(this, 0.0D);

    jdoSetasignacionOtraMoneda(this, 0.0D);

    jdoSetdeduccionLeyBs(this, 0.0D);

    jdoSetdeduccionLeyOtraMoneda(this, 0.0D);

    jdoSetdeduccionPersonalBs(this, 0.0D);

    jdoSetdeduccionPersonalOtraMoneda(this, 0.0D);

    jdoSetasignacionMre(this, 0.0D);

    jdoSetotrasAsignaciones(this, 0.0D);

    jdoSetfluctuacionMre(this, 0.0D);

    jdoSetasignacionAnualOnu(this, 0.0D);

    jdoSetasignacionMensualOnu(this, 0.0D);

    jdoSetajusteAnualOnu(this, 0.0D);

    jdoSetprimaDestinoMre(this, 0.0D);

    jdoSetprimaDestinoOnu(this, 0.0D);

    jdoSetmultiplicador(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetprimaDestinoMre(this));
    String c = b.format(jdoGetprimaDestinoOnu(this));

    return jdoGetmes(this) + " - Prima Destino MRE " + a + " Prima Destino ONU " + c;
  }

  public double getAjusteAnualOnu()
  {
    return jdoGetajusteAnualOnu(this);
  }

  public void setAjusteAnualOnu(double ajusteAnualOnu) {
    jdoSetajusteAnualOnu(this, ajusteAnualOnu);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public double getAsignacionAnualOnu() {
    return jdoGetasignacionAnualOnu(this);
  }

  public void setAsignacionAnualOnu(double asignacionAnualOnu) {
    jdoSetasignacionAnualOnu(this, asignacionAnualOnu);
  }

  public double getAsignacionBs() {
    return jdoGetasignacionBs(this);
  }

  public void setAsignacionBs(double asignacionBs) {
    jdoSetasignacionBs(this, asignacionBs);
  }

  public double getAsignacionMensualOnu() {
    return jdoGetasignacionMensualOnu(this);
  }

  public void setAsignacionMensualOnu(double asignacionMensualOnu) {
    jdoSetasignacionMensualOnu(this, asignacionMensualOnu);
  }

  public double getAsignacionMre() {
    return jdoGetasignacionMre(this);
  }

  public void setAsignacionMre(double asignacionMre) {
    jdoSetasignacionMre(this, asignacionMre);
  }

  public double getAsignacionOtraMoneda() {
    return jdoGetasignacionOtraMoneda(this);
  }

  public void setAsignacionOtraMoneda(double asignacionOtraMoneda) {
    jdoSetasignacionOtraMoneda(this, asignacionOtraMoneda);
  }

  public Cargo getCargo() {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }

  public double getDeduccionLeyBs() {
    return jdoGetdeduccionLeyBs(this);
  }

  public void setDeduccionLeyBs(double deduccionLeyBs) {
    jdoSetdeduccionLeyBs(this, deduccionLeyBs);
  }

  public double getDeduccionPersonalBs() {
    return jdoGetdeduccionPersonalBs(this);
  }

  public void setDeduccionPersonalBs(double deduccionPersonalBs) {
    jdoSetdeduccionPersonalBs(this, deduccionPersonalBs);
  }

  public double getDeduccionPersonalOtraMoneda() {
    return jdoGetdeduccionPersonalOtraMoneda(this);
  }

  public void setDeduccionPersonalOtraMoneda(double deduccionPersonalOtraMoneda) {
    jdoSetdeduccionPersonalOtraMoneda(this, deduccionPersonalOtraMoneda);
  }

  public Dependencia getDependencia() {
    return jdoGetdependencia(this);
  }

  public void setDependencia(Dependencia dependencia) {
    jdoSetdependencia(this, dependencia);
  }

  public double getFluctuacionMre() {
    return jdoGetfluctuacionMre(this);
  }

  public void setFluctuacionMre(double fluctuacionMre) {
    jdoSetfluctuacionMre(this, fluctuacionMre);
  }

  public int getMes() {
    return jdoGetmes(this);
  }

  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }

  public double getMultiplicador() {
    return jdoGetmultiplicador(this);
  }

  public void setMultiplicador(double multiplicador) {
    jdoSetmultiplicador(this, multiplicador);
  }

  public double getOtrasAsignaciones() {
    return jdoGetotrasAsignaciones(this);
  }

  public void setOtrasAsignaciones(double otrasAsignaciones) {
    jdoSetotrasAsignaciones(this, otrasAsignaciones);
  }

  public double getPrimaDestinoMre() {
    return jdoGetprimaDestinoMre(this);
  }

  public void setPrimaDestinoMre(double primaDestinoMre) {
    jdoSetprimaDestinoMre(this, primaDestinoMre);
  }

  public double getPrimaDestinoOnu() {
    return jdoGetprimaDestinoOnu(this);
  }

  public void setPrimaDestinoOnu(double primaDestinoOnu) {
    jdoSetprimaDestinoOnu(this, primaDestinoOnu);
  }

  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }

  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public long getIdNominaDiplomatico() {
    return jdoGetidNominaDiplomatico(this);
  }

  public void setIdNominaDiplomatico(long idNominaDiplomatico) {
    jdoSetidNominaDiplomatico(this, idNominaDiplomatico);
  }

  public double getDeduccionLeyOtraMoneda() {
    return jdoGetdeduccionLeyOtraMoneda(this);
  }

  public void setDeduccionLeyOtraMoneda(double deduccionLeyOtraMoneda) {
    jdoSetdeduccionLeyOtraMoneda(this, deduccionLeyOtraMoneda);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 21;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.mre.NominaDiplomatico"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NominaDiplomatico());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NominaDiplomatico localNominaDiplomatico = new NominaDiplomatico();
    localNominaDiplomatico.jdoFlags = 1;
    localNominaDiplomatico.jdoStateManager = paramStateManager;
    return localNominaDiplomatico;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NominaDiplomatico localNominaDiplomatico = new NominaDiplomatico();
    localNominaDiplomatico.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNominaDiplomatico.jdoFlags = 1;
    localNominaDiplomatico.jdoStateManager = paramStateManager;
    return localNominaDiplomatico;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ajusteAnualOnu);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.asignacionAnualOnu);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.asignacionBs);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.asignacionMensualOnu);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.asignacionMre);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.asignacionOtraMoneda);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.deduccionLeyBs);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.deduccionLeyOtraMoneda);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.deduccionPersonalBs);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.deduccionPersonalOtraMoneda);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.fluctuacionMre);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNominaDiplomatico);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.multiplicador);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.otrasAsignaciones);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaDestinoMre);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaDestinoOnu);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ajusteAnualOnu = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignacionAnualOnu = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignacionBs = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignacionMensualOnu = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignacionMre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignacionOtraMoneda = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deduccionLeyBs = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deduccionLeyOtraMoneda = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deduccionPersonalBs = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.deduccionPersonalOtraMoneda = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fluctuacionMre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNominaDiplomatico = localStateManager.replacingLongField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.multiplicador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otrasAsignaciones = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaDestinoMre = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaDestinoOnu = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NominaDiplomatico paramNominaDiplomatico, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.ajusteAnualOnu = paramNominaDiplomatico.ajusteAnualOnu;
      return;
    case 1:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramNominaDiplomatico.anio;
      return;
    case 2:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.asignacionAnualOnu = paramNominaDiplomatico.asignacionAnualOnu;
      return;
    case 3:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.asignacionBs = paramNominaDiplomatico.asignacionBs;
      return;
    case 4:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.asignacionMensualOnu = paramNominaDiplomatico.asignacionMensualOnu;
      return;
    case 5:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.asignacionMre = paramNominaDiplomatico.asignacionMre;
      return;
    case 6:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.asignacionOtraMoneda = paramNominaDiplomatico.asignacionOtraMoneda;
      return;
    case 7:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramNominaDiplomatico.cargo;
      return;
    case 8:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.deduccionLeyBs = paramNominaDiplomatico.deduccionLeyBs;
      return;
    case 9:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.deduccionLeyOtraMoneda = paramNominaDiplomatico.deduccionLeyOtraMoneda;
      return;
    case 10:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.deduccionPersonalBs = paramNominaDiplomatico.deduccionPersonalBs;
      return;
    case 11:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.deduccionPersonalOtraMoneda = paramNominaDiplomatico.deduccionPersonalOtraMoneda;
      return;
    case 12:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramNominaDiplomatico.dependencia;
      return;
    case 13:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.fluctuacionMre = paramNominaDiplomatico.fluctuacionMre;
      return;
    case 14:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.idNominaDiplomatico = paramNominaDiplomatico.idNominaDiplomatico;
      return;
    case 15:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramNominaDiplomatico.mes;
      return;
    case 16:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.multiplicador = paramNominaDiplomatico.multiplicador;
      return;
    case 17:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.otrasAsignaciones = paramNominaDiplomatico.otrasAsignaciones;
      return;
    case 18:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.primaDestinoMre = paramNominaDiplomatico.primaDestinoMre;
      return;
    case 19:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.primaDestinoOnu = paramNominaDiplomatico.primaDestinoOnu;
      return;
    case 20:
      if (paramNominaDiplomatico == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramNominaDiplomatico.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NominaDiplomatico))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NominaDiplomatico localNominaDiplomatico = (NominaDiplomatico)paramObject;
    if (localNominaDiplomatico.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNominaDiplomatico, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NominaDiplomaticoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NominaDiplomaticoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NominaDiplomaticoPK))
      throw new IllegalArgumentException("arg1");
    NominaDiplomaticoPK localNominaDiplomaticoPK = (NominaDiplomaticoPK)paramObject;
    localNominaDiplomaticoPK.idNominaDiplomatico = this.idNominaDiplomatico;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NominaDiplomaticoPK))
      throw new IllegalArgumentException("arg1");
    NominaDiplomaticoPK localNominaDiplomaticoPK = (NominaDiplomaticoPK)paramObject;
    this.idNominaDiplomatico = localNominaDiplomaticoPK.idNominaDiplomatico;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NominaDiplomaticoPK))
      throw new IllegalArgumentException("arg2");
    NominaDiplomaticoPK localNominaDiplomaticoPK = (NominaDiplomaticoPK)paramObject;
    localNominaDiplomaticoPK.idNominaDiplomatico = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 14);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NominaDiplomaticoPK))
      throw new IllegalArgumentException("arg2");
    NominaDiplomaticoPK localNominaDiplomaticoPK = (NominaDiplomaticoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 14, localNominaDiplomaticoPK.idNominaDiplomatico);
  }

  private static final double jdoGetajusteAnualOnu(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.ajusteAnualOnu;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.ajusteAnualOnu;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 0))
      return paramNominaDiplomatico.ajusteAnualOnu;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 0, paramNominaDiplomatico.ajusteAnualOnu);
  }

  private static final void jdoSetajusteAnualOnu(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.ajusteAnualOnu = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.ajusteAnualOnu = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 0, paramNominaDiplomatico.ajusteAnualOnu, paramDouble);
  }

  private static final int jdoGetanio(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.anio;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.anio;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 1))
      return paramNominaDiplomatico.anio;
    return localStateManager.getIntField(paramNominaDiplomatico, jdoInheritedFieldCount + 1, paramNominaDiplomatico.anio);
  }

  private static final void jdoSetanio(NominaDiplomatico paramNominaDiplomatico, int paramInt)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramNominaDiplomatico, jdoInheritedFieldCount + 1, paramNominaDiplomatico.anio, paramInt);
  }

  private static final double jdoGetasignacionAnualOnu(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.asignacionAnualOnu;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.asignacionAnualOnu;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 2))
      return paramNominaDiplomatico.asignacionAnualOnu;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 2, paramNominaDiplomatico.asignacionAnualOnu);
  }

  private static final void jdoSetasignacionAnualOnu(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.asignacionAnualOnu = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.asignacionAnualOnu = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 2, paramNominaDiplomatico.asignacionAnualOnu, paramDouble);
  }

  private static final double jdoGetasignacionBs(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.asignacionBs;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.asignacionBs;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 3))
      return paramNominaDiplomatico.asignacionBs;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 3, paramNominaDiplomatico.asignacionBs);
  }

  private static final void jdoSetasignacionBs(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.asignacionBs = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.asignacionBs = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 3, paramNominaDiplomatico.asignacionBs, paramDouble);
  }

  private static final double jdoGetasignacionMensualOnu(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.asignacionMensualOnu;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.asignacionMensualOnu;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 4))
      return paramNominaDiplomatico.asignacionMensualOnu;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 4, paramNominaDiplomatico.asignacionMensualOnu);
  }

  private static final void jdoSetasignacionMensualOnu(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.asignacionMensualOnu = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.asignacionMensualOnu = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 4, paramNominaDiplomatico.asignacionMensualOnu, paramDouble);
  }

  private static final double jdoGetasignacionMre(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.asignacionMre;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.asignacionMre;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 5))
      return paramNominaDiplomatico.asignacionMre;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 5, paramNominaDiplomatico.asignacionMre);
  }

  private static final void jdoSetasignacionMre(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.asignacionMre = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.asignacionMre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 5, paramNominaDiplomatico.asignacionMre, paramDouble);
  }

  private static final double jdoGetasignacionOtraMoneda(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.asignacionOtraMoneda;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.asignacionOtraMoneda;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 6))
      return paramNominaDiplomatico.asignacionOtraMoneda;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 6, paramNominaDiplomatico.asignacionOtraMoneda);
  }

  private static final void jdoSetasignacionOtraMoneda(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.asignacionOtraMoneda = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.asignacionOtraMoneda = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 6, paramNominaDiplomatico.asignacionOtraMoneda, paramDouble);
  }

  private static final Cargo jdoGetcargo(NominaDiplomatico paramNominaDiplomatico)
  {
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.cargo;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 7))
      return paramNominaDiplomatico.cargo;
    return (Cargo)localStateManager.getObjectField(paramNominaDiplomatico, jdoInheritedFieldCount + 7, paramNominaDiplomatico.cargo);
  }

  private static final void jdoSetcargo(NominaDiplomatico paramNominaDiplomatico, Cargo paramCargo)
  {
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramNominaDiplomatico, jdoInheritedFieldCount + 7, paramNominaDiplomatico.cargo, paramCargo);
  }

  private static final double jdoGetdeduccionLeyBs(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.deduccionLeyBs;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.deduccionLeyBs;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 8))
      return paramNominaDiplomatico.deduccionLeyBs;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 8, paramNominaDiplomatico.deduccionLeyBs);
  }

  private static final void jdoSetdeduccionLeyBs(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.deduccionLeyBs = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.deduccionLeyBs = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 8, paramNominaDiplomatico.deduccionLeyBs, paramDouble);
  }

  private static final double jdoGetdeduccionLeyOtraMoneda(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.deduccionLeyOtraMoneda;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.deduccionLeyOtraMoneda;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 9))
      return paramNominaDiplomatico.deduccionLeyOtraMoneda;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 9, paramNominaDiplomatico.deduccionLeyOtraMoneda);
  }

  private static final void jdoSetdeduccionLeyOtraMoneda(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.deduccionLeyOtraMoneda = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.deduccionLeyOtraMoneda = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 9, paramNominaDiplomatico.deduccionLeyOtraMoneda, paramDouble);
  }

  private static final double jdoGetdeduccionPersonalBs(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.deduccionPersonalBs;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.deduccionPersonalBs;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 10))
      return paramNominaDiplomatico.deduccionPersonalBs;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 10, paramNominaDiplomatico.deduccionPersonalBs);
  }

  private static final void jdoSetdeduccionPersonalBs(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.deduccionPersonalBs = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.deduccionPersonalBs = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 10, paramNominaDiplomatico.deduccionPersonalBs, paramDouble);
  }

  private static final double jdoGetdeduccionPersonalOtraMoneda(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.deduccionPersonalOtraMoneda;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.deduccionPersonalOtraMoneda;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 11))
      return paramNominaDiplomatico.deduccionPersonalOtraMoneda;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 11, paramNominaDiplomatico.deduccionPersonalOtraMoneda);
  }

  private static final void jdoSetdeduccionPersonalOtraMoneda(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.deduccionPersonalOtraMoneda = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.deduccionPersonalOtraMoneda = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 11, paramNominaDiplomatico.deduccionPersonalOtraMoneda, paramDouble);
  }

  private static final Dependencia jdoGetdependencia(NominaDiplomatico paramNominaDiplomatico)
  {
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.dependencia;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 12))
      return paramNominaDiplomatico.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramNominaDiplomatico, jdoInheritedFieldCount + 12, paramNominaDiplomatico.dependencia);
  }

  private static final void jdoSetdependencia(NominaDiplomatico paramNominaDiplomatico, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramNominaDiplomatico, jdoInheritedFieldCount + 12, paramNominaDiplomatico.dependencia, paramDependencia);
  }

  private static final double jdoGetfluctuacionMre(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.fluctuacionMre;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.fluctuacionMre;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 13))
      return paramNominaDiplomatico.fluctuacionMre;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 13, paramNominaDiplomatico.fluctuacionMre);
  }

  private static final void jdoSetfluctuacionMre(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.fluctuacionMre = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.fluctuacionMre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 13, paramNominaDiplomatico.fluctuacionMre, paramDouble);
  }

  private static final long jdoGetidNominaDiplomatico(NominaDiplomatico paramNominaDiplomatico)
  {
    return paramNominaDiplomatico.idNominaDiplomatico;
  }

  private static final void jdoSetidNominaDiplomatico(NominaDiplomatico paramNominaDiplomatico, long paramLong)
  {
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.idNominaDiplomatico = paramLong;
      return;
    }
    localStateManager.setLongField(paramNominaDiplomatico, jdoInheritedFieldCount + 14, paramNominaDiplomatico.idNominaDiplomatico, paramLong);
  }

  private static final int jdoGetmes(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.mes;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.mes;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 15))
      return paramNominaDiplomatico.mes;
    return localStateManager.getIntField(paramNominaDiplomatico, jdoInheritedFieldCount + 15, paramNominaDiplomatico.mes);
  }

  private static final void jdoSetmes(NominaDiplomatico paramNominaDiplomatico, int paramInt)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramNominaDiplomatico, jdoInheritedFieldCount + 15, paramNominaDiplomatico.mes, paramInt);
  }

  private static final double jdoGetmultiplicador(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.multiplicador;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.multiplicador;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 16))
      return paramNominaDiplomatico.multiplicador;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 16, paramNominaDiplomatico.multiplicador);
  }

  private static final void jdoSetmultiplicador(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.multiplicador = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.multiplicador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 16, paramNominaDiplomatico.multiplicador, paramDouble);
  }

  private static final double jdoGetotrasAsignaciones(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.otrasAsignaciones;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.otrasAsignaciones;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 17))
      return paramNominaDiplomatico.otrasAsignaciones;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 17, paramNominaDiplomatico.otrasAsignaciones);
  }

  private static final void jdoSetotrasAsignaciones(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.otrasAsignaciones = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.otrasAsignaciones = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 17, paramNominaDiplomatico.otrasAsignaciones, paramDouble);
  }

  private static final double jdoGetprimaDestinoMre(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.primaDestinoMre;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.primaDestinoMre;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 18))
      return paramNominaDiplomatico.primaDestinoMre;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 18, paramNominaDiplomatico.primaDestinoMre);
  }

  private static final void jdoSetprimaDestinoMre(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.primaDestinoMre = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.primaDestinoMre = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 18, paramNominaDiplomatico.primaDestinoMre, paramDouble);
  }

  private static final double jdoGetprimaDestinoOnu(NominaDiplomatico paramNominaDiplomatico)
  {
    if (paramNominaDiplomatico.jdoFlags <= 0)
      return paramNominaDiplomatico.primaDestinoOnu;
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.primaDestinoOnu;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 19))
      return paramNominaDiplomatico.primaDestinoOnu;
    return localStateManager.getDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 19, paramNominaDiplomatico.primaDestinoOnu);
  }

  private static final void jdoSetprimaDestinoOnu(NominaDiplomatico paramNominaDiplomatico, double paramDouble)
  {
    if (paramNominaDiplomatico.jdoFlags == 0)
    {
      paramNominaDiplomatico.primaDestinoOnu = paramDouble;
      return;
    }
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.primaDestinoOnu = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramNominaDiplomatico, jdoInheritedFieldCount + 19, paramNominaDiplomatico.primaDestinoOnu, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(NominaDiplomatico paramNominaDiplomatico)
  {
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
      return paramNominaDiplomatico.trabajador;
    if (localStateManager.isLoaded(paramNominaDiplomatico, jdoInheritedFieldCount + 20))
      return paramNominaDiplomatico.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramNominaDiplomatico, jdoInheritedFieldCount + 20, paramNominaDiplomatico.trabajador);
  }

  private static final void jdoSettrabajador(NominaDiplomatico paramNominaDiplomatico, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramNominaDiplomatico.jdoStateManager;
    if (localStateManager == null)
    {
      paramNominaDiplomatico.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramNominaDiplomatico, jdoInheritedFieldCount + 20, paramNominaDiplomatico.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}