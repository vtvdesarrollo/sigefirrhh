package sigefirrhh.personal.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class HistoricoNominaMre
  implements Serializable, PersistenceCapable
{
  private long idHistoricoNominaMre;
  private int anio;
  private int mes;
  private double monto;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "conceptoTipoPersonal", "idHistoricoNominaMre", "mes", "monto", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public long getIdHistoricoNominaMre()
  {
    return jdoGetidHistoricoNominaMre(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setIdHistoricoNominaMre(long l)
  {
    jdoSetidHistoricoNominaMre(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.mre.HistoricoNominaMre"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoNominaMre());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoNominaMre localHistoricoNominaMre = new HistoricoNominaMre();
    localHistoricoNominaMre.jdoFlags = 1;
    localHistoricoNominaMre.jdoStateManager = paramStateManager;
    return localHistoricoNominaMre;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoNominaMre localHistoricoNominaMre = new HistoricoNominaMre();
    localHistoricoNominaMre.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoNominaMre.jdoFlags = 1;
    localHistoricoNominaMre.jdoStateManager = paramStateManager;
    return localHistoricoNominaMre;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoNominaMre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoNominaMre = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoNominaMre paramHistoricoNominaMre, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoNominaMre.anio;
      return;
    case 1:
      if (paramHistoricoNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramHistoricoNominaMre.conceptoTipoPersonal;
      return;
    case 2:
      if (paramHistoricoNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoNominaMre = paramHistoricoNominaMre.idHistoricoNominaMre;
      return;
    case 3:
      if (paramHistoricoNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoNominaMre.mes;
      return;
    case 4:
      if (paramHistoricoNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramHistoricoNominaMre.monto;
      return;
    case 5:
      if (paramHistoricoNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHistoricoNominaMre.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoNominaMre))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoNominaMre localHistoricoNominaMre = (HistoricoNominaMre)paramObject;
    if (localHistoricoNominaMre.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoNominaMre, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoNominaMrePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoNominaMrePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoNominaMrePK))
      throw new IllegalArgumentException("arg1");
    HistoricoNominaMrePK localHistoricoNominaMrePK = (HistoricoNominaMrePK)paramObject;
    localHistoricoNominaMrePK.idHistoricoNominaMre = this.idHistoricoNominaMre;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoNominaMrePK))
      throw new IllegalArgumentException("arg1");
    HistoricoNominaMrePK localHistoricoNominaMrePK = (HistoricoNominaMrePK)paramObject;
    this.idHistoricoNominaMre = localHistoricoNominaMrePK.idHistoricoNominaMre;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoNominaMrePK))
      throw new IllegalArgumentException("arg2");
    HistoricoNominaMrePK localHistoricoNominaMrePK = (HistoricoNominaMrePK)paramObject;
    localHistoricoNominaMrePK.idHistoricoNominaMre = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoNominaMrePK))
      throw new IllegalArgumentException("arg2");
    HistoricoNominaMrePK localHistoricoNominaMrePK = (HistoricoNominaMrePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localHistoricoNominaMrePK.idHistoricoNominaMre);
  }

  private static final int jdoGetanio(HistoricoNominaMre paramHistoricoNominaMre)
  {
    if (paramHistoricoNominaMre.jdoFlags <= 0)
      return paramHistoricoNominaMre.anio;
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNominaMre.anio;
    if (localStateManager.isLoaded(paramHistoricoNominaMre, jdoInheritedFieldCount + 0))
      return paramHistoricoNominaMre.anio;
    return localStateManager.getIntField(paramHistoricoNominaMre, jdoInheritedFieldCount + 0, paramHistoricoNominaMre.anio);
  }

  private static final void jdoSetanio(HistoricoNominaMre paramHistoricoNominaMre, int paramInt)
  {
    if (paramHistoricoNominaMre.jdoFlags == 0)
    {
      paramHistoricoNominaMre.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNominaMre.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNominaMre, jdoInheritedFieldCount + 0, paramHistoricoNominaMre.anio, paramInt);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(HistoricoNominaMre paramHistoricoNominaMre)
  {
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNominaMre.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoNominaMre, jdoInheritedFieldCount + 1))
      return paramHistoricoNominaMre.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramHistoricoNominaMre, jdoInheritedFieldCount + 1, paramHistoricoNominaMre.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(HistoricoNominaMre paramHistoricoNominaMre, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNominaMre.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNominaMre, jdoInheritedFieldCount + 1, paramHistoricoNominaMre.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidHistoricoNominaMre(HistoricoNominaMre paramHistoricoNominaMre)
  {
    return paramHistoricoNominaMre.idHistoricoNominaMre;
  }

  private static final void jdoSetidHistoricoNominaMre(HistoricoNominaMre paramHistoricoNominaMre, long paramLong)
  {
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNominaMre.idHistoricoNominaMre = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoNominaMre, jdoInheritedFieldCount + 2, paramHistoricoNominaMre.idHistoricoNominaMre, paramLong);
  }

  private static final int jdoGetmes(HistoricoNominaMre paramHistoricoNominaMre)
  {
    if (paramHistoricoNominaMre.jdoFlags <= 0)
      return paramHistoricoNominaMre.mes;
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNominaMre.mes;
    if (localStateManager.isLoaded(paramHistoricoNominaMre, jdoInheritedFieldCount + 3))
      return paramHistoricoNominaMre.mes;
    return localStateManager.getIntField(paramHistoricoNominaMre, jdoInheritedFieldCount + 3, paramHistoricoNominaMre.mes);
  }

  private static final void jdoSetmes(HistoricoNominaMre paramHistoricoNominaMre, int paramInt)
  {
    if (paramHistoricoNominaMre.jdoFlags == 0)
    {
      paramHistoricoNominaMre.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNominaMre.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoNominaMre, jdoInheritedFieldCount + 3, paramHistoricoNominaMre.mes, paramInt);
  }

  private static final double jdoGetmonto(HistoricoNominaMre paramHistoricoNominaMre)
  {
    if (paramHistoricoNominaMre.jdoFlags <= 0)
      return paramHistoricoNominaMre.monto;
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNominaMre.monto;
    if (localStateManager.isLoaded(paramHistoricoNominaMre, jdoInheritedFieldCount + 4))
      return paramHistoricoNominaMre.monto;
    return localStateManager.getDoubleField(paramHistoricoNominaMre, jdoInheritedFieldCount + 4, paramHistoricoNominaMre.monto);
  }

  private static final void jdoSetmonto(HistoricoNominaMre paramHistoricoNominaMre, double paramDouble)
  {
    if (paramHistoricoNominaMre.jdoFlags == 0)
    {
      paramHistoricoNominaMre.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNominaMre.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoNominaMre, jdoInheritedFieldCount + 4, paramHistoricoNominaMre.monto, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(HistoricoNominaMre paramHistoricoNominaMre)
  {
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoNominaMre.trabajador;
    if (localStateManager.isLoaded(paramHistoricoNominaMre, jdoInheritedFieldCount + 5))
      return paramHistoricoNominaMre.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHistoricoNominaMre, jdoInheritedFieldCount + 5, paramHistoricoNominaMre.trabajador);
  }

  private static final void jdoSettrabajador(HistoricoNominaMre paramHistoricoNominaMre, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHistoricoNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoNominaMre.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHistoricoNominaMre, jdoInheritedFieldCount + 5, paramHistoricoNominaMre.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}