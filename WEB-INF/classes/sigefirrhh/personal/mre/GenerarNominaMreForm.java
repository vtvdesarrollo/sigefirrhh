package sigefirrhh.personal.mre;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;
import sigefirrhh.personal.procesoNomina.SeguridadOrdinaria;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class GenerarNominaMreForm
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarNominaMreForm.class.getName());
  private String selectGrupoNomina;
  private Date inicio;
  private Date fin;
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private String periodicidad;
  private String recalculo;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private MreNoGenFacade mreNoGenFacade = new MreNoGenFacade();
  private boolean show;
  private boolean show2;
  private boolean show3;
  private int anio;
  private int mes;
  private String proceso = "P";

  public GenerarNominaMreForm() { this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.show3 = false;
    try
    {
      if (this.idGrupoNomina != 0L) {
        SeguridadOrdinaria seguridadOrdinaria = this.procesoNominaNoGenFacade.findNominaMre(this.idGrupoNomina);
        this.inicio = seguridadOrdinaria.getFechaInicio();
        this.fin = seguridadOrdinaria.getFechaFin();
        this.anio = seguridadOrdinaria.getAnio();
        this.mes = seguridadOrdinaria.getMes();
        this.show3 = true;
      }
    } catch (Exception e) {
      this.show = false;
      this.show3 = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      log.error("vacio:" + this.listGrupoNomina.isEmpty());
    }
    catch (Exception e) {
      log.error(e.getCause());
      this.listGrupoNomina = new ArrayList();
    }
  }

  public String preGenerate() { FacesContext context = FacesContext.getCurrentInstance();
    this.show2 = true;
    this.show = false;
    return null; }

  public String abort() {
    this.show = true;
    this.show2 = false;
    return null;
  }
  public String generate() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();

    log.error("idGrupoNomina " + this.idGrupoNomina);
    log.error("Desde " + this.inicio);
    log.error("Hasta " + this.fin);
    log.error("Periodicidad" + this.periodicidad);
    log.error("Recalculo" + this.recalculo);
    log.error("idorganimo " + this.login.getOrganismo().getIdOrganismo());
    GrupoNomina grupoNomina = this.definicionesFacade.findGrupoNominaById(this.idGrupoNomina);
    try {
      this.mreNoGenFacade.generarNominaMre(this.idGrupoNomina, 0, 0L, this.anio, this.mes, this.proceso);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', grupoNomina);

      context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
      this.show = false;
      this.show2 = false;
      this.show3 = false;
      this.selectGrupoNomina = null;
    } catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }

  public boolean isShow() {
    return this.show;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public boolean isShow3() {
    return this.show3;
  }

  public String getRecalculo() {
    return this.recalculo;
  }
  public void setRecalculo(String recalculo) {
    this.recalculo = recalculo;
  }
  public Date getFin() {
    return this.fin;
  }

  public void setFin(Date fin) {
    this.fin = fin;
  }

  public Date getInicio() {
    return this.inicio;
  }

  public void setInicio(Date inicio) {
    this.inicio = inicio;
  }

  public String getProceso() {
    return this.proceso;
  }

  public void setProceso(String proceso) {
    this.proceso = proceso;
  }
}