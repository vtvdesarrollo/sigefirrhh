package sigefirrhh.personal.mre;

import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;
import sigefirrhh.personal.procesoNomina.SeguridadOrdinaria;

public class ReportNominaMreForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportNominaMreForm.class.getName());

  private int tipoReporte = 1;
  private int reportId;
  private String reportName;
  private String selectGrupoNomina;
  private Date inicio;
  private Date fin;
  private long idGrupoNomina;
  private Collection listGrupoNomina;
  private long idUnidadEjecutora;
  private Collection listUnidadEjecutora;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private boolean show;
  private boolean auxShow;
  private boolean showSemana;
  private String orden = "1";
  private int anio;
  private int mes;

  public ReportNominaMreForm()
  {
    this.reportName = "conversionnominaalfuel";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.inicio = null;
    this.fin = null;
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportNominaMreForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public boolean isShowConcepto() {
    return (this.tipoReporte >= 50) && (this.tipoReporte <= 99);
  }
  public boolean isShowRecibo() {
    return this.tipoReporte >= 500;
  }
  private void cambiarNombreAReporte() {
    this.reportName = "";
    if (this.tipoReporte == 1)
      this.reportName = "conversionnomina";
    else if (this.tipoReporte == 2)
      this.reportName = "primamre";
    else if (this.tipoReporte == 3)
      this.reportName = "primaonu";
    else if (this.tipoReporte == 4) {
      this.reportName = "netopagar";
    }
    if (this.orden.equals("1"))
      this.reportName += "alf";
    else if (this.orden.equals("2"))
      this.reportName += "ced";
    else if (this.orden.equals("3")) {
      this.reportName += "cod";
    }

    this.reportName += "uel";

    if (this.idUnidadEjecutora != 0L)
      this.reportName += "1";
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    this.idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.showSemana = false;

      SeguridadOrdinaria seguridadOrdinaria = this.procesoNominaNoGenFacade.findNominaMre(this.idGrupoNomina);
      this.inicio = seguridadOrdinaria.getFechaInicio();
      this.fin = seguridadOrdinaria.getFechaFin();
      this.anio = seguridadOrdinaria.getAnio();
      this.mes = seguridadOrdinaria.getMes();
      this.auxShow = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findGrupoNominaWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadEjecutora = this.estructuraFacade.findAllUnidadEjecutora();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
      this.listUnidadEjecutora = new ArrayList();
    }
  }

  public String runReport() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      Map parameters = new Hashtable();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));

      parameters.put("id_tipo_personal", new Long(100L));
      parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));

      this.reportName = "";

      if (this.tipoReporte == 1)
        this.reportName = "conversionnomina";
      else if (this.tipoReporte == 2)
        this.reportName = "primamre";
      else if (this.tipoReporte == 3)
        this.reportName = "primaonu";
      else if (this.tipoReporte == 4) {
        this.reportName = "netopagar";
      }
      if (this.orden.equals("1"))
        this.reportName += "alf";
      else if (this.orden.equals("2"))
        this.reportName += "ced";
      else if (this.orden.equals("3")) {
        this.reportName += "cod";
      }

      this.reportName += "uel";

      if (this.idUnidadEjecutora != 0L) {
        this.reportName += "1";
        parameters.put("id_unidad_ejecutora", new Long(this.idUnidadEjecutora));
      }
      JasperForWeb report = new JasperForWeb();

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/mre");

      report.start();
      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public Collection getListGrupoNomina()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoNomina.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListUnidadEjecutora() {
    Collection col = new ArrayList();
    Iterator iterator = this.listUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadEjecutora.getIdUnidadEjecutora()), 
        unidadEjecutora.toString()));
    }
    return col;
  }
  public String getSelectGrupoNomina() {
    return this.selectGrupoNomina;
  }
  public void setSelectGrupoNomina(String string) {
    this.selectGrupoNomina = string;
  }

  public Date getFin() {
    return this.fin;
  }
  public void setFin(Date fin) {
    this.fin = fin;
  }
  public Date getInicio() {
    return this.inicio;
  }
  public void setInicio(Date inicio) {
    this.inicio = inicio;
  }
  public boolean isShow() {
    return this.auxShow;
  }
  public int getTipoReporte() {
    return this.tipoReporte;
  }
  public void setTipoReporte(int i) {
    this.tipoReporte = i;
  }

  public long getIdGrupoNomina() {
    return this.idGrupoNomina;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setIdGrupoNomina(long l) {
    this.idGrupoNomina = l;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public boolean isShowSemana() {
    return this.showSemana;
  }
  public long getIdUnidadEjecutora() {
    return this.idUnidadEjecutora;
  }
  public void setIdUnidadEjecutora(long idUnidadEjecutora) {
    this.idUnidadEjecutora = idUnidadEjecutora;
  }
  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }
}