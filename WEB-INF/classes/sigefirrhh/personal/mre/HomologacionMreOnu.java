package sigefirrhh.personal.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class HomologacionMreOnu
  implements Serializable, PersistenceCapable
{
  private long idHomologacionMreOnu;
  private double monto;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "idHomologacionMreOnu", "monto", "trabajador" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public long getIdHomologacionMreOnu()
  {
    return jdoGetidHomologacionMreOnu(this);
  }

  public double getMonto()
  {
    return jdoGetmonto(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal personal)
  {
    jdoSetconceptoTipoPersonal(this, personal);
  }

  public void setIdHomologacionMreOnu(long l)
  {
    jdoSetidHomologacionMreOnu(this, l);
  }

  public void setMonto(double d)
  {
    jdoSetmonto(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.mre.HomologacionMreOnu"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HomologacionMreOnu());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HomologacionMreOnu localHomologacionMreOnu = new HomologacionMreOnu();
    localHomologacionMreOnu.jdoFlags = 1;
    localHomologacionMreOnu.jdoStateManager = paramStateManager;
    return localHomologacionMreOnu;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HomologacionMreOnu localHomologacionMreOnu = new HomologacionMreOnu();
    localHomologacionMreOnu.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHomologacionMreOnu.jdoFlags = 1;
    localHomologacionMreOnu.jdoStateManager = paramStateManager;
    return localHomologacionMreOnu;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHomologacionMreOnu);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHomologacionMreOnu = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HomologacionMreOnu paramHomologacionMreOnu, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHomologacionMreOnu == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramHomologacionMreOnu.conceptoTipoPersonal;
      return;
    case 1:
      if (paramHomologacionMreOnu == null)
        throw new IllegalArgumentException("arg1");
      this.idHomologacionMreOnu = paramHomologacionMreOnu.idHomologacionMreOnu;
      return;
    case 2:
      if (paramHomologacionMreOnu == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramHomologacionMreOnu.monto;
      return;
    case 3:
      if (paramHomologacionMreOnu == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHomologacionMreOnu.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HomologacionMreOnu))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HomologacionMreOnu localHomologacionMreOnu = (HomologacionMreOnu)paramObject;
    if (localHomologacionMreOnu.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHomologacionMreOnu, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HomologacionMreOnuPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HomologacionMreOnuPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HomologacionMreOnuPK))
      throw new IllegalArgumentException("arg1");
    HomologacionMreOnuPK localHomologacionMreOnuPK = (HomologacionMreOnuPK)paramObject;
    localHomologacionMreOnuPK.idHomologacionMreOnu = this.idHomologacionMreOnu;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HomologacionMreOnuPK))
      throw new IllegalArgumentException("arg1");
    HomologacionMreOnuPK localHomologacionMreOnuPK = (HomologacionMreOnuPK)paramObject;
    this.idHomologacionMreOnu = localHomologacionMreOnuPK.idHomologacionMreOnu;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HomologacionMreOnuPK))
      throw new IllegalArgumentException("arg2");
    HomologacionMreOnuPK localHomologacionMreOnuPK = (HomologacionMreOnuPK)paramObject;
    localHomologacionMreOnuPK.idHomologacionMreOnu = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HomologacionMreOnuPK))
      throw new IllegalArgumentException("arg2");
    HomologacionMreOnuPK localHomologacionMreOnuPK = (HomologacionMreOnuPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localHomologacionMreOnuPK.idHomologacionMreOnu);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(HomologacionMreOnu paramHomologacionMreOnu)
  {
    StateManager localStateManager = paramHomologacionMreOnu.jdoStateManager;
    if (localStateManager == null)
      return paramHomologacionMreOnu.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramHomologacionMreOnu, jdoInheritedFieldCount + 0))
      return paramHomologacionMreOnu.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramHomologacionMreOnu, jdoInheritedFieldCount + 0, paramHomologacionMreOnu.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(HomologacionMreOnu paramHomologacionMreOnu, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramHomologacionMreOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramHomologacionMreOnu.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHomologacionMreOnu, jdoInheritedFieldCount + 0, paramHomologacionMreOnu.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidHomologacionMreOnu(HomologacionMreOnu paramHomologacionMreOnu)
  {
    return paramHomologacionMreOnu.idHomologacionMreOnu;
  }

  private static final void jdoSetidHomologacionMreOnu(HomologacionMreOnu paramHomologacionMreOnu, long paramLong)
  {
    StateManager localStateManager = paramHomologacionMreOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramHomologacionMreOnu.idHomologacionMreOnu = paramLong;
      return;
    }
    localStateManager.setLongField(paramHomologacionMreOnu, jdoInheritedFieldCount + 1, paramHomologacionMreOnu.idHomologacionMreOnu, paramLong);
  }

  private static final double jdoGetmonto(HomologacionMreOnu paramHomologacionMreOnu)
  {
    if (paramHomologacionMreOnu.jdoFlags <= 0)
      return paramHomologacionMreOnu.monto;
    StateManager localStateManager = paramHomologacionMreOnu.jdoStateManager;
    if (localStateManager == null)
      return paramHomologacionMreOnu.monto;
    if (localStateManager.isLoaded(paramHomologacionMreOnu, jdoInheritedFieldCount + 2))
      return paramHomologacionMreOnu.monto;
    return localStateManager.getDoubleField(paramHomologacionMreOnu, jdoInheritedFieldCount + 2, paramHomologacionMreOnu.monto);
  }

  private static final void jdoSetmonto(HomologacionMreOnu paramHomologacionMreOnu, double paramDouble)
  {
    if (paramHomologacionMreOnu.jdoFlags == 0)
    {
      paramHomologacionMreOnu.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramHomologacionMreOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramHomologacionMreOnu.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHomologacionMreOnu, jdoInheritedFieldCount + 2, paramHomologacionMreOnu.monto, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(HomologacionMreOnu paramHomologacionMreOnu)
  {
    StateManager localStateManager = paramHomologacionMreOnu.jdoStateManager;
    if (localStateManager == null)
      return paramHomologacionMreOnu.trabajador;
    if (localStateManager.isLoaded(paramHomologacionMreOnu, jdoInheritedFieldCount + 3))
      return paramHomologacionMreOnu.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHomologacionMreOnu, jdoInheritedFieldCount + 3, paramHomologacionMreOnu.trabajador);
  }

  private static final void jdoSettrabajador(HomologacionMreOnu paramHomologacionMreOnu, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHomologacionMreOnu.jdoStateManager;
    if (localStateManager == null)
    {
      paramHomologacionMreOnu.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHomologacionMreOnu, jdoInheritedFieldCount + 3, paramHomologacionMreOnu.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}