package sigefirrhh.personal.mre;

import java.io.Serializable;

public class NominaDiplomaticoPK
  implements Serializable
{
  public long idNominaDiplomatico;

  public NominaDiplomaticoPK()
  {
  }

  public NominaDiplomaticoPK(long idNominaDiplomatico)
  {
    this.idNominaDiplomatico = idNominaDiplomatico;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NominaDiplomaticoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NominaDiplomaticoPK thatPK)
  {
    return 
      this.idNominaDiplomatico == thatPK.idNominaDiplomatico;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNominaDiplomatico)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNominaDiplomatico);
  }
}