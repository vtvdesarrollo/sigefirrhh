package sigefirrhh.personal.mre;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class MreBusiness extends AbstractBusiness
  implements Serializable
{
  private NominaDiplomaticoBeanBusiness nominaDiplomaticoBeanBusiness = new NominaDiplomaticoBeanBusiness();

  private ConceptoDiplomaticoBeanBusiness conceptoDiplomaticoBeanBusiness = new ConceptoDiplomaticoBeanBusiness();

  public void addNominaDiplomatico(NominaDiplomatico nominaDiplomatico)
    throws Exception
  {
    this.nominaDiplomaticoBeanBusiness.addNominaDiplomatico(nominaDiplomatico);
  }

  public void updateNominaDiplomatico(NominaDiplomatico nominaDiplomatico) throws Exception {
    this.nominaDiplomaticoBeanBusiness.updateNominaDiplomatico(nominaDiplomatico);
  }

  public void deleteNominaDiplomatico(NominaDiplomatico nominaDiplomatico) throws Exception {
    this.nominaDiplomaticoBeanBusiness.deleteNominaDiplomatico(nominaDiplomatico);
  }

  public NominaDiplomatico findNominaDiplomaticoById(long nominaDiplomaticoId) throws Exception {
    return this.nominaDiplomaticoBeanBusiness.findNominaDiplomaticoById(nominaDiplomaticoId);
  }

  public Collection findAllNominaDiplomatico() throws Exception {
    return this.nominaDiplomaticoBeanBusiness.findNominaDiplomaticoAll();
  }

  public Collection findNominaDiplomaticoByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.nominaDiplomaticoBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico)
    throws Exception
  {
    this.conceptoDiplomaticoBeanBusiness.addConceptoDiplomatico(conceptoDiplomatico);
  }

  public void updateConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico) throws Exception {
    this.conceptoDiplomaticoBeanBusiness.updateConceptoDiplomatico(conceptoDiplomatico);
  }

  public void deleteConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico) throws Exception {
    this.conceptoDiplomaticoBeanBusiness.deleteConceptoDiplomatico(conceptoDiplomatico);
  }

  public ConceptoDiplomatico findConceptoDiplomaticoById(long conceptoDiplomaticoId) throws Exception {
    return this.conceptoDiplomaticoBeanBusiness.findConceptoDiplomaticoById(conceptoDiplomaticoId);
  }

  public Collection findAllConceptoDiplomatico() throws Exception {
    return this.conceptoDiplomaticoBeanBusiness.findConceptoDiplomaticoAll();
  }

  public Collection findConceptoDiplomaticoByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.conceptoDiplomaticoBeanBusiness.findByTrabajador(idTrabajador);
  }
}