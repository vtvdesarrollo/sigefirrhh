package sigefirrhh.personal.mre;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class ConceptoDiplomaticoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoDiplomatico conceptoDiplomaticoNew = 
      (ConceptoDiplomatico)BeanUtils.cloneBean(
      conceptoDiplomatico);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoDiplomaticoNew.getConceptoTipoPersonal() != null) {
      conceptoDiplomaticoNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoDiplomaticoNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoDiplomaticoNew.getTrabajador() != null) {
      conceptoDiplomaticoNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoDiplomaticoNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(conceptoDiplomaticoNew);
  }

  public void updateConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico) throws Exception
  {
    ConceptoDiplomatico conceptoDiplomaticoModify = 
      findConceptoDiplomaticoById(conceptoDiplomatico.getIdConceptoDiplomatico());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoDiplomatico.getConceptoTipoPersonal() != null) {
      conceptoDiplomatico.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoDiplomatico.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoDiplomatico.getTrabajador() != null) {
      conceptoDiplomatico.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoDiplomatico.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(conceptoDiplomaticoModify, conceptoDiplomatico);
  }

  public void deleteConceptoDiplomatico(ConceptoDiplomatico conceptoDiplomatico) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoDiplomatico conceptoDiplomaticoDelete = 
      findConceptoDiplomaticoById(conceptoDiplomatico.getIdConceptoDiplomatico());
    pm.deletePersistent(conceptoDiplomaticoDelete);
  }

  public ConceptoDiplomatico findConceptoDiplomaticoById(long idConceptoDiplomatico) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoDiplomatico == pIdConceptoDiplomatico";
    Query query = pm.newQuery(ConceptoDiplomatico.class, filter);

    query.declareParameters("long pIdConceptoDiplomatico");

    parameters.put("pIdConceptoDiplomatico", new Long(idConceptoDiplomatico));

    Collection colConceptoDiplomatico = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoDiplomatico.iterator();
    return (ConceptoDiplomatico)iterator.next();
  }

  public Collection findConceptoDiplomaticoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoDiplomaticoExtent = pm.getExtent(
      ConceptoDiplomatico.class, true);
    Query query = pm.newQuery(conceptoDiplomaticoExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoDiplomatico.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoDiplomatico = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoDiplomatico);

    return colConceptoDiplomatico;
  }
}