package sigefirrhh.personal.mre;

import java.io.Serializable;

public class SeguridadNominaMrePK
  implements Serializable
{
  public long idSeguridadNominaMre;

  public SeguridadNominaMrePK()
  {
  }

  public SeguridadNominaMrePK(long idSeguridadNominaMre)
  {
    this.idSeguridadNominaMre = idSeguridadNominaMre;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadNominaMrePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadNominaMrePK thatPK)
  {
    return 
      this.idSeguridadNominaMre == thatPK.idSeguridadNominaMre;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadNominaMre)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadNominaMre);
  }
}