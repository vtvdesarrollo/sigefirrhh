package sigefirrhh.personal.mre;

import java.io.Serializable;

public class HistoricoNominaMrePK
  implements Serializable
{
  public long idHistoricoNominaMre;

  public HistoricoNominaMrePK()
  {
  }

  public HistoricoNominaMrePK(long idHistoricoNominaMre)
  {
    this.idHistoricoNominaMre = idHistoricoNominaMre;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoNominaMrePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoNominaMrePK thatPK)
  {
    return 
      this.idHistoricoNominaMre == thatPK.idHistoricoNominaMre;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoNominaMre)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoNominaMre);
  }
}