package sigefirrhh.personal.mre;

import java.io.Serializable;

public class HomologacionMreOnuPK
  implements Serializable
{
  public long idHomologacionMreOnu;

  public HomologacionMreOnuPK()
  {
  }

  public HomologacionMreOnuPK(long idHomologacionMreOnu)
  {
    this.idHomologacionMreOnu = idHomologacionMreOnu;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HomologacionMreOnuPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HomologacionMreOnuPK thatPK)
  {
    return 
      this.idHomologacionMreOnu == thatPK.idHomologacionMreOnu;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHomologacionMreOnu)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHomologacionMreOnu);
  }
}