package sigefirrhh.personal.mre;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.sistema.Usuario;

public class SeguridadNominaMre
  implements Serializable, PersistenceCapable
{
  private long idSeguridadNominaMre;
  private int anio;
  private int mes;
  private int semanaQuincena;
  private Date fechaUltima;
  private Date fechaProceso;
  private Usuario usuario;
  private GrupoNomina grupoNomina;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "fechaProceso", "fechaUltima", "grupoNomina", "idSeguridadNominaMre", "mes", "semanaQuincena", "usuario" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.GrupoNomina"), Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.sistema.Usuario") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public Date getFechaUltima()
  {
    return jdoGetfechaUltima(this);
  }

  public GrupoNomina getGrupoNomina()
  {
    return jdoGetgrupoNomina(this);
  }

  public long getIdSeguridadNominaMre()
  {
    return jdoGetidSeguridadNominaMre(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public int getSemanaQuincena()
  {
    return jdoGetsemanaQuincena(this);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setFechaProceso(Date date)
  {
    jdoSetfechaProceso(this, date);
  }

  public void setFechaUltima(Date date)
  {
    jdoSetfechaUltima(this, date);
  }

  public void setGrupoNomina(GrupoNomina nomina)
  {
    jdoSetgrupoNomina(this, nomina);
  }

  public void setIdSeguridadNominaMre(long l)
  {
    jdoSetidSeguridadNominaMre(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setSemanaQuincena(int i)
  {
    jdoSetsemanaQuincena(this, i);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.mre.SeguridadNominaMre"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadNominaMre());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadNominaMre localSeguridadNominaMre = new SeguridadNominaMre();
    localSeguridadNominaMre.jdoFlags = 1;
    localSeguridadNominaMre.jdoStateManager = paramStateManager;
    return localSeguridadNominaMre;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadNominaMre localSeguridadNominaMre = new SeguridadNominaMre();
    localSeguridadNominaMre.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadNominaMre.jdoFlags = 1;
    localSeguridadNominaMre.jdoStateManager = paramStateManager;
    return localSeguridadNominaMre;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaUltima);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoNomina);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadNominaMre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.semanaQuincena);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaUltima = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoNomina = ((GrupoNomina)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadNominaMre = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.semanaQuincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadNominaMre paramSeguridadNominaMre, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadNominaMre.anio;
      return;
    case 1:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadNominaMre.fechaProceso;
      return;
    case 2:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.fechaUltima = paramSeguridadNominaMre.fechaUltima;
      return;
    case 3:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.grupoNomina = paramSeguridadNominaMre.grupoNomina;
      return;
    case 4:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadNominaMre = paramSeguridadNominaMre.idSeguridadNominaMre;
      return;
    case 5:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadNominaMre.mes;
      return;
    case 6:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.semanaQuincena = paramSeguridadNominaMre.semanaQuincena;
      return;
    case 7:
      if (paramSeguridadNominaMre == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadNominaMre.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadNominaMre))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadNominaMre localSeguridadNominaMre = (SeguridadNominaMre)paramObject;
    if (localSeguridadNominaMre.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadNominaMre, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadNominaMrePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadNominaMrePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadNominaMrePK))
      throw new IllegalArgumentException("arg1");
    SeguridadNominaMrePK localSeguridadNominaMrePK = (SeguridadNominaMrePK)paramObject;
    localSeguridadNominaMrePK.idSeguridadNominaMre = this.idSeguridadNominaMre;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadNominaMrePK))
      throw new IllegalArgumentException("arg1");
    SeguridadNominaMrePK localSeguridadNominaMrePK = (SeguridadNominaMrePK)paramObject;
    this.idSeguridadNominaMre = localSeguridadNominaMrePK.idSeguridadNominaMre;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadNominaMrePK))
      throw new IllegalArgumentException("arg2");
    SeguridadNominaMrePK localSeguridadNominaMrePK = (SeguridadNominaMrePK)paramObject;
    localSeguridadNominaMrePK.idSeguridadNominaMre = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadNominaMrePK))
      throw new IllegalArgumentException("arg2");
    SeguridadNominaMrePK localSeguridadNominaMrePK = (SeguridadNominaMrePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localSeguridadNominaMrePK.idSeguridadNominaMre);
  }

  private static final int jdoGetanio(SeguridadNominaMre paramSeguridadNominaMre)
  {
    if (paramSeguridadNominaMre.jdoFlags <= 0)
      return paramSeguridadNominaMre.anio;
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadNominaMre.anio;
    if (localStateManager.isLoaded(paramSeguridadNominaMre, jdoInheritedFieldCount + 0))
      return paramSeguridadNominaMre.anio;
    return localStateManager.getIntField(paramSeguridadNominaMre, jdoInheritedFieldCount + 0, paramSeguridadNominaMre.anio);
  }

  private static final void jdoSetanio(SeguridadNominaMre paramSeguridadNominaMre, int paramInt)
  {
    if (paramSeguridadNominaMre.jdoFlags == 0)
    {
      paramSeguridadNominaMre.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadNominaMre, jdoInheritedFieldCount + 0, paramSeguridadNominaMre.anio, paramInt);
  }

  private static final Date jdoGetfechaProceso(SeguridadNominaMre paramSeguridadNominaMre)
  {
    if (paramSeguridadNominaMre.jdoFlags <= 0)
      return paramSeguridadNominaMre.fechaProceso;
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadNominaMre.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadNominaMre, jdoInheritedFieldCount + 1))
      return paramSeguridadNominaMre.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 1, paramSeguridadNominaMre.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadNominaMre paramSeguridadNominaMre, Date paramDate)
  {
    if (paramSeguridadNominaMre.jdoFlags == 0)
    {
      paramSeguridadNominaMre.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 1, paramSeguridadNominaMre.fechaProceso, paramDate);
  }

  private static final Date jdoGetfechaUltima(SeguridadNominaMre paramSeguridadNominaMre)
  {
    if (paramSeguridadNominaMre.jdoFlags <= 0)
      return paramSeguridadNominaMre.fechaUltima;
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadNominaMre.fechaUltima;
    if (localStateManager.isLoaded(paramSeguridadNominaMre, jdoInheritedFieldCount + 2))
      return paramSeguridadNominaMre.fechaUltima;
    return (Date)localStateManager.getObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 2, paramSeguridadNominaMre.fechaUltima);
  }

  private static final void jdoSetfechaUltima(SeguridadNominaMre paramSeguridadNominaMre, Date paramDate)
  {
    if (paramSeguridadNominaMre.jdoFlags == 0)
    {
      paramSeguridadNominaMre.fechaUltima = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.fechaUltima = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 2, paramSeguridadNominaMre.fechaUltima, paramDate);
  }

  private static final GrupoNomina jdoGetgrupoNomina(SeguridadNominaMre paramSeguridadNominaMre)
  {
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadNominaMre.grupoNomina;
    if (localStateManager.isLoaded(paramSeguridadNominaMre, jdoInheritedFieldCount + 3))
      return paramSeguridadNominaMre.grupoNomina;
    return (GrupoNomina)localStateManager.getObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 3, paramSeguridadNominaMre.grupoNomina);
  }

  private static final void jdoSetgrupoNomina(SeguridadNominaMre paramSeguridadNominaMre, GrupoNomina paramGrupoNomina)
  {
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.grupoNomina = paramGrupoNomina;
      return;
    }
    localStateManager.setObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 3, paramSeguridadNominaMre.grupoNomina, paramGrupoNomina);
  }

  private static final long jdoGetidSeguridadNominaMre(SeguridadNominaMre paramSeguridadNominaMre)
  {
    return paramSeguridadNominaMre.idSeguridadNominaMre;
  }

  private static final void jdoSetidSeguridadNominaMre(SeguridadNominaMre paramSeguridadNominaMre, long paramLong)
  {
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.idSeguridadNominaMre = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadNominaMre, jdoInheritedFieldCount + 4, paramSeguridadNominaMre.idSeguridadNominaMre, paramLong);
  }

  private static final int jdoGetmes(SeguridadNominaMre paramSeguridadNominaMre)
  {
    if (paramSeguridadNominaMre.jdoFlags <= 0)
      return paramSeguridadNominaMre.mes;
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadNominaMre.mes;
    if (localStateManager.isLoaded(paramSeguridadNominaMre, jdoInheritedFieldCount + 5))
      return paramSeguridadNominaMre.mes;
    return localStateManager.getIntField(paramSeguridadNominaMre, jdoInheritedFieldCount + 5, paramSeguridadNominaMre.mes);
  }

  private static final void jdoSetmes(SeguridadNominaMre paramSeguridadNominaMre, int paramInt)
  {
    if (paramSeguridadNominaMre.jdoFlags == 0)
    {
      paramSeguridadNominaMre.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadNominaMre, jdoInheritedFieldCount + 5, paramSeguridadNominaMre.mes, paramInt);
  }

  private static final int jdoGetsemanaQuincena(SeguridadNominaMre paramSeguridadNominaMre)
  {
    if (paramSeguridadNominaMre.jdoFlags <= 0)
      return paramSeguridadNominaMre.semanaQuincena;
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadNominaMre.semanaQuincena;
    if (localStateManager.isLoaded(paramSeguridadNominaMre, jdoInheritedFieldCount + 6))
      return paramSeguridadNominaMre.semanaQuincena;
    return localStateManager.getIntField(paramSeguridadNominaMre, jdoInheritedFieldCount + 6, paramSeguridadNominaMre.semanaQuincena);
  }

  private static final void jdoSetsemanaQuincena(SeguridadNominaMre paramSeguridadNominaMre, int paramInt)
  {
    if (paramSeguridadNominaMre.jdoFlags == 0)
    {
      paramSeguridadNominaMre.semanaQuincena = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.semanaQuincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadNominaMre, jdoInheritedFieldCount + 6, paramSeguridadNominaMre.semanaQuincena, paramInt);
  }

  private static final Usuario jdoGetusuario(SeguridadNominaMre paramSeguridadNominaMre)
  {
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadNominaMre.usuario;
    if (localStateManager.isLoaded(paramSeguridadNominaMre, jdoInheritedFieldCount + 7))
      return paramSeguridadNominaMre.usuario;
    return (Usuario)localStateManager.getObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 7, paramSeguridadNominaMre.usuario);
  }

  private static final void jdoSetusuario(SeguridadNominaMre paramSeguridadNominaMre, Usuario paramUsuario)
  {
    StateManager localStateManager = paramSeguridadNominaMre.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadNominaMre.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramSeguridadNominaMre, jdoInheritedFieldCount + 7, paramSeguridadNominaMre.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}