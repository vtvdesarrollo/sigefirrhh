package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportCategoriaPresupuestariaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportCategoriaPresupuestariaForm.class.getName());
  private int reportId;
  private String formato = "1";
  private String reportName;
  private String orden = "C";
  private int anio;
  private LoginSession login;
  private SigecofNoGenFacade sigecofFacade;
  private Collection listAnio = new ArrayList();

  public ReportCategoriaPresupuestariaForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.sigecofFacade = new SigecofNoGenFacade();

    this.reportName = "categoriapresupuestaria";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.anio = (new Date().getYear() + 1900);

    int anio = 1970;
    int cantidadSumar = this.anio - anio;

    for (int cantidad = 0; cantidadSumar == cantidad; cantidad++) {
      this.listAnio.add(Long.valueOf(String.valueOf(anio)));
      this.listAnio.add(String.valueOf(anio));
      anio++;
    }

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportCategoriaPresupuestariaForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
  }

  public void cambiarNombreAReporte()
  {
    this.reportName = null;
    if (this.orden != null) {
      this.reportName = "";
      if (this.orden.equals("C"))
        this.reportName = "categoriapresupuestaria";
      else {
        this.reportName = "categoriapresupuestariauel";
      }

      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = null;
      if (this.orden != null) {
        this.reportName = "";
        if (this.orden.equals("C"))
          this.reportName = "categoriapresupuestaria";
        else {
          this.reportName = "categoriapresupuestariauel";
        }

        if (this.formato.equals("2")) {
          this.reportName = ("a_" + this.reportName);
        }

      }

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));

      parameters.put("id_organismo", new Long(this.login.getIdOrganismo()));
      parameters.put("anio", new Integer(this.anio));

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/sigecof");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListAnio()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listAnio.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setOrden(String string)
  {
    this.orden = string;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public int getAnio()
  {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
}