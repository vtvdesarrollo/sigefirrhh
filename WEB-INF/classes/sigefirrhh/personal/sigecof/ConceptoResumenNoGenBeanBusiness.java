package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoResumenNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByTrabajadorAnioMes(long idTrabajador, int anio, int mes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && anio == pAnio && mes == pMes";

    Query query = pm.newQuery(ConceptoResumen.class, filter);

    query.declareParameters("long pIdTrabajador, int pAnio, int pMes");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pAnio", new Integer(anio));
    parameters.put("pMes", new Integer(mes));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoResumen = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoResumen);

    return colConceptoResumen;
  }
}