package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class FuenteFinanciamientoPK
  implements Serializable
{
  public long idFuenteFinanciamiento;

  public FuenteFinanciamientoPK()
  {
  }

  public FuenteFinanciamientoPK(long idFuenteFinanciamiento)
  {
    this.idFuenteFinanciamiento = idFuenteFinanciamiento;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((FuenteFinanciamientoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(FuenteFinanciamientoPK thatPK)
  {
    return 
      this.idFuenteFinanciamiento == thatPK.idFuenteFinanciamiento;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idFuenteFinanciamiento)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idFuenteFinanciamiento);
  }
}