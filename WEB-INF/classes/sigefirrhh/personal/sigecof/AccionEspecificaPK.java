package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class AccionEspecificaPK
  implements Serializable
{
  public long idAccionEspecifica;

  public AccionEspecificaPK()
  {
  }

  public AccionEspecificaPK(long idAccionEspecifica)
  {
    this.idAccionEspecifica = idAccionEspecifica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AccionEspecificaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AccionEspecificaPK thatPK)
  {
    return 
      this.idAccionEspecifica == thatPK.idAccionEspecifica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAccionEspecifica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAccionEspecifica);
  }
}