package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.CategoriaPresupuesto;
import sigefirrhh.base.definiciones.CategoriaPresupuestoBeanBusiness;

public class SeguridadPresupuestoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadPresupuesto seguridadPresupuestoNew = 
      (SeguridadPresupuesto)BeanUtils.cloneBean(
      seguridadPresupuesto);

    CategoriaPresupuestoBeanBusiness categoriaPresupuestoBeanBusiness = new CategoriaPresupuestoBeanBusiness();

    if (seguridadPresupuestoNew.getCategoriaPresupuesto() != null) {
      seguridadPresupuestoNew.setCategoriaPresupuesto(
        categoriaPresupuestoBeanBusiness.findCategoriaPresupuestoById(
        seguridadPresupuestoNew.getCategoriaPresupuesto().getIdCategoriaPresupuesto()));
    }
    pm.makePersistent(seguridadPresupuestoNew);
  }

  public void updateSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto) throws Exception
  {
    SeguridadPresupuesto seguridadPresupuestoModify = 
      findSeguridadPresupuestoById(seguridadPresupuesto.getIdSeguridadPresupuesto());

    CategoriaPresupuestoBeanBusiness categoriaPresupuestoBeanBusiness = new CategoriaPresupuestoBeanBusiness();

    if (seguridadPresupuesto.getCategoriaPresupuesto() != null) {
      seguridadPresupuesto.setCategoriaPresupuesto(
        categoriaPresupuestoBeanBusiness.findCategoriaPresupuestoById(
        seguridadPresupuesto.getCategoriaPresupuesto().getIdCategoriaPresupuesto()));
    }

    BeanUtils.copyProperties(seguridadPresupuestoModify, seguridadPresupuesto);
  }

  public void deleteSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadPresupuesto seguridadPresupuestoDelete = 
      findSeguridadPresupuestoById(seguridadPresupuesto.getIdSeguridadPresupuesto());
    pm.deletePersistent(seguridadPresupuestoDelete);
  }

  public SeguridadPresupuesto findSeguridadPresupuestoById(long idSeguridadPresupuesto) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadPresupuesto == pIdSeguridadPresupuesto";
    Query query = pm.newQuery(SeguridadPresupuesto.class, filter);

    query.declareParameters("long pIdSeguridadPresupuesto");

    parameters.put("pIdSeguridadPresupuesto", new Long(idSeguridadPresupuesto));

    Collection colSeguridadPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadPresupuesto.iterator();
    return (SeguridadPresupuesto)iterator.next();
  }

  public Collection findSeguridadPresupuestoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadPresupuestoExtent = pm.getExtent(
      SeguridadPresupuesto.class, true);
    Query query = pm.newQuery(seguridadPresupuestoExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCategoriaPresupuesto(long idCategoriaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "categoriaPresupuesto.idCategoriaPresupuesto == pIdCategoriaPresupuesto";

    Query query = pm.newQuery(SeguridadPresupuesto.class, filter);

    query.declareParameters("long pIdCategoriaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pIdCategoriaPresupuesto", new Long(idCategoriaPresupuesto));

    query.setOrdering("anio ascending, mes ascending");

    Collection colSeguridadPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadPresupuesto);

    return colSeguridadPresupuesto;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(SeguridadPresupuesto.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, mes ascending");

    Collection colSeguridadPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadPresupuesto);

    return colSeguridadPresupuesto;
  }
}