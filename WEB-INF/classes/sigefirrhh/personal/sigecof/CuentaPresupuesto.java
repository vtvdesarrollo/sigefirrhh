package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class CuentaPresupuesto
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idCuentaPresupuesto;
  private String codPresupuesto;
  private String descripcion;
  private String tipo;
  private int indicador;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codPresupuesto", "descripcion", "idCuentaPresupuesto", "indicador", "organismo", "tipo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaPresupuesto"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CuentaPresupuesto());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("D", "DETALLE");
    LISTA_TIPO.put("T", "TITULO");
  }

  public CuentaPresupuesto()
  {
    jdoSettipo(this, "D");

    jdoSetindicador(this, 0);
  }

  public String toString()
  {
    return jdoGetcodPresupuesto(this) + " " + 
      jdoGetdescripcion(this);
  }

  public String getCodPresupuesto()
  {
    return jdoGetcodPresupuesto(this);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public long getIdCuentaPresupuesto()
  {
    return jdoGetidCuentaPresupuesto(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public String getTipo()
  {
    return jdoGettipo(this);
  }

  public void setCodPresupuesto(String string)
  {
    jdoSetcodPresupuesto(this, string);
  }

  public void setDescripcion(String string)
  {
    jdoSetdescripcion(this, string);
  }

  public void setIdCuentaPresupuesto(long l)
  {
    jdoSetidCuentaPresupuesto(this, l);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setTipo(String string)
  {
    jdoSettipo(this, string);
  }

  public int getIndicador()
  {
    return jdoGetindicador(this);
  }

  public void setIndicador(int indicador)
  {
    jdoSetindicador(this, indicador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CuentaPresupuesto localCuentaPresupuesto = new CuentaPresupuesto();
    localCuentaPresupuesto.jdoFlags = 1;
    localCuentaPresupuesto.jdoStateManager = paramStateManager;
    return localCuentaPresupuesto;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CuentaPresupuesto localCuentaPresupuesto = new CuentaPresupuesto();
    localCuentaPresupuesto.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCuentaPresupuesto.jdoFlags = 1;
    localCuentaPresupuesto.jdoStateManager = paramStateManager;
    return localCuentaPresupuesto;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPresupuesto);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCuentaPresupuesto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.indicador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPresupuesto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCuentaPresupuesto = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.indicador = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CuentaPresupuesto paramCuentaPresupuesto, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCuentaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.codPresupuesto = paramCuentaPresupuesto.codPresupuesto;
      return;
    case 1:
      if (paramCuentaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramCuentaPresupuesto.descripcion;
      return;
    case 2:
      if (paramCuentaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.idCuentaPresupuesto = paramCuentaPresupuesto.idCuentaPresupuesto;
      return;
    case 3:
      if (paramCuentaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.indicador = paramCuentaPresupuesto.indicador;
      return;
    case 4:
      if (paramCuentaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramCuentaPresupuesto.organismo;
      return;
    case 5:
      if (paramCuentaPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramCuentaPresupuesto.tipo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CuentaPresupuesto))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CuentaPresupuesto localCuentaPresupuesto = (CuentaPresupuesto)paramObject;
    if (localCuentaPresupuesto.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCuentaPresupuesto, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CuentaPresupuestoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CuentaPresupuestoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CuentaPresupuestoPK))
      throw new IllegalArgumentException("arg1");
    CuentaPresupuestoPK localCuentaPresupuestoPK = (CuentaPresupuestoPK)paramObject;
    localCuentaPresupuestoPK.idCuentaPresupuesto = this.idCuentaPresupuesto;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CuentaPresupuestoPK))
      throw new IllegalArgumentException("arg1");
    CuentaPresupuestoPK localCuentaPresupuestoPK = (CuentaPresupuestoPK)paramObject;
    this.idCuentaPresupuesto = localCuentaPresupuestoPK.idCuentaPresupuesto;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CuentaPresupuestoPK))
      throw new IllegalArgumentException("arg2");
    CuentaPresupuestoPK localCuentaPresupuestoPK = (CuentaPresupuestoPK)paramObject;
    localCuentaPresupuestoPK.idCuentaPresupuesto = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CuentaPresupuestoPK))
      throw new IllegalArgumentException("arg2");
    CuentaPresupuestoPK localCuentaPresupuestoPK = (CuentaPresupuestoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCuentaPresupuestoPK.idCuentaPresupuesto);
  }

  private static final String jdoGetcodPresupuesto(CuentaPresupuesto paramCuentaPresupuesto)
  {
    if (paramCuentaPresupuesto.jdoFlags <= 0)
      return paramCuentaPresupuesto.codPresupuesto;
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaPresupuesto.codPresupuesto;
    if (localStateManager.isLoaded(paramCuentaPresupuesto, jdoInheritedFieldCount + 0))
      return paramCuentaPresupuesto.codPresupuesto;
    return localStateManager.getStringField(paramCuentaPresupuesto, jdoInheritedFieldCount + 0, paramCuentaPresupuesto.codPresupuesto);
  }

  private static final void jdoSetcodPresupuesto(CuentaPresupuesto paramCuentaPresupuesto, String paramString)
  {
    if (paramCuentaPresupuesto.jdoFlags == 0)
    {
      paramCuentaPresupuesto.codPresupuesto = paramString;
      return;
    }
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaPresupuesto.codPresupuesto = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaPresupuesto, jdoInheritedFieldCount + 0, paramCuentaPresupuesto.codPresupuesto, paramString);
  }

  private static final String jdoGetdescripcion(CuentaPresupuesto paramCuentaPresupuesto)
  {
    if (paramCuentaPresupuesto.jdoFlags <= 0)
      return paramCuentaPresupuesto.descripcion;
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaPresupuesto.descripcion;
    if (localStateManager.isLoaded(paramCuentaPresupuesto, jdoInheritedFieldCount + 1))
      return paramCuentaPresupuesto.descripcion;
    return localStateManager.getStringField(paramCuentaPresupuesto, jdoInheritedFieldCount + 1, paramCuentaPresupuesto.descripcion);
  }

  private static final void jdoSetdescripcion(CuentaPresupuesto paramCuentaPresupuesto, String paramString)
  {
    if (paramCuentaPresupuesto.jdoFlags == 0)
    {
      paramCuentaPresupuesto.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaPresupuesto.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaPresupuesto, jdoInheritedFieldCount + 1, paramCuentaPresupuesto.descripcion, paramString);
  }

  private static final long jdoGetidCuentaPresupuesto(CuentaPresupuesto paramCuentaPresupuesto)
  {
    return paramCuentaPresupuesto.idCuentaPresupuesto;
  }

  private static final void jdoSetidCuentaPresupuesto(CuentaPresupuesto paramCuentaPresupuesto, long paramLong)
  {
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaPresupuesto.idCuentaPresupuesto = paramLong;
      return;
    }
    localStateManager.setLongField(paramCuentaPresupuesto, jdoInheritedFieldCount + 2, paramCuentaPresupuesto.idCuentaPresupuesto, paramLong);
  }

  private static final int jdoGetindicador(CuentaPresupuesto paramCuentaPresupuesto)
  {
    if (paramCuentaPresupuesto.jdoFlags <= 0)
      return paramCuentaPresupuesto.indicador;
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaPresupuesto.indicador;
    if (localStateManager.isLoaded(paramCuentaPresupuesto, jdoInheritedFieldCount + 3))
      return paramCuentaPresupuesto.indicador;
    return localStateManager.getIntField(paramCuentaPresupuesto, jdoInheritedFieldCount + 3, paramCuentaPresupuesto.indicador);
  }

  private static final void jdoSetindicador(CuentaPresupuesto paramCuentaPresupuesto, int paramInt)
  {
    if (paramCuentaPresupuesto.jdoFlags == 0)
    {
      paramCuentaPresupuesto.indicador = paramInt;
      return;
    }
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaPresupuesto.indicador = paramInt;
      return;
    }
    localStateManager.setIntField(paramCuentaPresupuesto, jdoInheritedFieldCount + 3, paramCuentaPresupuesto.indicador, paramInt);
  }

  private static final Organismo jdoGetorganismo(CuentaPresupuesto paramCuentaPresupuesto)
  {
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaPresupuesto.organismo;
    if (localStateManager.isLoaded(paramCuentaPresupuesto, jdoInheritedFieldCount + 4))
      return paramCuentaPresupuesto.organismo;
    return (Organismo)localStateManager.getObjectField(paramCuentaPresupuesto, jdoInheritedFieldCount + 4, paramCuentaPresupuesto.organismo);
  }

  private static final void jdoSetorganismo(CuentaPresupuesto paramCuentaPresupuesto, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaPresupuesto.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramCuentaPresupuesto, jdoInheritedFieldCount + 4, paramCuentaPresupuesto.organismo, paramOrganismo);
  }

  private static final String jdoGettipo(CuentaPresupuesto paramCuentaPresupuesto)
  {
    if (paramCuentaPresupuesto.jdoFlags <= 0)
      return paramCuentaPresupuesto.tipo;
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaPresupuesto.tipo;
    if (localStateManager.isLoaded(paramCuentaPresupuesto, jdoInheritedFieldCount + 5))
      return paramCuentaPresupuesto.tipo;
    return localStateManager.getStringField(paramCuentaPresupuesto, jdoInheritedFieldCount + 5, paramCuentaPresupuesto.tipo);
  }

  private static final void jdoSettipo(CuentaPresupuesto paramCuentaPresupuesto, String paramString)
  {
    if (paramCuentaPresupuesto.jdoFlags == 0)
    {
      paramCuentaPresupuesto.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramCuentaPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaPresupuesto.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaPresupuesto, jdoInheritedFieldCount + 5, paramCuentaPresupuesto.tipo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}