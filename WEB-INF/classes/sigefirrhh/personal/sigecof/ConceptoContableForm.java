package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ConceptoContableForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoContableForm.class.getName());
  private ConceptoContable conceptoContable;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showConceptoContableByConceptoTipoPersonal;
  private boolean showConceptoContableByCuentaContable;
  private String findSelectTipoPersonalForConceptoTipoPersonal;
  private String findSelectConceptoTipoPersonal;
  private String findSelectCuentaContable;
  private Collection findColTipoPersonalForConceptoTipoPersonal;
  private Collection findColConceptoTipoPersonal;
  private Collection findColCuentaContable;
  private Collection colTipoPersonalForConceptoTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colCuentaContable;
  private String selectTipoPersonalForConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectCuentaContable;
  private Object stateResultConceptoContableByConceptoTipoPersonal = null;

  private Object stateResultConceptoContableByCuentaContable = null;

  public Collection getFindColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonalForConceptoTipoPersonal() {
    return this.findSelectTipoPersonalForConceptoTipoPersonal;
  }
  public void setFindSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.findSelectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void findChangeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L)
        this.findColConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowTipoPersonalForConceptoTipoPersonal() { return this.findColTipoPersonalForConceptoTipoPersonal != null; }

  public String getFindSelectConceptoTipoPersonal() {
    return this.findSelectConceptoTipoPersonal;
  }
  public void setFindSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    this.findSelectConceptoTipoPersonal = valConceptoTipoPersonal;
  }

  public Collection getFindColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }
  public boolean isFindShowConceptoTipoPersonal() {
    return this.findColConceptoTipoPersonal != null;
  }
  public String getFindSelectCuentaContable() {
    return this.findSelectCuentaContable;
  }
  public void setFindSelectCuentaContable(String valCuentaContable) {
    this.findSelectCuentaContable = valCuentaContable;
  }

  public Collection getFindColCuentaContable() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCuentaContable.iterator();
    CuentaContable cuentaContable = null;
    while (iterator.hasNext()) {
      cuentaContable = (CuentaContable)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cuentaContable.getIdCuentaContable()), 
        cuentaContable.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonalForConceptoTipoPersonal()
  {
    return this.selectTipoPersonalForConceptoTipoPersonal;
  }
  public void setSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.selectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void changeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
      } else {
        this.selectConceptoTipoPersonal = null;
        this.conceptoContable.setConceptoTipoPersonal(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConceptoTipoPersonal = null;
      this.conceptoContable.setConceptoTipoPersonal(
        null);
    }
  }

  public boolean isShowTipoPersonalForConceptoTipoPersonal() { return this.colTipoPersonalForConceptoTipoPersonal != null; }

  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoContable.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoContable.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public String getSelectCuentaContable() {
    return this.selectCuentaContable;
  }
  public void setSelectCuentaContable(String valCuentaContable) {
    Iterator iterator = this.colCuentaContable.iterator();
    CuentaContable cuentaContable = null;
    this.conceptoContable.setCuentaContable(null);
    while (iterator.hasNext()) {
      cuentaContable = (CuentaContable)iterator.next();
      if (String.valueOf(cuentaContable.getIdCuentaContable()).equals(
        valCuentaContable)) {
        this.conceptoContable.setCuentaContable(
          cuentaContable);
        break;
      }
    }
    this.selectCuentaContable = valCuentaContable;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoContable getConceptoContable() {
    if (this.conceptoContable == null) {
      this.conceptoContable = new ConceptoContable();
    }
    return this.conceptoContable;
  }

  public ConceptoContableForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColCuentaContable()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCuentaContable.iterator();
    CuentaContable cuentaContable = null;
    while (iterator.hasNext()) {
      cuentaContable = (CuentaContable)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cuentaContable.getIdCuentaContable()), 
        cuentaContable.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColCuentaContable = 
        this.sigecofFacade.findCuentaContableByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colCuentaContable = 
        this.sigecofFacade.findCuentaContableByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoContableByConceptoTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findConceptoContableByConceptoTipoPersonal(Long.valueOf(this.findSelectConceptoTipoPersonal).longValue());
      this.showConceptoContableByConceptoTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoContableByConceptoTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConceptoTipoPersonal = null;
    this.findSelectConceptoTipoPersonal = null;
    this.findSelectCuentaContable = null;

    return null;
  }

  public String findConceptoContableByCuentaContable()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findConceptoContableByCuentaContable(Long.valueOf(this.findSelectCuentaContable).longValue());
      this.showConceptoContableByCuentaContable = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoContableByCuentaContable)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConceptoTipoPersonal = null;
    this.findSelectConceptoTipoPersonal = null;
    this.findSelectCuentaContable = null;

    return null;
  }

  public boolean isShowConceptoContableByConceptoTipoPersonal() {
    return this.showConceptoContableByConceptoTipoPersonal;
  }
  public boolean isShowConceptoContableByCuentaContable() {
    return this.showConceptoContableByCuentaContable;
  }

  public String selectConceptoContable()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;
    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectCuentaContable = null;

    long idConceptoContable = 
      Long.parseLong((String)requestParameterMap.get("idConceptoContable"));
    try
    {
      this.conceptoContable = 
        this.sigecofFacade.findConceptoContableById(
        idConceptoContable);
      if (this.conceptoContable.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoContable.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.conceptoContable.getCuentaContable() != null) {
        this.selectCuentaContable = 
          String.valueOf(this.conceptoContable.getCuentaContable().getIdCuentaContable());
      }

      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;

      if (this.conceptoContable.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.conceptoContable.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.conceptoContable.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoTipoPersonal = String.valueOf(idTipoPersonalForConceptoTipoPersonal);
        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
        this.colTipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoContable = null;
    this.showConceptoContableByConceptoTipoPersonal = false;
    this.showConceptoContableByCuentaContable = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addConceptoContable(
          this.conceptoContable);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateConceptoContable(
          this.conceptoContable);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteConceptoContable(
        this.conceptoContable);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoContable = new ConceptoContable();

    this.selectConceptoTipoPersonal = null;

    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectCuentaContable = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoContable.setIdConceptoContable(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.ConceptoContable"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoContable = new ConceptoContable();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}