package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PartidaUelEspecificaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PartidaUelEspecifica partidaUelEspecificaNew = 
      (PartidaUelEspecifica)BeanUtils.cloneBean(
      partidaUelEspecifica);

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (partidaUelEspecificaNew.getUelEspecifica() != null) {
      partidaUelEspecificaNew.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        partidaUelEspecificaNew.getUelEspecifica().getIdUelEspecifica()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (partidaUelEspecificaNew.getCuentaPresupuesto() != null) {
      partidaUelEspecificaNew.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        partidaUelEspecificaNew.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }

    FuenteFinanciamientoBeanBusiness fuenteFinanciamientoBeanBusiness = new FuenteFinanciamientoBeanBusiness();

    if (partidaUelEspecificaNew.getFuenteFinanciamiento() != null) {
      partidaUelEspecificaNew.setFuenteFinanciamiento(
        fuenteFinanciamientoBeanBusiness.findFuenteFinanciamientoById(
        partidaUelEspecificaNew.getFuenteFinanciamiento().getIdFuenteFinanciamiento()));
    }
    pm.makePersistent(partidaUelEspecificaNew);
  }

  public void updatePartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica) throws Exception
  {
    PartidaUelEspecifica partidaUelEspecificaModify = 
      findPartidaUelEspecificaById(partidaUelEspecifica.getIdPartidaUelEspecifica());

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (partidaUelEspecifica.getUelEspecifica() != null) {
      partidaUelEspecifica.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        partidaUelEspecifica.getUelEspecifica().getIdUelEspecifica()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (partidaUelEspecifica.getCuentaPresupuesto() != null) {
      partidaUelEspecifica.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        partidaUelEspecifica.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }

    FuenteFinanciamientoBeanBusiness fuenteFinanciamientoBeanBusiness = new FuenteFinanciamientoBeanBusiness();

    if (partidaUelEspecifica.getFuenteFinanciamiento() != null) {
      partidaUelEspecifica.setFuenteFinanciamiento(
        fuenteFinanciamientoBeanBusiness.findFuenteFinanciamientoById(
        partidaUelEspecifica.getFuenteFinanciamiento().getIdFuenteFinanciamiento()));
    }

    BeanUtils.copyProperties(partidaUelEspecificaModify, partidaUelEspecifica);
  }

  public void deletePartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PartidaUelEspecifica partidaUelEspecificaDelete = 
      findPartidaUelEspecificaById(partidaUelEspecifica.getIdPartidaUelEspecifica());
    pm.deletePersistent(partidaUelEspecificaDelete);
  }

  public PartidaUelEspecifica findPartidaUelEspecificaById(long idPartidaUelEspecifica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPartidaUelEspecifica == pIdPartidaUelEspecifica";
    Query query = pm.newQuery(PartidaUelEspecifica.class, filter);

    query.declareParameters("long pIdPartidaUelEspecifica");

    parameters.put("pIdPartidaUelEspecifica", new Long(idPartidaUelEspecifica));

    Collection colPartidaUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPartidaUelEspecifica.iterator();
    return (PartidaUelEspecifica)iterator.next();
  }

  public Collection findPartidaUelEspecificaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent partidaUelEspecificaExtent = pm.getExtent(
      PartidaUelEspecifica.class, true);
    Query query = pm.newQuery(partidaUelEspecificaExtent);
    query.setOrdering("cuentaPresupuesto.codPresupuesto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "uelEspecifica.idUelEspecifica == pIdUelEspecifica";

    Query query = pm.newQuery(PartidaUelEspecifica.class, filter);

    query.declareParameters("long pIdUelEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));

    query.setOrdering("cuentaPresupuesto.codPresupuesto ascending");

    Collection colPartidaUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPartidaUelEspecifica);

    return colPartidaUelEspecifica;
  }

  public Collection findByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cuentaPresupuesto.idCuentaPresupuesto == pIdCuentaPresupuesto";

    Query query = pm.newQuery(PartidaUelEspecifica.class, filter);

    query.declareParameters("long pIdCuentaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pIdCuentaPresupuesto", new Long(idCuentaPresupuesto));

    query.setOrdering("cuentaPresupuesto.codPresupuesto ascending");

    Collection colPartidaUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPartidaUelEspecifica);

    return colPartidaUelEspecifica;
  }

  public Collection findByFuenteFinanciamiento(long idFuenteFinanciamiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "fuenteFinanciamiento.idFuenteFinanciamiento == pIdFuenteFinanciamiento";

    Query query = pm.newQuery(PartidaUelEspecifica.class, filter);

    query.declareParameters("long pIdFuenteFinanciamiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdFuenteFinanciamiento", new Long(idFuenteFinanciamiento));

    query.setOrdering("cuentaPresupuesto.codPresupuesto ascending");

    Collection colPartidaUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPartidaUelEspecifica);

    return colPartidaUelEspecifica;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(PartidaUelEspecifica.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("cuentaPresupuesto.codPresupuesto ascending");

    Collection colPartidaUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPartidaUelEspecifica);

    return colPartidaUelEspecifica;
  }
}