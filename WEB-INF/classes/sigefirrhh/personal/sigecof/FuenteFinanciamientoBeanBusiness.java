package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class FuenteFinanciamientoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    FuenteFinanciamiento fuenteFinanciamientoNew = 
      (FuenteFinanciamiento)BeanUtils.cloneBean(
      fuenteFinanciamiento);

    pm.makePersistent(fuenteFinanciamientoNew);
  }

  public void updateFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento) throws Exception
  {
    FuenteFinanciamiento fuenteFinanciamientoModify = 
      findFuenteFinanciamientoById(fuenteFinanciamiento.getIdFuenteFinanciamiento());

    BeanUtils.copyProperties(fuenteFinanciamientoModify, fuenteFinanciamiento);
  }

  public void deleteFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    FuenteFinanciamiento fuenteFinanciamientoDelete = 
      findFuenteFinanciamientoById(fuenteFinanciamiento.getIdFuenteFinanciamiento());
    pm.deletePersistent(fuenteFinanciamientoDelete);
  }

  public FuenteFinanciamiento findFuenteFinanciamientoById(long idFuenteFinanciamiento) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idFuenteFinanciamiento == pIdFuenteFinanciamiento";
    Query query = pm.newQuery(FuenteFinanciamiento.class, filter);

    query.declareParameters("long pIdFuenteFinanciamiento");

    parameters.put("pIdFuenteFinanciamiento", new Long(idFuenteFinanciamiento));

    Collection colFuenteFinanciamiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colFuenteFinanciamiento.iterator();
    return (FuenteFinanciamiento)iterator.next();
  }

  public Collection findFuenteFinanciamientoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent fuenteFinanciamientoExtent = pm.getExtent(
      FuenteFinanciamiento.class, true);
    Query query = pm.newQuery(fuenteFinanciamientoExtent);
    query.setOrdering("codFuenteFinanciamiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodFuenteFinanciamiento(String codFuenteFinanciamiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codFuenteFinanciamiento == pCodFuenteFinanciamiento";

    Query query = pm.newQuery(FuenteFinanciamiento.class, filter);

    query.declareParameters("java.lang.String pCodFuenteFinanciamiento");
    HashMap parameters = new HashMap();

    parameters.put("pCodFuenteFinanciamiento", new String(codFuenteFinanciamiento));

    query.setOrdering("codFuenteFinanciamiento ascending");

    Collection colFuenteFinanciamiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFuenteFinanciamiento);

    return colFuenteFinanciamiento;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(FuenteFinanciamiento.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codFuenteFinanciamiento ascending");

    Collection colFuenteFinanciamiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colFuenteFinanciamiento);

    return colFuenteFinanciamiento;
  }
}