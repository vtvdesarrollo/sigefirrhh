package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.UnidadAdministradora;

public class EncabezadoResumenMensual
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idEncabezadoResumenMensual;
  private int anio;
  private int mes;
  private int numeroNomina;
  private int numeroExpediente;
  private String titulo;
  private UnidadAdministradora unidadAdministradora;
  private String cerrado;
  private String usuario;
  private Date fechaCierre;
  private String aportes;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "aportes", "cerrado", "fechaCierre", "idEncabezadoResumenMensual", "mes", "numeroExpediente", "numeroNomina", "titulo", "unidadAdministradora", "usuario" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadAdministradora"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.EncabezadoResumenMensual"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EncabezadoResumenMensual());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public EncabezadoResumenMensual()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetnumeroNomina(this, 0);
  }

  public String toString()
  {
    return jdoGettitulo(this) + " " + jdoGetanio(this) + " " + jdoGetmes(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public String getCerrado() {
    return jdoGetcerrado(this);
  }
  public void setCerrado(String cerrado) {
    jdoSetcerrado(this, cerrado);
  }
  public Date getFechaCierre() {
    return jdoGetfechaCierre(this);
  }
  public void setFechaCierre(Date fechaCierre) {
    jdoSetfechaCierre(this, fechaCierre);
  }

  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public int getNumeroExpediente() {
    return jdoGetnumeroExpediente(this);
  }
  public void setNumeroExpediente(int numeroExpediente) {
    jdoSetnumeroExpediente(this, numeroExpediente);
  }
  public int getNumeroNomina() {
    return jdoGetnumeroNomina(this);
  }
  public void setNumeroNomina(int numeroNomina) {
    jdoSetnumeroNomina(this, numeroNomina);
  }
  public String getTitulo() {
    return jdoGettitulo(this);
  }
  public void setTitulo(String titulo) {
    jdoSettitulo(this, titulo);
  }
  public UnidadAdministradora getUnidadAdministradora() {
    return jdoGetunidadAdministradora(this);
  }

  public void setUnidadAdministradora(UnidadAdministradora unidadAdministradora) {
    jdoSetunidadAdministradora(this, unidadAdministradora);
  }
  public String getUsuario() {
    return jdoGetusuario(this);
  }
  public void setUsuario(String usuario) {
    jdoSetusuario(this, usuario);
  }
  public String getAportes() {
    return jdoGetaportes(this);
  }
  public void setAportes(String aportes) {
    jdoSetaportes(this, aportes);
  }

  public long getIdEncabezadoResumenMensual() {
    return jdoGetidEncabezadoResumenMensual(this);
  }
  public void setIdEncabezadoResumenMensual(long idEncabezadoResumenMensual) {
    jdoSetidEncabezadoResumenMensual(this, idEncabezadoResumenMensual);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EncabezadoResumenMensual localEncabezadoResumenMensual = new EncabezadoResumenMensual();
    localEncabezadoResumenMensual.jdoFlags = 1;
    localEncabezadoResumenMensual.jdoStateManager = paramStateManager;
    return localEncabezadoResumenMensual;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EncabezadoResumenMensual localEncabezadoResumenMensual = new EncabezadoResumenMensual();
    localEncabezadoResumenMensual.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEncabezadoResumenMensual.jdoFlags = 1;
    localEncabezadoResumenMensual.jdoStateManager = paramStateManager;
    return localEncabezadoResumenMensual;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aportes);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cerrado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCierre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEncabezadoResumenMensual);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroExpediente);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.titulo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadAdministradora);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aportes = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cerrado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCierre = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEncabezadoResumenMensual = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroExpediente = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.titulo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadAdministradora = ((UnidadAdministradora)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EncabezadoResumenMensual paramEncabezadoResumenMensual, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramEncabezadoResumenMensual.anio;
      return;
    case 1:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.aportes = paramEncabezadoResumenMensual.aportes;
      return;
    case 2:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.cerrado = paramEncabezadoResumenMensual.cerrado;
      return;
    case 3:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCierre = paramEncabezadoResumenMensual.fechaCierre;
      return;
    case 4:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.idEncabezadoResumenMensual = paramEncabezadoResumenMensual.idEncabezadoResumenMensual;
      return;
    case 5:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramEncabezadoResumenMensual.mes;
      return;
    case 6:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.numeroExpediente = paramEncabezadoResumenMensual.numeroExpediente;
      return;
    case 7:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramEncabezadoResumenMensual.numeroNomina;
      return;
    case 8:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.titulo = paramEncabezadoResumenMensual.titulo;
      return;
    case 9:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.unidadAdministradora = paramEncabezadoResumenMensual.unidadAdministradora;
      return;
    case 10:
      if (paramEncabezadoResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramEncabezadoResumenMensual.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EncabezadoResumenMensual))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EncabezadoResumenMensual localEncabezadoResumenMensual = (EncabezadoResumenMensual)paramObject;
    if (localEncabezadoResumenMensual.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEncabezadoResumenMensual, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EncabezadoResumenMensualPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EncabezadoResumenMensualPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EncabezadoResumenMensualPK))
      throw new IllegalArgumentException("arg1");
    EncabezadoResumenMensualPK localEncabezadoResumenMensualPK = (EncabezadoResumenMensualPK)paramObject;
    localEncabezadoResumenMensualPK.idEncabezadoResumenMensual = this.idEncabezadoResumenMensual;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EncabezadoResumenMensualPK))
      throw new IllegalArgumentException("arg1");
    EncabezadoResumenMensualPK localEncabezadoResumenMensualPK = (EncabezadoResumenMensualPK)paramObject;
    this.idEncabezadoResumenMensual = localEncabezadoResumenMensualPK.idEncabezadoResumenMensual;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EncabezadoResumenMensualPK))
      throw new IllegalArgumentException("arg2");
    EncabezadoResumenMensualPK localEncabezadoResumenMensualPK = (EncabezadoResumenMensualPK)paramObject;
    localEncabezadoResumenMensualPK.idEncabezadoResumenMensual = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EncabezadoResumenMensualPK))
      throw new IllegalArgumentException("arg2");
    EncabezadoResumenMensualPK localEncabezadoResumenMensualPK = (EncabezadoResumenMensualPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localEncabezadoResumenMensualPK.idEncabezadoResumenMensual);
  }

  private static final int jdoGetanio(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.anio;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.anio;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 0))
      return paramEncabezadoResumenMensual.anio;
    return localStateManager.getIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 0, paramEncabezadoResumenMensual.anio);
  }

  private static final void jdoSetanio(EncabezadoResumenMensual paramEncabezadoResumenMensual, int paramInt)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 0, paramEncabezadoResumenMensual.anio, paramInt);
  }

  private static final String jdoGetaportes(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.aportes;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.aportes;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 1))
      return paramEncabezadoResumenMensual.aportes;
    return localStateManager.getStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 1, paramEncabezadoResumenMensual.aportes);
  }

  private static final void jdoSetaportes(EncabezadoResumenMensual paramEncabezadoResumenMensual, String paramString)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.aportes = paramString;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.aportes = paramString;
      return;
    }
    localStateManager.setStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 1, paramEncabezadoResumenMensual.aportes, paramString);
  }

  private static final String jdoGetcerrado(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.cerrado;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.cerrado;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 2))
      return paramEncabezadoResumenMensual.cerrado;
    return localStateManager.getStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 2, paramEncabezadoResumenMensual.cerrado);
  }

  private static final void jdoSetcerrado(EncabezadoResumenMensual paramEncabezadoResumenMensual, String paramString)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.cerrado = paramString;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.cerrado = paramString;
      return;
    }
    localStateManager.setStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 2, paramEncabezadoResumenMensual.cerrado, paramString);
  }

  private static final Date jdoGetfechaCierre(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.fechaCierre;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.fechaCierre;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 3))
      return paramEncabezadoResumenMensual.fechaCierre;
    return (Date)localStateManager.getObjectField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 3, paramEncabezadoResumenMensual.fechaCierre);
  }

  private static final void jdoSetfechaCierre(EncabezadoResumenMensual paramEncabezadoResumenMensual, Date paramDate)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.fechaCierre = paramDate;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.fechaCierre = paramDate;
      return;
    }
    localStateManager.setObjectField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 3, paramEncabezadoResumenMensual.fechaCierre, paramDate);
  }

  private static final long jdoGetidEncabezadoResumenMensual(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    return paramEncabezadoResumenMensual.idEncabezadoResumenMensual;
  }

  private static final void jdoSetidEncabezadoResumenMensual(EncabezadoResumenMensual paramEncabezadoResumenMensual, long paramLong)
  {
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.idEncabezadoResumenMensual = paramLong;
      return;
    }
    localStateManager.setLongField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 4, paramEncabezadoResumenMensual.idEncabezadoResumenMensual, paramLong);
  }

  private static final int jdoGetmes(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.mes;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.mes;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 5))
      return paramEncabezadoResumenMensual.mes;
    return localStateManager.getIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 5, paramEncabezadoResumenMensual.mes);
  }

  private static final void jdoSetmes(EncabezadoResumenMensual paramEncabezadoResumenMensual, int paramInt)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 5, paramEncabezadoResumenMensual.mes, paramInt);
  }

  private static final int jdoGetnumeroExpediente(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.numeroExpediente;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.numeroExpediente;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 6))
      return paramEncabezadoResumenMensual.numeroExpediente;
    return localStateManager.getIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 6, paramEncabezadoResumenMensual.numeroExpediente);
  }

  private static final void jdoSetnumeroExpediente(EncabezadoResumenMensual paramEncabezadoResumenMensual, int paramInt)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.numeroExpediente = paramInt;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.numeroExpediente = paramInt;
      return;
    }
    localStateManager.setIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 6, paramEncabezadoResumenMensual.numeroExpediente, paramInt);
  }

  private static final int jdoGetnumeroNomina(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.numeroNomina;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.numeroNomina;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 7))
      return paramEncabezadoResumenMensual.numeroNomina;
    return localStateManager.getIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 7, paramEncabezadoResumenMensual.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(EncabezadoResumenMensual paramEncabezadoResumenMensual, int paramInt)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 7, paramEncabezadoResumenMensual.numeroNomina, paramInt);
  }

  private static final String jdoGettitulo(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.titulo;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.titulo;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 8))
      return paramEncabezadoResumenMensual.titulo;
    return localStateManager.getStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 8, paramEncabezadoResumenMensual.titulo);
  }

  private static final void jdoSettitulo(EncabezadoResumenMensual paramEncabezadoResumenMensual, String paramString)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.titulo = paramString;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.titulo = paramString;
      return;
    }
    localStateManager.setStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 8, paramEncabezadoResumenMensual.titulo, paramString);
  }

  private static final UnidadAdministradora jdoGetunidadAdministradora(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.unidadAdministradora;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 9))
      return paramEncabezadoResumenMensual.unidadAdministradora;
    return (UnidadAdministradora)localStateManager.getObjectField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 9, paramEncabezadoResumenMensual.unidadAdministradora);
  }

  private static final void jdoSetunidadAdministradora(EncabezadoResumenMensual paramEncabezadoResumenMensual, UnidadAdministradora paramUnidadAdministradora)
  {
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.unidadAdministradora = paramUnidadAdministradora;
      return;
    }
    localStateManager.setObjectField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 9, paramEncabezadoResumenMensual.unidadAdministradora, paramUnidadAdministradora);
  }

  private static final String jdoGetusuario(EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    if (paramEncabezadoResumenMensual.jdoFlags <= 0)
      return paramEncabezadoResumenMensual.usuario;
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEncabezadoResumenMensual.usuario;
    if (localStateManager.isLoaded(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 10))
      return paramEncabezadoResumenMensual.usuario;
    return localStateManager.getStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 10, paramEncabezadoResumenMensual.usuario);
  }

  private static final void jdoSetusuario(EncabezadoResumenMensual paramEncabezadoResumenMensual, String paramString)
  {
    if (paramEncabezadoResumenMensual.jdoFlags == 0)
    {
      paramEncabezadoResumenMensual.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramEncabezadoResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEncabezadoResumenMensual.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramEncabezadoResumenMensual, jdoInheritedFieldCount + 10, paramEncabezadoResumenMensual.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}