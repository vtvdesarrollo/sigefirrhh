package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadAdministradoraBeanBusiness;

public class EncabezadoResumenMensualBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    EncabezadoResumenMensual encabezadoResumenMensualNew = 
      (EncabezadoResumenMensual)BeanUtils.cloneBean(
      encabezadoResumenMensual);

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (encabezadoResumenMensualNew.getUnidadAdministradora() != null) {
      encabezadoResumenMensualNew.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        encabezadoResumenMensualNew.getUnidadAdministradora().getIdUnidadAdministradora()));
    }
    pm.makePersistent(encabezadoResumenMensualNew);
  }

  public void updateEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual) throws Exception
  {
    EncabezadoResumenMensual encabezadoResumenMensualModify = 
      findEncabezadoResumenMensualById(encabezadoResumenMensual.getIdEncabezadoResumenMensual());

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (encabezadoResumenMensual.getUnidadAdministradora() != null) {
      encabezadoResumenMensual.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        encabezadoResumenMensual.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    BeanUtils.copyProperties(encabezadoResumenMensualModify, encabezadoResumenMensual);
  }

  public void deleteEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    EncabezadoResumenMensual encabezadoResumenMensualDelete = 
      findEncabezadoResumenMensualById(encabezadoResumenMensual.getIdEncabezadoResumenMensual());
    pm.deletePersistent(encabezadoResumenMensualDelete);
  }

  public EncabezadoResumenMensual findEncabezadoResumenMensualById(long idEncabezadoResumenMensual) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEncabezadoResumenMensual == pIdEncabezadoResumenMensual";
    Query query = pm.newQuery(EncabezadoResumenMensual.class, filter);

    query.declareParameters("long pIdEncabezadoResumenMensual");

    parameters.put("pIdEncabezadoResumenMensual", new Long(idEncabezadoResumenMensual));

    Collection colEncabezadoResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEncabezadoResumenMensual.iterator();
    return (EncabezadoResumenMensual)iterator.next();
  }

  public Collection findEncabezadoResumenMensualAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent encabezadoResumenMensualExtent = pm.getExtent(
      EncabezadoResumenMensual.class, true);
    Query query = pm.newQuery(encabezadoResumenMensualExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(EncabezadoResumenMensual.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, mes ascending");

    Collection colEncabezadoResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEncabezadoResumenMensual);

    return colEncabezadoResumenMensual;
  }

  public Collection findByMes(int mes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "mes == pMes";

    Query query = pm.newQuery(EncabezadoResumenMensual.class, filter);

    query.declareParameters("int pMes");
    HashMap parameters = new HashMap();

    parameters.put("pMes", new Integer(mes));

    query.setOrdering("anio ascending, mes ascending");

    Collection colEncabezadoResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEncabezadoResumenMensual);

    return colEncabezadoResumenMensual;
  }

  public Collection findByNumeroNomina(int numeroNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "numeroNomina == pNumeroNomina";

    Query query = pm.newQuery(EncabezadoResumenMensual.class, filter);

    query.declareParameters("int pNumeroNomina");
    HashMap parameters = new HashMap();

    parameters.put("pNumeroNomina", new Integer(numeroNomina));

    query.setOrdering("anio ascending, mes ascending");

    Collection colEncabezadoResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEncabezadoResumenMensual);

    return colEncabezadoResumenMensual;
  }

  public Collection findByNumeroExpediente(int numeroExpediente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "numeroExpediente == pNumeroExpediente";

    Query query = pm.newQuery(EncabezadoResumenMensual.class, filter);

    query.declareParameters("int pNumeroExpediente");
    HashMap parameters = new HashMap();

    parameters.put("pNumeroExpediente", new Integer(numeroExpediente));

    query.setOrdering("anio ascending, mes ascending");

    Collection colEncabezadoResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEncabezadoResumenMensual);

    return colEncabezadoResumenMensual;
  }

  public Collection findByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadAdministradora.idUnidadAdministradora == pIdUnidadAdministradora";

    Query query = pm.newQuery(EncabezadoResumenMensual.class, filter);

    query.declareParameters("long pIdUnidadAdministradora");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadAdministradora", new Long(idUnidadAdministradora));

    query.setOrdering("anio ascending, mes ascending");

    Collection colEncabezadoResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEncabezadoResumenMensual);

    return colEncabezadoResumenMensual;
  }
}