package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonalBeanBusiness;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadAdministradoraBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class ConceptoResumenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoResumen(ConceptoResumen conceptoResumen)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoResumen conceptoResumenNew = 
      (ConceptoResumen)BeanUtils.cloneBean(
      conceptoResumen);

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (conceptoResumenNew.getUnidadAdministradora() != null) {
      conceptoResumenNew.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        conceptoResumenNew.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoResumenNew.getConceptoTipoPersonal() != null) {
      conceptoResumenNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoResumenNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoResumenNew.getFrecuenciaTipoPersonal() != null) {
      conceptoResumenNew.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoResumenNew.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoResumenNew.getTrabajador() != null) {
      conceptoResumenNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoResumenNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(conceptoResumenNew);
  }

  public void updateConceptoResumen(ConceptoResumen conceptoResumen) throws Exception
  {
    ConceptoResumen conceptoResumenModify = 
      findConceptoResumenById(conceptoResumen.getIdConceptoResumen());

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (conceptoResumen.getUnidadAdministradora() != null) {
      conceptoResumen.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        conceptoResumen.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoResumen.getConceptoTipoPersonal() != null) {
      conceptoResumen.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoResumen.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FrecuenciaTipoPersonalBeanBusiness frecuenciaTipoPersonalBeanBusiness = new FrecuenciaTipoPersonalBeanBusiness();

    if (conceptoResumen.getFrecuenciaTipoPersonal() != null) {
      conceptoResumen.setFrecuenciaTipoPersonal(
        frecuenciaTipoPersonalBeanBusiness.findFrecuenciaTipoPersonalById(
        conceptoResumen.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (conceptoResumen.getTrabajador() != null) {
      conceptoResumen.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        conceptoResumen.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(conceptoResumenModify, conceptoResumen);
  }

  public void deleteConceptoResumen(ConceptoResumen conceptoResumen) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoResumen conceptoResumenDelete = 
      findConceptoResumenById(conceptoResumen.getIdConceptoResumen());
    pm.deletePersistent(conceptoResumenDelete);
  }

  public ConceptoResumen findConceptoResumenById(long idConceptoResumen) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoResumen == pIdConceptoResumen";
    Query query = pm.newQuery(ConceptoResumen.class, filter);

    query.declareParameters("long pIdConceptoResumen");

    parameters.put("pIdConceptoResumen", new Long(idConceptoResumen));

    Collection colConceptoResumen = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoResumen.iterator();
    return (ConceptoResumen)iterator.next();
  }

  public Collection findConceptoResumenAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoResumenExtent = pm.getExtent(
      ConceptoResumen.class, true);
    Query query = pm.newQuery(conceptoResumenExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadAdministradora.idUnidadAdministradora == pIdUnidadAdministradora";

    Query query = pm.newQuery(ConceptoResumen.class, filter);

    query.declareParameters("long pIdUnidadAdministradora");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadAdministradora", new Long(idUnidadAdministradora));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoResumen = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoResumen);

    return colConceptoResumen;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ConceptoResumen.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoResumen = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoResumen);

    return colConceptoResumen;
  }
}