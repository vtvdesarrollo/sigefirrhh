package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AccionEspecificaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAccionEspecifica(AccionEspecifica accionEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AccionEspecifica accionEspecificaNew = 
      (AccionEspecifica)BeanUtils.cloneBean(
      accionEspecifica);

    ProyectoBeanBusiness proyectoBeanBusiness = new ProyectoBeanBusiness();

    if (accionEspecificaNew.getProyecto() != null) {
      accionEspecificaNew.setProyecto(
        proyectoBeanBusiness.findProyectoById(
        accionEspecificaNew.getProyecto().getIdProyecto()));
    }

    AccionCentralizadaBeanBusiness accionCentralizadaBeanBusiness = new AccionCentralizadaBeanBusiness();

    if (accionEspecificaNew.getAccionCentralizada() != null) {
      accionEspecificaNew.setAccionCentralizada(
        accionCentralizadaBeanBusiness.findAccionCentralizadaById(
        accionEspecificaNew.getAccionCentralizada().getIdAccionCentralizada()));
    }
    pm.makePersistent(accionEspecificaNew);
  }

  public void updateAccionEspecifica(AccionEspecifica accionEspecifica) throws Exception
  {
    AccionEspecifica accionEspecificaModify = 
      findAccionEspecificaById(accionEspecifica.getIdAccionEspecifica());

    ProyectoBeanBusiness proyectoBeanBusiness = new ProyectoBeanBusiness();

    if (accionEspecifica.getProyecto() != null) {
      accionEspecifica.setProyecto(
        proyectoBeanBusiness.findProyectoById(
        accionEspecifica.getProyecto().getIdProyecto()));
    }

    AccionCentralizadaBeanBusiness accionCentralizadaBeanBusiness = new AccionCentralizadaBeanBusiness();

    if (accionEspecifica.getAccionCentralizada() != null) {
      accionEspecifica.setAccionCentralizada(
        accionCentralizadaBeanBusiness.findAccionCentralizadaById(
        accionEspecifica.getAccionCentralizada().getIdAccionCentralizada()));
    }

    BeanUtils.copyProperties(accionEspecificaModify, accionEspecifica);
  }

  public void deleteAccionEspecifica(AccionEspecifica accionEspecifica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AccionEspecifica accionEspecificaDelete = 
      findAccionEspecificaById(accionEspecifica.getIdAccionEspecifica());
    pm.deletePersistent(accionEspecificaDelete);
  }

  public AccionEspecifica findAccionEspecificaById(long idAccionEspecifica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAccionEspecifica == pIdAccionEspecifica";
    Query query = pm.newQuery(AccionEspecifica.class, filter);

    query.declareParameters("long pIdAccionEspecifica");

    parameters.put("pIdAccionEspecifica", new Long(idAccionEspecifica));

    Collection colAccionEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAccionEspecifica.iterator();
    return (AccionEspecifica)iterator.next();
  }

  public Collection findAccionEspecificaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent accionEspecificaExtent = pm.getExtent(
      AccionEspecifica.class, true);
    Query query = pm.newQuery(accionEspecificaExtent);
    query.setOrdering("anio ascending, codAccionEspecifica ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByProyecto(long idProyecto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "proyecto.idProyecto == pIdProyecto";

    Query query = pm.newQuery(AccionEspecifica.class, filter);

    query.declareParameters("long pIdProyecto");
    HashMap parameters = new HashMap();

    parameters.put("pIdProyecto", new Long(idProyecto));

    query.setOrdering("anio ascending, proyecto.codProyecto ascending, codAccionEspecifica ascending");

    Collection colAccionEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionEspecifica);

    return colAccionEspecifica;
  }

  public Collection findByAccionCentralizada(long idAccionCentralizada)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "accionCentralizada.idAccionCentralizada == pIdAccionCentralizada";

    Query query = pm.newQuery(AccionEspecifica.class, filter);

    query.declareParameters("long pIdAccionCentralizada");
    HashMap parameters = new HashMap();

    parameters.put("pIdAccionCentralizada", new Long(idAccionCentralizada));

    query.setOrdering("anio ascending, accionCentralizada.codAccionCentralizada ascending,codAccionEspecifica ascending");

    Collection colAccionEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionEspecifica);

    return colAccionEspecifica;
  }

  public Collection findByCodAccionEspecifica(String codAccionEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codAccionEspecifica == pCodAccionEspecifica";

    Query query = pm.newQuery(AccionEspecifica.class, filter);

    query.declareParameters("java.lang.String pCodAccionEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pCodAccionEspecifica", new String(codAccionEspecifica));

    query.setOrdering("anio ascending, codAccionEspecifica ascending");

    Collection colAccionEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionEspecifica);

    return colAccionEspecifica;
  }

  public Collection findByDenominacion(String denominacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "denominacion.startsWith(pDenominacion)";

    Query query = pm.newQuery(AccionEspecifica.class, filter);

    query.declareParameters("java.lang.String pDenominacion");
    HashMap parameters = new HashMap();

    parameters.put("pDenominacion", new String(denominacion));

    query.setOrdering("anio ascending, codAccionEspecifica ascending");

    Collection colAccionEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionEspecifica);

    return colAccionEspecifica;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(AccionEspecifica.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, codAccionEspecifica ascending");

    Collection colAccionEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionEspecifica);

    return colAccionEspecifica;
  }
}