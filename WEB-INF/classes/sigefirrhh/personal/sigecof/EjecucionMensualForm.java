package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class EjecucionMensualForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EjecucionMensualForm.class.getName());
  private EjecucionMensual ejecucionMensual;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showEjecucionMensualByPresupuestoEspecifica;
  private boolean showEjecucionMensualByAnio;
  private boolean showEjecucionMensualByMes;
  private String findSelectPresupuestoEspecifica;
  private int findAnio;
  private int findMes;
  private Collection findColPresupuestoEspecifica;
  private Collection colPresupuestoEspecifica;
  private String selectPresupuestoEspecifica;
  private Object stateResultEjecucionMensualByPresupuestoEspecifica = null;

  private Object stateResultEjecucionMensualByAnio = null;

  private Object stateResultEjecucionMensualByMes = null;

  public String getFindSelectPresupuestoEspecifica()
  {
    return this.findSelectPresupuestoEspecifica;
  }
  public void setFindSelectPresupuestoEspecifica(String valPresupuestoEspecifica) {
    this.findSelectPresupuestoEspecifica = valPresupuestoEspecifica;
  }

  public Collection getFindColPresupuestoEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPresupuestoEspecifica.iterator();
    PresupuestoEspecifica presupuestoEspecifica = null;
    while (iterator.hasNext()) {
      presupuestoEspecifica = (PresupuestoEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(presupuestoEspecifica.getIdPresupuestoEspecifica()), 
        presupuestoEspecifica.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public int getFindMes() {
    return this.findMes;
  }
  public void setFindMes(int findMes) {
    this.findMes = findMes;
  }

  public String getSelectPresupuestoEspecifica()
  {
    return this.selectPresupuestoEspecifica;
  }
  public void setSelectPresupuestoEspecifica(String valPresupuestoEspecifica) {
    Iterator iterator = this.colPresupuestoEspecifica.iterator();
    PresupuestoEspecifica presupuestoEspecifica = null;
    this.ejecucionMensual.setPresupuestoEspecifica(null);
    while (iterator.hasNext()) {
      presupuestoEspecifica = (PresupuestoEspecifica)iterator.next();
      if (String.valueOf(presupuestoEspecifica.getIdPresupuestoEspecifica()).equals(
        valPresupuestoEspecifica)) {
        this.ejecucionMensual.setPresupuestoEspecifica(
          presupuestoEspecifica);
        break;
      }
    }
    this.selectPresupuestoEspecifica = valPresupuestoEspecifica;
  }
  public Collection getResult() {
    return this.result;
  }

  public EjecucionMensual getEjecucionMensual() {
    if (this.ejecucionMensual == null) {
      this.ejecucionMensual = new EjecucionMensual();
    }
    return this.ejecucionMensual;
  }

  public EjecucionMensualForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPresupuestoEspecifica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPresupuestoEspecifica.iterator();
    PresupuestoEspecifica presupuestoEspecifica = null;
    while (iterator.hasNext()) {
      presupuestoEspecifica = (PresupuestoEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(presupuestoEspecifica.getIdPresupuestoEspecifica()), 
        presupuestoEspecifica.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPresupuestoEspecifica = 
        this.sigecofFacade.findAllPresupuestoEspecifica();

      this.colPresupuestoEspecifica = 
        this.sigecofFacade.findAllPresupuestoEspecifica();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findEjecucionMensualByPresupuestoEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findEjecucionMensualByPresupuestoEspecifica(Long.valueOf(this.findSelectPresupuestoEspecifica).longValue());
      this.showEjecucionMensualByPresupuestoEspecifica = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEjecucionMensualByPresupuestoEspecifica)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPresupuestoEspecifica = null;
    this.findAnio = 0;
    this.findMes = 0;

    return null;
  }

  public String findEjecucionMensualByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findEjecucionMensualByAnio(this.findAnio);
      this.showEjecucionMensualByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEjecucionMensualByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPresupuestoEspecifica = null;
    this.findAnio = 0;
    this.findMes = 0;

    return null;
  }

  public String findEjecucionMensualByMes()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findEjecucionMensualByMes(this.findMes);
      this.showEjecucionMensualByMes = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEjecucionMensualByMes)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPresupuestoEspecifica = null;
    this.findAnio = 0;
    this.findMes = 0;

    return null;
  }

  public boolean isShowEjecucionMensualByPresupuestoEspecifica() {
    return this.showEjecucionMensualByPresupuestoEspecifica;
  }
  public boolean isShowEjecucionMensualByAnio() {
    return this.showEjecucionMensualByAnio;
  }
  public boolean isShowEjecucionMensualByMes() {
    return this.showEjecucionMensualByMes;
  }

  public String selectEjecucionMensual()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPresupuestoEspecifica = null;

    long idEjecucionMensual = 
      Long.parseLong((String)requestParameterMap.get("idEjecucionMensual"));
    try
    {
      this.ejecucionMensual = 
        this.sigecofFacade.findEjecucionMensualById(
        idEjecucionMensual);
      if (this.ejecucionMensual.getPresupuestoEspecifica() != null) {
        this.selectPresupuestoEspecifica = 
          String.valueOf(this.ejecucionMensual.getPresupuestoEspecifica().getIdPresupuestoEspecifica());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.ejecucionMensual = null;
    this.showEjecucionMensualByPresupuestoEspecifica = false;
    this.showEjecucionMensualByAnio = false;
    this.showEjecucionMensualByMes = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addEjecucionMensual(
          this.ejecucionMensual);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateEjecucionMensual(
          this.ejecucionMensual);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteEjecucionMensual(
        this.ejecucionMensual);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.ejecucionMensual = new EjecucionMensual();

    this.selectPresupuestoEspecifica = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.ejecucionMensual.setIdEjecucionMensual(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.EjecucionMensual"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.ejecucionMensual = new EjecucionMensual();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}