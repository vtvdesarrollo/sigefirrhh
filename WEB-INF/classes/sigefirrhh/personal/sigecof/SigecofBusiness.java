package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class SigecofBusiness extends AbstractBusiness
  implements Serializable
{
  private ResumenMensualBeanBusiness resumenMensualBeanBusiness = new ResumenMensualBeanBusiness();

  private EncabezadoResumenMensualBeanBusiness encabezadoResumenMensualBeanBusiness = new EncabezadoResumenMensualBeanBusiness();

  private SeguridadPresupuestoBeanBusiness seguridadPresupuestoBeanBusiness = new SeguridadPresupuestoBeanBusiness();

  private ConceptoResumenBeanBusiness conceptoResumenBeanBusiness = new ConceptoResumenBeanBusiness();

  private ConceptoEspecificaBeanBusiness conceptoEspecificaBeanBusiness = new ConceptoEspecificaBeanBusiness();

  private CargoEspecificaBeanBusiness cargoEspecificaBeanBusiness = new CargoEspecificaBeanBusiness();

  private PresupuestoEspecificaBeanBusiness presupuestoEspecificaBeanBusiness = new PresupuestoEspecificaBeanBusiness();

  private AccionEspecificaBeanBusiness accionEspecificaBeanBusiness = new AccionEspecificaBeanBusiness();

  private AccionCentralizadaBeanBusiness accionCentralizadaBeanBusiness = new AccionCentralizadaBeanBusiness();

  private ProyectoBeanBusiness proyectoBeanBusiness = new ProyectoBeanBusiness();

  private CuentaContableBeanBusiness cuentaContableBeanBusiness = new CuentaContableBeanBusiness();

  private ConceptoContableBeanBusiness conceptoContableBeanBusiness = new ConceptoContableBeanBusiness();

  private UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

  private ConceptoCuentaBeanBusiness conceptoCuentaBeanBusiness = new ConceptoCuentaBeanBusiness();

  private CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

  private TrabajadorEspecificaBeanBusiness trabajadorEspecificaBeanBusiness = new TrabajadorEspecificaBeanBusiness();

  private EjecucionMensualBeanBusiness ejecucionMensualBeanBusiness = new EjecucionMensualBeanBusiness();

  private FuenteFinanciamientoBeanBusiness fuenteFinanciamientoBeanBusiness = new FuenteFinanciamientoBeanBusiness();

  private PartidaUelEspecificaBeanBusiness partidaUelEspecificaBeanBusiness = new PartidaUelEspecificaBeanBusiness();

  private ConceptoCuentaContableBeanBusiness conceptoCuentaContableBeanBusiness = new ConceptoCuentaContableBeanBusiness();

  public void addResumenMensual(ResumenMensual resumenMensual)
    throws Exception
  {
    this.resumenMensualBeanBusiness.addResumenMensual(resumenMensual);
  }

  public void updateResumenMensual(ResumenMensual resumenMensual) throws Exception {
    this.resumenMensualBeanBusiness.updateResumenMensual(resumenMensual);
  }

  public void deleteResumenMensual(ResumenMensual resumenMensual) throws Exception {
    this.resumenMensualBeanBusiness.deleteResumenMensual(resumenMensual);
  }

  public ResumenMensual findResumenMensualById(long resumenMensualId) throws Exception {
    return this.resumenMensualBeanBusiness.findResumenMensualById(resumenMensualId);
  }

  public Collection findAllResumenMensual() throws Exception {
    return this.resumenMensualBeanBusiness.findResumenMensualAll();
  }

  public Collection findResumenMensualByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    return this.resumenMensualBeanBusiness.findByUnidadAdministradora(idUnidadAdministradora);
  }

  public Collection findResumenMensualByEncabezadoResumenMensual(long idEncabezadoResumenMensual)
    throws Exception
  {
    return this.resumenMensualBeanBusiness.findByEncabezadoResumenMensual(idEncabezadoResumenMensual);
  }

  public Collection findResumenMensualByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    return this.resumenMensualBeanBusiness.findByUelEspecifica(idUelEspecifica);
  }

  public Collection findResumenMensualByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    return this.resumenMensualBeanBusiness.findByCuentaPresupuesto(idCuentaPresupuesto);
  }

  public Collection findResumenMensualByUnidadEjecutora(long idUnidadEjecutora)
    throws Exception
  {
    return this.resumenMensualBeanBusiness.findByUnidadEjecutora(idUnidadEjecutora);
  }

  public Collection findResumenMensualByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.resumenMensualBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findResumenMensualByFuenteFinanciamiento(long idFuenteFinanciamiento)
    throws Exception
  {
    return this.resumenMensualBeanBusiness.findByFuenteFinanciamiento(idFuenteFinanciamiento);
  }

  public void addEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual)
    throws Exception
  {
    this.encabezadoResumenMensualBeanBusiness.addEncabezadoResumenMensual(encabezadoResumenMensual);
  }

  public void updateEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual) throws Exception {
    this.encabezadoResumenMensualBeanBusiness.updateEncabezadoResumenMensual(encabezadoResumenMensual);
  }

  public void deleteEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual) throws Exception {
    this.encabezadoResumenMensualBeanBusiness.deleteEncabezadoResumenMensual(encabezadoResumenMensual);
  }

  public EncabezadoResumenMensual findEncabezadoResumenMensualById(long encabezadoResumenMensualId) throws Exception {
    return this.encabezadoResumenMensualBeanBusiness.findEncabezadoResumenMensualById(encabezadoResumenMensualId);
  }

  public Collection findAllEncabezadoResumenMensual() throws Exception {
    return this.encabezadoResumenMensualBeanBusiness.findEncabezadoResumenMensualAll();
  }

  public Collection findEncabezadoResumenMensualByAnio(int anio)
    throws Exception
  {
    return this.encabezadoResumenMensualBeanBusiness.findByAnio(anio);
  }

  public Collection findEncabezadoResumenMensualByMes(int mes)
    throws Exception
  {
    return this.encabezadoResumenMensualBeanBusiness.findByMes(mes);
  }

  public Collection findEncabezadoResumenMensualByNumeroNomina(int numeroNomina)
    throws Exception
  {
    return this.encabezadoResumenMensualBeanBusiness.findByNumeroNomina(numeroNomina);
  }

  public Collection findEncabezadoResumenMensualByNumeroExpediente(int numeroExpediente)
    throws Exception
  {
    return this.encabezadoResumenMensualBeanBusiness.findByNumeroExpediente(numeroExpediente);
  }

  public Collection findEncabezadoResumenMensualByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    return this.encabezadoResumenMensualBeanBusiness.findByUnidadAdministradora(idUnidadAdministradora);
  }

  public void addSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto)
    throws Exception
  {
    this.seguridadPresupuestoBeanBusiness.addSeguridadPresupuesto(seguridadPresupuesto);
  }

  public void updateSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto) throws Exception {
    this.seguridadPresupuestoBeanBusiness.updateSeguridadPresupuesto(seguridadPresupuesto);
  }

  public void deleteSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto) throws Exception {
    this.seguridadPresupuestoBeanBusiness.deleteSeguridadPresupuesto(seguridadPresupuesto);
  }

  public SeguridadPresupuesto findSeguridadPresupuestoById(long seguridadPresupuestoId) throws Exception {
    return this.seguridadPresupuestoBeanBusiness.findSeguridadPresupuestoById(seguridadPresupuestoId);
  }

  public Collection findAllSeguridadPresupuesto() throws Exception {
    return this.seguridadPresupuestoBeanBusiness.findSeguridadPresupuestoAll();
  }

  public Collection findSeguridadPresupuestoByCategoriaPresupuesto(long idCategoriaPresupuesto)
    throws Exception
  {
    return this.seguridadPresupuestoBeanBusiness.findByCategoriaPresupuesto(idCategoriaPresupuesto);
  }

  public Collection findSeguridadPresupuestoByAnio(int anio)
    throws Exception
  {
    return this.seguridadPresupuestoBeanBusiness.findByAnio(anio);
  }

  public void addConceptoResumen(ConceptoResumen conceptoResumen)
    throws Exception
  {
    this.conceptoResumenBeanBusiness.addConceptoResumen(conceptoResumen);
  }

  public void updateConceptoResumen(ConceptoResumen conceptoResumen) throws Exception {
    this.conceptoResumenBeanBusiness.updateConceptoResumen(conceptoResumen);
  }

  public void deleteConceptoResumen(ConceptoResumen conceptoResumen) throws Exception {
    this.conceptoResumenBeanBusiness.deleteConceptoResumen(conceptoResumen);
  }

  public ConceptoResumen findConceptoResumenById(long conceptoResumenId) throws Exception {
    return this.conceptoResumenBeanBusiness.findConceptoResumenById(conceptoResumenId);
  }

  public Collection findAllConceptoResumen() throws Exception {
    return this.conceptoResumenBeanBusiness.findConceptoResumenAll();
  }

  public Collection findConceptoResumenByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    return this.conceptoResumenBeanBusiness.findByUnidadAdministradora(idUnidadAdministradora);
  }

  public Collection findConceptoResumenByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.conceptoResumenBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addConceptoEspecifica(ConceptoEspecifica conceptoEspecifica)
    throws Exception
  {
    this.conceptoEspecificaBeanBusiness.addConceptoEspecifica(conceptoEspecifica);
  }

  public void updateConceptoEspecifica(ConceptoEspecifica conceptoEspecifica) throws Exception {
    this.conceptoEspecificaBeanBusiness.updateConceptoEspecifica(conceptoEspecifica);
  }

  public void deleteConceptoEspecifica(ConceptoEspecifica conceptoEspecifica) throws Exception {
    this.conceptoEspecificaBeanBusiness.deleteConceptoEspecifica(conceptoEspecifica);
  }

  public ConceptoEspecifica findConceptoEspecificaById(long conceptoEspecificaId) throws Exception {
    return this.conceptoEspecificaBeanBusiness.findConceptoEspecificaById(conceptoEspecificaId);
  }

  public Collection findAllConceptoEspecifica() throws Exception {
    return this.conceptoEspecificaBeanBusiness.findConceptoEspecificaAll();
  }

  public Collection findConceptoEspecificaByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.conceptoEspecificaBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findConceptoEspecificaByAnio(int anio)
    throws Exception
  {
    return this.conceptoEspecificaBeanBusiness.findByAnio(anio);
  }

  public void addCargoEspecifica(CargoEspecifica cargoEspecifica)
    throws Exception
  {
    this.cargoEspecificaBeanBusiness.addCargoEspecifica(cargoEspecifica);
  }

  public void updateCargoEspecifica(CargoEspecifica cargoEspecifica) throws Exception {
    this.cargoEspecificaBeanBusiness.updateCargoEspecifica(cargoEspecifica);
  }

  public void deleteCargoEspecifica(CargoEspecifica cargoEspecifica) throws Exception {
    this.cargoEspecificaBeanBusiness.deleteCargoEspecifica(cargoEspecifica);
  }

  public CargoEspecifica findCargoEspecificaById(long cargoEspecificaId) throws Exception {
    return this.cargoEspecificaBeanBusiness.findCargoEspecificaById(cargoEspecificaId);
  }

  public Collection findAllCargoEspecifica() throws Exception {
    return this.cargoEspecificaBeanBusiness.findCargoEspecificaAll();
  }

  public Collection findCargoEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    return this.cargoEspecificaBeanBusiness.findByUelEspecifica(idUelEspecifica);
  }

  public Collection findCargoEspecificaByRegistroCargos(long idRegistroCargos)
    throws Exception
  {
    return this.cargoEspecificaBeanBusiness.findByRegistroCargos(idRegistroCargos);
  }

  public Collection findCargoEspecificaByAnio(int anio)
    throws Exception
  {
    return this.cargoEspecificaBeanBusiness.findByAnio(anio);
  }

  public void addPresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica)
    throws Exception
  {
    this.presupuestoEspecificaBeanBusiness.addPresupuestoEspecifica(presupuestoEspecifica);
  }

  public void updatePresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica) throws Exception {
    this.presupuestoEspecificaBeanBusiness.updatePresupuestoEspecifica(presupuestoEspecifica);
  }

  public void deletePresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica) throws Exception {
    this.presupuestoEspecificaBeanBusiness.deletePresupuestoEspecifica(presupuestoEspecifica);
  }

  public PresupuestoEspecifica findPresupuestoEspecificaById(long presupuestoEspecificaId) throws Exception {
    return this.presupuestoEspecificaBeanBusiness.findPresupuestoEspecificaById(presupuestoEspecificaId);
  }

  public Collection findAllPresupuestoEspecifica() throws Exception {
    return this.presupuestoEspecificaBeanBusiness.findPresupuestoEspecificaAll();
  }

  public Collection findPresupuestoEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    return this.presupuestoEspecificaBeanBusiness.findByUelEspecifica(idUelEspecifica);
  }

  public Collection findPresupuestoEspecificaByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    return this.presupuestoEspecificaBeanBusiness.findByCuentaPresupuesto(idCuentaPresupuesto);
  }

  public Collection findPresupuestoEspecificaByAnio(int anio)
    throws Exception
  {
    return this.presupuestoEspecificaBeanBusiness.findByAnio(anio);
  }

  public void addAccionEspecifica(AccionEspecifica accionEspecifica)
    throws Exception
  {
    this.accionEspecificaBeanBusiness.addAccionEspecifica(accionEspecifica);
  }

  public void updateAccionEspecifica(AccionEspecifica accionEspecifica) throws Exception {
    this.accionEspecificaBeanBusiness.updateAccionEspecifica(accionEspecifica);
  }

  public void deleteAccionEspecifica(AccionEspecifica accionEspecifica) throws Exception {
    this.accionEspecificaBeanBusiness.deleteAccionEspecifica(accionEspecifica);
  }

  public AccionEspecifica findAccionEspecificaById(long accionEspecificaId) throws Exception {
    return this.accionEspecificaBeanBusiness.findAccionEspecificaById(accionEspecificaId);
  }

  public Collection findAllAccionEspecifica() throws Exception {
    return this.accionEspecificaBeanBusiness.findAccionEspecificaAll();
  }

  public Collection findAccionEspecificaByProyecto(long idProyecto)
    throws Exception
  {
    return this.accionEspecificaBeanBusiness.findByProyecto(idProyecto);
  }

  public Collection findAccionEspecificaByAccionCentralizada(long idAccionCentralizada)
    throws Exception
  {
    return this.accionEspecificaBeanBusiness.findByAccionCentralizada(idAccionCentralizada);
  }

  public Collection findAccionEspecificaByCodAccionEspecifica(String codAccionEspecifica)
    throws Exception
  {
    return this.accionEspecificaBeanBusiness.findByCodAccionEspecifica(codAccionEspecifica);
  }

  public Collection findAccionEspecificaByDenominacion(String denominacion)
    throws Exception
  {
    return this.accionEspecificaBeanBusiness.findByDenominacion(denominacion);
  }

  public Collection findAccionEspecificaByAnio(int anio)
    throws Exception
  {
    return this.accionEspecificaBeanBusiness.findByAnio(anio);
  }

  public void addAccionCentralizada(AccionCentralizada accionCentralizada)
    throws Exception
  {
    this.accionCentralizadaBeanBusiness.addAccionCentralizada(accionCentralizada);
  }

  public void updateAccionCentralizada(AccionCentralizada accionCentralizada) throws Exception {
    this.accionCentralizadaBeanBusiness.updateAccionCentralizada(accionCentralizada);
  }

  public void deleteAccionCentralizada(AccionCentralizada accionCentralizada) throws Exception {
    this.accionCentralizadaBeanBusiness.deleteAccionCentralizada(accionCentralizada);
  }

  public AccionCentralizada findAccionCentralizadaById(long accionCentralizadaId) throws Exception {
    return this.accionCentralizadaBeanBusiness.findAccionCentralizadaById(accionCentralizadaId);
  }

  public Collection findAllAccionCentralizada() throws Exception {
    return this.accionCentralizadaBeanBusiness.findAccionCentralizadaAll();
  }

  public Collection findAccionCentralizadaByAnio(int anio, long idOrganismo)
    throws Exception
  {
    return this.accionCentralizadaBeanBusiness.findByAnio(anio, idOrganismo);
  }

  public Collection findAccionCentralizadaByCodAccionCentralizada(String codAccionCentralizada, long idOrganismo)
    throws Exception
  {
    return this.accionCentralizadaBeanBusiness.findByCodAccionCentralizada(codAccionCentralizada, idOrganismo);
  }

  public Collection findAccionCentralizadaByDenominacion(String denominacion, long idOrganismo)
    throws Exception
  {
    return this.accionCentralizadaBeanBusiness.findByDenominacion(denominacion, idOrganismo);
  }

  public Collection findAccionCentralizadaByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.accionCentralizadaBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addProyecto(Proyecto proyecto)
    throws Exception
  {
    this.proyectoBeanBusiness.addProyecto(proyecto);
  }

  public void updateProyecto(Proyecto proyecto) throws Exception {
    this.proyectoBeanBusiness.updateProyecto(proyecto);
  }

  public void deleteProyecto(Proyecto proyecto) throws Exception {
    this.proyectoBeanBusiness.deleteProyecto(proyecto);
  }

  public Proyecto findProyectoById(long proyectoId) throws Exception {
    return this.proyectoBeanBusiness.findProyectoById(proyectoId);
  }

  public Collection findAllProyecto() throws Exception {
    return this.proyectoBeanBusiness.findProyectoAll();
  }

  public Collection findProyectoByAnio(int anio, long idOrganismo)
    throws Exception
  {
    return this.proyectoBeanBusiness.findByAnio(anio, idOrganismo);
  }

  public Collection findProyectoByCodProyecto(String codProyecto, long idOrganismo)
    throws Exception
  {
    return this.proyectoBeanBusiness.findByCodProyecto(codProyecto, idOrganismo);
  }

  public Collection findProyectoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.proyectoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addCuentaContable(CuentaContable cuentaContable)
    throws Exception
  {
    this.cuentaContableBeanBusiness.addCuentaContable(cuentaContable);
  }

  public void updateCuentaContable(CuentaContable cuentaContable) throws Exception {
    this.cuentaContableBeanBusiness.updateCuentaContable(cuentaContable);
  }

  public void deleteCuentaContable(CuentaContable cuentaContable) throws Exception {
    this.cuentaContableBeanBusiness.deleteCuentaContable(cuentaContable);
  }

  public CuentaContable findCuentaContableById(long cuentaContableId) throws Exception {
    return this.cuentaContableBeanBusiness.findCuentaContableById(cuentaContableId);
  }

  public Collection findAllCuentaContable() throws Exception {
    return this.cuentaContableBeanBusiness.findCuentaContableAll();
  }

  public Collection findCuentaContableByCodContable(String codContable, long idOrganismo)
    throws Exception
  {
    return this.cuentaContableBeanBusiness.findByCodContable(codContable, idOrganismo);
  }

  public Collection findCuentaContableByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    return this.cuentaContableBeanBusiness.findByDescripcion(descripcion, idOrganismo);
  }

  public Collection findCuentaContableByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.cuentaContableBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addConceptoContable(ConceptoContable conceptoContable)
    throws Exception
  {
    this.conceptoContableBeanBusiness.addConceptoContable(conceptoContable);
  }

  public void updateConceptoContable(ConceptoContable conceptoContable) throws Exception {
    this.conceptoContableBeanBusiness.updateConceptoContable(conceptoContable);
  }

  public void deleteConceptoContable(ConceptoContable conceptoContable) throws Exception {
    this.conceptoContableBeanBusiness.deleteConceptoContable(conceptoContable);
  }

  public ConceptoContable findConceptoContableById(long conceptoContableId) throws Exception {
    return this.conceptoContableBeanBusiness.findConceptoContableById(conceptoContableId);
  }

  public Collection findAllConceptoContable() throws Exception {
    return this.conceptoContableBeanBusiness.findConceptoContableAll();
  }

  public Collection findConceptoContableByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.conceptoContableBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findConceptoContableByCuentaContable(long idCuentaContable)
    throws Exception
  {
    return this.conceptoContableBeanBusiness.findByCuentaContable(idCuentaContable);
  }

  public void addUelEspecifica(UelEspecifica uelEspecifica)
    throws Exception
  {
    this.uelEspecificaBeanBusiness.addUelEspecifica(uelEspecifica);
  }

  public void updateUelEspecifica(UelEspecifica uelEspecifica) throws Exception {
    this.uelEspecificaBeanBusiness.updateUelEspecifica(uelEspecifica);
  }

  public void deleteUelEspecifica(UelEspecifica uelEspecifica) throws Exception {
    this.uelEspecificaBeanBusiness.deleteUelEspecifica(uelEspecifica);
  }

  public UelEspecifica findUelEspecificaById(long uelEspecificaId) throws Exception {
    return this.uelEspecificaBeanBusiness.findUelEspecificaById(uelEspecificaId);
  }

  public Collection findAllUelEspecifica() throws Exception {
    return this.uelEspecificaBeanBusiness.findUelEspecificaAll();
  }

  public Collection findUelEspecificaByAccionEspecifica(long idAccionEspecifica)
    throws Exception
  {
    return this.uelEspecificaBeanBusiness.findByAccionEspecifica(idAccionEspecifica);
  }

  public Collection findUelEspecificaByUnidadEjecutora(long idUnidadEjecutora)
    throws Exception
  {
    return this.uelEspecificaBeanBusiness.findByUnidadEjecutora(idUnidadEjecutora);
  }

  public Collection findUelEspecificaByCategoriaPresupuesto(String categoriaPresupuesto)
    throws Exception
  {
    return this.uelEspecificaBeanBusiness.findByCategoriaPresupuesto(categoriaPresupuesto);
  }

  public Collection findUelEspecificaByAnio(int anio)
    throws Exception
  {
    return this.uelEspecificaBeanBusiness.findByAnio(anio);
  }

  public void addConceptoCuenta(ConceptoCuenta conceptoCuenta)
    throws Exception
  {
    this.conceptoCuentaBeanBusiness.addConceptoCuenta(conceptoCuenta);
  }

  public void updateConceptoCuenta(ConceptoCuenta conceptoCuenta) throws Exception {
    this.conceptoCuentaBeanBusiness.updateConceptoCuenta(conceptoCuenta);
  }

  public void deleteConceptoCuenta(ConceptoCuenta conceptoCuenta) throws Exception {
    this.conceptoCuentaBeanBusiness.deleteConceptoCuenta(conceptoCuenta);
  }

  public ConceptoCuenta findConceptoCuentaById(long conceptoCuentaId) throws Exception {
    return this.conceptoCuentaBeanBusiness.findConceptoCuentaById(conceptoCuentaId);
  }

  public Collection findAllConceptoCuenta() throws Exception {
    return this.conceptoCuentaBeanBusiness.findConceptoCuentaAll();
  }

  public Collection findConceptoCuentaByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.conceptoCuentaBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findConceptoCuentaByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    return this.conceptoCuentaBeanBusiness.findByCuentaPresupuesto(idCuentaPresupuesto);
  }

  public void addCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto)
    throws Exception
  {
    this.cuentaPresupuestoBeanBusiness.addCuentaPresupuesto(cuentaPresupuesto);
  }

  public void updateCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) throws Exception {
    this.cuentaPresupuestoBeanBusiness.updateCuentaPresupuesto(cuentaPresupuesto);
  }

  public void deleteCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) throws Exception {
    this.cuentaPresupuestoBeanBusiness.deleteCuentaPresupuesto(cuentaPresupuesto);
  }

  public CuentaPresupuesto findCuentaPresupuestoById(long cuentaPresupuestoId) throws Exception {
    return this.cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(cuentaPresupuestoId);
  }

  public Collection findAllCuentaPresupuesto() throws Exception {
    return this.cuentaPresupuestoBeanBusiness.findCuentaPresupuestoAll();
  }

  public Collection findCuentaPresupuestoByCodPresupuesto(String codPresupuesto, long idOrganismo)
    throws Exception
  {
    return this.cuentaPresupuestoBeanBusiness.findByCodPresupuesto(codPresupuesto, idOrganismo);
  }

  public Collection findCuentaPresupuestoByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    return this.cuentaPresupuestoBeanBusiness.findByDescripcion(descripcion, idOrganismo);
  }

  public Collection findCuentaPresupuestoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.cuentaPresupuestoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica)
    throws Exception
  {
    this.trabajadorEspecificaBeanBusiness.addTrabajadorEspecifica(trabajadorEspecifica);
  }

  public void updateTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica) throws Exception {
    this.trabajadorEspecificaBeanBusiness.updateTrabajadorEspecifica(trabajadorEspecifica);
  }

  public void deleteTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica) throws Exception {
    this.trabajadorEspecificaBeanBusiness.deleteTrabajadorEspecifica(trabajadorEspecifica);
  }

  public TrabajadorEspecifica findTrabajadorEspecificaById(long trabajadorEspecificaId) throws Exception {
    return this.trabajadorEspecificaBeanBusiness.findTrabajadorEspecificaById(trabajadorEspecificaId);
  }

  public Collection findAllTrabajadorEspecifica() throws Exception {
    return this.trabajadorEspecificaBeanBusiness.findTrabajadorEspecificaAll();
  }

  public Collection findTrabajadorEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    return this.trabajadorEspecificaBeanBusiness.findByUelEspecifica(idUelEspecifica);
  }

  public Collection findTrabajadorEspecificaByAnio(int anio)
    throws Exception
  {
    return this.trabajadorEspecificaBeanBusiness.findByAnio(anio);
  }

  public void addEjecucionMensual(EjecucionMensual ejecucionMensual)
    throws Exception
  {
    this.ejecucionMensualBeanBusiness.addEjecucionMensual(ejecucionMensual);
  }

  public void updateEjecucionMensual(EjecucionMensual ejecucionMensual) throws Exception {
    this.ejecucionMensualBeanBusiness.updateEjecucionMensual(ejecucionMensual);
  }

  public void deleteEjecucionMensual(EjecucionMensual ejecucionMensual) throws Exception {
    this.ejecucionMensualBeanBusiness.deleteEjecucionMensual(ejecucionMensual);
  }

  public EjecucionMensual findEjecucionMensualById(long ejecucionMensualId) throws Exception {
    return this.ejecucionMensualBeanBusiness.findEjecucionMensualById(ejecucionMensualId);
  }

  public Collection findAllEjecucionMensual() throws Exception {
    return this.ejecucionMensualBeanBusiness.findEjecucionMensualAll();
  }

  public Collection findEjecucionMensualByPresupuestoEspecifica(long idPresupuestoEspecifica)
    throws Exception
  {
    return this.ejecucionMensualBeanBusiness.findByPresupuestoEspecifica(idPresupuestoEspecifica);
  }

  public Collection findEjecucionMensualByAnio(int anio)
    throws Exception
  {
    return this.ejecucionMensualBeanBusiness.findByAnio(anio);
  }

  public Collection findEjecucionMensualByMes(int mes)
    throws Exception
  {
    return this.ejecucionMensualBeanBusiness.findByMes(mes);
  }

  public void addFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento)
    throws Exception
  {
    this.fuenteFinanciamientoBeanBusiness.addFuenteFinanciamiento(fuenteFinanciamiento);
  }

  public void updateFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento) throws Exception {
    this.fuenteFinanciamientoBeanBusiness.updateFuenteFinanciamiento(fuenteFinanciamiento);
  }

  public void deleteFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento) throws Exception {
    this.fuenteFinanciamientoBeanBusiness.deleteFuenteFinanciamiento(fuenteFinanciamiento);
  }

  public FuenteFinanciamiento findFuenteFinanciamientoById(long fuenteFinanciamientoId) throws Exception {
    return this.fuenteFinanciamientoBeanBusiness.findFuenteFinanciamientoById(fuenteFinanciamientoId);
  }

  public Collection findAllFuenteFinanciamiento() throws Exception {
    return this.fuenteFinanciamientoBeanBusiness.findFuenteFinanciamientoAll();
  }

  public Collection findFuenteFinanciamientoByCodFuenteFinanciamiento(String codFuenteFinanciamiento)
    throws Exception
  {
    return this.fuenteFinanciamientoBeanBusiness.findByCodFuenteFinanciamiento(codFuenteFinanciamiento);
  }

  public Collection findFuenteFinanciamientoByNombre(String nombre)
    throws Exception
  {
    return this.fuenteFinanciamientoBeanBusiness.findByNombre(nombre);
  }

  public void addPartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica)
    throws Exception
  {
    this.partidaUelEspecificaBeanBusiness.addPartidaUelEspecifica(partidaUelEspecifica);
  }

  public void updatePartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica) throws Exception {
    this.partidaUelEspecificaBeanBusiness.updatePartidaUelEspecifica(partidaUelEspecifica);
  }

  public void deletePartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica) throws Exception {
    this.partidaUelEspecificaBeanBusiness.deletePartidaUelEspecifica(partidaUelEspecifica);
  }

  public PartidaUelEspecifica findPartidaUelEspecificaById(long partidaUelEspecificaId) throws Exception {
    return this.partidaUelEspecificaBeanBusiness.findPartidaUelEspecificaById(partidaUelEspecificaId);
  }

  public Collection findAllPartidaUelEspecifica() throws Exception {
    return this.partidaUelEspecificaBeanBusiness.findPartidaUelEspecificaAll();
  }

  public Collection findPartidaUelEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    return this.partidaUelEspecificaBeanBusiness.findByUelEspecifica(idUelEspecifica);
  }

  public Collection findPartidaUelEspecificaByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    return this.partidaUelEspecificaBeanBusiness.findByCuentaPresupuesto(idCuentaPresupuesto);
  }

  public Collection findPartidaUelEspecificaByFuenteFinanciamiento(long idFuenteFinanciamiento)
    throws Exception
  {
    return this.partidaUelEspecificaBeanBusiness.findByFuenteFinanciamiento(idFuenteFinanciamiento);
  }

  public Collection findPartidaUelEspecificaByAnio(int anio)
    throws Exception
  {
    return this.partidaUelEspecificaBeanBusiness.findByAnio(anio);
  }

  public void addConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable)
    throws Exception
  {
    this.conceptoCuentaContableBeanBusiness.addConceptoCuentaContable(conceptoCuentaContable);
  }

  public void updateConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable) throws Exception {
    this.conceptoCuentaContableBeanBusiness.updateConceptoCuentaContable(conceptoCuentaContable);
  }

  public void deleteConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable) throws Exception {
    this.conceptoCuentaContableBeanBusiness.deleteConceptoCuentaContable(conceptoCuentaContable);
  }

  public ConceptoCuentaContable findConceptoCuentaContableById(long conceptoCuentaContableId) throws Exception {
    return this.conceptoCuentaContableBeanBusiness.findConceptoCuentaContableById(conceptoCuentaContableId);
  }

  public Collection findAllConceptoCuentaContable() throws Exception {
    return this.conceptoCuentaContableBeanBusiness.findConceptoCuentaContableAll();
  }

  public Collection findConceptoCuentaContableByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.conceptoCuentaContableBeanBusiness.findByConceptoTipoPersonal(idConceptoTipoPersonal);
  }

  public Collection findConceptoCuentaContableByCuentaContable(long idCuentaContable)
    throws Exception
  {
    return this.conceptoCuentaContableBeanBusiness.findByCuentaContable(idCuentaContable);
  }
}