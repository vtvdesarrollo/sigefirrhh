package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.personal.trabajador.Trabajador;

public class TrabajadorEspecifica
  implements Serializable, PersistenceCapable
{
  private long idTrabajadorEspecifica;
  private UelEspecifica uelEspecifica;
  private Trabajador trabajador;
  private int anio;
  private double porcentaje;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "idTrabajadorEspecifica", "porcentaje", "trabajador", "uelEspecifica" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), sunjdo$classForName$("sigefirrhh.personal.sigecof.UelEspecifica") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public TrabajadorEspecifica()
  {
    jdoSetporcentaje(this, 100.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetporcentaje(this));

    return jdoGetuelEspecifica(this).getCategoriaPresupuesto() + " - " + jdoGetuelEspecifica(this).getUnidadEjecutora().getCodUnidadEjecutora() + " - " + a + "%";
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public long getIdTrabajadorEspecifica() {
    return jdoGetidTrabajadorEspecifica(this);
  }
  public void setIdTrabajadorEspecifica(long idTrabajadorEspecifica) {
    jdoSetidTrabajadorEspecifica(this, idTrabajadorEspecifica);
  }
  public double getPorcentaje() {
    return jdoGetporcentaje(this);
  }
  public void setPorcentaje(double porcentaje) {
    jdoSetporcentaje(this, porcentaje);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }
  public UelEspecifica getUelEspecifica() {
    return jdoGetuelEspecifica(this);
  }
  public void setUelEspecifica(UelEspecifica uelEspecifica) {
    jdoSetuelEspecifica(this, uelEspecifica);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.TrabajadorEspecifica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TrabajadorEspecifica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TrabajadorEspecifica localTrabajadorEspecifica = new TrabajadorEspecifica();
    localTrabajadorEspecifica.jdoFlags = 1;
    localTrabajadorEspecifica.jdoStateManager = paramStateManager;
    return localTrabajadorEspecifica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TrabajadorEspecifica localTrabajadorEspecifica = new TrabajadorEspecifica();
    localTrabajadorEspecifica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTrabajadorEspecifica.jdoFlags = 1;
    localTrabajadorEspecifica.jdoStateManager = paramStateManager;
    return localTrabajadorEspecifica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTrabajadorEspecifica);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.uelEspecifica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTrabajadorEspecifica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.uelEspecifica = ((UelEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TrabajadorEspecifica paramTrabajadorEspecifica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTrabajadorEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramTrabajadorEspecifica.anio;
      return;
    case 1:
      if (paramTrabajadorEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.idTrabajadorEspecifica = paramTrabajadorEspecifica.idTrabajadorEspecifica;
      return;
    case 2:
      if (paramTrabajadorEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramTrabajadorEspecifica.porcentaje;
      return;
    case 3:
      if (paramTrabajadorEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramTrabajadorEspecifica.trabajador;
      return;
    case 4:
      if (paramTrabajadorEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.uelEspecifica = paramTrabajadorEspecifica.uelEspecifica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TrabajadorEspecifica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TrabajadorEspecifica localTrabajadorEspecifica = (TrabajadorEspecifica)paramObject;
    if (localTrabajadorEspecifica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTrabajadorEspecifica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TrabajadorEspecificaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TrabajadorEspecificaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorEspecificaPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorEspecificaPK localTrabajadorEspecificaPK = (TrabajadorEspecificaPK)paramObject;
    localTrabajadorEspecificaPK.idTrabajadorEspecifica = this.idTrabajadorEspecifica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TrabajadorEspecificaPK))
      throw new IllegalArgumentException("arg1");
    TrabajadorEspecificaPK localTrabajadorEspecificaPK = (TrabajadorEspecificaPK)paramObject;
    this.idTrabajadorEspecifica = localTrabajadorEspecificaPK.idTrabajadorEspecifica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorEspecificaPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorEspecificaPK localTrabajadorEspecificaPK = (TrabajadorEspecificaPK)paramObject;
    localTrabajadorEspecificaPK.idTrabajadorEspecifica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TrabajadorEspecificaPK))
      throw new IllegalArgumentException("arg2");
    TrabajadorEspecificaPK localTrabajadorEspecificaPK = (TrabajadorEspecificaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTrabajadorEspecificaPK.idTrabajadorEspecifica);
  }

  private static final int jdoGetanio(TrabajadorEspecifica paramTrabajadorEspecifica)
  {
    if (paramTrabajadorEspecifica.jdoFlags <= 0)
      return paramTrabajadorEspecifica.anio;
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorEspecifica.anio;
    if (localStateManager.isLoaded(paramTrabajadorEspecifica, jdoInheritedFieldCount + 0))
      return paramTrabajadorEspecifica.anio;
    return localStateManager.getIntField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 0, paramTrabajadorEspecifica.anio);
  }

  private static final void jdoSetanio(TrabajadorEspecifica paramTrabajadorEspecifica, int paramInt)
  {
    if (paramTrabajadorEspecifica.jdoFlags == 0)
    {
      paramTrabajadorEspecifica.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorEspecifica.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 0, paramTrabajadorEspecifica.anio, paramInt);
  }

  private static final long jdoGetidTrabajadorEspecifica(TrabajadorEspecifica paramTrabajadorEspecifica)
  {
    return paramTrabajadorEspecifica.idTrabajadorEspecifica;
  }

  private static final void jdoSetidTrabajadorEspecifica(TrabajadorEspecifica paramTrabajadorEspecifica, long paramLong)
  {
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorEspecifica.idTrabajadorEspecifica = paramLong;
      return;
    }
    localStateManager.setLongField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 1, paramTrabajadorEspecifica.idTrabajadorEspecifica, paramLong);
  }

  private static final double jdoGetporcentaje(TrabajadorEspecifica paramTrabajadorEspecifica)
  {
    if (paramTrabajadorEspecifica.jdoFlags <= 0)
      return paramTrabajadorEspecifica.porcentaje;
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorEspecifica.porcentaje;
    if (localStateManager.isLoaded(paramTrabajadorEspecifica, jdoInheritedFieldCount + 2))
      return paramTrabajadorEspecifica.porcentaje;
    return localStateManager.getDoubleField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 2, paramTrabajadorEspecifica.porcentaje);
  }

  private static final void jdoSetporcentaje(TrabajadorEspecifica paramTrabajadorEspecifica, double paramDouble)
  {
    if (paramTrabajadorEspecifica.jdoFlags == 0)
    {
      paramTrabajadorEspecifica.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorEspecifica.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 2, paramTrabajadorEspecifica.porcentaje, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(TrabajadorEspecifica paramTrabajadorEspecifica)
  {
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorEspecifica.trabajador;
    if (localStateManager.isLoaded(paramTrabajadorEspecifica, jdoInheritedFieldCount + 3))
      return paramTrabajadorEspecifica.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 3, paramTrabajadorEspecifica.trabajador);
  }

  private static final void jdoSettrabajador(TrabajadorEspecifica paramTrabajadorEspecifica, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorEspecifica.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 3, paramTrabajadorEspecifica.trabajador, paramTrabajador);
  }

  private static final UelEspecifica jdoGetuelEspecifica(TrabajadorEspecifica paramTrabajadorEspecifica)
  {
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramTrabajadorEspecifica.uelEspecifica;
    if (localStateManager.isLoaded(paramTrabajadorEspecifica, jdoInheritedFieldCount + 4))
      return paramTrabajadorEspecifica.uelEspecifica;
    return (UelEspecifica)localStateManager.getObjectField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 4, paramTrabajadorEspecifica.uelEspecifica);
  }

  private static final void jdoSetuelEspecifica(TrabajadorEspecifica paramTrabajadorEspecifica, UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramTrabajadorEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramTrabajadorEspecifica.uelEspecifica = paramUelEspecifica;
      return;
    }
    localStateManager.setObjectField(paramTrabajadorEspecifica, jdoInheritedFieldCount + 4, paramTrabajadorEspecifica.uelEspecifica, paramUelEspecifica);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}