package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class ProyectoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addProyecto(Proyecto proyecto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Proyecto proyectoNew = 
      (Proyecto)BeanUtils.cloneBean(
      proyecto);

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (proyectoNew.getCiudad() != null) {
      proyectoNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        proyectoNew.getCiudad().getIdCiudad()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (proyectoNew.getOrganismo() != null) {
      proyectoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        proyectoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(proyectoNew);
  }

  public void updateProyecto(Proyecto proyecto) throws Exception
  {
    Proyecto proyectoModify = 
      findProyectoById(proyecto.getIdProyecto());

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (proyecto.getCiudad() != null) {
      proyecto.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        proyecto.getCiudad().getIdCiudad()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (proyecto.getOrganismo() != null) {
      proyecto.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        proyecto.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(proyectoModify, proyecto);
  }

  public void deleteProyecto(Proyecto proyecto) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Proyecto proyectoDelete = 
      findProyectoById(proyecto.getIdProyecto());
    pm.deletePersistent(proyectoDelete);
  }

  public Proyecto findProyectoById(long idProyecto) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idProyecto == pIdProyecto";
    Query query = pm.newQuery(Proyecto.class, filter);

    query.declareParameters("long pIdProyecto");

    parameters.put("pIdProyecto", new Long(idProyecto));

    Collection colProyecto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colProyecto.iterator();
    return (Proyecto)iterator.next();
  }

  public Collection findProyectoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent proyectoExtent = pm.getExtent(
      Proyecto.class, true);
    Query query = pm.newQuery(proyectoExtent);
    query.setOrdering("anio ascending, codProyecto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Proyecto.class, filter);

    query.declareParameters("int pAnio, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio ascending, codProyecto ascending");

    Collection colProyecto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProyecto);

    return colProyecto;
  }

  public Collection findByCodProyecto(String codProyecto, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codProyecto == pCodProyecto &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Proyecto.class, filter);

    query.declareParameters("java.lang.String pCodProyecto, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodProyecto", new String(codProyecto));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio ascending, codProyecto ascending");

    Collection colProyecto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProyecto);

    return colProyecto;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Proyecto.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio ascending, codProyecto ascending");

    Collection colProyecto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProyecto);

    return colProyecto;
  }
}