package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.ubicacion.Ciudad;

public class Proyecto
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idProyecto;
  private int anio;
  private String codProyecto;
  private String enunciado;
  private String descripcion;
  private String objetivoEstrategico;
  private String objetivosEspecificos;
  private String resultado;
  private double meta;
  private String estatus;
  private Date fechaInicio;
  private Date fechaFin;
  private Date fechaSuspension;
  private Ciudad ciudad;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "ciudad", "codProyecto", "descripcion", "enunciado", "estatus", "fechaFin", "fechaInicio", "fechaSuspension", "idProyecto", "meta", "objetivoEstrategico", "objetivosEspecificos", "organismo", "resultado" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.Proyecto"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Proyecto());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("I", "POR INICIAR");
    LISTA_ESTATUS.put("R", "REACTIVAR");
    LISTA_ESTATUS.put("E", "EN EJECUCION");
    LISTA_ESTATUS.put("F", "FINALIZADO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
  }

  public Proyecto()
  {
    jdoSetmeta(this, 0.0D);

    jdoSetestatus(this, "I");
  }

  public String toString()
  {
    return jdoGetcodProyecto(this) + " - " + jdoGetenunciado(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public Ciudad getCiudad()
  {
    return jdoGetciudad(this);
  }

  public String getCodProyecto()
  {
    return jdoGetcodProyecto(this);
  }

  public String getEnunciado()
  {
    return jdoGetenunciado(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public long getIdProyecto()
  {
    return jdoGetidProyecto(this);
  }

  public double getMeta()
  {
    return jdoGetmeta(this);
  }

  public String getObjetivoEstrategico()
  {
    return jdoGetobjetivoEstrategico(this);
  }

  public String getObjetivosEspecificos()
  {
    return jdoGetobjetivosEspecificos(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public String getResultado()
  {
    return jdoGetresultado(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setCiudad(Ciudad ciudad)
  {
    jdoSetciudad(this, ciudad);
  }

  public void setCodProyecto(String string)
  {
    jdoSetcodProyecto(this, string);
  }

  public void setEnunciado(String string)
  {
    jdoSetenunciado(this, string);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setIdProyecto(long l)
  {
    jdoSetidProyecto(this, l);
  }

  public void setMeta(double d)
  {
    jdoSetmeta(this, d);
  }

  public void setObjetivoEstrategico(String string)
  {
    jdoSetobjetivoEstrategico(this, string);
  }

  public void setObjetivosEspecificos(String string)
  {
    jdoSetobjetivosEspecificos(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setResultado(String string)
  {
    jdoSetresultado(this, string);
  }

  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }
  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }
  public Date getFechaSuspension() {
    return jdoGetfechaSuspension(this);
  }
  public void setFechaSuspension(Date fechaSuspension) {
    jdoSetfechaSuspension(this, fechaSuspension);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Proyecto localProyecto = new Proyecto();
    localProyecto.jdoFlags = 1;
    localProyecto.jdoStateManager = paramStateManager;
    return localProyecto;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Proyecto localProyecto = new Proyecto();
    localProyecto.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProyecto.jdoFlags = 1;
    localProyecto.jdoStateManager = paramStateManager;
    return localProyecto;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codProyecto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.enunciado);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaSuspension);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProyecto);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.meta);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.objetivoEstrategico);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.objetivosEspecificos);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.resultado);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codProyecto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.enunciado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaSuspension = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProyecto = localStateManager.replacingLongField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.meta = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.objetivoEstrategico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.objetivosEspecificos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultado = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Proyecto paramProyecto, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramProyecto.anio;
      return;
    case 1:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramProyecto.ciudad;
      return;
    case 2:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.codProyecto = paramProyecto.codProyecto;
      return;
    case 3:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramProyecto.descripcion;
      return;
    case 4:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.enunciado = paramProyecto.enunciado;
      return;
    case 5:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramProyecto.estatus;
      return;
    case 6:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramProyecto.fechaFin;
      return;
    case 7:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramProyecto.fechaInicio;
      return;
    case 8:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.fechaSuspension = paramProyecto.fechaSuspension;
      return;
    case 9:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.idProyecto = paramProyecto.idProyecto;
      return;
    case 10:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.meta = paramProyecto.meta;
      return;
    case 11:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.objetivoEstrategico = paramProyecto.objetivoEstrategico;
      return;
    case 12:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.objetivosEspecificos = paramProyecto.objetivosEspecificos;
      return;
    case 13:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramProyecto.organismo;
      return;
    case 14:
      if (paramProyecto == null)
        throw new IllegalArgumentException("arg1");
      this.resultado = paramProyecto.resultado;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Proyecto))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Proyecto localProyecto = (Proyecto)paramObject;
    if (localProyecto.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProyecto, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProyectoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProyectoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProyectoPK))
      throw new IllegalArgumentException("arg1");
    ProyectoPK localProyectoPK = (ProyectoPK)paramObject;
    localProyectoPK.idProyecto = this.idProyecto;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProyectoPK))
      throw new IllegalArgumentException("arg1");
    ProyectoPK localProyectoPK = (ProyectoPK)paramObject;
    this.idProyecto = localProyectoPK.idProyecto;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProyectoPK))
      throw new IllegalArgumentException("arg2");
    ProyectoPK localProyectoPK = (ProyectoPK)paramObject;
    localProyectoPK.idProyecto = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 9);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProyectoPK))
      throw new IllegalArgumentException("arg2");
    ProyectoPK localProyectoPK = (ProyectoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 9, localProyectoPK.idProyecto);
  }

  private static final int jdoGetanio(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.anio;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.anio;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 0))
      return paramProyecto.anio;
    return localStateManager.getIntField(paramProyecto, jdoInheritedFieldCount + 0, paramProyecto.anio);
  }

  private static final void jdoSetanio(Proyecto paramProyecto, int paramInt)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramProyecto, jdoInheritedFieldCount + 0, paramProyecto.anio, paramInt);
  }

  private static final Ciudad jdoGetciudad(Proyecto paramProyecto)
  {
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.ciudad;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 1))
      return paramProyecto.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramProyecto, jdoInheritedFieldCount + 1, paramProyecto.ciudad);
  }

  private static final void jdoSetciudad(Proyecto paramProyecto, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramProyecto, jdoInheritedFieldCount + 1, paramProyecto.ciudad, paramCiudad);
  }

  private static final String jdoGetcodProyecto(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.codProyecto;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.codProyecto;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 2))
      return paramProyecto.codProyecto;
    return localStateManager.getStringField(paramProyecto, jdoInheritedFieldCount + 2, paramProyecto.codProyecto);
  }

  private static final void jdoSetcodProyecto(Proyecto paramProyecto, String paramString)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.codProyecto = paramString;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.codProyecto = paramString;
      return;
    }
    localStateManager.setStringField(paramProyecto, jdoInheritedFieldCount + 2, paramProyecto.codProyecto, paramString);
  }

  private static final String jdoGetdescripcion(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.descripcion;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.descripcion;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 3))
      return paramProyecto.descripcion;
    return localStateManager.getStringField(paramProyecto, jdoInheritedFieldCount + 3, paramProyecto.descripcion);
  }

  private static final void jdoSetdescripcion(Proyecto paramProyecto, String paramString)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramProyecto, jdoInheritedFieldCount + 3, paramProyecto.descripcion, paramString);
  }

  private static final String jdoGetenunciado(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.enunciado;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.enunciado;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 4))
      return paramProyecto.enunciado;
    return localStateManager.getStringField(paramProyecto, jdoInheritedFieldCount + 4, paramProyecto.enunciado);
  }

  private static final void jdoSetenunciado(Proyecto paramProyecto, String paramString)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.enunciado = paramString;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.enunciado = paramString;
      return;
    }
    localStateManager.setStringField(paramProyecto, jdoInheritedFieldCount + 4, paramProyecto.enunciado, paramString);
  }

  private static final String jdoGetestatus(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.estatus;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.estatus;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 5))
      return paramProyecto.estatus;
    return localStateManager.getStringField(paramProyecto, jdoInheritedFieldCount + 5, paramProyecto.estatus);
  }

  private static final void jdoSetestatus(Proyecto paramProyecto, String paramString)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramProyecto, jdoInheritedFieldCount + 5, paramProyecto.estatus, paramString);
  }

  private static final Date jdoGetfechaFin(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.fechaFin;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.fechaFin;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 6))
      return paramProyecto.fechaFin;
    return (Date)localStateManager.getObjectField(paramProyecto, jdoInheritedFieldCount + 6, paramProyecto.fechaFin);
  }

  private static final void jdoSetfechaFin(Proyecto paramProyecto, Date paramDate)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProyecto, jdoInheritedFieldCount + 6, paramProyecto.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.fechaInicio;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.fechaInicio;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 7))
      return paramProyecto.fechaInicio;
    return (Date)localStateManager.getObjectField(paramProyecto, jdoInheritedFieldCount + 7, paramProyecto.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Proyecto paramProyecto, Date paramDate)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProyecto, jdoInheritedFieldCount + 7, paramProyecto.fechaInicio, paramDate);
  }

  private static final Date jdoGetfechaSuspension(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.fechaSuspension;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.fechaSuspension;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 8))
      return paramProyecto.fechaSuspension;
    return (Date)localStateManager.getObjectField(paramProyecto, jdoInheritedFieldCount + 8, paramProyecto.fechaSuspension);
  }

  private static final void jdoSetfechaSuspension(Proyecto paramProyecto, Date paramDate)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.fechaSuspension = paramDate;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.fechaSuspension = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProyecto, jdoInheritedFieldCount + 8, paramProyecto.fechaSuspension, paramDate);
  }

  private static final long jdoGetidProyecto(Proyecto paramProyecto)
  {
    return paramProyecto.idProyecto;
  }

  private static final void jdoSetidProyecto(Proyecto paramProyecto, long paramLong)
  {
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.idProyecto = paramLong;
      return;
    }
    localStateManager.setLongField(paramProyecto, jdoInheritedFieldCount + 9, paramProyecto.idProyecto, paramLong);
  }

  private static final double jdoGetmeta(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.meta;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.meta;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 10))
      return paramProyecto.meta;
    return localStateManager.getDoubleField(paramProyecto, jdoInheritedFieldCount + 10, paramProyecto.meta);
  }

  private static final void jdoSetmeta(Proyecto paramProyecto, double paramDouble)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.meta = paramDouble;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.meta = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramProyecto, jdoInheritedFieldCount + 10, paramProyecto.meta, paramDouble);
  }

  private static final String jdoGetobjetivoEstrategico(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.objetivoEstrategico;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.objetivoEstrategico;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 11))
      return paramProyecto.objetivoEstrategico;
    return localStateManager.getStringField(paramProyecto, jdoInheritedFieldCount + 11, paramProyecto.objetivoEstrategico);
  }

  private static final void jdoSetobjetivoEstrategico(Proyecto paramProyecto, String paramString)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.objetivoEstrategico = paramString;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.objetivoEstrategico = paramString;
      return;
    }
    localStateManager.setStringField(paramProyecto, jdoInheritedFieldCount + 11, paramProyecto.objetivoEstrategico, paramString);
  }

  private static final String jdoGetobjetivosEspecificos(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.objetivosEspecificos;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.objetivosEspecificos;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 12))
      return paramProyecto.objetivosEspecificos;
    return localStateManager.getStringField(paramProyecto, jdoInheritedFieldCount + 12, paramProyecto.objetivosEspecificos);
  }

  private static final void jdoSetobjetivosEspecificos(Proyecto paramProyecto, String paramString)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.objetivosEspecificos = paramString;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.objetivosEspecificos = paramString;
      return;
    }
    localStateManager.setStringField(paramProyecto, jdoInheritedFieldCount + 12, paramProyecto.objetivosEspecificos, paramString);
  }

  private static final Organismo jdoGetorganismo(Proyecto paramProyecto)
  {
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.organismo;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 13))
      return paramProyecto.organismo;
    return (Organismo)localStateManager.getObjectField(paramProyecto, jdoInheritedFieldCount + 13, paramProyecto.organismo);
  }

  private static final void jdoSetorganismo(Proyecto paramProyecto, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramProyecto, jdoInheritedFieldCount + 13, paramProyecto.organismo, paramOrganismo);
  }

  private static final String jdoGetresultado(Proyecto paramProyecto)
  {
    if (paramProyecto.jdoFlags <= 0)
      return paramProyecto.resultado;
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
      return paramProyecto.resultado;
    if (localStateManager.isLoaded(paramProyecto, jdoInheritedFieldCount + 14))
      return paramProyecto.resultado;
    return localStateManager.getStringField(paramProyecto, jdoInheritedFieldCount + 14, paramProyecto.resultado);
  }

  private static final void jdoSetresultado(Proyecto paramProyecto, String paramString)
  {
    if (paramProyecto.jdoFlags == 0)
    {
      paramProyecto.resultado = paramString;
      return;
    }
    StateManager localStateManager = paramProyecto.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyecto.resultado = paramString;
      return;
    }
    localStateManager.setStringField(paramProyecto, jdoInheritedFieldCount + 14, paramProyecto.resultado, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}