package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class FuenteFinanciamientoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(FuenteFinanciamientoForm.class.getName());
  private FuenteFinanciamiento fuenteFinanciamiento;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showFuenteFinanciamientoByCodFuenteFinanciamiento;
  private boolean showFuenteFinanciamientoByNombre;
  private String findCodFuenteFinanciamiento;
  private String findNombre;
  private Object stateResultFuenteFinanciamientoByCodFuenteFinanciamiento = null;

  private Object stateResultFuenteFinanciamientoByNombre = null;

  public String getFindCodFuenteFinanciamiento()
  {
    return this.findCodFuenteFinanciamiento;
  }
  public void setFindCodFuenteFinanciamiento(String findCodFuenteFinanciamiento) {
    this.findCodFuenteFinanciamiento = findCodFuenteFinanciamiento;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public FuenteFinanciamiento getFuenteFinanciamiento() {
    if (this.fuenteFinanciamiento == null) {
      this.fuenteFinanciamiento = new FuenteFinanciamiento();
    }
    return this.fuenteFinanciamiento;
  }

  public FuenteFinanciamientoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findFuenteFinanciamientoByCodFuenteFinanciamiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findFuenteFinanciamientoByCodFuenteFinanciamiento(this.findCodFuenteFinanciamiento);
      this.showFuenteFinanciamientoByCodFuenteFinanciamiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showFuenteFinanciamientoByCodFuenteFinanciamiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodFuenteFinanciamiento = null;
    this.findNombre = null;

    return null;
  }

  public String findFuenteFinanciamientoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findFuenteFinanciamientoByNombre(this.findNombre);
      this.showFuenteFinanciamientoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showFuenteFinanciamientoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodFuenteFinanciamiento = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowFuenteFinanciamientoByCodFuenteFinanciamiento() {
    return this.showFuenteFinanciamientoByCodFuenteFinanciamiento;
  }
  public boolean isShowFuenteFinanciamientoByNombre() {
    return this.showFuenteFinanciamientoByNombre;
  }

  public String selectFuenteFinanciamiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idFuenteFinanciamiento = 
      Long.parseLong((String)requestParameterMap.get("idFuenteFinanciamiento"));
    try
    {
      this.fuenteFinanciamiento = 
        this.sigecofFacade.findFuenteFinanciamientoById(
        idFuenteFinanciamiento);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.fuenteFinanciamiento = null;
    this.showFuenteFinanciamientoByCodFuenteFinanciamiento = false;
    this.showFuenteFinanciamientoByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addFuenteFinanciamiento(
          this.fuenteFinanciamiento);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateFuenteFinanciamiento(
          this.fuenteFinanciamiento);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteFuenteFinanciamiento(
        this.fuenteFinanciamiento);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.fuenteFinanciamiento = new FuenteFinanciamiento();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.fuenteFinanciamiento.setIdFuenteFinanciamiento(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.FuenteFinanciamiento"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.fuenteFinanciamiento = new FuenteFinanciamiento();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}