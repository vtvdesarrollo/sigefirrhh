package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;

public class ConceptoCuentaContableBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoCuentaContable conceptoCuentaContableNew = 
      (ConceptoCuentaContable)BeanUtils.cloneBean(
      conceptoCuentaContable);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCuentaContableNew.getConceptoTipoPersonal() != null) {
      conceptoCuentaContableNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCuentaContableNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CuentaContableBeanBusiness cuentaContableBeanBusiness = new CuentaContableBeanBusiness();

    if (conceptoCuentaContableNew.getCuentaContable() != null) {
      conceptoCuentaContableNew.setCuentaContable(
        cuentaContableBeanBusiness.findCuentaContableById(
        conceptoCuentaContableNew.getCuentaContable().getIdCuentaContable()));
    }
    pm.makePersistent(conceptoCuentaContableNew);
  }

  public void updateConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable) throws Exception
  {
    ConceptoCuentaContable conceptoCuentaContableModify = 
      findConceptoCuentaContableById(conceptoCuentaContable.getIdConceptoCuentaContable());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCuentaContable.getConceptoTipoPersonal() != null) {
      conceptoCuentaContable.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCuentaContable.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CuentaContableBeanBusiness cuentaContableBeanBusiness = new CuentaContableBeanBusiness();

    if (conceptoCuentaContable.getCuentaContable() != null) {
      conceptoCuentaContable.setCuentaContable(
        cuentaContableBeanBusiness.findCuentaContableById(
        conceptoCuentaContable.getCuentaContable().getIdCuentaContable()));
    }

    BeanUtils.copyProperties(conceptoCuentaContableModify, conceptoCuentaContable);
  }

  public void deleteConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoCuentaContable conceptoCuentaContableDelete = 
      findConceptoCuentaContableById(conceptoCuentaContable.getIdConceptoCuentaContable());
    pm.deletePersistent(conceptoCuentaContableDelete);
  }

  public ConceptoCuentaContable findConceptoCuentaContableById(long idConceptoCuentaContable) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoCuentaContable == pIdConceptoCuentaContable";
    Query query = pm.newQuery(ConceptoCuentaContable.class, filter);

    query.declareParameters("long pIdConceptoCuentaContable");

    parameters.put("pIdConceptoCuentaContable", new Long(idConceptoCuentaContable));

    Collection colConceptoCuentaContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoCuentaContable.iterator();
    return (ConceptoCuentaContable)iterator.next();
  }

  public Collection findConceptoCuentaContableAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoCuentaContableExtent = pm.getExtent(
      ConceptoCuentaContable.class, true);
    Query query = pm.newQuery(conceptoCuentaContableExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoCuentaContable.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoCuentaContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCuentaContable);

    return colConceptoCuentaContable;
  }

  public Collection findByCuentaContable(long idCuentaContable)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cuentaContable.idCuentaContable == pIdCuentaContable";

    Query query = pm.newQuery(ConceptoCuentaContable.class, filter);

    query.declareParameters("long pIdCuentaContable");
    HashMap parameters = new HashMap();

    parameters.put("pIdCuentaContable", new Long(idCuentaContable));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoCuentaContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCuentaContable);

    return colConceptoCuentaContable;
  }
}