package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PresupuestoEspecifica
  implements Serializable, PersistenceCapable
{
  private long idPresupuestoEspecifica;
  private UelEspecifica uelEspecifica;
  private CuentaPresupuesto cuentaPresupuesto;
  private int anio;
  private double montoPresupuestado;
  private double montoEjecutado;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "cuentaPresupuesto", "idPresupuestoEspecifica", "montoEjecutado", "montoPresupuestado", "uelEspecifica" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaPresupuesto"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.UelEspecifica") };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public PresupuestoEspecifica()
  {
    jdoSetmontoPresupuestado(this, 0.0D);

    jdoSetmontoEjecutado(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoPresupuestado(this));

    return jdoGetanio(this) + " - " + jdoGetuelEspecifica(this).getCategoriaPresupuesto() + " - " + jdoGetcuentaPresupuesto(this).getCodPresupuesto() + " " + a;
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public CuentaPresupuesto getCuentaPresupuesto() {
    return jdoGetcuentaPresupuesto(this);
  }
  public void setCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) {
    jdoSetcuentaPresupuesto(this, cuentaPresupuesto);
  }
  public long getIdPresupuestoEspecifica() {
    return jdoGetidPresupuestoEspecifica(this);
  }
  public void setIdPresupuestoEspecifica(long idPresupuestoEspecifica) {
    jdoSetidPresupuestoEspecifica(this, idPresupuestoEspecifica);
  }
  public double getMontoEjecutado() {
    return jdoGetmontoEjecutado(this);
  }
  public void setMontoEjecutado(double montoEjecutado) {
    jdoSetmontoEjecutado(this, montoEjecutado);
  }
  public double getMontoPresupuestado() {
    return jdoGetmontoPresupuestado(this);
  }
  public void setMontoPresupuestado(double montoPresupuestado) {
    jdoSetmontoPresupuestado(this, montoPresupuestado);
  }
  public UelEspecifica getUelEspecifica() {
    return jdoGetuelEspecifica(this);
  }
  public void setUelEspecifica(UelEspecifica uelEspecifica) {
    jdoSetuelEspecifica(this, uelEspecifica);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.PresupuestoEspecifica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PresupuestoEspecifica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PresupuestoEspecifica localPresupuestoEspecifica = new PresupuestoEspecifica();
    localPresupuestoEspecifica.jdoFlags = 1;
    localPresupuestoEspecifica.jdoStateManager = paramStateManager;
    return localPresupuestoEspecifica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PresupuestoEspecifica localPresupuestoEspecifica = new PresupuestoEspecifica();
    localPresupuestoEspecifica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPresupuestoEspecifica.jdoFlags = 1;
    localPresupuestoEspecifica.jdoStateManager = paramStateManager;
    return localPresupuestoEspecifica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cuentaPresupuesto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPresupuestoEspecifica);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoEjecutado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPresupuestado);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.uelEspecifica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaPresupuesto = ((CuentaPresupuesto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPresupuestoEspecifica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoEjecutado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPresupuestado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.uelEspecifica = ((UelEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PresupuestoEspecifica paramPresupuestoEspecifica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPresupuestoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPresupuestoEspecifica.anio;
      return;
    case 1:
      if (paramPresupuestoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaPresupuesto = paramPresupuestoEspecifica.cuentaPresupuesto;
      return;
    case 2:
      if (paramPresupuestoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.idPresupuestoEspecifica = paramPresupuestoEspecifica.idPresupuestoEspecifica;
      return;
    case 3:
      if (paramPresupuestoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.montoEjecutado = paramPresupuestoEspecifica.montoEjecutado;
      return;
    case 4:
      if (paramPresupuestoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.montoPresupuestado = paramPresupuestoEspecifica.montoPresupuestado;
      return;
    case 5:
      if (paramPresupuestoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.uelEspecifica = paramPresupuestoEspecifica.uelEspecifica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PresupuestoEspecifica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PresupuestoEspecifica localPresupuestoEspecifica = (PresupuestoEspecifica)paramObject;
    if (localPresupuestoEspecifica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPresupuestoEspecifica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PresupuestoEspecificaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PresupuestoEspecificaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PresupuestoEspecificaPK))
      throw new IllegalArgumentException("arg1");
    PresupuestoEspecificaPK localPresupuestoEspecificaPK = (PresupuestoEspecificaPK)paramObject;
    localPresupuestoEspecificaPK.idPresupuestoEspecifica = this.idPresupuestoEspecifica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PresupuestoEspecificaPK))
      throw new IllegalArgumentException("arg1");
    PresupuestoEspecificaPK localPresupuestoEspecificaPK = (PresupuestoEspecificaPK)paramObject;
    this.idPresupuestoEspecifica = localPresupuestoEspecificaPK.idPresupuestoEspecifica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PresupuestoEspecificaPK))
      throw new IllegalArgumentException("arg2");
    PresupuestoEspecificaPK localPresupuestoEspecificaPK = (PresupuestoEspecificaPK)paramObject;
    localPresupuestoEspecificaPK.idPresupuestoEspecifica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PresupuestoEspecificaPK))
      throw new IllegalArgumentException("arg2");
    PresupuestoEspecificaPK localPresupuestoEspecificaPK = (PresupuestoEspecificaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localPresupuestoEspecificaPK.idPresupuestoEspecifica);
  }

  private static final int jdoGetanio(PresupuestoEspecifica paramPresupuestoEspecifica)
  {
    if (paramPresupuestoEspecifica.jdoFlags <= 0)
      return paramPresupuestoEspecifica.anio;
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPresupuestoEspecifica.anio;
    if (localStateManager.isLoaded(paramPresupuestoEspecifica, jdoInheritedFieldCount + 0))
      return paramPresupuestoEspecifica.anio;
    return localStateManager.getIntField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 0, paramPresupuestoEspecifica.anio);
  }

  private static final void jdoSetanio(PresupuestoEspecifica paramPresupuestoEspecifica, int paramInt)
  {
    if (paramPresupuestoEspecifica.jdoFlags == 0)
    {
      paramPresupuestoEspecifica.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPresupuestoEspecifica.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 0, paramPresupuestoEspecifica.anio, paramInt);
  }

  private static final CuentaPresupuesto jdoGetcuentaPresupuesto(PresupuestoEspecifica paramPresupuestoEspecifica)
  {
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPresupuestoEspecifica.cuentaPresupuesto;
    if (localStateManager.isLoaded(paramPresupuestoEspecifica, jdoInheritedFieldCount + 1))
      return paramPresupuestoEspecifica.cuentaPresupuesto;
    return (CuentaPresupuesto)localStateManager.getObjectField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 1, paramPresupuestoEspecifica.cuentaPresupuesto);
  }

  private static final void jdoSetcuentaPresupuesto(PresupuestoEspecifica paramPresupuestoEspecifica, CuentaPresupuesto paramCuentaPresupuesto)
  {
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPresupuestoEspecifica.cuentaPresupuesto = paramCuentaPresupuesto;
      return;
    }
    localStateManager.setObjectField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 1, paramPresupuestoEspecifica.cuentaPresupuesto, paramCuentaPresupuesto);
  }

  private static final long jdoGetidPresupuestoEspecifica(PresupuestoEspecifica paramPresupuestoEspecifica)
  {
    return paramPresupuestoEspecifica.idPresupuestoEspecifica;
  }

  private static final void jdoSetidPresupuestoEspecifica(PresupuestoEspecifica paramPresupuestoEspecifica, long paramLong)
  {
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPresupuestoEspecifica.idPresupuestoEspecifica = paramLong;
      return;
    }
    localStateManager.setLongField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 2, paramPresupuestoEspecifica.idPresupuestoEspecifica, paramLong);
  }

  private static final double jdoGetmontoEjecutado(PresupuestoEspecifica paramPresupuestoEspecifica)
  {
    if (paramPresupuestoEspecifica.jdoFlags <= 0)
      return paramPresupuestoEspecifica.montoEjecutado;
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPresupuestoEspecifica.montoEjecutado;
    if (localStateManager.isLoaded(paramPresupuestoEspecifica, jdoInheritedFieldCount + 3))
      return paramPresupuestoEspecifica.montoEjecutado;
    return localStateManager.getDoubleField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 3, paramPresupuestoEspecifica.montoEjecutado);
  }

  private static final void jdoSetmontoEjecutado(PresupuestoEspecifica paramPresupuestoEspecifica, double paramDouble)
  {
    if (paramPresupuestoEspecifica.jdoFlags == 0)
    {
      paramPresupuestoEspecifica.montoEjecutado = paramDouble;
      return;
    }
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPresupuestoEspecifica.montoEjecutado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 3, paramPresupuestoEspecifica.montoEjecutado, paramDouble);
  }

  private static final double jdoGetmontoPresupuestado(PresupuestoEspecifica paramPresupuestoEspecifica)
  {
    if (paramPresupuestoEspecifica.jdoFlags <= 0)
      return paramPresupuestoEspecifica.montoPresupuestado;
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPresupuestoEspecifica.montoPresupuestado;
    if (localStateManager.isLoaded(paramPresupuestoEspecifica, jdoInheritedFieldCount + 4))
      return paramPresupuestoEspecifica.montoPresupuestado;
    return localStateManager.getDoubleField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 4, paramPresupuestoEspecifica.montoPresupuestado);
  }

  private static final void jdoSetmontoPresupuestado(PresupuestoEspecifica paramPresupuestoEspecifica, double paramDouble)
  {
    if (paramPresupuestoEspecifica.jdoFlags == 0)
    {
      paramPresupuestoEspecifica.montoPresupuestado = paramDouble;
      return;
    }
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPresupuestoEspecifica.montoPresupuestado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 4, paramPresupuestoEspecifica.montoPresupuestado, paramDouble);
  }

  private static final UelEspecifica jdoGetuelEspecifica(PresupuestoEspecifica paramPresupuestoEspecifica)
  {
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPresupuestoEspecifica.uelEspecifica;
    if (localStateManager.isLoaded(paramPresupuestoEspecifica, jdoInheritedFieldCount + 5))
      return paramPresupuestoEspecifica.uelEspecifica;
    return (UelEspecifica)localStateManager.getObjectField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 5, paramPresupuestoEspecifica.uelEspecifica);
  }

  private static final void jdoSetuelEspecifica(PresupuestoEspecifica paramPresupuestoEspecifica, UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramPresupuestoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPresupuestoEspecifica.uelEspecifica = paramUelEspecifica;
      return;
    }
    localStateManager.setObjectField(paramPresupuestoEspecifica, jdoInheritedFieldCount + 5, paramPresupuestoEspecifica.uelEspecifica, paramUelEspecifica);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}