package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarPartidaUelEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarPartidaUelEspecificaForm.class.getName());
  private long idFuenteFinanciamiento;
  private Collection listFuenteFinanciamiento;
  private int anio;
  private SigecofNoGenFacade sigecofFacade;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;

  public GenerarPartidaUelEspecificaForm()
  {
    this.sigecofFacade = new SigecofNoGenFacade();

    FacesContext context = FacesContext.getCurrentInstance();
    this.anio = Calendar.getInstance().get(1);
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listFuenteFinanciamiento = this.sigecofFacade.findAllFuenteFinanciamiento();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listFuenteFinanciamiento = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.sigecofFacade.generarPartidaUelEspecifica(this.idFuenteFinanciamiento, this.anio);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_add", new FacesMessage("Se asignó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListFuenteFinanciamiento()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFuenteFinanciamiento, "sigefirrhh.personal.sigecof.FuenteFinanciamiento");
  }
  public int getAnio() {
    return this.anio;
  }

  public void setAnio(int i) {
    this.anio = i;
  }

  public long getIdFuenteFinanciamiento() {
    return this.idFuenteFinanciamiento;
  }

  public void setIdFuenteFinanciamiento(long idFuenteFinanciamiento) {
    this.idFuenteFinanciamiento = idFuenteFinanciamiento;
  }
}