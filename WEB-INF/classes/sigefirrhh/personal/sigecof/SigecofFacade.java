package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class SigecofFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private SigecofBusiness sigecofBusiness = new SigecofBusiness();

  public void addResumenMensual(ResumenMensual resumenMensual)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addResumenMensual(resumenMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateResumenMensual(ResumenMensual resumenMensual) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateResumenMensual(resumenMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteResumenMensual(ResumenMensual resumenMensual) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteResumenMensual(resumenMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ResumenMensual findResumenMensualById(long resumenMensualId) throws Exception
  {
    try { this.txn.open();
      ResumenMensual resumenMensual = 
        this.sigecofBusiness.findResumenMensualById(resumenMensualId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(resumenMensual);
      return resumenMensual;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllResumenMensual() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllResumenMensual();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findResumenMensualByUnidadAdministradora(idUnidadAdministradora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByEncabezadoResumenMensual(long idEncabezadoResumenMensual)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findResumenMensualByEncabezadoResumenMensual(idEncabezadoResumenMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findResumenMensualByUelEspecifica(idUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findResumenMensualByCuentaPresupuesto(idCuentaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByUnidadEjecutora(long idUnidadEjecutora)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findResumenMensualByUnidadEjecutora(idUnidadEjecutora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findResumenMensualByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByFuenteFinanciamiento(long idFuenteFinanciamiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findResumenMensualByFuenteFinanciamiento(idFuenteFinanciamiento);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addEncabezadoResumenMensual(encabezadoResumenMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateEncabezadoResumenMensual(encabezadoResumenMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteEncabezadoResumenMensual(encabezadoResumenMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public EncabezadoResumenMensual findEncabezadoResumenMensualById(long encabezadoResumenMensualId) throws Exception
  {
    try { this.txn.open();
      EncabezadoResumenMensual encabezadoResumenMensual = 
        this.sigecofBusiness.findEncabezadoResumenMensualById(encabezadoResumenMensualId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(encabezadoResumenMensual);
      return encabezadoResumenMensual;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEncabezadoResumenMensual() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllEncabezadoResumenMensual();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEncabezadoResumenMensualByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEncabezadoResumenMensualByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEncabezadoResumenMensualByMes(int mes)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEncabezadoResumenMensualByMes(mes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEncabezadoResumenMensualByNumeroNomina(int numeroNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEncabezadoResumenMensualByNumeroNomina(numeroNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEncabezadoResumenMensualByNumeroExpediente(int numeroExpediente)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEncabezadoResumenMensualByNumeroExpediente(numeroExpediente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEncabezadoResumenMensualByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEncabezadoResumenMensualByUnidadAdministradora(idUnidadAdministradora);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addSeguridadPresupuesto(seguridadPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateSeguridadPresupuesto(seguridadPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadPresupuesto(SeguridadPresupuesto seguridadPresupuesto) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteSeguridadPresupuesto(seguridadPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadPresupuesto findSeguridadPresupuestoById(long seguridadPresupuestoId) throws Exception
  {
    try { this.txn.open();
      SeguridadPresupuesto seguridadPresupuesto = 
        this.sigecofBusiness.findSeguridadPresupuestoById(seguridadPresupuestoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadPresupuesto);
      return seguridadPresupuesto;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadPresupuesto() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllSeguridadPresupuesto();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadPresupuestoByCategoriaPresupuesto(long idCategoriaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findSeguridadPresupuestoByCategoriaPresupuesto(idCategoriaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadPresupuestoByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findSeguridadPresupuestoByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoResumen(ConceptoResumen conceptoResumen)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addConceptoResumen(conceptoResumen);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoResumen(ConceptoResumen conceptoResumen) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateConceptoResumen(conceptoResumen);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoResumen(ConceptoResumen conceptoResumen) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteConceptoResumen(conceptoResumen);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoResumen findConceptoResumenById(long conceptoResumenId) throws Exception
  {
    try { this.txn.open();
      ConceptoResumen conceptoResumen = 
        this.sigecofBusiness.findConceptoResumenById(conceptoResumenId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoResumen);
      return conceptoResumen;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoResumen() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllConceptoResumen();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoResumenByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoResumenByUnidadAdministradora(idUnidadAdministradora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoResumenByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoResumenByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoEspecifica(ConceptoEspecifica conceptoEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addConceptoEspecifica(conceptoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoEspecifica(ConceptoEspecifica conceptoEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateConceptoEspecifica(conceptoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoEspecifica(ConceptoEspecifica conceptoEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteConceptoEspecifica(conceptoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoEspecifica findConceptoEspecificaById(long conceptoEspecificaId) throws Exception
  {
    try { this.txn.open();
      ConceptoEspecifica conceptoEspecifica = 
        this.sigecofBusiness.findConceptoEspecificaById(conceptoEspecificaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoEspecifica);
      return conceptoEspecifica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoEspecifica() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllConceptoEspecifica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoEspecificaByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoEspecificaByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoEspecificaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoEspecificaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCargoEspecifica(CargoEspecifica cargoEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addCargoEspecifica(cargoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCargoEspecifica(CargoEspecifica cargoEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateCargoEspecifica(cargoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCargoEspecifica(CargoEspecifica cargoEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteCargoEspecifica(cargoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CargoEspecifica findCargoEspecificaById(long cargoEspecificaId) throws Exception
  {
    try { this.txn.open();
      CargoEspecifica cargoEspecifica = 
        this.sigecofBusiness.findCargoEspecificaById(cargoEspecificaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cargoEspecifica);
      return cargoEspecifica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCargoEspecifica() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllCargoEspecifica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargoEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCargoEspecificaByUelEspecifica(idUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargoEspecificaByRegistroCargos(long idRegistroCargos)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCargoEspecificaByRegistroCargos(idRegistroCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargoEspecificaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCargoEspecificaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addPresupuestoEspecifica(presupuestoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updatePresupuestoEspecifica(presupuestoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deletePresupuestoEspecifica(presupuestoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PresupuestoEspecifica findPresupuestoEspecificaById(long presupuestoEspecificaId) throws Exception
  {
    try { this.txn.open();
      PresupuestoEspecifica presupuestoEspecifica = 
        this.sigecofBusiness.findPresupuestoEspecificaById(presupuestoEspecificaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(presupuestoEspecifica);
      return presupuestoEspecifica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPresupuestoEspecifica() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllPresupuestoEspecifica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPresupuestoEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findPresupuestoEspecificaByUelEspecifica(idUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPresupuestoEspecificaByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findPresupuestoEspecificaByCuentaPresupuesto(idCuentaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPresupuestoEspecificaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findPresupuestoEspecificaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAccionEspecifica(AccionEspecifica accionEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addAccionEspecifica(accionEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAccionEspecifica(AccionEspecifica accionEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateAccionEspecifica(accionEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAccionEspecifica(AccionEspecifica accionEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteAccionEspecifica(accionEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AccionEspecifica findAccionEspecificaById(long accionEspecificaId) throws Exception
  {
    try { this.txn.open();
      AccionEspecifica accionEspecifica = 
        this.sigecofBusiness.findAccionEspecificaById(accionEspecificaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(accionEspecifica);
      return accionEspecifica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAccionEspecifica() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllAccionEspecifica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionEspecificaByProyecto(long idProyecto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionEspecificaByProyecto(idProyecto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionEspecificaByAccionCentralizada(long idAccionCentralizada)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionEspecificaByAccionCentralizada(idAccionCentralizada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionEspecificaByCodAccionEspecifica(String codAccionEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionEspecificaByCodAccionEspecifica(codAccionEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionEspecificaByDenominacion(String denominacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionEspecificaByDenominacion(denominacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionEspecificaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionEspecificaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAccionCentralizada(AccionCentralizada accionCentralizada)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addAccionCentralizada(accionCentralizada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAccionCentralizada(AccionCentralizada accionCentralizada) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateAccionCentralizada(accionCentralizada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAccionCentralizada(AccionCentralizada accionCentralizada) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteAccionCentralizada(accionCentralizada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AccionCentralizada findAccionCentralizadaById(long accionCentralizadaId) throws Exception
  {
    try { this.txn.open();
      AccionCentralizada accionCentralizada = 
        this.sigecofBusiness.findAccionCentralizadaById(accionCentralizadaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(accionCentralizada);
      return accionCentralizada;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAccionCentralizada() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllAccionCentralizada();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionCentralizadaByAnio(int anio, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionCentralizadaByAnio(anio, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionCentralizadaByCodAccionCentralizada(String codAccionCentralizada, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionCentralizadaByCodAccionCentralizada(codAccionCentralizada, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionCentralizadaByDenominacion(String denominacion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionCentralizadaByDenominacion(denominacion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionCentralizadaByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findAccionCentralizadaByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addProyecto(Proyecto proyecto)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addProyecto(proyecto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateProyecto(Proyecto proyecto) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateProyecto(proyecto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteProyecto(Proyecto proyecto) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteProyecto(proyecto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Proyecto findProyectoById(long proyectoId) throws Exception
  {
    try { this.txn.open();
      Proyecto proyecto = 
        this.sigecofBusiness.findProyectoById(proyectoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(proyecto);
      return proyecto;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllProyecto() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllProyecto();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProyectoByAnio(int anio, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findProyectoByAnio(anio, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProyectoByCodProyecto(String codProyecto, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findProyectoByCodProyecto(codProyecto, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProyectoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findProyectoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCuentaContable(CuentaContable cuentaContable)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addCuentaContable(cuentaContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCuentaContable(CuentaContable cuentaContable) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateCuentaContable(cuentaContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCuentaContable(CuentaContable cuentaContable) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteCuentaContable(cuentaContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CuentaContable findCuentaContableById(long cuentaContableId) throws Exception
  {
    try { this.txn.open();
      CuentaContable cuentaContable = 
        this.sigecofBusiness.findCuentaContableById(cuentaContableId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cuentaContable);
      return cuentaContable;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCuentaContable() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllCuentaContable();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaContableByCodContable(String codContable, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCuentaContableByCodContable(codContable, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaContableByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCuentaContableByDescripcion(descripcion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaContableByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCuentaContableByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoContable(ConceptoContable conceptoContable)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addConceptoContable(conceptoContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoContable(ConceptoContable conceptoContable) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateConceptoContable(conceptoContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoContable(ConceptoContable conceptoContable) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteConceptoContable(conceptoContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoContable findConceptoContableById(long conceptoContableId) throws Exception
  {
    try { this.txn.open();
      ConceptoContable conceptoContable = 
        this.sigecofBusiness.findConceptoContableById(conceptoContableId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoContable);
      return conceptoContable;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoContable() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllConceptoContable();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoContableByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoContableByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoContableByCuentaContable(long idCuentaContable)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoContableByCuentaContable(idCuentaContable);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUelEspecifica(UelEspecifica uelEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addUelEspecifica(uelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUelEspecifica(UelEspecifica uelEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateUelEspecifica(uelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUelEspecifica(UelEspecifica uelEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteUelEspecifica(uelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UelEspecifica findUelEspecificaById(long uelEspecificaId) throws Exception
  {
    try { this.txn.open();
      UelEspecifica uelEspecifica = 
        this.sigecofBusiness.findUelEspecificaById(uelEspecificaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(uelEspecifica);
      return uelEspecifica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUelEspecifica() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllUelEspecifica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUelEspecificaByAccionEspecifica(long idAccionEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findUelEspecificaByAccionEspecifica(idAccionEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUelEspecificaByUnidadEjecutora(long idUnidadEjecutora)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findUelEspecificaByUnidadEjecutora(idUnidadEjecutora);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUelEspecificaByCategoriaPresupuesto(String categoriaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findUelEspecificaByCategoriaPresupuesto(categoriaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUelEspecificaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findUelEspecificaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoCuenta(ConceptoCuenta conceptoCuenta)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addConceptoCuenta(conceptoCuenta);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoCuenta(ConceptoCuenta conceptoCuenta) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateConceptoCuenta(conceptoCuenta);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoCuenta(ConceptoCuenta conceptoCuenta) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteConceptoCuenta(conceptoCuenta);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoCuenta findConceptoCuentaById(long conceptoCuentaId) throws Exception
  {
    try { this.txn.open();
      ConceptoCuenta conceptoCuenta = 
        this.sigecofBusiness.findConceptoCuentaById(conceptoCuentaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoCuenta);
      return conceptoCuenta;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoCuenta() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllConceptoCuenta();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCuentaByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoCuentaByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCuentaByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoCuentaByCuentaPresupuesto(idCuentaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addCuentaPresupuesto(cuentaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateCuentaPresupuesto(cuentaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteCuentaPresupuesto(cuentaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CuentaPresupuesto findCuentaPresupuestoById(long cuentaPresupuestoId) throws Exception
  {
    try { this.txn.open();
      CuentaPresupuesto cuentaPresupuesto = 
        this.sigecofBusiness.findCuentaPresupuestoById(cuentaPresupuestoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cuentaPresupuesto);
      return cuentaPresupuesto;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCuentaPresupuesto() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllCuentaPresupuesto();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaPresupuestoByCodPresupuesto(String codPresupuesto, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCuentaPresupuestoByCodPresupuesto(codPresupuesto, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaPresupuestoByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCuentaPresupuestoByDescripcion(descripcion, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCuentaPresupuestoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findCuentaPresupuestoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addTrabajadorEspecifica(trabajadorEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateTrabajadorEspecifica(trabajadorEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteTrabajadorEspecifica(trabajadorEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TrabajadorEspecifica findTrabajadorEspecificaById(long trabajadorEspecificaId) throws Exception
  {
    try { this.txn.open();
      TrabajadorEspecifica trabajadorEspecifica = 
        this.sigecofBusiness.findTrabajadorEspecificaById(trabajadorEspecificaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(trabajadorEspecifica);
      return trabajadorEspecifica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTrabajadorEspecifica() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllTrabajadorEspecifica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findTrabajadorEspecificaByUelEspecifica(idUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorEspecificaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findTrabajadorEspecificaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEjecucionMensual(EjecucionMensual ejecucionMensual)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addEjecucionMensual(ejecucionMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEjecucionMensual(EjecucionMensual ejecucionMensual) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateEjecucionMensual(ejecucionMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEjecucionMensual(EjecucionMensual ejecucionMensual) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteEjecucionMensual(ejecucionMensual);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public EjecucionMensual findEjecucionMensualById(long ejecucionMensualId) throws Exception
  {
    try { this.txn.open();
      EjecucionMensual ejecucionMensual = 
        this.sigecofBusiness.findEjecucionMensualById(ejecucionMensualId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(ejecucionMensual);
      return ejecucionMensual;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEjecucionMensual() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllEjecucionMensual();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEjecucionMensualByPresupuestoEspecifica(long idPresupuestoEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEjecucionMensualByPresupuestoEspecifica(idPresupuestoEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEjecucionMensualByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEjecucionMensualByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEjecucionMensualByMes(int mes)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findEjecucionMensualByMes(mes);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addFuenteFinanciamiento(fuenteFinanciamiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateFuenteFinanciamiento(fuenteFinanciamiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteFuenteFinanciamiento(fuenteFinanciamiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public FuenteFinanciamiento findFuenteFinanciamientoById(long fuenteFinanciamientoId) throws Exception
  {
    try { this.txn.open();
      FuenteFinanciamiento fuenteFinanciamiento = 
        this.sigecofBusiness.findFuenteFinanciamientoById(fuenteFinanciamientoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(fuenteFinanciamiento);
      return fuenteFinanciamiento;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllFuenteFinanciamiento() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllFuenteFinanciamiento();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFuenteFinanciamientoByCodFuenteFinanciamiento(String codFuenteFinanciamiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findFuenteFinanciamientoByCodFuenteFinanciamiento(codFuenteFinanciamiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findFuenteFinanciamientoByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findFuenteFinanciamientoByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addPartidaUelEspecifica(partidaUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updatePartidaUelEspecifica(partidaUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePartidaUelEspecifica(PartidaUelEspecifica partidaUelEspecifica) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deletePartidaUelEspecifica(partidaUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PartidaUelEspecifica findPartidaUelEspecificaById(long partidaUelEspecificaId) throws Exception
  {
    try { this.txn.open();
      PartidaUelEspecifica partidaUelEspecifica = 
        this.sigecofBusiness.findPartidaUelEspecificaById(partidaUelEspecificaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(partidaUelEspecifica);
      return partidaUelEspecifica;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPartidaUelEspecifica() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllPartidaUelEspecifica();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPartidaUelEspecificaByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findPartidaUelEspecificaByUelEspecifica(idUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPartidaUelEspecificaByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findPartidaUelEspecificaByCuentaPresupuesto(idCuentaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPartidaUelEspecificaByFuenteFinanciamiento(long idFuenteFinanciamiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findPartidaUelEspecificaByFuenteFinanciamiento(idFuenteFinanciamiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPartidaUelEspecificaByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findPartidaUelEspecificaByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sigecofBusiness.addConceptoCuentaContable(conceptoCuentaContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.updateConceptoCuentaContable(conceptoCuentaContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoCuentaContable(ConceptoCuentaContable conceptoCuentaContable) throws Exception
  {
    try { this.txn.open();
      this.sigecofBusiness.deleteConceptoCuentaContable(conceptoCuentaContable);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoCuentaContable findConceptoCuentaContableById(long conceptoCuentaContableId) throws Exception
  {
    try { this.txn.open();
      ConceptoCuentaContable conceptoCuentaContable = 
        this.sigecofBusiness.findConceptoCuentaContableById(conceptoCuentaContableId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoCuentaContable);
      return conceptoCuentaContable;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoCuentaContable() throws Exception
  {
    try { this.txn.open();
      return this.sigecofBusiness.findAllConceptoCuentaContable();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCuentaContableByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoCuentaContableByConceptoTipoPersonal(idConceptoTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCuentaContableByCuentaContable(long idCuentaContable)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofBusiness.findConceptoCuentaContableByCuentaContable(idCuentaContable);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}