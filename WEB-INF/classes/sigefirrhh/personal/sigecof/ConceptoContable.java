package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;

public class ConceptoContable
  implements Serializable, PersistenceCapable
{
  private long idConceptoContable;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private CuentaContable cuentaContable;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "cuentaContable", "idConceptoContable" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaContable"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - CP- " + 
      jdoGetcuentaContable(this).getCodContable();
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public long getIdConceptoContable() {
    return jdoGetidConceptoContable(this);
  }

  public void setIdConceptoContable(long idConceptoContable) {
    jdoSetidConceptoContable(this, idConceptoContable);
  }

  public CuentaContable getCuentaContable() {
    return jdoGetcuentaContable(this);
  }

  public void setCuentaContable(CuentaContable cuentaContable) {
    jdoSetcuentaContable(this, cuentaContable);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.ConceptoContable"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoContable());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoContable localConceptoContable = new ConceptoContable();
    localConceptoContable.jdoFlags = 1;
    localConceptoContable.jdoStateManager = paramStateManager;
    return localConceptoContable;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoContable localConceptoContable = new ConceptoContable();
    localConceptoContable.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoContable.jdoFlags = 1;
    localConceptoContable.jdoStateManager = paramStateManager;
    return localConceptoContable;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cuentaContable);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoContable);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaContable = ((CuentaContable)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoContable = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoContable paramConceptoContable, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoContable == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoContable.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoContable == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaContable = paramConceptoContable.cuentaContable;
      return;
    case 2:
      if (paramConceptoContable == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoContable = paramConceptoContable.idConceptoContable;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoContable))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoContable localConceptoContable = (ConceptoContable)paramObject;
    if (localConceptoContable.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoContable, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoContablePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoContablePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoContablePK))
      throw new IllegalArgumentException("arg1");
    ConceptoContablePK localConceptoContablePK = (ConceptoContablePK)paramObject;
    localConceptoContablePK.idConceptoContable = this.idConceptoContable;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoContablePK))
      throw new IllegalArgumentException("arg1");
    ConceptoContablePK localConceptoContablePK = (ConceptoContablePK)paramObject;
    this.idConceptoContable = localConceptoContablePK.idConceptoContable;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoContablePK))
      throw new IllegalArgumentException("arg2");
    ConceptoContablePK localConceptoContablePK = (ConceptoContablePK)paramObject;
    localConceptoContablePK.idConceptoContable = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoContablePK))
      throw new IllegalArgumentException("arg2");
    ConceptoContablePK localConceptoContablePK = (ConceptoContablePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConceptoContablePK.idConceptoContable);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoContable paramConceptoContable)
  {
    StateManager localStateManager = paramConceptoContable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoContable.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoContable, jdoInheritedFieldCount + 0))
      return paramConceptoContable.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoContable, jdoInheritedFieldCount + 0, paramConceptoContable.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoContable paramConceptoContable, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoContable.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoContable, jdoInheritedFieldCount + 0, paramConceptoContable.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final CuentaContable jdoGetcuentaContable(ConceptoContable paramConceptoContable)
  {
    StateManager localStateManager = paramConceptoContable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoContable.cuentaContable;
    if (localStateManager.isLoaded(paramConceptoContable, jdoInheritedFieldCount + 1))
      return paramConceptoContable.cuentaContable;
    return (CuentaContable)localStateManager.getObjectField(paramConceptoContable, jdoInheritedFieldCount + 1, paramConceptoContable.cuentaContable);
  }

  private static final void jdoSetcuentaContable(ConceptoContable paramConceptoContable, CuentaContable paramCuentaContable)
  {
    StateManager localStateManager = paramConceptoContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoContable.cuentaContable = paramCuentaContable;
      return;
    }
    localStateManager.setObjectField(paramConceptoContable, jdoInheritedFieldCount + 1, paramConceptoContable.cuentaContable, paramCuentaContable);
  }

  private static final long jdoGetidConceptoContable(ConceptoContable paramConceptoContable)
  {
    return paramConceptoContable.idConceptoContable;
  }

  private static final void jdoSetidConceptoContable(ConceptoContable paramConceptoContable, long paramLong)
  {
    StateManager localStateManager = paramConceptoContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoContable.idConceptoContable = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoContable, jdoInheritedFieldCount + 2, paramConceptoContable.idConceptoContable, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}