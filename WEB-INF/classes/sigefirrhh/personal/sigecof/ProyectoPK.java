package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ProyectoPK
  implements Serializable
{
  public long idProyecto;

  public ProyectoPK()
  {
  }

  public ProyectoPK(long idProyecto)
  {
    this.idProyecto = idProyecto;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProyectoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProyectoPK thatPK)
  {
    return 
      this.idProyecto == thatPK.idProyecto;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProyecto)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProyecto);
  }
}