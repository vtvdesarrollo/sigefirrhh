package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CerrarResumenMensualForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CerrarResumenMensualForm.class.getName());
  private String selectCategoriaPresupuesto;
  private int mes;
  private int anio;
  private long idCategoriaPresupuesto;
  private Collection listCategoriaPresupuesto;
  private String tipoNomina;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private boolean show;
  private boolean auxShow;

  public void changeCategoriaPresupuesto(ValueChangeEvent event)
  {
    this.idCategoriaPresupuesto = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.auxShow = false;
      if (this.idCategoriaPresupuesto != 0L) {
        SeguridadPresupuesto seguridadPresupuesto = this.sigecofFacade.findSeguridadPresupuestoByUltimoResumen(this.idCategoriaPresupuesto);

        if (seguridadPresupuesto.getMes() == 12) {
          this.anio = (seguridadPresupuesto.getAnio() + 1);
          this.mes = 1;
        } else {
          this.anio = seguridadPresupuesto.getAnio();
          this.mes = (seguridadPresupuesto.getMes() + 1);
        }
        this.auxShow = true;
      }

    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoNomina(ValueChangeEvent event)
  {
    String tipoNomina = 
      (String)event.getNewValue();
  }

  public CerrarResumenMensualForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try
    {
      this.listCategoriaPresupuesto = this.definicionesFacade.findAllCategoriaPresupuesto();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.listCategoriaPresupuesto = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.sigecofFacade.cerrarResumenMensual(this.idCategoriaPresupuesto, this.anio, this.mes, 0, this.login.getUsuario());
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_add", new FacesMessage("Se cerró con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListCategoriaPresupuesto()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCategoriaPresupuesto, "sigefirrhh.base.definiciones.CategoriaPresupuesto");
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }

  public String getTipoNomina()
  {
    return this.tipoNomina;
  }

  public void setTipoNomina(String string)
  {
    this.tipoNomina = string;
  }

  public String getSelectCategoriaPresupuesto() {
    return this.selectCategoriaPresupuesto;
  }
  public void setSelectCategoriaPresupuesto(String selectCategoriaPresupuesto) {
    this.selectCategoriaPresupuesto = selectCategoriaPresupuesto;
  }
}