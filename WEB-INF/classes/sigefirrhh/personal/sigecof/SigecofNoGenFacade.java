package sigefirrhh.personal.sigecof;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class SigecofNoGenFacade extends SigecofFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private SigecofNoGenBusiness sigecofNoGenBusiness = new SigecofNoGenBusiness();

  public Collection findUelEspecificaByUnidadEjecutoraAndAnio(long idUnidadEjecutora, int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sigecofNoGenBusiness.findUelEspecificaByUnidadEjecutoraAndAnio(idUnidadEjecutora, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUelEspecificaByCategoriaPresupuestoAndAnio(String codCategoriaPresupuesto, int anio) throws Exception
  {
    try {
      this.txn.open();
      return this.sigecofNoGenBusiness.findUelEspecificaByCategoriaPresupuestoAndAnio(codCategoriaPresupuesto, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUelEspecificaByUnidadEjecutoraAndProyecto(long idUnidadEjecutora, long idProyecto) throws Exception
  {
    try {
      this.txn.open();
      return this.sigecofNoGenBusiness.findUelEspecificaByUnidadEjecutoraAndProyecto(idUnidadEjecutora, idProyecto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUelEspecificaByUnidadEjecutoraAndAccionCentralizada(long idUnidadEjecutora, long idAccionCentralizada) throws Exception
  {
    try {
      this.txn.open();
      return this.sigecofNoGenBusiness.findUelEspecificaByUnidadEjecutoraAndAccionCentralizada(idUnidadEjecutora, idAccionCentralizada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrabajadorEspecificaByTrabajadorAndAnio(long idTrabajador, int anio) throws Exception
  {
    try {
      this.txn.open();
      return this.sigecofNoGenBusiness.findTrabajadorEspecificaByTrabajadorAndAnio(idTrabajador, anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void generarTrabajadorCargoEspecifica(long idUnidadEjecutora, long idUelEspecifica, long idTipoPersonal, int anio, double porcentaje, long idOrganismo)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarTrabajadorCargoEspecifica(idUnidadEjecutora, idUelEspecifica, idTipoPersonal, anio, porcentaje, idOrganismo);
  }

  public Collection findUelEspecificaByUnidadEjecutoraAnioAndTipo(long idUnidadEjecutora, int anio, String tipo) throws Exception
  {
    try {
      this.txn.open();
      return this.sigecofNoGenBusiness.findUelEspecificaByUnidadEjecutoraAnioAndTipo(idUnidadEjecutora, anio, tipo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoEspecificaByConceptoTipoPersonalAndAnio(long idConceptoTipoPersonal, int anio) throws Exception
  {
    try { this.txn.open();
      return this.sigecofNoGenBusiness.findConceptoEspecificaByConceptoTipoPersonalAndAnio(idConceptoTipoPersonal, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CargoEspecifica findCargoEspecificaByRegistroCargosAndUelEspecifico(long idRegistroCargos, long idUelEspecifica) throws Exception
  {
    try { this.txn.open();
      return this.sigecofNoGenBusiness.findCargoEspecificaByRegistroCargosAndUelEspecifica(idRegistroCargos, idUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void generarBaseResumen(long idTipoPersonal, int anio, int mes, int numeroNomina, String periodicidad, String estatus, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, int frecuenciaEspecial)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarBaseResumen(idTipoPersonal, anio, mes, numeroNomina, periodicidad, estatus, tieneSemana5, idUnidadAdministradora, codUnidadAdministradora, usuario, titulo, frecuenciaEspecial);
  }

  public void generarResumenMensual(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarResumenMensual(idTipoPersonal, anio, mes, numeroNomina, 
      tieneSemana5, idUnidadAdministradora, codUnidadAdministradora, usuario, titulo);
  }

  public void generarResumenAdicional(long idUnidadAdministradora, long idConceptoTipoPersonal, double monto, int anio, int mes, long idUelEspecifica, long idUnidadEjecutora, int quincena, String codUnidadAdministradora, int numeroNomina, String usuario, String titulo, String aportes, long idAporte, int numero)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarResumenAdicional(idUnidadAdministradora, idConceptoTipoPersonal, 
      monto, anio, mes, idUelEspecifica, idUnidadEjecutora, quincena, codUnidadAdministradora, 
      numeroNomina, usuario, titulo, aportes, idAporte, numero);
  }

  public void cerrarResumenMensual(long idTipoPersonal, int anio, int mes, int numeroNomina, String usuario)
    throws Exception
  {
    this.sigecofNoGenBusiness.cerrarResumenMensual(idTipoPersonal, anio, mes, numeroNomina, usuario);
  }

  public void generarResumenAportes(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, long idConceptoAporte)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarResumenAportes(idTipoPersonal, anio, mes, numeroNomina, 
      tieneSemana5, idUnidadAdministradora, codUnidadAdministradora, usuario, titulo, idConceptoAporte);
  }

  public void generarRendicionAportes(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, long idConceptoAporte, int periodo)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarRendicionAportes(idTipoPersonal, anio, mes, numeroNomina, 
      tieneSemana5, idUnidadAdministradora, codUnidadAdministradora, usuario, titulo, idConceptoAporte, periodo);
  }

  public void generarRendicionMensual(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, int quincena)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarRendicionMensual(idTipoPersonal, anio, mes, numeroNomina, 
      tieneSemana5, idUnidadAdministradora, codUnidadAdministradora, usuario, titulo, quincena);
  }

  public SeguridadPresupuesto findSeguridadPresupuestoByUltimoResumen(long idCategoriaPresupuesto) throws Exception {
    try {
      this.txn.open();
      return this.sigecofNoGenBusiness.findSeguridadPresupuestoByUltimoResumen(idCategoriaPresupuesto);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoResumenByTrabajadorAnioMes(long idTrabajador, int anio, int mes) throws Exception
  {
    try { this.txn.open();
      return this.sigecofNoGenBusiness.findConceptoResumenByTrabajadorAnioMes(idTrabajador, anio, mes);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoCuentaByTipoPersonal(long idTipoPersonal) throws Exception
  {
    try { this.txn.open();
      return this.sigecofNoGenBusiness.findConceptoCuentaByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResumenMensualByUnidadAdministradora(long idUnidadAdministradora, long idCategoriaPresupuesto, long idUelEspecifica, int mes, String cerrado) throws Exception
  {
    try { this.txn.open();
      return this.sigecofNoGenBusiness.findResumenMensualByUnidadAdministradora(idUnidadAdministradora, idCategoriaPresupuesto, idUelEspecifica, mes, cerrado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CargoEspecifica findCargoEspecificaByRegistroCargosAndUelEspecifica(long idRegistroCargos, long idUelEspecifica) throws Exception
  {
    try { this.txn.open();
      return this.sigecofNoGenBusiness.findCargoEspecificaByRegistroCargosAndUelEspecifica(idRegistroCargos, idUelEspecifica);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void generarPartidaUelEspecifica(long idFuenteFinanciamiento, int anio)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarPartidaUelEspecifica(idFuenteFinanciamiento, anio);
  }

  public void generarCuadroOnapre(long idRelacionPersonal, int anio)
    throws Exception
  {
    this.sigecofNoGenBusiness.generarCuadroOnapre(idRelacionPersonal, anio);
  }
}