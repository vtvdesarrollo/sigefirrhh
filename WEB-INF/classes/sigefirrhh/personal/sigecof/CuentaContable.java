package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class CuentaContable
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idCuentaContable;
  private String codContable;
  private String descripcion;
  private String tipo;
  private int indicador;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codContable", "descripcion", "idCuentaContable", "indicador", "organismo", "tipo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaContable"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CuentaContable());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("D", "DETALLE");
    LISTA_TIPO.put("T", "TITULO");
  }

  public CuentaContable()
  {
    jdoSettipo(this, "D");

    jdoSetindicador(this, 0);
  }

  public String toString()
  {
    return jdoGetcodContable(this) + " " + 
      jdoGetdescripcion(this);
  }
  public String getCodContable() {
    return jdoGetcodContable(this);
  }
  public void setCodContable(String codContable) {
    jdoSetcodContable(this, codContable);
  }
  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }
  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }
  public long getIdCuentaContable() {
    return jdoGetidCuentaContable(this);
  }
  public void setIdCuentaContable(long idCuentaContable) {
    jdoSetidCuentaContable(this, idCuentaContable);
  }
  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }
  public String getTipo() {
    return jdoGettipo(this);
  }
  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }
  public int getIndicador() {
    return jdoGetindicador(this);
  }
  public void setIndicador(int indicador) {
    jdoSetindicador(this, indicador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CuentaContable localCuentaContable = new CuentaContable();
    localCuentaContable.jdoFlags = 1;
    localCuentaContable.jdoStateManager = paramStateManager;
    return localCuentaContable;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CuentaContable localCuentaContable = new CuentaContable();
    localCuentaContable.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCuentaContable.jdoFlags = 1;
    localCuentaContable.jdoStateManager = paramStateManager;
    return localCuentaContable;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codContable);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCuentaContable);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.indicador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codContable = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCuentaContable = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.indicador = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CuentaContable paramCuentaContable, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.codContable = paramCuentaContable.codContable;
      return;
    case 1:
      if (paramCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramCuentaContable.descripcion;
      return;
    case 2:
      if (paramCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.idCuentaContable = paramCuentaContable.idCuentaContable;
      return;
    case 3:
      if (paramCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.indicador = paramCuentaContable.indicador;
      return;
    case 4:
      if (paramCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramCuentaContable.organismo;
      return;
    case 5:
      if (paramCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramCuentaContable.tipo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CuentaContable))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CuentaContable localCuentaContable = (CuentaContable)paramObject;
    if (localCuentaContable.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCuentaContable, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CuentaContablePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CuentaContablePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CuentaContablePK))
      throw new IllegalArgumentException("arg1");
    CuentaContablePK localCuentaContablePK = (CuentaContablePK)paramObject;
    localCuentaContablePK.idCuentaContable = this.idCuentaContable;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CuentaContablePK))
      throw new IllegalArgumentException("arg1");
    CuentaContablePK localCuentaContablePK = (CuentaContablePK)paramObject;
    this.idCuentaContable = localCuentaContablePK.idCuentaContable;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CuentaContablePK))
      throw new IllegalArgumentException("arg2");
    CuentaContablePK localCuentaContablePK = (CuentaContablePK)paramObject;
    localCuentaContablePK.idCuentaContable = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CuentaContablePK))
      throw new IllegalArgumentException("arg2");
    CuentaContablePK localCuentaContablePK = (CuentaContablePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localCuentaContablePK.idCuentaContable);
  }

  private static final String jdoGetcodContable(CuentaContable paramCuentaContable)
  {
    if (paramCuentaContable.jdoFlags <= 0)
      return paramCuentaContable.codContable;
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaContable.codContable;
    if (localStateManager.isLoaded(paramCuentaContable, jdoInheritedFieldCount + 0))
      return paramCuentaContable.codContable;
    return localStateManager.getStringField(paramCuentaContable, jdoInheritedFieldCount + 0, paramCuentaContable.codContable);
  }

  private static final void jdoSetcodContable(CuentaContable paramCuentaContable, String paramString)
  {
    if (paramCuentaContable.jdoFlags == 0)
    {
      paramCuentaContable.codContable = paramString;
      return;
    }
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaContable.codContable = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaContable, jdoInheritedFieldCount + 0, paramCuentaContable.codContable, paramString);
  }

  private static final String jdoGetdescripcion(CuentaContable paramCuentaContable)
  {
    if (paramCuentaContable.jdoFlags <= 0)
      return paramCuentaContable.descripcion;
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaContable.descripcion;
    if (localStateManager.isLoaded(paramCuentaContable, jdoInheritedFieldCount + 1))
      return paramCuentaContable.descripcion;
    return localStateManager.getStringField(paramCuentaContable, jdoInheritedFieldCount + 1, paramCuentaContable.descripcion);
  }

  private static final void jdoSetdescripcion(CuentaContable paramCuentaContable, String paramString)
  {
    if (paramCuentaContable.jdoFlags == 0)
    {
      paramCuentaContable.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaContable.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaContable, jdoInheritedFieldCount + 1, paramCuentaContable.descripcion, paramString);
  }

  private static final long jdoGetidCuentaContable(CuentaContable paramCuentaContable)
  {
    return paramCuentaContable.idCuentaContable;
  }

  private static final void jdoSetidCuentaContable(CuentaContable paramCuentaContable, long paramLong)
  {
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaContable.idCuentaContable = paramLong;
      return;
    }
    localStateManager.setLongField(paramCuentaContable, jdoInheritedFieldCount + 2, paramCuentaContable.idCuentaContable, paramLong);
  }

  private static final int jdoGetindicador(CuentaContable paramCuentaContable)
  {
    if (paramCuentaContable.jdoFlags <= 0)
      return paramCuentaContable.indicador;
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaContable.indicador;
    if (localStateManager.isLoaded(paramCuentaContable, jdoInheritedFieldCount + 3))
      return paramCuentaContable.indicador;
    return localStateManager.getIntField(paramCuentaContable, jdoInheritedFieldCount + 3, paramCuentaContable.indicador);
  }

  private static final void jdoSetindicador(CuentaContable paramCuentaContable, int paramInt)
  {
    if (paramCuentaContable.jdoFlags == 0)
    {
      paramCuentaContable.indicador = paramInt;
      return;
    }
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaContable.indicador = paramInt;
      return;
    }
    localStateManager.setIntField(paramCuentaContable, jdoInheritedFieldCount + 3, paramCuentaContable.indicador, paramInt);
  }

  private static final Organismo jdoGetorganismo(CuentaContable paramCuentaContable)
  {
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaContable.organismo;
    if (localStateManager.isLoaded(paramCuentaContable, jdoInheritedFieldCount + 4))
      return paramCuentaContable.organismo;
    return (Organismo)localStateManager.getObjectField(paramCuentaContable, jdoInheritedFieldCount + 4, paramCuentaContable.organismo);
  }

  private static final void jdoSetorganismo(CuentaContable paramCuentaContable, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaContable.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramCuentaContable, jdoInheritedFieldCount + 4, paramCuentaContable.organismo, paramOrganismo);
  }

  private static final String jdoGettipo(CuentaContable paramCuentaContable)
  {
    if (paramCuentaContable.jdoFlags <= 0)
      return paramCuentaContable.tipo;
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
      return paramCuentaContable.tipo;
    if (localStateManager.isLoaded(paramCuentaContable, jdoInheritedFieldCount + 5))
      return paramCuentaContable.tipo;
    return localStateManager.getStringField(paramCuentaContable, jdoInheritedFieldCount + 5, paramCuentaContable.tipo);
  }

  private static final void jdoSettipo(CuentaContable paramCuentaContable, String paramString)
  {
    if (paramCuentaContable.jdoFlags == 0)
    {
      paramCuentaContable.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramCuentaContable.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramCuentaContable, jdoInheritedFieldCount + 5, paramCuentaContable.tipo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}