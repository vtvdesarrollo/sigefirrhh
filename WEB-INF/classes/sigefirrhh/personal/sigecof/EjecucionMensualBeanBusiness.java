package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class EjecucionMensualBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addEjecucionMensual(EjecucionMensual ejecucionMensual)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    EjecucionMensual ejecucionMensualNew = 
      (EjecucionMensual)BeanUtils.cloneBean(
      ejecucionMensual);

    PresupuestoEspecificaBeanBusiness presupuestoEspecificaBeanBusiness = new PresupuestoEspecificaBeanBusiness();

    if (ejecucionMensualNew.getPresupuestoEspecifica() != null) {
      ejecucionMensualNew.setPresupuestoEspecifica(
        presupuestoEspecificaBeanBusiness.findPresupuestoEspecificaById(
        ejecucionMensualNew.getPresupuestoEspecifica().getIdPresupuestoEspecifica()));
    }
    pm.makePersistent(ejecucionMensualNew);
  }

  public void updateEjecucionMensual(EjecucionMensual ejecucionMensual) throws Exception
  {
    EjecucionMensual ejecucionMensualModify = 
      findEjecucionMensualById(ejecucionMensual.getIdEjecucionMensual());

    PresupuestoEspecificaBeanBusiness presupuestoEspecificaBeanBusiness = new PresupuestoEspecificaBeanBusiness();

    if (ejecucionMensual.getPresupuestoEspecifica() != null) {
      ejecucionMensual.setPresupuestoEspecifica(
        presupuestoEspecificaBeanBusiness.findPresupuestoEspecificaById(
        ejecucionMensual.getPresupuestoEspecifica().getIdPresupuestoEspecifica()));
    }

    BeanUtils.copyProperties(ejecucionMensualModify, ejecucionMensual);
  }

  public void deleteEjecucionMensual(EjecucionMensual ejecucionMensual) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    EjecucionMensual ejecucionMensualDelete = 
      findEjecucionMensualById(ejecucionMensual.getIdEjecucionMensual());
    pm.deletePersistent(ejecucionMensualDelete);
  }

  public EjecucionMensual findEjecucionMensualById(long idEjecucionMensual) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idEjecucionMensual == pIdEjecucionMensual";
    Query query = pm.newQuery(EjecucionMensual.class, filter);

    query.declareParameters("long pIdEjecucionMensual");

    parameters.put("pIdEjecucionMensual", new Long(idEjecucionMensual));

    Collection colEjecucionMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colEjecucionMensual.iterator();
    return (EjecucionMensual)iterator.next();
  }

  public Collection findEjecucionMensualAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent ejecucionMensualExtent = pm.getExtent(
      EjecucionMensual.class, true);
    Query query = pm.newQuery(ejecucionMensualExtent);
    query.setOrdering("dependencia.codDependencia ascending, conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPresupuestoEspecifica(long idPresupuestoEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "presupuestoEspecifica.idPresupuestoEspecifica == pIdPresupuestoEspecifica";

    Query query = pm.newQuery(EjecucionMensual.class, filter);

    query.declareParameters("long pIdPresupuestoEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdPresupuestoEspecifica", new Long(idPresupuestoEspecifica));

    query.setOrdering("dependencia.codDependencia ascending, conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colEjecucionMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEjecucionMensual);

    return colEjecucionMensual;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(EjecucionMensual.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("dependencia.codDependencia ascending, conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colEjecucionMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEjecucionMensual);

    return colEjecucionMensual;
  }

  public Collection findByMes(int mes)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "mes == pMes";

    Query query = pm.newQuery(EjecucionMensual.class, filter);

    query.declareParameters("int pMes");
    HashMap parameters = new HashMap();

    parameters.put("pMes", new Integer(mes));

    query.setOrdering("dependencia.codDependencia ascending, conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colEjecucionMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEjecucionMensual);

    return colEjecucionMensual;
  }
}