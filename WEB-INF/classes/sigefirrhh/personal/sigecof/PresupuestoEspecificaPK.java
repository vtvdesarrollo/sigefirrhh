package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class PresupuestoEspecificaPK
  implements Serializable
{
  public long idPresupuestoEspecifica;

  public PresupuestoEspecificaPK()
  {
  }

  public PresupuestoEspecificaPK(long idPresupuestoEspecifica)
  {
    this.idPresupuestoEspecifica = idPresupuestoEspecifica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PresupuestoEspecificaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PresupuestoEspecificaPK thatPK)
  {
    return 
      this.idPresupuestoEspecifica == thatPK.idPresupuestoEspecifica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPresupuestoEspecifica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPresupuestoEspecifica);
  }
}