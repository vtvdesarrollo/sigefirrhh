package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ResumenMensualPK
  implements Serializable
{
  public long idResumenMensual;

  public ResumenMensualPK()
  {
  }

  public ResumenMensualPK(long idResumenMensual)
  {
    this.idResumenMensual = idResumenMensual;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ResumenMensualPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ResumenMensualPK thatPK)
  {
    return 
      this.idResumenMensual == thatPK.idResumenMensual;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idResumenMensual)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idResumenMensual);
  }
}