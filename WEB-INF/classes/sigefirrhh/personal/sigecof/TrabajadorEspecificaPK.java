package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class TrabajadorEspecificaPK
  implements Serializable
{
  public long idTrabajadorEspecifica;

  public TrabajadorEspecificaPK()
  {
  }

  public TrabajadorEspecificaPK(long idTrabajadorEspecifica)
  {
    this.idTrabajadorEspecifica = idTrabajadorEspecifica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TrabajadorEspecificaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TrabajadorEspecificaPK thatPK)
  {
    return 
      this.idTrabajadorEspecifica == thatPK.idTrabajadorEspecifica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTrabajadorEspecifica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTrabajadorEspecifica);
  }
}