package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.CategoriaPresupuesto;

public class SeguridadPresupuesto
  implements Serializable, PersistenceCapable
{
  private long idSeguridadPresupuesto;
  private CategoriaPresupuesto categoriaPresupuesto;
  private int anio;
  private int mes;
  private Date fechaCierre;
  private String usuario;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "categoriaPresupuesto", "fechaCierre", "idSeguridadPresupuesto", "mes", "usuario" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.CategoriaPresupuesto"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcategoriaPresupuesto(this).getDescCategoria() + "  -  " + 
      jdoGetanio(this) + "  -  " + 
      jdoGetmes(this) + "  -  " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaCierre(this));
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public CategoriaPresupuesto getCategoriaPresupuesto() {
    return jdoGetcategoriaPresupuesto(this);
  }

  public void setCategoriaPresupuesto(CategoriaPresupuesto categoriaPresupuesto) {
    jdoSetcategoriaPresupuesto(this, categoriaPresupuesto);
  }
  public long getIdSeguridadPresupuesto() {
    return jdoGetidSeguridadPresupuesto(this);
  }
  public void setIdSeguridadPresupuesto(long idSeguridadPresupuesto) {
    jdoSetidSeguridadPresupuesto(this, idSeguridadPresupuesto);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public String getUsuario() {
    return jdoGetusuario(this);
  }
  public void setUsuario(String usuario) {
    jdoSetusuario(this, usuario);
  }
  public Date getFechaCierre() {
    return jdoGetfechaCierre(this);
  }
  public void setFechaCierre(Date fechaCierre) {
    jdoSetfechaCierre(this, fechaCierre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.SeguridadPresupuesto"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadPresupuesto());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadPresupuesto localSeguridadPresupuesto = new SeguridadPresupuesto();
    localSeguridadPresupuesto.jdoFlags = 1;
    localSeguridadPresupuesto.jdoStateManager = paramStateManager;
    return localSeguridadPresupuesto;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadPresupuesto localSeguridadPresupuesto = new SeguridadPresupuesto();
    localSeguridadPresupuesto.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadPresupuesto.jdoFlags = 1;
    localSeguridadPresupuesto.jdoStateManager = paramStateManager;
    return localSeguridadPresupuesto;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.categoriaPresupuesto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCierre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadPresupuesto);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.categoriaPresupuesto = ((CategoriaPresupuesto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCierre = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadPresupuesto = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadPresupuesto paramSeguridadPresupuesto, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadPresupuesto.anio;
      return;
    case 1:
      if (paramSeguridadPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.categoriaPresupuesto = paramSeguridadPresupuesto.categoriaPresupuesto;
      return;
    case 2:
      if (paramSeguridadPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCierre = paramSeguridadPresupuesto.fechaCierre;
      return;
    case 3:
      if (paramSeguridadPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadPresupuesto = paramSeguridadPresupuesto.idSeguridadPresupuesto;
      return;
    case 4:
      if (paramSeguridadPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadPresupuesto.mes;
      return;
    case 5:
      if (paramSeguridadPresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadPresupuesto.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadPresupuesto))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadPresupuesto localSeguridadPresupuesto = (SeguridadPresupuesto)paramObject;
    if (localSeguridadPresupuesto.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadPresupuesto, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadPresupuestoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadPresupuestoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadPresupuestoPK))
      throw new IllegalArgumentException("arg1");
    SeguridadPresupuestoPK localSeguridadPresupuestoPK = (SeguridadPresupuestoPK)paramObject;
    localSeguridadPresupuestoPK.idSeguridadPresupuesto = this.idSeguridadPresupuesto;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadPresupuestoPK))
      throw new IllegalArgumentException("arg1");
    SeguridadPresupuestoPK localSeguridadPresupuestoPK = (SeguridadPresupuestoPK)paramObject;
    this.idSeguridadPresupuesto = localSeguridadPresupuestoPK.idSeguridadPresupuesto;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadPresupuestoPK))
      throw new IllegalArgumentException("arg2");
    SeguridadPresupuestoPK localSeguridadPresupuestoPK = (SeguridadPresupuestoPK)paramObject;
    localSeguridadPresupuestoPK.idSeguridadPresupuesto = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadPresupuestoPK))
      throw new IllegalArgumentException("arg2");
    SeguridadPresupuestoPK localSeguridadPresupuestoPK = (SeguridadPresupuestoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localSeguridadPresupuestoPK.idSeguridadPresupuesto);
  }

  private static final int jdoGetanio(SeguridadPresupuesto paramSeguridadPresupuesto)
  {
    if (paramSeguridadPresupuesto.jdoFlags <= 0)
      return paramSeguridadPresupuesto.anio;
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPresupuesto.anio;
    if (localStateManager.isLoaded(paramSeguridadPresupuesto, jdoInheritedFieldCount + 0))
      return paramSeguridadPresupuesto.anio;
    return localStateManager.getIntField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 0, paramSeguridadPresupuesto.anio);
  }

  private static final void jdoSetanio(SeguridadPresupuesto paramSeguridadPresupuesto, int paramInt)
  {
    if (paramSeguridadPresupuesto.jdoFlags == 0)
    {
      paramSeguridadPresupuesto.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPresupuesto.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 0, paramSeguridadPresupuesto.anio, paramInt);
  }

  private static final CategoriaPresupuesto jdoGetcategoriaPresupuesto(SeguridadPresupuesto paramSeguridadPresupuesto)
  {
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPresupuesto.categoriaPresupuesto;
    if (localStateManager.isLoaded(paramSeguridadPresupuesto, jdoInheritedFieldCount + 1))
      return paramSeguridadPresupuesto.categoriaPresupuesto;
    return (CategoriaPresupuesto)localStateManager.getObjectField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 1, paramSeguridadPresupuesto.categoriaPresupuesto);
  }

  private static final void jdoSetcategoriaPresupuesto(SeguridadPresupuesto paramSeguridadPresupuesto, CategoriaPresupuesto paramCategoriaPresupuesto)
  {
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPresupuesto.categoriaPresupuesto = paramCategoriaPresupuesto;
      return;
    }
    localStateManager.setObjectField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 1, paramSeguridadPresupuesto.categoriaPresupuesto, paramCategoriaPresupuesto);
  }

  private static final Date jdoGetfechaCierre(SeguridadPresupuesto paramSeguridadPresupuesto)
  {
    if (paramSeguridadPresupuesto.jdoFlags <= 0)
      return paramSeguridadPresupuesto.fechaCierre;
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPresupuesto.fechaCierre;
    if (localStateManager.isLoaded(paramSeguridadPresupuesto, jdoInheritedFieldCount + 2))
      return paramSeguridadPresupuesto.fechaCierre;
    return (Date)localStateManager.getObjectField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 2, paramSeguridadPresupuesto.fechaCierre);
  }

  private static final void jdoSetfechaCierre(SeguridadPresupuesto paramSeguridadPresupuesto, Date paramDate)
  {
    if (paramSeguridadPresupuesto.jdoFlags == 0)
    {
      paramSeguridadPresupuesto.fechaCierre = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPresupuesto.fechaCierre = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 2, paramSeguridadPresupuesto.fechaCierre, paramDate);
  }

  private static final long jdoGetidSeguridadPresupuesto(SeguridadPresupuesto paramSeguridadPresupuesto)
  {
    return paramSeguridadPresupuesto.idSeguridadPresupuesto;
  }

  private static final void jdoSetidSeguridadPresupuesto(SeguridadPresupuesto paramSeguridadPresupuesto, long paramLong)
  {
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPresupuesto.idSeguridadPresupuesto = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 3, paramSeguridadPresupuesto.idSeguridadPresupuesto, paramLong);
  }

  private static final int jdoGetmes(SeguridadPresupuesto paramSeguridadPresupuesto)
  {
    if (paramSeguridadPresupuesto.jdoFlags <= 0)
      return paramSeguridadPresupuesto.mes;
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPresupuesto.mes;
    if (localStateManager.isLoaded(paramSeguridadPresupuesto, jdoInheritedFieldCount + 4))
      return paramSeguridadPresupuesto.mes;
    return localStateManager.getIntField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 4, paramSeguridadPresupuesto.mes);
  }

  private static final void jdoSetmes(SeguridadPresupuesto paramSeguridadPresupuesto, int paramInt)
  {
    if (paramSeguridadPresupuesto.jdoFlags == 0)
    {
      paramSeguridadPresupuesto.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPresupuesto.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 4, paramSeguridadPresupuesto.mes, paramInt);
  }

  private static final String jdoGetusuario(SeguridadPresupuesto paramSeguridadPresupuesto)
  {
    if (paramSeguridadPresupuesto.jdoFlags <= 0)
      return paramSeguridadPresupuesto.usuario;
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadPresupuesto.usuario;
    if (localStateManager.isLoaded(paramSeguridadPresupuesto, jdoInheritedFieldCount + 5))
      return paramSeguridadPresupuesto.usuario;
    return localStateManager.getStringField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 5, paramSeguridadPresupuesto.usuario);
  }

  private static final void jdoSetusuario(SeguridadPresupuesto paramSeguridadPresupuesto, String paramString)
  {
    if (paramSeguridadPresupuesto.jdoFlags == 0)
    {
      paramSeguridadPresupuesto.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadPresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadPresupuesto.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadPresupuesto, jdoInheritedFieldCount + 5, paramSeguridadPresupuesto.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}