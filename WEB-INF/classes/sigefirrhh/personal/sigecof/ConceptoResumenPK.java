package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ConceptoResumenPK
  implements Serializable
{
  public long idConceptoResumen;

  public ConceptoResumenPK()
  {
  }

  public ConceptoResumenPK(long idConceptoResumen)
  {
    this.idConceptoResumen = idConceptoResumen;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoResumenPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoResumenPK thatPK)
  {
    return 
      this.idConceptoResumen == thatPK.idConceptoResumen;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoResumen)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoResumen);
  }
}