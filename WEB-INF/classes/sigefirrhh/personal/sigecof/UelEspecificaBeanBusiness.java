package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.base.estructura.UnidadEjecutoraBeanBusiness;

public class UelEspecificaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUelEspecifica(UelEspecifica uelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UelEspecifica uelEspecificaNew = 
      (UelEspecifica)BeanUtils.cloneBean(
      uelEspecifica);

    AccionEspecificaBeanBusiness accionEspecificaBeanBusiness = new AccionEspecificaBeanBusiness();

    if (uelEspecificaNew.getAccionEspecifica() != null) {
      uelEspecificaNew.setAccionEspecifica(
        accionEspecificaBeanBusiness.findAccionEspecificaById(
        uelEspecificaNew.getAccionEspecifica().getIdAccionEspecifica()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (uelEspecificaNew.getUnidadEjecutora() != null) {
      uelEspecificaNew.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        uelEspecificaNew.getUnidadEjecutora().getIdUnidadEjecutora()));
    }
    pm.makePersistent(uelEspecificaNew);
  }

  public void updateUelEspecifica(UelEspecifica uelEspecifica) throws Exception
  {
    UelEspecifica uelEspecificaModify = 
      findUelEspecificaById(uelEspecifica.getIdUelEspecifica());

    AccionEspecificaBeanBusiness accionEspecificaBeanBusiness = new AccionEspecificaBeanBusiness();

    if (uelEspecifica.getAccionEspecifica() != null) {
      uelEspecifica.setAccionEspecifica(
        accionEspecificaBeanBusiness.findAccionEspecificaById(
        uelEspecifica.getAccionEspecifica().getIdAccionEspecifica()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (uelEspecifica.getUnidadEjecutora() != null) {
      uelEspecifica.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        uelEspecifica.getUnidadEjecutora().getIdUnidadEjecutora()));
    }

    BeanUtils.copyProperties(uelEspecificaModify, uelEspecifica);
  }

  public void deleteUelEspecifica(UelEspecifica uelEspecifica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UelEspecifica uelEspecificaDelete = 
      findUelEspecificaById(uelEspecifica.getIdUelEspecifica());
    pm.deletePersistent(uelEspecificaDelete);
  }

  public UelEspecifica findUelEspecificaById(long idUelEspecifica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUelEspecifica == pIdUelEspecifica";
    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("long pIdUelEspecifica");

    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUelEspecifica.iterator();
    return (UelEspecifica)iterator.next();
  }

  public Collection findUelEspecificaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent uelEspecificaExtent = pm.getExtent(
      UelEspecifica.class, true);
    Query query = pm.newQuery(uelEspecificaExtent);
    query.setOrdering("anio ascending, unidadEjecutora.codUnidadEjecutora ascending, categoriaPresupuesto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAccionEspecifica(long idAccionEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "accionEspecifica.idAccionEspecifica == pIdAccionEspecifica";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("long pIdAccionEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdAccionEspecifica", new Long(idAccionEspecifica));

    query.setOrdering("anio ascending, unidadEjecutora.codUnidadEjecutora ascending, categoriaPresupuesto ascending");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }

  public Collection findByUnidadEjecutora(long idUnidadEjecutora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadEjecutora.idUnidadEjecutora == pIdUnidadEjecutora";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("long pIdUnidadEjecutora");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadEjecutora", new Long(idUnidadEjecutora));

    query.setOrdering("anio ascending, unidadEjecutora.codUnidadEjecutora ascending, categoriaPresupuesto ascending");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }

  public Collection findByCategoriaPresupuesto(String categoriaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "categoriaPresupuesto == pCategoriaPresupuesto";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("java.lang.String pCategoriaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pCategoriaPresupuesto", new String(categoriaPresupuesto));

    query.setOrdering("anio ascending, unidadEjecutora.codUnidadEjecutora ascending, categoriaPresupuesto ascending");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, unidadEjecutora.codUnidadEjecutora ascending, categoriaPresupuesto ascending");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }
}