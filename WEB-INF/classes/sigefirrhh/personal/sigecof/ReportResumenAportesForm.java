package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportResumenAportesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportResumenAportesForm.class.getName());
  private TipoPersonal tipoPersonal;
  private int reportId;
  private String formato = "1";
  private int idTipoPersonal;
  private long idUnidadAdministradora;
  private String selectCategoriaPresupuesto;
  private String selectConceptoAporte;
  private String selectTipoPersonal;
  private long idCategoriaPresupuesto;
  private long idConceptoAporte;
  private Collection listCategoriaPresupuesto;
  private String reportName;
  private String orden;
  private String detallarUnidadesEjecutoras = "S";
  private String detallarConceptos = "S";
  private Date fechaTope;
  private int anio;
  private int mes;
  private int periodo;
  private Calendar fechaInicial;
  private Calendar fechaFinal;
  private Collection listTipoPersonal;
  private Collection listUnidadAdministradora;
  private Collection listConceptoAporte;
  private DefinicionesNoGenFacade definicionesFacade;
  private DefinicionesFacadeExtend definicionesFacadeExtend;
  private LoginSession login;
  private EstructuraFacade estructuraFacade;
  private boolean showTipoPersonal = false;

  public boolean isShowAporte() {
    return (this.listConceptoAporte != null) && (!this.listConceptoAporte.isEmpty());
  }
  public boolean isShowTipoPersonal() {
    return this.showTipoPersonal;
  }
  public ReportResumenAportesForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.definicionesFacadeExtend = new DefinicionesFacadeExtend();
    this.reportName = "resumenapouelpp";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportResumenAportesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listUnidadAdministradora = this.estructuraFacade.findAllUnidadAdministradora();
      this.listCategoriaPresupuesto = this.definicionesFacade.findAllCategoriaPresupuesto();
      this.listConceptoAporte = this.definicionesFacade.findConceptoByTipoPrestamo(this.login.getIdOrganismo(), "A");
      log.error("size .............." + this.listConceptoAporte.size());
    } catch (Exception e) {
      this.listUnidadAdministradora = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public boolean isShowSemana() {
    if (this.tipoPersonal != null) {
      if (this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        return true;
      }
      return false;
    }

    return false;
  }
  public boolean isShowQuincena() {
    if (this.tipoPersonal != null) {
      if (!this.tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        return true;
      }
      return false;
    }

    return false;
  }
  public void cambiarNombreAReporte() {
    try {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      this.reportName += "resumenapo";

      if (this.idTipoPersonal != 0) {
        this.reportName += "tp";
      }

      this.reportName += "uel";

      if (this.detallarConceptos.equals("S"))
        this.reportName += "con";
      else {
        this.reportName += "pp";
      }

      if (this.idUnidadAdministradora != 0L)
        this.reportName += "_ua";
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      this.reportName += "resumenapo";

      if (this.idTipoPersonal != 0) {
        this.reportName += "tp";
      }

      this.reportName += "uel";

      if (this.detallarConceptos.equals("S"))
        this.reportName += "con";
      else {
        this.reportName += "pp";
      }

      if (this.idUnidadAdministradora != 0L) {
        this.reportName += "_ua";
      }

      FacesContext context = FacesContext.getCurrentInstance();

      if ((this.mes < 1) || (this.mes > 12)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_concepto", new Long(this.idConceptoAporte));
      if (this.idTipoPersonal != 0)
        parameters.put("id_tipo_personal", new Integer(this.idTipoPersonal));
      else {
        parameters.put("id_categoria_presupuesto", new Long(this.idCategoriaPresupuesto));
      }

      if (this.idUnidadAdministradora != 0L) {
        parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
      }
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));
      parameters.put("numero_nomina", new Integer(0));

      log.error("numero_periodo " + this.periodo);
      log.error("id_tipo_personal " + this.idTipoPersonal);
      log.error("id_unidad_administradora " + this.idUnidadAdministradora);
      log.error("anio " + this.anio);
      log.error("nes " + this.mes);

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/sigecof");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public void changeCategoriaPresupuesto(ValueChangeEvent event)
  {
    this.idCategoriaPresupuesto = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.listTipoPersonal = null;
      this.showTipoPersonal = false;
      if (this.idCategoriaPresupuesto != 0L) {
        this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByCategoriaPresupuesto(this.idCategoriaPresupuesto);

        this.showTipoPersonal = true;
      }

    }
    catch (Exception e)
    {
      this.showTipoPersonal = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Integer.valueOf(
      (String)event.getNewValue()).intValue();
    try {
      log.error("pasa 1");
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeConceptoAporte(ValueChangeEvent event)
  {
    this.idConceptoAporte = Long.valueOf(
      (String)event.getNewValue()).longValue();
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l)
  {
    this.idTipoPersonal = Integer.parseInt(l);
    try {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
    }
    catch (Exception localException) {
    }
  }

  public Collection getListCategoriaPresupuesto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCategoriaPresupuesto, "sigefirrhh.base.definiciones.CategoriaPresupuesto");
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public Collection getListUnidadAdministradora()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
  public Collection getListConceptoAporte() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoAporte, "sigefirrhh.base.definiciones.Concepto");
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setOrden(String string)
  {
    this.orden = string;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public Date getFechaTope()
  {
    return this.fechaTope;
  }
  public void setFechaTope(Date fechaTope) {
    this.fechaTope = fechaTope;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public int getPeriodo() {
    return this.periodo;
  }
  public void setPeriodo(int periodo) {
    this.periodo = periodo;
  }
  public long getIdUnidadAdministradora() {
    return this.idUnidadAdministradora;
  }
  public void setIdUnidadAdministradora(long idUnidadAdministradora) {
    this.idUnidadAdministradora = idUnidadAdministradora;
  }
  public String getDetallarConceptos() {
    return this.detallarConceptos;
  }
  public void setDetallarConceptos(String detallarConceptos) {
    this.detallarConceptos = detallarConceptos;
  }
  public String getDetallarUnidadesEjecutoras() {
    return this.detallarUnidadesEjecutoras;
  }
  public void setDetallarUnidadesEjecutoras(String detallarUnidadesEjecutoras) {
    this.detallarUnidadesEjecutoras = detallarUnidadesEjecutoras;
  }
  public String getSelectCategoriaPresupuesto() {
    return this.selectCategoriaPresupuesto;
  }
  public void setSelectCategoriaPresupuesto(String selectCategoriaPresupuesto) {
    this.selectCategoriaPresupuesto = selectCategoriaPresupuesto;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String selectTipoPersonal) {
    this.selectTipoPersonal = selectTipoPersonal;
  }
  public String getSelectConceptoAporte() {
    return this.selectConceptoAporte;
  }
  public void setSelectConceptoAporte(String selectConceptoAporte) {
    this.selectConceptoAporte = selectConceptoAporte;
  }
}