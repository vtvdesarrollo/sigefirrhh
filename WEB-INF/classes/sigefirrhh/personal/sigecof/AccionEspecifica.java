package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class AccionEspecifica
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idAccionEspecifica;
  private String tipo;
  private Proyecto proyecto;
  private AccionCentralizada accionCentralizada;
  private String codAccionEspecifica;
  private String denominacion;
  private int anio;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "accionCentralizada", "anio", "codAccionEspecifica", "denominacion", "idAccionEspecifica", "proyecto", "tipo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.sigecof.AccionCentralizada"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.Proyecto"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 24, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.AccionEspecifica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AccionEspecifica());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("P", "PROYECTO");
    LISTA_TIPO.put("A", "ACCION CENTRALIZADA");
  }

  public AccionEspecifica()
  {
    jdoSettipo(this, "A");
  }

  public String toString()
  {
    String string;
    String string;
    if (jdoGetproyecto(this) != null)
      string = jdoGetproyecto(this).getOrganismo().getCodSigecof() + jdoGetproyecto(this).getCodProyecto();
    else {
      string = jdoGetaccionCentralizada(this).getOrganismo().getCodSigecof() + jdoGetaccionCentralizada(this).getCodAccionCentralizada();
    }

    return string + jdoGetcodAccionEspecifica(this) + " - " + jdoGetdenominacion(this);
  }

  public AccionCentralizada getAccionCentralizada() {
    return jdoGetaccionCentralizada(this);
  }
  public void setAccionCentralizada(AccionCentralizada accionCentralizada) {
    jdoSetaccionCentralizada(this, accionCentralizada);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public String getCodAccionEspecifica() {
    return jdoGetcodAccionEspecifica(this);
  }
  public void setCodAccionEspecifica(String codAccionEspecifica) {
    jdoSetcodAccionEspecifica(this, codAccionEspecifica);
  }
  public String getDenominacion() {
    return jdoGetdenominacion(this);
  }
  public void setDenominacion(String denominacion) {
    jdoSetdenominacion(this, denominacion);
  }
  public long getIdAccionEspecifica() {
    return jdoGetidAccionEspecifica(this);
  }
  public void setIdAccionEspecifica(long idAccionEspecifica) {
    jdoSetidAccionEspecifica(this, idAccionEspecifica);
  }
  public Proyecto getProyecto() {
    return jdoGetproyecto(this);
  }
  public void setProyecto(Proyecto proyecto) {
    jdoSetproyecto(this, proyecto);
  }
  public String getTipo() {
    return jdoGettipo(this);
  }
  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AccionEspecifica localAccionEspecifica = new AccionEspecifica();
    localAccionEspecifica.jdoFlags = 1;
    localAccionEspecifica.jdoStateManager = paramStateManager;
    return localAccionEspecifica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AccionEspecifica localAccionEspecifica = new AccionEspecifica();
    localAccionEspecifica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAccionEspecifica.jdoFlags = 1;
    localAccionEspecifica.jdoStateManager = paramStateManager;
    return localAccionEspecifica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.accionCentralizada);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codAccionEspecifica);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.denominacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAccionEspecifica);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proyecto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accionCentralizada = ((AccionCentralizada)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codAccionEspecifica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.denominacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAccionEspecifica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proyecto = ((Proyecto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AccionEspecifica paramAccionEspecifica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAccionEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.accionCentralizada = paramAccionEspecifica.accionCentralizada;
      return;
    case 1:
      if (paramAccionEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramAccionEspecifica.anio;
      return;
    case 2:
      if (paramAccionEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.codAccionEspecifica = paramAccionEspecifica.codAccionEspecifica;
      return;
    case 3:
      if (paramAccionEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.denominacion = paramAccionEspecifica.denominacion;
      return;
    case 4:
      if (paramAccionEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.idAccionEspecifica = paramAccionEspecifica.idAccionEspecifica;
      return;
    case 5:
      if (paramAccionEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.proyecto = paramAccionEspecifica.proyecto;
      return;
    case 6:
      if (paramAccionEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramAccionEspecifica.tipo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AccionEspecifica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AccionEspecifica localAccionEspecifica = (AccionEspecifica)paramObject;
    if (localAccionEspecifica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAccionEspecifica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AccionEspecificaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AccionEspecificaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionEspecificaPK))
      throw new IllegalArgumentException("arg1");
    AccionEspecificaPK localAccionEspecificaPK = (AccionEspecificaPK)paramObject;
    localAccionEspecificaPK.idAccionEspecifica = this.idAccionEspecifica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionEspecificaPK))
      throw new IllegalArgumentException("arg1");
    AccionEspecificaPK localAccionEspecificaPK = (AccionEspecificaPK)paramObject;
    this.idAccionEspecifica = localAccionEspecificaPK.idAccionEspecifica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionEspecificaPK))
      throw new IllegalArgumentException("arg2");
    AccionEspecificaPK localAccionEspecificaPK = (AccionEspecificaPK)paramObject;
    localAccionEspecificaPK.idAccionEspecifica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionEspecificaPK))
      throw new IllegalArgumentException("arg2");
    AccionEspecificaPK localAccionEspecificaPK = (AccionEspecificaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localAccionEspecificaPK.idAccionEspecifica);
  }

  private static final AccionCentralizada jdoGetaccionCentralizada(AccionEspecifica paramAccionEspecifica)
  {
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEspecifica.accionCentralizada;
    if (localStateManager.isLoaded(paramAccionEspecifica, jdoInheritedFieldCount + 0))
      return paramAccionEspecifica.accionCentralizada;
    return (AccionCentralizada)localStateManager.getObjectField(paramAccionEspecifica, jdoInheritedFieldCount + 0, paramAccionEspecifica.accionCentralizada);
  }

  private static final void jdoSetaccionCentralizada(AccionEspecifica paramAccionEspecifica, AccionCentralizada paramAccionCentralizada)
  {
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEspecifica.accionCentralizada = paramAccionCentralizada;
      return;
    }
    localStateManager.setObjectField(paramAccionEspecifica, jdoInheritedFieldCount + 0, paramAccionEspecifica.accionCentralizada, paramAccionCentralizada);
  }

  private static final int jdoGetanio(AccionEspecifica paramAccionEspecifica)
  {
    if (paramAccionEspecifica.jdoFlags <= 0)
      return paramAccionEspecifica.anio;
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEspecifica.anio;
    if (localStateManager.isLoaded(paramAccionEspecifica, jdoInheritedFieldCount + 1))
      return paramAccionEspecifica.anio;
    return localStateManager.getIntField(paramAccionEspecifica, jdoInheritedFieldCount + 1, paramAccionEspecifica.anio);
  }

  private static final void jdoSetanio(AccionEspecifica paramAccionEspecifica, int paramInt)
  {
    if (paramAccionEspecifica.jdoFlags == 0)
    {
      paramAccionEspecifica.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEspecifica.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramAccionEspecifica, jdoInheritedFieldCount + 1, paramAccionEspecifica.anio, paramInt);
  }

  private static final String jdoGetcodAccionEspecifica(AccionEspecifica paramAccionEspecifica)
  {
    if (paramAccionEspecifica.jdoFlags <= 0)
      return paramAccionEspecifica.codAccionEspecifica;
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEspecifica.codAccionEspecifica;
    if (localStateManager.isLoaded(paramAccionEspecifica, jdoInheritedFieldCount + 2))
      return paramAccionEspecifica.codAccionEspecifica;
    return localStateManager.getStringField(paramAccionEspecifica, jdoInheritedFieldCount + 2, paramAccionEspecifica.codAccionEspecifica);
  }

  private static final void jdoSetcodAccionEspecifica(AccionEspecifica paramAccionEspecifica, String paramString)
  {
    if (paramAccionEspecifica.jdoFlags == 0)
    {
      paramAccionEspecifica.codAccionEspecifica = paramString;
      return;
    }
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEspecifica.codAccionEspecifica = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionEspecifica, jdoInheritedFieldCount + 2, paramAccionEspecifica.codAccionEspecifica, paramString);
  }

  private static final String jdoGetdenominacion(AccionEspecifica paramAccionEspecifica)
  {
    if (paramAccionEspecifica.jdoFlags <= 0)
      return paramAccionEspecifica.denominacion;
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEspecifica.denominacion;
    if (localStateManager.isLoaded(paramAccionEspecifica, jdoInheritedFieldCount + 3))
      return paramAccionEspecifica.denominacion;
    return localStateManager.getStringField(paramAccionEspecifica, jdoInheritedFieldCount + 3, paramAccionEspecifica.denominacion);
  }

  private static final void jdoSetdenominacion(AccionEspecifica paramAccionEspecifica, String paramString)
  {
    if (paramAccionEspecifica.jdoFlags == 0)
    {
      paramAccionEspecifica.denominacion = paramString;
      return;
    }
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEspecifica.denominacion = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionEspecifica, jdoInheritedFieldCount + 3, paramAccionEspecifica.denominacion, paramString);
  }

  private static final long jdoGetidAccionEspecifica(AccionEspecifica paramAccionEspecifica)
  {
    return paramAccionEspecifica.idAccionEspecifica;
  }

  private static final void jdoSetidAccionEspecifica(AccionEspecifica paramAccionEspecifica, long paramLong)
  {
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEspecifica.idAccionEspecifica = paramLong;
      return;
    }
    localStateManager.setLongField(paramAccionEspecifica, jdoInheritedFieldCount + 4, paramAccionEspecifica.idAccionEspecifica, paramLong);
  }

  private static final Proyecto jdoGetproyecto(AccionEspecifica paramAccionEspecifica)
  {
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEspecifica.proyecto;
    if (localStateManager.isLoaded(paramAccionEspecifica, jdoInheritedFieldCount + 5))
      return paramAccionEspecifica.proyecto;
    return (Proyecto)localStateManager.getObjectField(paramAccionEspecifica, jdoInheritedFieldCount + 5, paramAccionEspecifica.proyecto);
  }

  private static final void jdoSetproyecto(AccionEspecifica paramAccionEspecifica, Proyecto paramProyecto)
  {
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEspecifica.proyecto = paramProyecto;
      return;
    }
    localStateManager.setObjectField(paramAccionEspecifica, jdoInheritedFieldCount + 5, paramAccionEspecifica.proyecto, paramProyecto);
  }

  private static final String jdoGettipo(AccionEspecifica paramAccionEspecifica)
  {
    if (paramAccionEspecifica.jdoFlags <= 0)
      return paramAccionEspecifica.tipo;
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEspecifica.tipo;
    if (localStateManager.isLoaded(paramAccionEspecifica, jdoInheritedFieldCount + 6))
      return paramAccionEspecifica.tipo;
    return localStateManager.getStringField(paramAccionEspecifica, jdoInheritedFieldCount + 6, paramAccionEspecifica.tipo);
  }

  private static final void jdoSettipo(AccionEspecifica paramAccionEspecifica, String paramString)
  {
    if (paramAccionEspecifica.jdoFlags == 0)
    {
      paramAccionEspecifica.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramAccionEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEspecifica.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionEspecifica, jdoInheritedFieldCount + 6, paramAccionEspecifica.tipo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}