package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PartidaUelEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PartidaUelEspecificaForm.class.getName());
  private PartidaUelEspecifica partidaUelEspecifica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showPartidaUelEspecificaByUelEspecifica;
  private boolean showPartidaUelEspecificaByCuentaPresupuesto;
  private boolean showPartidaUelEspecificaByFuenteFinanciamiento;
  private boolean showPartidaUelEspecificaByAnio;
  private String findSelectUelEspecifica;
  private String findSelectCuentaPresupuesto;
  private String findSelectFuenteFinanciamiento;
  private int findAnio;
  private Collection findColUelEspecifica;
  private Collection findColCuentaPresupuesto;
  private Collection findColFuenteFinanciamiento;
  private Collection colUelEspecifica;
  private Collection colCuentaPresupuesto;
  private Collection colFuenteFinanciamiento;
  private String selectUelEspecifica;
  private String selectCuentaPresupuesto;
  private String selectFuenteFinanciamiento;
  private Object stateResultPartidaUelEspecificaByUelEspecifica = null;

  private Object stateResultPartidaUelEspecificaByCuentaPresupuesto = null;

  private Object stateResultPartidaUelEspecificaByFuenteFinanciamiento = null;

  private Object stateResultPartidaUelEspecificaByAnio = null;

  public String getFindSelectUelEspecifica()
  {
    return this.findSelectUelEspecifica;
  }
  public void setFindSelectUelEspecifica(String valUelEspecifica) {
    this.findSelectUelEspecifica = valUelEspecifica;
  }

  public Collection getFindColUelEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }
  public String getFindSelectCuentaPresupuesto() {
    return this.findSelectCuentaPresupuesto;
  }
  public void setFindSelectCuentaPresupuesto(String valCuentaPresupuesto) {
    this.findSelectCuentaPresupuesto = valCuentaPresupuesto;
  }

  public Collection getFindColCuentaPresupuesto() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCuentaPresupuesto.iterator();
    CuentaPresupuesto cuentaPresupuesto = null;
    while (iterator.hasNext()) {
      cuentaPresupuesto = (CuentaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cuentaPresupuesto.getIdCuentaPresupuesto()), 
        cuentaPresupuesto.toString()));
    }
    return col;
  }
  public String getFindSelectFuenteFinanciamiento() {
    return this.findSelectFuenteFinanciamiento;
  }
  public void setFindSelectFuenteFinanciamiento(String valFuenteFinanciamiento) {
    this.findSelectFuenteFinanciamiento = valFuenteFinanciamiento;
  }

  public Collection getFindColFuenteFinanciamiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColFuenteFinanciamiento.iterator();
    FuenteFinanciamiento fuenteFinanciamiento = null;
    while (iterator.hasNext()) {
      fuenteFinanciamiento = (FuenteFinanciamiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(fuenteFinanciamiento.getIdFuenteFinanciamiento()), 
        fuenteFinanciamiento.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectUelEspecifica()
  {
    return this.selectUelEspecifica;
  }
  public void setSelectUelEspecifica(String valUelEspecifica) {
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    this.partidaUelEspecifica.setUelEspecifica(null);
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      if (String.valueOf(uelEspecifica.getIdUelEspecifica()).equals(
        valUelEspecifica)) {
        this.partidaUelEspecifica.setUelEspecifica(
          uelEspecifica);
        break;
      }
    }
    this.selectUelEspecifica = valUelEspecifica;
  }
  public String getSelectCuentaPresupuesto() {
    return this.selectCuentaPresupuesto;
  }
  public void setSelectCuentaPresupuesto(String valCuentaPresupuesto) {
    Iterator iterator = this.colCuentaPresupuesto.iterator();
    CuentaPresupuesto cuentaPresupuesto = null;
    this.partidaUelEspecifica.setCuentaPresupuesto(null);
    while (iterator.hasNext()) {
      cuentaPresupuesto = (CuentaPresupuesto)iterator.next();
      if (String.valueOf(cuentaPresupuesto.getIdCuentaPresupuesto()).equals(
        valCuentaPresupuesto)) {
        this.partidaUelEspecifica.setCuentaPresupuesto(
          cuentaPresupuesto);
        break;
      }
    }
    this.selectCuentaPresupuesto = valCuentaPresupuesto;
  }
  public String getSelectFuenteFinanciamiento() {
    return this.selectFuenteFinanciamiento;
  }
  public void setSelectFuenteFinanciamiento(String valFuenteFinanciamiento) {
    Iterator iterator = this.colFuenteFinanciamiento.iterator();
    FuenteFinanciamiento fuenteFinanciamiento = null;
    this.partidaUelEspecifica.setFuenteFinanciamiento(null);
    while (iterator.hasNext()) {
      fuenteFinanciamiento = (FuenteFinanciamiento)iterator.next();
      if (String.valueOf(fuenteFinanciamiento.getIdFuenteFinanciamiento()).equals(
        valFuenteFinanciamiento)) {
        this.partidaUelEspecifica.setFuenteFinanciamiento(
          fuenteFinanciamiento);
        break;
      }
    }
    this.selectFuenteFinanciamiento = valFuenteFinanciamiento;
  }
  public Collection getResult() {
    return this.result;
  }

  public PartidaUelEspecifica getPartidaUelEspecifica() {
    if (this.partidaUelEspecifica == null) {
      this.partidaUelEspecifica = new PartidaUelEspecifica();
    }
    return this.partidaUelEspecifica;
  }

  public PartidaUelEspecificaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUelEspecifica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }

  public Collection getColCuentaPresupuesto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCuentaPresupuesto.iterator();
    CuentaPresupuesto cuentaPresupuesto = null;
    while (iterator.hasNext()) {
      cuentaPresupuesto = (CuentaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cuentaPresupuesto.getIdCuentaPresupuesto()), 
        cuentaPresupuesto.toString()));
    }
    return col;
  }

  public Collection getColFuenteFinanciamiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFuenteFinanciamiento.iterator();
    FuenteFinanciamiento fuenteFinanciamiento = null;
    while (iterator.hasNext()) {
      fuenteFinanciamiento = (FuenteFinanciamiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(fuenteFinanciamiento.getIdFuenteFinanciamiento()), 
        fuenteFinanciamiento.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColUelEspecifica = 
        this.sigecofFacade.findAllUelEspecifica();
      this.findColCuentaPresupuesto = 
        this.sigecofFacade.findCuentaPresupuestoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColFuenteFinanciamiento = 
        this.sigecofFacade.findAllFuenteFinanciamiento();

      this.colUelEspecifica = 
        this.sigecofFacade.findAllUelEspecifica();
      this.colCuentaPresupuesto = 
        this.sigecofFacade.findCuentaPresupuestoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colFuenteFinanciamiento = 
        this.sigecofFacade.findAllFuenteFinanciamiento();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPartidaUelEspecificaByUelEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findPartidaUelEspecificaByUelEspecifica(Long.valueOf(this.findSelectUelEspecifica).longValue());
      this.showPartidaUelEspecificaByUelEspecifica = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPartidaUelEspecificaByUelEspecifica)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findSelectCuentaPresupuesto = null;
    this.findSelectFuenteFinanciamiento = null;
    this.findAnio = 0;

    return null;
  }

  public String findPartidaUelEspecificaByCuentaPresupuesto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findPartidaUelEspecificaByCuentaPresupuesto(Long.valueOf(this.findSelectCuentaPresupuesto).longValue());
      this.showPartidaUelEspecificaByCuentaPresupuesto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPartidaUelEspecificaByCuentaPresupuesto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findSelectCuentaPresupuesto = null;
    this.findSelectFuenteFinanciamiento = null;
    this.findAnio = 0;

    return null;
  }

  public String findPartidaUelEspecificaByFuenteFinanciamiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findPartidaUelEspecificaByFuenteFinanciamiento(Long.valueOf(this.findSelectFuenteFinanciamiento).longValue());
      this.showPartidaUelEspecificaByFuenteFinanciamiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPartidaUelEspecificaByFuenteFinanciamiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findSelectCuentaPresupuesto = null;
    this.findSelectFuenteFinanciamiento = null;
    this.findAnio = 0;

    return null;
  }

  public String findPartidaUelEspecificaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findPartidaUelEspecificaByAnio(this.findAnio);
      this.showPartidaUelEspecificaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPartidaUelEspecificaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findSelectCuentaPresupuesto = null;
    this.findSelectFuenteFinanciamiento = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowPartidaUelEspecificaByUelEspecifica() {
    return this.showPartidaUelEspecificaByUelEspecifica;
  }
  public boolean isShowPartidaUelEspecificaByCuentaPresupuesto() {
    return this.showPartidaUelEspecificaByCuentaPresupuesto;
  }
  public boolean isShowPartidaUelEspecificaByFuenteFinanciamiento() {
    return this.showPartidaUelEspecificaByFuenteFinanciamiento;
  }
  public boolean isShowPartidaUelEspecificaByAnio() {
    return this.showPartidaUelEspecificaByAnio;
  }

  public String selectPartidaUelEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUelEspecifica = null;
    this.selectCuentaPresupuesto = null;
    this.selectFuenteFinanciamiento = null;

    long idPartidaUelEspecifica = 
      Long.parseLong((String)requestParameterMap.get("idPartidaUelEspecifica"));
    try
    {
      this.partidaUelEspecifica = 
        this.sigecofFacade.findPartidaUelEspecificaById(
        idPartidaUelEspecifica);
      if (this.partidaUelEspecifica.getUelEspecifica() != null) {
        this.selectUelEspecifica = 
          String.valueOf(this.partidaUelEspecifica.getUelEspecifica().getIdUelEspecifica());
      }
      if (this.partidaUelEspecifica.getCuentaPresupuesto() != null) {
        this.selectCuentaPresupuesto = 
          String.valueOf(this.partidaUelEspecifica.getCuentaPresupuesto().getIdCuentaPresupuesto());
      }
      if (this.partidaUelEspecifica.getFuenteFinanciamiento() != null) {
        this.selectFuenteFinanciamiento = 
          String.valueOf(this.partidaUelEspecifica.getFuenteFinanciamiento().getIdFuenteFinanciamiento());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.partidaUelEspecifica = null;
    this.showPartidaUelEspecificaByUelEspecifica = false;
    this.showPartidaUelEspecificaByCuentaPresupuesto = false;
    this.showPartidaUelEspecificaByFuenteFinanciamiento = false;
    this.showPartidaUelEspecificaByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addPartidaUelEspecifica(
          this.partidaUelEspecifica);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updatePartidaUelEspecifica(
          this.partidaUelEspecifica);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deletePartidaUelEspecifica(
        this.partidaUelEspecifica);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.partidaUelEspecifica = new PartidaUelEspecifica();

    this.selectUelEspecifica = null;

    this.selectCuentaPresupuesto = null;

    this.selectFuenteFinanciamiento = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.partidaUelEspecifica.setIdPartidaUelEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.PartidaUelEspecifica"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.partidaUelEspecifica = new PartidaUelEspecifica();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}