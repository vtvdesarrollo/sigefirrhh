package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class AccionCentralizadaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AccionCentralizadaForm.class.getName());
  private AccionCentralizada accionCentralizada;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showAccionCentralizadaByAnio;
  private boolean showAccionCentralizadaByCodAccionCentralizada;
  private boolean showAccionCentralizadaByDenominacion;
  private int findAnio;
  private String findCodAccionCentralizada;
  private String findDenominacion;
  private Object stateResultAccionCentralizadaByAnio = null;

  private Object stateResultAccionCentralizadaByCodAccionCentralizada = null;

  private Object stateResultAccionCentralizadaByDenominacion = null;

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public String getFindCodAccionCentralizada() {
    return this.findCodAccionCentralizada;
  }
  public void setFindCodAccionCentralizada(String findCodAccionCentralizada) {
    this.findCodAccionCentralizada = findCodAccionCentralizada;
  }
  public String getFindDenominacion() {
    return this.findDenominacion;
  }
  public void setFindDenominacion(String findDenominacion) {
    this.findDenominacion = findDenominacion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public AccionCentralizada getAccionCentralizada() {
    if (this.accionCentralizada == null) {
      this.accionCentralizada = new AccionCentralizada();
    }
    return this.accionCentralizada;
  }

  public AccionCentralizadaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findAccionCentralizadaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.sigecofFacade.findAccionCentralizadaByAnio(this.findAnio, idOrganismo);
      this.showAccionCentralizadaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAccionCentralizadaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findCodAccionCentralizada = null;
    this.findDenominacion = null;

    return null;
  }

  public String findAccionCentralizadaByCodAccionCentralizada()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.sigecofFacade.findAccionCentralizadaByCodAccionCentralizada(this.findCodAccionCentralizada, idOrganismo);
      this.showAccionCentralizadaByCodAccionCentralizada = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAccionCentralizadaByCodAccionCentralizada)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findCodAccionCentralizada = null;
    this.findDenominacion = null;

    return null;
  }

  public String findAccionCentralizadaByDenominacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.sigecofFacade.findAccionCentralizadaByDenominacion(this.findDenominacion, idOrganismo);
      this.showAccionCentralizadaByDenominacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAccionCentralizadaByDenominacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findCodAccionCentralizada = null;
    this.findDenominacion = null;

    return null;
  }

  public boolean isShowAccionCentralizadaByAnio() {
    return this.showAccionCentralizadaByAnio;
  }
  public boolean isShowAccionCentralizadaByCodAccionCentralizada() {
    return this.showAccionCentralizadaByCodAccionCentralizada;
  }
  public boolean isShowAccionCentralizadaByDenominacion() {
    return this.showAccionCentralizadaByDenominacion;
  }

  public String selectAccionCentralizada()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idAccionCentralizada = 
      Long.parseLong((String)requestParameterMap.get("idAccionCentralizada"));
    try
    {
      this.accionCentralizada = 
        this.sigecofFacade.findAccionCentralizadaById(
        idAccionCentralizada);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.accionCentralizada = null;
    this.showAccionCentralizadaByAnio = false;
    this.showAccionCentralizadaByCodAccionCentralizada = false;
    this.showAccionCentralizadaByDenominacion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addAccionCentralizada(
          this.accionCentralizada);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateAccionCentralizada(
          this.accionCentralizada);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteAccionCentralizada(
        this.accionCentralizada);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.accionCentralizada = new AccionCentralizada();

    this.accionCentralizada.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.accionCentralizada.setIdAccionCentralizada(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.AccionCentralizada"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.accionCentralizada = new AccionCentralizada();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}