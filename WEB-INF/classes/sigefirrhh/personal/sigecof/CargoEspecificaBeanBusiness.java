package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBeanBusiness;

public class CargoEspecificaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCargoEspecifica(CargoEspecifica cargoEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CargoEspecifica cargoEspecificaNew = 
      (CargoEspecifica)BeanUtils.cloneBean(
      cargoEspecifica);

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (cargoEspecificaNew.getUelEspecifica() != null) {
      cargoEspecificaNew.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        cargoEspecificaNew.getUelEspecifica().getIdUelEspecifica()));
    }

    RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();

    if (cargoEspecificaNew.getRegistroCargos() != null) {
      cargoEspecificaNew.setRegistroCargos(
        registroCargosBeanBusiness.findRegistroCargosById(
        cargoEspecificaNew.getRegistroCargos().getIdRegistroCargos()));
    }
    pm.makePersistent(cargoEspecificaNew);
  }

  public void updateCargoEspecifica(CargoEspecifica cargoEspecifica) throws Exception
  {
    CargoEspecifica cargoEspecificaModify = 
      findCargoEspecificaById(cargoEspecifica.getIdCargoEspecifica());

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (cargoEspecifica.getUelEspecifica() != null) {
      cargoEspecifica.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        cargoEspecifica.getUelEspecifica().getIdUelEspecifica()));
    }

    RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();

    if (cargoEspecifica.getRegistroCargos() != null) {
      cargoEspecifica.setRegistroCargos(
        registroCargosBeanBusiness.findRegistroCargosById(
        cargoEspecifica.getRegistroCargos().getIdRegistroCargos()));
    }

    BeanUtils.copyProperties(cargoEspecificaModify, cargoEspecifica);
  }

  public void deleteCargoEspecifica(CargoEspecifica cargoEspecifica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CargoEspecifica cargoEspecificaDelete = 
      findCargoEspecificaById(cargoEspecifica.getIdCargoEspecifica());
    pm.deletePersistent(cargoEspecificaDelete);
  }

  public CargoEspecifica findCargoEspecificaById(long idCargoEspecifica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCargoEspecifica == pIdCargoEspecifica";
    Query query = pm.newQuery(CargoEspecifica.class, filter);

    query.declareParameters("long pIdCargoEspecifica");

    parameters.put("pIdCargoEspecifica", new Long(idCargoEspecifica));

    Collection colCargoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCargoEspecifica.iterator();
    return (CargoEspecifica)iterator.next();
  }

  public Collection findCargoEspecificaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cargoEspecificaExtent = pm.getExtent(
      CargoEspecifica.class, true);
    Query query = pm.newQuery(cargoEspecificaExtent);
    query.setOrdering("anio ascending, uelEspecifica.accionEspecifica.codAccionEspecifica ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "uelEspecifica.idUelEspecifica == pIdUelEspecifica";

    Query query = pm.newQuery(CargoEspecifica.class, filter);

    query.declareParameters("long pIdUelEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));

    query.setOrdering("anio ascending, uelEspecifica.accionEspecifica.codAccionEspecifica ascending");

    Collection colCargoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargoEspecifica);

    return colCargoEspecifica;
  }

  public Collection findByRegistroCargos(long idRegistroCargos)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registroCargos.idRegistroCargos == pIdRegistroCargos";

    Query query = pm.newQuery(CargoEspecifica.class, filter);

    query.declareParameters("long pIdRegistroCargos");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistroCargos", new Long(idRegistroCargos));

    query.setOrdering("anio ascending, uelEspecifica.accionEspecifica.codAccionEspecifica ascending");

    Collection colCargoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargoEspecifica);

    return colCargoEspecifica;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(CargoEspecifica.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, uelEspecifica.accionEspecifica.codAccionEspecifica ascending");

    Collection colCargoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargoEspecifica);

    return colCargoEspecifica;
  }
}