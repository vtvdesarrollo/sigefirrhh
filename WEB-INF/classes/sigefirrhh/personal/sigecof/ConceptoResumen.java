package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.personal.trabajador.Trabajador;

public class ConceptoResumen
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  protected static final Map LISTA_SINO;
  private long idConceptoResumen;
  private UnidadAdministradora unidadAdministradora;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private int anio;
  private int mes;
  private int numeroNomina;
  private double unidades;
  private double monto;
  private String tipo;
  private String cerrado;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "cerrado", "conceptoTipoPersonal", "frecuenciaTipoPersonal", "idConceptoResumen", "mes", "monto", "numeroNomina", "tipo", "trabajador", "unidadAdministradora", "unidades" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaTipoPersonal"), Long.TYPE, Integer.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadAdministradora"), Double.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 26, 26, 24, 21, 21, 21, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.ConceptoResumen"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoResumen());

    LISTA_TIPO = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_TIPO.put("F", "FIJO");
    LISTA_TIPO.put("V", "VARIABLE");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ConceptoResumen()
  {
    jdoSettipo(this, "F");

    jdoSetcerrado(this, "S");
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + " - " + 
      jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + 
      a + " - " + jdoGetfrecuenciaTipoPersonal(this).getFrecuenciaPago().getCodFrecuenciaPago();
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public FrecuenciaTipoPersonal getFrecuenciaTipoPersonal() {
    return jdoGetfrecuenciaTipoPersonal(this);
  }

  public void setFrecuenciaTipoPersonal(FrecuenciaTipoPersonal frecuenciaTipoPersonal) {
    jdoSetfrecuenciaTipoPersonal(this, frecuenciaTipoPersonal);
  }
  public long getIdConceptoResumen() {
    return jdoGetidConceptoResumen(this);
  }
  public void setIdConceptoResumen(long idConceptoResumen) {
    jdoSetidConceptoResumen(this, idConceptoResumen);
  }
  public double getMonto() {
    return jdoGetmonto(this);
  }
  public void setMonto(double monto) {
    jdoSetmonto(this, monto);
  }
  public String getTipo() {
    return jdoGettipo(this);
  }
  public void setTipo(String tipo) {
    jdoSettipo(this, tipo);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }
  public double getUnidades() {
    return jdoGetunidades(this);
  }
  public void setUnidades(double unidades) {
    jdoSetunidades(this, unidades);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public String getCerrado() {
    return jdoGetcerrado(this);
  }
  public void setCerrado(String cerrado) {
    jdoSetcerrado(this, cerrado);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public int getNumeroNomina() {
    return jdoGetnumeroNomina(this);
  }
  public void setNumeroNomina(int numeroNomina) {
    jdoSetnumeroNomina(this, numeroNomina);
  }
  public UnidadAdministradora getUnidadAdministradora() {
    return jdoGetunidadAdministradora(this);
  }

  public void setUnidadAdministradora(UnidadAdministradora unidadAdministradora) {
    jdoSetunidadAdministradora(this, unidadAdministradora);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 12;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoResumen localConceptoResumen = new ConceptoResumen();
    localConceptoResumen.jdoFlags = 1;
    localConceptoResumen.jdoStateManager = paramStateManager;
    return localConceptoResumen;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoResumen localConceptoResumen = new ConceptoResumen();
    localConceptoResumen.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoResumen.jdoFlags = 1;
    localConceptoResumen.jdoStateManager = paramStateManager;
    return localConceptoResumen;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cerrado);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaTipoPersonal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoResumen);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroNomina);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadAdministradora);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cerrado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaTipoPersonal = ((FrecuenciaTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoResumen = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadAdministradora = ((UnidadAdministradora)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoResumen paramConceptoResumen, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramConceptoResumen.anio;
      return;
    case 1:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.cerrado = paramConceptoResumen.cerrado;
      return;
    case 2:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoResumen.conceptoTipoPersonal;
      return;
    case 3:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaTipoPersonal = paramConceptoResumen.frecuenciaTipoPersonal;
      return;
    case 4:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoResumen = paramConceptoResumen.idConceptoResumen;
      return;
    case 5:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramConceptoResumen.mes;
      return;
    case 6:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramConceptoResumen.monto;
      return;
    case 7:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.numeroNomina = paramConceptoResumen.numeroNomina;
      return;
    case 8:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramConceptoResumen.tipo;
      return;
    case 9:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramConceptoResumen.trabajador;
      return;
    case 10:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.unidadAdministradora = paramConceptoResumen.unidadAdministradora;
      return;
    case 11:
      if (paramConceptoResumen == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramConceptoResumen.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoResumen))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoResumen localConceptoResumen = (ConceptoResumen)paramObject;
    if (localConceptoResumen.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoResumen, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoResumenPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoResumenPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoResumenPK))
      throw new IllegalArgumentException("arg1");
    ConceptoResumenPK localConceptoResumenPK = (ConceptoResumenPK)paramObject;
    localConceptoResumenPK.idConceptoResumen = this.idConceptoResumen;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoResumenPK))
      throw new IllegalArgumentException("arg1");
    ConceptoResumenPK localConceptoResumenPK = (ConceptoResumenPK)paramObject;
    this.idConceptoResumen = localConceptoResumenPK.idConceptoResumen;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoResumenPK))
      throw new IllegalArgumentException("arg2");
    ConceptoResumenPK localConceptoResumenPK = (ConceptoResumenPK)paramObject;
    localConceptoResumenPK.idConceptoResumen = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoResumenPK))
      throw new IllegalArgumentException("arg2");
    ConceptoResumenPK localConceptoResumenPK = (ConceptoResumenPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localConceptoResumenPK.idConceptoResumen);
  }

  private static final int jdoGetanio(ConceptoResumen paramConceptoResumen)
  {
    if (paramConceptoResumen.jdoFlags <= 0)
      return paramConceptoResumen.anio;
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.anio;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 0))
      return paramConceptoResumen.anio;
    return localStateManager.getIntField(paramConceptoResumen, jdoInheritedFieldCount + 0, paramConceptoResumen.anio);
  }

  private static final void jdoSetanio(ConceptoResumen paramConceptoResumen, int paramInt)
  {
    if (paramConceptoResumen.jdoFlags == 0)
    {
      paramConceptoResumen.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoResumen, jdoInheritedFieldCount + 0, paramConceptoResumen.anio, paramInt);
  }

  private static final String jdoGetcerrado(ConceptoResumen paramConceptoResumen)
  {
    if (paramConceptoResumen.jdoFlags <= 0)
      return paramConceptoResumen.cerrado;
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.cerrado;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 1))
      return paramConceptoResumen.cerrado;
    return localStateManager.getStringField(paramConceptoResumen, jdoInheritedFieldCount + 1, paramConceptoResumen.cerrado);
  }

  private static final void jdoSetcerrado(ConceptoResumen paramConceptoResumen, String paramString)
  {
    if (paramConceptoResumen.jdoFlags == 0)
    {
      paramConceptoResumen.cerrado = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.cerrado = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoResumen, jdoInheritedFieldCount + 1, paramConceptoResumen.cerrado, paramString);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoResumen paramConceptoResumen)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 2))
      return paramConceptoResumen.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoResumen, jdoInheritedFieldCount + 2, paramConceptoResumen.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoResumen paramConceptoResumen, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoResumen, jdoInheritedFieldCount + 2, paramConceptoResumen.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final FrecuenciaTipoPersonal jdoGetfrecuenciaTipoPersonal(ConceptoResumen paramConceptoResumen)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.frecuenciaTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 3))
      return paramConceptoResumen.frecuenciaTipoPersonal;
    return (FrecuenciaTipoPersonal)localStateManager.getObjectField(paramConceptoResumen, jdoInheritedFieldCount + 3, paramConceptoResumen.frecuenciaTipoPersonal);
  }

  private static final void jdoSetfrecuenciaTipoPersonal(ConceptoResumen paramConceptoResumen, FrecuenciaTipoPersonal paramFrecuenciaTipoPersonal)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.frecuenciaTipoPersonal = paramFrecuenciaTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoResumen, jdoInheritedFieldCount + 3, paramConceptoResumen.frecuenciaTipoPersonal, paramFrecuenciaTipoPersonal);
  }

  private static final long jdoGetidConceptoResumen(ConceptoResumen paramConceptoResumen)
  {
    return paramConceptoResumen.idConceptoResumen;
  }

  private static final void jdoSetidConceptoResumen(ConceptoResumen paramConceptoResumen, long paramLong)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.idConceptoResumen = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoResumen, jdoInheritedFieldCount + 4, paramConceptoResumen.idConceptoResumen, paramLong);
  }

  private static final int jdoGetmes(ConceptoResumen paramConceptoResumen)
  {
    if (paramConceptoResumen.jdoFlags <= 0)
      return paramConceptoResumen.mes;
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.mes;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 5))
      return paramConceptoResumen.mes;
    return localStateManager.getIntField(paramConceptoResumen, jdoInheritedFieldCount + 5, paramConceptoResumen.mes);
  }

  private static final void jdoSetmes(ConceptoResumen paramConceptoResumen, int paramInt)
  {
    if (paramConceptoResumen.jdoFlags == 0)
    {
      paramConceptoResumen.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoResumen, jdoInheritedFieldCount + 5, paramConceptoResumen.mes, paramInt);
  }

  private static final double jdoGetmonto(ConceptoResumen paramConceptoResumen)
  {
    if (paramConceptoResumen.jdoFlags <= 0)
      return paramConceptoResumen.monto;
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.monto;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 6))
      return paramConceptoResumen.monto;
    return localStateManager.getDoubleField(paramConceptoResumen, jdoInheritedFieldCount + 6, paramConceptoResumen.monto);
  }

  private static final void jdoSetmonto(ConceptoResumen paramConceptoResumen, double paramDouble)
  {
    if (paramConceptoResumen.jdoFlags == 0)
    {
      paramConceptoResumen.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoResumen, jdoInheritedFieldCount + 6, paramConceptoResumen.monto, paramDouble);
  }

  private static final int jdoGetnumeroNomina(ConceptoResumen paramConceptoResumen)
  {
    if (paramConceptoResumen.jdoFlags <= 0)
      return paramConceptoResumen.numeroNomina;
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.numeroNomina;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 7))
      return paramConceptoResumen.numeroNomina;
    return localStateManager.getIntField(paramConceptoResumen, jdoInheritedFieldCount + 7, paramConceptoResumen.numeroNomina);
  }

  private static final void jdoSetnumeroNomina(ConceptoResumen paramConceptoResumen, int paramInt)
  {
    if (paramConceptoResumen.jdoFlags == 0)
    {
      paramConceptoResumen.numeroNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.numeroNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoResumen, jdoInheritedFieldCount + 7, paramConceptoResumen.numeroNomina, paramInt);
  }

  private static final String jdoGettipo(ConceptoResumen paramConceptoResumen)
  {
    if (paramConceptoResumen.jdoFlags <= 0)
      return paramConceptoResumen.tipo;
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.tipo;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 8))
      return paramConceptoResumen.tipo;
    return localStateManager.getStringField(paramConceptoResumen, jdoInheritedFieldCount + 8, paramConceptoResumen.tipo);
  }

  private static final void jdoSettipo(ConceptoResumen paramConceptoResumen, String paramString)
  {
    if (paramConceptoResumen.jdoFlags == 0)
    {
      paramConceptoResumen.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoResumen, jdoInheritedFieldCount + 8, paramConceptoResumen.tipo, paramString);
  }

  private static final Trabajador jdoGettrabajador(ConceptoResumen paramConceptoResumen)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.trabajador;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 9))
      return paramConceptoResumen.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramConceptoResumen, jdoInheritedFieldCount + 9, paramConceptoResumen.trabajador);
  }

  private static final void jdoSettrabajador(ConceptoResumen paramConceptoResumen, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramConceptoResumen, jdoInheritedFieldCount + 9, paramConceptoResumen.trabajador, paramTrabajador);
  }

  private static final UnidadAdministradora jdoGetunidadAdministradora(ConceptoResumen paramConceptoResumen)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.unidadAdministradora;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 10))
      return paramConceptoResumen.unidadAdministradora;
    return (UnidadAdministradora)localStateManager.getObjectField(paramConceptoResumen, jdoInheritedFieldCount + 10, paramConceptoResumen.unidadAdministradora);
  }

  private static final void jdoSetunidadAdministradora(ConceptoResumen paramConceptoResumen, UnidadAdministradora paramUnidadAdministradora)
  {
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.unidadAdministradora = paramUnidadAdministradora;
      return;
    }
    localStateManager.setObjectField(paramConceptoResumen, jdoInheritedFieldCount + 10, paramConceptoResumen.unidadAdministradora, paramUnidadAdministradora);
  }

  private static final double jdoGetunidades(ConceptoResumen paramConceptoResumen)
  {
    if (paramConceptoResumen.jdoFlags <= 0)
      return paramConceptoResumen.unidades;
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoResumen.unidades;
    if (localStateManager.isLoaded(paramConceptoResumen, jdoInheritedFieldCount + 11))
      return paramConceptoResumen.unidades;
    return localStateManager.getDoubleField(paramConceptoResumen, jdoInheritedFieldCount + 11, paramConceptoResumen.unidades);
  }

  private static final void jdoSetunidades(ConceptoResumen paramConceptoResumen, double paramDouble)
  {
    if (paramConceptoResumen.jdoFlags == 0)
    {
      paramConceptoResumen.unidades = paramDouble;
      return;
    }
    StateManager localStateManager = paramConceptoResumen.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoResumen.unidades = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramConceptoResumen, jdoInheritedFieldCount + 11, paramConceptoResumen.unidades, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}