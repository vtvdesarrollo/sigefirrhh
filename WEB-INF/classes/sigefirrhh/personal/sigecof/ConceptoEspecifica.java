package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.estructura.UnidadEjecutora;

public class ConceptoEspecifica
  implements Serializable, PersistenceCapable
{
  private long idConceptoEspecifica;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private UelEspecifica uelEspecifica;
  private int anio;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "conceptoTipoPersonal", "idConceptoEspecifica", "uelEspecifica" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.UelEspecifica") };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public String toString() {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + jdoGetuelEspecifica(this).getCategoriaPresupuesto() + " - " + jdoGetuelEspecifica(this).getUnidadEjecutora().getCodUnidadEjecutora();
  }

  public UelEspecifica getUelEspecifica()
  {
    return jdoGetuelEspecifica(this);
  }
  public void setUelEspecifica(UelEspecifica uelEspecifica) {
    jdoSetuelEspecifica(this, uelEspecifica);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public long getIdConceptoEspecifica() {
    return jdoGetidConceptoEspecifica(this);
  }
  public void setIdConceptoEspecifica(long idConceptoEspecifica) {
    jdoSetidConceptoEspecifica(this, idConceptoEspecifica);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.ConceptoEspecifica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoEspecifica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoEspecifica localConceptoEspecifica = new ConceptoEspecifica();
    localConceptoEspecifica.jdoFlags = 1;
    localConceptoEspecifica.jdoStateManager = paramStateManager;
    return localConceptoEspecifica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoEspecifica localConceptoEspecifica = new ConceptoEspecifica();
    localConceptoEspecifica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoEspecifica.jdoFlags = 1;
    localConceptoEspecifica.jdoStateManager = paramStateManager;
    return localConceptoEspecifica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoEspecifica);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.uelEspecifica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoEspecifica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.uelEspecifica = ((UelEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoEspecifica paramConceptoEspecifica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramConceptoEspecifica.anio;
      return;
    case 1:
      if (paramConceptoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoEspecifica.conceptoTipoPersonal;
      return;
    case 2:
      if (paramConceptoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoEspecifica = paramConceptoEspecifica.idConceptoEspecifica;
      return;
    case 3:
      if (paramConceptoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.uelEspecifica = paramConceptoEspecifica.uelEspecifica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoEspecifica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoEspecifica localConceptoEspecifica = (ConceptoEspecifica)paramObject;
    if (localConceptoEspecifica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoEspecifica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoEspecificaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoEspecificaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoEspecificaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoEspecificaPK localConceptoEspecificaPK = (ConceptoEspecificaPK)paramObject;
    localConceptoEspecificaPK.idConceptoEspecifica = this.idConceptoEspecifica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoEspecificaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoEspecificaPK localConceptoEspecificaPK = (ConceptoEspecificaPK)paramObject;
    this.idConceptoEspecifica = localConceptoEspecificaPK.idConceptoEspecifica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoEspecificaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoEspecificaPK localConceptoEspecificaPK = (ConceptoEspecificaPK)paramObject;
    localConceptoEspecificaPK.idConceptoEspecifica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoEspecificaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoEspecificaPK localConceptoEspecificaPK = (ConceptoEspecificaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConceptoEspecificaPK.idConceptoEspecifica);
  }

  private static final int jdoGetanio(ConceptoEspecifica paramConceptoEspecifica)
  {
    if (paramConceptoEspecifica.jdoFlags <= 0)
      return paramConceptoEspecifica.anio;
    StateManager localStateManager = paramConceptoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoEspecifica.anio;
    if (localStateManager.isLoaded(paramConceptoEspecifica, jdoInheritedFieldCount + 0))
      return paramConceptoEspecifica.anio;
    return localStateManager.getIntField(paramConceptoEspecifica, jdoInheritedFieldCount + 0, paramConceptoEspecifica.anio);
  }

  private static final void jdoSetanio(ConceptoEspecifica paramConceptoEspecifica, int paramInt)
  {
    if (paramConceptoEspecifica.jdoFlags == 0)
    {
      paramConceptoEspecifica.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramConceptoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEspecifica.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramConceptoEspecifica, jdoInheritedFieldCount + 0, paramConceptoEspecifica.anio, paramInt);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoEspecifica paramConceptoEspecifica)
  {
    StateManager localStateManager = paramConceptoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoEspecifica.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoEspecifica, jdoInheritedFieldCount + 1))
      return paramConceptoEspecifica.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoEspecifica, jdoInheritedFieldCount + 1, paramConceptoEspecifica.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoEspecifica paramConceptoEspecifica, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEspecifica.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoEspecifica, jdoInheritedFieldCount + 1, paramConceptoEspecifica.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final long jdoGetidConceptoEspecifica(ConceptoEspecifica paramConceptoEspecifica)
  {
    return paramConceptoEspecifica.idConceptoEspecifica;
  }

  private static final void jdoSetidConceptoEspecifica(ConceptoEspecifica paramConceptoEspecifica, long paramLong)
  {
    StateManager localStateManager = paramConceptoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEspecifica.idConceptoEspecifica = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoEspecifica, jdoInheritedFieldCount + 2, paramConceptoEspecifica.idConceptoEspecifica, paramLong);
  }

  private static final UelEspecifica jdoGetuelEspecifica(ConceptoEspecifica paramConceptoEspecifica)
  {
    StateManager localStateManager = paramConceptoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoEspecifica.uelEspecifica;
    if (localStateManager.isLoaded(paramConceptoEspecifica, jdoInheritedFieldCount + 3))
      return paramConceptoEspecifica.uelEspecifica;
    return (UelEspecifica)localStateManager.getObjectField(paramConceptoEspecifica, jdoInheritedFieldCount + 3, paramConceptoEspecifica.uelEspecifica);
  }

  private static final void jdoSetuelEspecifica(ConceptoEspecifica paramConceptoEspecifica, UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramConceptoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEspecifica.uelEspecifica = paramUelEspecifica;
      return;
    }
    localStateManager.setObjectField(paramConceptoEspecifica, jdoInheritedFieldCount + 3, paramConceptoEspecifica.uelEspecifica, paramUelEspecifica);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}