package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportInterfaceSapForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportInterfaceSapForm.class.getName());
  private int reportId;
  private String formato = "1";
  private long idTipoPersonal;
  private String reportName;
  private String orden;
  private java.util.Date fechaFinMes;
  private int anio;
  private int mes;
  private Collection listTipoPersonal;
  private Collection listUnidadAdministradora;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private LoginSession login;
  private int interfaz = 1;
  private int quincena = 1;
  private long idUnidadAdministradora = 0L;

  public ReportInterfaceSapForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.reportName = "interfazpresap";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportInterfaceSapForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      this.listTipoPersonal = new ArrayList();
      this.listUnidadAdministradora = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      this.reportName += "interfaz";

      if (this.interfaz == 1)
        this.reportName += "presap";
      else if (this.interfaz == 2)
        this.reportName += "consap";
      else {
        this.reportName += "consapcont";
      }

      if (this.idUnidadAdministradora != 0L)
        this.reportName += "ua";
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";
      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }
      this.reportName += "interfaz";

      if (this.interfaz == 1)
        this.reportName += "presap";
      else if (this.interfaz == 2)
        this.reportName += "consap";
      else {
        this.reportName += "consapcont";
      }

      if (this.idUnidadAdministradora != 0L) {
        this.reportName += "ua";
      }
      FacesContext context = FacesContext.getCurrentInstance();

      if ((this.mes < 1) || (this.mes > 12)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idUnidadAdministradora != 0L) {
        parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
      }
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));

      parameters.put("periodo", new Integer(this.quincena));
      log.error("Quincena:" + this.quincena);
      if (this.interfaz == 3)
      {
        if (this.mes < 12) {
          java.sql.Date fechaTopeSql = new java.sql.Date(this.anio - 1900, this.mes, 1);
          this.fechaFinMes = fechaTopeSql;
          log.error("fecha Sql1 " + fechaTopeSql);
        }
        else {
          java.sql.Date fechaTopeSql = new java.sql.Date(this.anio - 1900 + 1, 0, 1);
          this.fechaFinMes = fechaTopeSql;
          log.error("fecha Sql2 " + fechaTopeSql);
        }
        log.error("fecha fin mes " + this.fechaFinMes);
        parameters.put("fecha_fin", this.fechaFinMes);
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/sigecof");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListUnidadAdministradora()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int i) {
    this.reportId = i;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }

  public void setOrden(String string) {
    this.orden = string;
  }

  public String getFormato() {
    return this.formato;
  }

  public void setFormato(String string) {
    this.formato = string;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public int getInterfaz()
  {
    return this.interfaz;
  }

  public void setInterfaz(int interfaz)
  {
    this.interfaz = interfaz;
  }

  public int getQuincena()
  {
    return this.quincena;
  }

  public void setQuincena(int quincena)
  {
    this.quincena = quincena;
  }

  public long getIdTipoPersonal()
  {
    return this.idTipoPersonal;
  }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }

  public long getIdUnidadAdministradora()
  {
    return this.idUnidadAdministradora;
  }

  public void setIdUnidadAdministradora(long idUnidadAdministradora)
  {
    this.idUnidadAdministradora = idUnidadAdministradora;
  }
}