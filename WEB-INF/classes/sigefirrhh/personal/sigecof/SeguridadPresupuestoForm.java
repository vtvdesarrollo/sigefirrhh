package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.CategoriaPresupuesto;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.login.LoginSession;

public class SeguridadPresupuestoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeguridadPresupuestoForm.class.getName());
  private SeguridadPresupuesto seguridadPresupuesto;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showSeguridadPresupuestoByCategoriaPresupuesto;
  private boolean showSeguridadPresupuestoByAnio;
  private String findSelectCategoriaPresupuesto;
  private int findAnio;
  private Collection findColCategoriaPresupuesto;
  private Collection colCategoriaPresupuesto;
  private String selectCategoriaPresupuesto;
  private Object stateResultSeguridadPresupuestoByCategoriaPresupuesto = null;

  private Object stateResultSeguridadPresupuestoByAnio = null;

  public String getFindSelectCategoriaPresupuesto()
  {
    return this.findSelectCategoriaPresupuesto;
  }
  public void setFindSelectCategoriaPresupuesto(String valCategoriaPresupuesto) {
    this.findSelectCategoriaPresupuesto = valCategoriaPresupuesto;
  }

  public Collection getFindColCategoriaPresupuesto() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCategoriaPresupuesto.iterator();
    CategoriaPresupuesto categoriaPresupuesto = null;
    while (iterator.hasNext()) {
      categoriaPresupuesto = (CategoriaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaPresupuesto.getIdCategoriaPresupuesto()), 
        categoriaPresupuesto.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectCategoriaPresupuesto()
  {
    return this.selectCategoriaPresupuesto;
  }
  public void setSelectCategoriaPresupuesto(String valCategoriaPresupuesto) {
    Iterator iterator = this.colCategoriaPresupuesto.iterator();
    CategoriaPresupuesto categoriaPresupuesto = null;
    this.seguridadPresupuesto.setCategoriaPresupuesto(null);
    while (iterator.hasNext()) {
      categoriaPresupuesto = (CategoriaPresupuesto)iterator.next();
      if (String.valueOf(categoriaPresupuesto.getIdCategoriaPresupuesto()).equals(
        valCategoriaPresupuesto)) {
        this.seguridadPresupuesto.setCategoriaPresupuesto(
          categoriaPresupuesto);
        break;
      }
    }
    this.selectCategoriaPresupuesto = valCategoriaPresupuesto;
  }
  public Collection getResult() {
    return this.result;
  }

  public SeguridadPresupuesto getSeguridadPresupuesto() {
    if (this.seguridadPresupuesto == null) {
      this.seguridadPresupuesto = new SeguridadPresupuesto();
    }
    return this.seguridadPresupuesto;
  }

  public SeguridadPresupuestoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColCategoriaPresupuesto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCategoriaPresupuesto.iterator();
    CategoriaPresupuesto categoriaPresupuesto = null;
    while (iterator.hasNext()) {
      categoriaPresupuesto = (CategoriaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaPresupuesto.getIdCategoriaPresupuesto()), 
        categoriaPresupuesto.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColCategoriaPresupuesto = 
        this.definicionesFacade.findAllCategoriaPresupuesto();

      this.colCategoriaPresupuesto = 
        this.definicionesFacade.findAllCategoriaPresupuesto();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSeguridadPresupuestoByCategoriaPresupuesto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findSeguridadPresupuestoByCategoriaPresupuesto(Long.valueOf(this.findSelectCategoriaPresupuesto).longValue());
      this.showSeguridadPresupuestoByCategoriaPresupuesto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadPresupuestoByCategoriaPresupuesto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectCategoriaPresupuesto = null;
    this.findAnio = 0;

    return null;
  }

  public String findSeguridadPresupuestoByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findSeguridadPresupuestoByAnio(this.findAnio);
      this.showSeguridadPresupuestoByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadPresupuestoByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectCategoriaPresupuesto = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowSeguridadPresupuestoByCategoriaPresupuesto() {
    return this.showSeguridadPresupuestoByCategoriaPresupuesto;
  }
  public boolean isShowSeguridadPresupuestoByAnio() {
    return this.showSeguridadPresupuestoByAnio;
  }

  public String selectSeguridadPresupuesto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCategoriaPresupuesto = null;

    long idSeguridadPresupuesto = 
      Long.parseLong((String)requestParameterMap.get("idSeguridadPresupuesto"));
    try
    {
      this.seguridadPresupuesto = 
        this.sigecofFacade.findSeguridadPresupuestoById(
        idSeguridadPresupuesto);
      if (this.seguridadPresupuesto.getCategoriaPresupuesto() != null) {
        this.selectCategoriaPresupuesto = 
          String.valueOf(this.seguridadPresupuesto.getCategoriaPresupuesto().getIdCategoriaPresupuesto());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.seguridadPresupuesto = null;
    this.showSeguridadPresupuestoByCategoriaPresupuesto = false;
    this.showSeguridadPresupuestoByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.seguridadPresupuesto.getFechaCierre() != null) && 
      (this.seguridadPresupuesto.getFechaCierre().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha de Cierre no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addSeguridadPresupuesto(
          this.seguridadPresupuesto);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateSeguridadPresupuesto(
          this.seguridadPresupuesto);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteSeguridadPresupuesto(
        this.seguridadPresupuesto);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.seguridadPresupuesto = new SeguridadPresupuesto();

    this.selectCategoriaPresupuesto = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.seguridadPresupuesto.setIdSeguridadPresupuesto(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.SeguridadPresupuesto"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.seguridadPresupuesto = new SeguridadPresupuesto();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}