package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ConceptoEspecificaPK
  implements Serializable
{
  public long idConceptoEspecifica;

  public ConceptoEspecificaPK()
  {
  }

  public ConceptoEspecificaPK(long idConceptoEspecifica)
  {
    this.idConceptoEspecifica = idConceptoEspecifica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoEspecificaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoEspecificaPK thatPK)
  {
    return 
      this.idConceptoEspecifica == thatPK.idConceptoEspecifica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoEspecifica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoEspecifica);
  }
}