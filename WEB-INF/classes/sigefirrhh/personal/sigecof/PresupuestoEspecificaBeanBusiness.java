package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PresupuestoEspecificaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PresupuestoEspecifica presupuestoEspecificaNew = 
      (PresupuestoEspecifica)BeanUtils.cloneBean(
      presupuestoEspecifica);

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (presupuestoEspecificaNew.getUelEspecifica() != null) {
      presupuestoEspecificaNew.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        presupuestoEspecificaNew.getUelEspecifica().getIdUelEspecifica()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (presupuestoEspecificaNew.getCuentaPresupuesto() != null) {
      presupuestoEspecificaNew.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        presupuestoEspecificaNew.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }
    pm.makePersistent(presupuestoEspecificaNew);
  }

  public void updatePresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica) throws Exception
  {
    PresupuestoEspecifica presupuestoEspecificaModify = 
      findPresupuestoEspecificaById(presupuestoEspecifica.getIdPresupuestoEspecifica());

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (presupuestoEspecifica.getUelEspecifica() != null) {
      presupuestoEspecifica.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        presupuestoEspecifica.getUelEspecifica().getIdUelEspecifica()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (presupuestoEspecifica.getCuentaPresupuesto() != null) {
      presupuestoEspecifica.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        presupuestoEspecifica.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }

    BeanUtils.copyProperties(presupuestoEspecificaModify, presupuestoEspecifica);
  }

  public void deletePresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PresupuestoEspecifica presupuestoEspecificaDelete = 
      findPresupuestoEspecificaById(presupuestoEspecifica.getIdPresupuestoEspecifica());
    pm.deletePersistent(presupuestoEspecificaDelete);
  }

  public PresupuestoEspecifica findPresupuestoEspecificaById(long idPresupuestoEspecifica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPresupuestoEspecifica == pIdPresupuestoEspecifica";
    Query query = pm.newQuery(PresupuestoEspecifica.class, filter);

    query.declareParameters("long pIdPresupuestoEspecifica");

    parameters.put("pIdPresupuestoEspecifica", new Long(idPresupuestoEspecifica));

    Collection colPresupuestoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPresupuestoEspecifica.iterator();
    return (PresupuestoEspecifica)iterator.next();
  }

  public Collection findPresupuestoEspecificaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent presupuestoEspecificaExtent = pm.getExtent(
      PresupuestoEspecifica.class, true);
    Query query = pm.newQuery(presupuestoEspecificaExtent);
    query.setOrdering("anio ascending, codAccionEspecifica ascending, codCuentaPresupuesto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "uelEspecifica.idUelEspecifica == pIdUelEspecifica";

    Query query = pm.newQuery(PresupuestoEspecifica.class, filter);

    query.declareParameters("long pIdUelEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));

    query.setOrdering("anio ascending, codAccionEspecifica ascending, codCuentaPresupuesto ascending");

    Collection colPresupuestoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPresupuestoEspecifica);

    return colPresupuestoEspecifica;
  }

  public Collection findByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cuentaPresupuesto.idCuentaPresupuesto == pIdCuentaPresupuesto";

    Query query = pm.newQuery(PresupuestoEspecifica.class, filter);

    query.declareParameters("long pIdCuentaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pIdCuentaPresupuesto", new Long(idCuentaPresupuesto));

    query.setOrdering("anio ascending, codAccionEspecifica ascending, codCuentaPresupuesto ascending");

    Collection colPresupuestoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPresupuestoEspecifica);

    return colPresupuestoEspecifica;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(PresupuestoEspecifica.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, codAccionEspecifica ascending, codCuentaPresupuesto ascending");

    Collection colPresupuestoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPresupuestoEspecifica);

    return colPresupuestoEspecifica;
  }
}