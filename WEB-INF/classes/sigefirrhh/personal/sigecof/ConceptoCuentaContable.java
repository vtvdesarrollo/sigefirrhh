package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;

public class ConceptoCuentaContable
  implements Serializable, PersistenceCapable
{
  private long idConceptoCuentaContable;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private CuentaContable cuentaContable;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "cuentaContable", "idConceptoCuentaContable" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaContable"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - CC- " + jdoGetcuentaContable(this).getCodContable();
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public CuentaContable getCuentaContable() {
    return jdoGetcuentaContable(this);
  }

  public void setCuentaContable(CuentaContable cuentaContable) {
    jdoSetcuentaContable(this, cuentaContable);
  }

  public long getIdConceptoCuentaContable() {
    return jdoGetidConceptoCuentaContable(this);
  }

  public void setIdConceptoCuentaContable(long idConceptoCuentaContable) {
    jdoSetidConceptoCuentaContable(this, idConceptoCuentaContable);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.ConceptoCuentaContable"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoCuentaContable());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoCuentaContable localConceptoCuentaContable = new ConceptoCuentaContable();
    localConceptoCuentaContable.jdoFlags = 1;
    localConceptoCuentaContable.jdoStateManager = paramStateManager;
    return localConceptoCuentaContable;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoCuentaContable localConceptoCuentaContable = new ConceptoCuentaContable();
    localConceptoCuentaContable.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoCuentaContable.jdoFlags = 1;
    localConceptoCuentaContable.jdoStateManager = paramStateManager;
    return localConceptoCuentaContable;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cuentaContable);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoCuentaContable);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaContable = ((CuentaContable)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoCuentaContable = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoCuentaContable paramConceptoCuentaContable, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoCuentaContable.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaContable = paramConceptoCuentaContable.cuentaContable;
      return;
    case 2:
      if (paramConceptoCuentaContable == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoCuentaContable = paramConceptoCuentaContable.idConceptoCuentaContable;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoCuentaContable))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoCuentaContable localConceptoCuentaContable = (ConceptoCuentaContable)paramObject;
    if (localConceptoCuentaContable.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoCuentaContable, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoCuentaContablePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoCuentaContablePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCuentaContablePK))
      throw new IllegalArgumentException("arg1");
    ConceptoCuentaContablePK localConceptoCuentaContablePK = (ConceptoCuentaContablePK)paramObject;
    localConceptoCuentaContablePK.idConceptoCuentaContable = this.idConceptoCuentaContable;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCuentaContablePK))
      throw new IllegalArgumentException("arg1");
    ConceptoCuentaContablePK localConceptoCuentaContablePK = (ConceptoCuentaContablePK)paramObject;
    this.idConceptoCuentaContable = localConceptoCuentaContablePK.idConceptoCuentaContable;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCuentaContablePK))
      throw new IllegalArgumentException("arg2");
    ConceptoCuentaContablePK localConceptoCuentaContablePK = (ConceptoCuentaContablePK)paramObject;
    localConceptoCuentaContablePK.idConceptoCuentaContable = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCuentaContablePK))
      throw new IllegalArgumentException("arg2");
    ConceptoCuentaContablePK localConceptoCuentaContablePK = (ConceptoCuentaContablePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConceptoCuentaContablePK.idConceptoCuentaContable);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoCuentaContable paramConceptoCuentaContable)
  {
    StateManager localStateManager = paramConceptoCuentaContable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCuentaContable.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoCuentaContable, jdoInheritedFieldCount + 0))
      return paramConceptoCuentaContable.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoCuentaContable, jdoInheritedFieldCount + 0, paramConceptoCuentaContable.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoCuentaContable paramConceptoCuentaContable, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCuentaContable.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoCuentaContable, jdoInheritedFieldCount + 0, paramConceptoCuentaContable.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final CuentaContable jdoGetcuentaContable(ConceptoCuentaContable paramConceptoCuentaContable)
  {
    StateManager localStateManager = paramConceptoCuentaContable.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCuentaContable.cuentaContable;
    if (localStateManager.isLoaded(paramConceptoCuentaContable, jdoInheritedFieldCount + 1))
      return paramConceptoCuentaContable.cuentaContable;
    return (CuentaContable)localStateManager.getObjectField(paramConceptoCuentaContable, jdoInheritedFieldCount + 1, paramConceptoCuentaContable.cuentaContable);
  }

  private static final void jdoSetcuentaContable(ConceptoCuentaContable paramConceptoCuentaContable, CuentaContable paramCuentaContable)
  {
    StateManager localStateManager = paramConceptoCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCuentaContable.cuentaContable = paramCuentaContable;
      return;
    }
    localStateManager.setObjectField(paramConceptoCuentaContable, jdoInheritedFieldCount + 1, paramConceptoCuentaContable.cuentaContable, paramCuentaContable);
  }

  private static final long jdoGetidConceptoCuentaContable(ConceptoCuentaContable paramConceptoCuentaContable)
  {
    return paramConceptoCuentaContable.idConceptoCuentaContable;
  }

  private static final void jdoSetidConceptoCuentaContable(ConceptoCuentaContable paramConceptoCuentaContable, long paramLong)
  {
    StateManager localStateManager = paramConceptoCuentaContable.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCuentaContable.idConceptoCuentaContable = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoCuentaContable, jdoInheritedFieldCount + 2, paramConceptoCuentaContable.idConceptoCuentaContable, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}