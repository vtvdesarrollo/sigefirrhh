package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class CuentaPresupuestoPK
  implements Serializable
{
  public long idCuentaPresupuesto;

  public CuentaPresupuestoPK()
  {
  }

  public CuentaPresupuestoPK(long idCuentaPresupuesto)
  {
    this.idCuentaPresupuesto = idCuentaPresupuesto;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CuentaPresupuestoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CuentaPresupuestoPK thatPK)
  {
    return 
      this.idCuentaPresupuesto == thatPK.idCuentaPresupuesto;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCuentaPresupuesto)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCuentaPresupuesto);
  }
}