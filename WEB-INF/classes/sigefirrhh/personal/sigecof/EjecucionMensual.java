package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class EjecucionMensual
  implements Serializable, PersistenceCapable
{
  private long idEjecucionMensual;
  private PresupuestoEspecifica presupuestoEspecifica;
  private int anio;
  private int mes;
  private double montoEjecutado;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "idEjecucionMensual", "mes", "montoEjecutado", "presupuestoEspecifica" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.PresupuestoEspecifica") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public EjecucionMensual()
  {
    jdoSetmontoEjecutado(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoEjecutado(this));

    return jdoGetmes(this) + " - " + jdoGetpresupuestoEspecifica(this).getUelEspecifica().getCategoriaPresupuesto() + " - " + jdoGetpresupuestoEspecifica(this).getCuentaPresupuesto().getCodPresupuesto() + " - " + a;
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public long getIdEjecucionMensual() {
    return jdoGetidEjecucionMensual(this);
  }
  public void setIdEjecucionMensual(long idEjecucionMensual) {
    jdoSetidEjecucionMensual(this, idEjecucionMensual);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public double getMontoEjecutado() {
    return jdoGetmontoEjecutado(this);
  }
  public void setMontoEjecutado(double montoEjecutado) {
    jdoSetmontoEjecutado(this, montoEjecutado);
  }
  public PresupuestoEspecifica getPresupuestoEspecifica() {
    return jdoGetpresupuestoEspecifica(this);
  }

  public void setPresupuestoEspecifica(PresupuestoEspecifica presupuestoEspecifica) {
    jdoSetpresupuestoEspecifica(this, presupuestoEspecifica);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.EjecucionMensual"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new EjecucionMensual());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    EjecucionMensual localEjecucionMensual = new EjecucionMensual();
    localEjecucionMensual.jdoFlags = 1;
    localEjecucionMensual.jdoStateManager = paramStateManager;
    return localEjecucionMensual;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    EjecucionMensual localEjecucionMensual = new EjecucionMensual();
    localEjecucionMensual.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEjecucionMensual.jdoFlags = 1;
    localEjecucionMensual.jdoStateManager = paramStateManager;
    return localEjecucionMensual;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEjecucionMensual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoEjecutado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.presupuestoEspecifica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEjecucionMensual = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoEjecutado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.presupuestoEspecifica = ((PresupuestoEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(EjecucionMensual paramEjecucionMensual, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEjecucionMensual == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramEjecucionMensual.anio;
      return;
    case 1:
      if (paramEjecucionMensual == null)
        throw new IllegalArgumentException("arg1");
      this.idEjecucionMensual = paramEjecucionMensual.idEjecucionMensual;
      return;
    case 2:
      if (paramEjecucionMensual == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramEjecucionMensual.mes;
      return;
    case 3:
      if (paramEjecucionMensual == null)
        throw new IllegalArgumentException("arg1");
      this.montoEjecutado = paramEjecucionMensual.montoEjecutado;
      return;
    case 4:
      if (paramEjecucionMensual == null)
        throw new IllegalArgumentException("arg1");
      this.presupuestoEspecifica = paramEjecucionMensual.presupuestoEspecifica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof EjecucionMensual))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    EjecucionMensual localEjecucionMensual = (EjecucionMensual)paramObject;
    if (localEjecucionMensual.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEjecucionMensual, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EjecucionMensualPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EjecucionMensualPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EjecucionMensualPK))
      throw new IllegalArgumentException("arg1");
    EjecucionMensualPK localEjecucionMensualPK = (EjecucionMensualPK)paramObject;
    localEjecucionMensualPK.idEjecucionMensual = this.idEjecucionMensual;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EjecucionMensualPK))
      throw new IllegalArgumentException("arg1");
    EjecucionMensualPK localEjecucionMensualPK = (EjecucionMensualPK)paramObject;
    this.idEjecucionMensual = localEjecucionMensualPK.idEjecucionMensual;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EjecucionMensualPK))
      throw new IllegalArgumentException("arg2");
    EjecucionMensualPK localEjecucionMensualPK = (EjecucionMensualPK)paramObject;
    localEjecucionMensualPK.idEjecucionMensual = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EjecucionMensualPK))
      throw new IllegalArgumentException("arg2");
    EjecucionMensualPK localEjecucionMensualPK = (EjecucionMensualPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localEjecucionMensualPK.idEjecucionMensual);
  }

  private static final int jdoGetanio(EjecucionMensual paramEjecucionMensual)
  {
    if (paramEjecucionMensual.jdoFlags <= 0)
      return paramEjecucionMensual.anio;
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEjecucionMensual.anio;
    if (localStateManager.isLoaded(paramEjecucionMensual, jdoInheritedFieldCount + 0))
      return paramEjecucionMensual.anio;
    return localStateManager.getIntField(paramEjecucionMensual, jdoInheritedFieldCount + 0, paramEjecucionMensual.anio);
  }

  private static final void jdoSetanio(EjecucionMensual paramEjecucionMensual, int paramInt)
  {
    if (paramEjecucionMensual.jdoFlags == 0)
    {
      paramEjecucionMensual.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEjecucionMensual.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramEjecucionMensual, jdoInheritedFieldCount + 0, paramEjecucionMensual.anio, paramInt);
  }

  private static final long jdoGetidEjecucionMensual(EjecucionMensual paramEjecucionMensual)
  {
    return paramEjecucionMensual.idEjecucionMensual;
  }

  private static final void jdoSetidEjecucionMensual(EjecucionMensual paramEjecucionMensual, long paramLong)
  {
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEjecucionMensual.idEjecucionMensual = paramLong;
      return;
    }
    localStateManager.setLongField(paramEjecucionMensual, jdoInheritedFieldCount + 1, paramEjecucionMensual.idEjecucionMensual, paramLong);
  }

  private static final int jdoGetmes(EjecucionMensual paramEjecucionMensual)
  {
    if (paramEjecucionMensual.jdoFlags <= 0)
      return paramEjecucionMensual.mes;
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEjecucionMensual.mes;
    if (localStateManager.isLoaded(paramEjecucionMensual, jdoInheritedFieldCount + 2))
      return paramEjecucionMensual.mes;
    return localStateManager.getIntField(paramEjecucionMensual, jdoInheritedFieldCount + 2, paramEjecucionMensual.mes);
  }

  private static final void jdoSetmes(EjecucionMensual paramEjecucionMensual, int paramInt)
  {
    if (paramEjecucionMensual.jdoFlags == 0)
    {
      paramEjecucionMensual.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEjecucionMensual.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramEjecucionMensual, jdoInheritedFieldCount + 2, paramEjecucionMensual.mes, paramInt);
  }

  private static final double jdoGetmontoEjecutado(EjecucionMensual paramEjecucionMensual)
  {
    if (paramEjecucionMensual.jdoFlags <= 0)
      return paramEjecucionMensual.montoEjecutado;
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEjecucionMensual.montoEjecutado;
    if (localStateManager.isLoaded(paramEjecucionMensual, jdoInheritedFieldCount + 3))
      return paramEjecucionMensual.montoEjecutado;
    return localStateManager.getDoubleField(paramEjecucionMensual, jdoInheritedFieldCount + 3, paramEjecucionMensual.montoEjecutado);
  }

  private static final void jdoSetmontoEjecutado(EjecucionMensual paramEjecucionMensual, double paramDouble)
  {
    if (paramEjecucionMensual.jdoFlags == 0)
    {
      paramEjecucionMensual.montoEjecutado = paramDouble;
      return;
    }
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEjecucionMensual.montoEjecutado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEjecucionMensual, jdoInheritedFieldCount + 3, paramEjecucionMensual.montoEjecutado, paramDouble);
  }

  private static final PresupuestoEspecifica jdoGetpresupuestoEspecifica(EjecucionMensual paramEjecucionMensual)
  {
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
      return paramEjecucionMensual.presupuestoEspecifica;
    if (localStateManager.isLoaded(paramEjecucionMensual, jdoInheritedFieldCount + 4))
      return paramEjecucionMensual.presupuestoEspecifica;
    return (PresupuestoEspecifica)localStateManager.getObjectField(paramEjecucionMensual, jdoInheritedFieldCount + 4, paramEjecucionMensual.presupuestoEspecifica);
  }

  private static final void jdoSetpresupuestoEspecifica(EjecucionMensual paramEjecucionMensual, PresupuestoEspecifica paramPresupuestoEspecifica)
  {
    StateManager localStateManager = paramEjecucionMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramEjecucionMensual.presupuestoEspecifica = paramPresupuestoEspecifica;
      return;
    }
    localStateManager.setObjectField(paramEjecucionMensual, jdoInheritedFieldCount + 4, paramEjecucionMensual.presupuestoEspecifica, paramPresupuestoEspecifica);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}