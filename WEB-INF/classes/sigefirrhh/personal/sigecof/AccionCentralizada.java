package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class AccionCentralizada
  implements Serializable, PersistenceCapable
{
  private long idAccionCentralizada;
  private int anio;
  private String codAccionCentralizada;
  private String denominacion;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "codAccionCentralizada", "denominacion", "idAccionCentralizada", "organismo" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public AccionCentralizada()
  {
    jdoSetanio(this, 0);
  }

  public String toString()
  {
    return jdoGetcodAccionCentralizada(this) + " - " + jdoGetdenominacion(this);
  }

  public String getCodAccionCentralizada()
  {
    return jdoGetcodAccionCentralizada(this);
  }

  public void setCodAccionCentralizada(String codAccionCentralizada)
  {
    jdoSetcodAccionCentralizada(this, codAccionCentralizada);
  }

  public String getDenominacion()
  {
    return jdoGetdenominacion(this);
  }

  public void setDenominacion(String denominacion)
  {
    jdoSetdenominacion(this, denominacion);
  }

  public long getIdAccionCentralizada()
  {
    return jdoGetidAccionCentralizada(this);
  }

  public void setIdAccionCentralizada(long idAccionCentralizada)
  {
    jdoSetidAccionCentralizada(this, idAccionCentralizada);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.AccionCentralizada"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AccionCentralizada());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AccionCentralizada localAccionCentralizada = new AccionCentralizada();
    localAccionCentralizada.jdoFlags = 1;
    localAccionCentralizada.jdoStateManager = paramStateManager;
    return localAccionCentralizada;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AccionCentralizada localAccionCentralizada = new AccionCentralizada();
    localAccionCentralizada.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAccionCentralizada.jdoFlags = 1;
    localAccionCentralizada.jdoStateManager = paramStateManager;
    return localAccionCentralizada;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codAccionCentralizada);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.denominacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAccionCentralizada);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codAccionCentralizada = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.denominacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAccionCentralizada = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AccionCentralizada paramAccionCentralizada, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAccionCentralizada == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramAccionCentralizada.anio;
      return;
    case 1:
      if (paramAccionCentralizada == null)
        throw new IllegalArgumentException("arg1");
      this.codAccionCentralizada = paramAccionCentralizada.codAccionCentralizada;
      return;
    case 2:
      if (paramAccionCentralizada == null)
        throw new IllegalArgumentException("arg1");
      this.denominacion = paramAccionCentralizada.denominacion;
      return;
    case 3:
      if (paramAccionCentralizada == null)
        throw new IllegalArgumentException("arg1");
      this.idAccionCentralizada = paramAccionCentralizada.idAccionCentralizada;
      return;
    case 4:
      if (paramAccionCentralizada == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramAccionCentralizada.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AccionCentralizada))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AccionCentralizada localAccionCentralizada = (AccionCentralizada)paramObject;
    if (localAccionCentralizada.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAccionCentralizada, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AccionCentralizadaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AccionCentralizadaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionCentralizadaPK))
      throw new IllegalArgumentException("arg1");
    AccionCentralizadaPK localAccionCentralizadaPK = (AccionCentralizadaPK)paramObject;
    localAccionCentralizadaPK.idAccionCentralizada = this.idAccionCentralizada;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionCentralizadaPK))
      throw new IllegalArgumentException("arg1");
    AccionCentralizadaPK localAccionCentralizadaPK = (AccionCentralizadaPK)paramObject;
    this.idAccionCentralizada = localAccionCentralizadaPK.idAccionCentralizada;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionCentralizadaPK))
      throw new IllegalArgumentException("arg2");
    AccionCentralizadaPK localAccionCentralizadaPK = (AccionCentralizadaPK)paramObject;
    localAccionCentralizadaPK.idAccionCentralizada = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionCentralizadaPK))
      throw new IllegalArgumentException("arg2");
    AccionCentralizadaPK localAccionCentralizadaPK = (AccionCentralizadaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localAccionCentralizadaPK.idAccionCentralizada);
  }

  private static final int jdoGetanio(AccionCentralizada paramAccionCentralizada)
  {
    if (paramAccionCentralizada.jdoFlags <= 0)
      return paramAccionCentralizada.anio;
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
      return paramAccionCentralizada.anio;
    if (localStateManager.isLoaded(paramAccionCentralizada, jdoInheritedFieldCount + 0))
      return paramAccionCentralizada.anio;
    return localStateManager.getIntField(paramAccionCentralizada, jdoInheritedFieldCount + 0, paramAccionCentralizada.anio);
  }

  private static final void jdoSetanio(AccionCentralizada paramAccionCentralizada, int paramInt)
  {
    if (paramAccionCentralizada.jdoFlags == 0)
    {
      paramAccionCentralizada.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionCentralizada.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramAccionCentralizada, jdoInheritedFieldCount + 0, paramAccionCentralizada.anio, paramInt);
  }

  private static final String jdoGetcodAccionCentralizada(AccionCentralizada paramAccionCentralizada)
  {
    if (paramAccionCentralizada.jdoFlags <= 0)
      return paramAccionCentralizada.codAccionCentralizada;
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
      return paramAccionCentralizada.codAccionCentralizada;
    if (localStateManager.isLoaded(paramAccionCentralizada, jdoInheritedFieldCount + 1))
      return paramAccionCentralizada.codAccionCentralizada;
    return localStateManager.getStringField(paramAccionCentralizada, jdoInheritedFieldCount + 1, paramAccionCentralizada.codAccionCentralizada);
  }

  private static final void jdoSetcodAccionCentralizada(AccionCentralizada paramAccionCentralizada, String paramString)
  {
    if (paramAccionCentralizada.jdoFlags == 0)
    {
      paramAccionCentralizada.codAccionCentralizada = paramString;
      return;
    }
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionCentralizada.codAccionCentralizada = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionCentralizada, jdoInheritedFieldCount + 1, paramAccionCentralizada.codAccionCentralizada, paramString);
  }

  private static final String jdoGetdenominacion(AccionCentralizada paramAccionCentralizada)
  {
    if (paramAccionCentralizada.jdoFlags <= 0)
      return paramAccionCentralizada.denominacion;
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
      return paramAccionCentralizada.denominacion;
    if (localStateManager.isLoaded(paramAccionCentralizada, jdoInheritedFieldCount + 2))
      return paramAccionCentralizada.denominacion;
    return localStateManager.getStringField(paramAccionCentralizada, jdoInheritedFieldCount + 2, paramAccionCentralizada.denominacion);
  }

  private static final void jdoSetdenominacion(AccionCentralizada paramAccionCentralizada, String paramString)
  {
    if (paramAccionCentralizada.jdoFlags == 0)
    {
      paramAccionCentralizada.denominacion = paramString;
      return;
    }
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionCentralizada.denominacion = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionCentralizada, jdoInheritedFieldCount + 2, paramAccionCentralizada.denominacion, paramString);
  }

  private static final long jdoGetidAccionCentralizada(AccionCentralizada paramAccionCentralizada)
  {
    return paramAccionCentralizada.idAccionCentralizada;
  }

  private static final void jdoSetidAccionCentralizada(AccionCentralizada paramAccionCentralizada, long paramLong)
  {
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionCentralizada.idAccionCentralizada = paramLong;
      return;
    }
    localStateManager.setLongField(paramAccionCentralizada, jdoInheritedFieldCount + 3, paramAccionCentralizada.idAccionCentralizada, paramLong);
  }

  private static final Organismo jdoGetorganismo(AccionCentralizada paramAccionCentralizada)
  {
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
      return paramAccionCentralizada.organismo;
    if (localStateManager.isLoaded(paramAccionCentralizada, jdoInheritedFieldCount + 4))
      return paramAccionCentralizada.organismo;
    return (Organismo)localStateManager.getObjectField(paramAccionCentralizada, jdoInheritedFieldCount + 4, paramAccionCentralizada.organismo);
  }

  private static final void jdoSetorganismo(AccionCentralizada paramAccionCentralizada, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramAccionCentralizada.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionCentralizada.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramAccionCentralizada, jdoInheritedFieldCount + 4, paramAccionCentralizada.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}