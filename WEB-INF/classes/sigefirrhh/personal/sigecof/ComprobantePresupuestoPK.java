package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ComprobantePresupuestoPK
  implements Serializable
{
  public long idComprobantePresupuesto;

  public ComprobantePresupuestoPK()
  {
  }

  public ComprobantePresupuestoPK(long idComprobantePresupuesto)
  {
    this.idComprobantePresupuesto = idComprobantePresupuesto;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ComprobantePresupuestoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ComprobantePresupuestoPK thatPK)
  {
    return 
      this.idComprobantePresupuesto == thatPK.idComprobantePresupuesto;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idComprobantePresupuesto)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idComprobantePresupuesto);
  }
}