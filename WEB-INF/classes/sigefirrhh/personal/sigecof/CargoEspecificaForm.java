package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.AdministradoraUel;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CargoEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargoEspecificaForm.class.getName());
  private CargoEspecifica cargoEspecifica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private RegistroCargosFacade registroCargosFacade = new RegistroCargosFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showCargoEspecificaByUelEspecifica;
  private boolean showCargoEspecificaByAnio;
  private String findSelectUelEspecifica;
  private int findAnio;
  private Collection findColUelEspecifica;
  private Collection colUelEspecifica;
  private Collection colRegistroForRegistroCargos;
  private Collection colRegistroCargos;
  private String selectUelEspecifica;
  private String selectRegistroForRegistroCargos;
  private String selectRegistroCargos;
  private String tipo = "P";
  private String findSelectRegistroForRegistroCargos;
  private String findSelectRegistroCargos;
  private Collection findColRegistroForRegistroCargos;
  private Collection findColRegistroCargos;
  private boolean showCargoEspecificaByRegistroCargos;
  private Object stateResultCargoEspecificaByUelEspecifica = null;

  private Object stateResultCargoEspecificaByAnio = null;

  public Collection getFindColRegistroForRegistroCargos()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegistroForRegistroCargos.iterator();
    Registro registroForRegistroCargos = null;
    while (iterator.hasNext()) {
      registroForRegistroCargos = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroForRegistroCargos.getIdRegistro()), 
        registroForRegistroCargos.toString()));
    }
    return col;
  }
  public String getFindSelectRegistroForRegistroCargos() {
    return this.findSelectRegistroForRegistroCargos;
  }
  public void setFindSelectRegistroForRegistroCargos(String valRegistroForRegistroCargos) {
    this.findSelectRegistroForRegistroCargos = valRegistroForRegistroCargos;
  }
  public boolean isFindShowRegistroForRegistroCargos() {
    return this.findColRegistroForRegistroCargos != null;
  }
  public String getFindSelectRegistroCargos() {
    return this.findSelectRegistroCargos;
  }
  public void setFindSelectRegistroCargos(String valRegistroCargos) {
    this.findSelectRegistroCargos = valRegistroCargos;
  }

  public Collection getFindColRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroCargos.getIdRegistroCargos()), 
        registroCargos.toString()));
    }
    return col;
  }
  public boolean isFindShowRegistroCargos() {
    return this.findColRegistroCargos != null;
  }

  public void findChangeRegistroForRegistroCargos(ValueChangeEvent event) {
    long idRegistro = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColRegistroCargos = null;
      if (idRegistro > 0L)
        this.findColRegistroCargos = 
          this.registroCargosFacade.findRegistroCargosByRegistro(
          idRegistro);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowUelEspecifica() {
    return (this.colUelEspecifica != null) && (!this.colUelEspecifica.isEmpty());
  }

  public void changeTipo(ValueChangeEvent event)
  {
    String tipo = 
      (String)event.getNewValue();
    try
    {
      log.error(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,tipo" + tipo);
      this.tipo = tipo;

      this.selectUelEspecifica = null;

      findProyectoAccionByAnio();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findProyectoAccionByAnio() { FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.colUelEspecifica = null;
      log.error(".................................................idUE " + this.cargoEspecifica.getRegistroCargos().getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora());
      log.error("--------------------------------------------------anio " + this.findAnio);
      log.error("//////////////////////////////////////////////////tipo " + this.tipo);
      if (this.tipo.equals("P")) {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAnioAndTipo(this.cargoEspecifica.getRegistroCargos().getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora(), this.findAnio, "P");
        log.error("*********************************************1-colUEL " + this.colUelEspecifica.size());
      }
      else {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAnioAndTipo(this.cargoEspecifica.getRegistroCargos().getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora(), this.findAnio, "A");
        log.error("********************************************2-colUEL " + this.colUelEspecifica.size());
      }

      this.selectUelEspecifica = null;
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null; }

  public String getFindSelectUelEspecifica()
  {
    return this.findSelectUelEspecifica;
  }
  public void setFindSelectUelEspecifica(String valUelEspecifica) {
    this.findSelectUelEspecifica = valUelEspecifica;
  }

  public Collection getFindColUelEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectUelEspecifica()
  {
    return this.selectUelEspecifica;
  }
  public void setSelectUelEspecifica(String valUelEspecifica) {
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    this.cargoEspecifica.setUelEspecifica(null);
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      if (String.valueOf(uelEspecifica.getIdUelEspecifica()).equals(
        valUelEspecifica)) {
        this.cargoEspecifica.setUelEspecifica(
          uelEspecifica);
        break;
      }
    }
    this.selectUelEspecifica = valUelEspecifica;
  }
  public String getSelectRegistroForRegistroCargos() {
    return this.selectRegistroForRegistroCargos;
  }
  public void setSelectRegistroForRegistroCargos(String valRegistroForRegistroCargos) {
    this.selectRegistroForRegistroCargos = valRegistroForRegistroCargos;
  }
  public void changeRegistroForRegistroCargos(ValueChangeEvent event) {
    long idRegistro = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colRegistroCargos = null;
      if (idRegistro > 0L) {
        this.colRegistroCargos = 
          this.registroCargosFacade.findRegistroCargosByRegistro(
          idRegistro);
      } else {
        this.selectRegistroCargos = null;
        this.cargoEspecifica.setRegistroCargos(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectRegistroCargos = null;
      this.cargoEspecifica.setRegistroCargos(
        null);
    }
  }

  public void changeRegistroCargos(ValueChangeEvent event) {
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (idRegistroCargos > 0L)
      {
        findProyectoAccionByAnio();
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowRegistroForRegistroCargos() {
    return this.colRegistroForRegistroCargos != null;
  }
  public String getSelectRegistroCargos() {
    return this.selectRegistroCargos;
  }
  public void setSelectRegistroCargos(String valRegistroCargos) {
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    this.cargoEspecifica.setRegistroCargos(null);
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      if (String.valueOf(registroCargos.getIdRegistroCargos()).equals(
        valRegistroCargos)) {
        this.cargoEspecifica.setRegistroCargos(
          registroCargos);
        break;
      }
    }
    this.selectRegistroCargos = valRegistroCargos;
  }
  public boolean isShowRegistroCargos() {
    return this.colRegistroCargos != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public CargoEspecifica getCargoEspecifica() {
    if (this.cargoEspecifica == null) {
      this.cargoEspecifica = new CargoEspecifica();
    }
    return this.cargoEspecifica;
  }

  public CargoEspecificaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.findAnio = (new Date().getYear() + 1900);

    refresh();
  }

  public Collection getColUelEspecifica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }

  public Collection getColRegistroForRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroForRegistroCargos.iterator();
    Registro registroForRegistroCargos = null;
    while (iterator.hasNext()) {
      registroForRegistroCargos = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroForRegistroCargos.getIdRegistro()), 
        registroForRegistroCargos.toString()));
    }
    return col;
  }

  public Collection getColRegistroCargos()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroCargos.getIdRegistroCargos()), 
        registroCargos.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colRegistroForRegistroCargos = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.findColRegistroForRegistroCargos = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCargoEspecificaByRegistroCargos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findCargoEspecificaByRegistroCargos(Long.valueOf(this.findSelectRegistroCargos).longValue());
      this.showCargoEspecificaByRegistroCargos = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCargoEspecificaByRegistroCargos)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String findCargoEspecificaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findCargoEspecificaByAnio(this.findAnio);
      this.showCargoEspecificaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCargoEspecificaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowCargoEspecificaByUelEspecifica() {
    return this.showCargoEspecificaByUelEspecifica;
  }
  public boolean isShowCargoEspecificaByAnio() {
    return this.showCargoEspecificaByAnio;
  }

  public String selectCargoEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUelEspecifica = null;
    this.selectRegistroCargos = null;
    this.selectRegistroForRegistroCargos = null;

    long idCargoEspecifica = 
      Long.parseLong((String)requestParameterMap.get("idCargoEspecifica"));
    try
    {
      this.cargoEspecifica = 
        this.sigecofFacade.findCargoEspecificaById(
        idCargoEspecifica);
      if (this.cargoEspecifica.getUelEspecifica() != null) {
        this.selectUelEspecifica = 
          String.valueOf(this.cargoEspecifica.getUelEspecifica().getIdUelEspecifica());
      }
      if (this.cargoEspecifica.getRegistroCargos() != null) {
        this.selectRegistroCargos = 
          String.valueOf(this.cargoEspecifica.getRegistroCargos().getIdRegistroCargos());
      }

      RegistroCargos registroCargos = null;
      Registro registroForRegistroCargos = null;

      if (this.cargoEspecifica.getRegistroCargos() != null) {
        long idRegistroCargos = 
          this.cargoEspecifica.getRegistroCargos().getIdRegistroCargos();
        this.selectRegistroCargos = String.valueOf(idRegistroCargos);
        registroCargos = this.registroCargosFacade.findRegistroCargosById(
          idRegistroCargos);
        this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosByRegistro(
          registroCargos.getRegistro().getIdRegistro());

        long idRegistroForRegistroCargos = 
          this.cargoEspecifica.getRegistroCargos().getRegistro().getIdRegistro();
        this.selectRegistroForRegistroCargos = String.valueOf(idRegistroForRegistroCargos);
        registroForRegistroCargos = 
          this.registroFacade.findRegistroById(
          idRegistroForRegistroCargos);
        this.colRegistroForRegistroCargos = 
          this.registroFacade.findAllRegistro();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.cargoEspecifica = null;
    this.showCargoEspecificaByUelEspecifica = false;
    this.showCargoEspecificaByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addCargoEspecifica(
          this.cargoEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.cargoEspecifica);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateCargoEspecifica(
          this.cargoEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.cargoEspecifica);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cargo ya se encuentra asociado a esa categoría presupuestaria", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteCargoEspecifica(
        this.cargoEspecifica);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.cargoEspecifica);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.cargoEspecifica = new CargoEspecifica();

    this.selectUelEspecifica = null;

    this.selectRegistroCargos = null;

    this.selectRegistroForRegistroCargos = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.cargoEspecifica.setIdCargoEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.CargoEspecifica"));
    this.cargoEspecifica.setAnio(this.findAnio);
    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.cargoEspecifica = new CargoEspecifica();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public String getTipo()
  {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  public boolean isShowCargoEspecificaByRegistroCargos() {
    return this.showCargoEspecificaByRegistroCargos;
  }
}