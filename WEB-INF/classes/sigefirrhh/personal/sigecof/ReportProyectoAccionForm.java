package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportProyectoAccionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportProyectoAccionForm.class.getName());
  private int reportId;
  private String formato = "1";
  private long idProyecto;
  private long idAccionCentralizada;
  private String reportName;
  private String orden = "A";
  private String listar = "P";
  private String detalle = "C";
  private Collection listProyecto;
  private Collection listAccionCentralizada;
  private int anio;
  private LoginSession login;
  private SigecofNoGenFacade sigecofFacade;
  private Collection listAnio = new ArrayList();

  public boolean isShowProyecto() {
    return this.listar.equals("P");
  }
  public boolean isShowAccionCentralizada() {
    return this.listar.equals("A");
  }
  public boolean isShowOrdenado() {
    return this.detalle.equals("T");
  }
  public ReportProyectoAccionForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.sigecofFacade = new SigecofNoGenFacade();

    this.reportName = "proyectopersonalalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.anio = (new Date().getYear() + 1900);

    int anio = 1970;
    int cantidadSumar = this.anio - anio;

    for (int cantidad = 0; cantidadSumar == cantidad; cantidad++) {
      this.listAnio.add(Long.valueOf(String.valueOf(anio)));
      this.listAnio.add(String.valueOf(anio));
      anio++;
    }

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportProyectoAccionForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listAccionCentralizada = this.sigecofFacade.findAccionCentralizadaByAnio(this.anio, this.login.getIdOrganismo());
      this.listProyecto = this.sigecofFacade.findProyectoByAnio(this.anio, this.login.getIdOrganismo());
      log.error("list Proyecto " + this.listProyecto.size());
    }
    catch (Exception e) {
      this.listProyecto = new ArrayList();
      this.listAccionCentralizada = new ArrayList();
    }
  }

  public void changeTipo(ValueChangeEvent event) {
    String listar = 
      (String)event.getNewValue();
    try
    {
      this.listar = listar;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void cambiarNombreAReporte() {
    try { this.reportName = null;
      if (this.orden != null) {
        this.reportName = "";
        if (this.detalle.equals("C"))
          this.reportName = "cargo";
        else {
          this.reportName = "personal";
        }

        if (this.listar.equals("P")) {
          this.reportName = ("proyecto" + this.reportName);
          if (!this.detalle.equals("C")) {
            if (this.orden.equals("A"))
              this.reportName += "alf";
            else if (this.orden.equals("C"))
              this.reportName += "ced";
            else {
              this.reportName += "cod";
            }
          }
          if (this.idProyecto != 0L)
            this.reportName += "1";
        }
        else
        {
          this.reportName = ("accion" + this.reportName);
          if (!this.detalle.equals("C")) {
            if (this.orden.equals("A"))
              this.reportName += "alf";
            else if (this.orden.equals("C"))
              this.reportName += "ced";
            else {
              this.reportName += "cod";
            }
          }
          if (this.idAccionCentralizada != 0L) {
            this.reportName += "1";
          }
        }

        if (this.formato.equals("2"))
          this.reportName = ("a_" + this.reportName);
      }
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = null;
      if (this.orden != null) {
        this.reportName = "";
        if (this.detalle.equals("C"))
          this.reportName = "cargo";
        else {
          this.reportName = "personal";
        }

        if (this.listar.equals("P")) {
          this.reportName = ("proyecto" + this.reportName);
          if (!this.detalle.equals("C")) {
            if (this.orden.equals("A"))
              this.reportName += "alf";
            else if (this.orden.equals("C"))
              this.reportName += "ced";
            else {
              this.reportName += "cod";
            }
          }
          if (this.idProyecto != 0L)
            this.reportName += "1";
        }
        else
        {
          this.reportName = ("accion" + this.reportName);
          if (!this.detalle.equals("C")) {
            if (this.orden.equals("A"))
              this.reportName += "alf";
            else if (this.orden.equals("C"))
              this.reportName += "ced";
            else {
              this.reportName += "cod";
            }
          }
          if (this.idAccionCentralizada != 0L) {
            this.reportName += "1";
          }
        }

        if (this.formato.equals("2")) {
          this.reportName = ("a_" + this.reportName);
        }

      }

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      if ((this.listar.equals("P")) && (this.idProyecto != 0L)) {
        parameters.put("id_proyecto", new Long(this.idProyecto));
      }
      if ((this.listar.equals("A")) && (this.idAccionCentralizada != 0L)) {
        parameters.put("id_accion_centralizada", new Long(this.idAccionCentralizada));
      }
      parameters.put("id_organismo", new Long(this.login.getIdOrganismo()));
      parameters.put("anio", new Integer(this.anio));

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/sigecof");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListAnio()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listAnio.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListAccionCentralizada()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listAccionCentralizada, "sigefirrhh.personal.sigecof.AccionCentralizada");
  }

  public Collection getListProyecto()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listProyecto, "sigefirrhh.personal.sigecof.Proyecto");
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getOrden()
  {
    return this.orden;
  }

  public void setOrden(String string)
  {
    this.orden = string;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public int getAnio()
  {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public long getIdAccionCentralizada() {
    return this.idAccionCentralizada;
  }
  public void setIdAccionCentralizada(long idAccionCentralizada) {
    this.idAccionCentralizada = idAccionCentralizada;
  }
  public long getIdProyecto() {
    return this.idProyecto;
  }
  public void setIdProyecto(long idProyecto) {
    this.idProyecto = idProyecto;
  }
  public String getListar() {
    return this.listar;
  }
  public void setListar(String listar) {
    this.listar = listar;
  }
  public String getDetalle() {
    return this.detalle;
  }
  public void setDetalle(String detalle) {
    this.detalle = detalle;
  }
}