package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SeguridadPresupuestoNoGenBeanBusiness extends AbstractBeanBusiness
{
  public SeguridadPresupuesto findUltimoResumen(long idCategoriaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "categoriaPresupuesto.idCategoriaPresupuesto == pIdCategoriaPresupuesto ";

    Query query = pm.newQuery(SeguridadPresupuesto.class, filter);

    query.declareParameters("long pIdCategoriaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pIdCategoriaPresupuesto", new Long(idCategoriaPresupuesto));

    query.setOrdering("anio descending, mes descending");

    Collection colSeguridadPrestaciones = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadPrestaciones);
    Iterator iterSeguridadPrestaciones = colSeguridadPrestaciones.iterator();

    return (SeguridadPresupuesto)iterSeguridadPrestaciones.next();
  }
}