package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.UnidadEjecutora;

public class UelEspecifica
  implements Serializable, PersistenceCapable
{
  private long idUelEspecifica;
  private AccionEspecifica accionEspecifica;
  private UnidadEjecutora unidadEjecutora;
  private String categoriaPresupuesto;
  private int anio;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "accionEspecifica", "anio", "categoriaPresupuesto", "idUelEspecifica", "unidadEjecutora" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.sigecof.AccionEspecifica"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadEjecutora") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcategoriaPresupuesto(this) + " - " + jdoGetunidadEjecutora(this).getCodUnidadEjecutora();
  }

  public AccionEspecifica getAccionEspecifica() {
    return jdoGetaccionEspecifica(this);
  }
  public void setAccionEspecifica(AccionEspecifica accionEspecifica) {
    jdoSetaccionEspecifica(this, accionEspecifica);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public String getCategoriaPresupuesto() {
    return jdoGetcategoriaPresupuesto(this);
  }
  public void setCategoriaPresupuesto(String categoriaPresupuesto) {
    jdoSetcategoriaPresupuesto(this, categoriaPresupuesto);
  }
  public long getIdUelEspecifica() {
    return jdoGetidUelEspecifica(this);
  }
  public void setIdUelEspecifica(long idUelEspecifica) {
    jdoSetidUelEspecifica(this, idUelEspecifica);
  }
  public UnidadEjecutora getUnidadEjecutora() {
    return jdoGetunidadEjecutora(this);
  }
  public void setUnidadEjecutora(UnidadEjecutora unidadEjecutora) {
    jdoSetunidadEjecutora(this, unidadEjecutora);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.UelEspecifica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UelEspecifica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UelEspecifica localUelEspecifica = new UelEspecifica();
    localUelEspecifica.jdoFlags = 1;
    localUelEspecifica.jdoStateManager = paramStateManager;
    return localUelEspecifica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UelEspecifica localUelEspecifica = new UelEspecifica();
    localUelEspecifica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUelEspecifica.jdoFlags = 1;
    localUelEspecifica.jdoStateManager = paramStateManager;
    return localUelEspecifica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.accionEspecifica);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.categoriaPresupuesto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUelEspecifica);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadEjecutora);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accionEspecifica = ((AccionEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.categoriaPresupuesto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUelEspecifica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadEjecutora = ((UnidadEjecutora)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UelEspecifica paramUelEspecifica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.accionEspecifica = paramUelEspecifica.accionEspecifica;
      return;
    case 1:
      if (paramUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramUelEspecifica.anio;
      return;
    case 2:
      if (paramUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.categoriaPresupuesto = paramUelEspecifica.categoriaPresupuesto;
      return;
    case 3:
      if (paramUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.idUelEspecifica = paramUelEspecifica.idUelEspecifica;
      return;
    case 4:
      if (paramUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.unidadEjecutora = paramUelEspecifica.unidadEjecutora;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UelEspecifica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UelEspecifica localUelEspecifica = (UelEspecifica)paramObject;
    if (localUelEspecifica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUelEspecifica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UelEspecificaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UelEspecificaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UelEspecificaPK))
      throw new IllegalArgumentException("arg1");
    UelEspecificaPK localUelEspecificaPK = (UelEspecificaPK)paramObject;
    localUelEspecificaPK.idUelEspecifica = this.idUelEspecifica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UelEspecificaPK))
      throw new IllegalArgumentException("arg1");
    UelEspecificaPK localUelEspecificaPK = (UelEspecificaPK)paramObject;
    this.idUelEspecifica = localUelEspecificaPK.idUelEspecifica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UelEspecificaPK))
      throw new IllegalArgumentException("arg2");
    UelEspecificaPK localUelEspecificaPK = (UelEspecificaPK)paramObject;
    localUelEspecificaPK.idUelEspecifica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UelEspecificaPK))
      throw new IllegalArgumentException("arg2");
    UelEspecificaPK localUelEspecificaPK = (UelEspecificaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localUelEspecificaPK.idUelEspecifica);
  }

  private static final AccionEspecifica jdoGetaccionEspecifica(UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramUelEspecifica.accionEspecifica;
    if (localStateManager.isLoaded(paramUelEspecifica, jdoInheritedFieldCount + 0))
      return paramUelEspecifica.accionEspecifica;
    return (AccionEspecifica)localStateManager.getObjectField(paramUelEspecifica, jdoInheritedFieldCount + 0, paramUelEspecifica.accionEspecifica);
  }

  private static final void jdoSetaccionEspecifica(UelEspecifica paramUelEspecifica, AccionEspecifica paramAccionEspecifica)
  {
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUelEspecifica.accionEspecifica = paramAccionEspecifica;
      return;
    }
    localStateManager.setObjectField(paramUelEspecifica, jdoInheritedFieldCount + 0, paramUelEspecifica.accionEspecifica, paramAccionEspecifica);
  }

  private static final int jdoGetanio(UelEspecifica paramUelEspecifica)
  {
    if (paramUelEspecifica.jdoFlags <= 0)
      return paramUelEspecifica.anio;
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramUelEspecifica.anio;
    if (localStateManager.isLoaded(paramUelEspecifica, jdoInheritedFieldCount + 1))
      return paramUelEspecifica.anio;
    return localStateManager.getIntField(paramUelEspecifica, jdoInheritedFieldCount + 1, paramUelEspecifica.anio);
  }

  private static final void jdoSetanio(UelEspecifica paramUelEspecifica, int paramInt)
  {
    if (paramUelEspecifica.jdoFlags == 0)
    {
      paramUelEspecifica.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUelEspecifica.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramUelEspecifica, jdoInheritedFieldCount + 1, paramUelEspecifica.anio, paramInt);
  }

  private static final String jdoGetcategoriaPresupuesto(UelEspecifica paramUelEspecifica)
  {
    if (paramUelEspecifica.jdoFlags <= 0)
      return paramUelEspecifica.categoriaPresupuesto;
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramUelEspecifica.categoriaPresupuesto;
    if (localStateManager.isLoaded(paramUelEspecifica, jdoInheritedFieldCount + 2))
      return paramUelEspecifica.categoriaPresupuesto;
    return localStateManager.getStringField(paramUelEspecifica, jdoInheritedFieldCount + 2, paramUelEspecifica.categoriaPresupuesto);
  }

  private static final void jdoSetcategoriaPresupuesto(UelEspecifica paramUelEspecifica, String paramString)
  {
    if (paramUelEspecifica.jdoFlags == 0)
    {
      paramUelEspecifica.categoriaPresupuesto = paramString;
      return;
    }
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUelEspecifica.categoriaPresupuesto = paramString;
      return;
    }
    localStateManager.setStringField(paramUelEspecifica, jdoInheritedFieldCount + 2, paramUelEspecifica.categoriaPresupuesto, paramString);
  }

  private static final long jdoGetidUelEspecifica(UelEspecifica paramUelEspecifica)
  {
    return paramUelEspecifica.idUelEspecifica;
  }

  private static final void jdoSetidUelEspecifica(UelEspecifica paramUelEspecifica, long paramLong)
  {
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUelEspecifica.idUelEspecifica = paramLong;
      return;
    }
    localStateManager.setLongField(paramUelEspecifica, jdoInheritedFieldCount + 3, paramUelEspecifica.idUelEspecifica, paramLong);
  }

  private static final UnidadEjecutora jdoGetunidadEjecutora(UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramUelEspecifica.unidadEjecutora;
    if (localStateManager.isLoaded(paramUelEspecifica, jdoInheritedFieldCount + 4))
      return paramUelEspecifica.unidadEjecutora;
    return (UnidadEjecutora)localStateManager.getObjectField(paramUelEspecifica, jdoInheritedFieldCount + 4, paramUelEspecifica.unidadEjecutora);
  }

  private static final void jdoSetunidadEjecutora(UelEspecifica paramUelEspecifica, UnidadEjecutora paramUnidadEjecutora)
  {
    StateManager localStateManager = paramUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramUelEspecifica.unidadEjecutora = paramUnidadEjecutora;
      return;
    }
    localStateManager.setObjectField(paramUelEspecifica, jdoInheritedFieldCount + 4, paramUelEspecifica.unidadEjecutora, paramUnidadEjecutora);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}