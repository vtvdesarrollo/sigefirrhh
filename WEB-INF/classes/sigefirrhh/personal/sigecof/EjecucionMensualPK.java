package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class EjecucionMensualPK
  implements Serializable
{
  public long idEjecucionMensual;

  public EjecucionMensualPK()
  {
  }

  public EjecucionMensualPK(long idEjecucionMensual)
  {
    this.idEjecucionMensual = idEjecucionMensual;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EjecucionMensualPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EjecucionMensualPK thatPK)
  {
    return 
      this.idEjecucionMensual == thatPK.idEjecucionMensual;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEjecucionMensual)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEjecucionMensual);
  }
}