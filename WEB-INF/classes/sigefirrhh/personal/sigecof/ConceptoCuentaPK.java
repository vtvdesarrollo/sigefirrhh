package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ConceptoCuentaPK
  implements Serializable
{
  public long idConceptoCuenta;

  public ConceptoCuentaPK()
  {
  }

  public ConceptoCuentaPK(long idConceptoCuenta)
  {
    this.idConceptoCuenta = idConceptoCuenta;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoCuentaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoCuentaPK thatPK)
  {
    return 
      this.idConceptoCuenta == thatPK.idConceptoCuenta;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoCuenta)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoCuenta);
  }
}