package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ConceptoContablePK
  implements Serializable
{
  public long idConceptoContable;

  public ConceptoContablePK()
  {
  }

  public ConceptoContablePK(long idConceptoContable)
  {
    this.idConceptoContable = idConceptoContable;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoContablePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoContablePK thatPK)
  {
    return 
      this.idConceptoContable == thatPK.idConceptoContable;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoContable)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoContable);
  }
}