package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class AccionCentralizadaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAccionCentralizada(AccionCentralizada accionCentralizada)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AccionCentralizada accionCentralizadaNew = 
      (AccionCentralizada)BeanUtils.cloneBean(
      accionCentralizada);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (accionCentralizadaNew.getOrganismo() != null) {
      accionCentralizadaNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        accionCentralizadaNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(accionCentralizadaNew);
  }

  public void updateAccionCentralizada(AccionCentralizada accionCentralizada) throws Exception
  {
    AccionCentralizada accionCentralizadaModify = 
      findAccionCentralizadaById(accionCentralizada.getIdAccionCentralizada());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (accionCentralizada.getOrganismo() != null) {
      accionCentralizada.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        accionCentralizada.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(accionCentralizadaModify, accionCentralizada);
  }

  public void deleteAccionCentralizada(AccionCentralizada accionCentralizada) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AccionCentralizada accionCentralizadaDelete = 
      findAccionCentralizadaById(accionCentralizada.getIdAccionCentralizada());
    pm.deletePersistent(accionCentralizadaDelete);
  }

  public AccionCentralizada findAccionCentralizadaById(long idAccionCentralizada) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAccionCentralizada == pIdAccionCentralizada";
    Query query = pm.newQuery(AccionCentralizada.class, filter);

    query.declareParameters("long pIdAccionCentralizada");

    parameters.put("pIdAccionCentralizada", new Long(idAccionCentralizada));

    Collection colAccionCentralizada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAccionCentralizada.iterator();
    return (AccionCentralizada)iterator.next();
  }

  public Collection findAccionCentralizadaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent accionCentralizadaExtent = pm.getExtent(
      AccionCentralizada.class, true);
    Query query = pm.newQuery(accionCentralizadaExtent);
    query.setOrdering("codAccionCentralizada ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(AccionCentralizada.class, filter);

    query.declareParameters("int pAnio, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codAccionCentralizada ascending");

    Collection colAccionCentralizada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionCentralizada);

    return colAccionCentralizada;
  }

  public Collection findByCodAccionCentralizada(String codAccionCentralizada, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codAccionCentralizada == pCodAccionCentralizada &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(AccionCentralizada.class, filter);

    query.declareParameters("java.lang.String pCodAccionCentralizada, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodAccionCentralizada", new String(codAccionCentralizada));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codAccionCentralizada ascending");

    Collection colAccionCentralizada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionCentralizada);

    return colAccionCentralizada;
  }

  public Collection findByDenominacion(String denominacion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "denominacion.startsWith(pDenominacion) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(AccionCentralizada.class, filter);

    query.declareParameters("java.lang.String pDenominacion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pDenominacion", new String(denominacion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codAccionCentralizada ascending");

    Collection colAccionCentralizada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionCentralizada);

    return colAccionCentralizada;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(AccionCentralizada.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codAccionCentralizada ascending");

    Collection colAccionCentralizada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionCentralizada);

    return colAccionCentralizada;
  }
}