package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.RelacionPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportCuadroOnapreForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportCuadroOnapreForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private Map parameters = null;
  private String selectProceso;
  private String selectRelacionPersonal;
  private int mes;
  private int anio;
  private long idRelacionPersonal;
  private Collection listRelacionPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private SigecofNoGenFacade sigecofNoGenacade;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private RelacionPersonal RelacionPersonal;

  public ReportCuadroOnapreForm()
  {
    this.reportName = "cuadroonapreactivos";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.sigecofNoGenacade = new SigecofNoGenFacade();
    this.selectRelacionPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    this.parameters = new Hashtable();

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportCuadroOnapreForm.this.cambiarNombreAReporte(ReportCuadroOnapreForm.this.parameters);
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRelacionPersonal = 
        this.definicionesFacade.findAllRelacionPersonal();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listRelacionPersonal = new ArrayList();
    }
  }

  public void changeRelacionPersonal(ValueChangeEvent event)
  {
    this.idRelacionPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.auxShow = false;
    try {
      if (this.idRelacionPersonal != 0L) {
        this.RelacionPersonal = this.definicionesFacade.findRelacionPersonalById(this.idRelacionPersonal);
        this.auxShow = true;
      }

    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  private void cambiarNombreAReporte(Map parameters)
  {
    if (this.tipoReporte == 10)
      this.reportName = "cuadroonapreactivos";
    else if (this.tipoReporte == 15)
      this.reportName = "cuadroonaprepasivos";
    else if (this.tipoReporte == 20)
      this.reportName = "cuadroonapreescalasalario";
    else if (this.tipoReporte == 25)
      this.reportName = "cuadroonapreescalasueldo";
    else if (this.tipoReporte == 30)
      this.reportName = "cuadroonapreescalasueldojub";
    else if (this.tipoReporte == 35)
      this.reportName = "cuadroonapreescalasueldopen";
  }

  public String runReport()
  {
    log.error("........................pasa 1");
    this.parameters = new Hashtable();
    log.error("........................pasa 2");
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      this.parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
      this.parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      this.parameters.put("panio", new Integer(this.anio));
      this.parameters.put("id_relacion_personal", new Long(this.idRelacionPersonal));
      log.error("........................pasa 3");

      cambiarNombreAReporte(this.parameters);

      JasperForWeb report = new JasperForWeb();
      log.error("........................pasa 4");

      report.setReportName(this.reportName);
      report.setParameters(this.parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/sigecof");

      log.error("Path de reporte " + this.reportName + report.getPath());
      log.error("........................pasa 5");
      report.start();
      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);
      log.error("........................pasa 6");
      this.reportId = JasperForWeb.newReportId(this.reportId);
      log.error("........................pasa 7");
    } catch (Exception e) {
      log.error("........................pasa 9");
      log.error("Excepcion controlada:", e);
    }

    log.error("........................pasa 8");
    return null;
  }

  public Collection getListRelacionPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRelacionPersonal, "sigefirrhh.base.definiciones.RelacionPersonal");
  }

  public String getSelectRelacionPersonal() {
    return this.selectRelacionPersonal;
  }

  public void setSelectRelacionPersonal(String string) {
    this.selectRelacionPersonal = string;
  }

  public boolean isShow() {
    return this.show;
  }
  public boolean isAuxShow() {
    return this.auxShow;
  }

  public String getSelectProceso()
  {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }
  public int getReportId() {
    return this.reportId;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }
}