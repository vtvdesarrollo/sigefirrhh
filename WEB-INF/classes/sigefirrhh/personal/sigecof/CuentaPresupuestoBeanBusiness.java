package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class CuentaPresupuestoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CuentaPresupuesto cuentaPresupuestoNew = 
      (CuentaPresupuesto)BeanUtils.cloneBean(
      cuentaPresupuesto);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (cuentaPresupuestoNew.getOrganismo() != null) {
      cuentaPresupuestoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        cuentaPresupuestoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(cuentaPresupuestoNew);
  }

  public void updateCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) throws Exception
  {
    CuentaPresupuesto cuentaPresupuestoModify = 
      findCuentaPresupuestoById(cuentaPresupuesto.getIdCuentaPresupuesto());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (cuentaPresupuesto.getOrganismo() != null) {
      cuentaPresupuesto.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        cuentaPresupuesto.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(cuentaPresupuestoModify, cuentaPresupuesto);
  }

  public void deleteCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CuentaPresupuesto cuentaPresupuestoDelete = 
      findCuentaPresupuestoById(cuentaPresupuesto.getIdCuentaPresupuesto());
    pm.deletePersistent(cuentaPresupuestoDelete);
  }

  public CuentaPresupuesto findCuentaPresupuestoById(long idCuentaPresupuesto) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCuentaPresupuesto == pIdCuentaPresupuesto";
    Query query = pm.newQuery(CuentaPresupuesto.class, filter);

    query.declareParameters("long pIdCuentaPresupuesto");

    parameters.put("pIdCuentaPresupuesto", new Long(idCuentaPresupuesto));

    Collection colCuentaPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCuentaPresupuesto.iterator();
    return (CuentaPresupuesto)iterator.next();
  }

  public Collection findCuentaPresupuestoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cuentaPresupuestoExtent = pm.getExtent(
      CuentaPresupuesto.class, true);
    Query query = pm.newQuery(cuentaPresupuestoExtent);
    query.setOrdering("codPresupuesto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodPresupuesto(String codPresupuesto, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codPresupuesto == pCodPresupuesto &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaPresupuesto.class, filter);

    query.declareParameters("java.lang.String pCodPresupuesto, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodPresupuesto", new String(codPresupuesto));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codPresupuesto ascending");

    Collection colCuentaPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaPresupuesto);

    return colCuentaPresupuesto;
  }

  public Collection findByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaPresupuesto.class, filter);

    query.declareParameters("java.lang.String pDescripcion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codPresupuesto ascending");

    Collection colCuentaPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaPresupuesto);

    return colCuentaPresupuesto;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaPresupuesto.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codPresupuesto ascending");

    Collection colCuentaPresupuesto = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaPresupuesto);

    return colCuentaPresupuesto;
  }
}