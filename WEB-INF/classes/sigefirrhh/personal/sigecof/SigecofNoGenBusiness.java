package sigefirrhh.personal.sigecof;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collection;
import org.apache.log4j.Logger;
import sigefirrhh.personal.procesoAniversario.ProcesoAniversarioNoGenBusiness;

public class SigecofNoGenBusiness extends SigecofBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(SigecofNoGenBusiness.class.getName());

  private UelEspecificaNoGenBeanBusiness uelEspecificaNoGenBeanBusiness = new UelEspecificaNoGenBeanBusiness();

  private TrabajadorEspecificaNoGenBeanBusiness trabajadorEspecificaNoGenBeanBusiness = new TrabajadorEspecificaNoGenBeanBusiness();

  private ConceptoEspecificaNoGenBeanBusiness conceptoEspecificaNoGenBeanBusiness = new ConceptoEspecificaNoGenBeanBusiness();

  private CargoEspecificaNoGenBeanBusiness cargoEspecificaNoGenBeanBusiness = new CargoEspecificaNoGenBeanBusiness();

  private SeguridadPresupuestoNoGenBeanBusiness seguridadPresupuestoNoGenBeanBusiness = new SeguridadPresupuestoNoGenBeanBusiness();

  private ConceptoResumenNoGenBeanBusiness conceptoResumenNoGenBeanBusiness = new ConceptoResumenNoGenBeanBusiness();

  private ConceptoCuentaNoGenBeanBusiness conceptoCuentaNoGenBeanBusiness = new ConceptoCuentaNoGenBeanBusiness();

  private ResumenMensualNoGenBeanBusiness resumenMensualNoGenBeanBusiness = new ResumenMensualNoGenBeanBusiness();

  public void generarBaseResumen(long idTipoPersonal, int anio, int mes, int numeroNomina, String periodicidad, String estatus, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, int frecuenciaEspecial) throws Exception {
    this.log.error("idTipoPersonal " + idTipoPersonal);
    this.log.error("numeroNomina " + numeroNomina);
    this.log.error("periodicidad " + periodicidad);
    this.log.error("estatus " + estatus);
    this.log.error("anio " + anio);
    this.log.error("mes " + mes);
    this.log.error("IdUnidadAdministradora " + idUnidadAdministradora);
    this.log.error("frecuenciaEspecial " + frecuenciaEspecial);

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    long id;
    try { connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select generar_conceptoresumen(?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, numeroNomina);
      st.setString(3, periodicidad);
      st.setString(4, estatus);
      st.setInt(5, anio);
      st.setInt(6, mes);
      st.setLong(7, idUnidadAdministradora);
      st.setInt(8, frecuenciaEspecial);

      rs = st.executeQuery();
      rs.next();
      long id = rs.getLong(1);

      connection.commit();

      this.log.error("anio " + anio);
      this.log.error("mes " + mes);

      int anio_ = anio;
      int mes_ = mes - 1;

      java.util.Date fecha = new java.util.Date();
      fecha.setYear(anio_);
      fecha.setMonth(mes_);
      fecha.setDate(1);

      this.log.error("fecha " + fecha);

      Calendar fechaInicio = Calendar.getInstance();
      fechaInicio.set(anio_, mes_, 1);

      Calendar fechaFin = Calendar.getInstance();
      fechaFin.set(anio_, mes_, 1);

      fechaFin.add(2, 1);
      fechaFin.add(5, -1);

      this.log.error("fecha inicio_ " + fechaInicio.getTime());
      this.log.error("fecha fin_ " + fechaFin.getTime());

      ProcesoAniversarioNoGenBusiness procesoAniversarioBusiness = new ProcesoAniversarioNoGenBusiness();
      id = procesoAniversarioBusiness.proyectarPrimaAntiguedad(idTipoPersonal, fechaInicio.getTime(), fechaFin.getTime(), idUnidadAdministradora, anio, mes, id);
      procesoAniversarioBusiness.proyectarBonoVacacional(idTipoPersonal, fechaInicio.getTime(), fechaFin.getTime(), idUnidadAdministradora, anio, mes, id);

      generarResumenMensual(idTipoPersonal, anio, mes, numeroNomina, tieneSemana5, idUnidadAdministradora, codUnidadAdministradora, usuario, titulo);
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
    long id;
  }

  public void generarResumenMensual(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo)
    throws Exception
  {
    this.log.error("idTipoPersonal " + idTipoPersonal);
    this.log.error("numeroNomina " + numeroNomina);
    this.log.error("anio " + anio);
    this.log.error("mes " + mes);
    this.log.error("idUnidadAdministradora " + idUnidadAdministradora);

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select crear_encabezadoresumenmensual(?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idUnidadAdministradora);
      st.setString(2, codUnidadAdministradora);
      st.setInt(3, anio);
      st.setInt(4, mes);
      st.setLong(5, numeroNomina);
      st.setString(6, usuario);
      st.setString(7, titulo);
      st.setString(8, "N");
      st.setLong(9, 0L);

      long idEncabezado = 0L;
      rs = st.executeQuery();
      rs.next();
      idEncabezado = rs.getLong(1);

      connection.commit();

      this.log.error("ejecutó SP crear_encabezado_resumen_mensual");

      rs = st.executeQuery();

      this.log.error("idEncabezado " + idEncabezado);
      this.log.error("idTipoPersonal " + idTipoPersonal);
      this.log.error("numeroNomina " + numeroNomina);
      this.log.error("anio " + anio);
      this.log.error("mes " + mes);
      this.log.error("tieneSemana5 " + tieneSemana5);
      this.log.error("idUnidadAdministradora " + idUnidadAdministradora);

      rs = null;
      st = null;

      sql = new StringBuffer();

      sql.append("select generar_resumenmensual(?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idEncabezado);
      st.setLong(2, idTipoPersonal);
      st.setLong(3, numeroNomina);
      st.setInt(4, anio);
      st.setInt(5, mes);
      st.setBoolean(6, tieneSemana5);
      st.setLong(7, idUnidadAdministradora);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP generar_resumen_mensual");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void generarResumenAdicional(long idUnidadAdministradora, long idConceptoTipoPersonal, double monto, int anio, int mes, long idUelEspecifica, long idUnidadEjecutora, int quincena, String codUnidadAdministradora, int numeroNomina, String usuario, String titulo, String aportes, long idAporte, int numero)
    throws Exception
  {
    this.log.error("idUnidadAdministradora " + idUnidadAdministradora);
    this.log.error("idConceptoTipoPersonal " + idConceptoTipoPersonal);
    this.log.error("monto " + monto);
    this.log.error("anio " + anio);
    this.log.error("mes " + mes);
    this.log.error("idUelEspecifica " + idUelEspecifica);
    this.log.error("idUnidadEjecutora " + idUnidadEjecutora);
    this.log.error("quincena " + quincena);
    this.log.error("codUnidadAdministradora " + codUnidadAdministradora);
    this.log.error("numeronomina " + numeroNomina);
    this.log.error("usuario " + usuario);
    this.log.error("titulo " + titulo);
    this.log.error("aportes " + aportes);
    this.log.error("idAporte " + idAporte);
    this.log.error("numero " + numero);

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select generar_resumenmensual_adicional(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idUnidadAdministradora);
      st.setLong(2, idConceptoTipoPersonal);
      st.setDouble(3, monto);
      st.setInt(4, anio);
      st.setInt(5, mes);
      st.setLong(6, idUelEspecifica);
      st.setLong(7, idUnidadEjecutora);
      st.setInt(8, quincena);
      st.setString(9, codUnidadAdministradora);
      st.setInt(10, numeroNomina);
      st.setString(11, usuario);
      st.setString(12, titulo);
      st.setString(13, aportes);
      st.setLong(14, idAporte);
      st.setInt(15, numero);

      rs = st.executeQuery();
      rs.next();

      connection.commit();

      this.log.error("ejecutó SP generar_resumen_adicional");
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void generarResumenAportes(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, long idConceptoAporte)
    throws Exception
  {
    this.log.error("idTipoPersonal " + idTipoPersonal);
    this.log.error("numeroNomina " + numeroNomina);
    this.log.error("anio " + anio);
    this.log.error("mes " + mes);
    this.log.error("idUnidadAdministradora " + idUnidadAdministradora);

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select crear_encabezadoresumenmensual(?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idUnidadAdministradora);
      st.setString(2, codUnidadAdministradora);
      st.setInt(3, anio);
      st.setInt(4, mes);
      st.setLong(5, numeroNomina);
      st.setString(6, usuario);
      st.setString(7, titulo);
      st.setString(8, "S");
      st.setLong(9, idConceptoAporte);

      long idEncabezado = 0L;
      rs = st.executeQuery();
      rs.next();
      idEncabezado = rs.getLong(1);

      connection.commit();

      this.log.error("ejecutó SP crear_encabezado_resumen_mensual");

      rs = st.executeQuery();

      this.log.error("idEncabezado " + idEncabezado);
      this.log.error("idTipoPersonal " + idTipoPersonal);
      this.log.error("numeroNomina " + numeroNomina);
      this.log.error("anio " + anio);
      this.log.error("mes " + mes);
      this.log.error("tieneSemana5 " + tieneSemana5);
      this.log.error("idUnidadAdministradora " + idUnidadAdministradora);
      this.log.error("idConceptoAporte " + idConceptoAporte);

      rs = null;
      st = null;

      sql = new StringBuffer();

      sql.append("select generar_resumenmensual_aportes(?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idEncabezado);
      st.setLong(2, idTipoPersonal);
      st.setLong(3, numeroNomina);
      st.setInt(4, anio);
      st.setInt(5, mes);
      st.setBoolean(6, tieneSemana5);
      st.setLong(7, idUnidadAdministradora);
      st.setLong(8, idConceptoAporte);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP generar_resumen_mensual_aportes");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void generarRendicionAportes(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, long idConceptoAporte, int periodo)
    throws Exception
  {
    this.log.error("idTipoPersonal " + idTipoPersonal);
    this.log.error("numeroNomina " + numeroNomina);
    this.log.error("anio " + anio);
    this.log.error("mes " + mes);
    this.log.error("idUnidadAdministradora " + idUnidadAdministradora);

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select crear_encabezadorendicionmensual(?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idUnidadAdministradora);
      st.setString(2, codUnidadAdministradora);
      st.setInt(3, anio);
      st.setInt(4, mes);
      st.setLong(5, numeroNomina);
      st.setString(6, usuario);
      st.setString(7, titulo);
      st.setString(8, "S");
      st.setLong(9, idConceptoAporte);

      long idEncabezado = 0L;
      rs = st.executeQuery();
      rs.next();
      idEncabezado = rs.getLong(1);

      connection.commit();

      this.log.error("ejecutó SP crear_encabezado_rendicion_mensual");

      rs = st.executeQuery();

      this.log.error("idEncabezado " + idEncabezado);
      this.log.error("idTipoPersonal " + idTipoPersonal);
      this.log.error("numeroNomina " + numeroNomina);
      this.log.error("anio " + anio);
      this.log.error("mes " + mes);
      this.log.error("tieneSemana5 " + tieneSemana5);
      this.log.error("idUnidadAdministradora " + idUnidadAdministradora);
      this.log.error("idConceptoAporte " + idConceptoAporte);

      rs = null;
      st = null;

      sql = new StringBuffer();

      sql.append("select generar_rendicionmensual_aportes(?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idEncabezado);
      st.setLong(2, idTipoPersonal);
      st.setLong(3, numeroNomina);
      st.setInt(4, anio);
      st.setInt(5, mes);
      st.setBoolean(6, tieneSemana5);
      st.setLong(7, idUnidadAdministradora);
      st.setLong(8, idConceptoAporte);
      st.setLong(9, periodo);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP generar_rendicion_mensual_aportes");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void cerrarResumenMensual(long idCategoriaPresupuesto, int numeroNomina, int anio, int mes, String usuario)
    throws Exception
  {
    java.util.Date fechaCierre = new java.util.Date();
    java.sql.Date fechaCierreSql = new java.sql.Date(fechaCierre.getYear(), fechaCierre.getMonth(), fechaCierre.getDate());

    this.log.error("idCategoriaPresupuesto " + idCategoriaPresupuesto);
    this.log.error("numeroNomina " + numeroNomina);
    this.log.error("anio " + anio);
    this.log.error("mes " + mes);
    this.log.error("usuario " + usuario);
    this.log.error("fechacierre " + fechaCierre);

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select cerrar_resumenmensual(?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idCategoriaPresupuesto);
      st.setLong(2, numeroNomina);
      st.setInt(3, anio);
      st.setInt(4, mes);
      st.setString(5, usuario);
      st.setDate(6, fechaCierreSql);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP cerrar_resumen_mensual");
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void generarRendicionMensual(long idTipoPersonal, int anio, int mes, int numeroNomina, boolean tieneSemana5, long idUnidadAdministradora, String codUnidadAdministradora, String usuario, String titulo, int quincena)
    throws Exception
  {
    this.log.error("idTipoPersonal " + idTipoPersonal);
    this.log.error("numeroNomina " + numeroNomina);
    this.log.error("anio " + anio);
    this.log.error("mes " + mes);
    this.log.error("idUnidadAdministradora " + idUnidadAdministradora);

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      sql.append("select crear_encabezadorendicionmensual(?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idUnidadAdministradora);
      st.setString(2, codUnidadAdministradora);
      st.setInt(3, anio);
      st.setInt(4, mes);
      st.setLong(5, numeroNomina);
      st.setString(6, usuario);
      st.setString(7, titulo);
      st.setBoolean(8, false);
      st.setLong(9, 0L);

      long idEncabezado = 0L;
      rs = st.executeQuery();
      rs.next();
      idEncabezado = rs.getLong(1);

      this.log.error("ejecutó SP crear_encabezado_rendicion_mensual");

      this.log.error("-- idEncabezado " + idEncabezado);
      this.log.error("-- idTipoPersonal " + idTipoPersonal);
      this.log.error("-- numeroNomina " + numeroNomina);
      this.log.error("-- anio " + anio);
      this.log.error("-- mes " + mes);
      this.log.error("-- tieneSemana5 " + tieneSemana5);
      this.log.error("-- idUnidadAdministradora " + idUnidadAdministradora);
      this.log.error("-- quincena " + quincena);

      rs = null;
      st = null;

      sql = new StringBuffer();

      sql.append("select generar_rendicionmensual(?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idEncabezado);
      st.setLong(2, idTipoPersonal);
      st.setLong(3, numeroNomina);
      st.setInt(4, anio);
      st.setInt(5, mes);
      st.setBoolean(6, tieneSemana5);
      st.setLong(7, idUnidadAdministradora);
      st.setInt(8, quincena);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP generar_rendicion_mensual");
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void generarTrabajadorCargoEspecifica(long idUnidadEjecutora, long idUelEspecifica, long idTipoPersonal, int anio, double porcentaje, long idOrganismo)
    throws Exception
  {
    this.log.error("1-idUnidadEjecutora " + idUnidadEjecutora);
    this.log.error("2-idUelEspecifica " + idUelEspecifica);
    this.log.error("3-idTipoPersonal " + idTipoPersonal);
    this.log.error("4-anio " + anio);
    this.log.error("5-porcentaje " + porcentaje);

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select generar_trabajadorcargoespecifica(?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idUnidadEjecutora);
      st.setLong(2, idUelEspecifica);
      st.setLong(3, idTipoPersonal);
      st.setInt(4, anio);
      st.setDouble(5, porcentaje);
      st.setLong(6, idOrganismo);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP trabajadorcargoespecifica");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        } 
    }
  }

  public void generarPartidaUelEspecifica(long idFuenteFinanciamiento, int anio) throws Exception {
    this.log.error("1-idFuenteFinanciamiento " + idFuenteFinanciamiento);
    this.log.error("2-anio " + anio);

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select generar_partidauelespecifica(?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idFuenteFinanciamiento);
      st.setInt(2, anio);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("ejecutó SP trabajadorcargoespecifica");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        } 
    }
  }

  public void generarCuadroOnapre(long idRelacionPersonal, int anio)
    throws Exception
  {
    this.log.error("1-idRelacionPersonal " + idRelacionPersonal);
    this.log.error("2-anio " + anio);

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select generar_cuadro_onapre(?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setInt(1, anio);
      st.setLong(2, idRelacionPersonal);

      rs = st.executeQuery();

      connection.commit();

      this.log.error("Ejecutó SP generar_cuadro_onapre");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        } 
    }
  }

  public Collection findUelEspecificaByUnidadEjecutoraAndAnio(long idUnidadEjecutora, int anio) throws Exception { return this.uelEspecificaNoGenBeanBusiness.findByUnidadEjecutoraAndAnio(idUnidadEjecutora, anio); }

  public Collection findUelEspecificaByCategoriaPresupuestoAndAnio(String codCategoriaPresupuesto, int anio) throws Exception
  {
    return this.uelEspecificaNoGenBeanBusiness.findByCategoriaPresupuestoAndAnio(codCategoriaPresupuesto, anio);
  }

  public Collection findUelEspecificaByUnidadEjecutoraAndProyecto(long idUnidadEjecutora, long idProyecto) throws Exception {
    return this.uelEspecificaNoGenBeanBusiness.findByUnidadEjecutoraAndProyecto(idUnidadEjecutora, idProyecto);
  }

  public Collection findUelEspecificaByUnidadEjecutoraAndAccionCentralizada(long idUnidadEjecutora, long idAccionCentralizada) throws Exception {
    return this.uelEspecificaNoGenBeanBusiness.findByUnidadEjecutoraAndAccionCentralizada(idUnidadEjecutora, idAccionCentralizada);
  }

  public Collection findTrabajadorEspecificaByTrabajadorAndAnio(long idTrabajador, int anio) throws Exception {
    return this.trabajadorEspecificaNoGenBeanBusiness.findByTrabajadorAndAnio(idTrabajador, anio);
  }

  public Collection findUelEspecificaByUnidadEjecutoraAnioAndTipo(long idUnidadEjecutora, int anio, String tipo) throws Exception {
    return this.uelEspecificaNoGenBeanBusiness.findByUnidadEjecutoraAnioAndTipo(idUnidadEjecutora, anio, tipo);
  }

  public Collection findConceptoEspecificaByConceptoTipoPersonalAndAnio(long idConceptoTipoPersonal, int anio) throws Exception {
    return this.conceptoEspecificaNoGenBeanBusiness.findByConceptoTipoPersonalAndAnio(idConceptoTipoPersonal, anio);
  }

  public CargoEspecifica findCargoEspecificaByRegistroCargosAndUelEspecifica(long idRegistroCargos, long idUelEspecifica) throws Exception {
    return this.cargoEspecificaNoGenBeanBusiness.findByRegistroCargosAndUelEspecifica(idRegistroCargos, idUelEspecifica);
  }

  public SeguridadPresupuesto findSeguridadPresupuestoByUltimoResumen(long idCategoriaPresupuesto) throws Exception {
    return this.seguridadPresupuestoNoGenBeanBusiness.findUltimoResumen(idCategoriaPresupuesto);
  }

  public Collection findConceptoResumenByTrabajadorAnioMes(long idTrabajador, int anio, int mes) throws Exception {
    return this.conceptoResumenNoGenBeanBusiness.findByTrabajadorAnioMes(idTrabajador, anio, mes);
  }

  public Collection findConceptoCuentaByTipoPersonal(long idTipoPersonal) throws Exception {
    return this.conceptoCuentaNoGenBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findResumenMensualByUnidadAdministradora(long idUnidadAdministradora, long idCategoriaPresupuesto, long idUelEspecifica, int mes, String cerrado) throws Exception {
    return this.resumenMensualNoGenBeanBusiness.findByUnidadAdministradora(idUnidadAdministradora, idCategoriaPresupuesto, idUelEspecifica, mes, cerrado);
  }
}