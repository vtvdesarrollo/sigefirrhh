package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CuentaPresupuestoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CuentaPresupuestoForm.class.getName());
  private CuentaPresupuesto cuentaPresupuesto;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showCuentaPresupuestoByCodPresupuesto;
  private boolean showCuentaPresupuestoByDescripcion;
  private String findCodPresupuesto;
  private String findDescripcion;
  private Object stateResultCuentaPresupuestoByCodPresupuesto = null;

  private Object stateResultCuentaPresupuestoByDescripcion = null;

  public String getFindCodPresupuesto()
  {
    return this.findCodPresupuesto;
  }
  public void setFindCodPresupuesto(String findCodPresupuesto) {
    this.findCodPresupuesto = findCodPresupuesto;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public CuentaPresupuesto getCuentaPresupuesto() {
    if (this.cuentaPresupuesto == null) {
      this.cuentaPresupuesto = new CuentaPresupuesto();
    }
    return this.cuentaPresupuesto;
  }

  public CuentaPresupuestoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListTipo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = CuentaPresupuesto.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findCuentaPresupuestoByCodPresupuesto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.sigecofFacade.findCuentaPresupuestoByCodPresupuesto(this.findCodPresupuesto, idOrganismo);
      this.showCuentaPresupuestoByCodPresupuesto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCuentaPresupuestoByCodPresupuesto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPresupuesto = null;
    this.findDescripcion = null;

    return null;
  }

  public String findCuentaPresupuestoByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.sigecofFacade.findCuentaPresupuestoByDescripcion(this.findDescripcion, idOrganismo);
      this.showCuentaPresupuestoByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCuentaPresupuestoByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPresupuesto = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowCuentaPresupuestoByCodPresupuesto() {
    return this.showCuentaPresupuestoByCodPresupuesto;
  }
  public boolean isShowCuentaPresupuestoByDescripcion() {
    return this.showCuentaPresupuestoByDescripcion;
  }

  public String selectCuentaPresupuesto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idCuentaPresupuesto = 
      Long.parseLong((String)requestParameterMap.get("idCuentaPresupuesto"));
    try
    {
      this.cuentaPresupuesto = 
        this.sigecofFacade.findCuentaPresupuestoById(
        idCuentaPresupuesto);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.cuentaPresupuesto = null;
    this.showCuentaPresupuestoByCodPresupuesto = false;
    this.showCuentaPresupuestoByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addCuentaPresupuesto(
          this.cuentaPresupuesto);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateCuentaPresupuesto(
          this.cuentaPresupuesto);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteCuentaPresupuesto(
        this.cuentaPresupuesto);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.cuentaPresupuesto = new CuentaPresupuesto();

    this.cuentaPresupuesto.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.cuentaPresupuesto.setIdCuentaPresupuesto(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.CuentaPresupuesto"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.cuentaPresupuesto = new CuentaPresupuesto();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}