package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.NominaEspecial;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarResumenMensualForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarResumenMensualForm.class.getName());
  private String selectProceso;
  private String selectTipoPersonal;
  private String selectCategoriaPresupuesto;
  private String selectUnidadAdministradora;
  private int mes;
  private int anio;
  private long idTipoPersonal;
  private long idCategoriaPresupuesto;
  private long idUnidadAdministradora;
  private Collection listTipoPersonal;
  private Collection listCategoriaPresupuesto;
  private Collection listUnidadAdministradora;
  private Collection listNominaEspecial;
  private String tipoNomina = "O";

  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private LoginSession login;
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private EstructuraFacade estructurafFacade = new EstructuraFacade();
  private boolean show;
  private boolean auxShow;
  private boolean showNominaEspecial;
  private long idNominaEspecial;
  private NominaEspecial nominaEspecial;
  private String selectNominaEspecial;

  public boolean isShowTipoPersonal()
  {
    return (this.listTipoPersonal != null) && (!this.listTipoPersonal.isEmpty());
  }
  private void llenarNominaEspecial(long idGrupoNomina) {
    try {
      this.listNominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialByEstatus("P");
      this.showNominaEspecial = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeNominaEspecial(ValueChangeEvent event) {
    this.idNominaEspecial = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.nominaEspecial = this.procesoNominaNoGenFacade.findNominaEspecialById(this.idNominaEspecial);
      this.auxShow = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCategoriaPresupuesto(ValueChangeEvent event)
  {
    this.idCategoriaPresupuesto = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.listTipoPersonal = null;
      this.auxShow = false;
      if (this.idCategoriaPresupuesto != 0L) {
        this.listTipoPersonal = this.definicionesFacade.findTipoPersonalByCategoriaPresupuesto(this.idCategoriaPresupuesto);
        this.auxShow = true;
      }
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
  }

  public void changeUnidadAdministradora(ValueChangeEvent event)
  {
    this.idUnidadAdministradora = Long.valueOf(
      (String)event.getNewValue()).longValue();
  }

  public void changeTipoNomina(ValueChangeEvent event)
  {
    String tipoNomina = (String)event.getNewValue();
    try {
      this.auxShow = false;
      this.showNominaEspecial = false;
      if (tipoNomina.equals("O"))
        this.auxShow = true;
      else {
        llenarNominaEspecial(0L);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public GenerarResumenMensualForm()
  {
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try {
      this.listCategoriaPresupuesto = this.definicionesFacade.findAllCategoriaPresupuesto();
      this.listUnidadAdministradora = this.estructurafFacade.findAllUnidadAdministradora();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listCategoriaPresupuesto = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    String titulo = null;
    if (this.tipoNomina.equals("O")) {
      titulo = "NOMINA ORDINARIA";
    }

    try
    {
      int frecuenciaEspecial = 0;
      int numeroNomina = 0;
      if (this.tipoNomina.equals("E")) {
        numeroNomina = this.nominaEspecial.getNumeroNomina();
        titulo = this.nominaEspecial.toString();
      }

      if (this.idUnidadAdministradora == 0L) {
        Iterator iterUA = this.listUnidadAdministradora.iterator();
        while (iterUA.hasNext()) {
          UnidadAdministradora unidadAdministradora = (UnidadAdministradora)iterUA.next();
          if (this.idTipoPersonal == 0L) {
            Iterator iter = this.listTipoPersonal.iterator();
            while (iter.hasNext()) {
              TipoPersonal tipoPersonal = (TipoPersonal)iter.next();
              this.sigecofFacade.generarResumenMensual(tipoPersonal.getIdTipoPersonal(), this.anio, this.mes, numeroNomina, false, unidadAdministradora.getIdUnidadAdministradora(), unidadAdministradora.getCodUnidadAdminist(), this.login.getUsuario(), titulo);
            }
          } else {
            this.sigecofFacade.generarResumenMensual(this.idTipoPersonal, this.anio, this.mes, numeroNomina, false, unidadAdministradora.getIdUnidadAdministradora(), unidadAdministradora.getCodUnidadAdminist(), this.login.getUsuario(), titulo);
          }
        }
      }
      else
      {
        UnidadAdministradora unidadAdministradora = this.estructurafFacade.findUnidadAdministradoraById(this.idUnidadAdministradora);

        if (this.idTipoPersonal == 0L) {
          Iterator iter = this.listTipoPersonal.iterator();
          while (iter.hasNext()) {
            TipoPersonal tipoPersonal = (TipoPersonal)iter.next();
            this.sigecofFacade.generarResumenMensual(tipoPersonal.getIdTipoPersonal(), this.anio, this.mes, numeroNomina, false, this.idUnidadAdministradora, unidadAdministradora.getCodUnidadAdminist(), this.login.getUsuario(), titulo);
          }
        } else {
          this.sigecofFacade.generarResumenMensual(this.idTipoPersonal, this.anio, this.mes, numeroNomina, false, this.idUnidadAdministradora, unidadAdministradora.getCodUnidadAdminist(), this.login.getUsuario(), titulo);
        }

      }

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListNominaEspecial() {
    if ((this.listNominaEspecial != null) && (!this.listNominaEspecial.isEmpty())) {
      return ListUtil.convertCollectionToSelectItemsWithId(
        this.listNominaEspecial, "sigefirrhh.personal.procesoNomina.NominaEspecial");
    }
    return null;
  }

  public Collection getListTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListUnidadAdministradora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }
  public Collection getListCategoriaPresupuesto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCategoriaPresupuesto, "sigefirrhh.base.definiciones.CategoriaPresupuesto");
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }

  public String getTipoNomina()
  {
    return this.tipoNomina;
  }

  public void setTipoNomina(String string)
  {
    this.tipoNomina = string;
  }

  public String getSelectCategoriaPresupuesto() {
    return this.selectCategoriaPresupuesto;
  }
  public void setSelectCategoriaPresupuesto(String selectCategoriaPresupuesto) {
    this.selectCategoriaPresupuesto = selectCategoriaPresupuesto;
  }

  public String getSelectUnidadAdministradora() {
    return this.selectUnidadAdministradora;
  }
  public void setSelectUnidadAdministradora(String selectUnidadAdministradora) {
    this.selectUnidadAdministradora = selectUnidadAdministradora;
  }
  public String getSelectNominaEspecial() {
    return this.selectNominaEspecial;
  }
  public void setSelectNominaEspecial(String selectNominaEspecial) {
    this.selectNominaEspecial = selectNominaEspecial;
  }
  public boolean isShowNominaEspecial() {
    return this.showNominaEspecial;
  }
}