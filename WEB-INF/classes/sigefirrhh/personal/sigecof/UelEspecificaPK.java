package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class UelEspecificaPK
  implements Serializable
{
  public long idUelEspecifica;

  public UelEspecificaPK()
  {
  }

  public UelEspecificaPK(long idUelEspecifica)
  {
    this.idUelEspecifica = idUelEspecifica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UelEspecificaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UelEspecificaPK thatPK)
  {
    return 
      this.idUelEspecifica == thatPK.idUelEspecifica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUelEspecifica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUelEspecifica);
  }
}