package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;

public class ConceptoContableBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoContable(ConceptoContable conceptoContable)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoContable conceptoContableNew = 
      (ConceptoContable)BeanUtils.cloneBean(
      conceptoContable);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoContableNew.getConceptoTipoPersonal() != null) {
      conceptoContableNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoContableNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CuentaContableBeanBusiness cuentaContableBeanBusiness = new CuentaContableBeanBusiness();

    if (conceptoContableNew.getCuentaContable() != null) {
      conceptoContableNew.setCuentaContable(
        cuentaContableBeanBusiness.findCuentaContableById(
        conceptoContableNew.getCuentaContable().getIdCuentaContable()));
    }
    pm.makePersistent(conceptoContableNew);
  }

  public void updateConceptoContable(ConceptoContable conceptoContable) throws Exception
  {
    ConceptoContable conceptoContableModify = 
      findConceptoContableById(conceptoContable.getIdConceptoContable());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoContable.getConceptoTipoPersonal() != null) {
      conceptoContable.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoContable.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CuentaContableBeanBusiness cuentaContableBeanBusiness = new CuentaContableBeanBusiness();

    if (conceptoContable.getCuentaContable() != null) {
      conceptoContable.setCuentaContable(
        cuentaContableBeanBusiness.findCuentaContableById(
        conceptoContable.getCuentaContable().getIdCuentaContable()));
    }

    BeanUtils.copyProperties(conceptoContableModify, conceptoContable);
  }

  public void deleteConceptoContable(ConceptoContable conceptoContable) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoContable conceptoContableDelete = 
      findConceptoContableById(conceptoContable.getIdConceptoContable());
    pm.deletePersistent(conceptoContableDelete);
  }

  public ConceptoContable findConceptoContableById(long idConceptoContable) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoContable == pIdConceptoContable";
    Query query = pm.newQuery(ConceptoContable.class, filter);

    query.declareParameters("long pIdConceptoContable");

    parameters.put("pIdConceptoContable", new Long(idConceptoContable));

    Collection colConceptoContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoContable.iterator();
    return (ConceptoContable)iterator.next();
  }

  public Collection findConceptoContableAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoContableExtent = pm.getExtent(
      ConceptoContable.class, true);
    Query query = pm.newQuery(conceptoContableExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoContable.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoContable);

    return colConceptoContable;
  }

  public Collection findByCuentaContable(long idCuentaContable)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cuentaContable.idCuentaContable == pIdCuentaContable";

    Query query = pm.newQuery(ConceptoContable.class, filter);

    query.declareParameters("long pIdCuentaContable");
    HashMap parameters = new HashMap();

    parameters.put("pIdCuentaContable", new Long(idCuentaContable));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoContable);

    return colConceptoContable;
  }
}