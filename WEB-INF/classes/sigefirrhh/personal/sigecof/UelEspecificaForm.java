package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class UelEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UelEspecificaForm.class.getName());
  private UelEspecifica uelEspecifica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showUelEspecificaByAccionEspecifica;
  private boolean showUelEspecificaByUnidadEjecutora;
  private boolean showUelEspecificaByCategoriaPresupuesto;
  private boolean showUelEspecificaByAnio;
  private String findSelectAccionEspecifica;
  private String findSelectUnidadEjecutora;
  private String findCategoriaPresupuesto;
  private int findAnio;
  private Collection findColAccionEspecifica;
  private Collection findColUnidadEjecutora;
  private Collection colAccionEspecifica;
  private Collection colUnidadEjecutora;
  private String selectAccionEspecifica;
  private String selectUnidadEjecutora;
  private boolean showAccionEspecifica;
  private String tipo = "P";
  private Collection colProyecto;
  private Collection colAccionCentralizada;
  private String selectProyecto;
  private String selectAccionCentralizada;
  private Object stateResultUelEspecificaByAccionEspecifica = null;

  private Object stateResultUelEspecificaByUnidadEjecutora = null;

  private Object stateResultUelEspecificaByCategoriaPresupuesto = null;

  public boolean isShowAccionEspecifica()
  {
    return (this.colAccionEspecifica != null) && (!this.colAccionEspecifica.isEmpty());
  }
  public boolean isShowProyecto() {
    return (this.tipo.equals("P")) && (this.colProyecto != null);
  }
  public boolean isShowAccionCentralizada() {
    return (this.tipo.equals("A")) && (this.colAccionCentralizada != null);
  }
  public String getFindSelectAccionEspecifica() {
    return this.findSelectAccionEspecifica;
  }
  public void setFindSelectAccionEspecifica(String valAccionEspecifica) {
    this.findSelectAccionEspecifica = valAccionEspecifica;
  }

  public Collection getFindColAccionEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAccionEspecifica.iterator();
    AccionEspecifica accionEspecifica = null;
    while (iterator.hasNext()) {
      accionEspecifica = (AccionEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(accionEspecifica.getIdAccionEspecifica()), 
        accionEspecifica.toString()));
    }
    return col;
  }
  public String getFindSelectUnidadEjecutora() {
    return this.findSelectUnidadEjecutora;
  }
  public void setFindSelectUnidadEjecutora(String valUnidadEjecutora) {
    this.findSelectUnidadEjecutora = valUnidadEjecutora;
  }

  public Collection getFindColUnidadEjecutora() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadEjecutora.getIdUnidadEjecutora()), 
        unidadEjecutora.toString()));
    }
    return col;
  }
  public String getFindCategoriaPresupuesto() {
    return this.findCategoriaPresupuesto;
  }
  public void setFindCategoriaPresupuesto(String findCategoriaPresupuesto) {
    this.findCategoriaPresupuesto = findCategoriaPresupuesto;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public void changeProyecto(ValueChangeEvent event)
  {
    long idProyecto = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colAccionEspecifica = null;
      this.selectProyecto = String.valueOf(idProyecto);
      this.selectAccionEspecifica = null;
      if (idProyecto != 0L) {
        this.colAccionEspecifica = this.sigecofFacade.findAccionEspecificaByProyecto(idProyecto);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeAccionCentralizada(ValueChangeEvent event) {
    long idAccionCentralizada = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colAccionEspecifica = null;
      this.selectAccionCentralizada = String.valueOf(idAccionCentralizada);
      this.selectAccionEspecifica = null;

      if (idAccionCentralizada != 0L)
      {
        this.colAccionEspecifica = this.sigecofFacade.findAccionEspecificaByAccionCentralizada(idAccionCentralizada);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipo(ValueChangeEvent event) {
    String tipo = 
      (String)event.getNewValue();
    try
    {
      this.tipo = tipo;

      this.selectAccionCentralizada = null;
      this.selectProyecto = null;
      this.selectAccionEspecifica = null;

      findProyectoAccionByAnio();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String getSelectAccionEspecifica() { return this.selectAccionEspecifica; }

  public void setSelectAccionEspecifica(String valAccionEspecifica) {
    Iterator iterator = this.colAccionEspecifica.iterator();
    AccionEspecifica accionEspecifica = null;
    this.uelEspecifica.setAccionEspecifica(null);
    while (iterator.hasNext()) {
      accionEspecifica = (AccionEspecifica)iterator.next();
      if (String.valueOf(accionEspecifica.getIdAccionEspecifica()).equals(
        valAccionEspecifica)) {
        this.uelEspecifica.setAccionEspecifica(
          accionEspecifica);
        break;
      }
    }
    this.selectAccionEspecifica = valAccionEspecifica;
  }
  public String getSelectUnidadEjecutora() {
    return this.selectUnidadEjecutora;
  }
  public void setSelectUnidadEjecutora(String valUnidadEjecutora) {
    Iterator iterator = this.colUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    this.uelEspecifica.setUnidadEjecutora(null);
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      if (String.valueOf(unidadEjecutora.getIdUnidadEjecutora()).equals(
        valUnidadEjecutora)) {
        this.uelEspecifica.setUnidadEjecutora(
          unidadEjecutora);
        break;
      }
    }
    this.selectUnidadEjecutora = valUnidadEjecutora;
  }
  public Collection getResult() {
    return this.result;
  }

  public UelEspecifica getUelEspecifica() {
    if (this.uelEspecifica == null) {
      this.uelEspecifica = new UelEspecifica();
    }
    return this.uelEspecifica;
  }

  public UelEspecificaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    this.findAnio = (new Date().getYear() + 1900);

    refresh();
  }

  public Collection getColAccionEspecifica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAccionEspecifica.iterator();
    AccionEspecifica accionEspecifica = null;
    while (iterator.hasNext()) {
      accionEspecifica = (AccionEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(accionEspecifica.getIdAccionEspecifica()), 
        accionEspecifica.toString()));
    }
    return col;
  }

  public Collection getColUnidadEjecutora()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadEjecutora.iterator();
    UnidadEjecutora unidadEjecutora = null;
    while (iterator.hasNext()) {
      unidadEjecutora = (UnidadEjecutora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadEjecutora.getIdUnidadEjecutora()), 
        unidadEjecutora.toString()));
    }
    return col;
  }

  public Collection getColProyecto() {
    Collection col = new ArrayList();
    Iterator iterator = this.colProyecto.iterator();
    Proyecto proyecto = null;
    while (iterator.hasNext()) {
      proyecto = (Proyecto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(proyecto.getIdProyecto()), 
        proyecto.toString()));
    }
    return col;
  }
  public Collection getColAccionCentralizada() {
    Collection col = new ArrayList();
    Iterator iterator = this.colAccionCentralizada.iterator();
    AccionCentralizada accionCentralizada = null;
    while (iterator.hasNext()) {
      accionCentralizada = (AccionCentralizada)iterator.next();
      col.add(new SelectItem(
        String.valueOf(accionCentralizada.getIdAccionCentralizada()), 
        accionCentralizada.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColUnidadEjecutora = 
        this.estructuraFacade.findAllUnidadEjecutora();

      this.colAccionEspecifica = 
        new ArrayList();
      this.colUnidadEjecutora = 
        this.estructuraFacade.findAllUnidadEjecutora();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findUelEspecificaByAccionEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findUelEspecificaByAccionEspecifica(Long.valueOf(this.findSelectAccionEspecifica).longValue());
      this.showUelEspecificaByAccionEspecifica = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUelEspecificaByAccionEspecifica)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectAccionEspecifica = null;
    this.findSelectUnidadEjecutora = null;
    this.findCategoriaPresupuesto = null;
    this.findAnio = 0;

    return null;
  }

  public String findUelEspecificaByUnidadEjecutora()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAndAnio(Long.valueOf(this.findSelectUnidadEjecutora).longValue(), this.findAnio);
      this.showUelEspecificaByUnidadEjecutora = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUelEspecificaByUnidadEjecutora)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectAccionEspecifica = null;
    this.findSelectUnidadEjecutora = null;
    this.findCategoriaPresupuesto = null;

    return null;
  }

  public String findUelEspecificaByCategoriaPresupuesto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();

      this.result = 
        this.sigecofFacade.findUelEspecificaByCategoriaPresupuestoAndAnio(this.findCategoriaPresupuesto, this.findAnio);

      this.showUelEspecificaByCategoriaPresupuesto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUelEspecificaByCategoriaPresupuesto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectAccionEspecifica = null;
    this.findSelectUnidadEjecutora = null;
    this.findCategoriaPresupuesto = null;

    return null;
  }
  public String findProyectoAccionByAnio() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.colProyecto = null;
      this.colAccionCentralizada = null;
      if (this.tipo.equals("P"))
        this.colProyecto = this.sigecofFacade.findProyectoByAnio(this.findAnio, this.login.getIdOrganismo());
      else {
        this.colAccionCentralizada = this.sigecofFacade.findAccionCentralizadaByAnio(this.findAnio, this.login.getIdOrganismo());
      }
      this.selectAccionCentralizada = "0";
      this.selectProyecto = "0";
      this.selectAccionEspecifica = "0";

      this.colAccionEspecifica = null;
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public boolean isShowUelEspecificaByAccionEspecifica()
  {
    return this.showUelEspecificaByAccionEspecifica;
  }
  public boolean isShowUelEspecificaByUnidadEjecutora() {
    return this.showUelEspecificaByUnidadEjecutora;
  }
  public boolean isShowUelEspecificaByCategoriaPresupuesto() {
    return this.showUelEspecificaByCategoriaPresupuesto;
  }
  public boolean isShowUelEspecificaByAnio() {
    return this.showUelEspecificaByAnio;
  }

  public String selectUelEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectAccionEspecifica = null;
    this.selectUnidadEjecutora = null;

    long idUelEspecifica = 
      Long.parseLong((String)requestParameterMap.get("idUelEspecifica"));
    try
    {
      this.uelEspecifica = 
        this.sigecofFacade.findUelEspecificaById(
        idUelEspecifica);
      if (this.uelEspecifica.getAccionEspecifica() != null) {
        this.selectAccionEspecifica = 
          String.valueOf(this.uelEspecifica.getAccionEspecifica().getIdAccionEspecifica());
      }
      if (this.uelEspecifica.getUnidadEjecutora() != null) {
        this.selectUnidadEjecutora = 
          String.valueOf(this.uelEspecifica.getUnidadEjecutora().getIdUnidadEjecutora());
      }
      if (this.uelEspecifica.getAccionEspecifica().getTipo().equals("P"))
        this.colAccionEspecifica = this.sigecofFacade.findAccionEspecificaByProyecto(this.uelEspecifica.getAccionEspecifica().getProyecto().getIdProyecto());
      else {
        this.colAccionEspecifica = this.sigecofFacade.findAccionEspecificaByAccionCentralizada(this.uelEspecifica.getAccionEspecifica().getAccionCentralizada().getIdAccionCentralizada());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.uelEspecifica = null;
    this.showUelEspecificaByAccionEspecifica = false;
    this.showUelEspecificaByUnidadEjecutora = false;
    this.showUelEspecificaByCategoriaPresupuesto = false;
    this.showUelEspecificaByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      this.uelEspecifica.setAnio(this.uelEspecifica.getAccionEspecifica().getAnio());
      if ((this.uelEspecifica.getAccionEspecifica().getTipo().equals("P")) && (this.uelEspecifica.getAccionEspecifica() != null))
        this.uelEspecifica.setCategoriaPresupuesto(this.login.getOrganismo().getCodSigecof() + this.uelEspecifica.getAccionEspecifica().getProyecto().getCodProyecto() + this.uelEspecifica.getAccionEspecifica().getCodAccionEspecifica());
      else {
        this.uelEspecifica.setCategoriaPresupuesto(this.login.getOrganismo().getCodSigecof() + this.uelEspecifica.getAccionEspecifica().getAccionCentralizada().getCodAccionCentralizada() + this.uelEspecifica.getAccionEspecifica().getCodAccionEspecifica());
      }

      if (this.adding) {
        this.sigecofFacade.addUelEspecifica(
          this.uelEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.uelEspecifica);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateUelEspecifica(
          this.uelEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.uelEspecifica);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteUelEspecifica(
        this.uelEspecifica);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.uelEspecifica);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.uelEspecifica = new UelEspecifica();

    this.selectAccionEspecifica = null;

    this.selectUnidadEjecutora = null;

    this.colProyecto = null;
    this.colAccionCentralizada = null;
    this.colAccionEspecifica = null;
    this.selectAccionCentralizada = null;
    this.selectProyecto = null;
    this.selectAccionEspecifica = null;
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.uelEspecifica.setIdUelEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.UelEspecifica"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.uelEspecifica = new UelEspecifica();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public String getTipo()
  {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  public String getSelectAccionCentralizada() {
    return this.selectAccionCentralizada;
  }
  public void setSelectAccionCentralizada(String selectAccionCentralizada) {
    this.selectAccionCentralizada = selectAccionCentralizada;
  }
  public String getSelectProyecto() {
    return this.selectProyecto;
  }
  public void setSelectProyecto(String selectProyecto) {
    this.selectProyecto = selectProyecto;
  }
}