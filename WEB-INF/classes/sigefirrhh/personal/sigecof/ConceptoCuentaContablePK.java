package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class ConceptoCuentaContablePK
  implements Serializable
{
  public long idConceptoCuentaContable;

  public ConceptoCuentaContablePK()
  {
  }

  public ConceptoCuentaContablePK(long idConceptoCuentaContable)
  {
    this.idConceptoCuentaContable = idConceptoCuentaContable;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoCuentaContablePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoCuentaContablePK thatPK)
  {
    return 
      this.idConceptoCuentaContable == thatPK.idConceptoCuentaContable;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoCuentaContable)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoCuentaContable);
  }
}