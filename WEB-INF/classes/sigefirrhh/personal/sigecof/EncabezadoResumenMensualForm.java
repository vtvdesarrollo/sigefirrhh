package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.login.LoginSession;

public class EncabezadoResumenMensualForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EncabezadoResumenMensualForm.class.getName());
  private EncabezadoResumenMensual encabezadoResumenMensual;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showEncabezadoResumenMensualByAnio;
  private boolean showEncabezadoResumenMensualByMes;
  private boolean showEncabezadoResumenMensualByNumeroNomina;
  private boolean showEncabezadoResumenMensualByNumeroExpediente;
  private int findAnio;
  private int findMes;
  private int findNumeroNomina;
  private int findNumeroExpediente;
  private Collection colUnidadAdministradora;
  private String selectUnidadAdministradora;
  private Object stateResultEncabezadoResumenMensualByAnio = null;

  private Object stateResultEncabezadoResumenMensualByMes = null;

  private Object stateResultEncabezadoResumenMensualByNumeroNomina = null;

  private Object stateResultEncabezadoResumenMensualByNumeroExpediente = null;

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public int getFindMes() {
    return this.findMes;
  }
  public void setFindMes(int findMes) {
    this.findMes = findMes;
  }
  public int getFindNumeroNomina() {
    return this.findNumeroNomina;
  }
  public void setFindNumeroNomina(int findNumeroNomina) {
    this.findNumeroNomina = findNumeroNomina;
  }
  public int getFindNumeroExpediente() {
    return this.findNumeroExpediente;
  }
  public void setFindNumeroExpediente(int findNumeroExpediente) {
    this.findNumeroExpediente = findNumeroExpediente;
  }

  public String getSelectUnidadAdministradora()
  {
    return this.selectUnidadAdministradora;
  }
  public void setSelectUnidadAdministradora(String valUnidadAdministradora) {
    Iterator iterator = this.colUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    this.encabezadoResumenMensual.setUnidadAdministradora(null);
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      if (String.valueOf(unidadAdministradora.getIdUnidadAdministradora()).equals(
        valUnidadAdministradora)) {
        this.encabezadoResumenMensual.setUnidadAdministradora(
          unidadAdministradora);
        break;
      }
    }
    this.selectUnidadAdministradora = valUnidadAdministradora;
  }
  public Collection getResult() {
    return this.result;
  }

  public EncabezadoResumenMensual getEncabezadoResumenMensual() {
    if (this.encabezadoResumenMensual == null) {
      this.encabezadoResumenMensual = new EncabezadoResumenMensual();
    }
    return this.encabezadoResumenMensual;
  }

  public EncabezadoResumenMensualForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUnidadAdministradora()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadAdministradora.getIdUnidadAdministradora()), 
        unidadAdministradora.toString()));
    }
    return col;
  }

  public Collection getListCerrado() {
    Collection col = new ArrayList();

    Iterator iterEntry = EncabezadoResumenMensual.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findEncabezadoResumenMensualByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findEncabezadoResumenMensualByAnio(this.findAnio);
      this.showEncabezadoResumenMensualByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEncabezadoResumenMensualByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findMes = 0;
    this.findNumeroNomina = 0;
    this.findNumeroExpediente = 0;

    return null;
  }

  public String findEncabezadoResumenMensualByMes()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findEncabezadoResumenMensualByMes(this.findMes);
      this.showEncabezadoResumenMensualByMes = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEncabezadoResumenMensualByMes)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findMes = 0;
    this.findNumeroNomina = 0;
    this.findNumeroExpediente = 0;

    return null;
  }

  public String findEncabezadoResumenMensualByNumeroNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findEncabezadoResumenMensualByNumeroNomina(this.findNumeroNomina);
      this.showEncabezadoResumenMensualByNumeroNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEncabezadoResumenMensualByNumeroNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findMes = 0;
    this.findNumeroNomina = 0;
    this.findNumeroExpediente = 0;

    return null;
  }

  public String findEncabezadoResumenMensualByNumeroExpediente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findEncabezadoResumenMensualByNumeroExpediente(this.findNumeroExpediente);
      this.showEncabezadoResumenMensualByNumeroExpediente = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showEncabezadoResumenMensualByNumeroExpediente)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findMes = 0;
    this.findNumeroNomina = 0;
    this.findNumeroExpediente = 0;

    return null;
  }

  public boolean isShowEncabezadoResumenMensualByAnio() {
    return this.showEncabezadoResumenMensualByAnio;
  }
  public boolean isShowEncabezadoResumenMensualByMes() {
    return this.showEncabezadoResumenMensualByMes;
  }
  public boolean isShowEncabezadoResumenMensualByNumeroNomina() {
    return this.showEncabezadoResumenMensualByNumeroNomina;
  }
  public boolean isShowEncabezadoResumenMensualByNumeroExpediente() {
    return this.showEncabezadoResumenMensualByNumeroExpediente;
  }

  public String selectEncabezadoResumenMensual()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUnidadAdministradora = null;

    long idEncabezadoResumenMensual = 
      Long.parseLong((String)requestParameterMap.get("idEncabezadoResumenMensual"));
    try
    {
      this.encabezadoResumenMensual = 
        this.sigecofFacade.findEncabezadoResumenMensualById(
        idEncabezadoResumenMensual);
      if (this.encabezadoResumenMensual.getUnidadAdministradora() != null) {
        this.selectUnidadAdministradora = 
          String.valueOf(this.encabezadoResumenMensual.getUnidadAdministradora().getIdUnidadAdministradora());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.encabezadoResumenMensual = null;
    this.showEncabezadoResumenMensualByAnio = false;
    this.showEncabezadoResumenMensualByMes = false;
    this.showEncabezadoResumenMensualByNumeroNomina = false;
    this.showEncabezadoResumenMensualByNumeroExpediente = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.encabezadoResumenMensual.getFechaCierre() != null) && 
      (this.encabezadoResumenMensual.getFechaCierre().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Cierre no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addEncabezadoResumenMensual(
          this.encabezadoResumenMensual);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateEncabezadoResumenMensual(
          this.encabezadoResumenMensual);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteEncabezadoResumenMensual(
        this.encabezadoResumenMensual);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.encabezadoResumenMensual = new EncabezadoResumenMensual();

    this.selectUnidadAdministradora = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.encabezadoResumenMensual.setIdEncabezadoResumenMensual(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.EncabezadoResumenMensual"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.encabezadoResumenMensual = new EncabezadoResumenMensual();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}