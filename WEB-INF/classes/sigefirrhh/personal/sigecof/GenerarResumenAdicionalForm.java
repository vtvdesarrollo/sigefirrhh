package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.CategoriaPresupuesto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.AdministradoraUel;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.login.LoginSession;

public class GenerarResumenAdicionalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarResumenAdicionalForm.class.getName());
  private ResumenMensual resumenMensual;
  private Collection result;
  private LoginSession login;
  private EstructuraNoGenFacade estructuraFacade = new EstructuraNoGenFacade();
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private boolean showResumenMensualByUnidadAdministradora;
  private String findSelectTipoPersonal;
  private Collection findColTipoPersonal;
  private String findSelectConceptoTipoPersonal;
  private Collection findColConceptoTipoPersonal;
  private String findSelectUnidadAdministradora;
  private Collection findColUnidadAdministradora;
  private String findSelectCategoriaPresupuesto;
  private Collection findColCategoriaPresupuesto;
  private String findSelectAdministradoraUel;
  private Collection findColAdministradoraUel;
  private String findSelectUelEspecifica;
  private Collection findColUelEspecifica;
  private AdministradoraUel administradoraUel;
  private int findAnio;
  private int findMes;
  private double monto;
  private int numero = 1;
  private int quincena = 1;
  private UnidadAdministradora unidadAdministradora;

  public boolean isShowFindUelEspecifica()
  {
    return (this.findColUelEspecifica != null) && (!this.findColUelEspecifica.isEmpty());
  }
  public boolean isShowConcepto() {
    return (this.findColConceptoTipoPersonal != null) && (!this.findColConceptoTipoPersonal.isEmpty());
  }
  public void changeFindAdministradoraUel(ValueChangeEvent event) {
    long idUnidadEjecutora = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findSelectAdministradoraUel = String.valueOf(idUnidadEjecutora);

      if (idUnidadEjecutora != 0L) {
        this.findColUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutora(idUnidadEjecutora);
        this.administradoraUel = this.estructuraFacade.findAdministradoraUelById(idUnidadEjecutora);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.findColAdministradoraUel = null;
    }
  }

  public boolean isShowFindAdministradoraUel() { return (this.findColAdministradoraUel != null) && (!this.findColAdministradoraUel.isEmpty()); }

  public void changeFindUnidadAdministradora(ValueChangeEvent event) {
    long idUnidadAdministradora = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findSelectUnidadAdministradora = String.valueOf(idUnidadAdministradora);
      log.error("ID UAD " + idUnidadAdministradora);
      if (idUnidadAdministradora != 0L) {
        this.findColAdministradoraUel = this.estructuraFacade.findAdministradoraUelByUnidadAdministradora(idUnidadAdministradora);
        this.unidadAdministradora = this.estructuraFacade.findUnidadAdministradoraById(idUnidadAdministradora);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.findColAdministradoraUel = null;
    }
  }

  public void changeFindTipoPersonal(ValueChangeEvent event) { long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findSelectTipoPersonal = String.valueOf(idTipoPersonal);
      if (idTipoPersonal != 0L)
        this.findColConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonalAsignaciones(idTipoPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.findColAdministradoraUel = null;
    } }

  public String getFindSelectUnidadAdministradora() {
    return this.findSelectUnidadAdministradora;
  }
  public void setFindSelectUnidadAdministradora(String valUnidadAdministradora) {
    this.findSelectUnidadAdministradora = valUnidadAdministradora;
  }
  public Collection getFindColUnidadAdministradora() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadAdministradora.getIdUnidadAdministradora()), 
        unidadAdministradora.toString()));
    }
    return col;
  }

  public String getFindSelectCategoriaPresupuesto() {
    return this.findSelectCategoriaPresupuesto;
  }
  public void setFindSelectCategoriaPresupuesto(String valCategoriaPresupuesto) {
    this.findSelectCategoriaPresupuesto = valCategoriaPresupuesto;
  }
  public Collection getFindColCategoriaPresupuesto() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCategoriaPresupuesto.iterator();
    CategoriaPresupuesto categoriaPresupuesto = null;
    while (iterator.hasNext()) {
      categoriaPresupuesto = (CategoriaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaPresupuesto.getIdCategoriaPresupuesto()), 
        categoriaPresupuesto.toString()));
    }
    return col;
  }

  public String getFindSelectAdministradoraUel() {
    return this.findSelectAdministradoraUel;
  }
  public void setFindSelectAdministradoraUel(String valAdministradoraUel) {
    this.findSelectAdministradoraUel = valAdministradoraUel;
  }
  public Collection getFindColAdministradoraUel() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAdministradoraUel.iterator();
    AdministradoraUel administradoraUel = null;
    while (iterator.hasNext()) {
      administradoraUel = (AdministradoraUel)iterator.next();
      col.add(new SelectItem(
        String.valueOf(administradoraUel.getUnidadEjecutora().getIdUnidadEjecutora()), 
        administradoraUel.getUnidadEjecutora().toString()));
    }
    return col;
  }
  public Collection getFindColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectUelEspecifica() {
    return this.findSelectUelEspecifica;
  }
  public void setFindSelectUelEspecifica(String valUelEspecifica) {
    this.findSelectUelEspecifica = valUelEspecifica;
  }
  public Collection getFindColUelEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }
  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getResult() {
    return this.result;
  }

  public ResumenMensual getResumenMensual() {
    if (this.resumenMensual == null) {
      this.resumenMensual = new ResumenMensual();
    }
    return this.resumenMensual;
  }

  public GenerarResumenAdicionalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.findAnio = (new Date().getYear() + 1900);
    this.findMes = (new Date().getMonth() + 1);
    refresh();
  }

  public void refresh()
  {
    try
    {
      this.findColUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColCategoriaPresupuesto = 
        this.definicionesFacade.findAllCategoriaPresupuesto();
      this.findColTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.findColUnidadAdministradora = new ArrayList();
      this.findColCategoriaPresupuesto = new ArrayList();
    }
  }

  public String generar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.sigecofFacade.generarResumenAdicional(this.unidadAdministradora.getIdUnidadAdministradora(), 
        Long.valueOf(this.findSelectConceptoTipoPersonal).longValue(), this.monto, this.findAnio, 
        this.findMes, Long.valueOf(this.findSelectUelEspecifica).longValue(), 
        this.administradoraUel.getUnidadEjecutora().getIdUnidadEjecutora(), this.quincena, 
        this.unidadAdministradora.getCodUnidadAdminist(), 0, this.login.getUsuario(), 
        "RESUMEN COMPLEMENTARIO - NOMINA ORDINARIA", "N", 0L, this.numero);
      context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public boolean isShowResumenMensualByUnidadAdministradora() {
    return this.showResumenMensualByUnidadAdministradora;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public int getFindMes() {
    return this.findMes;
  }
  public void setFindMes(int findMes) {
    this.findMes = findMes;
  }
  public String getFindSelectConceptoTipoPersonal() {
    return this.findSelectConceptoTipoPersonal;
  }

  public void setFindSelectConceptoTipoPersonal(String findSelectConceptoTipoPersonal) {
    this.findSelectConceptoTipoPersonal = findSelectConceptoTipoPersonal;
  }
  public String getFindSelectTipoPersonal() {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String findSelectTipoPersonal) {
    this.findSelectTipoPersonal = findSelectTipoPersonal;
  }
  public double getMonto() {
    return this.monto;
  }
  public void setMonto(double monto) {
    this.monto = monto;
  }
  public int getNumero() {
    return this.numero;
  }
  public void setNumero(int numero) {
    this.numero = numero;
  }
  public int getQuincena() {
    return this.quincena;
  }
  public void setQuincena(int quincena) {
    this.quincena = quincena;
  }
}