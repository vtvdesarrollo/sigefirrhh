package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class UnidadMedida
  implements Serializable, PersistenceCapable
{
  private long idUnidadMedida;
  private String codUnidadMedida;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codUnidadMedida", "descripcion", "idUnidadMedida" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodUnidadMedida(this) + "-" + jdoGetdescripcion(this);
  }

  public String getCodUnidadMedida()
  {
    return jdoGetcodUnidadMedida(this);
  }

  public void setCodUnidadMedida(String codUnidadMedida)
  {
    jdoSetcodUnidadMedida(this, codUnidadMedida);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion)
  {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdUnidadMedida()
  {
    return jdoGetidUnidadMedida(this);
  }

  public void setIdUnidadMedida(long idUnidadMedida)
  {
    jdoSetidUnidadMedida(this, idUnidadMedida);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.UnidadMedida"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UnidadMedida());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UnidadMedida localUnidadMedida = new UnidadMedida();
    localUnidadMedida.jdoFlags = 1;
    localUnidadMedida.jdoStateManager = paramStateManager;
    return localUnidadMedida;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UnidadMedida localUnidadMedida = new UnidadMedida();
    localUnidadMedida.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUnidadMedida.jdoFlags = 1;
    localUnidadMedida.jdoStateManager = paramStateManager;
    return localUnidadMedida;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUnidadMedida);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUnidadMedida);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUnidadMedida = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUnidadMedida = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UnidadMedida paramUnidadMedida, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUnidadMedida == null)
        throw new IllegalArgumentException("arg1");
      this.codUnidadMedida = paramUnidadMedida.codUnidadMedida;
      return;
    case 1:
      if (paramUnidadMedida == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramUnidadMedida.descripcion;
      return;
    case 2:
      if (paramUnidadMedida == null)
        throw new IllegalArgumentException("arg1");
      this.idUnidadMedida = paramUnidadMedida.idUnidadMedida;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UnidadMedida))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UnidadMedida localUnidadMedida = (UnidadMedida)paramObject;
    if (localUnidadMedida.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUnidadMedida, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UnidadMedidaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UnidadMedidaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadMedidaPK))
      throw new IllegalArgumentException("arg1");
    UnidadMedidaPK localUnidadMedidaPK = (UnidadMedidaPK)paramObject;
    localUnidadMedidaPK.idUnidadMedida = this.idUnidadMedida;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UnidadMedidaPK))
      throw new IllegalArgumentException("arg1");
    UnidadMedidaPK localUnidadMedidaPK = (UnidadMedidaPK)paramObject;
    this.idUnidadMedida = localUnidadMedidaPK.idUnidadMedida;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadMedidaPK))
      throw new IllegalArgumentException("arg2");
    UnidadMedidaPK localUnidadMedidaPK = (UnidadMedidaPK)paramObject;
    localUnidadMedidaPK.idUnidadMedida = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UnidadMedidaPK))
      throw new IllegalArgumentException("arg2");
    UnidadMedidaPK localUnidadMedidaPK = (UnidadMedidaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localUnidadMedidaPK.idUnidadMedida);
  }

  private static final String jdoGetcodUnidadMedida(UnidadMedida paramUnidadMedida)
  {
    if (paramUnidadMedida.jdoFlags <= 0)
      return paramUnidadMedida.codUnidadMedida;
    StateManager localStateManager = paramUnidadMedida.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadMedida.codUnidadMedida;
    if (localStateManager.isLoaded(paramUnidadMedida, jdoInheritedFieldCount + 0))
      return paramUnidadMedida.codUnidadMedida;
    return localStateManager.getStringField(paramUnidadMedida, jdoInheritedFieldCount + 0, paramUnidadMedida.codUnidadMedida);
  }

  private static final void jdoSetcodUnidadMedida(UnidadMedida paramUnidadMedida, String paramString)
  {
    if (paramUnidadMedida.jdoFlags == 0)
    {
      paramUnidadMedida.codUnidadMedida = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadMedida.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadMedida.codUnidadMedida = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadMedida, jdoInheritedFieldCount + 0, paramUnidadMedida.codUnidadMedida, paramString);
  }

  private static final String jdoGetdescripcion(UnidadMedida paramUnidadMedida)
  {
    if (paramUnidadMedida.jdoFlags <= 0)
      return paramUnidadMedida.descripcion;
    StateManager localStateManager = paramUnidadMedida.jdoStateManager;
    if (localStateManager == null)
      return paramUnidadMedida.descripcion;
    if (localStateManager.isLoaded(paramUnidadMedida, jdoInheritedFieldCount + 1))
      return paramUnidadMedida.descripcion;
    return localStateManager.getStringField(paramUnidadMedida, jdoInheritedFieldCount + 1, paramUnidadMedida.descripcion);
  }

  private static final void jdoSetdescripcion(UnidadMedida paramUnidadMedida, String paramString)
  {
    if (paramUnidadMedida.jdoFlags == 0)
    {
      paramUnidadMedida.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramUnidadMedida.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadMedida.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramUnidadMedida, jdoInheritedFieldCount + 1, paramUnidadMedida.descripcion, paramString);
  }

  private static final long jdoGetidUnidadMedida(UnidadMedida paramUnidadMedida)
  {
    return paramUnidadMedida.idUnidadMedida;
  }

  private static final void jdoSetidUnidadMedida(UnidadMedida paramUnidadMedida, long paramLong)
  {
    StateManager localStateManager = paramUnidadMedida.jdoStateManager;
    if (localStateManager == null)
    {
      paramUnidadMedida.idUnidadMedida = paramLong;
      return;
    }
    localStateManager.setLongField(paramUnidadMedida, jdoInheritedFieldCount + 2, paramUnidadMedida.idUnidadMedida, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}