package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class CargoEspecificaPK
  implements Serializable
{
  public long idCargoEspecifica;

  public CargoEspecificaPK()
  {
  }

  public CargoEspecificaPK(long idCargoEspecifica)
  {
    this.idCargoEspecifica = idCargoEspecifica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CargoEspecificaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CargoEspecificaPK thatPK)
  {
    return 
      this.idCargoEspecifica == thatPK.idCargoEspecifica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCargoEspecifica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCargoEspecifica);
  }
}