package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PresupuestoEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PresupuestoEspecificaForm.class.getName());
  private PresupuestoEspecifica presupuestoEspecifica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showPresupuestoEspecificaByUelEspecifica;
  private boolean showPresupuestoEspecificaByCuentaPresupuesto;
  private boolean showPresupuestoEspecificaByAnio;
  private String findSelectUelEspecifica;
  private String findSelectCuentaPresupuesto;
  private int findAnio;
  private Collection findColUelEspecifica = null;
  private Collection findColCuentaPresupuesto = null;
  private Collection colUelEspecifica;
  private Collection colCuentaPresupuesto;
  private String selectUelEspecifica;
  private String selectCuentaPresupuesto;
  private Object stateResultPresupuestoEspecificaByUelEspecifica = null;

  private Object stateResultPresupuestoEspecificaByCuentaPresupuesto = null;

  private Object stateResultPresupuestoEspecificaByAnio = null;

  public String getFindSelectUelEspecifica()
  {
    return this.findSelectUelEspecifica;
  }
  public void setFindSelectUelEspecifica(String valUelEspecifica) {
    this.findSelectUelEspecifica = valUelEspecifica;
  }

  public Collection getFindColUelEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }
  public String getFindSelectCuentaPresupuesto() {
    return this.findSelectCuentaPresupuesto;
  }
  public void setFindSelectCuentaPresupuesto(String valCuentaPresupuesto) {
    this.findSelectCuentaPresupuesto = valCuentaPresupuesto;
  }

  public Collection getFindColCuentaPresupuesto() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCuentaPresupuesto.iterator();
    CuentaPresupuesto cuentaPresupuesto = null;
    while (iterator.hasNext()) {
      cuentaPresupuesto = (CuentaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cuentaPresupuesto.getIdCuentaPresupuesto()), 
        cuentaPresupuesto.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectUelEspecifica()
  {
    return this.selectUelEspecifica;
  }
  public void setSelectUelEspecifica(String valUelEspecifica) {
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    this.presupuestoEspecifica.setUelEspecifica(null);
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      if (String.valueOf(uelEspecifica.getIdUelEspecifica()).equals(
        valUelEspecifica)) {
        this.presupuestoEspecifica.setUelEspecifica(
          uelEspecifica);
        break;
      }
    }
    this.selectUelEspecifica = valUelEspecifica;
  }
  public String getSelectCuentaPresupuesto() {
    return this.selectCuentaPresupuesto;
  }
  public void setSelectCuentaPresupuesto(String valCuentaPresupuesto) {
    Iterator iterator = this.colCuentaPresupuesto.iterator();
    CuentaPresupuesto cuentaPresupuesto = null;
    this.presupuestoEspecifica.setCuentaPresupuesto(null);
    while (iterator.hasNext()) {
      cuentaPresupuesto = (CuentaPresupuesto)iterator.next();
      if (String.valueOf(cuentaPresupuesto.getIdCuentaPresupuesto()).equals(
        valCuentaPresupuesto)) {
        this.presupuestoEspecifica.setCuentaPresupuesto(
          cuentaPresupuesto);
        break;
      }
    }
    this.selectCuentaPresupuesto = valCuentaPresupuesto;
  }
  public Collection getResult() {
    return this.result;
  }

  public PresupuestoEspecifica getPresupuestoEspecifica() {
    if (this.presupuestoEspecifica == null) {
      this.presupuestoEspecifica = new PresupuestoEspecifica();
    }
    return this.presupuestoEspecifica;
  }

  public PresupuestoEspecificaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUelEspecifica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }

  public Collection getColCuentaPresupuesto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCuentaPresupuesto.iterator();
    CuentaPresupuesto cuentaPresupuesto = null;
    while (iterator.hasNext()) {
      cuentaPresupuesto = (CuentaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cuentaPresupuesto.getIdCuentaPresupuesto()), 
        cuentaPresupuesto.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColUelEspecifica = 
        this.sigecofFacade.findAllUelEspecifica();
      this.findColCuentaPresupuesto = 
        this.sigecofFacade.findCuentaPresupuestoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colUelEspecifica = 
        this.sigecofFacade.findAllUelEspecifica();
      this.colCuentaPresupuesto = 
        this.sigecofFacade.findCuentaPresupuestoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Error al buscar cuentas especificas por id Organismo: " + e);
      log.error("Con traza:");
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPresupuestoEspecificaByUelEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findPresupuestoEspecificaByUelEspecifica(Long.valueOf(this.findSelectUelEspecifica).longValue());
      this.showPresupuestoEspecificaByUelEspecifica = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPresupuestoEspecificaByUelEspecifica)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findSelectCuentaPresupuesto = null;
    this.findAnio = 0;

    return null;
  }

  public String findPresupuestoEspecificaByCuentaPresupuesto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findPresupuestoEspecificaByCuentaPresupuesto(Long.valueOf(this.findSelectCuentaPresupuesto).longValue());
      this.showPresupuestoEspecificaByCuentaPresupuesto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPresupuestoEspecificaByCuentaPresupuesto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findSelectCuentaPresupuesto = null;
    this.findAnio = 0;

    return null;
  }

  public String findPresupuestoEspecificaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findPresupuestoEspecificaByAnio(this.findAnio);
      this.showPresupuestoEspecificaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPresupuestoEspecificaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUelEspecifica = null;
    this.findSelectCuentaPresupuesto = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowPresupuestoEspecificaByUelEspecifica() {
    return this.showPresupuestoEspecificaByUelEspecifica;
  }
  public boolean isShowPresupuestoEspecificaByCuentaPresupuesto() {
    return this.showPresupuestoEspecificaByCuentaPresupuesto;
  }
  public boolean isShowPresupuestoEspecificaByAnio() {
    return this.showPresupuestoEspecificaByAnio;
  }

  public String selectPresupuestoEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUelEspecifica = null;
    this.selectCuentaPresupuesto = null;

    long idPresupuestoEspecifica = 
      Long.parseLong((String)requestParameterMap.get("idPresupuestoEspecifica"));
    try
    {
      this.presupuestoEspecifica = 
        this.sigecofFacade.findPresupuestoEspecificaById(
        idPresupuestoEspecifica);
      if (this.presupuestoEspecifica.getUelEspecifica() != null) {
        this.selectUelEspecifica = 
          String.valueOf(this.presupuestoEspecifica.getUelEspecifica().getIdUelEspecifica());
      }
      if (this.presupuestoEspecifica.getCuentaPresupuesto() != null) {
        this.selectCuentaPresupuesto = 
          String.valueOf(this.presupuestoEspecifica.getCuentaPresupuesto().getIdCuentaPresupuesto());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.presupuestoEspecifica = null;
    this.showPresupuestoEspecificaByUelEspecifica = false;
    this.showPresupuestoEspecificaByCuentaPresupuesto = false;
    this.showPresupuestoEspecificaByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addPresupuestoEspecifica(
          this.presupuestoEspecifica);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updatePresupuestoEspecifica(
          this.presupuestoEspecifica);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deletePresupuestoEspecifica(
        this.presupuestoEspecifica);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.presupuestoEspecifica = new PresupuestoEspecifica();

    this.selectUelEspecifica = null;

    this.selectCuentaPresupuesto = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.presupuestoEspecifica.setIdPresupuestoEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.PresupuestoEspecifica"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.presupuestoEspecifica = new PresupuestoEspecifica();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}