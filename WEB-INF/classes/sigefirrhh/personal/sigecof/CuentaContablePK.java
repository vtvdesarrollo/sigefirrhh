package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class CuentaContablePK
  implements Serializable
{
  public long idCuentaContable;

  public CuentaContablePK()
  {
  }

  public CuentaContablePK(long idCuentaContable)
  {
    this.idCuentaContable = idCuentaContable;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CuentaContablePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CuentaContablePK thatPK)
  {
    return 
      this.idCuentaContable == thatPK.idCuentaContable;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCuentaContable)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCuentaContable);
  }
}