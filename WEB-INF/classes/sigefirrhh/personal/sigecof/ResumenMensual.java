package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadEjecutora;

public class ResumenMensual
  implements Serializable, PersistenceCapable
{
  private long idResumenMensual;
  private UnidadAdministradora unidadAdministradora;
  private EncabezadoResumenMensual encabezadoResumenMensual;
  private UelEspecifica uelEspecifica;
  private CuentaPresupuesto cuentaPresupuesto;
  private UnidadEjecutora unidadEjecutora;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FuenteFinanciamiento fuenteFinanciamiento;
  private double monto;
  private double porcentaje;
  private int quincena;
  private String categoriaPresupuesto;
  private String codConcepto;
  private String codUnidadEjecutora;
  private String codPresupuesto;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "categoriaPresupuesto", "codConcepto", "codPresupuesto", "codUnidadEjecutora", "conceptoTipoPersonal", "cuentaPresupuesto", "encabezadoResumenMensual", "fuenteFinanciamiento", "idResumenMensual", "monto", "porcentaje", "quincena", "uelEspecifica", "unidadAdministradora", "unidadEjecutora" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaPresupuesto"), sunjdo$classForName$("sigefirrhh.personal.sigecof.EncabezadoResumenMensual"), sunjdo$classForName$("sigefirrhh.personal.sigecof.FuenteFinanciamiento"), Long.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.UelEspecifica"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadAdministradora"), sunjdo$classForName$("sigefirrhh.base.estructura.UnidadEjecutora") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 26, 26, 26, 26, 24, 21, 21, 21, 26, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ResumenMensual()
  {
    jdoSetmonto(this, 0.0D);

    jdoSetporcentaje(this, 0.0D);

    jdoSetquincena(this, 0);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmonto(this));

    return "UEL " + jdoGetcodUnidadEjecutora(this) + jdoGetcodPresupuesto(this) + "  - " + jdoGetconceptoTipoPersonal(this).getConcepto().getCodConcepto() + 
      " " + jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - " + a + " - " + jdoGetquincena(this) + " - TP " + jdoGetconceptoTipoPersonal(this).getTipoPersonal().getCodTipoPersonal();
  }

  public String getCategoriaPresupuesto()
  {
    return jdoGetcategoriaPresupuesto(this);
  }
  public void setCategoriaPresupuesto(String categoriaPresupuesto) {
    jdoSetcategoriaPresupuesto(this, categoriaPresupuesto);
  }
  public String getCodConcepto() {
    return jdoGetcodConcepto(this);
  }
  public void setCodConcepto(String codConcepto) {
    jdoSetcodConcepto(this, codConcepto);
  }
  public String getCodPresupuesto() {
    return jdoGetcodPresupuesto(this);
  }
  public void setCodPresupuesto(String codPresupuesto) {
    jdoSetcodPresupuesto(this, codPresupuesto);
  }
  public String getCodUnidadEjecutora() {
    return jdoGetcodUnidadEjecutora(this);
  }
  public void setCodUnidadEjecutora(String codUnidadEjecutora) {
    jdoSetcodUnidadEjecutora(this, codUnidadEjecutora);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public CuentaPresupuesto getCuentaPresupuesto() {
    return jdoGetcuentaPresupuesto(this);
  }
  public void setCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) {
    jdoSetcuentaPresupuesto(this, cuentaPresupuesto);
  }
  public EncabezadoResumenMensual getEncabezadoResumenMensual() {
    return jdoGetencabezadoResumenMensual(this);
  }

  public void setEncabezadoResumenMensual(EncabezadoResumenMensual encabezadoResumenMensual) {
    jdoSetencabezadoResumenMensual(this, encabezadoResumenMensual);
  }
  public long getIdResumenMensual() {
    return jdoGetidResumenMensual(this);
  }
  public void setIdResumenMensual(long idResumenMensual) {
    jdoSetidResumenMensual(this, idResumenMensual);
  }
  public double getMonto() {
    return jdoGetmonto(this);
  }
  public void setMonto(double monto) {
    jdoSetmonto(this, monto);
  }
  public double getPorcentaje() {
    return jdoGetporcentaje(this);
  }
  public void setPorcentaje(double porcentaje) {
    jdoSetporcentaje(this, porcentaje);
  }
  public int getQuincena() {
    return jdoGetquincena(this);
  }
  public void setQuincena(int quincena) {
    jdoSetquincena(this, quincena);
  }
  public UelEspecifica getUelEspecifica() {
    return jdoGetuelEspecifica(this);
  }
  public void setUelEspecifica(UelEspecifica uelEspecifica) {
    jdoSetuelEspecifica(this, uelEspecifica);
  }
  public UnidadAdministradora getUnidadAdministradora() {
    return jdoGetunidadAdministradora(this);
  }

  public void setUnidadAdministradora(UnidadAdministradora unidadAdministradora) {
    jdoSetunidadAdministradora(this, unidadAdministradora);
  }
  public UnidadEjecutora getUnidadEjecutora() {
    return jdoGetunidadEjecutora(this);
  }
  public void setUnidadEjecutora(UnidadEjecutora unidadEjecutora) {
    jdoSetunidadEjecutora(this, unidadEjecutora);
  }

  public FuenteFinanciamiento getFuenteFinanciamiento()
  {
    return jdoGetfuenteFinanciamiento(this);
  }

  public void setFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento)
  {
    jdoSetfuenteFinanciamiento(this, fuenteFinanciamiento);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.ResumenMensual"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ResumenMensual());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ResumenMensual localResumenMensual = new ResumenMensual();
    localResumenMensual.jdoFlags = 1;
    localResumenMensual.jdoStateManager = paramStateManager;
    return localResumenMensual;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ResumenMensual localResumenMensual = new ResumenMensual();
    localResumenMensual.jdoCopyKeyFieldsFromObjectId(paramObject);
    localResumenMensual.jdoFlags = 1;
    localResumenMensual.jdoStateManager = paramStateManager;
    return localResumenMensual;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.categoriaPresupuesto);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcepto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPresupuesto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codUnidadEjecutora);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cuentaPresupuesto);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.encabezadoResumenMensual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fuenteFinanciamiento);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idResumenMensual);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.monto);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.quincena);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.uelEspecifica);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadAdministradora);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadEjecutora);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.categoriaPresupuesto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcepto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPresupuesto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codUnidadEjecutora = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaPresupuesto = ((CuentaPresupuesto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.encabezadoResumenMensual = ((EncabezadoResumenMensual)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fuenteFinanciamiento = ((FuenteFinanciamiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idResumenMensual = localStateManager.replacingLongField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.monto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.quincena = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.uelEspecifica = ((UelEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadAdministradora = ((UnidadAdministradora)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadEjecutora = ((UnidadEjecutora)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ResumenMensual paramResumenMensual, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.categoriaPresupuesto = paramResumenMensual.categoriaPresupuesto;
      return;
    case 1:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.codConcepto = paramResumenMensual.codConcepto;
      return;
    case 2:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.codPresupuesto = paramResumenMensual.codPresupuesto;
      return;
    case 3:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.codUnidadEjecutora = paramResumenMensual.codUnidadEjecutora;
      return;
    case 4:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramResumenMensual.conceptoTipoPersonal;
      return;
    case 5:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaPresupuesto = paramResumenMensual.cuentaPresupuesto;
      return;
    case 6:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.encabezadoResumenMensual = paramResumenMensual.encabezadoResumenMensual;
      return;
    case 7:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.fuenteFinanciamiento = paramResumenMensual.fuenteFinanciamiento;
      return;
    case 8:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.idResumenMensual = paramResumenMensual.idResumenMensual;
      return;
    case 9:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.monto = paramResumenMensual.monto;
      return;
    case 10:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramResumenMensual.porcentaje;
      return;
    case 11:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.quincena = paramResumenMensual.quincena;
      return;
    case 12:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.uelEspecifica = paramResumenMensual.uelEspecifica;
      return;
    case 13:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.unidadAdministradora = paramResumenMensual.unidadAdministradora;
      return;
    case 14:
      if (paramResumenMensual == null)
        throw new IllegalArgumentException("arg1");
      this.unidadEjecutora = paramResumenMensual.unidadEjecutora;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ResumenMensual))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ResumenMensual localResumenMensual = (ResumenMensual)paramObject;
    if (localResumenMensual.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localResumenMensual, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ResumenMensualPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ResumenMensualPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResumenMensualPK))
      throw new IllegalArgumentException("arg1");
    ResumenMensualPK localResumenMensualPK = (ResumenMensualPK)paramObject;
    localResumenMensualPK.idResumenMensual = this.idResumenMensual;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResumenMensualPK))
      throw new IllegalArgumentException("arg1");
    ResumenMensualPK localResumenMensualPK = (ResumenMensualPK)paramObject;
    this.idResumenMensual = localResumenMensualPK.idResumenMensual;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResumenMensualPK))
      throw new IllegalArgumentException("arg2");
    ResumenMensualPK localResumenMensualPK = (ResumenMensualPK)paramObject;
    localResumenMensualPK.idResumenMensual = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 8);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResumenMensualPK))
      throw new IllegalArgumentException("arg2");
    ResumenMensualPK localResumenMensualPK = (ResumenMensualPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 8, localResumenMensualPK.idResumenMensual);
  }

  private static final String jdoGetcategoriaPresupuesto(ResumenMensual paramResumenMensual)
  {
    if (paramResumenMensual.jdoFlags <= 0)
      return paramResumenMensual.categoriaPresupuesto;
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.categoriaPresupuesto;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 0))
      return paramResumenMensual.categoriaPresupuesto;
    return localStateManager.getStringField(paramResumenMensual, jdoInheritedFieldCount + 0, paramResumenMensual.categoriaPresupuesto);
  }

  private static final void jdoSetcategoriaPresupuesto(ResumenMensual paramResumenMensual, String paramString)
  {
    if (paramResumenMensual.jdoFlags == 0)
    {
      paramResumenMensual.categoriaPresupuesto = paramString;
      return;
    }
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.categoriaPresupuesto = paramString;
      return;
    }
    localStateManager.setStringField(paramResumenMensual, jdoInheritedFieldCount + 0, paramResumenMensual.categoriaPresupuesto, paramString);
  }

  private static final String jdoGetcodConcepto(ResumenMensual paramResumenMensual)
  {
    if (paramResumenMensual.jdoFlags <= 0)
      return paramResumenMensual.codConcepto;
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.codConcepto;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 1))
      return paramResumenMensual.codConcepto;
    return localStateManager.getStringField(paramResumenMensual, jdoInheritedFieldCount + 1, paramResumenMensual.codConcepto);
  }

  private static final void jdoSetcodConcepto(ResumenMensual paramResumenMensual, String paramString)
  {
    if (paramResumenMensual.jdoFlags == 0)
    {
      paramResumenMensual.codConcepto = paramString;
      return;
    }
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.codConcepto = paramString;
      return;
    }
    localStateManager.setStringField(paramResumenMensual, jdoInheritedFieldCount + 1, paramResumenMensual.codConcepto, paramString);
  }

  private static final String jdoGetcodPresupuesto(ResumenMensual paramResumenMensual)
  {
    if (paramResumenMensual.jdoFlags <= 0)
      return paramResumenMensual.codPresupuesto;
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.codPresupuesto;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 2))
      return paramResumenMensual.codPresupuesto;
    return localStateManager.getStringField(paramResumenMensual, jdoInheritedFieldCount + 2, paramResumenMensual.codPresupuesto);
  }

  private static final void jdoSetcodPresupuesto(ResumenMensual paramResumenMensual, String paramString)
  {
    if (paramResumenMensual.jdoFlags == 0)
    {
      paramResumenMensual.codPresupuesto = paramString;
      return;
    }
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.codPresupuesto = paramString;
      return;
    }
    localStateManager.setStringField(paramResumenMensual, jdoInheritedFieldCount + 2, paramResumenMensual.codPresupuesto, paramString);
  }

  private static final String jdoGetcodUnidadEjecutora(ResumenMensual paramResumenMensual)
  {
    if (paramResumenMensual.jdoFlags <= 0)
      return paramResumenMensual.codUnidadEjecutora;
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.codUnidadEjecutora;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 3))
      return paramResumenMensual.codUnidadEjecutora;
    return localStateManager.getStringField(paramResumenMensual, jdoInheritedFieldCount + 3, paramResumenMensual.codUnidadEjecutora);
  }

  private static final void jdoSetcodUnidadEjecutora(ResumenMensual paramResumenMensual, String paramString)
  {
    if (paramResumenMensual.jdoFlags == 0)
    {
      paramResumenMensual.codUnidadEjecutora = paramString;
      return;
    }
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.codUnidadEjecutora = paramString;
      return;
    }
    localStateManager.setStringField(paramResumenMensual, jdoInheritedFieldCount + 3, paramResumenMensual.codUnidadEjecutora, paramString);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ResumenMensual paramResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 4))
      return paramResumenMensual.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramResumenMensual, jdoInheritedFieldCount + 4, paramResumenMensual.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ResumenMensual paramResumenMensual, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramResumenMensual, jdoInheritedFieldCount + 4, paramResumenMensual.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final CuentaPresupuesto jdoGetcuentaPresupuesto(ResumenMensual paramResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.cuentaPresupuesto;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 5))
      return paramResumenMensual.cuentaPresupuesto;
    return (CuentaPresupuesto)localStateManager.getObjectField(paramResumenMensual, jdoInheritedFieldCount + 5, paramResumenMensual.cuentaPresupuesto);
  }

  private static final void jdoSetcuentaPresupuesto(ResumenMensual paramResumenMensual, CuentaPresupuesto paramCuentaPresupuesto)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.cuentaPresupuesto = paramCuentaPresupuesto;
      return;
    }
    localStateManager.setObjectField(paramResumenMensual, jdoInheritedFieldCount + 5, paramResumenMensual.cuentaPresupuesto, paramCuentaPresupuesto);
  }

  private static final EncabezadoResumenMensual jdoGetencabezadoResumenMensual(ResumenMensual paramResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.encabezadoResumenMensual;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 6))
      return paramResumenMensual.encabezadoResumenMensual;
    return (EncabezadoResumenMensual)localStateManager.getObjectField(paramResumenMensual, jdoInheritedFieldCount + 6, paramResumenMensual.encabezadoResumenMensual);
  }

  private static final void jdoSetencabezadoResumenMensual(ResumenMensual paramResumenMensual, EncabezadoResumenMensual paramEncabezadoResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.encabezadoResumenMensual = paramEncabezadoResumenMensual;
      return;
    }
    localStateManager.setObjectField(paramResumenMensual, jdoInheritedFieldCount + 6, paramResumenMensual.encabezadoResumenMensual, paramEncabezadoResumenMensual);
  }

  private static final FuenteFinanciamiento jdoGetfuenteFinanciamiento(ResumenMensual paramResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.fuenteFinanciamiento;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 7))
      return paramResumenMensual.fuenteFinanciamiento;
    return (FuenteFinanciamiento)localStateManager.getObjectField(paramResumenMensual, jdoInheritedFieldCount + 7, paramResumenMensual.fuenteFinanciamiento);
  }

  private static final void jdoSetfuenteFinanciamiento(ResumenMensual paramResumenMensual, FuenteFinanciamiento paramFuenteFinanciamiento)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.fuenteFinanciamiento = paramFuenteFinanciamiento;
      return;
    }
    localStateManager.setObjectField(paramResumenMensual, jdoInheritedFieldCount + 7, paramResumenMensual.fuenteFinanciamiento, paramFuenteFinanciamiento);
  }

  private static final long jdoGetidResumenMensual(ResumenMensual paramResumenMensual)
  {
    return paramResumenMensual.idResumenMensual;
  }

  private static final void jdoSetidResumenMensual(ResumenMensual paramResumenMensual, long paramLong)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.idResumenMensual = paramLong;
      return;
    }
    localStateManager.setLongField(paramResumenMensual, jdoInheritedFieldCount + 8, paramResumenMensual.idResumenMensual, paramLong);
  }

  private static final double jdoGetmonto(ResumenMensual paramResumenMensual)
  {
    if (paramResumenMensual.jdoFlags <= 0)
      return paramResumenMensual.monto;
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.monto;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 9))
      return paramResumenMensual.monto;
    return localStateManager.getDoubleField(paramResumenMensual, jdoInheritedFieldCount + 9, paramResumenMensual.monto);
  }

  private static final void jdoSetmonto(ResumenMensual paramResumenMensual, double paramDouble)
  {
    if (paramResumenMensual.jdoFlags == 0)
    {
      paramResumenMensual.monto = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.monto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenMensual, jdoInheritedFieldCount + 9, paramResumenMensual.monto, paramDouble);
  }

  private static final double jdoGetporcentaje(ResumenMensual paramResumenMensual)
  {
    if (paramResumenMensual.jdoFlags <= 0)
      return paramResumenMensual.porcentaje;
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.porcentaje;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 10))
      return paramResumenMensual.porcentaje;
    return localStateManager.getDoubleField(paramResumenMensual, jdoInheritedFieldCount + 10, paramResumenMensual.porcentaje);
  }

  private static final void jdoSetporcentaje(ResumenMensual paramResumenMensual, double paramDouble)
  {
    if (paramResumenMensual.jdoFlags == 0)
    {
      paramResumenMensual.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResumenMensual, jdoInheritedFieldCount + 10, paramResumenMensual.porcentaje, paramDouble);
  }

  private static final int jdoGetquincena(ResumenMensual paramResumenMensual)
  {
    if (paramResumenMensual.jdoFlags <= 0)
      return paramResumenMensual.quincena;
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.quincena;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 11))
      return paramResumenMensual.quincena;
    return localStateManager.getIntField(paramResumenMensual, jdoInheritedFieldCount + 11, paramResumenMensual.quincena);
  }

  private static final void jdoSetquincena(ResumenMensual paramResumenMensual, int paramInt)
  {
    if (paramResumenMensual.jdoFlags == 0)
    {
      paramResumenMensual.quincena = paramInt;
      return;
    }
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.quincena = paramInt;
      return;
    }
    localStateManager.setIntField(paramResumenMensual, jdoInheritedFieldCount + 11, paramResumenMensual.quincena, paramInt);
  }

  private static final UelEspecifica jdoGetuelEspecifica(ResumenMensual paramResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.uelEspecifica;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 12))
      return paramResumenMensual.uelEspecifica;
    return (UelEspecifica)localStateManager.getObjectField(paramResumenMensual, jdoInheritedFieldCount + 12, paramResumenMensual.uelEspecifica);
  }

  private static final void jdoSetuelEspecifica(ResumenMensual paramResumenMensual, UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.uelEspecifica = paramUelEspecifica;
      return;
    }
    localStateManager.setObjectField(paramResumenMensual, jdoInheritedFieldCount + 12, paramResumenMensual.uelEspecifica, paramUelEspecifica);
  }

  private static final UnidadAdministradora jdoGetunidadAdministradora(ResumenMensual paramResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.unidadAdministradora;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 13))
      return paramResumenMensual.unidadAdministradora;
    return (UnidadAdministradora)localStateManager.getObjectField(paramResumenMensual, jdoInheritedFieldCount + 13, paramResumenMensual.unidadAdministradora);
  }

  private static final void jdoSetunidadAdministradora(ResumenMensual paramResumenMensual, UnidadAdministradora paramUnidadAdministradora)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.unidadAdministradora = paramUnidadAdministradora;
      return;
    }
    localStateManager.setObjectField(paramResumenMensual, jdoInheritedFieldCount + 13, paramResumenMensual.unidadAdministradora, paramUnidadAdministradora);
  }

  private static final UnidadEjecutora jdoGetunidadEjecutora(ResumenMensual paramResumenMensual)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
      return paramResumenMensual.unidadEjecutora;
    if (localStateManager.isLoaded(paramResumenMensual, jdoInheritedFieldCount + 14))
      return paramResumenMensual.unidadEjecutora;
    return (UnidadEjecutora)localStateManager.getObjectField(paramResumenMensual, jdoInheritedFieldCount + 14, paramResumenMensual.unidadEjecutora);
  }

  private static final void jdoSetunidadEjecutora(ResumenMensual paramResumenMensual, UnidadEjecutora paramUnidadEjecutora)
  {
    StateManager localStateManager = paramResumenMensual.jdoStateManager;
    if (localStateManager == null)
    {
      paramResumenMensual.unidadEjecutora = paramUnidadEjecutora;
      return;
    }
    localStateManager.setObjectField(paramResumenMensual, jdoInheritedFieldCount + 14, paramResumenMensual.unidadEjecutora, paramUnidadEjecutora);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}