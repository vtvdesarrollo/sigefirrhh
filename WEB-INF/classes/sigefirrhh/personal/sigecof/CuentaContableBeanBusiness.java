package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class CuentaContableBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCuentaContable(CuentaContable cuentaContable)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CuentaContable cuentaContableNew = 
      (CuentaContable)BeanUtils.cloneBean(
      cuentaContable);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (cuentaContableNew.getOrganismo() != null) {
      cuentaContableNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        cuentaContableNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(cuentaContableNew);
  }

  public void updateCuentaContable(CuentaContable cuentaContable) throws Exception
  {
    CuentaContable cuentaContableModify = 
      findCuentaContableById(cuentaContable.getIdCuentaContable());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (cuentaContable.getOrganismo() != null) {
      cuentaContable.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        cuentaContable.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(cuentaContableModify, cuentaContable);
  }

  public void deleteCuentaContable(CuentaContable cuentaContable) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CuentaContable cuentaContableDelete = 
      findCuentaContableById(cuentaContable.getIdCuentaContable());
    pm.deletePersistent(cuentaContableDelete);
  }

  public CuentaContable findCuentaContableById(long idCuentaContable) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCuentaContable == pIdCuentaContable";
    Query query = pm.newQuery(CuentaContable.class, filter);

    query.declareParameters("long pIdCuentaContable");

    parameters.put("pIdCuentaContable", new Long(idCuentaContable));

    Collection colCuentaContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCuentaContable.iterator();
    return (CuentaContable)iterator.next();
  }

  public Collection findCuentaContableAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cuentaContableExtent = pm.getExtent(
      CuentaContable.class, true);
    Query query = pm.newQuery(cuentaContableExtent);
    query.setOrdering("codContable ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodContable(String codContable, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codContable == pCodContable &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaContable.class, filter);

    query.declareParameters("java.lang.String pCodContable, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodContable", new String(codContable));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codContable ascending");

    Collection colCuentaContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaContable);

    return colCuentaContable;
  }

  public Collection findByDescripcion(String descripcion, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaContable.class, filter);

    query.declareParameters("java.lang.String pDescripcion, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codContable ascending");

    Collection colCuentaContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaContable);

    return colCuentaContable;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(CuentaContable.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codContable ascending");

    Collection colCuentaContable = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCuentaContable);

    return colCuentaContable;
  }
}