package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.personal.registroCargos.RegistroCargos;

public class CargoEspecifica
  implements Serializable, PersistenceCapable
{
  private long idCargoEspecifica;
  private UelEspecifica uelEspecifica;
  private RegistroCargos registroCargos;
  private int anio;
  private double porcentaje;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "idCargoEspecifica", "porcentaje", "registroCargos", "uelEspecifica" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroCargos"), sunjdo$classForName$("sigefirrhh.personal.sigecof.UelEspecifica") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public CargoEspecifica()
  {
    jdoSetporcentaje(this, 100.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetporcentaje(this));

    return jdoGetuelEspecifica(this).getCategoriaPresupuesto() + " - " + jdoGetuelEspecifica(this).getUnidadEjecutora().getCodUnidadEjecutora() + " - " + a + "% " + jdoGetregistroCargos(this).getSituacion() + " - " + jdoGetregistroCargos(this).getCargo().getDescripcionCargo();
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public double getPorcentaje() {
    return jdoGetporcentaje(this);
  }
  public void setPorcentaje(double porcentaje) {
    jdoSetporcentaje(this, porcentaje);
  }
  public UelEspecifica getUelEspecifica() {
    return jdoGetuelEspecifica(this);
  }
  public void setUelEspecifica(UelEspecifica uelEspecifica) {
    jdoSetuelEspecifica(this, uelEspecifica);
  }
  public long getIdCargoEspecifica() {
    return jdoGetidCargoEspecifica(this);
  }
  public void setIdCargoEspecifica(long idCargoEspecifica) {
    jdoSetidCargoEspecifica(this, idCargoEspecifica);
  }
  public RegistroCargos getRegistroCargos() {
    return jdoGetregistroCargos(this);
  }
  public void setRegistroCargos(RegistroCargos registroCargos) {
    jdoSetregistroCargos(this, registroCargos);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.CargoEspecifica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CargoEspecifica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CargoEspecifica localCargoEspecifica = new CargoEspecifica();
    localCargoEspecifica.jdoFlags = 1;
    localCargoEspecifica.jdoStateManager = paramStateManager;
    return localCargoEspecifica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CargoEspecifica localCargoEspecifica = new CargoEspecifica();
    localCargoEspecifica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCargoEspecifica.jdoFlags = 1;
    localCargoEspecifica.jdoStateManager = paramStateManager;
    return localCargoEspecifica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCargoEspecifica);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registroCargos);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.uelEspecifica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCargoEspecifica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroCargos = ((RegistroCargos)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.uelEspecifica = ((UelEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CargoEspecifica paramCargoEspecifica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCargoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramCargoEspecifica.anio;
      return;
    case 1:
      if (paramCargoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.idCargoEspecifica = paramCargoEspecifica.idCargoEspecifica;
      return;
    case 2:
      if (paramCargoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramCargoEspecifica.porcentaje;
      return;
    case 3:
      if (paramCargoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.registroCargos = paramCargoEspecifica.registroCargos;
      return;
    case 4:
      if (paramCargoEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.uelEspecifica = paramCargoEspecifica.uelEspecifica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CargoEspecifica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CargoEspecifica localCargoEspecifica = (CargoEspecifica)paramObject;
    if (localCargoEspecifica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCargoEspecifica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CargoEspecificaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CargoEspecificaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CargoEspecificaPK))
      throw new IllegalArgumentException("arg1");
    CargoEspecificaPK localCargoEspecificaPK = (CargoEspecificaPK)paramObject;
    localCargoEspecificaPK.idCargoEspecifica = this.idCargoEspecifica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CargoEspecificaPK))
      throw new IllegalArgumentException("arg1");
    CargoEspecificaPK localCargoEspecificaPK = (CargoEspecificaPK)paramObject;
    this.idCargoEspecifica = localCargoEspecificaPK.idCargoEspecifica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CargoEspecificaPK))
      throw new IllegalArgumentException("arg2");
    CargoEspecificaPK localCargoEspecificaPK = (CargoEspecificaPK)paramObject;
    localCargoEspecificaPK.idCargoEspecifica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CargoEspecificaPK))
      throw new IllegalArgumentException("arg2");
    CargoEspecificaPK localCargoEspecificaPK = (CargoEspecificaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localCargoEspecificaPK.idCargoEspecifica);
  }

  private static final int jdoGetanio(CargoEspecifica paramCargoEspecifica)
  {
    if (paramCargoEspecifica.jdoFlags <= 0)
      return paramCargoEspecifica.anio;
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramCargoEspecifica.anio;
    if (localStateManager.isLoaded(paramCargoEspecifica, jdoInheritedFieldCount + 0))
      return paramCargoEspecifica.anio;
    return localStateManager.getIntField(paramCargoEspecifica, jdoInheritedFieldCount + 0, paramCargoEspecifica.anio);
  }

  private static final void jdoSetanio(CargoEspecifica paramCargoEspecifica, int paramInt)
  {
    if (paramCargoEspecifica.jdoFlags == 0)
    {
      paramCargoEspecifica.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargoEspecifica.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargoEspecifica, jdoInheritedFieldCount + 0, paramCargoEspecifica.anio, paramInt);
  }

  private static final long jdoGetidCargoEspecifica(CargoEspecifica paramCargoEspecifica)
  {
    return paramCargoEspecifica.idCargoEspecifica;
  }

  private static final void jdoSetidCargoEspecifica(CargoEspecifica paramCargoEspecifica, long paramLong)
  {
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargoEspecifica.idCargoEspecifica = paramLong;
      return;
    }
    localStateManager.setLongField(paramCargoEspecifica, jdoInheritedFieldCount + 1, paramCargoEspecifica.idCargoEspecifica, paramLong);
  }

  private static final double jdoGetporcentaje(CargoEspecifica paramCargoEspecifica)
  {
    if (paramCargoEspecifica.jdoFlags <= 0)
      return paramCargoEspecifica.porcentaje;
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramCargoEspecifica.porcentaje;
    if (localStateManager.isLoaded(paramCargoEspecifica, jdoInheritedFieldCount + 2))
      return paramCargoEspecifica.porcentaje;
    return localStateManager.getDoubleField(paramCargoEspecifica, jdoInheritedFieldCount + 2, paramCargoEspecifica.porcentaje);
  }

  private static final void jdoSetporcentaje(CargoEspecifica paramCargoEspecifica, double paramDouble)
  {
    if (paramCargoEspecifica.jdoFlags == 0)
    {
      paramCargoEspecifica.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargoEspecifica.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCargoEspecifica, jdoInheritedFieldCount + 2, paramCargoEspecifica.porcentaje, paramDouble);
  }

  private static final RegistroCargos jdoGetregistroCargos(CargoEspecifica paramCargoEspecifica)
  {
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramCargoEspecifica.registroCargos;
    if (localStateManager.isLoaded(paramCargoEspecifica, jdoInheritedFieldCount + 3))
      return paramCargoEspecifica.registroCargos;
    return (RegistroCargos)localStateManager.getObjectField(paramCargoEspecifica, jdoInheritedFieldCount + 3, paramCargoEspecifica.registroCargos);
  }

  private static final void jdoSetregistroCargos(CargoEspecifica paramCargoEspecifica, RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargoEspecifica.registroCargos = paramRegistroCargos;
      return;
    }
    localStateManager.setObjectField(paramCargoEspecifica, jdoInheritedFieldCount + 3, paramCargoEspecifica.registroCargos, paramRegistroCargos);
  }

  private static final UelEspecifica jdoGetuelEspecifica(CargoEspecifica paramCargoEspecifica)
  {
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramCargoEspecifica.uelEspecifica;
    if (localStateManager.isLoaded(paramCargoEspecifica, jdoInheritedFieldCount + 4))
      return paramCargoEspecifica.uelEspecifica;
    return (UelEspecifica)localStateManager.getObjectField(paramCargoEspecifica, jdoInheritedFieldCount + 4, paramCargoEspecifica.uelEspecifica);
  }

  private static final void jdoSetuelEspecifica(CargoEspecifica paramCargoEspecifica, UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramCargoEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargoEspecifica.uelEspecifica = paramUelEspecifica;
      return;
    }
    localStateManager.setObjectField(paramCargoEspecifica, jdoInheritedFieldCount + 4, paramCargoEspecifica.uelEspecifica, paramUelEspecifica);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}