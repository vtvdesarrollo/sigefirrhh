package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoEspecificaForm.class.getName());
  private ConceptoEspecifica conceptoEspecifica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showConceptoEspecificaByConceptoTipoPersonal;
  private boolean showConceptoEspecificaByAnio;
  private String findSelectTipoPersonalForConceptoTipoPersonal;
  private String findSelectConceptoTipoPersonal;
  private int findAnio;
  private Collection findColTipoPersonalForConceptoTipoPersonal;
  private Collection findColConceptoTipoPersonal;
  private Collection colTipoPersonalForConceptoTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colUnidadEjecutoraForUelEspecifica;
  private Collection colUelEspecifica;
  private String selectTipoPersonalForConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectUnidadEjecutoraForUelEspecifica;
  private String selectUelEspecifica;
  private Object stateResultConceptoEspecificaByConceptoTipoPersonal = null;

  private Object stateResultConceptoEspecificaByAnio = null;

  public Collection getFindColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonalForConceptoTipoPersonal() {
    return this.findSelectTipoPersonalForConceptoTipoPersonal;
  }
  public void setFindSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.findSelectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void findChangeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L)
        this.findColConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByDistribucion(
          idTipoPersonal, "N");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowTipoPersonalForConceptoTipoPersonal() { return this.findColTipoPersonalForConceptoTipoPersonal != null; }

  public String getFindSelectConceptoTipoPersonal() {
    return this.findSelectConceptoTipoPersonal;
  }
  public void setFindSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    this.findSelectConceptoTipoPersonal = valConceptoTipoPersonal;
  }

  public Collection getFindColConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }
  public boolean isFindShowConceptoTipoPersonal() {
    return this.findColConceptoTipoPersonal != null;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectTipoPersonalForConceptoTipoPersonal()
  {
    return this.selectTipoPersonalForConceptoTipoPersonal;
  }
  public void setSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.selectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }
  public void changeTipoPersonalForConceptoTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConceptoTipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.colConceptoTipoPersonal = 
          this.definicionesFacade.findConceptoTipoPersonalByDistribucion(
          idTipoPersonal, "N");
      } else {
        this.selectConceptoTipoPersonal = null;
        this.conceptoEspecifica.setConceptoTipoPersonal(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConceptoTipoPersonal = null;
      this.conceptoEspecifica.setConceptoTipoPersonal(
        null);
    }
  }

  public boolean isShowTipoPersonalForConceptoTipoPersonal() { return this.colTipoPersonalForConceptoTipoPersonal != null; }

  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoEspecifica.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoEspecifica.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public String getSelectUnidadEjecutoraForUelEspecifica() {
    return this.selectUnidadEjecutoraForUelEspecifica;
  }
  public void setSelectUnidadEjecutoraForUelEspecifica(String valUnidadEjecutoraForUelEspecifica) {
    this.selectUnidadEjecutoraForUelEspecifica = valUnidadEjecutoraForUelEspecifica;
  }
  public void changeUnidadEjecutoraForUelEspecifica(ValueChangeEvent event) {
    long idUnidadEjecutora = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colUelEspecifica = null;
      if (idUnidadEjecutora > 0L) {
        this.colUelEspecifica = 
          this.sigecofFacade.findUelEspecificaByUnidadEjecutora(
          idUnidadEjecutora);
      } else {
        this.selectUelEspecifica = null;
        this.conceptoEspecifica.setUelEspecifica(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectUelEspecifica = null;
      this.conceptoEspecifica.setUelEspecifica(
        null);
    }
  }

  public boolean isShowUnidadEjecutoraForUelEspecifica() { return this.colUnidadEjecutoraForUelEspecifica != null; }

  public String getSelectUelEspecifica() {
    return this.selectUelEspecifica;
  }
  public void setSelectUelEspecifica(String valUelEspecifica) {
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    this.conceptoEspecifica.setUelEspecifica(null);
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      if (String.valueOf(uelEspecifica.getIdUelEspecifica()).equals(
        valUelEspecifica)) {
        this.conceptoEspecifica.setUelEspecifica(
          uelEspecifica);
        break;
      }
    }
    this.selectUelEspecifica = valUelEspecifica;
  }
  public boolean isShowUelEspecifica() {
    return this.colUelEspecifica != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoEspecifica getConceptoEspecifica() {
    if (this.conceptoEspecifica == null) {
      this.conceptoEspecifica = new ConceptoEspecifica();
    }
    return this.conceptoEspecifica;
  }

  public ConceptoEspecificaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.findAnio = (new Date().getYear() + 1900);

    refresh();
  }

  public Collection getColTipoPersonalForConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColUnidadEjecutoraForUelEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadEjecutoraForUelEspecifica.iterator();
    UnidadEjecutora unidadEjecutoraForUelEspecifica = null;
    while (iterator.hasNext()) {
      unidadEjecutoraForUelEspecifica = (UnidadEjecutora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadEjecutoraForUelEspecifica.getIdUnidadEjecutora()), 
        unidadEjecutoraForUelEspecifica.toString()));
    }
    return col;
  }

  public Collection getColUelEspecifica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colUnidadEjecutoraForUelEspecifica = 
        this.estructuraFacade.findAllUnidadEjecutora();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConceptoEspecificaByConceptoTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findConceptoEspecificaByConceptoTipoPersonalAndAnio(Long.valueOf(this.findSelectConceptoTipoPersonal).longValue(), this.findAnio);
      this.showConceptoEspecificaByConceptoTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoEspecificaByConceptoTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String findConceptoEspecificaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findConceptoEspecificaByAnio(this.findAnio);
      this.showConceptoEspecificaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConceptoEspecificaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonalForConceptoTipoPersonal = null;
    this.findSelectConceptoTipoPersonal = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowConceptoEspecificaByConceptoTipoPersonal() {
    return this.showConceptoEspecificaByConceptoTipoPersonal;
  }
  public boolean isShowConceptoEspecificaByAnio() {
    return this.showConceptoEspecificaByAnio;
  }

  public String selectConceptoEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConceptoTipoPersonal = null;
    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectUelEspecifica = null;
    this.selectUnidadEjecutoraForUelEspecifica = null;

    long idConceptoEspecifica = 
      Long.parseLong((String)requestParameterMap.get("idConceptoEspecifica"));
    try
    {
      this.conceptoEspecifica = 
        this.sigecofFacade.findConceptoEspecificaById(
        idConceptoEspecifica);
      if (this.conceptoEspecifica.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoEspecifica.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.conceptoEspecifica.getUelEspecifica() != null) {
        this.selectUelEspecifica = 
          String.valueOf(this.conceptoEspecifica.getUelEspecifica().getIdUelEspecifica());
      }

      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
      UelEspecifica uelEspecifica = null;
      UnidadEjecutora unidadEjecutoraForUelEspecifica = null;

      if (this.conceptoEspecifica.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.conceptoEspecifica.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.conceptoEspecifica.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoTipoPersonal = String.valueOf(idTipoPersonalForConceptoTipoPersonal);
        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
        this.colTipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
      if (this.conceptoEspecifica.getUelEspecifica() != null) {
        long idUelEspecifica = 
          this.conceptoEspecifica.getUelEspecifica().getIdUelEspecifica();
        this.selectUelEspecifica = String.valueOf(idUelEspecifica);
        uelEspecifica = this.sigecofFacade.findUelEspecificaById(
          idUelEspecifica);
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutora(
          uelEspecifica.getUnidadEjecutora().getIdUnidadEjecutora());

        long idUnidadEjecutoraForUelEspecifica = 
          this.conceptoEspecifica.getUelEspecifica().getUnidadEjecutora().getIdUnidadEjecutora();
        this.selectUnidadEjecutoraForUelEspecifica = String.valueOf(idUnidadEjecutoraForUelEspecifica);
        unidadEjecutoraForUelEspecifica = 
          this.estructuraFacade.findUnidadEjecutoraById(
          idUnidadEjecutoraForUelEspecifica);
        this.colUnidadEjecutoraForUelEspecifica = 
          this.estructuraFacade.findAllUnidadEjecutora();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.conceptoEspecifica = null;
    this.showConceptoEspecificaByConceptoTipoPersonal = false;
    this.showConceptoEspecificaByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addConceptoEspecifica(
          this.conceptoEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoEspecifica);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateConceptoEspecifica(
          this.conceptoEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoEspecifica);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteConceptoEspecifica(
        this.conceptoEspecifica);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoEspecifica);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.conceptoEspecifica = new ConceptoEspecifica();

    this.selectConceptoTipoPersonal = null;

    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectUelEspecifica = null;

    this.selectUnidadEjecutoraForUelEspecifica = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoEspecifica.setIdConceptoEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.ConceptoEspecifica"));
    this.conceptoEspecifica.setAnio(this.findAnio);
    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.conceptoEspecifica = new ConceptoEspecifica();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}