package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;

public class ConceptoCuentaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoCuenta(ConceptoCuenta conceptoCuenta)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoCuenta conceptoCuentaNew = 
      (ConceptoCuenta)BeanUtils.cloneBean(
      conceptoCuenta);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCuentaNew.getConceptoTipoPersonal() != null) {
      conceptoCuentaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCuentaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (conceptoCuentaNew.getCuentaPresupuesto() != null) {
      conceptoCuentaNew.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        conceptoCuentaNew.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }
    pm.makePersistent(conceptoCuentaNew);
  }

  public void updateConceptoCuenta(ConceptoCuenta conceptoCuenta) throws Exception
  {
    ConceptoCuenta conceptoCuentaModify = 
      findConceptoCuentaById(conceptoCuenta.getIdConceptoCuenta());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoCuenta.getConceptoTipoPersonal() != null) {
      conceptoCuenta.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoCuenta.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (conceptoCuenta.getCuentaPresupuesto() != null) {
      conceptoCuenta.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        conceptoCuenta.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }

    BeanUtils.copyProperties(conceptoCuentaModify, conceptoCuenta);
  }

  public void deleteConceptoCuenta(ConceptoCuenta conceptoCuenta) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoCuenta conceptoCuentaDelete = 
      findConceptoCuentaById(conceptoCuenta.getIdConceptoCuenta());
    pm.deletePersistent(conceptoCuentaDelete);
  }

  public ConceptoCuenta findConceptoCuentaById(long idConceptoCuenta) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoCuenta == pIdConceptoCuenta";
    Query query = pm.newQuery(ConceptoCuenta.class, filter);

    query.declareParameters("long pIdConceptoCuenta");

    parameters.put("pIdConceptoCuenta", new Long(idConceptoCuenta));

    Collection colConceptoCuenta = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoCuenta.iterator();
    return (ConceptoCuenta)iterator.next();
  }

  public Collection findConceptoCuentaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoCuentaExtent = pm.getExtent(
      ConceptoCuenta.class, true);
    Query query = pm.newQuery(conceptoCuentaExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoCuenta.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoCuenta = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCuenta);

    return colConceptoCuenta;
  }

  public Collection findByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cuentaPresupuesto.idCuentaPresupuesto == pIdCuentaPresupuesto";

    Query query = pm.newQuery(ConceptoCuenta.class, filter);

    query.declareParameters("long pIdCuentaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pIdCuentaPresupuesto", new Long(idCuentaPresupuesto));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoCuenta = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoCuenta);

    return colConceptoCuenta;
  }
}