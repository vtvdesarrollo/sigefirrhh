package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;

public class ConceptoCuenta
  implements Serializable, PersistenceCapable
{
  private long idConceptoCuenta;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private CuentaPresupuesto cuentaPresupuesto;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "cuentaPresupuesto", "idConceptoCuenta" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaPresupuesto"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 26, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion() + " - CP- " + jdoGetcuentaPresupuesto(this).getCodPresupuesto();
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public CuentaPresupuesto getCuentaPresupuesto() {
    return jdoGetcuentaPresupuesto(this);
  }
  public void setCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) {
    jdoSetcuentaPresupuesto(this, cuentaPresupuesto);
  }
  public long getIdConceptoCuenta() {
    return jdoGetidConceptoCuenta(this);
  }
  public void setIdConceptoCuenta(long idConceptoCuenta) {
    jdoSetidConceptoCuenta(this, idConceptoCuenta);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.ConceptoCuenta"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoCuenta());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoCuenta localConceptoCuenta = new ConceptoCuenta();
    localConceptoCuenta.jdoFlags = 1;
    localConceptoCuenta.jdoStateManager = paramStateManager;
    return localConceptoCuenta;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoCuenta localConceptoCuenta = new ConceptoCuenta();
    localConceptoCuenta.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoCuenta.jdoFlags = 1;
    localConceptoCuenta.jdoStateManager = paramStateManager;
    return localConceptoCuenta;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cuentaPresupuesto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoCuenta);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaPresupuesto = ((CuentaPresupuesto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoCuenta = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoCuenta paramConceptoCuenta, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoCuenta == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoCuenta.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoCuenta == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaPresupuesto = paramConceptoCuenta.cuentaPresupuesto;
      return;
    case 2:
      if (paramConceptoCuenta == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoCuenta = paramConceptoCuenta.idConceptoCuenta;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoCuenta))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoCuenta localConceptoCuenta = (ConceptoCuenta)paramObject;
    if (localConceptoCuenta.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoCuenta, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoCuentaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoCuentaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCuentaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoCuentaPK localConceptoCuentaPK = (ConceptoCuentaPK)paramObject;
    localConceptoCuentaPK.idConceptoCuenta = this.idConceptoCuenta;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoCuentaPK))
      throw new IllegalArgumentException("arg1");
    ConceptoCuentaPK localConceptoCuentaPK = (ConceptoCuentaPK)paramObject;
    this.idConceptoCuenta = localConceptoCuentaPK.idConceptoCuenta;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCuentaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoCuentaPK localConceptoCuentaPK = (ConceptoCuentaPK)paramObject;
    localConceptoCuentaPK.idConceptoCuenta = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoCuentaPK))
      throw new IllegalArgumentException("arg2");
    ConceptoCuentaPK localConceptoCuentaPK = (ConceptoCuentaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConceptoCuentaPK.idConceptoCuenta);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoCuenta paramConceptoCuenta)
  {
    StateManager localStateManager = paramConceptoCuenta.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCuenta.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoCuenta, jdoInheritedFieldCount + 0))
      return paramConceptoCuenta.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoCuenta, jdoInheritedFieldCount + 0, paramConceptoCuenta.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoCuenta paramConceptoCuenta, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoCuenta.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCuenta.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoCuenta, jdoInheritedFieldCount + 0, paramConceptoCuenta.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final CuentaPresupuesto jdoGetcuentaPresupuesto(ConceptoCuenta paramConceptoCuenta)
  {
    StateManager localStateManager = paramConceptoCuenta.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoCuenta.cuentaPresupuesto;
    if (localStateManager.isLoaded(paramConceptoCuenta, jdoInheritedFieldCount + 1))
      return paramConceptoCuenta.cuentaPresupuesto;
    return (CuentaPresupuesto)localStateManager.getObjectField(paramConceptoCuenta, jdoInheritedFieldCount + 1, paramConceptoCuenta.cuentaPresupuesto);
  }

  private static final void jdoSetcuentaPresupuesto(ConceptoCuenta paramConceptoCuenta, CuentaPresupuesto paramCuentaPresupuesto)
  {
    StateManager localStateManager = paramConceptoCuenta.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCuenta.cuentaPresupuesto = paramCuentaPresupuesto;
      return;
    }
    localStateManager.setObjectField(paramConceptoCuenta, jdoInheritedFieldCount + 1, paramConceptoCuenta.cuentaPresupuesto, paramCuentaPresupuesto);
  }

  private static final long jdoGetidConceptoCuenta(ConceptoCuenta paramConceptoCuenta)
  {
    return paramConceptoCuenta.idConceptoCuenta;
  }

  private static final void jdoSetidConceptoCuenta(ConceptoCuenta paramConceptoCuenta, long paramLong)
  {
    StateManager localStateManager = paramConceptoCuenta.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoCuenta.idConceptoCuenta = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoCuenta, jdoInheritedFieldCount + 2, paramConceptoCuenta.idConceptoCuenta, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}