package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ResumenMensualNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByUnidadAdministradora(long idUnidadAdministradora, long idCategoriaPresupuesto, long idUelEspecifica, int mes, String cerrado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadAdministradora.idUnidadAdministradora == pIdUnidadAdministradora && conceptoTipoPersonal.tipoPersonal.clasificacionPersonal.categoriaPresupuesto.idCategoriaPresupuesto == pIdCategoriaPresupuesto && uelEspecifica.idUelEspecifica == pIdUelEspecifica && encabezadoResumenMensual.mes == pMes && encabezadoResumenMensual.cerrado == pCerrado";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdUnidadAdministradora, long pIdCategoriaPresupuesto, long pIdUelEspecifica, int pMes, String pCerrado");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadAdministradora", new Long(idUnidadAdministradora));
    parameters.put("pIdCategoriaPresupuesto", new Long(idCategoriaPresupuesto));
    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));
    parameters.put("pMes", new Integer(mes));
    parameters.put("pCerrado", cerrado);

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }
}