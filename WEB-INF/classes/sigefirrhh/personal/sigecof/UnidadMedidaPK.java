package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class UnidadMedidaPK
  implements Serializable
{
  public long idUnidadMedida;

  public UnidadMedidaPK()
  {
  }

  public UnidadMedidaPK(long idUnidadMedida)
  {
    this.idUnidadMedida = idUnidadMedida;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UnidadMedidaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UnidadMedidaPK thatPK)
  {
    return 
      this.idUnidadMedida == thatPK.idUnidadMedida;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUnidadMedida)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUnidadMedida);
  }
}