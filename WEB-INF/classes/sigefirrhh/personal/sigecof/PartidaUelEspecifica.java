package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.UnidadEjecutora;

public class PartidaUelEspecifica
  implements Serializable, PersistenceCapable
{
  private long idPartidaUelEspecifica;
  private UelEspecifica uelEspecifica;
  private CuentaPresupuesto cuentaPresupuesto;
  private FuenteFinanciamiento fuenteFinanciamiento;
  private int anio;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "cuentaPresupuesto", "fuenteFinanciamiento", "idPartidaUelEspecifica", "uelEspecifica" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.CuentaPresupuesto"), sunjdo$classForName$("sigefirrhh.personal.sigecof.FuenteFinanciamiento"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.sigecof.UelEspecifica") };
  private static final byte[] jdoFieldFlags = { 21, 26, 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcuentaPresupuesto(this).getCodPresupuesto() + " " + jdoGetuelEspecifica(this).getUnidadEjecutora().getCodUnidadEjecutora() + " " + jdoGetfuenteFinanciamiento(this).getNombre();
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public CuentaPresupuesto getCuentaPresupuesto() {
    return jdoGetcuentaPresupuesto(this);
  }

  public void setCuentaPresupuesto(CuentaPresupuesto cuentaPresupuesto) {
    jdoSetcuentaPresupuesto(this, cuentaPresupuesto);
  }

  public FuenteFinanciamiento getFuenteFinanciamiento() {
    return jdoGetfuenteFinanciamiento(this);
  }

  public void setFuenteFinanciamiento(FuenteFinanciamiento fuenteFinanciamiento) {
    jdoSetfuenteFinanciamiento(this, fuenteFinanciamiento);
  }

  public UelEspecifica getUelEspecifica() {
    return jdoGetuelEspecifica(this);
  }

  public void setUelEspecifica(UelEspecifica uelEspecifica) {
    jdoSetuelEspecifica(this, uelEspecifica);
  }

  public long getIdPartidaUelEspecifica() {
    return jdoGetidPartidaUelEspecifica(this);
  }

  public void setIdPartidaUelEspecifica(long idPartidaUelEspecifica) {
    jdoSetidPartidaUelEspecifica(this, idPartidaUelEspecifica);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.PartidaUelEspecifica"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PartidaUelEspecifica());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PartidaUelEspecifica localPartidaUelEspecifica = new PartidaUelEspecifica();
    localPartidaUelEspecifica.jdoFlags = 1;
    localPartidaUelEspecifica.jdoStateManager = paramStateManager;
    return localPartidaUelEspecifica;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PartidaUelEspecifica localPartidaUelEspecifica = new PartidaUelEspecifica();
    localPartidaUelEspecifica.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPartidaUelEspecifica.jdoFlags = 1;
    localPartidaUelEspecifica.jdoStateManager = paramStateManager;
    return localPartidaUelEspecifica;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cuentaPresupuesto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fuenteFinanciamiento);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPartidaUelEspecifica);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.uelEspecifica);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuentaPresupuesto = ((CuentaPresupuesto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fuenteFinanciamiento = ((FuenteFinanciamiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPartidaUelEspecifica = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.uelEspecifica = ((UelEspecifica)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PartidaUelEspecifica paramPartidaUelEspecifica, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPartidaUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPartidaUelEspecifica.anio;
      return;
    case 1:
      if (paramPartidaUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.cuentaPresupuesto = paramPartidaUelEspecifica.cuentaPresupuesto;
      return;
    case 2:
      if (paramPartidaUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.fuenteFinanciamiento = paramPartidaUelEspecifica.fuenteFinanciamiento;
      return;
    case 3:
      if (paramPartidaUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.idPartidaUelEspecifica = paramPartidaUelEspecifica.idPartidaUelEspecifica;
      return;
    case 4:
      if (paramPartidaUelEspecifica == null)
        throw new IllegalArgumentException("arg1");
      this.uelEspecifica = paramPartidaUelEspecifica.uelEspecifica;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PartidaUelEspecifica))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PartidaUelEspecifica localPartidaUelEspecifica = (PartidaUelEspecifica)paramObject;
    if (localPartidaUelEspecifica.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPartidaUelEspecifica, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PartidaUelEspecificaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PartidaUelEspecificaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PartidaUelEspecificaPK))
      throw new IllegalArgumentException("arg1");
    PartidaUelEspecificaPK localPartidaUelEspecificaPK = (PartidaUelEspecificaPK)paramObject;
    localPartidaUelEspecificaPK.idPartidaUelEspecifica = this.idPartidaUelEspecifica;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PartidaUelEspecificaPK))
      throw new IllegalArgumentException("arg1");
    PartidaUelEspecificaPK localPartidaUelEspecificaPK = (PartidaUelEspecificaPK)paramObject;
    this.idPartidaUelEspecifica = localPartidaUelEspecificaPK.idPartidaUelEspecifica;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PartidaUelEspecificaPK))
      throw new IllegalArgumentException("arg2");
    PartidaUelEspecificaPK localPartidaUelEspecificaPK = (PartidaUelEspecificaPK)paramObject;
    localPartidaUelEspecificaPK.idPartidaUelEspecifica = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PartidaUelEspecificaPK))
      throw new IllegalArgumentException("arg2");
    PartidaUelEspecificaPK localPartidaUelEspecificaPK = (PartidaUelEspecificaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localPartidaUelEspecificaPK.idPartidaUelEspecifica);
  }

  private static final int jdoGetanio(PartidaUelEspecifica paramPartidaUelEspecifica)
  {
    if (paramPartidaUelEspecifica.jdoFlags <= 0)
      return paramPartidaUelEspecifica.anio;
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPartidaUelEspecifica.anio;
    if (localStateManager.isLoaded(paramPartidaUelEspecifica, jdoInheritedFieldCount + 0))
      return paramPartidaUelEspecifica.anio;
    return localStateManager.getIntField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 0, paramPartidaUelEspecifica.anio);
  }

  private static final void jdoSetanio(PartidaUelEspecifica paramPartidaUelEspecifica, int paramInt)
  {
    if (paramPartidaUelEspecifica.jdoFlags == 0)
    {
      paramPartidaUelEspecifica.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPartidaUelEspecifica.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 0, paramPartidaUelEspecifica.anio, paramInt);
  }

  private static final CuentaPresupuesto jdoGetcuentaPresupuesto(PartidaUelEspecifica paramPartidaUelEspecifica)
  {
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPartidaUelEspecifica.cuentaPresupuesto;
    if (localStateManager.isLoaded(paramPartidaUelEspecifica, jdoInheritedFieldCount + 1))
      return paramPartidaUelEspecifica.cuentaPresupuesto;
    return (CuentaPresupuesto)localStateManager.getObjectField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 1, paramPartidaUelEspecifica.cuentaPresupuesto);
  }

  private static final void jdoSetcuentaPresupuesto(PartidaUelEspecifica paramPartidaUelEspecifica, CuentaPresupuesto paramCuentaPresupuesto)
  {
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPartidaUelEspecifica.cuentaPresupuesto = paramCuentaPresupuesto;
      return;
    }
    localStateManager.setObjectField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 1, paramPartidaUelEspecifica.cuentaPresupuesto, paramCuentaPresupuesto);
  }

  private static final FuenteFinanciamiento jdoGetfuenteFinanciamiento(PartidaUelEspecifica paramPartidaUelEspecifica)
  {
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPartidaUelEspecifica.fuenteFinanciamiento;
    if (localStateManager.isLoaded(paramPartidaUelEspecifica, jdoInheritedFieldCount + 2))
      return paramPartidaUelEspecifica.fuenteFinanciamiento;
    return (FuenteFinanciamiento)localStateManager.getObjectField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 2, paramPartidaUelEspecifica.fuenteFinanciamiento);
  }

  private static final void jdoSetfuenteFinanciamiento(PartidaUelEspecifica paramPartidaUelEspecifica, FuenteFinanciamiento paramFuenteFinanciamiento)
  {
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPartidaUelEspecifica.fuenteFinanciamiento = paramFuenteFinanciamiento;
      return;
    }
    localStateManager.setObjectField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 2, paramPartidaUelEspecifica.fuenteFinanciamiento, paramFuenteFinanciamiento);
  }

  private static final long jdoGetidPartidaUelEspecifica(PartidaUelEspecifica paramPartidaUelEspecifica)
  {
    return paramPartidaUelEspecifica.idPartidaUelEspecifica;
  }

  private static final void jdoSetidPartidaUelEspecifica(PartidaUelEspecifica paramPartidaUelEspecifica, long paramLong)
  {
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPartidaUelEspecifica.idPartidaUelEspecifica = paramLong;
      return;
    }
    localStateManager.setLongField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 3, paramPartidaUelEspecifica.idPartidaUelEspecifica, paramLong);
  }

  private static final UelEspecifica jdoGetuelEspecifica(PartidaUelEspecifica paramPartidaUelEspecifica)
  {
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
      return paramPartidaUelEspecifica.uelEspecifica;
    if (localStateManager.isLoaded(paramPartidaUelEspecifica, jdoInheritedFieldCount + 4))
      return paramPartidaUelEspecifica.uelEspecifica;
    return (UelEspecifica)localStateManager.getObjectField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 4, paramPartidaUelEspecifica.uelEspecifica);
  }

  private static final void jdoSetuelEspecifica(PartidaUelEspecifica paramPartidaUelEspecifica, UelEspecifica paramUelEspecifica)
  {
    StateManager localStateManager = paramPartidaUelEspecifica.jdoStateManager;
    if (localStateManager == null)
    {
      paramPartidaUelEspecifica.uelEspecifica = paramUelEspecifica;
      return;
    }
    localStateManager.setObjectField(paramPartidaUelEspecifica, jdoInheritedFieldCount + 4, paramPartidaUelEspecifica.uelEspecifica, paramUelEspecifica);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}