package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.CategoriaPresupuesto;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.personal.procesoNomina.NominaEspecial;

public class ComprobantePresupuesto
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_ETAPA;
  protected static final Map LISTA_NOMINA;
  private long idComprobantePresupuesto;
  private CategoriaPresupuesto categoriaPresupuesto;
  private int numero;
  private int expediente;
  private String descripcion;
  private UnidadAdministradora unidadAdministradora;
  private int anio;
  private int mes;
  private int periodo;
  private Date fechaRegistro;
  private String etapa;
  private String nomina;
  private NominaEspecial nominaEspecial;
  private String estatus;
  private Date fechaProceso;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "categoriaPresupuesto", "descripcion", "estatus", "etapa", "expediente", "fechaProceso", "fechaRegistro", "idComprobantePresupuesto", "mes", "nomina", "nominaEspecial", "numero", "periodo", "unidadAdministradora" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.CategoriaPresupuesto"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.procesoNomina.NominaEspecial"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadAdministradora") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 21, 21, 21, 24, 21, 21, 26, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.ComprobantePresupuesto"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ComprobantePresupuesto());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_ETAPA = 
      new LinkedHashMap();
    LISTA_NOMINA = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("P", "POR PROCESAR");
    LISTA_ESTATUS.put("R", "PROCESADO");
    LISTA_ETAPA.put("C", "COMPROMISO");
    LISTA_ETAPA.put("A", "CAUSADO");
    LISTA_ETAPA.put("P", "PAGADO");
    LISTA_NOMINA.put("O", "ORDINARIA");
    LISTA_NOMINA.put("E", "ESPECIAL");
  }

  public ComprobantePresupuesto()
  {
    jdoSetnumero(this, 0);

    jdoSetexpediente(this, 0);

    jdoSetetapa(this, "C");

    jdoSetnomina(this, "O");

    jdoSetestatus(this, "P");
  }

  public String toString()
  {
    return jdoGetnumero(this) + " " + jdoGetexpediente(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion)
  {
    jdoSetdescripcion(this, descripcion);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus)
  {
    jdoSetestatus(this, estatus);
  }

  public String getEtapa()
  {
    return jdoGetetapa(this);
  }

  public void setEtapa(String etapa)
  {
    jdoSetetapa(this, etapa);
  }

  public int getExpediente()
  {
    return jdoGetexpediente(this);
  }

  public void setExpediente(int expediente)
  {
    jdoSetexpediente(this, expediente);
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public void setFechaProceso(Date fechaProceso)
  {
    jdoSetfechaProceso(this, fechaProceso);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public void setFechaRegistro(Date fechaRegistro)
  {
    jdoSetfechaRegistro(this, fechaRegistro);
  }

  public long getIdComprobantePresupuesto()
  {
    return jdoGetidComprobantePresupuesto(this);
  }

  public void setIdComprobantePresupuesto(long idComprobantePresupuesto)
  {
    jdoSetidComprobantePresupuesto(this, idComprobantePresupuesto);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int mes)
  {
    jdoSetmes(this, mes);
  }

  public int getNumero()
  {
    return jdoGetnumero(this);
  }

  public void setNumero(int numero)
  {
    jdoSetnumero(this, numero);
  }

  public int getPeriodo()
  {
    return jdoGetperiodo(this);
  }

  public void setPeriodo(int periodo)
  {
    jdoSetperiodo(this, periodo);
  }

  public UnidadAdministradora getUnidadAdministradora()
  {
    return jdoGetunidadAdministradora(this);
  }

  public void setUnidadAdministradora(UnidadAdministradora unidadAdministradora)
  {
    jdoSetunidadAdministradora(this, unidadAdministradora);
  }

  public String getNomina()
  {
    return jdoGetnomina(this);
  }

  public void setNomina(String string)
  {
    jdoSetnomina(this, string);
  }

  public NominaEspecial getNominaEspecial()
  {
    return jdoGetnominaEspecial(this);
  }

  public void setNominaEspecial(NominaEspecial especial)
  {
    jdoSetnominaEspecial(this, especial);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ComprobantePresupuesto localComprobantePresupuesto = new ComprobantePresupuesto();
    localComprobantePresupuesto.jdoFlags = 1;
    localComprobantePresupuesto.jdoStateManager = paramStateManager;
    return localComprobantePresupuesto;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ComprobantePresupuesto localComprobantePresupuesto = new ComprobantePresupuesto();
    localComprobantePresupuesto.jdoCopyKeyFieldsFromObjectId(paramObject);
    localComprobantePresupuesto.jdoFlags = 1;
    localComprobantePresupuesto.jdoStateManager = paramStateManager;
    return localComprobantePresupuesto;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.categoriaPresupuesto);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.etapa);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.expediente);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idComprobantePresupuesto);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nomina);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nominaEspecial);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numero);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.periodo);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadAdministradora);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.categoriaPresupuesto = ((CategoriaPresupuesto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.etapa = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.expediente = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idComprobantePresupuesto = localStateManager.replacingLongField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nomina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nominaEspecial = ((NominaEspecial)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.periodo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadAdministradora = ((UnidadAdministradora)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ComprobantePresupuesto paramComprobantePresupuesto, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramComprobantePresupuesto.anio;
      return;
    case 1:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.categoriaPresupuesto = paramComprobantePresupuesto.categoriaPresupuesto;
      return;
    case 2:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramComprobantePresupuesto.descripcion;
      return;
    case 3:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramComprobantePresupuesto.estatus;
      return;
    case 4:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.etapa = paramComprobantePresupuesto.etapa;
      return;
    case 5:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.expediente = paramComprobantePresupuesto.expediente;
      return;
    case 6:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramComprobantePresupuesto.fechaProceso;
      return;
    case 7:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramComprobantePresupuesto.fechaRegistro;
      return;
    case 8:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.idComprobantePresupuesto = paramComprobantePresupuesto.idComprobantePresupuesto;
      return;
    case 9:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramComprobantePresupuesto.mes;
      return;
    case 10:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.nomina = paramComprobantePresupuesto.nomina;
      return;
    case 11:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.nominaEspecial = paramComprobantePresupuesto.nominaEspecial;
      return;
    case 12:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramComprobantePresupuesto.numero;
      return;
    case 13:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.periodo = paramComprobantePresupuesto.periodo;
      return;
    case 14:
      if (paramComprobantePresupuesto == null)
        throw new IllegalArgumentException("arg1");
      this.unidadAdministradora = paramComprobantePresupuesto.unidadAdministradora;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ComprobantePresupuesto))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ComprobantePresupuesto localComprobantePresupuesto = (ComprobantePresupuesto)paramObject;
    if (localComprobantePresupuesto.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localComprobantePresupuesto, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ComprobantePresupuestoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ComprobantePresupuestoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ComprobantePresupuestoPK))
      throw new IllegalArgumentException("arg1");
    ComprobantePresupuestoPK localComprobantePresupuestoPK = (ComprobantePresupuestoPK)paramObject;
    localComprobantePresupuestoPK.idComprobantePresupuesto = this.idComprobantePresupuesto;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ComprobantePresupuestoPK))
      throw new IllegalArgumentException("arg1");
    ComprobantePresupuestoPK localComprobantePresupuestoPK = (ComprobantePresupuestoPK)paramObject;
    this.idComprobantePresupuesto = localComprobantePresupuestoPK.idComprobantePresupuesto;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ComprobantePresupuestoPK))
      throw new IllegalArgumentException("arg2");
    ComprobantePresupuestoPK localComprobantePresupuestoPK = (ComprobantePresupuestoPK)paramObject;
    localComprobantePresupuestoPK.idComprobantePresupuesto = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 8);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ComprobantePresupuestoPK))
      throw new IllegalArgumentException("arg2");
    ComprobantePresupuestoPK localComprobantePresupuestoPK = (ComprobantePresupuestoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 8, localComprobantePresupuestoPK.idComprobantePresupuesto);
  }

  private static final int jdoGetanio(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.anio;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.anio;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 0))
      return paramComprobantePresupuesto.anio;
    return localStateManager.getIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 0, paramComprobantePresupuesto.anio);
  }

  private static final void jdoSetanio(ComprobantePresupuesto paramComprobantePresupuesto, int paramInt)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 0, paramComprobantePresupuesto.anio, paramInt);
  }

  private static final CategoriaPresupuesto jdoGetcategoriaPresupuesto(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.categoriaPresupuesto;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 1))
      return paramComprobantePresupuesto.categoriaPresupuesto;
    return (CategoriaPresupuesto)localStateManager.getObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 1, paramComprobantePresupuesto.categoriaPresupuesto);
  }

  private static final void jdoSetcategoriaPresupuesto(ComprobantePresupuesto paramComprobantePresupuesto, CategoriaPresupuesto paramCategoriaPresupuesto)
  {
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.categoriaPresupuesto = paramCategoriaPresupuesto;
      return;
    }
    localStateManager.setObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 1, paramComprobantePresupuesto.categoriaPresupuesto, paramCategoriaPresupuesto);
  }

  private static final String jdoGetdescripcion(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.descripcion;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.descripcion;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 2))
      return paramComprobantePresupuesto.descripcion;
    return localStateManager.getStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 2, paramComprobantePresupuesto.descripcion);
  }

  private static final void jdoSetdescripcion(ComprobantePresupuesto paramComprobantePresupuesto, String paramString)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 2, paramComprobantePresupuesto.descripcion, paramString);
  }

  private static final String jdoGetestatus(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.estatus;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.estatus;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 3))
      return paramComprobantePresupuesto.estatus;
    return localStateManager.getStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 3, paramComprobantePresupuesto.estatus);
  }

  private static final void jdoSetestatus(ComprobantePresupuesto paramComprobantePresupuesto, String paramString)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 3, paramComprobantePresupuesto.estatus, paramString);
  }

  private static final String jdoGetetapa(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.etapa;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.etapa;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 4))
      return paramComprobantePresupuesto.etapa;
    return localStateManager.getStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 4, paramComprobantePresupuesto.etapa);
  }

  private static final void jdoSetetapa(ComprobantePresupuesto paramComprobantePresupuesto, String paramString)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.etapa = paramString;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.etapa = paramString;
      return;
    }
    localStateManager.setStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 4, paramComprobantePresupuesto.etapa, paramString);
  }

  private static final int jdoGetexpediente(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.expediente;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.expediente;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 5))
      return paramComprobantePresupuesto.expediente;
    return localStateManager.getIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 5, paramComprobantePresupuesto.expediente);
  }

  private static final void jdoSetexpediente(ComprobantePresupuesto paramComprobantePresupuesto, int paramInt)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.expediente = paramInt;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.expediente = paramInt;
      return;
    }
    localStateManager.setIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 5, paramComprobantePresupuesto.expediente, paramInt);
  }

  private static final Date jdoGetfechaProceso(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.fechaProceso;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.fechaProceso;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 6))
      return paramComprobantePresupuesto.fechaProceso;
    return (Date)localStateManager.getObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 6, paramComprobantePresupuesto.fechaProceso);
  }

  private static final void jdoSetfechaProceso(ComprobantePresupuesto paramComprobantePresupuesto, Date paramDate)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 6, paramComprobantePresupuesto.fechaProceso, paramDate);
  }

  private static final Date jdoGetfechaRegistro(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.fechaRegistro;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.fechaRegistro;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 7))
      return paramComprobantePresupuesto.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 7, paramComprobantePresupuesto.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(ComprobantePresupuesto paramComprobantePresupuesto, Date paramDate)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 7, paramComprobantePresupuesto.fechaRegistro, paramDate);
  }

  private static final long jdoGetidComprobantePresupuesto(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    return paramComprobantePresupuesto.idComprobantePresupuesto;
  }

  private static final void jdoSetidComprobantePresupuesto(ComprobantePresupuesto paramComprobantePresupuesto, long paramLong)
  {
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.idComprobantePresupuesto = paramLong;
      return;
    }
    localStateManager.setLongField(paramComprobantePresupuesto, jdoInheritedFieldCount + 8, paramComprobantePresupuesto.idComprobantePresupuesto, paramLong);
  }

  private static final int jdoGetmes(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.mes;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.mes;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 9))
      return paramComprobantePresupuesto.mes;
    return localStateManager.getIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 9, paramComprobantePresupuesto.mes);
  }

  private static final void jdoSetmes(ComprobantePresupuesto paramComprobantePresupuesto, int paramInt)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 9, paramComprobantePresupuesto.mes, paramInt);
  }

  private static final String jdoGetnomina(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.nomina;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.nomina;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 10))
      return paramComprobantePresupuesto.nomina;
    return localStateManager.getStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 10, paramComprobantePresupuesto.nomina);
  }

  private static final void jdoSetnomina(ComprobantePresupuesto paramComprobantePresupuesto, String paramString)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.nomina = paramString;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.nomina = paramString;
      return;
    }
    localStateManager.setStringField(paramComprobantePresupuesto, jdoInheritedFieldCount + 10, paramComprobantePresupuesto.nomina, paramString);
  }

  private static final NominaEspecial jdoGetnominaEspecial(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.nominaEspecial;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 11))
      return paramComprobantePresupuesto.nominaEspecial;
    return (NominaEspecial)localStateManager.getObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 11, paramComprobantePresupuesto.nominaEspecial);
  }

  private static final void jdoSetnominaEspecial(ComprobantePresupuesto paramComprobantePresupuesto, NominaEspecial paramNominaEspecial)
  {
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.nominaEspecial = paramNominaEspecial;
      return;
    }
    localStateManager.setObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 11, paramComprobantePresupuesto.nominaEspecial, paramNominaEspecial);
  }

  private static final int jdoGetnumero(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.numero;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.numero;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 12))
      return paramComprobantePresupuesto.numero;
    return localStateManager.getIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 12, paramComprobantePresupuesto.numero);
  }

  private static final void jdoSetnumero(ComprobantePresupuesto paramComprobantePresupuesto, int paramInt)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.numero = paramInt;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.numero = paramInt;
      return;
    }
    localStateManager.setIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 12, paramComprobantePresupuesto.numero, paramInt);
  }

  private static final int jdoGetperiodo(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    if (paramComprobantePresupuesto.jdoFlags <= 0)
      return paramComprobantePresupuesto.periodo;
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.periodo;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 13))
      return paramComprobantePresupuesto.periodo;
    return localStateManager.getIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 13, paramComprobantePresupuesto.periodo);
  }

  private static final void jdoSetperiodo(ComprobantePresupuesto paramComprobantePresupuesto, int paramInt)
  {
    if (paramComprobantePresupuesto.jdoFlags == 0)
    {
      paramComprobantePresupuesto.periodo = paramInt;
      return;
    }
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.periodo = paramInt;
      return;
    }
    localStateManager.setIntField(paramComprobantePresupuesto, jdoInheritedFieldCount + 13, paramComprobantePresupuesto.periodo, paramInt);
  }

  private static final UnidadAdministradora jdoGetunidadAdministradora(ComprobantePresupuesto paramComprobantePresupuesto)
  {
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
      return paramComprobantePresupuesto.unidadAdministradora;
    if (localStateManager.isLoaded(paramComprobantePresupuesto, jdoInheritedFieldCount + 14))
      return paramComprobantePresupuesto.unidadAdministradora;
    return (UnidadAdministradora)localStateManager.getObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 14, paramComprobantePresupuesto.unidadAdministradora);
  }

  private static final void jdoSetunidadAdministradora(ComprobantePresupuesto paramComprobantePresupuesto, UnidadAdministradora paramUnidadAdministradora)
  {
    StateManager localStateManager = paramComprobantePresupuesto.jdoStateManager;
    if (localStateManager == null)
    {
      paramComprobantePresupuesto.unidadAdministradora = paramUnidadAdministradora;
      return;
    }
    localStateManager.setObjectField(paramComprobantePresupuesto, jdoInheritedFieldCount + 14, paramComprobantePresupuesto.unidadAdministradora, paramUnidadAdministradora);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}