package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class TrabajadorEspecificaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByTrabajadorAndAnio(long idTrabajador, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && anio == pAnio";

    Query query = pm.newQuery(TrabajadorEspecifica.class, filter);

    query.declareParameters("long pIdTrabajador, int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("uelEspecifica.accionEspecifica.codAccionEspecifica  ascending");

    Collection colTrabajadorEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajadorEspecifica);

    return colTrabajadorEspecifica;
  }
}