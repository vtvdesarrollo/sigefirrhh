package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class AccionCentralizadaPK
  implements Serializable
{
  public long idAccionCentralizada;

  public AccionCentralizadaPK()
  {
  }

  public AccionCentralizadaPK(long idAccionCentralizada)
  {
    this.idAccionCentralizada = idAccionCentralizada;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AccionCentralizadaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AccionCentralizadaPK thatPK)
  {
    return 
      this.idAccionCentralizada == thatPK.idAccionCentralizada;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAccionCentralizada)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAccionCentralizada);
  }
}