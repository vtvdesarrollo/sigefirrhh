package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class UelEspecificaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByUnidadEjecutoraAndAnio(long idUnidadEjecutora, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadEjecutora.idUnidadEjecutora == pIdUnidadEjecutora && anio == pAnio";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("long pIdUnidadEjecutora, int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadEjecutora", new Long(idUnidadEjecutora));
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, categoriaPresupuesto ascending ");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }

  public Collection findByCategoriaPresupuestoAndAnio(String categoriaPresupuesto, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "categoriaPresupuesto == pCategoriaPresupuesto && anio == pAnio";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("java.lang.String pCategoriaPresupuesto, int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pCategoriaPresupuesto", new String(categoriaPresupuesto));
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, categoriaPresupuesto ascending ");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }

  public Collection findByUnidadEjecutoraAndProyecto(long idUnidadEjecutora, long idProyecto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadEjecutora.idUnidadEjecutora == pIdUnidadEjecutora && accionEspecifica.proyecto.idProyecto == pIdProyecto";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("long pIdUnidadEjecutora, long pIdProyecto");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadEjecutora", new Long(idUnidadEjecutora));
    parameters.put("pIdProyecto", new Long(idProyecto));

    query.setOrdering("anio ascending, categoriaPresupuesto ascending ");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }

  public Collection findByUnidadEjecutoraAndAccionCentralizada(long idUnidadEjecutora, long idAccionCentralizada) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadEjecutora.idUnidadEjecutora == pIdUnidadEjecutora && accionEspecifica.accionCentralizada.idAccionCentralizada == pIdAccionCentralizada";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("long pIdUnidadEjecutora, long pIdAccionCentralizada");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadEjecutora", new Long(idUnidadEjecutora));
    parameters.put("pIdAccionCentralizada", new Long(idAccionCentralizada));

    query.setOrdering("anio ascending, categoriaPresupuesto ascending ");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }

  public Collection findByUnidadEjecutoraAnioAndTipo(long idUnidadEjecutora, int anio, String tipo) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadEjecutora.idUnidadEjecutora == pIdUnidadEjecutora && anio == pAnio && accionEspecifica.tipo == pTipo";

    Query query = pm.newQuery(UelEspecifica.class, filter);

    query.declareParameters("long pIdUnidadEjecutora, int pAnio, String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadEjecutora", new Long(idUnidadEjecutora));
    parameters.put("pAnio", new Integer(anio));
    parameters.put("pTipo", tipo);

    query.setOrdering("anio ascending, categoriaPresupuesto ascending ");

    Collection colUelEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUelEspecifica);

    return colUelEspecifica;
  }
}