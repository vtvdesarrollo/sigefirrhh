package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.AdministradoraUel;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class TrabajadorEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TrabajadorEspecificaForm.class.getName());
  private TrabajadorEspecifica trabajadorEspecifica;
  private CargoEspecifica cargoEspecifica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colUelEspecifica;
  private Collection colTrabajador;
  private String selectUelEspecifica;
  private String selectTrabajador;
  private String tipo = "P";
  private String selectProyecto;
  private String selectAccionCentralizada;
  private Collection findColTipoPersonal;
  private Collection colProyecto;
  private Collection colAccionCentralizada;
  private int findAnio = 0;

  private Object stateResultTrabajadorEspecificaByTrabajador = null;

  public boolean isShowUelEspecifica()
  {
    return (this.colUelEspecifica != null) && (!this.colUelEspecifica.isEmpty());
  }
  public boolean isShowProyecto() {
    return (this.tipo.equals("P")) && (this.colProyecto != null);
  }
  public boolean isShowAccionCentralizada() {
    return (this.tipo.equals("A")) && (this.colAccionCentralizada != null);
  }

  public void changeProyecto(ValueChangeEvent event) {
    long idProyecto = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colUelEspecifica = null;
      this.selectProyecto = String.valueOf(idProyecto);
      this.selectUelEspecifica = null;
      if (idProyecto != 0L) {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAndProyecto(this.trabajador.getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora(), idProyecto);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeAccionCentralizada(ValueChangeEvent event) {
    long idAccionCentralizada = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colUelEspecifica = null;
      this.selectAccionCentralizada = String.valueOf(idAccionCentralizada);
      this.selectUelEspecifica = null;

      if (idAccionCentralizada != 0L)
      {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAndAccionCentralizada(this.trabajador.getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora(), idAccionCentralizada);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipo(ValueChangeEvent event) {
    String tipo = 
      (String)event.getNewValue();
    try
    {
      log.error(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,tipo" + tipo);
      this.tipo = tipo;

      this.selectAccionCentralizada = null;
      this.selectProyecto = null;
      this.selectUelEspecifica = null;

      findProyectoAccionByAnio();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findProyectoAccionByAnio() { FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.colProyecto = null;
      this.colAccionCentralizada = null;
      this.colUelEspecifica = null;
      log.error(".................................................idUE " + this.trabajador.getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora());
      log.error("--------------------------------------------------anio " + this.findAnio);
      log.error("//////////////////////////////////////////////////tipo " + this.tipo);
      if (this.tipo.equals("P")) {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAnioAndTipo(this.trabajador.getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora(), this.findAnio, "P");
        log.error("*********************************************1-colUEL " + this.colUelEspecifica.size());
      }
      else {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAnioAndTipo(this.trabajador.getDependencia().getAdministradoraUel().getUnidadEjecutora().getIdUnidadEjecutora(), this.findAnio, "A");
        log.error("********************************************2-colUEL " + this.colUelEspecifica.size());
      }

      this.selectAccionCentralizada = null;
      this.selectProyecto = null;
      this.selectUelEspecifica = null;
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null; }

  public Collection getColProyecto() {
    Collection col = new ArrayList();
    Iterator iterator = this.colProyecto.iterator();
    Proyecto proyecto = null;
    while (iterator.hasNext()) {
      proyecto = (Proyecto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(proyecto.getIdProyecto()), 
        proyecto.toString()));
    }
    return col;
  }
  public Collection getColAccionCentralizada() {
    Collection col = new ArrayList();
    Iterator iterator = this.colAccionCentralizada.iterator();
    AccionCentralizada accionCentralizada = null;
    while (iterator.hasNext()) {
      accionCentralizada = (AccionCentralizada)iterator.next();
      col.add(new SelectItem(
        String.valueOf(accionCentralizada.getIdAccionCentralizada()), 
        accionCentralizada.toString()));
    }
    return col;
  }
  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectUelEspecifica()
  {
    return this.selectUelEspecifica;
  }
  public void setSelectUelEspecifica(String valUelEspecifica) {
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    this.trabajadorEspecifica.setUelEspecifica(null);
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      if (String.valueOf(uelEspecifica.getIdUelEspecifica()).equals(
        valUelEspecifica)) {
        this.trabajadorEspecifica.setUelEspecifica(
          uelEspecifica);
        break;
      }
    }
    this.selectUelEspecifica = valUelEspecifica;
  }
  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    this.trabajadorEspecifica.setTrabajador(null);
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador)) {
        this.trabajadorEspecifica.setTrabajador(
          trabajador);
        break;
      }
    }
    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public TrabajadorEspecifica getTrabajadorEspecifica() {
    if (this.trabajadorEspecifica == null) {
      this.trabajadorEspecifica = new TrabajadorEspecifica();
    }
    return this.trabajadorEspecifica;
  }

  public TrabajadorEspecificaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.findAnio = (new Date().getYear() + 1900);
    refresh();
  }

  public Collection getColUelEspecifica()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorEspecificaByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding) {
        this.result = 
          this.sigecofFacade.findTrabajadorEspecificaByTrabajadorAndAnio(
          this.trabajador.getIdTrabajador(), this.findAnio);
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajadorEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUelEspecifica = null;
    this.selectTrabajador = null;

    long idTrabajadorEspecifica = 
      Long.parseLong((String)requestParameterMap.get("idTrabajadorEspecifica"));
    try
    {
      this.trabajadorEspecifica = 
        this.sigecofFacade.findTrabajadorEspecificaById(
        idTrabajadorEspecifica);

      if (this.trabajadorEspecifica.getUelEspecifica() != null) {
        this.selectUelEspecifica = 
          String.valueOf(this.trabajadorEspecifica.getUelEspecifica().getIdUelEspecifica());
      }
      if (this.trabajadorEspecifica.getTrabajador() != null) {
        this.selectTrabajador = 
          String.valueOf(this.trabajadorEspecifica.getTrabajador().getIdTrabajador());
      }
      if (this.trabajadorEspecifica.getTrabajador().getRegistroCargos() != null) {
        this.cargoEspecifica = 
          this.sigecofFacade.findCargoEspecificaByRegistroCargosAndUelEspecifico(this.trabajadorEspecifica.getTrabajador().getRegistroCargos().getIdRegistroCargos(), this.trabajadorEspecifica.getUelEspecifica().getIdUelEspecifica());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.trabajadorEspecifica.setTrabajador(
          this.trabajador);
        this.sigecofFacade.addTrabajadorEspecifica(
          this.trabajadorEspecifica);
        if (this.trabajadorEspecifica.getTrabajador().getRegistroCargos() != null) {
          this.cargoEspecifica.setRegistroCargos(this.trabajadorEspecifica.getTrabajador().getRegistroCargos());
          this.cargoEspecifica.setAnio(this.trabajadorEspecifica.getAnio());
          this.cargoEspecifica.setPorcentaje(this.trabajadorEspecifica.getPorcentaje());
          this.cargoEspecifica.setUelEspecifica(this.trabajadorEspecifica.getUelEspecifica());
          this.sigecofFacade.addCargoEspecifica(this.cargoEspecifica);
        }
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.trabajadorEspecifica, this.trabajador.getPersonal());

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateTrabajadorEspecifica(
          this.trabajadorEspecifica);
        this.cargoEspecifica.setPorcentaje(this.trabajadorEspecifica.getPorcentaje());
        this.sigecofFacade.updateCargoEspecifica(this.cargoEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.trabajadorEspecifica, this.trabajador.getPersonal());

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    } catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Trabajador ya se encuentra asociado a esa categoría presupuestaria\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteTrabajadorEspecifica(
        this.trabajadorEspecifica);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.trabajadorEspecifica, this.trabajador.getPersonal());
      try
      {
        if (this.trabajadorEspecifica.getTrabajador().getRegistroCargos() != null)
          this.sigecofFacade.deleteCargoEspecifica(this.cargoEspecifica);
      }
      catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectUelEspecifica = null;

    this.selectTrabajador = null;

    this.trabajadorEspecifica = new TrabajadorEspecifica();

    this.trabajadorEspecifica.setTrabajador(this.trabajador);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.trabajadorEspecifica.setIdTrabajadorEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.TrabajadorEspecifica"));

    if (this.trabajadorEspecifica.getTrabajador().getRegistroCargos() != null) {
      this.cargoEspecifica = new CargoEspecifica();
      this.cargoEspecifica.setIdCargoEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.CargoEspecifica"));
    }
    findProyectoAccionByAnio();
    this.trabajadorEspecifica.setAnio(this.findAnio);
    this.trabajadorEspecifica.setPorcentaje(100.0D);
    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.trabajadorEspecifica = new TrabajadorEspecifica();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.trabajadorEspecifica = new TrabajadorEspecifica();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
}