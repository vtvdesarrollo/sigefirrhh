package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.CategoriaPresupuesto;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.estructura.AdministradoraUel;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ResumenMensualForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ResumenMensualForm.class.getName());
  private ResumenMensual resumenMensual;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraNoGenFacade estructuraFacade = new EstructuraNoGenFacade();
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showResumenMensualByUnidadAdministradora;
  private String findSelectUnidadAdministradora;
  private Collection findColUnidadAdministradora;
  private String findSelectCategoriaPresupuesto;
  private Collection findColCategoriaPresupuesto;
  private String findSelectAdministradoraUel;
  private Collection findColAdministradoraUel;
  private String findSelectUelEspecifica;
  private Collection findColUelEspecifica;
  private int findAnio;
  private int findMes;
  private Object stateResultResumenMensualByUnidadAdministradora = null;

  public boolean isShowFindUelEspecifica()
  {
    return (this.findColUelEspecifica != null) && (!this.findColUelEspecifica.isEmpty());
  }
  public void changeFindAdministradoraUel(ValueChangeEvent event) {
    long idUnidadEjecutora = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findSelectAdministradoraUel = String.valueOf(idUnidadEjecutora);
      log.error("ID UEL " + idUnidadEjecutora);

      if (idUnidadEjecutora != 0L)
        this.findColUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutora(idUnidadEjecutora);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.findColAdministradoraUel = null;
    }
  }

  public boolean isShowFindAdministradoraUel() { return (this.findColAdministradoraUel != null) && (!this.findColAdministradoraUel.isEmpty()); }

  public void changeFindUnidadAdministradora(ValueChangeEvent event) {
    long idUnidadAdministradora = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findSelectUnidadAdministradora = String.valueOf(idUnidadAdministradora);
      log.error("ID UAD " + idUnidadAdministradora);
      if (idUnidadAdministradora != 0L)
        this.findColAdministradoraUel = this.estructuraFacade.findAdministradoraUelByUnidadAdministradora(idUnidadAdministradora);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.findColAdministradoraUel = null;
    }
  }

  public String getFindSelectUnidadAdministradora() { return this.findSelectUnidadAdministradora; }

  public void setFindSelectUnidadAdministradora(String valUnidadAdministradora) {
    this.findSelectUnidadAdministradora = valUnidadAdministradora;
  }
  public Collection getFindColUnidadAdministradora() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadAdministradora.getIdUnidadAdministradora()), 
        unidadAdministradora.toString()));
    }
    return col;
  }

  public String getFindSelectCategoriaPresupuesto() {
    return this.findSelectCategoriaPresupuesto;
  }
  public void setFindSelectCategoriaPresupuesto(String valCategoriaPresupuesto) {
    this.findSelectCategoriaPresupuesto = valCategoriaPresupuesto;
  }
  public Collection getFindColCategoriaPresupuesto() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCategoriaPresupuesto.iterator();
    CategoriaPresupuesto categoriaPresupuesto = null;
    while (iterator.hasNext()) {
      categoriaPresupuesto = (CategoriaPresupuesto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(categoriaPresupuesto.getIdCategoriaPresupuesto()), 
        categoriaPresupuesto.toString()));
    }
    return col;
  }

  public String getFindSelectAdministradoraUel() {
    return this.findSelectAdministradoraUel;
  }
  public void setFindSelectAdministradoraUel(String valAdministradoraUel) {
    this.findSelectAdministradoraUel = valAdministradoraUel;
  }
  public Collection getFindColAdministradoraUel() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAdministradoraUel.iterator();
    AdministradoraUel administradoraUel = null;
    while (iterator.hasNext()) {
      administradoraUel = (AdministradoraUel)iterator.next();
      col.add(new SelectItem(
        String.valueOf(administradoraUel.getUnidadEjecutora().getIdUnidadEjecutora()), 
        administradoraUel.getUnidadEjecutora().toString()));
    }
    return col;
  }

  public String getFindSelectUelEspecifica() {
    return this.findSelectUelEspecifica;
  }
  public void setFindSelectUelEspecifica(String valUelEspecifica) {
    this.findSelectUelEspecifica = valUelEspecifica;
  }
  public Collection getFindColUelEspecifica() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUelEspecifica.iterator();
    UelEspecifica uelEspecifica = null;
    while (iterator.hasNext()) {
      uelEspecifica = (UelEspecifica)iterator.next();
      col.add(new SelectItem(
        String.valueOf(uelEspecifica.getIdUelEspecifica()), 
        uelEspecifica.toString()));
    }
    return col;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public ResumenMensual getResumenMensual() {
    if (this.resumenMensual == null) {
      this.resumenMensual = new ResumenMensual();
    }
    return this.resumenMensual;
  }

  public ResumenMensualForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.findAnio = (new Date().getYear() + 1900);
    this.findMes = (new Date().getMonth() + 1);
    refresh();
  }

  public void refresh()
  {
    try
    {
      this.findColUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColCategoriaPresupuesto = 
        this.definicionesFacade.findAllCategoriaPresupuesto();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.findColUnidadAdministradora = new ArrayList();
      this.findColCategoriaPresupuesto = new ArrayList();
    }
  }

  public String findResumenMensual()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findResumenMensualByUnidadAdministradora(Long.valueOf(this.findSelectUnidadAdministradora).longValue(), 
        Long.valueOf(this.findSelectCategoriaPresupuesto).longValue(), Long.valueOf(this.findSelectUelEspecifica).longValue(), 
        this.findMes, "N");
      this.showResumenMensualByUnidadAdministradora = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showResumenMensualByUnidadAdministradora)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public boolean isShowResumenMensualByUnidadAdministradora() {
    return this.showResumenMensualByUnidadAdministradora;
  }

  public String selectResumenMensual()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idResumenMensual = 
      Long.parseLong((String)requestParameterMap.get("idResumenMensual"));
    try
    {
      this.resumenMensual = 
        this.sigecofFacade.findResumenMensualById(
        idResumenMensual);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.resumenMensual = null;
    this.showResumenMensualByUnidadAdministradora = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sigecofFacade.addResumenMensual(
          this.resumenMensual);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.resumenMensual);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateResumenMensual(
          this.resumenMensual);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.resumenMensual);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteResumenMensual(
        this.resumenMensual);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.resumenMensual);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.resumenMensual = new ResumenMensual();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public int getFindMes() {
    return this.findMes;
  }
  public void setFindMes(int findMes) {
    this.findMes = findMes;
  }
}