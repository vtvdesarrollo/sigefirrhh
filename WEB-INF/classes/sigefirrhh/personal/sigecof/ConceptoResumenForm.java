package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.AdministradoraUel;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConceptoResumenForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConceptoResumenForm.class.getName());
  private ConceptoResumen conceptoResumen;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int findAnio;
  private int findMes;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private SigecofNoGenFacade sigecofFacade = new SigecofNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colUnidadAdministradora;
  private Collection colTipoPersonalForConceptoTipoPersonal;
  private Collection colConceptoTipoPersonal;
  private Collection colTipoPersonalForFrecuenciaTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colTrabajador;
  private String selectUnidadAdministradora;
  private String selectTipoPersonalForConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectTipoPersonalForFrecuenciaTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Object stateResultConceptoResumenByTrabajador = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectTipoPersonalForConceptoTipoPersonal()
  {
    return this.selectTipoPersonalForConceptoTipoPersonal;
  }
  public void setSelectTipoPersonalForConceptoTipoPersonal(String valTipoPersonalForConceptoTipoPersonal) {
    this.selectTipoPersonalForConceptoTipoPersonal = valTipoPersonalForConceptoTipoPersonal;
  }

  public boolean isShowTipoPersonalForConceptoTipoPersonal()
  {
    return this.colTipoPersonalForConceptoTipoPersonal != null;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String valConceptoTipoPersonal) {
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    this.conceptoResumen.setConceptoTipoPersonal(null);
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()).equals(
        valConceptoTipoPersonal)) {
        this.conceptoResumen.setConceptoTipoPersonal(
          conceptoTipoPersonal);
        break;
      }
    }
    this.selectConceptoTipoPersonal = valConceptoTipoPersonal;
  }
  public boolean isShowConceptoTipoPersonal() {
    return this.colConceptoTipoPersonal != null;
  }
  public String getSelectTipoPersonalForFrecuenciaTipoPersonal() {
    return this.selectTipoPersonalForFrecuenciaTipoPersonal;
  }
  public void setSelectTipoPersonalForFrecuenciaTipoPersonal(String valTipoPersonalForFrecuenciaTipoPersonal) {
    this.selectTipoPersonalForFrecuenciaTipoPersonal = valTipoPersonalForFrecuenciaTipoPersonal;
  }

  public boolean isShowTipoPersonalForFrecuenciaTipoPersonal()
  {
    return this.colTipoPersonalForFrecuenciaTipoPersonal != null;
  }
  public String getSelectFrecuenciaTipoPersonal() {
    return this.selectFrecuenciaTipoPersonal;
  }
  public void setSelectFrecuenciaTipoPersonal(String valFrecuenciaTipoPersonal) {
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    this.conceptoResumen.setFrecuenciaTipoPersonal(null);
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        valFrecuenciaTipoPersonal)) {
        this.conceptoResumen.setFrecuenciaTipoPersonal(
          frecuenciaTipoPersonal);
        break;
      }
    }
    this.selectFrecuenciaTipoPersonal = valFrecuenciaTipoPersonal;
  }
  public boolean isShowFrecuenciaTipoPersonal() {
    return this.colFrecuenciaTipoPersonal != null;
  }
  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    this.conceptoResumen.setTrabajador(null);
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador)) {
        this.conceptoResumen.setTrabajador(
          trabajador);
        break;
      }
    }
    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConceptoResumen getConceptoResumen() {
    if (this.conceptoResumen == null) {
      this.conceptoResumen = new ConceptoResumen();
    }
    return this.conceptoResumen;
  }

  public ConceptoResumenForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.findAnio = (new Date().getYear() + 1900);
    this.findMes = (new Date().getMonth() + 1);
    refresh();
  }

  public Collection getColUnidadAdministradora()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadAdministradora.iterator();
    UnidadAdministradora unidadAdministradora = null;
    while (iterator.hasNext()) {
      unidadAdministradora = (UnidadAdministradora)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadAdministradora.getIdUnidadAdministradora()), 
        unidadAdministradora.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonalForConceptoTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForConceptoTipoPersonal.iterator();
    TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForConceptoTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForConceptoTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForConceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColConceptoTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConceptoTipoPersonal.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonalForFrecuenciaTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonalForFrecuenciaTipoPersonal.iterator();
    TipoPersonal tipoPersonalForFrecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonalForFrecuenciaTipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonalForFrecuenciaTipoPersonal.getIdTipoPersonal()), 
        tipoPersonalForFrecuenciaTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColFrecuenciaTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()), 
        frecuenciaTipoPersonal.toString()));
    }
    return col;
  }

  public Collection getListTipo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoResumen.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCerrado() {
    Collection col = new ArrayList();

    Iterator iterEntry = ConceptoResumen.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colUnidadAdministradora = 
        this.estructuraFacade.findUnidadAdministradoraByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoPersonalForConceptoTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoPersonalForFrecuenciaTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findConceptoResumenByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding) {
        this.result = 
          this.sigecofFacade.findConceptoResumenByTrabajadorAnioMes(
          this.trabajador.getIdTrabajador(), this.findAnio, this.findMes);
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectConceptoResumen()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUnidadAdministradora = null;
    this.selectConceptoTipoPersonal = null;
    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectFrecuenciaTipoPersonal = null;
    this.selectTipoPersonalForFrecuenciaTipoPersonal = null;

    this.selectTrabajador = null;

    long idConceptoResumen = 
      Long.parseLong((String)requestParameterMap.get("idConceptoResumen"));
    try
    {
      this.conceptoResumen = 
        this.sigecofFacade.findConceptoResumenById(
        idConceptoResumen);

      if (this.conceptoResumen.getConceptoTipoPersonal() != null) {
        this.selectConceptoTipoPersonal = 
          String.valueOf(this.conceptoResumen.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
      }
      if (this.conceptoResumen.getFrecuenciaTipoPersonal() != null) {
        this.selectFrecuenciaTipoPersonal = 
          String.valueOf(this.conceptoResumen.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
      }
      if (this.conceptoResumen.getTrabajador() != null) {
        this.selectTrabajador = 
          String.valueOf(this.conceptoResumen.getTrabajador().getIdTrabajador());
      }

      ConceptoTipoPersonal conceptoTipoPersonal = null;
      TipoPersonal tipoPersonalForConceptoTipoPersonal = null;
      FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;
      TipoPersonal tipoPersonalForFrecuenciaTipoPersonal = null;

      if (this.conceptoResumen.getConceptoTipoPersonal() != null) {
        long idConceptoTipoPersonal = 
          this.conceptoResumen.getConceptoTipoPersonal().getIdConceptoTipoPersonal();
        this.selectConceptoTipoPersonal = String.valueOf(idConceptoTipoPersonal);
        conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(
          idConceptoTipoPersonal);
        this.colConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          conceptoTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForConceptoTipoPersonal = 
          this.conceptoResumen.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForConceptoTipoPersonal = String.valueOf(idTipoPersonalForConceptoTipoPersonal);
        tipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForConceptoTipoPersonal);
        this.colTipoPersonalForConceptoTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
      if (this.conceptoResumen.getFrecuenciaTipoPersonal() != null) {
        long idFrecuenciaTipoPersonal = 
          this.conceptoResumen.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal();
        this.selectFrecuenciaTipoPersonal = String.valueOf(idFrecuenciaTipoPersonal);
        frecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalById(
          idFrecuenciaTipoPersonal);
        this.colFrecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(
          frecuenciaTipoPersonal.getTipoPersonal().getIdTipoPersonal());

        long idTipoPersonalForFrecuenciaTipoPersonal = 
          this.conceptoResumen.getFrecuenciaTipoPersonal().getTipoPersonal().getIdTipoPersonal();
        this.selectTipoPersonalForFrecuenciaTipoPersonal = String.valueOf(idTipoPersonalForFrecuenciaTipoPersonal);
        tipoPersonalForFrecuenciaTipoPersonal = 
          this.definicionesFacade.findTipoPersonalById(
          idTipoPersonalForFrecuenciaTipoPersonal);
        this.colTipoPersonalForFrecuenciaTipoPersonal = 
          this.definicionesFacade.findAllTipoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);

      this.colConceptoTipoPersonal = 
        this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
        this.trabajador.getTipoPersonal().getIdTipoPersonal());
      this.colFrecuenciaTipoPersonal = 
        this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(
        this.trabajador.getTipoPersonal().getIdTipoPersonal());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.conceptoResumen.setTrabajador(
          this.trabajador);
        this.conceptoResumen.setUnidadAdministradora(this.trabajador.getDependencia().getAdministradoraUel().getUnidadAdministradora());
        this.sigecofFacade.addConceptoResumen(
          this.conceptoResumen);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.conceptoResumen);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      }
      else {
        this.sigecofFacade.updateConceptoResumen(
          this.conceptoResumen);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.conceptoResumen);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteConceptoResumen(
        this.conceptoResumen);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.conceptoResumen);

      context.addMessage("success_delete", new FacesMessage("Se eliminï¿½ con ï¿½xito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectUnidadAdministradora = null;

    this.selectConceptoTipoPersonal = null;

    this.selectTipoPersonalForConceptoTipoPersonal = null;

    this.selectFrecuenciaTipoPersonal = null;

    this.selectTipoPersonalForFrecuenciaTipoPersonal = null;

    this.selectTrabajador = null;

    this.conceptoResumen = new ConceptoResumen();

    this.conceptoResumen.setTrabajador(this.trabajador);
    this.conceptoResumen.setAnio(this.findAnio);
    this.conceptoResumen.setMes(this.findMes);
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.conceptoResumen.setIdConceptoResumen(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.ConceptoResumen"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.conceptoResumen = new ConceptoResumen();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.conceptoResumen = new ConceptoResumen();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public int getFindMes() {
    return this.findMes;
  }
  public void setFindMes(int findMes) {
    this.findMes = findMes;
  }
}