package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportCatalogoCuentasForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportCatalogoCuentasForm.class.getName());
  private int reportId;
  private String reportName;
  private String reporte;
  private String formato = "1";
  private DefinicionesFacade definicionesFacade;
  private LoginSession login;

  public ReportCatalogoCuentasForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.definicionesFacade = new DefinicionesFacade();
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.reportName = "CuentaPresupuesto";

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportCatalogoCuentasForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      if (this.reporte != null) {
        this.reportName = "";
        if (this.formato.equals("2")) {
          this.reportName = "a_";
        }
        if (this.reporte.equals("1"))
          this.reportName += "CuentaPresupuesto";
        else if (this.reporte.equals("2"))
          this.reportName += "CuentaContable";
      }
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      if (this.reporte != null) {
        this.reportName = "";
        if (this.formato.equals("2")) {
          this.reportName = "a_";
        }
        if (this.reporte.equals("1"))
          this.reportName += "CuentaPresupuesto";
        else if (this.reporte.equals("2")) {
          this.reportName += "CuentaContable";
        }
      }
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_organismo", new Long(this.login.getIdOrganismo()));
      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/sigecof");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public String getReporte() {
    return this.reporte;
  }
  public void setReporte(String reporte) {
    this.reporte = reporte;
  }
}