package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;

public class ConceptoEspecificaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoEspecifica(ConceptoEspecifica conceptoEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoEspecifica conceptoEspecificaNew = 
      (ConceptoEspecifica)BeanUtils.cloneBean(
      conceptoEspecifica);

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoEspecificaNew.getConceptoTipoPersonal() != null) {
      conceptoEspecificaNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoEspecificaNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (conceptoEspecificaNew.getUelEspecifica() != null) {
      conceptoEspecificaNew.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        conceptoEspecificaNew.getUelEspecifica().getIdUelEspecifica()));
    }
    pm.makePersistent(conceptoEspecificaNew);
  }

  public void updateConceptoEspecifica(ConceptoEspecifica conceptoEspecifica) throws Exception
  {
    ConceptoEspecifica conceptoEspecificaModify = 
      findConceptoEspecificaById(conceptoEspecifica.getIdConceptoEspecifica());

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoEspecifica.getConceptoTipoPersonal() != null) {
      conceptoEspecifica.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoEspecifica.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (conceptoEspecifica.getUelEspecifica() != null) {
      conceptoEspecifica.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        conceptoEspecifica.getUelEspecifica().getIdUelEspecifica()));
    }

    BeanUtils.copyProperties(conceptoEspecificaModify, conceptoEspecifica);
  }

  public void deleteConceptoEspecifica(ConceptoEspecifica conceptoEspecifica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoEspecifica conceptoEspecificaDelete = 
      findConceptoEspecificaById(conceptoEspecifica.getIdConceptoEspecifica());
    pm.deletePersistent(conceptoEspecificaDelete);
  }

  public ConceptoEspecifica findConceptoEspecificaById(long idConceptoEspecifica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoEspecifica == pIdConceptoEspecifica";
    Query query = pm.newQuery(ConceptoEspecifica.class, filter);

    query.declareParameters("long pIdConceptoEspecifica");

    parameters.put("pIdConceptoEspecifica", new Long(idConceptoEspecifica));

    Collection colConceptoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoEspecifica.iterator();
    return (ConceptoEspecifica)iterator.next();
  }

  public Collection findConceptoEspecificaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoEspecificaExtent = pm.getExtent(
      ConceptoEspecifica.class, true);
    Query query = pm.newQuery(conceptoEspecificaExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ConceptoEspecifica.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoEspecifica);

    return colConceptoEspecifica;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(ConceptoEspecifica.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoEspecifica);

    return colConceptoEspecifica;
  }
}