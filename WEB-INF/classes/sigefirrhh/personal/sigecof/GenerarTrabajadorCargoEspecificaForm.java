package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarTrabajadorCargoEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarTrabajadorCargoEspecificaForm.class.getName());
  private String selectProceso;
  private String selectTipoPersonal;
  private int mes;
  private int anio;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private String tipoNomina;
  private String descripcion;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade;
  private SigecofNoGenFacade sigecofFacade;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private Collection colUelEspecifica;
  private Collection colUnidadEjecutora;
  private String tipo = "P";
  private String selectProyecto;
  private String selectAccionCentralizada;
  private String selectUnidadEjecutora;
  private Collection findColTipoPersonal;
  private Collection colProyecto;
  private Collection colAccionCentralizada;
  private int findAnio = 0;
  private double porcentaje = 100.0D;
  private String selectUelEspecifica;

  public boolean isShowTipo()
  {
    return (this.selectUnidadEjecutora != null) && (!this.selectUnidadEjecutora.equals("0"));
  }
  public boolean isShowAccionEspecifica() {
    return (this.colUelEspecifica != null) && (!this.colUelEspecifica.isEmpty());
  }
  public boolean isShowProyecto() {
    return (this.tipo.equals("P")) && (this.colProyecto != null);
  }
  public boolean isShowAccionCentralizada() {
    return (this.tipo.equals("A")) && (this.colAccionCentralizada != null);
  }
  public void changeProyecto(ValueChangeEvent event) {
    long idProyecto = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colUelEspecifica = null;
      this.selectProyecto = String.valueOf(idProyecto);
      this.selectUelEspecifica = null;
      if (idProyecto != 0L) {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAndProyecto(Long.valueOf(this.selectUnidadEjecutora).longValue(), idProyecto);
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeAccionCentralizada(ValueChangeEvent event) {
    long idAccionCentralizada = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colUelEspecifica = null;
      this.selectAccionCentralizada = String.valueOf(idAccionCentralizada);
      this.selectUelEspecifica = null;

      if (idAccionCentralizada != 0L)
      {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAndAccionCentralizada(Long.valueOf(this.selectUnidadEjecutora).longValue(), idAccionCentralizada);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeUnidadEjecutora(ValueChangeEvent event) { long idUnidadEjecutora = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colUelEspecifica = null;
      this.selectUnidadEjecutora = String.valueOf(idUnidadEjecutora);
      this.selectUelEspecifica = null;
      this.selectProyecto = null;
      this.selectAccionCentralizada = null;
      this.colProyecto = null;
      this.colAccionCentralizada = null;

      if (idUnidadEjecutora != 0L)
      {
        findProyectoAccionByAnio();
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    } }

  public void changeUelEspecifica(ValueChangeEvent event) {
    long idUelEspecifica = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.show = false;
      log.error("idUel " + idUelEspecifica);
      if (idUelEspecifica != 0L)
      {
        this.show = true;
        log.error("show " + this.show);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipo(ValueChangeEvent event) { String tipo = 
      (String)event.getNewValue();
    try
    {
      this.tipo = tipo;

      this.selectAccionCentralizada = null;
      this.selectProyecto = null;
      this.selectUelEspecifica = null;

      findProyectoAccionByAnio();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    } }

  public String findProyectoAccionByAnio() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.colProyecto = null;
      this.colAccionCentralizada = null;
      log.error("idUE " + Long.valueOf(this.selectUnidadEjecutora).longValue());
      log.error("anio " + this.anio);
      if (this.tipo.equals("P")) {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAnioAndTipo(Long.valueOf(this.selectUnidadEjecutora).longValue(), this.anio, "P");
        log.error("1-colUEL " + this.colUelEspecifica.size());
      }
      else {
        this.colUelEspecifica = this.sigecofFacade.findUelEspecificaByUnidadEjecutoraAnioAndTipo(Long.valueOf(this.selectUnidadEjecutora).longValue(), this.anio, "A");
        log.error("2-colUEL " + this.colUelEspecifica.size());
      }

      this.selectAccionCentralizada = null;
      this.selectProyecto = null;
      this.selectUelEspecifica = null;
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public GenerarTrabajadorCargoEspecificaForm() {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.sigecofFacade = new SigecofNoGenFacade();
    this.estructuraFacade = new EstructuraFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.anio = (new Date().getYear() + 1900);
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
      this.colUnidadEjecutora = this.estructuraFacade.findAllUnidadEjecutora();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      long idUelEspecifica = 0L;
      long idTipoPersonal = 0L;
      long idUnidadEjecutora = 0L;

      if (this.selectUelEspecifica != null) {
        idUelEspecifica = Long.valueOf(this.selectUelEspecifica).longValue();
      }
      if (this.selectTipoPersonal != null) {
        idTipoPersonal = Long.valueOf(this.selectTipoPersonal).longValue();
      }
      if (this.selectUnidadEjecutora != null) {
        idUnidadEjecutora = Long.valueOf(this.selectUnidadEjecutora).longValue();
      }
      this.sigecofFacade.generarTrabajadorCargoEspecifica(idUnidadEjecutora, idUelEspecifica, idTipoPersonal, this.anio, this.porcentaje, this.login.getIdOrganismo());

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_add", new FacesMessage("Se asignó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getColUnidadEjecutora() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colUnidadEjecutora, "sigefirrhh.base.estructura.UnidadEjecutora");
  }
  public Collection getColProyecto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colProyecto, "sigefirrhh.personal.sigecof.Proyecto");
  }
  public Collection getColAccionCentralizada() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colAccionCentralizada, "sigefirrhh.personal.sigecof.AccionCentralizada");
  }
  public Collection getColUelEspecifica() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colUelEspecifica, "sigefirrhh.personal.sigecof.UelEspecifica");
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }

  public String getDescripcion()
  {
    return this.descripcion;
  }

  public String getTipoNomina()
  {
    return this.tipoNomina;
  }

  public void setDescripcion(String string)
  {
    this.descripcion = string;
  }

  public void setTipoNomina(String string)
  {
    this.tipoNomina = string;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getSelectAccionCentralizada() {
    return this.selectAccionCentralizada;
  }
  public void setSelectAccionCentralizada(String selectAccionCentralizada) {
    this.selectAccionCentralizada = selectAccionCentralizada;
  }
  public String getSelectProyecto() {
    return this.selectProyecto;
  }
  public void setSelectProyecto(String selectProyecto) {
    this.selectProyecto = selectProyecto;
  }
  public String getSelectUelEspecifica() {
    return this.selectUelEspecifica;
  }
  public void setSelectUelEspecifica(String selectUelEspecifica) {
    this.selectUelEspecifica = selectUelEspecifica;
  }
  public String getSelectUnidadEjecutora() {
    return this.selectUnidadEjecutora;
  }
  public void setSelectUnidadEjecutora(String selectUnidadEjecutora) {
    this.selectUnidadEjecutora = selectUnidadEjecutora;
  }
  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  public double getPorcentaje() {
    return this.porcentaje;
  }
  public void setPorcentaje(double porcentaje) {
    this.porcentaje = porcentaje;
  }
}