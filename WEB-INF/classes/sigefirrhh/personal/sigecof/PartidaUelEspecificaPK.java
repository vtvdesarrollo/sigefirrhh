package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class PartidaUelEspecificaPK
  implements Serializable
{
  public long idPartidaUelEspecifica;

  public PartidaUelEspecificaPK()
  {
  }

  public PartidaUelEspecificaPK(long idPartidaUelEspecifica)
  {
    this.idPartidaUelEspecifica = idPartidaUelEspecifica;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PartidaUelEspecificaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PartidaUelEspecificaPK thatPK)
  {
    return 
      this.idPartidaUelEspecifica == thatPK.idPartidaUelEspecifica;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPartidaUelEspecifica)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPartidaUelEspecifica);
  }
}