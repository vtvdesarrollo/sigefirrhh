package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class CargoEspecificaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public CargoEspecifica findByRegistroCargosAndUelEspecifica(long idRegistroCargos, long idUelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registroCargos.idRegistroCargos == pIdRegistroCargos && uelEspecifica.idUelEspecifica == pIdUelEspecifica";

    Query query = pm.newQuery(CargoEspecifica.class, filter);

    query.declareParameters("long pIdRegistroCargos, long pIdUelEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistroCargos", new Long(idRegistroCargos));
    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));

    query.setOrdering("uelEspecifica.accionEspecifica.codAccionEspecifica  ascending");

    Collection colCargoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    CargoEspecifica cargoEspecifica = (CargoEspecifica)colCargoEspecifica.iterator().next();
    pm.makeTransient(cargoEspecifica);

    return cargoEspecifica;
  }
}