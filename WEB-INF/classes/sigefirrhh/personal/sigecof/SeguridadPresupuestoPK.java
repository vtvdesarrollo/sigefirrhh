package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class SeguridadPresupuestoPK
  implements Serializable
{
  public long idSeguridadPresupuesto;

  public SeguridadPresupuestoPK()
  {
  }

  public SeguridadPresupuestoPK(long idSeguridadPresupuesto)
  {
    this.idSeguridadPresupuesto = idSeguridadPresupuesto;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadPresupuestoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadPresupuestoPK thatPK)
  {
    return 
      this.idSeguridadPresupuesto == thatPK.idSeguridadPresupuesto;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadPresupuesto)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadPresupuesto);
  }
}