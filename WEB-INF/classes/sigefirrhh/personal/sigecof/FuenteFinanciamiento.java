package sigefirrhh.personal.sigecof;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class FuenteFinanciamiento
  implements Serializable, PersistenceCapable
{
  private long idFuenteFinanciamiento;
  private String codFuenteFinanciamiento;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codFuenteFinanciamiento", "idFuenteFinanciamiento", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public String getCodFuenteFinanciamiento() {
    return jdoGetcodFuenteFinanciamiento(this);
  }

  public void setCodFuenteFinanciamiento(String codFuenteFinanciamiento) {
    jdoSetcodFuenteFinanciamiento(this, codFuenteFinanciamiento);
  }

  public long getIdFuenteFinanciamiento() {
    return jdoGetidFuenteFinanciamiento(this);
  }

  public void setIdFuenteFinanciamiento(long idFuenteFinanciamiento) {
    jdoSetidFuenteFinanciamiento(this, idFuenteFinanciamiento);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.sigecof.FuenteFinanciamiento"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new FuenteFinanciamiento());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    FuenteFinanciamiento localFuenteFinanciamiento = new FuenteFinanciamiento();
    localFuenteFinanciamiento.jdoFlags = 1;
    localFuenteFinanciamiento.jdoStateManager = paramStateManager;
    return localFuenteFinanciamiento;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    FuenteFinanciamiento localFuenteFinanciamiento = new FuenteFinanciamiento();
    localFuenteFinanciamiento.jdoCopyKeyFieldsFromObjectId(paramObject);
    localFuenteFinanciamiento.jdoFlags = 1;
    localFuenteFinanciamiento.jdoStateManager = paramStateManager;
    return localFuenteFinanciamiento;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codFuenteFinanciamiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idFuenteFinanciamiento);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codFuenteFinanciamiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idFuenteFinanciamiento = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(FuenteFinanciamiento paramFuenteFinanciamiento, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramFuenteFinanciamiento == null)
        throw new IllegalArgumentException("arg1");
      this.codFuenteFinanciamiento = paramFuenteFinanciamiento.codFuenteFinanciamiento;
      return;
    case 1:
      if (paramFuenteFinanciamiento == null)
        throw new IllegalArgumentException("arg1");
      this.idFuenteFinanciamiento = paramFuenteFinanciamiento.idFuenteFinanciamiento;
      return;
    case 2:
      if (paramFuenteFinanciamiento == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramFuenteFinanciamiento.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof FuenteFinanciamiento))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    FuenteFinanciamiento localFuenteFinanciamiento = (FuenteFinanciamiento)paramObject;
    if (localFuenteFinanciamiento.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localFuenteFinanciamiento, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new FuenteFinanciamientoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new FuenteFinanciamientoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FuenteFinanciamientoPK))
      throw new IllegalArgumentException("arg1");
    FuenteFinanciamientoPK localFuenteFinanciamientoPK = (FuenteFinanciamientoPK)paramObject;
    localFuenteFinanciamientoPK.idFuenteFinanciamiento = this.idFuenteFinanciamiento;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof FuenteFinanciamientoPK))
      throw new IllegalArgumentException("arg1");
    FuenteFinanciamientoPK localFuenteFinanciamientoPK = (FuenteFinanciamientoPK)paramObject;
    this.idFuenteFinanciamiento = localFuenteFinanciamientoPK.idFuenteFinanciamiento;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FuenteFinanciamientoPK))
      throw new IllegalArgumentException("arg2");
    FuenteFinanciamientoPK localFuenteFinanciamientoPK = (FuenteFinanciamientoPK)paramObject;
    localFuenteFinanciamientoPK.idFuenteFinanciamiento = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof FuenteFinanciamientoPK))
      throw new IllegalArgumentException("arg2");
    FuenteFinanciamientoPK localFuenteFinanciamientoPK = (FuenteFinanciamientoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localFuenteFinanciamientoPK.idFuenteFinanciamiento);
  }

  private static final String jdoGetcodFuenteFinanciamiento(FuenteFinanciamiento paramFuenteFinanciamiento)
  {
    if (paramFuenteFinanciamiento.jdoFlags <= 0)
      return paramFuenteFinanciamiento.codFuenteFinanciamiento;
    StateManager localStateManager = paramFuenteFinanciamiento.jdoStateManager;
    if (localStateManager == null)
      return paramFuenteFinanciamiento.codFuenteFinanciamiento;
    if (localStateManager.isLoaded(paramFuenteFinanciamiento, jdoInheritedFieldCount + 0))
      return paramFuenteFinanciamiento.codFuenteFinanciamiento;
    return localStateManager.getStringField(paramFuenteFinanciamiento, jdoInheritedFieldCount + 0, paramFuenteFinanciamiento.codFuenteFinanciamiento);
  }

  private static final void jdoSetcodFuenteFinanciamiento(FuenteFinanciamiento paramFuenteFinanciamiento, String paramString)
  {
    if (paramFuenteFinanciamiento.jdoFlags == 0)
    {
      paramFuenteFinanciamiento.codFuenteFinanciamiento = paramString;
      return;
    }
    StateManager localStateManager = paramFuenteFinanciamiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramFuenteFinanciamiento.codFuenteFinanciamiento = paramString;
      return;
    }
    localStateManager.setStringField(paramFuenteFinanciamiento, jdoInheritedFieldCount + 0, paramFuenteFinanciamiento.codFuenteFinanciamiento, paramString);
  }

  private static final long jdoGetidFuenteFinanciamiento(FuenteFinanciamiento paramFuenteFinanciamiento)
  {
    return paramFuenteFinanciamiento.idFuenteFinanciamiento;
  }

  private static final void jdoSetidFuenteFinanciamiento(FuenteFinanciamiento paramFuenteFinanciamiento, long paramLong)
  {
    StateManager localStateManager = paramFuenteFinanciamiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramFuenteFinanciamiento.idFuenteFinanciamiento = paramLong;
      return;
    }
    localStateManager.setLongField(paramFuenteFinanciamiento, jdoInheritedFieldCount + 1, paramFuenteFinanciamiento.idFuenteFinanciamiento, paramLong);
  }

  private static final String jdoGetnombre(FuenteFinanciamiento paramFuenteFinanciamiento)
  {
    if (paramFuenteFinanciamiento.jdoFlags <= 0)
      return paramFuenteFinanciamiento.nombre;
    StateManager localStateManager = paramFuenteFinanciamiento.jdoStateManager;
    if (localStateManager == null)
      return paramFuenteFinanciamiento.nombre;
    if (localStateManager.isLoaded(paramFuenteFinanciamiento, jdoInheritedFieldCount + 2))
      return paramFuenteFinanciamiento.nombre;
    return localStateManager.getStringField(paramFuenteFinanciamiento, jdoInheritedFieldCount + 2, paramFuenteFinanciamiento.nombre);
  }

  private static final void jdoSetnombre(FuenteFinanciamiento paramFuenteFinanciamiento, String paramString)
  {
    if (paramFuenteFinanciamiento.jdoFlags == 0)
    {
      paramFuenteFinanciamiento.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramFuenteFinanciamiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramFuenteFinanciamiento.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramFuenteFinanciamiento, jdoInheritedFieldCount + 2, paramFuenteFinanciamiento.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}