package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.estructura.UnidadAdministradora;
import sigefirrhh.base.estructura.UnidadAdministradoraBeanBusiness;
import sigefirrhh.base.estructura.UnidadEjecutora;
import sigefirrhh.base.estructura.UnidadEjecutoraBeanBusiness;

public class ResumenMensualBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addResumenMensual(ResumenMensual resumenMensual)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ResumenMensual resumenMensualNew = 
      (ResumenMensual)BeanUtils.cloneBean(
      resumenMensual);

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (resumenMensualNew.getUnidadAdministradora() != null) {
      resumenMensualNew.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        resumenMensualNew.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    EncabezadoResumenMensualBeanBusiness encabezadoResumenMensualBeanBusiness = new EncabezadoResumenMensualBeanBusiness();

    if (resumenMensualNew.getEncabezadoResumenMensual() != null) {
      resumenMensualNew.setEncabezadoResumenMensual(
        encabezadoResumenMensualBeanBusiness.findEncabezadoResumenMensualById(
        resumenMensualNew.getEncabezadoResumenMensual().getIdEncabezadoResumenMensual()));
    }

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (resumenMensualNew.getUelEspecifica() != null) {
      resumenMensualNew.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        resumenMensualNew.getUelEspecifica().getIdUelEspecifica()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (resumenMensualNew.getCuentaPresupuesto() != null) {
      resumenMensualNew.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        resumenMensualNew.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (resumenMensualNew.getUnidadEjecutora() != null) {
      resumenMensualNew.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        resumenMensualNew.getUnidadEjecutora().getIdUnidadEjecutora()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (resumenMensualNew.getConceptoTipoPersonal() != null) {
      resumenMensualNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        resumenMensualNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FuenteFinanciamientoBeanBusiness fuenteFinanciamientoBeanBusiness = new FuenteFinanciamientoBeanBusiness();

    if (resumenMensualNew.getFuenteFinanciamiento() != null) {
      resumenMensualNew.setFuenteFinanciamiento(
        fuenteFinanciamientoBeanBusiness.findFuenteFinanciamientoById(
        resumenMensualNew.getFuenteFinanciamiento().getIdFuenteFinanciamiento()));
    }
    pm.makePersistent(resumenMensualNew);
  }

  public void updateResumenMensual(ResumenMensual resumenMensual) throws Exception
  {
    ResumenMensual resumenMensualModify = 
      findResumenMensualById(resumenMensual.getIdResumenMensual());

    UnidadAdministradoraBeanBusiness unidadAdministradoraBeanBusiness = new UnidadAdministradoraBeanBusiness();

    if (resumenMensual.getUnidadAdministradora() != null) {
      resumenMensual.setUnidadAdministradora(
        unidadAdministradoraBeanBusiness.findUnidadAdministradoraById(
        resumenMensual.getUnidadAdministradora().getIdUnidadAdministradora()));
    }

    EncabezadoResumenMensualBeanBusiness encabezadoResumenMensualBeanBusiness = new EncabezadoResumenMensualBeanBusiness();

    if (resumenMensual.getEncabezadoResumenMensual() != null) {
      resumenMensual.setEncabezadoResumenMensual(
        encabezadoResumenMensualBeanBusiness.findEncabezadoResumenMensualById(
        resumenMensual.getEncabezadoResumenMensual().getIdEncabezadoResumenMensual()));
    }

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (resumenMensual.getUelEspecifica() != null) {
      resumenMensual.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        resumenMensual.getUelEspecifica().getIdUelEspecifica()));
    }

    CuentaPresupuestoBeanBusiness cuentaPresupuestoBeanBusiness = new CuentaPresupuestoBeanBusiness();

    if (resumenMensual.getCuentaPresupuesto() != null) {
      resumenMensual.setCuentaPresupuesto(
        cuentaPresupuestoBeanBusiness.findCuentaPresupuestoById(
        resumenMensual.getCuentaPresupuesto().getIdCuentaPresupuesto()));
    }

    UnidadEjecutoraBeanBusiness unidadEjecutoraBeanBusiness = new UnidadEjecutoraBeanBusiness();

    if (resumenMensual.getUnidadEjecutora() != null) {
      resumenMensual.setUnidadEjecutora(
        unidadEjecutoraBeanBusiness.findUnidadEjecutoraById(
        resumenMensual.getUnidadEjecutora().getIdUnidadEjecutora()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (resumenMensual.getConceptoTipoPersonal() != null) {
      resumenMensual.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        resumenMensual.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    FuenteFinanciamientoBeanBusiness fuenteFinanciamientoBeanBusiness = new FuenteFinanciamientoBeanBusiness();

    if (resumenMensual.getFuenteFinanciamiento() != null) {
      resumenMensual.setFuenteFinanciamiento(
        fuenteFinanciamientoBeanBusiness.findFuenteFinanciamientoById(
        resumenMensual.getFuenteFinanciamiento().getIdFuenteFinanciamiento()));
    }

    BeanUtils.copyProperties(resumenMensualModify, resumenMensual);
  }

  public void deleteResumenMensual(ResumenMensual resumenMensual) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ResumenMensual resumenMensualDelete = 
      findResumenMensualById(resumenMensual.getIdResumenMensual());
    pm.deletePersistent(resumenMensualDelete);
  }

  public ResumenMensual findResumenMensualById(long idResumenMensual) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idResumenMensual == pIdResumenMensual";
    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdResumenMensual");

    parameters.put("pIdResumenMensual", new Long(idResumenMensual));

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colResumenMensual.iterator();
    return (ResumenMensual)iterator.next();
  }

  public Collection findResumenMensualAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent resumenMensualExtent = pm.getExtent(
      ResumenMensual.class, true);
    Query query = pm.newQuery(resumenMensualExtent);
    query.setOrdering("codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUnidadAdministradora(long idUnidadAdministradora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadAdministradora.idUnidadAdministradora == pIdUnidadAdministradora";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdUnidadAdministradora");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadAdministradora", new Long(idUnidadAdministradora));

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }

  public Collection findByEncabezadoResumenMensual(long idEncabezadoResumenMensual)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "encabezadoResumenMensual.idEncabezadoResumenMensual == pIdEncabezadoResumenMensual";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdEncabezadoResumenMensual");
    HashMap parameters = new HashMap();

    parameters.put("pIdEncabezadoResumenMensual", new Long(idEncabezadoResumenMensual));

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }

  public Collection findByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "uelEspecifica.idUelEspecifica == pIdUelEspecifica";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdUelEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }

  public Collection findByCuentaPresupuesto(long idCuentaPresupuesto)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cuentaPresupuesto.idCuentaPresupuesto == pIdCuentaPresupuesto";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdCuentaPresupuesto");
    HashMap parameters = new HashMap();

    parameters.put("pIdCuentaPresupuesto", new Long(idCuentaPresupuesto));

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }

  public Collection findByUnidadEjecutora(long idUnidadEjecutora)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadEjecutora.idUnidadEjecutora == pIdUnidadEjecutora";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdUnidadEjecutora");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadEjecutora", new Long(idUnidadEjecutora));

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }

  public Collection findByConceptoTipoPersonal(long idConceptoTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }

  public Collection findByFuenteFinanciamiento(long idFuenteFinanciamiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "fuenteFinanciamiento.idFuenteFinanciamiento == pIdFuenteFinanciamiento";

    Query query = pm.newQuery(ResumenMensual.class, filter);

    query.declareParameters("long pIdFuenteFinanciamiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdFuenteFinanciamiento", new Long(idFuenteFinanciamiento));

    query.setOrdering("codConcepto ascending");

    Collection colResumenMensual = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResumenMensual);

    return colResumenMensual;
  }
}