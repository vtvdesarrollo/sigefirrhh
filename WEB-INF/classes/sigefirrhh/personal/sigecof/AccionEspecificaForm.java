package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class AccionEspecificaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AccionEspecificaForm.class.getName());
  private AccionEspecifica accionEspecifica;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private boolean showAccionEspecificaByProyecto;
  private boolean showAccionEspecificaByAccionCentralizada;
  private boolean showAccionEspecificaByCodAccionEspecifica;
  private boolean showAccionEspecificaByDenominacion;
  private boolean showAccionEspecificaByAnio;
  private String findSelectProyecto;
  private String findSelectAccionCentralizada;
  private String findCodAccionEspecifica;
  private String findDenominacion;
  private int findAnio;
  private Collection findColProyecto;
  private Collection findColAccionCentralizada;
  private Collection colProyecto;
  private Collection colAccionCentralizada;
  private String selectProyecto;
  private String selectAccionCentralizada;
  private Object stateResultAccionEspecificaByProyecto = null;

  private Object stateResultAccionEspecificaByAccionCentralizada = null;

  private Object stateResultAccionEspecificaByAnio = null;

  public boolean isShowAccionCentralizada()
  {
    return (this.colAccionCentralizada != null) && (!this.colAccionCentralizada.isEmpty());
  }
  public boolean isShowProyecto() {
    return (this.colProyecto != null) && (!this.colProyecto.isEmpty());
  }

  public String getFindSelectProyecto() {
    return this.findSelectProyecto;
  }
  public void setFindSelectProyecto(String valProyecto) {
    this.findSelectProyecto = valProyecto;
  }

  public Collection getFindColProyecto() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColProyecto.iterator();
    Proyecto proyecto = null;
    while (iterator.hasNext()) {
      proyecto = (Proyecto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(proyecto.getIdProyecto()), 
        proyecto.toString()));
    }
    return col;
  }
  public String getFindSelectAccionCentralizada() {
    return this.findSelectAccionCentralizada;
  }
  public void setFindSelectAccionCentralizada(String valAccionCentralizada) {
    this.findSelectAccionCentralizada = valAccionCentralizada;
  }

  public Collection getFindColAccionCentralizada() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAccionCentralizada.iterator();
    AccionCentralizada accionCentralizada = null;
    while (iterator.hasNext()) {
      accionCentralizada = (AccionCentralizada)iterator.next();
      col.add(new SelectItem(
        String.valueOf(accionCentralizada.getIdAccionCentralizada()), 
        accionCentralizada.toString()));
    }
    return col;
  }
  public String getFindCodAccionEspecifica() {
    return this.findCodAccionEspecifica;
  }
  public void setFindCodAccionEspecifica(String findCodAccionEspecifica) {
    this.findCodAccionEspecifica = findCodAccionEspecifica;
  }
  public String getFindDenominacion() {
    return this.findDenominacion;
  }
  public void setFindDenominacion(String findDenominacion) {
    this.findDenominacion = findDenominacion;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectProyecto()
  {
    return this.selectProyecto;
  }
  public void setSelectProyecto(String valProyecto) {
    Iterator iterator = this.colProyecto.iterator();
    Proyecto proyecto = null;
    this.accionEspecifica.setProyecto(null);
    while (iterator.hasNext()) {
      proyecto = (Proyecto)iterator.next();
      if (String.valueOf(proyecto.getIdProyecto()).equals(
        valProyecto)) {
        this.accionEspecifica.setProyecto(
          proyecto);
        break;
      }
    }
    this.selectProyecto = valProyecto;
  }
  public String getSelectAccionCentralizada() {
    return this.selectAccionCentralizada;
  }
  public void setSelectAccionCentralizada(String valAccionCentralizada) {
    Iterator iterator = this.colAccionCentralizada.iterator();
    AccionCentralizada accionCentralizada = null;
    this.accionEspecifica.setAccionCentralizada(null);
    while (iterator.hasNext()) {
      accionCentralizada = (AccionCentralizada)iterator.next();
      if (String.valueOf(accionCentralizada.getIdAccionCentralizada()).equals(
        valAccionCentralizada)) {
        this.accionEspecifica.setAccionCentralizada(
          accionCentralizada);
        break;
      }
    }
    this.selectAccionCentralizada = valAccionCentralizada;
  }
  public Collection getResult() {
    return this.result;
  }

  public AccionEspecifica getAccionEspecifica() {
    if (this.accionEspecifica == null) {
      this.accionEspecifica = new AccionEspecifica();
    }
    return this.accionEspecifica;
  }

  public AccionEspecificaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    Date fechaActual = new Date();

    this.findAnio = (fechaActual.getYear() + 1900);

    refresh();
  }

  public Collection getListTipo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = AccionEspecifica.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColProyecto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colProyecto.iterator();
    Proyecto proyecto = null;
    while (iterator.hasNext()) {
      proyecto = (Proyecto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(proyecto.getIdProyecto()), 
        proyecto.toString()));
    }
    return col;
  }

  public Collection getColAccionCentralizada()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAccionCentralizada.iterator();
    AccionCentralizada accionCentralizada = null;
    while (iterator.hasNext()) {
      accionCentralizada = (AccionCentralizada)iterator.next();
      col.add(new SelectItem(
        String.valueOf(accionCentralizada.getIdAccionCentralizada()), 
        accionCentralizada.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColProyecto = 
        this.sigecofFacade.findProyectoByAnio(
        this.findAnio, this.login.getOrganismo().getIdOrganismo());
      log.error("anio " + this.findAnio);
      log.error("col Proyecto " + this.findColProyecto.size());

      this.findColAccionCentralizada = 
        this.sigecofFacade.findAccionCentralizadaByAnio(
        this.findAnio, this.login.getOrganismo().getIdOrganismo());
      this.colProyecto = 
        this.sigecofFacade.findProyectoByAnio(
        this.findAnio, this.login.getOrganismo().getIdOrganismo());
      this.colAccionCentralizada = 
        this.sigecofFacade.findAccionCentralizadaByAnio(
        this.findAnio, this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      this.findColProyecto = new ArrayList();
      this.findColAccionCentralizada = new ArrayList();
      this.colProyecto = new ArrayList();
      this.colAccionCentralizada = new ArrayList();
      log.error("Excepcion controlada:", e);
    }
  }

  public String findAccionEspecificaByProyecto()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findAccionEspecificaByProyecto(Long.valueOf(this.findSelectProyecto).longValue());
      this.showAccionEspecificaByProyecto = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAccionEspecificaByProyecto)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectProyecto = null;
    this.findSelectAccionCentralizada = null;
    this.findCodAccionEspecifica = null;
    this.findDenominacion = null;
    this.findAnio = 0;

    return null;
  }

  public String findAccionEspecificaByAccionCentralizada()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sigecofFacade.findAccionEspecificaByAccionCentralizada(Long.valueOf(this.findSelectAccionCentralizada).longValue());
      this.showAccionEspecificaByAccionCentralizada = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAccionEspecificaByAccionCentralizada)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectProyecto = null;
    this.findSelectAccionCentralizada = null;
    this.findCodAccionEspecifica = null;
    this.findDenominacion = null;
    this.findAnio = 0;

    return null;
  }

  public String findAccionEspecificaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try
    {
      this.findColAccionCentralizada = this.sigecofFacade.findAccionCentralizadaByAnio(this.findAnio, this.login.getIdOrganismo());
      this.findColProyecto = this.sigecofFacade.findProyectoByAnio(this.findAnio, this.login.getIdOrganismo());

      if (!this.showAccionEspecificaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectProyecto = null;
    this.findSelectAccionCentralizada = null;
    this.findCodAccionEspecifica = null;
    this.findDenominacion = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowAccionEspecificaByProyecto() {
    return this.showAccionEspecificaByProyecto;
  }
  public boolean isShowAccionEspecificaByAccionCentralizada() {
    return this.showAccionEspecificaByAccionCentralizada;
  }
  public boolean isShowAccionEspecificaByCodAccionEspecifica() {
    return this.showAccionEspecificaByCodAccionEspecifica;
  }
  public boolean isShowAccionEspecificaByDenominacion() {
    return this.showAccionEspecificaByDenominacion;
  }
  public boolean isShowAccionEspecificaByAnio() {
    return this.showAccionEspecificaByAnio;
  }

  public String selectAccionEspecifica()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectProyecto = null;
    this.selectAccionCentralizada = null;

    long idAccionEspecifica = 
      Long.parseLong((String)requestParameterMap.get("idAccionEspecifica"));
    try
    {
      this.accionEspecifica = 
        this.sigecofFacade.findAccionEspecificaById(
        idAccionEspecifica);
      if (this.accionEspecifica.getProyecto() != null) {
        this.selectProyecto = 
          String.valueOf(this.accionEspecifica.getProyecto().getIdProyecto());
      }
      if (this.accionEspecifica.getAccionCentralizada() != null) {
        this.selectAccionCentralizada = 
          String.valueOf(this.accionEspecifica.getAccionCentralizada().getIdAccionCentralizada());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.accionEspecifica = null;
    this.showAccionEspecificaByProyecto = false;
    this.showAccionEspecificaByAccionCentralizada = false;
    this.showAccionEspecificaByCodAccionEspecifica = false;
    this.showAccionEspecificaByDenominacion = false;
    this.showAccionEspecificaByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }
    if (this.accionEspecifica.getTipo().equals("A"))
      this.accionEspecifica.setAnio(this.accionEspecifica.getAccionCentralizada().getAnio());
    else
      this.accionEspecifica.setAnio(this.accionEspecifica.getProyecto().getAnio());
    try
    {
      if (this.adding) {
        this.sigecofFacade.addAccionEspecifica(
          this.accionEspecifica);

        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.accionEspecifica);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sigecofFacade.updateAccionEspecifica(
          this.accionEspecifica);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.accionEspecifica);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sigecofFacade.deleteAccionEspecifica(
        this.accionEspecifica);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.accionEspecifica);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.accionEspecifica = new AccionEspecifica();

    this.selectProyecto = null;

    this.selectAccionCentralizada = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.accionEspecifica.setIdAccionEspecifica(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.sigecof.AccionEspecifica"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.accionEspecifica = new AccionEspecifica();
    return "cancel";
  }

  public boolean isShowProyectoAux()
  {
    try
    {
      return this.accionEspecifica.getTipo().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowAccionCentralizadaAux()
  {
    try {
      return this.accionEspecifica.getTipo().equals("A"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}