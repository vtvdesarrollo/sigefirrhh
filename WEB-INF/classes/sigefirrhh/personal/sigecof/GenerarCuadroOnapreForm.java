package sigefirrhh.personal.sigecof;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.RelacionPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarCuadroOnapreForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarCuadroOnapreForm.class.getName());
  private String selectProceso;
  private String selectRelacionPersonal;
  private int mes;
  private int anio;
  private long idRelacionPersonal;
  private Collection listRelacionPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private SigecofNoGenFacade sigecofNoGenacade;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private RelacionPersonal RelacionPersonal;

  public void changeRelacionPersonal(ValueChangeEvent event)
  {
    this.idRelacionPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.auxShow = false;
    try {
      if (this.idRelacionPersonal != 0L) {
        this.RelacionPersonal = this.definicionesFacade.findRelacionPersonalById(this.idRelacionPersonal);
        this.auxShow = true;
      }

    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public GenerarCuadroOnapreForm() {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.sigecofNoGenacade = new SigecofNoGenFacade();
    this.selectRelacionPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listRelacionPersonal = 
        this.definicionesFacade.findAllRelacionPersonal();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listRelacionPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.sigecofNoGenacade.generarCuadroOnapre(this.idRelacionPersonal, this.anio);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListRelacionPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRelacionPersonal, "sigefirrhh.base.definiciones.RelacionPersonal");
  }

  public String getSelectRelacionPersonal() {
    return this.selectRelacionPersonal;
  }

  public void setSelectRelacionPersonal(String string) {
    this.selectRelacionPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }
}