package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ConceptoEspecificaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByConceptoTipoPersonalAndAnio(long idConceptoTipoPersonal, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "conceptoTipoPersonal.idConceptoTipoPersonal == pIdConceptoTipoPersonal && anio == pAnio";

    Query query = pm.newQuery(ConceptoEspecifica.class, filter);

    query.declareParameters("long pIdConceptoTipoPersonal, int pAnio");

    HashMap parameters = new HashMap();

    parameters.put("pIdConceptoTipoPersonal", new Long(idConceptoTipoPersonal));
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoEspecifica);

    return colConceptoEspecifica;
  }
}