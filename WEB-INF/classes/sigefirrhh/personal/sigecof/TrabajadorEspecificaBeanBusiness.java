package sigefirrhh.personal.sigecof;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class TrabajadorEspecificaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(TrabajadorEspecificaBeanBusiness.class.getName());

  public void addTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    this.log.error("1");
    TrabajadorEspecifica trabajadorEspecificaNew = 
      (TrabajadorEspecifica)BeanUtils.cloneBean(
      trabajadorEspecifica);
    this.log.error("2");

    if (verificarTopeTrabajador(trabajadorEspecifica.getTrabajador().getIdTrabajador(), 
      trabajadorEspecifica.getPorcentaje(), true, 0L, trabajadorEspecifica.getAnio()) > 0.0D) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El porcentaje excede el 100%");
      throw error;
    }
    this.log.error("3");

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (trabajadorEspecificaNew.getUelEspecifica() != null) {
      trabajadorEspecificaNew.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        trabajadorEspecificaNew.getUelEspecifica().getIdUelEspecifica()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (trabajadorEspecificaNew.getTrabajador() != null) {
      trabajadorEspecificaNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        trabajadorEspecificaNew.getTrabajador().getIdTrabajador()));
    }
    this.log.error("4");

    pm.makePersistent(trabajadorEspecificaNew);
  }

  public double verificarTopeTrabajador(long idTrabajador, double porcentaje, boolean adding, long idTrabajadorEspecifica, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    double porcentajeTotal = 0.0D;
    HashMap parameters = new HashMap();
    Query query;
    if (adding) {
      String filter = "trabajador.idTrabajador == pIdTrabajador && anio == pAnio ";
      Query query = pm.newQuery(TrabajadorEspecifica.class, filter);
      query.declareParameters("long pIdTrabajador, int pAnio");
      parameters.put("pIdTrabajador", new Long(idTrabajador));
      parameters.put("pAnio", new Integer(anio));
    }
    else {
      String filter = "trabajador.idTrabajador == pIdTrabajador && anio == pAnio && idTrabajadorEspecifica != pIdTrabajadorEspecifica";
      query = pm.newQuery(TrabajadorEspecifica.class, filter);
      query.declareParameters("long pIdTrabajador, int pAnio ,long pIdTrabajadorEspecifica");
      parameters.put("pIdTrabajador", new Long(idTrabajador));
      parameters.put("pAnio", new Integer(anio));
      parameters.put("pIdTrabajadorEspecifica", new Long(idTrabajadorEspecifica));
    }

    Collection colTrabajadorEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iter = colTrabajadorEspecifica.iterator();

    while (iter.hasNext()) {
      TrabajadorEspecifica trabajadorEspecifica = (TrabajadorEspecifica)iter.next();
      porcentajeTotal += trabajadorEspecifica.getPorcentaje();
    }

    porcentajeTotal += porcentaje;

    if (porcentajeTotal > 100.0D)
      porcentajeTotal -= 100.0D;
    else {
      porcentajeTotal = 0.0D;
    }
    return porcentajeTotal;
  }

  public void updateTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica) throws Exception
  {
    TrabajadorEspecifica trabajadorEspecificaModify = 
      findTrabajadorEspecificaById(trabajadorEspecifica.getIdTrabajadorEspecifica());

    UelEspecificaBeanBusiness uelEspecificaBeanBusiness = new UelEspecificaBeanBusiness();

    if (verificarTopeTrabajador(trabajadorEspecifica.getTrabajador().getIdTrabajador(), 
      trabajadorEspecifica.getPorcentaje(), false, trabajadorEspecifica.getIdTrabajadorEspecifica(), trabajadorEspecifica.getAnio()) > 0.0D) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El porcentaje excede el 100%");
      throw error;
    }

    if (trabajadorEspecifica.getUelEspecifica() != null) {
      trabajadorEspecifica.setUelEspecifica(
        uelEspecificaBeanBusiness.findUelEspecificaById(
        trabajadorEspecifica.getUelEspecifica().getIdUelEspecifica()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (trabajadorEspecifica.getTrabajador() != null) {
      trabajadorEspecifica.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        trabajadorEspecifica.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(trabajadorEspecificaModify, trabajadorEspecifica);
  }

  public void deleteTrabajadorEspecifica(TrabajadorEspecifica trabajadorEspecifica) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    TrabajadorEspecifica trabajadorEspecificaDelete = 
      findTrabajadorEspecificaById(trabajadorEspecifica.getIdTrabajadorEspecifica());
    pm.deletePersistent(trabajadorEspecificaDelete);
  }

  public TrabajadorEspecifica findTrabajadorEspecificaById(long idTrabajadorEspecifica) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTrabajadorEspecifica == pIdTrabajadorEspecifica";
    Query query = pm.newQuery(TrabajadorEspecifica.class, filter);

    query.declareParameters("long pIdTrabajadorEspecifica");

    parameters.put("pIdTrabajadorEspecifica", new Long(idTrabajadorEspecifica));

    Collection colTrabajadorEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTrabajadorEspecifica.iterator();
    return (TrabajadorEspecifica)iterator.next();
  }

  public Collection findTrabajadorEspecificaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent trabajadorEspecificaExtent = pm.getExtent(
      TrabajadorEspecifica.class, true);
    Query query = pm.newQuery(trabajadorEspecificaExtent);
    query.setOrdering("anio ascending, uelEspecifica.accionEspecifica.codAccionEspecifica ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUelEspecifica(long idUelEspecifica)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "uelEspecifica.idUelEspecifica == pIdUelEspecifica";

    Query query = pm.newQuery(TrabajadorEspecifica.class, filter);

    query.declareParameters("long pIdUelEspecifica");
    HashMap parameters = new HashMap();

    parameters.put("pIdUelEspecifica", new Long(idUelEspecifica));

    query.setOrdering("anio ascending, uelEspecifica.accionEspecifica.codAccionEspecifica ascending");

    Collection colTrabajadorEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajadorEspecifica);

    return colTrabajadorEspecifica;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(TrabajadorEspecifica.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, uelEspecifica.accionEspecifica.codAccionEspecifica ascending");

    Collection colTrabajadorEspecifica = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTrabajadorEspecifica);

    return colTrabajadorEspecifica;
  }
}