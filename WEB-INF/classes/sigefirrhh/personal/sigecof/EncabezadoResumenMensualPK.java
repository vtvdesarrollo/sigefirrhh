package sigefirrhh.personal.sigecof;

import java.io.Serializable;

public class EncabezadoResumenMensualPK
  implements Serializable
{
  public long idEncabezadoResumenMensual;

  public EncabezadoResumenMensualPK()
  {
  }

  public EncabezadoResumenMensualPK(long idEncabezadoResumenMensual)
  {
    this.idEncabezadoResumenMensual = idEncabezadoResumenMensual;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EncabezadoResumenMensualPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EncabezadoResumenMensualPK thatPK)
  {
    return 
      this.idEncabezadoResumenMensual == thatPK.idEncabezadoResumenMensual;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEncabezadoResumenMensual)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEncabezadoResumenMensual);
  }
}