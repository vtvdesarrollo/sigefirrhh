package sigefirrhh.personal.conceptos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class RecalculoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RecalculoForm.class.getName());
  private Collection listFrecuenciaTipoPersonal;
  private Collection listTipoPersonal;
  private Collection listConcepto;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private ConceptosFacade conceptosFacade;
  private TipoPersonal tipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private ConceptoTipoPersonal concepto;
  private long idFrecuenciaTipoPersonal;
  private long idConcepto;
  private long idTipoPersonal;
  private String tipoConcepto;
  private boolean show1 = false;
  private boolean show2 = false;
  private double unidades;
  private int registros = 0;

  public RecalculoForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.conceptosFacade = new ConceptosFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.frecuenciaTipoPersonal = null;
    this.idFrecuenciaTipoPersonal = 0L;
    this.listFrecuenciaTipoPersonal = null;

    this.concepto = null;
    this.idConcepto = 0L;
    this.listConcepto = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.listFrecuenciaTipoPersonal = 
          this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(idTipoPersonal);

        this.listConcepto = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByRecalculo(
          idTipoPersonal, "S");

        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.show1 = true;
    this.show2 = false;
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.unidades == 0.0D) {
        this.unidades = 1.0D;
      }
      if (this.conceptosFacade.executeRecalculo(this.concepto.getIdConceptoTipoPersonal(), this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal(), this.tipoConcepto, this.unidades)) {
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.concepto + " " + this.tipoPersonal);

        context.addMessage("success_add", new FacesMessage("Se ejecutó con éxito"));
        this.show2 = false;
        this.show1 = true;
        return null;
      }
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.registros = this.conceptosFacade.findRegistros(this.concepto.getIdConceptoTipoPersonal(), this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal(), this.tipoConcepto);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }
    this.show2 = true;
    this.show1 = false;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    this.show1 = true;
    return null;
  }
  public Collection getListFrecuenciaTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFrecuenciaTipoPersonal, "sigefirrhh.base.definiciones.FrecuenciaTipoPersonal");
  }

  public Collection getListConcepto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConcepto, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }
  public String getIdFrecuenciaTipoPersonal() {
    return String.valueOf(this.idFrecuenciaTipoPersonal);
  }

  public void setIdFrecuenciaTipoPersonal(String l) {
    this.idFrecuenciaTipoPersonal = Long.parseLong(l);

    Iterator iterator = this.listFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;

    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        l)) {
        this.frecuenciaTipoPersonal = frecuenciaTipoPersonal;
        break;
      }
    }
  }

  public String getIdConcepto()
  {
    return String.valueOf(this.idConcepto);
  }

  public void setIdConcepto(String l) {
    this.idConcepto = Long.parseLong(l);
    Iterator iterator = this.listConcepto.iterator();
    ConceptoTipoPersonal concepto = null;

    while (iterator.hasNext()) {
      concepto = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(concepto.getIdConceptoTipoPersonal()).equals(
        l)) {
        this.concepto = concepto;
        break;
      }
    }
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public String getTipoConcepto() {
    return this.tipoConcepto;
  }

  public void setTipoConcepto(String tipoConcepto) {
    this.tipoConcepto = tipoConcepto;
  }

  public boolean isShowFrecuenciaTipoPersonal()
  {
    return (this.listFrecuenciaTipoPersonal != null) && 
      (!this.listFrecuenciaTipoPersonal.isEmpty());
  }

  public boolean isShowConcepto()
  {
    return (this.listConcepto != null) && 
      (!this.listConcepto.isEmpty());
  }
  public boolean isShow1() {
    return this.show1;
  }
  public boolean isShow2() {
    return this.show2;
  }

  public int getRegistros()
  {
    return this.registros;
  }

  public double getUnidades()
  {
    return this.unidades;
  }
  public void setUnidades(double unidades) {
    this.unidades = unidades;
  }
}