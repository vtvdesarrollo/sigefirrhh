package sigefirrhh.personal.conceptos;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.StringTokenizer;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.TrabajadorFacadeExtend;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CargaCajaAhorrosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargaCajaAhorrosForm.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private Collection listFrecuenciaPago;
  private Collection listTipoPersonal;
  private Collection listError;
  private long idFrecuenciaPago;
  private long idTipoPersonal;
  private String grabarConceptos;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private String accion;
  private UploadedFile archivo;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private TrabajadorFacadeExtend trabajadorFacade;
  private Collection temporalConceptos;
  private TipoPersonal tipoPersonal;
  private FrecuenciaPago frecuenciaPago;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal = new FrecuenciaTipoPersonal();
  private CalcularConceptoBeanBusiness calcularConceptoBeanBusines = new CalcularConceptoBeanBusiness();
  private File fileError;
  private boolean hasError;
  private StringBuffer sqlConcepto = new StringBuffer();
  private StringBuffer sqlTrabajador = new StringBuffer();
  private StringBuffer sqlCajaAhorro = new StringBuffer();
  private StringBuffer sqlConceptoVariable = new StringBuffer();
  private StringBuffer sqlActualizar = new StringBuffer();

  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  Statement stExecuteDelete = null;

  private ResultSet rsTrabajador = null;
  private PreparedStatement stTrabajador = null;
  private ResultSet rsCajaAhorro = null;
  private PreparedStatement stCajaAhorro = null;
  private ResultSet rsConceptoVariable = null;
  private PreparedStatement stConceptoVariable = null;
  private long idCargo;
  private String tipo;
  private java.util.Date fechaInicio;
  private java.sql.Date fechaInicioSql;
  int dia = 0;
  int mes = 0;
  int anio = 0;
  String fecha;
  private long idTrabajador;
  private java.util.Date fechaActual = new java.util.Date();
  java.sql.Date fechaActualSql = new java.sql.Date(this.fechaActual.getYear(), this.fechaActual.getMonth(), this.fechaActual.getDate());

  CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

  double montoMasMonto = 0.0D;

  public CargaCajaAhorrosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.trabajadorFacade = new TrabajadorFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    try {
      this.connection.setAutoCommit(false);
      this.connection = Resource.getConnection();

      this.sqlTrabajador.append("select t.id_trabajador, t.id_cargo, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal ");
      this.sqlTrabajador.append(" from trabajador t, turno tu, tipopersonal tp");
      this.sqlTrabajador.append(" where cedula = ? ");
      this.sqlTrabajador.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      this.sqlTrabajador.append(" and t.id_turno = tu.id_turno");
      this.sqlTrabajador.append(" and tp.id_tipo_personal = ? ");
      this.sqlTrabajador.append(" and t.estatus = 'A' ");
      this.stTrabajador = this.connection.prepareStatement(
        this.sqlTrabajador.toString(), 
        1003, 
        1007);

      this.sqlCajaAhorro.append("select id_caja_ahorro ");
      this.sqlCajaAhorro.append(" from cajaahorro");
      this.sqlCajaAhorro.append(" where id_trabajador = ? ");
      this.stCajaAhorro = this.connection.prepareStatement(
        this.sqlCajaAhorro.toString(), 
        1003, 
        1007);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return;
    }
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String cargar()
  {
    log.error("VERSION DEL 0310 ");
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

    actualizarCampos();
    this.listError = new ArrayList();
    try {
      this.stExecute = null;
      this.stExecuteDelete = null;
      this.stExecute = this.connection.createStatement();
      this.stExecuteDelete = this.connection.createStatement();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }

    boolean success = new File(request.getRealPath("temp")).mkdir();

    this.fileError = new File(
      ((ServletContext)context.getExternalContext().getContext())
      .getRealPath("/temp") + "/error" + newFileError(0) + ".txt");
    FileWriter fileErrorWriter = null;
    try {
      this.fileError.createNewFile();
      fileErrorWriter = new FileWriter(this.fileError);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    int i = 0;
    try {
      BufferedReader file = 
        new BufferedReader(
        new InputStreamReader(
        this.archivo.getInputStream()), 250);
      String linea = null;
      StringTokenizer lineaHelper = null;

      int cedula = 0;
      double AporteTrab = 0.0D;
      double AportePatron = 0.0D;
      double AcumTrab = 0.0D;
      double AcumPatron = 0.0D;
      boolean error = false;

      this.hasError = false;
      int j = 0;
      while ((linea = file.readLine()) != null) {
        log.error("PASO 1 ");
        log.error("ACCION " + this.accion);
        j++;

        lineaHelper = new StringTokenizer(linea, "\t");
        error = false;
        i++;

        cedula = Integer.parseInt(lineaHelper.nextToken());

        this.stTrabajador.setInt(1, cedula);
        this.stTrabajador.setLong(2, this.tipoPersonal.getIdTipoPersonal());
        this.rsTrabajador = this.stTrabajador.executeQuery();

        if (!this.rsTrabajador.next()) {
          fileErrorWriter.write(
            "El trabajador con la cédula " + cedula + " no existe para el tipo de personal " + this.tipoPersonal.getNombre() + ". Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idTrabajador = this.rsTrabajador.getLong("id_trabajador");
          this.idCargo = this.rsTrabajador.getLong("id_cargo");
        }

        log.error("PASO 2 ");
        this.rsTrabajador.close();

        AporteTrab = NumberTools.twoDecimal(Double.parseDouble(lineaHelper.nextToken()));
        AportePatron = NumberTools.twoDecimal(Double.parseDouble(lineaHelper.nextToken()));
        AcumTrab = NumberTools.twoDecimal(Double.parseDouble(lineaHelper.nextToken()));
        AcumPatron = NumberTools.twoDecimal(Double.parseDouble(lineaHelper.nextToken()));

        if ((!error) && (AporteTrab != 0.0D)) {
          this.sqlActualizar = new StringBuffer();

          this.stCajaAhorro.setLong(1, this.idTrabajador);
          this.rsCajaAhorro = this.stCajaAhorro.executeQuery();

          if (this.rsCajaAhorro.next()) {
            this.sqlActualizar = new StringBuffer();
            log.error("PASO 1 ");
            this.sqlActualizar.append("update cajaahorro set aporte_trabajador = " + AporteTrab + ", aporte_patron = ");
            this.sqlActualizar.append(AportePatron + ", acumulado_trabajador = " + AcumTrab + ", acumulado_patron=" + AcumPatron);
            this.sqlActualizar.append(" where id_Caja_Ahorro = " + this.rsCajaAhorro.getLong("id_Caja_Ahorro"));

            this.stExecute.addBatch(this.sqlActualizar.toString());
          }
          else
          {
            this.sqlActualizar = new StringBuffer();
            log.error("PASO 2 ");
            this.sqlActualizar.append("insert into CajaAhorro (id_trabajador, ");
            this.sqlActualizar.append("aporte_trabajador,aporte_patron, acumulado_trabajador,acumulado_patron, ");
            this.sqlActualizar.append(" id_Caja_Ahorro) values(");
            this.sqlActualizar.append(this.idTrabajador + ", ");
            this.sqlActualizar.append(AporteTrab + ", ");
            this.sqlActualizar.append(AportePatron + ", " + AcumTrab + ", " + AcumPatron + ",");
            this.sqlActualizar.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.CajaAhorro") + ")");
            this.stExecute.addBatch(this.sqlActualizar.toString());
          }
          this.rsCajaAhorro.close();
        }
      }

      if (this.hasError)
        context.addMessage("success", new FacesMessage("Se cargó con éxito. Pero hay registros con datos incoherentes. Este archivo podrá ser procesado sin embargo, las incoherencias no será procesadas. Baje el archivo a continuación para obtener mas detalle de los errores (use el botón derecho del mouse)."));
      else {
        context.addMessage("success", new FacesMessage("Se cargó con éxito"));
      }
      file.close();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.temporalConceptos = null;
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Archivo con error. Linea " + i, ""));
    }
    try {
      if (fileErrorWriter != null)
        fileErrorWriter.close();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String getUrlError() {
    return this.fileError != null ? "/sigefirrhh/temp/" + this.fileError.getName() : null;
  }

  public static String newFileError(int fileId) {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(10000));
    while (id == fileId);
    return String.valueOf(id);
  }

  public String procesar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    actualizarCampos();
    try
    {
      this.stExecute.executeBatch();
      this.stExecute.close();

      this.stExecuteDelete.close();
      this.stExecute.close();

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.tipoPersonal);

      context.addMessage("success", new FacesMessage("Se procesó con éxito"));
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hay un error en el archivo", ""));
    }
    return null;
  }

  public void actualizarCampos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(
        this.idTipoPersonal);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", 
        new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ha seleccionado un tipo de personal", ""));
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.frecuenciaPago = null;
    this.idFrecuenciaPago = 0L;
    this.listFrecuenciaPago = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.listFrecuenciaPago = 
          this.definicionesFacade.findFrecuenciaPagoByTipoPersonal(
          idTipoPersonal);
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowFrecuenciaPago()
  {
    return (this.listFrecuenciaPago != null) && 
      (!this.listFrecuenciaPago.isEmpty());
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public UploadedFile getArchivo() {
    return this.archivo;
  }

  public String getGrabarConceptos() {
    return this.grabarConceptos;
  }

  public Collection getListFrecuenciaPago() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFrecuenciaPago, "sigefirrhh.base.definiciones.FrecuenciaPago");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public String getIdFrecuenciaPago() {
    return String.valueOf(this.idFrecuenciaPago);
  }

  public void setIdFrecuenciaPago(String l) {
    this.idFrecuenciaPago = Long.parseLong(l);
  }

  public void setArchivo(UploadedFile file) {
    this.archivo = file;
  }

  public void setGrabarConceptos(String string) {
    this.grabarConceptos = string;
  }

  public void setListFrecuenciaPago(Collection collection) {
    this.listFrecuenciaPago = collection;
  }

  public void setListTipoPersonal(Collection collection) {
    this.listTipoPersonal = collection;
  }
  public String getAccion() {
    return this.accion;
  }

  public void setAccion(String string) {
    this.accion = string;
  }

  public Collection getListError() {
    return this.listError;
  }

  public void setListError(Collection collection) {
    this.listError = collection;
  }

  public boolean isHasError() {
    return this.hasError;
  }

  public void setHasError(boolean b) {
    this.hasError = b;
  }
}