package sigefirrhh.personal.conceptos;

import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;

public class ActualizarConceptosBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(ActualizarConceptosBeanBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public int findRegistros(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, String tipo)
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      if (tipo.equals("F")) {
        sql.append("select count(id_concepto_fijo) as cantidad");
        sql.append(" from conceptofijo");
      }
      else {
        sql.append("select count(id_concepto_variable) as cantidad");
        sql.append(" from conceptovariable");
      }
      sql.append(" where ");
      sql.append(" id_concepto_tipo_personal = ? ");
      sql.append(" and id_frecuencia_tipo_personal = ? ");
      sql.append(" and id_trabajador in ( select id_trabajador ");
      sql.append(" from trabajador where estatus = 'A') ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConceptoTipoPersonal);
      stRegistros.setLong(2, idFrecuenciaTipoPersonal);
      rsRegistros = stRegistros.executeQuery();

      int registros = 0;

      if (rsRegistros.next()) {
        registros = rsRegistros.getInt("cantidad");
      }

      return registros;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return 0;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException7) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException8) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException9)
        {
        } 
    }
  }

  public int findRegistrosCambioFrecuencia(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonalActual, long idFrecuenciaTipoPersonalNuevo, String tipo)
    throws Exception
  {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      if (tipo.equals("F")) {
        sql.append("select count(c.id_concepto_fijo) as cantidad");
        sql.append(" from conceptofijo c, trabajador t  ");
      }
      else {
        sql.append("select count(c.id_concepto_variable) as cantidad");
        sql.append(" from conceptovariable c, trabajador t ");
      }
      sql.append(" where ");
      sql.append(" t.id_trabajador = c.id_trabajador ");
      sql.append(" and t.estatus = 'A' ");
      sql.append(" and c.id_concepto_tipo_personal = ? ");
      sql.append(" and c.id_frecuencia_tipo_personal = ? ");
      sql.append(" and c.id_trabajador  not in (select id_trabajador from ");
      sql.append(" conceptofijo  where id_concepto_tipo_personal = ? and ");
      sql.append(" id_frecuencia_tipo_personal = ?) ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConceptoTipoPersonal);
      stRegistros.setLong(2, idFrecuenciaTipoPersonalActual);
      stRegistros.setLong(3, idConceptoTipoPersonal);
      stRegistros.setLong(4, idFrecuenciaTipoPersonalNuevo);
      rsRegistros = stRegistros.executeQuery();

      int registros = 0;

      if (rsRegistros.next()) {
        registros = rsRegistros.getInt("cantidad");
      }

      return registros;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return 0;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException7) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException8) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException9)
        {
        } 
    }
  }

  public boolean executeCambioFrecuencia(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonalActual, long idFrecuenciaTipoPersonalNuevo, int codFrecuenciaPagoActual, int codFrecuenciaPagoNuevo, String tipo)
    throws Exception
  {
    Connection connection = null;
    Statement stExecute = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      stExecute = connection.createStatement();
      sql = new StringBuffer();

      if (tipo.equals("F"))
        sql.append("update conceptofijo ");
      else {
        sql.append("update conceptovariable ");
      }

      sql.append(" set id_frecuencia_tipo_personal = " + idFrecuenciaTipoPersonalNuevo);
      if ((codFrecuenciaPagoActual == 1) || ((codFrecuenciaPagoActual == 2) && (codFrecuenciaPagoNuevo == 3)))
        sql.append(" ,monto = monto/2");
      else if ((codFrecuenciaPagoActual == 3) && ((codFrecuenciaPagoNuevo == 1) || (codFrecuenciaPagoNuevo == 2)))
        sql.append(" ,monto = monto*2");
      else if ((codFrecuenciaPagoActual == 4) && (codFrecuenciaPagoNuevo > 4) && (codFrecuenciaPagoNuevo < 10))
        sql.append(" ,monto = (monto/7)*30");
      else if ((codFrecuenciaPagoActual > 4) && (codFrecuenciaPagoActual < 10) && (codFrecuenciaPagoNuevo == 4)) {
        sql.append(" ,monto = (monto/30)*7");
      }
      sql.append(" where id_frecuencia_tipo_personal = " + idFrecuenciaTipoPersonalActual);
      sql.append(" and id_concepto_tipo_personal = " + idConceptoTipoPersonal);
      sql.append(" and id_trabajador  not in (select id_trabajador from ");
      sql.append(" conceptofijo  where id_concepto_tipo_personal = " + idConceptoTipoPersonal + " and ");
      sql.append(" id_frecuencia_tipo_personal = " + idFrecuenciaTipoPersonalNuevo + ") ");
      sql.append(" and id_trabajador in (select id_trabajador from trabajador where estatus = 'A')");

      stExecute.addBatch(sql.toString());
      stExecute.executeBatch();

      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally
    {
      if (stExecute != null) try {
          stExecute.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        } 
    }
  }

  public int findRegistrosCambioEstatus(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, String estatus, String tipo)
    throws Exception
  {
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      sql = new StringBuffer();

      if (tipo.equals("F")) {
        sql.append("select count(id_concepto_fijo) as cantidad");
        sql.append(" from conceptofijo");
      }
      else {
        sql.append("select count(id_concepto_variable) as cantidad");
        sql.append(" from conceptovariable");
      }
      sql.append(" where ");
      sql.append(" id_concepto_tipo_personal = ? ");
      sql.append(" and id_frecuencia_tipo_personal = ? ");
      sql.append(" and estatus = ? ");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConceptoTipoPersonal);
      stRegistros.setLong(2, idFrecuenciaTipoPersonal);
      stRegistros.setString(3, estatus);
      rsRegistros = stRegistros.executeQuery();

      int registros = 0;

      if (rsRegistros.next()) {
        registros = rsRegistros.getInt("cantidad");
      }
      connection.close(); connection = null;
      connection = null;

      return registros;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return 0;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException7) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException8) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException9)
        {
        } 
    }
  }

  public boolean executeCambioEstatus(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, String estatusActual, String estatusNuevo, String tipo)
    throws Exception
  {
    Connection connection = null;
    Statement stExecute = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      stExecute = connection.createStatement();
      sql = new StringBuffer();

      if (tipo.equals("F"))
        sql.append("update conceptofijo ");
      else {
        sql.append("update conceptovariable ");
      }

      sql.append(" set estatus = '" + estatusNuevo + "'");
      sql.append(" where id_frecuencia_tipo_personal = " + idFrecuenciaTipoPersonal);
      sql.append(" and id_concepto_tipo_personal = " + idConceptoTipoPersonal);
      sql.append(" and estatus = '" + estatusActual + "'");

      stExecute.addBatch(sql.toString());
      stExecute.executeBatch();

      stExecute.close();
      connection.close(); connection = null;
      connection = null;
      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally
    {
      if (stExecute != null) try {
          stExecute.close(); connection = null;
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        }
    }
  }

  public boolean executeActualizarMonto(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, double multiplicar, double dividir, double unidades, String tipo)
    throws Exception
  {
    Connection connection = null;
    Statement stExecute = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      stExecute = connection.createStatement();
      sql = new StringBuffer();

      if (dividir == 0.0D) {
        dividir = 1.0D;
      }

      if (tipo.equals("F"))
        sql.append("update conceptofijo ");
      else {
        sql.append("update conceptovariable ");
      }

      sql.append(" set monto = monto*" + multiplicar + "/" + dividir + ", unidades =" + unidades);
      sql.append(" where id_frecuencia_tipo_personal = " + idFrecuenciaTipoPersonal);
      sql.append(" and id_concepto_tipo_personal = " + idConceptoTipoPersonal);
      sql.append(" and id_trabajador in ( select id_trabajador ");
      sql.append(" from trabajador where estatus = 'A') ");

      stExecute.addBatch(sql.toString());
      stExecute.executeBatch();
      connection.close(); connection = null;
      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally
    {
      if (stExecute != null) try {
          stExecute.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        } 
    }
  }

  public boolean executeEliminarConceptos(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, String tipo) throws Exception {
    Connection connection = null;
    Statement stExecute = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      stExecute = connection.createStatement();
      sql = new StringBuffer();

      if (tipo.equals("F"))
        sql.append("delete from conceptofijo ");
      else {
        sql.append("delete from conceptovariable ");
      }
      sql.append(" where id_frecuencia_tipo_personal = " + idFrecuenciaTipoPersonal);
      sql.append(" and id_concepto_tipo_personal = " + idConceptoTipoPersonal);
      sql.append(" and id_trabajador in ( select id_trabajador ");
      sql.append(" from trabajador where estatus = 'A') ");

      stExecute.addBatch(sql.toString());
      stExecute.executeBatch();
      connection.close(); connection = null;
      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally
    {
      if (stExecute != null) try {
          stExecute.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        } 
    }
  }

  public boolean executeEliminarPrestamos(long idConceptoTipoPersonal) throws Exception { Connection connection = null;
    Statement stExecute = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      stExecute = connection.createStatement();
      sql = new StringBuffer();

      sql.append("delete from prestamo ");
      sql.append(" where id_concepto_tipo_personal = " + idConceptoTipoPersonal);

      stExecute.addBatch(sql.toString());
      stExecute.executeBatch();
      connection.close(); connection = null;
      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally
    {
      if (stExecute != null) try {
          stExecute.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        } 
    } } 
  public boolean executeRecalculo(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, String tipo, double unidades)
    throws Exception
  {
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    Statement stBorrar = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      if (tipo.equals("F")) {
        sql.append("select cf.id_concepto_fijo, t.id_trabajador, cf.unidades, ctp.tipo, fp.cod_frecuencia_pago, ");
        sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
        sql.append(" from trabajador t, conceptofijo cf, conceptotipopersonal ctp, tipopersonal tp,");
        sql.append(" frecuenciapago fp, turno tu, frecuenciatipopersonal ftp");
        sql.append(" where t.id_trabajador = cf.id_trabajador");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
        sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
        sql.append(" and ctp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
        sql.append(" and cf.id_concepto_tipo_personal = ?");
        sql.append(" and cf.id_frecuencia_tipo_personal = ?");
        sql.append(" and t.estatus = 'A'");
      }
      else
      {
        sql.append("select cv.id_concepto_variable, t.id_trabajador, cv.unidades, ctp.tipo, fp.cod_frecuencia_pago, ");
        sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
        sql.append(" from trabajador t, conceptovariable cv, conceptotipopersonal ctp, tipopersonal tp,");
        sql.append(" frecuenciapago fp, turno tu, frecuenciatipopersonal ftp");
        sql.append(" where t.id_trabajador = cv.id_trabajador");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and cv.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
        sql.append(" and cv.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
        sql.append(" and ctp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
        sql.append(" and cv.id_concepto_tipo_personal = ?");
        sql.append(" and cv.id_frecuencia_tipo_personal = ?");
        sql.append(" and t.estatus = 'A'");
      }

      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConceptoTipoPersonal);
      stRegistros.setLong(2, idFrecuenciaTipoPersonal);
      rsRegistros = stRegistros.executeQuery();

      double monto = 0.0D;

      stBorrar = connection.createStatement();
      while (rsRegistros.next())
      {
        sql = new StringBuffer();
        if (rsRegistros.getString("tipo").equals("I"))
          monto = calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonal, rsRegistros.getLong("id_trabajador"), rsRegistros.getDouble("unidades"), rsRegistros.getString("tipo"), rsRegistros.getInt("cod_frecuencia_pago"), rsRegistros.getDouble("jornada_diaria"), rsRegistros.getDouble("jornada_semanal"), rsRegistros.getString("formula_integral"), rsRegistros.getString("formula_semanal"), rsRegistros.getLong("id_cargo"), rsRegistros.getDouble("valor"), rsRegistros.getDouble("tope_minimo"), rsRegistros.getDouble("tope_maximo"));
        else {
          monto = calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonal, rsRegistros.getLong("id_trabajador"), unidades, rsRegistros.getString("tipo"), rsRegistros.getInt("cod_frecuencia_pago"), rsRegistros.getDouble("jornada_diaria"), rsRegistros.getDouble("jornada_semanal"), rsRegistros.getString("formula_integral"), rsRegistros.getString("formula_semanal"), rsRegistros.getLong("id_cargo"), rsRegistros.getDouble("valor"), rsRegistros.getDouble("tope_minimo"), rsRegistros.getDouble("tope_maximo"));
        }
        if (tipo.equals("F")) {
          sql.append("update conceptofijo set monto_anterior = monto where monto <>  " + monto + " and  id_concepto_fijo = " + rsRegistros.getLong("id_concepto_fijo") + ";");
          sql.append("update conceptofijo set monto = " + monto + " where  id_concepto_fijo = " + rsRegistros.getLong("id_concepto_fijo"));
        } else {
          sql.append("update conceptovariable set monto = " + monto + " where id_concepto_variable = " + rsRegistros.getLong("id_concepto_variable"));
        }

        stBorrar.addBatch(sql.toString());
      }

      stBorrar.executeBatch();
      connection.close(); connection = null;

      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException9) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException10) {
        } if (stBorrar != null) try {
          stBorrar.close();
        } catch (Exception localException11) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException12)
        {
        } 
    }
  }

  public boolean executeRecalculoProyectado(long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, double unidades) throws Exception {
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    Statement stBorrar = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      sql = new StringBuffer();

      sql.append("select cf.id_concepto_fijo, t.id_trabajador, cf.unidades, ctp.tipo, fp.cod_frecuencia_pago, ");
      sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
      sql.append(" from trabajador t, conceptofijo cf, conceptotipopersonal ctp, tipopersonal tp,");
      sql.append(" frecuenciapago fp, turno tu, frecuenciatipopersonal ftp");
      sql.append(" where t.id_trabajador = cf.id_trabajador");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ctp.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and cf.id_concepto_tipo_personal = ?");
      sql.append(" and cf.id_frecuencia_tipo_personal = ?");
      sql.append(" and t.estatus = 'A'");

      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConceptoTipoPersonal);
      stRegistros.setLong(2, idFrecuenciaTipoPersonal);
      rsRegistros = stRegistros.executeQuery();

      double monto = 0.0D;

      stBorrar = connection.createStatement();
      while (rsRegistros.next())
      {
        sql = new StringBuffer();

        monto = calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonal, rsRegistros.getLong("id_trabajador"), unidades, rsRegistros.getString("tipo"), rsRegistros.getInt("cod_frecuencia_pago"), rsRegistros.getDouble("jornada_diaria"), rsRegistros.getDouble("jornada_semanal"), rsRegistros.getString("formula_integral"), rsRegistros.getString("formula_semanal"), rsRegistros.getLong("id_cargo"), rsRegistros.getDouble("valor"), rsRegistros.getDouble("tope_minimo"), rsRegistros.getDouble("tope_maximo"));

        sql.append("update conceptofijo set monto_proyectado = " + monto + " where  id_concepto_fijo = " + rsRegistros.getLong("id_concepto_fijo"));

        stBorrar.addBatch(sql.toString());
      }

      stBorrar.executeBatch();
      connection.close(); connection = null;

      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException9) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException10) {
        } if (stBorrar != null) try {
          stBorrar.close();
        } catch (Exception localException11) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException12)
        {
        } 
    }
  }

  public boolean executeRecalculoPorAsociado(long idConceptoTipoPersonal, String tipo) throws Exception {
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    Statement stBorrar = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql = new StringBuffer();

      if (tipo.equals("F")) {
        sql.append("select cf.id_concepto_fijo, t.id_trabajador, cf.unidades, ctp.tipo, fp.cod_frecuencia_pago, ");
        sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
        sql.append(" from trabajador t, conceptofijo cf, conceptotipopersonal ctp, tipopersonal tp,");
        sql.append(" frecuenciapago fp, turno tu, frecuenciatipopersonal ftp, concepto c");
        sql.append(" where t.id_trabajador = cf.id_trabajador");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
        sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
        sql.append(" and ctp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
        sql.append(" and cf.id_concepto_tipo_personal in (select id_concepto_tipo_personal from conceptoasociado where id_concepto_asociar=?)");
        sql.append(" and ctp.id_concepto=c.id_concepto");
        sql.append(" and ctp.recalculo ='S'");
        sql.append(" and t.estatus = 'A'");
      }
      else
      {
        sql.append("select cf.id_concepto_variable, t.id_trabajador, cf.unidades, ctp.tipo, fp.cod_frecuencia_pago, ");
        sql.append(" tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, t.id_cargo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
        sql.append(" from trabajador t, conceptovariable cf, conceptotipopersonal ctp, tipopersonal tp,");
        sql.append(" frecuenciapago fp, turno tu, frecuenciatipopersonal ftp, concepto c");
        sql.append(" where t.id_trabajador = cf.id_trabajador");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
        sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
        sql.append(" and ctp.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
        sql.append(" and cf.id_concepto_tipo_personal in (select id_concepto_tipo_personal from conceptoasociado where id_concepto_asociar=?)");
        sql.append(" and ctp.id_concepto=c.id_concepto");
        sql.append(" and ctp.recalculo ='S'");
        sql.append(" and t.estatus = 'A'");
      }

      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConceptoTipoPersonal);
      rsRegistros = stRegistros.executeQuery();

      double monto = 0.0D;

      stBorrar = connection.createStatement();
      while (rsRegistros.next())
      {
        sql = new StringBuffer();
        monto = calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonal, rsRegistros.getLong("id_trabajador"), rsRegistros.getDouble("unidades"), rsRegistros.getString("tipo"), rsRegistros.getInt("cod_frecuencia_pago"), rsRegistros.getDouble("jornada_diaria"), rsRegistros.getDouble("jornada_semanal"), rsRegistros.getString("formula_integral"), rsRegistros.getString("formula_semanal"), rsRegistros.getLong("id_cargo"), rsRegistros.getDouble("valor"), rsRegistros.getDouble("tope_minimo"), rsRegistros.getDouble("tope_maximo"));
        if (tipo.equals("F")) {
          sql.append("update conceptofijo set monto_anterior = monto where monto <>  " + monto + " and  id_concepto_fijo = " + rsRegistros.getLong("id_concepto_fijo") + ";");
          sql.append("update conceptofijo set monto = " + monto + " where id_concepto_fijo = " + rsRegistros.getLong("id_concepto_fijo"));
        } else {
          sql.append("update conceptovariable set monto = " + monto + " where id_concepto_variable = " + rsRegistros.getLong("id_concepto_variable"));
        }

        stBorrar.addBatch(sql.toString());
      }

      stBorrar.executeBatch();
      connection.close(); connection = null;

      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      rsRegistros.close();
      connection.close(); connection = null;
      return false;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException9) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException10) {
        } if (stBorrar != null) try {
          stBorrar.close();
        } catch (Exception localException11) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException12)
        {
        }
    }
  }

  public boolean executeRecalculoPorCriterio(long idTipoPersonal, long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, String tipo, long idRegion, int gradoDesde, int gradoHasta, java.util.Date ingresoDesde, java.util.Date ingresoHasta, double sueldoBasicoDesde, double sueldoBasicoHasta, double sueldoIntegralDesde, double sueldoIntegralHasta, long idCargo, double unidades)
    throws Exception
  {
    this.txn.open();
    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();
    ConceptoTipoPersonal conceptoTipoPersonal = conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(idConceptoTipoPersonal);
    this.txn.close();

    int contador = 0;
    java.sql.Date ingresoDesdeSql = new java.sql.Date(ingresoDesde.getYear(), ingresoDesde.getMonth(), ingresoDesde.getDate());
    java.sql.Date ingresoHastaSql = new java.sql.Date(ingresoHasta.getYear(), ingresoHasta.getMonth(), ingresoHasta.getDate());
    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());

    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;
    Statement stActualizar = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      stActualizar = connection.createStatement();

      sql = new StringBuffer();

      sql.append("select t.id_trabajador, tu.jornada_diaria, tu.jornada_semanal, ");
      sql.append(" tp.formula_integral, tp.formula_semanal, t.id_cargo");
      sql.append(" from trabajador t, turno tu, cargo c, sueldopromedio su, dependencia d, sede s, region r, tipopersonal tp");
      sql.append(" where t.id_turno = tu.id_turno ");
      sql.append(" and t.id_cargo = c.id_cargo ");
      sql.append(" and su.id_trabajador = t.id_trabajador ");
      sql.append(" and t.id_dependencia = d.id_dependencia ");
      sql.append(" and d.id_sede = s.id_sede ");
      sql.append(" and s.id_region = r.id_region ");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_tipo_personal = ? ");
      sql.append(" and t.estatus = 'A'");
      sql.append(" and c.grado >= ? ");
      sql.append(" and c.grado <= ? ");
      sql.append(" and t.sueldo_basico >= ?");
      sql.append(" and sueldo_basico <= ? ");
      sql.append(" and su.promedio_integral >= ?");
      sql.append(" and su.promedio_integral <= ?");
      sql.append(" and t.fecha_ingreso >= ?");
      sql.append(" and t.fecha_ingreso <= ?");

      if (idRegion != 0L) {
        sql.append(" and r.id_region = " + idRegion);
      }
      if (idCargo != 0L) {
        sql.append(" and c.id_cargo = " + idCargo);
      }

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, idTipoPersonal);
      stTrabajadores.setInt(2, gradoDesde);
      stTrabajadores.setInt(3, gradoHasta);
      stTrabajadores.setDouble(4, sueldoBasicoDesde);
      stTrabajadores.setDouble(5, sueldoBasicoHasta);
      stTrabajadores.setDouble(6, sueldoIntegralDesde);
      stTrabajadores.setDouble(7, sueldoIntegralHasta);
      stTrabajadores.setDate(8, ingresoDesdeSql);
      stTrabajadores.setDate(9, ingresoHastaSql);

      rsTrabajadores = stTrabajadores.executeQuery();

      while (rsTrabajadores.next()) {
        contador++;
        sql = new StringBuffer();
        if (tipo.equals("F"))
        {
          sql.append("select cf.id_concepto_fijo, cf.unidades, ctp.tipo, fp.cod_frecuencia_pago, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
          sql.append(" from conceptofijo cf, conceptotipopersonal ctp, ");
          sql.append(" frecuenciapago fp, frecuenciatipopersonal ftp");
          sql.append(" where cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
          sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
          sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
          sql.append(" and cf.id_concepto_tipo_personal = ?");
          sql.append(" and cf.id_frecuencia_tipo_personal = ?");
          sql.append(" and cf.id_trabajador = ? ");
        } else {
          sql.append("select cv.id_concepto_variable, cv.unidades, ctp.tipo, fp.cod_frecuencia_pago, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
          sql.append(" from conceptovariable cv, conceptotipopersonal ctp, ");
          sql.append(" frecuenciapago fp, frecuenciatipopersonal ftp");
          sql.append(" where cv.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
          sql.append(" and cv.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
          sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
          sql.append(" and cv.id_concepto_tipo_personal = ?");
          sql.append(" and cv.id_frecuencia_tipo_personal = ?");
          sql.append(" and cv.id_trabajador = ? ");
        }
        CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idConceptoTipoPersonal);
        stRegistros.setLong(2, idFrecuenciaTipoPersonal);
        stRegistros.setLong(3, rsTrabajadores.getLong("id_trabajador"));
        rsRegistros = stRegistros.executeQuery();

        double monto = 0.0D;
        sql = new StringBuffer();
        if (rsRegistros.next()) {
          monto = calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonal, rsTrabajadores.getLong("id_trabajador"), rsRegistros.getDouble("unidades"), rsRegistros.getString("tipo"), rsRegistros.getInt("cod_frecuencia_pago"), rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsRegistros.getDouble("valor"), rsRegistros.getDouble("tope_minimo"), rsRegistros.getDouble("tope_maximo"));
          if (monto > 0.0D) {
            if (tipo.equals("F"))
              sql.append("update conceptofijo set monto = " + monto + " where id_concepto_fijo = " + rsRegistros.getLong("id_concepto_fijo"));
            else {
              sql.append("update conceptovariable set monto = " + monto + " where id_concepto_variable = " + rsRegistros.getLong("id_concepto_variable"));
            }
            stActualizar.addBatch(sql.toString());
          }
        } else {
          monto = calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonal, rsTrabajadores.getLong("id_trabajador"), unidades, conceptoTipoPersonal.getTipo(), conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago(), rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), conceptoTipoPersonal.getValor(), conceptoTipoPersonal.getTopeMinimo(), conceptoTipoPersonal.getTopeMaximo());

          if (monto > 0.0D) {
            if (tipo.equals("F")) {
              sql.append("insert into conceptofijo (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, estatus, id_concepto_fijo) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(idConceptoTipoPersonal + ", ");
              sql.append(idFrecuenciaTipoPersonal + "," + unidades + ", ");
              sql.append(monto + ", '");
              sql.append(fechaActualSql + "', 'A', ");
              sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");
            }
            else
            {
              sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
              sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, estatus, id_concepto_variable) values(");
              sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
              sql.append(idConceptoTipoPersonal + ", ");
              sql.append(idFrecuenciaTipoPersonal + "," + unidades + ", ");
              sql.append(monto + ", '");
              sql.append(fechaActualSql + "', 'A', ");
              sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");
            }

            stActualizar.addBatch(sql.toString());
          }
        }

        if (contador == 500) {
          stActualizar.executeBatch();
          contador = 0;
        }
      }

      stActualizar.executeBatch();
      connection.commit();
      connection.close(); connection = null;

      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException13) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException14) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException15) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException16) {
        } if (stActualizar != null) try {
          stActualizar.close();
        } catch (Exception localException17) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException18)
        {
        } 
    }
  }

  public boolean executeIngresarConceptos(long idConceptoTipoPersonal, long idConceptoTipoPersonalIngresar, String tipo, double unidades, long idFrecuenciaTipoPersonal)
    throws Exception
  {
    this.txn.open();
    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();
    ConceptoTipoPersonal conceptoTipoPersonal = conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(idConceptoTipoPersonalIngresar);
    this.txn.close();

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    ResultSet rsRegistroTrabajador = null;
    PreparedStatement stRegistroTrabajador = null;
    Statement stConceptos = null;

    StringBuffer sql = new StringBuffer();

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      if (tipo.equals("F")) {
        sql.append("select cf.id_concepto_fijo, t.id_trabajador, ctp.valor, ctp.unidades ");
        sql.append(" from trabajador t, conceptofijo cf, conceptotipopersonal ctp ");
        sql.append(" where t.id_trabajador = cf.id_trabajador");
        sql.append(" and cf.id_concepto_tipo_personal = ?");
        sql.append(" and cf.id_trabajador = ?");
        sql.append(" and ctp.id_concepto_tipo_personal = cf.id_concepto_tipo_personal");
        sql.append(" and t.estatus = 'A'");
      } else {
        sql.append("select cv.id_concepto_variable, t.id_trabajador, ctp.valor, ctp.unidades ");
        sql.append(" from trabajador t, conceptovariable cv, conceptotipopersonal ctp ");
        sql.append(" where t.id_trabajador = cv.id_trabajador");
        sql.append(" and cv.id_concepto_tipo_personal = ?");
        sql.append(" and cv.id_trabajador = ?");
        sql.append(" and ctp.id_concepto_tipo_personal = cv.id_concepto_tipo_personal");
        sql.append(" and t.estatus = 'A'");
      }

      stRegistroTrabajador = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      sql = new StringBuffer();

      sql.append("select cf.id_concepto_fijo, t.id_trabajador, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal ");
      sql.append(" from trabajador t, conceptofijo cf, conceptotipopersonal ctp, turno tu, tipopersonal tp ");
      sql.append(" where t.id_trabajador = cf.id_trabajador");
      sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and cf.id_concepto_tipo_personal = ?");
      sql.append(" and ctp.id_concepto_tipo_personal = cf.id_concepto_tipo_personal");
      sql.append(" and t.estatus = 'A'");
      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConceptoTipoPersonal);
      rsRegistros = stRegistros.executeQuery();

      double monto = 0.0D;

      stConceptos = connection.createStatement();
      while (rsRegistros.next())
      {
        stRegistroTrabajador.setLong(1, rsRegistros.getLong("id_trabajador"));
        stRegistroTrabajador.setLong(2, idConceptoTipoPersonalIngresar);
        rsRegistroTrabajador = stRegistroTrabajador.executeQuery();
        if (!rsRegistroTrabajador.next())
        {
          sql = new StringBuffer();
          monto = calcularConceptoBeanBusiness.calcular(idConceptoTipoPersonalIngresar, rsRegistros.getLong("id_trabajador"), unidades, conceptoTipoPersonal.getTipo(), conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago(), rsRegistros.getDouble("jornada_diaria"), rsRegistros.getDouble("jornada_semanal"), rsRegistros.getString("formula_integral"), rsRegistros.getString("formula_semanal"), 0L, conceptoTipoPersonal.getValor(), conceptoTipoPersonal.getTopeMinimo(), conceptoTipoPersonal.getTopeMaximo());
          if (tipo.equals("F")) {
            sql.append("insert into conceptofijo (id_trabajador, id_concepto_tipo_personal, ");
            sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, fecha_comienzo, ");
            sql.append(" documento_soporte, estatus, id_concepto_fijo) values(");
            sql.append(rsRegistros.getLong("id_trabajador") + ", " + idConceptoTipoPersonalIngresar + ", ");
            sql.append(idFrecuenciaTipoPersonal + ", " + unidades + ", " + monto + ", '" + fechaActualSql + "', '");
            sql.append(fechaActualSql + "', null, 'A', ");
            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");
          }
          else {
            sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
            sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro,  ");
            sql.append(" documento_soporte, estatus, id_concepto_variable) values(");
            sql.append(rsRegistros.getLong("id_trabajador") + ", " + idConceptoTipoPersonalIngresar + ", ");
            sql.append(idFrecuenciaTipoPersonal + ", " + unidades + ", " + monto + ", '" + fechaActualSql + "', ");
            sql.append("null, 'A', ");
            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");
          }

          stConceptos.addBatch(sql.toString());
        }
      }

      stConceptos.executeBatch();
      connection.commit();
      connection.close(); connection = null;

      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException13) {
        } if (rsRegistroTrabajador != null) try {
          rsRegistroTrabajador.close();
        } catch (Exception localException14) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException15) {
        } if (stRegistroTrabajador != null) try {
          stRegistroTrabajador.close();
        } catch (Exception localException16) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException17) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException18)
        {
        } 
    }
  }

  public boolean executeSumarConcepto(long idConceptoActualizar, long idConceptoSumar, String tipoActualizar, String tipoSumar) throws Exception {
    Connection connection = null;
    ResultSet rsConceptoSumar = null;
    PreparedStatement stConceptoSumar = null;
    ResultSet rsConceptoActualizar = null;
    PreparedStatement stConceptoActualizar = null;
    Statement stConceptos = null;

    StringBuffer sql = new StringBuffer();

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      if (tipoSumar.equals("F")) {
        sql.append("select cf.monto, cf.id_concepto_fijo, t.id_trabajador, ctp.valor, ctp.unidades ");
        sql.append(" from trabajador t, conceptofijo cf, conceptotipopersonal ctp ");
        sql.append(" where t.id_trabajador = cf.id_trabajador");
        sql.append(" and cf.id_concepto_tipo_personal = ?");
        sql.append(" and cf.id_trabajador = ?");
        sql.append(" and ctp.id_concepto_tipo_personal = cf.id_concepto_tipo_personal");
        sql.append(" and t.estatus = 'A'");
      } else {
        sql.append("select cv.monto, cv.id_concepto_variable, t.id_trabajador, ctp.valor, ctp.unidades ");
        sql.append(" from trabajador t, conceptovariable cv, conceptotipopersonal ctp ");
        sql.append(" where t.id_trabajador = cv.id_trabajador");
        sql.append(" and cv.id_concepto_tipo_personal = ?");
        sql.append(" and cv.id_trabajador = ?");
        sql.append(" and ctp.id_concepto_tipo_personal = cv.id_concepto_tipo_personal");
        sql.append(" and t.estatus = 'A'");
      }

      stConceptoSumar = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();

      if (tipoActualizar.equals("F")) {
        sql.append("select cf.monto, cf.id_concepto_fijo, t.id_trabajador, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal ");
        sql.append(" from trabajador t, conceptofijo cf, conceptotipopersonal ctp, turno tu, tipopersonal tp ");
        sql.append(" where t.id_trabajador = cf.id_trabajador");
        sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and cf.id_concepto_tipo_personal = ?");
        sql.append(" and ctp.id_concepto_tipo_personal = cf.id_concepto_tipo_personal");
        sql.append(" and t.estatus = 'A'");
        stConceptoActualizar = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
      } else {
        sql.append("select cv.monto, cv.id_concepto_variable, t.id_trabajador, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal ");
        sql.append(" from trabajador t, conceptovariable cv, conceptotipopersonal ctp, turno tu, tipopersonal tp ");
        sql.append(" where t.id_trabajador = cv.id_trabajador");
        sql.append(" and t.id_tipo_personal = tp.id_tipo_personal");
        sql.append(" and t.id_turno = tu.id_turno");
        sql.append(" and cv.id_concepto_tipo_personal = ?");
        sql.append(" and ctp.id_concepto_tipo_personal = cv.id_concepto_tipo_personal");
        sql.append(" and t.estatus = 'A'");
        stConceptoActualizar = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
      }
      stConceptoActualizar.setLong(1, idConceptoActualizar);
      rsConceptoActualizar = stConceptoActualizar.executeQuery();

      double monto = 0.0D;

      stConceptos = connection.createStatement();
      while (rsConceptoActualizar.next())
      {
        stConceptoSumar.setLong(1, idConceptoSumar);
        stConceptoSumar.setLong(2, rsConceptoActualizar.getLong("id_trabajador"));
        rsConceptoSumar = stConceptoSumar.executeQuery();
        if (rsConceptoSumar.next())
        {
          monto = rsConceptoSumar.getDouble("monto") + rsConceptoActualizar.getDouble("monto");

          sql = new StringBuffer();

          if (tipoActualizar.equals("F")) {
            sql.append("update conceptofijo set monto =  " + NumberTools.twoDecimal(monto));
            sql.append(" where id_concepto_fijo = " + rsConceptoActualizar.getLong("id_concepto_fijo"));
            stConceptos.addBatch(sql.toString());

            sql = new StringBuffer();
            sql.append("delete from  conceptofijo where id_concepto_fijo =  " + rsConceptoSumar.getLong("id_concepto_fijo"));
            stConceptos.addBatch(sql.toString());
          }
          else {
            sql.append("update conceptovariable set monto =  " + NumberTools.twoDecimal(monto));
            sql.append(" where id_concepto_variable = " + rsConceptoActualizar.getLong("id_concepto_variable"));
            stConceptos.addBatch(sql.toString());

            sql = new StringBuffer();
            sql.append("delete from  conceptovariable where id_concepto_variable =  " + rsConceptoSumar.getLong("id_concepto_variable"));
            stConceptos.addBatch(sql.toString());
          }

        }

      }

      stConceptos.executeBatch();

      connection.commit();
      connection.close(); connection = null;

      return true;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    }
    finally {
      if (rsConceptoSumar != null) try {
          rsConceptoSumar.close();
        } catch (Exception localException13) {
        } if (rsConceptoActualizar != null) try {
          rsConceptoActualizar.close();
        } catch (Exception localException14) {
        } if (stConceptoSumar != null) try {
          stConceptoSumar.close();
        } catch (Exception localException15) {
        } if (stConceptoActualizar != null) try {
          stConceptoActualizar.close();
        } catch (Exception localException16) {
        } if (stConceptos != null) try {
          stConceptos.close();
        } catch (Exception localException17) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException18)
        {
        }
    }
  }
}