package sigefirrhh.personal.conceptos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class SumarConceptoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SumarConceptoForm.class.getName());
  private Collection listTipoPersonal;
  private Collection listConceptoActualizar;
  private Collection listConceptoSumar;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private ConceptosFacade conceptosFacade;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoActualizar;
  private ConceptoTipoPersonal conceptoSumar;
  private long idConceptoActualizar;
  private long idConceptoSumar;
  private long idTipoPersonal;
  private String tipoConceptoActualizar;
  private String tipoConceptoSumar;
  private boolean show1 = false;
  private boolean show2 = false;

  private int registros = 0;
  private double unidades = 0.0D;

  public SumarConceptoForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.conceptosFacade = new ConceptosFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.conceptoSumar = null;
    this.conceptoActualizar = null;
    this.idConceptoSumar = 0L;
    this.idConceptoActualizar = 0L;
    this.listConceptoActualizar = null;
    this.listConceptoSumar = null;
    try
    {
      if (idTipoPersonal > 0L)
      {
        this.listConceptoActualizar = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
        this.listConceptoSumar = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.show1 = true;
    this.show2 = false;
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      ConceptoTipoPersonal concepto = this.definicionesFacade.findConceptoTipoPersonalById(this.idConceptoActualizar);

      if (this.conceptosFacade.executeSumarConcepto(this.idConceptoActualizar, this.idConceptoSumar, this.tipoConceptoActualizar, this.tipoConceptoSumar))
      {
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', concepto + " " + this.tipoPersonal);

        context.addMessage("success_add", new FacesMessage("Se ejecutó con éxito"));
        this.show2 = false;
        this.show1 = true;
        return null;
      }
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = true;
    this.show1 = false;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    this.show1 = true;
    return null;
  }

  public Collection getListConceptoActualizar()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoActualizar, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListConceptoSumar() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoSumar, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public boolean isShowConcepto()
  {
    return (this.listConceptoActualizar != null) && 
      (!this.listConceptoActualizar.isEmpty());
  }
  public boolean isShow1() {
    return this.show1;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public int getRegistros() {
    return this.registros;
  }
  public double getUnidades() {
    return this.unidades;
  }
  public void setUnidades(double unidades) {
    this.unidades = unidades;
  }
  public long getIdConceptoActualizar() {
    return this.idConceptoActualizar;
  }
  public void setIdConceptoActualizar(long idConceptoActualizar) {
    this.idConceptoActualizar = idConceptoActualizar;
  }
  public long getIdConceptoSumar() {
    return this.idConceptoSumar;
  }
  public void setIdConceptoSumar(long idConceptoSumar) {
    this.idConceptoSumar = idConceptoSumar;
  }
  public String getTipoConceptoActualizar() {
    return this.tipoConceptoActualizar;
  }
  public void setTipoConceptoActualizar(String tipoConceptoActualizar) {
    this.tipoConceptoActualizar = tipoConceptoActualizar;
  }
  public String getTipoConceptoSumar() {
    return this.tipoConceptoSumar;
  }
  public void setTipoConceptoSumar(String tipoConceptoSumar) {
    this.tipoConceptoSumar = tipoConceptoSumar;
  }
}