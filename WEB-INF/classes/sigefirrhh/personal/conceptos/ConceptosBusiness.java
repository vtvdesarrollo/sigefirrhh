package sigefirrhh.personal.conceptos;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Date;

public class ConceptosBusiness extends AbstractBusiness
  implements Serializable
{
  ActualizarConceptosBeanBusiness actualizarConceptosBeanBusiness = new ActualizarConceptosBeanBusiness();

  public int findRegistrosCambioFrecuencia(long idConceptoTipoPersonal, long idFrecuenciatipoPersonalActual, int idFrecuenciaTipoPersonalNuevo, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.findRegistrosCambioFrecuencia(idConceptoTipoPersonal, idFrecuenciatipoPersonalActual, idFrecuenciaTipoPersonalNuevo, tipo);
  }

  public boolean executeCambioFrecuencia(long idConceptoTipoPersonal, long idFrecuenciatipoPersonalActual, long idFrecuenciaTipoPersonalNuevo, int codFrecuenciaPagoActual, int codFrecuenciaPagoNuevo, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeCambioFrecuencia(idConceptoTipoPersonal, idFrecuenciatipoPersonalActual, idFrecuenciaTipoPersonalNuevo, codFrecuenciaPagoActual, codFrecuenciaPagoNuevo, tipo);
  }

  public int findRegistrosCambioEstatus(long idConceptoTipoPersonal, long idFrecuenciatipoPersonal, String estatus, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.findRegistrosCambioEstatus(idConceptoTipoPersonal, idFrecuenciatipoPersonal, estatus, tipo);
  }

  public boolean executeCambioEstatus(long idConceptoTipoPersonal, long idFrecuenciatipoPersonal, String estatusActual, String estatusNuevo, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeCambioEstatus(idConceptoTipoPersonal, idFrecuenciatipoPersonal, estatusActual, estatusNuevo, tipo);
  }

  public int findRegistros(long idConceptoTipoPersonal, long idFrecuenciatipoPersonal, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.findRegistros(idConceptoTipoPersonal, idFrecuenciatipoPersonal, tipo);
  }

  public boolean executeActualizarMonto(long idConceptoTipoPersonal, long idFrecuenciatipoPersonal, double multiplicar, double dividir, double unidades, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeActualizarMonto(idConceptoTipoPersonal, idFrecuenciatipoPersonal, multiplicar, dividir, unidades, tipo);
  }

  public boolean executeEliminarConceptos(long idConceptoTipoPersonal, long idFrecuenciatipoPersonal, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeEliminarConceptos(idConceptoTipoPersonal, idFrecuenciatipoPersonal, tipo);
  }

  public boolean executeEliminarPrestamos(long idConceptoTipoPersonal)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeEliminarPrestamos(idConceptoTipoPersonal);
  }

  public boolean executeRecalculo(long idConceptoTipoPersonal, long idFrecuenciatipoPersonal, String tipo, double unidades)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeRecalculo(idConceptoTipoPersonal, idFrecuenciatipoPersonal, tipo, unidades);
  }

  public boolean executeRecalculoPorAsociado(long idConceptoTipoPersonal, String tipo)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeRecalculoPorAsociado(idConceptoTipoPersonal, tipo);
  }

  public boolean executeRecalculoPorCriterio(long idTipoPersonal, long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, String tipo, long idRegion, int gradoDesde, int gradoHasta, Date ingresoDesde, Date ingresoHasta, double sueldoBasicoDesde, double sueldoBasicoHasta, double sueldoIntegralDesde, double sueldoIntegralHasta, long idCargo, double unidades)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeRecalculoPorCriterio(idTipoPersonal, idConceptoTipoPersonal, 
      idFrecuenciaTipoPersonal, tipo, 
      idRegion, gradoDesde, gradoHasta, ingresoDesde, 
      ingresoHasta, sueldoBasicoDesde, sueldoBasicoHasta, 
      sueldoIntegralDesde, sueldoIntegralHasta, idCargo, unidades);
  }

  public boolean executeIngresarConceptos(long idConceptoTipoPersonal, long idConceptoTipoPersonalIngresar, String tipo, double unidades, long idFrecuenciaTipoPersonal)
    throws Exception
  {
    return this.actualizarConceptosBeanBusiness.executeIngresarConceptos(idConceptoTipoPersonal, 
      idConceptoTipoPersonalIngresar, tipo, unidades, idFrecuenciaTipoPersonal);
  }

  public boolean executeSumarConcepto(long idConceptoActualizar, long idConceptoSumar, String tipoActualizar, String tipoSumar) throws Exception {
    return this.actualizarConceptosBeanBusiness.executeSumarConcepto(idConceptoActualizar, 
      idConceptoSumar, tipoActualizar, tipoSumar);
  }
}