package sigefirrhh.personal.conceptos;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.ConceptoVariable;

public class CargaSobretiempoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargaSobretiempoForm.class.getName());
  private boolean showEliminar;
  private String accion = "A";
  private Collection listFrecuenciaTipoPersonal;
  private Collection listTipoPersonal;
  private Collection listConcepto;
  private LoginSession login;
  private int anio;
  private DefinicionesNoGenFacade definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private TipoPersonal tipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private ConceptoTipoPersonal concepto;
  private long idFrecuenciaTipoPersonal;
  private long idConcepto;
  private long idTipoPersonal;
  private int cedula;
  private double horasDiasCantidad;
  private String documentoSoporte;
  private double monto;
  private int cedulaFinal;
  private double horasDiasCantidadFinal;
  private double montoFinal;
  private String documentoSoporteFinal;
  private String nombre;
  private String apellido;
  private String tipoConcepto;
  private double jornadaDiaria;
  private double jornadaSemanal;
  private int mes;
  private ConceptoFijo conceptoFijo;
  private ConceptoVariable conceptoVariable;
  private Collection colConceptoFijo;
  private Collection colConceptoVariable;
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  private java.util.Date fechaActual = new java.util.Date();
  java.sql.Date fechaActualSql = new java.sql.Date(this.fechaActual.getYear(), this.fechaActual.getMonth(), this.fechaActual.getDate());

  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  StringBuffer sql = new StringBuffer();

  private ResultSet rsRegistroTrabajador = null;
  private PreparedStatement stRegistroTrabajador = null;
  private ResultSet rsRegistroConceptoFijo = null;
  private PreparedStatement stRegistroConceptoFijo = null;
  private long idTrabajador = 0L;
  private boolean conceptoFijoExiste = false;
  private boolean conceptoVariableExiste = false;

  public CargaSobretiempoForm() { FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    java.util.Date fechaActual = new java.util.Date();
    this.anio = (fechaActual.getYear() + 1900);
    this.mes = (fechaActual.getMonth() + 1);
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.connection.setAutoCommit(true);
      this.connection = Resource.getConnection();

      this.sql = new StringBuffer();
      this.sql.append("select t.id_trabajador, p.primer_nombre, p.primer_apellido, tu.jornada_diaria, tu.jornada_semanal ");
      this.sql.append(" from trabajador t, personal p, turno tu");
      this.sql.append(" where t.id_personal = p.id_personal");
      this.sql.append(" and tu.id_turno = t.id_turno");
      this.sql.append(" and t.estatus = 'A'");
      this.sql.append(" and  t.cedula = ? and t.id_tipo_personal = ?");

      this.stRegistroTrabajador = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      return;
    }

    refresh();
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.frecuenciaTipoPersonal = null;
    this.idFrecuenciaTipoPersonal = 0L;
    this.listFrecuenciaTipoPersonal = null;

    this.concepto = null;
    this.idConcepto = 0L;
    this.listConcepto = null;
    try
    {
      if (idTipoPersonal > 0L)
      {
        this.listConcepto = 
          this.definicionesFacade.findConceptoTipoPersonalBySobretiempo(
          idTipoPersonal, "S");
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.nombre = "";
    this.apellido = "";
    this.monto = 0.0D;
    this.horasDiasCantidad = 0.0D;
    this.montoFinal = 0.0D;
    this.horasDiasCantidadFinal = 0.0D;
    this.cedulaFinal = 0;
  }

  public void changeMonto(ValueChangeEvent event) {
    this.monto = ((Double)event.getNewValue()).doubleValue();
  }

  public void changeUnidades(ValueChangeEvent event) {
    this.horasDiasCantidad = ((Double)event.getNewValue()).doubleValue();
  }

  public String buscarTrabajador()
  {
    return null;
  }

  public String eliminar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.stRegistroTrabajador.setInt(1, this.cedula);
      this.stRegistroTrabajador.setLong(2, this.idTipoPersonal);
      this.rsRegistroTrabajador = this.stRegistroTrabajador.executeQuery();

      if (this.rsRegistroTrabajador.next()) {
        this.idTrabajador = this.rsRegistroTrabajador.getLong("id_trabajador");
        this.nombre = this.rsRegistroTrabajador.getString("primer_nombre");
        this.apellido = this.rsRegistroTrabajador.getString("primer_apellido");
      }

      log.error("ID_TRABAJADOR  = " + this.idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador no existe", ""));
      return null;
    }
    try
    {
      double tope = this.definicionesFacade.verificarTopeSobretiempo(this.idConcepto, this.idTrabajador, this.mes, this.anio, this.horasDiasCantidad);
      if (tope != 0.0D) {
        if (tope < 0.0D) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pueden agregar mas horas extras al trabajador, el tope mensual no lo permite ", ""));
          return null;
        }
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El tope mensual solo permite agregar " + tope + " horas ", ""));
        return null;
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error : " + e, ""));
      return null;
    }
    this.sql = new StringBuffer();
    try
    {
      this.stExecute = this.connection.createStatement();
      if (this.tipoConcepto.equals("F"))
        this.sql.append("delete from conceptofijo where");
      else {
        this.sql.append("delete from conceptovariable where");
      }

      this.sql.append(" id_frecuencia_tipo_personal = " + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());
      this.sql.append(" and id_concepto_tipo_personal = " + this.concepto.getIdConceptoTipoPersonal());
      this.sql.append(" and id_trabajador = " + this.idTrabajador);
      this.stExecute.execute(this.sql.toString());
      this.stExecute.close();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
      return null;
    }

    this.cedulaFinal = this.cedula;

    this.cedula = 0;
    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public String verificarTope()
  {
    return null;
  }
  public String agregar() {
    double montoTotal = 0.0D;
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

    double montoIntermedio = 0.0D;
    try
    {
      this.stRegistroTrabajador.setInt(1, this.cedula);
      this.stRegistroTrabajador.setLong(2, this.idTipoPersonal);
      this.rsRegistroTrabajador = this.stRegistroTrabajador.executeQuery();

      if (this.rsRegistroTrabajador.next()) {
        this.idTrabajador = this.rsRegistroTrabajador.getLong("id_trabajador");
        this.nombre = this.rsRegistroTrabajador.getString("primer_nombre");
        this.apellido = this.rsRegistroTrabajador.getString("primer_apellido");
        this.jornadaDiaria = this.rsRegistroTrabajador.getDouble("jornada_diaria");
        this.jornadaSemanal = this.rsRegistroTrabajador.getDouble("jornada_semanal");
      }
      else {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador no existe", ""));
        return null;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras buscaba al trabajador", ""));
      return null;
    }

    log.error("esta continuando");
    this.cedulaFinal = this.cedula;

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = 
      new CalcularConceptoBeanBusiness();

    if (this.monto == 0.0D) {
      try {
        montoIntermedio = calcularConceptoBeanBusiness.calcular(this.idConcepto, this.idTrabajador, this.horasDiasCantidad, this.concepto.getTipo(), this.frecuenciaTipoPersonal.getFrecuenciaPago().getCodFrecuenciaPago(), this.jornadaDiaria, this.jornadaSemanal, this.concepto.getTipoPersonal().getFormulaIntegral(), this.concepto.getTipoPersonal().getFormulaSemanal(), 0L, this.concepto.getValor(), this.concepto.getTopeMinimo(), this.concepto.getTopeMaximo());
        log.error("monto " + this.montoFinal);
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador no existe", ""));
        return null;
      }
    }

    if ((this.horasDiasCantidad != 0.0D) && (this.monto != 0.0D))
      montoIntermedio = this.monto * this.horasDiasCantidad;
    else if ((this.horasDiasCantidad == 0.0D) && (this.monto != 0.0D)) {
      montoIntermedio = this.monto;
    }

    if (montoIntermedio != 0.0D) {
      if (this.tipoConcepto.equals("F"))
      {
        try
        {
          this.stRegistroConceptoFijo.setLong(1, this.concepto.getIdConceptoTipoPersonal());
          this.stRegistroConceptoFijo.setLong(2, this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());
          this.stRegistroConceptoFijo.setLong(3, this.idTrabajador);
          this.rsRegistroConceptoFijo = this.stRegistroConceptoFijo.executeQuery();
          if (this.rsRegistroConceptoFijo.next())
            this.conceptoFijoExiste = true;
          else {
            this.conceptoFijoExiste = false;
          }
        }
        catch (Exception e)
        {
          return null;
        }

        if (!this.conceptoFijoExiste) {
          this.sql = new StringBuffer();
          try
          {
            this.stExecute = this.connection.createStatement();

            this.sql.append("insert into conceptofijo (id_trabajador, monto,");
            this.sql.append(" id_frecuencia_tipo_personal, id_concepto_tipo_personal, ");
            this.sql.append(" unidades, fecha_registro, documento_soporte, id_concepto_fijo) values(");
            this.sql.append(this.idTrabajador + ", " + montoIntermedio + ", ");
            this.sql.append(this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ", ");
            this.sql.append(this.concepto.getIdConceptoTipoPersonal() + ", ");
            this.sql.append(this.horasDiasCantidad + ", '");
            this.sql.append(this.fechaActualSql + "',");
            this.sql.append("'" + this.documentoSoporte + "',");
            this.sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");
            this.stExecute.addBatch(this.sql.toString());
            this.stExecute.executeBatch();

            this.stExecute.close();
          }
          catch (Exception e) {
            log.error("Excepcion controlada:", e);
            context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
            return null;
          }

        }
        else if (this.accion.equals("M")) {
          this.sql = new StringBuffer();
          try
          {
            this.stExecute = this.connection.createStatement();

            this.sql.append("update conceptofijo set  monto = " + montoIntermedio);
            this.sql.append(" , unidades = " + this.horasDiasCantidad);
            this.sql.append(" where id_concepto_fijo = " + this.rsRegistroConceptoFijo.getLong("id_concepto_fijo"));
            this.stExecute.addBatch(this.sql.toString());
            this.stExecute.executeBatch();

            this.stExecute.close();
          } catch (Exception e) {
            context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error", ""));
            log.error("Excepcion controlada:", e);
            return null;
          }
        } else if (this.accion.equals("S")) {
          this.sql = new StringBuffer();
          try
          {
            this.stExecute = this.connection.createStatement();
            montoTotal = montoIntermedio + this.rsRegistroConceptoFijo.getDouble("monto");
            this.sql.append("update conceptofijo set  monto = " + montoTotal);
            this.sql.append(" , unidades = " + this.horasDiasCantidad);
            this.sql.append(" where id_concepto_fijo = " + this.rsRegistroConceptoFijo.getLong("id_concepto_fijo"));
            this.stExecute.addBatch(this.sql.toString());
            this.stExecute.executeBatch();

            this.stExecute.close();
          } catch (Exception e) {
            context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error", ""));
            log.error("Excepcion controlada:", e);
            return null;
          }

        }

      }
      else
      {
        this.sql = new StringBuffer();
        try
        {
          this.stExecute = this.connection.createStatement();

          this.sql.append("insert into conceptovariable (id_trabajador, monto,");
          this.sql.append(" id_frecuencia_tipo_personal, id_concepto_tipo_personal, ");
          this.sql.append(" unidades, fecha_registro, documento_soporte, id_concepto_variable) values(");
          this.sql.append(this.idTrabajador + ", " + montoIntermedio + ", ");
          this.sql.append(this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ", ");
          this.sql.append(this.concepto.getIdConceptoTipoPersonal() + ", ");
          this.sql.append(this.horasDiasCantidad + ", '");
          this.sql.append(this.fechaActualSql + "',");
          this.sql.append("'" + this.documentoSoporte + "',");
          this.sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

          this.stExecute.addBatch(this.sql.toString());
          this.stExecute.executeBatch();

          this.stExecute.close();
        }
        catch (Exception e) {
          log.error("Excepcion controlada:", e);
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El registro ya existe", ""));
          return null;
        }
      }
    }
    else
    {
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pueden agregar conceptos con monto 0", ""));
      return null;
    }

    this.cedula = 0;
    this.horasDiasCantidadFinal = this.horasDiasCantidad;
    this.documentoSoporteFinal = this.documentoSoporte;
    this.montoFinal = montoIntermedio;
    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListFrecuenciaTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFrecuenciaTipoPersonal, "sigefirrhh.base.definiciones.FrecuenciaTipoPersonal");
  }
  public Collection getListConcepto() {
    Collection col = new ArrayList();
    Iterator iterator = this.listConcepto.iterator();
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    while (iterator.hasNext()) {
      conceptoTipoPersonal = (ConceptoTipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(conceptoTipoPersonal.getIdConceptoTipoPersonal()), 
        conceptoTipoPersonal.toString() + "- FREC " + conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago()));
    }
    return col;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public String getIdFrecuenciaTipoPersonal() {
    return String.valueOf(this.idFrecuenciaTipoPersonal);
  }

  public void setIdFrecuenciaTipoPersonal(String l) {
    this.idFrecuenciaTipoPersonal = Long.parseLong(l);

    Iterator iterator = this.listFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;

    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        l)) {
        this.frecuenciaTipoPersonal = frecuenciaTipoPersonal;
        break;
      }
    }
  }

  public String getIdConcepto()
  {
    return String.valueOf(this.idConcepto);
  }

  public void setIdConcepto(String l) {
    this.idConcepto = Long.parseLong(l);
    Iterator iterator = this.listConcepto.iterator();
    ConceptoTipoPersonal concepto = null;

    while (iterator.hasNext()) {
      concepto = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(concepto.getIdConceptoTipoPersonal()).equals(
        l)) {
        this.concepto = concepto;
        break;
      }
    }
  }

  public int getCedula() {
    return this.cedula;
  }

  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public int getCedulaFinal() {
    return this.cedulaFinal;
  }

  public void setCedulaFinal(int cedulaFinal) {
    this.cedulaFinal = cedulaFinal;
  }

  public double getHorasDiasCantidad() {
    return this.horasDiasCantidad;
  }

  public void setHorasDiasCantidad(double horasDiasCantidad) {
    this.horasDiasCantidad = horasDiasCantidad;
  }

  public double getHorasDiasCantidadFinal() {
    return this.horasDiasCantidadFinal;
  }

  public void setHorasDiasCantidadFinal(double horasDiasCantidadFinal) {
    this.horasDiasCantidadFinal = horasDiasCantidadFinal;
  }

  public double getMonto() {
    return this.monto;
  }

  public void setMonto(double monto) {
    this.monto = monto;
  }

  public double getMontoFinal() {
    return this.montoFinal;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public String getTipoConcepto() {
    return this.tipoConcepto;
  }

  public void setTipoConcepto(String tipoConcepto) {
    this.tipoConcepto = tipoConcepto;
  }

  public boolean isShowFrecuenciaTipoPersonal()
  {
    return (this.listFrecuenciaTipoPersonal != null) && 
      (!this.listFrecuenciaTipoPersonal.isEmpty());
  }

  public boolean isShowConcepto()
  {
    return (this.listConcepto != null) && 
      (!this.listConcepto.isEmpty());
  }

  public String getApellido()
  {
    return this.apellido;
  }

  public void setApellido(String apellido)
  {
    this.apellido = apellido;
  }

  public String getNombre()
  {
    return this.nombre;
  }

  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }

  public String getDocumentoSoporte()
  {
    return this.documentoSoporte;
  }

  public void setDocumentoSoporte(String string)
  {
    this.documentoSoporte = string;
  }

  public String getDocumentoSoporteFinal()
  {
    return this.documentoSoporteFinal;
  }

  public void setDocumentoSoporteFinal(String string)
  {
    this.documentoSoporteFinal = string;
  }

  public String getAccion()
  {
    return this.accion;
  }

  public void setAccion(String string)
  {
    this.accion = string;
  }

  public boolean isShowEliminar()
  {
    return this.accion.equals("E");
  }

  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
}