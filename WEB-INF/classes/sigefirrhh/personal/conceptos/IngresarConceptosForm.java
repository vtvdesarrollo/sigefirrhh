package sigefirrhh.personal.conceptos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class IngresarConceptosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(IngresarConceptosForm.class.getName());
  private Collection listTipoPersonal;
  private Collection listConcepto;
  private Collection listConceptoIngresar;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private ConceptosFacade conceptosFacade;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal concepto;
  private ConceptoTipoPersonal conceptoIngresar;
  private long idConcepto;
  private long idConceptoIngresar;
  private long idTipoPersonal;
  private String tipoConcepto;
  private boolean show1 = false;
  private boolean show2 = false;

  private int registros = 0;
  private double unidades = 0.0D;

  public IngresarConceptosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.conceptosFacade = new ConceptosFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.concepto = null;
    this.idConcepto = 0L;
    this.listConcepto = null;
    try
    {
      if (idTipoPersonal > 0L)
      {
        this.listConcepto = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
        this.listConceptoIngresar = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);

        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.show1 = true;
    this.show2 = false;
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      ConceptoTipoPersonal concepto = this.definicionesFacade.findConceptoTipoPersonalById(this.idConceptoIngresar);
      if (this.conceptosFacade.executeIngresarConceptos(this.idConcepto, this.idConceptoIngresar, this.tipoConcepto, this.unidades, concepto.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal())) {
        context.addMessage("success_add", new FacesMessage("Se ejecutó con éxito"));
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.concepto + " " + this.tipoPersonal);

        this.show2 = false;
        this.show1 = true;
        return null;
      }
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = true;
    this.show1 = false;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    this.show1 = true;
    return null;
  }

  public Collection getListConcepto()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConcepto, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListConceptoIngresar() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoIngresar, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public String getIdConcepto()
  {
    return String.valueOf(this.idConcepto);
  }
  public String getIdConceptoIngresar() {
    return String.valueOf(this.idConceptoIngresar);
  }
  public void setIdConcepto(String l) {
    this.idConcepto = Long.parseLong(l);
    Iterator iterator = this.listConcepto.iterator();
    ConceptoTipoPersonal concepto = null;

    while (iterator.hasNext()) {
      concepto = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(concepto.getIdConceptoTipoPersonal()).equals(
        l)) {
        this.concepto = concepto;
        break;
      }
    }
  }

  public void setIdConceptoIngresar(String l) { this.idConceptoIngresar = Long.parseLong(l);
    Iterator iterator = this.listConceptoIngresar.iterator();
    ConceptoTipoPersonal concepto = null;

    while (iterator.hasNext()) {
      concepto = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(concepto.getIdConceptoTipoPersonal()).equals(
        l)) {
        this.conceptoIngresar = concepto;
        break;
      }
    } }

  public LoginSession getLogin() {
    return this.login;
  }

  public String getTipoConcepto() {
    return this.tipoConcepto;
  }
  public void setTipoConcepto(String tipoConcepto) {
    this.tipoConcepto = tipoConcepto;
  }

  public boolean isShowConcepto() {
    return (this.listConcepto != null) && 
      (!this.listConcepto.isEmpty());
  }

  public boolean isShowConceptoIngresar() {
    return (this.listConceptoIngresar != null) && 
      (!this.listConceptoIngresar.isEmpty());
  }
  public boolean isShow1() {
    return this.show1;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public int getRegistros() {
    return this.registros;
  }
  public double getUnidades() {
    return this.unidades;
  }
  public void setUnidades(double unidades) {
    this.unidades = unidades;
  }
}