package sigefirrhh.personal.conceptos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportConceptosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportConceptosForm.class.getName());
  private int reportId;
  private long idConcepto;
  private long idTipoPersonal;
  private String reportName;
  private boolean showConcepto;
  private boolean showReport;
  private Collection listConcepto;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal concepto;

  public ReportConceptosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "aportesquiuel";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportConceptosForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findAllTipoPersonal();
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      parameters.put("id_concepto_tipo_personal", new Long(this.idConcepto));

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/conceptos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(String.valueOf(event.getNewValue())).longValue();
    this.idTipoPersonal = idTipoPersonal;
    this.tipoPersonal = null;
    this.listConcepto = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.showReport = false;
        this.showConcepto = false;
        this.listConcepto = 
          this.definicionesFacade.findConceptoTipoPersonalByTipoPersonal(
          idTipoPersonal);
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
        this.showConcepto = true;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeConcepto(ValueChangeEvent event)
  {
    long idConcepto = Long.valueOf(String.valueOf(event.getNewValue())).longValue();
    this.idConcepto = idConcepto;

    this.concepto = null;
    try
    {
      if (idConcepto > 0L)
      {
        this.showReport = false;
        this.concepto = this.definicionesFacade.findConceptoTipoPersonalById(this.idConcepto);
        this.showReport = true;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListConcepto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConcepto, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public boolean isShowConcepto() {
    return (this.showConcepto) && (this.listConcepto != null) && 
      (!this.listConcepto.isEmpty());
  }
  public boolean isShowReport() {
    return this.showReport;
  }
  public long getIdConcepto() {
    return this.idConcepto;
  }
  public void setIdConcepto(long idConcepto) {
    this.idConcepto = idConcepto;
  }
  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }
}