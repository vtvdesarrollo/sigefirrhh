package sigefirrhh.personal.conceptos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.ManualPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class RecalculoPorCriterioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RecalculoPorCriterioForm.class.getName());
  private Collection listFrecuenciaTipoPersonal;
  private Collection listTipoPersonal;
  private Collection listConcepto;
  private Collection listRegion;
  private Collection listCargo;
  private Collection listManualCargo;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private ConceptosFacade conceptosFacade;
  private CargoNoGenFacade cargoFacade;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal concepto;
  private long idFrecuenciaTipoPersonal;
  private long idConcepto;
  private long idTipoPersonal;
  private String idManualCargo;
  private long idCargo;
  private long idRegion;
  private String tipoConcepto;
  private boolean show1 = false;
  private boolean show2 = false;
  private boolean show = false;

  private int desdeGrado = 0;
  private int hastaGrado = 100;
  private Date desdeFechaIngreso;
  private Date hastaFechaIngreso;
  private double desdeSueldoBasico = 0.0D;
  private double hastaSueldoBasico = 99000000.0D;
  private double desdeSueldoIntegral = 0.0D;
  private double hastaSueldoIntegral = 99000000.0D;
  private Calendar desdeFechaIngresoAux;
  private Calendar hastaFechaIngresoAux;
  private int registros = 0;
  private String accion;
  private double unidades = 0.0D;

  public Collection getListRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public RecalculoPorCriterioForm() {
    log.error("a1");

    this.desdeFechaIngresoAux = Calendar.getInstance();

    this.desdeFechaIngresoAux.setTime(new Date());

    this.desdeFechaIngresoAux.add(1, -100);

    this.desdeFechaIngreso = this.desdeFechaIngresoAux.getTime();
    this.hastaFechaIngreso = new Date();

    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.conceptosFacade = new ConceptosFacade();
    this.cargoFacade = new CargoNoGenFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = new ArrayList();
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), 
        this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listRegion = this.estructuraFacade.findAllRegion();
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.idFrecuenciaTipoPersonal = 0L;
    this.listFrecuenciaTipoPersonal = null;

    this.concepto = null;
    this.idConcepto = 0L;
    this.listConcepto = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.listFrecuenciaTipoPersonal = 
          this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(idTipoPersonal);

        this.listConcepto = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByRecalculo(
          idTipoPersonal, "S");
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
        Collection colManualPersonal = this.cargoFacade.findManualPersonalByTipoPersonal(this.tipoPersonal.getIdTipoPersonal());
        Iterator iterManualPersonal = colManualPersonal.iterator();
        this.listManualCargo = new ArrayList();
        while (iterManualPersonal.hasNext()) {
          ManualPersonal manualPersonal = (ManualPersonal)iterManualPersonal.next();
          ManualCargo manualCargo = this.cargoFacade.findManualCargoById(manualPersonal.getManualCargo().getIdManualCargo());
          this.listManualCargo.add(manualCargo);
        }
        log.error("listManual size" + this.listManualCargo.size());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.show1 = true;
    this.show2 = false;
    this.show = true;
  }

  public void changeManualCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.idManualCargo = null;
    this.listCargo = null;
    try
    {
      if (idManualCargo > 0L)
        this.listCargo = 
          this.cargoFacade.findCargoByManualCargo(idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.conceptosFacade.executeRecalculoPorCriterio(this.idTipoPersonal, this.idConcepto, 
        this.idFrecuenciaTipoPersonal, this.tipoConcepto, 
        this.idRegion, this.desdeGrado, this.hastaGrado, this.desdeFechaIngreso, 
        this.hastaFechaIngreso, this.desdeSueldoBasico, this.hastaSueldoBasico, 
        this.desdeSueldoIntegral, this.hastaSueldoIntegral, this.idCargo, this.unidades)) {
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.concepto + " " + this.tipoPersonal);

        context.addMessage("success_add", new FacesMessage("Se ejecutó con éxito"));
        this.show2 = false;
        this.show1 = true;
        return null;
      }
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = true;
    this.show1 = false;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    this.show1 = true;
    return null;
  }
  public Collection getListFrecuenciaTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFrecuenciaTipoPersonal, "sigefirrhh.base.definiciones.FrecuenciaTipoPersonal");
  }

  public Collection getListConcepto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConcepto, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }
  public String getIdFrecuenciaTipoPersonal() {
    return String.valueOf(this.idFrecuenciaTipoPersonal);
  }

  public String getIdConcepto()
  {
    return String.valueOf(this.idConcepto);
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public String getTipoConcepto() {
    return this.tipoConcepto;
  }

  public void setTipoConcepto(String tipoConcepto) {
    this.tipoConcepto = tipoConcepto;
  }

  public boolean isShowFrecuenciaTipoPersonal()
  {
    return (this.listFrecuenciaTipoPersonal != null) && 
      (!this.listFrecuenciaTipoPersonal.isEmpty());
  }

  public boolean isShowConcepto()
  {
    return (this.listConcepto != null) && 
      (!this.listConcepto.isEmpty());
  }

  public boolean isShowManual() {
    return (this.listManualCargo != null) && 
      (!this.listManualCargo.isEmpty());
  }

  public boolean isShowCargo() {
    return (this.listCargo != null) && 
      (!this.listCargo.isEmpty());
  }
  public boolean isShow1() {
    return this.show1;
  }
  public boolean isShow2() {
    return this.show2;
  }

  public int getRegistros()
  {
    return this.registros;
  }

  public int getDesdeGrado()
  {
    return this.desdeGrado;
  }

  public int getHastaGrado()
  {
    return this.hastaGrado;
  }

  public void setDesdeGrado(int i)
  {
    this.desdeGrado = i;
  }

  public void setHastaGrado(int i)
  {
    this.hastaGrado = i;
  }

  public long getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(long l)
  {
    this.idRegion = l;
  }

  public boolean isShow()
  {
    return this.show;
  }

  public void setShow(boolean b)
  {
    this.show = b;
  }

  public String getAccion()
  {
    return this.accion;
  }

  public void setAccion(String string)
  {
    this.accion = string;
  }

  public long getIdCargo()
  {
    return this.idCargo;
  }

  public Collection getListManualCargo()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listManualCargo, "sigefirrhh.base.cargo.ManualCargo");
  }

  public Date getDesdeFechaIngreso()
  {
    return this.desdeFechaIngreso;
  }
  public void setDesdeFechaIngreso(Date desdeFechaIngreso) {
    this.desdeFechaIngreso = desdeFechaIngreso;
  }
  public Date getHastaFechaIngreso() {
    return this.hastaFechaIngreso;
  }
  public void setHastaFechaIngreso(Date hastaFechaIngreso) {
    this.hastaFechaIngreso = hastaFechaIngreso;
  }

  public double getDesdeSueldoBasico()
  {
    return this.desdeSueldoBasico;
  }

  public double getHastaSueldoBasico()
  {
    return this.hastaSueldoBasico;
  }

  public double getHastaSueldoIntegral()
  {
    return this.hastaSueldoIntegral;
  }

  public void setDesdeSueldoBasico(double d)
  {
    this.desdeSueldoBasico = d;
  }

  public void setHastaSueldoBasico(double d)
  {
    this.hastaSueldoBasico = d;
  }

  public void setHastaSueldoIntegral(double d)
  {
    this.hastaSueldoIntegral = d;
  }

  public double getDesdeSueldoIntegral()
  {
    return this.desdeSueldoIntegral;
  }

  public void setDesdeSueldoIntegral(double d)
  {
    this.desdeSueldoIntegral = d;
  }

  public String getIdManualCargo()
  {
    return this.idManualCargo;
  }

  public void setIdManualCargo(String string)
  {
    this.idManualCargo = string;
  }

  public Collection getListCargo()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCargo, "sigefirrhh.base.cargo.Cargo");
  }

  public void setIdConcepto(String l)
  {
    this.idConcepto = Long.parseLong(l);
    Iterator iterator = this.listConcepto.iterator();
    ConceptoTipoPersonal concepto = null;

    while (iterator.hasNext()) {
      concepto = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(concepto.getIdConceptoTipoPersonal()).equals(
        l)) {
        this.idConcepto = concepto.getIdConceptoTipoPersonal();
        break;
      }
    }
  }

  public void setIdFrecuenciaTipoPersonal(String l) { this.idFrecuenciaTipoPersonal = Long.parseLong(l);

    Iterator iterator = this.listFrecuenciaTipoPersonal.iterator();
    FrecuenciaTipoPersonal frecuenciaTipoPersonal = null;

    while (iterator.hasNext()) {
      frecuenciaTipoPersonal = (FrecuenciaTipoPersonal)iterator.next();
      if (String.valueOf(frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal()).equals(
        l)) {
        this.idFrecuenciaTipoPersonal = frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal();
        break;
      }
    } }

  public void setIdCargo(long idCargo)
  {
    this.idCargo = idCargo;
  }
  public double getUnidades() {
    return this.unidades;
  }
  public void setUnidades(double unidades) {
    this.unidades = unidades;
  }
}