package sigefirrhh.personal.conceptos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.tools.NumberTools;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

public class CalcularConceptoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(CalcularConceptoBeanBusiness.class.getName());

  public double calcular(long idConceptoTipoPersonal, long idTrabajador, double unidades, String tipo, int codFrecuenciaPago, double jornadaDiaria, double jornadaSemanal, String formulaIntegral, String formulaSemanal, long idCargo, double valor, double topeMinimo, double topeMaximo)
    throws Exception
  {
    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      this.log.error("Entro en CalcularConceptoBeanBusiness -> calcular...");
      this.log.error("parametros = idConceptoTipoPersonal = " + idConceptoTipoPersonal);
      this.log.error("idTrabajador = " + idTrabajador);
      this.log.error("unidades = " + unidades);
      this.log.error("tipo = " + tipo);
      this.log.error("codFrecuenciaPago = " + codFrecuenciaPago);
      this.log.error("jornadaDiaria = " + jornadaDiaria);
      this.log.error("jornadaSemanal = " + jornadaSemanal);
      this.log.error("formulaIntegral = " + formulaIntegral);
      this.log.error("formulaSemanal = " + formulaSemanal);
      this.log.error("idCargo = " + idCargo);
      this.log.error("valor = " + valor);
      this.log.error("topeMinimo = " + topeMinimo);
      this.log.error("topeMaximo = " + topeMaximo);

      sql.append("select calcular_concepto(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idConceptoTipoPersonal);
      st.setLong(2, idTrabajador);
      st.setDouble(3, unidades);
      st.setString(4, tipo);
      st.setInt(5, codFrecuenciaPago);
      st.setDouble(6, jornadaDiaria);
      st.setDouble(7, jornadaSemanal);
      st.setString(8, formulaIntegral);
      st.setString(9, formulaSemanal);
      st.setLong(10, idCargo);
      st.setDouble(11, valor);
      st.setDouble(12, topeMinimo);
      st.setDouble(13, topeMaximo);

      rs = st.executeQuery();

      this.log.error("Se esta ejecutando ");

      rs.next();
      return NumberTools.twoDecimal(rs.getDouble(1));
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public double calcular_constancia(long idConceptoTipoPersonal, long idTrabajador, double unidades, String tipo, int codFrecuenciaPago, double jornadaDiaria, double jornadaSemanal, String formulaIntegral, String formulaSemanal, long idCargo, double valor, double topeMinimo, double topeMaximo) throws Exception {
    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select calcular_concepto_constancia(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idConceptoTipoPersonal);
      st.setLong(2, idTrabajador);
      st.setDouble(3, unidades);
      st.setString(4, tipo);
      st.setInt(5, codFrecuenciaPago);
      st.setDouble(6, jornadaDiaria);
      st.setDouble(7, jornadaSemanal);
      st.setString(8, formulaIntegral);
      st.setString(9, formulaSemanal);
      st.setLong(10, idCargo);
      st.setDouble(11, valor);
      st.setDouble(12, topeMinimo);
      st.setDouble(13, topeMaximo);

      rs = st.executeQuery();

      rs.next();
      return NumberTools.twoDecimal(rs.getDouble(1));
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }

  public double calcularProyectado(long idConceptoTipoPersonal, long idTrabajador, double unidades, String tipo, int codFrecuenciaPago, double jornadaDiaria, double jornadaSemanal, String formulaIntegral, String formulaSemanal, long idCargo, double valor, double topeMinimo, double topeMaximo)
    throws Exception
  {
    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select calcular_concepto_proyectado_movimiento (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idConceptoTipoPersonal);
      st.setLong(2, idTrabajador);
      st.setDouble(3, unidades);
      st.setString(4, tipo);
      st.setInt(5, codFrecuenciaPago);
      st.setDouble(6, jornadaDiaria);
      st.setDouble(7, jornadaSemanal);
      st.setString(8, formulaIntegral);
      st.setString(9, formulaSemanal);
      st.setLong(10, idCargo);
      st.setDouble(11, valor);
      st.setDouble(12, topeMinimo);
      st.setDouble(13, topeMaximo);

      rs = st.executeQuery();

      rs.next();
      return NumberTools.twoDecimal(rs.getDouble(1));
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public double calcularDocente(long idConceptoTipoPersonal, long idConceptoDocente, double unidades, String tipo, int codFrecuenciaPago, long idCargo, double valor, double topeMinimo, double topeMaximo, double horasSemanales)
    throws Exception
  {
    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select calcular_conceptodocente(?,?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idConceptoTipoPersonal);
      st.setLong(2, idConceptoDocente);
      st.setDouble(3, unidades);
      st.setString(4, tipo);
      st.setInt(5, codFrecuenciaPago);
      st.setLong(6, idCargo);
      st.setDouble(7, valor);
      st.setDouble(8, topeMinimo);
      st.setDouble(9, topeMaximo);
      st.setDouble(10, horasSemanales);

      rs = st.executeQuery();

      rs.next();
      return NumberTools.twoDecimal(rs.getDouble(1));
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}