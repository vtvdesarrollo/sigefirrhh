package sigefirrhh.personal.conceptos;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.StringTokenizer;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.TrabajadorFacadeExtend;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.Usuario;

public class CargaConceptosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargaConceptosForm.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private Collection listFrecuenciaPago;
  private Collection listTipoPersonal;
  private Collection listError;
  private long idFrecuenciaPago;
  private long idTipoPersonal;
  private String grabarConceptos;
  private String estatus = "A";
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private String accion;
  private UploadedFile archivo;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private TrabajadorFacadeExtend trabajadorFacade;
  private Collection temporalConceptos;
  private TipoPersonal tipoPersonal;
  private FrecuenciaPago frecuenciaPago;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal = new FrecuenciaTipoPersonal();
  private CalcularConceptoBeanBusiness calcularConceptoBeanBusines = new CalcularConceptoBeanBusiness();
  private File fileError;
  private boolean hasError;
  private StringBuffer sqlConcepto = new StringBuffer();
  private StringBuffer sqlTrabajador = new StringBuffer();
  private StringBuffer sqlConceptoFijo = new StringBuffer();
  private StringBuffer sqlConceptoVariable = new StringBuffer();
  private StringBuffer sqlActualizar = new StringBuffer();

  private StringBuffer sqlActualizarCargaMasiva = new StringBuffer();
  Statement stExecuteCargaMasiva = null;

  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  Statement stExecuteDelete = null;

  private ResultSet rsConcepto = null;
  private PreparedStatement stConcepto = null;
  private ResultSet rsTrabajador = null;
  private PreparedStatement stTrabajador = null;
  private ResultSet rsConceptoFijo = null;
  private PreparedStatement stConceptoFijo = null;
  private ResultSet rsConceptoVariable = null;
  private PreparedStatement stConceptoVariable = null;
  private long idConceptoTipoPersonal;
  private long idFrecuenciaTipoPersonal;
  private long idCargo;
  private double valor;
  private double topeMinimo;
  private double topeMaximo;
  private double jornadaDiaria;
  private double jornadaSemanal;
  private String formulaIntegral;
  private String formulaSemanal;
  private String tipo;
  private long idTrabajador;
  private java.util.Date fechaActual = new java.util.Date();
  java.sql.Date fechaActualSql = new java.sql.Date(this.fechaActual.getYear(), this.fechaActual.getMonth(), this.fechaActual.getDate());

  CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

  double montoMasMonto = 0.0D;

  public CargaConceptosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.trabajadorFacade = new TrabajadorFacadeExtend();
    try {
      this.connection.setAutoCommit(false);
      this.connection = Resource.getConnection();

      this.sqlConcepto.append("select ctp.id_concepto_tipo_personal, ctp.tipo, fp.cod_frecuencia_pago, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
      this.sqlConcepto.append(" from conceptotipopersonal ctp, concepto c, frecuenciatipopersonal ftp, frecuenciapago fp ");
      this.sqlConcepto.append(" where ctp.id_concepto = c.id_concepto ");
      this.sqlConcepto.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal ");
      this.sqlConcepto.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      this.sqlConcepto.append(" and c.cod_concepto = ?");
      this.sqlConcepto.append(" and ctp.id_tipo_personal = ?");
      this.stConcepto = this.connection.prepareStatement(
        this.sqlConcepto.toString(), 
        1003, 
        1007);

      this.sqlTrabajador.append("select t.id_trabajador, t.id_cargo, tu.jornada_diaria, tu.jornada_semanal, tp.formula_integral, tp.formula_semanal ");
      this.sqlTrabajador.append(" from trabajador t, turno tu, tipopersonal tp");
      this.sqlTrabajador.append(" where cedula = ? ");
      this.sqlTrabajador.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      this.sqlTrabajador.append(" and t.id_turno = tu.id_turno");
      this.sqlTrabajador.append(" and tp.id_tipo_personal = ? ");
      this.sqlTrabajador.append(" and t.estatus = ? ");

      this.stTrabajador = this.connection.prepareStatement(
        this.sqlTrabajador.toString(), 
        1003, 
        1007);

      this.sqlConceptoFijo.append("select id_concepto_fijo, monto, unidades ");
      this.sqlConceptoFijo.append(" from conceptofijo");
      this.sqlConceptoFijo.append(" where id_trabajador = ? ");
      this.sqlConceptoFijo.append(" and id_concepto_tipo_personal = ? ");
      this.sqlConceptoFijo.append(" and id_frecuencia_tipo_personal = ? ");
      this.stConceptoFijo = this.connection.prepareStatement(
        this.sqlConceptoFijo.toString(), 
        1003, 
        1007);

      this.sqlConceptoVariable.append("select id_concepto_variable, monto, unidades ");
      this.sqlConceptoVariable.append(" from conceptovariable");
      this.sqlConceptoVariable.append(" where id_trabajador = ? ");
      this.sqlConceptoVariable.append(" and id_concepto_tipo_personal = ? ");
      this.sqlConceptoVariable.append(" and id_frecuencia_tipo_personal = ? ");
      this.stConceptoVariable = this.connection.prepareStatement(
        this.sqlConceptoVariable.toString(), 
        1003, 
        1007);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      return;
    }
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String cargar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
    String codConcepto = null;

    Usuario usuario = this.login.getUsuarioObject();

    Calendar tiempo = Calendar.getInstance();
    tiempo.setTime(new java.util.Date());
    Time time = new Time(tiempo.getTimeInMillis());

    actualizarCampos();
    this.listError = new ArrayList();
    try {
      this.stExecute = null;
      this.stExecuteDelete = null;
      this.stExecuteCargaMasiva = null;
      this.stExecute = this.connection.createStatement();
      this.stExecuteCargaMasiva = this.connection.createStatement();
      this.stExecuteDelete = this.connection.createStatement();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }

    boolean success = new File(request.getRealPath("temp/error")).mkdirs();

    this.fileError = new File(
      ((ServletContext)context.getExternalContext().getContext())
      .getRealPath("/temp") + "/error" + newFileError(0) + ".txt");
    FileWriter fileErrorWriter = null;
    try {
      this.fileError.createNewFile();
      fileErrorWriter = new FileWriter(this.fileError);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    int i = 0;
    try {
      BufferedReader file = 
        new BufferedReader(
        new InputStreamReader(
        this.archivo.getInputStream()), 250);
      String linea = null;
      StringTokenizer lineaHelper = null;

      int cedula = 0;
      double monto = 0.0D;
      boolean error = false;
      double montoCalculado = 0.0D;

      this.hasError = false;
      int j = 0;
      while ((linea = file.readLine()) != null)
      {
        j++;

        log.error(" PROCESANDO LA LINEA " + j);
        lineaHelper = new StringTokenizer(linea, "\t");
        error = false;
        i++;
        codConcepto = lineaHelper.nextToken();

        log.error(" codConcepto= " + codConcepto);

        this.stConcepto.setString(1, codConcepto);
        this.stConcepto.setLong(2, this.tipoPersonal.getIdTipoPersonal());
        this.rsConcepto = this.stConcepto.executeQuery();

        if (!this.rsConcepto.next())
        {
          fileErrorWriter.write(
            "El concepto con el código " + codConcepto + " no existe para el tipo de personal " + this.tipoPersonal.getNombre() + ". Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idConceptoTipoPersonal = this.rsConcepto.getLong("id_concepto_tipo_personal");
          this.valor = this.rsConcepto.getDouble("valor");
          this.topeMinimo = this.rsConcepto.getDouble("tope_minimo");
          this.topeMaximo = this.rsConcepto.getDouble("tope_maximo");
          this.tipo = this.rsConcepto.getString("tipo");
        }

        cedula = Integer.parseInt(lineaHelper.nextToken());

        log.error(" cedula= " + cedula);

        this.rsConcepto.close();

        this.stTrabajador.setInt(1, cedula);
        this.stTrabajador.setLong(2, this.tipoPersonal.getIdTipoPersonal());
        this.stTrabajador.setString(3, this.estatus);
        this.rsTrabajador = this.stTrabajador.executeQuery();

        if (!this.rsTrabajador.next()) {
          fileErrorWriter.write(
            "El trabajador con la cédula " + cedula + " no existe para el tipo de personal " + this.tipoPersonal.getNombre() + ". Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idTrabajador = this.rsTrabajador.getLong("id_trabajador");
          this.idCargo = this.rsTrabajador.getLong("id_cargo");
        }

        this.rsTrabajador.close();
        if ((this.accion.equals("IP")) || (this.accion.equals("SP")))
          monto = Double.parseDouble(lineaHelper.nextToken());
        else {
          monto = NumberTools.twoDecimal(Double.parseDouble(lineaHelper.nextToken()));
        }

        log.error(" monto= " + monto);

        if (monto == 0.0D) {
          this.sqlActualizar = new StringBuffer();
          if (this.grabarConceptos.equals("F")) {
            this.sqlActualizar.append("delete from conceptofijo where id_trabajador = " + this.idTrabajador + " and id_concepto_tipo_personal = " + this.idConceptoTipoPersonal + " and id_frecuencia_tipo_personal = " + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());
            this.stExecuteDelete.addBatch(this.sqlActualizar.toString());
          } else {
            this.sqlActualizar.append("delete from conceptovariable where id_trabajador = " + this.idTrabajador + " and id_concepto_tipo_personal = " + this.idConceptoTipoPersonal + " and id_frecuencia_tipo_personal = " + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());
            this.stExecuteDelete.addBatch(this.sqlActualizar.toString());
          }
          this.stExecuteDelete.executeBatch();
        }

        if ((!error) && (monto != 0.0D)) {
          this.sqlActualizar = new StringBuffer();
          this.sqlActualizarCargaMasiva = new StringBuffer();

          log.error(" TRAZA 1 ");

          if (this.grabarConceptos.equals("F")) {
            this.stConceptoFijo.setLong(1, this.idTrabajador);
            this.stConceptoFijo.setLong(2, this.idConceptoTipoPersonal);
            this.stConceptoFijo.setLong(3, this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());
            this.rsConceptoFijo = this.stConceptoFijo.executeQuery();
          }
          else {
            this.stConceptoVariable.setLong(1, this.idTrabajador);
            this.stConceptoVariable.setLong(2, this.idConceptoTipoPersonal);
            this.stConceptoVariable.setLong(3, this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());
            this.rsConceptoVariable = this.stConceptoVariable.executeQuery();
          }

          log.error("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Fijo','" + codConcepto + "'," + monto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");

          log.error(" usuario.getIdUsuario()= " + usuario.getIdUsuario());
          log.error(" usuario.getIdOrganismo()= " + usuario.getIdOrganismo());
          log.error(" time= " + time);
          log.error(" fechaActualSql= " + this.fechaActualSql);
          log.error(" usuario.toString()= " + usuario.toString());
          log.error(" accion= " + this.accion);
          log.error(" cedula= " + cedula);
          log.error(" idTrabajador= " + this.idTrabajador);
          log.error(" codConcepto= " + codConcepto);
          log.error(" monto= " + monto);
          log.error(" idTrabajador= " + this.idTrabajador);
          log.error(" frecuencia= " + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal());

          log.error(" TRAZA 2 ");
          if (this.grabarConceptos.equals("F"))
          {
            log.error(" TRAZA 2.1 ");

            if (this.rsConceptoFijo.next()) {
              log.error(" TRAZA 2.1.1 ");
              if (this.accion.equals("M")) {
                this.sqlActualizar.append("update conceptofijo set monto = " + monto + " where id_concepto_fijo = " + this.rsConceptoFijo.getLong("id_concepto_fijo"));
                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Fijo','" + codConcepto + "'," + monto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
              else if (this.accion.equals("S")) {
                this.montoMasMonto = 0.0D;
                this.montoMasMonto = (monto + NumberTools.twoDecimal(this.rsConceptoFijo.getDouble("monto")));
                this.sqlActualizar.append("update conceptofijo set monto = " + this.montoMasMonto + " where id_concepto_fijo = " + this.rsConceptoFijo.getLong("id_concepto_fijo"));
                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Fijo','" + codConcepto + "'," + this.montoMasMonto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
              else if (this.accion.equals("SP")) {
                montoCalculado = this.calcularConceptoBeanBusiness.calcular(this.idConceptoTipoPersonal, this.idTrabajador, monto, this.tipo, this.frecuenciaTipoPersonal.getFrecuenciaPago().getCodFrecuenciaPago(), this.jornadaDiaria, this.jornadaSemanal, this.formulaIntegral, this.formulaSemanal, this.idCargo, this.valor, this.topeMinimo, this.topeMaximo);
                this.sqlActualizar.append("update conceptofijo set unidades = " + monto + ", monto = " + montoCalculado + " where id_concepto_fijo = " + this.rsConceptoFijo.getLong("id_concepto_fijo"));
                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Fijo','" + codConcepto + "'," + montoCalculado + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
            }
            else {
              log.error(" TRAZA 2.1.2 ");
              if ((this.accion.equals("IP")) || (this.accion.equals("SP")))
              {
                montoCalculado = this.calcularConceptoBeanBusiness.calcular(this.idConceptoTipoPersonal, this.idTrabajador, monto, this.tipo, this.frecuenciaTipoPersonal.getFrecuenciaPago().getCodFrecuenciaPago(), this.jornadaDiaria, this.jornadaSemanal, this.formulaIntegral, this.formulaSemanal, this.idCargo, this.valor, this.topeMinimo, this.topeMaximo);
                this.sqlActualizar.append("insert into conceptofijo (id_trabajador, ");
                this.sqlActualizar.append("id_concepto_tipo_personal, id_frecuencia_tipo_personal,");
                this.sqlActualizar.append("monto, unidades, estatus, fecha_registro, id_concepto_fijo) values(");
                this.sqlActualizar.append(this.idTrabajador + ", ");
                this.sqlActualizar.append(this.idConceptoTipoPersonal + ", ");
                this.sqlActualizar.append(this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ", " + montoCalculado + ", ");
                this.sqlActualizar.append(monto + ", 'A', '" + this.fechaActualSql + "', ");
                this.sqlActualizar.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");

                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Fijo','" + codConcepto + "'," + montoCalculado + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
              else
              {
                this.sqlActualizar.append("insert into conceptofijo (id_trabajador, ");
                this.sqlActualizar.append("id_concepto_tipo_personal, id_frecuencia_tipo_personal,");
                this.sqlActualizar.append("unidades, monto, estatus, fecha_registro, id_concepto_fijo) values(");
                this.sqlActualizar.append(this.idTrabajador + ", ");
                this.sqlActualizar.append(this.idConceptoTipoPersonal + ", ");
                this.sqlActualizar.append(this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ", 0, ");
                this.sqlActualizar.append(monto + ", 'A', '" + this.fechaActualSql + "', ");
                this.sqlActualizar.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");

                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Fijo','" + codConcepto + "'," + monto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
            }

            this.rsConceptoFijo.close();
          }
          else {
            log.error(" TRAZA 2.2 ");

            if (this.rsConceptoVariable.next()) {
              log.error(" TRAZA 2.2.1 ");
              if (this.accion.equals("M")) {
                this.sqlActualizar.append("update conceptovariable set monto = " + monto + " where id_concepto_variable = " + this.rsConceptoVariable.getLong("id_concepto_variable"));
                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Variable','" + codConcepto + "'," + monto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
              else if (this.accion.equals("S")) {
                this.montoMasMonto = 0.0D;
                this.montoMasMonto = (monto + this.rsConceptoVariable.getDouble("monto"));
                this.sqlActualizar.append("update variable set monto = " + this.montoMasMonto + " where id_concepto_variable = " + this.rsConceptoVariable.getLong("id_concepto_variable"));
                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Variable','" + codConcepto + "'," + this.montoMasMonto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
              else if (this.accion.equals("SP")) {
                montoCalculado = this.calcularConceptoBeanBusiness.calcular(this.idConceptoTipoPersonal, this.idTrabajador, monto, this.tipo, this.frecuenciaTipoPersonal.getFrecuenciaPago().getCodFrecuenciaPago(), this.jornadaDiaria, this.jornadaSemanal, this.formulaIntegral, this.formulaSemanal, this.idCargo, this.valor, this.topeMinimo, this.topeMaximo);
                this.sqlActualizar.append("update conceptovariable set unidades = " + monto + ", monto = " + montoCalculado + " where id_concepto_variable = " + this.rsConceptoVariable.getLong("id_concepto_variable"));
                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Variable','" + codConcepto + "'," + montoCalculado + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
            }
            else {
              log.error(" TRAZA 2.2.2 ");
              if ((this.accion.equals("IP")) || (this.accion.equals("SP"))) {
                montoCalculado = this.calcularConceptoBeanBusiness.calcular(this.idConceptoTipoPersonal, this.idTrabajador, monto, this.tipo, this.frecuenciaTipoPersonal.getFrecuenciaPago().getCodFrecuenciaPago(), this.jornadaDiaria, this.jornadaSemanal, this.formulaIntegral, this.formulaSemanal, this.idCargo, this.valor, this.topeMinimo, this.topeMaximo);
                this.sqlActualizar.append("insert into conceptovariable (id_trabajador, ");
                this.sqlActualizar.append("id_concepto_tipo_personal, id_frecuencia_tipo_personal,");
                this.sqlActualizar.append("monto, unidades ,estatus, fecha_registro, id_concepto_variable) values(");
                this.sqlActualizar.append(this.idTrabajador + ", ");
                this.sqlActualizar.append(this.idConceptoTipoPersonal + ", ");
                this.sqlActualizar.append(this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ", " + montoCalculado + ", ");
                this.sqlActualizar.append(monto + ", 'A', '" + this.fechaActualSql + "', ");
                this.sqlActualizar.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Variable','" + codConcepto + "'," + monto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
              else {
                this.sqlActualizar.append("insert into conceptovariable (id_trabajador, ");
                this.sqlActualizar.append("id_concepto_tipo_personal, id_frecuencia_tipo_personal,");
                this.sqlActualizar.append("unidades, monto, estatus, fecha_registro, id_concepto_variable) values(");
                this.sqlActualizar.append(this.idTrabajador + ", ");
                this.sqlActualizar.append(this.idConceptoTipoPersonal + ", ");
                this.sqlActualizar.append(this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ", 0, ");
                this.sqlActualizar.append(monto + ", 'A', '" + this.fechaActualSql + "', ");
                this.sqlActualizar.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");

                this.sqlActualizarCargaMasiva.append("INSERT INTO auditoriacargamasiva VALUES(" + usuario.getIdUsuario() + "," + usuario.getIdOrganismo() + ",'" + time + "','" + this.fechaActualSql + "','" + usuario.toString() + "','" + this.accion + "'," + cedula + "," + this.idTrabajador + ",'Concepto Variable','" + codConcepto + "'," + monto + "," + this.frecuenciaTipoPersonal.getIdFrecuenciaTipoPersonal() + ")");
              }
            }

            this.rsConceptoVariable.close();
          }

          log.error(" TRAZA 3 ");

          this.stExecute.addBatch(this.sqlActualizar.toString());
          this.stExecuteCargaMasiva.addBatch(this.sqlActualizarCargaMasiva.toString());

          log.error(" TRAZA 4");
          log.error(this.sqlActualizar.toString());
          log.error(this.sqlActualizarCargaMasiva.toString());
        }

      }

      if (this.hasError)
        context.addMessage("success", new FacesMessage("Se cargó con éxito. Pero hay registros con datos incoherentes. Este archivo podrá ser procesado sin embargo, las incoherencias no será procesadas. Baje el archivo a continuación para obtener mas detalle de los errores (use el botón derecho del mouse)."));
      else {
        context.addMessage("success", new FacesMessage("Se cargó con éxito"));
      }
      file.close();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.temporalConceptos = null;
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Archivo con error. Linea " + i, ""));
    }
    try {
      fileErrorWriter.close();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String getUrlError() {
    return this.fileError != null ? "/sigefirrhh/temp/" + this.fileError.getName() : null;
  }

  public static String newFileError(int fileId) {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(10000));
    while (id == fileId);
    return String.valueOf(id);
  }

  public String procesar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    actualizarCampos();
    try
    {
      this.stExecute.executeBatch();
      this.stExecute.close();

      this.stExecuteCargaMasiva.executeBatch();
      this.stExecuteCargaMasiva.close();

      this.stExecuteDelete.close();
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.tipoPersonal);

      context.addMessage("success", new FacesMessage("Se procesó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hay un error en el archivo", ""));
    }
    return null;
  }

  public void actualizarCampos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(
        this.idTipoPersonal);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", 
        new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ha seleccionado un tipo de personal", ""));
    }
    try {
      this.frecuenciaPago = this.definicionesFacade.findFrecuenciaPagoById(
        this.idFrecuenciaPago);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", 
        new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ha seleccionado una frecuencia pago", ""));
    }
    try {
      this.frecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(
        this.frecuenciaPago.getIdFrecuenciaPago(), this.tipoPersonal.getIdTipoPersonal());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", 
        new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ha seleccionado una frecuencia pago", ""));
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.frecuenciaPago = null;
    this.idFrecuenciaPago = 0L;
    this.listFrecuenciaPago = null;
    try
    {
      if (idTipoPersonal > 0L) {
        this.listFrecuenciaPago = 
          this.definicionesFacade.findFrecuenciaPagoByTipoPersonal(
          idTipoPersonal);

        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowFrecuenciaPago()
  {
    return (this.listFrecuenciaPago != null) && 
      (!this.listFrecuenciaPago.isEmpty());
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public UploadedFile getArchivo() {
    return this.archivo;
  }

  public String getGrabarConceptos() {
    return this.grabarConceptos;
  }

  public Collection getListFrecuenciaPago() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFrecuenciaPago, "sigefirrhh.base.definiciones.FrecuenciaPago");
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public String getIdFrecuenciaPago() {
    return String.valueOf(this.idFrecuenciaPago);
  }

  public void setIdFrecuenciaPago(String l) {
    this.idFrecuenciaPago = Long.parseLong(l);
  }

  public void setArchivo(UploadedFile file) {
    this.archivo = file;
  }

  public void setGrabarConceptos(String string) {
    this.grabarConceptos = string;
  }

  public void setListFrecuenciaPago(Collection collection) {
    this.listFrecuenciaPago = collection;
  }

  public void setListTipoPersonal(Collection collection) {
    this.listTipoPersonal = collection;
  }
  public String getAccion() {
    return this.accion;
  }

  public void setAccion(String string) {
    this.accion = string;
  }

  public Collection getListError() {
    return this.listError;
  }

  public void setListError(Collection collection) {
    this.listError = collection;
  }

  public boolean isHasError() {
    return this.hasError;
  }

  public void setHasError(boolean b) {
    this.hasError = b;
  }

  public String getEstatus() {
    return this.estatus;
  }

  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
}