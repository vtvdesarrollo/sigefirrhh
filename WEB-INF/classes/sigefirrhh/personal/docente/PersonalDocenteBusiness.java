package sigefirrhh.personal.docente;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoDependencia;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesBusinessExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.docente.SeguridadDocente;
import sigefirrhh.base.estructura.CaracteristicaDependencia;
import sigefirrhh.base.estructura.ClasificacionDependencia;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraNoGenBusiness;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;
import sigefirrhh.personal.docentes.ConceptoDocente;
import sigefirrhh.personal.registroCargos.AperturaEscolar;
import sigefirrhh.personal.registroCargos.RegistroDocente;
import sigefirrhh.personal.registroCargos.RegistroDocenteBeanBusiness;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class PersonalDocenteBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(PersonalDocenteBusiness.class.getName());

  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();
  private EstructuraNoGenBusiness estructuraBusiness = new EstructuraNoGenBusiness();
  private TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();
  private DefinicionesBusinessExtend definicionesBusinessExtend = new DefinicionesBusinessExtend();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public boolean validarDependenciaNivel(long idDependencia, String digitoNivel)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    connection.setAutoCommit(true);
    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    sql.append("select d.id_dependencia");
    sql.append(" from dependencia d, caracteristicadependencia car, clasificaciondependencia cla, tipocaracteristica tc");
    sql.append(" where d.id_dependencia = ?");
    sql.append(" and d.id_dependencia = cla.id_dependencia");
    sql.append(" and cla.id_caracteristica_dependencia = car.id_caracteristica_dependencia");
    sql.append(" and car.id_tipo_caracteristica = tc.id_tipo_caracteristica");
    sql.append(" and tc.cod_tipo_caracteristica = 'ND'");
    sql.append(" and car.codigo = ?");
    stRegistros = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);
    stRegistros.setLong(1, idDependencia);
    stRegistros.setString(2, digitoNivel);
    rsRegistros = stRegistros.executeQuery();

    if (rsRegistros.next())
    {
      return true;
    }
    connection.close(); connection = null;
    return false;
  }

  public void calcularConceptos(long idMovimiento, long idCargo, double horasSemanales) {
    ResultSet rsConceptosCalculados = null;
    PreparedStatement stConceptosCalculados = null;

    Statement stActualizarConceptosCalculados = null;
    Connection connection = Resource.getConnection();

    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();
    Statement stSueldoPromedio = null;

    String periodicidad = "Q";

    StringBuffer sql = new StringBuffer();
    sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, ctp.tipo, ctp.valor, ctp.tope_minimo, ctp.tope_maximo");
    sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
    sql.append("  concepto c, frecuenciapago fp");
    sql.append(" where");
    sql.append(" cf.id_trabajador = ? ");
    sql.append("  and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
    sql.append(" and ctp.id_concepto = c.id_concepto");
    sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
    sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
    sql.append(" and c.sueldo_basico = 'N'");
    sql.append(" and ctp.recalculo = 'S'");
    sql.append(" and ctp.cod_concepto not in('5001', '5003', '5004')");
    sql.append(" order by c.cod_concepto");
    try
    {
      connection.setAutoCommit(true);

      stActualizarConceptosCalculados = connection.createStatement();
      stSueldoPromedio = connection.createStatement();

      stConceptosCalculados = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptosCalculados.setLong(1, idMovimiento);
      rsConceptosCalculados = stConceptosCalculados.executeQuery();

      ResultSet rs = null;
      PreparedStatement st = null;

      for (; rsConceptosCalculados.next(); 
        connection = null)
      {
        double montoCalculado = 0.0D;
        sql = new StringBuffer();

        montoCalculado = calcularConceptoBeanBusiness.calcularDocente(
          rsConceptosCalculados.getLong("id_concepto_tipo_personal"), idMovimiento, 
          rsConceptosCalculados.getDouble("unidades"), rsConceptosCalculados.getString("tipo"), 
          rsConceptosCalculados.getInt("cod_frecuencia_pago"), idCargo, rsConceptosCalculados.getDouble("valor"), 
          rsConceptosCalculados.getDouble("tope_minimo"), rsConceptosCalculados.getDouble("tope_maximo"), horasSemanales);

        sql = new StringBuffer();
        sql.append("select update_monto_concepto_fijo(?, ?)");

        st = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        st.setLong(1, rsConceptosCalculados.getLong("id_concepto_fijo"));
        st.setDouble(2, montoCalculado);

        rs = st.executeQuery();
        connection.close();
      }
    }
    catch (Exception localException) {
    }
  }

  public Collection agregarPrimas(long id, long idTipoPersonal, long idTrabajador, String prima) {
    Collection col = new ArrayList();
    java.util.Date fecha = new java.util.Date();
    java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

    StringBuffer sql = new StringBuffer();
    PreparedStatement stConcepto = null;
    Connection connection = Resource.getConnection();
    ResultSet rsConcepto = null;
    ConceptoDocente conceptoDocente = null;
    try
    {
      sql.append("select ctp.id_concepto_tipo_personal ");
      sql.append("from conceptodocente cd, conceptotipopersonal ctp, concepto c");
      sql.append(" where cd.id_concepto_docente = ?");
      sql.append(" and cd.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.cod_concepto = ?");
      sql.append(" and ctp.id_tipo_personal = ?");

      stConcepto = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConcepto.setLong(1, id);
      stConcepto.setString(2, prima);
      stConcepto.setLong(3, idTipoPersonal);
      rsConcepto = stConcepto.executeQuery();

      if (!rsConcepto.next()) {
        this.log.error("1111111111yesssssssssssssssssssssss");
        sql = new StringBuffer();
        sql.append("select ctp.id_concepto_tipo_personal ");
        sql.append("from conceptotipopersonal ctp,  concepto c");
        sql.append(" where ");
        sql.append(" ctp.id_concepto = c.id_concepto");
        sql.append(" and ctp.id_tipo_personal = ?");
        sql.append(" and c.cod_concepto = ?");

        stConcepto = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stConcepto.setLong(1, idTipoPersonal);
        stConcepto.setString(2, prima);
        this.log.error("sql............." + sql.toString());
        rsConcepto = stConcepto.executeQuery();
        if (rsConcepto.next()) {
          this.log.error("777777777777777777777777777777");
          ConceptoTipoPersonal conceptoTipoPersonal = this.definicionesBusiness.findConceptoTipoPersonalById(rsConcepto.getLong("id_concepto_tipo_personal"));
          conceptoDocente = new ConceptoDocente();
          conceptoDocente.setVariable("N");
          conceptoDocente.setFechaRegistro(new java.util.Date());
          conceptoDocente.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
          conceptoDocente.setConceptoTipoPersonal(conceptoTipoPersonal);
          conceptoDocente.setUnidades(conceptoTipoPersonal.getUnidades());
          conceptoDocente.setMonto(0.0D);
          conceptoDocente.setIdMovimiento(id);
          sql = new StringBuffer();
          sql.append("select add_conceptodocente(?,?,?,?,?,?)");
          stConcepto = connection.prepareStatement(
            sql.toString(), 
            1003, 
            1007);

          stConcepto.setLong(1, id);
          stConcepto.setLong(2, conceptoDocente.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
          stConcepto.setLong(3, conceptoDocente.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
          stConcepto.setDouble(4, conceptoDocente.getUnidades());
          stConcepto.setDouble(5, conceptoDocente.getMonto());
          stConcepto.setDate(6, fechaSql);

          rsConcepto = stConcepto.executeQuery();
          rsConcepto.next();
          conceptoDocente.setIdConceptoDocente(rsConcepto.getLong(1));
          col.add(conceptoDocente);
          if (prima.equals("3031")) {
            sql = new StringBuffer();
            sql.append("delete from conceptodocente where id_movimiento = " + id);
            sql.append(" and id_concepto_tipo_personal in ");
            sql.append(" (select id_concepto_tipo_personal from conceptotipopersonal a, concepto b ");
            sql.append(" where a.id_tipo_personal = " + idTipoPersonal);
            sql.append(" and a.id_concepto = b.id_concepto");
            sql.append(" and b.cod_concepto = '3130')");
          }
        }
      }

      connection.close(); connection = null;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    return col;
  }

  public Collection agregarConceptosFijos(long id, long idTipoPersonal, double sueldoBasico, Dependencia dependencia)
  {
    Collection col = new ArrayList();
    try {
      java.util.Date fecha = new java.util.Date();
      java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

      Connection connection = Resource.getConnection();
      PreparedStatement stConceptos = null;
      ResultSet rsConceptos = null;
      StringBuffer sql = new StringBuffer();

      ClasificacionDependencia clasificacionDependencia = new ClasificacionDependencia();
      Collection colConceptoTipoPersonal = this.definicionesBusiness.findConceptoTipoPersonalForIngresoTrabajador(idTipoPersonal);
      Collection colConceptoCargo = new ArrayList();
      Collection colClasificacionDependencia = new ArrayList();

      colClasificacionDependencia = this.estructuraBusiness.findClasificacionDependenciaByDependencia(dependencia.getIdDependencia());

      Iterator iterClasificacionDependencia = colClasificacionDependencia.iterator();
      while (iterClasificacionDependencia.hasNext()) {
        clasificacionDependencia = (ClasificacionDependencia)iterClasificacionDependencia.next();
        colConceptoCargo.addAll(this.definicionesBusiness.findConceptoDependenciaByCaracteristicaDependencia(
          clasificacionDependencia.getCaracteristicaDependencia().getIdCaracteristicaDependencia()));
      }

      Iterator iterConceptoTipoPersonal = colConceptoTipoPersonal.iterator();

      ConceptoTipoPersonal conceptoTipoPersonal = null;
      this.log.error("col CTP " + colConceptoTipoPersonal.size());
      int idConceptoDocente = 0;
      while (iterConceptoTipoPersonal.hasNext()) {
        sql = new StringBuffer();
        idConceptoDocente++;
        Object obj = iterConceptoTipoPersonal.next();
        if ((obj instanceof ConceptoTipoPersonal))
          conceptoTipoPersonal = (ConceptoTipoPersonal)obj;
        else {
          conceptoTipoPersonal = ((ConceptoDependencia)obj).getConceptoTipoPersonal();
        }

        this.log.error(conceptoTipoPersonal.getConcepto().getDescripcion());
        ConceptoDocente conceptoDocente = new ConceptoDocente();
        conceptoDocente.setVariable("N");
        conceptoDocente.setFechaRegistro(new java.util.Date());
        conceptoDocente.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
        conceptoDocente.setConceptoTipoPersonal(conceptoTipoPersonal);
        conceptoDocente.setUnidades(conceptoTipoPersonal.getUnidades());
        conceptoDocente.setMonto(0.0D);
        conceptoDocente.setIdMovimiento(id);

        if (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0001")) {
          conceptoDocente.setMonto(sueldoBasico);
          if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
            conceptoDocente.setMonto(NumberTools.twoDecimal(conceptoDocente.getMonto() / 2.0D));
          }
        }

        sql.append("select add_conceptodocente(?,?,?,?,?,?)");
        stConceptos = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);

        stConceptos.setLong(1, id);
        stConceptos.setLong(2, conceptoDocente.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
        stConceptos.setLong(3, conceptoDocente.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
        stConceptos.setDouble(4, conceptoDocente.getUnidades());
        stConceptos.setDouble(5, conceptoDocente.getMonto());
        stConceptos.setDate(6, fechaSql);

        rsConceptos = stConceptos.executeQuery();
        rsConceptos.next();
        conceptoDocente.setIdConceptoDocente(rsConceptos.getLong(1));

        this.log.error("ConceptoDocente ---------" + conceptoDocente);
        this.log.error("IdConceptoDocente ---------" + conceptoDocente.getIdConceptoDocente());
        col.add(conceptoDocente);
      }
      connection.close(); connection = null;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return null;
    }
    return col;
  }

  public Collection agregarConceptosRetroactivos(long id, Collection colConceptosRetroactivo, SeguridadDocente seguridadDocente, int anioIngreso, int mesIngreso, int diaIngreso)
  {
    Connection connection = Resource.getConnection();
    PreparedStatement stConceptos = null;
    ResultSet rsConceptos = null;
    StringBuffer sql = new StringBuffer();
    Collection col = new ArrayList();

    java.util.Date fecha = new java.util.Date();
    java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());
    try
    {
      int diasRetroactivo = 0;
      int diasRetroactivoAnioAnterior = 0;

      if (seguridadDocente.getAnio() != anioIngreso)
      {
        diasRetroactivo = 30 + 30 * (seguridadDocente.getMes() - 2);
        diasRetroactivoAnioAnterior = 30 - (diaIngreso - 1) + 30 * (12 - (mesIngreso + 1));
      } else {
        diasRetroactivo = 30 - (diaIngreso - 1) + 30 * (seguridadDocente.getMes() - (mesIngreso + 1));
      }

      Iterator iterConceptosRetroactivo = colConceptosRetroactivo.iterator();
      while (iterConceptosRetroactivo.hasNext()) {
        ConceptoDocente conceptoDocente = (ConceptoDocente)iterConceptosRetroactivo.next();
        if (conceptoDocente.getConceptoTipoPersonal().getConcepto().getRetroactivo().equals("S")) {
          ConceptoDocente conceptoDocenteRetroactivo = new ConceptoDocente();
          try {
            conceptoDocenteRetroactivo.setConceptoTipoPersonal((ConceptoTipoPersonal)this.definicionesBusiness.findConceptoTipoPersonalByTipoPersonalAndConcepto(conceptoDocente.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal(), conceptoDocente.getConceptoTipoPersonal().getConcepto().getConceptoRetroactivo().getIdConcepto()).iterator().next());
          } catch (Exception e) {
            this.log.error("Excepcion controlada:", e);
            ErrorSistema error = new ErrorSistema();
            error.setDescription("Ocurrió un error buscando el concepto retroactivo para el concepto " + conceptoDocente.getConceptoTipoPersonal().getConcepto().toString());
            throw error;
          }
          this.log.error(".............conceptoTipoPersonalRetroactivo " + conceptoDocenteRetroactivo.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
          conceptoDocenteRetroactivo.setFechaRegistro(new java.util.Date());
          conceptoDocenteRetroactivo.setUnidades(diasRetroactivo);
          if (conceptoDocente.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
            conceptoDocenteRetroactivo.setMonto(NumberTools.twoDecimal(conceptoDocente.getMonto() / 15.0D * diasRetroactivo));
          else if (conceptoDocente.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() != 2) {
            conceptoDocenteRetroactivo.setMonto(NumberTools.twoDecimal(conceptoDocente.getMonto() / 30.0D * diasRetroactivo));
          }
          this.log.error("monto Concepto Retroactivo " + conceptoDocenteRetroactivo.getMonto());
          this.log.error("unidades Concepto Retroactivo" + conceptoDocenteRetroactivo.getUnidades());

          conceptoDocenteRetroactivo.setFrecuenciaTipoPersonal(this.definicionesBusinessExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(1, conceptoDocente.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal()));
          conceptoDocenteRetroactivo.setVariable("S");
          col.add(conceptoDocenteRetroactivo);

          sql = new StringBuffer();
          sql.append("select add_conceptodocente(?,?,?,?,?,?)");
          stConceptos = connection.prepareStatement(
            sql.toString(), 
            1003, 
            1007);

          stConceptos.setLong(1, id);
          stConceptos.setLong(2, conceptoDocenteRetroactivo.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
          stConceptos.setLong(3, conceptoDocenteRetroactivo.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
          stConceptos.setDouble(4, conceptoDocenteRetroactivo.getUnidades());
          stConceptos.setDouble(5, conceptoDocenteRetroactivo.getMonto());
          stConceptos.setDate(6, fechaSql);

          rsConceptos = stConceptos.executeQuery();
          rsConceptos.next();
          conceptoDocenteRetroactivo.setIdConceptoDocente(rsConceptos.getLong(1));

          this.log.error("ConceptoDocente ---------" + conceptoDocente);
          this.log.error("IdConceptoDocente ---------" + conceptoDocente.getIdConceptoDocente());

          if (diasRetroactivoAnioAnterior > 0) {
            conceptoDocenteRetroactivo = new ConceptoDocente();
            try {
              conceptoDocenteRetroactivo.setConceptoTipoPersonal((ConceptoTipoPersonal)this.definicionesBusiness.findConceptoTipoPersonalByTipoPersonalAndConcepto(conceptoDocente.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal(), conceptoDocente.getConceptoTipoPersonal().getConcepto().getConceptoRetroactivoAnterior().getIdConcepto()).iterator().next());
            } catch (Exception e) {
              this.log.error("Excepcion controlada:", e);
              ErrorSistema error = new ErrorSistema();
              error.setDescription("Ocurrió un error buscando el concepto retroactivo del año anterior para el concepto " + conceptoDocente.getConceptoTipoPersonal().getConcepto().toString());
              throw error;
            }
            this.log.error("conceptoTipoPersonal Retroactivo Anio Ant  " + conceptoDocenteRetroactivo.getConceptoTipoPersonal().getIdConceptoTipoPersonal());

            conceptoDocenteRetroactivo.setFechaRegistro(new java.util.Date());
            conceptoDocenteRetroactivo.setUnidades(diasRetroactivo);
            if (conceptoDocente.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
              conceptoDocenteRetroactivo.setMonto(NumberTools.twoDecimal(conceptoDocente.getMonto() / 15.0D * diasRetroactivoAnioAnterior));
            else if (conceptoDocente.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() != 2) {
              conceptoDocenteRetroactivo.setMonto(NumberTools.twoDecimal(conceptoDocente.getMonto() / 30.0D * diasRetroactivoAnioAnterior));
            }
            this.log.error("monto Concepto Retroactivo ANio Ant. " + conceptoDocenteRetroactivo.getMonto());
            this.log.error("unidades Concepto Retroactivo Anio Ant.  " + conceptoDocenteRetroactivo.getUnidades());
            conceptoDocenteRetroactivo.setFrecuenciaTipoPersonal(this.definicionesBusinessExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(1, conceptoDocente.getConceptoTipoPersonal().getTipoPersonal().getIdTipoPersonal()));

            col.add(conceptoDocenteRetroactivo);

            sql = new StringBuffer();
            sql.append("select add_conceptodocente(?,?,?,?,?,?)");
            stConceptos = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);

            stConceptos.setLong(1, id);
            stConceptos.setLong(2, conceptoDocenteRetroactivo.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
            stConceptos.setLong(3, conceptoDocenteRetroactivo.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal());
            stConceptos.setDouble(4, conceptoDocenteRetroactivo.getUnidades());
            stConceptos.setDouble(5, conceptoDocenteRetroactivo.getMonto());
            stConceptos.setDate(6, fechaSql);

            rsConceptos = stConceptos.executeQuery();
            rsConceptos.next();
            conceptoDocenteRetroactivo.setIdConceptoDocente(rsConceptos.getLong(1));
          }

        }

      }

      connection.close(); connection = null;
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    return col;
  }

  public void generarIngreso(RegistroDocente registroDocente, AperturaEscolar aperturaEscolar)
  {
    try {
      RegistroDocenteBeanBusiness registroDocenteBeanBusiness = new RegistroDocenteBeanBusiness();

      this.log.error("..........pasa RD2.2");
      registroDocenteBeanBusiness.addRegistroDocente(registroDocente);
      this.log.error("..........pasa RD3");
    }
    catch (Exception e) {
      this.log.error("..........pasa gm4.1");
      this.log.error("Excepcion controlada:", e);
    }

    this.log.error("..........pasa gm5");
  }
}