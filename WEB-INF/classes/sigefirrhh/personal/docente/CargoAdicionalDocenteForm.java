package sigefirrhh.personal.docente;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.Banco;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.docente.Asignatura;
import sigefirrhh.base.docente.CategoriaDocente;
import sigefirrhh.base.docente.CausaDocente;
import sigefirrhh.base.docente.DedicacionDocente;
import sigefirrhh.base.docente.DocenteNoGenFacade;
import sigefirrhh.base.docente.GradoDocente;
import sigefirrhh.base.docente.JerarquiaCategoriaDocente;
import sigefirrhh.base.docente.JerarquiaDocente;
import sigefirrhh.base.docente.NivelDocente;
import sigefirrhh.base.docente.SeguridadDocente;
import sigefirrhh.base.docente.TurnoDocente;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.docentes.ConceptoDocente;
import sigefirrhh.personal.docentes.DocentesNoGenFacade;
import sigefirrhh.personal.docentes.TrayectoriaDocente;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.AperturaEscolar;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.registroCargos.RegistroDocente;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;

public class CargoAdicionalDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargoAdicionalDocenteForm.class.getName());

  protected static final Map LISTA_SI_NO = new LinkedHashMap();

  protected static final Map LISTA_CONDICION = new LinkedHashMap();

  protected static final Map LISTA_TURNO = new LinkedHashMap();
  private LoginSession login;
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private EstructuraNoGenFacade estructuraFacade = new EstructuraNoGenFacade();
  private DocenteNoGenFacade docenteFacade = new DocenteNoGenFacade();
  private PersonalDocenteFacade personalDocenteFacade = new PersonalDocenteFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();
  private SeguridadDocente seguridadDocente = new SeguridadDocente();
  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private DocentesNoGenFacade docentesFacade = new DocentesNoGenFacade();
  private Collection colTipoPersonal;
  private Collection colRegion;
  private Collection colGradoDocente;
  private Collection colNivelDocente;
  private Collection colJerarquiaDocente;
  private Collection colJerarquiaCategoriaDocente;
  private Collection colTurnoDocente;
  private Collection colDedicacionDocente;
  private Collection colAsignatura;
  private Collection resultConceptoFijo;
  private Collection resultConceptoRetroactivo;
  private Collection resultCargosActuales;
  private String selectRegion;
  private String selectTipoPersonal;
  private String selectGradoDocente;
  private String selectNivelDocente;
  private String selectJerarquiaDocente;
  private String selectJerarquiaCategoriaDocente;
  private String selectTurnoDocente;
  private String selectDedicacionDocente;
  private String turno;
  private String condicion;
  private String selectAsignatura;
  private Date fechaMovimiento;
  private Tabulador tabulador;
  private DetalleTabulador detalleTabulador;
  private Cargo cargo;
  private double horasDocente = 0.0D;
  private double horasAdministrativas = 0.0D;
  private AperturaEscolar aperturaEscolar;
  private String mesNomina;
  private double totalHoras = 0.0D;
  private RegistroDocente registroDocente = new RegistroDocente();
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private String sexo;
  private String estadoCivil;
  private String nacionalidad;
  private Date fechaNacimiento;
  private String findCodDependencia;
  private Dependencia dependencia;
  private double sueldo;
  private String procedenciaHoras = "1";
  private String cuentaNomina;
  private String repetirCuentaNomina;
  DedicacionDocente dedicacionDocente = new DedicacionDocente();
  TurnoDocente turnoDocente = new TurnoDocente();
  GradoDocente gradoDocente = new GradoDocente();
  NivelDocente nivelDocente = new NivelDocente();
  JerarquiaDocente jerarquiaDocente = new JerarquiaDocente();
  CategoriaDocente categoriaDocente = new CategoriaDocente();

  Asignatura asignatura = null;

  TipoPersonal tipoPersonal = new TipoPersonal();
  Personal personal;
  Trabajador trabajador;
  Trabajador trabajadorActual;
  Banco bancoNomina;
  private int cedulaSustitucion = 0;
  private String nombreSustitucion;
  private String apellidoSustitucion;
  private Collection listSustitucion = new ArrayList();
  private boolean show1;
  private boolean show2;
  private boolean show3;
  private boolean show4;
  private boolean show5;
  private boolean show6;
  private boolean show7;
  private boolean show8;
  private boolean showDatosPersonales;
  private boolean showCargaHoraria;
  private boolean showConfirmacion;
  private boolean showSustitucion;
  private boolean showFinal;
  private StringBuffer codigo = new StringBuffer();
  private UIData tableConceptoRetroactivo;
  private long idModificar = 0L;
  private Collection colConceptoRetroactivoAnterior = new ArrayList();
  private ConceptoDocente conceptoDocenteModificar;
  private double montoAnterior = 0.0D;
  private CausaDocente causaDocente;
  private TrayectoriaDocente trayectoriaDocente;

  static
  {
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_CONDICION.put("1", "INTERINO");
    LISTA_CONDICION.put("2", "TITULAR");
    LISTA_TURNO.put("1", "DIURNO");
    LISTA_TURNO.put("2", "NOCTURNO");
  }

  public String editItem()
  {
    try
    {
      this.colConceptoRetroactivoAnterior = new ArrayList();
      this.colConceptoRetroactivoAnterior.addAll(this.resultConceptoRetroactivo);
      ConceptoDocente concepto = (ConceptoDocente)this.tableConceptoRetroactivo.getRowData();
      this.conceptoDocenteModificar = new ConceptoDocente();
      this.conceptoDocenteModificar.setIdConceptoDocente(concepto.getIdConceptoDocente());
      this.conceptoDocenteModificar.setMonto(concepto.getMonto());
      this.idModificar = this.conceptoDocenteModificar.getIdConceptoDocente();
      this.montoAnterior = this.conceptoDocenteModificar.getMonto();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String saveItem() {
    Iterator iter = this.colConceptoRetroactivoAnterior.iterator();

    this.resultConceptoRetroactivo = new ArrayList();

    while (iter.hasNext()) {
      ConceptoDocente concepto = (ConceptoDocente)iter.next();
      if ((this.conceptoDocenteModificar.getIdConceptoDocente() == concepto.getIdConceptoDocente()) && 
        (this.conceptoDocenteModificar.getMonto() <= this.montoAnterior)) {
        concepto.setMonto(this.conceptoDocenteModificar.getMonto());
      }
      this.resultConceptoRetroactivo.add(concepto);
    }
    this.idModificar = 0L;
    this.montoAnterior = 0.0D;
    this.conceptoDocenteModificar = null;
    return null;
  }
  public String undoItem() {
    this.idModificar = 0L;
    this.montoAnterior = 0.0D;
    this.conceptoDocenteModificar = null;

    return null;
  }

  public String next() {
    FacesContext context = FacesContext.getCurrentInstance();
    if (this.show1) {
      if (buscarTipoPersonalAndValidarFecha(context)) {
        this.showDatosPersonales = true;
        this.show1 = false;
        this.show2 = true;
      }
    } else if (this.show2) {
      this.show2 = false;
      this.show3 = true;
    } else if (this.show3) {
      if (this.dependencia == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar una dependencia para continuar", ""));
        return null;
      }
      if (validarDependenciaNivel(context)) {
        this.show3 = false;
        this.show4 = true;
      }
    } else if (this.show4) {
      findTurnoAndCategoria(context);
      this.show4 = false;
      this.show5 = true;
    } else if (this.show5) {
      if (findCargoAndTabulador(context)) {
        this.show5 = false;
        this.show6 = true;
      }
    } else if (this.show6) {
      if ((validarTopeHoras(context)) && 
        (calcularSueldo(context))) {
        this.show6 = false;
        this.show7 = true;
      }
    }
    else if (this.show7)
    {
      if (this.procedenciaHoras.equals("1"))
      {
        if (validarAperturaEscolar(context))
        {
          calcularConceptos(context);
          this.show7 = false;
          this.showFinal = true;
          this.showDatosPersonales = false;
        }
      }
      if (this.procedenciaHoras.equals("2")) {
        this.show7 = false;
        this.showSustitucion = true;
      }
    }
    else if (this.showSustitucion) {
      if (validarRegistroDocente(context))
      {
        calcularConceptos(context);
        this.showSustitucion = false;
        this.showFinal = true;
      }
    } else if (this.showFinal) {
      log.error("..........show8");

      if (!this.showConfirmacion) {
        this.showConfirmacion = true;
        log.error("..........show82");
      } else {
        log.error("..........show3");
        agregarTrabajador(context);
        agregarConceptos(context);
        if (generarMovimiento(context)) {
          log.error("..........show4");
          this.showFinal = false;
          this.show1 = true;
          abort();
        }
      }

    }

    return null;
  }
  public String findRegistroDocente() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      Collection col = this.registroCargosFacade.findRegistroDocenteByCedulaAndDependenciaAndSituacion(this.cedulaSustitucion, this.dependencia.getIdDependencia(), "V");
      if (col.size() > 0) {
        Iterator iter = col.iterator();
        while (iter.hasNext()) {
          RegistroDocente registroDocente = (RegistroDocente)iter.next();
          this.nombreSustitucion = (registroDocente.getTrabajador().getPersonal().getPrimerNombre() + " " + registroDocente.getTrabajador().getPersonal().getSegundoNombre());
          this.apellidoSustitucion = (registroDocente.getTrabajador().getPersonal().getPrimerApellido() + " " + registroDocente.getTrabajador().getPersonal().getSegundoApellido());
          RegistroDocenteAux registroDocenteAux = new RegistroDocenteAux();
          registroDocenteAux.setRegistroDocente(registroDocente);
          this.listSustitucion.add(registroDocenteAux);
        }
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar el personal", ""));
    }

    return null;
  }
  public boolean generarMovimiento(FacesContext context) {
    try {
      if (this.procedenciaHoras.equals("1")) {
        this.aperturaEscolar.setCargosRestante(this.aperturaEscolar.getCargosRestante() - 1);
        this.aperturaEscolar.setHorasRestante(this.aperturaEscolar.getHorasRestante() - this.totalHoras);
        this.registroCargosFacade.updateAperturaEscolar(this.aperturaEscolar);
        this.registroDocente.setAperturaEscolar(this.aperturaEscolar);
      }
      if (this.procedenciaHoras.equals("2")) {
        Iterator iter = this.listSustitucion.iterator();
        while (iter.hasNext()) {
          RegistroDocenteAux registroDocenteAux = (RegistroDocenteAux)iter.next();
          RegistroDocente registroDocente = this.registroCargosFacade.findRegistroDocenteById(registroDocenteAux.getRegistroDocente().getIdRegistroDocente());
          registroDocente.setHorasRestante(registroDocente.getHorasRestante() - registroDocenteAux.getCantidad());
          this.registroCargosFacade.updateRegistroDocente(registroDocente);
        }
      }

      this.registroDocente.setCargo(this.cargo);
      this.registroDocente.setDependencia(this.dependencia);
      this.registroDocente.setSituacion("2");
      this.registroDocente.setHorasAdministrativas(this.horasAdministrativas);
      this.registroDocente.setHorasDocente(this.totalHoras);
      this.registroDocente.setTotalHoras(this.totalHoras);
      this.registroDocente.setSituacion("O");
      this.registroDocente.setTrabajador(this.trabajador);
      this.registroDocente.setIdRegistroDocente(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.registroCargos.RegistroDocente"));
      this.personalDocenteFacade.generarIngreso(this.registroDocente, this.aperturaEscolar);

      this.trayectoriaDocente.setCausaDocente(this.causaDocente);
      this.trayectoriaDocente.setPersonal(this.personal);
      this.trayectoriaDocente.setFechaMovimiento(this.fechaMovimiento);
      this.trayectoriaDocente.setNumeroMovimiento(this.docentesFacade.findLastNumeroTrayectoria() + 1);
      this.trayectoriaDocente.setOrganismo(this.login.getOrganismo());
      this.trayectoriaDocente.setCodCargo(this.trabajador.getCodCargo());
      this.trayectoriaDocente.setCodDependencia(this.trabajador.getDependencia().getCodDependencia());
      this.trayectoriaDocente.setCodCausaDocente(this.causaDocente.getCodCausaDocente());
      this.trayectoriaDocente.setAnio(this.fechaMovimiento.getYear() + 1900);
      this.trayectoriaDocente.setMes(this.fechaMovimiento.getMonth() + 1);
      this.trayectoriaDocente.setNombreCargo(this.trabajador.getCargo().getDescripcionCargo());
      this.trayectoriaDocente.setNombreDespendencia(this.trabajador.getDependencia().getNombre());

      this.docentesFacade.addTrayectoriaDocente(this.trayectoriaDocente);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar el registro del tipo de personal", ""));
      return false;
    }
    return true;
  }
  public boolean buscarTipoPersonalAndValidarFecha(FacesContext context) {
    try {
      this.personal = ((Personal)this.expedienteFacade.findPersonalByCedula(this.cedula, this.login.getIdOrganismo()).iterator().next());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cedula no se encuentra registrada en el sistema", ""));
      return false;
    }
    try {
      this.resultCargosActuales = this.registroCargosFacade.findRegistroDocenteByPersonalAndEstatus(this.personal.getIdPersonal(), "A");
      log.error("cargos_actuales--------------------------" + this.resultCargosActuales.size());
      if ((this.resultCargosActuales == null) || (this.resultCargosActuales.isEmpty())) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona no posee cargos actualmente", ""));
        return false;
      }

      RegistroDocente registroDocenteActual = (RegistroDocente)this.resultCargosActuales.iterator().next();
      String codigoCargo = registroDocenteActual.getCargo().getCodCargo();
      this.gradoDocente = ((GradoDocente)this.docenteFacade.findGradoDocenteByDigitoGrado(codigoCargo.substring(0, 1)).iterator().next());
      this.jerarquiaDocente = ((JerarquiaDocente)this.docenteFacade.findJerarquiaDocenteByDigitoJerarquia(codigoCargo.substring(1, 2)).iterator().next());
      this.categoriaDocente = ((CategoriaDocente)this.docenteFacade.findCategoriaDocenteByDigitoCategoria(codigoCargo.substring(3, 4)).iterator().next());
      this.dedicacionDocente = ((DedicacionDocente)this.docenteFacade.findDedicacionDocenteByDigitoDedicacion("H").iterator().next());
      this.trabajadorActual = registroDocenteActual.getTrabajador();

      log.error(this.gradoDocente);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar los cargos actuales de la persona", ""));
      return false;
    }
    try {
      this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(Long.valueOf(this.selectTipoPersonal).longValue());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar el registro del tipo de personal", ""));
      return false;
    }
    try
    {
      if ((this.fechaMovimiento.getDate() != 1) && (this.fechaMovimiento.getDate() != 16)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de ingreso solo puede realizarse los dias 1 o 16", ""));
        return false;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de validar la fecha", ""));
      return false;
    }

    this.trayectoriaDocente = new TrayectoriaDocente();
    this.trayectoriaDocente.setIdTrayectoriaDocente(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.docentes.TrayectoriaDocente"));

    return true;
  }
  public String abort() {
    this.selectAsignatura = null;
    this.cedula = 0;
    this.primerApellido = null;
    this.segundoApellido = null;
    this.primerNombre = null;
    this.segundoNombre = null;
    this.fechaNacimiento = null;
    this.cuentaNomina = null;
    this.horasAdministrativas = 0.0D;
    this.horasDocente = 0.0D;
    this.totalHoras = 0.0D;

    return null;
  }
  private boolean validarPersonal(FacesContext context) {
    try {
      Collection colPersonal = this.expedienteFacade.findPersonalByCedula(this.cedula, this.login.getIdOrganismo());

      if (colPersonal.iterator().hasNext()) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona ya esta registrada en el sistema", ""));
        return false;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
    }
    return true;
  }

  private boolean validarAperturaEscolar(FacesContext context) {
    try {
      try {
        this.aperturaEscolar = ((AperturaEscolar)this.registroCargosFacade.findAperturaEscolarByCodCargo(this.codigo.toString()).iterator().next());
      } catch (Exception e) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se aperturaron horas para este cargo", ""));
        log.error("Excepcion controlada:", e);
        return false;
      }
      if (this.aperturaEscolar.getHorasRestante() < this.totalHoras) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las horas de la apertura escolar no cubren este cargo", ""));
        return false;
      }
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      log.error("Excepcion controlada:", e);
      return false;
    }
    return true;
  }
  private boolean validarRegistroDocente(FacesContext context) {
    try {
      int cantidad = 0;
      Iterator iter = this.listSustitucion.iterator();
      while (iter.hasNext()) {
        RegistroDocenteAux registroDocenteAux = (RegistroDocenteAux)iter.next();
        if (registroDocenteAux.getCantidad() > registroDocenteAux.getRegistroDocente().getHorasRestante()) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cantidad a sustituir es mayor a la cantidad restante", ""));
          return false;
        }

        cantidad += registroDocenteAux.getCantidad();
      }

      if (cantidad < this.totalHoras) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las horas que seleccionó no cubren el cargo", ""));
        return false;
      }
      if (cantidad > this.totalHoras) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las horas que seleccionó exeden a las requeridas para el cargo", ""));
        return false;
      }
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      log.error("Excepcion controlada:", e);
      return false;
    }
    return true;
  }

  private void agregarTrabajador(FacesContext context)
  {
    this.trabajador = new Trabajador();
    this.trabajador.setPersonal(this.personal);
    this.trabajador.setCargo(this.cargo);
    this.trabajador.setFechaIngreso(this.trabajadorActual.getFechaIngreso());
    this.trabajador.setDiaIngreso(this.trabajadorActual.getDiaIngreso());
    this.trabajador.setMesIngreso(this.trabajadorActual.getMesIngreso());
    this.trabajador.setAnioIngreso(this.trabajadorActual.getAnioIngreso());
    this.trabajador.setFechaVacaciones(this.trabajadorActual.getFechaVacaciones());
    this.trabajador.setDiaVacaciones(this.trabajadorActual.getDiaVacaciones());
    this.trabajador.setMesVacaciones(this.trabajadorActual.getMesVacaciones());
    this.trabajador.setAnioVacaciones(this.trabajadorActual.getAnioVacaciones());
    this.trabajador.setFechaPrestaciones(this.trabajadorActual.getFechaPrestaciones());
    this.trabajador.setDiaPrestaciones(this.trabajadorActual.getDiaPrestaciones());
    this.trabajador.setMesPrestaciones(this.trabajadorActual.getMesPrestaciones());
    this.trabajador.setAnioPrestaciones(this.trabajadorActual.getAnioPrestaciones());
    this.trabajador.setCedula(this.cedula);
    this.trabajador.setTurno(this.tipoPersonal.getTurno());
    this.trabajador.setBancoNomina(this.trabajadorActual.getBancoNomina());
    this.trabajador.setCodCargo(this.cargo.getCodCargo());
    this.trabajador.setCodTipoPersonal(this.tipoPersonal.getCodTipoPersonal());
    this.trabajador.setEstatus("A");
    this.trabajador.setSueldoBasico(this.sueldo);
    this.trabajador.setTipoCtaNomina(this.trabajadorActual.getTipoCtaNomina());
    this.trabajador.setCotizaFju(this.tipoPersonal.getCotizaFju());
    this.trabajador.setCotizaLph(this.tipoPersonal.getCotizaLph());
    this.trabajador.setCotizaSpf(this.tipoPersonal.getCotizaSpf());
    this.trabajador.setCotizaSso(this.tipoPersonal.getCotizaSso());
    this.trabajador.setPaso(0);
    this.trabajador.setSituacion("1");
    this.trabajador.setCuentaNomina(this.trabajadorActual.getCuentaNomina());
    this.trabajador.setTipoPersonal(this.tipoPersonal);
    this.trabajador.setOrganismo(this.login.getOrganismo());
    this.trabajador.setHorasSemanales(this.totalHoras);
    this.trabajador.setDependencia(this.dependencia);

    this.trabajador.setIdTrabajador(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.Trabajador"));
    try {
      this.trabajadorFacade.addTrabajador(this.trabajador);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de agregar el registro en trabajador", ""));
    }
  }

  private boolean agregarConceptos(FacesContext context)
  {
    Iterator iter = this.resultConceptoFijo.iterator();

    while (iter.hasNext()) {
      ConceptoDocente conceptoDocente = (ConceptoDocente)iter.next();
      ConceptoFijo conceptoFijo = new ConceptoFijo();
      conceptoFijo.setConceptoTipoPersonal(conceptoDocente.getConceptoTipoPersonal());
      conceptoFijo.setEstatus("A");
      conceptoFijo.setFechaComienzo(Calendar.getInstance().getTime());
      conceptoFijo.setFechaRegistro(Calendar.getInstance().getTime());
      conceptoFijo.setFrecuenciaTipoPersonal(conceptoDocente.getFrecuenciaTipoPersonal());
      conceptoFijo.setMonto(conceptoDocente.getMonto());
      conceptoFijo.setMontoAnterior(0.0D);
      conceptoFijo.setMontoRestituir(0.0D);
      conceptoFijo.setRestituir("N");
      conceptoFijo.setTrabajador(this.trabajador);
      conceptoFijo.setUnidades(conceptoDocente.getUnidades());
      conceptoFijo.setUnidadesRestituir(0.0D);
      conceptoFijo.setIdConceptoFijo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
      try {
        this.trabajadorFacade.addConceptoFijo(conceptoFijo);
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de agregar los conceptos fijos al trabajador", ""));
      }

    }

    iter = this.resultConceptoRetroactivo.iterator();

    while (iter.hasNext()) {
      ConceptoDocente conceptoDocente = (ConceptoDocente)iter.next();
      ConceptoVariable conceptoVariable = new ConceptoVariable();
      conceptoVariable.setConceptoTipoPersonal(conceptoDocente.getConceptoTipoPersonal());
      conceptoVariable.setEstatus("A");
      conceptoVariable.setFechaRegistro(Calendar.getInstance().getTime());
      conceptoVariable.setFrecuenciaTipoPersonal(conceptoDocente.getFrecuenciaTipoPersonal());
      conceptoVariable.setMonto(conceptoDocente.getMonto());
      conceptoVariable.setTrabajador(this.trabajador);
      conceptoVariable.setUnidades(conceptoDocente.getUnidades());
      conceptoVariable.setIdConceptoVariable(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable"));
      try {
        this.trabajadorFacade.addConceptoVariable(conceptoVariable);
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de agregar los conceptos retroactivos al trabajador", ""));
      }

    }

    return true;
  }

  private boolean calcularConceptos(FacesContext context)
  {
    try
    {
      this.resultConceptoFijo = this.personalDocenteFacade.agregarConceptosFijos(this.trayectoriaDocente.getIdTrayectoriaDocente(), Long.valueOf(this.selectTipoPersonal).longValue(), this.sueldo, this.dependencia);
      this.resultConceptoFijo = this.calcularSueldosPromedioBeanBusiness.calcularConceptoDocente(this.resultConceptoFijo, this.cargo.getIdCargo(), this.totalHoras);

      this.resultConceptoRetroactivo = this.personalDocenteFacade.agregarConceptosRetroactivos(this.trayectoriaDocente.getIdTrayectoriaDocente(), this.resultConceptoFijo, this.seguridadDocente, this.fechaMovimiento.getYear() + 1900, this.fechaMovimiento.getMonth() + 1, this.fechaMovimiento.getDate());
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error en la asignación de los conceptos", ""));
      log.error("Excepcion controlada:", e);
      return false;
    }

    return true;
  }

  public boolean findCargoAndTabulador(FacesContext context) {
    try {
      this.nivelDocente = this.docenteFacade.findNivelDocenteById(Long.valueOf(this.selectNivelDocente).longValue());

      this.turnoDocente = this.docenteFacade.findTurnoDocenteById(Long.valueOf(this.selectTurnoDocente).longValue());

      this.codigo = new StringBuffer();
      this.codigo.append(this.gradoDocente.getDigitoGrado());
      this.codigo.append(this.jerarquiaDocente.getDigitoJerarquia());
      this.codigo.append(this.nivelDocente.getDigitoNivel());
      this.codigo.append(this.categoriaDocente.getDigitoCategoria());
      this.codigo.append(this.turnoDocente.getDigitoTurno());
      this.codigo.append(this.dedicacionDocente.getDigitoDedicacion());

      log.error("CODIGO CARGO : " + this.codigo);
      try {
        this.cargo = this.cargoFacade.findCargoByCodCargoAndCodManualCargo(this.codigo.toString(), 999);
      } catch (Exception e) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "EL cargo no esta registrado", ""));
        log.error("Excepcion controlada:", e);
        return false;
      }
      try {
        this.detalleTabulador = this.cargoFacade.findDetalleTabuladorForRegistroCargos(this.tabulador.getIdTabulador(), this.cargo.getGrado(), this.cargo.getSubGrado(), 1);
      } catch (Exception e) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El detalle tabulador para este cargo no esta registrado", ""));
        log.error("Excepcion controlada:", e);
        return false;
      }
      if ((this.dedicacionDocente.getDigitoDedicacion().equals("I")) || (this.dedicacionDocente.getDigitoDedicacion().equals("C")))
        this.showCargaHoraria = true;
      else
        this.showCargaHoraria = false;
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurriò un error", ""));
      log.error("Excepcion controlada:", e);
      return false;
    }
    return true;
  }

  public void findTurnoAndCategoria(FacesContext context)
  {
    try
    {
      this.colTurnoDocente = this.docenteFacade.findTurnoDocenteByTurnoAndCondicion(this.turno, this.condicion);
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurriò un error", ""));
      log.error("Excepcion controlada:", e);
    }
  }

  public void buscarMesNomina() {
    try {
      SeguridadDocente seguridadDocente = (SeguridadDocente)this.docenteFacade.findSeguridadDocenteByCerrado("N").iterator().next();
      this.mesNomina = (" Mes: " + seguridadDocente.getMes() + " Año: " + seguridadDocente.getAnio());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDependenciaByCodigoAndRegion() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.dependencia = null;
      this.dependencia = this.estructuraFacade.findDependenciaByCodigoAndRegion(this.findCodDependencia, Long.valueOf(this.selectRegion).longValue());
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Dependencia no existe", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  private boolean validarDependenciaNivel(FacesContext context) {
    try {
      NivelDocente nivelDocente = this.docenteFacade.findNivelDocenteById(Long.valueOf(this.selectNivelDocente).longValue());
      boolean dependenciaValida = this.personalDocenteFacade.validarDependenciaNivel(this.dependencia.getIdDependencia(), nivelDocente.getDigitoNivel());

      if (!dependenciaValida) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Dependencia no corresponde al nivel indicado", ""));
      }
      if (!this.selectAsignatura.equals("0")) {
        this.asignatura = this.docenteFacade.findAsignaturaById(Long.valueOf(this.selectAsignatura).longValue());
      }
      if (((nivelDocente.getDigitoNivel().equals("3")) || (nivelDocente.getDigitoNivel().equals("4")) || 
        (nivelDocente.getDigitoNivel().equals("5")) || (nivelDocente.getDigitoNivel().equals("7")) || 
        (nivelDocente.getDigitoNivel().equals("8")) || (nivelDocente.getDigitoNivel().equals("9"))) && (this.asignatura == null))
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "EL Nivel obliga seleccionar una materia", ""));
      return false;
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      log.error("Excepcion controlada:", e);
    }return false;
  }

  private boolean validarTopeHoras(FacesContext context)
  {
    try
    {
      if (this.dedicacionDocente.getDigitoDedicacion().equals("H")) {
        if (this.horasAdministrativas + this.horasDocente > this.turnoDocente.getTopeCargaSemanal()) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El total de horas supera el tope semamal del cargo", ""));
          return false;
        }
        this.totalHoras = (this.horasAdministrativas + this.horasDocente);
      } else {
        this.totalHoras = this.detalleTabulador.getCargaHoraria();
      }
      if (this.totalHoras > 20.0D) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No pueden ser mas de 20 horas ", ""));
        return false;
      }

      double tope = 36.0D;
      double totalHorasActuales = 0.0D;
      if (this.asignatura != null) {
        tope = this.asignatura.getTopeHoras();
        log.error("tope_asignatura actual " + tope);
      }

      Iterator iter = this.resultCargosActuales.iterator();

      while (iter.hasNext()) {
        RegistroDocente registroDocente = (RegistroDocente)iter.next();
        totalHorasActuales += registroDocente.getTotalHoras();
        log.error("tope_horas_actuales " + totalHorasActuales);
        log.error("toral_horas_registro " + registroDocente.getTotalHoras());
        if (tope < registroDocente.getTotalHoras()) {
          tope = registroDocente.getTotalHoras();
        }
      }
      log.error("toral_horas_actuales+total_horas " + (totalHorasActuales + this.totalHoras));
      log.error("tope_final " + tope);
      if (totalHorasActuales + this.totalHoras > tope) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "EL total tope de horas no permite agregar la cantidad de horas seleccionadas", ""));
        return false;
      }
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      log.error("Excepcion controlada:", e);
      return false;
    }
    return true;
  }

  private boolean calcularSueldo(FacesContext context) {
    try {
      if (this.dedicacionDocente.getDigitoDedicacion().equals("H"))
        this.sueldo = ((this.horasAdministrativas + this.horasDocente) * this.detalleTabulador.getSueldoHora() * 2.0D);
      else
        this.sueldo = this.detalleTabulador.getMonto();
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      log.error("Excepcion controlada:", e);
      return false;
    }
    return true;
  }
  public boolean isShowFindDependencia() {
    return this.dependencia != null;
  }

  public CargoAdicionalDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraNoGenFacade();
    this.docenteFacade = new DocenteNoGenFacade();
    this.personalDocenteFacade = new PersonalDocenteFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
      }

      public PhaseId getPhaseId()
      {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }
  public void refresh() {
    try {
      this.show1 = true;
      this.colTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridadDocenteIngreso(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.colRegion = this.estructuraFacade.findAllRegion();

      this.colGradoDocente = this.docenteFacade.findAllGradoDocente();
      this.colNivelDocente = this.docenteFacade.findAllNivelDocente();
      this.colAsignatura = this.docenteFacade.findAllAsignatura();

      this.jerarquiaDocente = ((JerarquiaDocente)this.docenteFacade.findJerarquiaDocenteByDigitoJerarquia("1").iterator().next());
      this.colDedicacionDocente = this.docenteFacade.findAllDedicacionDocente();
      this.tabulador = ((Tabulador)this.cargoFacade.findTabuladorByCodTabulador("999", this.login.getIdOrganismo()).iterator().next());
      this.seguridadDocente = ((SeguridadDocente)this.docenteFacade.findSeguridadDocenteByCerrado("N").iterator().next());
      this.causaDocente = ((CausaDocente)this.docenteFacade.findCausaDocenteByCodCausaDocente("40").iterator().next());

      buscarMesNomina();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.colTipoPersonal = new ArrayList();
      this.colRegion = new ArrayList();
    }
  }

  public Collection getListTurno() {
    Collection col = new ArrayList();

    Iterator iterEntry = LISTA_TURNO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListCondicion() {
    Collection col = new ArrayList();

    Iterator iterEntry = LISTA_CONDICION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }

    return col;
  }
  public Collection getColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public Collection getColGradoDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.colGradoDocente.iterator();
    GradoDocente gradoDocente = null;
    while (iterator.hasNext()) {
      gradoDocente = (GradoDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(gradoDocente.getIdGradoDocente()), 
        gradoDocente.toString()));
    }
    return col;
  }
  public Collection getColNivelDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelDocente.iterator();
    NivelDocente nivelDocente = null;
    while (iterator.hasNext()) {
      nivelDocente = (NivelDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelDocente.getIdNivelDocente()), 
        nivelDocente.toString()));
    }
    return col;
  }
  public Collection getColJerarquiaDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.colJerarquiaDocente.iterator();
    JerarquiaDocente jerarquiaDocente = null;
    while (iterator.hasNext()) {
      jerarquiaDocente = (JerarquiaDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(jerarquiaDocente.getIdJerarquiaDocente()), 
        jerarquiaDocente.toString()));
    }
    return col;
  }
  public Collection getColJerarquiaCategoriaDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.colJerarquiaCategoriaDocente.iterator();
    JerarquiaCategoriaDocente jerarquiaCategoriaDocente = null;
    while (iterator.hasNext()) {
      jerarquiaCategoriaDocente = (JerarquiaCategoriaDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(jerarquiaCategoriaDocente.getIdJerarquiaCategoriaDocente()), 
        jerarquiaCategoriaDocente.toString()));
    }
    return col;
  }
  public Collection getColTurnoDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTurnoDocente.iterator();
    TurnoDocente turnoDocente = null;
    while (iterator.hasNext()) {
      turnoDocente = (TurnoDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(turnoDocente.getIdTurnoDocente()), 
        turnoDocente.toString()));
    }
    return col;
  }
  public Collection getColDedicacionDocente() {
    Collection col = new ArrayList();
    Iterator iterator = this.colDedicacionDocente.iterator();
    DedicacionDocente dedicacionDocente = null;
    while (iterator.hasNext()) {
      dedicacionDocente = (DedicacionDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dedicacionDocente.getIdDedicacionDocente()), 
        dedicacionDocente.toString()));
    }
    return col;
  }
  public Collection getColAsignatura() {
    Collection col = new ArrayList();
    Iterator iterator = this.colAsignatura.iterator();
    Asignatura asignatura = null;
    while (iterator.hasNext()) {
      asignatura = (Asignatura)iterator.next();
      col.add(new SelectItem(
        String.valueOf(asignatura.getIdAsignatura()), 
        asignatura.toString()));
    }
    return col;
  }
  public int getCedula() {
    return this.cedula;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String selectRegion) {
    this.selectRegion = selectRegion;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String selectTipoPersonal) {
    this.selectTipoPersonal = selectTipoPersonal;
  }
  public boolean isShow1() {
    return this.show1;
  }
  public void setShow1(boolean show1) {
    this.show1 = show1;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public void setLogin(LoginSession login) {
    this.login = login;
  }
  public String getEstadoCivil() {
    return this.estadoCivil;
  }
  public void setEstadoCivil(String estadoCivil) {
    this.estadoCivil = estadoCivil;
  }
  public Date getFechaNacimiento() {
    return this.fechaNacimiento;
  }
  public void setFechaNacimiento(Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }
  public String getMesNomina() {
    return this.mesNomina;
  }
  public void setMesNomina(String mesNomina) {
    this.mesNomina = mesNomina;
  }
  public String getNacionalidad() {
    return this.nacionalidad;
  }
  public void setNacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }
  public String getPrimerNombre() {
    return this.primerNombre;
  }
  public void setPrimerNombre(String primerNombre) {
    this.primerNombre = primerNombre;
  }
  public String getSegundoApellido() {
    return this.segundoApellido;
  }
  public void setSegundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }
  public String getSegundoNombre() {
    return this.segundoNombre;
  }
  public void setSegundoNombre(String segundoNombre) {
    this.segundoNombre = segundoNombre;
  }
  public String getSexo() {
    return this.sexo;
  }
  public void setSexo(String sexo) {
    this.sexo = sexo;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public void setShow2(boolean show2) {
    this.show2 = show2;
  }
  public String getPrimerApellido() {
    return this.primerApellido;
  }
  public void setPrimerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
  }
  public boolean isShow3() {
    return this.show3;
  }
  public void setShow3(boolean show3) {
    this.show3 = show3;
  }
  public String getFindCodDependencia() {
    return this.findCodDependencia;
  }
  public void setFindCodDependencia(String findCodDependencia) {
    this.findCodDependencia = findCodDependencia;
  }
  public String getSelectAsignatura() {
    return this.selectAsignatura;
  }
  public void setSelectAsignatura(String selectAsignatura) {
    this.selectAsignatura = selectAsignatura;
  }
  public String getSelectGradoDocente() {
    return this.selectGradoDocente;
  }
  public void setSelectGradoDocente(String selectGradoDocente) {
    this.selectGradoDocente = selectGradoDocente;
  }
  public String getSelectNivelDocente() {
    return this.selectNivelDocente;
  }
  public void setSelectNivelDocente(String selectNivelDocente) {
    this.selectNivelDocente = selectNivelDocente;
  }
  public Dependencia getDependencia() {
    return this.dependencia;
  }
  public boolean isShow4() {
    return this.show4;
  }
  public void setShow4(boolean show4) {
    this.show4 = show4;
  }
  public boolean isShowDatosPersonales() {
    return this.showDatosPersonales;
  }
  public void setShowDatosPersonales(boolean showDatosPersonales) {
    this.showDatosPersonales = showDatosPersonales;
  }
  public boolean isShow5() {
    return this.show5;
  }
  public void setShow5(boolean show5) {
    this.show5 = show5;
  }
  public String getSelectJerarquiaCategoriaDocente() {
    return this.selectJerarquiaCategoriaDocente;
  }

  public void setSelectJerarquiaCategoriaDocente(String selectJerarquiaCategoriaDocente) {
    this.selectJerarquiaCategoriaDocente = selectJerarquiaCategoriaDocente;
  }
  public String getSelectJerarquiaDocente() {
    return this.selectJerarquiaDocente;
  }
  public void setSelectJerarquiaDocente(String selectJerarquiaDocente) {
    this.selectJerarquiaDocente = selectJerarquiaDocente;
  }
  public String getSelectDedicacionDocente() {
    return this.selectDedicacionDocente;
  }
  public void setSelectDedicacionDocente(String selectDedicacionDocente) {
    this.selectDedicacionDocente = selectDedicacionDocente;
  }
  public String getSelectTurnoDocente() {
    return this.selectTurnoDocente;
  }
  public void setSelectTurnoDocente(String selectTurnoDocente) {
    this.selectTurnoDocente = selectTurnoDocente;
  }
  public String getCondicion() {
    return this.condicion;
  }
  public void setCondicion(String condicion) {
    this.condicion = condicion;
  }
  public String getTurno() {
    return this.turno;
  }
  public void setTurno(String turno) {
    this.turno = turno;
  }
  public double getHorasAdministrativas() {
    return this.horasAdministrativas;
  }
  public void setHorasAdministrativas(double horasAdministrativas) {
    this.horasAdministrativas = horasAdministrativas;
  }
  public double getHorasDocente() {
    return this.horasDocente;
  }
  public void setHorasDocente(double horasDocente) {
    this.horasDocente = horasDocente;
  }
  public Cargo getCargo() {
    return this.cargo;
  }
  public DetalleTabulador getDetalleTabulador() {
    return this.detalleTabulador;
  }
  public boolean isShow6() {
    return this.show6;
  }
  public void setShow6(boolean show6) {
    this.show6 = show6;
  }
  public boolean isShowCargaHoraria() {
    return this.showCargaHoraria;
  }
  public void setShowCargaHoraria(boolean showCargaHoraria) {
    this.showCargaHoraria = showCargaHoraria;
  }
  public String getCuentaNomina() {
    return this.cuentaNomina;
  }
  public void setCuentaNomina(String cuentaNomina) {
    this.cuentaNomina = cuentaNomina;
  }
  public String getProcedenciaHoras() {
    return this.procedenciaHoras;
  }
  public void setProcedenciaHoras(String procedenciaHoras) {
    this.procedenciaHoras = procedenciaHoras;
  }
  public double getSueldo() {
    return this.sueldo;
  }

  public void setSueldo(double sueldo) {
    this.sueldo = sueldo;
  }
  public boolean isShow7() {
    return this.show7;
  }
  public void setShow7(boolean show7) {
    this.show7 = show7;
  }
  public boolean isShow8() {
    return this.show8;
  }
  public void setShow8(boolean show8) {
    this.show8 = show8;
  }
  public Collection getResultConceptoFijo() {
    return this.resultConceptoFijo;
  }
  public void setResultConceptoFijo(Collection resultConceptoFijo) {
    this.resultConceptoFijo = resultConceptoFijo;
  }
  public Collection getResultConceptoRetroactivo() {
    return this.resultConceptoRetroactivo;
  }
  public void setResultConceptoRetroactivo(Collection resultConceptoRetroactivo) {
    this.resultConceptoRetroactivo = resultConceptoRetroactivo;
  }
  public double getTotalHoras() {
    return this.totalHoras;
  }
  public Banco getBancoNomina() {
    return this.bancoNomina;
  }
  public void setBancoNomina(Banco bancoNomina) {
    this.bancoNomina = bancoNomina;
  }
  public DedicacionDocente getDedicacionDocente() {
    return this.dedicacionDocente;
  }
  public TurnoDocente getTurnoDocente() {
    return this.turnoDocente;
  }
  public CategoriaDocente getCategoriaDocente() {
    return this.categoriaDocente;
  }
  public GradoDocente getGradoDocente() {
    return this.gradoDocente;
  }
  public JerarquiaDocente getJerarquiaDocente() {
    return this.jerarquiaDocente;
  }
  public NivelDocente getNivelDocente() {
    return this.nivelDocente;
  }
  public boolean isShowConfirmacion() {
    return this.showConfirmacion;
  }

  public String getRepetirCuentaNomina() {
    return this.repetirCuentaNomina;
  }
  public void setRepetirCuentaNomina(String repetirCuentaNomina) {
    this.repetirCuentaNomina = repetirCuentaNomina;
  }
  public boolean isShowSustitucion() {
    return this.showSustitucion;
  }
  public boolean isShowFinal() {
    return this.showFinal;
  }
  public int getCedulaSustitucion() {
    return this.cedulaSustitucion;
  }
  public void setCedulaSustitucion(int cedulaSustitucion) {
    this.cedulaSustitucion = cedulaSustitucion;
  }
  public Collection getListSustitucion() {
    return this.listSustitucion;
  }
  public String getApellidoSustitucion() {
    return this.apellidoSustitucion;
  }
  public void setApellidoSustitucion(String apellidoSustitucion) {
    this.apellidoSustitucion = apellidoSustitucion;
  }
  public String getNombreSustitucion() {
    return this.nombreSustitucion;
  }
  public void setNombreSustitucion(String nombreSustitucion) {
    this.nombreSustitucion = nombreSustitucion;
  }
  public Collection getResultCargosActuales() {
    return this.resultCargosActuales;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public long getIdModificar() {
    return this.idModificar;
  }
  public void setIdModificar(long idModificar) {
    this.idModificar = idModificar;
  }
  public ConceptoDocente getConceptoDocenteModificar() {
    return this.conceptoDocenteModificar;
  }
  public void setConceptoDocenteModificar(ConceptoDocente conceptoDocenteModificar) {
    this.conceptoDocenteModificar = conceptoDocenteModificar;
  }
  public UIData getTableConceptoRetroactivo() {
    return this.tableConceptoRetroactivo;
  }
  public void setTableConceptoRetroactivo(UIData tableConceptoRetroactivo) {
    this.tableConceptoRetroactivo = tableConceptoRetroactivo;
  }
}