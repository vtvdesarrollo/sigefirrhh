package sigefirrhh.personal.docente;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;
import sigefirrhh.personal.docentes.DocentesBusiness;
import sigefirrhh.personal.docentes.TrayectoriaDocente;

public class DocentesFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DocentesBusiness docentesBusiness = new DocentesBusiness();

  public void addTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.docentesBusiness.addTrayectoriaDocente(trayectoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente) throws Exception
  {
    try { this.txn.open();
      this.docentesBusiness.updateTrayectoriaDocente(trayectoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTrayectoriaDocente(TrayectoriaDocente trayectoriaDocente) throws Exception
  {
    try { this.txn.open();
      this.docentesBusiness.deleteTrayectoriaDocente(trayectoriaDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TrayectoriaDocente findTrayectoriaDocenteById(long trayectoriaDocenteId) throws Exception
  {
    try { this.txn.open();
      TrayectoriaDocente trayectoriaDocente = 
        this.docentesBusiness.findTrayectoriaDocenteById(trayectoriaDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(trayectoriaDocente);
      return trayectoriaDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTrayectoriaDocente() throws Exception
  {
    try { this.txn.open();
      return this.docentesBusiness.findAllTrayectoriaDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaDocenteByCausaDocente(long idCausaDocente, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docentesBusiness.findTrayectoriaDocenteByCausaDocente(idCausaDocente, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaDocenteByCodCausaDocente(String codCausaDocente, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docentesBusiness.findTrayectoriaDocenteByCodCausaDocente(codCausaDocente, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaDocenteByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docentesBusiness.findTrayectoriaDocenteByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTrayectoriaDocenteByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.docentesBusiness.findTrayectoriaDocenteByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}