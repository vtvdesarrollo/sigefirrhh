package sigefirrhh.personal.docente;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.docente.CausaDocente;
import sigefirrhh.base.docente.DocenteFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.docentes.DocentesNoGenFacade;
import sigefirrhh.personal.docentes.TrayectoriaDocente;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;

public class ModificacionDatosDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ModificacionDatosDocenteForm.class.getName());

  protected static final Map LISTA_ESTADO_CIVIL = new LinkedHashMap();

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();
  private DocenteFacade docenteFacade = new DocenteFacade();
  private DocentesNoGenFacade docentesFacade = new DocentesNoGenFacade();
  private Collection colTipoPersonal;
  private Collection resultCargosActuales;
  private String selectRegion;
  private String selectTipoPersonal;
  private String tipoModificacion = "1";
  private Date fechaMovimiento;
  private int cedula;
  private int nuevaCedula;
  private String nuevoPrimerApellido;
  private String nuevoSegundoApellido;
  private String nuevoPrimerNombre;
  private String nuevoSegundoNombre;
  private String nuevoEstadoCivil;
  private Date nuevaFechaNacimiento;
  TipoPersonal tipoPersonal = new TipoPersonal();
  Personal personal;
  Trabajador trabajador;
  private boolean show1;
  private boolean showCedula;
  private boolean showNombre;
  private boolean showEstadoCivil;
  private boolean showFechaNacimiento;
  private boolean showConfirmacion;
  private CausaDocente causaDocente;

  static
  {
    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
    LISTA_ESTADO_CIVIL.put("D", "DIVORCIADO(A)");
    LISTA_ESTADO_CIVIL.put("V", "VIUDO(A)");
    LISTA_ESTADO_CIVIL.put("U", "CONCUBINO(A)");
    LISTA_ESTADO_CIVIL.put("O", "OTRO");
  }

  public String next()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    if ((this.show1) && 
      (buscarTipoPersonalAndValidarFecha(context))) {
      this.show1 = false;
      if (this.tipoModificacion.equals("1")) {
        this.showCedula = true;
        this.nuevaCedula = this.cedula;
      } else if (this.tipoModificacion.equals("2")) {
        this.showNombre = true;
        this.nuevoPrimerApellido = this.personal.getPrimerApellido();
        this.nuevoSegundoApellido = this.personal.getSegundoApellido();
        this.nuevoPrimerNombre = this.personal.getPrimerNombre();
        this.nuevoSegundoNombre = this.personal.getSegundoNombre();
      } else if (this.tipoModificacion.equals("3")) {
        this.showEstadoCivil = true;
        this.nuevoEstadoCivil = this.personal.getEstadoCivil();
      } else if (this.tipoModificacion.equals("4")) {
        this.showFechaNacimiento = true;
        this.nuevaFechaNacimiento = this.personal.getFechaNacimiento();
      }

    }

    return null;
  }

  public String preGenerate() {
    this.showConfirmacion = true;

    return null;
  }
  public String generate() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      Connection connection = Resource.getConnection();
      Statement stExecute = null;

      StringBuffer sql = new StringBuffer();

      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      stExecute = connection.createStatement();

      ResultSet rsRegistros = null;
      PreparedStatement stRegistros = null;

      if (this.tipoModificacion.equals("1")) {
        sql.append(" select count(*) as cantidad ");
        sql.append(" from personal ");
        sql.append(" where cedula = ?  ");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setInt(1, this.nuevaCedula);

        rsRegistros = stRegistros.executeQuery();
        rsRegistros.next();

        if (rsRegistros.getInt("cantidad") > 0) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "NO SE REALIZO EL MOVIMIENTO. La cedula ya se encuentra registrada", ""));
          abort();
          return null;
        }
        sql = new StringBuffer();
        sql.append("update personal set cedula =  " + this.nuevaCedula + " where id_personal = " + this.personal.getIdPersonal() + "; ");
        sql.append("update trabajador set cedula =  " + this.nuevaCedula + " where id_personal = " + this.personal.getIdPersonal() + "; ");
      }
      else if (this.tipoModificacion.equals("2"))
      {
        sql.append(" update personal set primer_apellido =  '" + this.nuevoPrimerApellido + "', ");
        sql.append("                     segundo_apellido =  '" + this.nuevoSegundoApellido + "', ");
        sql.append("                     primer_nombre =  '" + this.nuevoPrimerNombre + "', ");
        sql.append("                     segundo_nombre =  '" + this.nuevoSegundoNombre + "' ");
        sql.append(" where id_personal = " + this.personal.getIdPersonal() + "; ");
      }
      else if (this.tipoModificacion.equals("3"))
      {
        sql.append("update personal set estado_civil =  '" + this.nuevoEstadoCivil + "' where id_personal = " + this.personal.getIdPersonal() + "; ");
      }
      else if (this.tipoModificacion.equals("4"))
      {
        sql.append("update personal set fecha_nacimiento =  '" + this.nuevaFechaNacimiento + "' where id_personal = " + this.personal.getIdPersonal() + "; ");
      }

      log.error(sql.toString());

      stExecute.addBatch(sql.toString());
      stExecute.executeBatch();

      stExecute.close();

      TrayectoriaDocente trayectoriaDocente = new TrayectoriaDocente();
      trayectoriaDocente.setCausaDocente(this.causaDocente);
      trayectoriaDocente.setPersonal(this.personal);
      trayectoriaDocente.setFechaMovimiento(this.fechaMovimiento);
      trayectoriaDocente.setNumeroMovimiento(this.docentesFacade.findLastNumeroTrayectoria() + 1);
      trayectoriaDocente.setIdTrayectoriaDocente(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.docentes.TrayectoriaDocente"));
      trayectoriaDocente.setOrganismo(this.login.getOrganismo());

      this.docentesFacade.addTrayectoriaDocente(trayectoriaDocente);

      context.addMessage("success_modify", new FacesMessage("Se realizó con éxito"));

      connection.commit();
      connection.close(); connection = null;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }

    abort();

    return null;
  }

  public boolean buscarTipoPersonalAndValidarFecha(FacesContext context) {
    try {
      this.personal = ((Personal)this.expedienteFacade.findPersonalByCedula(this.cedula, this.login.getIdOrganismo()).iterator().next());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cedula no se encuentra registrada en el sistema", ""));
      return false;
    }
    try {
      this.resultCargosActuales = this.registroCargosFacade.findRegistroDocenteByPersonalAndEstatus(this.personal.getIdPersonal(), "A");
      if ((this.resultCargosActuales == null) || (this.resultCargosActuales.isEmpty())) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona no posee cargos actualmente", ""));
        return false;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar los cargos actuales de la persona", ""));
      return false;
    }
    try {
      this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(Long.valueOf(this.selectTipoPersonal).longValue());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar el registro del tipo de personal", ""));
      return false;
    }

    return true;
  }
  public String abort() {
    this.cedula = 0;
    this.showConfirmacion = false;
    this.show1 = true;
    this.showCedula = false;
    this.showNombre = false;
    this.showFechaNacimiento = false;
    this.showEstadoCivil = false;

    return null;
  }

  public ModificacionDatosDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
      }

      public PhaseId getPhaseId()
      {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }
  public void refresh() {
    try {
      this.show1 = true;
      this.colTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridadDocenteIngreso(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
      this.causaDocente = ((CausaDocente)this.docenteFacade.findCausaDocenteByCodCausaDocente("50").iterator().next());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.colTipoPersonal = new ArrayList();
    }
  }

  public Collection getListEstadoCivil() {
    Collection col = new ArrayList();

    Iterator iterEntry = LISTA_ESTADO_CIVIL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }

    return col;
  }

  public int getCedula() {
    return this.cedula;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String selectRegion) {
    this.selectRegion = selectRegion;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String selectTipoPersonal) {
    this.selectTipoPersonal = selectTipoPersonal;
  }
  public boolean isShow1() {
    return this.show1;
  }
  public void setShow1(boolean show1) {
    this.show1 = show1;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public void setLogin(LoginSession login) {
    this.login = login;
  }

  public boolean isShowConfirmacion() {
    return this.showConfirmacion;
  }

  public Collection getResultCargosActuales() {
    return this.resultCargosActuales;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public int getNuevaCedula() {
    return this.nuevaCedula;
  }
  public void setNuevaCedula(int nuevaCedula) {
    this.nuevaCedula = nuevaCedula;
  }
  public Date getNuevaFechaNacimiento() {
    return this.nuevaFechaNacimiento;
  }
  public void setNuevaFechaNacimiento(Date nuevaFechaNacimiento) {
    this.nuevaFechaNacimiento = nuevaFechaNacimiento;
  }
  public String getNuevoEstadoCivil() {
    return this.nuevoEstadoCivil;
  }
  public void setNuevoEstadoCivil(String nuevoEstadoCivil) {
    this.nuevoEstadoCivil = nuevoEstadoCivil;
  }
  public String getNuevoPrimerApellido() {
    return this.nuevoPrimerApellido;
  }
  public void setNuevoPrimerApellido(String nuevoPrimerApellido) {
    this.nuevoPrimerApellido = nuevoPrimerApellido;
  }
  public String getNuevoPrimerNombre() {
    return this.nuevoPrimerNombre;
  }
  public void setNuevoPrimerNombre(String nuevoPrimerNombre) {
    this.nuevoPrimerNombre = nuevoPrimerNombre;
  }
  public String getNuevoSegundoApellido() {
    return this.nuevoSegundoApellido;
  }
  public void setNuevoSegundoApellido(String nuevoSegundoApellido) {
    this.nuevoSegundoApellido = nuevoSegundoApellido;
  }
  public String getNuevoSegundoNombre() {
    return this.nuevoSegundoNombre;
  }
  public void setNuevoSegundoNombre(String nuevoSegundoNombre) {
    this.nuevoSegundoNombre = nuevoSegundoNombre;
  }
  public boolean isShowCedula() {
    return this.showCedula;
  }
  public boolean isShowNombre() {
    return this.showNombre;
  }
  public String getTipoModificacion() {
    return this.tipoModificacion;
  }
  public void setTipoModificacion(String tipoModificacion) {
    this.tipoModificacion = tipoModificacion;
  }
  public boolean isShowEstadoCivil() {
    return this.showEstadoCivil;
  }
  public boolean isShowFechaNacimiento() {
    return this.showFechaNacimiento;
  }
}