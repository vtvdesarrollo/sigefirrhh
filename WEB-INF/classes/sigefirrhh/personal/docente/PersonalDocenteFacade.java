package sigefirrhh.personal.docente;

import eforserver.business.AbstractFacade;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import sigefirrhh.base.docente.SeguridadDocente;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.personal.registroCargos.AperturaEscolar;
import sigefirrhh.personal.registroCargos.RegistroDocente;

public class PersonalDocenteFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PersonalDocenteBusiness personalDocenteBusiness = new PersonalDocenteBusiness();

  public boolean validarDependenciaNivel(long idDependencia, String digitoNivel)
    throws Exception
  {
    return this.personalDocenteBusiness.validarDependenciaNivel(idDependencia, digitoNivel);
  }

  public Collection agregarConceptosFijos(long id, long idTipoPersonal, double sueldoBasico, Dependencia dependencia) throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalDocenteBusiness.agregarConceptosFijos(id, idTipoPersonal, sueldoBasico, dependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection agregarPrimas(long id, long idTipoPersonal, long idTrabajador, String prima) throws Exception
  {
    try {
      this.txn.open();
      return this.personalDocenteBusiness.agregarPrimas(id, idTipoPersonal, idTrabajador, prima);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection agregarConceptosRetroactivos(long id, Collection colConceptosRetroactivo, SeguridadDocente seguridadDocente, int anioIngreso, int mesIngreso, int diaIngreso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.personalDocenteBusiness.agregarConceptosRetroactivos(id, colConceptosRetroactivo, seguridadDocente, anioIngreso, mesIngreso, diaIngreso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void generarIngreso(RegistroDocente registroDocente, AperturaEscolar aperturaEscolar) throws Exception
  {
    try { this.txn.open();
      this.personalDocenteBusiness.generarIngreso(registroDocente, aperturaEscolar);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}