package sigefirrhh.personal.docente;

import sigefirrhh.personal.registroCargos.RegistroDocente;

public class RegistroDocenteAux
{
  private RegistroDocente registroDocente;
  private int cantidad;

  public int getCantidad()
  {
    return this.cantidad;
  }
  public void setCantidad(int cantidad) {
    this.cantidad = cantidad;
  }
  public RegistroDocente getRegistroDocente() {
    return this.registroDocente;
  }
  public void setRegistroDocente(RegistroDocente registroDocente) {
    this.registroDocente = registroDocente;
  }
}