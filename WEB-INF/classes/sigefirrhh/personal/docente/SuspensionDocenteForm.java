package sigefirrhh.personal.docente;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.registroCargos.RegistroDocente;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;

public class SuspensionDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SuspensionDocenteForm.class.getName());
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();
  private Collection colTipoPersonal;
  private Collection resultCargosActuales;
  private String selectRegion;
  private String selectTipoPersonal;
  private String tipoMovimiento = "1";
  private Date fechaMovimiento;
  private int cedula;
  TipoPersonal tipoPersonal = new TipoPersonal();
  Personal personal;
  Trabajador trabajador;
  Trabajador trabajadorActual;
  private boolean show1;
  private boolean show2;
  private boolean show3;
  private boolean showConfirmacion;
  private UIData tableCargosActuales;
  private RegistroDocente cargoSuspender;

  public String next()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    if ((this.show1) && 
      (buscarTipoPersonalAndValidarFecha(context))) {
      this.show1 = false;
      if (this.tipoMovimiento.equals("1"))
        this.show2 = true;
      else {
        this.show3 = true;
      }

    }

    return null;
  }

  public String preGenerate() {
    this.showConfirmacion = true;

    return null;
  }
  public String generate() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.tipoMovimiento.equals("1")) {
        Trabajador trabajador = this.trabajadorFacade.findTrabajadorById(this.cargoSuspender.getTrabajador().getIdTrabajador());
        RegistroDocente registroDocente = this.registroCargosFacade.findRegistroDocenteById(this.cargoSuspender.getIdRegistroDocente());

        trabajador.setEstatus("S");
        registroDocente.setSituacion("C");

        this.trabajadorFacade.updateTrabajador(trabajador);
        this.registroCargosFacade.updateRegistroDocente(registroDocente);
      } else {
        Iterator iter = this.resultCargosActuales.iterator();
        while (iter.hasNext()) {
          RegistroDocente registroDocente = (RegistroDocente)iter.next();
          Trabajador trabajador = this.trabajadorFacade.findTrabajadorById(registroDocente.getTrabajador().getIdTrabajador());
          registroDocente = this.registroCargosFacade.findRegistroDocenteById(registroDocente.getIdRegistroDocente());

          trabajador.setEstatus("S");
          registroDocente.setSituacion("C");

          this.trabajadorFacade.updateTrabajador(trabajador);
          this.registroCargosFacade.updateRegistroDocente(registroDocente);
        }
      }
      context.addMessage("success_modify", new FacesMessage("Se realizó con éxito"));
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }

    this.showConfirmacion = false;
    this.show1 = true;
    this.show2 = false;
    this.show3 = false;
    abort();

    return null;
  }
  public String seleccionar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      log.error("wwwwwwwwwwwwww");
      this.cargoSuspender = ((RegistroDocente)this.tableCargosActuales.getRowData());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    return null;
  }
  public boolean buscarTipoPersonalAndValidarFecha(FacesContext context) {
    try {
      this.personal = ((Personal)this.expedienteFacade.findPersonalByCedula(this.cedula, this.login.getIdOrganismo()).iterator().next());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cedula no se encuentra registrada en el sistema", ""));
      return false;
    }
    try {
      this.resultCargosActuales = this.registroCargosFacade.findRegistroDocenteByPersonalAndEstatus(this.personal.getIdPersonal(), "A");
      if ((this.resultCargosActuales == null) || (this.resultCargosActuales.isEmpty())) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona no posee cargos actualmente", ""));
        return false;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar los cargos actuales de la persona", ""));
      return false;
    }
    try {
      this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(Long.valueOf(this.selectTipoPersonal).longValue());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de buscar el registro del tipo de personal", ""));
      return false;
    }
    try
    {
      if ((this.fechaMovimiento.getDate() != 1) && (this.fechaMovimiento.getDate() != 16)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de ingreso solo puede realizarse los dias 1 o 16", ""));
        return false;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error al tratar de validar la fecha", ""));
      return false;
    }

    return true;
  }
  public String abort() {
    this.cedula = 0;

    return null;
  }
  private boolean validarPersonal(FacesContext context) {
    try {
      Collection colPersonal = this.expedienteFacade.findPersonalByCedula(this.cedula, this.login.getIdOrganismo());

      if (colPersonal.iterator().hasNext()) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona ya esta registrada en el sistema", ""));
        return false;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
    }
    return true;
  }

  public SuspensionDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
      }

      public PhaseId getPhaseId()
      {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }
  public void refresh() {
    try {
      this.show1 = true;
      this.colTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridadDocenteIngreso(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.colTipoPersonal = new ArrayList();
    }
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }

    return col;
  }

  public int getCedula() {
    return this.cedula;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String selectRegion) {
    this.selectRegion = selectRegion;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String selectTipoPersonal) {
    this.selectTipoPersonal = selectTipoPersonal;
  }
  public boolean isShow1() {
    return this.show1;
  }
  public void setShow1(boolean show1) {
    this.show1 = show1;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public void setLogin(LoginSession login) {
    this.login = login;
  }

  public boolean isShow2() {
    return this.show2;
  }
  public void setShow2(boolean show2) {
    this.show2 = show2;
  }

  public boolean isShowConfirmacion() {
    return this.showConfirmacion;
  }

  public Collection getResultCargosActuales() {
    return this.resultCargosActuales;
  }
  public Personal getPersonal() {
    return this.personal;
  }

  public UIData getTableCargosActuales() {
    return this.tableCargosActuales;
  }

  public void setTableCargosActuales(UIData tableCargosActuales) {
    this.tableCargosActuales = tableCargosActuales;
  }
  public String getTipoMovimiento() {
    return this.tipoMovimiento;
  }
  public void setTipoMovimiento(String tipoMovimiento) {
    this.tipoMovimiento = tipoMovimiento;
  }
  public boolean isShow3() {
    return this.show3;
  }
  public RegistroDocente getCargoSuspender() {
    return this.cargoSuspender;
  }
}