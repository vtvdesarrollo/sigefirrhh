package sigefirrhh.personal.registroCargos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.Registro;

public class HistoricoCargos
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_MOVIMIENTO;
  protected static final Map LISTA_SITUACION;
  private long idHistoricoCargos;
  private Registro registro;
  private int codigoNomina;
  private String situacion;
  private String movimiento;
  private Date fechaMovimiento;
  private CausaMovimiento causaMovimiento;
  private Cargo cargo;
  private Dependencia dependencia;
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private double horas;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cargo", "causaMovimiento", "cedula", "codigoNomina", "dependencia", "fechaMovimiento", "horas", "idHistoricoCargos", "movimiento", "primerApellido", "primerNombre", "registro", "segundoApellido", "segundoNombre", "situacion" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.util.Date"), Double.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.registro.Registro"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 26, 26, 21, 21, 26, 21, 21, 24, 21, 21, 21, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.registroCargos.HistoricoCargos"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoCargos());

    LISTA_MOVIMIENTO = 
      new LinkedHashMap();
    LISTA_SITUACION = 
      new LinkedHashMap();
    LISTA_MOVIMIENTO.put("1", "CREACION");
    LISTA_MOVIMIENTO.put("2", "ACTUALIZACION");
    LISTA_MOVIMIENTO.put("3", "CONGELACION");
    LISTA_MOVIMIENTO.put("4", "ELIMINACION");
    LISTA_SITUACION.put("O", "OCUPADO");
    LISTA_SITUACION.put("V", "VACANTE");
    LISTA_SITUACION.put("E", "ELIMINADO");
    LISTA_SITUACION.put("C", "CONGELADO");
    LISTA_SITUACION.put("T", "TRANSFERIDO");
  }

  public HistoricoCargos()
  {
    jdoSetsituacion(this, "O");

    jdoSetmovimiento(this, "1");

    jdoSethoras(this, 8.0D);
  }

  public String toString()
  {
    return jdoGetcodigoNomina(this) + " - " + 
      jdoGetcargo(this).getDescripcionCargo() + " - " + 
      LISTA_MOVIMIENTO.get(jdoGetmovimiento(this)) + " - " + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaMovimiento(this));
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }
  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }
  public CausaMovimiento getCausaMovimiento() {
    return jdoGetcausaMovimiento(this);
  }
  public void setCausaMovimiento(CausaMovimiento causaMovimiento) {
    jdoSetcausaMovimiento(this, causaMovimiento);
  }
  public int getCedula() {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }
  public int getCodigoNomina() {
    return jdoGetcodigoNomina(this);
  }
  public void setCodigoNomina(int codigoNomina) {
    jdoSetcodigoNomina(this, codigoNomina);
  }
  public Dependencia getDependencia() {
    return jdoGetdependencia(this);
  }
  public void setDependencia(Dependencia dependencia) {
    jdoSetdependencia(this, dependencia);
  }
  public Date getFechaMovimiento() {
    return jdoGetfechaMovimiento(this);
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    jdoSetfechaMovimiento(this, fechaMovimiento);
  }
  public double getHoras() {
    return jdoGethoras(this);
  }
  public void setHoras(double horas) {
    jdoSethoras(this, horas);
  }
  public long getIdHistoricoCargos() {
    return jdoGetidHistoricoCargos(this);
  }
  public void setIdHistoricoCargos(long idHistoricoCargos) {
    jdoSetidHistoricoCargos(this, idHistoricoCargos);
  }
  public String getMovimiento() {
    return jdoGetmovimiento(this);
  }
  public void setMovimiento(String movimiento) {
    jdoSetmovimiento(this, movimiento);
  }
  public String getPrimerApellido() {
    return jdoGetprimerApellido(this);
  }
  public void setPrimerApellido(String primerApellido) {
    jdoSetprimerApellido(this, primerApellido);
  }
  public String getPrimerNombre() {
    return jdoGetprimerNombre(this);
  }
  public void setPrimerNombre(String primerNombre) {
    jdoSetprimerNombre(this, primerNombre);
  }
  public Registro getRegistro() {
    return jdoGetregistro(this);
  }
  public void setRegistro(Registro registro) {
    jdoSetregistro(this, registro);
  }
  public String getSegundoApellido() {
    return jdoGetsegundoApellido(this);
  }
  public void setSegundoApellido(String segundoApellido) {
    jdoSetsegundoApellido(this, segundoApellido);
  }
  public String getSegundoNombre() {
    return jdoGetsegundoNombre(this);
  }
  public void setSegundoNombre(String segundoNombre) {
    jdoSetsegundoNombre(this, segundoNombre);
  }
  public String getSituacion() {
    return jdoGetsituacion(this);
  }
  public void setSituacion(String situacion) {
    jdoSetsituacion(this, situacion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoCargos localHistoricoCargos = new HistoricoCargos();
    localHistoricoCargos.jdoFlags = 1;
    localHistoricoCargos.jdoStateManager = paramStateManager;
    return localHistoricoCargos;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoCargos localHistoricoCargos = new HistoricoCargos();
    localHistoricoCargos.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoCargos.jdoFlags = 1;
    localHistoricoCargos.jdoStateManager = paramStateManager;
    return localHistoricoCargos;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaMovimiento);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horas);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoCargos);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.movimiento);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registro);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaMovimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoCargos = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.movimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registro = ((Registro)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoCargos paramHistoricoCargos, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramHistoricoCargos.cargo;
      return;
    case 1:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramHistoricoCargos.causaMovimiento;
      return;
    case 2:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramHistoricoCargos.cedula;
      return;
    case 3:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramHistoricoCargos.codigoNomina;
      return;
    case 4:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramHistoricoCargos.dependencia;
      return;
    case 5:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.fechaMovimiento = paramHistoricoCargos.fechaMovimiento;
      return;
    case 6:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramHistoricoCargos.horas;
      return;
    case 7:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoCargos = paramHistoricoCargos.idHistoricoCargos;
      return;
    case 8:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.movimiento = paramHistoricoCargos.movimiento;
      return;
    case 9:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramHistoricoCargos.primerApellido;
      return;
    case 10:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramHistoricoCargos.primerNombre;
      return;
    case 11:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.registro = paramHistoricoCargos.registro;
      return;
    case 12:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramHistoricoCargos.segundoApellido;
      return;
    case 13:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramHistoricoCargos.segundoNombre;
      return;
    case 14:
      if (paramHistoricoCargos == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramHistoricoCargos.situacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoCargos))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoCargos localHistoricoCargos = (HistoricoCargos)paramObject;
    if (localHistoricoCargos.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoCargos, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoCargosPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoCargosPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoCargosPK))
      throw new IllegalArgumentException("arg1");
    HistoricoCargosPK localHistoricoCargosPK = (HistoricoCargosPK)paramObject;
    localHistoricoCargosPK.idHistoricoCargos = this.idHistoricoCargos;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoCargosPK))
      throw new IllegalArgumentException("arg1");
    HistoricoCargosPK localHistoricoCargosPK = (HistoricoCargosPK)paramObject;
    this.idHistoricoCargos = localHistoricoCargosPK.idHistoricoCargos;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoCargosPK))
      throw new IllegalArgumentException("arg2");
    HistoricoCargosPK localHistoricoCargosPK = (HistoricoCargosPK)paramObject;
    localHistoricoCargosPK.idHistoricoCargos = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoCargosPK))
      throw new IllegalArgumentException("arg2");
    HistoricoCargosPK localHistoricoCargosPK = (HistoricoCargosPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localHistoricoCargosPK.idHistoricoCargos);
  }

  private static final Cargo jdoGetcargo(HistoricoCargos paramHistoricoCargos)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.cargo;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 0))
      return paramHistoricoCargos.cargo;
    return (Cargo)localStateManager.getObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 0, paramHistoricoCargos.cargo);
  }

  private static final void jdoSetcargo(HistoricoCargos paramHistoricoCargos, Cargo paramCargo)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 0, paramHistoricoCargos.cargo, paramCargo);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(HistoricoCargos paramHistoricoCargos)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.causaMovimiento;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 1))
      return paramHistoricoCargos.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 1, paramHistoricoCargos.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(HistoricoCargos paramHistoricoCargos, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 1, paramHistoricoCargos.causaMovimiento, paramCausaMovimiento);
  }

  private static final int jdoGetcedula(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.cedula;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.cedula;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 2))
      return paramHistoricoCargos.cedula;
    return localStateManager.getIntField(paramHistoricoCargos, jdoInheritedFieldCount + 2, paramHistoricoCargos.cedula);
  }

  private static final void jdoSetcedula(HistoricoCargos paramHistoricoCargos, int paramInt)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoCargos, jdoInheritedFieldCount + 2, paramHistoricoCargos.cedula, paramInt);
  }

  private static final int jdoGetcodigoNomina(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.codigoNomina;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.codigoNomina;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 3))
      return paramHistoricoCargos.codigoNomina;
    return localStateManager.getIntField(paramHistoricoCargos, jdoInheritedFieldCount + 3, paramHistoricoCargos.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(HistoricoCargos paramHistoricoCargos, int paramInt)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoCargos, jdoInheritedFieldCount + 3, paramHistoricoCargos.codigoNomina, paramInt);
  }

  private static final Dependencia jdoGetdependencia(HistoricoCargos paramHistoricoCargos)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.dependencia;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 4))
      return paramHistoricoCargos.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 4, paramHistoricoCargos.dependencia);
  }

  private static final void jdoSetdependencia(HistoricoCargos paramHistoricoCargos, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 4, paramHistoricoCargos.dependencia, paramDependencia);
  }

  private static final Date jdoGetfechaMovimiento(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.fechaMovimiento;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.fechaMovimiento;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 5))
      return paramHistoricoCargos.fechaMovimiento;
    return (Date)localStateManager.getObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 5, paramHistoricoCargos.fechaMovimiento);
  }

  private static final void jdoSetfechaMovimiento(HistoricoCargos paramHistoricoCargos, Date paramDate)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.fechaMovimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.fechaMovimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 5, paramHistoricoCargos.fechaMovimiento, paramDate);
  }

  private static final double jdoGethoras(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.horas;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.horas;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 6))
      return paramHistoricoCargos.horas;
    return localStateManager.getDoubleField(paramHistoricoCargos, jdoInheritedFieldCount + 6, paramHistoricoCargos.horas);
  }

  private static final void jdoSethoras(HistoricoCargos paramHistoricoCargos, double paramDouble)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.horas = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.horas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoCargos, jdoInheritedFieldCount + 6, paramHistoricoCargos.horas, paramDouble);
  }

  private static final long jdoGetidHistoricoCargos(HistoricoCargos paramHistoricoCargos)
  {
    return paramHistoricoCargos.idHistoricoCargos;
  }

  private static final void jdoSetidHistoricoCargos(HistoricoCargos paramHistoricoCargos, long paramLong)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.idHistoricoCargos = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoCargos, jdoInheritedFieldCount + 7, paramHistoricoCargos.idHistoricoCargos, paramLong);
  }

  private static final String jdoGetmovimiento(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.movimiento;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.movimiento;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 8))
      return paramHistoricoCargos.movimiento;
    return localStateManager.getStringField(paramHistoricoCargos, jdoInheritedFieldCount + 8, paramHistoricoCargos.movimiento);
  }

  private static final void jdoSetmovimiento(HistoricoCargos paramHistoricoCargos, String paramString)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.movimiento = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.movimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoCargos, jdoInheritedFieldCount + 8, paramHistoricoCargos.movimiento, paramString);
  }

  private static final String jdoGetprimerApellido(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.primerApellido;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.primerApellido;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 9))
      return paramHistoricoCargos.primerApellido;
    return localStateManager.getStringField(paramHistoricoCargos, jdoInheritedFieldCount + 9, paramHistoricoCargos.primerApellido);
  }

  private static final void jdoSetprimerApellido(HistoricoCargos paramHistoricoCargos, String paramString)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoCargos, jdoInheritedFieldCount + 9, paramHistoricoCargos.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.primerNombre;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.primerNombre;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 10))
      return paramHistoricoCargos.primerNombre;
    return localStateManager.getStringField(paramHistoricoCargos, jdoInheritedFieldCount + 10, paramHistoricoCargos.primerNombre);
  }

  private static final void jdoSetprimerNombre(HistoricoCargos paramHistoricoCargos, String paramString)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoCargos, jdoInheritedFieldCount + 10, paramHistoricoCargos.primerNombre, paramString);
  }

  private static final Registro jdoGetregistro(HistoricoCargos paramHistoricoCargos)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.registro;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 11))
      return paramHistoricoCargos.registro;
    return (Registro)localStateManager.getObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 11, paramHistoricoCargos.registro);
  }

  private static final void jdoSetregistro(HistoricoCargos paramHistoricoCargos, Registro paramRegistro)
  {
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.registro = paramRegistro;
      return;
    }
    localStateManager.setObjectField(paramHistoricoCargos, jdoInheritedFieldCount + 11, paramHistoricoCargos.registro, paramRegistro);
  }

  private static final String jdoGetsegundoApellido(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.segundoApellido;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.segundoApellido;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 12))
      return paramHistoricoCargos.segundoApellido;
    return localStateManager.getStringField(paramHistoricoCargos, jdoInheritedFieldCount + 12, paramHistoricoCargos.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(HistoricoCargos paramHistoricoCargos, String paramString)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoCargos, jdoInheritedFieldCount + 12, paramHistoricoCargos.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.segundoNombre;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.segundoNombre;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 13))
      return paramHistoricoCargos.segundoNombre;
    return localStateManager.getStringField(paramHistoricoCargos, jdoInheritedFieldCount + 13, paramHistoricoCargos.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(HistoricoCargos paramHistoricoCargos, String paramString)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoCargos, jdoInheritedFieldCount + 13, paramHistoricoCargos.segundoNombre, paramString);
  }

  private static final String jdoGetsituacion(HistoricoCargos paramHistoricoCargos)
  {
    if (paramHistoricoCargos.jdoFlags <= 0)
      return paramHistoricoCargos.situacion;
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoCargos.situacion;
    if (localStateManager.isLoaded(paramHistoricoCargos, jdoInheritedFieldCount + 14))
      return paramHistoricoCargos.situacion;
    return localStateManager.getStringField(paramHistoricoCargos, jdoInheritedFieldCount + 14, paramHistoricoCargos.situacion);
  }

  private static final void jdoSetsituacion(HistoricoCargos paramHistoricoCargos, String paramString)
  {
    if (paramHistoricoCargos.jdoFlags == 0)
    {
      paramHistoricoCargos.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoCargos.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoCargos, jdoInheritedFieldCount + 14, paramHistoricoCargos.situacion, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}