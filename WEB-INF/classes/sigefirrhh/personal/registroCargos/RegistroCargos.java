package sigefirrhh.personal.registroCargos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.personal.Gremio;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.personal.trabajador.Trabajador;

public class RegistroCargos
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SITUACION;
  protected static final Map LISTA_CONDICION;
  protected static final Map LISTA_SINO;
  private long idRegistroCargos;
  private Registro registro;
  private int codigoNomina;
  private String situacion;
  private String estatus;
  private String condicion;
  private Date fechaCreacion;
  private String convenioGremial;
  private Gremio gremio;
  private Cargo cargo;
  private Dependencia dependencia;
  private Sede sede;
  private Trabajador trabajador;
  private int idSitp;
  private Date tiempoSitp;
  private double sueldoBasico;
  private double compensacion;
  private double primasCargo;
  private double primasTrabajador;
  private double horas;
  private int reportaRac;
  private String aprobadoMpd;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aprobadoMpd", "cargo", "codigoNomina", "compensacion", "condicion", "convenioGremial", "dependencia", "estatus", "fechaCreacion", "gremio", "horas", "idRegistroCargos", "idSitp", "primasCargo", "primasTrabajador", "registro", "reportaRac", "sede", "situacion", "sueldoBasico", "tiempoSitp", "trabajador" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.Gremio"), Double.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.registro.Registro"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Sede"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 21, 26, 21, 21, 26, 21, 24, 21, 21, 21, 26, 21, 26, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroCargos"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RegistroCargos());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_SITUACION = 
      new LinkedHashMap();
    LISTA_CONDICION = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("O", "NO APLICA");
    LISTA_ESTATUS.put("1", "EN TRAMITE INTERNO");
    LISTA_ESTATUS.put("2", "APROBADO INTERNO");
    LISTA_ESTATUS.put("3", "EN TRAMITE MPD");
    LISTA_ESTATUS.put("4", "APROBADO MPD");
    LISTA_CONDICION.put("1", "ACTIVO");
    LISTA_CONDICION.put("2", "COMISION DE SERVICIOS");
    LISTA_CONDICION.put("3", "SUSPENDIDO");
    LISTA_CONDICION.put("4", "ENCARGADO");
    LISTA_CONDICION.put("5", "LICENCIA SIN SUELDO");
    LISTA_CONDICION.put("6", "LICENCIA CON SUELDO");
    LISTA_SITUACION.put("O", "OCUPADO");
    LISTA_SITUACION.put("V", "VACANTE");
    LISTA_SITUACION.put("E", "ELIMINADO");
    LISTA_SITUACION.put("C", "CONGELADO");
    LISTA_SITUACION.put("T", "TRANSFERIDO");

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public RegistroCargos()
  {
    jdoSetsituacion(this, "O");

    jdoSetestatus(this, "4");

    jdoSetcondicion(this, "1");

    jdoSetconvenioGremial(this, "N");

    jdoSetsueldoBasico(this, 0.0D);

    jdoSetcompensacion(this, 0.0D);

    jdoSetprimasCargo(this, 0.0D);

    jdoSetprimasTrabajador(this, 0.0D);

    jdoSethoras(this, 8.0D);

    jdoSetreportaRac(this, 1);

    jdoSetaprobadoMpd(this, "N");
  }

  public String toString() {
    return jdoGetcodigoNomina(this) + " - " + 
      jdoGetcargo(this).getDescripcionCargo() + " - " + 
      jdoGetsituacion(this) + " - " + jdoGetcargo(this).getGrado();
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public int getCodigoNomina()
  {
    return jdoGetcodigoNomina(this);
  }

  public String getCondicion()
  {
    return jdoGetcondicion(this);
  }

  public String getConvenioGremial()
  {
    return jdoGetconvenioGremial(this);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaCreacion()
  {
    return jdoGetfechaCreacion(this);
  }

  public Gremio getGremio()
  {
    return jdoGetgremio(this);
  }

  public double getHoras()
  {
    return jdoGethoras(this);
  }

  public long getIdRegistroCargos()
  {
    return jdoGetidRegistroCargos(this);
  }

  public Registro getRegistro()
  {
    return jdoGetregistro(this);
  }

  public int getReportaRac()
  {
    return jdoGetreportaRac(this);
  }

  public Sede getSede()
  {
    return jdoGetsede(this);
  }

  public String getSituacion()
  {
    return jdoGetsituacion(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public void setCodigoNomina(int i)
  {
    jdoSetcodigoNomina(this, i);
  }

  public void setCondicion(String string)
  {
    jdoSetcondicion(this, string);
  }

  public void setConvenioGremial(String string)
  {
    jdoSetconvenioGremial(this, string);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaCreacion(Date date)
  {
    jdoSetfechaCreacion(this, date);
  }

  public void setGremio(Gremio gremio)
  {
    jdoSetgremio(this, gremio);
  }

  public void setHoras(double d)
  {
    jdoSethoras(this, d);
  }

  public void setIdRegistroCargos(long l)
  {
    jdoSetidRegistroCargos(this, l);
  }

  public void setRegistro(Registro registro)
  {
    jdoSetregistro(this, registro);
  }

  public void setReportaRac(int i)
  {
    jdoSetreportaRac(this, i);
  }

  public void setSede(Sede sede)
  {
    jdoSetsede(this, sede);
  }

  public void setSituacion(String string)
  {
    jdoSetsituacion(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public double getCompensacion()
  {
    return jdoGetcompensacion(this);
  }

  public double getPrimasCargo()
  {
    return jdoGetprimasCargo(this);
  }

  public double getPrimasTrabajador()
  {
    return jdoGetprimasTrabajador(this);
  }

  public double getSueldoBasico()
  {
    return jdoGetsueldoBasico(this);
  }

  public void setCompensacion(double d)
  {
    jdoSetcompensacion(this, d);
  }

  public void setPrimasCargo(double d)
  {
    jdoSetprimasCargo(this, d);
  }

  public void setPrimasTrabajador(double d)
  {
    jdoSetprimasTrabajador(this, d);
  }

  public void setSueldoBasico(double d)
  {
    jdoSetsueldoBasico(this, d);
  }

  public String getAprobadoMpd()
  {
    return jdoGetaprobadoMpd(this);
  }

  public void setAprobadoMpd(String string)
  {
    jdoSetaprobadoMpd(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 22;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RegistroCargos localRegistroCargos = new RegistroCargos();
    localRegistroCargos.jdoFlags = 1;
    localRegistroCargos.jdoStateManager = paramStateManager;
    return localRegistroCargos;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RegistroCargos localRegistroCargos = new RegistroCargos();
    localRegistroCargos.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegistroCargos.jdoFlags = 1;
    localRegistroCargos.jdoStateManager = paramStateManager;
    return localRegistroCargos;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobadoMpd);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.condicion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.convenioGremial);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCreacion);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.gremio);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horas);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegistroCargos);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasCargo);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasTrabajador);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registro);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.reportaRac);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sede);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoBasico);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobadoMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.condicion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.convenioGremial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCreacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gremio = ((Gremio)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegistroCargos = localStateManager.replacingLongField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registro = ((Registro)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reportaRac = localStateManager.replacingIntField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sede = ((Sede)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoBasico = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RegistroCargos paramRegistroCargos, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.aprobadoMpd = paramRegistroCargos.aprobadoMpd;
      return;
    case 1:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramRegistroCargos.cargo;
      return;
    case 2:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramRegistroCargos.codigoNomina;
      return;
    case 3:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramRegistroCargos.compensacion;
      return;
    case 4:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.condicion = paramRegistroCargos.condicion;
      return;
    case 5:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.convenioGremial = paramRegistroCargos.convenioGremial;
      return;
    case 6:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramRegistroCargos.dependencia;
      return;
    case 7:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramRegistroCargos.estatus;
      return;
    case 8:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCreacion = paramRegistroCargos.fechaCreacion;
      return;
    case 9:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.gremio = paramRegistroCargos.gremio;
      return;
    case 10:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramRegistroCargos.horas;
      return;
    case 11:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.idRegistroCargos = paramRegistroCargos.idRegistroCargos;
      return;
    case 12:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramRegistroCargos.idSitp;
      return;
    case 13:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.primasCargo = paramRegistroCargos.primasCargo;
      return;
    case 14:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.primasTrabajador = paramRegistroCargos.primasTrabajador;
      return;
    case 15:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.registro = paramRegistroCargos.registro;
      return;
    case 16:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.reportaRac = paramRegistroCargos.reportaRac;
      return;
    case 17:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.sede = paramRegistroCargos.sede;
      return;
    case 18:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramRegistroCargos.situacion;
      return;
    case 19:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoBasico = paramRegistroCargos.sueldoBasico;
      return;
    case 20:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramRegistroCargos.tiempoSitp;
      return;
    case 21:
      if (paramRegistroCargos == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramRegistroCargos.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RegistroCargos))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RegistroCargos localRegistroCargos = (RegistroCargos)paramObject;
    if (localRegistroCargos.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegistroCargos, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegistroCargosPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegistroCargosPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroCargosPK))
      throw new IllegalArgumentException("arg1");
    RegistroCargosPK localRegistroCargosPK = (RegistroCargosPK)paramObject;
    localRegistroCargosPK.idRegistroCargos = this.idRegistroCargos;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroCargosPK))
      throw new IllegalArgumentException("arg1");
    RegistroCargosPK localRegistroCargosPK = (RegistroCargosPK)paramObject;
    this.idRegistroCargos = localRegistroCargosPK.idRegistroCargos;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroCargosPK))
      throw new IllegalArgumentException("arg2");
    RegistroCargosPK localRegistroCargosPK = (RegistroCargosPK)paramObject;
    localRegistroCargosPK.idRegistroCargos = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 11);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroCargosPK))
      throw new IllegalArgumentException("arg2");
    RegistroCargosPK localRegistroCargosPK = (RegistroCargosPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 11, localRegistroCargosPK.idRegistroCargos);
  }

  private static final String jdoGetaprobadoMpd(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.aprobadoMpd;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.aprobadoMpd;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 0))
      return paramRegistroCargos.aprobadoMpd;
    return localStateManager.getStringField(paramRegistroCargos, jdoInheritedFieldCount + 0, paramRegistroCargos.aprobadoMpd);
  }

  private static final void jdoSetaprobadoMpd(RegistroCargos paramRegistroCargos, String paramString)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.aprobadoMpd = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.aprobadoMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroCargos, jdoInheritedFieldCount + 0, paramRegistroCargos.aprobadoMpd, paramString);
  }

  private static final Cargo jdoGetcargo(RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.cargo;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 1))
      return paramRegistroCargos.cargo;
    return (Cargo)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 1, paramRegistroCargos.cargo);
  }

  private static final void jdoSetcargo(RegistroCargos paramRegistroCargos, Cargo paramCargo)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 1, paramRegistroCargos.cargo, paramCargo);
  }

  private static final int jdoGetcodigoNomina(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.codigoNomina;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.codigoNomina;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 2))
      return paramRegistroCargos.codigoNomina;
    return localStateManager.getIntField(paramRegistroCargos, jdoInheritedFieldCount + 2, paramRegistroCargos.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(RegistroCargos paramRegistroCargos, int paramInt)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroCargos, jdoInheritedFieldCount + 2, paramRegistroCargos.codigoNomina, paramInt);
  }

  private static final double jdoGetcompensacion(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.compensacion;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.compensacion;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 3))
      return paramRegistroCargos.compensacion;
    return localStateManager.getDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 3, paramRegistroCargos.compensacion);
  }

  private static final void jdoSetcompensacion(RegistroCargos paramRegistroCargos, double paramDouble)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.compensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.compensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 3, paramRegistroCargos.compensacion, paramDouble);
  }

  private static final String jdoGetcondicion(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.condicion;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.condicion;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 4))
      return paramRegistroCargos.condicion;
    return localStateManager.getStringField(paramRegistroCargos, jdoInheritedFieldCount + 4, paramRegistroCargos.condicion);
  }

  private static final void jdoSetcondicion(RegistroCargos paramRegistroCargos, String paramString)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.condicion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.condicion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroCargos, jdoInheritedFieldCount + 4, paramRegistroCargos.condicion, paramString);
  }

  private static final String jdoGetconvenioGremial(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.convenioGremial;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.convenioGremial;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 5))
      return paramRegistroCargos.convenioGremial;
    return localStateManager.getStringField(paramRegistroCargos, jdoInheritedFieldCount + 5, paramRegistroCargos.convenioGremial);
  }

  private static final void jdoSetconvenioGremial(RegistroCargos paramRegistroCargos, String paramString)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.convenioGremial = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.convenioGremial = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroCargos, jdoInheritedFieldCount + 5, paramRegistroCargos.convenioGremial, paramString);
  }

  private static final Dependencia jdoGetdependencia(RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.dependencia;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 6))
      return paramRegistroCargos.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 6, paramRegistroCargos.dependencia);
  }

  private static final void jdoSetdependencia(RegistroCargos paramRegistroCargos, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 6, paramRegistroCargos.dependencia, paramDependencia);
  }

  private static final String jdoGetestatus(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.estatus;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.estatus;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 7))
      return paramRegistroCargos.estatus;
    return localStateManager.getStringField(paramRegistroCargos, jdoInheritedFieldCount + 7, paramRegistroCargos.estatus);
  }

  private static final void jdoSetestatus(RegistroCargos paramRegistroCargos, String paramString)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroCargos, jdoInheritedFieldCount + 7, paramRegistroCargos.estatus, paramString);
  }

  private static final Date jdoGetfechaCreacion(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.fechaCreacion;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.fechaCreacion;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 8))
      return paramRegistroCargos.fechaCreacion;
    return (Date)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 8, paramRegistroCargos.fechaCreacion);
  }

  private static final void jdoSetfechaCreacion(RegistroCargos paramRegistroCargos, Date paramDate)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.fechaCreacion = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.fechaCreacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 8, paramRegistroCargos.fechaCreacion, paramDate);
  }

  private static final Gremio jdoGetgremio(RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.gremio;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 9))
      return paramRegistroCargos.gremio;
    return (Gremio)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 9, paramRegistroCargos.gremio);
  }

  private static final void jdoSetgremio(RegistroCargos paramRegistroCargos, Gremio paramGremio)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.gremio = paramGremio;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 9, paramRegistroCargos.gremio, paramGremio);
  }

  private static final double jdoGethoras(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.horas;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.horas;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 10))
      return paramRegistroCargos.horas;
    return localStateManager.getDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 10, paramRegistroCargos.horas);
  }

  private static final void jdoSethoras(RegistroCargos paramRegistroCargos, double paramDouble)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.horas = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.horas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 10, paramRegistroCargos.horas, paramDouble);
  }

  private static final long jdoGetidRegistroCargos(RegistroCargos paramRegistroCargos)
  {
    return paramRegistroCargos.idRegistroCargos;
  }

  private static final void jdoSetidRegistroCargos(RegistroCargos paramRegistroCargos, long paramLong)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.idRegistroCargos = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegistroCargos, jdoInheritedFieldCount + 11, paramRegistroCargos.idRegistroCargos, paramLong);
  }

  private static final int jdoGetidSitp(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.idSitp;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.idSitp;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 12))
      return paramRegistroCargos.idSitp;
    return localStateManager.getIntField(paramRegistroCargos, jdoInheritedFieldCount + 12, paramRegistroCargos.idSitp);
  }

  private static final void jdoSetidSitp(RegistroCargos paramRegistroCargos, int paramInt)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroCargos, jdoInheritedFieldCount + 12, paramRegistroCargos.idSitp, paramInt);
  }

  private static final double jdoGetprimasCargo(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.primasCargo;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.primasCargo;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 13))
      return paramRegistroCargos.primasCargo;
    return localStateManager.getDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 13, paramRegistroCargos.primasCargo);
  }

  private static final void jdoSetprimasCargo(RegistroCargos paramRegistroCargos, double paramDouble)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.primasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.primasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 13, paramRegistroCargos.primasCargo, paramDouble);
  }

  private static final double jdoGetprimasTrabajador(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.primasTrabajador;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.primasTrabajador;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 14))
      return paramRegistroCargos.primasTrabajador;
    return localStateManager.getDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 14, paramRegistroCargos.primasTrabajador);
  }

  private static final void jdoSetprimasTrabajador(RegistroCargos paramRegistroCargos, double paramDouble)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.primasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.primasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 14, paramRegistroCargos.primasTrabajador, paramDouble);
  }

  private static final Registro jdoGetregistro(RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.registro;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 15))
      return paramRegistroCargos.registro;
    return (Registro)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 15, paramRegistroCargos.registro);
  }

  private static final void jdoSetregistro(RegistroCargos paramRegistroCargos, Registro paramRegistro)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.registro = paramRegistro;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 15, paramRegistroCargos.registro, paramRegistro);
  }

  private static final int jdoGetreportaRac(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.reportaRac;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.reportaRac;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 16))
      return paramRegistroCargos.reportaRac;
    return localStateManager.getIntField(paramRegistroCargos, jdoInheritedFieldCount + 16, paramRegistroCargos.reportaRac);
  }

  private static final void jdoSetreportaRac(RegistroCargos paramRegistroCargos, int paramInt)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.reportaRac = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.reportaRac = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroCargos, jdoInheritedFieldCount + 16, paramRegistroCargos.reportaRac, paramInt);
  }

  private static final Sede jdoGetsede(RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.sede;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 17))
      return paramRegistroCargos.sede;
    return (Sede)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 17, paramRegistroCargos.sede);
  }

  private static final void jdoSetsede(RegistroCargos paramRegistroCargos, Sede paramSede)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.sede = paramSede;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 17, paramRegistroCargos.sede, paramSede);
  }

  private static final String jdoGetsituacion(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.situacion;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.situacion;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 18))
      return paramRegistroCargos.situacion;
    return localStateManager.getStringField(paramRegistroCargos, jdoInheritedFieldCount + 18, paramRegistroCargos.situacion);
  }

  private static final void jdoSetsituacion(RegistroCargos paramRegistroCargos, String paramString)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroCargos, jdoInheritedFieldCount + 18, paramRegistroCargos.situacion, paramString);
  }

  private static final double jdoGetsueldoBasico(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.sueldoBasico;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.sueldoBasico;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 19))
      return paramRegistroCargos.sueldoBasico;
    return localStateManager.getDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 19, paramRegistroCargos.sueldoBasico);
  }

  private static final void jdoSetsueldoBasico(RegistroCargos paramRegistroCargos, double paramDouble)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.sueldoBasico = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.sueldoBasico = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroCargos, jdoInheritedFieldCount + 19, paramRegistroCargos.sueldoBasico, paramDouble);
  }

  private static final Date jdoGettiempoSitp(RegistroCargos paramRegistroCargos)
  {
    if (paramRegistroCargos.jdoFlags <= 0)
      return paramRegistroCargos.tiempoSitp;
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.tiempoSitp;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 20))
      return paramRegistroCargos.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 20, paramRegistroCargos.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(RegistroCargos paramRegistroCargos, Date paramDate)
  {
    if (paramRegistroCargos.jdoFlags == 0)
    {
      paramRegistroCargos.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 20, paramRegistroCargos.tiempoSitp, paramDate);
  }

  private static final Trabajador jdoGettrabajador(RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargos.trabajador;
    if (localStateManager.isLoaded(paramRegistroCargos, jdoInheritedFieldCount + 21))
      return paramRegistroCargos.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramRegistroCargos, jdoInheritedFieldCount + 21, paramRegistroCargos.trabajador);
  }

  private static final void jdoSettrabajador(RegistroCargos paramRegistroCargos, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramRegistroCargos.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargos.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargos, jdoInheritedFieldCount + 21, paramRegistroCargos.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}