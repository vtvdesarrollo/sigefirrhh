package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class RegistroCargosFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();

  public void addAperturaEscolar(AperturaEscolar aperturaEscolar)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroCargosBusiness.addAperturaEscolar(aperturaEscolar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAperturaEscolar(AperturaEscolar aperturaEscolar) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.updateAperturaEscolar(aperturaEscolar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAperturaEscolar(AperturaEscolar aperturaEscolar) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.deleteAperturaEscolar(aperturaEscolar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AperturaEscolar findAperturaEscolarById(long aperturaEscolarId) throws Exception
  {
    try { this.txn.open();
      AperturaEscolar aperturaEscolar = 
        this.registroCargosBusiness.findAperturaEscolarById(aperturaEscolarId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(aperturaEscolar);
      return aperturaEscolar;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAperturaEscolar() throws Exception
  {
    try { this.txn.open();
      return this.registroCargosBusiness.findAllAperturaEscolar();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAperturaEscolarByDependencia(long idDependencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findAperturaEscolarByDependencia(idDependencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAperturaEscolarByCodCargo(String codCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findAperturaEscolarByCodCargo(codCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroCargosBusiness.addRelacionRegistroDocente(relacionRegistroDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.updateRelacionRegistroDocente(relacionRegistroDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.deleteRelacionRegistroDocente(relacionRegistroDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RelacionRegistroDocente findRelacionRegistroDocenteById(long relacionRegistroDocenteId) throws Exception
  {
    try { this.txn.open();
      RelacionRegistroDocente relacionRegistroDocente = 
        this.registroCargosBusiness.findRelacionRegistroDocenteById(relacionRegistroDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(relacionRegistroDocente);
      return relacionRegistroDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRelacionRegistroDocente() throws Exception
  {
    try { this.txn.open();
      return this.registroCargosBusiness.findAllRelacionRegistroDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRelacionRegistroDocenteByRegistroDocenteOrigen(long idRegistroDocenteOrigen)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findRelacionRegistroDocenteByRegistroDocenteOrigen(idRegistroDocenteOrigen);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRelacionRegistroDocenteByRegistroDocenteDestino(long idRegistroDocenteDestino)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findRelacionRegistroDocenteByRegistroDocenteDestino(idRegistroDocenteDestino);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRegistroDocente(RegistroDocente registroDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroCargosBusiness.addRegistroDocente(registroDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegistroDocente(RegistroDocente registroDocente) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.updateRegistroDocente(registroDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegistroDocente(RegistroDocente registroDocente) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.deleteRegistroDocente(registroDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RegistroDocente findRegistroDocenteById(long registroDocenteId) throws Exception
  {
    try { this.txn.open();
      RegistroDocente registroDocente = 
        this.registroCargosBusiness.findRegistroDocenteById(registroDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(registroDocente);
      return registroDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegistroDocente() throws Exception
  {
    try { this.txn.open();
      return this.registroCargosBusiness.findAllRegistroDocente();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRegistroCargos(RegistroCargos registroCargos)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroCargosBusiness.addRegistroCargos(registroCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegistroCargos(RegistroCargos registroCargos) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.updateRegistroCargos(registroCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegistroCargos(RegistroCargos registroCargos) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.deleteRegistroCargos(registroCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RegistroCargos findRegistroCargosById(long registroCargosId) throws Exception
  {
    try { this.txn.open();
      RegistroCargos registroCargos = 
        this.registroCargosBusiness.findRegistroCargosById(registroCargosId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(registroCargos);
      return registroCargos;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegistroCargos() throws Exception
  {
    try { this.txn.open();
      return this.registroCargosBusiness.findAllRegistroCargos();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistro(long idRegistro)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findRegistroCargosByRegistro(idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByCodigoNomina(int codigoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findRegistroCargosByCodigoNomina(codigoNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHistoricoCargos(HistoricoCargos historicoCargos)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.registroCargosBusiness.addHistoricoCargos(historicoCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistoricoCargos(HistoricoCargos historicoCargos) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.updateHistoricoCargos(historicoCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistoricoCargos(HistoricoCargos historicoCargos) throws Exception
  {
    try { this.txn.open();
      this.registroCargosBusiness.deleteHistoricoCargos(historicoCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistoricoCargos findHistoricoCargosById(long historicoCargosId) throws Exception
  {
    try { this.txn.open();
      HistoricoCargos historicoCargos = 
        this.registroCargosBusiness.findHistoricoCargosById(historicoCargosId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historicoCargos);
      return historicoCargos;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistoricoCargos() throws Exception
  {
    try { this.txn.open();
      return this.registroCargosBusiness.findAllHistoricoCargos();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoCargosByRegistro(long idRegistro)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findHistoricoCargosByRegistro(idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoCargosByCodigoNomina(int codigoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosBusiness.findHistoricoCargosByCodigoNomina(codigoNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}