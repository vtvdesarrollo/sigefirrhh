package sigefirrhh.personal.registroCargos;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class RegistroDocenteNoGenBeanBusiness extends RegistroDocenteBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(RegistroDocenteNoGenBeanBusiness.class.getName());

  public Collection findByCedulaAndDependenciaAndSituacion(int cedula, long idDependencia, String situacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.cedula == pCedula && dependencia.idDependencia == pIdDependencia && situacion == pSituacion";

    Query query = pm.newQuery(RegistroDocente.class, filter);

    query.declareParameters("int pCedula, long pIdDependencia, String pSituacion");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdDependencia", new Long(idDependencia));
    parameters.put("pSituacion", situacion);

    query.setOrdering("trabajador.fechaIngresoCargo ascending");
    Collection colCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCurso);

    return colCurso;
  }

  public Collection findByPersonalAndEstatus(long idPersonal, String estatus)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    this.log.error("idPersonal.................." + idPersonal);
    this.log.error("estatus.................." + estatus);
    String filter = "trabajador.personal.idPersonal == pIdPersonal && trabajador.estatus == pEstatus";

    Query query = pm.newQuery(RegistroDocente.class, filter);

    query.declareParameters("long pIdPersonal, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pEstatus", estatus);

    Collection colCurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    this.log.error("size 1 .................." + colCurso.size());
    pm.makeTransientAll(colCurso);

    return colCurso;
  }

  public boolean actualizarNuevoRegistroCargos(Long idTipoPersonal, Long idRegistro, Long idRegistro2)
    throws Exception
  {
    Connection connection = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select aplicar_nueva_estructura(?,?,?)");

      this.log.error("Parametros para actualizar_nuevo_registro_cargos:");
      this.log.error("1- " + idRegistro);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      st.executeQuery();

      connection.commit();
      this.log.error("Ejecutó el proceso de actualizar estructura");
      return true;
    }
    catch (Exception e) {
      this.log.error("Error en el proceso de liquidar trabajador, en el PasivoLaboralNoGenBusiness: " + e);
      throw e;
    }
    finally
    {
      if (st != null) try {
          st.close();
        } catch (Exception localException3) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException4)
        {
        }
    }
  }

  public boolean reemplazarDependencia(Long idRegistro, Long idRegion, Long idDependencia, Long idDependencia2, Long idManualCargo, Long idCargo, int gradoDesde, int gradoHasta, String situacion, String estatus)
    throws Exception
  {
    Connection connection = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select reemplazar_dependencia(?,?,?,?,?,?,?,?,?,?)");

      this.log.error("Parametros para reemplazar_dependencia:");
      this.log.error("1- idregistro " + idRegistro);
      this.log.error("2- idregion " + idRegion);
      this.log.error("3- iddependencia de" + idDependencia);
      this.log.error("4- iddependencia a" + idDependencia);
      this.log.error("5- idmnualcargo" + idManualCargo);
      this.log.error("6- idcargo" + idCargo);
      this.log.error("7- gradodesde" + gradoDesde);
      this.log.error("8- gradoHasta" + gradoHasta);
      this.log.error("9- situacion" + situacion);
      this.log.error("10- estatus " + estatus);
      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      st.setLong(7, gradoDesde);
      st.setLong(8, gradoHasta);
      st.setString(9, situacion);
      st.setString(10, estatus);

      st.executeQuery();

      connection.commit();
      this.log.error("Ejecutó el proceso de reemplazar dependencia con éxito");
      return true;
    }
    catch (Exception e) {
      this.log.error("Error en el proceso de reemplazar dependencia : " + e);
      throw e;
    }
    finally
    {
      if (st != null) try {
          st.close();
        } catch (Exception localException3) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException4)
        {
        }
    }
  }
}