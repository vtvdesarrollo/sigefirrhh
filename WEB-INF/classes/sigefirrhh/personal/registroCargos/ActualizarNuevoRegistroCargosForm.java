package sigefirrhh.personal.registroCargos;

import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ActualizarNuevoRegistroCargosForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarNuevoRegistroCargosForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String formato = "1";
  private String findRegistro;
  private String findRegistro2;
  private String situacion;
  private String estatus;
  private Collection colFindRegistro;
  private Collection colFindRegistro2;
  private Collection colRegion;
  private Collection colDependencia;
  private Collection colManualCargo;
  private Collection colTipoPersonal;
  private Collection colCargo;
  private String idTipoPersonal;
  private String idRegion;
  private String idDependencia;
  private String idManualCargo;
  private long idCargo;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();

  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private RegistroNoGenFacade registroFacade = new RegistroNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosNoGenFacade = new RegistroCargosNoGenFacade();

  private int gradoDesde = 1;
  private int gradoHasta = 99;
  private boolean show;
  private boolean auxShow;
  private String seleccionUbicacion = "T";
  private String seleccionCargo = "T";
  private boolean showCargo = false;
  private boolean showManualCargo = false;
  private boolean showRegion = false;
  private boolean showDependencia = false;

  public boolean isShowDependencia()
  {
    return this.showDependencia;
  }
  public boolean isShowRegion() {
    return this.showRegion;
  }
  public boolean isShowCargo() {
    return this.showCargo;
  }

  public boolean isShowManualCargo() {
    return this.showManualCargo;
  }
  public ActualizarNuevoRegistroCargosForm() {
    this.reportName = "racgeneral";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
      }

      public PhaseId getPhaseId()
      {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeSeleccionUbicacion(ValueChangeEvent event) {
    String seleccionUbicacion = 
      (String)event.getNewValue();
    this.seleccionUbicacion = seleccionUbicacion;
    try {
      this.showRegion = false;
      this.showDependencia = false;
      if (seleccionUbicacion.equals("R"))
        this.showRegion = true;
      else if (seleccionUbicacion.equals("D")) {
        this.showDependencia = true;
      }
      else if (this.idManualCargo != null)
      {
        this.colCargo = this.cargoFacade.findCargoByIdManualCargoNoLista(Long.valueOf(this.idManualCargo).longValue());
      }

    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeSeleccionCargo(ValueChangeEvent event)
  {
    String seleccionCargo = 
      (String)event.getNewValue();
    this.seleccionCargo = seleccionCargo;
    try {
      this.showManualCargo = false;
      this.showCargo = false;
      if (seleccionCargo.equals("C")) {
        this.showManualCargo = true;
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    String idTipoPersonal = 
      (String)event.getNewValue();
  }

  public void changeRegion(ValueChangeEvent event)
  {
    String idRegion = 
      (String)event.getNewValue();
    try {
      if ((Long.valueOf(idRegion).longValue() != 0L) && (this.idManualCargo != null))
      {
        this.colCargo = this.cargoFacade.findCargoByIdManualCargoAndIdRegion(Long.valueOf(this.idManualCargo).longValue(), Long.valueOf(idRegion).longValue());
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeDependencia(ValueChangeEvent event)
  {
    String idDependencia = 
      (String)event.getNewValue();
    try {
      if ((Long.valueOf(idDependencia).longValue() != 0L) && (this.idManualCargo != null))
      {
        this.colCargo = this.cargoFacade.findCargoByIdManualCargoAndIdDependencia(Long.valueOf(this.idManualCargo).longValue(), Long.valueOf(idDependencia).longValue());
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeManualCargo(ValueChangeEvent event)
  {
    long idManualCargo = 
      Long.valueOf((String)event.getNewValue()).longValue();
    try {
      this.showCargo = false;
      if (idManualCargo != 0L) {
        if (this.seleccionUbicacion.equals("R"))
          this.colCargo = this.cargoFacade.findCargoByIdManualCargoAndIdRegion(idManualCargo, Long.valueOf(this.idRegion).longValue());
        else if (this.seleccionUbicacion.equals("D"))
          this.colCargo = this.cargoFacade.findCargoByIdManualCargoAndIdDependencia(idManualCargo, Long.valueOf(this.idDependencia).longValue());
        else {
          this.colCargo = this.cargoFacade.findCargoByIdManualCargoNoLista(idManualCargo);
        }
        this.showCargo = true;
      }
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegistro(ValueChangeEvent event)
  {
    long idRegistro = 
      Long.valueOf((String)event.getNewValue()).longValue();
  }

  public void changeRegistro2(ValueChangeEvent event)
  {
    long idRegistro = 
      Long.valueOf((String)event.getNewValue()).longValue();
  }

  public Collection getColFindRegistro()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFindRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }

  public Collection getColFindRegistro2() {
    Collection col = new ArrayList();
    Iterator iterator = this.colFindRegistro2.iterator();
    Registro registro2 = null;
    while (iterator.hasNext()) {
      registro2 = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro2.getIdRegistro()), 
        registro2.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipopersonal = null;
    while (iterator.hasNext()) {
      tipopersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipopersonal.getIdTipoPersonal()), 
        tipopersonal.toString()));
    }
    return col;
  }

  public Collection getColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }
  public Collection getColDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }
  public Collection getColManualCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }
    return col;
  }
  public Collection getColCargo() {
    Collection col = new ArrayList();
    if ((this.colCargo != null) && (!this.colCargo.isEmpty())) {
      Iterator iterator = this.colCargo.iterator();

      while (iterator.hasNext()) {
        Long id = (Long)iterator.next();
        String nombre = (String)iterator.next();
        col.add(new SelectItem(
          String.valueOf(id), 
          nombre));
      }
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());

      this.colFindRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colFindRegistro2 = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error(e.getCause());
    }
  }

  public String procesar()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      boolean estado = this.registroCargosNoGenFacade.actualizarNuevoRegistroCargos(new Long(this.idTipoPersonal), new Long(this.findRegistro), new Long(this.findRegistro2));

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      if (estado)
        context.addMessage("success_add", new FacesMessage("Se actualizó con éxito"));
      else
        context.addMessage("error_data", new FacesMessage("Error ejecutando el proceso"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public int getGradoDesde()
  {
    return this.gradoDesde;
  }

  public int getGradoHasta()
  {
    return this.gradoHasta;
  }

  public void setGradoDesde(int i)
  {
    this.gradoDesde = i;
  }

  public void setGradoHasta(int i)
  {
    this.gradoHasta = i;
  }

  public String getSituacion()
  {
    return this.situacion;
  }

  public void setSituacion(String string)
  {
    this.situacion = string;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public String getEstatus() {
    return this.estatus;
  }
  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }

  public String getSeleccionCargo()
  {
    return this.seleccionCargo;
  }
  public void setSeleccionCargo(String seleccionCargo) {
    this.seleccionCargo = seleccionCargo;
  }
  public String getSeleccionUbicacion() {
    return this.seleccionUbicacion;
  }
  public void setSeleccionUbicacion(String seleccionUbicacion) {
    this.seleccionUbicacion = seleccionUbicacion;
  }
  public long getIdCargo() {
    return this.idCargo;
  }
  public void setIdCargo(long idCargo) {
    this.idCargo = idCargo;
  }

  public String getIdTipoPersonal()
  {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(String idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getIdDependencia() {
    return this.idDependencia;
  }
  public void setIdDependencia(String idDependencia) {
    this.idDependencia = idDependencia;
  }
  public String getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(String idRegion) {
    this.idRegion = idRegion;
  }
  public String getFindRegistro() {
    return this.findRegistro;
  }
  public void setFindRegistro(String findRegistro) {
    this.findRegistro = findRegistro;
  }
  public String getIdManualCargo() {
    return this.idManualCargo;
  }
  public void setIdManualCargo(String idManualCargo) {
    this.idManualCargo = idManualCargo;
  }
}