package sigefirrhh.personal.registroCargos;

import java.io.Serializable;

public class RegistroCargosPK
  implements Serializable
{
  public long idRegistroCargos;

  public RegistroCargosPK()
  {
  }

  public RegistroCargosPK(long idRegistroCargos)
  {
    this.idRegistroCargos = idRegistroCargos;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegistroCargosPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegistroCargosPK thatPK)
  {
    return 
      this.idRegistroCargos == thatPK.idRegistroCargos;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegistroCargos)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegistroCargos);
  }
}