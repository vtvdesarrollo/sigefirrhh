package sigefirrhh.personal.registroCargos;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.personal.Gremio;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.sistema.RegistrarAuditoria;

public class AgregarRegistroCargosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AgregarRegistroCargosForm.class.getName());
  private RegistroCargos registroCargos;
  private Collection result;
  private String findRegistro;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private RegistroFacade registroFacade = new RegistroFacade();
  private PersonalFacade personalFacade = new PersonalFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();

  private RegistroCargosFacade registroCargosFacade = new RegistroCargosFacade();
  private RegistroCargosNoGenFacade registroCargosNoGenFacade = new RegistroCargosNoGenFacade();
  private int findRegistroCargosCodigoNomina;
  private int findRegistroCargosNumeroRegistro;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colRegistro;
  private Collection colFindRegistro;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colDependencia;
  private Collection colSede;
  private Collection colTrabajador;
  private String selectRegistro;
  private String selectGremio;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectDependencia;
  private String selectSede;
  private Collection findColTipoPersonal;
  private Registro registro;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectRegistro()
  {
    return this.selectRegistro;
  }
  public void setSelectRegistro(String valRegistro) {
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    this.registroCargos.setRegistro(null);
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      if (String.valueOf(registro.getIdRegistro()).equals(
        valRegistro)) {
        this.registroCargos.setRegistro(
          registro);
      }
    }
    this.selectRegistro = valRegistro;
  }

  public String getSelectManualCargoForCargo() {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L)
        this.colCargo = this.cargoFacade.findCargoByManualCargo(idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegistro(ValueChangeEvent event) { long idRegistro = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.registro = this.registroFacade.findRegistroById(idRegistro);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    } }

  public boolean isShowManualCargoForCargo() {
    return this.colManualCargoForCargo != null;
  }
  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.registroCargos.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.registroCargos.setCargo(cargo);
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public boolean isShowDependencia() {
    return this.colDependencia != null;
  }
  public boolean isShowSede() {
    return this.colSede != null;
  }
  public String getSelectDependencia() {
    return this.selectDependencia;
  }
  public void setSelectDependencia(String valDependencia) {
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    this.registroCargos.setDependencia(null);
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      if (String.valueOf(dependencia.getIdDependencia()).equals(
        valDependencia)) {
        this.registroCargos.setDependencia(dependencia);
      }
    }
    this.selectDependencia = valDependencia;
  }

  public String getSelectSede() {
    return this.selectSede;
  }

  public void setSelectSede(String valSede)
  {
    this.selectSede = valSede;
  }
  public void changeSede(ValueChangeEvent event) {
    long idSede = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colDependencia = null;
      if (idSede > 0L)
        this.colDependencia = this.estructuraFacade.findDependenciaBySede(idSede, this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getResult() {
    return this.result;
  }

  public RegistroCargos getRegistroCargos() {
    if (this.registroCargos == null) {
      this.registroCargos = new RegistroCargos();
    }
    return this.registroCargos;
  }

  public AgregarRegistroCargosForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegistro()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }
  public Collection getColFindRegistro() {
    Collection col = new ArrayList();
    Iterator iterator = this.colFindRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }

  public Collection getListSituacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = RegistroCargos.LISTA_SITUACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEstatus() {
    Collection col = new ArrayList();

    Iterator iterEntry = RegistroCargos.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListCondicion() {
    Collection col = new ArrayList();

    Iterator iterEntry = RegistroCargos.LISTA_CONDICION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListConvenioGremial()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = RegistroCargos.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public Collection getColSede()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSede.iterator();
    Sede sede = null;
    while (iterator.hasNext()) {
      sede = (Sede)iterator.next();
      col.add(new SelectItem(
        String.valueOf(sede.getIdSede()), 
        sede.toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(this.login.getOrganismo().getIdOrganismo());

      this.colRegistro = 
        this.registroFacade.findRegistroByOrganismo(this.login.getOrganismo().getIdOrganismo());
      this.colFindRegistro = 
        this.registroFacade.findRegistroByOrganismo(this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRegistroCargosByCodigoNominaNumeroRegistro()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.result = 
        this.registroCargosNoGenFacade.findRegistroCargosByCodigoNominaAndRegistro(
        this.findRegistroCargosCodigoNomina, Long.valueOf(this.findRegistro).longValue());

      this.adding = 
        this.result.isEmpty();

      if (!this.adding)
      {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El código ya se encuentra registrado", ""));
      }
      else {
        this.colManualCargoForCargo = this.cargoFacade.findManualCargoByRegistro(
          Long.valueOf(this.findRegistro).longValue());
        this.colSede = this.estructuraFacade.findSedeByOrganismo(this.login.getOrganismo().getIdOrganismo());

        add();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectRegistroCargos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRegistro = null;
    this.selectGremio = null;
    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectDependencia = null;
    this.selectSede = null;

    long idRegistroCargos = 
      Long.parseLong((String)requestParameterMap.get("idRegistroCargos"));
    try
    {
      this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);

      if (this.registroCargos.getRegistro() != null) {
        this.selectRegistro = String.valueOf(this.registroCargos.getRegistro().getIdRegistro());
      }
      if (this.registroCargos.getGremio() != null) {
        this.selectGremio = String.valueOf(this.registroCargos.getGremio().getIdGremio());
      }
      if (this.registroCargos.getCargo() != null) {
        this.selectCargo = String.valueOf(this.registroCargos.getCargo().getIdCargo());
      }
      if (this.registroCargos.getDependencia() != null) {
        this.selectDependencia = String.valueOf(this.registroCargos.getDependencia().getIdDependencia());
      }
      if (this.registroCargos.getSede() != null) {
        this.selectSede = 
          String.valueOf(this.registroCargos.getSede().getIdSede());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;

      if (this.registroCargos.getCargo() != null) {
        long idCargo = this.registroCargos.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.registroCargos.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(idManualCargoForCargo);
        this.colManualCargoForCargo = this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;
    refresh();
    return null;
  }

  public String edit() {
    this.editing = true;
    refresh();
    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    this.registroCargos.setSituacion("V");
    this.registroCargos.setTrabajador(null);
    this.registroCargos.setCondicion("1");
    this.registroCargos.setEstatus("0");
    this.registroCargos.setRegistro(this.registro);
    this.registroCargos.setCodigoNomina(this.findRegistroCargosCodigoNomina);
    this.registroCargos.setSede(this.registroCargos.getDependencia().getSede());

    if ((this.registroCargos.getTiempoSitp() != null) && 
      (this.registroCargos.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding)
      {
        this.registroCargosFacade.addRegistroCargos(this.registroCargos);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.registroCargos);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroCargosFacade.updateRegistroCargos(this.registroCargos);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.registroCargos);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
      refresh();
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }

  public String add()
  {
    this.adding = true;

    this.registroCargos = new RegistroCargos();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.registroCargos.setIdRegistroCargos(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.registroCargos.RegistroCargos"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;

    this.registroCargos = new RegistroCargos();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;

    this.registroCargos = new RegistroCargos();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isShowAdd() {
    return this.adding;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getFindRegistroCargosCodigoNomina()
  {
    return this.findRegistroCargosCodigoNomina;
  }

  public int getFindRegistroCargosNumeroRegistro()
  {
    return this.findRegistroCargosNumeroRegistro;
  }

  public void setFindRegistroCargosCodigoNomina(int i)
  {
    this.findRegistroCargosCodigoNomina = i;
  }

  public void setFindRegistroCargosNumeroRegistro(int i)
  {
    this.findRegistroCargosNumeroRegistro = i;
  }

  public String getFindRegistro()
  {
    return this.findRegistro;
  }

  public void setFindRegistro(String string)
  {
    this.findRegistro = string;
  }
}