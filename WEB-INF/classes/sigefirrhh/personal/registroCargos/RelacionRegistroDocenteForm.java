package sigefirrhh.personal.registroCargos;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class RelacionRegistroDocenteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RelacionRegistroDocenteForm.class.getName());
  private RelacionRegistroDocente relacionRegistroDocente;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private RegistroCargosFacade registroCargosFacade = new RegistroCargosFacade();
  private boolean showRelacionRegistroDocenteByRegistroDocenteOrigen;
  private boolean showRelacionRegistroDocenteByRegistroDocenteDestino;
  private String findSelectRegistroDocenteOrigen;
  private String findSelectRegistroDocenteDestino;
  private Collection findColRegistroDocenteOrigen;
  private Collection findColRegistroDocenteDestino;
  private Collection colRegistroDocenteOrigen;
  private Collection colRegistroDocenteDestino;
  private String selectRegistroDocenteOrigen;
  private String selectRegistroDocenteDestino;
  private Object stateResultRelacionRegistroDocenteByRegistroDocenteOrigen = null;

  private Object stateResultRelacionRegistroDocenteByRegistroDocenteDestino = null;

  public String getFindSelectRegistroDocenteOrigen()
  {
    return this.findSelectRegistroDocenteOrigen;
  }
  public void setFindSelectRegistroDocenteOrigen(String valRegistroDocenteOrigen) {
    this.findSelectRegistroDocenteOrigen = valRegistroDocenteOrigen;
  }

  public Collection getFindColRegistroDocenteOrigen() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegistroDocenteOrigen.iterator();
    RegistroDocente registroDocenteOrigen = null;
    while (iterator.hasNext()) {
      registroDocenteOrigen = (RegistroDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroDocenteOrigen.getIdRegistroDocente()), 
        registroDocenteOrigen.toString()));
    }
    return col;
  }
  public String getFindSelectRegistroDocenteDestino() {
    return this.findSelectRegistroDocenteDestino;
  }
  public void setFindSelectRegistroDocenteDestino(String valRegistroDocenteDestino) {
    this.findSelectRegistroDocenteDestino = valRegistroDocenteDestino;
  }

  public Collection getFindColRegistroDocenteDestino() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegistroDocenteDestino.iterator();
    RegistroDocente registroDocenteDestino = null;
    while (iterator.hasNext()) {
      registroDocenteDestino = (RegistroDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroDocenteDestino.getIdRegistroDocente()), 
        registroDocenteDestino.toString()));
    }
    return col;
  }

  public String getSelectRegistroDocenteOrigen()
  {
    return this.selectRegistroDocenteOrigen;
  }
  public void setSelectRegistroDocenteOrigen(String valRegistroDocenteOrigen) {
    Iterator iterator = this.colRegistroDocenteOrigen.iterator();
    RegistroDocente registroDocenteOrigen = null;
    this.relacionRegistroDocente.setRegistroDocenteOrigen(null);
    while (iterator.hasNext()) {
      registroDocenteOrigen = (RegistroDocente)iterator.next();
      if (String.valueOf(registroDocenteOrigen.getIdRegistroDocente()).equals(
        valRegistroDocenteOrigen)) {
        this.relacionRegistroDocente.setRegistroDocenteOrigen(
          registroDocenteOrigen);
        break;
      }
    }
    this.selectRegistroDocenteOrigen = valRegistroDocenteOrigen;
  }
  public String getSelectRegistroDocenteDestino() {
    return this.selectRegistroDocenteDestino;
  }
  public void setSelectRegistroDocenteDestino(String valRegistroDocenteDestino) {
    Iterator iterator = this.colRegistroDocenteDestino.iterator();
    RegistroDocente registroDocenteDestino = null;
    this.relacionRegistroDocente.setRegistroDocenteDestino(null);
    while (iterator.hasNext()) {
      registroDocenteDestino = (RegistroDocente)iterator.next();
      if (String.valueOf(registroDocenteDestino.getIdRegistroDocente()).equals(
        valRegistroDocenteDestino)) {
        this.relacionRegistroDocente.setRegistroDocenteDestino(
          registroDocenteDestino);
        break;
      }
    }
    this.selectRegistroDocenteDestino = valRegistroDocenteDestino;
  }
  public Collection getResult() {
    return this.result;
  }

  public RelacionRegistroDocente getRelacionRegistroDocente() {
    if (this.relacionRegistroDocente == null) {
      this.relacionRegistroDocente = new RelacionRegistroDocente();
    }
    return this.relacionRegistroDocente;
  }

  public RelacionRegistroDocenteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegistroDocenteOrigen()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroDocenteOrigen.iterator();
    RegistroDocente registroDocenteOrigen = null;
    while (iterator.hasNext()) {
      registroDocenteOrigen = (RegistroDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroDocenteOrigen.getIdRegistroDocente()), 
        registroDocenteOrigen.toString()));
    }
    return col;
  }

  public Collection getColRegistroDocenteDestino()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroDocenteDestino.iterator();
    RegistroDocente registroDocenteDestino = null;
    while (iterator.hasNext()) {
      registroDocenteDestino = (RegistroDocente)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroDocenteDestino.getIdRegistroDocente()), 
        registroDocenteDestino.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColRegistroDocenteOrigen = 
        this.registroCargosFacade.findAllRegistroDocente();
      this.findColRegistroDocenteDestino = 
        this.registroCargosFacade.findAllRegistroDocente();

      this.colRegistroDocenteOrigen = 
        this.registroCargosFacade.findAllRegistroDocente();
      this.colRegistroDocenteDestino = 
        this.registroCargosFacade.findAllRegistroDocente();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRelacionRegistroDocenteByRegistroDocenteOrigen()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroCargosFacade.findRelacionRegistroDocenteByRegistroDocenteOrigen(Long.valueOf(this.findSelectRegistroDocenteOrigen).longValue());
      this.showRelacionRegistroDocenteByRegistroDocenteOrigen = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRelacionRegistroDocenteByRegistroDocenteOrigen)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegistroDocenteOrigen = null;
    this.findSelectRegistroDocenteDestino = null;

    return null;
  }

  public String findRelacionRegistroDocenteByRegistroDocenteDestino()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroCargosFacade.findRelacionRegistroDocenteByRegistroDocenteDestino(Long.valueOf(this.findSelectRegistroDocenteDestino).longValue());
      this.showRelacionRegistroDocenteByRegistroDocenteDestino = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRelacionRegistroDocenteByRegistroDocenteDestino)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegistroDocenteOrigen = null;
    this.findSelectRegistroDocenteDestino = null;

    return null;
  }

  public boolean isShowRelacionRegistroDocenteByRegistroDocenteOrigen() {
    return this.showRelacionRegistroDocenteByRegistroDocenteOrigen;
  }
  public boolean isShowRelacionRegistroDocenteByRegistroDocenteDestino() {
    return this.showRelacionRegistroDocenteByRegistroDocenteDestino;
  }

  public String selectRelacionRegistroDocente()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRegistroDocenteOrigen = null;
    this.selectRegistroDocenteDestino = null;

    long idRelacionRegistroDocente = 
      Long.parseLong((String)requestParameterMap.get("idRelacionRegistroDocente"));
    try
    {
      this.relacionRegistroDocente = 
        this.registroCargosFacade.findRelacionRegistroDocenteById(
        idRelacionRegistroDocente);
      if (this.relacionRegistroDocente.getRegistroDocenteOrigen() != null) {
        this.selectRegistroDocenteOrigen = 
          String.valueOf(this.relacionRegistroDocente.getRegistroDocenteOrigen().getIdRegistroDocente());
      }
      if (this.relacionRegistroDocente.getRegistroDocenteDestino() != null) {
        this.selectRegistroDocenteDestino = 
          String.valueOf(this.relacionRegistroDocente.getRegistroDocenteDestino().getIdRegistroDocente());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.relacionRegistroDocente = null;
    this.showRelacionRegistroDocenteByRegistroDocenteOrigen = false;
    this.showRelacionRegistroDocenteByRegistroDocenteDestino = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroCargosFacade.addRelacionRegistroDocente(
          this.relacionRegistroDocente);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroCargosFacade.updateRelacionRegistroDocente(
          this.relacionRegistroDocente);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroCargosFacade.deleteRelacionRegistroDocente(
        this.relacionRegistroDocente);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.relacionRegistroDocente = new RelacionRegistroDocente();

    this.selectRegistroDocenteOrigen = null;

    this.selectRegistroDocenteDestino = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.relacionRegistroDocente.setIdRelacionRegistroDocente(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.registroCargos.RelacionRegistroDocente"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.relacionRegistroDocente = new RelacionRegistroDocente();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}