package sigefirrhh.personal.registroCargos;

import java.util.Collection;

public class RegistroCargosNoGenBusiness extends RegistroCargosBusiness
{
  private RegistroCargosNoGenBeanBusiness registroCargosNoGenBeanBusiness = new RegistroCargosNoGenBeanBusiness();

  private HistoricoCargosNoGenBeanBusiness historicoCargosNoGenBeanBusiness = new HistoricoCargosNoGenBeanBusiness();

  private ActualizarRegistroCargosBeanBusiness actualizarRegistroCargosBeanBusiness = new ActualizarRegistroCargosBeanBusiness();

  private RegistroDocenteNoGenBeanBusiness registroDocenteNoGenBeanBusiness = new RegistroDocenteNoGenBeanBusiness();

  public Collection findRegistroCargosByCodigoNominaAndRegistro(int codigoNomina, long idRegistro) throws Exception
  {
    return this.registroCargosNoGenBeanBusiness.findByCodigoNominaAndRegistro(codigoNomina, idRegistro);
  }

  public Collection findRegistroCargosByCedulaAndRegistro(int Cedula, long idRegistro)
    throws Exception
  {
    return this.registroCargosNoGenBeanBusiness.findRegistroCargosByCedulaAndRegistro(Cedula, idRegistro);
  }

  public Collection findRegistroCargosForAscenso(long idRegistro, String situacion, int grado)
    throws Exception
  {
    return this.registroCargosNoGenBeanBusiness.findForAscenso(idRegistro, situacion, grado);
  }

  public boolean actualizarRegistroCargos(String situacion, long idTrabajador, long idRegistroCargos) throws Exception {
    return this.actualizarRegistroCargosBeanBusiness.actualizar(situacion, idTrabajador, idRegistroCargos);
  }

  public Collection findRegistroCargosByRegistroAndSituacion(long idRegistro, String situacion) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacion(idRegistro, situacion);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndGrado99(long idRegistro, String situacion, String grado99) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndGrado99(idRegistro, situacion, grado99);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndGrado99AndTipo0(long idRegistro, String situacion, String grado99) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndGrado99AndTipo0(idRegistro, situacion, grado99);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo0(long idRegistro, String situacion) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndTipo0(idRegistro, situacion);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo123(long idRegistro, String situacion) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndTipo123(idRegistro, situacion);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo3(long idRegistro, String situacion) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndTipo3(idRegistro, situacion);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo123(long idRegistro, String situacion, int grado) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndTipo123(idRegistro, situacion, grado);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo12(long idRegistro, String situacion) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndTipo12(idRegistro, situacion);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo12(long idRegistro, String situacion, int grado) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndTipo12(idRegistro, situacion, grado);
  }

  public Collection findRegistroCargosByRegistroAndTipo3(long idRegistro) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndTipo3(idRegistro);
  }

  public Collection findRegistroCargosByRegistroAndTipo12(long idRegistro) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndTipo12(idRegistro);
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo123MismoGrado(long idRegistro, String situacion, int grado) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroAndSituacionAndTipo123MismoGrado(idRegistro, situacion, grado);
  }

  public Collection findRegistroCargosByIdRegistroNoGrado99(long idRegistro) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByIdRegistroNoGrado99(idRegistro);
  }

  public Collection findHistoricoCargosByRegistroAndCodigoNomina(long idRegistro, int codigoNomina) throws Exception {
    return this.historicoCargosNoGenBeanBusiness.findByRegistroAndCodigoNomina(idRegistro, codigoNomina);
  }

  public Collection findRegistroCargosByRegistroDependenciaAndSituacion(long idRegistro, long idDependencia, String situacion) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findByRegistroDependenciaAndSituacion(idRegistro, idDependencia, situacion);
  }

  public Collection findRegistroCargosForConcurso(long idRegistro) throws Exception {
    return this.registroCargosNoGenBeanBusiness.findForConcurso(idRegistro);
  }

  public Collection findRegistroDocenteByCedulaAndDependenciaAndSituacion(int cedula, long idDependencia, String situacion) throws Exception {
    return this.registroDocenteNoGenBeanBusiness.findByCedulaAndDependenciaAndSituacion(cedula, idDependencia, situacion);
  }

  public Collection findRegistroDocenteByPersonalAndEstatus(long idPersonal, String estatus) throws Exception {
    return this.registroDocenteNoGenBeanBusiness.findByPersonalAndEstatus(idPersonal, estatus);
  }

  public boolean reemplazarDependencia(Long idRegistro, Long idRegion, Long idDependencia, Long idDependencia2, Long idManualCargo, Long idCargo, int gradoDesde, int gradoHasta, String situacion, String estatus)
    throws Exception
  {
    return this.registroDocenteNoGenBeanBusiness.reemplazarDependencia(idRegistro, idRegion, idDependencia, idDependencia2, 
      idManualCargo, idCargo, gradoDesde, gradoHasta, 
      situacion, estatus);
  }

  public boolean actualizarNuevoRegistroCargos(Long idTipoPersonal, Long idRegistro, Long idRegistro2) throws Exception
  {
    return this.registroDocenteNoGenBeanBusiness.actualizarNuevoRegistroCargos(idTipoPersonal, idRegistro, idRegistro2);
  }
}