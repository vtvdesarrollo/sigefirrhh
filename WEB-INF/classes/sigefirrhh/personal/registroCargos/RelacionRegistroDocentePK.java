package sigefirrhh.personal.registroCargos;

import java.io.Serializable;

public class RelacionRegistroDocentePK
  implements Serializable
{
  public long idRelacionRegistroDocente;

  public RelacionRegistroDocentePK()
  {
  }

  public RelacionRegistroDocentePK(long idRelacionRegistroDocente)
  {
    this.idRelacionRegistroDocente = idRelacionRegistroDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RelacionRegistroDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RelacionRegistroDocentePK thatPK)
  {
    return 
      this.idRelacionRegistroDocente == thatPK.idRelacionRegistroDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRelacionRegistroDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRelacionRegistroDocente);
  }
}