package sigefirrhh.personal.registroCargos;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;

public class HistoricoCargosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(HistoricoCargosForm.class.getName());
  private HistoricoCargos historicoCargos;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private RegistroFacade registroFacade = new RegistroFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroCargosFacade registroCargosFacade = new RegistroCargosFacade();
  private boolean showHistoricoCargosByRegistro;
  private boolean showHistoricoCargosByCodigoNomina;
  private String findSelectRegistro;
  private int findCodigoNomina;
  private Collection findColRegistro;
  private Collection colRegistro;
  private Collection colCausaMovimiento;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colDependencia;
  private String selectRegistro;
  private String selectCausaMovimiento;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectDependencia;
  private Object stateResultHistoricoCargosByRegistro = null;

  private Object stateResultHistoricoCargosByCodigoNomina = null;

  public String getFindSelectRegistro()
  {
    return this.findSelectRegistro;
  }
  public void setFindSelectRegistro(String valRegistro) {
    this.findSelectRegistro = valRegistro;
  }

  public Collection getFindColRegistro() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }
  public int getFindCodigoNomina() {
    return this.findCodigoNomina;
  }
  public void setFindCodigoNomina(int findCodigoNomina) {
    this.findCodigoNomina = findCodigoNomina;
  }

  public String getSelectRegistro()
  {
    return this.selectRegistro;
  }
  public void setSelectRegistro(String valRegistro) {
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    this.historicoCargos.setRegistro(null);
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      if (String.valueOf(registro.getIdRegistro()).equals(
        valRegistro)) {
        this.historicoCargos.setRegistro(
          registro);
        break;
      }
    }
    this.selectRegistro = valRegistro;
  }
  public String getSelectCausaMovimiento() {
    return this.selectCausaMovimiento;
  }
  public void setSelectCausaMovimiento(String valCausaMovimiento) {
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    this.historicoCargos.setCausaMovimiento(null);
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      if (String.valueOf(causaMovimiento.getIdCausaMovimiento()).equals(
        valCausaMovimiento)) {
        this.historicoCargos.setCausaMovimiento(
          causaMovimiento);
        break;
      }
    }
    this.selectCausaMovimiento = valCausaMovimiento;
  }
  public String getSelectManualCargoForCargo() {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.historicoCargos.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.historicoCargos.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.historicoCargos.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.historicoCargos.setCargo(
          cargo);
        break;
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectDependencia() {
    return this.selectDependencia;
  }
  public void setSelectDependencia(String valDependencia) {
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    this.historicoCargos.setDependencia(null);
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      if (String.valueOf(dependencia.getIdDependencia()).equals(
        valDependencia)) {
        this.historicoCargos.setDependencia(
          dependencia);
        break;
      }
    }
    this.selectDependencia = valDependencia;
  }
  public Collection getResult() {
    return this.result;
  }

  public HistoricoCargos getHistoricoCargos() {
    if (this.historicoCargos == null) {
      this.historicoCargos = new HistoricoCargos();
    }
    return this.historicoCargos;
  }

  public HistoricoCargosForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegistro()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }

  public Collection getListSituacion()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = HistoricoCargos.LISTA_SITUACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMovimiento() {
    Collection col = new ArrayList();

    Iterator iterEntry = HistoricoCargos.LISTA_MOVIMIENTO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColCausaMovimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaMovimiento.getIdCausaMovimiento()), 
        causaMovimiento.toString()));
    }
    return col;
  }

  public Collection getColManualCargoForCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colCausaMovimiento = 
        this.registroFacade.findAllCausaMovimiento();
      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colDependencia = 
        this.estructuraFacade.findDependenciaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findHistoricoCargosByRegistro()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroCargosFacade.findHistoricoCargosByRegistro(Long.valueOf(this.findSelectRegistro).longValue());
      this.showHistoricoCargosByRegistro = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showHistoricoCargosByRegistro)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegistro = null;
    this.findCodigoNomina = 0;

    return null;
  }

  public String findHistoricoCargosByCodigoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroCargosFacade.findHistoricoCargosByCodigoNomina(this.findCodigoNomina);
      this.showHistoricoCargosByCodigoNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showHistoricoCargosByCodigoNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegistro = null;
    this.findCodigoNomina = 0;

    return null;
  }

  public boolean isShowHistoricoCargosByRegistro() {
    return this.showHistoricoCargosByRegistro;
  }
  public boolean isShowHistoricoCargosByCodigoNomina() {
    return this.showHistoricoCargosByCodigoNomina;
  }

  public String selectHistoricoCargos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRegistro = null;
    this.selectCausaMovimiento = null;
    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectDependencia = null;

    long idHistoricoCargos = 
      Long.parseLong((String)requestParameterMap.get("idHistoricoCargos"));
    try
    {
      this.historicoCargos = 
        this.registroCargosFacade.findHistoricoCargosById(
        idHistoricoCargos);
      if (this.historicoCargos.getRegistro() != null) {
        this.selectRegistro = 
          String.valueOf(this.historicoCargos.getRegistro().getIdRegistro());
      }
      if (this.historicoCargos.getCausaMovimiento() != null) {
        this.selectCausaMovimiento = 
          String.valueOf(this.historicoCargos.getCausaMovimiento().getIdCausaMovimiento());
      }
      if (this.historicoCargos.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.historicoCargos.getCargo().getIdCargo());
      }
      if (this.historicoCargos.getDependencia() != null) {
        this.selectDependencia = 
          String.valueOf(this.historicoCargos.getDependencia().getIdDependencia());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;

      if (this.historicoCargos.getCargo() != null) {
        long idCargo = 
          this.historicoCargos.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.historicoCargos.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.historicoCargos = null;
    this.showHistoricoCargosByRegistro = false;
    this.showHistoricoCargosByCodigoNomina = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.historicoCargos.getFechaMovimiento() != null) && 
      (this.historicoCargos.getFechaMovimiento().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha del Movimiento no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroCargosFacade.addHistoricoCargos(
          this.historicoCargos);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroCargosFacade.updateHistoricoCargos(
          this.historicoCargos);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroCargosFacade.deleteHistoricoCargos(
        this.historicoCargos);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.historicoCargos = new HistoricoCargos();

    this.selectRegistro = null;

    this.selectCausaMovimiento = null;

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    this.selectDependencia = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.historicoCargos.setIdHistoricoCargos(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.registroCargos.HistoricoCargos"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.historicoCargos = new HistoricoCargos();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}