package sigefirrhh.personal.registroCargos;

import java.io.Serializable;

public class RegistroDocentePK
  implements Serializable
{
  public long idRegistroDocente;

  public RegistroDocentePK()
  {
  }

  public RegistroDocentePK(long idRegistroDocente)
  {
    this.idRegistroDocente = idRegistroDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegistroDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegistroDocentePK thatPK)
  {
    return 
      this.idRegistroDocente == thatPK.idRegistroDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegistroDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegistroDocente);
  }
}