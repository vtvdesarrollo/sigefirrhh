package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class RegistroCargosBusiness extends AbstractBusiness
  implements Serializable
{
  private AperturaEscolarBeanBusiness aperturaEscolarBeanBusiness = new AperturaEscolarBeanBusiness();

  private RelacionRegistroDocenteBeanBusiness relacionRegistroDocenteBeanBusiness = new RelacionRegistroDocenteBeanBusiness();

  private RegistroDocenteBeanBusiness registroDocenteBeanBusiness = new RegistroDocenteBeanBusiness();

  private RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();

  private HistoricoCargosBeanBusiness historicoCargosBeanBusiness = new HistoricoCargosBeanBusiness();

  public void addAperturaEscolar(AperturaEscolar aperturaEscolar)
    throws Exception
  {
    this.aperturaEscolarBeanBusiness.addAperturaEscolar(aperturaEscolar);
  }

  public void updateAperturaEscolar(AperturaEscolar aperturaEscolar) throws Exception {
    this.aperturaEscolarBeanBusiness.updateAperturaEscolar(aperturaEscolar);
  }

  public void deleteAperturaEscolar(AperturaEscolar aperturaEscolar) throws Exception {
    this.aperturaEscolarBeanBusiness.deleteAperturaEscolar(aperturaEscolar);
  }

  public AperturaEscolar findAperturaEscolarById(long aperturaEscolarId) throws Exception {
    return this.aperturaEscolarBeanBusiness.findAperturaEscolarById(aperturaEscolarId);
  }

  public Collection findAllAperturaEscolar() throws Exception {
    return this.aperturaEscolarBeanBusiness.findAperturaEscolarAll();
  }

  public Collection findAperturaEscolarByDependencia(long idDependencia)
    throws Exception
  {
    return this.aperturaEscolarBeanBusiness.findByDependencia(idDependencia);
  }

  public Collection findAperturaEscolarByCodCargo(String codCargo)
    throws Exception
  {
    return this.aperturaEscolarBeanBusiness.findByCodCargo(codCargo);
  }

  public void addRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente)
    throws Exception
  {
    this.relacionRegistroDocenteBeanBusiness.addRelacionRegistroDocente(relacionRegistroDocente);
  }

  public void updateRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente) throws Exception {
    this.relacionRegistroDocenteBeanBusiness.updateRelacionRegistroDocente(relacionRegistroDocente);
  }

  public void deleteRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente) throws Exception {
    this.relacionRegistroDocenteBeanBusiness.deleteRelacionRegistroDocente(relacionRegistroDocente);
  }

  public RelacionRegistroDocente findRelacionRegistroDocenteById(long relacionRegistroDocenteId) throws Exception {
    return this.relacionRegistroDocenteBeanBusiness.findRelacionRegistroDocenteById(relacionRegistroDocenteId);
  }

  public Collection findAllRelacionRegistroDocente() throws Exception {
    return this.relacionRegistroDocenteBeanBusiness.findRelacionRegistroDocenteAll();
  }

  public Collection findRelacionRegistroDocenteByRegistroDocenteOrigen(long idRegistroDocenteOrigen)
    throws Exception
  {
    return this.relacionRegistroDocenteBeanBusiness.findByRegistroDocenteOrigen(idRegistroDocenteOrigen);
  }

  public Collection findRelacionRegistroDocenteByRegistroDocenteDestino(long idRegistroDocenteDestino)
    throws Exception
  {
    return this.relacionRegistroDocenteBeanBusiness.findByRegistroDocenteDestino(idRegistroDocenteDestino);
  }

  public void addRegistroDocente(RegistroDocente registroDocente)
    throws Exception
  {
    this.registroDocenteBeanBusiness.addRegistroDocente(registroDocente);
  }

  public void updateRegistroDocente(RegistroDocente registroDocente) throws Exception {
    this.registroDocenteBeanBusiness.updateRegistroDocente(registroDocente);
  }

  public void deleteRegistroDocente(RegistroDocente registroDocente) throws Exception {
    this.registroDocenteBeanBusiness.deleteRegistroDocente(registroDocente);
  }

  public RegistroDocente findRegistroDocenteById(long registroDocenteId) throws Exception {
    return this.registroDocenteBeanBusiness.findRegistroDocenteById(registroDocenteId);
  }

  public Collection findAllRegistroDocente() throws Exception {
    return this.registroDocenteBeanBusiness.findRegistroDocenteAll();
  }

  public void addRegistroCargos(RegistroCargos registroCargos)
    throws Exception
  {
    this.registroCargosBeanBusiness.addRegistroCargos(registroCargos);
  }

  public void updateRegistroCargos(RegistroCargos registroCargos) throws Exception {
    this.registroCargosBeanBusiness.updateRegistroCargos(registroCargos);
  }

  public void deleteRegistroCargos(RegistroCargos registroCargos) throws Exception {
    this.registroCargosBeanBusiness.deleteRegistroCargos(registroCargos);
  }

  public RegistroCargos findRegistroCargosById(long registroCargosId) throws Exception {
    return this.registroCargosBeanBusiness.findRegistroCargosById(registroCargosId);
  }

  public Collection findAllRegistroCargos() throws Exception {
    return this.registroCargosBeanBusiness.findRegistroCargosAll();
  }

  public Collection findRegistroCargosByRegistro(long idRegistro)
    throws Exception
  {
    return this.registroCargosBeanBusiness.findByRegistro(idRegistro);
  }

  public Collection findRegistroCargosByCodigoNomina(int codigoNomina)
    throws Exception
  {
    return this.registroCargosBeanBusiness.findByCodigoNomina(codigoNomina);
  }

  public void addHistoricoCargos(HistoricoCargos historicoCargos)
    throws Exception
  {
    this.historicoCargosBeanBusiness.addHistoricoCargos(historicoCargos);
  }

  public void updateHistoricoCargos(HistoricoCargos historicoCargos) throws Exception {
    this.historicoCargosBeanBusiness.updateHistoricoCargos(historicoCargos);
  }

  public void deleteHistoricoCargos(HistoricoCargos historicoCargos) throws Exception {
    this.historicoCargosBeanBusiness.deleteHistoricoCargos(historicoCargos);
  }

  public HistoricoCargos findHistoricoCargosById(long historicoCargosId) throws Exception {
    return this.historicoCargosBeanBusiness.findHistoricoCargosById(historicoCargosId);
  }

  public Collection findAllHistoricoCargos() throws Exception {
    return this.historicoCargosBeanBusiness.findHistoricoCargosAll();
  }

  public Collection findHistoricoCargosByRegistro(long idRegistro)
    throws Exception
  {
    return this.historicoCargosBeanBusiness.findByRegistro(idRegistro);
  }

  public Collection findHistoricoCargosByCodigoNomina(int codigoNomina)
    throws Exception
  {
    return this.historicoCargosBeanBusiness.findByCodigoNomina(codigoNomina);
  }
}