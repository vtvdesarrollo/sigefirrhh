package sigefirrhh.personal.registroCargos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class RelacionRegistroDocente
  implements Serializable, PersistenceCapable
{
  private long idRelacionRegistroDocente;
  private RegistroDocente registroDocenteOrigen;
  private RegistroDocente registroDocenteDestino;
  private double horas;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "horas", "idRelacionRegistroDocente", "registroDocenteDestino", "registroDocenteOrigen" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroDocente"), sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroDocente") };
  private static final byte[] jdoFieldFlags = { 21, 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public RelacionRegistroDocente()
  {
    jdoSethoras(this, 0.0D);
  }

  public String toString()
  {
    return jdoGetregistroDocenteDestino(this) + " " + jdoGethoras(this);
  }

  public double getHoras()
  {
    return jdoGethoras(this);
  }
  public void setHoras(double horas) {
    jdoSethoras(this, horas);
  }
  public long getIdRelacionRegistroDocente() {
    return jdoGetidRelacionRegistroDocente(this);
  }
  public void setIdRelacionRegistroDocente(long idRelacionRegistroDocente) {
    jdoSetidRelacionRegistroDocente(this, idRelacionRegistroDocente);
  }
  public RegistroDocente getRegistroDocenteDestino() {
    return jdoGetregistroDocenteDestino(this);
  }
  public void setRegistroDocenteDestino(RegistroDocente registroDocenteDestino) {
    jdoSetregistroDocenteDestino(this, registroDocenteDestino);
  }
  public RegistroDocente getRegistroDocenteOrigen() {
    return jdoGetregistroDocenteOrigen(this);
  }
  public void setRegistroDocenteOrigen(RegistroDocente registroDocenteOrigen) {
    jdoSetregistroDocenteOrigen(this, registroDocenteOrigen);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.registroCargos.RelacionRegistroDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RelacionRegistroDocente());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RelacionRegistroDocente localRelacionRegistroDocente = new RelacionRegistroDocente();
    localRelacionRegistroDocente.jdoFlags = 1;
    localRelacionRegistroDocente.jdoStateManager = paramStateManager;
    return localRelacionRegistroDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RelacionRegistroDocente localRelacionRegistroDocente = new RelacionRegistroDocente();
    localRelacionRegistroDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRelacionRegistroDocente.jdoFlags = 1;
    localRelacionRegistroDocente.jdoStateManager = paramStateManager;
    return localRelacionRegistroDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horas);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRelacionRegistroDocente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registroDocenteDestino);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registroDocenteOrigen);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRelacionRegistroDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroDocenteDestino = ((RegistroDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroDocenteOrigen = ((RegistroDocente)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RelacionRegistroDocente paramRelacionRegistroDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRelacionRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramRelacionRegistroDocente.horas;
      return;
    case 1:
      if (paramRelacionRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idRelacionRegistroDocente = paramRelacionRegistroDocente.idRelacionRegistroDocente;
      return;
    case 2:
      if (paramRelacionRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.registroDocenteDestino = paramRelacionRegistroDocente.registroDocenteDestino;
      return;
    case 3:
      if (paramRelacionRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.registroDocenteOrigen = paramRelacionRegistroDocente.registroDocenteOrigen;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RelacionRegistroDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RelacionRegistroDocente localRelacionRegistroDocente = (RelacionRegistroDocente)paramObject;
    if (localRelacionRegistroDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRelacionRegistroDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RelacionRegistroDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RelacionRegistroDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RelacionRegistroDocentePK))
      throw new IllegalArgumentException("arg1");
    RelacionRegistroDocentePK localRelacionRegistroDocentePK = (RelacionRegistroDocentePK)paramObject;
    localRelacionRegistroDocentePK.idRelacionRegistroDocente = this.idRelacionRegistroDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RelacionRegistroDocentePK))
      throw new IllegalArgumentException("arg1");
    RelacionRegistroDocentePK localRelacionRegistroDocentePK = (RelacionRegistroDocentePK)paramObject;
    this.idRelacionRegistroDocente = localRelacionRegistroDocentePK.idRelacionRegistroDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RelacionRegistroDocentePK))
      throw new IllegalArgumentException("arg2");
    RelacionRegistroDocentePK localRelacionRegistroDocentePK = (RelacionRegistroDocentePK)paramObject;
    localRelacionRegistroDocentePK.idRelacionRegistroDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RelacionRegistroDocentePK))
      throw new IllegalArgumentException("arg2");
    RelacionRegistroDocentePK localRelacionRegistroDocentePK = (RelacionRegistroDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localRelacionRegistroDocentePK.idRelacionRegistroDocente);
  }

  private static final double jdoGethoras(RelacionRegistroDocente paramRelacionRegistroDocente)
  {
    if (paramRelacionRegistroDocente.jdoFlags <= 0)
      return paramRelacionRegistroDocente.horas;
    StateManager localStateManager = paramRelacionRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRelacionRegistroDocente.horas;
    if (localStateManager.isLoaded(paramRelacionRegistroDocente, jdoInheritedFieldCount + 0))
      return paramRelacionRegistroDocente.horas;
    return localStateManager.getDoubleField(paramRelacionRegistroDocente, jdoInheritedFieldCount + 0, paramRelacionRegistroDocente.horas);
  }

  private static final void jdoSethoras(RelacionRegistroDocente paramRelacionRegistroDocente, double paramDouble)
  {
    if (paramRelacionRegistroDocente.jdoFlags == 0)
    {
      paramRelacionRegistroDocente.horas = paramDouble;
      return;
    }
    StateManager localStateManager = paramRelacionRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionRegistroDocente.horas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRelacionRegistroDocente, jdoInheritedFieldCount + 0, paramRelacionRegistroDocente.horas, paramDouble);
  }

  private static final long jdoGetidRelacionRegistroDocente(RelacionRegistroDocente paramRelacionRegistroDocente)
  {
    return paramRelacionRegistroDocente.idRelacionRegistroDocente;
  }

  private static final void jdoSetidRelacionRegistroDocente(RelacionRegistroDocente paramRelacionRegistroDocente, long paramLong)
  {
    StateManager localStateManager = paramRelacionRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionRegistroDocente.idRelacionRegistroDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramRelacionRegistroDocente, jdoInheritedFieldCount + 1, paramRelacionRegistroDocente.idRelacionRegistroDocente, paramLong);
  }

  private static final RegistroDocente jdoGetregistroDocenteDestino(RelacionRegistroDocente paramRelacionRegistroDocente)
  {
    StateManager localStateManager = paramRelacionRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRelacionRegistroDocente.registroDocenteDestino;
    if (localStateManager.isLoaded(paramRelacionRegistroDocente, jdoInheritedFieldCount + 2))
      return paramRelacionRegistroDocente.registroDocenteDestino;
    return (RegistroDocente)localStateManager.getObjectField(paramRelacionRegistroDocente, jdoInheritedFieldCount + 2, paramRelacionRegistroDocente.registroDocenteDestino);
  }

  private static final void jdoSetregistroDocenteDestino(RelacionRegistroDocente paramRelacionRegistroDocente, RegistroDocente paramRegistroDocente)
  {
    StateManager localStateManager = paramRelacionRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionRegistroDocente.registroDocenteDestino = paramRegistroDocente;
      return;
    }
    localStateManager.setObjectField(paramRelacionRegistroDocente, jdoInheritedFieldCount + 2, paramRelacionRegistroDocente.registroDocenteDestino, paramRegistroDocente);
  }

  private static final RegistroDocente jdoGetregistroDocenteOrigen(RelacionRegistroDocente paramRelacionRegistroDocente)
  {
    StateManager localStateManager = paramRelacionRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRelacionRegistroDocente.registroDocenteOrigen;
    if (localStateManager.isLoaded(paramRelacionRegistroDocente, jdoInheritedFieldCount + 3))
      return paramRelacionRegistroDocente.registroDocenteOrigen;
    return (RegistroDocente)localStateManager.getObjectField(paramRelacionRegistroDocente, jdoInheritedFieldCount + 3, paramRelacionRegistroDocente.registroDocenteOrigen);
  }

  private static final void jdoSetregistroDocenteOrigen(RelacionRegistroDocente paramRelacionRegistroDocente, RegistroDocente paramRegistroDocente)
  {
    StateManager localStateManager = paramRelacionRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRelacionRegistroDocente.registroDocenteOrigen = paramRegistroDocente;
      return;
    }
    localStateManager.setObjectField(paramRelacionRegistroDocente, jdoInheritedFieldCount + 3, paramRelacionRegistroDocente.registroDocenteOrigen, paramRegistroDocente);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}