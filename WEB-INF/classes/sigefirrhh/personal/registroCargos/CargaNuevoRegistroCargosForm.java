package sigefirrhh.personal.registroCargos;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.StringTokenizer;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;
import sigefirrhh.personal.trabajador.TrabajadorFacadeExtend;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CargaNuevoRegistroCargosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargaNuevoRegistroCargosForm.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private Collection listTipoPersonal;
  private Collection listError;
  private long idTipoPersonal;
  private String grabarConceptos;
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private String accion;
  private UploadedFile archivo;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private TrabajadorFacadeExtend trabajadorFacade;
  private Collection temporalConceptos;
  private TipoPersonal tipoPersonal;
  private CalcularConceptoBeanBusiness calcularConceptoBeanBusines = new CalcularConceptoBeanBusiness();
  private File fileError;
  private boolean hasError;
  private StringBuffer sqlConcepto = new StringBuffer();
  private StringBuffer sqlTrabajador = new StringBuffer();
  private StringBuffer sqlPrestamo = new StringBuffer();
  private StringBuffer sqlConceptoVariable = new StringBuffer();
  private StringBuffer sqlActualizar = new StringBuffer();

  private StringBuffer sqlDependencia = new StringBuffer();
  private StringBuffer sqlSede = new StringBuffer();
  private StringBuffer sqlRegistro = new StringBuffer();
  private StringBuffer sqlManualCargo = new StringBuffer();
  private StringBuffer sqlCargo = new StringBuffer();
  private StringBuffer sqlValida1 = new StringBuffer();
  private StringBuffer sqlValida2 = new StringBuffer();
  private StringBuffer sqlValida3 = new StringBuffer();
  private StringBuffer sqlValida4 = new StringBuffer();
  private StringBuffer sqlValida5 = new StringBuffer();

  StringBuffer sql = new StringBuffer();

  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  Statement stExecuteDelete = null;
  Statement stBorrarRegistro = null;

  private ResultSet rsConcepto = null;
  private PreparedStatement stConcepto = null;
  private ResultSet rsTrabajador = null;
  private PreparedStatement stTrabajador = null;
  private ResultSet rsPrestamo = null;
  private PreparedStatement stPrestamo = null;
  private ResultSet rsConceptoVariable = null;
  private PreparedStatement stConceptoVariable = null;

  private ResultSet rsDependencia = null;
  private PreparedStatement stDependencia = null;
  private ResultSet rsSede = null;
  private PreparedStatement stSede = null;
  private ResultSet rsRegistro = null;
  private PreparedStatement stRegistro = null;
  private ResultSet rsManualCargo = null;
  private PreparedStatement stManualCargo = null;
  private ResultSet rsCargo = null;
  private PreparedStatement stCargo = null;

  private ResultSet rsValida1 = null;
  private PreparedStatement stValida1 = null;
  private ResultSet rsValida2 = null;
  private PreparedStatement stValida2 = null;
  private ResultSet rsValida3 = null;
  private PreparedStatement stValida3 = null;
  private ResultSet rsValida4 = null;
  private PreparedStatement stValida4 = null;
  private ResultSet rsValida5 = null;
  private PreparedStatement stValida5 = null;
  private java.util.Date fechaInicio;
  private java.sql.Date fechaInicioSql;
  int dia = 0;
  int mes = 0;
  int anio = 0;
  String fecha;
  private long idTrabajador;
  private long idCargo;
  private long idManualCargo;
  private long idRegistro;
  private long idDependencia;
  private long idSede;
  private java.util.Date fechaActual = new java.util.Date();
  java.sql.Date fechaActualSql = new java.sql.Date(this.fechaActual.getYear(), this.fechaActual.getMonth(), this.fechaActual.getDate());

  public CargaNuevoRegistroCargosForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.trabajadorFacade = new TrabajadorFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    try {
      this.connection.setAutoCommit(false);
      this.connection = Resource.getConnection();

      this.stBorrarRegistro = this.connection.createStatement();
      this.sql.append("delete from registrocargosnuevo");
      this.stBorrarRegistro.addBatch(this.sql.toString());
      this.stBorrarRegistro.executeBatch();

      this.sqlTrabajador.append("select t.id_trabajador ");
      this.sqlTrabajador.append(" from trabajador t, tipopersonal tp");
      this.sqlTrabajador.append(" where cedula = ? ");
      this.sqlTrabajador.append(" and t.id_tipo_personal = tp.id_tipo_personal");
      this.sqlTrabajador.append(" and tp.id_tipo_personal = ? ");
      this.sqlTrabajador.append(" and t.estatus = 'A' ");
      this.stTrabajador = this.connection.prepareStatement(
        this.sqlTrabajador.toString(), 
        1003, 
        1007);

      this.sqlRegistro.append("select id_registro ");
      this.sqlRegistro.append(" from registro");
      this.sqlRegistro.append(" where numero_registro  = ? ");
      this.stRegistro = this.connection.prepareStatement(
        this.sqlRegistro.toString(), 
        1003, 
        1007);

      this.sqlSede.append("select id_sede ");
      this.sqlSede.append(" from sede");
      this.sqlSede.append(" where cod_sede  = ? ");
      this.stSede = this.connection.prepareStatement(
        this.sqlSede.toString(), 
        1003, 
        1007);

      this.sqlDependencia.append("select id_dependencia  ");
      this.sqlDependencia.append(" from dependencia ");
      this.sqlDependencia.append(" where cod_dependencia  = ? ");
      this.stDependencia = this.connection.prepareStatement(
        this.sqlDependencia.toString(), 
        1003, 
        1007);

      this.sqlManualCargo.append("select id_manual_cargo  ");
      this.sqlManualCargo.append(" from manualcargo");
      this.sqlManualCargo.append(" where cod_manual_cargo  = ? ");
      this.stManualCargo = this.connection.prepareStatement(
        this.sqlManualCargo.toString(), 
        1003, 
        1007);

      this.sqlCargo.append("select id_cargo ");
      this.sqlCargo.append(" from cargo");
      this.sqlCargo.append(" where id_manual_cargo  = ? ");
      this.sqlCargo.append("   and cod_cargo  = ? ");

      this.stCargo = this.connection.prepareStatement(
        this.sqlCargo.toString(), 
        1003, 
        1007);

      this.sqlValida1.append("select max(id_registro) as maximo,min(id_registro) as minimo ");
      this.sqlValida1.append(" from registrocargosnuevo");

      this.stValida1 = this.connection.prepareStatement(
        this.sqlValida1.toString(), 
        1003, 
        1007);

      this.sqlValida2.append("select count(*) as contador from registrocargos ");
      this.sqlValida2.append(" where id_registro = ? ");

      this.stValida2 = this.connection.prepareStatement(
        this.sqlValida2.toString(), 
        1003, 
        1007);

      this.sqlValida3.append("select count(*) as contador,codigo_nomina  from registrocargosnuevo");
      this.sqlValida3.append("  group by codigo_nomina having count(*) > 1 ");

      this.stValida3 = this.connection.prepareStatement(
        this.sqlValida3.toString(), 
        1003, 
        1007);

      this.sqlValida4.append("select count(*) as contador,t.cedula   from registrocargosnuevo r, trabajador t");
      this.sqlValida4.append("where r.id_trabajador = t.id_trabajador");
      this.sqlValida4.append("  group by t.cedula having count(*) > 1 ");

      this.stValida4 = this.connection.prepareStatement(
        this.sqlValida4.toString(), 
        1003, 
        1007);

      this.sqlValida5.append("select t.cedula from trabajador t ");
      this.sqlValida5.append(" where t.id_tipo_personal = ? ");
      this.sqlValida5.append("   and t.estatus ='A'  ");
      this.sqlValida5.append("   and t.id_trabajador not in (select rn.id_trabajador from registrocargosnuevo rn) ");

      this.stValida5 = this.connection.prepareStatement(
        this.sqlValida5.toString(), 
        1003, 
        1007);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      return;
    }
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String cargar()
  {
    log.error("VERSION DEL 0310 ");
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

    actualizarCampos();
    this.listError = new ArrayList();
    try {
      this.stExecute = null;
      this.stExecuteDelete = null;
      this.stExecute = this.connection.createStatement();
      this.stExecuteDelete = this.connection.createStatement();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return null;
    }

    boolean success = new File(request.getRealPath("temp")).mkdir();

    this.fileError = new File(
      ((ServletContext)context.getExternalContext().getContext())
      .getRealPath("/temp") + "/error" + newFileError(0) + ".txt");
    FileWriter fileErrorWriter = null;
    try {
      this.fileError.createNewFile();
      fileErrorWriter = new FileWriter(this.fileError);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    int i = 0;
    try {
      BufferedReader file = 
        new BufferedReader(
        new InputStreamReader(
        this.archivo.getInputStream()), 250);
      String linea = null;
      StringTokenizer lineaHelper = null;

      int cedula = 0;
      int codigoNomina = 0;
      int numeroRegistro = 0;
      double horas = 0.0D;
      boolean error = false;
      String codConcepto = null;
      String codCargo = null;
      String codSede = null;
      String codDependencia = null;
      String codManualCargo = null;
      String situacion = null;
      String aprobadoMpd = null;

      this.hasError = false;
      int j = 0;
      while ((linea = file.readLine()) != null) {
        log.error("PASO 1 ");
        log.error("ACCION " + this.accion);
        j++;

        lineaHelper = new StringTokenizer(linea, "\t");
        error = false;
        i++;

        numeroRegistro = Integer.parseInt(lineaHelper.nextToken());

        this.stRegistro.setInt(1, numeroRegistro);
        this.rsRegistro = this.stRegistro.executeQuery();

        if (!this.rsRegistro.next()) {
          fileErrorWriter.write(
            "El número de registro " + numeroRegistro + " no esta creado aun. Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idRegistro = this.rsRegistro.getLong("id_registro");
        }

        log.error("PASO 2 ");
        this.rsRegistro.close();

        codigoNomina = Integer.parseInt(lineaHelper.nextToken());

        codManualCargo = lineaHelper.nextToken();

        this.stManualCargo.setString(1, codManualCargo);
        this.rsManualCargo = this.stManualCargo.executeQuery();

        if (!this.rsManualCargo.next())
        {
          fileErrorWriter.write(
            "El manual de cargos " + codManualCargo + " no existe esta creado aún. Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idManualCargo = this.rsManualCargo.getInt("id_manual_cargo");
        }

        log.error("PASO 3 ");
        this.rsManualCargo.close();

        codCargo = lineaHelper.nextToken();

        this.stCargo.setLong(1, this.idManualCargo);
        this.stCargo.setString(2, codCargo);
        this.rsCargo = this.stCargo.executeQuery();

        if (!this.rsCargo.next())
        {
          fileErrorWriter.write(
            "El codigo de cargo " + codCargo + " no existe en la manual de cargos " + codManualCargo + ". Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idCargo = this.rsCargo.getInt("id_cargo");
        }

        log.error("PASO 4 ");
        this.rsCargo.close();

        codSede = lineaHelper.nextToken();

        this.stSede.setString(1, codSede);
        this.rsSede = this.stSede.executeQuery();

        if (!this.rsSede.next())
        {
          fileErrorWriter.write(
            "La sede " + codSede + " no existe . Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idSede = this.rsSede.getInt("id_sede");
        }

        log.error("PASO 5 ");
        this.rsSede.close();

        codDependencia = lineaHelper.nextToken();

        this.stDependencia.setString(1, codDependencia);
        this.rsDependencia = this.stDependencia.executeQuery();

        if (!this.rsDependencia.next())
        {
          fileErrorWriter.write(
            "La dependencia " + codDependencia + " no existe  aún. Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        } else {
          this.idDependencia = this.rsDependencia.getInt("id_dependencia");
        }

        log.error("PASO 6 ");
        this.rsDependencia.close();

        aprobadoMpd = lineaHelper.nextToken();
        if ((!aprobadoMpd.equals("S")) && (!aprobadoMpd.equals("N"))) {
          fileErrorWriter.write(
            "Aprobado MPD es incorrecta valores correctos deben ser (S=Aprobado,N=No aprobado). Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        }

        situacion = lineaHelper.nextToken();
        if ((!situacion.equals("O")) && (!situacion.equals("V"))) {
          fileErrorWriter.write(
            "La situacion es incorrecta valores correctos deben ser (V=Vacante,O=Ocupado). Línea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        }

        cedula = Integer.parseInt(lineaHelper.nextToken());

        if (cedula != 0) {
          this.stTrabajador.setInt(1, cedula);
          this.stTrabajador.setLong(2, this.tipoPersonal.getIdTipoPersonal());
          this.rsTrabajador = this.stTrabajador.executeQuery();

          if (!this.rsTrabajador.next()) {
            fileErrorWriter.write(
              "El trabajador con la cédula " + cedula + " no existe para el tipo de personal " + this.tipoPersonal.getNombre() + ". Línea " + i + "\n");
            error = true;
            this.hasError = error;
            fileErrorWriter.flush();
          } else {
            this.idTrabajador = this.rsTrabajador.getLong("id_trabajador");
          }

        }

        if ((cedula == 0) && (situacion.equals("O"))) {
          fileErrorWriter.write(
            "Código de Nómina dice Ocupado y no tiene Cédula asociada. Linea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        }

        if ((cedula != 0) && (situacion.equals("V"))) {
          fileErrorWriter.write(
            "Código de Nómina dice Vacante y tiene cédula asociada. Linea " + i + "\n");
          error = true;
          this.hasError = error;
          fileErrorWriter.flush();
        }

        log.error("PASO 7 ");
        if (cedula != 0) {
          this.rsTrabajador.close();
        }

        if (lineaHelper.hasMoreTokens())
        {
          try
          {
            this.fecha = lineaHelper.nextToken();
            log.error("fecha" + this.fecha);
            this.dia = Integer.valueOf(this.fecha.substring(0, 2)).intValue();
            log.error("dia" + this.dia);
            this.mes = Integer.valueOf(this.fecha.substring(3, 5)).intValue();
            log.error("mes" + this.mes);
            this.anio = Integer.valueOf(this.fecha.toString().substring(6, 10)).intValue();
            log.error("anio" + this.anio);
            this.fechaInicioSql = new java.sql.Date(this.anio - 1900, this.mes - 1, this.dia);
          } catch (Exception e) {
            log.error("Excepcion controlada:", e);
            fileErrorWriter.write(
              "Error, verifique el formato de la fecha. Línea " + i + "\n");
            error = true;
            this.hasError = error;
            fileErrorWriter.flush();
          }
        }

        horas = NumberTools.twoDecimal(Double.parseDouble(lineaHelper.nextToken()));

        log.error("PASO 8 ");

        if (!error) {
          this.sqlActualizar = new StringBuffer();
          this.sqlActualizar = new StringBuffer();
          log.error("PASO 9 ");
          this.sqlActualizar.append("insert into registrocargosnuevo (id_registro_cargos, id_registro, ");
          this.sqlActualizar.append(" codigo_nomina , situacion ,fecha_creacion ,");
          this.sqlActualizar.append(" horas , id_trabajador , id_cargo ,");
          this.sqlActualizar.append(" id_dependencia , id_sede , aprobado_mpd) values(");
          this.sqlActualizar.append(i + ", ");
          this.sqlActualizar.append(this.idRegistro + ", ");
          this.sqlActualizar.append(codigoNomina + ", '" + situacion + "', '");
          this.sqlActualizar.append(this.fechaInicioSql + "'," + horas + ", " + this.idTrabajador + ", " + this.idCargo + ", ");
          this.sqlActualizar.append(this.idDependencia + "," + this.idSede + ", '" + aprobadoMpd + "')");
          this.stExecute.addBatch(this.sqlActualizar.toString());
          log.error("PASO 10 ");
        }

      }

      if (this.hasError)
        context.addMessage("success", new FacesMessage("Se cargó con éxito. Pero hay registros con datos incoherentes. Este archivo podrá ser procesado sin embargo, las incoherencias no será procesadas. Baje el archivo a continuación para obtener mas detalle de los errores (use el botón derecho del mouse)."));
      else {
        context.addMessage("success", new FacesMessage("Se cargó con éxito"));
      }
      file.close();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.temporalConceptos = null;
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Archivo con error. Linea " + i, ""));
    }
    try {
      if (fileErrorWriter != null)
        fileErrorWriter.close();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String getUrlError() {
    return this.fileError != null ? "/sigefirrhh/temp/" + this.fileError.getName() : null;
  }

  public static String newFileError(int fileId) {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(10000));
    while (id == fileId);
    return String.valueOf(id);
  }

  public String procesar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    actualizarCampos();
    try
    {
      this.stExecute.executeBatch();
      this.stExecute.close();

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.tipoPersonal);

      this.rsValida1 = this.stValida1.executeQuery();

      if (this.rsValida1.next()) {
        int maximo = this.rsValida1.getInt("maximo");
        int minimo = this.rsValida1.getInt("minimo");
        if (maximo != minimo) {
          context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Tiene mas de un tipo Número de registro de cargos a cargar", ""));
        }
      }

      this.stValida2.setLong(1, this.idRegistro);
      this.rsValida2 = this.stValida2.executeQuery();

      if (this.rsValida2.next()) {
        int contador = this.rsValida2.getInt("contador");
        if (contador > 0) {
          context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ya se han creado Registros Cargos para este número de registro por la aplicación", ""));
        }
      }

      this.rsValida3 = this.stValida3.executeQuery();

      if (this.rsValida3.next()) {
        int contador = this.rsValida3.getInt("contador");
        int codigoNomina = this.rsValida3.getInt("codigo_nomina");

        if (contador > 0) {
          context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El código de nómina " + codigoNomina, " esta creado más de una vez en el archivo indicado. Revise y depure el archivo"));
        }
      }

      this.rsValida4 = this.stValida4.executeQuery();

      if (this.rsValida4.next()) {
        int contador = this.rsValida4.getInt("contador");
        int cedula = this.rsValida4.getInt("cedula");

        if (contador > 0) {
          context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador con la cédula " + cedula, " tiene asociado más de un código de nómina. Revise y depure el archivo"));
        }
      }

      this.stValida5.setLong(1, this.tipoPersonal.getIdTipoPersonal());
      this.rsValida5 = this.stValida5.executeQuery();

      if (this.rsValida5.next()) {
        int cedula = this.rsValida5.getInt("cedula");
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador con la cédula " + cedula, " esta activo en el tipo de personal a procesar y no tiene asociado código de nómina en el archivo indicado. Revise y depure el archivo"));
      }

      context.addMessage("success", new FacesMessage("Se procesó con éxito"));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hay un error en el archivo", ""));
    }
    return null;
  }

  public void actualizarCampos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(
        this.idTipoPersonal);
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", 
        new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ha seleccionado un tipo de personal", ""));
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (idTipoPersonal > 0L)
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public UploadedFile getArchivo() {
    return this.archivo;
  }

  public String getGrabarConceptos() {
    return this.grabarConceptos;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public void setArchivo(UploadedFile file) {
    this.archivo = file;
  }

  public void setGrabarConceptos(String string) {
    this.grabarConceptos = string;
  }

  public void setListTipoPersonal(Collection collection)
  {
    this.listTipoPersonal = collection;
  }
  public String getAccion() {
    return this.accion;
  }

  public void setAccion(String string) {
    this.accion = string;
  }

  public Collection getListError() {
    return this.listError;
  }

  public void setListError(Collection collection) {
    this.listError = collection;
  }

  public boolean isHasError() {
    return this.hasError;
  }

  public void setHasError(boolean b) {
    this.hasError = b;
  }
}