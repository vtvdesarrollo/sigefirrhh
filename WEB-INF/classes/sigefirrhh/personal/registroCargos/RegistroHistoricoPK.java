package sigefirrhh.personal.registroCargos;

import java.io.Serializable;

public class RegistroHistoricoPK
  implements Serializable
{
  public long idRegistroHistorico;

  public RegistroHistoricoPK()
  {
  }

  public RegistroHistoricoPK(long idRegistroHistorico)
  {
    this.idRegistroHistorico = idRegistroHistorico;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegistroHistoricoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegistroHistoricoPK thatPK)
  {
    return 
      this.idRegistroHistorico == thatPK.idRegistroHistorico;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegistroHistorico)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegistroHistorico);
  }
}