package sigefirrhh.personal.registroCargos;

import java.io.Serializable;

public class AperturaEscolarPK
  implements Serializable
{
  public long idAperturaEscolar;

  public AperturaEscolarPK()
  {
  }

  public AperturaEscolarPK(long idAperturaEscolar)
  {
    this.idAperturaEscolar = idAperturaEscolar;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AperturaEscolarPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AperturaEscolarPK thatPK)
  {
    return 
      this.idAperturaEscolar == thatPK.idAperturaEscolar;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAperturaEscolar)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAperturaEscolar);
  }
}