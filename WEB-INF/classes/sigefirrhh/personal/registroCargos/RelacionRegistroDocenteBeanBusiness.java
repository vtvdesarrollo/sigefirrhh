package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RelacionRegistroDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RelacionRegistroDocente relacionRegistroDocenteNew = 
      (RelacionRegistroDocente)BeanUtils.cloneBean(
      relacionRegistroDocente);

    RegistroDocenteBeanBusiness registroDocenteOrigenBeanBusiness = new RegistroDocenteBeanBusiness();

    if (relacionRegistroDocenteNew.getRegistroDocenteOrigen() != null) {
      relacionRegistroDocenteNew.setRegistroDocenteOrigen(
        registroDocenteOrigenBeanBusiness.findRegistroDocenteById(
        relacionRegistroDocenteNew.getRegistroDocenteOrigen().getIdRegistroDocente()));
    }

    RegistroDocenteBeanBusiness registroDocenteDestinoBeanBusiness = new RegistroDocenteBeanBusiness();

    if (relacionRegistroDocenteNew.getRegistroDocenteDestino() != null) {
      relacionRegistroDocenteNew.setRegistroDocenteDestino(
        registroDocenteDestinoBeanBusiness.findRegistroDocenteById(
        relacionRegistroDocenteNew.getRegistroDocenteDestino().getIdRegistroDocente()));
    }
    pm.makePersistent(relacionRegistroDocenteNew);
  }

  public void updateRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente) throws Exception
  {
    RelacionRegistroDocente relacionRegistroDocenteModify = 
      findRelacionRegistroDocenteById(relacionRegistroDocente.getIdRelacionRegistroDocente());

    RegistroDocenteBeanBusiness registroDocenteOrigenBeanBusiness = new RegistroDocenteBeanBusiness();

    if (relacionRegistroDocente.getRegistroDocenteOrigen() != null) {
      relacionRegistroDocente.setRegistroDocenteOrigen(
        registroDocenteOrigenBeanBusiness.findRegistroDocenteById(
        relacionRegistroDocente.getRegistroDocenteOrigen().getIdRegistroDocente()));
    }

    RegistroDocenteBeanBusiness registroDocenteDestinoBeanBusiness = new RegistroDocenteBeanBusiness();

    if (relacionRegistroDocente.getRegistroDocenteDestino() != null) {
      relacionRegistroDocente.setRegistroDocenteDestino(
        registroDocenteDestinoBeanBusiness.findRegistroDocenteById(
        relacionRegistroDocente.getRegistroDocenteDestino().getIdRegistroDocente()));
    }

    BeanUtils.copyProperties(relacionRegistroDocenteModify, relacionRegistroDocente);
  }

  public void deleteRelacionRegistroDocente(RelacionRegistroDocente relacionRegistroDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RelacionRegistroDocente relacionRegistroDocenteDelete = 
      findRelacionRegistroDocenteById(relacionRegistroDocente.getIdRelacionRegistroDocente());
    pm.deletePersistent(relacionRegistroDocenteDelete);
  }

  public RelacionRegistroDocente findRelacionRegistroDocenteById(long idRelacionRegistroDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRelacionRegistroDocente == pIdRelacionRegistroDocente";
    Query query = pm.newQuery(RelacionRegistroDocente.class, filter);

    query.declareParameters("long pIdRelacionRegistroDocente");

    parameters.put("pIdRelacionRegistroDocente", new Long(idRelacionRegistroDocente));

    Collection colRelacionRegistroDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRelacionRegistroDocente.iterator();
    return (RelacionRegistroDocente)iterator.next();
  }

  public Collection findRelacionRegistroDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent relacionRegistroDocenteExtent = pm.getExtent(
      RelacionRegistroDocente.class, true);
    Query query = pm.newQuery(relacionRegistroDocenteExtent);
    query.setOrdering("registroDocenteOrigen.codigoNomina ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByRegistroDocenteOrigen(long idRegistroDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registroDocenteOrigen.idRegistroDocente == pIdRegistroDocente";

    Query query = pm.newQuery(RelacionRegistroDocente.class, filter);

    query.declareParameters("long pIdRegistroDocente");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistroDocente", new Long(idRegistroDocente));

    query.setOrdering("registroDocenteOrigen.codigoNomina ascending");

    Collection colRelacionRegistroDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRelacionRegistroDocente);

    return colRelacionRegistroDocente;
  }

  public Collection findByRegistroDocenteDestino(long idRegistroDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registroDocenteDestino.idRegistroDocente == pIdRegistroDocente";

    Query query = pm.newQuery(RelacionRegistroDocente.class, filter);

    query.declareParameters("long pIdRegistroDocente");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistroDocente", new Long(idRegistroDocente));

    query.setOrdering("registroDocenteOrigen.codigoNomina ascending");

    Collection colRelacionRegistroDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRelacionRegistroDocente);

    return colRelacionRegistroDocente;
  }
}