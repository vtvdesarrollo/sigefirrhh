package sigefirrhh.personal.registroCargos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.trabajador.Trabajador;

public class RegistroDocente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ORIGEN;
  protected static final Map LISTA_SITUACION;
  private long idRegistroDocente;
  private String situacion;
  private String origen;
  private Cargo cargo;
  private Dependencia dependencia;
  private AperturaEscolar aperturaEscolar;
  private Trabajador trabajador;
  private double horasAdministrativas;
  private double horasDocente;
  private double totalHoras;
  private double horasRestante;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aperturaEscolar", "cargo", "dependencia", "horasAdministrativas", "horasDocente", "horasRestante", "idRegistroDocente", "origen", "situacion", "totalHoras", "trabajador" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.registroCargos.AperturaEscolar"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Double.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 26, 26, 26, 21, 21, 21, 24, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RegistroDocente());

    LISTA_ORIGEN = 
      new LinkedHashMap();
    LISTA_SITUACION = 
      new LinkedHashMap();
    LISTA_ORIGEN.put("A", "APERTURA");
    LISTA_ORIGEN.put("S", "SUSTITUCION");
    LISTA_ORIGEN.put("T", "TRASLADO");
    LISTA_SITUACION.put("O", "OCUPADO");
    LISTA_SITUACION.put("V", "VACANTE");
    LISTA_SITUACION.put("E", "ELIMINADO");
    LISTA_SITUACION.put("C", "CONGELADO");
    LISTA_SITUACION.put("T", "TRANSFERIDO");
  }

  public RegistroDocente()
  {
    jdoSetsituacion(this, "O");

    jdoSetorigen(this, "1");

    jdoSethorasAdministrativas(this, 0.0D);

    jdoSethorasDocente(this, 0.0D);

    jdoSettotalHoras(this, 0.0D);

    jdoSethorasRestante(this, 0.0D);
  }

  public String toString()
  {
    return 
      jdoGetcargo(this).getCodCargo() + " " + jdoGetcargo(this).getDescripcionCargo() + " - " + 
      jdoGettrabajador(this).getCedula() + " " + 
      jdoGettrabajador(this).getPersonal().getPrimerApellido() + " " + 
      jdoGettrabajador(this).getPersonal().getPrimerNombre();
  }

  public AperturaEscolar getAperturaEscolar()
  {
    return jdoGetaperturaEscolar(this);
  }

  public void setAperturaEscolar(AperturaEscolar aperturaEscolar)
  {
    jdoSetaperturaEscolar(this, aperturaEscolar);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public double getHorasAdministrativas()
  {
    return jdoGethorasAdministrativas(this);
  }

  public void setHorasAdministrativas(double horasAdministrativas)
  {
    jdoSethorasAdministrativas(this, horasAdministrativas);
  }

  public long getIdRegistroDocente() {
    return jdoGetidRegistroDocente(this);
  }

  public void setIdRegistroDocente(long idRegistroDocente)
  {
    jdoSetidRegistroDocente(this, idRegistroDocente);
  }

  public String getSituacion() {
    return jdoGetsituacion(this);
  }

  public void setSituacion(String situacion)
  {
    jdoSetsituacion(this, situacion);
  }

  public double getTotalHoras()
  {
    return jdoGettotalHoras(this);
  }

  public void setTotalHoras(double totalHoras)
  {
    jdoSettotalHoras(this, totalHoras);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public double getHorasDocente() {
    return jdoGethorasDocente(this);
  }

  public void setHorasDocente(double horasDocente) {
    jdoSethorasDocente(this, horasDocente);
  }

  public double getHorasRestante() {
    return jdoGethorasRestante(this);
  }

  public void setHorasRestante(double horasRestante) {
    jdoSethorasRestante(this, horasRestante);
  }

  public String getOrigen() {
    return jdoGetorigen(this);
  }

  public void setOrigen(String origen) {
    jdoSetorigen(this, origen);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RegistroDocente localRegistroDocente = new RegistroDocente();
    localRegistroDocente.jdoFlags = 1;
    localRegistroDocente.jdoStateManager = paramStateManager;
    return localRegistroDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RegistroDocente localRegistroDocente = new RegistroDocente();
    localRegistroDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegistroDocente.jdoFlags = 1;
    localRegistroDocente.jdoStateManager = paramStateManager;
    return localRegistroDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.aperturaEscolar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horasAdministrativas);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horasDocente);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horasRestante);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegistroDocente);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalHoras);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aperturaEscolar = ((AperturaEscolar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horasAdministrativas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horasDocente = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horasRestante = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegistroDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalHoras = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RegistroDocente paramRegistroDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.aperturaEscolar = paramRegistroDocente.aperturaEscolar;
      return;
    case 1:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramRegistroDocente.cargo;
      return;
    case 2:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramRegistroDocente.dependencia;
      return;
    case 3:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.horasAdministrativas = paramRegistroDocente.horasAdministrativas;
      return;
    case 4:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.horasDocente = paramRegistroDocente.horasDocente;
      return;
    case 5:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.horasRestante = paramRegistroDocente.horasRestante;
      return;
    case 6:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idRegistroDocente = paramRegistroDocente.idRegistroDocente;
      return;
    case 7:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramRegistroDocente.origen;
      return;
    case 8:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramRegistroDocente.situacion;
      return;
    case 9:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.totalHoras = paramRegistroDocente.totalHoras;
      return;
    case 10:
      if (paramRegistroDocente == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramRegistroDocente.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RegistroDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RegistroDocente localRegistroDocente = (RegistroDocente)paramObject;
    if (localRegistroDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegistroDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegistroDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegistroDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroDocentePK))
      throw new IllegalArgumentException("arg1");
    RegistroDocentePK localRegistroDocentePK = (RegistroDocentePK)paramObject;
    localRegistroDocentePK.idRegistroDocente = this.idRegistroDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroDocentePK))
      throw new IllegalArgumentException("arg1");
    RegistroDocentePK localRegistroDocentePK = (RegistroDocentePK)paramObject;
    this.idRegistroDocente = localRegistroDocentePK.idRegistroDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroDocentePK))
      throw new IllegalArgumentException("arg2");
    RegistroDocentePK localRegistroDocentePK = (RegistroDocentePK)paramObject;
    localRegistroDocentePK.idRegistroDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroDocentePK))
      throw new IllegalArgumentException("arg2");
    RegistroDocentePK localRegistroDocentePK = (RegistroDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localRegistroDocentePK.idRegistroDocente);
  }

  private static final AperturaEscolar jdoGetaperturaEscolar(RegistroDocente paramRegistroDocente)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.aperturaEscolar;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 0))
      return paramRegistroDocente.aperturaEscolar;
    return (AperturaEscolar)localStateManager.getObjectField(paramRegistroDocente, jdoInheritedFieldCount + 0, paramRegistroDocente.aperturaEscolar);
  }

  private static final void jdoSetaperturaEscolar(RegistroDocente paramRegistroDocente, AperturaEscolar paramAperturaEscolar)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.aperturaEscolar = paramAperturaEscolar;
      return;
    }
    localStateManager.setObjectField(paramRegistroDocente, jdoInheritedFieldCount + 0, paramRegistroDocente.aperturaEscolar, paramAperturaEscolar);
  }

  private static final Cargo jdoGetcargo(RegistroDocente paramRegistroDocente)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.cargo;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 1))
      return paramRegistroDocente.cargo;
    return (Cargo)localStateManager.getObjectField(paramRegistroDocente, jdoInheritedFieldCount + 1, paramRegistroDocente.cargo);
  }

  private static final void jdoSetcargo(RegistroDocente paramRegistroDocente, Cargo paramCargo)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramRegistroDocente, jdoInheritedFieldCount + 1, paramRegistroDocente.cargo, paramCargo);
  }

  private static final Dependencia jdoGetdependencia(RegistroDocente paramRegistroDocente)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.dependencia;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 2))
      return paramRegistroDocente.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramRegistroDocente, jdoInheritedFieldCount + 2, paramRegistroDocente.dependencia);
  }

  private static final void jdoSetdependencia(RegistroDocente paramRegistroDocente, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramRegistroDocente, jdoInheritedFieldCount + 2, paramRegistroDocente.dependencia, paramDependencia);
  }

  private static final double jdoGethorasAdministrativas(RegistroDocente paramRegistroDocente)
  {
    if (paramRegistroDocente.jdoFlags <= 0)
      return paramRegistroDocente.horasAdministrativas;
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.horasAdministrativas;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 3))
      return paramRegistroDocente.horasAdministrativas;
    return localStateManager.getDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 3, paramRegistroDocente.horasAdministrativas);
  }

  private static final void jdoSethorasAdministrativas(RegistroDocente paramRegistroDocente, double paramDouble)
  {
    if (paramRegistroDocente.jdoFlags == 0)
    {
      paramRegistroDocente.horasAdministrativas = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.horasAdministrativas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 3, paramRegistroDocente.horasAdministrativas, paramDouble);
  }

  private static final double jdoGethorasDocente(RegistroDocente paramRegistroDocente)
  {
    if (paramRegistroDocente.jdoFlags <= 0)
      return paramRegistroDocente.horasDocente;
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.horasDocente;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 4))
      return paramRegistroDocente.horasDocente;
    return localStateManager.getDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 4, paramRegistroDocente.horasDocente);
  }

  private static final void jdoSethorasDocente(RegistroDocente paramRegistroDocente, double paramDouble)
  {
    if (paramRegistroDocente.jdoFlags == 0)
    {
      paramRegistroDocente.horasDocente = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.horasDocente = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 4, paramRegistroDocente.horasDocente, paramDouble);
  }

  private static final double jdoGethorasRestante(RegistroDocente paramRegistroDocente)
  {
    if (paramRegistroDocente.jdoFlags <= 0)
      return paramRegistroDocente.horasRestante;
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.horasRestante;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 5))
      return paramRegistroDocente.horasRestante;
    return localStateManager.getDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 5, paramRegistroDocente.horasRestante);
  }

  private static final void jdoSethorasRestante(RegistroDocente paramRegistroDocente, double paramDouble)
  {
    if (paramRegistroDocente.jdoFlags == 0)
    {
      paramRegistroDocente.horasRestante = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.horasRestante = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 5, paramRegistroDocente.horasRestante, paramDouble);
  }

  private static final long jdoGetidRegistroDocente(RegistroDocente paramRegistroDocente)
  {
    return paramRegistroDocente.idRegistroDocente;
  }

  private static final void jdoSetidRegistroDocente(RegistroDocente paramRegistroDocente, long paramLong)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.idRegistroDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegistroDocente, jdoInheritedFieldCount + 6, paramRegistroDocente.idRegistroDocente, paramLong);
  }

  private static final String jdoGetorigen(RegistroDocente paramRegistroDocente)
  {
    if (paramRegistroDocente.jdoFlags <= 0)
      return paramRegistroDocente.origen;
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.origen;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 7))
      return paramRegistroDocente.origen;
    return localStateManager.getStringField(paramRegistroDocente, jdoInheritedFieldCount + 7, paramRegistroDocente.origen);
  }

  private static final void jdoSetorigen(RegistroDocente paramRegistroDocente, String paramString)
  {
    if (paramRegistroDocente.jdoFlags == 0)
    {
      paramRegistroDocente.origen = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroDocente, jdoInheritedFieldCount + 7, paramRegistroDocente.origen, paramString);
  }

  private static final String jdoGetsituacion(RegistroDocente paramRegistroDocente)
  {
    if (paramRegistroDocente.jdoFlags <= 0)
      return paramRegistroDocente.situacion;
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.situacion;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 8))
      return paramRegistroDocente.situacion;
    return localStateManager.getStringField(paramRegistroDocente, jdoInheritedFieldCount + 8, paramRegistroDocente.situacion);
  }

  private static final void jdoSetsituacion(RegistroDocente paramRegistroDocente, String paramString)
  {
    if (paramRegistroDocente.jdoFlags == 0)
    {
      paramRegistroDocente.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroDocente, jdoInheritedFieldCount + 8, paramRegistroDocente.situacion, paramString);
  }

  private static final double jdoGettotalHoras(RegistroDocente paramRegistroDocente)
  {
    if (paramRegistroDocente.jdoFlags <= 0)
      return paramRegistroDocente.totalHoras;
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.totalHoras;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 9))
      return paramRegistroDocente.totalHoras;
    return localStateManager.getDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 9, paramRegistroDocente.totalHoras);
  }

  private static final void jdoSettotalHoras(RegistroDocente paramRegistroDocente, double paramDouble)
  {
    if (paramRegistroDocente.jdoFlags == 0)
    {
      paramRegistroDocente.totalHoras = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.totalHoras = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroDocente, jdoInheritedFieldCount + 9, paramRegistroDocente.totalHoras, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(RegistroDocente paramRegistroDocente)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroDocente.trabajador;
    if (localStateManager.isLoaded(paramRegistroDocente, jdoInheritedFieldCount + 10))
      return paramRegistroDocente.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramRegistroDocente, jdoInheritedFieldCount + 10, paramRegistroDocente.trabajador);
  }

  private static final void jdoSettrabajador(RegistroDocente paramRegistroDocente, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramRegistroDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroDocente.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramRegistroDocente, jdoInheritedFieldCount + 10, paramRegistroDocente.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}