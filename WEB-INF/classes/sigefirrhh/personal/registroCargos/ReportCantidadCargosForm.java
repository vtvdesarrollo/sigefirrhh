package sigefirrhh.personal.registroCargos;

import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;

public class ReportCantidadCargosForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportCantidadCargosForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String formato = "1";
  private String findRegistro;
  private String orden = "alf";
  private Collection colFindRegistro;
  private Collection colRegion;
  private Collection colUnidadFuncional;
  private String idRegion;
  private String idUnidadFuncional;
  private DefinicionesFacade definicionesFacade;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private LoginSession login;
  private ProcesoNominaNoGenFacade procesoNominaNoGenFacade = new ProcesoNominaNoGenFacade();
  private RegistroNoGenFacade registroFacade = new RegistroNoGenFacade();

  private int gradoDesde = 1;
  private int gradoHasta = 99;
  private boolean show;
  private boolean auxShow;
  private String seleccionUbicacion = "T";
  private String seleccionCargo = "T";

  private boolean showRegion = false;
  private boolean showUnidadFuncional = false;

  public boolean isShowUnidadFuncional()
  {
    return this.showUnidadFuncional;
  }
  public boolean isShowRegion() {
    return this.showRegion;
  }

  public ReportCantidadCargosForm() {
    this.reportName = "cantidadcargosalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.definicionesFacade = new DefinicionesFacade();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportCantidadCargosForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeSeleccionUbicacion(ValueChangeEvent event) {
    String seleccionUbicacion = 
      (String)event.getNewValue();
    this.seleccionUbicacion = seleccionUbicacion;
    try {
      this.showRegion = false;
      this.showUnidadFuncional = false;
      if (seleccionUbicacion.equals("R"))
        this.showRegion = true;
      else if (seleccionUbicacion.equals("U")) {
        this.showUnidadFuncional = true;
      }
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegion(ValueChangeEvent event)
  {
    String idRegion = 
      (String)event.getNewValue();
  }

  public void changeUnidadFuncional(ValueChangeEvent event)
  {
    String idUnidadFuncional = 
      (String)event.getNewValue();
  }

  public void changeRegistro(ValueChangeEvent event)
  {
    long idRegistro = 
      Long.valueOf((String)event.getNewValue()).longValue();
  }

  public Collection getColFindRegistro()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFindRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }
  public Collection getColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }
  public Collection getColUnidadFuncional() {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }

  private void cambiarNombreAReporte() {
    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }
    this.reportName = (this.reportName + "cantidadcargos" + this.orden);
    if (this.seleccionUbicacion.equals("R"))
      this.reportName += "reg";
    else if (this.seleccionUbicacion.equals("U"))
      this.reportName += "uf";
  }

  public void refresh()
  {
    try
    {
      this.colFindRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(this.login.getIdOrganismo());

      this.colUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error(e.getCause());
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_registro", new Long(this.findRegistro));
    parameters.put("p_gradodesde", new Integer(this.gradoDesde - 1));
    parameters.put("p_gradohasta", new Integer(this.gradoHasta + 1));

    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }

    this.reportName = (this.reportName + "cantidadcargos" + this.orden);
    if (this.seleccionUbicacion.equals("R")) {
      this.reportName += "reg";
      parameters.put("id_region", new Long(this.idRegion));
    } else if (this.seleccionUbicacion.equals("U")) {
      this.reportName += "uf";
      parameters.put("id_unidad_funcional", new Long(this.idUnidadFuncional));
    }

    JasperForWeb report = new JasperForWeb();

    if (this.formato.equals("2"))
      report.setType(3);
    else {
      report.setType(1);
    }
    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/registroCargos");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte() {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i) {
    this.tipoReporte = i;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int i) {
    this.reportId = i;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String string) {
    this.reportName = string;
  }

  public int getGradoDesde() {
    return this.gradoDesde;
  }

  public int getGradoHasta() {
    return this.gradoHasta;
  }

  public void setGradoDesde(int i) {
    this.gradoDesde = i;
  }

  public void setGradoHasta(int i) {
    this.gradoHasta = i;
  }

  public String getFormato() {
    return this.formato;
  }

  public void setFormato(String string) {
    this.formato = string;
  }
  public String getSeleccionUbicacion() {
    return this.seleccionUbicacion;
  }
  public void setSeleccionUbicacion(String seleccionUbicacion) {
    this.seleccionUbicacion = seleccionUbicacion;
  }

  public String getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(String idRegion) {
    this.idRegion = idRegion;
  }
  public String getFindRegistro() {
    return this.findRegistro;
  }
  public void setFindRegistro(String findRegistro) {
    this.findRegistro = findRegistro;
  }
  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }
  public String getIdUnidadFuncional() {
    return this.idUnidadFuncional;
  }
  public void setIdUnidadFuncional(String idUnidadFuncional) {
    this.idUnidadFuncional = idUnidadFuncional;
  }
}