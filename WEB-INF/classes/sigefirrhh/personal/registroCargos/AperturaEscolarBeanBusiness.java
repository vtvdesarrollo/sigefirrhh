package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;

public class AperturaEscolarBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAperturaEscolar(AperturaEscolar aperturaEscolar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AperturaEscolar aperturaEscolarNew = 
      (AperturaEscolar)BeanUtils.cloneBean(
      aperturaEscolar);

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (aperturaEscolarNew.getDependencia() != null) {
      aperturaEscolarNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        aperturaEscolarNew.getDependencia().getIdDependencia()));
    }
    pm.makePersistent(aperturaEscolarNew);
  }

  public void updateAperturaEscolar(AperturaEscolar aperturaEscolar) throws Exception
  {
    AperturaEscolar aperturaEscolarModify = 
      findAperturaEscolarById(aperturaEscolar.getIdAperturaEscolar());

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (aperturaEscolar.getDependencia() != null) {
      aperturaEscolar.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        aperturaEscolar.getDependencia().getIdDependencia()));
    }

    BeanUtils.copyProperties(aperturaEscolarModify, aperturaEscolar);
  }

  public void deleteAperturaEscolar(AperturaEscolar aperturaEscolar) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AperturaEscolar aperturaEscolarDelete = 
      findAperturaEscolarById(aperturaEscolar.getIdAperturaEscolar());
    pm.deletePersistent(aperturaEscolarDelete);
  }

  public AperturaEscolar findAperturaEscolarById(long idAperturaEscolar) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAperturaEscolar == pIdAperturaEscolar";
    Query query = pm.newQuery(AperturaEscolar.class, filter);

    query.declareParameters("long pIdAperturaEscolar");

    parameters.put("pIdAperturaEscolar", new Long(idAperturaEscolar));

    Collection colAperturaEscolar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAperturaEscolar.iterator();
    return (AperturaEscolar)iterator.next();
  }

  public Collection findAperturaEscolarAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent aperturaEscolarExtent = pm.getExtent(
      AperturaEscolar.class, true);
    Query query = pm.newQuery(aperturaEscolarExtent);
    query.setOrdering("codCargo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByDependencia(long idDependencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "dependencia.idDependencia == pIdDependencia";

    Query query = pm.newQuery(AperturaEscolar.class, filter);

    query.declareParameters("long pIdDependencia");
    HashMap parameters = new HashMap();

    parameters.put("pIdDependencia", new Long(idDependencia));

    query.setOrdering("codCargo ascending");

    Collection colAperturaEscolar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAperturaEscolar);

    return colAperturaEscolar;
  }

  public Collection findByCodCargo(String codCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codCargo == pCodCargo";

    Query query = pm.newQuery(AperturaEscolar.class, filter);

    query.declareParameters("java.lang.String pCodCargo");
    HashMap parameters = new HashMap();

    parameters.put("pCodCargo", new String(codCargo));

    query.setOrdering("codCargo ascending");

    Collection colAperturaEscolar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAperturaEscolar);

    return colAperturaEscolar;
  }
}