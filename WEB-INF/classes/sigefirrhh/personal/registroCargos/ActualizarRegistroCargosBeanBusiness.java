package sigefirrhh.personal.registroCargos;

import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenBusiness;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;

public class ActualizarRegistroCargosBeanBusiness
{
  Logger log = Logger.getLogger(ActualizarRegistroCargosBeanBusiness.class.getName());

  private TrabajadorNoGenBusiness trabajadorNoGenBusiness = new TrabajadorNoGenBusiness();
  private CargoNoGenBusiness cargoNoGenBusiness = new CargoNoGenBusiness();

  public boolean actualizar(String situacion, long idTrabajador, long idRegistroCargos)
    throws Exception
  {
    RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();
    RegistroCargos registroCargos = registroCargosBeanBusiness.findRegistroCargosById(idRegistroCargos);
    this.log.error("A2");
    if (situacion.equals("O"))
    {
      double sueldoBasico = 0.0D;
      double compensacion = 0.0D;
      double primasTrabajador = 0.0D;
      double primasCargo = 0.0D;

      sueldoBasico = sumarMonto(idTrabajador, 3, "sueldoBasico");
      sueldoBasico += sumarMonto(idTrabajador, 1, "sueldoBasico");
      sueldoBasico += sumarMonto(idTrabajador, 2, "sueldoBasico");

      compensacion = sumarMonto(idTrabajador, 3, "compensacion");
      compensacion += sumarMonto(idTrabajador, 1, "compensacion");
      compensacion += sumarMonto(idTrabajador, 2, "compensacion");

      primasTrabajador = sumarMonto(idTrabajador, 3, "primasTrabajador");
      primasTrabajador += sumarMonto(idTrabajador, 1, "primasTrabajador");
      primasTrabajador += sumarMonto(idTrabajador, 2, "primasTrabajador");

      primasCargo = sumarMonto(idTrabajador, 3, "primasCargo");
      primasCargo += sumarMonto(idTrabajador, 1, "primasCargo");
      primasCargo += sumarMonto(idTrabajador, 2, "primasCargo");

      registroCargos.setSueldoBasico(sueldoBasico);
      registroCargos.setCompensacion(compensacion);
      registroCargos.setPrimasTrabajador(primasTrabajador);
      registroCargos.setPrimasCargo(primasCargo);
    } else {
      this.log.error("A3");
      DetalleTabulador detalleTabulador = this.cargoNoGenBusiness.findDetalleTabuladorForRegistroCargos(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado(), 1);
      registroCargos.setSueldoBasico(detalleTabulador.getMonto());
    }

    return true;
  }

  private double sumarMonto(long idTrabajador, int codFrecuenciaPago, String metodo) throws Exception
  {
    ConceptoFijo conceptoFijo = new ConceptoFijo();
    double monto = 0.0D;

    Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoForRegistroCargos(idTrabajador, 3, metodo);
    Iterator iteratorConceptoFijo = colConceptoFijo.iterator();
    while (iteratorConceptoFijo.hasNext()) {
      conceptoFijo = new ConceptoFijo();
      conceptoFijo = (ConceptoFijo)iteratorConceptoFijo.next();
      monto += conceptoFijo.getMonto();
    }
    if (codFrecuenciaPago == 3) {
      monto /= 2.0D;
    }

    return monto;
  }
}