package sigefirrhh.personal.registroCargos;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class RegistroCargosNoGenFacade extends RegistroCargosFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private RegistroCargosNoGenBusiness registroCargosNoGenBusiness = new RegistroCargosNoGenBusiness();

  public Collection findRegistroCargosByCodigoNominaAndRegistro(int codigoNomina, long idRegistro) throws Exception
  {
    try {
      this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByCodigoNominaAndRegistro(codigoNomina, idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByCedulaAndRegistro(int Cedula, long idRegistro)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByCedulaAndRegistro(Cedula, idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosForAscenso(long idRegistro, String situacion, int grado)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosForAscenso(idRegistro, situacion, grado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean actualizarRegistroCargos(String situacion, long idTrabajador, long idRegistroCargos) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.actualizarRegistroCargos(situacion, idTrabajador, idRegistroCargos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacion(long idRegistro, String situacion) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacion(idRegistro, situacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndGrado99(long idRegistro, String situacion, String grado99) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndGrado99(idRegistro, situacion, grado99);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndGrado99AndTipo0(long idRegistro, String situacion, String grado99) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndGrado99AndTipo0(idRegistro, situacion, grado99);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo0(long idRegistro, String situacion) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndTipo0(idRegistro, situacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo123(long idRegistro, String situacion) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndTipo123(idRegistro, situacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo3(long idRegistro, String situacion) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndTipo3(idRegistro, situacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo123(long idRegistro, String situacion, int grado) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndTipo123(idRegistro, situacion, grado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo12(long idRegistro, String situacion) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndTipo12(idRegistro, situacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo23(long idRegistro, String situacion, int grado) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndTipo12(idRegistro, situacion, grado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndTipo3(long idRegistro) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndTipo3(idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndTipo12(long idRegistro) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndTipo12(idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroAndSituacionAndTipo123MismoGrado(long idRegistro, String situacion, int grado) throws Exception
  {
    try {
      this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroAndSituacionAndTipo123MismoGrado(idRegistro, situacion, grado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByIdRegistroNoGrado99(long idRegistro)
    throws Exception
  {
    return this.registroCargosNoGenBusiness.findRegistroCargosByIdRegistroNoGrado99(idRegistro);
  }

  public Collection findHistoricoCargosByRegistroAndCodigoNomina(long idRegistro, int codigoNomina) throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosNoGenBusiness.findHistoricoCargosByRegistroAndCodigoNomina(idRegistro, codigoNomina);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosByRegistroDependenciaAndSituacion(long idRegistro, long idDependencia, String situacion) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.findRegistroCargosByRegistroDependenciaAndSituacion(idRegistro, idDependencia, situacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosForConcurso(long idRegistro)
    throws Exception
  {
    return this.registroCargosNoGenBusiness.findRegistroCargosForConcurso(idRegistro);
  }

  public Collection findRegistroDocenteByCedulaAndDependenciaAndSituacion(int cedula, long idDependencia, String situacion) throws Exception {
    try {
      this.txn.open();

      return this.registroCargosNoGenBusiness.findRegistroDocenteByCedulaAndDependenciaAndSituacion(cedula, idDependencia, situacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroDocenteByPersonalAndEstatus(long idPersonal, String estatus) throws Exception {
    try { this.txn.open();

      return this.registroCargosNoGenBusiness.findRegistroDocenteByPersonalAndEstatus(idPersonal, estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public boolean actualizarNuevoRegistroCargos(Long idTipoPersonal, Long idRegistro, Long idRegistro2) throws Exception
  {
    try { this.txn.open();
      return this.registroCargosNoGenBusiness.actualizarNuevoRegistroCargos(idTipoPersonal, idRegistro, idRegistro2);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public boolean reemplazarDependencia(Long idRegistro, Long idRegion, Long idDependencia, Long idDependencia2, Long idManualCargo, Long idCargo, int gradoDesde, int gradoHasta, String situacion, String estatus)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.registroCargosNoGenBusiness.reemplazarDependencia(idRegistro, idRegion, idDependencia, idDependencia2, 
        idManualCargo, idCargo, gradoDesde, gradoHasta, 
        situacion, estatus);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}