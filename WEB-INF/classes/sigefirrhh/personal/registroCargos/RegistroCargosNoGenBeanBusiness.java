package sigefirrhh.personal.registroCargos;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class RegistroCargosNoGenBeanBusiness extends RegistroCargosBeanBusiness
{
  Logger log = Logger.getLogger(RegistroCargosNoGenBeanBusiness.class.getName());

  public Collection findByCodigoNominaAndRegistro(int codigoNomina, long idRegistro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigoNomina == pCodigoNomina && registro.idRegistro == pIdRegistro";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("int pCodigoNomina, long pIdRegistro");
    HashMap parameters = new HashMap();

    parameters.put("pCodigoNomina", new Integer(codigoNomina));
    parameters.put("pIdRegistro", new Long(idRegistro));

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findRegistroCargosByCedulaAndRegistro(int Cedula, long idRegistro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "Cedula == pCedula && registro.idRegistro == pIdRegistro";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("int pCedula, long pIdRegistro");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(Cedula));
    parameters.put("pIdRegistro", new Long(idRegistro));

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findForAscenso(long idRegistro, String situacion, int grado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.grado > pGrado && cargo.grado < 99 ";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, int pGrado");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pGrado", new Integer(grado));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacion(long idRegistro, String situacion) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndGrado99(long idRegistro, String situacion, String grado99) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter;
    String filter;
    if (grado99.equals("S"))
      filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.grado == 99";
    else {
      filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.grado != 99";
    }
    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndGrado99AndTipo0(long idRegistro, String situacion, String grado99) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo = "0";
    String filter;
    String filter;
    if (grado99.equals("S"))
      filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.grado == 99 && cargo.tipoCargo == pTipo";
    else {
      filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.grado != 99 && cargo.tipoCargo == pTipo";
    }

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo", tipo);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndTipo0(long idRegistro, String situacion) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo = "0";
    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.tipoCargo == pTipo";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo", tipo);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndTipo123(long idRegistro, String situacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo = "0";
    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.tipoCargo != pTipo";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo", tipo);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndTipo123(long idRegistro, String situacion, int grado) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo = "0";
    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.tipoCargo != pTipo && (cargo.grado <= pGrado || cargo.grado ==99)";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo, int pGrado");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo", tipo);
    parameters.put("pGrado", new Integer(grado));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndTipo12(long idRegistro, String situacion) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo1 = "1";
    String tipo2 = "2";
    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && (cargo.tipoCargo == pTipo1 || cargo.tipoCargo == pTipo2)";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo1, String pTipo2");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo1", tipo1);
    parameters.put("pTipo2", tipo2);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndTipo12(long idRegistro, String situacion, int grado) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo1 = "1";
    String tipo2 = "2";
    this.log.error("grado" + grado);
    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && (cargo.tipoCargo == pTipo1 || cargo.tipoCargo == pTipo2) && cargo.grado > pGrado";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo1, String pTipo2, int pGrado");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo1", tipo1);
    parameters.put("pTipo2", tipo2);
    parameters.put("pGrado", new Integer(grado));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndTipo3(long idRegistro, String situacion) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo1 = "3";
    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && cargo.tipoCargo == pTipo1";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo1");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo1", tipo1);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndSituacionAndTipo123MismoGrado(long idRegistro, String situacion, int grado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo1 = "1";
    String tipo2 = "2";
    String tipo3 = "3";
    String filter = "registro.idRegistro == pIdRegistro && situacion == pSituacion && (((cargo.tipoCargo == pTipo1 || cargo.tipoCargo == pTipo2) && cargo.grado == pGrado) || (cargo.tipoCargo == pTipo3))";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pSituacion, String pTipo1, String pTipo2, String pTipo3, int pGrado ");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pSituacion", situacion);
    parameters.put("pTipo1", tipo1);
    parameters.put("pTipo2", tipo2);
    parameters.put("pTipo3", tipo3);
    parameters.put("pGrado", new Integer(grado));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndTipo3(long idRegistro) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo = "3";

    String filter = "registro.idRegistro == pIdRegistro && cargo.tipoCargo == pTipo ";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pTipo");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pTipo", tipo);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByRegistroAndTipo12(long idRegistro) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String tipo1 = "1";
    String tipo2 = "2";

    String filter = "registro.idRegistro == pIdRegistro && (cargo.tipoCargo == pTipo1 || cargo.tipoCargo == pTipo2) ";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, String pTipo1, String pTipo2");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pTipo1", tipo1);
    parameters.put("pTipo2", tipo2);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByIdRegistroNoGrado99(long idRegistro)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;

    StringBuffer sql = new StringBuffer();
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select rc.id_registro_cargos, (rc.codigo_nomina || ' - ' || c.descripcion_cargo || ' - ' || rc.situacion )  as descripcion  ");
      sql.append(" from registrocargos rc, cargo c");
      sql.append(" where rc.id_cargo = c.id_cargo");
      sql.append(" and rc.id_registro = ?");
      sql.append(" and c.grado <> 99");
      sql.append(" order by rc.codigo_nomina");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idRegistro);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_registro_cargos"))));
        col.add(rsRegistros.getString("descripcion"));
      }
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }

  public Collection findForConcurso(long idRegistro)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;

    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select rc.id_registro_cargos, (rc.codigo_nomina || ' - ' || c.descripcion_cargo || ' - ' || rc.situacion )  as descripcion  ");
      sql.append(" from registrocargos rc, cargo c");
      sql.append(" where rc.id_cargo = c.id_cargo");
      sql.append(" and rc.id_registro = ?");
      sql.append(" and c.grado <> 99 and rc.situacion = 'V' ");
      sql.append(" and id_registro_cargos not in (");
      sql.append(" select id_registro_cargos from concursocargo");
      sql.append(" where estatus <> '3')");
      sql.append(" order by rc.codigo_nomina");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idRegistro);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id_registro_cargos"))));
        col.add(rsRegistros.getString("descripcion"));
      }
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }

  public Collection findByRegistroDependenciaAndSituacion(long idRegistro, long idDependencia, String situacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro && dependencia.idDependencia == pIdDependencia && situacion == pSituacion";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro, long pIdDependencia, String pSituacion");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pIdDependencia", new Long(idDependencia));
    parameters.put("pSituacion", situacion);

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }
}