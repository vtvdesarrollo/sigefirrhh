package sigefirrhh.personal.registroCargos;

import java.io.Serializable;

public class HistoricoCargosPK
  implements Serializable
{
  public long idHistoricoCargos;

  public HistoricoCargosPK()
  {
  }

  public HistoricoCargosPK(long idHistoricoCargos)
  {
    this.idHistoricoCargos = idHistoricoCargos;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoCargosPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoCargosPK thatPK)
  {
    return 
      this.idHistoricoCargos == thatPK.idHistoricoCargos;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoCargos)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoCargos);
  }
}