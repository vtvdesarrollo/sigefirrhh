package sigefirrhh.personal.registroCargos;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class AperturaEscolarForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AperturaEscolarForm.class.getName());
  private AperturaEscolar aperturaEscolar;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraNoGenFacade estructuraFacade = new EstructuraNoGenFacade();
  private RegistroCargosFacade registroCargosFacade = new RegistroCargosFacade();
  private boolean showAperturaEscolarByDependencia;
  private boolean showAperturaEscolarByCodCargo;
  private String findSelectRegionForDependencia;
  private String findSelectDependencia;
  private String findCodCargo;
  private Collection findColDependencia;
  private Collection colRegionForDependencia;
  private Collection colDependencia;
  private String selectRegionForDependencia;
  private String selectDependencia;
  private Dependencia dependencia;
  private long findIdRegion;
  private String findCodDependencia;
  private Collection findColRegion;
  private Object stateResultAperturaEscolarByDependencia = null;

  private Object stateResultAperturaEscolarByCodCargo = null;

  public boolean isShowFindDependencia()
  {
    return this.dependencia != null;
  }

  public Collection getFindColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }
  public String getFindSelectRegionForDependencia() {
    return this.findSelectRegionForDependencia;
  }
  public void setFindSelectRegionForDependencia(String valRegionForDependencia) {
    this.findSelectRegionForDependencia = valRegionForDependencia;
  }
  public String findDependenciaByCodigoAndRegion() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.dependencia = null;
      this.dependencia = this.estructuraFacade.findDependenciaByCodigoAndRegion(this.findCodDependencia, this.findIdRegion);
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La Dependencia no existe", ""));
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String getFindSelectDependencia()
  {
    return this.findSelectDependencia;
  }
  public void setFindSelectDependencia(String valDependencia) {
    this.findSelectDependencia = valDependencia;
  }

  public Collection getFindColDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }
  public boolean isFindShowDependencia() {
    return this.findColDependencia != null;
  }
  public String getFindCodCargo() {
    return this.findCodCargo;
  }
  public void setFindCodCargo(String findCodCargo) {
    this.findCodCargo = findCodCargo;
  }

  public String getSelectRegionForDependencia()
  {
    return this.selectRegionForDependencia;
  }
  public void setSelectRegionForDependencia(String valRegionForDependencia) {
    this.selectRegionForDependencia = valRegionForDependencia;
  }

  public boolean isShowRegionForDependencia()
  {
    return this.colRegionForDependencia != null;
  }
  public String getSelectDependencia() {
    return this.selectDependencia;
  }
  public void setSelectDependencia(String valDependencia) {
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    this.aperturaEscolar.setDependencia(null);
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      if (String.valueOf(dependencia.getIdDependencia()).equals(
        valDependencia)) {
        this.aperturaEscolar.setDependencia(
          dependencia);
        break;
      }
    }
    this.selectDependencia = valDependencia;
  }
  public boolean isShowDependencia() {
    return this.colDependencia != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public AperturaEscolar getAperturaEscolar() {
    if (this.aperturaEscolar == null) {
      this.aperturaEscolar = new AperturaEscolar();
    }
    return this.aperturaEscolar;
  }

  public AperturaEscolarForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegionForDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegionForDependencia.iterator();
    Region regionForDependencia = null;
    while (iterator.hasNext()) {
      regionForDependencia = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(regionForDependencia.getIdRegion()), 
        regionForDependencia.toString()));
    }
    return col;
  }

  public Collection getColDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findAperturaEscolarByDependencia()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroCargosFacade.findAperturaEscolarByDependencia(this.dependencia.getIdDependencia());
      this.showAperturaEscolarByDependencia = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAperturaEscolarByDependencia)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegionForDependencia = null;
    this.findSelectDependencia = null;
    this.findCodCargo = null;

    return null;
  }

  public String findAperturaEscolarByCodCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.registroCargosFacade.findAperturaEscolarByCodCargo(this.findCodCargo);
      this.showAperturaEscolarByCodCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAperturaEscolarByCodCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRegionForDependencia = null;
    this.findSelectDependencia = null;
    this.findCodCargo = null;

    return null;
  }

  public boolean isShowAperturaEscolarByDependencia() {
    return this.showAperturaEscolarByDependencia;
  }
  public boolean isShowAperturaEscolarByCodCargo() {
    return this.showAperturaEscolarByCodCargo;
  }

  public String selectAperturaEscolar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectDependencia = null;
    this.selectRegionForDependencia = null;

    long idAperturaEscolar = 
      Long.parseLong((String)requestParameterMap.get("idAperturaEscolar"));
    try
    {
      this.aperturaEscolar = 
        this.registroCargosFacade.findAperturaEscolarById(
        idAperturaEscolar);
      if (this.aperturaEscolar.getDependencia() != null) {
        this.selectDependencia = 
          String.valueOf(this.aperturaEscolar.getDependencia().getIdDependencia());
      }

      Dependencia dependencia = null;
      Region regionForDependencia = null;

      this.dependencia = this.aperturaEscolar.getDependencia();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.aperturaEscolar = null;
    this.showAperturaEscolarByDependencia = false;
    this.showAperturaEscolarByCodCargo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.registroCargosFacade.addAperturaEscolar(
          this.aperturaEscolar);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.aperturaEscolar);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.registroCargosFacade.updateAperturaEscolar(
          this.aperturaEscolar);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.aperturaEscolar);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.registroCargosFacade.deleteAperturaEscolar(
        this.aperturaEscolar);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.aperturaEscolar);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.aperturaEscolar = new AperturaEscolar();

    this.selectDependencia = null;

    this.selectRegionForDependencia = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.aperturaEscolar.setIdAperturaEscolar(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.registroCargos.AperturaEscolar"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.aperturaEscolar = new AperturaEscolar();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public Dependencia getDependencia()
  {
    return this.dependencia;
  }

  public void setDependencia(Dependencia dependencia)
  {
    this.dependencia = dependencia;
  }

  public String getFindCodDependencia()
  {
    return this.findCodDependencia;
  }

  public void setFindCodDependencia(String findCodDependencia)
  {
    this.findCodDependencia = findCodDependencia;
  }

  public long getFindIdRegion()
  {
    return this.findIdRegion;
  }

  public void setFindIdRegion(long findIdRegion)
  {
    this.findIdRegion = findIdRegion;
  }
}