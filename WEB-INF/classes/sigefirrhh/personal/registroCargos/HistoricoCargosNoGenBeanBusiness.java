package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class HistoricoCargosNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByRegistroAndCodigoNomina(long idRegistro, int codigoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro && codigoNomina == pCodigoNomina";

    Query query = pm.newQuery(HistoricoCargos.class, filter);

    query.declareParameters("long pIdRegistro, int pCodigoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));
    parameters.put("pCodigoNomina", new Integer(codigoNomina));

    query.setOrdering("fechaMovimiento ascending");

    Collection colHistoricoCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoCargos);

    return colHistoricoCargos;
  }
}