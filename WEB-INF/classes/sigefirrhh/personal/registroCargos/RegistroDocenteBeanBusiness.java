package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class RegistroDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRegistroDocente(RegistroDocente registroDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RegistroDocente registroDocenteNew = 
      (RegistroDocente)BeanUtils.cloneBean(
      registroDocente);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (registroDocenteNew.getCargo() != null) {
      registroDocenteNew.setCargo(
        cargoBeanBusiness.findCargoById(
        registroDocenteNew.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (registroDocenteNew.getDependencia() != null) {
      registroDocenteNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        registroDocenteNew.getDependencia().getIdDependencia()));
    }

    AperturaEscolarBeanBusiness aperturaEscolarBeanBusiness = new AperturaEscolarBeanBusiness();

    if (registroDocenteNew.getAperturaEscolar() != null) {
      registroDocenteNew.setAperturaEscolar(
        aperturaEscolarBeanBusiness.findAperturaEscolarById(
        registroDocenteNew.getAperturaEscolar().getIdAperturaEscolar()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (registroDocenteNew.getTrabajador() != null) {
      registroDocenteNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        registroDocenteNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(registroDocenteNew);
  }

  public void updateRegistroDocente(RegistroDocente registroDocente) throws Exception
  {
    RegistroDocente registroDocenteModify = 
      findRegistroDocenteById(registroDocente.getIdRegistroDocente());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (registroDocente.getCargo() != null) {
      registroDocente.setCargo(
        cargoBeanBusiness.findCargoById(
        registroDocente.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (registroDocente.getDependencia() != null) {
      registroDocente.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        registroDocente.getDependencia().getIdDependencia()));
    }

    AperturaEscolarBeanBusiness aperturaEscolarBeanBusiness = new AperturaEscolarBeanBusiness();

    if (registroDocente.getAperturaEscolar() != null) {
      registroDocente.setAperturaEscolar(
        aperturaEscolarBeanBusiness.findAperturaEscolarById(
        registroDocente.getAperturaEscolar().getIdAperturaEscolar()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (registroDocente.getTrabajador() != null) {
      registroDocente.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        registroDocente.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(registroDocenteModify, registroDocente);
  }

  public void deleteRegistroDocente(RegistroDocente registroDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RegistroDocente registroDocenteDelete = 
      findRegistroDocenteById(registroDocente.getIdRegistroDocente());
    pm.deletePersistent(registroDocenteDelete);
  }

  public RegistroDocente findRegistroDocenteById(long idRegistroDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegistroDocente == pIdRegistroDocente";
    Query query = pm.newQuery(RegistroDocente.class, filter);

    query.declareParameters("long pIdRegistroDocente");

    parameters.put("pIdRegistroDocente", new Long(idRegistroDocente));

    Collection colRegistroDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegistroDocente.iterator();
    return (RegistroDocente)iterator.next();
  }

  public Collection findRegistroDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent registroDocenteExtent = pm.getExtent(
      RegistroDocente.class, true);
    Query query = pm.newQuery(registroDocenteExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}