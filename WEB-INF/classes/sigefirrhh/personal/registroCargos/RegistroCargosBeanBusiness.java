package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.estructura.SedeBeanBusiness;
import sigefirrhh.base.personal.Gremio;
import sigefirrhh.base.personal.GremioBeanBusiness;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class RegistroCargosBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRegistroCargos(RegistroCargos registroCargos)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RegistroCargos registroCargosNew = 
      (RegistroCargos)BeanUtils.cloneBean(
      registroCargos);

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (registroCargosNew.getRegistro() != null) {
      registroCargosNew.setRegistro(
        registroBeanBusiness.findRegistroById(
        registroCargosNew.getRegistro().getIdRegistro()));
    }

    GremioBeanBusiness gremioBeanBusiness = new GremioBeanBusiness();

    if (registroCargosNew.getGremio() != null) {
      registroCargosNew.setGremio(
        gremioBeanBusiness.findGremioById(
        registroCargosNew.getGremio().getIdGremio()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (registroCargosNew.getCargo() != null) {
      registroCargosNew.setCargo(
        cargoBeanBusiness.findCargoById(
        registroCargosNew.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (registroCargosNew.getDependencia() != null) {
      registroCargosNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        registroCargosNew.getDependencia().getIdDependencia()));
    }

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (registroCargosNew.getSede() != null) {
      registroCargosNew.setSede(
        sedeBeanBusiness.findSedeById(
        registroCargosNew.getSede().getIdSede()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (registroCargosNew.getTrabajador() != null) {
      registroCargosNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        registroCargosNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(registroCargosNew);
  }

  public void updateRegistroCargos(RegistroCargos registroCargos) throws Exception
  {
    RegistroCargos registroCargosModify = 
      findRegistroCargosById(registroCargos.getIdRegistroCargos());

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (registroCargos.getRegistro() != null) {
      registroCargos.setRegistro(
        registroBeanBusiness.findRegistroById(
        registroCargos.getRegistro().getIdRegistro()));
    }

    GremioBeanBusiness gremioBeanBusiness = new GremioBeanBusiness();

    if (registroCargos.getGremio() != null) {
      registroCargos.setGremio(
        gremioBeanBusiness.findGremioById(
        registroCargos.getGremio().getIdGremio()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (registroCargos.getCargo() != null) {
      registroCargos.setCargo(
        cargoBeanBusiness.findCargoById(
        registroCargos.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (registroCargos.getDependencia() != null) {
      registroCargos.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        registroCargos.getDependencia().getIdDependencia()));
    }

    SedeBeanBusiness sedeBeanBusiness = new SedeBeanBusiness();

    if (registroCargos.getSede() != null) {
      registroCargos.setSede(
        sedeBeanBusiness.findSedeById(
        registroCargos.getSede().getIdSede()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (registroCargos.getTrabajador() != null) {
      registroCargos.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        registroCargos.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(registroCargosModify, registroCargos);
  }

  public void deleteRegistroCargos(RegistroCargos registroCargos) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RegistroCargos registroCargosDelete = 
      findRegistroCargosById(registroCargos.getIdRegistroCargos());
    pm.deletePersistent(registroCargosDelete);
  }

  public RegistroCargos findRegistroCargosById(long idRegistroCargos) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegistroCargos == pIdRegistroCargos";
    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistroCargos");

    parameters.put("pIdRegistroCargos", new Long(idRegistroCargos));

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegistroCargos.iterator();
    return (RegistroCargos)iterator.next();
  }

  public Collection findRegistroCargosAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent registroCargosExtent = pm.getExtent(
      RegistroCargos.class, true);
    Query query = pm.newQuery(registroCargosExtent);
    query.setOrdering("codigoNomina ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByRegistro(long idRegistro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("long pIdRegistro");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }

  public Collection findByCodigoNomina(int codigoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigoNomina == pCodigoNomina";

    Query query = pm.newQuery(RegistroCargos.class, filter);

    query.declareParameters("int pCodigoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pCodigoNomina", new Integer(codigoNomina));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargos);

    return colRegistroCargos;
  }
}