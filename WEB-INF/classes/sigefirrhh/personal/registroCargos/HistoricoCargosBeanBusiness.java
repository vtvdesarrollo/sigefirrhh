package sigefirrhh.personal.registroCargos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaMovimientoBeanBusiness;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroBeanBusiness;

public class HistoricoCargosBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHistoricoCargos(HistoricoCargos historicoCargos)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HistoricoCargos historicoCargosNew = 
      (HistoricoCargos)BeanUtils.cloneBean(
      historicoCargos);

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (historicoCargosNew.getRegistro() != null) {
      historicoCargosNew.setRegistro(
        registroBeanBusiness.findRegistroById(
        historicoCargosNew.getRegistro().getIdRegistro()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (historicoCargosNew.getCausaMovimiento() != null) {
      historicoCargosNew.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        historicoCargosNew.getCausaMovimiento().getIdCausaMovimiento()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (historicoCargosNew.getCargo() != null) {
      historicoCargosNew.setCargo(
        cargoBeanBusiness.findCargoById(
        historicoCargosNew.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (historicoCargosNew.getDependencia() != null) {
      historicoCargosNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        historicoCargosNew.getDependencia().getIdDependencia()));
    }
    pm.makePersistent(historicoCargosNew);
  }

  public void updateHistoricoCargos(HistoricoCargos historicoCargos) throws Exception
  {
    HistoricoCargos historicoCargosModify = 
      findHistoricoCargosById(historicoCargos.getIdHistoricoCargos());

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (historicoCargos.getRegistro() != null) {
      historicoCargos.setRegistro(
        registroBeanBusiness.findRegistroById(
        historicoCargos.getRegistro().getIdRegistro()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (historicoCargos.getCausaMovimiento() != null) {
      historicoCargos.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        historicoCargos.getCausaMovimiento().getIdCausaMovimiento()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (historicoCargos.getCargo() != null) {
      historicoCargos.setCargo(
        cargoBeanBusiness.findCargoById(
        historicoCargos.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (historicoCargos.getDependencia() != null) {
      historicoCargos.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        historicoCargos.getDependencia().getIdDependencia()));
    }

    BeanUtils.copyProperties(historicoCargosModify, historicoCargos);
  }

  public void deleteHistoricoCargos(HistoricoCargos historicoCargos) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistoricoCargos historicoCargosDelete = 
      findHistoricoCargosById(historicoCargos.getIdHistoricoCargos());
    pm.deletePersistent(historicoCargosDelete);
  }

  public HistoricoCargos findHistoricoCargosById(long idHistoricoCargos) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistoricoCargos == pIdHistoricoCargos";
    Query query = pm.newQuery(HistoricoCargos.class, filter);

    query.declareParameters("long pIdHistoricoCargos");

    parameters.put("pIdHistoricoCargos", new Long(idHistoricoCargos));

    Collection colHistoricoCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistoricoCargos.iterator();
    return (HistoricoCargos)iterator.next();
  }

  public Collection findHistoricoCargosAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historicoCargosExtent = pm.getExtent(
      HistoricoCargos.class, true);
    Query query = pm.newQuery(historicoCargosExtent);
    query.setOrdering("fechaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByRegistro(long idRegistro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro";

    Query query = pm.newQuery(HistoricoCargos.class, filter);

    query.declareParameters("long pIdRegistro");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));

    query.setOrdering("fechaMovimiento ascending");

    Collection colHistoricoCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoCargos);

    return colHistoricoCargos;
  }

  public Collection findByCodigoNomina(int codigoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigoNomina == pCodigoNomina";

    Query query = pm.newQuery(HistoricoCargos.class, filter);

    query.declareParameters("int pCodigoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pCodigoNomina", new Integer(codigoNomina));

    query.setOrdering("fechaMovimiento ascending");

    Collection colHistoricoCargos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoCargos);

    return colHistoricoCargos;
  }
}