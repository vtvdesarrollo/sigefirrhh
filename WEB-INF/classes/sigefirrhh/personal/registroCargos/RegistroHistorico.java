package sigefirrhh.personal.registroCargos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.personal.Gremio;
import sigefirrhh.personal.trabajador.Trabajador;

public class RegistroHistorico
  implements Serializable, PersistenceCapable
{
  private long idRegistroHistorico;
  private RegistroCargos registroCargos;
  private int anioRac;
  private int mesRac;
  private int codigoRac;
  private String situacion;
  private String estatus;
  private String condicion;
  private Date fechaCreacion;
  private String convenioGremial;
  private Gremio gremio;
  private Cargo cargo;
  private Dependencia dependencia;
  private Sede sede;
  private int horas;
  private int reportaRac;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anioRac", "cargo", "codigoRac", "condicion", "convenioGremial", "dependencia", "estatus", "fechaCreacion", "gremio", "horas", "idRegistroHistorico", "mesRac", "registroCargos", "reportaRac", "sede", "situacion", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.Gremio"), Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroCargos"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Sede"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 21, 26, 21, 21, 26, 21, 24, 21, 26, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnioRac()
  {
    return jdoGetanioRac(this);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public int getCodigoRac()
  {
    return jdoGetcodigoRac(this);
  }

  public String getCondicion()
  {
    return jdoGetcondicion(this);
  }

  public String getConvenioGremial()
  {
    return jdoGetconvenioGremial(this);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaCreacion()
  {
    return jdoGetfechaCreacion(this);
  }

  public Gremio getGremio()
  {
    return jdoGetgremio(this);
  }

  public int getHoras()
  {
    return jdoGethoras(this);
  }

  public long getIdRegistroHistorico()
  {
    return jdoGetidRegistroHistorico(this);
  }

  public int getMesRac()
  {
    return jdoGetmesRac(this);
  }

  public RegistroCargos getRegistroCargos()
  {
    return jdoGetregistroCargos(this);
  }

  public int getReportaRac()
  {
    return jdoGetreportaRac(this);
  }

  public Sede getSede()
  {
    return jdoGetsede(this);
  }

  public String getSituacion()
  {
    return jdoGetsituacion(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAnioRac(int i)
  {
    jdoSetanioRac(this, i);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public void setCodigoRac(int i)
  {
    jdoSetcodigoRac(this, i);
  }

  public void setCondicion(String string)
  {
    jdoSetcondicion(this, string);
  }

  public void setConvenioGremial(String string)
  {
    jdoSetconvenioGremial(this, string);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaCreacion(Date date)
  {
    jdoSetfechaCreacion(this, date);
  }

  public void setGremio(Gremio gremio)
  {
    jdoSetgremio(this, gremio);
  }

  public void setHoras(int i)
  {
    jdoSethoras(this, i);
  }

  public void setIdRegistroHistorico(long l)
  {
    jdoSetidRegistroHistorico(this, l);
  }

  public void setMesRac(int i)
  {
    jdoSetmesRac(this, i);
  }

  public void setRegistroCargos(RegistroCargos cargos)
  {
    jdoSetregistroCargos(this, cargos);
  }

  public void setReportaRac(int i)
  {
    jdoSetreportaRac(this, i);
  }

  public void setSede(Sede sede)
  {
    jdoSetsede(this, sede);
  }

  public void setSituacion(String string)
  {
    jdoSetsituacion(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 17;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroHistorico"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RegistroHistorico());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RegistroHistorico localRegistroHistorico = new RegistroHistorico();
    localRegistroHistorico.jdoFlags = 1;
    localRegistroHistorico.jdoStateManager = paramStateManager;
    return localRegistroHistorico;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RegistroHistorico localRegistroHistorico = new RegistroHistorico();
    localRegistroHistorico.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegistroHistorico.jdoFlags = 1;
    localRegistroHistorico.jdoStateManager = paramStateManager;
    return localRegistroHistorico;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioRac);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoRac);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.condicion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.convenioGremial);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCreacion);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.gremio);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.horas);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegistroHistorico);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesRac);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registroCargos);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.reportaRac);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sede);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioRac = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoRac = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.condicion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.convenioGremial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCreacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gremio = ((Gremio)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegistroHistorico = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesRac = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroCargos = ((RegistroCargos)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reportaRac = localStateManager.replacingIntField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sede = ((Sede)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RegistroHistorico paramRegistroHistorico, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.anioRac = paramRegistroHistorico.anioRac;
      return;
    case 1:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramRegistroHistorico.cargo;
      return;
    case 2:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.codigoRac = paramRegistroHistorico.codigoRac;
      return;
    case 3:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.condicion = paramRegistroHistorico.condicion;
      return;
    case 4:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.convenioGremial = paramRegistroHistorico.convenioGremial;
      return;
    case 5:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramRegistroHistorico.dependencia;
      return;
    case 6:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramRegistroHistorico.estatus;
      return;
    case 7:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCreacion = paramRegistroHistorico.fechaCreacion;
      return;
    case 8:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.gremio = paramRegistroHistorico.gremio;
      return;
    case 9:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramRegistroHistorico.horas;
      return;
    case 10:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.idRegistroHistorico = paramRegistroHistorico.idRegistroHistorico;
      return;
    case 11:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.mesRac = paramRegistroHistorico.mesRac;
      return;
    case 12:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.registroCargos = paramRegistroHistorico.registroCargos;
      return;
    case 13:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.reportaRac = paramRegistroHistorico.reportaRac;
      return;
    case 14:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.sede = paramRegistroHistorico.sede;
      return;
    case 15:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramRegistroHistorico.situacion;
      return;
    case 16:
      if (paramRegistroHistorico == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramRegistroHistorico.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RegistroHistorico))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RegistroHistorico localRegistroHistorico = (RegistroHistorico)paramObject;
    if (localRegistroHistorico.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegistroHistorico, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegistroHistoricoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegistroHistoricoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroHistoricoPK))
      throw new IllegalArgumentException("arg1");
    RegistroHistoricoPK localRegistroHistoricoPK = (RegistroHistoricoPK)paramObject;
    localRegistroHistoricoPK.idRegistroHistorico = this.idRegistroHistorico;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroHistoricoPK))
      throw new IllegalArgumentException("arg1");
    RegistroHistoricoPK localRegistroHistoricoPK = (RegistroHistoricoPK)paramObject;
    this.idRegistroHistorico = localRegistroHistoricoPK.idRegistroHistorico;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroHistoricoPK))
      throw new IllegalArgumentException("arg2");
    RegistroHistoricoPK localRegistroHistoricoPK = (RegistroHistoricoPK)paramObject;
    localRegistroHistoricoPK.idRegistroHistorico = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroHistoricoPK))
      throw new IllegalArgumentException("arg2");
    RegistroHistoricoPK localRegistroHistoricoPK = (RegistroHistoricoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localRegistroHistoricoPK.idRegistroHistorico);
  }

  private static final int jdoGetanioRac(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.anioRac;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.anioRac;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 0))
      return paramRegistroHistorico.anioRac;
    return localStateManager.getIntField(paramRegistroHistorico, jdoInheritedFieldCount + 0, paramRegistroHistorico.anioRac);
  }

  private static final void jdoSetanioRac(RegistroHistorico paramRegistroHistorico, int paramInt)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.anioRac = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.anioRac = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroHistorico, jdoInheritedFieldCount + 0, paramRegistroHistorico.anioRac, paramInt);
  }

  private static final Cargo jdoGetcargo(RegistroHistorico paramRegistroHistorico)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.cargo;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 1))
      return paramRegistroHistorico.cargo;
    return (Cargo)localStateManager.getObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 1, paramRegistroHistorico.cargo);
  }

  private static final void jdoSetcargo(RegistroHistorico paramRegistroHistorico, Cargo paramCargo)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 1, paramRegistroHistorico.cargo, paramCargo);
  }

  private static final int jdoGetcodigoRac(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.codigoRac;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.codigoRac;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 2))
      return paramRegistroHistorico.codigoRac;
    return localStateManager.getIntField(paramRegistroHistorico, jdoInheritedFieldCount + 2, paramRegistroHistorico.codigoRac);
  }

  private static final void jdoSetcodigoRac(RegistroHistorico paramRegistroHistorico, int paramInt)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.codigoRac = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.codigoRac = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroHistorico, jdoInheritedFieldCount + 2, paramRegistroHistorico.codigoRac, paramInt);
  }

  private static final String jdoGetcondicion(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.condicion;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.condicion;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 3))
      return paramRegistroHistorico.condicion;
    return localStateManager.getStringField(paramRegistroHistorico, jdoInheritedFieldCount + 3, paramRegistroHistorico.condicion);
  }

  private static final void jdoSetcondicion(RegistroHistorico paramRegistroHistorico, String paramString)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.condicion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.condicion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroHistorico, jdoInheritedFieldCount + 3, paramRegistroHistorico.condicion, paramString);
  }

  private static final String jdoGetconvenioGremial(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.convenioGremial;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.convenioGremial;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 4))
      return paramRegistroHistorico.convenioGremial;
    return localStateManager.getStringField(paramRegistroHistorico, jdoInheritedFieldCount + 4, paramRegistroHistorico.convenioGremial);
  }

  private static final void jdoSetconvenioGremial(RegistroHistorico paramRegistroHistorico, String paramString)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.convenioGremial = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.convenioGremial = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroHistorico, jdoInheritedFieldCount + 4, paramRegistroHistorico.convenioGremial, paramString);
  }

  private static final Dependencia jdoGetdependencia(RegistroHistorico paramRegistroHistorico)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.dependencia;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 5))
      return paramRegistroHistorico.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 5, paramRegistroHistorico.dependencia);
  }

  private static final void jdoSetdependencia(RegistroHistorico paramRegistroHistorico, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 5, paramRegistroHistorico.dependencia, paramDependencia);
  }

  private static final String jdoGetestatus(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.estatus;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.estatus;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 6))
      return paramRegistroHistorico.estatus;
    return localStateManager.getStringField(paramRegistroHistorico, jdoInheritedFieldCount + 6, paramRegistroHistorico.estatus);
  }

  private static final void jdoSetestatus(RegistroHistorico paramRegistroHistorico, String paramString)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroHistorico, jdoInheritedFieldCount + 6, paramRegistroHistorico.estatus, paramString);
  }

  private static final Date jdoGetfechaCreacion(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.fechaCreacion;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.fechaCreacion;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 7))
      return paramRegistroHistorico.fechaCreacion;
    return (Date)localStateManager.getObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 7, paramRegistroHistorico.fechaCreacion);
  }

  private static final void jdoSetfechaCreacion(RegistroHistorico paramRegistroHistorico, Date paramDate)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.fechaCreacion = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.fechaCreacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 7, paramRegistroHistorico.fechaCreacion, paramDate);
  }

  private static final Gremio jdoGetgremio(RegistroHistorico paramRegistroHistorico)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.gremio;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 8))
      return paramRegistroHistorico.gremio;
    return (Gremio)localStateManager.getObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 8, paramRegistroHistorico.gremio);
  }

  private static final void jdoSetgremio(RegistroHistorico paramRegistroHistorico, Gremio paramGremio)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.gremio = paramGremio;
      return;
    }
    localStateManager.setObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 8, paramRegistroHistorico.gremio, paramGremio);
  }

  private static final int jdoGethoras(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.horas;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.horas;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 9))
      return paramRegistroHistorico.horas;
    return localStateManager.getIntField(paramRegistroHistorico, jdoInheritedFieldCount + 9, paramRegistroHistorico.horas);
  }

  private static final void jdoSethoras(RegistroHistorico paramRegistroHistorico, int paramInt)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.horas = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.horas = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroHistorico, jdoInheritedFieldCount + 9, paramRegistroHistorico.horas, paramInt);
  }

  private static final long jdoGetidRegistroHistorico(RegistroHistorico paramRegistroHistorico)
  {
    return paramRegistroHistorico.idRegistroHistorico;
  }

  private static final void jdoSetidRegistroHistorico(RegistroHistorico paramRegistroHistorico, long paramLong)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.idRegistroHistorico = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegistroHistorico, jdoInheritedFieldCount + 10, paramRegistroHistorico.idRegistroHistorico, paramLong);
  }

  private static final int jdoGetmesRac(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.mesRac;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.mesRac;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 11))
      return paramRegistroHistorico.mesRac;
    return localStateManager.getIntField(paramRegistroHistorico, jdoInheritedFieldCount + 11, paramRegistroHistorico.mesRac);
  }

  private static final void jdoSetmesRac(RegistroHistorico paramRegistroHistorico, int paramInt)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.mesRac = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.mesRac = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroHistorico, jdoInheritedFieldCount + 11, paramRegistroHistorico.mesRac, paramInt);
  }

  private static final RegistroCargos jdoGetregistroCargos(RegistroHistorico paramRegistroHistorico)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.registroCargos;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 12))
      return paramRegistroHistorico.registroCargos;
    return (RegistroCargos)localStateManager.getObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 12, paramRegistroHistorico.registroCargos);
  }

  private static final void jdoSetregistroCargos(RegistroHistorico paramRegistroHistorico, RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.registroCargos = paramRegistroCargos;
      return;
    }
    localStateManager.setObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 12, paramRegistroHistorico.registroCargos, paramRegistroCargos);
  }

  private static final int jdoGetreportaRac(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.reportaRac;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.reportaRac;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 13))
      return paramRegistroHistorico.reportaRac;
    return localStateManager.getIntField(paramRegistroHistorico, jdoInheritedFieldCount + 13, paramRegistroHistorico.reportaRac);
  }

  private static final void jdoSetreportaRac(RegistroHistorico paramRegistroHistorico, int paramInt)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.reportaRac = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.reportaRac = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroHistorico, jdoInheritedFieldCount + 13, paramRegistroHistorico.reportaRac, paramInt);
  }

  private static final Sede jdoGetsede(RegistroHistorico paramRegistroHistorico)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.sede;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 14))
      return paramRegistroHistorico.sede;
    return (Sede)localStateManager.getObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 14, paramRegistroHistorico.sede);
  }

  private static final void jdoSetsede(RegistroHistorico paramRegistroHistorico, Sede paramSede)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.sede = paramSede;
      return;
    }
    localStateManager.setObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 14, paramRegistroHistorico.sede, paramSede);
  }

  private static final String jdoGetsituacion(RegistroHistorico paramRegistroHistorico)
  {
    if (paramRegistroHistorico.jdoFlags <= 0)
      return paramRegistroHistorico.situacion;
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.situacion;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 15))
      return paramRegistroHistorico.situacion;
    return localStateManager.getStringField(paramRegistroHistorico, jdoInheritedFieldCount + 15, paramRegistroHistorico.situacion);
  }

  private static final void jdoSetsituacion(RegistroHistorico paramRegistroHistorico, String paramString)
  {
    if (paramRegistroHistorico.jdoFlags == 0)
    {
      paramRegistroHistorico.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroHistorico, jdoInheritedFieldCount + 15, paramRegistroHistorico.situacion, paramString);
  }

  private static final Trabajador jdoGettrabajador(RegistroHistorico paramRegistroHistorico)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroHistorico.trabajador;
    if (localStateManager.isLoaded(paramRegistroHistorico, jdoInheritedFieldCount + 16))
      return paramRegistroHistorico.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 16, paramRegistroHistorico.trabajador);
  }

  private static final void jdoSettrabajador(RegistroHistorico paramRegistroHistorico, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramRegistroHistorico.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroHistorico.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramRegistroHistorico, jdoInheritedFieldCount + 16, paramRegistroHistorico.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}