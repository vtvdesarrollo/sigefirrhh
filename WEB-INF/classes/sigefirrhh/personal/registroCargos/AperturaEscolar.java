package sigefirrhh.personal.registroCargos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Dependencia;

public class AperturaEscolar
  implements Serializable, PersistenceCapable
{
  private long idAperturaEscolar;
  private Dependencia dependencia;
  private String codCargo;
  private int cargosApertura;
  private int cargosRestante;
  private double horasApertura;
  private double horasRestante;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargosApertura", "cargosRestante", "codCargo", "dependencia", "horasApertura", "horasRestante", "idAperturaEscolar" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Double.TYPE, Double.TYPE, Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public AperturaEscolar()
  {
    jdoSetcargosApertura(this, 0);

    jdoSetcargosRestante(this, 0);

    jdoSethorasApertura(this, 0.0D);

    jdoSethorasRestante(this, 0.0D);
  }
  public String toString() {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String horasApertura = b.format(jdoGethorasApertura(this));
    String horasRestante = b.format(jdoGethorasRestante(this));

    return jdoGetdependencia(this) + " - " + jdoGetcodCargo(this) + " - " + horasApertura + " - " + horasRestante;
  }

  public int getCargosApertura() {
    return jdoGetcargosApertura(this);
  }

  public void setCargosApertura(int cargosApertura) {
    jdoSetcargosApertura(this, cargosApertura);
  }

  public int getCargosRestante() {
    return jdoGetcargosRestante(this);
  }

  public void setCargosRestante(int cargosRestante) {
    jdoSetcargosRestante(this, cargosRestante);
  }

  public String getCodCargo() {
    return jdoGetcodCargo(this);
  }

  public void setCodCargo(String codCargo) {
    jdoSetcodCargo(this, codCargo);
  }

  public Dependencia getDependencia() {
    return jdoGetdependencia(this);
  }

  public void setDependencia(Dependencia dependencia) {
    jdoSetdependencia(this, dependencia);
  }

  public double getHorasApertura() {
    return jdoGethorasApertura(this);
  }

  public void setHorasApertura(double horasApertura) {
    jdoSethorasApertura(this, horasApertura);
  }

  public double getHorasRestante() {
    return jdoGethorasRestante(this);
  }

  public void setHorasRestante(double horasRestante) {
    jdoSethorasRestante(this, horasRestante);
  }

  public long getIdAperturaEscolar() {
    return jdoGetidAperturaEscolar(this);
  }

  public void setIdAperturaEscolar(long idAperturaEscolar) {
    jdoSetidAperturaEscolar(this, idAperturaEscolar);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.registroCargos.AperturaEscolar"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AperturaEscolar());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AperturaEscolar localAperturaEscolar = new AperturaEscolar();
    localAperturaEscolar.jdoFlags = 1;
    localAperturaEscolar.jdoStateManager = paramStateManager;
    return localAperturaEscolar;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AperturaEscolar localAperturaEscolar = new AperturaEscolar();
    localAperturaEscolar.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAperturaEscolar.jdoFlags = 1;
    localAperturaEscolar.jdoStateManager = paramStateManager;
    return localAperturaEscolar;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cargosApertura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cargosRestante);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horasApertura);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horasRestante);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAperturaEscolar);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargosApertura = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargosRestante = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horasApertura = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horasRestante = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAperturaEscolar = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AperturaEscolar paramAperturaEscolar, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAperturaEscolar == null)
        throw new IllegalArgumentException("arg1");
      this.cargosApertura = paramAperturaEscolar.cargosApertura;
      return;
    case 1:
      if (paramAperturaEscolar == null)
        throw new IllegalArgumentException("arg1");
      this.cargosRestante = paramAperturaEscolar.cargosRestante;
      return;
    case 2:
      if (paramAperturaEscolar == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramAperturaEscolar.codCargo;
      return;
    case 3:
      if (paramAperturaEscolar == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramAperturaEscolar.dependencia;
      return;
    case 4:
      if (paramAperturaEscolar == null)
        throw new IllegalArgumentException("arg1");
      this.horasApertura = paramAperturaEscolar.horasApertura;
      return;
    case 5:
      if (paramAperturaEscolar == null)
        throw new IllegalArgumentException("arg1");
      this.horasRestante = paramAperturaEscolar.horasRestante;
      return;
    case 6:
      if (paramAperturaEscolar == null)
        throw new IllegalArgumentException("arg1");
      this.idAperturaEscolar = paramAperturaEscolar.idAperturaEscolar;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AperturaEscolar))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AperturaEscolar localAperturaEscolar = (AperturaEscolar)paramObject;
    if (localAperturaEscolar.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAperturaEscolar, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AperturaEscolarPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AperturaEscolarPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AperturaEscolarPK))
      throw new IllegalArgumentException("arg1");
    AperturaEscolarPK localAperturaEscolarPK = (AperturaEscolarPK)paramObject;
    localAperturaEscolarPK.idAperturaEscolar = this.idAperturaEscolar;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AperturaEscolarPK))
      throw new IllegalArgumentException("arg1");
    AperturaEscolarPK localAperturaEscolarPK = (AperturaEscolarPK)paramObject;
    this.idAperturaEscolar = localAperturaEscolarPK.idAperturaEscolar;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AperturaEscolarPK))
      throw new IllegalArgumentException("arg2");
    AperturaEscolarPK localAperturaEscolarPK = (AperturaEscolarPK)paramObject;
    localAperturaEscolarPK.idAperturaEscolar = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AperturaEscolarPK))
      throw new IllegalArgumentException("arg2");
    AperturaEscolarPK localAperturaEscolarPK = (AperturaEscolarPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localAperturaEscolarPK.idAperturaEscolar);
  }

  private static final int jdoGetcargosApertura(AperturaEscolar paramAperturaEscolar)
  {
    if (paramAperturaEscolar.jdoFlags <= 0)
      return paramAperturaEscolar.cargosApertura;
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
      return paramAperturaEscolar.cargosApertura;
    if (localStateManager.isLoaded(paramAperturaEscolar, jdoInheritedFieldCount + 0))
      return paramAperturaEscolar.cargosApertura;
    return localStateManager.getIntField(paramAperturaEscolar, jdoInheritedFieldCount + 0, paramAperturaEscolar.cargosApertura);
  }

  private static final void jdoSetcargosApertura(AperturaEscolar paramAperturaEscolar, int paramInt)
  {
    if (paramAperturaEscolar.jdoFlags == 0)
    {
      paramAperturaEscolar.cargosApertura = paramInt;
      return;
    }
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
    {
      paramAperturaEscolar.cargosApertura = paramInt;
      return;
    }
    localStateManager.setIntField(paramAperturaEscolar, jdoInheritedFieldCount + 0, paramAperturaEscolar.cargosApertura, paramInt);
  }

  private static final int jdoGetcargosRestante(AperturaEscolar paramAperturaEscolar)
  {
    if (paramAperturaEscolar.jdoFlags <= 0)
      return paramAperturaEscolar.cargosRestante;
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
      return paramAperturaEscolar.cargosRestante;
    if (localStateManager.isLoaded(paramAperturaEscolar, jdoInheritedFieldCount + 1))
      return paramAperturaEscolar.cargosRestante;
    return localStateManager.getIntField(paramAperturaEscolar, jdoInheritedFieldCount + 1, paramAperturaEscolar.cargosRestante);
  }

  private static final void jdoSetcargosRestante(AperturaEscolar paramAperturaEscolar, int paramInt)
  {
    if (paramAperturaEscolar.jdoFlags == 0)
    {
      paramAperturaEscolar.cargosRestante = paramInt;
      return;
    }
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
    {
      paramAperturaEscolar.cargosRestante = paramInt;
      return;
    }
    localStateManager.setIntField(paramAperturaEscolar, jdoInheritedFieldCount + 1, paramAperturaEscolar.cargosRestante, paramInt);
  }

  private static final String jdoGetcodCargo(AperturaEscolar paramAperturaEscolar)
  {
    if (paramAperturaEscolar.jdoFlags <= 0)
      return paramAperturaEscolar.codCargo;
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
      return paramAperturaEscolar.codCargo;
    if (localStateManager.isLoaded(paramAperturaEscolar, jdoInheritedFieldCount + 2))
      return paramAperturaEscolar.codCargo;
    return localStateManager.getStringField(paramAperturaEscolar, jdoInheritedFieldCount + 2, paramAperturaEscolar.codCargo);
  }

  private static final void jdoSetcodCargo(AperturaEscolar paramAperturaEscolar, String paramString)
  {
    if (paramAperturaEscolar.jdoFlags == 0)
    {
      paramAperturaEscolar.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
    {
      paramAperturaEscolar.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramAperturaEscolar, jdoInheritedFieldCount + 2, paramAperturaEscolar.codCargo, paramString);
  }

  private static final Dependencia jdoGetdependencia(AperturaEscolar paramAperturaEscolar)
  {
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
      return paramAperturaEscolar.dependencia;
    if (localStateManager.isLoaded(paramAperturaEscolar, jdoInheritedFieldCount + 3))
      return paramAperturaEscolar.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramAperturaEscolar, jdoInheritedFieldCount + 3, paramAperturaEscolar.dependencia);
  }

  private static final void jdoSetdependencia(AperturaEscolar paramAperturaEscolar, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
    {
      paramAperturaEscolar.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramAperturaEscolar, jdoInheritedFieldCount + 3, paramAperturaEscolar.dependencia, paramDependencia);
  }

  private static final double jdoGethorasApertura(AperturaEscolar paramAperturaEscolar)
  {
    if (paramAperturaEscolar.jdoFlags <= 0)
      return paramAperturaEscolar.horasApertura;
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
      return paramAperturaEscolar.horasApertura;
    if (localStateManager.isLoaded(paramAperturaEscolar, jdoInheritedFieldCount + 4))
      return paramAperturaEscolar.horasApertura;
    return localStateManager.getDoubleField(paramAperturaEscolar, jdoInheritedFieldCount + 4, paramAperturaEscolar.horasApertura);
  }

  private static final void jdoSethorasApertura(AperturaEscolar paramAperturaEscolar, double paramDouble)
  {
    if (paramAperturaEscolar.jdoFlags == 0)
    {
      paramAperturaEscolar.horasApertura = paramDouble;
      return;
    }
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
    {
      paramAperturaEscolar.horasApertura = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAperturaEscolar, jdoInheritedFieldCount + 4, paramAperturaEscolar.horasApertura, paramDouble);
  }

  private static final double jdoGethorasRestante(AperturaEscolar paramAperturaEscolar)
  {
    if (paramAperturaEscolar.jdoFlags <= 0)
      return paramAperturaEscolar.horasRestante;
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
      return paramAperturaEscolar.horasRestante;
    if (localStateManager.isLoaded(paramAperturaEscolar, jdoInheritedFieldCount + 5))
      return paramAperturaEscolar.horasRestante;
    return localStateManager.getDoubleField(paramAperturaEscolar, jdoInheritedFieldCount + 5, paramAperturaEscolar.horasRestante);
  }

  private static final void jdoSethorasRestante(AperturaEscolar paramAperturaEscolar, double paramDouble)
  {
    if (paramAperturaEscolar.jdoFlags == 0)
    {
      paramAperturaEscolar.horasRestante = paramDouble;
      return;
    }
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
    {
      paramAperturaEscolar.horasRestante = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAperturaEscolar, jdoInheritedFieldCount + 5, paramAperturaEscolar.horasRestante, paramDouble);
  }

  private static final long jdoGetidAperturaEscolar(AperturaEscolar paramAperturaEscolar)
  {
    return paramAperturaEscolar.idAperturaEscolar;
  }

  private static final void jdoSetidAperturaEscolar(AperturaEscolar paramAperturaEscolar, long paramLong)
  {
    StateManager localStateManager = paramAperturaEscolar.jdoStateManager;
    if (localStateManager == null)
    {
      paramAperturaEscolar.idAperturaEscolar = paramLong;
      return;
    }
    localStateManager.setLongField(paramAperturaEscolar, jdoInheritedFieldCount + 6, paramAperturaEscolar.idAperturaEscolar, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}