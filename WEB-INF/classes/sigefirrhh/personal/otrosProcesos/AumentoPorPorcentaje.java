package sigefirrhh.personal.otrosProcesos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class AumentoPorPorcentaje
  implements Serializable, PersistenceCapable
{
  private long idAumentoPorPorcentaje;
  private int gradoActual;
  private int pasoActual;
  private double sueldoActual;
  private double otrosSueldo;
  private double compensacionActual;
  private double porcentaje;
  private double sueldoNuevo;
  private double ajusteEscala;
  private double pasoNuevo;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "ajusteEscala", "compensacionActual", "gradoActual", "idAumentoPorPorcentaje", "otrosSueldo", "pasoActual", "pasoNuevo", "porcentaje", "sueldoActual", "sueldoNuevo", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, Integer.TYPE, Long.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getAjusteEscala()
  {
    return jdoGetajusteEscala(this);
  }

  public double getCompensacionActual()
  {
    return jdoGetcompensacionActual(this);
  }

  public int getGradoActual()
  {
    return jdoGetgradoActual(this);
  }

  public long getIdAumentoPorPorcentaje()
  {
    return jdoGetidAumentoPorPorcentaje(this);
  }

  public double getOtrosSueldo()
  {
    return jdoGetotrosSueldo(this);
  }

  public int getPasoActual()
  {
    return jdoGetpasoActual(this);
  }

  public double getPasoNuevo()
  {
    return jdoGetpasoNuevo(this);
  }

  public double getPorcentaje()
  {
    return jdoGetporcentaje(this);
  }

  public double getSueldoActual()
  {
    return jdoGetsueldoActual(this);
  }

  public double getSueldoNuevo()
  {
    return jdoGetsueldoNuevo(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAjusteEscala(double d)
  {
    jdoSetajusteEscala(this, d);
  }

  public void setCompensacionActual(double d)
  {
    jdoSetcompensacionActual(this, d);
  }

  public void setGradoActual(int i)
  {
    jdoSetgradoActual(this, i);
  }

  public void setIdAumentoPorPorcentaje(long l)
  {
    jdoSetidAumentoPorPorcentaje(this, l);
  }

  public void setOtrosSueldo(double d)
  {
    jdoSetotrosSueldo(this, d);
  }

  public void setPasoActual(int i)
  {
    jdoSetpasoActual(this, i);
  }

  public void setPasoNuevo(double d)
  {
    jdoSetpasoNuevo(this, d);
  }

  public void setPorcentaje(double d)
  {
    jdoSetporcentaje(this, d);
  }

  public void setSueldoActual(double d)
  {
    jdoSetsueldoActual(this, d);
  }

  public void setSueldoNuevo(double d)
  {
    jdoSetsueldoNuevo(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.otrosProcesos.AumentoPorPorcentaje"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AumentoPorPorcentaje());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AumentoPorPorcentaje localAumentoPorPorcentaje = new AumentoPorPorcentaje();
    localAumentoPorPorcentaje.jdoFlags = 1;
    localAumentoPorPorcentaje.jdoStateManager = paramStateManager;
    return localAumentoPorPorcentaje;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AumentoPorPorcentaje localAumentoPorPorcentaje = new AumentoPorPorcentaje();
    localAumentoPorPorcentaje.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAumentoPorPorcentaje.jdoFlags = 1;
    localAumentoPorPorcentaje.jdoStateManager = paramStateManager;
    return localAumentoPorPorcentaje;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ajusteEscala);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacionActual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.gradoActual);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAumentoPorPorcentaje);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.otrosSueldo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.pasoActual);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.pasoNuevo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoActual);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoNuevo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ajusteEscala = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacionActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gradoActual = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAumentoPorPorcentaje = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otrosSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pasoActual = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pasoNuevo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoNuevo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AumentoPorPorcentaje paramAumentoPorPorcentaje, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.ajusteEscala = paramAumentoPorPorcentaje.ajusteEscala;
      return;
    case 1:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.compensacionActual = paramAumentoPorPorcentaje.compensacionActual;
      return;
    case 2:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.gradoActual = paramAumentoPorPorcentaje.gradoActual;
      return;
    case 3:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.idAumentoPorPorcentaje = paramAumentoPorPorcentaje.idAumentoPorPorcentaje;
      return;
    case 4:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.otrosSueldo = paramAumentoPorPorcentaje.otrosSueldo;
      return;
    case 5:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.pasoActual = paramAumentoPorPorcentaje.pasoActual;
      return;
    case 6:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.pasoNuevo = paramAumentoPorPorcentaje.pasoNuevo;
      return;
    case 7:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramAumentoPorPorcentaje.porcentaje;
      return;
    case 8:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoActual = paramAumentoPorPorcentaje.sueldoActual;
      return;
    case 9:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoNuevo = paramAumentoPorPorcentaje.sueldoNuevo;
      return;
    case 10:
      if (paramAumentoPorPorcentaje == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramAumentoPorPorcentaje.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AumentoPorPorcentaje))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AumentoPorPorcentaje localAumentoPorPorcentaje = (AumentoPorPorcentaje)paramObject;
    if (localAumentoPorPorcentaje.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAumentoPorPorcentaje, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AumentoPorPorcentajePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AumentoPorPorcentajePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AumentoPorPorcentajePK))
      throw new IllegalArgumentException("arg1");
    AumentoPorPorcentajePK localAumentoPorPorcentajePK = (AumentoPorPorcentajePK)paramObject;
    localAumentoPorPorcentajePK.idAumentoPorPorcentaje = this.idAumentoPorPorcentaje;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AumentoPorPorcentajePK))
      throw new IllegalArgumentException("arg1");
    AumentoPorPorcentajePK localAumentoPorPorcentajePK = (AumentoPorPorcentajePK)paramObject;
    this.idAumentoPorPorcentaje = localAumentoPorPorcentajePK.idAumentoPorPorcentaje;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AumentoPorPorcentajePK))
      throw new IllegalArgumentException("arg2");
    AumentoPorPorcentajePK localAumentoPorPorcentajePK = (AumentoPorPorcentajePK)paramObject;
    localAumentoPorPorcentajePK.idAumentoPorPorcentaje = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AumentoPorPorcentajePK))
      throw new IllegalArgumentException("arg2");
    AumentoPorPorcentajePK localAumentoPorPorcentajePK = (AumentoPorPorcentajePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localAumentoPorPorcentajePK.idAumentoPorPorcentaje);
  }

  private static final double jdoGetajusteEscala(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.ajusteEscala;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.ajusteEscala;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 0))
      return paramAumentoPorPorcentaje.ajusteEscala;
    return localStateManager.getDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 0, paramAumentoPorPorcentaje.ajusteEscala);
  }

  private static final void jdoSetajusteEscala(AumentoPorPorcentaje paramAumentoPorPorcentaje, double paramDouble)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.ajusteEscala = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.ajusteEscala = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 0, paramAumentoPorPorcentaje.ajusteEscala, paramDouble);
  }

  private static final double jdoGetcompensacionActual(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.compensacionActual;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.compensacionActual;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 1))
      return paramAumentoPorPorcentaje.compensacionActual;
    return localStateManager.getDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 1, paramAumentoPorPorcentaje.compensacionActual);
  }

  private static final void jdoSetcompensacionActual(AumentoPorPorcentaje paramAumentoPorPorcentaje, double paramDouble)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.compensacionActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.compensacionActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 1, paramAumentoPorPorcentaje.compensacionActual, paramDouble);
  }

  private static final int jdoGetgradoActual(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.gradoActual;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.gradoActual;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 2))
      return paramAumentoPorPorcentaje.gradoActual;
    return localStateManager.getIntField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 2, paramAumentoPorPorcentaje.gradoActual);
  }

  private static final void jdoSetgradoActual(AumentoPorPorcentaje paramAumentoPorPorcentaje, int paramInt)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.gradoActual = paramInt;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.gradoActual = paramInt;
      return;
    }
    localStateManager.setIntField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 2, paramAumentoPorPorcentaje.gradoActual, paramInt);
  }

  private static final long jdoGetidAumentoPorPorcentaje(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    return paramAumentoPorPorcentaje.idAumentoPorPorcentaje;
  }

  private static final void jdoSetidAumentoPorPorcentaje(AumentoPorPorcentaje paramAumentoPorPorcentaje, long paramLong)
  {
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.idAumentoPorPorcentaje = paramLong;
      return;
    }
    localStateManager.setLongField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 3, paramAumentoPorPorcentaje.idAumentoPorPorcentaje, paramLong);
  }

  private static final double jdoGetotrosSueldo(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.otrosSueldo;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.otrosSueldo;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 4))
      return paramAumentoPorPorcentaje.otrosSueldo;
    return localStateManager.getDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 4, paramAumentoPorPorcentaje.otrosSueldo);
  }

  private static final void jdoSetotrosSueldo(AumentoPorPorcentaje paramAumentoPorPorcentaje, double paramDouble)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.otrosSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.otrosSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 4, paramAumentoPorPorcentaje.otrosSueldo, paramDouble);
  }

  private static final int jdoGetpasoActual(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.pasoActual;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.pasoActual;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 5))
      return paramAumentoPorPorcentaje.pasoActual;
    return localStateManager.getIntField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 5, paramAumentoPorPorcentaje.pasoActual);
  }

  private static final void jdoSetpasoActual(AumentoPorPorcentaje paramAumentoPorPorcentaje, int paramInt)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.pasoActual = paramInt;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.pasoActual = paramInt;
      return;
    }
    localStateManager.setIntField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 5, paramAumentoPorPorcentaje.pasoActual, paramInt);
  }

  private static final double jdoGetpasoNuevo(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.pasoNuevo;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.pasoNuevo;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 6))
      return paramAumentoPorPorcentaje.pasoNuevo;
    return localStateManager.getDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 6, paramAumentoPorPorcentaje.pasoNuevo);
  }

  private static final void jdoSetpasoNuevo(AumentoPorPorcentaje paramAumentoPorPorcentaje, double paramDouble)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.pasoNuevo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.pasoNuevo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 6, paramAumentoPorPorcentaje.pasoNuevo, paramDouble);
  }

  private static final double jdoGetporcentaje(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.porcentaje;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.porcentaje;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 7))
      return paramAumentoPorPorcentaje.porcentaje;
    return localStateManager.getDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 7, paramAumentoPorPorcentaje.porcentaje);
  }

  private static final void jdoSetporcentaje(AumentoPorPorcentaje paramAumentoPorPorcentaje, double paramDouble)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 7, paramAumentoPorPorcentaje.porcentaje, paramDouble);
  }

  private static final double jdoGetsueldoActual(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.sueldoActual;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.sueldoActual;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 8))
      return paramAumentoPorPorcentaje.sueldoActual;
    return localStateManager.getDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 8, paramAumentoPorPorcentaje.sueldoActual);
  }

  private static final void jdoSetsueldoActual(AumentoPorPorcentaje paramAumentoPorPorcentaje, double paramDouble)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.sueldoActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.sueldoActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 8, paramAumentoPorPorcentaje.sueldoActual, paramDouble);
  }

  private static final double jdoGetsueldoNuevo(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    if (paramAumentoPorPorcentaje.jdoFlags <= 0)
      return paramAumentoPorPorcentaje.sueldoNuevo;
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.sueldoNuevo;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 9))
      return paramAumentoPorPorcentaje.sueldoNuevo;
    return localStateManager.getDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 9, paramAumentoPorPorcentaje.sueldoNuevo);
  }

  private static final void jdoSetsueldoNuevo(AumentoPorPorcentaje paramAumentoPorPorcentaje, double paramDouble)
  {
    if (paramAumentoPorPorcentaje.jdoFlags == 0)
    {
      paramAumentoPorPorcentaje.sueldoNuevo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.sueldoNuevo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 9, paramAumentoPorPorcentaje.sueldoNuevo, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(AumentoPorPorcentaje paramAumentoPorPorcentaje)
  {
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorPorcentaje.trabajador;
    if (localStateManager.isLoaded(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 10))
      return paramAumentoPorPorcentaje.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 10, paramAumentoPorPorcentaje.trabajador);
  }

  private static final void jdoSettrabajador(AumentoPorPorcentaje paramAumentoPorPorcentaje, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramAumentoPorPorcentaje.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorPorcentaje.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramAumentoPorPorcentaje, jdoInheritedFieldCount + 10, paramAumentoPorPorcentaje.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}