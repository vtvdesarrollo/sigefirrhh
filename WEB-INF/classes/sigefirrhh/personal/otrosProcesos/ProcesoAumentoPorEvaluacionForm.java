package sigefirrhh.personal.otrosProcesos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ProcesoAumentoPorEvaluacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProcesoAumentoPorEvaluacionForm.class.getName());

  private String selectProceso = "1";
  private String selectTipoPersonal;
  private int mes;
  private int anio;
  private int mesHistorico;
  private int anioHistorico;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private DefinicionesFacadeExtend definicionesFacadeExtend;
  private OtrosProcesosNoGenFacade otrosProcesosFacade;
  private LoginSession login;
  private boolean show;
  private boolean showOrigen = false;
  private boolean show2;
  private boolean auxShow;
  private Collection listConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String grabar = "F";
  private String metodo = "P";
  private String registro = "S";
  private String origen = "F";
  private int reportId;
  private String reportName;
  private String formato = "1";
  private String retroactivo;
  private long idConceptoRetroactivo = 0L;
  private long idFrecuenciaRetroactivo = 0L;
  private int diasRetroactivo = 0;
  private String selectConceptoRetroactivo;
  private Collection listConceptoRetroactivo;
  private boolean showRetroactivo;

  public boolean isShowConcepto()
  {
    return (this.listConceptoTipoPersonal != null) && (this.selectProceso.equals("2")) && (this.registro.equals("O"));
  }
  public boolean isShowOrigen() {
    return this.origen.equals("H");
  }
  public boolean isShowConceptoRetroactivo() {
    return (this.showRetroactivo) && (this.selectProceso.equals("2"));
  }
  public boolean isShowRetroactivo() {
    return this.showRetroactivo;
  }
  public void changeRetroactivo(ValueChangeEvent event) {
    try {
      this.retroactivo = ((String)event.getNewValue());
      this.showRetroactivo = false;
      if (this.retroactivo.equals("S")) {
        this.showRetroactivo = true;
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeConceptoRetroactivo(ValueChangeEvent event)
  {
    this.idConceptoRetroactivo = Long.valueOf((String)event.getNewValue()).longValue();
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf((String)event.getNewValue()).longValue();
    try {
      this.auxShow = false;
      if (this.idTipoPersonal != 0L) {
        this.auxShow = true;
        this.listConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByTipoPersonalAsignaciones(this.idTipoPersonal);
        log.error("size ................." + this.listConceptoTipoPersonal.size());
        this.listConceptoRetroactivo = this.definicionesFacadeExtend.findConceptoTipoPersonalAsignacionesByTipoPersonalAndTipoPrestamo(this.idTipoPersonal, "R");
      }
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegistro(ValueChangeEvent event) {
    this.registro = ((String)event.getNewValue());
    try
    {
      this.auxShow = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public ProcesoAumentoPorEvaluacionForm() {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.definicionesFacadeExtend = new DefinicionesFacadeExtend();
    this.otrosProcesosFacade = new OtrosProcesosNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();

    this.reportName = "aumentoevaluacionsue";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event) {
        ProcesoAumentoPorEvaluacionForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    if (this.registro.equals("S"))
      this.reportName = "aumentoevaluacionsue";
    else if (this.registro.equals("C"))
      this.reportName = "aumentoevaluacioncom";
    else if (this.registro.equals("A"))
      this.reportName = "aumentoevaluacionesc";
    else {
      this.reportName = "aumentoevaluacionaum";
    }
    if (this.formato.equals("2"))
      this.reportName = ("a_" + this.reportName);
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));

    parameters.put("anio", new Integer(this.anio));
    parameters.put("mes", new Integer(this.mes));

    if (this.registro.equals("S"))
      this.reportName = "aumentoevaluacionsue";
    else if (this.registro.equals("C"))
      this.reportName = "aumentoevaluacioncom";
    else if (this.registro.equals("A"))
      this.reportName = "aumentoevaluacionesc";
    else {
      this.reportName = "aumentoevaluacionaum";
    }
    if (this.formato.equals("2")) {
      this.reportName = ("a_" + this.reportName);
    }
    JasperForWeb report = new JasperForWeb();
    if (this.formato.equals("2")) {
      report.setType(3);
    }

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/evaluacion");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String preGenerate() throws Exception { FacesContext context = FacesContext.getCurrentInstance();
    if (this.selectProceso.equals("2"))
      this.show2 = true;
    else {
      generate();
    }

    this.show = false;
    return null; }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if ((this.mes < 1) || (this.mes > 12)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes debe estar comprendido entre 1 y 12", ""));
        return null;
      }

      long idConcepto = 0L;
      if (this.selectConceptoTipoPersonal != null) {
        idConcepto = Long.valueOf(String.valueOf(this.selectConceptoTipoPersonal)).longValue();
      }
      if (this.idTipoPersonal != 0L) {
        TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

        this.otrosProcesosFacade.procesoAumentoPorEvaluacion(this.idTipoPersonal, this.anio, this.mes, 
          this.selectProceso, this.login.getUsuario(), tipoPersonal.getGrupoNomina().getPeriodicidad(), this.metodo, this.registro, this.grabar, new Date(), idConcepto, this.retroactivo, this.diasRetroactivo, this.idConceptoRetroactivo, this.idFrecuenciaRetroactivo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

        context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
        this.show = false;
        this.show2 = false;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String abort() {
    this.show = true;
    this.show2 = false;
    return null;
  }
  public Collection getListTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListConceptoTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoTipoPersonal, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListConceptoRetroactivo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoRetroactivo, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }

  public String getGrabar() {
    return this.grabar;
  }
  public void setGrabar(String grabar) {
    this.grabar = grabar;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getMetodo() {
    return this.metodo;
  }
  public void setMetodo(String metodo) {
    this.metodo = metodo;
  }
  public String getRegistro() {
    return this.registro;
  }
  public void setRegistro(String registro) {
    this.registro = registro;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String selectConceptoTipoPersonal) {
    this.selectConceptoTipoPersonal = selectConceptoTipoPersonal;
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
  public String getRetroactivo() {
    return this.retroactivo;
  }
  public void setRetroactivo(String retroactivo) {
    this.retroactivo = retroactivo;
  }
  public int getDiasRetroactivo() {
    return this.diasRetroactivo;
  }
  public void setDiasRetroactivo(int diasRetroactivo) {
    this.diasRetroactivo = diasRetroactivo;
  }
  public String getSelectConceptoRetroactivo() {
    return this.selectConceptoRetroactivo;
  }
  public void setSelectConceptoRetroactivo(String selectConceptoRetroactivo) {
    this.selectConceptoRetroactivo = selectConceptoRetroactivo;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }

  public int getAnioHistorico() {
    return this.anioHistorico;
  }
  public void setAnioHistorico(int anioHistorico) {
    this.anioHistorico = anioHistorico;
  }
  public int getMesHistorico() {
    return this.mesHistorico;
  }
  public void setMesHistorico(int mesHistorico) {
    this.mesHistorico = mesHistorico;
  }
  public String getOrigen() {
    return this.origen;
  }
  public void setOrigen(String origen) {
    this.origen = origen;
  }
}