package sigefirrhh.personal.otrosProcesos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class CalcularBonoFinAnioBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(CalcularBonoFinAnioBeanBusiness.class.getName());

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public int calcular(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, String proceso, String estatus, String usuario)
    throws Exception
  {
    this.txn.open();

    TipoPersonal tipoPersonal = new TipoPersonal();
    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    tipoPersonal = tipoPersonalBeanBusiness.findTipoPersonalById(idTipoPersonal);

    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());

    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_bono_fin_anio(?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setDate(2, fechaInicioSql);
      st.setDate(3, fechaFinSql);
      st.setString(4, proceso);
      st.setString(5, estatus);
      st.setString(6, usuario);
      st.setString(7, tipoPersonal.getGrupoNomina().getPeriodicidad());

      rs = st.executeQuery();
      connection.commit();
      this.log.error("ejecutó SP calculo_bono_fin_anio con exito");

      this.txn.close();

      return 0;
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        }
    }
  }
}