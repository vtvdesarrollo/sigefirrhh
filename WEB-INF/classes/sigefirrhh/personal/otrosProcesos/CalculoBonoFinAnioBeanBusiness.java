package sigefirrhh.personal.otrosProcesos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class CalculoBonoFinAnioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CalculoBonoFinAnio calculoBonoFinAnioNew = 
      (CalculoBonoFinAnio)BeanUtils.cloneBean(
      calculoBonoFinAnio);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (calculoBonoFinAnioNew.getTrabajador() != null) {
      calculoBonoFinAnioNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        calculoBonoFinAnioNew.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (calculoBonoFinAnioNew.getTipoPersonal() != null) {
      calculoBonoFinAnioNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        calculoBonoFinAnioNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(calculoBonoFinAnioNew);
  }

  public void updateCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio) throws Exception
  {
    CalculoBonoFinAnio calculoBonoFinAnioModify = 
      findCalculoBonoFinAnioById(calculoBonoFinAnio.getIdCalculoBonoFinAnio());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (calculoBonoFinAnio.getTrabajador() != null) {
      calculoBonoFinAnio.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        calculoBonoFinAnio.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (calculoBonoFinAnio.getTipoPersonal() != null) {
      calculoBonoFinAnio.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        calculoBonoFinAnio.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(calculoBonoFinAnioModify, calculoBonoFinAnio);
  }

  public void deleteCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CalculoBonoFinAnio calculoBonoFinAnioDelete = 
      findCalculoBonoFinAnioById(calculoBonoFinAnio.getIdCalculoBonoFinAnio());
    pm.deletePersistent(calculoBonoFinAnioDelete);
  }

  public CalculoBonoFinAnio findCalculoBonoFinAnioById(long idCalculoBonoFinAnio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCalculoBonoFinAnio == pIdCalculoBonoFinAnio";
    Query query = pm.newQuery(CalculoBonoFinAnio.class, filter);

    query.declareParameters("long pIdCalculoBonoFinAnio");

    parameters.put("pIdCalculoBonoFinAnio", new Long(idCalculoBonoFinAnio));

    Collection colCalculoBonoFinAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCalculoBonoFinAnio.iterator();
    return (CalculoBonoFinAnio)iterator.next();
  }

  public Collection findCalculoBonoFinAnioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent calculoBonoFinAnioExtent = pm.getExtent(
      CalculoBonoFinAnio.class, true);
    Query query = pm.newQuery(calculoBonoFinAnioExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(CalculoBonoFinAnio.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    Collection colCalculoBonoFinAnio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCalculoBonoFinAnio);

    return colCalculoBonoFinAnio;
  }
}