package sigefirrhh.personal.otrosProcesos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class AjusteSueldoMinimo
  implements Serializable, PersistenceCapable
{
  private long idAjusteSueldoMinimo;
  private int gradoActual;
  private int pasoActual;
  private double sueldoActual;
  private double otrosSueldo;
  private double compensacionActual;
  private double sueldoNuevo;
  private double pasoNuevo;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "compensacionActual", "gradoActual", "idAjusteSueldoMinimo", "otrosSueldo", "pasoActual", "pasoNuevo", "sueldoActual", "sueldoNuevo", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, Long.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getCompensacionActual()
  {
    return jdoGetcompensacionActual(this);
  }

  public int getGradoActual()
  {
    return jdoGetgradoActual(this);
  }

  public long getIdAjusteSueldoMinimo()
  {
    return jdoGetidAjusteSueldoMinimo(this);
  }

  public double getOtrosSueldo()
  {
    return jdoGetotrosSueldo(this);
  }

  public int getPasoActual()
  {
    return jdoGetpasoActual(this);
  }

  public double getPasoNuevo()
  {
    return jdoGetpasoNuevo(this);
  }

  public double getSueldoActual()
  {
    return jdoGetsueldoActual(this);
  }

  public double getSueldoNuevo()
  {
    return jdoGetsueldoNuevo(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setCompensacionActual(double d)
  {
    jdoSetcompensacionActual(this, d);
  }

  public void setGradoActual(int i)
  {
    jdoSetgradoActual(this, i);
  }

  public void setIdAjusteSueldoMinimo(long l)
  {
    jdoSetidAjusteSueldoMinimo(this, l);
  }

  public void setOtrosSueldo(double d)
  {
    jdoSetotrosSueldo(this, d);
  }

  public void setPasoActual(int i)
  {
    jdoSetpasoActual(this, i);
  }

  public void setPasoNuevo(double d)
  {
    jdoSetpasoNuevo(this, d);
  }

  public void setSueldoActual(double d)
  {
    jdoSetsueldoActual(this, d);
  }

  public void setSueldoNuevo(double d)
  {
    jdoSetsueldoNuevo(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.otrosProcesos.AjusteSueldoMinimo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AjusteSueldoMinimo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AjusteSueldoMinimo localAjusteSueldoMinimo = new AjusteSueldoMinimo();
    localAjusteSueldoMinimo.jdoFlags = 1;
    localAjusteSueldoMinimo.jdoStateManager = paramStateManager;
    return localAjusteSueldoMinimo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AjusteSueldoMinimo localAjusteSueldoMinimo = new AjusteSueldoMinimo();
    localAjusteSueldoMinimo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAjusteSueldoMinimo.jdoFlags = 1;
    localAjusteSueldoMinimo.jdoStateManager = paramStateManager;
    return localAjusteSueldoMinimo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacionActual);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.gradoActual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAjusteSueldoMinimo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.otrosSueldo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.pasoActual);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.pasoNuevo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoActual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoNuevo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacionActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gradoActual = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAjusteSueldoMinimo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otrosSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pasoActual = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pasoNuevo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoNuevo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AjusteSueldoMinimo paramAjusteSueldoMinimo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.compensacionActual = paramAjusteSueldoMinimo.compensacionActual;
      return;
    case 1:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.gradoActual = paramAjusteSueldoMinimo.gradoActual;
      return;
    case 2:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.idAjusteSueldoMinimo = paramAjusteSueldoMinimo.idAjusteSueldoMinimo;
      return;
    case 3:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.otrosSueldo = paramAjusteSueldoMinimo.otrosSueldo;
      return;
    case 4:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.pasoActual = paramAjusteSueldoMinimo.pasoActual;
      return;
    case 5:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.pasoNuevo = paramAjusteSueldoMinimo.pasoNuevo;
      return;
    case 6:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoActual = paramAjusteSueldoMinimo.sueldoActual;
      return;
    case 7:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoNuevo = paramAjusteSueldoMinimo.sueldoNuevo;
      return;
    case 8:
      if (paramAjusteSueldoMinimo == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramAjusteSueldoMinimo.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AjusteSueldoMinimo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AjusteSueldoMinimo localAjusteSueldoMinimo = (AjusteSueldoMinimo)paramObject;
    if (localAjusteSueldoMinimo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAjusteSueldoMinimo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AjusteSueldoMinimoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AjusteSueldoMinimoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AjusteSueldoMinimoPK))
      throw new IllegalArgumentException("arg1");
    AjusteSueldoMinimoPK localAjusteSueldoMinimoPK = (AjusteSueldoMinimoPK)paramObject;
    localAjusteSueldoMinimoPK.idAjusteSueldoMinimo = this.idAjusteSueldoMinimo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AjusteSueldoMinimoPK))
      throw new IllegalArgumentException("arg1");
    AjusteSueldoMinimoPK localAjusteSueldoMinimoPK = (AjusteSueldoMinimoPK)paramObject;
    this.idAjusteSueldoMinimo = localAjusteSueldoMinimoPK.idAjusteSueldoMinimo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AjusteSueldoMinimoPK))
      throw new IllegalArgumentException("arg2");
    AjusteSueldoMinimoPK localAjusteSueldoMinimoPK = (AjusteSueldoMinimoPK)paramObject;
    localAjusteSueldoMinimoPK.idAjusteSueldoMinimo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AjusteSueldoMinimoPK))
      throw new IllegalArgumentException("arg2");
    AjusteSueldoMinimoPK localAjusteSueldoMinimoPK = (AjusteSueldoMinimoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAjusteSueldoMinimoPK.idAjusteSueldoMinimo);
  }

  private static final double jdoGetcompensacionActual(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    if (paramAjusteSueldoMinimo.jdoFlags <= 0)
      return paramAjusteSueldoMinimo.compensacionActual;
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.compensacionActual;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 0))
      return paramAjusteSueldoMinimo.compensacionActual;
    return localStateManager.getDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 0, paramAjusteSueldoMinimo.compensacionActual);
  }

  private static final void jdoSetcompensacionActual(AjusteSueldoMinimo paramAjusteSueldoMinimo, double paramDouble)
  {
    if (paramAjusteSueldoMinimo.jdoFlags == 0)
    {
      paramAjusteSueldoMinimo.compensacionActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.compensacionActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 0, paramAjusteSueldoMinimo.compensacionActual, paramDouble);
  }

  private static final int jdoGetgradoActual(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    if (paramAjusteSueldoMinimo.jdoFlags <= 0)
      return paramAjusteSueldoMinimo.gradoActual;
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.gradoActual;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 1))
      return paramAjusteSueldoMinimo.gradoActual;
    return localStateManager.getIntField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 1, paramAjusteSueldoMinimo.gradoActual);
  }

  private static final void jdoSetgradoActual(AjusteSueldoMinimo paramAjusteSueldoMinimo, int paramInt)
  {
    if (paramAjusteSueldoMinimo.jdoFlags == 0)
    {
      paramAjusteSueldoMinimo.gradoActual = paramInt;
      return;
    }
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.gradoActual = paramInt;
      return;
    }
    localStateManager.setIntField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 1, paramAjusteSueldoMinimo.gradoActual, paramInt);
  }

  private static final long jdoGetidAjusteSueldoMinimo(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    return paramAjusteSueldoMinimo.idAjusteSueldoMinimo;
  }

  private static final void jdoSetidAjusteSueldoMinimo(AjusteSueldoMinimo paramAjusteSueldoMinimo, long paramLong)
  {
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.idAjusteSueldoMinimo = paramLong;
      return;
    }
    localStateManager.setLongField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 2, paramAjusteSueldoMinimo.idAjusteSueldoMinimo, paramLong);
  }

  private static final double jdoGetotrosSueldo(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    if (paramAjusteSueldoMinimo.jdoFlags <= 0)
      return paramAjusteSueldoMinimo.otrosSueldo;
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.otrosSueldo;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 3))
      return paramAjusteSueldoMinimo.otrosSueldo;
    return localStateManager.getDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 3, paramAjusteSueldoMinimo.otrosSueldo);
  }

  private static final void jdoSetotrosSueldo(AjusteSueldoMinimo paramAjusteSueldoMinimo, double paramDouble)
  {
    if (paramAjusteSueldoMinimo.jdoFlags == 0)
    {
      paramAjusteSueldoMinimo.otrosSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.otrosSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 3, paramAjusteSueldoMinimo.otrosSueldo, paramDouble);
  }

  private static final int jdoGetpasoActual(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    if (paramAjusteSueldoMinimo.jdoFlags <= 0)
      return paramAjusteSueldoMinimo.pasoActual;
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.pasoActual;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 4))
      return paramAjusteSueldoMinimo.pasoActual;
    return localStateManager.getIntField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 4, paramAjusteSueldoMinimo.pasoActual);
  }

  private static final void jdoSetpasoActual(AjusteSueldoMinimo paramAjusteSueldoMinimo, int paramInt)
  {
    if (paramAjusteSueldoMinimo.jdoFlags == 0)
    {
      paramAjusteSueldoMinimo.pasoActual = paramInt;
      return;
    }
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.pasoActual = paramInt;
      return;
    }
    localStateManager.setIntField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 4, paramAjusteSueldoMinimo.pasoActual, paramInt);
  }

  private static final double jdoGetpasoNuevo(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    if (paramAjusteSueldoMinimo.jdoFlags <= 0)
      return paramAjusteSueldoMinimo.pasoNuevo;
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.pasoNuevo;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 5))
      return paramAjusteSueldoMinimo.pasoNuevo;
    return localStateManager.getDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 5, paramAjusteSueldoMinimo.pasoNuevo);
  }

  private static final void jdoSetpasoNuevo(AjusteSueldoMinimo paramAjusteSueldoMinimo, double paramDouble)
  {
    if (paramAjusteSueldoMinimo.jdoFlags == 0)
    {
      paramAjusteSueldoMinimo.pasoNuevo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.pasoNuevo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 5, paramAjusteSueldoMinimo.pasoNuevo, paramDouble);
  }

  private static final double jdoGetsueldoActual(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    if (paramAjusteSueldoMinimo.jdoFlags <= 0)
      return paramAjusteSueldoMinimo.sueldoActual;
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.sueldoActual;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 6))
      return paramAjusteSueldoMinimo.sueldoActual;
    return localStateManager.getDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 6, paramAjusteSueldoMinimo.sueldoActual);
  }

  private static final void jdoSetsueldoActual(AjusteSueldoMinimo paramAjusteSueldoMinimo, double paramDouble)
  {
    if (paramAjusteSueldoMinimo.jdoFlags == 0)
    {
      paramAjusteSueldoMinimo.sueldoActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.sueldoActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 6, paramAjusteSueldoMinimo.sueldoActual, paramDouble);
  }

  private static final double jdoGetsueldoNuevo(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    if (paramAjusteSueldoMinimo.jdoFlags <= 0)
      return paramAjusteSueldoMinimo.sueldoNuevo;
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.sueldoNuevo;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 7))
      return paramAjusteSueldoMinimo.sueldoNuevo;
    return localStateManager.getDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 7, paramAjusteSueldoMinimo.sueldoNuevo);
  }

  private static final void jdoSetsueldoNuevo(AjusteSueldoMinimo paramAjusteSueldoMinimo, double paramDouble)
  {
    if (paramAjusteSueldoMinimo.jdoFlags == 0)
    {
      paramAjusteSueldoMinimo.sueldoNuevo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.sueldoNuevo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 7, paramAjusteSueldoMinimo.sueldoNuevo, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(AjusteSueldoMinimo paramAjusteSueldoMinimo)
  {
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
      return paramAjusteSueldoMinimo.trabajador;
    if (localStateManager.isLoaded(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 8))
      return paramAjusteSueldoMinimo.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 8, paramAjusteSueldoMinimo.trabajador);
  }

  private static final void jdoSettrabajador(AjusteSueldoMinimo paramAjusteSueldoMinimo, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramAjusteSueldoMinimo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAjusteSueldoMinimo.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramAjusteSueldoMinimo, jdoInheritedFieldCount + 8, paramAjusteSueldoMinimo.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}