package sigefirrhh.personal.otrosProcesos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.trabajador.Trabajador;

public class ProcesoDiario
  implements Serializable, PersistenceCapable
{
  private long idProcesoDiario;
  private Date fecha;
  private int numeroTransaccion;
  private String tipoTransaccion;
  private int aniosServicio;
  private int edadFamiliar;
  private Familiar familiar;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "edadFamiliar", "familiar", "fecha", "idProcesoDiario", "numeroTransaccion", "tipoTransaccion", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 21, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }

  public int getEdadFamiliar()
  {
    return jdoGetedadFamiliar(this);
  }

  public Familiar getFamiliar()
  {
    return jdoGetfamiliar(this);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public long getIdProcesoDiario()
  {
    return jdoGetidProcesoDiario(this);
  }

  public int getNumeroTransaccion()
  {
    return jdoGetnumeroTransaccion(this);
  }

  public String getTipoTransaccion()
  {
    return jdoGettipoTransaccion(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setAniosServicio(int i)
  {
    jdoSetaniosServicio(this, i);
  }

  public void setEdadFamiliar(int i)
  {
    jdoSetedadFamiliar(this, i);
  }

  public void setFamiliar(Familiar familiar)
  {
    jdoSetfamiliar(this, familiar);
  }

  public void setFecha(Date date)
  {
    jdoSetfecha(this, date);
  }

  public void setIdProcesoDiario(long l)
  {
    jdoSetidProcesoDiario(this, l);
  }

  public void setNumeroTransaccion(int i)
  {
    jdoSetnumeroTransaccion(this, i);
  }

  public void setTipoTransaccion(String string)
  {
    jdoSettipoTransaccion(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.otrosProcesos.ProcesoDiario"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ProcesoDiario());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ProcesoDiario localProcesoDiario = new ProcesoDiario();
    localProcesoDiario.jdoFlags = 1;
    localProcesoDiario.jdoStateManager = paramStateManager;
    return localProcesoDiario;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ProcesoDiario localProcesoDiario = new ProcesoDiario();
    localProcesoDiario.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProcesoDiario.jdoFlags = 1;
    localProcesoDiario.jdoStateManager = paramStateManager;
    return localProcesoDiario;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.edadFamiliar);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProcesoDiario);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroTransaccion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoTransaccion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.edadFamiliar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProcesoDiario = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroTransaccion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoTransaccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ProcesoDiario paramProcesoDiario, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramProcesoDiario.aniosServicio;
      return;
    case 1:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.edadFamiliar = paramProcesoDiario.edadFamiliar;
      return;
    case 2:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramProcesoDiario.familiar;
      return;
    case 3:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramProcesoDiario.fecha;
      return;
    case 4:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.idProcesoDiario = paramProcesoDiario.idProcesoDiario;
      return;
    case 5:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.numeroTransaccion = paramProcesoDiario.numeroTransaccion;
      return;
    case 6:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.tipoTransaccion = paramProcesoDiario.tipoTransaccion;
      return;
    case 7:
      if (paramProcesoDiario == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramProcesoDiario.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ProcesoDiario))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ProcesoDiario localProcesoDiario = (ProcesoDiario)paramObject;
    if (localProcesoDiario.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProcesoDiario, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProcesoDiarioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProcesoDiarioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProcesoDiarioPK))
      throw new IllegalArgumentException("arg1");
    ProcesoDiarioPK localProcesoDiarioPK = (ProcesoDiarioPK)paramObject;
    localProcesoDiarioPK.idProcesoDiario = this.idProcesoDiario;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProcesoDiarioPK))
      throw new IllegalArgumentException("arg1");
    ProcesoDiarioPK localProcesoDiarioPK = (ProcesoDiarioPK)paramObject;
    this.idProcesoDiario = localProcesoDiarioPK.idProcesoDiario;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProcesoDiarioPK))
      throw new IllegalArgumentException("arg2");
    ProcesoDiarioPK localProcesoDiarioPK = (ProcesoDiarioPK)paramObject;
    localProcesoDiarioPK.idProcesoDiario = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProcesoDiarioPK))
      throw new IllegalArgumentException("arg2");
    ProcesoDiarioPK localProcesoDiarioPK = (ProcesoDiarioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localProcesoDiarioPK.idProcesoDiario);
  }

  private static final int jdoGetaniosServicio(ProcesoDiario paramProcesoDiario)
  {
    if (paramProcesoDiario.jdoFlags <= 0)
      return paramProcesoDiario.aniosServicio;
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
      return paramProcesoDiario.aniosServicio;
    if (localStateManager.isLoaded(paramProcesoDiario, jdoInheritedFieldCount + 0))
      return paramProcesoDiario.aniosServicio;
    return localStateManager.getIntField(paramProcesoDiario, jdoInheritedFieldCount + 0, paramProcesoDiario.aniosServicio);
  }

  private static final void jdoSetaniosServicio(ProcesoDiario paramProcesoDiario, int paramInt)
  {
    if (paramProcesoDiario.jdoFlags == 0)
    {
      paramProcesoDiario.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramProcesoDiario, jdoInheritedFieldCount + 0, paramProcesoDiario.aniosServicio, paramInt);
  }

  private static final int jdoGetedadFamiliar(ProcesoDiario paramProcesoDiario)
  {
    if (paramProcesoDiario.jdoFlags <= 0)
      return paramProcesoDiario.edadFamiliar;
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
      return paramProcesoDiario.edadFamiliar;
    if (localStateManager.isLoaded(paramProcesoDiario, jdoInheritedFieldCount + 1))
      return paramProcesoDiario.edadFamiliar;
    return localStateManager.getIntField(paramProcesoDiario, jdoInheritedFieldCount + 1, paramProcesoDiario.edadFamiliar);
  }

  private static final void jdoSetedadFamiliar(ProcesoDiario paramProcesoDiario, int paramInt)
  {
    if (paramProcesoDiario.jdoFlags == 0)
    {
      paramProcesoDiario.edadFamiliar = paramInt;
      return;
    }
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.edadFamiliar = paramInt;
      return;
    }
    localStateManager.setIntField(paramProcesoDiario, jdoInheritedFieldCount + 1, paramProcesoDiario.edadFamiliar, paramInt);
  }

  private static final Familiar jdoGetfamiliar(ProcesoDiario paramProcesoDiario)
  {
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
      return paramProcesoDiario.familiar;
    if (localStateManager.isLoaded(paramProcesoDiario, jdoInheritedFieldCount + 2))
      return paramProcesoDiario.familiar;
    return (Familiar)localStateManager.getObjectField(paramProcesoDiario, jdoInheritedFieldCount + 2, paramProcesoDiario.familiar);
  }

  private static final void jdoSetfamiliar(ProcesoDiario paramProcesoDiario, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramProcesoDiario, jdoInheritedFieldCount + 2, paramProcesoDiario.familiar, paramFamiliar);
  }

  private static final Date jdoGetfecha(ProcesoDiario paramProcesoDiario)
  {
    if (paramProcesoDiario.jdoFlags <= 0)
      return paramProcesoDiario.fecha;
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
      return paramProcesoDiario.fecha;
    if (localStateManager.isLoaded(paramProcesoDiario, jdoInheritedFieldCount + 3))
      return paramProcesoDiario.fecha;
    return (Date)localStateManager.getObjectField(paramProcesoDiario, jdoInheritedFieldCount + 3, paramProcesoDiario.fecha);
  }

  private static final void jdoSetfecha(ProcesoDiario paramProcesoDiario, Date paramDate)
  {
    if (paramProcesoDiario.jdoFlags == 0)
    {
      paramProcesoDiario.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProcesoDiario, jdoInheritedFieldCount + 3, paramProcesoDiario.fecha, paramDate);
  }

  private static final long jdoGetidProcesoDiario(ProcesoDiario paramProcesoDiario)
  {
    return paramProcesoDiario.idProcesoDiario;
  }

  private static final void jdoSetidProcesoDiario(ProcesoDiario paramProcesoDiario, long paramLong)
  {
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.idProcesoDiario = paramLong;
      return;
    }
    localStateManager.setLongField(paramProcesoDiario, jdoInheritedFieldCount + 4, paramProcesoDiario.idProcesoDiario, paramLong);
  }

  private static final int jdoGetnumeroTransaccion(ProcesoDiario paramProcesoDiario)
  {
    if (paramProcesoDiario.jdoFlags <= 0)
      return paramProcesoDiario.numeroTransaccion;
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
      return paramProcesoDiario.numeroTransaccion;
    if (localStateManager.isLoaded(paramProcesoDiario, jdoInheritedFieldCount + 5))
      return paramProcesoDiario.numeroTransaccion;
    return localStateManager.getIntField(paramProcesoDiario, jdoInheritedFieldCount + 5, paramProcesoDiario.numeroTransaccion);
  }

  private static final void jdoSetnumeroTransaccion(ProcesoDiario paramProcesoDiario, int paramInt)
  {
    if (paramProcesoDiario.jdoFlags == 0)
    {
      paramProcesoDiario.numeroTransaccion = paramInt;
      return;
    }
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.numeroTransaccion = paramInt;
      return;
    }
    localStateManager.setIntField(paramProcesoDiario, jdoInheritedFieldCount + 5, paramProcesoDiario.numeroTransaccion, paramInt);
  }

  private static final String jdoGettipoTransaccion(ProcesoDiario paramProcesoDiario)
  {
    if (paramProcesoDiario.jdoFlags <= 0)
      return paramProcesoDiario.tipoTransaccion;
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
      return paramProcesoDiario.tipoTransaccion;
    if (localStateManager.isLoaded(paramProcesoDiario, jdoInheritedFieldCount + 6))
      return paramProcesoDiario.tipoTransaccion;
    return localStateManager.getStringField(paramProcesoDiario, jdoInheritedFieldCount + 6, paramProcesoDiario.tipoTransaccion);
  }

  private static final void jdoSettipoTransaccion(ProcesoDiario paramProcesoDiario, String paramString)
  {
    if (paramProcesoDiario.jdoFlags == 0)
    {
      paramProcesoDiario.tipoTransaccion = paramString;
      return;
    }
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.tipoTransaccion = paramString;
      return;
    }
    localStateManager.setStringField(paramProcesoDiario, jdoInheritedFieldCount + 6, paramProcesoDiario.tipoTransaccion, paramString);
  }

  private static final Trabajador jdoGettrabajador(ProcesoDiario paramProcesoDiario)
  {
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
      return paramProcesoDiario.trabajador;
    if (localStateManager.isLoaded(paramProcesoDiario, jdoInheritedFieldCount + 7))
      return paramProcesoDiario.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramProcesoDiario, jdoInheritedFieldCount + 7, paramProcesoDiario.trabajador);
  }

  private static final void jdoSettrabajador(ProcesoDiario paramProcesoDiario, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramProcesoDiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramProcesoDiario.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramProcesoDiario, jdoInheritedFieldCount + 7, paramProcesoDiario.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}