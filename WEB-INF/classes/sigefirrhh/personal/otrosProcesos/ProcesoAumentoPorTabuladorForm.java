package sigefirrhh.personal.otrosProcesos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ProcesoAumentoPorTabuladorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProcesoAumentoPorTabuladorForm.class.getName());

  private String selectProceso = "1";
  private String selectTipoPersonal;
  private String selectTabuladorActual;
  private String selectTabuladorNuevo;
  private long idTabuladorActual;
  private long idTabuladorNuevo;
  private int mes;
  private int anio;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private DefinicionesFacadeExtend definicionesFacadeExtend;
  private OtrosProcesosNoGenFacade otrosProcesosFacade;
  private CargoNoGenFacade cargoFacade;
  private LoginSession login;
  private boolean show;
  private boolean show2;
  private boolean auxShow;
  private Collection listTabuladorActual;
  private Collection listTabuladorNuevo;
  private String metodo = "P";
  private String mantenerPaso = "S";
  private int reportId;
  private String reportName;
  private String formato = "1";
  private String retroactivo;
  private long idConceptoRetroactivo = 0L;
  private long idFrecuenciaRetroactivo = 0L;
  private int diasRetroactivo = 0;
  private String selectConceptoRetroactivo;
  private Collection listConceptoRetroactivo;
  private boolean showRetroactivo;
  private String periodicidad;

  public boolean isShowCondicionQuincenal()
  {
    return (this.periodicidad != null) && (!this.periodicidad.equals("S"));
  }
  public boolean isShowCondicionSemanal() {
    return (this.periodicidad != null) && (this.periodicidad.equals("S"));
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.auxShow = false;
      if (this.idTipoPersonal != 0L) {
        this.auxShow = true;
        TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
        this.periodicidad = tipoPersonal.getGrupoNomina().getPeriodicidad();
      }

    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTabuladorActual(ValueChangeEvent event) {
    this.idTabuladorActual = Long.valueOf(
      (String)event.getNewValue()).longValue();
  }

  public void changeTabuladorNuevo(ValueChangeEvent event)
  {
    this.idTabuladorNuevo = Long.valueOf(
      (String)event.getNewValue()).longValue();
  }

  public ProcesoAumentoPorTabuladorForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.definicionesFacadeExtend = new DefinicionesFacadeExtend();
    this.otrosProcesosFacade = new OtrosProcesosNoGenFacade();

    this.cargoFacade = new CargoNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();

    this.reportName = "aplicartabulador";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event) {
        ProcesoAumentoPorTabuladorForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    try {
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport() {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
    parameters.put("anio", new Integer(this.anio));
    parameters.put("mes", new Integer(this.mes));

    if (this.formato.equals("2")) {
      this.reportName = ("a_" + this.reportName);
    }

    JasperForWeb report = new JasperForWeb();
    if (this.formato.equals("2")) {
      report.setType(3);
    }

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/otrosProcesos");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());

      this.listTabuladorActual = 
        this.cargoFacade.findTabuladorByOrganismo(this.login.getIdOrganismo());
      this.listTabuladorNuevo = 
        this.cargoFacade.findTabuladorByOrganismo(this.login.getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
      this.listTabuladorActual = new ArrayList();
      this.listTabuladorNuevo = new ArrayList();
    }
  }

  public String preGenerate() throws Exception { FacesContext context = FacesContext.getCurrentInstance();
    if (this.selectProceso.equals("2"))
      this.show2 = true;
    else {
      generate();
    }

    this.show = false;
    return null; }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
      this.otrosProcesosFacade.procesoAumentoPorTabulador(this.idTipoPersonal, this.idTabuladorActual, this.idTabuladorNuevo, 
        this.selectProceso, this.login.getUsuario(), this.mantenerPaso, this.periodicidad);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      context.addMessage("success_add", new FacesMessage("Se calculó con éxito"));
      this.show = false;
      this.show2 = false;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String abort() {
    this.show = true;
    this.show2 = false;
    return null;
  }
  public Collection getListTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getListTabuladorActual() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTabuladorActual, "sigefirrhh.base.cargo.Tabulador");
  }
  public Collection getListTabuladorNuevo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTabuladorNuevo, "sigefirrhh.base.cargo.Tabulador");
  }
  public Collection getListConceptoRetroactivo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoRetroactivo, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }

  public boolean isShow() {
    return this.auxShow;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }

  public void setSelectProceso(String string) {
    this.selectProceso = string;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public int getMes()
  {
    return this.mes;
  }

  public void setAnio(int i)
  {
    this.anio = i;
  }

  public void setMes(int i)
  {
    this.mes = i;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getMetodo() {
    return this.metodo;
  }
  public void setMetodo(String metodo) {
    this.metodo = metodo;
  }

  public int getReportId()
  {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }

  public String getMantenerPaso() {
    return this.mantenerPaso;
  }
  public void setMantenerPaso(String mantenerPaso) {
    this.mantenerPaso = mantenerPaso;
  }
  public String getSelectTabuladorActual() {
    return this.selectTabuladorActual;
  }
  public void setSelectTabuladorActual(String selectTabuladorActual) {
    this.selectTabuladorActual = selectTabuladorActual;
  }
  public String getSelectTabuladorNuevo() {
    return this.selectTabuladorNuevo;
  }
  public void setSelectTabuladorNuevo(String selectTabuladorNuevo) {
    this.selectTabuladorNuevo = selectTabuladorNuevo;
  }
  public int getDiasRetroactivo() {
    return this.diasRetroactivo;
  }
  public void setDiasRetroactivo(int diasRetroactivo) {
    this.diasRetroactivo = diasRetroactivo;
  }
  public String getRetroactivo() {
    return this.retroactivo;
  }
  public void setRetroactivo(String retroactivo) {
    this.retroactivo = retroactivo;
  }
  public String getSelectConceptoRetroactivo() {
    return this.selectConceptoRetroactivo;
  }
  public void setSelectConceptoRetroactivo(String selectConceptoRetroactivo) {
    this.selectConceptoRetroactivo = selectConceptoRetroactivo;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public void setShow2(boolean show2) {
    this.show2 = show2;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
}