package sigefirrhh.personal.otrosProcesos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ProcesoAumentoPorPorcentajeForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProcesoAumentoPorPorcentajeForm.class.getName());
  private Collection listTipoPersonal;
  private Collection listConcepto = new ArrayList();
  private Collection listConceptoRetroactivo;
  private Collection listDependencia;
  private Collection listCargo;
  private Collection listManualCargo;
  private Collection listRegion;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private CargoFacade cargoFacade;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private OtrosProcesosNoGenFacade otrosProcesosNoGenFacade = new OtrosProcesosNoGenFacade();
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal concepto;
  private ConceptoTipoPersonal conceptoRetroactivo;
  private long idConcepto;
  private long idConceptoRetroactivo = 0L;
  private long idFrecuenciaRetroactivo = 0L;
  private long idTipoPersonal;
  private String idManualCargo;
  private long idCargo;
  private long idDependencia;
  private long idRegion;
  private int diasRetroactivo = 0;
  private String tipoConcepto;
  private boolean show1 = false;
  private boolean show2 = false;
  private boolean show = false;

  private boolean showRegion = false;
  private boolean showDependencia = false;
  private boolean showManualCargo = false;
  private boolean showCargo = false;
  private boolean showGrado = false;
  private boolean showFecha = false;
  private boolean showConcepto = false;
  private boolean showConceptoRetroactivo = false;
  private String tipoAumento;
  private double porcentajeAumento = 0.0D;
  private String retroactivo;
  private String descripcionRetroactivo;
  private int desdeGrado = 0;
  private int hastaGrado = 100;
  private Date desdeFechaIngreso;
  private Date hastaFechaIngreso;
  private String criterio;
  private Calendar inicioAux;
  private Calendar finAux;
  private int registros = 0;
  private String accion;

  public Collection getListDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public ProcesoAumentoPorPorcentajeForm() {
    log.error("a1");
    this.tipoAumento = "1";

    this.inicioAux = Calendar.getInstance();
    this.finAux = Calendar.getInstance();

    this.inicioAux.setTime(new Date());
    this.finAux.setTime(new Date());

    this.desdeFechaIngreso = null;
    this.hastaFechaIngreso = null;

    FacesContext context = FacesContext.getCurrentInstance();
    this.definicionesFacade = new DefinicionesFacadeExtend();
    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();

    this.cargoFacade = new CargoFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listDependencia = this.estructuraFacade.findAllDependencia();
      this.listRegion = this.estructuraFacade.findAllRegion();
      this.listManualCargo = this.cargoFacade.findAllManualCargo();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    refresh();
  }

  public void refresh()
  {
  }

  public void changeTipoAumento(ValueChangeEvent event)
  {
    findConceptoTipoPersonal();
  }
  public void changeRetroactivo(ValueChangeEvent event) {
    try {
      this.retroactivo = 
        ((String)event.getNewValue());

      if (this.retroactivo.equals("S"))
        findRetroactivo();
      else
        this.conceptoRetroactivo = new ConceptoTipoPersonal();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void findRetroactivo()
  {
    try {
      this.showConceptoRetroactivo = false;
      if (this.retroactivo.equals("S")) {
        this.conceptoRetroactivo = ((ConceptoTipoPersonal)this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonalAndConcepto(this.concepto.getTipoPersonal().getIdTipoPersonal(), this.concepto.getConcepto().getConceptoRetroactivo().getIdConcepto()).iterator().next());
        this.showConceptoRetroactivo = true;
        this.descripcionRetroactivo = this.conceptoRetroactivo.toString();
        this.idConceptoRetroactivo = this.conceptoRetroactivo.getIdConceptoTipoPersonal();
        this.idFrecuenciaRetroactivo = this.conceptoRetroactivo.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal();
        log.error("concepto-Retroactivo" + this.conceptoRetroactivo);
      }
      else {
        this.conceptoRetroactivo = new ConceptoTipoPersonal();
        this.idConceptoRetroactivo = 0L;
        this.idFrecuenciaRetroactivo = 0L;
        this.descripcionRetroactivo = "";
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.conceptoRetroactivo = new ConceptoTipoPersonal();
    }
  }

  public void changeCriterio(ValueChangeEvent event)
  {
    String criterio = 
      (String)event.getNewValue();

    this.showRegion = false;
    this.showDependencia = false;
    this.showGrado = false;
    this.showCargo = false;
    this.showFecha = false;
    try
    {
      if (criterio.equals("1"))
        this.showRegion = true;
      else if (criterio.equals("2"))
        this.showGrado = true;
      else if (criterio.equals("3")) {
        this.showFecha = true;
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.show1 = true;
    this.show2 = false;
    this.show = true;
  }

  private void findConceptoTipoPersonal() {
    try {
      if (this.tipoAumento.equals("1"))
        this.listConcepto = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonalSueldoBasico(
          this.idTipoPersonal);
      else if (this.tipoAumento.equals("2"))
        this.listConcepto = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonalAjusteSueldo(
          this.idTipoPersonal);
      else if (this.tipoAumento.equals("3"))
        this.listConcepto = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonalCompensacion(
          this.idTipoPersonal);
      else if (this.tipoAumento.equals("4"))
        this.listConcepto = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonalPrimasTrabajador(
          this.idTipoPersonal);
      else if (this.tipoAumento.equals("5"))
        this.listConcepto = 
          this.definicionesNoGenFacade.findConceptoTipoPersonalByTipoPersonalPrimasCargo(
          this.idTipoPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.descripcionRetroactivo = "";
      findRetroactivo();
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.listConcepto = null;
    try
    {
      if (this.idTipoPersonal > 0L)
      {
        findConceptoTipoPersonal();
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.show1 = true;
    this.show2 = false;
    this.show = true;
  }
  public void changeConcepto(ValueChangeEvent event) {
    long id = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.concepto = new ConceptoTipoPersonal();
    try
    {
      if (this.idTipoPersonal > 0L)
      {
        this.concepto = this.definicionesFacade.findConceptoTipoPersonalById(id);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.show1 = true;
    this.show2 = false;
    this.show = true;
  }

  public void changeManualCargo(ValueChangeEvent event)
  {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.idManualCargo = null;
    this.listCargo = null;
    try
    {
      if (idManualCargo > 0L)
        this.listCargo = 
          this.cargoFacade.findCargoByManualCargo(idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
      this.otrosProcesosNoGenFacade.procesoAumentoPorPorcentaje(this.idTipoPersonal, this.criterio, 
        this.idRegion, this.desdeGrado, this.hastaGrado, 
        this.desdeFechaIngreso, this.hastaFechaIngreso, this.retroactivo, 
        this.idConceptoRetroactivo, this.idFrecuenciaRetroactivo, 
        this.diasRetroactivo, this.tipoAumento, 
        this.concepto.getIdConceptoTipoPersonal(), this.porcentajeAumento, 
        this.concepto.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal(), 
        this.concepto.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago());

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      this.show1 = false;
      this.show2 = false;

      context.addMessage("success_add", new FacesMessage("Se ejecutó con éxito"));

      return null;
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = true;
    this.show1 = false;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    this.show1 = true;
    return null;
  }

  public Collection getListConcepto() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConcepto, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListConceptoRetroactivo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoRetroactivo, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public String getIdConcepto() {
    return String.valueOf(this.idConcepto);
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public String getTipoConcepto() {
    return this.tipoConcepto;
  }

  public void setTipoConcepto(String tipoConcepto) {
    this.tipoConcepto = tipoConcepto;
  }

  public boolean isShowCargo()
  {
    return (this.listCargo != null) && 
      (!this.listCargo.isEmpty());
  }
  public boolean isShow1() {
    return this.show1;
  }
  public boolean isShow2() {
    return this.show2;
  }
  public boolean isShowConcepto() {
    return (this.listConcepto != null) && (!this.listConcepto.isEmpty());
  }

  public int getRegistros() {
    return this.registros;
  }

  public int getDesdeGrado()
  {
    return this.desdeGrado;
  }

  public int getHastaGrado()
  {
    return this.hastaGrado;
  }

  public void setDesdeGrado(int i)
  {
    this.desdeGrado = i;
  }

  public void setHastaGrado(int i)
  {
    this.hastaGrado = i;
  }

  public long getIdDependencia()
  {
    return this.idDependencia;
  }

  public void setIdDependencia(long l)
  {
    this.idDependencia = l;
  }

  public boolean isShow()
  {
    return this.show;
  }

  public void setShow(boolean b)
  {
    this.show = b;
  }

  public String getAccion()
  {
    return this.accion;
  }

  public void setAccion(String string)
  {
    this.accion = string;
  }

  public long getIdCargo()
  {
    return this.idCargo;
  }

  public Collection getListManualCargo()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listManualCargo, "sigefirrhh.base.cargo.ManualCargo");
  }

  public String getHastaFechaIngreso()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.finAux.getTime());
  }

  public String getDesdeFechaIngreso()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(this.inicioAux.getTime());
  }

  public String getIdManualCargo()
  {
    return this.idManualCargo;
  }

  public void setIdManualCargo(String string)
  {
    this.idManualCargo = string;
  }

  public Collection getListRegion()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }

  public Collection getListCargo()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCargo, "sigefirrhh.base.cargo.Cargo");
  }

  public void setIdCargo(String l)
  {
    this.idCargo = Long.parseLong(l);

    Iterator iterator = this.listCargo.iterator();
    Cargo cargo = null;

    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        l)) {
        this.idCargo = cargo.getIdCargo();
        break;
      }
    }
  }

  public void setIdConcepto(String l)
  {
    this.idConcepto = Long.parseLong(l);
    Iterator iterator = this.listConcepto.iterator();
    ConceptoTipoPersonal concepto = null;

    while (iterator.hasNext()) {
      concepto = (ConceptoTipoPersonal)iterator.next();
      if (String.valueOf(concepto.getIdConceptoTipoPersonal()).equals(
        l)) {
        this.idConcepto = concepto.getIdConceptoTipoPersonal();
        break;
      }
    }
    findRetroactivo();
  }

  public String getCriterio()
  {
    return this.criterio;
  }

  public void setCriterio(String string)
  {
    this.criterio = string;
  }

  public boolean isShowRegion()
  {
    return this.showRegion;
  }

  public void setShowRegion(boolean b)
  {
    this.showRegion = b;
  }

  public boolean isShowConceptoRetroactivo()
  {
    return this.showConceptoRetroactivo;
  }

  public boolean isShowDependencia()
  {
    return this.showDependencia;
  }

  public boolean isShowFecha()
  {
    return this.showFecha;
  }

  public boolean isShowGrado()
  {
    return this.showGrado;
  }

  public boolean isShowManualCargo()
  {
    return this.showManualCargo;
  }

  public void setShowCargo(boolean b)
  {
    this.showCargo = b;
  }

  public double getPorcentajeAumento()
  {
    return this.porcentajeAumento;
  }

  public void setPorcentajeAumento(double d)
  {
    this.porcentajeAumento = d;
  }

  public String getRetroactivo()
  {
    return this.retroactivo;
  }
  public void setRetroactivo(String retroactivo) {
    this.retroactivo = retroactivo;
  }
  public long getIdConceptoRetroactivo() {
    return this.idConceptoRetroactivo;
  }
  public void setIdConceptoRetroactivo(long idConceptoRetroactivo) {
    this.idConceptoRetroactivo = idConceptoRetroactivo;
  }
  public long getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(long idRegion) {
    this.idRegion = idRegion;
  }
  public void setIdCargo(long idCargo) {
    this.idCargo = idCargo;
  }
  public String getTipoAumento() {
    return this.tipoAumento;
  }
  public void setTipoAumento(String tipoAumento) {
    this.tipoAumento = tipoAumento;
  }
  public void setDesdeFechaIngreso(Date desdeFechaIngreso) {
    this.desdeFechaIngreso = desdeFechaIngreso;
  }
  public void setHastaFechaIngreso(Date hastaFechaIngreso) {
    this.hastaFechaIngreso = hastaFechaIngreso;
  }

  public int getDiasRetroactivo()
  {
    return this.diasRetroactivo;
  }

  public void setDiasRetroactivo(int i)
  {
    this.diasRetroactivo = i;
  }

  public ConceptoTipoPersonal getConceptoRetroactivo()
  {
    return this.conceptoRetroactivo;
  }

  public String getDescripcionRetroactivo()
  {
    return this.descripcionRetroactivo;
  }

  public void setDescripcionRetroactivo(String string)
  {
    this.descripcionRetroactivo = string;
  }
}