package sigefirrhh.personal.otrosProcesos;

import eforserver.business.AbstractBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesBusiness;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.ParametroVarios;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.VacacionesPorAnio;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;

public class OtrosProcesosNoGenBusiness extends AbstractBusiness
{
  Logger log = Logger.getLogger(OtrosProcesosNoGenBusiness.class.getName());

  private ProcesoAumentoPorPorcentajeBeanBusiness procesoAumentoPorPorcentajeBeanBusiness = new ProcesoAumentoPorPorcentajeBeanBusiness();

  private ActualizarPromediosBeanBusiness actualizarPromediosBeanBusiness = new ActualizarPromediosBeanBusiness();

  private CalcularBonoFinAnioBeanBusiness calcularBonoFinAnioBeanBusiness = new CalcularBonoFinAnioBeanBusiness();
  private DefinicionesNoGenBusiness definicionesNoGenBusiness = new DefinicionesNoGenBusiness();
  private DefinicionesBusiness definicionesBusiness = new DefinicionesBusiness();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

  public void procesoAumentoPorTabulador(long idTipoPersonal, long idTabuladorActual, long idTabuladorNuevo, String proceso, String usuario, String mantenerPaso, String periodicidad)
    throws Exception
  {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      java.sql.Date fechaSql = new java.sql.Date(new java.util.Date().getYear(), new java.util.Date().getMonth(), new java.util.Date().getDate());

      this.log.error("1-idTipoPersonal " + idTipoPersonal);
      this.log.error("2-idTabuladorActual " + idTabuladorActual);
      this.log.error("3-idTabuladorNuevo " + idTabuladorNuevo);
      this.log.error("4-proceso " + proceso);
      this.log.error("5-usuario " + usuario);
      this.log.error("6-fecha " + fechaSql);
      this.log.error("7-mantenerPaso " + mantenerPaso);
      this.log.error("8-periodicidad " + periodicidad);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_aumento_tabulador(?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idTabuladorActual);
      st.setLong(3, idTabuladorNuevo);
      st.setString(4, proceso);
      st.setString(5, usuario);
      st.setDate(6, fechaSql);
      st.setString(7, mantenerPaso);
      st.setString(8, periodicidad);

      rs = st.executeQuery();
      connection.commit();

      this.log.error("ejecutó SP ");
    } finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        } 
    }
  }

  public void procesoAumentoPorEvaluacion(long idTipoPersonal, int anio, int mes, String proceso, String usuario, String periodicidad, String metodo, String registro, String grabar, java.util.Date fecha, long idConceptoTipoPersonal, String pagarRetroactivo, int diasRetroactivo, long idConceptoRetroactivo, long idFrecuenciaRetroactivo)
    throws Exception
  {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      java.sql.Date fechaSql = new java.sql.Date(new java.util.Date().getYear(), new java.util.Date().getMonth(), new java.util.Date().getDate());

      this.log.error("1-idTipoPersonal " + idTipoPersonal);
      this.log.error("2-mes " + mes);
      this.log.error("3-anio " + anio);
      this.log.error("4-proceso " + proceso);
      this.log.error("5-usuario " + usuario);
      this.log.error("6-periodicidad " + periodicidad);
      this.log.error("7-metodo " + metodo);
      this.log.error("8-registro " + registro);
      this.log.error("9-grabar " + grabar);
      this.log.error("10-fecha " + fechaSql);
      this.log.error("11-id_ctp " + idConceptoTipoPersonal);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_aumento_evaluacion(?,?,?,?,?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setInt(2, anio);
      st.setInt(3, mes);
      st.setString(4, proceso);
      st.setString(5, usuario);
      st.setString(6, periodicidad);
      st.setString(7, metodo);
      st.setString(8, registro);
      st.setString(9, grabar);
      st.setDate(10, fechaSql);
      st.setLong(11, idConceptoTipoPersonal);

      rs = st.executeQuery();
      connection.commit();

      this.log.error("ejecutó SP ");
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        } 
    }
  }

  public void calcularRetroactivo(long idTipoPersonal, long idConceptoTipoPersonal, java.util.Date fechaVigencia, int dias, String periodicidad, long idFrecuenciaTipoPersonal) throws Exception { Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      java.util.Date fechaProceso = new java.util.Date();
      java.sql.Date fechaProcesoSql = new java.sql.Date(fechaProceso.getYear(), fechaProceso.getMonth(), fechaProceso.getDate());
      java.sql.Date fechaVigenciaSql = new java.sql.Date(fechaVigencia.getYear(), fechaVigencia.getMonth(), fechaVigencia.getDate());

      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      this.log.error("1-idTipoPersonal " + idTipoPersonal);
      this.log.error("2-idConceptoTipoPersonal " + idConceptoTipoPersonal);
      this.log.error("3-fechaVigencia " + fechaVigencia);
      this.log.error("4-fechaProceso " + fechaProceso);
      this.log.error("5-dias " + dias);
      this.log.error("6-periodicidad " + periodicidad);
      this.log.error("7-idFrecuenciaTipoPersonal " + idFrecuenciaTipoPersonal);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_retroactivo(?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idConceptoTipoPersonal);
      st.setDate(3, fechaVigenciaSql);
      st.setDate(4, fechaProcesoSql);
      st.setInt(5, dias);
      st.setString(6, periodicidad);
      st.setLong(7, idFrecuenciaTipoPersonal);

      rs = st.executeQuery();
      connection.commit();

      this.log.error("ejecutó SP ");
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {
        } 
    } } 
  public int suspenderTrabajadores(long idTipoPersonal) throws Exception {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      this.log.error("1-idTipoPersonal " + idTipoPersonal);

      StringBuffer sql = new StringBuffer();
      sql.append("select suspender_trabajadores(?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);

      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);

      connection.commit();
      this.log.error("ejecutó SP suspender trabajadores");
      return valor;
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    }
  }

  public int desactivarFeVida(long idTipoPersonal) throws Exception { Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      this.log.error("1-idTipoPersonal " + idTipoPersonal);

      StringBuffer sql = new StringBuffer();
      sql.append("select desactivar_fe_vida(?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);

      int valor = 0;
      rs = st.executeQuery();
      rs.next();
      valor = rs.getInt(1);

      connection.commit();

      this.log.error("ejecutó SP desactivar_fe_vida");
      return valor;
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException3) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5)
        {
        } 
    } } 
  public boolean calcular(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, String proceso, long idSeguridadAniversario, String usuario)
    throws Exception
  {
    CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

    ParametroVarios parametroVarios = new ParametroVarios();
    Collection colParametroVarios = this.definicionesBusiness.findParametroVariosByTipoPersonal(idTipoPersonal);
    Iterator iteratorParametroVarios = colParametroVarios.iterator();
    parametroVarios = (ParametroVarios)iteratorParametroVarios.next();

    TipoPersonal tipoPersonal = new TipoPersonal();
    tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    int diaInicio = fechaInicio.getDate();
    int mesInicio = fechaInicio.getMonth() + 1;
    int diaFin = fechaFin.getDate();
    int mesFin = fechaFin.getMonth() + 1;

    java.util.Date fechaActual = new java.util.Date();
    java.sql.Date fechaActualSql = new java.sql.Date(fechaActual.getYear(), fechaActual.getMonth(), fechaActual.getDate());
    java.sql.Date fechaFinSql = new java.sql.Date(fechaFin.getYear(), fechaFin.getMonth(), fechaFin.getDate());

    java.sql.Date fechaInicioSql = new java.sql.Date(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDate());

    double montoA = 0.0D;
    double montoB = 0.0D;
    double montoC = 0.0D;
    double montoD = 0.0D;
    double totalMontoA = 0.0D;
    double totalMontoB = 0.0D;
    double totalMontoC = 0.0D;
    double totalMontoD = 0.0D;
    double totalUnidades = 0.0D;
    int aniosCumple = 0;
    long diasDividir = 0L;

    double montoVariable = 0.0D;
    int mes = 0;

    Connection connection = null;

    ResultSet rsSemana = null;
    PreparedStatement stSemana = null;

    ResultSet rsConceptoPetrolero = null;
    PreparedStatement stConceptoPetrolero = null;

    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;

    Statement stInsert = null;
    Statement stInsert2 = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      stInsert = connection.createStatement();
      stInsert2 = connection.createStatement();
      StringBuffer sql = new StringBuffer();

      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal, ctp.tipo, ctp.unidades, ctp.valor, ctp.tope_minimo, ctp.tope_maximo ");
      sql.append(" from conceptotipopersonal ctp, concepto c ");
      sql.append("  where ctp.id_tipo_personal = ? and c.cod_concepto = '1700'");
      sql.append(" and ctp.id_concepto = c.id_concepto");

      stConceptoPetrolero = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoPetrolero.setLong(1, idTipoPersonal);
      rsConceptoPetrolero = stConceptoPetrolero.executeQuery();
      rsConceptoPetrolero.next();

      if (tipoPersonal.getGrupoNomina().getPeriodicidad().equals("S")) {
        sql = new StringBuffer();
        sql.append(" select anio, mes from semana where fecha_inicio = ? ");
        stSemana = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stSemana.setDate(1, fechaInicioSql);
        rsSemana = stSemana.executeQuery();
        rsSemana.next();
      }

      sql = new StringBuffer();
      sql.append("select t.id_trabajador, t.anio_vacaciones, tu.jornada_diaria, t.id_cargo, ");
      sql.append(" tu.jornada_semanal, tp.formula_integral, tp.formula_semanal, p.anios_servicio_apn");
      sql.append(" from trabajador t, personal p, turno tu, tipopersonal tp ");
      sql.append(" where t.id_tipo_personal = tp.id_tipo_personal");
      sql.append(" and t.id_personal = p.id_personal");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and tp.id_tipo_personal = ? and t.estatus = 'A'");
      sql.append("  and (t.mes_vacaciones*100)+t.dia_vacaciones >= ? ");
      sql.append("  and (t.mes_vacaciones)*100+t.dia_vacaciones <= ? ");
      sql.append(" and t.anio_vacaciones < ?");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stTrabajadores.setLong(1, idTipoPersonal);
      stTrabajadores.setInt(2, mesInicio * 100 + diaInicio);
      stTrabajadores.setInt(3, mesFin * 100 + diaFin);
      stTrabajadores.setInt(4, fechaInicio.getYear() + 1900);
      rsTrabajadores = stTrabajadores.executeQuery();

      while (rsTrabajadores.next())
      {
        totalUnidades = 0.0D;
        totalMontoA = 0.0D;
        totalMontoB = 0.0D;
        totalMontoC = 0.0D;
        totalMontoD = 0.0D;
        aniosCumple = 0;
        aniosCumple = fechaFin.getYear() + 1900 - rsTrabajadores.getInt("anio_vacaciones");
        if ((parametroVarios.getSumoApn().equals("S")) && (rsTrabajadores.getInt("anios_servicio_apn") > 0)) {
          aniosCumple += rsTrabajadores.getInt("anios_servicio_apn");
        }
        this.log.error("aniosCumple: " + aniosCumple);

        VacacionesPorAnio vacacionesPorAnio = this.definicionesNoGenBusiness.findVacacionesPorAnioForAniosServicio(idTipoPersonal, aniosCumple);

        stInsert2.executeBatch();

        double montoPetrolero = calcularConceptoBeanBusiness.calcular(rsConceptoPetrolero.getLong("id_concepto_tipo_personal"), rsTrabajadores.getLong("id_trabajador"), 1.0D, rsConceptoPetrolero.getString("tipo"), 1, rsTrabajadores.getDouble("jornada_diaria"), rsTrabajadores.getDouble("jornada_semanal"), rsTrabajadores.getString("formula_integral"), rsTrabajadores.getString("formula_semanal"), rsTrabajadores.getLong("id_cargo"), rsConceptoPetrolero.getDouble("valor"), rsConceptoPetrolero.getDouble("tope_minimo"), rsConceptoPetrolero.getDouble("tope_maximo"));
        montoPetrolero = (parametroVarios.getConstantePetroleroA() + parametroVarios.getConstantePetroleroB() * vacacionesPorAnio.getDiasBono()) * montoPetrolero;
        montoPetrolero /= parametroVarios.getConstantePetroleroC();
        montoPetrolero *= vacacionesPorAnio.getDiasBono();

        mes = fechaInicio.getMonth() + 1;

        if (proceso.equals("2")) {
          sql = new StringBuffer();
          sql.append("insert into conceptovariable (id_trabajador, id_concepto_tipo_personal, ");
          sql.append(" id_frecuencia_tipo_personal, unidades, monto, fecha_registro, ");
          sql.append(" estatus, id_concepto_variable) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", ");
          sql.append(rsConceptoPetrolero.getLong("id_concepto_tipo_personal") + ", ");
          sql.append(rsConceptoPetrolero.getLong("id_frecuencia_tipo_personal") + ", ");
          sql.append(vacacionesPorAnio.getDiasBono() + ",");
          sql.append(NumberTools.twoDecimal(montoPetrolero) + ",");
          sql.append("'" + fechaActualSql + "',");
          sql.append("'A', ");
          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");
          stInsert.addBatch(sql.toString());
        }
      }

      stInsert.executeBatch();
      connection.commit();
    } finally {
      if (rsSemana != null) try {
          rsSemana.close();
        } catch (Exception localException) {
        } if (rsConceptoPetrolero != null) try {
          rsConceptoPetrolero.close();
        } catch (Exception localException1) {
        } if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException2) {
        } if (stSemana != null) try {
          stSemana.close();
        } catch (Exception localException3) {
        } if (stConceptoPetrolero != null) try {
          stConceptoPetrolero.close();
        } catch (Exception localException4) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException5) {
        } if (stInsert != null) try {
          stInsert.close();
        } catch (Exception localException6) {
        } if (stInsert2 != null) try {
          stInsert2.close();
        } catch (Exception localException7) {
        } if (connection != null) try {
          connection.close(); connection = null;
        } catch (Exception localException8) {  }
 
    }
    return true;
  }

  public void calcularBonoPetrolero(long idTipoPersonal, java.util.Date fechaProceso, java.util.Date fechaEgresado) throws Exception {
    Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      java.sql.Date fechaProcesoSql = new java.sql.Date(fechaProceso.getYear(), fechaProceso.getMonth(), fechaProceso.getDate());
      java.sql.Date fechaEgresadoSql = new java.sql.Date(fechaEgresado.getYear(), fechaEgresado.getMonth(), fechaEgresado.getDate());

      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      this.log.error("1-idTipoPersonal " + idTipoPersonal);
      this.log.error("2-fechaProceso " + fechaProceso);
      this.log.error("2-fechaEgresadoo " + fechaEgresado);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_bonopetrolero(?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setDate(2, fechaProcesoSql);
      st.setDate(3, fechaEgresadoSql);

      rs = st.executeQuery();
      connection.commit();

      this.log.error("ejecutó SP ");
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
  }

  public void procesoAumentoPorPorcentaje(long idTipoPersonal, String criterio, long idRegion, int desdeGrado, int hastaGrado, java.util.Date desdeFecha, java.util.Date hastaFecha, String retroactivo, long idConceptoRetroactivo, long idFrecuenciaTipoPersonalRetroactivo, int diasRetroactivo, String tipoAumento, long idConcepto, double porcentajeAumento, long idFrecuenciaTipoPersonalConcepto, int codFrecuenciaPagoConcepto)
    throws Exception
  {
    this.procesoAumentoPorPorcentajeBeanBusiness.proceso(idTipoPersonal, criterio, idRegion, desdeGrado, hastaGrado, desdeFecha, hastaFecha, retroactivo, idConceptoRetroactivo, idFrecuenciaTipoPersonalRetroactivo, diasRetroactivo, tipoAumento, idConcepto, porcentajeAumento, idFrecuenciaTipoPersonalConcepto, codFrecuenciaPagoConcepto);
  }

  public void actualizarPromedios(long idOrganismo, long idGrupoNomina, String periodicidad) throws Exception {
    this.actualizarPromediosBeanBusiness.calcular(idOrganismo, idGrupoNomina, periodicidad);
  }

  public void calcularBonoFinAnio(long idTipoPersonal, java.util.Date fechaInicio, java.util.Date fechaFin, String proceso, String estatus, String usuario) throws Exception {
    this.calcularBonoFinAnioBeanBusiness.calcular(idTipoPersonal, fechaInicio, fechaFin, proceso, estatus, usuario);
  }
}