package sigefirrhh.personal.otrosProcesos;

import java.io.Serializable;

public class AumentoPorPorcentajePK
  implements Serializable
{
  public long idAumentoPorPorcentaje;

  public AumentoPorPorcentajePK()
  {
  }

  public AumentoPorPorcentajePK(long idAumentoPorPorcentaje)
  {
    this.idAumentoPorPorcentaje = idAumentoPorPorcentaje;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AumentoPorPorcentajePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AumentoPorPorcentajePK thatPK)
  {
    return 
      this.idAumentoPorPorcentaje == thatPK.idAumentoPorPorcentaje;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAumentoPorPorcentaje)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAumentoPorPorcentaje);
  }
}