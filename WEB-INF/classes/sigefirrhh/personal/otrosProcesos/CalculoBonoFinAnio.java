package sigefirrhh.personal.otrosProcesos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.trabajador.Trabajador;

public class CalculoBonoFinAnio
  implements Serializable, PersistenceCapable
{
  private long idCalculoBonoFinAnio;
  private double totalBono;
  private double montoAlicuota;
  private double bonoSinAlicuota;
  private double dias;
  private double baseFijo;
  private double basePromedio;
  private double baseDevengado;
  private double baseMensual;
  private double baseProyectado;
  private double baseAlicuota;
  private int aniosServicio;
  private Trabajador trabajador;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosServicio", "baseAlicuota", "baseDevengado", "baseFijo", "baseMensual", "basePromedio", "baseProyectado", "bonoSinAlicuota", "dias", "idCalculoBonoFinAnio", "montoAlicuota", "tipoPersonal", "totalBono", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAniosServicio()
  {
    return jdoGetaniosServicio(this);
  }
  public void setAniosServicio(int aniosServicio) {
    jdoSetaniosServicio(this, aniosServicio);
  }
  public double getBaseAlicuota() {
    return jdoGetbaseAlicuota(this);
  }
  public void setBaseAlicuota(double baseAlicuota) {
    jdoSetbaseAlicuota(this, baseAlicuota);
  }
  public double getBaseDevengado() {
    return jdoGetbaseDevengado(this);
  }
  public void setBaseDevengado(double baseDevengado) {
    jdoSetbaseDevengado(this, baseDevengado);
  }
  public double getBaseFijo() {
    return jdoGetbaseFijo(this);
  }
  public void setBaseFijo(double baseFijo) {
    jdoSetbaseFijo(this, baseFijo);
  }
  public double getBaseMensual() {
    return jdoGetbaseMensual(this);
  }
  public void setBaseMensual(double baseMensual) {
    jdoSetbaseMensual(this, baseMensual);
  }
  public double getBasePromedio() {
    return jdoGetbasePromedio(this);
  }
  public void setBasePromedio(double basePromedio) {
    jdoSetbasePromedio(this, basePromedio);
  }
  public double getBaseProyectado() {
    return jdoGetbaseProyectado(this);
  }
  public void setBaseProyectado(double baseProyectado) {
    jdoSetbaseProyectado(this, baseProyectado);
  }
  public double getBonoSinAlicuota() {
    return jdoGetbonoSinAlicuota(this);
  }
  public void setBonoSinAlicuota(double bonoSinAlicuota) {
    jdoSetbonoSinAlicuota(this, bonoSinAlicuota);
  }
  public double getDias() {
    return jdoGetdias(this);
  }
  public void setDias(double dias) {
    jdoSetdias(this, dias);
  }
  public long getIdCalculoBonoFinAnio() {
    return jdoGetidCalculoBonoFinAnio(this);
  }
  public void setIdCalculoBonoFinAnio(long idCalculoBonoFinAnio) {
    jdoSetidCalculoBonoFinAnio(this, idCalculoBonoFinAnio);
  }
  public double getMontoAlicuota() {
    return jdoGetmontoAlicuota(this);
  }
  public void setMontoAlicuota(double montoAlicuota) {
    jdoSetmontoAlicuota(this, montoAlicuota);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public double getTotalBono() {
    return jdoGettotalBono(this);
  }
  public void setTotalBono(double totalBono) {
    jdoSettotalBono(this, totalBono);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.otrosProcesos.CalculoBonoFinAnio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CalculoBonoFinAnio());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CalculoBonoFinAnio localCalculoBonoFinAnio = new CalculoBonoFinAnio();
    localCalculoBonoFinAnio.jdoFlags = 1;
    localCalculoBonoFinAnio.jdoStateManager = paramStateManager;
    return localCalculoBonoFinAnio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CalculoBonoFinAnio localCalculoBonoFinAnio = new CalculoBonoFinAnio();
    localCalculoBonoFinAnio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCalculoBonoFinAnio.jdoFlags = 1;
    localCalculoBonoFinAnio.jdoStateManager = paramStateManager;
    return localCalculoBonoFinAnio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseAlicuota);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseDevengado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseFijo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseMensual);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.basePromedio);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.baseProyectado);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.bonoSinAlicuota);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.dias);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCalculoBonoFinAnio);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAlicuota);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.totalBono);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseAlicuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseDevengado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseFijo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseMensual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.basePromedio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.baseProyectado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.bonoSinAlicuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dias = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCalculoBonoFinAnio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAlicuota = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalBono = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CalculoBonoFinAnio paramCalculoBonoFinAnio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicio = paramCalculoBonoFinAnio.aniosServicio;
      return;
    case 1:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.baseAlicuota = paramCalculoBonoFinAnio.baseAlicuota;
      return;
    case 2:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.baseDevengado = paramCalculoBonoFinAnio.baseDevengado;
      return;
    case 3:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.baseFijo = paramCalculoBonoFinAnio.baseFijo;
      return;
    case 4:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.baseMensual = paramCalculoBonoFinAnio.baseMensual;
      return;
    case 5:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.basePromedio = paramCalculoBonoFinAnio.basePromedio;
      return;
    case 6:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.baseProyectado = paramCalculoBonoFinAnio.baseProyectado;
      return;
    case 7:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.bonoSinAlicuota = paramCalculoBonoFinAnio.bonoSinAlicuota;
      return;
    case 8:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.dias = paramCalculoBonoFinAnio.dias;
      return;
    case 9:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.idCalculoBonoFinAnio = paramCalculoBonoFinAnio.idCalculoBonoFinAnio;
      return;
    case 10:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.montoAlicuota = paramCalculoBonoFinAnio.montoAlicuota;
      return;
    case 11:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramCalculoBonoFinAnio.tipoPersonal;
      return;
    case 12:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.totalBono = paramCalculoBonoFinAnio.totalBono;
      return;
    case 13:
      if (paramCalculoBonoFinAnio == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramCalculoBonoFinAnio.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CalculoBonoFinAnio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CalculoBonoFinAnio localCalculoBonoFinAnio = (CalculoBonoFinAnio)paramObject;
    if (localCalculoBonoFinAnio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCalculoBonoFinAnio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CalculoBonoFinAnioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CalculoBonoFinAnioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CalculoBonoFinAnioPK))
      throw new IllegalArgumentException("arg1");
    CalculoBonoFinAnioPK localCalculoBonoFinAnioPK = (CalculoBonoFinAnioPK)paramObject;
    localCalculoBonoFinAnioPK.idCalculoBonoFinAnio = this.idCalculoBonoFinAnio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CalculoBonoFinAnioPK))
      throw new IllegalArgumentException("arg1");
    CalculoBonoFinAnioPK localCalculoBonoFinAnioPK = (CalculoBonoFinAnioPK)paramObject;
    this.idCalculoBonoFinAnio = localCalculoBonoFinAnioPK.idCalculoBonoFinAnio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CalculoBonoFinAnioPK))
      throw new IllegalArgumentException("arg2");
    CalculoBonoFinAnioPK localCalculoBonoFinAnioPK = (CalculoBonoFinAnioPK)paramObject;
    localCalculoBonoFinAnioPK.idCalculoBonoFinAnio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 9);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CalculoBonoFinAnioPK))
      throw new IllegalArgumentException("arg2");
    CalculoBonoFinAnioPK localCalculoBonoFinAnioPK = (CalculoBonoFinAnioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 9, localCalculoBonoFinAnioPK.idCalculoBonoFinAnio);
  }

  private static final int jdoGetaniosServicio(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.aniosServicio;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.aniosServicio;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 0))
      return paramCalculoBonoFinAnio.aniosServicio;
    return localStateManager.getIntField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 0, paramCalculoBonoFinAnio.aniosServicio);
  }

  private static final void jdoSetaniosServicio(CalculoBonoFinAnio paramCalculoBonoFinAnio, int paramInt)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.aniosServicio = paramInt;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.aniosServicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 0, paramCalculoBonoFinAnio.aniosServicio, paramInt);
  }

  private static final double jdoGetbaseAlicuota(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.baseAlicuota;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.baseAlicuota;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 1))
      return paramCalculoBonoFinAnio.baseAlicuota;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 1, paramCalculoBonoFinAnio.baseAlicuota);
  }

  private static final void jdoSetbaseAlicuota(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.baseAlicuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.baseAlicuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 1, paramCalculoBonoFinAnio.baseAlicuota, paramDouble);
  }

  private static final double jdoGetbaseDevengado(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.baseDevengado;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.baseDevengado;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 2))
      return paramCalculoBonoFinAnio.baseDevengado;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 2, paramCalculoBonoFinAnio.baseDevengado);
  }

  private static final void jdoSetbaseDevengado(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.baseDevengado = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.baseDevengado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 2, paramCalculoBonoFinAnio.baseDevengado, paramDouble);
  }

  private static final double jdoGetbaseFijo(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.baseFijo;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.baseFijo;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 3))
      return paramCalculoBonoFinAnio.baseFijo;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 3, paramCalculoBonoFinAnio.baseFijo);
  }

  private static final void jdoSetbaseFijo(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.baseFijo = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.baseFijo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 3, paramCalculoBonoFinAnio.baseFijo, paramDouble);
  }

  private static final double jdoGetbaseMensual(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.baseMensual;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.baseMensual;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 4))
      return paramCalculoBonoFinAnio.baseMensual;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 4, paramCalculoBonoFinAnio.baseMensual);
  }

  private static final void jdoSetbaseMensual(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.baseMensual = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.baseMensual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 4, paramCalculoBonoFinAnio.baseMensual, paramDouble);
  }

  private static final double jdoGetbasePromedio(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.basePromedio;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.basePromedio;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 5))
      return paramCalculoBonoFinAnio.basePromedio;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 5, paramCalculoBonoFinAnio.basePromedio);
  }

  private static final void jdoSetbasePromedio(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.basePromedio = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.basePromedio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 5, paramCalculoBonoFinAnio.basePromedio, paramDouble);
  }

  private static final double jdoGetbaseProyectado(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.baseProyectado;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.baseProyectado;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 6))
      return paramCalculoBonoFinAnio.baseProyectado;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 6, paramCalculoBonoFinAnio.baseProyectado);
  }

  private static final void jdoSetbaseProyectado(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.baseProyectado = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.baseProyectado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 6, paramCalculoBonoFinAnio.baseProyectado, paramDouble);
  }

  private static final double jdoGetbonoSinAlicuota(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.bonoSinAlicuota;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.bonoSinAlicuota;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 7))
      return paramCalculoBonoFinAnio.bonoSinAlicuota;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 7, paramCalculoBonoFinAnio.bonoSinAlicuota);
  }

  private static final void jdoSetbonoSinAlicuota(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.bonoSinAlicuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.bonoSinAlicuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 7, paramCalculoBonoFinAnio.bonoSinAlicuota, paramDouble);
  }

  private static final double jdoGetdias(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.dias;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.dias;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 8))
      return paramCalculoBonoFinAnio.dias;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 8, paramCalculoBonoFinAnio.dias);
  }

  private static final void jdoSetdias(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.dias = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.dias = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 8, paramCalculoBonoFinAnio.dias, paramDouble);
  }

  private static final long jdoGetidCalculoBonoFinAnio(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    return paramCalculoBonoFinAnio.idCalculoBonoFinAnio;
  }

  private static final void jdoSetidCalculoBonoFinAnio(CalculoBonoFinAnio paramCalculoBonoFinAnio, long paramLong)
  {
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.idCalculoBonoFinAnio = paramLong;
      return;
    }
    localStateManager.setLongField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 9, paramCalculoBonoFinAnio.idCalculoBonoFinAnio, paramLong);
  }

  private static final double jdoGetmontoAlicuota(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.montoAlicuota;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.montoAlicuota;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 10))
      return paramCalculoBonoFinAnio.montoAlicuota;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 10, paramCalculoBonoFinAnio.montoAlicuota);
  }

  private static final void jdoSetmontoAlicuota(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.montoAlicuota = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.montoAlicuota = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 10, paramCalculoBonoFinAnio.montoAlicuota, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.tipoPersonal;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 11))
      return paramCalculoBonoFinAnio.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 11, paramCalculoBonoFinAnio.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(CalculoBonoFinAnio paramCalculoBonoFinAnio, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 11, paramCalculoBonoFinAnio.tipoPersonal, paramTipoPersonal);
  }

  private static final double jdoGettotalBono(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    if (paramCalculoBonoFinAnio.jdoFlags <= 0)
      return paramCalculoBonoFinAnio.totalBono;
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.totalBono;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 12))
      return paramCalculoBonoFinAnio.totalBono;
    return localStateManager.getDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 12, paramCalculoBonoFinAnio.totalBono);
  }

  private static final void jdoSettotalBono(CalculoBonoFinAnio paramCalculoBonoFinAnio, double paramDouble)
  {
    if (paramCalculoBonoFinAnio.jdoFlags == 0)
    {
      paramCalculoBonoFinAnio.totalBono = paramDouble;
      return;
    }
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.totalBono = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 12, paramCalculoBonoFinAnio.totalBono, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(CalculoBonoFinAnio paramCalculoBonoFinAnio)
  {
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
      return paramCalculoBonoFinAnio.trabajador;
    if (localStateManager.isLoaded(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 13))
      return paramCalculoBonoFinAnio.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 13, paramCalculoBonoFinAnio.trabajador);
  }

  private static final void jdoSettrabajador(CalculoBonoFinAnio paramCalculoBonoFinAnio, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramCalculoBonoFinAnio.jdoStateManager;
    if (localStateManager == null)
    {
      paramCalculoBonoFinAnio.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramCalculoBonoFinAnio, jdoInheritedFieldCount + 13, paramCalculoBonoFinAnio.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}