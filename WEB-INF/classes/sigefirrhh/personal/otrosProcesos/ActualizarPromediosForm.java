package sigefirrhh.personal.otrosProcesos;

import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ActualizarPromediosForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarPromediosForm.class.getName());
  private String selectGrupoNomina;
  private Collection listGrupoNomina;
  private GrupoNomina grupoNomina;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private LoginSession login;
  private OtrosProcesosNoGenFacade otrosProcesosNoGenFacade = new OtrosProcesosNoGenFacade();
  private boolean show;
  private boolean auxShow;

  public ActualizarPromediosForm()
  {
    this.selectGrupoNomina = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void changeGrupoNomina(ValueChangeEvent event)
  {
    long idGrupoNomina = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.grupoNomina = this.definicionesFacade.findGrupoNominaById(idGrupoNomina);
      this.auxShow = true;
    } catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh()
  {
    try
    {
      this.listGrupoNomina = this.definicionesFacade.findAllGrupoNomina();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listGrupoNomina = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.otrosProcesosNoGenFacade.actualizarPromedios(this.login.getIdOrganismo(), this.grupoNomina.getIdGrupoNomina(), this.grupoNomina.getPeriodicidad());
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.grupoNomina);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListGrupoNomina()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listGrupoNomina, "sigefirrhh.base.definiciones.GrupoNomina");
  }

  public String getSelectGrupoNomina()
  {
    return this.selectGrupoNomina;
  }

  public void setSelectGrupoNomina(String string)
  {
    this.selectGrupoNomina = string;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }
}