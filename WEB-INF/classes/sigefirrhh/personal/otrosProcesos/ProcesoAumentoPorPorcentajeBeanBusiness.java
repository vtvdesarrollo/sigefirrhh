package sigefirrhh.personal.otrosProcesos;

import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;

public class ProcesoAumentoPorPorcentajeBeanBusiness
{
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public boolean proceso(long idTipoPersonal, String criterio, long idRegion, int desdeGrado, int hastaGrado, java.util.Date desdeFecha, java.util.Date hastaFecha, String retroactivo, long idConceptoRetroactivo, long idFrecuenciaTipoPersonalRetroactivo, int diasRetroactivo, String tipoAumento, long idConcepto, double porcentajeAumento, long idFrecuenciaTipoPersonalConcepto, int codFrecuenciaPagoConcepto)
    throws Exception
  {
    Connection connection = null;
    Statement stActualizar = null;
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;
    ResultSet rsOtroConcepto = null;
    PreparedStatement stOtroConcepto = null;

    CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = 
      new CalcularSueldosPromedioBeanBusiness();

    double montoAumento = 0.0D;
    double sueldo = 0.0D;
    double montoActualOtroConcepto = 0.0D;
    double montoOtroConcepto = 0.0D;
    boolean otroConceptoExiste = false;

    long idConceptoFijoOtroConcepto = 0L;
    double montoRetroactivo = 0.0D;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();

      sql = new StringBuffer();
      if (criterio.equals("0")) {
        sql.append("select cf.id_concepto_fijo, t.id_trabajador, t.id_tipo_personal, ");
        sql.append(" fp.cod_frecuencia_pago, cf.monto, t.id_trabajador, t.sueldo_basico ");
        sql.append(" from trabajador t, conceptofijo cf, ");
        sql.append(" concepto c, conceptotipopersonal ctp, ");
        sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp ");
        sql.append(" where t.id_tipo_personal = ?");
        sql.append(" and t.id_trabajador = cf.id_trabajador ");
        sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
        sql.append(" and ctp.id_concepto = c.id_concepto ");
        sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal ");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
        sql.append(" and c.sueldo_basico = 'S' ");
        sql.append(" and t.estatus = 'A'");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, idTipoPersonal);
        rsTrabajadores = stTrabajadores.executeQuery();
      } else if (criterio.equals("1")) {
        sql.append("select cf.id_concepto_fijo, t.id_trabajador, t.id_tipo_personal, ");
        sql.append(" fp.cod_frecuencia_pago, cf.monto, t.id_trabajador, t.sueldo_basico ");
        sql.append(" from trabajador t, conceptofijo cf, ");
        sql.append(" concepto c, conceptotipopersonal ctp, dependencia d, sede s, ");
        sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp ");
        sql.append(" where t.id_tipo_personal = ?");
        sql.append(" and t.id_dependencia = d.id_dependencia ");
        sql.append(" and d.id_sede = s.id_sede ");
        sql.append(" and s.id_region = ?");
        sql.append(" and t.id_trabajador = cf.id_trabajador ");
        sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
        sql.append(" and ctp.id_concepto = c.id_concepto ");
        sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal ");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
        sql.append(" and c.sueldo_basico = 'S' ");
        sql.append(" and t.estatus = 'A'");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, idTipoPersonal);
        stTrabajadores.setLong(2, idRegion);
        rsTrabajadores = stTrabajadores.executeQuery();
      } else if (criterio.equals("2")) {
        sql.append("select cf.id_concepto_fijo, t.id_trabajador, t.id_tipo_personal, ");
        sql.append(" fp.cod_frecuencia_pago, cf.monto, t.id_trabajador, t.sueldo_basico ");
        sql.append(" from trabajador t, conceptofijo cf, ");
        sql.append(" concepto c, conceptotipopersonal ctp, cargo ca, ");
        sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp ");
        sql.append(" where t.id_tipo_personal = ?");
        sql.append(" and t.id_cargo = ca.id_cargo");
        sql.append(" and ca.grado between (?) and (?)");
        sql.append(" and t.id_trabajador = cf.id_trabajador ");
        sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
        sql.append(" and ctp.id_concepto = c.id_concepto ");
        sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal ");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
        sql.append(" and c.sueldo_basico = 'S' ");
        sql.append(" and t.estatus = 'A'");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, idTipoPersonal);
        stTrabajadores.setInt(2, desdeGrado);
        stTrabajadores.setInt(3, hastaGrado);
        rsTrabajadores = stTrabajadores.executeQuery();
      } else if (criterio.equals("3")) {
        java.sql.Date desdeFechaSql = new java.sql.Date(desdeFecha.getYear(), desdeFecha.getMonth(), desdeFecha.getDate());
        java.sql.Date hastaFechaSql = new java.sql.Date(hastaFecha.getYear(), hastaFecha.getMonth(), hastaFecha.getDate());
        sql.append("select cf.id_concepto_fijo, t.id_trabajador, t.id_tipo_personal, ");
        sql.append(" fp.cod_frecuencia_pago, cf.monto, t.id_trabajador, t.sueldo_basico ");
        sql.append(" from trabajador t, conceptofijo cf, ");
        sql.append(" concepto c, conceptotipopersonal ctp, cargo ca, ");
        sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp ");
        sql.append(" where t.id_tipo_personal = ?");
        sql.append(" and t.fecha_ingreso between (?) and (?)");
        sql.append(" and t.id_trabajador = cf.id_trabajador ");
        sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
        sql.append(" and ctp.id_concepto = c.id_concepto ");
        sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal ");
        sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
        sql.append(" and c.sueldo_basico = 'S' ");
        sql.append(" and t.estatus = 'A'");
        stTrabajadores = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTrabajadores.setLong(1, idTipoPersonal);
        stTrabajadores.setDate(2, desdeFechaSql);
        stTrabajadores.setDate(3, hastaFechaSql);
        rsTrabajadores = stTrabajadores.executeQuery();
      }
      stActualizar = connection.createStatement();

      while (rsTrabajadores.next()) {
        stActualizar = connection.createStatement();

        montoAumento = 0.0D;
        sueldo = 0.0D;

        montoAumento = rsTrabajadores.getDouble("monto") * porcentajeAumento / 100.0D;
        sueldo = montoAumento + rsTrabajadores.getDouble("monto");
        if (tipoAumento.equals("1")) {
          sql = new StringBuffer();
          sql.append("Update conceptofijo set monto_anterior = monto, monto = " + sueldo + " where id_concepto_fijo = " + rsTrabajadores.getLong("id_concepto_fijo"));
          stActualizar.addBatch(sql.toString());
          switch (rsTrabajadores.getInt("cod_frecuencia_pago"))
          {
          case 1:
          case 2:
            break;
          case 3:
            sueldo *= 2.0D;
            break;
          case 4:
            sueldo /= 7.0D;
          }

          sql = new StringBuffer();
          sql.append("Update trabajador set sueldo_basico = " + sueldo + " where id_trabajador = " + rsTrabajadores.getLong("id_trabajador"));
          stActualizar.addBatch(sql.toString());
        } else {
          sql = new StringBuffer();
          sql.append("select cf.id_concepto_fijo, fp.cod_frecuencia_pago, cf.monto ");
          sql.append("from conceptofijo cf, conceptotipopersonal ctp, ");
          sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp ");
          sql.append(" where cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal ");
          sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal ");
          sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago ");
          sql.append(" and ctp.id_concepto_tipo_personal = ?");
          sql.append(" and cf.id_trabajador = ?");

          stOtroConcepto = connection.prepareStatement(
            sql.toString(), 
            1003, 
            1007);
          stOtroConcepto.setLong(1, idConcepto);
          stOtroConcepto.setLong(2, rsTrabajadores.getLong("id_trabajador"));
          rsOtroConcepto = stOtroConcepto.executeQuery();

          montoActualOtroConcepto = 0.0D;
          otroConceptoExiste = false;
          int codFrecuenciaPago;
          if (rsOtroConcepto.next()) {
            otroConceptoExiste = true;
            int codFrecuenciaPago = rsOtroConcepto.getInt("cod_frecuencia_pago");
            montoActualOtroConcepto = rsOtroConcepto.getDouble("monto");
            idConceptoFijoOtroConcepto = rsOtroConcepto.getLong("id_concepto_fijo");
          }
          else {
            codFrecuenciaPago = codFrecuenciaPagoConcepto;
          }

          switch (rsTrabajadores.getInt("cod_frecuencia_pago")) {
          case 1:
          case 2:
            if ((codFrecuenciaPago == 1) || 
              (codFrecuenciaPago == 2))
              montoOtroConcepto = montoActualOtroConcepto + montoAumento;
            else if (codFrecuenciaPago == 3) {
              montoOtroConcepto = montoActualOtroConcepto + montoAumento / 2.0D;
            }
            break;
          case 3:
            if ((codFrecuenciaPago == 1) || 
              (codFrecuenciaPago == 2))
              montoOtroConcepto = montoActualOtroConcepto + montoAumento * 2.0D;
            else if (codFrecuenciaPago == 3) {
              montoOtroConcepto = montoActualOtroConcepto + montoAumento;
            }
            break;
          case 4:
            if (codFrecuenciaPago == 4)
              montoOtroConcepto = montoActualOtroConcepto + montoAumento;
            else if (codFrecuenciaPago > 4) {
              montoOtroConcepto = (montoActualOtroConcepto + montoAumento / 7.0D) * 30.0D;
            }
            break;
          }

          if (otroConceptoExiste) {
            sql = new StringBuffer();
            sql.append("Update conceptofijo set monto_anterior = monto, monto = " + montoOtroConcepto + " where id_concepto_fijo = " + idConceptoFijoOtroConcepto);
            stActualizar.addBatch(sql.toString());
          } else {
            sql = new StringBuffer();
            sql.append("insert into conceptofijo (id_trabajador, id_frecuencia_tipo_personal, id_concepto_tipo_personal, monto, estatus, unidades, id_concepto_fijo) values(");
            sql.append(rsTrabajadores.getLong("id_trabajador") + ", " + idFrecuenciaTipoPersonalConcepto + ", " + idConcepto + ", " + montoOtroConcepto + ", 'A', 0.0, ");
            sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo") + ")");
            stActualizar.addBatch(sql.toString());
          }
        }

        if (retroactivo.equals("S"))
        {
          switch (rsTrabajadores.getInt("cod_frecuencia_pago")) {
          case 1:
          case 2:
            montoRetroactivo = montoAumento / 30.0D * diasRetroactivo;
            break;
          case 3:
            montoRetroactivo = montoAumento / 15.0D * diasRetroactivo;
            break;
          case 4:
            montoRetroactivo = montoAumento / 7.0D * diasRetroactivo;
          }

          sql = new StringBuffer();
          sql.append("insert into conceptovariable (id_trabajador, id_frecuencia_tipo_personal, id_concepto_tipo_personal, monto, estatus, unidades, id_concepto_variable) values(");
          sql.append(rsTrabajadores.getLong("id_trabajador") + ", " + idFrecuenciaTipoPersonalRetroactivo + ", " + idConceptoRetroactivo + ", " + montoRetroactivo + ", 'A', 0.0, ");
          sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable") + ")");
          stActualizar.addBatch(sql.toString());
        }

        stActualizar.executeBatch();
        stActualizar.close();
      }

      connection.commit();

      calcularSueldosPromedioBeanBusiness.reCalcularConceptosPortipoPersonal(idTipoPersonal);
    }
    finally {
      if (rsTrabajadores != null) try {
          rsTrabajadores.close();
        } catch (Exception localException) {
        } if (rsOtroConcepto != null) try {
          rsOtroConcepto.close();
        } catch (Exception localException1) {
        } if (stTrabajadores != null) try {
          stTrabajadores.close();
        } catch (Exception localException2) {
        } if (stOtroConcepto != null) try {
          stOtroConcepto.close();
        } catch (Exception localException3) {
        } if (stActualizar != null) try {
          stActualizar.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        } catch (Exception localException5) {  }
 
    }
    return true;
  }
}