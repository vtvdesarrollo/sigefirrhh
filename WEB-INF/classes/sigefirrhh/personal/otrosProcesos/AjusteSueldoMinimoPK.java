package sigefirrhh.personal.otrosProcesos;

import java.io.Serializable;

public class AjusteSueldoMinimoPK
  implements Serializable
{
  public long idAjusteSueldoMinimo;

  public AjusteSueldoMinimoPK()
  {
  }

  public AjusteSueldoMinimoPK(long idAjusteSueldoMinimo)
  {
    this.idAjusteSueldoMinimo = idAjusteSueldoMinimo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AjusteSueldoMinimoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AjusteSueldoMinimoPK thatPK)
  {
    return 
      this.idAjusteSueldoMinimo == thatPK.idAjusteSueldoMinimo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAjusteSueldoMinimo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAjusteSueldoMinimo);
  }
}