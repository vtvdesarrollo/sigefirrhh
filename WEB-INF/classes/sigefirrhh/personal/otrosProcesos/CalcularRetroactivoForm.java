package sigefirrhh.personal.otrosProcesos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CalcularRetroactivoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularRetroactivoForm.class.getName());
  private int tipoReporte;
  private String reportName;
  private int reportId;
  private String selectTipoPersonal;
  private String selectConceptoTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private OtrosProcesosNoGenFacade otrosProcesosFacade = new OtrosProcesosNoGenFacade();
  private long idTipoPersonal;
  private long idConceptoTipoPersonal;
  private long idFrecuenciaTipoPersonal;
  private Collection listTipoPersonal;
  private Collection listConceptoTipoPersonal;
  private Collection listFrecuenciaTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private String formato = "1";
  private boolean show;
  private Date fechaVigencia;
  private int dias;

  public boolean isShowConcepto()
  {
    return (this.listConceptoTipoPersonal != null) && (!this.listConceptoTipoPersonal.isEmpty());
  }
  public boolean isShowFrecuencia() {
    return (this.listFrecuenciaTipoPersonal != null) && (!this.listFrecuenciaTipoPersonal.isEmpty());
  }
  public CalcularRetroactivoForm() {
    this.reportName = "bonofinanio";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.fechaVigencia = new Date();
    this.dias = 1;
    refresh();
    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event) {
        CalcularRetroactivoForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    this.reportName = "";
    if (this.formato.equals("2"))
      this.reportName = "a_";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));

    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }

    JasperForWeb report = new JasperForWeb();
    if (this.formato.equals("2")) {
      report.setType(3);
    }
    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/otrosProcesos");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.listConceptoTipoPersonal = null;
      this.listFrecuenciaTipoPersonal = null;
      if (this.idTipoPersonal != 0L) {
        this.listConceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalByRetroactivo(this.idTipoPersonal, "S");
        this.listFrecuenciaTipoPersonal = this.definicionesFacade.findFrecuenciaTipoPersonalByTipoPersonal(this.idTipoPersonal);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeConceptoTipoPersonal(ValueChangeEvent event) {
    this.idConceptoTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.show = false;
      if (this.idConceptoTipoPersonal != 0L) {
        this.show = true;
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeFrecuenciaTipoPersonal(ValueChangeEvent event) {
    this.idFrecuenciaTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.show = false;
      if (this.idFrecuenciaTipoPersonal != 0L) {
        this.show = true;
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh() {
    try { this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador()); }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

      this.otrosProcesosFacade.calcularRetroactivo(this.idTipoPersonal, this.idConceptoTipoPersonal, 
        this.fechaVigencia, this.dias, tipoPersonal.getGrupoNomina().getPeriodicidad(), this.idFrecuenciaTipoPersonal);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListConceptoTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoTipoPersonal, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListFrecuenciaTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listFrecuenciaTipoPersonal, "sigefirrhh.base.definiciones.FrecuenciaTipoPersonal");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string)
  {
    this.selectTipoPersonal = string;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public boolean isShow() {
    return this.show;
  }
  public void setShow(boolean show) {
    this.show = show;
  }
  public int getDias() {
    return this.dias;
  }
  public void setDias(int dias) {
    this.dias = dias;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String selectConceptoTipoPersonal) {
    this.selectConceptoTipoPersonal = selectConceptoTipoPersonal;
  }
  public Date getFechaVigencia() {
    return this.fechaVigencia;
  }
  public void setFechaVigencia(Date fechaVigencia) {
    this.fechaVigencia = fechaVigencia;
  }
  public String getSelectFrecuenciaTipoPersonal() {
    return this.selectFrecuenciaTipoPersonal;
  }
  public void setSelectFrecuenciaTipoPersonal(String selectFrecuenciaTipoPersonal) {
    this.selectFrecuenciaTipoPersonal = selectFrecuenciaTipoPersonal;
  }
}