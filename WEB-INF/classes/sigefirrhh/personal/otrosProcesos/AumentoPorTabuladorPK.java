package sigefirrhh.personal.otrosProcesos;

import java.io.Serializable;

public class AumentoPorTabuladorPK
  implements Serializable
{
  public long idAumentoPorTabulador;

  public AumentoPorTabuladorPK()
  {
  }

  public AumentoPorTabuladorPK(long idAumentoPorTabulador)
  {
    this.idAumentoPorTabulador = idAumentoPorTabulador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AumentoPorTabuladorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AumentoPorTabuladorPK thatPK)
  {
    return 
      this.idAumentoPorTabulador == thatPK.idAumentoPorTabulador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAumentoPorTabulador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAumentoPorTabulador);
  }
}