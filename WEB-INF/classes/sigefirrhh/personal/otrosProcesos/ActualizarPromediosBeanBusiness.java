package sigefirrhh.personal.otrosProcesos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.tools.NumberTools;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class ActualizarPromediosBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(ActualizarPromediosBeanBusiness.class.getName());

  public void calcular(long idOrganismo, long idGrupoNomina, String periodicidad)
    throws Exception
  {
    Connection connection = null;

    ResultSet rsTrabajadores1 = null;
    PreparedStatement stTrabajadores1 = null;

    ResultSet rsPromedios = null;
    PreparedStatement stPromedios = null;
    Statement stSueldoPromedio = null;

    ResultSet rsTipoPersonal = null;
    PreparedStatement stTipoPersonal = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();

      sql.append("select t.id_trabajador, min(t.regimen) as regimen, min(tp.formula_semanal) as formula_semanal,");
      sql.append(" min(tp.formula_integral) as formula_integral, min(t.riesgo) as riesgo,  ");
      sql.append(" min(jornada_semanal) as jornada_semanal, min(tu.jornada_diaria) as jornada_diaria");
      sql.append(" from trabajador t, tipopersonal tp, conceptotipopersonal ctp, concepto c, turno tu ");
      sql.append(" where ");
      sql.append(" t.id_tipo_personal = tp.id_tipo_personal ");
      sql.append(" and t.id_turno = tu.id_turno");
      sql.append(" and t.estatus = 'A' ");
      sql.append(" and tp.id_tipo_personal = ctp.id_tipo_personal ");
      sql.append(" and ctp.id_concepto = c.id_concepto ");
      sql.append(" and c.cod_concepto in('5001', '5003', '5004') ");
      sql.append(" and tp.id_tipo_personal = ? ");
      sql.append(" group by  t.id_trabajador ");
      stTrabajadores1 = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.id_concepto_tipo_personal, fp.cod_frecuencia_pago, cf.unidades, cf.monto, ");
      sql.append(" c.sueldo_basico, c.sueldo_integral, c.compensacion, c.primas_trabajador, c.primas_cargo, c.ajuste_sueldo");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, frecuenciatipopersonal ftp, ");
      sql.append("  concepto c, frecuenciapago fp");
      sql.append(" where");
      sql.append("  cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and ctp.cod_concepto < '5000'");
      sql.append(" and cf.id_trabajador = ? ");
      sql.append(" and (c.sueldo_basico = 'S' ");
      sql.append(" or c.sueldo_integral = 'S' ");
      sql.append(" or c.compensacion = 'S' ");
      sql.append(" or c.primas_trabajador = 'S' ");
      sql.append(" or c.primas_cargo = 'S' ");
      sql.append(" or c.ajuste_sueldo= 'S') ");

      stPromedios = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      double montoBase1Si = 0.0D;
      double montoBase2Si = 0.0D;
      double montoBaseTotalSi = 0.0D;

      double montoBase1Sb = 0.0D;
      double montoBase2Sb = 0.0D;
      double montoBaseTotalSb = 0.0D;

      double montoBase1Co = 0.0D;
      double montoBase2Co = 0.0D;
      double montoBaseTotalCo = 0.0D;

      double montoBase1Pt = 0.0D;
      double montoBase2Pt = 0.0D;
      double montoBaseTotalPt = 0.0D;

      double montoBase1Pc = 0.0D;
      double montoBase2Pc = 0.0D;
      double montoBaseTotalPc = 0.0D;

      double montoBase1As = 0.0D;
      double montoBase2As = 0.0D;
      double montoBaseTotalAs = 0.0D;

      sql = new StringBuffer();
      try
      {
        sql = new StringBuffer();

        sql.append("select id_tipo_personal, id_grupo_organismo ");
        sql.append(" from tipopersonal");
        sql.append(" where ");
        sql.append(" id_grupo_nomina = ? ");

        stTipoPersonal = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stTipoPersonal.setLong(1, idGrupoNomina);
        rsTipoPersonal = stTipoPersonal.executeQuery();
        this.log.error("SI ESTA PASANDO 1");

        stSueldoPromedio = connection.createStatement();

        for (; rsTipoPersonal.next(); 
          rsTrabajadores1.next())
        {
          stTrabajadores1.setLong(1, rsTipoPersonal.getLong("id_tipo_personal"));
          rsTrabajadores1 = stTrabajadores1.executeQuery();

          continue;

          montoBase1Si = 0.0D;
          montoBase2Si = 0.0D;
          montoBaseTotalSi = 0.0D;

          montoBase1Sb = 0.0D;
          montoBase2Sb = 0.0D;
          montoBaseTotalSb = 0.0D;

          montoBase1Co = 0.0D;
          montoBase2Co = 0.0D;
          montoBaseTotalCo = 0.0D;

          montoBase1Pt = 0.0D;
          montoBase2Pt = 0.0D;
          montoBaseTotalPt = 0.0D;

          montoBase1Pc = 0.0D;
          montoBase2Pc = 0.0D;
          montoBaseTotalPc = 0.0D;

          montoBase1As = 0.0D;
          montoBase2As = 0.0D;
          montoBaseTotalAs = 0.0D;

          stPromedios.setLong(1, rsTrabajadores1.getLong("id_trabajador"));
          rsPromedios = stPromedios.executeQuery();

          while (rsPromedios.next()) {
            if (!periodicidad.equals("S")) {
              if (rsPromedios.getString("sueldo_integral").equals("S")) {
                if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
                  montoBase1Si += rsPromedios.getDouble("monto") * 2.0D;
                else {
                  montoBase2Si += rsPromedios.getDouble("monto");
                }
              }
              if (rsPromedios.getString("sueldo_basico").equals("S")) {
                if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
                  montoBase1Sb += rsPromedios.getDouble("monto") * 2.0D;
                else
                  montoBase2Sb += rsPromedios.getDouble("monto");
              }
              else if (rsPromedios.getString("compensacion").equals("S")) {
                if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
                  montoBase1Co += rsPromedios.getDouble("monto") * 2.0D;
                else
                  montoBase2Co += rsPromedios.getDouble("monto");
              }
              else if (rsPromedios.getString("primas_trabajador").equals("S")) {
                if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
                  montoBase1Pt += rsPromedios.getDouble("monto") * 2.0D;
                else
                  montoBase2Pt += rsPromedios.getDouble("monto");
              }
              else if (rsPromedios.getString("primas_cargo").equals("S")) {
                if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
                  montoBase1Pc += rsPromedios.getDouble("monto") * 2.0D;
                else
                  montoBase2Pc += rsPromedios.getDouble("monto");
              }
              else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
                if (rsPromedios.getInt("cod_frecuencia_pago") == 3)
                  montoBase1As += rsPromedios.getDouble("monto") * 2.0D;
                else {
                  montoBase2As += rsPromedios.getDouble("monto");
                }
              }

            }
            else if (rsPromedios.getInt("cod_frecuencia_pago") == 4) {
              if (rsPromedios.getString("sueldo_integral").equals("S")) {
                montoBase1Si += rsPromedios.getDouble("monto");
              }
              if (rsPromedios.getString("sueldo_basico").equals("S"))
                montoBase1Sb += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("compensacion").equals("S"))
                montoBase1Co += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("primas_trabajador").equals("S"))
                montoBase1Pt += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("primas_cargo").equals("S"))
                montoBase1Pc += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("ajuste_sueldo").equals("S"))
                montoBase1As += rsPromedios.getDouble("monto");
            }
            else {
              if (rsPromedios.getString("sueldo_integral").equals("S")) {
                montoBase2Si += rsPromedios.getDouble("monto");
              }
              if (rsPromedios.getString("sueldo_basico").equals("S"))
                montoBase2Sb += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("compensacion").equals("S"))
                montoBase2Co += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("primas_trabajador").equals("S"))
                montoBase2Pt += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("primas_cargo").equals("S"))
                montoBase2Pc += rsPromedios.getDouble("monto");
              else if (rsPromedios.getString("ajuste_sueldo").equals("S")) {
                montoBase2As += rsPromedios.getDouble("monto");
              }

            }

          }

          if (periodicidad.equals("S")) {
            if (rsTrabajadores1.getString("formula_integral").equals("1")) {
              montoBaseTotalSi = (montoBase1Si / 7.0D + montoBase2Si / 30.0D) * 365.0D / 12.0D;
              montoBaseTotalSb = (montoBase1Sb / 7.0D + montoBase2Sb / 30.0D) * 365.0D / 12.0D;
              montoBaseTotalCo = (montoBase1Co / 7.0D + montoBase2Co / 30.0D) * 365.0D / 12.0D;
              montoBaseTotalPt = (montoBase1Pt / 7.0D + montoBase2Pt / 30.0D) * 365.0D / 12.0D;
              montoBaseTotalPc = (montoBase1Pc / 7.0D + montoBase2Pc / 30.0D) * 365.0D / 12.0D;
              montoBaseTotalAs = (montoBase1As / 7.0D + montoBase2As / 30.0D) * 365.0D / 12.0D;
            } else if (rsTrabajadores1.getString("formula_integral").equals("2")) {
              montoBaseTotalSi = montoBase1Si / 7.0D * 30.0D + montoBase2Si;
              montoBaseTotalSb = montoBase1Sb / 7.0D * 30.0D + montoBase2Sb;
              montoBaseTotalCo = montoBase1Co / 7.0D * 30.0D + montoBase2Co;
              montoBaseTotalPt = montoBase1Pt / 7.0D * 30.0D + montoBase2Pt;
              montoBaseTotalPc = montoBase1Pc / 7.0D * 30.0D + montoBase2Pc;
              montoBaseTotalAs = montoBase1As / 7.0D * 30.0D + montoBase2As;
            } else if (rsTrabajadores1.getString("formula_integral").equals("3")) {
              montoBaseTotalSi = montoBase1Si * 52.0D / 12.0D + montoBase2Si;
              montoBaseTotalSb = montoBase1Sb * 52.0D / 12.0D + montoBase2Sb;
              montoBaseTotalCo = montoBase1Co * 52.0D / 12.0D + montoBase2Co;
              montoBaseTotalPt = montoBase1Pt * 52.0D / 12.0D + montoBase2Pt;
              montoBaseTotalPc = montoBase1Pc * 52.0D / 12.0D + montoBase2Pc;
              montoBaseTotalAs = montoBase1As * 52.0D / 12.0D + montoBase2As;
            }
          } else {
            montoBaseTotalSi = montoBase1Si + montoBase2Si;
            montoBaseTotalSb = montoBase1Sb + montoBase2Sb;
            montoBaseTotalCo = montoBase1Co + montoBase2Co;
            montoBaseTotalPt = montoBase1Pt + montoBase2Pt;
            montoBaseTotalPc = montoBase1Pc + montoBase2Pc;
            montoBaseTotalAs = montoBase1As + montoBase2As;
          }

          sql = new StringBuffer();
          sql.append("UPDATE sueldoPromedio SET ");
          sql.append("promedio_integral = ");
          sql.append(NumberTools.twoDecimal(montoBaseTotalSi));
          sql.append(",promedio_sueldo = ");
          sql.append(NumberTools.twoDecimal(montoBaseTotalSb));
          sql.append(",promedio_compensacion = ");
          sql.append(NumberTools.twoDecimal(montoBaseTotalCo));
          sql.append(",promedio_primast = ");
          sql.append(NumberTools.twoDecimal(montoBaseTotalPt));
          sql.append(",promedio_primasc = ");
          sql.append(NumberTools.twoDecimal(montoBaseTotalPc));
          sql.append(",promedio_ajustes = ");
          sql.append(NumberTools.twoDecimal(montoBaseTotalAs));
          sql.append(" WHERE id_trabajador = ");
          sql.append(rsTrabajadores1.getLong("id_trabajador"));

          stSueldoPromedio.addBatch(sql.toString());
        }

        stSueldoPromedio.executeBatch();
        connection.commit();
      }
      catch (Exception e)
      {
        this.log.error("Excepcion controlada:", e);
      }
    } finally {
      if (rsTrabajadores1 != null) try {
          rsTrabajadores1.close();
        } catch (Exception localException1) {
        } if (rsPromedios != null) try {
          rsPromedios.close();
        } catch (Exception localException2) {
        } if (rsTipoPersonal != null) try {
          rsTipoPersonal.close();
        } catch (Exception localException3) {
        } if (stTrabajadores1 != null) try {
          stTrabajadores1.close();
        } catch (Exception localException4) {
        } if (stPromedios != null) try {
          stPromedios.close();
        } catch (Exception localException5) {
        } if (stTipoPersonal != null) try {
          stTipoPersonal.close();
        } catch (Exception localException6) {
        } if (stSueldoPromedio != null) try {
          stSueldoPromedio.close();
        } catch (Exception localException7) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException8)
        {
        }
    }
  }
}