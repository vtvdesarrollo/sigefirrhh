package sigefirrhh.personal.otrosProcesos;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class OtrosProcesosFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private OtrosProcesosBusiness otrosProcesosBusiness = new OtrosProcesosBusiness();

  public void addCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.otrosProcesosBusiness.addCalculoBonoFinAnio(calculoBonoFinAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio) throws Exception
  {
    try { this.txn.open();
      this.otrosProcesosBusiness.updateCalculoBonoFinAnio(calculoBonoFinAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio) throws Exception
  {
    try { this.txn.open();
      this.otrosProcesosBusiness.deleteCalculoBonoFinAnio(calculoBonoFinAnio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CalculoBonoFinAnio findCalculoBonoFinAnioById(long calculoBonoFinAnioId) throws Exception
  {
    try { this.txn.open();
      CalculoBonoFinAnio calculoBonoFinAnio = 
        this.otrosProcesosBusiness.findCalculoBonoFinAnioById(calculoBonoFinAnioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(calculoBonoFinAnio);
      return calculoBonoFinAnio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCalculoBonoFinAnio() throws Exception
  {
    try { this.txn.open();
      return this.otrosProcesosBusiness.findAllCalculoBonoFinAnio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCalculoBonoFinAnioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.otrosProcesosBusiness.findCalculoBonoFinAnioByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}