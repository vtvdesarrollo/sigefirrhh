package sigefirrhh.personal.otrosProcesos;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CalcularBonoPetroleroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularBonoPetroleroForm.class.getName());
  private int tipoReporte;
  private String reportName;
  private int reportId;
  private OtrosProcesosNoGenFacade otrosProcesosFacade = new OtrosProcesosNoGenFacade();
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private String formato = "1";
  private Date fecha = new Date();
  private Date fechaEgresado = new Date();

  public CalcularBonoPetroleroForm() {
    log.error(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
    this.reportName = "bonofinanio";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.definicionesFacade = new DefinicionesNoGenFacade();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event) {
        CalcularBonoPetroleroForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    this.reportName = "";
    if (this.formato.equals("2"))
      this.reportName = "a_";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));

    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }

    JasperForWeb report = new JasperForWeb();
    if (this.formato.equals("2")) {
      report.setType(3);
    }
    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/otrosProcesos");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public void refresh()
  {
    try
    {
      log.error("2,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      log.error("size,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, " + this.listTipoPersonal.size());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.fecha == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de corte de ingresos", ""));
        return null;
      }
      if (this.fechaEgresado == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de corte de personal egresado", ""));
        return null;
      }

      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

      this.otrosProcesosFacade.calcularBonoPetrolero(this.idTipoPersonal, this.fecha, this.fechaEgresado);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getTipoReporte() {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i) {
    this.tipoReporte = i;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String string) {
    this.reportName = string;
  }

  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public Date getFecha() {
    return this.fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public Date getFechaEgresado()
  {
    return this.fechaEgresado;
  }
  public void setFechaEgresado(Date fechaEgresado) {
    this.fechaEgresado = fechaEgresado;
  }
}