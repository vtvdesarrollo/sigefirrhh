package sigefirrhh.personal.otrosProcesos;

import java.io.Serializable;

public class ProcesoDiarioPK
  implements Serializable
{
  public long idProcesoDiario;

  public ProcesoDiarioPK()
  {
  }

  public ProcesoDiarioPK(long idProcesoDiario)
  {
    this.idProcesoDiario = idProcesoDiario;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProcesoDiarioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProcesoDiarioPK thatPK)
  {
    return 
      this.idProcesoDiario == thatPK.idProcesoDiario;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProcesoDiario)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProcesoDiario);
  }
}