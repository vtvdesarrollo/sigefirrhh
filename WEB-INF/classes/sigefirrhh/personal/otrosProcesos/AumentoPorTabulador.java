package sigefirrhh.personal.otrosProcesos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class AumentoPorTabulador
  implements Serializable, PersistenceCapable
{
  private long idAumentoPorTabulador;
  private int gradoActual;
  private int pasoActual;
  private double sueldoActual;
  private double otrosSueldo;
  private double compensacionActual;
  private double sueldoNuevo;
  private double pasoNuevo;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "compensacionActual", "gradoActual", "idAumentoPorTabulador", "otrosSueldo", "pasoActual", "pasoNuevo", "sueldoActual", "sueldoNuevo", "trabajador" };
  private static final Class[] jdoFieldTypes = { Double.TYPE, Integer.TYPE, Long.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public double getCompensacionActual()
  {
    return jdoGetcompensacionActual(this);
  }

  public int getGradoActual()
  {
    return jdoGetgradoActual(this);
  }

  public long getIdAumentoPorTabulador()
  {
    return jdoGetidAumentoPorTabulador(this);
  }

  public double getOtrosSueldo()
  {
    return jdoGetotrosSueldo(this);
  }

  public int getPasoActual()
  {
    return jdoGetpasoActual(this);
  }

  public double getPasoNuevo()
  {
    return jdoGetpasoNuevo(this);
  }

  public double getSueldoActual()
  {
    return jdoGetsueldoActual(this);
  }

  public double getSueldoNuevo()
  {
    return jdoGetsueldoNuevo(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setCompensacionActual(double d)
  {
    jdoSetcompensacionActual(this, d);
  }

  public void setGradoActual(int i)
  {
    jdoSetgradoActual(this, i);
  }

  public void setIdAumentoPorTabulador(long l)
  {
    jdoSetidAumentoPorTabulador(this, l);
  }

  public void setOtrosSueldo(double d)
  {
    jdoSetotrosSueldo(this, d);
  }

  public void setPasoActual(int i)
  {
    jdoSetpasoActual(this, i);
  }

  public void setPasoNuevo(double d)
  {
    jdoSetpasoNuevo(this, d);
  }

  public void setSueldoActual(double d)
  {
    jdoSetsueldoActual(this, d);
  }

  public void setSueldoNuevo(double d)
  {
    jdoSetsueldoNuevo(this, d);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.otrosProcesos.AumentoPorTabulador"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AumentoPorTabulador());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AumentoPorTabulador localAumentoPorTabulador = new AumentoPorTabulador();
    localAumentoPorTabulador.jdoFlags = 1;
    localAumentoPorTabulador.jdoStateManager = paramStateManager;
    return localAumentoPorTabulador;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AumentoPorTabulador localAumentoPorTabulador = new AumentoPorTabulador();
    localAumentoPorTabulador.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAumentoPorTabulador.jdoFlags = 1;
    localAumentoPorTabulador.jdoStateManager = paramStateManager;
    return localAumentoPorTabulador;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacionActual);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.gradoActual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAumentoPorTabulador);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.otrosSueldo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.pasoActual);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.pasoNuevo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoActual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoNuevo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacionActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gradoActual = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAumentoPorTabulador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otrosSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pasoActual = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pasoNuevo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoNuevo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AumentoPorTabulador paramAumentoPorTabulador, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.compensacionActual = paramAumentoPorTabulador.compensacionActual;
      return;
    case 1:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.gradoActual = paramAumentoPorTabulador.gradoActual;
      return;
    case 2:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.idAumentoPorTabulador = paramAumentoPorTabulador.idAumentoPorTabulador;
      return;
    case 3:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.otrosSueldo = paramAumentoPorTabulador.otrosSueldo;
      return;
    case 4:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.pasoActual = paramAumentoPorTabulador.pasoActual;
      return;
    case 5:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.pasoNuevo = paramAumentoPorTabulador.pasoNuevo;
      return;
    case 6:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoActual = paramAumentoPorTabulador.sueldoActual;
      return;
    case 7:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoNuevo = paramAumentoPorTabulador.sueldoNuevo;
      return;
    case 8:
      if (paramAumentoPorTabulador == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramAumentoPorTabulador.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AumentoPorTabulador))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AumentoPorTabulador localAumentoPorTabulador = (AumentoPorTabulador)paramObject;
    if (localAumentoPorTabulador.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAumentoPorTabulador, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AumentoPorTabuladorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AumentoPorTabuladorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AumentoPorTabuladorPK))
      throw new IllegalArgumentException("arg1");
    AumentoPorTabuladorPK localAumentoPorTabuladorPK = (AumentoPorTabuladorPK)paramObject;
    localAumentoPorTabuladorPK.idAumentoPorTabulador = this.idAumentoPorTabulador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AumentoPorTabuladorPK))
      throw new IllegalArgumentException("arg1");
    AumentoPorTabuladorPK localAumentoPorTabuladorPK = (AumentoPorTabuladorPK)paramObject;
    this.idAumentoPorTabulador = localAumentoPorTabuladorPK.idAumentoPorTabulador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AumentoPorTabuladorPK))
      throw new IllegalArgumentException("arg2");
    AumentoPorTabuladorPK localAumentoPorTabuladorPK = (AumentoPorTabuladorPK)paramObject;
    localAumentoPorTabuladorPK.idAumentoPorTabulador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AumentoPorTabuladorPK))
      throw new IllegalArgumentException("arg2");
    AumentoPorTabuladorPK localAumentoPorTabuladorPK = (AumentoPorTabuladorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAumentoPorTabuladorPK.idAumentoPorTabulador);
  }

  private static final double jdoGetcompensacionActual(AumentoPorTabulador paramAumentoPorTabulador)
  {
    if (paramAumentoPorTabulador.jdoFlags <= 0)
      return paramAumentoPorTabulador.compensacionActual;
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.compensacionActual;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 0))
      return paramAumentoPorTabulador.compensacionActual;
    return localStateManager.getDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 0, paramAumentoPorTabulador.compensacionActual);
  }

  private static final void jdoSetcompensacionActual(AumentoPorTabulador paramAumentoPorTabulador, double paramDouble)
  {
    if (paramAumentoPorTabulador.jdoFlags == 0)
    {
      paramAumentoPorTabulador.compensacionActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.compensacionActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 0, paramAumentoPorTabulador.compensacionActual, paramDouble);
  }

  private static final int jdoGetgradoActual(AumentoPorTabulador paramAumentoPorTabulador)
  {
    if (paramAumentoPorTabulador.jdoFlags <= 0)
      return paramAumentoPorTabulador.gradoActual;
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.gradoActual;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 1))
      return paramAumentoPorTabulador.gradoActual;
    return localStateManager.getIntField(paramAumentoPorTabulador, jdoInheritedFieldCount + 1, paramAumentoPorTabulador.gradoActual);
  }

  private static final void jdoSetgradoActual(AumentoPorTabulador paramAumentoPorTabulador, int paramInt)
  {
    if (paramAumentoPorTabulador.jdoFlags == 0)
    {
      paramAumentoPorTabulador.gradoActual = paramInt;
      return;
    }
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.gradoActual = paramInt;
      return;
    }
    localStateManager.setIntField(paramAumentoPorTabulador, jdoInheritedFieldCount + 1, paramAumentoPorTabulador.gradoActual, paramInt);
  }

  private static final long jdoGetidAumentoPorTabulador(AumentoPorTabulador paramAumentoPorTabulador)
  {
    return paramAumentoPorTabulador.idAumentoPorTabulador;
  }

  private static final void jdoSetidAumentoPorTabulador(AumentoPorTabulador paramAumentoPorTabulador, long paramLong)
  {
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.idAumentoPorTabulador = paramLong;
      return;
    }
    localStateManager.setLongField(paramAumentoPorTabulador, jdoInheritedFieldCount + 2, paramAumentoPorTabulador.idAumentoPorTabulador, paramLong);
  }

  private static final double jdoGetotrosSueldo(AumentoPorTabulador paramAumentoPorTabulador)
  {
    if (paramAumentoPorTabulador.jdoFlags <= 0)
      return paramAumentoPorTabulador.otrosSueldo;
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.otrosSueldo;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 3))
      return paramAumentoPorTabulador.otrosSueldo;
    return localStateManager.getDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 3, paramAumentoPorTabulador.otrosSueldo);
  }

  private static final void jdoSetotrosSueldo(AumentoPorTabulador paramAumentoPorTabulador, double paramDouble)
  {
    if (paramAumentoPorTabulador.jdoFlags == 0)
    {
      paramAumentoPorTabulador.otrosSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.otrosSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 3, paramAumentoPorTabulador.otrosSueldo, paramDouble);
  }

  private static final int jdoGetpasoActual(AumentoPorTabulador paramAumentoPorTabulador)
  {
    if (paramAumentoPorTabulador.jdoFlags <= 0)
      return paramAumentoPorTabulador.pasoActual;
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.pasoActual;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 4))
      return paramAumentoPorTabulador.pasoActual;
    return localStateManager.getIntField(paramAumentoPorTabulador, jdoInheritedFieldCount + 4, paramAumentoPorTabulador.pasoActual);
  }

  private static final void jdoSetpasoActual(AumentoPorTabulador paramAumentoPorTabulador, int paramInt)
  {
    if (paramAumentoPorTabulador.jdoFlags == 0)
    {
      paramAumentoPorTabulador.pasoActual = paramInt;
      return;
    }
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.pasoActual = paramInt;
      return;
    }
    localStateManager.setIntField(paramAumentoPorTabulador, jdoInheritedFieldCount + 4, paramAumentoPorTabulador.pasoActual, paramInt);
  }

  private static final double jdoGetpasoNuevo(AumentoPorTabulador paramAumentoPorTabulador)
  {
    if (paramAumentoPorTabulador.jdoFlags <= 0)
      return paramAumentoPorTabulador.pasoNuevo;
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.pasoNuevo;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 5))
      return paramAumentoPorTabulador.pasoNuevo;
    return localStateManager.getDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 5, paramAumentoPorTabulador.pasoNuevo);
  }

  private static final void jdoSetpasoNuevo(AumentoPorTabulador paramAumentoPorTabulador, double paramDouble)
  {
    if (paramAumentoPorTabulador.jdoFlags == 0)
    {
      paramAumentoPorTabulador.pasoNuevo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.pasoNuevo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 5, paramAumentoPorTabulador.pasoNuevo, paramDouble);
  }

  private static final double jdoGetsueldoActual(AumentoPorTabulador paramAumentoPorTabulador)
  {
    if (paramAumentoPorTabulador.jdoFlags <= 0)
      return paramAumentoPorTabulador.sueldoActual;
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.sueldoActual;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 6))
      return paramAumentoPorTabulador.sueldoActual;
    return localStateManager.getDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 6, paramAumentoPorTabulador.sueldoActual);
  }

  private static final void jdoSetsueldoActual(AumentoPorTabulador paramAumentoPorTabulador, double paramDouble)
  {
    if (paramAumentoPorTabulador.jdoFlags == 0)
    {
      paramAumentoPorTabulador.sueldoActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.sueldoActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 6, paramAumentoPorTabulador.sueldoActual, paramDouble);
  }

  private static final double jdoGetsueldoNuevo(AumentoPorTabulador paramAumentoPorTabulador)
  {
    if (paramAumentoPorTabulador.jdoFlags <= 0)
      return paramAumentoPorTabulador.sueldoNuevo;
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.sueldoNuevo;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 7))
      return paramAumentoPorTabulador.sueldoNuevo;
    return localStateManager.getDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 7, paramAumentoPorTabulador.sueldoNuevo);
  }

  private static final void jdoSetsueldoNuevo(AumentoPorTabulador paramAumentoPorTabulador, double paramDouble)
  {
    if (paramAumentoPorTabulador.jdoFlags == 0)
    {
      paramAumentoPorTabulador.sueldoNuevo = paramDouble;
      return;
    }
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.sueldoNuevo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAumentoPorTabulador, jdoInheritedFieldCount + 7, paramAumentoPorTabulador.sueldoNuevo, paramDouble);
  }

  private static final Trabajador jdoGettrabajador(AumentoPorTabulador paramAumentoPorTabulador)
  {
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
      return paramAumentoPorTabulador.trabajador;
    if (localStateManager.isLoaded(paramAumentoPorTabulador, jdoInheritedFieldCount + 8))
      return paramAumentoPorTabulador.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramAumentoPorTabulador, jdoInheritedFieldCount + 8, paramAumentoPorTabulador.trabajador);
  }

  private static final void jdoSettrabajador(AumentoPorTabulador paramAumentoPorTabulador, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramAumentoPorTabulador.jdoStateManager;
    if (localStateManager == null)
    {
      paramAumentoPorTabulador.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramAumentoPorTabulador, jdoInheritedFieldCount + 8, paramAumentoPorTabulador.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}