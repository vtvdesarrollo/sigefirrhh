package sigefirrhh.personal.otrosProcesos;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class OtrosProcesosBusiness extends AbstractBusiness
  implements Serializable
{
  private CalculoBonoFinAnioBeanBusiness calculoBonoFinAnioBeanBusiness = new CalculoBonoFinAnioBeanBusiness();

  public void addCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio)
    throws Exception
  {
    this.calculoBonoFinAnioBeanBusiness.addCalculoBonoFinAnio(calculoBonoFinAnio);
  }

  public void updateCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio) throws Exception {
    this.calculoBonoFinAnioBeanBusiness.updateCalculoBonoFinAnio(calculoBonoFinAnio);
  }

  public void deleteCalculoBonoFinAnio(CalculoBonoFinAnio calculoBonoFinAnio) throws Exception {
    this.calculoBonoFinAnioBeanBusiness.deleteCalculoBonoFinAnio(calculoBonoFinAnio);
  }

  public CalculoBonoFinAnio findCalculoBonoFinAnioById(long calculoBonoFinAnioId) throws Exception {
    return this.calculoBonoFinAnioBeanBusiness.findCalculoBonoFinAnioById(calculoBonoFinAnioId);
  }

  public Collection findAllCalculoBonoFinAnio() throws Exception {
    return this.calculoBonoFinAnioBeanBusiness.findCalculoBonoFinAnioAll();
  }

  public Collection findCalculoBonoFinAnioByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.calculoBonoFinAnioBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }
}