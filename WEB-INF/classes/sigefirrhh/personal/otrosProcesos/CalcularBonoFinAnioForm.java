package sigefirrhh.personal.otrosProcesos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CalcularBonoFinAnioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularBonoFinAnioForm.class.getName());
  private int tipoReporte;
  private String reportName;
  private int reportId;
  private String selectProceso;
  private String selectTipoPersonal;
  private Date inicio;
  private Date fin;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private OtrosProcesosNoGenFacade otrosProcesosFacade = new OtrosProcesosNoGenFacade();
  private Calendar inicioAux;
  private Calendar finAux;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private String formato = "1";
  private boolean show;
  private boolean showGenerate = true;
  private boolean auxShow;
  private int idUnidadAdministradora = 0;
  private String estatus;
  private Collection listUnidadAdministradora;

  public CalcularBonoFinAnioForm()
  {
    this.reportName = "bonofinanio";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.inicio = null;
    this.fin = null;

    this.fin = new Date(new Date().getYear(), 11, 31);
    this.inicio = new Date(new Date().getYear(), 0, 1);

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event) {
        CalcularBonoFinAnioForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }
    if (this.idUnidadAdministradora == 0) {
      if (this.tipoReporte == 1)
        this.reportName += "bonofinanio";
      else if (this.tipoReporte == 2) {
        this.reportName += "bonofinaniouel";
      }
    }
    else if (this.tipoReporte == 1)
      this.reportName += "bonofinanioua";
    else if (this.tipoReporte == 2)
      this.reportName += "bonofinaniouelua";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
    Date fec_ini = new Date(this.inicio.getYear(), this.inicio.getMonth(), this.inicio.getDate());
    Date fec_fin = new Date(this.fin.getYear(), this.fin.getMonth(), this.fin.getDate());
    parameters.put("fec_ini", fec_ini);
    parameters.put("fec_fin", fec_fin);
    if (this.idUnidadAdministradora != 0) {
      parameters.put("id_unidad_administradora", new Long(this.idUnidadAdministradora));
    }

    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }
    if (this.idUnidadAdministradora == 0) {
      if (this.tipoReporte == 1)
        this.reportName += "bonofinanio";
      else if (this.tipoReporte == 2) {
        this.reportName += "bonofinaniouel";
      }
    }
    else if (this.tipoReporte == 1)
      this.reportName += "bonofinanioua";
    else if (this.tipoReporte == 2) {
      this.reportName += "bonofinaniouelua";
    }

    JasperForWeb report = new JasperForWeb();
    if (this.formato.equals("2")) {
      report.setType(3);
    }
    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/otrosProcesos");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    try
    {
      this.auxShow = true;
      this.showGenerate = true;
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void refresh() {
    try {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listUnidadAdministradora = this.estructuraFacade.findUnidadAdministradoraByOrganismo(this.login.getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String generate() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);

      this.otrosProcesosFacade.calcularBonoFinAnio(this.idTipoPersonal, this.inicio, this.fin, this.selectProceso, this.estatus, this.login.getUsuario());

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);
      if (getSelectProceso().equalsIgnoreCase("2")) {
        this.showGenerate = false;
      }
      context.addMessage("success_add", new FacesMessage("Se generó con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListUnidadAdministradora()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listUnidadAdministradora, "sigefirrhh.base.estructura.UnidadAdministradora");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string)
  {
    this.selectTipoPersonal = string;
  }

  public Date getFin()
  {
    return this.fin;
  }

  public Date getInicio()
  {
    return this.inicio;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public String getSelectProceso()
  {
    return this.selectProceso;
  }

  public void setSelectProceso(String string)
  {
    this.selectProceso = string;
  }

  public void setFin(Date date)
  {
    this.fin = date;
  }

  public void setInicio(Date date)
  {
    this.inicio = date;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public int getIdUnidadAdministradora() {
    return this.idUnidadAdministradora;
  }
  public void setIdUnidadAdministradora(int idUnidadAdministradora) {
    this.idUnidadAdministradora = idUnidadAdministradora;
  }
  public String getEstatus() {
    return this.estatus;
  }
  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public boolean isShowGenerate() {
    return this.showGenerate;
  }
  public void setShowGenerate(boolean showGenerate) {
    this.showGenerate = showGenerate;
  }
}