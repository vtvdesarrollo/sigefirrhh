package sigefirrhh.personal.otrosProcesos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class SuspenderTrabajadoresForm
  implements Serializable
{
  static Logger log = Logger.getLogger(SuspenderTrabajadoresForm.class.getName());
  private String idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private LoginSession login;
  private OtrosProcesosNoGenFacade otrosProcesosNoGenFacade = new OtrosProcesosNoGenFacade();
  private boolean show;
  private boolean show2;

  public SuspenderTrabajadoresForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String preGenerate() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.show2 = true;
    this.show = false;
    return null;
  }
  public String generate() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      TipoPersonal tipoPersonal = this.definicionesFacade.findTipoPersonalById(Long.valueOf(String.valueOf(this.idTipoPersonal)).longValue());

      int valor = this.otrosProcesosNoGenFacade.suspenderTrabajadores(Long.valueOf(String.valueOf(this.idTipoPersonal)).longValue());

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', tipoPersonal);

      context.addMessage("success_add", new FacesMessage("Se generó con éxito. Se actualizaron " + valor + " trabajadores"));
      this.show = false;
      this.show2 = false;
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.idTipoPersonal = String.valueOf(idTipoPersonal);
    try
    {
      this.show = false;
      if (idTipoPersonal != 0L) {
        this.show = true;
      }
    }
    catch (Exception e)
    {
      this.show = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public String abort() { this.show = true;
    this.show2 = false;
    return null; }

  public String getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(String idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public boolean isShow() {
    return this.show;
  }

  public boolean isShow2()
  {
    return this.show2;
  }

  public void setShow2(boolean show2)
  {
    this.show2 = show2;
  }
}