package sigefirrhh.personal.otrosProcesos;

import java.io.Serializable;

public class CalculoBonoFinAnioPK
  implements Serializable
{
  public long idCalculoBonoFinAnio;

  public CalculoBonoFinAnioPK()
  {
  }

  public CalculoBonoFinAnioPK(long idCalculoBonoFinAnio)
  {
    this.idCalculoBonoFinAnio = idCalculoBonoFinAnio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CalculoBonoFinAnioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CalculoBonoFinAnioPK thatPK)
  {
    return 
      this.idCalculoBonoFinAnio == thatPK.idCalculoBonoFinAnio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCalculoBonoFinAnio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCalculoBonoFinAnio);
  }
}