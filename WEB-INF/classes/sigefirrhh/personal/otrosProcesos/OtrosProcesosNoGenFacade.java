package sigefirrhh.personal.otrosProcesos;

import eforserver.business.AbstractFacade;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Date;

public class OtrosProcesosNoGenFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private OtrosProcesosNoGenBusiness otrosProcesosNoGenBusiness = new OtrosProcesosNoGenBusiness();

  public void procesoAumentoPorPorcentaje(long idTipoPersonal, String criterio, long idRegion, int desdeGrado, int hastaGrado, Date desdeFecha, Date hastaFecha, String retroactivo, long idConceptoRetroactivo, long idFrecuenciaTipoPersonalRetroactivo, int diasRetroactivo, String tipoAumento, long idConcepto, double porcentajeAumento, long idFrecuenciaTipoPersonalConcepto, int codFrecuenciaPagoConcepto)
    throws Exception
  {
    this.otrosProcesosNoGenBusiness.procesoAumentoPorPorcentaje(idTipoPersonal, criterio, idRegion, desdeGrado, hastaGrado, desdeFecha, hastaFecha, retroactivo, idConceptoRetroactivo, idFrecuenciaTipoPersonalRetroactivo, diasRetroactivo, tipoAumento, idConcepto, porcentajeAumento, idFrecuenciaTipoPersonalConcepto, codFrecuenciaPagoConcepto);
  }

  public void actualizarPromedios(long idOrganismo, long idGrupoNomina, String periodicidad) throws Exception
  {
    this.otrosProcesosNoGenBusiness.actualizarPromedios(idOrganismo, idGrupoNomina, periodicidad);
  }

  public void calcularBonoFinAnio(long idTipoPersonal, Date fechaInicio, Date fechaFin, String proceso, String estatus, String usuario) throws Exception {
    this.otrosProcesosNoGenBusiness.calcularBonoFinAnio(idTipoPersonal, fechaInicio, fechaFin, proceso, estatus, usuario);
  }
  public void procesoAumentoPorEvaluacion(long idTipoPersonal, int anio, int mes, String proceso, String usuario, String periodicidad, String metodo, String registro, String grabar, Date fecha, long idConceptoTipoPersonal, String pagarRetroactivo, int diasRetroactivo, long idConceptoRetroactivo, long idFrecuenciaRetroactivo) throws Exception {
    this.otrosProcesosNoGenBusiness.procesoAumentoPorEvaluacion(idTipoPersonal, anio, mes, proceso, usuario, periodicidad, metodo, registro, grabar, fecha, idConceptoTipoPersonal, pagarRetroactivo, diasRetroactivo, idConceptoRetroactivo, idFrecuenciaRetroactivo);
  }

  public void calcularRetroactivo(long idTipoPersonal, long idConceptoTipoPersonal, Date fechaVigencia, int dias, String periodicidad, long idFrecuenciaTipoPersonal) throws Exception
  {
    this.otrosProcesosNoGenBusiness.calcularRetroactivo(idTipoPersonal, idConceptoTipoPersonal, 
      fechaVigencia, dias, periodicidad, idFrecuenciaTipoPersonal);
  }

  public int suspenderTrabajadores(long idTipoPersonal) throws Exception {
    return this.otrosProcesosNoGenBusiness.suspenderTrabajadores(idTipoPersonal);
  }

  public int desactivarFeVida(long idTipoPersonal) throws Exception {
    return this.otrosProcesosNoGenBusiness.desactivarFeVida(idTipoPersonal);
  }

  public void procesoAumentoPorTabulador(long idTipoPersonal, long idTabuladorActual, long idTabuladorNuevo, String proceso, String usuario, String mantenerPaso, String periodicidad) throws Exception {
    this.otrosProcesosNoGenBusiness.procesoAumentoPorTabulador(idTipoPersonal, idTabuladorActual, idTabuladorNuevo, 
      proceso, usuario, mantenerPaso, periodicidad);
  }

  public void calcularBonoPetrolero(long idTipoPersonal, Date fecha, Date fechaEgresado) throws Exception {
    this.otrosProcesosNoGenBusiness.calcularBonoPetrolero(idTipoPersonal, fecha, fechaEgresado);
  }
}