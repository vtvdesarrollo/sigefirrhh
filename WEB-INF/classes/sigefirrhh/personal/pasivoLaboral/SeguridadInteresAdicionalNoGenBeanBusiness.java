package sigefirrhh.personal.pasivoLaboral;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SeguridadInteresAdicionalNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public SeguridadInteresAdicional findLast(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(SeguridadInteresAdicional.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio descending, mes descending");

    Collection colSeguridadInteresAdicional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadInteresAdicional);

    return (SeguridadInteresAdicional)colSeguridadInteresAdicional.iterator().next();
  }
}