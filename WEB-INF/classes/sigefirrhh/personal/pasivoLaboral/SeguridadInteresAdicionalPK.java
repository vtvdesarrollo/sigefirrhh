package sigefirrhh.personal.pasivoLaboral;

import java.io.Serializable;

public class SeguridadInteresAdicionalPK
  implements Serializable
{
  public long idSeguridadInteresAdicional;

  public SeguridadInteresAdicionalPK()
  {
  }

  public SeguridadInteresAdicionalPK(long idSeguridadInteresAdicional)
  {
    this.idSeguridadInteresAdicional = idSeguridadInteresAdicional;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadInteresAdicionalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadInteresAdicionalPK thatPK)
  {
    return 
      this.idSeguridadInteresAdicional == thatPK.idSeguridadInteresAdicional;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadInteresAdicional)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadInteresAdicional);
  }
}