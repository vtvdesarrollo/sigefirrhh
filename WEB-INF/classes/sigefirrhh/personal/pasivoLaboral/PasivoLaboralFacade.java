package sigefirrhh.personal.pasivoLaboral;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class PasivoLaboralFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PasivoLaboralBusiness pasivoLaboralBusiness = new PasivoLaboralBusiness();

  public void addTasaBcv(TasaBcv tasaBcv)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.pasivoLaboralBusiness.addTasaBcv(tasaBcv);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTasaBcv(TasaBcv tasaBcv) throws Exception
  {
    try { this.txn.open();
      this.pasivoLaboralBusiness.updateTasaBcv(tasaBcv);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTasaBcv(TasaBcv tasaBcv) throws Exception
  {
    try { this.txn.open();
      this.pasivoLaboralBusiness.deleteTasaBcv(tasaBcv);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public TasaBcv findTasaBcvById(long tasaBcvId) throws Exception
  {
    try { this.txn.open();
      TasaBcv tasaBcv = 
        this.pasivoLaboralBusiness.findTasaBcvById(tasaBcvId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(tasaBcv);
      return tasaBcv;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTasaBcv() throws Exception
  {
    try { this.txn.open();
      return this.pasivoLaboralBusiness.findAllTasaBcv();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTasaBcvByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.pasivoLaboralBusiness.findTasaBcvByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.pasivoLaboralBusiness.addSeguridadInteresAdicional(seguridadInteresAdicional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional) throws Exception
  {
    try { this.txn.open();
      this.pasivoLaboralBusiness.updateSeguridadInteresAdicional(seguridadInteresAdicional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional) throws Exception
  {
    try { this.txn.open();
      this.pasivoLaboralBusiness.deleteSeguridadInteresAdicional(seguridadInteresAdicional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadInteresAdicional findSeguridadInteresAdicionalById(long seguridadInteresAdicionalId) throws Exception
  {
    try { this.txn.open();
      SeguridadInteresAdicional seguridadInteresAdicional = 
        this.pasivoLaboralBusiness.findSeguridadInteresAdicionalById(seguridadInteresAdicionalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadInteresAdicional);
      return seguridadInteresAdicional;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadInteresAdicional() throws Exception
  {
    try { this.txn.open();
      return this.pasivoLaboralBusiness.findAllSeguridadInteresAdicional();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadInteresAdicionalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.pasivoLaboralBusiness.findSeguridadInteresAdicionalByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadInteresAdicionalByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.pasivoLaboralBusiness.findSeguridadInteresAdicionalByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.pasivoLaboralBusiness.addHistoricoDevengadoIntegral(historicoDevengadoIntegral);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral) throws Exception
  {
    try { this.txn.open();
      this.pasivoLaboralBusiness.updateHistoricoDevengadoIntegral(historicoDevengadoIntegral);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral) throws Exception
  {
    try { this.txn.open();
      this.pasivoLaboralBusiness.deleteHistoricoDevengadoIntegral(historicoDevengadoIntegral);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistoricoDevengadoIntegral findHistoricoDevengadoIntegralById(long historicoDevengadoIntegralId) throws Exception
  {
    try { this.txn.open();
      HistoricoDevengadoIntegral historicoDevengadoIntegral = 
        this.pasivoLaboralBusiness.findHistoricoDevengadoIntegralById(historicoDevengadoIntegralId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historicoDevengadoIntegral);
      return historicoDevengadoIntegral;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistoricoDevengadoIntegral() throws Exception
  {
    try { this.txn.open();
      return this.pasivoLaboralBusiness.findAllHistoricoDevengadoIntegral();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoDevengadoIntegralByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.pasivoLaboralBusiness.findHistoricoDevengadoIntegralByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}