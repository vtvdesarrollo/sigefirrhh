package sigefirrhh.personal.pasivoLaboral;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import org.apache.log4j.Logger;

public class PasivoLaboralNoGenBusiness extends PasivoLaboralBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(PasivoLaboralNoGenBusiness.class.getName());

  private SeguridadInteresAdicionalNoGenBeanBusiness seguridadInteresAdicionalNoGenBeanBusiness = new SeguridadInteresAdicionalNoGenBeanBusiness();

  public void calcularInteresViejoRegimen(long idTipoPersonal, int anioMesDesde, String proceso)
    throws Exception
  {
    Connection connection = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select calcular_interes_viejo_regimen(?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + anioMesDesde);
      this.log.error("3- " + proceso);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, anioMesDesde);
      st.setString(3, proceso);
      st.executeQuery();

      connection.commit();
      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        } 
    }
  }

  public void liquidarTrabajador(long idTipoPersonal, long idTrabajador) throws Exception { Connection connection = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();
      sql.append("select liquidar_trabajador(?,?)");

      this.log.error("Parametros para liquidar_trabajador:");
      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + idTrabajador);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idTrabajador);

      st.executeQuery();

      connection.commit();
      this.log.error("Ejecutó el proceso de liquidar trabajador");
    }
    catch (Exception e)
    {
      this.log.error("Error en el proceso de liquidar trabajador, en el PasivoLaboralNoGenBusiness: " + e);
      throw e;
    }
    finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        } 
    } } 
  public void calcularInteresAdicional(long idTipoPersonal, int hastaAnio, int hastaMes, String usuario, String proceso)
    throws Exception
  {
    Connection connection = null;

    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      java.sql.Date fechaActual = new java.sql.Date(Calendar.getInstance().get(1) - 1900, 
        Calendar.getInstance().get(2), Calendar.getInstance().get(5));

      sql.append("select calcular_interes_adicional(?,?,?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + hastaAnio);
      this.log.error("3- " + hastaMes);
      this.log.error("4- " + fechaActual);
      this.log.error("5- " + usuario);
      this.log.error("6- " + proceso);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setInt(2, hastaAnio);
      st.setInt(3, hastaMes);
      st.setDate(4, fechaActual);
      st.setString(5, usuario);
      st.setString(6, proceso);
      st.executeQuery();

      connection.commit();

      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        } 
    }
  }

  public void calcularInteresAdicionalMensual(long idTipoPersonal, int anio, int mes, String usuario, String proceso) throws Exception { Connection connection = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      java.sql.Date fechaActual = new java.sql.Date(Calendar.getInstance().get(1) - 1900, 
        Calendar.getInstance().get(2), Calendar.getInstance().get(5));

      sql.append("select calcular_interes_adicional_mensual(?,?,?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + anio);
      this.log.error("3- " + mes);
      this.log.error("4- " + fechaActual);
      this.log.error("5- " + usuario);
      this.log.error("6- " + proceso);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setInt(2, anio);
      st.setInt(3, mes);
      st.setDate(4, fechaActual);
      st.setString(5, usuario);
      st.setString(6, proceso);
      st.executeQuery();

      connection.commit();

      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        } 
    } } 
  public void registrarAnticipos(long idTipoPersonal, int anio, int mes, java.util.Date fechaAnticipo, double montoAnticipo, String tipoAnticipo, String formaAnticipo) throws Exception
  {
    Connection connection = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      java.sql.Date fecha = new java.sql.Date(fechaAnticipo.getYear(), 
        fechaAnticipo.getMonth(), fechaAnticipo.getDate());

      sql.append("select registrar_anticipos(?,?,?,?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + anio);
      this.log.error("3- " + mes);
      this.log.error("4- " + fecha);
      this.log.error("5- " + montoAnticipo);
      this.log.error("6- " + tipoAnticipo);
      this.log.error("7- " + formaAnticipo);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setInt(2, anio);
      st.setInt(3, mes);
      st.setDate(4, fecha);
      st.setDouble(5, montoAnticipo);
      st.setString(6, tipoAnticipo);
      st.setString(7, formaAnticipo);
      st.executeQuery();

      connection.commit();

      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        } 
    }
  }

  public void calcularPrestacionesNuevoRegimen(long idTipoPersonal, int desdeAnio, int desdeMes, int hastaAnio, int hastaMes, String proceso) throws Exception { Connection connection = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select calcular_prestaciones_historico(?,?,?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + desdeAnio);
      this.log.error("3- " + desdeMes);
      this.log.error("4- " + hastaAnio);
      this.log.error("5- " + hastaMes);
      this.log.error("6- " + proceso);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setInt(2, desdeAnio);
      st.setInt(3, desdeMes);
      st.setInt(4, hastaAnio);
      st.setInt(5, hastaMes);
      st.setString(6, proceso);

      st.executeQuery();

      connection.commit();

      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        } 
    } } 
  public void calcularInteresesNuevoRegimen(long idTipoPersonal, int hastaAnio, int hastaMes, String proceso) throws Exception
  {
    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    connection.setAutoCommit(false);

    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      sql.append("select calcular_intereses_nuevo_regimen(?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + hastaAnio);
      this.log.error("3- " + hastaMes);
      this.log.error("4- " + proceso);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setInt(2, hastaAnio);
      st.setInt(3, hastaMes);
      st.setString(4, proceso);

      st.executeQuery();

      connection.commit();

      this.log.error("Se ejecutó el sp calcular_intereses_nuevo_regimen");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1) {
        } 
    }
  }

  public SeguridadInteresAdicional findSeguridadInteresAdicionalLast(long idTipoPersonal) throws Exception { return this.seguridadInteresAdicionalNoGenBeanBusiness.findLast(idTipoPersonal); }

}