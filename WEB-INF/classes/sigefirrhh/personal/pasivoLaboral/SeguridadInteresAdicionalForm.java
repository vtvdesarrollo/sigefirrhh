package sigefirrhh.personal.pasivoLaboral;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class SeguridadInteresAdicionalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeguridadInteresAdicionalForm.class.getName());
  private SeguridadInteresAdicional seguridadInteresAdicional;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private PasivoLaboralFacade pasivoLaboralFacade = new PasivoLaboralFacade();
  private boolean showSeguridadInteresAdicionalByTipoPersonal;
  private boolean showSeguridadInteresAdicionalByAnio;
  private String findSelectTipoPersonal;
  private int findAnio;
  private Collection findColTipoPersonal;
  private Collection colTipoPersonal;
  private String selectTipoPersonal;
  private Object stateResultSeguridadInteresAdicionalByTipoPersonal = null;

  private Object stateResultSeguridadInteresAdicionalByAnio = null;

  public String getFindSelectTipoPersonal()
  {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }
  public int getFindAnio() {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.seguridadInteresAdicional.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.seguridadInteresAdicional.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public SeguridadInteresAdicional getSeguridadInteresAdicional() {
    if (this.seguridadInteresAdicional == null) {
      this.seguridadInteresAdicional = new SeguridadInteresAdicional();
    }
    return this.seguridadInteresAdicional;
  }

  public SeguridadInteresAdicionalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSeguridadInteresAdicionalByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.pasivoLaboralFacade.findSeguridadInteresAdicionalByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showSeguridadInteresAdicionalByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadInteresAdicionalByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findAnio = 0;

    return null;
  }

  public String findSeguridadInteresAdicionalByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.pasivoLaboralFacade.findSeguridadInteresAdicionalByAnio(this.findAnio);
      this.showSeguridadInteresAdicionalByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadInteresAdicionalByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findAnio = 0;

    return null;
  }

  public boolean isShowSeguridadInteresAdicionalByTipoPersonal() {
    return this.showSeguridadInteresAdicionalByTipoPersonal;
  }
  public boolean isShowSeguridadInteresAdicionalByAnio() {
    return this.showSeguridadInteresAdicionalByAnio;
  }

  public String selectSeguridadInteresAdicional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;

    long idSeguridadInteresAdicional = 
      Long.parseLong((String)requestParameterMap.get("idSeguridadInteresAdicional"));
    try
    {
      this.seguridadInteresAdicional = 
        this.pasivoLaboralFacade.findSeguridadInteresAdicionalById(
        idSeguridadInteresAdicional);
      if (this.seguridadInteresAdicional.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.seguridadInteresAdicional.getTipoPersonal().getIdTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.seguridadInteresAdicional = null;
    this.showSeguridadInteresAdicionalByTipoPersonal = false;
    this.showSeguridadInteresAdicionalByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.seguridadInteresAdicional.getFechaProceso() != null) && 
      (this.seguridadInteresAdicional.getFechaProceso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha de Proceso no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.pasivoLaboralFacade.addSeguridadInteresAdicional(
          this.seguridadInteresAdicional);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.pasivoLaboralFacade.updateSeguridadInteresAdicional(
          this.seguridadInteresAdicional);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.pasivoLaboralFacade.deleteSeguridadInteresAdicional(
        this.seguridadInteresAdicional);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.seguridadInteresAdicional = new SeguridadInteresAdicional();

    this.selectTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.seguridadInteresAdicional.setIdSeguridadInteresAdicional(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.pasivoLaboral.SeguridadInteresAdicional"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.seguridadInteresAdicional = new SeguridadInteresAdicional();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}