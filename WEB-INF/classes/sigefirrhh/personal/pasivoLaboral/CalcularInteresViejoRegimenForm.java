package sigefirrhh.personal.pasivoLaboral;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.CategoriaPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CalcularInteresViejoRegimenForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularInteresViejoRegimenForm.class.getName());
  private Collection listTipoPersonal;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private PasivoLaboralNoGenFacade pasivoLaboralNoGenFacade;
  private long idTipoPersonal;
  private String selectIdTipoPersonal;
  private int anioMesDesde = 0;
  private TipoPersonal tipoPersonal;
  private String proceso;

  public CalcularInteresViejoRegimenForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.pasivoLaboralNoGenFacade = new PasivoLaboralNoGenFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridadForViejoRegimen(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();

      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(this.idTipoPersonal);
    try
    {
      if (this.idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(this.idTipoPersonal);
        if (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("1"))
          this.anioMesDesde = 199105;
        else if (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2"))
          this.anioMesDesde = 197505;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.pasivoLaboralNoGenFacade.calcularInteresViejoRegimen(this.idTipoPersonal, this.anioMesDesde, this.proceso);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error : " + e, ""));
      return null;
    }

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }

  public String getProceso() {
    return this.proceso;
  }

  public void setProceso(String proceso) {
    this.proceso = proceso;
  }
}