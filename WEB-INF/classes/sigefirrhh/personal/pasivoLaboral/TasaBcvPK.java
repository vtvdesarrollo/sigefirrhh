package sigefirrhh.personal.pasivoLaboral;

import java.io.Serializable;

public class TasaBcvPK
  implements Serializable
{
  public long idTasaBcv;

  public TasaBcvPK()
  {
  }

  public TasaBcvPK(long idTasaBcv)
  {
    this.idTasaBcv = idTasaBcv;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TasaBcvPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TasaBcvPK thatPK)
  {
    return 
      this.idTasaBcv == thatPK.idTasaBcv;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTasaBcv)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTasaBcv);
  }
}