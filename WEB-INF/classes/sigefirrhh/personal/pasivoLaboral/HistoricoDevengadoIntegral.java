package sigefirrhh.personal.pasivoLaboral;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Personal;

public class HistoricoDevengadoIntegral
  implements Serializable, PersistenceCapable
{
  private long idHistoricoDevengadoIntegral;
  private int anio;
  private int mes;
  private double sueldoIntegral;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "idHistoricoDevengadoIntegral", "mes", "personal", "sueldoIntegral" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public HistoricoDevengadoIntegral()
  {
    jdoSetanio(this, 0);

    jdoSetmes(this, 0);

    jdoSetsueldoIntegral(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetsueldoIntegral(this));

    return jdoGetanio(this) + " - " + jdoGetmes(this) + " - " + a;
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public long getIdHistoricoDevengadoIntegral()
  {
    return jdoGetidHistoricoDevengadoIntegral(this);
  }

  public void setIdHistoricoDevengadoIntegral(long idHistoricoDevengadoIntegral)
  {
    jdoSetidHistoricoDevengadoIntegral(this, idHistoricoDevengadoIntegral);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int mes)
  {
    jdoSetmes(this, mes);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public double getSueldoIntegral()
  {
    return jdoGetsueldoIntegral(this);
  }

  public void setSueldoIntegral(double sueldoIntegral)
  {
    jdoSetsueldoIntegral(this, sueldoIntegral);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.pasivoLaboral.HistoricoDevengadoIntegral"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoDevengadoIntegral());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoDevengadoIntegral localHistoricoDevengadoIntegral = new HistoricoDevengadoIntegral();
    localHistoricoDevengadoIntegral.jdoFlags = 1;
    localHistoricoDevengadoIntegral.jdoStateManager = paramStateManager;
    return localHistoricoDevengadoIntegral;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoDevengadoIntegral localHistoricoDevengadoIntegral = new HistoricoDevengadoIntegral();
    localHistoricoDevengadoIntegral.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoDevengadoIntegral.jdoFlags = 1;
    localHistoricoDevengadoIntegral.jdoStateManager = paramStateManager;
    return localHistoricoDevengadoIntegral;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoDevengadoIntegral);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoIntegral);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoDevengadoIntegral = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoIntegral = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoDevengadoIntegral == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoDevengadoIntegral.anio;
      return;
    case 1:
      if (paramHistoricoDevengadoIntegral == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoDevengadoIntegral = paramHistoricoDevengadoIntegral.idHistoricoDevengadoIntegral;
      return;
    case 2:
      if (paramHistoricoDevengadoIntegral == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoDevengadoIntegral.mes;
      return;
    case 3:
      if (paramHistoricoDevengadoIntegral == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramHistoricoDevengadoIntegral.personal;
      return;
    case 4:
      if (paramHistoricoDevengadoIntegral == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoIntegral = paramHistoricoDevengadoIntegral.sueldoIntegral;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoDevengadoIntegral))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoDevengadoIntegral localHistoricoDevengadoIntegral = (HistoricoDevengadoIntegral)paramObject;
    if (localHistoricoDevengadoIntegral.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoDevengadoIntegral, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoDevengadoIntegralPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoDevengadoIntegralPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoDevengadoIntegralPK))
      throw new IllegalArgumentException("arg1");
    HistoricoDevengadoIntegralPK localHistoricoDevengadoIntegralPK = (HistoricoDevengadoIntegralPK)paramObject;
    localHistoricoDevengadoIntegralPK.idHistoricoDevengadoIntegral = this.idHistoricoDevengadoIntegral;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoDevengadoIntegralPK))
      throw new IllegalArgumentException("arg1");
    HistoricoDevengadoIntegralPK localHistoricoDevengadoIntegralPK = (HistoricoDevengadoIntegralPK)paramObject;
    this.idHistoricoDevengadoIntegral = localHistoricoDevengadoIntegralPK.idHistoricoDevengadoIntegral;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoDevengadoIntegralPK))
      throw new IllegalArgumentException("arg2");
    HistoricoDevengadoIntegralPK localHistoricoDevengadoIntegralPK = (HistoricoDevengadoIntegralPK)paramObject;
    localHistoricoDevengadoIntegralPK.idHistoricoDevengadoIntegral = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoDevengadoIntegralPK))
      throw new IllegalArgumentException("arg2");
    HistoricoDevengadoIntegralPK localHistoricoDevengadoIntegralPK = (HistoricoDevengadoIntegralPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localHistoricoDevengadoIntegralPK.idHistoricoDevengadoIntegral);
  }

  private static final int jdoGetanio(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral)
  {
    if (paramHistoricoDevengadoIntegral.jdoFlags <= 0)
      return paramHistoricoDevengadoIntegral.anio;
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengadoIntegral.anio;
    if (localStateManager.isLoaded(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 0))
      return paramHistoricoDevengadoIntegral.anio;
    return localStateManager.getIntField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 0, paramHistoricoDevengadoIntegral.anio);
  }

  private static final void jdoSetanio(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral, int paramInt)
  {
    if (paramHistoricoDevengadoIntegral.jdoFlags == 0)
    {
      paramHistoricoDevengadoIntegral.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengadoIntegral.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 0, paramHistoricoDevengadoIntegral.anio, paramInt);
  }

  private static final long jdoGetidHistoricoDevengadoIntegral(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral)
  {
    return paramHistoricoDevengadoIntegral.idHistoricoDevengadoIntegral;
  }

  private static final void jdoSetidHistoricoDevengadoIntegral(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral, long paramLong)
  {
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengadoIntegral.idHistoricoDevengadoIntegral = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 1, paramHistoricoDevengadoIntegral.idHistoricoDevengadoIntegral, paramLong);
  }

  private static final int jdoGetmes(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral)
  {
    if (paramHistoricoDevengadoIntegral.jdoFlags <= 0)
      return paramHistoricoDevengadoIntegral.mes;
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengadoIntegral.mes;
    if (localStateManager.isLoaded(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 2))
      return paramHistoricoDevengadoIntegral.mes;
    return localStateManager.getIntField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 2, paramHistoricoDevengadoIntegral.mes);
  }

  private static final void jdoSetmes(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral, int paramInt)
  {
    if (paramHistoricoDevengadoIntegral.jdoFlags == 0)
    {
      paramHistoricoDevengadoIntegral.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengadoIntegral.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 2, paramHistoricoDevengadoIntegral.mes, paramInt);
  }

  private static final Personal jdoGetpersonal(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral)
  {
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengadoIntegral.personal;
    if (localStateManager.isLoaded(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 3))
      return paramHistoricoDevengadoIntegral.personal;
    return (Personal)localStateManager.getObjectField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 3, paramHistoricoDevengadoIntegral.personal);
  }

  private static final void jdoSetpersonal(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral, Personal paramPersonal)
  {
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengadoIntegral.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 3, paramHistoricoDevengadoIntegral.personal, paramPersonal);
  }

  private static final double jdoGetsueldoIntegral(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral)
  {
    if (paramHistoricoDevengadoIntegral.jdoFlags <= 0)
      return paramHistoricoDevengadoIntegral.sueldoIntegral;
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoDevengadoIntegral.sueldoIntegral;
    if (localStateManager.isLoaded(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 4))
      return paramHistoricoDevengadoIntegral.sueldoIntegral;
    return localStateManager.getDoubleField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 4, paramHistoricoDevengadoIntegral.sueldoIntegral);
  }

  private static final void jdoSetsueldoIntegral(HistoricoDevengadoIntegral paramHistoricoDevengadoIntegral, double paramDouble)
  {
    if (paramHistoricoDevengadoIntegral.jdoFlags == 0)
    {
      paramHistoricoDevengadoIntegral.sueldoIntegral = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoDevengadoIntegral.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoDevengadoIntegral.sueldoIntegral = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoDevengadoIntegral, jdoInheritedFieldCount + 4, paramHistoricoDevengadoIntegral.sueldoIntegral, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}