package sigefirrhh.personal.pasivoLaboral;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportInteresAdicionalMensualForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportInteresAdicionalMensualForm.class.getName());
  private int reportId;
  private String formato = "1";
  private long idTipoPersonal;
  private long idRegion;
  private String reportName;
  private String orden = "alf";
  private String tipo = "1";
  private Collection listTipoPersonal;
  private Collection listRegion;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private EstructuraFacade estructuraFacade;
  private int anio;
  private int mes;

  public boolean isShowOrden()
  {
    return this.tipo.equals("D");
  }
  public ReportInteresAdicionalMensualForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.estructuraFacade = new EstructuraFacade();
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "interesadicionalalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.anio = Calendar.getInstance().get(1);
    this.mes = Calendar.getInstance().get(2);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportInteresAdicionalMensualForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRegion = this.estructuraFacade.findAllRegion();
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listRegion = new ArrayList();
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      this.reportName = "";

      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }

      this.reportName += "interesadicional";

      this.reportName += this.orden;

      if (this.idRegion != 0L)
        this.reportName += "reg";
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "";

      if (this.formato.equals("2")) {
        this.reportName = "a_";
      }

      this.reportName += "interesadicional";

      this.reportName += this.orden;

      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }

      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));

      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/pasivoLaboral");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }

  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int i) {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String string) {
    this.orden = string;
  }

  public long getIdRegion() {
    return this.idRegion;
  }

  public void setIdRegion(long l) {
    this.idRegion = l;
  }

  public String getFormato() {
    return this.formato;
  }

  public void setFormato(String string) {
    this.formato = string;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo)
  {
    this.tipo = tipo;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
}