package sigefirrhh.personal.pasivoLaboral;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CalcularInteresAdicionalMensualForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularInteresAdicionalMensualForm.class.getName());
  private Collection listTipoPersonal;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private PasivoLaboralNoGenFacade pasivoLaboralNoGenFacade;
  private long idTipoPersonal;
  private String selectIdTipoPersonal;
  private TipoPersonal tipoPersonal;
  private String proceso;
  private int anio = 0;
  private int mes = 0;

  public CalcularInteresAdicionalMensualForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.pasivoLaboralNoGenFacade = new PasivoLaboralNoGenFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridadForInteresAdicional(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), "S");
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();

      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(this.idTipoPersonal);
    try {
      if (this.idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(this.idTipoPersonal);
        SeguridadInteresAdicional seguridadInteresAdicional = this.pasivoLaboralNoGenFacade.findSeguridadInteresAdicionalLast(this.idTipoPersonal);

        this.mes = seguridadInteresAdicional.getMes();
        this.anio = seguridadInteresAdicional.getAnio();

        if (this.mes == 12) {
          this.mes = 1;
          this.anio += 1;
        } else {
          this.mes += 1;
        }
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.mes = 0;
      this.anio = 0;
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if ((this.anio == 0) || (this.mes == 0)) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No existen registros de seguridad para este tipo de personal", ""));
        return null;
      }

      this.pasivoLaboralNoGenFacade.calcularInteresAdicionalMensual(this.idTipoPersonal, this.anio, this.mes, this.login.getUsuario(), this.proceso);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error : " + e, ""));
      return null;
    }

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }

  public String getProceso() {
    return this.proceso;
  }

  public void setProceso(String proceso) {
    this.proceso = proceso;
  }

  public int getAnio() {
    return this.anio;
  }

  public void setAnio(int anio) {
    this.anio = anio;
  }

  public int getMes() {
    return this.mes;
  }

  public void setMes(int mes) {
    this.mes = mes;
  }
}