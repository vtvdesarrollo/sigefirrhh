package sigefirrhh.personal.pasivoLaboral;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class TasaBcv
  implements Serializable, PersistenceCapable
{
  private long idTasaBcv;
  private int anio;
  private int mes;
  private double tasaPromedio;
  private double tasaActiva;
  private double tasaFideicomiso;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "idTasaBcv", "mes", "tasaActiva", "tasaFideicomiso", "tasaPromedio" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String activa = b.format(jdoGettasaActiva(this));
    String promedio = b.format(jdoGettasaPromedio(this));
    String fideicomiso = b.format(jdoGettasaFideicomiso(this));

    return jdoGetanio(this) + "-" + jdoGetmes(this) + " - Promedio: " + promedio + " - Activa: " + activa + " - Fideicomiso: " + fideicomiso;
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public long getIdTasaBcv() {
    return jdoGetidTasaBcv(this);
  }

  public void setIdTasaBcv(long idTasaBcv) {
    jdoSetidTasaBcv(this, idTasaBcv);
  }

  public int getMes() {
    return jdoGetmes(this);
  }

  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }

  public double getTasaActiva() {
    return jdoGettasaActiva(this);
  }

  public void setTasaActiva(double tasaActiva) {
    jdoSettasaActiva(this, tasaActiva);
  }

  public double getTasaFideicomiso() {
    return jdoGettasaFideicomiso(this);
  }

  public void setTasaFideicomiso(double tasaFideicomiso) {
    jdoSettasaFideicomiso(this, tasaFideicomiso);
  }

  public double getTasaPromedio() {
    return jdoGettasaPromedio(this);
  }

  public void setTasaPromedio(double tasaPromedio) {
    jdoSettasaPromedio(this, tasaPromedio);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.pasivoLaboral.TasaBcv"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new TasaBcv());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    TasaBcv localTasaBcv = new TasaBcv();
    localTasaBcv.jdoFlags = 1;
    localTasaBcv.jdoStateManager = paramStateManager;
    return localTasaBcv;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    TasaBcv localTasaBcv = new TasaBcv();
    localTasaBcv.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTasaBcv.jdoFlags = 1;
    localTasaBcv.jdoStateManager = paramStateManager;
    return localTasaBcv;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTasaBcv);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tasaActiva);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tasaFideicomiso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tasaPromedio);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTasaBcv = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tasaActiva = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tasaFideicomiso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tasaPromedio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(TasaBcv paramTasaBcv, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTasaBcv == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramTasaBcv.anio;
      return;
    case 1:
      if (paramTasaBcv == null)
        throw new IllegalArgumentException("arg1");
      this.idTasaBcv = paramTasaBcv.idTasaBcv;
      return;
    case 2:
      if (paramTasaBcv == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramTasaBcv.mes;
      return;
    case 3:
      if (paramTasaBcv == null)
        throw new IllegalArgumentException("arg1");
      this.tasaActiva = paramTasaBcv.tasaActiva;
      return;
    case 4:
      if (paramTasaBcv == null)
        throw new IllegalArgumentException("arg1");
      this.tasaFideicomiso = paramTasaBcv.tasaFideicomiso;
      return;
    case 5:
      if (paramTasaBcv == null)
        throw new IllegalArgumentException("arg1");
      this.tasaPromedio = paramTasaBcv.tasaPromedio;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof TasaBcv))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    TasaBcv localTasaBcv = (TasaBcv)paramObject;
    if (localTasaBcv.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTasaBcv, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TasaBcvPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TasaBcvPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TasaBcvPK))
      throw new IllegalArgumentException("arg1");
    TasaBcvPK localTasaBcvPK = (TasaBcvPK)paramObject;
    localTasaBcvPK.idTasaBcv = this.idTasaBcv;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TasaBcvPK))
      throw new IllegalArgumentException("arg1");
    TasaBcvPK localTasaBcvPK = (TasaBcvPK)paramObject;
    this.idTasaBcv = localTasaBcvPK.idTasaBcv;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TasaBcvPK))
      throw new IllegalArgumentException("arg2");
    TasaBcvPK localTasaBcvPK = (TasaBcvPK)paramObject;
    localTasaBcvPK.idTasaBcv = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TasaBcvPK))
      throw new IllegalArgumentException("arg2");
    TasaBcvPK localTasaBcvPK = (TasaBcvPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localTasaBcvPK.idTasaBcv);
  }

  private static final int jdoGetanio(TasaBcv paramTasaBcv)
  {
    if (paramTasaBcv.jdoFlags <= 0)
      return paramTasaBcv.anio;
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
      return paramTasaBcv.anio;
    if (localStateManager.isLoaded(paramTasaBcv, jdoInheritedFieldCount + 0))
      return paramTasaBcv.anio;
    return localStateManager.getIntField(paramTasaBcv, jdoInheritedFieldCount + 0, paramTasaBcv.anio);
  }

  private static final void jdoSetanio(TasaBcv paramTasaBcv, int paramInt)
  {
    if (paramTasaBcv.jdoFlags == 0)
    {
      paramTasaBcv.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
    {
      paramTasaBcv.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramTasaBcv, jdoInheritedFieldCount + 0, paramTasaBcv.anio, paramInt);
  }

  private static final long jdoGetidTasaBcv(TasaBcv paramTasaBcv)
  {
    return paramTasaBcv.idTasaBcv;
  }

  private static final void jdoSetidTasaBcv(TasaBcv paramTasaBcv, long paramLong)
  {
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
    {
      paramTasaBcv.idTasaBcv = paramLong;
      return;
    }
    localStateManager.setLongField(paramTasaBcv, jdoInheritedFieldCount + 1, paramTasaBcv.idTasaBcv, paramLong);
  }

  private static final int jdoGetmes(TasaBcv paramTasaBcv)
  {
    if (paramTasaBcv.jdoFlags <= 0)
      return paramTasaBcv.mes;
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
      return paramTasaBcv.mes;
    if (localStateManager.isLoaded(paramTasaBcv, jdoInheritedFieldCount + 2))
      return paramTasaBcv.mes;
    return localStateManager.getIntField(paramTasaBcv, jdoInheritedFieldCount + 2, paramTasaBcv.mes);
  }

  private static final void jdoSetmes(TasaBcv paramTasaBcv, int paramInt)
  {
    if (paramTasaBcv.jdoFlags == 0)
    {
      paramTasaBcv.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
    {
      paramTasaBcv.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramTasaBcv, jdoInheritedFieldCount + 2, paramTasaBcv.mes, paramInt);
  }

  private static final double jdoGettasaActiva(TasaBcv paramTasaBcv)
  {
    if (paramTasaBcv.jdoFlags <= 0)
      return paramTasaBcv.tasaActiva;
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
      return paramTasaBcv.tasaActiva;
    if (localStateManager.isLoaded(paramTasaBcv, jdoInheritedFieldCount + 3))
      return paramTasaBcv.tasaActiva;
    return localStateManager.getDoubleField(paramTasaBcv, jdoInheritedFieldCount + 3, paramTasaBcv.tasaActiva);
  }

  private static final void jdoSettasaActiva(TasaBcv paramTasaBcv, double paramDouble)
  {
    if (paramTasaBcv.jdoFlags == 0)
    {
      paramTasaBcv.tasaActiva = paramDouble;
      return;
    }
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
    {
      paramTasaBcv.tasaActiva = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTasaBcv, jdoInheritedFieldCount + 3, paramTasaBcv.tasaActiva, paramDouble);
  }

  private static final double jdoGettasaFideicomiso(TasaBcv paramTasaBcv)
  {
    if (paramTasaBcv.jdoFlags <= 0)
      return paramTasaBcv.tasaFideicomiso;
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
      return paramTasaBcv.tasaFideicomiso;
    if (localStateManager.isLoaded(paramTasaBcv, jdoInheritedFieldCount + 4))
      return paramTasaBcv.tasaFideicomiso;
    return localStateManager.getDoubleField(paramTasaBcv, jdoInheritedFieldCount + 4, paramTasaBcv.tasaFideicomiso);
  }

  private static final void jdoSettasaFideicomiso(TasaBcv paramTasaBcv, double paramDouble)
  {
    if (paramTasaBcv.jdoFlags == 0)
    {
      paramTasaBcv.tasaFideicomiso = paramDouble;
      return;
    }
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
    {
      paramTasaBcv.tasaFideicomiso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTasaBcv, jdoInheritedFieldCount + 4, paramTasaBcv.tasaFideicomiso, paramDouble);
  }

  private static final double jdoGettasaPromedio(TasaBcv paramTasaBcv)
  {
    if (paramTasaBcv.jdoFlags <= 0)
      return paramTasaBcv.tasaPromedio;
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
      return paramTasaBcv.tasaPromedio;
    if (localStateManager.isLoaded(paramTasaBcv, jdoInheritedFieldCount + 5))
      return paramTasaBcv.tasaPromedio;
    return localStateManager.getDoubleField(paramTasaBcv, jdoInheritedFieldCount + 5, paramTasaBcv.tasaPromedio);
  }

  private static final void jdoSettasaPromedio(TasaBcv paramTasaBcv, double paramDouble)
  {
    if (paramTasaBcv.jdoFlags == 0)
    {
      paramTasaBcv.tasaPromedio = paramDouble;
      return;
    }
    StateManager localStateManager = paramTasaBcv.jdoStateManager;
    if (localStateManager == null)
    {
      paramTasaBcv.tasaPromedio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTasaBcv, jdoInheritedFieldCount + 5, paramTasaBcv.tasaPromedio, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}