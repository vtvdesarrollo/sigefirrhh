package sigefirrhh.personal.pasivoLaboral;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.procesoAniversario.ProcesoAniversarioNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class LiquidacionTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(LiquidacionTrabajadorForm.class.getName());

  private boolean trabajadorEgresado = false;
  private Trabajador trabajador = null;
  private Collection result = null;

  private boolean procesing = false;
  private boolean selected = false;
  private LoginSession login;
  private ProcesoAniversarioNoGenFacade procesoAniversarioNoGenFacade = new ProcesoAniversarioNoGenFacade();

  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private PasivoLaboralNoGenFacade pasivoLaboralNoGenFacade = new PasivoLaboralNoGenFacade();
  private Collection resultTrabajador;
  private int findTrabajadorCedula;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private int findTrabajadorCodigoNomina;
  private boolean showResultPersonal = false;
  private boolean showAddResultPersonal = false;
  private boolean showResult = false;
  private boolean showResultTrabajador = false;
  private String findSelectPersonal;
  private Collection findColTipoPersonal = null;
  private String selectCausaMovimiento;
  private boolean showData = false;
  private String findSelectTrabajadorIdTipoPersonal;
  private long numeroMovimiento;
  private boolean showReport = false;
  private int reportId;
  private String reportName;
  private long idMovimientoSitp = 0L;
  private long idCausaPersonal;

  private void resetResultTrabajador()
  {
    this.resultTrabajador = null;

    this.trabajador = null;

    this.showResultTrabajador = false;
  }
  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
      log.error("showResult" + this.showResultTrabajador);
      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public void setFindSelectTrabajadorIdTipoPersonal(String string)
  {
    this.findSelectTrabajadorIdTipoPersonal = string;
  }

  public String getSelectCausaMovimiento() {
    return this.selectCausaMovimiento;
  }

  public Collection getResult() {
    return this.result;
  }

  public Trabajador getTrabajador() {
    if (this.trabajador == null) {
      this.trabajador = new Trabajador();
    }
    return this.trabajador;
  }

  public LiquidacionTrabajadorForm()
    throws Exception
  {
    if (this.login == null) {
      FacesContext context = FacesContext.getCurrentInstance();
      this.login = 
        ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
        context, 
        "loginSession"));
    }
  }

  public String runReport()
  {
    try {
      Map parameters = new Hashtable();
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");
      parameters.put("id_personal", new Long(this.trabajador.getPersonal().getIdPersonal()));
      parameters.put("id_causa_movimiento", new Long(this.idCausaPersonal));
      parameters.put("id_movimiento", new Long(this.idMovimientoSitp));

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String ejecutar()
  {
    this.procesing = true;
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.trabajador == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar un trabajador valido", ""));
        return null;
      }

      this.pasivoLaboralNoGenFacade.liquidarTrabajador(this.trabajador.getTipoPersonal().getIdTipoPersonal(), this.trabajador.getIdTrabajador());

      boolean estado = this.procesoAniversarioNoGenFacade.calcularBonoVacacionalLiquidacion(this.trabajador.getTipoPersonal().getIdTipoPersonal(), this.trabajador.getIdTrabajador(), this.login.getUsuario());

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());

      context.addMessage("success_add", new FacesMessage("Se calculó la liquidación con éxito"));
      this.showData = false;
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error al liquidar trabajador", ""));
      log.error("Error al intentar realizar proceso de liquidacion de trabajador");
      log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }

  public Collection getFindColTipoPersonal()
  {
    if (this.findColTipoPersonal == null) {
      try {
        log.error("paso por fctp 1");
        this.findColTipoPersonal = new ArrayList();
        this.findColTipoPersonal = 
          this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
        log.error("paso por fctp 2");
      } catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }
    }

    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListTipoCtaNomina() {
    Collection col = new ArrayList();

    Iterator iterEntry = Trabajador.LISTA_CUENTA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();
    this.selectCausaMovimiento = null;

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = null;
      this.trabajador = this.trabajadorFacade.findTrabajadorById(idTrabajador);

      this.trabajadorEgresado = this.trabajador.getEstatus().equalsIgnoreCase("E");
      if (!this.trabajadorEgresado) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede liquidar trabajador porque esta activo", ""));
        this.selected = false;
      } else {
        this.selected = true;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.showData = this.selected;
    this.procesing = (!this.selected);

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;

    this.showResult = false;
  }

  public String process()
  {
    this.procesing = true;

    return null;
  }

  public String abort() {
    this.procesing = false;
    this.selected = false;
    this.trabajador = new Trabajador();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.procesing = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.trabajador = new Trabajador();
    return "cancel";
  }

  public boolean isProcesing() {
    return this.procesing;
  }
  public boolean isShowData() {
    return this.selected;
  }

  public boolean isShowResultPersonal()
  {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return this.procesing;
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getFindTrabajadorCedula()
  {
    return this.findTrabajadorCedula;
  }

  public int getFindTrabajadorCodigoNomina()
  {
    return this.findTrabajadorCodigoNomina;
  }

  public String getFindTrabajadorPrimerApellido()
  {
    return this.findTrabajadorPrimerApellido;
  }

  public String getFindTrabajadorPrimerNombre()
  {
    return this.findTrabajadorPrimerNombre;
  }

  public String getFindTrabajadorSegundoApellido()
  {
    return this.findTrabajadorSegundoApellido;
  }

  public String getFindTrabajadorSegundoNombre()
  {
    return this.findTrabajadorSegundoNombre;
  }

  public void setFindTrabajadorCedula(int i)
  {
    this.findTrabajadorCedula = i;
  }

  public void setFindTrabajadorCodigoNomina(int i)
  {
    this.findTrabajadorCodigoNomina = i;
  }

  public void setFindTrabajadorPrimerApellido(String string)
  {
    this.findTrabajadorPrimerApellido = string;
  }

  public void setFindTrabajadorPrimerNombre(String string)
  {
    this.findTrabajadorPrimerNombre = string;
  }

  public void setFindTrabajadorSegundoApellido(String string)
  {
    this.findTrabajadorSegundoApellido = string;
  }

  public void setFindTrabajadorSegundoNombre(String string)
  {
    this.findTrabajadorSegundoNombre = string;
  }

  public String getFindSelectTrabajadorIdTipoPersonal()
  {
    return this.findSelectTrabajadorIdTipoPersonal;
  }

  public boolean isShowResultTrabajador()
  {
    return this.showResultTrabajador;
  }

  public Collection getResultTrabajador()
  {
    return this.resultTrabajador;
  }

  public void setShowData(boolean b)
  {
    this.showData = b;
  }

  public boolean isTrabajadorEgresado() {
    return this.trabajadorEgresado;
  }
}