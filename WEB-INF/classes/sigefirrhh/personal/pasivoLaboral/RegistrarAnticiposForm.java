package sigefirrhh.personal.pasivoLaboral;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class RegistrarAnticiposForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RegistrarAnticiposForm.class.getName());
  private Collection listTipoPersonal;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private PasivoLaboralNoGenFacade pasivoLaboralNoGenFacade;
  private long idTipoPersonal;
  private String selectIdTipoPersonal;
  private TipoPersonal tipoPersonal;
  private String tipoAnticipo = "N";
  private String formaAnticipo = "P";
  private double montoAnticipo = 0.0D;
  private Date fechaAnticipo;
  private int anio = 0;
  private int mes = 0;

  public RegistrarAnticiposForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.pasivoLaboralNoGenFacade = new PasivoLaboralNoGenFacade();
    this.fechaAnticipo = Calendar.getInstance().getTime();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();

      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(this.idTipoPersonal);
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.pasivoLaboralNoGenFacade.registrarAnticipos(this.idTipoPersonal, this.anio, this.mes, this.fechaAnticipo, this.montoAnticipo, this.tipoAnticipo, this.formaAnticipo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error : " + e, ""));
      return null;
    }

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }
  public int getAnio() {
    return this.anio;
  }

  public void setAnio(int anio) {
    this.anio = anio;
  }

  public int getMes() {
    return this.mes;
  }

  public void setMes(int mes) {
    this.mes = mes;
  }

  public Date getFechaAnticipo() {
    return this.fechaAnticipo;
  }

  public void setFechaAnticipo(Date fechaAnticipo) {
    this.fechaAnticipo = fechaAnticipo;
  }

  public String getFormaAnticipo() {
    return this.formaAnticipo;
  }

  public void setFormaAnticipo(String formaAnticipo) {
    this.formaAnticipo = formaAnticipo;
  }

  public double getMontoAnticipo() {
    return this.montoAnticipo;
  }

  public void setMontoAnticipo(double montoAnticipo) {
    this.montoAnticipo = montoAnticipo;
  }

  public String getTipoAnticipo() {
    return this.tipoAnticipo;
  }

  public void setTipoAnticipo(String tipoAnticipo) {
    this.tipoAnticipo = tipoAnticipo;
  }
}