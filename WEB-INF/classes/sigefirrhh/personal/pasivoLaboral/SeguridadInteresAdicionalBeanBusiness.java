package sigefirrhh.personal.pasivoLaboral;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class SeguridadInteresAdicionalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadInteresAdicional seguridadInteresAdicionalNew = 
      (SeguridadInteresAdicional)BeanUtils.cloneBean(
      seguridadInteresAdicional);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadInteresAdicionalNew.getTipoPersonal() != null) {
      seguridadInteresAdicionalNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadInteresAdicionalNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(seguridadInteresAdicionalNew);
  }

  public void updateSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional) throws Exception
  {
    SeguridadInteresAdicional seguridadInteresAdicionalModify = 
      findSeguridadInteresAdicionalById(seguridadInteresAdicional.getIdSeguridadInteresAdicional());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (seguridadInteresAdicional.getTipoPersonal() != null) {
      seguridadInteresAdicional.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        seguridadInteresAdicional.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(seguridadInteresAdicionalModify, seguridadInteresAdicional);
  }

  public void deleteSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadInteresAdicional seguridadInteresAdicionalDelete = 
      findSeguridadInteresAdicionalById(seguridadInteresAdicional.getIdSeguridadInteresAdicional());
    pm.deletePersistent(seguridadInteresAdicionalDelete);
  }

  public SeguridadInteresAdicional findSeguridadInteresAdicionalById(long idSeguridadInteresAdicional) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadInteresAdicional == pIdSeguridadInteresAdicional";
    Query query = pm.newQuery(SeguridadInteresAdicional.class, filter);

    query.declareParameters("long pIdSeguridadInteresAdicional");

    parameters.put("pIdSeguridadInteresAdicional", new Long(idSeguridadInteresAdicional));

    Collection colSeguridadInteresAdicional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadInteresAdicional.iterator();
    return (SeguridadInteresAdicional)iterator.next();
  }

  public Collection findSeguridadInteresAdicionalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadInteresAdicionalExtent = pm.getExtent(
      SeguridadInteresAdicional.class, true);
    Query query = pm.newQuery(seguridadInteresAdicionalExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(SeguridadInteresAdicional.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colSeguridadInteresAdicional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadInteresAdicional);

    return colSeguridadInteresAdicional;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(SeguridadInteresAdicional.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, mes ascending");

    Collection colSeguridadInteresAdicional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadInteresAdicional);

    return colSeguridadInteresAdicional;
  }
}