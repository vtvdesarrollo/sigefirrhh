package sigefirrhh.personal.pasivoLaboral;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class SeguridadInteresAdicional
  implements Serializable, PersistenceCapable
{
  private long idSeguridadInteresAdicional;
  private TipoPersonal tipoPersonal;
  private int anio;
  private int mes;
  private double tasaAplicada;
  private Date fechaProceso;
  private String usuario;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "fechaProceso", "idSeguridadInteresAdicional", "mes", "tasaAplicada", "tipoPersonal", "usuario" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public SeguridadInteresAdicional()
  {
    jdoSettasaAplicada(this, 0.0D);
  }

  public String toString()
  {
    return jdoGettipoPersonal(this).getNombre() + "  -  " + 
      jdoGetanio(this) + "  -  " + 
      jdoGetmes(this) + "  -  " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaProceso(this));
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public Date getFechaProceso() {
    return jdoGetfechaProceso(this);
  }
  public void setFechaProceso(Date fechaProceso) {
    jdoSetfechaProceso(this, fechaProceso);
  }
  public long getIdSeguridadInteresAdicional() {
    return jdoGetidSeguridadInteresAdicional(this);
  }
  public void setIdSeguridadInteresAdicional(long idSeguridadInteresAdicional) {
    jdoSetidSeguridadInteresAdicional(this, idSeguridadInteresAdicional);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public String getUsuario() {
    return jdoGetusuario(this);
  }
  public void setUsuario(String usuario) {
    jdoSetusuario(this, usuario);
  }
  public double getTasaAplicada() {
    return jdoGettasaAplicada(this);
  }
  public void setTasaAplicada(double tasaAplicada) {
    jdoSettasaAplicada(this, tasaAplicada);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.pasivoLaboral.SeguridadInteresAdicional"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadInteresAdicional());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadInteresAdicional localSeguridadInteresAdicional = new SeguridadInteresAdicional();
    localSeguridadInteresAdicional.jdoFlags = 1;
    localSeguridadInteresAdicional.jdoStateManager = paramStateManager;
    return localSeguridadInteresAdicional;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadInteresAdicional localSeguridadInteresAdicional = new SeguridadInteresAdicional();
    localSeguridadInteresAdicional.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadInteresAdicional.jdoFlags = 1;
    localSeguridadInteresAdicional.jdoStateManager = paramStateManager;
    return localSeguridadInteresAdicional;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadInteresAdicional);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tasaAplicada);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadInteresAdicional = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tasaAplicada = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadInteresAdicional paramSeguridadInteresAdicional, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadInteresAdicional == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadInteresAdicional.anio;
      return;
    case 1:
      if (paramSeguridadInteresAdicional == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadInteresAdicional.fechaProceso;
      return;
    case 2:
      if (paramSeguridadInteresAdicional == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadInteresAdicional = paramSeguridadInteresAdicional.idSeguridadInteresAdicional;
      return;
    case 3:
      if (paramSeguridadInteresAdicional == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadInteresAdicional.mes;
      return;
    case 4:
      if (paramSeguridadInteresAdicional == null)
        throw new IllegalArgumentException("arg1");
      this.tasaAplicada = paramSeguridadInteresAdicional.tasaAplicada;
      return;
    case 5:
      if (paramSeguridadInteresAdicional == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramSeguridadInteresAdicional.tipoPersonal;
      return;
    case 6:
      if (paramSeguridadInteresAdicional == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadInteresAdicional.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadInteresAdicional))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadInteresAdicional localSeguridadInteresAdicional = (SeguridadInteresAdicional)paramObject;
    if (localSeguridadInteresAdicional.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadInteresAdicional, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadInteresAdicionalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadInteresAdicionalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadInteresAdicionalPK))
      throw new IllegalArgumentException("arg1");
    SeguridadInteresAdicionalPK localSeguridadInteresAdicionalPK = (SeguridadInteresAdicionalPK)paramObject;
    localSeguridadInteresAdicionalPK.idSeguridadInteresAdicional = this.idSeguridadInteresAdicional;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadInteresAdicionalPK))
      throw new IllegalArgumentException("arg1");
    SeguridadInteresAdicionalPK localSeguridadInteresAdicionalPK = (SeguridadInteresAdicionalPK)paramObject;
    this.idSeguridadInteresAdicional = localSeguridadInteresAdicionalPK.idSeguridadInteresAdicional;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadInteresAdicionalPK))
      throw new IllegalArgumentException("arg2");
    SeguridadInteresAdicionalPK localSeguridadInteresAdicionalPK = (SeguridadInteresAdicionalPK)paramObject;
    localSeguridadInteresAdicionalPK.idSeguridadInteresAdicional = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadInteresAdicionalPK))
      throw new IllegalArgumentException("arg2");
    SeguridadInteresAdicionalPK localSeguridadInteresAdicionalPK = (SeguridadInteresAdicionalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSeguridadInteresAdicionalPK.idSeguridadInteresAdicional);
  }

  private static final int jdoGetanio(SeguridadInteresAdicional paramSeguridadInteresAdicional)
  {
    if (paramSeguridadInteresAdicional.jdoFlags <= 0)
      return paramSeguridadInteresAdicional.anio;
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadInteresAdicional.anio;
    if (localStateManager.isLoaded(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 0))
      return paramSeguridadInteresAdicional.anio;
    return localStateManager.getIntField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 0, paramSeguridadInteresAdicional.anio);
  }

  private static final void jdoSetanio(SeguridadInteresAdicional paramSeguridadInteresAdicional, int paramInt)
  {
    if (paramSeguridadInteresAdicional.jdoFlags == 0)
    {
      paramSeguridadInteresAdicional.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadInteresAdicional.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 0, paramSeguridadInteresAdicional.anio, paramInt);
  }

  private static final Date jdoGetfechaProceso(SeguridadInteresAdicional paramSeguridadInteresAdicional)
  {
    if (paramSeguridadInteresAdicional.jdoFlags <= 0)
      return paramSeguridadInteresAdicional.fechaProceso;
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadInteresAdicional.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 1))
      return paramSeguridadInteresAdicional.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 1, paramSeguridadInteresAdicional.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadInteresAdicional paramSeguridadInteresAdicional, Date paramDate)
  {
    if (paramSeguridadInteresAdicional.jdoFlags == 0)
    {
      paramSeguridadInteresAdicional.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadInteresAdicional.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 1, paramSeguridadInteresAdicional.fechaProceso, paramDate);
  }

  private static final long jdoGetidSeguridadInteresAdicional(SeguridadInteresAdicional paramSeguridadInteresAdicional)
  {
    return paramSeguridadInteresAdicional.idSeguridadInteresAdicional;
  }

  private static final void jdoSetidSeguridadInteresAdicional(SeguridadInteresAdicional paramSeguridadInteresAdicional, long paramLong)
  {
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadInteresAdicional.idSeguridadInteresAdicional = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 2, paramSeguridadInteresAdicional.idSeguridadInteresAdicional, paramLong);
  }

  private static final int jdoGetmes(SeguridadInteresAdicional paramSeguridadInteresAdicional)
  {
    if (paramSeguridadInteresAdicional.jdoFlags <= 0)
      return paramSeguridadInteresAdicional.mes;
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadInteresAdicional.mes;
    if (localStateManager.isLoaded(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 3))
      return paramSeguridadInteresAdicional.mes;
    return localStateManager.getIntField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 3, paramSeguridadInteresAdicional.mes);
  }

  private static final void jdoSetmes(SeguridadInteresAdicional paramSeguridadInteresAdicional, int paramInt)
  {
    if (paramSeguridadInteresAdicional.jdoFlags == 0)
    {
      paramSeguridadInteresAdicional.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadInteresAdicional.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 3, paramSeguridadInteresAdicional.mes, paramInt);
  }

  private static final double jdoGettasaAplicada(SeguridadInteresAdicional paramSeguridadInteresAdicional)
  {
    if (paramSeguridadInteresAdicional.jdoFlags <= 0)
      return paramSeguridadInteresAdicional.tasaAplicada;
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadInteresAdicional.tasaAplicada;
    if (localStateManager.isLoaded(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 4))
      return paramSeguridadInteresAdicional.tasaAplicada;
    return localStateManager.getDoubleField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 4, paramSeguridadInteresAdicional.tasaAplicada);
  }

  private static final void jdoSettasaAplicada(SeguridadInteresAdicional paramSeguridadInteresAdicional, double paramDouble)
  {
    if (paramSeguridadInteresAdicional.jdoFlags == 0)
    {
      paramSeguridadInteresAdicional.tasaAplicada = paramDouble;
      return;
    }
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadInteresAdicional.tasaAplicada = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 4, paramSeguridadInteresAdicional.tasaAplicada, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(SeguridadInteresAdicional paramSeguridadInteresAdicional)
  {
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadInteresAdicional.tipoPersonal;
    if (localStateManager.isLoaded(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 5))
      return paramSeguridadInteresAdicional.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 5, paramSeguridadInteresAdicional.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(SeguridadInteresAdicional paramSeguridadInteresAdicional, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadInteresAdicional.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 5, paramSeguridadInteresAdicional.tipoPersonal, paramTipoPersonal);
  }

  private static final String jdoGetusuario(SeguridadInteresAdicional paramSeguridadInteresAdicional)
  {
    if (paramSeguridadInteresAdicional.jdoFlags <= 0)
      return paramSeguridadInteresAdicional.usuario;
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadInteresAdicional.usuario;
    if (localStateManager.isLoaded(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 6))
      return paramSeguridadInteresAdicional.usuario;
    return localStateManager.getStringField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 6, paramSeguridadInteresAdicional.usuario);
  }

  private static final void jdoSetusuario(SeguridadInteresAdicional paramSeguridadInteresAdicional, String paramString)
  {
    if (paramSeguridadInteresAdicional.jdoFlags == 0)
    {
      paramSeguridadInteresAdicional.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadInteresAdicional.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadInteresAdicional.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadInteresAdicional, jdoInheritedFieldCount + 6, paramSeguridadInteresAdicional.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}