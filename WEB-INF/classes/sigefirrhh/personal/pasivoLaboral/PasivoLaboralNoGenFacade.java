package sigefirrhh.personal.pasivoLaboral;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Date;

public class PasivoLaboralNoGenFacade extends PasivoLaboralFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PasivoLaboralNoGenBusiness pasivoLaboralNoGenBusiness = new PasivoLaboralNoGenBusiness();

  public void calcularInteresViejoRegimen(long idTipoPersonal, int anioMesDesde, String proceso)
    throws Exception
  {
    this.pasivoLaboralNoGenBusiness.calcularInteresViejoRegimen(idTipoPersonal, anioMesDesde, proceso);
  }

  public void calcularInteresAdicional(long idTipoPersonal, int hastaAnio, int hastaMes, String usuario, String proceso) throws Exception {
    this.pasivoLaboralNoGenBusiness.calcularInteresAdicional(idTipoPersonal, hastaAnio, hastaMes, usuario, proceso);
  }

  public void calcularInteresAdicionalMensual(long idTipoPersonal, int hastaAnio, int hastaMes, String usuario, String proceso) throws Exception {
    this.pasivoLaboralNoGenBusiness.calcularInteresAdicionalMensual(idTipoPersonal, hastaAnio, hastaMes, usuario, proceso);
  }

  public void calcularPrestacionesNuevoRegimen(long idTipoPersonal, int desdeAnio, int desdeMes, int hastaAnio, int hastaMes, String proceso) throws Exception {
    this.pasivoLaboralNoGenBusiness.calcularPrestacionesNuevoRegimen(idTipoPersonal, desdeAnio, desdeMes, hastaAnio, hastaMes, proceso);
  }

  public void calcularInteresesNuevoRegimen(long idTipoPersonal, int hastaAnio, int hastaMes, String proceso) throws Exception {
    this.pasivoLaboralNoGenBusiness.calcularInteresesNuevoRegimen(idTipoPersonal, hastaAnio, hastaMes, proceso);
  }

  public void liquidarTrabajador(long idTipoPersonal, long idTrabajador) throws Exception
  {
    this.pasivoLaboralNoGenBusiness.liquidarTrabajador(idTipoPersonal, idTrabajador);
  }

  public void registrarAnticipos(long idTipoPersonal, int anio, int mes, Date fechaAnticipo, double montoAnticipo, String tipoAnticipo, String formaAnticipo)
    throws Exception
  {
    this.pasivoLaboralNoGenBusiness.registrarAnticipos(idTipoPersonal, anio, mes, fechaAnticipo, montoAnticipo, tipoAnticipo, formaAnticipo);
  }

  public SeguridadInteresAdicional findSeguridadInteresAdicionalLast(long idTipoPersonal) throws Exception
  {
    try
    {
      this.txn.open();
      return this.pasivoLaboralNoGenBusiness.findSeguridadInteresAdicionalLast(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}