package sigefirrhh.personal.pasivoLaboral;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class HistoricoDevengadoIntegralBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HistoricoDevengadoIntegral historicoDevengadoIntegralNew = 
      (HistoricoDevengadoIntegral)BeanUtils.cloneBean(
      historicoDevengadoIntegral);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (historicoDevengadoIntegralNew.getPersonal() != null) {
      historicoDevengadoIntegralNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        historicoDevengadoIntegralNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(historicoDevengadoIntegralNew);
  }

  public void updateHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral) throws Exception
  {
    HistoricoDevengadoIntegral historicoDevengadoIntegralModify = 
      findHistoricoDevengadoIntegralById(historicoDevengadoIntegral.getIdHistoricoDevengadoIntegral());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (historicoDevengadoIntegral.getPersonal() != null) {
      historicoDevengadoIntegral.setPersonal(
        personalBeanBusiness.findPersonalById(
        historicoDevengadoIntegral.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(historicoDevengadoIntegralModify, historicoDevengadoIntegral);
  }

  public void deleteHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistoricoDevengadoIntegral historicoDevengadoIntegralDelete = 
      findHistoricoDevengadoIntegralById(historicoDevengadoIntegral.getIdHistoricoDevengadoIntegral());
    pm.deletePersistent(historicoDevengadoIntegralDelete);
  }

  public HistoricoDevengadoIntegral findHistoricoDevengadoIntegralById(long idHistoricoDevengadoIntegral) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistoricoDevengadoIntegral == pIdHistoricoDevengadoIntegral";
    Query query = pm.newQuery(HistoricoDevengadoIntegral.class, filter);

    query.declareParameters("long pIdHistoricoDevengadoIntegral");

    parameters.put("pIdHistoricoDevengadoIntegral", new Long(idHistoricoDevengadoIntegral));

    Collection colHistoricoDevengadoIntegral = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistoricoDevengadoIntegral.iterator();
    return (HistoricoDevengadoIntegral)iterator.next();
  }

  public Collection findHistoricoDevengadoIntegralAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historicoDevengadoIntegralExtent = pm.getExtent(
      HistoricoDevengadoIntegral.class, true);
    Query query = pm.newQuery(historicoDevengadoIntegralExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(HistoricoDevengadoIntegral.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colHistoricoDevengadoIntegral = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoDevengadoIntegral);

    return colHistoricoDevengadoIntegral;
  }
}