package sigefirrhh.personal.pasivoLaboral;

import java.io.Serializable;

public class HistoricoDevengadoIntegralPK
  implements Serializable
{
  public long idHistoricoDevengadoIntegral;

  public HistoricoDevengadoIntegralPK()
  {
  }

  public HistoricoDevengadoIntegralPK(long idHistoricoDevengadoIntegral)
  {
    this.idHistoricoDevengadoIntegral = idHistoricoDevengadoIntegral;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoDevengadoIntegralPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoDevengadoIntegralPK thatPK)
  {
    return 
      this.idHistoricoDevengadoIntegral == thatPK.idHistoricoDevengadoIntegral;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoDevengadoIntegral)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoDevengadoIntegral);
  }
}