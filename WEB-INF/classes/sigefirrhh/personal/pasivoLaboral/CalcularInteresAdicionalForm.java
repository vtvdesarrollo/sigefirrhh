package sigefirrhh.personal.pasivoLaboral;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class CalcularInteresAdicionalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CalcularInteresAdicionalForm.class.getName());
  private Collection listTipoPersonal;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private PasivoLaboralNoGenFacade pasivoLaboralNoGenFacade;
  private long idTipoPersonal;
  private String selectIdTipoPersonal;
  private TipoPersonal tipoPersonal;
  private String proceso;
  private int hastaAnio = 0;
  private int hastaMes = 0;

  public CalcularInteresAdicionalForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.pasivoLaboralNoGenFacade = new PasivoLaboralNoGenFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridadForInteresAdicional(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), "N");
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();

      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(this.idTipoPersonal);
    try {
      if (this.idTipoPersonal > 0L)
        this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(this.idTipoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.hastaAnio < 1997) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El año debe ser mayor a 1996", ""));
        return null;
      }

      if ((this.hastaAnio == 1997) && (this.hastaMes < 7)) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hasta el año 1997 el debe ser mayor a Junio", ""));
        return null;
      }

      this.pasivoLaboralNoGenFacade.calcularInteresAdicional(this.idTipoPersonal, this.hastaAnio, this.hastaMes, this.login.getUsuario(), this.proceso);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error : " + e, ""));
      return null;
    }

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }

  public String getProceso() {
    return this.proceso;
  }

  public void setProceso(String proceso) {
    this.proceso = proceso;
  }

  public int getHastaAnio() {
    return this.hastaAnio;
  }

  public void setHastaAnio(int hastaAnio) {
    this.hastaAnio = hastaAnio;
  }

  public int getHastaMes() {
    return this.hastaMes;
  }

  public void setHastaMes(int hastaMes) {
    this.hastaMes = hastaMes;
  }
}