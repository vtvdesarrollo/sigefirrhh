package sigefirrhh.personal.pasivoLaboral;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class PasivoLaboralBusiness extends AbstractBusiness
  implements Serializable
{
  private TasaBcvBeanBusiness tasaBcvBeanBusiness = new TasaBcvBeanBusiness();

  private SeguridadInteresAdicionalBeanBusiness seguridadInteresAdicionalBeanBusiness = new SeguridadInteresAdicionalBeanBusiness();

  private HistoricoDevengadoIntegralBeanBusiness historicoDevengadoIntegralBeanBusiness = new HistoricoDevengadoIntegralBeanBusiness();

  public void addTasaBcv(TasaBcv tasaBcv)
    throws Exception
  {
    this.tasaBcvBeanBusiness.addTasaBcv(tasaBcv);
  }

  public void updateTasaBcv(TasaBcv tasaBcv) throws Exception {
    this.tasaBcvBeanBusiness.updateTasaBcv(tasaBcv);
  }

  public void deleteTasaBcv(TasaBcv tasaBcv) throws Exception {
    this.tasaBcvBeanBusiness.deleteTasaBcv(tasaBcv);
  }

  public TasaBcv findTasaBcvById(long tasaBcvId) throws Exception {
    return this.tasaBcvBeanBusiness.findTasaBcvById(tasaBcvId);
  }

  public Collection findAllTasaBcv() throws Exception {
    return this.tasaBcvBeanBusiness.findTasaBcvAll();
  }

  public Collection findTasaBcvByAnio(int anio)
    throws Exception
  {
    return this.tasaBcvBeanBusiness.findByAnio(anio);
  }

  public void addSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional)
    throws Exception
  {
    this.seguridadInteresAdicionalBeanBusiness.addSeguridadInteresAdicional(seguridadInteresAdicional);
  }

  public void updateSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional) throws Exception {
    this.seguridadInteresAdicionalBeanBusiness.updateSeguridadInteresAdicional(seguridadInteresAdicional);
  }

  public void deleteSeguridadInteresAdicional(SeguridadInteresAdicional seguridadInteresAdicional) throws Exception {
    this.seguridadInteresAdicionalBeanBusiness.deleteSeguridadInteresAdicional(seguridadInteresAdicional);
  }

  public SeguridadInteresAdicional findSeguridadInteresAdicionalById(long seguridadInteresAdicionalId) throws Exception {
    return this.seguridadInteresAdicionalBeanBusiness.findSeguridadInteresAdicionalById(seguridadInteresAdicionalId);
  }

  public Collection findAllSeguridadInteresAdicional() throws Exception {
    return this.seguridadInteresAdicionalBeanBusiness.findSeguridadInteresAdicionalAll();
  }

  public Collection findSeguridadInteresAdicionalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.seguridadInteresAdicionalBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findSeguridadInteresAdicionalByAnio(int anio)
    throws Exception
  {
    return this.seguridadInteresAdicionalBeanBusiness.findByAnio(anio);
  }

  public void addHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral)
    throws Exception
  {
    this.historicoDevengadoIntegralBeanBusiness.addHistoricoDevengadoIntegral(historicoDevengadoIntegral);
  }

  public void updateHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral) throws Exception {
    this.historicoDevengadoIntegralBeanBusiness.updateHistoricoDevengadoIntegral(historicoDevengadoIntegral);
  }

  public void deleteHistoricoDevengadoIntegral(HistoricoDevengadoIntegral historicoDevengadoIntegral) throws Exception {
    this.historicoDevengadoIntegralBeanBusiness.deleteHistoricoDevengadoIntegral(historicoDevengadoIntegral);
  }

  public HistoricoDevengadoIntegral findHistoricoDevengadoIntegralById(long historicoDevengadoIntegralId) throws Exception {
    return this.historicoDevengadoIntegralBeanBusiness.findHistoricoDevengadoIntegralById(historicoDevengadoIntegralId);
  }

  public Collection findAllHistoricoDevengadoIntegral() throws Exception {
    return this.historicoDevengadoIntegralBeanBusiness.findHistoricoDevengadoIntegralAll();
  }

  public Collection findHistoricoDevengadoIntegralByPersonal(long idPersonal)
    throws Exception
  {
    return this.historicoDevengadoIntegralBeanBusiness.findByPersonal(idPersonal);
  }
}