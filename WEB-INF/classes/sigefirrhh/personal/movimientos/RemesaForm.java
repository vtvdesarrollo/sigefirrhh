package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.SistemaFacade;

public class RemesaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RemesaForm.class.getName());
  private Remesa remesa;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private MovimientosNoGenFacade movimientosFacade = new MovimientosNoGenFacade();
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private boolean showRemesaByAnio;
  private boolean showRemesaByNumero;
  private boolean showRemesaByEstatus;
  private int findAnio;
  private int findNumero;
  private String findEstatus;
  private Object stateResultRemesaByAnio = null;

  private Object stateResultRemesaByNumero = null;

  private Object stateResultRemesaByEstatus = null;

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }
  public int getFindNumero() {
    return this.findNumero;
  }
  public void setFindNumero(int findNumero) {
    this.findNumero = findNumero;
  }
  public String getFindEstatus() {
    return this.findEstatus;
  }
  public void setFindEstatus(String findEstatus) {
    this.findEstatus = findEstatus;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Remesa getRemesa() {
    if (this.remesa == null) {
      this.remesa = new Remesa();
    }
    return this.remesa;
  }

  public RemesaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Remesa.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findRemesaByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.movimientosFacade.findRemesaByAnio(this.findAnio, idOrganismo);
      this.showRemesaByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRemesaByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findNumero = 0;
    this.findEstatus = null;

    return null;
  }

  public String findRemesaByNumero()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.movimientosFacade.findRemesaByNumero(this.findNumero, idOrganismo);
      this.showRemesaByNumero = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRemesaByNumero)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findNumero = 0;
    this.findEstatus = null;

    return null;
  }

  public String findRemesaByEstatus()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.movimientosFacade.findRemesaByEstatus(this.findEstatus, idOrganismo);
      this.showRemesaByEstatus = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRemesaByEstatus)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;
    this.findNumero = 0;
    this.findEstatus = null;

    return null;
  }

  public boolean isShowRemesaByAnio() {
    return this.showRemesaByAnio;
  }
  public boolean isShowRemesaByNumero() {
    return this.showRemesaByNumero;
  }
  public boolean isShowRemesaByEstatus() {
    return this.showRemesaByEstatus;
  }

  public String selectRemesa()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idRemesa = 
      Long.parseLong((String)requestParameterMap.get("idRemesa"));
    try
    {
      this.remesa = 
        this.movimientosFacade.findRemesaById(
        idRemesa);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.remesa = null;
    this.showRemesaByAnio = false;
    this.showRemesaByNumero = false;
    this.showRemesaByEstatus = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.remesa.getTiempoSitp() != null) && 
      (this.remesa.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.remesa.setUsuario(this.login.getUsuarioObject());
        this.remesa.setEstatus("A");
        this.remesa.setNumero(this.movimientosFacade.findLastNumeroRemesa(this.login.getIdOrganismo(), this.remesa.getAnio()) + 1);
        this.movimientosFacade.addRemesa(
          this.remesa);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.remesa);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.movimientosFacade.updateRemesa(
          this.remesa);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.remesa);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.movimientosFacade.deleteRemesa(
        this.remesa);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.remesa);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.remesa = new Remesa();

    this.remesa.setOrganismo(
      this.login.getOrganismo());
    this.remesa.setFechaCreacion(new Date());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.remesa.setIdRemesa(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.movimientos.Remesa"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.remesa = new Remesa();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}