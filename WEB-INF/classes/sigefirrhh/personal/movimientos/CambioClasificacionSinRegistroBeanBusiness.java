package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBusiness;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CambioClasificacionSinRegistroBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(CambioClasificacionSinRegistroBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private CargoBusiness cargoBusiness = new CargoBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();

  public boolean actualizar(long idTrabajador, long idCargo, long idCausaMovimiento, int numeroMovimiento, Date fechaVigencia, Organismo organismo2, long idUsuario, String observaciones)
    throws Exception
  {
    this.txn.open();
    PersistenceManager pm = PMThread.getPM();

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();
    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Cargo cargo = this.cargoBusiness.findCargoById(idCargo);

    Trabajador trabajador = new Trabajador();
    trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaVigencia, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        null, null, null, observaciones, usuario.getUsuario(), "4");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      RegistroSitp registroSitp = new RegistroSitp();
      registroSitp = this.registrosBusiness.agregarRegistroSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, null, 
        fechaVigencia, "0", "S", 
        usuario, null, null, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones);
      pm.makePersistent(registroSitp);

      Trabajador trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

      trabajador.setCargo(cargo);

      this.txn.close();
    }
    catch (ErrorSistema a) {
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return true;
  }
}