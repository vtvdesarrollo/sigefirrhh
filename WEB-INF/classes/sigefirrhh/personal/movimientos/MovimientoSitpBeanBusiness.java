package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonalBeanBusiness;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.definiciones.TurnoBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaMovimientoBeanBusiness;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.UsuarioBeanBusiness;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class MovimientoSitpBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(MovimientoSitpBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  public void addMovimientoSitp(MovimientoSitp movimientoSitp)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    MovimientoSitp movimientoSitpNew = (MovimientoSitp)BeanUtils.cloneBean(movimientoSitp);

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (movimientoSitpNew.getClasificacionPersonal() != null) {
      movimientoSitpNew.setClasificacionPersonal(clasificacionPersonalBeanBusiness.findClasificacionPersonalById(movimientoSitpNew.getClasificacionPersonal().getIdClasificacionPersonal()));
    }
    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (movimientoSitpNew.getTurno() != null) {
      movimientoSitpNew.setTurno(turnoBeanBusiness.findTurnoById(movimientoSitpNew.getTurno().getIdTurno()));
    }
    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (movimientoSitpNew.getCausaMovimiento() != null) {
      movimientoSitpNew.setCausaMovimiento(causaMovimientoBeanBusiness.findCausaMovimientoById(movimientoSitpNew.getCausaMovimiento().getIdCausaMovimiento()));
    }
    RemesaBeanBusiness remesaBeanBusiness = new RemesaBeanBusiness();

    if (movimientoSitpNew.getRemesa() != null) {
      movimientoSitpNew
        .setRemesa(remesaBeanBusiness.findRemesaById(movimientoSitpNew.getRemesa().getIdRemesa()));
    }
    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (movimientoSitpNew.getPersonal() != null) {
      movimientoSitpNew.setPersonal(personalBeanBusiness.findPersonalById(movimientoSitpNew.getPersonal().getIdPersonal()));
    }
    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (movimientoSitpNew.getOrganismo() != null) {
      movimientoSitpNew.setOrganismo(organismoBeanBusiness.findOrganismoById(movimientoSitpNew.getOrganismo().getIdOrganismo()));
    }
    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (movimientoSitpNew.getUsuario() != null) {
      movimientoSitpNew.setUsuario(usuarioBeanBusiness.findUsuarioById(movimientoSitpNew.getUsuario().getIdUsuario()));
    }
    pm.makePersistent(movimientoSitpNew);
  }

  public long addMovimientoSitp(MovimientoSitp movimientoSitp, Date fechaMovimiento, String codCausaMovimiento, int numeroMovimiento, Date fechaPuntoCuenta, String puntoCuenta, String observaciones)
    throws Exception
  {
    this.registrosBusiness.validarFechas(fechaMovimiento, fechaPuntoCuenta);

    this.txn.open();
    long id = 0L;

    PersistenceManager pm = PMThread.getPM();
    CausaMovimiento causaMovimiento = new CausaMovimiento();
    causaMovimiento = (CausaMovimiento)this.registroBusiness.findCausaMovimientoByCodCausaMovimiento(codCausaMovimiento).iterator().next();

    Trabajador trabajador = new Trabajador();
    trabajador = (Trabajador)this.trabajadorBusiness.findTrabajadorByCedula(movimientoSitp.getCedula(), movimientoSitp.getOrganismo().getIdOrganismo()).iterator().next();

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(
      trabajador.getCedula(), 
      causaMovimiento.getCodCausaMovimiento(), fechaMovimiento);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }
    this.log.error("no encontró problemas en trayectoria");

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }
    try {
      this.txn.open();
      pm = PMThread.getPM();

      Trayectoria trayectoria = new Trayectoria();

      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, 
        sueldoPromedio, new Date(), numeroMovimiento, 
        fechaMovimiento, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 
        0.0D, 0.0D, 0.0D, 0.0D, 0.0D, puntoCuenta, fechaPuntoCuenta, 
        null, observaciones, movimientoSitp.getUsuario()
        .getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA EN ANULACION");

      MovimientoSitp movimientoSitpNew = (MovimientoSitp)BeanUtils.cloneBean(movimientoSitp);
      movimientoSitpNew.setNumeroMovimiento(numeroMovimiento);
      movimientoSitpNew.setFechaMovimiento(fechaMovimiento);
      movimientoSitpNew.setFechaPuntoCuenta(fechaPuntoCuenta);
      movimientoSitpNew.setPuntoCuenta(puntoCuenta);
      movimientoSitpNew.setObservaciones(observaciones);
      movimientoSitpNew.setIdMovimientoSitp(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.movimientos.MovimientoSitp"));

      pm.makePersistent(movimientoSitpNew);
      id = movimientoSitpNew.getIdMovimientoSitp();
      this.log.error("GRABO MOVIMIENTOSITP EN ANULACION");

      trabajador.setEstatus("S");
      this.trabajadorBusiness.updateTrabajador(trabajador);
      this.log.error("GRABO TRABAJADOR EN ANULACION");

      this.txn.close();
    } catch (ErrorSistema a) {
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.rollback();
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return id;
  }

  public void updateMovimientoSitp(MovimientoSitp movimientoSitp) throws Exception
  {
    MovimientoSitp movimientoSitpModify = 
      findMovimientoSitpById(movimientoSitp.getIdMovimientoSitp());

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (movimientoSitp.getClasificacionPersonal() != null) {
      movimientoSitp
        .setClasificacionPersonal(clasificacionPersonalBeanBusiness
        .findClasificacionPersonalById(movimientoSitp
        .getClasificacionPersonal()
        .getIdClasificacionPersonal()));
    }
    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (movimientoSitp.getTurno() != null) {
      movimientoSitp.setTurno(turnoBeanBusiness
        .findTurnoById(movimientoSitp.getTurno().getIdTurno()));
    }
    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (movimientoSitp.getCausaMovimiento() != null) {
      movimientoSitp.setCausaMovimiento(causaMovimientoBeanBusiness
        .findCausaMovimientoById(movimientoSitp
        .getCausaMovimiento().getIdCausaMovimiento()));
    }
    RemesaBeanBusiness remesaBeanBusiness = new RemesaBeanBusiness();

    if (movimientoSitp.getRemesa() != null) {
      movimientoSitp.setRemesa(remesaBeanBusiness
        .findRemesaById(movimientoSitp.getRemesa().getIdRemesa()));
    }
    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (movimientoSitp.getPersonal() != null) {
      movimientoSitp.setPersonal(personalBeanBusiness
        .findPersonalById(movimientoSitp.getPersonal()
        .getIdPersonal()));
    }
    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (movimientoSitp.getOrganismo() != null) {
      movimientoSitp.setOrganismo(organismoBeanBusiness
        .findOrganismoById(movimientoSitp.getOrganismo()
        .getIdOrganismo()));
    }
    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (movimientoSitp.getUsuario() != null) {
      movimientoSitp
        .setUsuario(usuarioBeanBusiness
        .findUsuarioById(movimientoSitp.getUsuario()
        .getIdUsuario()));
    }

    BeanUtils.copyProperties(movimientoSitpModify, movimientoSitp);

    if (movimientoSitp.getEstatus().trim().equalsIgnoreCase("4")) {
      Trayectoria trayec = null;
      trayec = (Trayectoria)this.trayectoriaBeanBusiness.findByAnioAndNumeroMovimiento(movimientoSitp.getAnio(), movimientoSitp.getNumeroMovimiento()).iterator().next();
      if (trayec != null) {
        trayec.setEstatus(movimientoSitp.getEstatus());
        this.trayectoriaBeanBusiness.updateTrayectoria(trayec);
      }
    }
  }

  public void deleteMovimientoSitp(MovimientoSitp movimientoSitp) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    MovimientoSitp movimientoSitpDelete = 
      findMovimientoSitpById(movimientoSitp.getIdMovimientoSitp());
    pm.deletePersistent(movimientoSitpDelete);
  }

  public MovimientoSitp findMovimientoSitpById(long idMovimientoSitp) throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idMovimientoSitp == pIdMovimientoSitp";
    Query query = pm.newQuery(MovimientoSitp.class, filter);

    query.declareParameters("long pIdMovimientoSitp");

    parameters.put("pIdMovimientoSitp", 
      new Long(idMovimientoSitp));

    Collection colMovimientoSitp = new ArrayList(
      (Collection)query
      .executeWithMap(parameters));

    Iterator iterator = colMovimientoSitp.iterator();
    return (MovimientoSitp)iterator.next();
  }

  public MovimientoSitp findMovimientoSitpByIdPersonal(long idPersonal)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.idPersonal == pIdPersonal";
    Query query = pm.newQuery(MovimientoSitp.class, filter);

    query.declareParameters("long pIdPersonal");

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colMovimientoSitp = new ArrayList(
      (Collection)query
      .executeWithMap(parameters));

    Iterator iterator = colMovimientoSitp.iterator();
    if (iterator.hasNext())
      return (MovimientoSitp)iterator.next();
    this.log.error("Advertencia: No se encontro MovimientoSitp para el idPersonal = " + idPersonal);
    return null;
  }

  public Collection findMovimientoSitpAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent movimientoSitpExtent = pm.getExtent(MovimientoSitp.class, true);
    Query query = pm.newQuery(movimientoSitpExtent);
    query.setOrdering("fechaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(MovimientoSitp.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colMovimientoSitp = new ArrayList(
      (Collection)query
      .executeWithMap(parameters));

    pm.makeTransientAll(colMovimientoSitp);

    return colMovimientoSitp;
  }
}