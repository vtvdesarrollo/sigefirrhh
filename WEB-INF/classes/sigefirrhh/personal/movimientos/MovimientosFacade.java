package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;

public class MovimientosFacade extends AbstractFacade
  implements Serializable
{
  static Logger log = Logger.getLogger(MovimientosFacade.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private MovimientosBusiness movimientosBusiness = new MovimientosBusiness();

  public void addMovimientoSitp(MovimientoSitp movimientoSitp)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.movimientosBusiness.addMovimientoSitp(movimientoSitp);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public long addMovimientoSitp(MovimientoSitp movimientoSitp, Date fechaMovimiento, String codCausaMovimiento, int numeroMovimiento, Date fechaPuntoCuenta, String puntoCuenta, String observaciones) throws Exception
  {
    try {
      this.txn.open();
      return this.movimientosBusiness.addMovimientoSitp(movimientoSitp, fechaMovimiento, codCausaMovimiento, numeroMovimiento, fechaPuntoCuenta, puntoCuenta, observaciones);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateMovimientoSitp(MovimientoSitp movimientoSitp)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.movimientosBusiness.updateMovimientoSitp(movimientoSitp);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteMovimientoSitp(MovimientoSitp movimientoSitp) throws Exception
  {
    try { this.txn.open();
      this.movimientosBusiness.deleteMovimientoSitp(movimientoSitp);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public MovimientoSitp findMovimientoSitpById(long movimientoSitpId) throws Exception
  {
    try { this.txn.open();
      MovimientoSitp movimientoSitp = 
        this.movimientosBusiness.findMovimientoSitpById(movimientoSitpId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(movimientoSitp);
      return movimientoSitp;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllMovimientoSitp() throws Exception
  {
    try { this.txn.open();
      return this.movimientosBusiness.findAllMovimientoSitp();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientoSitpByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.movimientosBusiness.findMovimientoSitpByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRegistroSitp(RegistroSitp registroSitp)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.movimientosBusiness.addRegistroSitp(registroSitp);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegistroSitp(RegistroSitp registroSitp) throws Exception
  {
    try { this.txn.open();
      this.movimientosBusiness.updateRegistroSitp(registroSitp);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegistroSitp(RegistroSitp registroSitp) throws Exception
  {
    try { this.txn.open();
      this.movimientosBusiness.deleteRegistroSitp(registroSitp);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RegistroSitp findRegistroSitpById(long registroSitpId) throws Exception
  {
    try { this.txn.open();
      RegistroSitp registroSitp = 
        this.movimientosBusiness.findRegistroSitpById(registroSitpId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(registroSitp);
      return registroSitp;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegistroSitp() throws Exception
  {
    try { this.txn.open();
      return this.movimientosBusiness.findAllRegistroSitp();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroSitpByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.movimientosBusiness.findRegistroSitpByPersonal(idPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRemesa(Remesa remesa)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.movimientosBusiness.addRemesa(remesa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRemesa(Remesa remesa) throws Exception
  {
    try { this.txn.open();
      this.movimientosBusiness.updateRemesa(remesa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRemesa(Remesa remesa) throws Exception
  {
    try { this.txn.open();
      this.movimientosBusiness.deleteRemesa(remesa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Remesa findRemesaById(long remesaId) throws Exception
  {
    try { this.txn.open();
      Remesa remesa = 
        this.movimientosBusiness.findRemesaById(remesaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(remesa);
      return remesa;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRemesa() throws Exception
  {
    try { this.txn.open();
      return this.movimientosBusiness.findAllRemesa();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRemesaByAnio(int anio, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.movimientosBusiness.findRemesaByAnio(anio, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRemesaByNumero(int numero, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.movimientosBusiness.findRemesaByNumero(numero, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRemesaByEstatus(String estatus, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.movimientosBusiness.findRemesaByEstatus(estatus, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}