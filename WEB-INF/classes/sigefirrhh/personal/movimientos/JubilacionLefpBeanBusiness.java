package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class JubilacionLefpBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(JubilacionLefpBeanBusiness.class.getName());

  private TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  public long actualizar(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Organismo organismo2, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, double montoJubilacion, double porcentajeJubilacion, double sueldoPromedioJubilacion)
    throws Exception
  {
    long idMovimientoSitp = 0L;
    this.registrosBusiness.validarFechas(fechaMovimiento, fechaPuntoCuenta);

    this.txn.open();

    PersistenceManager pm = PMThread.getPM();

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);
    this.log.error("id_causa_movimiento................................" + idCausaMovimiento);
    this.log.error("causa_movimiento..................................." + causaMovimiento);
    Trabajador trabajadorEdit = new Trabajador();
    trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(trabajadorEdit.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaMovimiento);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }
    this.log.error("no encontró problemas en trayectoria");

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajadorEdit.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }

    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      if (idCausaMovimiento == 26L) {
        trabajadorEdit.setEstatus("S");
      } else if (idCausaMovimiento == 27L) {
        trabajadorEdit.setEstatus("A");
      } else if (idCausaMovimiento == 28L) {
        trabajadorEdit.setEstatus("E");
        trabajadorEdit.setFechaEgreso(fechaMovimiento);
      }
      if (idCausaMovimiento == 25L)
      {
        Collection colConceptoFijo = this.trabajadorBusiness.findConceptoFijoByTrabajadorAndCodConcepto(idTrabajador, "0010");
        ConceptoFijo conceptoFijo = (ConceptoFijo)colConceptoFijo.iterator().next();
        conceptoFijo.setMonto(montoJubilacion);
        this.trabajadorBusiness.updateConceptoFijo(conceptoFijo);
      }

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajadorEdit, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaMovimiento, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, null, observaciones, usuario.getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajadorEdit, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, null, 
        fechaMovimiento, null, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones, montoJubilacion, sueldoPromedioJubilacion, porcentajeJubilacion);
      pm.makePersistent(movimientoSitp);

      idMovimientoSitp = movimientoSitp.getIdMovimientoSitp();

      this.log.error("GRABO MOVIMIENTOSITP");

      if (organismo.getAprobacionMpd().equals("S"))
        trabajadorEdit.setMovimiento("T");
      else {
        trabajadorEdit.setMovimiento("A");
      }

      this.log.error("PASO no 5");

      this.txn.close();
      pm.close();
    } catch (ErrorSistema a) {
      this.txn.rollback();
      this.txn.close();
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e) {
      this.txn.rollback();
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return idMovimientoSitp;
  }

  public long ajustar_pension(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Organismo organismo2, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, double montoJubilacion, double porcentajeJubilacion, double sueldoPromedioJubilacion)
    throws Exception
  {
    long idMovimientoSitp = 0L;
    this.registrosBusiness.validarFechas(fechaMovimiento, fechaPuntoCuenta);

    this.txn.open();

    PersistenceManager pm = PMThread.getPM();

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);
    this.log.error("id_causa_movimiento................................" + idCausaMovimiento);
    this.log.error("causa_movimiento..................................." + causaMovimiento);
    Trabajador trabajadorEdit = new Trabajador();
    trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(trabajadorEdit.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaMovimiento);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }
    this.log.error("no encontró problemas en trayectoria");

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajadorEdit.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }

    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      if (idCausaMovimiento == 26L) {
        trabajadorEdit.setEstatus("S");
      } else if (idCausaMovimiento == 27L) {
        trabajadorEdit.setEstatus("A");
      } else if (idCausaMovimiento == 28L) {
        trabajadorEdit.setEstatus("E");
        trabajadorEdit.setFechaEgreso(fechaMovimiento);
      }
      if (idCausaMovimiento == 25L)
      {
        Collection colConceptoFijo = this.trabajadorBusiness.findConceptoFijoByTrabajadorAndCodConcepto(idTrabajador, "0011");
        ConceptoFijo conceptoFijo = (ConceptoFijo)colConceptoFijo.iterator().next();
        conceptoFijo.setMonto(montoJubilacion);
        this.trabajadorBusiness.updateConceptoFijo(conceptoFijo);
      }

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajadorEdit, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaMovimiento, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, null, observaciones, usuario.getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajadorEdit, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, null, 
        fechaMovimiento, null, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones, montoJubilacion, sueldoPromedioJubilacion, porcentajeJubilacion);
      pm.makePersistent(movimientoSitp);

      idMovimientoSitp = movimientoSitp.getIdMovimientoSitp();

      this.log.error("GRABO MOVIMIENTOSITP");

      if (organismo.getAprobacionMpd().equals("S"))
        trabajadorEdit.setMovimiento("T");
      else {
        trabajadorEdit.setMovimiento("A");
      }

      this.log.error("PASO no 5");

      this.txn.close();
      pm.close();
    } catch (ErrorSistema a) {
      this.txn.rollback();
      this.txn.close();
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e) {
      this.txn.rollback();
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return idMovimientoSitp;
  }
}