package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonalBeanBusiness;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.definiciones.TurnoBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaMovimientoBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.UsuarioBeanBusiness;

public class RegistroSitpBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(RegistroSitpBeanBusiness.class.getName());

  public void addRegistroSitp(RegistroSitp registroSitp)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RegistroSitp registroSitpNew = 
      (RegistroSitp)BeanUtils.cloneBean(
      registroSitp);

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (registroSitpNew.getClasificacionPersonal() != null) {
      registroSitpNew.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        registroSitpNew.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (registroSitpNew.getTurno() != null) {
      registroSitpNew.setTurno(
        turnoBeanBusiness.findTurnoById(
        registroSitpNew.getTurno().getIdTurno()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (registroSitpNew.getCausaMovimiento() != null) {
      registroSitpNew.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        registroSitpNew.getCausaMovimiento().getIdCausaMovimiento()));
    }

    RemesaBeanBusiness remesaBeanBusiness = new RemesaBeanBusiness();

    if (registroSitpNew.getRemesa() != null) {
      registroSitpNew.setRemesa(
        remesaBeanBusiness.findRemesaById(
        registroSitpNew.getRemesa().getIdRemesa()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (registroSitpNew.getPersonal() != null) {
      registroSitpNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        registroSitpNew.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (registroSitpNew.getOrganismo() != null) {
      registroSitpNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        registroSitpNew.getOrganismo().getIdOrganismo()));
    }

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (registroSitpNew.getUsuario() != null) {
      registroSitpNew.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        registroSitpNew.getUsuario().getIdUsuario()));
    }
    pm.makePersistent(registroSitpNew);
  }

  public void updateRegistroSitp(RegistroSitp registroSitp) throws Exception
  {
    RegistroSitp registroSitpModify = 
      findRegistroSitpById(registroSitp.getIdRegistroSitp());

    ClasificacionPersonalBeanBusiness clasificacionPersonalBeanBusiness = new ClasificacionPersonalBeanBusiness();

    if (registroSitp.getClasificacionPersonal() != null) {
      registroSitp.setClasificacionPersonal(
        clasificacionPersonalBeanBusiness.findClasificacionPersonalById(
        registroSitp.getClasificacionPersonal().getIdClasificacionPersonal()));
    }

    TurnoBeanBusiness turnoBeanBusiness = new TurnoBeanBusiness();

    if (registroSitp.getTurno() != null) {
      registroSitp.setTurno(
        turnoBeanBusiness.findTurnoById(
        registroSitp.getTurno().getIdTurno()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (registroSitp.getCausaMovimiento() != null) {
      registroSitp.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        registroSitp.getCausaMovimiento().getIdCausaMovimiento()));
    }

    RemesaBeanBusiness remesaBeanBusiness = new RemesaBeanBusiness();

    if (registroSitp.getRemesa() != null) {
      registroSitp.setRemesa(
        remesaBeanBusiness.findRemesaById(
        registroSitp.getRemesa().getIdRemesa()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (registroSitp.getPersonal() != null) {
      registroSitp.setPersonal(
        personalBeanBusiness.findPersonalById(
        registroSitp.getPersonal().getIdPersonal()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (registroSitp.getOrganismo() != null) {
      registroSitp.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        registroSitp.getOrganismo().getIdOrganismo()));
    }

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (registroSitp.getUsuario() != null) {
      registroSitp.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        registroSitp.getUsuario().getIdUsuario()));
    }

    BeanUtils.copyProperties(registroSitpModify, registroSitp);
  }

  public void deleteRegistroSitp(RegistroSitp registroSitp) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RegistroSitp registroSitpDelete = 
      findRegistroSitpById(registroSitp.getIdRegistroSitp());
    pm.deletePersistent(registroSitpDelete);
  }

  public RegistroSitp findRegistroSitpById(long idRegistroSitp) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegistroSitp == pIdRegistroSitp";
    Query query = pm.newQuery(RegistroSitp.class, filter);

    query.declareParameters("long pIdRegistroSitp");

    parameters.put("pIdRegistroSitp", new Long(idRegistroSitp));

    Collection colRegistroSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegistroSitp.iterator();
    return (RegistroSitp)iterator.next();
  }

  public RegistroSitp findRegistroSitpByIdPersonal(long idPersonal)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "personal.idPersonal == pIdPersonal";
    Query query = pm.newQuery(RegistroSitp.class, filter);

    query.declareParameters("long pIdPersonal");

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colRegistroSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegistroSitp.iterator();
    if (iterator.hasNext())
      return (RegistroSitp)iterator.next();
    this.log.error("Advertencia: No se encontro RegistroSitp para el idPersonal = " + idPersonal);
    return null;
  }

  public Collection findRegistroSitpAll()
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent registroSitpExtent = pm.getExtent(
      RegistroSitp.class, true);
    Query query = pm.newQuery(registroSitpExtent);
    query.setOrdering("fechaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(RegistroSitp.class, filter);

    query.declareParameters("long pIdPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaMovimiento ascending");

    Collection colRegistroSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroSitp);

    return colRegistroSitp;
  }
}