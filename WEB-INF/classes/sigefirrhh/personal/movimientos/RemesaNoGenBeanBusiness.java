package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.xml.namespace.QName;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.apache.log4j.Logger;
import org.mpd.sitp.model.AccionRemesa;
import org.mpd.sitp.model.Analista;
import org.mpd.sitp.model.MovimientoSitpPK;
import org.mpd.sitp.model.Opcion;
import org.mpd.sitp.model.Organismo;
import org.mpd.sitp.model.RemesaPK;
import org.mpd.sitp.model.Rol;
import org.mpd.sitp.model.TrayectoriaPK;
import sigefirrhh.personal.expediente.ExpedienteNoGenBusiness;

public class RemesaNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(RemesaNoGenBeanBusiness.class.getName());

  public void enviarRemesa(Remesa remesa)
    throws Exception
  {
  }

  public void recibirRemesa(long idOrganismo)
    throws Exception
  {
    TxnManager txn = TxnManagerFactory.makeTransactionManager();
    MovimientoSitpNoGenBeanBusiness movimientoSitpBeanBusiness = new MovimientoSitpNoGenBeanBusiness();
    ExpedienteNoGenBusiness expedienteBusiness = new ExpedienteNoGenBusiness();

    Service service = new Service();
    Call call = (Call)service.createCall();

    String endpoint = "http://cforero:8080/sitp/services/ServiceSigefirrhh";
    call.setTargetEndpointAddress(new URL(endpoint));
    call.setOperationName(new QName("sendRemesas"));

    QName qn = new QName("urn:ServiceSigefirrhh", "Remesa");
    call.registerTypeMapping(org.mpd.sitp.model.Remesa.class, qn, 
      new BeanSerializerFactory(org.mpd.sitp.model.Remesa.class, qn), 
      new BeanDeserializerFactory(org.mpd.sitp.model.Remesa.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "RemesaPK");
    call.registerTypeMapping(RemesaPK.class, qn, 
      new BeanSerializerFactory(RemesaPK.class, qn), 
      new BeanDeserializerFactory(RemesaPK.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "Organismo");
    call.registerTypeMapping(Organismo.class, qn, 
      new BeanSerializerFactory(Organismo.class, qn), 
      new BeanDeserializerFactory(Organismo.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "MovimientoSitp");
    call.registerTypeMapping(org.mpd.sitp.model.MovimientoSitp.class, qn, 
      new BeanSerializerFactory(org.mpd.sitp.model.MovimientoSitp.class, qn), 
      new BeanDeserializerFactory(org.mpd.sitp.model.MovimientoSitp.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "MovimientoSitpPK");
    call.registerTypeMapping(MovimientoSitpPK.class, qn, 
      new BeanSerializerFactory(MovimientoSitpPK.class, qn), 
      new BeanDeserializerFactory(MovimientoSitpPK.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "Trayectoria");
    call.registerTypeMapping(org.mpd.sitp.model.Trayectoria.class, qn, 
      new BeanSerializerFactory(org.mpd.sitp.model.Trayectoria.class, qn), 
      new BeanDeserializerFactory(org.mpd.sitp.model.Trayectoria.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "TrayectoriaPK");
    call.registerTypeMapping(TrayectoriaPK.class, qn, 
      new BeanSerializerFactory(TrayectoriaPK.class, qn), 
      new BeanDeserializerFactory(TrayectoriaPK.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "Analista");
    call.registerTypeMapping(Analista.class, qn, 
      new BeanSerializerFactory(Analista.class, qn), 
      new BeanDeserializerFactory(Analista.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "Rol");
    call.registerTypeMapping(Rol.class, qn, 
      new BeanSerializerFactory(Rol.class, qn), 
      new BeanDeserializerFactory(Rol.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "AccionRemesa");
    call.registerTypeMapping(AccionRemesa.class, qn, 
      new BeanSerializerFactory(AccionRemesa.class, qn), 
      new BeanDeserializerFactory(AccionRemesa.class, qn));
    qn = new QName("urn:ServiceSigefirrhh", "Opcion");
    call.registerTypeMapping(Opcion.class, qn, 
      new BeanSerializerFactory(Opcion.class, qn), 
      new BeanDeserializerFactory(Opcion.class, qn));

    Object[] remesas = 
      (Object[])call.invoke(new Object[] { 
      new Long(idOrganismo) });

    org.mpd.sitp.model.Remesa remesa = null;
    org.mpd.sitp.model.MovimientoSitp movimientoSitpMpd = null;
    MovimientoSitp movimientoSitp = null;

    org.mpd.sitp.model.Trayectoria trayectoriaMpd = null;
    sigefirrhh.personal.expediente.Trayectoria trayectoria = null;

    txn.open();
    for (int i = 0; i < remesas.length; i++) {
      remesa = (org.mpd.sitp.model.Remesa)remesas[i];
      Collection colMovimientoSitp = remesa.getMovimientosSitp();
      Iterator iterMovimientoSitp = colMovimientoSitp.iterator();
      while (iterMovimientoSitp.hasNext())
      {
        movimientoSitpMpd = (org.mpd.sitp.model.MovimientoSitp)iterMovimientoSitp.next();
        movimientoSitp = movimientoSitpBeanBusiness.findMovimientoSitpById(movimientoSitpMpd.getMovimientoSitpPK().getIdMovimientoSitp());
        this.log.error("id_movimientoSitp" + movimientoSitpMpd.getMovimientoSitpPK().getIdMovimientoSitp());
        if (movimientoSitpMpd.getEstatusMovimiento().equals(org.mpd.sitp.model.MovimientoSitp.APROBADO))
          movimientoSitp.setEstatus("4");
        else if ((movimientoSitpMpd.getEstatusMovimiento().equals(org.mpd.sitp.model.MovimientoSitp.RECHAZADO_ANALISTA)) || (movimientoSitpMpd.getEstatusMovimiento().equals(org.mpd.sitp.model.MovimientoSitp.RECHAZADO_SISTEMA))) {
          movimientoSitp.setEstatus("3");
        }
        movimientoSitpBeanBusiness.updateMovimientoSitp(movimientoSitp);
      }

      Collection colTrayectoria = remesa.getTrayectorias();
      Iterator iterTrayectoria = colTrayectoria.iterator();
      while (iterTrayectoria.hasNext())
      {
        trayectoriaMpd = (org.mpd.sitp.model.Trayectoria)iterTrayectoria.next();
        trayectoria = expedienteBusiness.findTrayectoriaById(trayectoriaMpd.getTrayectoriaPK().idTrayectoria);
        this.log.error("id_trayectoria" + trayectoriaMpd.getTrayectoriaPK().idTrayectoria);
        if (trayectoriaMpd.getEstatusTrayectoria().equals(org.mpd.sitp.model.Trayectoria.APROBADO))
          trayectoria.setEstatus("4");
        else if ((trayectoriaMpd.getEstatusTrayectoria().equals(org.mpd.sitp.model.Trayectoria.RECHAZADO_ANALISTA)) || (trayectoriaMpd.getEstatusTrayectoria().equals(org.mpd.sitp.model.Trayectoria.RECHAZADO_SISTEMA))) {
          trayectoria.setEstatus("3");
        }
        expedienteBusiness.updateTrayectoria(trayectoria);
      }

    }

    txn.close();
  }

  public int findLastNumero(long idOrganismo, int anio)
    throws Exception
  {
    ResultSet rsRegistro = null;
    PreparedStatement stRegistro = null;
    Connection connection = Resource.getConnection();
    connection.setAutoCommit(true);

    StringBuffer sql = new StringBuffer();
    sql.append("select max(numero) as ultimo from remesa ");
    sql.append(" where id_organismo = ? and anio = ?");
    stRegistro = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);

    stRegistro.setLong(1, idOrganismo);
    stRegistro.setInt(2, anio);
    rsRegistro = stRegistro.executeQuery();
    rsRegistro.next();
    return rsRegistro.getInt("ultimo");
  }

  public Collection findByEstatus(long idOrganismo, String estatus, long idUsuario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && estatus == pEstatus && usuario.idUsuario == pIdUsuario";

    Query query = pm.newQuery(Remesa.class, filter);

    query.declareParameters("long pIdOrganismo, String pEstatus, long pIdUsuario");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));
    parameters.put("pIdUsuario", new Long(idUsuario));
    parameters.put("pEstatus", estatus);

    query.setOrdering("anio descending, numero ascending");

    Collection colRemesa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRemesa);

    return colRemesa;
  }
}