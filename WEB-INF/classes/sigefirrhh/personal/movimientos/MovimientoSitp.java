package sigefirrhh.personal.movimientos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.MovimientoPersonal;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.sistema.Usuario;

public class MovimientoSitp
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_LOCALIDAD;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_TIPO;
  private long idMovimientoSitp;
  private int cedula;
  private String apellidosNombres;
  private ClasificacionPersonal clasificacionPersonal;
  private Turno turno;
  private String tipoPersonal;
  private String nombreTipoPersonal;
  private CausaMovimiento causaMovimiento;
  private String codCausaMovimiento;
  private Date fechaIngreso;
  private Date fechaMovimiento;
  private Date fechaRegistro;
  private Date fechaCulminacion;
  private int codManualCargo;
  private String codCargo;
  private String codTabulador;
  private String descripcionCargo;
  private int codigoNomina;
  private String codSede;
  private String nombreSede;
  private String codDependencia;
  private String nombreDependencia;
  private double sueldo;
  private double compensacion;
  private double primasCargo;
  private double primasTrabajador;
  private int grado;
  private int paso;
  private double montoJubilacion;
  private double porcJubilacion;
  private double sueldoPromedio;
  private Remesa remesa;
  private int numeroMovimiento;
  private String afectaSueldo;
  private String localidad;
  private String estatus;
  private String documentoSoporte;
  private Personal personal;
  private Organismo organismo;
  private String codOrganismo;
  private String nombreOrganismo;
  private String codOrganismoMpd;
  private String codRegion;
  private String nombreRegion;
  private String observaciones;
  private Usuario usuario;
  private String estatusMpd;
  private String codigoDevolucion;
  private String puntoCuenta;
  private Date fechaPuntoCuenta;
  private String codConcurso;
  private String analistaMpd;
  private int idAnalistaMpd;
  private Date fechaInicioMpd;
  private Date fechaFinMpd;
  private String observacionesMpd;
  private int anio;
  private int anteriorCodManualCargo;
  private String anteriorCodCargo;
  private String anteriorDescripcionCargo;
  private int anteriorCodigoNomina;
  private String anteriorCodSede;
  private String anteriorNombreSede;
  private String anteriorCodDependencia;
  private String anteriorNombreDependencia;
  private double anteriorSueldo;
  private double anteriorCompensacion;
  private double anteriorPrimasCargo;
  private double anteriorPrimasTrabajador;
  private int anteriorGrado;
  private int anteriorPaso;
  private String anteriorCodRegion;
  private String anteriorNombreRegion;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "afectaSueldo", "analistaMpd", "anio", "anteriorCodCargo", "anteriorCodDependencia", "anteriorCodManualCargo", "anteriorCodRegion", "anteriorCodSede", "anteriorCodigoNomina", "anteriorCompensacion", "anteriorDescripcionCargo", "anteriorGrado", "anteriorNombreDependencia", "anteriorNombreRegion", "anteriorNombreSede", "anteriorPaso", "anteriorPrimasCargo", "anteriorPrimasTrabajador", "anteriorSueldo", "apellidosNombres", "causaMovimiento", "cedula", "clasificacionPersonal", "codCargo", "codCausaMovimiento", "codConcurso", "codDependencia", "codManualCargo", "codOrganismo", "codOrganismoMpd", "codRegion", "codSede", "codTabulador", "codigoDevolucion", "codigoNomina", "compensacion", "descripcionCargo", "documentoSoporte", "estatus", "estatusMpd", "fechaCulminacion", "fechaFinMpd", "fechaIngreso", "fechaInicioMpd", "fechaMovimiento", "fechaPuntoCuenta", "fechaRegistro", "grado", "idAnalistaMpd", "idMovimientoSitp", "idSitp", "localidad", "montoJubilacion", "nombreDependencia", "nombreOrganismo", "nombreRegion", "nombreSede", "nombreTipoPersonal", "numeroMovimiento", "observaciones", "observacionesMpd", "organismo", "paso", "personal", "porcJubilacion", "primasCargo", "primasTrabajador", "puntoCuenta", "remesa", "sueldo", "sueldoPromedio", "tiempoSitp", "tipoPersonal", "turno", "usuario" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ClasificacionPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.movimientos.Remesa"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Turno"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 26, 21, 21, 21, 21, 26, 21, 21, 21, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.movimientos.MovimientoSitp"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new MovimientoSitp());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_LOCALIDAD = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_LOCALIDAD.put("C", "NIVEL CENTRAL");
    LISTA_LOCALIDAD.put("R", "NIVEL REGIONAL");
    LISTA_ESTATUS.put("0", "ANALISIS");
    LISTA_ESTATUS.put("1", "DEVUELTO");
    LISTA_ESTATUS.put("2", "CON REMESA");
    LISTA_ESTATUS.put("3", "DEVUELTO ");
    LISTA_ESTATUS.put("4", "APROBADO");
    LISTA_ESTATUS.put("5", "APROBADO RES 048");
    LISTA_TIPO.put("0", "ALTO NIVEL");
    LISTA_TIPO.put("1", "ADMINISTRATIVO");
    LISTA_TIPO.put("2", "PROFESIONAL Y TECNICO");
    LISTA_TIPO.put("3", "NO CLASIFICADO");
    LISTA_TIPO.put("4", "OBRERO");
  }

  public MovimientoSitp()
  {
    jdoSetcedula(this, 0);

    jdoSettipoPersonal(this, "1");

    jdoSetcodigoNomina(this, 0);

    jdoSetsueldo(this, 0.0D);

    jdoSetcompensacion(this, 0.0D);

    jdoSetprimasCargo(this, 0.0D);

    jdoSetprimasTrabajador(this, 0.0D);

    jdoSetgrado(this, 1);

    jdoSetpaso(this, 1);

    jdoSetmontoJubilacion(this, 0.0D);

    jdoSetporcJubilacion(this, 0.0D);

    jdoSetsueldoPromedio(this, 0.0D);

    jdoSetafectaSueldo(this, "S");

    jdoSetlocalidad(this, "C");

    jdoSetestatus(this, "0");

    jdoSetanteriorCodigoNomina(this, 0);

    jdoSetanteriorSueldo(this, 0.0D);

    jdoSetanteriorCompensacion(this, 0.0D);

    jdoSetanteriorPrimasCargo(this, 0.0D);

    jdoSetanteriorPrimasTrabajador(this, 0.0D);

    jdoSetanteriorGrado(this, 1);

    jdoSetanteriorPaso(this, 1);
  }

  public String toString()
  {
    return jdoGetanio(this) + " - " + jdoGetnumeroMovimiento(this) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaMovimiento(this)) + " - " + 
      jdoGetcausaMovimiento(this).getMovimientoPersonal().getDescripcion() + " - " + 
      jdoGetapellidosNombres(this);
  }

  public String getAfectaSueldo()
  {
    return jdoGetafectaSueldo(this);
  }

  public String getAnalistaMpd()
  {
    return jdoGetanalistaMpd(this);
  }

  public String getApellidosNombres()
  {
    return jdoGetapellidosNombres(this);
  }

  public CausaMovimiento getCausaMovimiento()
  {
    return jdoGetcausaMovimiento(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }

  public ClasificacionPersonal getClasificacionPersonal()
  {
    return jdoGetclasificacionPersonal(this);
  }

  public String getCodCargo()
  {
    return jdoGetcodCargo(this);
  }

  public String getCodCausaMovimiento()
  {
    return jdoGetcodCausaMovimiento(this);
  }

  public String getCodConcurso()
  {
    return jdoGetcodConcurso(this);
  }

  public String getCodDependencia()
  {
    return jdoGetcodDependencia(this);
  }

  public String getCodigoDevolucion()
  {
    return jdoGetcodigoDevolucion(this);
  }

  public int getCodigoNomina()
  {
    return jdoGetcodigoNomina(this);
  }

  public int getCodManualCargo()
  {
    return jdoGetcodManualCargo(this);
  }

  public String getCodOrganismo()
  {
    return jdoGetcodOrganismo(this);
  }

  public String getCodOrganismoMpd()
  {
    return jdoGetcodOrganismoMpd(this);
  }

  public String getCodRegion()
  {
    return jdoGetcodRegion(this);
  }

  public String getCodSede()
  {
    return jdoGetcodSede(this);
  }

  public String getCodTabulador()
  {
    return jdoGetcodTabulador(this);
  }

  public double getCompensacion()
  {
    return jdoGetcompensacion(this);
  }

  public String getDescripcionCargo()
  {
    return jdoGetdescripcionCargo(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public String getEstatusMpd()
  {
    return jdoGetestatusMpd(this);
  }

  public Date getFechaFinMpd()
  {
    return jdoGetfechaFinMpd(this);
  }

  public Date getFechaInicioMpd()
  {
    return jdoGetfechaInicioMpd(this);
  }

  public Date getFechaMovimiento()
  {
    return jdoGetfechaMovimiento(this);
  }

  public Date getFechaPuntoCuenta()
  {
    return jdoGetfechaPuntoCuenta(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public int getGrado()
  {
    return jdoGetgrado(this);
  }

  public int getIdAnalistaMpd()
  {
    return jdoGetidAnalistaMpd(this);
  }

  public long getIdMovimientoSitp()
  {
    return jdoGetidMovimientoSitp(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public String getLocalidad()
  {
    return jdoGetlocalidad(this);
  }

  public String getNombreDependencia()
  {
    return jdoGetnombreDependencia(this);
  }

  public String getNombreOrganismo()
  {
    return jdoGetnombreOrganismo(this);
  }

  public String getNombreRegion()
  {
    return jdoGetnombreRegion(this);
  }

  public String getNombreSede()
  {
    return jdoGetnombreSede(this);
  }

  public String getNombreTipoPersonal()
  {
    return jdoGetnombreTipoPersonal(this);
  }

  public int getNumeroMovimiento()
  {
    return jdoGetnumeroMovimiento(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public String getObservacionesMpd()
  {
    return jdoGetobservacionesMpd(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public int getPaso()
  {
    return jdoGetpaso(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public double getPrimasCargo()
  {
    return jdoGetprimasCargo(this);
  }

  public double getPrimasTrabajador()
  {
    return jdoGetprimasTrabajador(this);
  }

  public String getPuntoCuenta()
  {
    return jdoGetpuntoCuenta(this);
  }

  public double getSueldo()
  {
    return jdoGetsueldo(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public String getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setAfectaSueldo(String string)
  {
    jdoSetafectaSueldo(this, string);
  }

  public void setAnalistaMpd(String string)
  {
    jdoSetanalistaMpd(this, string);
  }

  public void setApellidosNombres(String string)
  {
    jdoSetapellidosNombres(this, string);
  }

  public void setCausaMovimiento(CausaMovimiento movimiento)
  {
    jdoSetcausaMovimiento(this, movimiento);
  }

  public void setCedula(int i)
  {
    jdoSetcedula(this, i);
  }

  public void setClasificacionPersonal(ClasificacionPersonal personal)
  {
    jdoSetclasificacionPersonal(this, personal);
  }

  public void setCodCargo(String string)
  {
    jdoSetcodCargo(this, string);
  }

  public void setCodCausaMovimiento(String string)
  {
    jdoSetcodCausaMovimiento(this, string);
  }

  public void setCodConcurso(String string)
  {
    jdoSetcodConcurso(this, string);
  }

  public void setCodDependencia(String string)
  {
    jdoSetcodDependencia(this, string);
  }

  public void setCodigoDevolucion(String string)
  {
    jdoSetcodigoDevolucion(this, string);
  }

  public void setCodigoNomina(int i)
  {
    jdoSetcodigoNomina(this, i);
  }

  public void setCodManualCargo(int i)
  {
    jdoSetcodManualCargo(this, i);
  }

  public void setCodOrganismo(String string)
  {
    jdoSetcodOrganismo(this, string);
  }

  public void setCodOrganismoMpd(String string)
  {
    jdoSetcodOrganismoMpd(this, string);
  }

  public void setCodRegion(String string)
  {
    jdoSetcodRegion(this, string);
  }

  public void setCodSede(String string)
  {
    jdoSetcodSede(this, string);
  }

  public void setCodTabulador(String string)
  {
    jdoSetcodTabulador(this, string);
  }

  public void setCompensacion(double d)
  {
    jdoSetcompensacion(this, d);
  }

  public void setDescripcionCargo(String string)
  {
    jdoSetdescripcionCargo(this, string);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setEstatusMpd(String string)
  {
    jdoSetestatusMpd(this, string);
  }

  public void setFechaFinMpd(Date date)
  {
    jdoSetfechaFinMpd(this, date);
  }

  public void setFechaInicioMpd(Date date)
  {
    jdoSetfechaInicioMpd(this, date);
  }

  public void setFechaMovimiento(Date date)
  {
    jdoSetfechaMovimiento(this, date);
  }

  public void setFechaPuntoCuenta(Date date)
  {
    jdoSetfechaPuntoCuenta(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setGrado(int i)
  {
    jdoSetgrado(this, i);
  }

  public void setIdAnalistaMpd(int i)
  {
    jdoSetidAnalistaMpd(this, i);
  }

  public void setIdMovimientoSitp(long l)
  {
    jdoSetidMovimientoSitp(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setLocalidad(String string)
  {
    jdoSetlocalidad(this, string);
  }

  public void setNombreDependencia(String string)
  {
    jdoSetnombreDependencia(this, string);
  }

  public void setNombreOrganismo(String string)
  {
    jdoSetnombreOrganismo(this, string);
  }

  public void setNombreRegion(String string)
  {
    jdoSetnombreRegion(this, string);
  }

  public void setNombreSede(String string)
  {
    jdoSetnombreSede(this, string);
  }

  public void setNombreTipoPersonal(String string)
  {
    jdoSetnombreTipoPersonal(this, string);
  }

  public void setNumeroMovimiento(int i)
  {
    jdoSetnumeroMovimiento(this, i);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setObservacionesMpd(String string)
  {
    jdoSetobservacionesMpd(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setPaso(int i)
  {
    jdoSetpaso(this, i);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPrimasCargo(double d)
  {
    jdoSetprimasCargo(this, d);
  }

  public void setPrimasTrabajador(double d)
  {
    jdoSetprimasTrabajador(this, d);
  }

  public void setPuntoCuenta(String string)
  {
    jdoSetpuntoCuenta(this, string);
  }

  public void setSueldo(double d)
  {
    jdoSetsueldo(this, d);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setTipoPersonal(String string)
  {
    jdoSettipoPersonal(this, string);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public Remesa getRemesa()
  {
    return jdoGetremesa(this);
  }

  public void setRemesa(Remesa remesa)
  {
    jdoSetremesa(this, remesa);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public String getAnteriorCodCargo() {
    return jdoGetanteriorCodCargo(this);
  }
  public void setAnteriorCodCargo(String anteriorCodCargo) {
    jdoSetanteriorCodCargo(this, anteriorCodCargo);
  }
  public String getAnteriorCodDependencia() {
    return jdoGetanteriorCodDependencia(this);
  }
  public void setAnteriorCodDependencia(String anteriorCodDependencia) {
    jdoSetanteriorCodDependencia(this, anteriorCodDependencia);
  }
  public int getAnteriorCodigoNomina() {
    return jdoGetanteriorCodigoNomina(this);
  }
  public void setAnteriorCodigoNomina(int anteriorCodigoNomina) {
    jdoSetanteriorCodigoNomina(this, anteriorCodigoNomina);
  }
  public int getAnteriorCodManualCargo() {
    return jdoGetanteriorCodManualCargo(this);
  }
  public void setAnteriorCodManualCargo(int anteriorCodManualCargo) {
    jdoSetanteriorCodManualCargo(this, anteriorCodManualCargo);
  }
  public String getAnteriorCodRegion() {
    return jdoGetanteriorCodRegion(this);
  }
  public void setAnteriorCodRegion(String anteriorCodRegion) {
    jdoSetanteriorCodRegion(this, anteriorCodRegion);
  }
  public String getAnteriorCodSede() {
    return jdoGetanteriorCodSede(this);
  }
  public void setAnteriorCodSede(String anteriorCodSede) {
    jdoSetanteriorCodSede(this, anteriorCodSede);
  }
  public double getAnteriorCompensacion() {
    return jdoGetanteriorCompensacion(this);
  }
  public void setAnteriorCompensacion(double anteriorCompensacion) {
    jdoSetanteriorCompensacion(this, anteriorCompensacion);
  }
  public String getAnteriorDescripcionCargo() {
    return jdoGetanteriorDescripcionCargo(this);
  }
  public void setAnteriorDescripcionCargo(String anteriorDescripcionCargo) {
    jdoSetanteriorDescripcionCargo(this, anteriorDescripcionCargo);
  }
  public int getAnteriorGrado() {
    return jdoGetanteriorGrado(this);
  }
  public void setAnteriorGrado(int anteriorGrado) {
    jdoSetanteriorGrado(this, anteriorGrado);
  }
  public String getAnteriorNombreDependencia() {
    return jdoGetanteriorNombreDependencia(this);
  }
  public void setAnteriorNombreDependencia(String anteriorNombreDependencia) {
    jdoSetanteriorNombreDependencia(this, anteriorNombreDependencia);
  }
  public String getAnteriorNombreRegion() {
    return jdoGetanteriorNombreRegion(this);
  }
  public void setAnteriorNombreRegion(String anteriorNombreRegion) {
    jdoSetanteriorNombreRegion(this, anteriorNombreRegion);
  }
  public String getAnteriorNombreSede() {
    return jdoGetanteriorNombreSede(this);
  }
  public void setAnteriorNombreSede(String anteriorNombreSede) {
    jdoSetanteriorNombreSede(this, anteriorNombreSede);
  }
  public int getAnteriorPaso() {
    return jdoGetanteriorPaso(this);
  }
  public void setAnteriorPaso(int anteriorPaso) {
    jdoSetanteriorPaso(this, anteriorPaso);
  }
  public double getAnteriorPrimasCargo() {
    return jdoGetanteriorPrimasCargo(this);
  }
  public void setAnteriorPrimasCargo(double anteriorPrimasCargo) {
    jdoSetanteriorPrimasCargo(this, anteriorPrimasCargo);
  }
  public double getAnteriorPrimasTrabajador() {
    return jdoGetanteriorPrimasTrabajador(this);
  }
  public void setAnteriorPrimasTrabajador(double anteriorPrimasTrabajador) {
    jdoSetanteriorPrimasTrabajador(this, anteriorPrimasTrabajador);
  }
  public double getAnteriorSueldo() {
    return jdoGetanteriorSueldo(this);
  }
  public void setAnteriorSueldo(double anteriorSueldo) {
    jdoSetanteriorSueldo(this, anteriorSueldo);
  }

  public Date getFechaIngreso() {
    return jdoGetfechaIngreso(this);
  }

  public void setFechaIngreso(Date fechaIngreso) {
    jdoSetfechaIngreso(this, fechaIngreso);
  }

  public Turno getTurno() {
    return jdoGetturno(this);
  }

  public void setTurno(Turno turno) {
    jdoSetturno(this, turno);
  }

  public Date getFechaCulminacion() {
    return jdoGetfechaCulminacion(this);
  }

  public void setFechaCulminacion(Date fechaCulminacion) {
    jdoSetfechaCulminacion(this, fechaCulminacion);
  }

  public double getMontoJubilacion() {
    return jdoGetmontoJubilacion(this);
  }

  public void setMontoJubilacion(double montoJubilacion) {
    jdoSetmontoJubilacion(this, montoJubilacion);
  }

  public double getPorcJubilacion() {
    return jdoGetporcJubilacion(this);
  }

  public void setPorcJubilacion(double porcJubilacion) {
    jdoSetporcJubilacion(this, porcJubilacion);
  }

  public double getSueldoPromedio() {
    return jdoGetsueldoPromedio(this);
  }

  public void setSueldoPromedio(double sueldoPromedio) {
    jdoSetsueldoPromedio(this, sueldoPromedio);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 75;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    MovimientoSitp localMovimientoSitp = new MovimientoSitp();
    localMovimientoSitp.jdoFlags = 1;
    localMovimientoSitp.jdoStateManager = paramStateManager;
    return localMovimientoSitp;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    MovimientoSitp localMovimientoSitp = new MovimientoSitp();
    localMovimientoSitp.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMovimientoSitp.jdoFlags = 1;
    localMovimientoSitp.jdoStateManager = paramStateManager;
    return localMovimientoSitp;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.afectaSueldo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.analistaMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodCargo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodDependencia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorCodManualCargo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodRegion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodSede);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorCodigoNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorCompensacion);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorDescripcionCargo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorGrado);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorNombreDependencia);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorNombreRegion);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorNombreSede);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorPaso);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorPrimasCargo);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorPrimasTrabajador);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorSueldo);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.apellidosNombres);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacionPersonal);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaMovimiento);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcurso);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codManualCargo);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismo);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismoMpd);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegion);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSede);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTabulador);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoDevolucion);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacion);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionCargo);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatusMpd);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCulminacion);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFinMpd);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicioMpd);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaMovimiento);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPuntoCuenta);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idAnalistaMpd);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMovimientoSitp);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.localidad);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoJubilacion);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDependencia);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreOrganismo);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreRegion);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSede);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreTipoPersonal);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMovimiento);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observacionesMpd);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcJubilacion);
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasCargo);
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasTrabajador);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.puntoCuenta);
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.remesa);
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldo);
      return;
    case 70:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoPromedio);
      return;
    case 71:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 72:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoPersonal);
      return;
    case 73:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.turno);
      return;
    case 74:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.afectaSueldo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.analistaMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodManualCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCompensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorDescripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorGrado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorNombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorNombreRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorNombreSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorPaso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorPrimasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorPrimasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.apellidosNombres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacionPersonal = ((ClasificacionPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codManualCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismoMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTabulador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoDevolucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatusMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCulminacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFinMpd = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicioMpd = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaMovimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPuntoCuenta = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAnalistaMpd = localStateManager.replacingIntField(this, paramInt);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMovimientoSitp = localStateManager.replacingLongField(this, paramInt);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.localidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreTipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMovimiento = localStateManager.replacingIntField(this, paramInt);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observacionesMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcJubilacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.puntoCuenta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remesa = ((Remesa)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 70:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoPromedio = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 71:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 72:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 73:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.turno = ((Turno)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 74:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.afectaSueldo = paramMovimientoSitp.afectaSueldo;
      return;
    case 1:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.analistaMpd = paramMovimientoSitp.analistaMpd;
      return;
    case 2:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramMovimientoSitp.anio;
      return;
    case 3:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodCargo = paramMovimientoSitp.anteriorCodCargo;
      return;
    case 4:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodDependencia = paramMovimientoSitp.anteriorCodDependencia;
      return;
    case 5:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodManualCargo = paramMovimientoSitp.anteriorCodManualCargo;
      return;
    case 6:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodRegion = paramMovimientoSitp.anteriorCodRegion;
      return;
    case 7:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodSede = paramMovimientoSitp.anteriorCodSede;
      return;
    case 8:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodigoNomina = paramMovimientoSitp.anteriorCodigoNomina;
      return;
    case 9:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCompensacion = paramMovimientoSitp.anteriorCompensacion;
      return;
    case 10:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorDescripcionCargo = paramMovimientoSitp.anteriorDescripcionCargo;
      return;
    case 11:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorGrado = paramMovimientoSitp.anteriorGrado;
      return;
    case 12:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorNombreDependencia = paramMovimientoSitp.anteriorNombreDependencia;
      return;
    case 13:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorNombreRegion = paramMovimientoSitp.anteriorNombreRegion;
      return;
    case 14:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorNombreSede = paramMovimientoSitp.anteriorNombreSede;
      return;
    case 15:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorPaso = paramMovimientoSitp.anteriorPaso;
      return;
    case 16:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorPrimasCargo = paramMovimientoSitp.anteriorPrimasCargo;
      return;
    case 17:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorPrimasTrabajador = paramMovimientoSitp.anteriorPrimasTrabajador;
      return;
    case 18:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorSueldo = paramMovimientoSitp.anteriorSueldo;
      return;
    case 19:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.apellidosNombres = paramMovimientoSitp.apellidosNombres;
      return;
    case 20:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramMovimientoSitp.causaMovimiento;
      return;
    case 21:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramMovimientoSitp.cedula;
      return;
    case 22:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacionPersonal = paramMovimientoSitp.clasificacionPersonal;
      return;
    case 23:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramMovimientoSitp.codCargo;
      return;
    case 24:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaMovimiento = paramMovimientoSitp.codCausaMovimiento;
      return;
    case 25:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codConcurso = paramMovimientoSitp.codConcurso;
      return;
    case 26:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramMovimientoSitp.codDependencia;
      return;
    case 27:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codManualCargo = paramMovimientoSitp.codManualCargo;
      return;
    case 28:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismo = paramMovimientoSitp.codOrganismo;
      return;
    case 29:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismoMpd = paramMovimientoSitp.codOrganismoMpd;
      return;
    case 30:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codRegion = paramMovimientoSitp.codRegion;
      return;
    case 31:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codSede = paramMovimientoSitp.codSede;
      return;
    case 32:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codTabulador = paramMovimientoSitp.codTabulador;
      return;
    case 33:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codigoDevolucion = paramMovimientoSitp.codigoDevolucion;
      return;
    case 34:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramMovimientoSitp.codigoNomina;
      return;
    case 35:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramMovimientoSitp.compensacion;
      return;
    case 36:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionCargo = paramMovimientoSitp.descripcionCargo;
      return;
    case 37:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramMovimientoSitp.documentoSoporte;
      return;
    case 38:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramMovimientoSitp.estatus;
      return;
    case 39:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.estatusMpd = paramMovimientoSitp.estatusMpd;
      return;
    case 40:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCulminacion = paramMovimientoSitp.fechaCulminacion;
      return;
    case 41:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFinMpd = paramMovimientoSitp.fechaFinMpd;
      return;
    case 42:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramMovimientoSitp.fechaIngreso;
      return;
    case 43:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicioMpd = paramMovimientoSitp.fechaInicioMpd;
      return;
    case 44:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaMovimiento = paramMovimientoSitp.fechaMovimiento;
      return;
    case 45:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPuntoCuenta = paramMovimientoSitp.fechaPuntoCuenta;
      return;
    case 46:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramMovimientoSitp.fechaRegistro;
      return;
    case 47:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramMovimientoSitp.grado;
      return;
    case 48:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.idAnalistaMpd = paramMovimientoSitp.idAnalistaMpd;
      return;
    case 49:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.idMovimientoSitp = paramMovimientoSitp.idMovimientoSitp;
      return;
    case 50:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramMovimientoSitp.idSitp;
      return;
    case 51:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.localidad = paramMovimientoSitp.localidad;
      return;
    case 52:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.montoJubilacion = paramMovimientoSitp.montoJubilacion;
      return;
    case 53:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDependencia = paramMovimientoSitp.nombreDependencia;
      return;
    case 54:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreOrganismo = paramMovimientoSitp.nombreOrganismo;
      return;
    case 55:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreRegion = paramMovimientoSitp.nombreRegion;
      return;
    case 56:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSede = paramMovimientoSitp.nombreSede;
      return;
    case 57:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreTipoPersonal = paramMovimientoSitp.nombreTipoPersonal;
      return;
    case 58:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMovimiento = paramMovimientoSitp.numeroMovimiento;
      return;
    case 59:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramMovimientoSitp.observaciones;
      return;
    case 60:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.observacionesMpd = paramMovimientoSitp.observacionesMpd;
      return;
    case 61:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramMovimientoSitp.organismo;
      return;
    case 62:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramMovimientoSitp.paso;
      return;
    case 63:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramMovimientoSitp.personal;
      return;
    case 64:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.porcJubilacion = paramMovimientoSitp.porcJubilacion;
      return;
    case 65:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.primasCargo = paramMovimientoSitp.primasCargo;
      return;
    case 66:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.primasTrabajador = paramMovimientoSitp.primasTrabajador;
      return;
    case 67:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.puntoCuenta = paramMovimientoSitp.puntoCuenta;
      return;
    case 68:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.remesa = paramMovimientoSitp.remesa;
      return;
    case 69:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.sueldo = paramMovimientoSitp.sueldo;
      return;
    case 70:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoPromedio = paramMovimientoSitp.sueldoPromedio;
      return;
    case 71:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramMovimientoSitp.tiempoSitp;
      return;
    case 72:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramMovimientoSitp.tipoPersonal;
      return;
    case 73:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.turno = paramMovimientoSitp.turno;
      return;
    case 74:
      if (paramMovimientoSitp == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramMovimientoSitp.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof MovimientoSitp))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    MovimientoSitp localMovimientoSitp = (MovimientoSitp)paramObject;
    if (localMovimientoSitp.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMovimientoSitp, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MovimientoSitpPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MovimientoSitpPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientoSitpPK))
      throw new IllegalArgumentException("arg1");
    MovimientoSitpPK localMovimientoSitpPK = (MovimientoSitpPK)paramObject;
    localMovimientoSitpPK.idMovimientoSitp = this.idMovimientoSitp;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientoSitpPK))
      throw new IllegalArgumentException("arg1");
    MovimientoSitpPK localMovimientoSitpPK = (MovimientoSitpPK)paramObject;
    this.idMovimientoSitp = localMovimientoSitpPK.idMovimientoSitp;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientoSitpPK))
      throw new IllegalArgumentException("arg2");
    MovimientoSitpPK localMovimientoSitpPK = (MovimientoSitpPK)paramObject;
    localMovimientoSitpPK.idMovimientoSitp = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 49);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientoSitpPK))
      throw new IllegalArgumentException("arg2");
    MovimientoSitpPK localMovimientoSitpPK = (MovimientoSitpPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 49, localMovimientoSitpPK.idMovimientoSitp);
  }

  private static final String jdoGetafectaSueldo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.afectaSueldo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.afectaSueldo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 0))
      return paramMovimientoSitp.afectaSueldo;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 0, paramMovimientoSitp.afectaSueldo);
  }

  private static final void jdoSetafectaSueldo(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.afectaSueldo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.afectaSueldo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 0, paramMovimientoSitp.afectaSueldo, paramString);
  }

  private static final String jdoGetanalistaMpd(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.analistaMpd;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.analistaMpd;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 1))
      return paramMovimientoSitp.analistaMpd;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 1, paramMovimientoSitp.analistaMpd);
  }

  private static final void jdoSetanalistaMpd(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.analistaMpd = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.analistaMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 1, paramMovimientoSitp.analistaMpd, paramString);
  }

  private static final int jdoGetanio(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anio;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anio;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 2))
      return paramMovimientoSitp.anio;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 2, paramMovimientoSitp.anio);
  }

  private static final void jdoSetanio(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 2, paramMovimientoSitp.anio, paramInt);
  }

  private static final String jdoGetanteriorCodCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorCodCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorCodCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 3))
      return paramMovimientoSitp.anteriorCodCargo;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 3, paramMovimientoSitp.anteriorCodCargo);
  }

  private static final void jdoSetanteriorCodCargo(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorCodCargo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorCodCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 3, paramMovimientoSitp.anteriorCodCargo, paramString);
  }

  private static final String jdoGetanteriorCodDependencia(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorCodDependencia;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorCodDependencia;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 4))
      return paramMovimientoSitp.anteriorCodDependencia;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 4, paramMovimientoSitp.anteriorCodDependencia);
  }

  private static final void jdoSetanteriorCodDependencia(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorCodDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorCodDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 4, paramMovimientoSitp.anteriorCodDependencia, paramString);
  }

  private static final int jdoGetanteriorCodManualCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorCodManualCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorCodManualCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 5))
      return paramMovimientoSitp.anteriorCodManualCargo;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 5, paramMovimientoSitp.anteriorCodManualCargo);
  }

  private static final void jdoSetanteriorCodManualCargo(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorCodManualCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorCodManualCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 5, paramMovimientoSitp.anteriorCodManualCargo, paramInt);
  }

  private static final String jdoGetanteriorCodRegion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorCodRegion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorCodRegion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 6))
      return paramMovimientoSitp.anteriorCodRegion;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 6, paramMovimientoSitp.anteriorCodRegion);
  }

  private static final void jdoSetanteriorCodRegion(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorCodRegion = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorCodRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 6, paramMovimientoSitp.anteriorCodRegion, paramString);
  }

  private static final String jdoGetanteriorCodSede(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorCodSede;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorCodSede;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 7))
      return paramMovimientoSitp.anteriorCodSede;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 7, paramMovimientoSitp.anteriorCodSede);
  }

  private static final void jdoSetanteriorCodSede(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorCodSede = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorCodSede = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 7, paramMovimientoSitp.anteriorCodSede, paramString);
  }

  private static final int jdoGetanteriorCodigoNomina(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorCodigoNomina;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorCodigoNomina;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 8))
      return paramMovimientoSitp.anteriorCodigoNomina;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 8, paramMovimientoSitp.anteriorCodigoNomina);
  }

  private static final void jdoSetanteriorCodigoNomina(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorCodigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorCodigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 8, paramMovimientoSitp.anteriorCodigoNomina, paramInt);
  }

  private static final double jdoGetanteriorCompensacion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorCompensacion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorCompensacion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 9))
      return paramMovimientoSitp.anteriorCompensacion;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 9, paramMovimientoSitp.anteriorCompensacion);
  }

  private static final void jdoSetanteriorCompensacion(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorCompensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorCompensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 9, paramMovimientoSitp.anteriorCompensacion, paramDouble);
  }

  private static final String jdoGetanteriorDescripcionCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorDescripcionCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorDescripcionCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 10))
      return paramMovimientoSitp.anteriorDescripcionCargo;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 10, paramMovimientoSitp.anteriorDescripcionCargo);
  }

  private static final void jdoSetanteriorDescripcionCargo(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorDescripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorDescripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 10, paramMovimientoSitp.anteriorDescripcionCargo, paramString);
  }

  private static final int jdoGetanteriorGrado(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorGrado;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorGrado;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 11))
      return paramMovimientoSitp.anteriorGrado;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 11, paramMovimientoSitp.anteriorGrado);
  }

  private static final void jdoSetanteriorGrado(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorGrado = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorGrado = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 11, paramMovimientoSitp.anteriorGrado, paramInt);
  }

  private static final String jdoGetanteriorNombreDependencia(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorNombreDependencia;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorNombreDependencia;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 12))
      return paramMovimientoSitp.anteriorNombreDependencia;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 12, paramMovimientoSitp.anteriorNombreDependencia);
  }

  private static final void jdoSetanteriorNombreDependencia(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorNombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorNombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 12, paramMovimientoSitp.anteriorNombreDependencia, paramString);
  }

  private static final String jdoGetanteriorNombreRegion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorNombreRegion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorNombreRegion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 13))
      return paramMovimientoSitp.anteriorNombreRegion;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 13, paramMovimientoSitp.anteriorNombreRegion);
  }

  private static final void jdoSetanteriorNombreRegion(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorNombreRegion = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorNombreRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 13, paramMovimientoSitp.anteriorNombreRegion, paramString);
  }

  private static final String jdoGetanteriorNombreSede(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorNombreSede;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorNombreSede;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 14))
      return paramMovimientoSitp.anteriorNombreSede;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 14, paramMovimientoSitp.anteriorNombreSede);
  }

  private static final void jdoSetanteriorNombreSede(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorNombreSede = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorNombreSede = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 14, paramMovimientoSitp.anteriorNombreSede, paramString);
  }

  private static final int jdoGetanteriorPaso(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorPaso;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorPaso;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 15))
      return paramMovimientoSitp.anteriorPaso;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 15, paramMovimientoSitp.anteriorPaso);
  }

  private static final void jdoSetanteriorPaso(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorPaso = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorPaso = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 15, paramMovimientoSitp.anteriorPaso, paramInt);
  }

  private static final double jdoGetanteriorPrimasCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorPrimasCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorPrimasCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 16))
      return paramMovimientoSitp.anteriorPrimasCargo;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 16, paramMovimientoSitp.anteriorPrimasCargo);
  }

  private static final void jdoSetanteriorPrimasCargo(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorPrimasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorPrimasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 16, paramMovimientoSitp.anteriorPrimasCargo, paramDouble);
  }

  private static final double jdoGetanteriorPrimasTrabajador(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorPrimasTrabajador;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorPrimasTrabajador;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 17))
      return paramMovimientoSitp.anteriorPrimasTrabajador;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 17, paramMovimientoSitp.anteriorPrimasTrabajador);
  }

  private static final void jdoSetanteriorPrimasTrabajador(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorPrimasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorPrimasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 17, paramMovimientoSitp.anteriorPrimasTrabajador, paramDouble);
  }

  private static final double jdoGetanteriorSueldo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.anteriorSueldo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.anteriorSueldo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 18))
      return paramMovimientoSitp.anteriorSueldo;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 18, paramMovimientoSitp.anteriorSueldo);
  }

  private static final void jdoSetanteriorSueldo(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.anteriorSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.anteriorSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 18, paramMovimientoSitp.anteriorSueldo, paramDouble);
  }

  private static final String jdoGetapellidosNombres(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.apellidosNombres;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.apellidosNombres;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 19))
      return paramMovimientoSitp.apellidosNombres;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 19, paramMovimientoSitp.apellidosNombres);
  }

  private static final void jdoSetapellidosNombres(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.apellidosNombres = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.apellidosNombres = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 19, paramMovimientoSitp.apellidosNombres, paramString);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(MovimientoSitp paramMovimientoSitp)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.causaMovimiento;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 20))
      return paramMovimientoSitp.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 20, paramMovimientoSitp.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(MovimientoSitp paramMovimientoSitp, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 20, paramMovimientoSitp.causaMovimiento, paramCausaMovimiento);
  }

  private static final int jdoGetcedula(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.cedula;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.cedula;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 21))
      return paramMovimientoSitp.cedula;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 21, paramMovimientoSitp.cedula);
  }

  private static final void jdoSetcedula(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 21, paramMovimientoSitp.cedula, paramInt);
  }

  private static final ClasificacionPersonal jdoGetclasificacionPersonal(MovimientoSitp paramMovimientoSitp)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.clasificacionPersonal;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 22))
      return paramMovimientoSitp.clasificacionPersonal;
    return (ClasificacionPersonal)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 22, paramMovimientoSitp.clasificacionPersonal);
  }

  private static final void jdoSetclasificacionPersonal(MovimientoSitp paramMovimientoSitp, ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.clasificacionPersonal = paramClasificacionPersonal;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 22, paramMovimientoSitp.clasificacionPersonal, paramClasificacionPersonal);
  }

  private static final String jdoGetcodCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 23))
      return paramMovimientoSitp.codCargo;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 23, paramMovimientoSitp.codCargo);
  }

  private static final void jdoSetcodCargo(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 23, paramMovimientoSitp.codCargo, paramString);
  }

  private static final String jdoGetcodCausaMovimiento(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codCausaMovimiento;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codCausaMovimiento;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 24))
      return paramMovimientoSitp.codCausaMovimiento;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 24, paramMovimientoSitp.codCausaMovimiento);
  }

  private static final void jdoSetcodCausaMovimiento(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codCausaMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codCausaMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 24, paramMovimientoSitp.codCausaMovimiento, paramString);
  }

  private static final String jdoGetcodConcurso(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codConcurso;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codConcurso;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 25))
      return paramMovimientoSitp.codConcurso;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 25, paramMovimientoSitp.codConcurso);
  }

  private static final void jdoSetcodConcurso(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codConcurso = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codConcurso = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 25, paramMovimientoSitp.codConcurso, paramString);
  }

  private static final String jdoGetcodDependencia(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codDependencia;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codDependencia;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 26))
      return paramMovimientoSitp.codDependencia;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 26, paramMovimientoSitp.codDependencia);
  }

  private static final void jdoSetcodDependencia(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 26, paramMovimientoSitp.codDependencia, paramString);
  }

  private static final int jdoGetcodManualCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codManualCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codManualCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 27))
      return paramMovimientoSitp.codManualCargo;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 27, paramMovimientoSitp.codManualCargo);
  }

  private static final void jdoSetcodManualCargo(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codManualCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codManualCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 27, paramMovimientoSitp.codManualCargo, paramInt);
  }

  private static final String jdoGetcodOrganismo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codOrganismo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codOrganismo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 28))
      return paramMovimientoSitp.codOrganismo;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 28, paramMovimientoSitp.codOrganismo);
  }

  private static final void jdoSetcodOrganismo(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 28, paramMovimientoSitp.codOrganismo, paramString);
  }

  private static final String jdoGetcodOrganismoMpd(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codOrganismoMpd;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codOrganismoMpd;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 29))
      return paramMovimientoSitp.codOrganismoMpd;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 29, paramMovimientoSitp.codOrganismoMpd);
  }

  private static final void jdoSetcodOrganismoMpd(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codOrganismoMpd = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codOrganismoMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 29, paramMovimientoSitp.codOrganismoMpd, paramString);
  }

  private static final String jdoGetcodRegion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codRegion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codRegion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 30))
      return paramMovimientoSitp.codRegion;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 30, paramMovimientoSitp.codRegion);
  }

  private static final void jdoSetcodRegion(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codRegion = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 30, paramMovimientoSitp.codRegion, paramString);
  }

  private static final String jdoGetcodSede(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codSede;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codSede;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 31))
      return paramMovimientoSitp.codSede;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 31, paramMovimientoSitp.codSede);
  }

  private static final void jdoSetcodSede(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codSede = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codSede = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 31, paramMovimientoSitp.codSede, paramString);
  }

  private static final String jdoGetcodTabulador(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codTabulador;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codTabulador;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 32))
      return paramMovimientoSitp.codTabulador;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 32, paramMovimientoSitp.codTabulador);
  }

  private static final void jdoSetcodTabulador(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codTabulador = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codTabulador = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 32, paramMovimientoSitp.codTabulador, paramString);
  }

  private static final String jdoGetcodigoDevolucion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codigoDevolucion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codigoDevolucion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 33))
      return paramMovimientoSitp.codigoDevolucion;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 33, paramMovimientoSitp.codigoDevolucion);
  }

  private static final void jdoSetcodigoDevolucion(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codigoDevolucion = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codigoDevolucion = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 33, paramMovimientoSitp.codigoDevolucion, paramString);
  }

  private static final int jdoGetcodigoNomina(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.codigoNomina;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.codigoNomina;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 34))
      return paramMovimientoSitp.codigoNomina;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 34, paramMovimientoSitp.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 34, paramMovimientoSitp.codigoNomina, paramInt);
  }

  private static final double jdoGetcompensacion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.compensacion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.compensacion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 35))
      return paramMovimientoSitp.compensacion;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 35, paramMovimientoSitp.compensacion);
  }

  private static final void jdoSetcompensacion(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.compensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.compensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 35, paramMovimientoSitp.compensacion, paramDouble);
  }

  private static final String jdoGetdescripcionCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.descripcionCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.descripcionCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 36))
      return paramMovimientoSitp.descripcionCargo;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 36, paramMovimientoSitp.descripcionCargo);
  }

  private static final void jdoSetdescripcionCargo(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.descripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.descripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 36, paramMovimientoSitp.descripcionCargo, paramString);
  }

  private static final String jdoGetdocumentoSoporte(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.documentoSoporte;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.documentoSoporte;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 37))
      return paramMovimientoSitp.documentoSoporte;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 37, paramMovimientoSitp.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 37, paramMovimientoSitp.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.estatus;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.estatus;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 38))
      return paramMovimientoSitp.estatus;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 38, paramMovimientoSitp.estatus);
  }

  private static final void jdoSetestatus(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 38, paramMovimientoSitp.estatus, paramString);
  }

  private static final String jdoGetestatusMpd(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.estatusMpd;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.estatusMpd;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 39))
      return paramMovimientoSitp.estatusMpd;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 39, paramMovimientoSitp.estatusMpd);
  }

  private static final void jdoSetestatusMpd(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.estatusMpd = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.estatusMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 39, paramMovimientoSitp.estatusMpd, paramString);
  }

  private static final Date jdoGetfechaCulminacion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.fechaCulminacion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.fechaCulminacion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 40))
      return paramMovimientoSitp.fechaCulminacion;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 40, paramMovimientoSitp.fechaCulminacion);
  }

  private static final void jdoSetfechaCulminacion(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.fechaCulminacion = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.fechaCulminacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 40, paramMovimientoSitp.fechaCulminacion, paramDate);
  }

  private static final Date jdoGetfechaFinMpd(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.fechaFinMpd;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.fechaFinMpd;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 41))
      return paramMovimientoSitp.fechaFinMpd;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 41, paramMovimientoSitp.fechaFinMpd);
  }

  private static final void jdoSetfechaFinMpd(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.fechaFinMpd = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.fechaFinMpd = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 41, paramMovimientoSitp.fechaFinMpd, paramDate);
  }

  private static final Date jdoGetfechaIngreso(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.fechaIngreso;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.fechaIngreso;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 42))
      return paramMovimientoSitp.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 42, paramMovimientoSitp.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 42, paramMovimientoSitp.fechaIngreso, paramDate);
  }

  private static final Date jdoGetfechaInicioMpd(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.fechaInicioMpd;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.fechaInicioMpd;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 43))
      return paramMovimientoSitp.fechaInicioMpd;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 43, paramMovimientoSitp.fechaInicioMpd);
  }

  private static final void jdoSetfechaInicioMpd(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.fechaInicioMpd = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.fechaInicioMpd = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 43, paramMovimientoSitp.fechaInicioMpd, paramDate);
  }

  private static final Date jdoGetfechaMovimiento(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.fechaMovimiento;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.fechaMovimiento;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 44))
      return paramMovimientoSitp.fechaMovimiento;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 44, paramMovimientoSitp.fechaMovimiento);
  }

  private static final void jdoSetfechaMovimiento(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.fechaMovimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.fechaMovimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 44, paramMovimientoSitp.fechaMovimiento, paramDate);
  }

  private static final Date jdoGetfechaPuntoCuenta(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.fechaPuntoCuenta;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.fechaPuntoCuenta;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 45))
      return paramMovimientoSitp.fechaPuntoCuenta;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 45, paramMovimientoSitp.fechaPuntoCuenta);
  }

  private static final void jdoSetfechaPuntoCuenta(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.fechaPuntoCuenta = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.fechaPuntoCuenta = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 45, paramMovimientoSitp.fechaPuntoCuenta, paramDate);
  }

  private static final Date jdoGetfechaRegistro(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.fechaRegistro;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.fechaRegistro;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 46))
      return paramMovimientoSitp.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 46, paramMovimientoSitp.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 46, paramMovimientoSitp.fechaRegistro, paramDate);
  }

  private static final int jdoGetgrado(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.grado;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.grado;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 47))
      return paramMovimientoSitp.grado;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 47, paramMovimientoSitp.grado);
  }

  private static final void jdoSetgrado(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 47, paramMovimientoSitp.grado, paramInt);
  }

  private static final int jdoGetidAnalistaMpd(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.idAnalistaMpd;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.idAnalistaMpd;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 48))
      return paramMovimientoSitp.idAnalistaMpd;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 48, paramMovimientoSitp.idAnalistaMpd);
  }

  private static final void jdoSetidAnalistaMpd(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.idAnalistaMpd = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.idAnalistaMpd = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 48, paramMovimientoSitp.idAnalistaMpd, paramInt);
  }

  private static final long jdoGetidMovimientoSitp(MovimientoSitp paramMovimientoSitp)
  {
    return paramMovimientoSitp.idMovimientoSitp;
  }

  private static final void jdoSetidMovimientoSitp(MovimientoSitp paramMovimientoSitp, long paramLong)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.idMovimientoSitp = paramLong;
      return;
    }
    localStateManager.setLongField(paramMovimientoSitp, jdoInheritedFieldCount + 49, paramMovimientoSitp.idMovimientoSitp, paramLong);
  }

  private static final int jdoGetidSitp(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.idSitp;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.idSitp;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 50))
      return paramMovimientoSitp.idSitp;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 50, paramMovimientoSitp.idSitp);
  }

  private static final void jdoSetidSitp(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 50, paramMovimientoSitp.idSitp, paramInt);
  }

  private static final String jdoGetlocalidad(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.localidad;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.localidad;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 51))
      return paramMovimientoSitp.localidad;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 51, paramMovimientoSitp.localidad);
  }

  private static final void jdoSetlocalidad(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.localidad = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.localidad = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 51, paramMovimientoSitp.localidad, paramString);
  }

  private static final double jdoGetmontoJubilacion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.montoJubilacion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.montoJubilacion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 52))
      return paramMovimientoSitp.montoJubilacion;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 52, paramMovimientoSitp.montoJubilacion);
  }

  private static final void jdoSetmontoJubilacion(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.montoJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.montoJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 52, paramMovimientoSitp.montoJubilacion, paramDouble);
  }

  private static final String jdoGetnombreDependencia(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.nombreDependencia;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.nombreDependencia;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 53))
      return paramMovimientoSitp.nombreDependencia;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 53, paramMovimientoSitp.nombreDependencia);
  }

  private static final void jdoSetnombreDependencia(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.nombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.nombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 53, paramMovimientoSitp.nombreDependencia, paramString);
  }

  private static final String jdoGetnombreOrganismo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.nombreOrganismo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.nombreOrganismo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 54))
      return paramMovimientoSitp.nombreOrganismo;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 54, paramMovimientoSitp.nombreOrganismo);
  }

  private static final void jdoSetnombreOrganismo(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.nombreOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.nombreOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 54, paramMovimientoSitp.nombreOrganismo, paramString);
  }

  private static final String jdoGetnombreRegion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.nombreRegion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.nombreRegion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 55))
      return paramMovimientoSitp.nombreRegion;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 55, paramMovimientoSitp.nombreRegion);
  }

  private static final void jdoSetnombreRegion(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.nombreRegion = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.nombreRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 55, paramMovimientoSitp.nombreRegion, paramString);
  }

  private static final String jdoGetnombreSede(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.nombreSede;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.nombreSede;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 56))
      return paramMovimientoSitp.nombreSede;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 56, paramMovimientoSitp.nombreSede);
  }

  private static final void jdoSetnombreSede(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.nombreSede = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.nombreSede = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 56, paramMovimientoSitp.nombreSede, paramString);
  }

  private static final String jdoGetnombreTipoPersonal(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.nombreTipoPersonal;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.nombreTipoPersonal;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 57))
      return paramMovimientoSitp.nombreTipoPersonal;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 57, paramMovimientoSitp.nombreTipoPersonal);
  }

  private static final void jdoSetnombreTipoPersonal(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.nombreTipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.nombreTipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 57, paramMovimientoSitp.nombreTipoPersonal, paramString);
  }

  private static final int jdoGetnumeroMovimiento(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.numeroMovimiento;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.numeroMovimiento;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 58))
      return paramMovimientoSitp.numeroMovimiento;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 58, paramMovimientoSitp.numeroMovimiento);
  }

  private static final void jdoSetnumeroMovimiento(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.numeroMovimiento = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.numeroMovimiento = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 58, paramMovimientoSitp.numeroMovimiento, paramInt);
  }

  private static final String jdoGetobservaciones(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.observaciones;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.observaciones;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 59))
      return paramMovimientoSitp.observaciones;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 59, paramMovimientoSitp.observaciones);
  }

  private static final void jdoSetobservaciones(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 59, paramMovimientoSitp.observaciones, paramString);
  }

  private static final String jdoGetobservacionesMpd(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.observacionesMpd;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.observacionesMpd;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 60))
      return paramMovimientoSitp.observacionesMpd;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 60, paramMovimientoSitp.observacionesMpd);
  }

  private static final void jdoSetobservacionesMpd(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.observacionesMpd = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.observacionesMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 60, paramMovimientoSitp.observacionesMpd, paramString);
  }

  private static final Organismo jdoGetorganismo(MovimientoSitp paramMovimientoSitp)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.organismo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 61))
      return paramMovimientoSitp.organismo;
    return (Organismo)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 61, paramMovimientoSitp.organismo);
  }

  private static final void jdoSetorganismo(MovimientoSitp paramMovimientoSitp, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 61, paramMovimientoSitp.organismo, paramOrganismo);
  }

  private static final int jdoGetpaso(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.paso;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.paso;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 62))
      return paramMovimientoSitp.paso;
    return localStateManager.getIntField(paramMovimientoSitp, jdoInheritedFieldCount + 62, paramMovimientoSitp.paso);
  }

  private static final void jdoSetpaso(MovimientoSitp paramMovimientoSitp, int paramInt)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientoSitp, jdoInheritedFieldCount + 62, paramMovimientoSitp.paso, paramInt);
  }

  private static final Personal jdoGetpersonal(MovimientoSitp paramMovimientoSitp)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.personal;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 63))
      return paramMovimientoSitp.personal;
    return (Personal)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 63, paramMovimientoSitp.personal);
  }

  private static final void jdoSetpersonal(MovimientoSitp paramMovimientoSitp, Personal paramPersonal)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 63, paramMovimientoSitp.personal, paramPersonal);
  }

  private static final double jdoGetporcJubilacion(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.porcJubilacion;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.porcJubilacion;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 64))
      return paramMovimientoSitp.porcJubilacion;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 64, paramMovimientoSitp.porcJubilacion);
  }

  private static final void jdoSetporcJubilacion(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.porcJubilacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.porcJubilacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 64, paramMovimientoSitp.porcJubilacion, paramDouble);
  }

  private static final double jdoGetprimasCargo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.primasCargo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.primasCargo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 65))
      return paramMovimientoSitp.primasCargo;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 65, paramMovimientoSitp.primasCargo);
  }

  private static final void jdoSetprimasCargo(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.primasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.primasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 65, paramMovimientoSitp.primasCargo, paramDouble);
  }

  private static final double jdoGetprimasTrabajador(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.primasTrabajador;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.primasTrabajador;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 66))
      return paramMovimientoSitp.primasTrabajador;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 66, paramMovimientoSitp.primasTrabajador);
  }

  private static final void jdoSetprimasTrabajador(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.primasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.primasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 66, paramMovimientoSitp.primasTrabajador, paramDouble);
  }

  private static final String jdoGetpuntoCuenta(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.puntoCuenta;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.puntoCuenta;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 67))
      return paramMovimientoSitp.puntoCuenta;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 67, paramMovimientoSitp.puntoCuenta);
  }

  private static final void jdoSetpuntoCuenta(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.puntoCuenta = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.puntoCuenta = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 67, paramMovimientoSitp.puntoCuenta, paramString);
  }

  private static final Remesa jdoGetremesa(MovimientoSitp paramMovimientoSitp)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.remesa;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 68))
      return paramMovimientoSitp.remesa;
    return (Remesa)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 68, paramMovimientoSitp.remesa);
  }

  private static final void jdoSetremesa(MovimientoSitp paramMovimientoSitp, Remesa paramRemesa)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.remesa = paramRemesa;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 68, paramMovimientoSitp.remesa, paramRemesa);
  }

  private static final double jdoGetsueldo(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.sueldo;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.sueldo;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 69))
      return paramMovimientoSitp.sueldo;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 69, paramMovimientoSitp.sueldo);
  }

  private static final void jdoSetsueldo(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.sueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.sueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 69, paramMovimientoSitp.sueldo, paramDouble);
  }

  private static final double jdoGetsueldoPromedio(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.sueldoPromedio;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.sueldoPromedio;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 70))
      return paramMovimientoSitp.sueldoPromedio;
    return localStateManager.getDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 70, paramMovimientoSitp.sueldoPromedio);
  }

  private static final void jdoSetsueldoPromedio(MovimientoSitp paramMovimientoSitp, double paramDouble)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.sueldoPromedio = paramDouble;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.sueldoPromedio = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMovimientoSitp, jdoInheritedFieldCount + 70, paramMovimientoSitp.sueldoPromedio, paramDouble);
  }

  private static final Date jdoGettiempoSitp(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.tiempoSitp;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.tiempoSitp;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 71))
      return paramMovimientoSitp.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 71, paramMovimientoSitp.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(MovimientoSitp paramMovimientoSitp, Date paramDate)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 71, paramMovimientoSitp.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoPersonal(MovimientoSitp paramMovimientoSitp)
  {
    if (paramMovimientoSitp.jdoFlags <= 0)
      return paramMovimientoSitp.tipoPersonal;
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.tipoPersonal;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 72))
      return paramMovimientoSitp.tipoPersonal;
    return localStateManager.getStringField(paramMovimientoSitp, jdoInheritedFieldCount + 72, paramMovimientoSitp.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(MovimientoSitp paramMovimientoSitp, String paramString)
  {
    if (paramMovimientoSitp.jdoFlags == 0)
    {
      paramMovimientoSitp.tipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.tipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramMovimientoSitp, jdoInheritedFieldCount + 72, paramMovimientoSitp.tipoPersonal, paramString);
  }

  private static final Turno jdoGetturno(MovimientoSitp paramMovimientoSitp)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.turno;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 73))
      return paramMovimientoSitp.turno;
    return (Turno)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 73, paramMovimientoSitp.turno);
  }

  private static final void jdoSetturno(MovimientoSitp paramMovimientoSitp, Turno paramTurno)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.turno = paramTurno;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 73, paramMovimientoSitp.turno, paramTurno);
  }

  private static final Usuario jdoGetusuario(MovimientoSitp paramMovimientoSitp)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientoSitp.usuario;
    if (localStateManager.isLoaded(paramMovimientoSitp, jdoInheritedFieldCount + 74))
      return paramMovimientoSitp.usuario;
    return (Usuario)localStateManager.getObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 74, paramMovimientoSitp.usuario);
  }

  private static final void jdoSetusuario(MovimientoSitp paramMovimientoSitp, Usuario paramUsuario)
  {
    StateManager localStateManager = paramMovimientoSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientoSitp.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramMovimientoSitp, jdoInheritedFieldCount + 74, paramMovimientoSitp.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}