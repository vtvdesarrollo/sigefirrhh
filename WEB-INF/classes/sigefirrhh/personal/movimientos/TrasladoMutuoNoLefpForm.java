package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.ManualPersonal;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.base.registro.RegistroPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.Usuario;

public class TrasladoMutuoNoLefpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TrasladoMutuoNoLefpForm.class
    .getName());
  private String observaciones;
  private int numeroMovimiento;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private RegistroCargos registroCargos1;
  private RegistroCargos registroCargos2;
  private Registro registro;
  private Collection colRegistro;
  private Collection colRegistroCargos;
  private Collection colCargo;
  private Collection colManualCargo;
  private String idRegistroCargos;
  private String idRegistro;
  private String idManualCargo;
  private String idCargo;
  private String pagarRetroactivo = "N";
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private Cargo cargo;
  private Date fechaMovimiento;
  private boolean showData;
  private boolean showTrabajador1;
  private int codigoNomina1;
  private int codigoNomina2;
  private double sueldoBasico;
  private boolean showTrabajador2;
  private boolean showObservaciones;
  private int successTrasladoMutuoNoLeft;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public boolean isShowTrabajador1()
  {
    return this.showTrabajador1;
  }

  public boolean isShowTrabajador2() {
    return this.showTrabajador2;
  }

  public boolean isShowObservaciones() {
    if (this.registroCargos1 != null);
    return this.showObservaciones;
  }

  public void setShowObservaciones(boolean showObservaciones) {
    this.showObservaciones = showObservaciones;
  }

  public void setShowTrabajador2(boolean showTrabajador2) {
    this.showTrabajador2 = showTrabajador2;
  }

  public boolean isShowRegistroCargos()
  {
    return (this.colRegistroCargos != null) && 
      (!this.colRegistroCargos.isEmpty());
  }

  public boolean isShowCargo() {
    return (this.colCargo != null) && (!this.colCargo.isEmpty());
  }

  public boolean isShowManualCargo() {
    return (this.colManualCargo != null) && (!this.colManualCargo.isEmpty());
  }

  public void changeRegistro(ValueChangeEvent event) {
    FacesContext context = FacesContext.getCurrentInstance();
    long idRegistro = Long.valueOf((String)event.getNewValue())
      .longValue();
    try {
      if (idRegistro != 0L) {
        this.registro = this.registroNoGenFacade
          .findRegistroById(idRegistro);

        this.colManualCargo = new ArrayList();
        Collection colRegistroPersonal = new ArrayList();
        ManualPersonal manualPersonal = new ManualPersonal();

        colRegistroPersonal = this.registroNoGenFacade
          .findRegistroPersonalByRegistro(this.registro
          .getIdRegistro());
        Iterator iterRegistroPersonal = colRegistroPersonal.iterator();
        Collection colManualPersonal = new ArrayList();
        Iterator iterManualPersonal;
        for (; iterRegistroPersonal.hasNext(); 
          iterManualPersonal.hasNext())
        {
          RegistroPersonal registroPersonal = (RegistroPersonal)iterRegistroPersonal
            .next();
          colManualPersonal = this.cargoNoGenFacade
            .findManualPersonalByTipoPersonal(registroPersonal
            .getTipoPersonal().getIdTipoPersonal());
          iterManualPersonal = colManualPersonal.iterator();
          continue;
          manualPersonal = (ManualPersonal)iterManualPersonal
            .next();
          this.colManualCargo.add(this.cargoNoGenFacade
            .findManualCargoById(manualPersonal
            .getManualCargo().getIdManualCargo()));
        }
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void buscarRegistroCargo() {
    setShowObservaciones(false);
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      log.error("codigo_nomina 1............." + this.codigoNomina1);
      this.registroCargos1 = 
        ((RegistroCargos)this.registroCargosFacade
        .findRegistroCargosByCodigoNominaAndRegistro(
        this.codigoNomina1, this.registro.getIdRegistro())
        .iterator().next());
      log.error("registro_cargos............." + this.registroCargos1);
      if (this.registroCargos1.getSituacion().equals("O")) {
        this.showTrabajador1 = true;
      }
      log.error("codigo_nomina 2............." + this.codigoNomina2);
      this.registroCargos2 = 
        ((RegistroCargos)this.registroCargosFacade
        .findRegistroCargosByCodigoNominaAndRegistro(
        this.codigoNomina2, this.registro.getIdRegistro())
        .iterator().next());
      log.error("registro_cargos 2 ............." + this.registroCargos2);
      if (this.registroCargos2.getSituacion().equals("O")) {
        setShowTrabajador2(true);
      }

      setShowObservaciones((isShowTrabajador1()) && (isShowTrabajador2()));
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(
        FacesMessage.SEVERITY_ERROR, 
        "Debe introducir un puesto de trabajo", ""));
    }
  }

  public void changeRegistroCargos(ValueChangeEvent event)
  {
    FacesContext context = FacesContext.getCurrentInstance();
    long idRegistroCargos = Long.valueOf((String)event.getNewValue())
      .longValue();
    try
    {
      this.showTrabajador1 = false;
      this.showObservaciones = false;
      if (idRegistroCargos != 0L) {
        this.registroCargos1 = this.registroCargosFacade
          .findRegistroCargosById(idRegistroCargos);
        if (this.registroCargos1.getSituacion().equals("O")) {
          this.showTrabajador1 = true;
          this.showObservaciones = true;
        }

      }

    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(
        FacesMessage.SEVERITY_ERROR, 
        "Debe introducir un puesto de trabajo", ""));
    }
  }

  public void changeCargo(ValueChangeEvent event)
  {
    long idCargo = Long.valueOf((String)event.getNewValue()).longValue();
    try {
      if (idCargo != 0L) {
        this.cargo = this.cargoNoGenFacade.findCargoById(idCargo);
        this.sueldoBasico = this.cargoNoGenFacade
          .findDetalleTabuladorForRegistroCargos(this.cargo
          .getManualCargo().getTabulador()
          .getIdTabulador(), this.cargo.getGrado(), 1, 1)
          .getMonto();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if ((this.codigoNomina1 != 0) && (this.codigoNomina2 != 0))
      {
        this.successTrasladoMutuoNoLeft = this.movimientosNoGenFacade.trasladoMutuoNoLefp(this.registroCargos1.getIdRegistroCargos(), this.registroCargos2.getIdRegistroCargos(), this.fechaMovimiento, this.fechaPuntoCuenta, this.puntoCuenta, this.observaciones, this.login.getUsuarioObject().getIdOrganismo(), this.login.getUsuarioObject().getIdUsuario());

        if (this.successTrasladoMutuoNoLeft != 0) {
          if (this.successTrasladoMutuoNoLeft == 101) {
            context.addMessage("error_data", new FacesMessage(
              FacesMessage.SEVERITY_ERROR, 
              "Ambos puestos deben encontrarse ocupados", ""));
          }
          if (this.successTrasladoMutuoNoLeft == 102) {
            context.addMessage("error_data", new FacesMessage(
              FacesMessage.SEVERITY_ERROR, 
              "Ambos trabajadores deben estar activos", ""));
          }
          if (this.successTrasladoMutuoNoLeft == 103) {
            context.addMessage("error_data", new FacesMessage(
              FacesMessage.SEVERITY_ERROR, 
              "Ambos trabajadores deben pertenecer al mismo tipo de personal", ""));
          }
          if (this.successTrasladoMutuoNoLeft == 104) {
            context.addMessage("error_data", new FacesMessage(
              FacesMessage.SEVERITY_ERROR, 
              "Ambos puestos deben pertenecer al mismo registro", ""));
          }
          if (this.successTrasladoMutuoNoLeft == 105) {
            context.addMessage("error_data", new FacesMessage(
              FacesMessage.SEVERITY_ERROR, 
              "Ambos cargos deben tener el mismo grado y/o sub grado", ""));
          }
          throw new Exception("No se ha podido realizar el traslado Mutuo No Left");
        }

        context.addMessage("success_add", new FacesMessage("Se ha realizado el traslado mutuo del trabajador : " + this.codigoNomina1 + " con el trabajador :  " + this.codigoNomina2 + " exitosamente. "));

        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.registroCargos2);
      }

    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(
        FacesMessage.SEVERITY_ERROR, 
        "No se ha podido realizar el traslado Mutuo No Left", ""));
      log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }

  public String abort()
  {
    this.selected = false;
    this.colRegistroCargos = null;

    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";
    this.registroCargos1 = null;
    this.registroCargos2 = null;
    this.observaciones = "";
    this.fechaMovimiento = null;
    this.idManualCargo = "0";
    this.idRegistro = "0";
    this.colCargo = null;
    this.showTrabajador1 = false;
    this.showTrabajador2 = false;
    this.showObservaciones = false;

    return "cancel";
  }

  public TrasladoMutuoNoLefpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication()
      .getVariableResolver().resolveVariable(context, "loginSession"));

    refresh();
  }

  public Collection getColRegistro() {
    return ListUtil.convertCollectionToSelectItemsWithId(this.colRegistro, 
      "sigefirrhh.base.registro.Registro");
  }

  public Collection getColRegistroCargos() {
    return ListUtil.convertCollectionToSelectItemsWithId(this.colRegistroCargos, 
      "sigefirrhh.personal.registroCargos.RegistroCargos");
  }

  public Collection getColManualCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(this.colManualCargo, 
      "sigefirrhh.base.cargo.ManualCargo");
  }

  public Collection getColCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(this.colCargo, 
      "sigefirrhh.base.cargo.Cargo");
  }

  public void refresh()
  {
    try {
      this.colRegistro = this.registroNoGenFacade.findRegistroByAprobacionMpd(
        this.login.getIdOrganismo(), "N");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public int getScrollx()
  {
    return this.scrollx;
  }

  public int getScrolly() {
    return this.scrolly;
  }

  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }

  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }

  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }

  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }

  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }

  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }

  public String getIdCargo() {
    return this.idCargo;
  }

  public void setIdCargo(String idCargo) {
    this.idCargo = idCargo;
  }

  public String getIdManualCargo() {
    return this.idManualCargo;
  }

  public void setIdManualCargo(String idManualCargo) {
    this.idManualCargo = idManualCargo;
  }

  public String getIdRegistro() {
    return this.idRegistro;
  }

  public void setIdRegistro(String idRegistro) {
    this.idRegistro = idRegistro;
  }

  public RegistroCargos getRegistroCargos1() {
    return this.registroCargos1;
  }

  public void setRegistroCargos1(RegistroCargos registroCargos1) {
    this.registroCargos1 = registroCargos1;
  }

  public RegistroCargos getRegistroCargos2() {
    return this.registroCargos2;
  }

  public void setRegistroCargos2(RegistroCargos registroCargos2) {
    this.registroCargos2 = registroCargos2;
  }

  public String getIdRegistroCargos() {
    return this.idRegistroCargos;
  }

  public void setIdRegistroCargos(String idRegistroCargos) {
    this.idRegistroCargos = idRegistroCargos;
  }

  public String getObservaciones() {
    return this.observaciones;
  }

  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }

  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }

  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }

  public int getCodigoNomina1() {
    return this.codigoNomina1;
  }

  public void setCodigoNomina1(int codigoNomina1) {
    this.codigoNomina1 = codigoNomina1;
  }

  public int getCodigoNomina2() {
    return this.codigoNomina2;
  }

  public void setCodigoNomina2(int codigoNomina2) {
    this.codigoNomina2 = codigoNomina2;
  }
}