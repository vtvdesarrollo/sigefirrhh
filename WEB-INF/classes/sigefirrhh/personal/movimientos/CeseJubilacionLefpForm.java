package sigefirrhh.personal.movimientos;

import eforserver.report.JasperForWeb;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CeseJubilacionLefpForm extends JubilacionAbstract
{
  static Logger log = Logger.getLogger(CeseJubilacionLefpForm.class.getName());
  private boolean showReport;
  private int reportId;
  private String reportName;
  private long idMovimientoSitp;
  private long idCausaMovimiento = 28L;

  public CeseJubilacionLefpForm() throws Exception
  {
    this.reportName = "formato01lefp";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.showReport = false;
    this.idMovimientoSitp = 0L;
  }

  public String validarEstatus() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      show();

      if (getTrabajador().getEstatus().equals("E")) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Jubilado ya se encuentra egresado", ""));
        abort();
      }
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
      log.error("Excepcion controlada:", e);
      abort();
    }

    return "cancel";
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", getLogin().getOrganismo().getNombreOrganismo());
      parameters.put("id_organismo", new Long(getLogin().getOrganismo().getIdOrganismo()));
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(getLogin().getURLLogo()));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");
      parameters.put("id_personal", new Long(getTrabajador().getPersonal().getIdPersonal()));
      parameters.put("id_causa_movimiento", new Long(this.idCausaMovimiento));
      parameters.put("id_movimiento", new Long(this.idMovimientoSitp));

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String ejecutar2()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.idMovimientoSitp = getMovimientosNoGenFacade().jubilacionLefp(
        getTrabajador().getIdTrabajador(), getFechaVigencia(), 
        this.idCausaMovimiento, getNumeroMovimiento(), 
        getLogin().getOrganismo(), getLogin().getIdUsuario(), 
        getFechaPuntoCuenta(), getPuntoCuenta(), getObservaciones(), 0.0D, 0.0D, 0.0D);

      this.showReport = true;

      registrarAuditoria(context);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      setShow(false);
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }

  public void refresh() {
    try {
      this.listTipoPersonal = getDefinicionesNoGenFacade().findTipoPersonalWithSeguridadRelacionCategoriaAprobacionMpd(getLogin().getIdUsuario(), getLogin().getOrganismo().getIdOrganismo(), getLogin().getAdministrador(), "4", "1", "S");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public int getReportId() { return this.reportId; }

  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
  public boolean isShowReport() {
    return this.showReport;
  }
  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }
}