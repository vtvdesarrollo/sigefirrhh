package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class TrasladoMutuoNoLefpBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(TrasladoMutuoNoLefpBeanBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public int trasladoMutuoNoLefp(long idRegistroCargos1, long idRegistroCargos2, java.util.Date fechaMovimiento, java.util.Date fechaPuntoCuenta, String puntoCuenta, String observaciones, long idOrganismo, long idUsuario)
    throws Exception
  {
    this.txn.open();

    Connection connection = null;

    ResultSet rs = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select procesar_traslado_mutuo(?, ?, ?, ?, ?, ?, ?, ?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idRegistroCargos1);
      st.setLong(2, idRegistroCargos2);
      st.setDate(3, new java.sql.Date(fechaMovimiento.getTime()));
      if (fechaPuntoCuenta != null)
        st.setDate(4, new java.sql.Date(fechaPuntoCuenta.getTime()));
      else {
        st.setDate(4, null);
      }
      st.setString(5, puntoCuenta);
      st.setString(6, observaciones);
      st.setLong(7, idUsuario);
      st.setLong(8, idOrganismo);

      rs = st.executeQuery();

      rs.next();
      return rs.getInt(1);
    }
    catch (ErrorSistema a)
    {
      this.txn.close();
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    finally
    {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException4) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        }
    }
  }
}