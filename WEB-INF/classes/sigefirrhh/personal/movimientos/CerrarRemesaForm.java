package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class CerrarRemesaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CerrarRemesaForm.class.getName());
  private Collection listRemesa;
  private LoginSession login;
  private long idRemesa;
  private boolean show2 = false;

  private int registros = 0;

  public CerrarRemesaForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh() {
    try {
      this.listRemesa = new MovimientosNoGenFacade().findRemesaByEstatus(this.login.getIdOrganismo(), "A", this.login.getIdUsuario());
    } catch (Exception e) {
      this.listRemesa = new ArrayList();
    }
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      MovimientosFacade movimientosFacade = new MovimientosFacade();
      Remesa remesa = movimientosFacade.findRemesaById(this.idRemesa);
      remesa.setEstatus("C");
      remesa.setFechaCierre(new Date());
      movimientosFacade.updateRemesa(remesa);
      refresh();

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', remesa);

      context.addMessage("success_add", new FacesMessage("Se cerró con éxito"));
      this.show2 = false;
      return null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    this.show2 = true;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    return null;
  }
  public Collection getListRemesa() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRemesa, "sigefirrhh.personal.movimientos.Remesa");
  }
  public long getIdRemesa() {
    return this.idRemesa;
  }
  public void setIdRemesa(long l) {
    this.idRemesa = l;
  }
  public boolean isShow2() {
    return this.show2;
  }
}