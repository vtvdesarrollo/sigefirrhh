package sigefirrhh.personal.movimientos;

import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.ExpedienteBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class IngresoTrabajadorRegistroBeanBusiness
{
  Logger log = Logger.getLogger(IngresoTrabajadorRegistroBeanBusiness.class.getName());

  private TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private ExpedienteBusiness expedienteBusiness = new ExpedienteBusiness();
  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();
  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();

  public long ingresarTrabajador(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones, int paso)
    throws Exception
  {
    this.txn.open();
    PersistenceManager pm = PMThread.getPM();
    long idSueldoPromedio = 0L;

    RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(idRegistroCargos);

    registroCargos.getSituacion().equals("0");

    Personal personal = this.expedienteBusiness.findPersonalById(idPersonal);
    TipoPersonal tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    CausaMovimiento causaMovimiento = new CausaMovimiento();
    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());

    Trabajador trabajador = new Trabajador();

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try
    {
      trabajador = this.registrosBusiness.agregarTrabajador(personal, tipoPersonal, 
        organismo, registroCargos, 
        causaMovimiento, fechaIngreso, sueldoBasico, paso);
      pm.makePersistent(trabajador);

      sueldoPromedio.setTrabajador(trabajador);
      this.log.error("GRABO TRABAJADOR");
      sueldoPromedio.setIdSueldoPromedio(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.SueldoPromedio"));
      pm.makePersistent(sueldoPromedio);
      idSueldoPromedio = sueldoPromedio.getIdSueldoPromedio();
      this.log.error("GRABO SUELDOPROMEDIO");

      registroCargos.setSituacion("O");
      registroCargos.setEstatus("3");
      registroCargos.setTrabajador(trabajador);

      this.registrosBusiness.buscarProximaNomina(trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());

      Collection col = new ArrayList();
      col.addAll(this.registrosBusiness.agregarConceptosFijos(trabajador, registroCargos, sueldoBasico, pagarRetroactivo));
      Iterator iter = col.iterator();
      while (iter.hasNext()) {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iter.next();
        pm.makePersistent(conceptoFijo);
      }

      this.log.error("GRABO CONCEPTOFIJO");

      this.txn.close();
      pm.close();
    }
    catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }

    try
    {
      this.txn.open();

      pm = PMThread.getPM();

      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      sueldoPromedio = this.trabajadorBusiness.findSueldoPromedioById(idSueldoPromedio);
      tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

      organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());
      causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);
      trabajador = this.trabajadorBusiness.findTrabajadorById(sueldoPromedio.getTrabajador().getIdTrabajador());

      this.calcularSueldosPromedioBeanBusiness.calcularUnTrabajadorParaMovimientos(trabajador, numeroMovimiento);
      this.registrosBusiness.borrarConceptosCero(trabajador.getIdTrabajador());

      this.log.error("RECALCULO CONCEPTOS");

      if (trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) > 0) {
        this.registrosBusiness.fraccionarConceptos(trabajador);
        this.log.error("FRACCIONO CONCEPTOS");
        if (!trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S")) {
          this.registrosBusiness.calcularLunesFraccionados(trabajador);
          this.log.error("Trabajador Lunes Fraccionados Lunes Primera Quincena " + trabajador.getLunesPrimera());
          this.log.error("Trabajador Lunes Fraccionados Lunes Segunda Quincena " + trabajador.getLunesSegunda());
        }

      }

      if (!trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S")) {
        if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) == 0) && 
          (this.registrosBusiness.getDia() == 16)) {
          this.registrosBusiness.calcularLunesFraccionados(trabajador);
        }
        if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) < 0) && 
          (trabajador.getFechaIngreso().getMonth() + 1 == this.registrosBusiness.getMes())) {
          this.registrosBusiness.calcularLunesFraccionados(trabajador);
        }
      }

      if ((!trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S")) && 
        (this.registrosBusiness.getFechaProximaNomina().compareTo(trabajador.getFechaIngreso()) > 0) && 
        (pagarRetroactivo.equals("S")))
      {
        Collection colRetroactivo = new ArrayList();

        colRetroactivo.addAll(this.registrosBusiness.calcularRetroactivos(trabajador));
        Iterator iterRetroactivo = colRetroactivo.iterator();
        while (iterRetroactivo.hasNext()) {
          this.log.error("PASO While");
          ConceptoVariable conceptoVariable = (ConceptoVariable)iterRetroactivo.next();
          pm.makePersistent(conceptoVariable);
        }
        this.registrosBusiness.calcularLunesRetroactivo(trabajador);
        this.log.error("CALCULO RETROACTIVOS");

        this.log.error("Trabajador Lunes Retroactivo " + trabajador.getLunesRetroactivo());
      }

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaIngreso, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, codConcurso, observaciones, usuario.getUsuario(), "4");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      RegistroSitp registroSitp = new RegistroSitp();
      registroSitp = this.registrosBusiness.agregarRegistroSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaIngreso, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones);

      pm.makePersistent(registroSitp);

      this.log.error("GRABO REGISTROSITP");

      this.txn.close();
    } catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      this.log.error("Excepcion controlada:", e);

      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }

    return trabajador.getIdTrabajador();
  }

  public long registrarContinuidadTrabajadorRegistro(long idTrabajador, long idTrabajadorEgresado)
  {
    Trabajador trabajador = null;
    Trabajador trabajadorEgresado = null;
    try
    {
      PersistenceManager pm = PMThread.getPM();

      trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);
      trabajadorEgresado = this.trabajadorBusiness.findTrabajadorById(idTrabajadorEgresado);

      pm.makeTransient(trabajadorEgresado);

      trabajador.setFechaIngreso(trabajadorEgresado.getFechaIngreso());
      trabajador.setFechaAntiguedad(trabajadorEgresado.getFechaAntiguedad());
      trabajador.setFechaVacaciones(trabajadorEgresado.getFechaVacaciones());
      trabajador.setFechaIngresoApn(trabajadorEgresado.getFechaIngresoApn());
      trabajador.setFechaPrestaciones(trabajadorEgresado.getFechaPrestaciones());

      trabajador.setDiaIngreso(trabajadorEgresado.getDiaIngreso());
      trabajador.setMesIngreso(trabajadorEgresado.getMesIngreso());
      trabajador.setAnioIngreso(trabajadorEgresado.getAnioIngreso());

      trabajador.setDiaAntiguedad(trabajadorEgresado.getDiaAntiguedad());
      trabajador.setMesAntiguedad(trabajadorEgresado.getMesAntiguedad());
      trabajador.setAnioAntiguedad(trabajadorEgresado.getAnioAntiguedad());

      trabajador.setDiaVacaciones(trabajadorEgresado.getDiaVacaciones());
      trabajador.setMesVacaciones(trabajadorEgresado.getMesVacaciones());
      trabajador.setAnioVacaciones(trabajadorEgresado.getAnioVacaciones());

      trabajador.setDiaIngresoApn(trabajadorEgresado.getDiaIngresoApn());
      trabajador.setMesIngresoApn(trabajadorEgresado.getMesIngresoApn());
      trabajador.setAnioIngresoApn(trabajadorEgresado.getAnioIngresoApn());

      trabajador.setDiaPrestaciones(trabajadorEgresado.getDiaPrestaciones());
      trabajador.setMesPrestaciones(trabajadorEgresado.getMesPrestaciones());
      trabajador.setAnioPrestaciones(trabajadorEgresado.getAnioPrestaciones());

      trabajador.setCotizaFju(trabajadorEgresado.getCotizaFju());
      trabajador.setCotizaLph(trabajadorEgresado.getCotizaLph());
      trabajador.setCotizaSpf(trabajadorEgresado.getCotizaSpf());
      trabajador.setCotizaSso(trabajadorEgresado.getCotizaSso());

      trabajador.setPorcentajeIslr(trabajadorEgresado.getPorcentajeIslr());

      trabajador.setBancoNomina(trabajadorEgresado.getBancoNomina());
      trabajador.setBancoLph(trabajadorEgresado.getBancoLph());
      trabajador.setBancoFid(trabajadorEgresado.getBancoFid());

      trabajador.setCuentaFid(trabajadorEgresado.getCuentaFid());
      trabajador.setCuentaLph(trabajadorEgresado.getCuentaLph());
      trabajador.setCuentaNomina(trabajadorEgresado.getCuentaNomina());
      trabajador.setTipoCtaNomina(trabajadorEgresado.getTipoCtaNomina());

      trabajador.setFormaPago(trabajadorEgresado.getFormaPago());
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);

      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return trabajador.getIdTrabajador();
  }
}