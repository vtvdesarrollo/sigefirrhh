package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ReincorporacionSinRegistroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReincorporacionSinRegistroForm.class.getName());
  private Date fechaEgresoReal;
  private Date fechaSalidaNomina;
  private String remesa;
  private int numeroMovimiento;
  private String observaciones;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String selectCausaPersonal;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colConceptoTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colTrabajador;
  private String selectConceptoTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Collection colRegistroCargos;
  private Collection colCausaPersonal;
  private boolean showRegistroCargosAux;
  private CausaPersonal causaPersonal;
  private boolean showFieldsAux;
  private RegistroCargos registroCargos;
  private boolean showButtonAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private Date fechaIngreso;
  private String selectIdRegistroCargos;
  private String pagarRetroactivo;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private boolean showData;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  private Object stateScrollConceptoFijoByTrabajador = null;
  private Object stateResultConceptoFijoByTrabajador = null;

  public boolean isShowRegistroCargos()
  {
    return (this.colRegistroCargos != null) && (this.showRegistroCargosAux);
  }
  public boolean isShowFields() {
    return this.showFieldsAux;
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      long idCausaPersonal;
      long idCausaPersonal;
      if (this.trabajador.getEstatus().equals("S"))
        idCausaPersonal = 16L;
      else {
        idCausaPersonal = 15L;
      }

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroMovimientoSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);
      this.movimientosNoGenFacade.licenciaSuspensionConSinRegistro(this.trabajador.getIdTrabajador(), this.fechaIngreso, null, idCausaPersonal, this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.fechaPuntoCuenta, this.puntoCuenta, this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }

  public String getSelectTrabajador()
  {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;

    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador);
    }

    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public ReincorporacionSinRegistroForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.colCausaPersonal = new ArrayList();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getFindColTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("N", "N", this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "S");

      if (this.resultTrabajador.isEmpty()) {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "P");
      }

      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;

    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "S");
      if (this.resultTrabajador.isEmpty()) {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
          this.findTrabajadorCodigoNomina, "P");
      }

      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;

    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String showReincorporacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();
      this.show = false;
      selectTrabajador();

      if ((!this.trabajador.getEstatus().equals("P")) && (!this.trabajador.getEstatus().equals("S")) && (
        (this.trabajador.getSituacion().equals("8")) || (this.trabajador.getSituacion().equals("9")) || (this.trabajador.getSituacion().equals("12")))) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Movimiento permitido solo para trabajadores con licencia o suspendidos", ""));
        return "cancel";
      }

      this.show = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);

      idClasificacionPersonal = this.trabajador.getTipoPersonal().getClasificacionPersonal().getIdClasificacionPersonal();
    }
    catch (Exception e)
    {
      long idClasificacionPersonal;
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String Ejecutar()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    return "cancel";
  }

  public String abort()
  {
    this.selected = false;
    resetResult();
    this.colCausaPersonal = null;
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.fechaIngreso = null;
    this.numeroMovimiento = 0;
    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";
    this.observaciones = "";
    return "cancel";
  }

  public String abortUpdate()
  {
    this.selected = false;
    this.result = null;
    this.showResult = false;

    return "cancel";
  }

  public boolean isShow() {
    return this.show;
  }

  public boolean isShowData() {
    return (this.show) && (this.selectedTrabajador);
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }

  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }

  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }

  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }

  public Date getFechaEgresoReal() {
    return this.fechaEgresoReal;
  }
  public void setFechaEgresoReal(Date fechaEgresoReal) {
    this.fechaEgresoReal = fechaEgresoReal;
  }
  public Date getFechaSalidaNomina() {
    return this.fechaSalidaNomina;
  }
  public void setFechaSalidaNomina(Date fechaSalidaNomina) {
    this.fechaSalidaNomina = fechaSalidaNomina;
  }
  public String getSelectCausaPersonal() {
    return this.selectCausaPersonal;
  }
  public void setSelectCausaPersonal(String string) {
    this.selectCausaPersonal = string;
  }
  public CausaPersonal getCausaPersonal() {
    return this.causaPersonal;
  }
  public void setCausaPersonal(CausaPersonal causaPersonal) {
    this.causaPersonal = causaPersonal;
  }
  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public void setDescripcionCargo(String descripcionCargo) {
    this.descripcionCargo = descripcionCargo;
  }
  public Date getFechaIngreso() {
    return this.fechaIngreso;
  }
  public void setFechaIngreso(Date fechaIngreso) {
    this.fechaIngreso = fechaIngreso;
  }
  public int getGrado() {
    return this.grado;
  }
  public void setGrado(int grado) {
    this.grado = grado;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public void setNombreDependencia(String nombreDependencia) {
    this.nombreDependencia = nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public void setNombreRegion(String nombreRegion) {
    this.nombreRegion = nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public void setNombreSede(String nombreSede) {
    this.nombreSede = nombreSede;
  }
  public double getSueldo() {
    return this.sueldo;
  }
  public void setSueldo(double sueldo) {
    this.sueldo = sueldo;
  }
  public String getSelectIdRegistroCargos() {
    return this.selectIdRegistroCargos;
  }
  public void setSelectIdRegistroCargos(String selectIdRegistroCargos) {
    this.selectIdRegistroCargos = selectIdRegistroCargos;
  }
  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }
  public void setPagarRetroactivo(String pagarRetroactivo) {
    this.pagarRetroactivo = pagarRetroactivo;
  }
  public String getCodConcurso() {
    return this.codConcurso;
  }
  public void setCodConcurso(String codConcurso) {
    this.codConcurso = codConcurso;
  }
  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
}