package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class EnviarRemesaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EnviarRemesaForm.class.getName());
  private Collection listRemesa;
  private LoginSession login;
  private MovimientosNoGenFacade movimientosFacade = new MovimientosNoGenFacade();
  private long idRemesa;
  private boolean show2 = false;

  private int registros = 0;

  public EnviarRemesaForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh() {
    try {
      this.listRemesa = new MovimientosNoGenFacade().findRemesaByEstatus(this.login.getIdOrganismo(), "C", this.login.getIdUsuario());
    } catch (Exception e) {
      this.listRemesa = new ArrayList();
    }
  }

  public String enviar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      Remesa remesa = this.movimientosFacade.findRemesaById(this.idRemesa);
      remesa.setEstatus("E");
      remesa.setFechaEnvio(new Date());
      this.movimientosFacade.updateRemesa(remesa);
      try
      {
        this.movimientosFacade.enviarRemesa(remesa);
        refresh();
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', remesa);

        context.addMessage("success_add", new FacesMessage("Se envió con éxito"));
      } catch (Exception e) {
        remesa.setEstatus("C");
        remesa.setFechaEnvio(null);
        this.movimientosFacade.updateRemesa(remesa);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pudo enviar la remesa", ""));
        log.error("Excepcion controlada:", e);
      }

      this.show2 = false;
      return null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    this.show2 = true;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    return null;
  }
  public Collection getListRemesa() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRemesa, "sigefirrhh.personal.movimientos.Remesa");
  }
  public long getIdRemesa() {
    return this.idRemesa;
  }
  public void setIdRemesa(long l) {
    this.idRemesa = l;
  }
  public boolean isShow2() {
    return this.show2;
  }
}