package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.UsuarioBeanBusiness;

public class RemesaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRemesa(Remesa remesa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Remesa remesaNew = 
      (Remesa)BeanUtils.cloneBean(
      remesa);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (remesaNew.getOrganismo() != null) {
      remesaNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        remesaNew.getOrganismo().getIdOrganismo()));
    }

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (remesaNew.getUsuario() != null) {
      remesaNew.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        remesaNew.getUsuario().getIdUsuario()));
    }
    pm.makePersistent(remesaNew);
  }

  public void updateRemesa(Remesa remesa) throws Exception
  {
    Remesa remesaModify = 
      findRemesaById(remesa.getIdRemesa());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (remesa.getOrganismo() != null) {
      remesa.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        remesa.getOrganismo().getIdOrganismo()));
    }

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (remesa.getUsuario() != null) {
      remesa.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        remesa.getUsuario().getIdUsuario()));
    }

    BeanUtils.copyProperties(remesaModify, remesa);
  }

  public void deleteRemesa(Remesa remesa) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Remesa remesaDelete = 
      findRemesaById(remesa.getIdRemesa());
    pm.deletePersistent(remesaDelete);
  }

  public Remesa findRemesaById(long idRemesa) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRemesa == pIdRemesa";
    Query query = pm.newQuery(Remesa.class, filter);

    query.declareParameters("long pIdRemesa");

    parameters.put("pIdRemesa", new Long(idRemesa));

    Collection colRemesa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRemesa.iterator();
    return (Remesa)iterator.next();
  }

  public Collection findRemesaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent remesaExtent = pm.getExtent(
      Remesa.class, true);
    Query query = pm.newQuery(remesaExtent);
    query.setOrdering("anio descending, numero ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Remesa.class, filter);

    query.declareParameters("int pAnio, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio descending, numero ascending");

    Collection colRemesa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRemesa);

    return colRemesa;
  }

  public Collection findByNumero(int numero, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "numero == pNumero &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Remesa.class, filter);

    query.declareParameters("int pNumero, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNumero", new Integer(numero));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio descending, numero ascending");

    Collection colRemesa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRemesa);

    return colRemesa;
  }

  public Collection findByEstatus(String estatus, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "estatus == pEstatus &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Remesa.class, filter);

    query.declareParameters("java.lang.String pEstatus, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pEstatus", new String(estatus));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio descending, numero ascending");

    Collection colRemesa = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRemesa);

    return colRemesa;
  }
}