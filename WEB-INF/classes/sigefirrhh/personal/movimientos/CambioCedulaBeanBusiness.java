package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoBusiness;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CambioCedulaBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(CambioCedulaBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();

  private PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();
  private TrayectoriaBeanBusiness trayectoriaBeanBusiness = new TrayectoriaBeanBusiness();
  private MovimientoSitpBeanBusiness movimientositpBeanBusiness = new MovimientoSitpBeanBusiness();
  private RegistroSitpBeanBusiness registrositpBeanBusiness = new RegistroSitpBeanBusiness();

  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private CargoBusiness cargoBusiness = new CargoBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();

  public boolean actualizar(long idTrabajador, int cedula, Organismo organismo2, long idUsuario, String observaciones)
    throws Exception
  {
    this.txn.open();
    PersistenceManager pm = PMThread.getPM();

    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    this.log.error("cambio de cedula 2");

    ResultSet rsPersonal = null;
    PreparedStatement stPersonal = null;
    StringBuffer sql = new StringBuffer();
    sql = new StringBuffer();
    sql.append("select p.cedula from  personal p ");
    sql.append("  where p.cedula = ? ");

    this.log.error("cambio de cedula 3");
    stPersonal = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);
    stPersonal.setLong(1, cedula);
    rsPersonal = stPersonal.executeQuery();

    if (rsPersonal.next()) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Esta cédula ya esta registrada");
      throw error;
    }

    this.log.error("cambio de cedula 4");

    Trabajador trabajador = new Trabajador();
    Personal personal = new Personal();
    Trayectoria trayectoria = new Trayectoria();
    MovimientoSitp movimientositp = new MovimientoSitp();
    RegistroSitp registrositp = new RegistroSitp();

    trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);
    personal = this.personalBeanBusiness.findPersonalById(trabajador.getPersonal().getIdPersonal());

    trayectoria = this.trayectoriaBeanBusiness.findTrayectoriaByIdPersonal(trabajador.getPersonal().getIdPersonal());
    movimientositp = this.movimientositpBeanBusiness.findMovimientoSitpByIdPersonal(trabajador.getPersonal().getIdPersonal());
    registrositp = this.registrositpBeanBusiness.findRegistroSitpByIdPersonal(trabajador.getPersonal().getIdPersonal());

    this.log.error("cambio de cedula 5");
    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      trabajador.setCedula(cedula);
      personal.setCedula(cedula);
      if (trayectoria != null)
        trayectoria.setCedula(cedula);
      if (movimientositp != null)
        movimientositp.setCedula(cedula);
      if (registrositp != null) {
        registrositp.setCedula(cedula);
      }
      this.txn.close();
      this.log.error("cambio de cedula 6");
    }
    catch (ErrorSistema a)
    {
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return true;
  }
}