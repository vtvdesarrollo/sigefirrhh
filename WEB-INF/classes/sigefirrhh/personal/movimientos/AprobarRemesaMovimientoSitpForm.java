package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteNoGenFacade;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.sistema.RegistrarAuditoria;

public class AprobarRemesaMovimientoSitpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AprobarRemesaMovimientoSitpForm.class.getName());
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private boolean showData;
  private boolean showFields;
  private boolean showReport;
  private boolean selectedMovimiento;
  private int reportId;
  private String reportName;
  private LoginSession login;
  private long idMovimientoSitp = 0L;
  private String observaciones;
  private int numeroMovimiento;
  private Date fechaMovimiento;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private ExpedienteNoGenFacade expedienteNoGenFacade = new ExpedienteNoGenFacade();
  private boolean showRemesaMovimientoSitpByRemesa;
  private String findSelectRemesa;
  private Collection findColRemesa;
  private Collection colRemesa;
  private Collection colMovimientoSitp;
  private String selectRemesa;
  private String selectMovimientoSitp;
  private MovimientoSitp movimientoSitp = null;

  private Object stateResultRemesaMovimientoSitpByRemesa = null;

  public String getFindSelectRemesa()
  {
    return this.findSelectRemesa;
  }
  public void setFindSelectRemesa(String valRemesa) {
    this.findSelectRemesa = valRemesa;
  }

  public Collection getFindColRemesa() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRemesa.iterator();
    Remesa remesa = null;
    while (iterator.hasNext()) {
      remesa = (Remesa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(remesa.getIdRemesa()), 
        remesa.toString()));
    }
    return col;
  }

  public String getSelectRemesa()
  {
    return this.selectRemesa;
  }
  public void setSelectRemesa(String valRemesa) {
    Iterator iterator = this.colRemesa.iterator();
    Remesa remesa = null;

    this.selectRemesa = valRemesa;
  }
  public String getSelectMovimientoSitp() {
    return this.selectMovimientoSitp;
  }

  public void setSelectMovimientoSitp(String valMovimientoSitp) {
    this.selectMovimientoSitp = valMovimientoSitp;
  }
  public Collection getResult() {
    return this.result;
  }

  public AprobarRemesaMovimientoSitpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRemesa()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRemesa.iterator();
    Remesa remesa = null;
    while (iterator.hasNext()) {
      remesa = (Remesa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(remesa.getIdRemesa()), 
        remesa.toString()));
    }
    return col;
  }

  public Collection getColMovimientoSitp()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMovimientoSitp.iterator();
    MovimientoSitp movimientoSitp = null;
    while (iterator.hasNext()) {
      movimientoSitp = (MovimientoSitp)iterator.next();
      col.add(new SelectItem(
        String.valueOf(movimientoSitp.getIdMovimientoSitp()), 
        movimientoSitp.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColRemesa = 
        this.movimientosNoGenFacade.findRemesaByEstatus(
        this.login.getOrganismo().getIdOrganismo(), "E", this.login.getIdUsuario());

      this.colRemesa = 
        this.movimientosNoGenFacade.findRemesaByEstatus(
        this.login.getOrganismo().getIdOrganismo(), "E", this.login.getIdUsuario());

      this.colMovimientoSitp = 
        this.movimientosNoGenFacade.findMovimientoSitpForRemesa(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void refreshMovimientoSitp()
  {
    try {
      this.colMovimientoSitp = 
        this.movimientosNoGenFacade.findMovimientoSitpForRemesa(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRemesaMovimientoSitpByRemesa()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    Collection col = null;
    try {
      resetResult();
      col = 
        this.movimientosNoGenFacade.findMovimientoSitpByRemesaAndStatus(Long.valueOf(this.findSelectRemesa).longValue(), "2");

      this.result = col;
      this.showRemesaMovimientoSitpByRemesa = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRemesaMovimientoSitpByRemesa)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado, [" + e + "]", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRemesa = null;
    this.selectedMovimiento = false;

    return null;
  }

  public boolean isShowRemesaMovimientoSitpByRemesa() {
    return this.showRemesaMovimientoSitpByRemesa;
  }

  public String selectRemesaMovimientoSitp()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRemesa = null;
    this.selectMovimientoSitp = null;

    long idRemesaMovimientoSitp = 
      Long.parseLong((String)requestParameterMap.get("idRemesaMovimientoSitp"));

    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.showRemesaMovimientoSitpByRemesa = false;
    this.showData = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.adding)
      {
        MovimientoSitp movimientoSitp = this.movimientosNoGenFacade.findMovimientoSitpById(Long.valueOf(this.selectMovimientoSitp).longValue());
        Remesa remesa = this.movimientosNoGenFacade.findRemesaById(Long.valueOf(this.selectRemesa).longValue());

        Trayectoria trayectoria = (Trayectoria)this.expedienteNoGenFacade.findTrayectoriaByAnioAndNumeroMovimiento(movimientoSitp.getAnio(), movimientoSitp.getNumeroMovimiento()).iterator().next();

        trayectoria.setNumeroRemesa(String.valueOf(remesa.getNumero()));

        this.expedienteNoGenFacade.updateTrayectoria(trayectoria);

        movimientoSitp.setRemesa(remesa);
        movimientoSitp.setEstatus("2");
        this.movimientosNoGenFacade.updateMovimientoSitp(movimientoSitp);

        refreshMovimientoSitp();
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', movimientoSitp);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      }

      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }

  public String selectMovimiento() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      Map requestParameterMap = 
        context.getExternalContext().getRequestParameterMap();

      this.idMovimientoSitp = 
        Long.parseLong((String)requestParameterMap.get("idMovimientoSitp"));
      this.movimientoSitp = this.movimientosNoGenFacade.findMovimientoSitpById(this.idMovimientoSitp);
    }
    catch (Exception e) {
      log.error("Error: " + e);
      log.error("Excepcion controlada:", e);
    }
    this.deleting = true;
    this.showData = true;
    this.selectedMovimiento = true;
    this.showFields = true;
    this.showRemesaMovimientoSitpByRemesa = false;

    return null;
  }
  public String ejecutar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaMovimiento == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha del movimiento de aprobación", ""));
        return null;
      }

      this.movimientoSitp.setEstatus("4");

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroMovimientoSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.movimientosNoGenFacade.updateMovimientoSitp(this.movimientoSitp);
      this.idMovimientoSitp = this.movimientoSitp.getIdMovimientoSitp();

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'R', this.movimientoSitp);

      context.addMessage("success_delete", new FacesMessage("Se Aprobó el Movimiento con éxito"));
      this.showReport = true;
      this.deleting = false;
      this.selected = false;
      this.showFields = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede Aprobar ", ""));
    }
    return "cancel";
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.showFields = false;
    this.showData = false;
    this.showFields = false;
    resetResult();
    this.movimientoSitp = null;

    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selectedMovimiento) && (!this.showRemesaMovimientoSitpByRemesa);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public MovimientoSitp getMovimientoSitp()
  {
    return this.movimientoSitp;
  }
  public void setMovimientoSitp(MovimientoSitp movimientoSitp) {
    this.movimientoSitp = movimientoSitp;
  }
  public boolean isSelected() {
    return this.selected;
  }
  public void setSelected(boolean selected) {
    this.selected = selected;
  }
  public void setAdding(boolean adding) {
    this.adding = adding;
  }
  public void setDeleting(boolean deleting) {
    this.deleting = deleting;
  }
  public void setEditing(boolean editing) {
    this.editing = editing;
  }
  public boolean isShowFields() {
    return this.showFields;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }
  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public long getIdMovimientoSitp() {
    return this.idMovimientoSitp;
  }
  public void setIdMovimientoSitp(long idMovimientoSitp) {
    this.idMovimientoSitp = idMovimientoSitp;
  }
  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
  public boolean isShowReport() {
    return this.showReport;
  }
  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }
  public boolean isSelectedMovimiento() {
    return this.selectedMovimiento;
  }
}