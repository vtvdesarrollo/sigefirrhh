package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.ManualPersonal;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class MovimientoTrabajadorSinRegistroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MovimientoTrabajadorSinRegistroForm.class.getName());
  private String observaciones;
  private String pagarRetroactivo;
  private String tieneContinuidad;
  private boolean egresado;
  private Date fechaIngreso;
  private String remesa;
  private int numeroMovimiento;
  private int valor;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private TipoPersonal tipoPersonal;
  private int codigoNomina;
  private double horas;
  private Collection result;
  private boolean error;
  private boolean showData1;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();

  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private Personal personal;
  private Tabulador tabulador;
  private Cargo cargo;
  private String findSelectTrabajadorIdTipoPersonal;
  private String selectCausaPersonalMovimiento;
  private String selectCausaPersonalEgreso;
  private Collection colTipoPersonal;
  private Collection colCausaPersonalMovimiento;
  private Collection colCausaPersonalEgreso;
  private Collection colRegion;
  private Collection colDependencia;
  private Collection colSede;
  private Collection colManualCargo = new ArrayList();
  private Collection colCargo;
  private int findPersonalCedula;
  private String selectIdTipoPersonal;
  private String selectIdCausaPersonalMovimiento;
  private String selectIdRegistroCargos;
  private String selectIdRegion;
  private String selectIdSede;
  private String selectIdDependencia;
  private String selectIdManualCargo;
  private String selectIdCargo;
  private Date fechaEgresoReal;
  private Date fechaSalidaNomina;
  private boolean show;
  private boolean selected;
  private int scrollx;
  private int scrolly;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private boolean showFechaIngreso = true;
  private String findSelectTrabajador;
  private Collection colConceptoTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colTrabajador;
  private String selectConceptoTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private RegistroCargos registroCargos;
  private boolean showFieldsAux;
  private boolean showRegistroCargosAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private CausaPersonal causaPersonal;
  private boolean showButtonAux;

  public String getSelectIdCargo()
  {
    return this.selectIdCargo;
  }
  public void setSelectIdCargo(String selectIdCargo) {
    this.selectIdCargo = selectIdCargo;
  }
  public String getSelectIdDependencia() {
    return this.selectIdDependencia;
  }
  public Date getFechaEgresoReal() {
    return this.fechaEgresoReal;
  }
  public void setFechaEgresoReal(Date fechaEgresoReal) {
    this.fechaEgresoReal = fechaEgresoReal;
  }
  public void setSelectIdDependencia(String selectIdDependencia) {
    this.selectIdDependencia = selectIdDependencia;
  }
  public String getSelectIdManualCargo() {
    return this.selectIdManualCargo;
  }
  public void setSelectIdManualCargo(String selectIdManualCargo) {
    this.selectIdManualCargo = selectIdManualCargo;
  }
  public String getSelectIdRegion() {
    return this.selectIdRegion;
  }
  public void setSelectIdRegion(String selectIdRegion) {
    this.selectIdRegion = selectIdRegion;
  }
  public String getSelectIdSede() {
    return this.selectIdSede;
  }
  public void setSelectIdSede(String selectIdSede) {
    this.selectIdSede = selectIdSede;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public void setFindTrabajadorCedula(int findTrabajadorCedula) {
    this.findTrabajadorCedula = findTrabajadorCedula;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int findTrabajadorCodigoNomina) {
    this.findTrabajadorCodigoNomina = findTrabajadorCodigoNomina;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public void setTrabajador(Trabajador trabajador) {
    this.trabajador = trabajador;
  }

  public boolean isShowRegion()
  {
    return this.showData1;
  }
  public boolean isShowSede() {
    return this.colSede != null;
  }
  public boolean isShowDependencia() {
    return this.colDependencia != null;
  }
  public boolean isShowManualCargo() {
    return this.colManualCargo != null;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public boolean isShowCausaPersonalMovimiento() {
    return (this.colCausaPersonalMovimiento != null) && (this.showData1);
  }
  public boolean isShowFields() {
    return (this.showData1) && (this.showFieldsAux);
  }
  public boolean isShowButton() {
    return this.showButtonAux;
  }
  public boolean isShowFechaIngreso() {
    return this.showFechaIngreso;
  }
  public void setShowFechaIngreso(boolean showFechaIngreso) {
    this.showFechaIngreso = showFechaIngreso;
  }

  public String redirectPersonal() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    try
    {
      externalContext.redirect("/sigefirrhh/sigefirrhh/personal/expediente/Personal.jsf");
      context.responseComplete();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    return "success";
  }
  public Collection getColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public String ejecutar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaIngreso == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de Ingreso del movimiento", ""));
        return null;
      }

      long idCausaPersonalEgreso = Long.valueOf(
        this.selectCausaPersonalMovimiento).longValue();

      Calendar calendar = Calendar.getInstance();
      calendar.setTime(this.fechaIngreso);
      calendar.add(5, -1);
      this.fechaEgresoReal = calendar.getTime();
      CausaPersonal causaPersonalEgreso = this.registroNoGenFacade.findCausaPersonalById(idCausaPersonalEgreso);

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.movimientosNoGenFacade.egresoTrabajadorRegistro(this.trabajador.getIdTrabajador(), this.fechaEgresoReal, this.fechaSalidaNomina, causaPersonalEgreso.getCausaMovimiento().getIdCausaMovimiento(), this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());

      log.error("Se procesó el Egreso con éxito");
      this.show = false;

      long idCausaPersonalMovimiento = Long.valueOf(
        this.selectCausaPersonalMovimiento).longValue();
      CausaPersonal causaPersonalMovimiento = this.registroNoGenFacade.findCausaPersonalById(idCausaPersonalMovimiento);

      if (this.valor == 3) {
        int valido = this.trabajadorNoGenFacade.validarFechaIngreso(this.personal.getIdPersonal(), this.login.getIdOrganismo(), this.fechaIngreso);
        if (valido == 1) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de ingreso debe ser mayor a la última fecha de egreso del trabajador en el organismo", ""));
          return null;
        }

      }

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      long idTrabajador = this.movimientosNoGenFacade.ingresoTrabajadorSinRegistro(this.trabajador.getPersonal().getIdPersonal(), this.tipoPersonal.getIdTipoPersonal(), this.fechaIngreso, causaPersonalMovimiento.getCausaMovimiento().getIdCausaMovimiento(), this.sueldo, this.numeroMovimiento, null, this.login.getIdUsuario(), Long.valueOf(this.selectIdDependencia).longValue(), Long.valueOf(this.selectIdCargo).longValue(), this.codigoNomina, this.horas, this.fechaPuntoCuenta, this.puntoCuenta, this.codConcurso, this.pagarRetroactivo, this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.personal, this.personal);

      log.error("Se procesó el Ingreso con éxito");

      if (this.tieneContinuidad.equals("S")) {
        this.movimientosNoGenFacade.registrarContinuidadTrabajadorRegistro(idTrabajador, this.trabajador.getIdTrabajador());

        log.error("Se procesó la Continuidad con éxito");
      }

      context.addMessage("success_add", new FacesMessage("Se procesó el Cambio de Tipo de Personal con éxito"));
    } catch (ErrorSistema e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getDescription(), ""));
      log.error("Excepcion controlada:", e);
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      log.error("Excepcion controlada:", e);
    }
    abort();

    return "cancel";
  }

  public void changeRegion(ValueChangeEvent event)
  {
    long idRegion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      log.error("idRegion " + idRegion);

      this.colSede = this.estructuraFacade.findSedeByRegion(idRegion, this.login.getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeSede(ValueChangeEvent event)
  {
    long idSede = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colDependencia = this.estructuraFacade.findDependenciaBySede(idSede, this.login.getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeManualCargo(ValueChangeEvent event)
  {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    log.error("Valor idManualCargo:" + idManualCargo);
    try
    {
      this.colCargo = null;

      this.colCargo = this.cargoNoGenFacade.findCargoByManualCargo(idManualCargo);
      ManualCargo manualCargo = this.cargoNoGenFacade.findManualCargoById(idManualCargo);
      log.error("Valor manualCargo:" + manualCargo);
      if (manualCargo.getTabulador() != null) {
        this.tabulador = this.cargoNoGenFacade.findTabuladorById(manualCargo.getTabulador().getIdTabulador());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCargo(ValueChangeEvent event)
  {
    long idCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if ((idCargo != 0L) && 
        (this.tabulador != null))
      {
        this.cargo = this.cargoNoGenFacade.findCargoById(idCargo);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTieneContinuidad(ValueChangeEvent event)
  {
    String tieneContinuidad = 
      (String)event.getNewValue();
    if (tieneContinuidad.equals("S"))
      setShowFechaIngreso(false);
  }

  private void llenarCausaPersonal()
  {
    try {
      Collection colManualPersonal = this.cargoNoGenFacade.findManualPersonalByTipoPersonal(this.tipoPersonal.getIdTipoPersonal());
      Iterator iterManualPersonal = colManualPersonal.iterator();
      while (iterManualPersonal.hasNext()) {
        ManualPersonal manualPersonal = (ManualPersonal)iterManualPersonal.next();

        ManualCargo manualCargo = this.cargoNoGenFacade.findManualCargoById(manualPersonal.getManualCargo().getIdManualCargo());
        this.colManualCargo.add(manualCargo);
      }
      long idClasificacionPersonal = this.tipoPersonal.getClasificacionPersonal().getIdClasificacionPersonal();
      log.error("idClasificacionPersonal: " + idClasificacionPersonal);
      this.colCausaPersonalMovimiento = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimiento(idClasificacionPersonal, 3L);

      log.error("colCausaPersonalMovimiento: " + this.colCausaPersonalMovimiento);

      setColCausaPersonalEgreso(this.colCausaPersonalMovimiento);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarCampos()
  {
    try {
      this.codigoNomina = (this.trabajadorNoGenFacade.buscarUltimoCodigoNomina(this.tipoPersonal.getIdTipoPersonal()) + 1);
      DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.tabulador.getIdTabulador(), this.cargo.getGrado(), this.cargo.getSubGrado(), 1);
      this.sueldo = 0.0D;
      this.sueldo = detalleTabulador.getMonto();
    }
    catch (Exception localException) {
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    FacesContext context = FacesContext.getCurrentInstance();

    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    log.error("Valor de idTipoPersonal Personal: " + idTipoPersonal);
    try
    {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);

      this.valor = this.trabajadorNoGenFacade.verificarSiTrabajadorPuedeIngresar(this.trabajador.getPersonal().getIdPersonal(), this.login.getIdOrganismo(), this.tipoPersonal.getIdTipoPersonal());

      llenarCausaPersonal();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    log.debug("Valor de ChangeTipo Personal: " + this.valor);
  }

  public void continuarConEgresado() {
    this.egresado = false;
    llenarCausaPersonal();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (tipoPersonal.getIdTipoPersonal() != this.trabajador.getTipoPersonal().getIdTipoPersonal()) {
        col.add(new SelectItem(
          String.valueOf(tipoPersonal.getIdTipoPersonal()), 
          tipoPersonal.toString()));
      }
    }
    return col;
  }
  public Collection getColDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }

    return col;
  }
  public Collection getColSede() {
    Collection col = new ArrayList();
    Iterator iterator = this.colSede.iterator();
    Sede sede = null;
    while (iterator.hasNext()) {
      sede = (Sede)iterator.next();
      col.add(new SelectItem(
        String.valueOf(sede.getIdSede()), 
        sede.toString()));
    }

    return col;
  }
  public Collection getColManualCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }

    return col;
  }
  public Collection getColCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }

    return col;
  }
  public Collection getColCausaPersonalMovimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonalMovimiento.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }
    return col;
  }

  public Collection getColCausaPersonalEgreso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonalEgreso.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }

    return col;
  }
  public void setColCausaPersonalEgreso(Collection colCausaPersonalEgreso) {
    this.colCausaPersonalEgreso = colCausaPersonalEgreso;
  }

  public MovimientoTrabajadorSinRegistroForm() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.colCausaPersonalMovimiento = new ArrayList();
    setColCausaPersonalEgreso(new ArrayList());
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        MovimientoTrabajadorSinRegistroForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("N", "N", this.login.getIdUsuario(), this.login.getAdministrador());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("N", "N", this.login.getIdUsuario(), this.login.getAdministrador());

      this.colRegion = 
        this.estructuraFacade.findAllRegion();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String abort()
  {
    this.egresado = false;
    this.showData1 = false;
    this.colCausaPersonalMovimiento = null;
    this.selectIdTipoPersonal = "0";
    this.selectIdRegistroCargos = "0";
    this.selectIdCausaPersonalMovimiento = "0";
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaIngreso = null;
    this.sueldo = 0.0D;
    this.numeroMovimiento = 0;
    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";
    this.observaciones = "";
    return "cancel";
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "A");
      this.trabajador = ((Trabajador)this.resultTrabajador.iterator().next());
      this.showData1 = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showData1)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);

      this.showData1 = false;
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonalMovimiento = null;
    this.selectCausaPersonalEgreso = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }

      idClasificacionPersonal = this.trabajador.getTipoPersonal().getClasificacionPersonal().getIdClasificacionPersonal();
    }
    catch (Exception e)
    {
      long idClasificacionPersonal;
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonalEgreso = null;
    this.selectCausaPersonalMovimiento = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public boolean isShowData1() {
    return this.showData1;
  }

  public boolean isShowData() {
    return (this.show) && (this.selectedTrabajador);
  }

  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }

  public String getSelectCausaPersonalMovimiento()
  {
    return this.selectCausaPersonalMovimiento;
  }

  public void setSelectCausaPersonalMovimiento(String string)
  {
    this.selectCausaPersonalMovimiento = string;
  }
  public String getSelectCausaPersonalEgreso() {
    return this.selectCausaPersonalEgreso;
  }

  public void setSelectCausaPersonalEgreso(String string)
  {
    this.selectCausaPersonalEgreso = string;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public void setFindPersonalCedula(int findPersonalCedula) {
    this.findPersonalCedula = findPersonalCedula;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }

  public String getSelectIdCausaPersonalMovimiento() {
    return this.selectIdCausaPersonalMovimiento;
  }
  public void setSelectIdCausaPersonalMovimiento(String selectIdCausaPersonalMovimiento) {
    this.selectIdCausaPersonalMovimiento = selectIdCausaPersonalMovimiento;
  }

  public String getSelectIdRegistroCargos()
  {
    return this.selectIdRegistroCargos;
  }

  public void setSelectIdRegistroCargos(String string)
  {
    this.selectIdRegistroCargos = string;
  }

  public RegistroCargos getRegistroCargos()
  {
    return this.registroCargos;
  }

  public void setRegistroCargos(RegistroCargos cargos)
  {
    this.registroCargos = cargos;
  }

  public String getDescripcionCargo()
  {
    return this.descripcionCargo;
  }

  public int getGrado()
  {
    return this.grado;
  }

  public String getNombreDependencia()
  {
    return this.nombreDependencia;
  }

  public String getNombreRegion()
  {
    return this.nombreRegion;
  }

  public String getNombreSede()
  {
    return this.nombreSede;
  }

  public double getSueldo()
  {
    return this.sueldo;
  }

  public void setDescripcionCargo(String string)
  {
    this.descripcionCargo = string;
  }

  public void setGrado(int i)
  {
    this.grado = i;
  }

  public void setNombreDependencia(String string)
  {
    this.nombreDependencia = string;
  }

  public void setNombreRegion(String string)
  {
    this.nombreRegion = string;
  }

  public void setNombreSede(String string)
  {
    this.nombreSede = string;
  }

  public void setSueldo(double d)
  {
    this.sueldo = d;
  }

  public Date getFechaIngreso()
  {
    return this.fechaIngreso;
  }

  public void setFechaIngreso(Date date)
  {
    this.fechaIngreso = date;
  }

  public boolean isEgresado()
  {
    return this.egresado;
  }

  public void setEgresado(boolean b)
  {
    this.egresado = b;
  }

  public double getHoras()
  {
    return this.horas;
  }

  public void setHoras(double d)
  {
    this.horas = d;
  }

  public int getCodigoNomina()
  {
    return this.codigoNomina;
  }

  public void setCodigoNomina(int i)
  {
    this.codigoNomina = i;
  }

  public String getCodConcurso()
  {
    return this.codConcurso;
  }

  public Date getFechaPuntoCuenta()
  {
    return this.fechaPuntoCuenta;
  }

  public String getPuntoCuenta()
  {
    return this.puntoCuenta;
  }

  public void setCodConcurso(String string)
  {
    this.codConcurso = string;
  }

  public void setFechaPuntoCuenta(Date date)
  {
    this.fechaPuntoCuenta = date;
  }

  public void setPuntoCuenta(String string)
  {
    this.puntoCuenta = string;
  }

  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }
  public void setPagarRetroactivo(String pagarRetroactivo) {
    this.pagarRetroactivo = pagarRetroactivo;
  }
  public String getTieneContinuidad() {
    return this.tieneContinuidad;
  }
  public void setTieneContinuidad(String tieneContinuidad) {
    this.tieneContinuidad = tieneContinuidad;
  }
  public boolean isError() {
    return this.error;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public Collection getFindColTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public void setFindColTipoPersonal(Collection findColTipoPersonal) {
    this.findColTipoPersonal = findColTipoPersonal;
  }
}