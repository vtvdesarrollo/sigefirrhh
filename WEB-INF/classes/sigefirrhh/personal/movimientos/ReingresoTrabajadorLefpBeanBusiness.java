package sigefirrhh.personal.movimientos;

import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.ExpedienteBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ReingresoTrabajadorLefpBeanBusiness
{
  Logger log = Logger.getLogger(ReingresoTrabajadorLefpBeanBusiness.class.getName());

  private TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private ExpedienteBusiness expedienteBusiness = new ExpedienteBusiness();
  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();
  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  public long ingresarTrabajador(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    if (fechaPuntoCuenta != null)
    {
      if (fechaPuntoCuenta.compareTo(fechaIngreso) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La fecha de Punto de cuenta no puede ser mayor a la fecha de vigencia");
        throw error;
      }
    }
    this.txn.open();
    PersistenceManager pm = PMThread.getPM();
    long idSueldoPromedio = 0L;

    RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(idRegistroCargos);

    if (registroCargos.getRegistro().getAnio() < fechaIngreso.getYear() + 1900) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe Rac aprobado a la fecha de vigencia del movimiento del personal");
      throw error;
    }

    Personal personal = this.expedienteBusiness.findPersonalById(idPersonal);
    TipoPersonal tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    CausaMovimiento causaMovimiento = new CausaMovimiento();
    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(personal.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaIngreso);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }
    this.log.error("no encontró problemas en trayectoria");

    Trabajador trabajador = new Trabajador();

    SueldoPromedio sueldoPromedio = new SueldoPromedio();

    long idHistoricoCargos = 0L;
    try
    {
      trabajador = this.registrosBusiness.agregarTrabajador(personal, tipoPersonal, 
        organismo, registroCargos, 
        causaMovimiento, fechaIngreso, sueldoBasico, 1);
      pm.makePersistent(trabajador);

      sueldoPromedio.setTrabajador(trabajador);
      this.log.error("GRABO TRABAJADOR");
      sueldoPromedio.setIdSueldoPromedio(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.SueldoPromedio"));
      pm.makePersistent(sueldoPromedio);
      idSueldoPromedio = sueldoPromedio.getIdSueldoPromedio();
      this.log.error("GRABO SUELDOPROMEDIO");

      registroCargos.setSituacion("O");
      registroCargos.setEstatus("3");
      registroCargos.setTrabajador(trabajador);

      idHistoricoCargos = this.registrosBusiness.agregarhistoricoCargos(pm, 
        registroCargos.getRegistro(), 
        causaMovimiento, 
        registroCargos.getCargo(), 
        trabajador.getDependencia(), 
        registroCargos.getCodigoNomina(), 
        registroCargos.getSituacion(), 
        "2", fechaIngreso, 
        trabajador.getCedula(), 
        trabajador.getPersonal().getPrimerApellido(), 
        trabajador.getPersonal().getSegundoApellido(), 
        trabajador.getPersonal().getPrimerNombre(), 
        trabajador.getPersonal().getSegundoNombre(), 
        registroCargos.getHoras());

      this.log.error("AGREGO REGISTRO EN HISTORICOCARGOS");

      this.registrosBusiness.buscarProximaNomina(trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());

      Collection col = new ArrayList();
      col.addAll(this.registrosBusiness.agregarConceptosFijos(trabajador, registroCargos, sueldoBasico, pagarRetroactivo));
      Iterator iter = col.iterator();
      while (iter.hasNext()) {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iter.next();
        pm.makePersistent(conceptoFijo);
      }

      this.log.error("GRABO CONCEPTOFIJO");

      this.txn.close();
      pm.close();
    }
    catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e)
    {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }

    try
    {
      this.txn.open();

      pm = PMThread.getPM();

      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      sueldoPromedio = this.trabajadorBusiness.findSueldoPromedioById(idSueldoPromedio);
      tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

      organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());
      causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);
      trabajador = this.trabajadorBusiness.findTrabajadorById(sueldoPromedio.getTrabajador().getIdTrabajador());

      this.calcularSueldosPromedioBeanBusiness.calcularUnTrabajadorParaMovimientos(trabajador, numeroMovimiento);
      this.registrosBusiness.borrarConceptosCero(trabajador.getIdTrabajador());

      this.log.error("RECALCULO CONCEPTOS");

      if (trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) > 0) {
        this.registrosBusiness.fraccionarConceptos(trabajador);
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
        this.log.error("Trabajador Lunes Fraccionados Lunes Primera Quincena " + trabajador.getLunesPrimera());
        this.log.error("Trabajador Lunes Fraccionados Lunes Segunda Quincena " + trabajador.getLunesSegunda());
        this.log.error("FRACCIONO CONCEPTOS");
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) == 0) && (this.registrosBusiness.getDia() == 16)) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) < 0) && (trabajador.getFechaIngreso().getMonth() + 1 == this.registrosBusiness.getMes())) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) < 0) && 
        (trabajador.getFechaIngreso().getMonth() + 1 != this.registrosBusiness.getMes()) && 
        (this.registrosBusiness.getDia() == 16)) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((this.registrosBusiness.getFechaProximaNomina().compareTo(trabajador.getFechaIngreso()) > 0) && (pagarRetroactivo.equals("S")))
      {
        Collection colRetroactivo = new ArrayList();

        colRetroactivo.addAll(this.registrosBusiness.calcularRetroactivos(trabajador));
        Iterator iterRetroactivo = colRetroactivo.iterator();
        while (iterRetroactivo.hasNext()) {
          this.log.error("PASO While");
          ConceptoVariable conceptoVariable = (ConceptoVariable)iterRetroactivo.next();
          pm.makePersistent(conceptoVariable);
        }
        this.registrosBusiness.calcularLunesRetroactivo(trabajador);
        this.log.error("CALCULO RETROACTIVOS");

        this.log.error("Trabajador Lunes Retroactivo " + trabajador.getLunesRetroactivo());
      }

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaIngreso, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, codConcurso, observaciones, usuario.getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaIngreso, null, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones, 0.0D, 0.0D, 0.0D);

      pm.makePersistent(movimientoSitp);

      this.log.error("GRABO MOVIMIENTOSITP");

      this.txn.close();
    } catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      this.log.error("Excepcion controlada:", e);

      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }

    return trabajador.getIdTrabajador();
  }
}