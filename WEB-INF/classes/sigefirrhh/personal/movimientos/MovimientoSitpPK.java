package sigefirrhh.personal.movimientos;

import java.io.Serializable;

public class MovimientoSitpPK
  implements Serializable
{
  public long idMovimientoSitp;

  public MovimientoSitpPK()
  {
  }

  public MovimientoSitpPK(long idMovimientoSitp)
  {
    this.idMovimientoSitp = idMovimientoSitp;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MovimientoSitpPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MovimientoSitpPK thatPK)
  {
    return 
      this.idMovimientoSitp == thatPK.idMovimientoSitp;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMovimientoSitp)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMovimientoSitp);
  }
}