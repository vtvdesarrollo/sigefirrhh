package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.HistorialOrganismo;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ReincorporacionLefpBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(ReincorporacionLefpBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();

  public long actualizar(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones)
    throws Exception
  {
    long idMovimientoSitp = 0L;
    if (fechaPuntoCuenta != null)
    {
      if (fechaPuntoCuenta.compareTo(fechaMovimiento) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La fecha de Punto de cuenta no puede ser mayor a la fecha de vigencia");
        throw error;
      }
    }
    this.txn.open();

    PersistenceManager pm = PMThread.getPM();

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Trabajador trabajador = new Trabajador();
    trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }

    HistorialOrganismo historialOrganismo = new HistorialOrganismo();
    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaMovimiento, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, null, observaciones, usuario.getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaMovimiento, null, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones, 0.0D, 0.0D, 0.0D);
      pm.makePersistent(movimientoSitp);

      idMovimientoSitp = movimientoSitp.getIdMovimientoSitp();
      this.log.error("GRABO MOVIMIENTOSITP");

      Trabajador trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

      RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(trabajador.getRegistroCargos().getIdRegistroCargos());

      registroCargos.setCondicion("1");

      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      trabajadorEdit.setFechaUltimoMovimiento(fechaMovimiento);

      trabajadorEdit.setEstatus("A");
      trabajadorEdit.setSituacion("1");

      if (organismo.getAprobacionMpd().equals("S"))
        trabajador.setMovimiento("T");
      else {
        trabajador.setMovimiento("A");
      }

      this.log.error("termino");

      this.txn.close();
    } catch (ErrorSistema a) {
      this.txn.close();
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e) {
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return idMovimientoSitp;
  }
}