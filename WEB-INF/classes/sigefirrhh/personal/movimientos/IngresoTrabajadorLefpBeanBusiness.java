package sigefirrhh.personal.movimientos;

import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.ExpedienteBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.planificacion.plan.MovimientosPlan;
import sigefirrhh.planificacion.plan.MovimientosPlanNoGenBeanBusiness;
import sigefirrhh.planificacion.seleccion.Concurso;
import sigefirrhh.planificacion.seleccion.ConcursoCargo;
import sigefirrhh.planificacion.seleccion.ConcursoCargoBeanBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class IngresoTrabajadorLefpBeanBusiness
{
  Logger log = Logger.getLogger(IngresoTrabajadorLefpBeanBusiness.class.getName());

  private TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();

  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();

  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();

  private ExpedienteBusiness expedienteBusiness = new ExpedienteBusiness();

  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();

  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();
  private MovimientosPlanNoGenBeanBusiness movimientosPlanNoGenBeanBusiness = new MovimientosPlanNoGenBeanBusiness();

  public long ingresarTrabajador(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, long idConcursoCargo, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    this.registrosBusiness.validarFechas(fechaIngreso, fechaPuntoCuenta);

    this.txn.open();
    PersistenceManager pm = PMThread.getPM();
    long idSueldoPromedio = 0L;
    long idMovimientoSitp = 0L;
    long idTrayectoria = 0L;
    this.log.error("ingreso..........3");
    RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(idRegistroCargos);

    this.log.error("ingreso..........4");
    ConcursoCargoBeanBusiness concursoCargoBeanBusiness = new ConcursoCargoBeanBusiness();
    ConcursoCargo concursoCargo = new ConcursoCargo();
    this.log.error("ingreso..........4 pedro");
    if (idConcursoCargo != 0L) {
      concursoCargoBeanBusiness = new ConcursoCargoBeanBusiness();
      this.log.error("ingreso..........5");
      concursoCargo = concursoCargoBeanBusiness.findConcursoCargoById(idConcursoCargo);
      this.log.error("ingreso..........6");
      if (concursoCargo.getConcurso().getEntregaInforme() == null) {
        this.log.error("ingreso..........7");
        ErrorSistema error = new ErrorSistema();
        error.setDescription("No se ha registrado la fecha de cierre de concurso");
        throw error;
      }
      if (concursoCargo.getConcurso().getEntregaInforme().compareTo(fechaIngreso) >= 0) {
        this.log.error("ingreso..........8");
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La fecha de apertura del concurso no puede ser igual o mayor a la fecha de ingreso");
        throw error;
      }
    }
    if (registroCargos.getRegistro().getAnio() < fechaIngreso.getYear() + 1900) {
      this.log.error("ingreso..........9");
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe Rac aprobado a la fecha de vigencia del movimiento del personal");
      throw error;
    }
    this.log.error("ingreso..........pedro antes");
    Personal personal = this.expedienteBusiness.findPersonalById(idPersonal);
    TipoPersonal tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    long idMovimientosPlan = 0L;
    if (idCausaMovimiento == 1L) {
      idMovimientosPlan = this.movimientosPlanNoGenBeanBusiness.validarMovimiento(causaMovimiento.getIdCausaMovimiento());
      if (idMovimientosPlan < 1L) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("No hay movimientos planificados");
        throw error;
      }

    }

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(personal.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaIngreso);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }
    this.log.error("no encontró problemas en trayectoria");

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());

    Trabajador trabajador = new Trabajador();

    SueldoPromedio sueldoPromedio = new SueldoPromedio();

    long idHistoricoCargos = 0L;
    try
    {
      trabajador = this.registrosBusiness.agregarTrabajador(personal, tipoPersonal, 
        organismo, registroCargos, 
        causaMovimiento, fechaIngreso, sueldoBasico, 1);
      pm.makePersistent(trabajador);

      sueldoPromedio.setTrabajador(trabajador);
      this.log.error("GRABO TRABAJADOR");
      sueldoPromedio.setIdSueldoPromedio(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.SueldoPromedio"));
      pm.makePersistent(sueldoPromedio);
      idSueldoPromedio = sueldoPromedio.getIdSueldoPromedio();
      this.log.error("GRABO SUELDOPROMEDIO");

      registroCargos.setSituacion("O");
      registroCargos.setEstatus("3");
      registroCargos.setTrabajador(trabajador);

      idHistoricoCargos = this.registrosBusiness.agregarhistoricoCargos(pm, 
        registroCargos.getRegistro(), 
        causaMovimiento, 
        registroCargos.getCargo(), 
        trabajador.getDependencia(), 
        registroCargos.getCodigoNomina(), 
        registroCargos.getSituacion(), 
        "2", fechaIngreso, 
        trabajador.getCedula(), 
        trabajador.getPersonal().getPrimerApellido(), 
        trabajador.getPersonal().getSegundoApellido(), 
        trabajador.getPersonal().getPrimerNombre(), 
        trabajador.getPersonal().getSegundoNombre(), 
        registroCargos.getHoras());

      this.log.error("AGREGO REGISTRO EN HISTORICOCARGOS");
      try
      {
        this.registrosBusiness.buscarProximaNomina(trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());
      } catch (Exception e) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("El grupo de nómina no tiene la Seguridad Ordinaria inicializada");
        throw error;
      }

      Collection col = new ArrayList();
      col.addAll(this.registrosBusiness.agregarConceptosFijos(trabajador, registroCargos, sueldoBasico, pagarRetroactivo));
      Iterator iter = col.iterator();
      while (iter.hasNext()) {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iter.next();
        pm.makePersistent(conceptoFijo);
      }

      this.log.error("GRABO CONCEPTOFIJO");

      this.txn.close();
      pm.close();
    }
    catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error:" + e);
      throw error;
    }

    try
    {
      this.txn.open();
      pm = PMThread.getPM();

      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      sueldoPromedio = this.trabajadorBusiness.findSueldoPromedioById(idSueldoPromedio);
      tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

      organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());
      causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);
      trabajador = this.trabajadorBusiness.findTrabajadorById(sueldoPromedio.getTrabajador().getIdTrabajador());

      this.calcularSueldosPromedioBeanBusiness.calcularUnTrabajadorParaMovimientos(trabajador, numeroMovimiento);
      this.registrosBusiness.borrarConceptosCero(trabajador.getIdTrabajador());

      if (trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) > 0) {
        this.registrosBusiness.fraccionarConceptos(trabajador);
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
        this.log.error("Trabajador Lunes Fraccionados Lunes Primera Quincena " + trabajador.getLunesPrimera());
        this.log.error("Trabajador Lunes Fraccionados Lunes Segunda Quincena " + trabajador.getLunesSegunda());
        this.log.error("FRACCIONO CONCEPTOS");
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) == 0) && (this.registrosBusiness.getDia() == 16)) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) < 0) && (trabajador.getFechaIngreso().getMonth() + 1 == this.registrosBusiness.getMes()) && (trabajador.getFechaIngreso().getYear() + 1900 == this.registrosBusiness.getAnio())) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) < 0) && 
        (trabajador.getFechaIngreso().getMonth() + 1 != this.registrosBusiness.getMes()) && 
        (this.registrosBusiness.getDia() == 16)) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((this.registrosBusiness.getFechaProximaNomina().compareTo(trabajador.getFechaIngreso()) > 0) && (pagarRetroactivo.equals("S")))
      {
        Collection colRetroactivo = new ArrayList();

        colRetroactivo.addAll(this.registrosBusiness.calcularRetroactivos(trabajador));
        Iterator iterRetroactivo = colRetroactivo.iterator();
        while (iterRetroactivo.hasNext()) {
          ConceptoVariable conceptoVariable = (ConceptoVariable)iterRetroactivo.next();
          pm.makePersistent(conceptoVariable);
        }
        this.registrosBusiness.calcularLunesRetroactivo(trabajador);
        this.log.error("CALCULO RETROACTIVOS");

        this.log.error("Trabajador Lunes Retroactivo " + trabajador.getLunesRetroactivo());
      }

      String codConcurso = null;

      if (idConcursoCargo != 0L) {
        concursoCargoBeanBusiness = new ConcursoCargoBeanBusiness();
        concursoCargo = concursoCargoBeanBusiness.findConcursoCargoById(idConcursoCargo);

        codConcurso = concursoCargo.getConcurso().getCodConcurso();
        concursoCargo.setEstatus("3");

        this.log.error("AACTUALIZO ESTATUS DE CONCURSO");
      }

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaIngreso, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, codConcurso, observaciones, usuario.getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaIngreso, null, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones, 0.0D, 0.0D, 0.0D);

      idMovimientoSitp = movimientoSitp.getIdMovimientoSitp();

      pm.makePersistent(movimientoSitp);

      this.log.error("GRABO MOVIMIENTOSITP");

      if (idCausaMovimiento == 1L) {
        MovimientosPlan movimientosPlan = this.movimientosPlanNoGenBeanBusiness.findMovimientosPlanById(idMovimientosPlan);
        movimientosPlan.setCantidadRealizados(movimientosPlan.getCantidadRealizados() + 1);
      }

      this.txn.close();
    } catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), idHistoricoCargos);
      this.log.error("Excepcion controlada:", e);

      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error: " + e);
      throw error;
    }

    return idMovimientoSitp;
  }
}