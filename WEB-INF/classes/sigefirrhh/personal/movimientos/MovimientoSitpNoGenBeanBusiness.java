package sigefirrhh.personal.movimientos;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class MovimientoSitpNoGenBeanBusiness extends MovimientoSitpBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(MovimientoSitpNoGenBeanBusiness.class.getName());

  public Collection findForRemesa(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && remesa == null";

    Query query = pm.newQuery(MovimientoSitp.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colMovimientoSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    this.log.error("size movSitp=" + colMovimientoSitp.size());
    pm.makeTransientAll(colMovimientoSitp);

    return colMovimientoSitp;
  }

  public Collection findByRemesa(long idRemesa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "remesa.idRemesa == pIdRemesa";

    Query query = pm.newQuery(MovimientoSitp.class, filter);

    query.declareParameters("long pIdRemesa");
    HashMap parameters = new HashMap();

    parameters.put("pIdRemesa", new Long(idRemesa));

    Collection colMovimientoSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMovimientoSitp);

    return colMovimientoSitp;
  }

  public Collection findByRemesaAndStatus(long idRemesa, String status)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "remesa.idRemesa == pIdRemesa && estatus == pStatus";

    Query query = pm.newQuery(MovimientoSitp.class, filter);

    query.declareParameters("long pIdRemesa, String pStatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdRemesa", new Long(idRemesa));
    parameters.put("pStatus", status);

    Collection colMovimientoSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMovimientoSitp);

    return colMovimientoSitp;
  }

  public int findLastNumero(long idOrganismo, int anio)
    throws Exception
  {
    ResultSet rsRegistro = null;
    PreparedStatement stRegistro = null;
    Connection connection = Resource.getConnection();
    connection.setAutoCommit(true);
    this.log.error("anio" + anio);
    StringBuffer sql = new StringBuffer();
    sql.append("select max(numero_movimiento) as ultimo from movimientositp ");
    sql.append(" where id_organismo = ? and anio = ?");
    stRegistro = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);

    stRegistro.setLong(1, idOrganismo);
    stRegistro.setInt(2, anio);
    rsRegistro = stRegistro.executeQuery();

    if (rsRegistro.next()) {
      return rsRegistro.getInt("ultimo");
    }
    return 0;
  }
}