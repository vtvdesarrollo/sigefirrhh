package sigefirrhh.personal.movimientos;

import eforserver.common.Resource;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.NumberTools;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenBusiness;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.CategoriaPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoCargo;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesBusinessExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.RelacionPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.GrupoOrganismo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.personal.conceptos.CalcularConceptoBeanBusiness;
import sigefirrhh.personal.expediente.HistorialApn;
import sigefirrhh.personal.expediente.HistorialOrganismo;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.registroCargos.HistoricoCargos;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class RegistrosBusiness
{
  Logger log = Logger.getLogger(RegistrosBusiness.class.getName());

  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();
  private DefinicionesBusinessExtend definicionesExtend = new DefinicionesBusinessExtend();
  private TrabajadorNoGenBusiness trabajadorNoGenBusiness = new TrabajadorNoGenBusiness();
  private CargoNoGenBusiness cargoNoGenBusiness = new CargoNoGenBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private int dia;
  private int mes;
  private int anio;
  private java.util.Date fechaProximaNomina;

  public void validarFechas(java.util.Date fechaVigencia, java.util.Date fechaPuntoCuenta)
  {
    ErrorSistema error = new ErrorSistema();
    boolean existeError = false;

    if (fechaVigencia == null) {
      existeError = true;
      error.setDescription("Debe introducir la fecha de vigencia. ");
    }

    if (fechaPuntoCuenta == null) {
      existeError = true;
      error.setDescription(error.getDescription() + "Debe introducir la fecha de punto de cuenta. ");
    }

    if (!existeError)
    {
      if (fechaPuntoCuenta.compareTo(fechaVigencia) != 0) {
        existeError = true;
        error.setDescription("La fecha de Punto de cuenta debe ser igual a la fecha de vigencia");
      }
    }
    if (existeError)
      throw error;
  }

  public JubilacionAux calcularJubilacion(long idTrabajador, long idTipoPersonal, String periodicidad, java.util.Date fechaVigencia, boolean especial)
    throws Exception
  {
    Calendar calDesde = Calendar.getInstance();
    Calendar calHasta = Calendar.getInstance();
    int anios = 0;
    int meses = 0;
    int dias = 0;
    int aniomes = 0;
    int anioDesde = 0;
    int mesDesde = 0;
    int diaDesde = 0;
    int anioHasta = 0;
    int mesHasta = 0;
    int diaHasta = 0;
    int totalAnios = 0;
    int totalMeses = 0;
    int totalDias = 0;
    String sexo = "F";
    int totalAniost = 0;
    int totalMesest = 0;
    int totalDiast = 0;
    int anioservicios = 0;
    double totalporcentaje = 0.0D;

    boolean cumpleRequerimientos = false;

    Calendar calendar = Calendar.getInstance();

    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    ResultSet rsParametro = null;
    PreparedStatement stParametro = null;

    this.log.error("calcular jubilacion 1");

    StringBuffer sql = new StringBuffer();
    sql.append("select * ");
    sql.append(" from parametrojubilacion ");
    sql.append("  where id_tipo_personal = ? ");

    stParametro = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);
    stParametro.setLong(1, idTipoPersonal);
    rsParametro = stParametro.executeQuery();

    if (!rsParametro.next())
    {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe el registro de parametro jubilación para el tipo de personal");
      throw error;
    }
    this.log.error("calcular jubilacion 2");

    ResultSet rsTrabajador = null;
    PreparedStatement stTrabajador = null;

    sql = new StringBuffer();
    sql.append("select p.sexo, p.fecha_nacimiento, t.fecha_ingreso, ");
    sql.append(" t.fecha_ingreso_apn,p.anios_servicio_apn,p.meses_servicio_apn, p.dias_servicio_apn ");
    sql.append(" from trabajador t, personal p ");
    sql.append("  where t.id_trabajador = ? ");
    sql.append("  and t.id_personal = p.id_personal ");

    this.log.error("calcular jubilacion 3");
    stTrabajador = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);
    stTrabajador.setLong(1, idTrabajador);
    rsTrabajador = stTrabajador.executeQuery();

    if (!rsTrabajador.next())
    {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No existe el registro de trabajador");
      throw error;
    }

    calHasta.setTime(fechaVigencia);
    calDesde.setTime(rsTrabajador.getTime("fecha_ingreso"));

    anioHasta = calHasta.get(1);
    mesHasta = calHasta.get(2);
    diaHasta = calHasta.get(5);

    aniomes = anioHasta * 100 + mesHasta;

    anioDesde = calDesde.get(1);
    mesDesde = calDesde.get(2);
    diaDesde = calDesde.get(5);

    this.log.error("calcular jubilacion 4");

    if (((mesHasta == 2) && (diaHasta >= 28)) || ((mesHasta != 2) && (diaHasta > 30))) {
      diaHasta = 35;
    }
    diaHasta++;
    anios = anioHasta - anioDesde;
    meses = mesHasta - mesDesde;
    dias = diaHasta - diaDesde;

    if (meses < 0) {
      anios--;
      meses = 12 - -meses;
    }
    if (dias < 0) {
      meses--;
      dias = 30 - -dias;
    }
    totalAnios += anios;
    totalMeses += meses;
    totalDias += dias;

    this.log.error("calcular jubilacion 5");
    JubilacionAux jubilacion = new JubilacionAux();

    if (!especial) {
      this.log.error("calcular jubilacion 6");
      if (rsTrabajador.getString("sexo").equalsIgnoreCase("F")) {
        calendar.setTime(fechaVigencia);
        calendar.add(1, -rsParametro.getInt("edad"));
        if (calendar.getTime().compareTo(rsTrabajador.getTime("fecha_nacimiento")) >= 0) {
          calendar.setTime(fechaVigencia);
          calendar.add(1, -rsParametro.getInt("anios_servicio"));
          if (calendar.getTime().compareTo(rsTrabajador.getTime("fecha_ingreso_apn")) >= 0) {
            cumpleRequerimientos = true;
          }
        }
        if (!cumpleRequerimientos) {
          calendar.setTime(fechaVigencia);
          calendar.add(1, -rsParametro.getInt("anios_servicio_sin_edad"));
          if (calendar.getTime().compareTo(rsTrabajador.getTime("fecha_ingreso_apn")) >= 0) {
            cumpleRequerimientos = true;
          }
        }
      }
      if (rsTrabajador.getString("sexo").equalsIgnoreCase("M")) {
        calendar.setTime(fechaVigencia);
        calendar.add(1, -rsParametro.getInt("edadm"));
        if (calendar.getTime().compareTo(rsTrabajador.getTime("fecha_nacimiento")) >= 0) {
          calendar.setTime(fechaVigencia);
          calendar.add(1, -rsParametro.getInt("anios_serviciom"));
          if (calendar.getTime().compareTo(rsTrabajador.getTime("fecha_ingreso_apn")) >= 0) {
            cumpleRequerimientos = true;
          }
        }
        if (!cumpleRequerimientos) {
          calendar.setTime(fechaVigencia);
          calendar.add(1, -rsParametro.getInt("anios_servicio_sin_edad"));
          if (calendar.getTime().compareTo(rsTrabajador.getTime("fecha_ingreso_apn")) >= 0) {
            cumpleRequerimientos = true;
          }
        }

      }

    }
    else
    {
      calendar.setTime(fechaVigencia);
      calendar.add(1, -rsParametro.getInt("anios_servicio_especial"));
      this.log.error("anios_servicio_especial " + rsParametro.getInt("anios_servicio_especial"));
      this.log.error("fecha - anios_serv_espe " + calendar.getTime());
      this.log.error("fecha - ing apn " + rsTrabajador.getTime("fecha_ingreso_apn"));
      if (calendar.getTime().compareTo(rsTrabajador.getTime("fecha_ingreso_apn")) >= 0) {
        cumpleRequerimientos = true;
      }
    }

    this.log.error("calcular jubilacion 8");
    if (cumpleRequerimientos)
    {
      ResultSet rsHistorico = null;
      PreparedStatement stHistorico = null;

      this.log.error("calcular jubilacion 9");
      sql = new StringBuffer();
      sql.append("select sum(monto_asigna) as monto");
      sql.append(" from historicoquincena hq, conceptotipopersonal ctp, concepto c");
      sql.append(" where hq.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.jubilacion ='S' ");
      sql.append(" and ((hq.anio *100 + hq.mes > ?) and (hq.anio *100 + hq.mes <= ?))");
      sql.append(" and hq.id_trabajador = ?");
      stHistorico = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stHistorico.setInt(1, aniomes - rsParametro.getInt("meses_promediar"));
      stHistorico.setInt(2, aniomes);
      stHistorico.setLong(3, idTrabajador);
      rsHistorico = stHistorico.executeQuery();

      rsHistorico.next();
      this.log.error("aniomes :");
      System.out.print(aniomes);

      this.log.error("-------------------------monto " + rsHistorico.getDouble("monto"));
      jubilacion.setSueldoPromedio(rsHistorico.getDouble("monto") / rsParametro.getInt("meses_promediar"));

      jubilacion.setAniosapn(rsTrabajador.getInt("anios_servicio_apn"));
      jubilacion.setMesesapn(rsTrabajador.getInt("meses_servicio_apn"));
      jubilacion.setDiasapn(rsTrabajador.getInt("dias_servicio_apn"));

      jubilacion.setAniosorganismo(totalAnios);
      jubilacion.setMesesorganismo(totalMeses);
      jubilacion.setDiasorganismo(totalDias);

      totalAniost = totalAnios + rsTrabajador.getInt("anios_servicio_apn");
      totalMesest = totalMeses + rsTrabajador.getInt("meses_servicio_apn");
      totalDiast = totalDias + rsTrabajador.getInt("dias_servicio_apn");

      this.log.error("calcular jubilacion 10");

      if (totalMesest > 12) {
        totalAniost++;
        totalMesest -= 12;
      }
      if (totalDiast >= 30) {
        totalMesest++;
        totalDiast -= 30;
      }

      jubilacion.setAniost(totalAniost);
      jubilacion.setMesest(totalMesest);
      jubilacion.setDiast(totalDiast);

      if (!especial)
        totalporcentaje = totalAniost * rsParametro.getDouble("factor");
      else {
        totalporcentaje = rsParametro.getInt("anios_servicio_especial") * rsParametro.getDouble("factor");
      }

      if (totalporcentaje > rsParametro.getDouble("porcentaje")) {
        totalporcentaje = rsParametro.getDouble("porcentaje");
      }
      jubilacion.setPorcentaje(totalporcentaje);
      jubilacion.setMontoJubilacion(NumberTools.twoDecimal(jubilacion.getSueldoPromedio() * totalporcentaje / 100.0D));

      this.log.error("calcular jubilacion 11");

      ResultSet rsDeleteMesesJubilacion = null;

      PreparedStatement stDeleteMesesJubilacion = null;

      sql = new StringBuffer();
      sql.append("delete from mesesjubilacion ");
      sql.append(" where id_trabajador = ?");
      stDeleteMesesJubilacion = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stDeleteMesesJubilacion.setLong(1, idTrabajador);

      boolean ok = false;
      ok = stDeleteMesesJubilacion.execute();

      this.log.error("calcular jubilacion 12");

      ResultSet rsInsertMesesJubilacion = null;
      PreparedStatement stInsertMesesJubilacion = null;

      sql = new StringBuffer();
      sql.append("insert into mesesjubilacion(id_trabajador,anio,mes,base_mensual)  ");
      sql.append("select hq.id_trabajador,hq.anio,hq.mes,sum(monto_asigna) as monto");
      sql.append(" from historicoquincena hq, conceptotipopersonal ctp, concepto c");
      sql.append(" where hq.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.jubilacion ='S' ");
      sql.append(" and ((hq.anio *100 + hq.mes > ?) and  (hq.anio *100 + hq.mes  <= ?))");
      sql.append(" and hq.id_trabajador = ? group by hq.id_trabajador,hq.anio,hq.mes");

      stInsertMesesJubilacion = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stInsertMesesJubilacion.setInt(1, aniomes - rsParametro.getInt("meses_promediar"));
      stInsertMesesJubilacion.setInt(2, aniomes);
      stInsertMesesJubilacion.setLong(3, idTrabajador);

      ok = stInsertMesesJubilacion.execute();

      this.log.error("calcular jubilacion 12");

      return jubilacion;
    }

    ErrorSistema error = new ErrorSistema();
    this.log.error("pasoooooooooooooooo222222");
    error.setDescription("El trabajador no cumple con los requerimientos para ser jubilado");
    this.log.error("pasoooooooooooooooo33333333");
    throw error;
  }

  public Trabajador agregarTrabajador(Personal personal, TipoPersonal tipoPersonal, Organismo organismo, RegistroCargos registroCargos, CausaMovimiento causaMovimiento, java.util.Date fechaIngreso, double sueldoBasico, int paso)
  {
    int diaFechaIngreso = fechaIngreso.getDate();
    int mesFechaIngreso = fechaIngreso.getMonth() + 1;
    int anioFechaIngreso = fechaIngreso.getYear() + 1900;
    java.util.Date fechaentrada = new java.util.Date();
    int diaentrada = fechaentrada.getDate();
    int mesentrada = fechaentrada.getMonth() + 1;
    int anioentrada = fechaentrada.getYear() + 1900;

    Trabajador trabajador = new Trabajador();
    trabajador.setAnioAntiguedad(anioFechaIngreso);
    trabajador.setAnioEgreso(0);
    trabajador.setAnioIngreso(anioFechaIngreso);
    trabajador.setAnioIngresoApn(anioFechaIngreso);
    trabajador.setAnioJubilacion(0);
    trabajador.setAnioPrestaciones(anioFechaIngreso);
    trabajador.setAnioVacaciones(anioFechaIngreso);
    trabajador.setBancoFid(null);
    trabajador.setBancoLph(null);
    trabajador.setBancoNomina(null);
    trabajador.setBaseJubilacion(0.0D);
    trabajador.setCargo(registroCargos.getCargo());
    trabajador.setCargoReal(registroCargos.getCargo());
    trabajador.setCausaMovimiento(causaMovimiento);
    trabajador.setCedula(personal.getCedula());
    try {
      trabajador.setCodCargo(registroCargos.getCargo().getCodCargo());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Registro de Cargo esta asociado a un cargo que no existe");
      throw error;
    }
    trabajador.setCodigoNomina(registroCargos.getCodigoNomina());
    trabajador.setCodigoNominaReal(registroCargos.getCodigoNomina());
    trabajador.setCodTipoPersonal(tipoPersonal.getCodTipoPersonal());
    trabajador.setCotizaFju(tipoPersonal.getCotizaFju());
    trabajador.setCotizaLph(tipoPersonal.getCotizaLph());
    trabajador.setCotizaSpf(tipoPersonal.getCotizaSpf());
    trabajador.setCotizaSso(tipoPersonal.getCotizaSso());
    trabajador.setCuentaFid(null);
    trabajador.setCuentaLph(null);
    trabajador.setCuentaNomina(null);
    trabajador.setDedProxNomina("S");
    trabajador.setDependencia(registroCargos.getDependencia());
    trabajador.setDependenciaReal(registroCargos.getDependencia());
    trabajador.setDiaAntiguedad(diaFechaIngreso);
    trabajador.setDiaEgreso(0);
    trabajador.setDiaIngreso(diaFechaIngreso);
    trabajador.setDiaIngresoApn(diaFechaIngreso);
    trabajador.setDiaJubilacion(0);
    trabajador.setDiaPrestaciones(diaFechaIngreso);
    trabajador.setDiaVacaciones(diaFechaIngreso);
    trabajador.setEstatus("A");
    trabajador.setFechaAntiguedad(fechaIngreso);
    trabajador.setFechaEgreso(null);
    trabajador.setFechaEntradaSig(fechaentrada);
    trabajador.setAnioEntrada(anioentrada);
    trabajador.setMesEntrada(mesentrada);
    trabajador.setDiaEntrada(diaentrada);
    trabajador.setFechaIngreso(fechaIngreso);
    trabajador.setFechaIngresoApn(fechaIngreso);
    trabajador.setFechaJubilacion(null);
    trabajador.setFechaPrestaciones(fechaIngreso);
    trabajador.setFechaSalidaSig(null);
    trabajador.setFechaVacaciones(fechaIngreso);
    trabajador.setFechaUltimoMovimiento(fechaIngreso);
    trabajador.setFechaUltimoMovimiento(fechaIngreso);
    trabajador.setFormaPago(tipoPersonal.getFormaPagoNomina());

    trabajador.setFeVida("S");
    try {
      trabajador.setLugarPago(registroCargos.getSede().getLugarPago());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Registro de Cargo esta asociado a una Sede que no existe");
      throw error;
    }
    try {
      trabajador.setBancoNomina(tipoPersonal.getBancoNomina());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal no tiene asociado Banco para Nómina");
      throw error;
    }
    try {
      trabajador.setBancoLph(tipoPersonal.getBancoLph());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal no tiene asociado Banco Lph");
      throw error;
    }
    try {
      trabajador.setBancoFid(tipoPersonal.getBancoFid());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal no tiene asociado Banco Fideicomiso");
      throw error;
    }

    trabajador.setMesAntiguedad(mesFechaIngreso);
    trabajador.setMesEgreso(0);
    trabajador.setMesIngreso(mesFechaIngreso);
    trabajador.setMesIngresoApn(mesFechaIngreso);
    trabajador.setMesJubilacion(0);
    trabajador.setMesPrestaciones(mesFechaIngreso);
    trabajador.setMesVacaciones(mesFechaIngreso);
    if (organismo.getAprobacionMpd().equals("S"))
      trabajador.setMovimiento("T");
    else {
      trabajador.setMovimiento("A");
    }
    trabajador.setOrganismo(organismo);
    trabajador.setParProxNomina("N");
    trabajador.setPaso(paso);
    trabajador.setPersonal(personal);
    trabajador.setPorcentajeIslr(0.0D);
    trabajador.setPorcentajeJubilacion(0.0D);
    trabajador.setRegistroCargos(registroCargos);
    trabajador.setSituacion("1");
    trabajador.setSueldoBasico(sueldoBasico);
    trabajador.setTipoCtaNomina("N");
    trabajador.setTipoPersonal(tipoPersonal);
    try {
      trabajador.setTurno(registroCargos.getSede().getTurno());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Sede esta asociado a un Turno que no existe");
      throw error;
    }
    trabajador.setRegimen(registroCargos.getSede().getRegimen());
    trabajador.setRiesgo(registroCargos.getSede().getRiesgo());
    trabajador.setIdTrabajador(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.Trabajador"));

    return trabajador;
  }
  public Trabajador agregarTrabajadorSinRegistro(Personal personal, TipoPersonal tipoPersonal, Organismo organismo, Cargo cargo, CausaMovimiento causaMovimiento, java.util.Date fechaIngreso, double sueldoBasico, int codigoNomina, Dependencia dependencia) {
    int diaFechaIngreso = fechaIngreso.getDate();
    int mesFechaIngreso = fechaIngreso.getMonth() + 1;
    int anioFechaIngreso = fechaIngreso.getYear() + 1900;

    Trabajador trabajador = new Trabajador();
    trabajador.setAnioAntiguedad(anioFechaIngreso);
    trabajador.setAnioEgreso(0);
    trabajador.setAnioIngreso(anioFechaIngreso);
    trabajador.setAnioIngresoApn(anioFechaIngreso);
    trabajador.setAnioJubilacion(0);
    trabajador.setAnioPrestaciones(anioFechaIngreso);
    trabajador.setAnioVacaciones(anioFechaIngreso);
    trabajador.setBancoFid(null);
    trabajador.setBancoLph(null);
    trabajador.setBancoNomina(null);
    trabajador.setBaseJubilacion(0.0D);
    trabajador.setCargo(cargo);
    trabajador.setCargoReal(cargo);
    trabajador.setCausaMovimiento(causaMovimiento);
    trabajador.setCedula(personal.getCedula());
    trabajador.setCodCargo(cargo.getCodCargo());
    trabajador.setCodigoNomina(codigoNomina);
    trabajador.setCodigoNominaReal(codigoNomina);
    trabajador.setCodTipoPersonal(tipoPersonal.getCodTipoPersonal());
    trabajador.setCotizaFju(tipoPersonal.getCotizaFju());
    trabajador.setCotizaLph(tipoPersonal.getCotizaLph());
    trabajador.setCotizaSpf(tipoPersonal.getCotizaSpf());
    trabajador.setCotizaSso(tipoPersonal.getCotizaSso());
    trabajador.setCuentaFid(null);
    trabajador.setCuentaLph(null);
    trabajador.setCuentaNomina(null);
    trabajador.setDedProxNomina("S");
    trabajador.setDependencia(dependencia);
    trabajador.setDependenciaReal(dependencia);
    trabajador.setDiaAntiguedad(diaFechaIngreso);
    trabajador.setDiaEgreso(0);
    trabajador.setDiaIngreso(diaFechaIngreso);
    trabajador.setDiaIngresoApn(diaFechaIngreso);
    trabajador.setDiaJubilacion(0);
    trabajador.setDiaPrestaciones(diaFechaIngreso);
    trabajador.setDiaVacaciones(diaFechaIngreso);
    trabajador.setEstatus("A");
    trabajador.setFechaAntiguedad(fechaIngreso);
    trabajador.setFechaEgreso(null);
    trabajador.setFechaEntradaSig(new java.util.Date());
    trabajador.setFechaIngreso(fechaIngreso);
    trabajador.setFechaIngresoApn(fechaIngreso);
    trabajador.setFechaJubilacion(null);
    trabajador.setFechaPrestaciones(fechaIngreso);
    trabajador.setFechaSalidaSig(null);
    trabajador.setFechaVacaciones(fechaIngreso);
    trabajador.setFeVida("S");
    trabajador.setFormaPago(tipoPersonal.getFormaPagoNomina());
    try {
      trabajador.setLugarPago(dependencia.getSede().getLugarPago());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Dependencia esta asociado a una Sede que no existe, o la Sede esta asociada a un Lugar de Pago que no existe");
      throw error;
    }
    try {
      trabajador.setBancoNomina(tipoPersonal.getBancoNomina());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal no tiene asociado Banco para Nómina");
      throw error;
    }
    try {
      trabajador.setBancoLph(tipoPersonal.getBancoLph());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal no tiene asociado Banco Lph");
      throw error;
    }
    try {
      trabajador.setBancoFid(tipoPersonal.getBancoFid());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal no tiene asociado Banco Fideicomiso");
      throw error;
    }

    trabajador.setMesAntiguedad(mesFechaIngreso);
    trabajador.setMesEgreso(0);
    trabajador.setMesIngreso(mesFechaIngreso);
    trabajador.setMesIngresoApn(mesFechaIngreso);
    trabajador.setMesJubilacion(0);
    trabajador.setMesPrestaciones(mesFechaIngreso);
    trabajador.setMesVacaciones(mesFechaIngreso);
    if (organismo.getAprobacionMpd().equals("S"))
      trabajador.setMovimiento("T");
    else {
      trabajador.setMovimiento("A");
    }
    trabajador.setOrganismo(organismo);
    trabajador.setParProxNomina("N");
    trabajador.setPaso(1);
    trabajador.setPersonal(personal);
    trabajador.setPorcentajeIslr(0.0D);
    trabajador.setPorcentajeJubilacion(0.0D);
    trabajador.setRegistroCargos(null);
    trabajador.setSituacion("1");
    trabajador.setSueldoBasico(sueldoBasico);
    trabajador.setTipoCtaNomina("N");
    trabajador.setTipoPersonal(tipoPersonal);
    try {
      trabajador.setTurno(tipoPersonal.getTurno());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Sede esta asociada a un Turno que no existe");
      throw error;
    }
    trabajador.setRiesgo(dependencia.getSede().getRiesgo());
    trabajador.setRegimen(dependencia.getSede().getRegimen());
    trabajador.setIdTrabajador(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.Trabajador"));

    return trabajador;
  }

  public Trayectoria agregarTrayectoria(Trabajador trabajador, SueldoPromedio sueldoPromedio, java.util.Date fechaPreparacion, int numeroMovimiento, java.util.Date fechaVigencia, java.util.Date fechaCulminacion, CausaMovimiento causaMovimiento, double montoJubilacion, double porcJubilacion, double sueldoProm, double montoJubilacionSobrev, double porcPensionSobrev, double montoPensionSobrev, double montoPensionInvalid, double porcPensionInvalid, double invalidezSact, String puntoCuenta, java.util.Date fechaPuntoCuenta, String codConcurso, String observaciones, String usuario, String estatus)
  {
    Trayectoria trayectoria = new Trayectoria();
    trayectoria.setCedula(trabajador.getCedula());
    trayectoria.setPrimerApellido(trabajador.getPersonal().getPrimerApellido());
    trayectoria.setSegundoApellido(trabajador.getPersonal().getSegundoApellido());
    trayectoria.setPrimerNombre(trabajador.getPersonal().getPrimerNombre());
    trayectoria.setSegundoNombre(trabajador.getPersonal().getSegundoNombre());
    trayectoria.setAnioPreparacion(fechaPreparacion.getYear() + 1900);
    trayectoria.setFechaPreparacion(fechaPreparacion);
    trayectoria.setCodigoAnteriorMpd(trabajador.getOrganismo().getCodigoAnteriorMpd());
    trayectoria.setCodOrganismo(trabajador.getOrganismo().getCodOrganismo());
    trayectoria.setNumeroRemesa(null);
    trayectoria.setNumeroMovimiento(numeroMovimiento);
    trayectoria.setCorrelativoMpd(0);
    trayectoria.setNombreCorto(trabajador.getOrganismo().getNombreCorto());
    trayectoria.setNombreOrganismo(trabajador.getOrganismo().getNombreOrganismo());
    trayectoria.setEstatus(estatus);
    trayectoria.setFechaEstatus(fechaPreparacion);
    trayectoria.setFechaVigencia(fechaVigencia);
    trayectoria.setFechaCulminacion(fechaCulminacion);
    try {
      trayectoria.setCodUbiGeografico(trabajador.getDependencia().getSede().getCiudad().getCodCiudad() + trabajador.getDependencia().getSede().getCiudad().getEstado().getCodEstado());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Dependencia esta asociado a una Sede que no existe, o la Sede esta asociada a una Ciudad que no existe");
      throw error;
    }

    trayectoria.setEstado(trabajador.getDependencia().getSede().getCiudad().getEstado().getNombre());

    trayectoria.setCiudad(trabajador.getDependencia().getSede().getCiudad().getNombre());
    trayectoria.setMunicipio(null);
    try {
      trayectoria.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Sede esta asociada a una Región que no existe");
      throw error;
    }
    trayectoria.setNombreRegion(trabajador.getDependencia().getSede().getRegion().getNombre());
    trayectoria.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    trayectoria.setNombreDependencia(trabajador.getDependencia().getNombre());
    trayectoria.setCodCausaMovimiento(causaMovimiento.getCodCausaMovimiento());
    trayectoria.setDescripcionMovimiento(causaMovimiento.getDescripcion());

    trayectoria.setCodGrupoOrganismo(trabajador.getTipoPersonal().getGrupoOrganismo().getCodGrupoOrganismo());
    trayectoria.setNombreCortoGrupo(trabajador.getTipoPersonal().getGrupoOrganismo().getNombreCorto());
    trayectoria.setNombreLargoGrupo(trabajador.getTipoPersonal().getGrupoOrganismo().getNombre());
    trayectoria.setCaucion(trabajador.getCargo().getCaucion());
    try {
      trayectoria.setCodManualCargo(String.valueOf(trabajador.getCargo().getManualCargo().getCodManualCargo()));
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Cargo esta asociado a un Manual Cargo que no existe");
      throw error;
    }
    trayectoria.setDescripcionCargo(trabajador.getCargo().getManualCargo().getNombre());
    trayectoria.setCodCargo(trabajador.getCargo().getCodCargo());
    trayectoria.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    try {
      trayectoria.setCodRelacion(trabajador.getTipoPersonal().getClasificacionPersonal().getRelacionPersonal().getCodRelacion());
      trayectoria.setDescRelacion(trabajador.getTipoPersonal().getClasificacionPersonal().getRelacionPersonal().getDescRelacion());
      trayectoria.setCodCategoria(trabajador.getTipoPersonal().getClasificacionPersonal().getCategoriaPersonal().getCodCategoria());
      trayectoria.setDescCategoria(trabajador.getTipoPersonal().getClasificacionPersonal().getCategoriaPersonal().getDescCategoria());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal esta asociado a una Clasificación de Personal que no existe, o la Clasificación de Personal esta asociada a una (Relación Personal o CategoriaPersonal) que no existe");
      throw error;
    }
    trayectoria.setGrado(trabajador.getCargo().getGrado());
    trayectoria.setPaso(trabajador.getPaso());
    trayectoria.setCodigoNomina(trabajador.getCodigoNomina());
    trayectoria.setSueldoBasico(sueldoPromedio.getPromedioSueldo());
    trayectoria.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    trayectoria.setPrimaJerarquia(0.0D);
    trayectoria.setPrimaServicio(0.0D);
    trayectoria.setAjusteSueldo(0.0D);
    trayectoria.setOtrosPagos(0.0D);
    trayectoria.setOtrosNoVicepladin(0.0D);
    trayectoria.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    trayectoria.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    trayectoria.setPuntoCuenta(puntoCuenta);
    trayectoria.setFechaPuntoCuenta(fechaPuntoCuenta);
    trayectoria.setCodConcurso(codConcurso);
    trayectoria.setObservaciones(observaciones);
    trayectoria.setUsuario(usuario);
    trayectoria.setOrigen("S");
    trayectoria.setPersonal(trabajador.getPersonal());
    trayectoria.setHoras(0.0D);
    trayectoria.setNombramiento(null);

    trayectoria.setPorcJubilacion(porcJubilacion);
    trayectoria.setMontoJubilacion(montoJubilacion);
    trayectoria.setIdTrayectoria(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Trayectoria"));

    return trayectoria;
  }

  public HistorialOrganismo agregarHistorialOrganismo(Trabajador trabajador, Organismo organismo, CausaMovimiento causaMovimiento, SueldoPromedio sueldoPromedio, int numeroMovimiento, Remesa remesa, java.util.Date fecha, String estatus, String origenMovimiento, Usuario usuario, java.util.Date fechaPuntoCuenta, String puntoCuenta, String codConcurso)
  {
    HistorialOrganismo historialOrganismo = new HistorialOrganismo();
    historialOrganismo.setAfectaSueldo("N");
    historialOrganismo.setApellidosNombres(trabajador.getPersonal().getPrimerApellido() + " " + trabajador.getPersonal().getPrimerNombre());
    if (organismo.getAprobacionMpd().equals("S"))
      historialOrganismo.setAprobacionMpd("S");
    else {
      historialOrganismo.setAprobacionMpd("N");
    }
    historialOrganismo.setCausaMovimiento(causaMovimiento);
    historialOrganismo.setCedula(trabajador.getCedula());
    try {
      historialOrganismo.setClasificacionPersonal(trabajador.getTipoPersonal().getClasificacionPersonal());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal esta asociado a una Clasificación de Personal que no existe");
      throw error;
    }
    historialOrganismo.setCodOrganismo(organismo.getCodOrganismo());
    historialOrganismo.setCodCargo(trabajador.getCargo().getCodCargo());
    historialOrganismo.setCodCausaMovimiento(causaMovimiento.getCodCausaMovimiento());

    historialOrganismo.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    historialOrganismo.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    historialOrganismo.setCodigoNomina(trabajador.getCodigoNomina());
    try {
      historialOrganismo.setCodManualCargo(trabajador.getCargo().getManualCargo().getCodManualCargo());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Cargo esta asociado a un Manual Cargo que no existe");
      throw error;
    }
    try {
      historialOrganismo.setCodSede(trabajador.getDependencia().getSede().getCodSede());
      historialOrganismo.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Dependencia esta asociada a una Sede que no existe, o la Sede esta asociada a una Región que no existe");
      throw error;
    }
    try {
      if (trabajador.getCargo().getManualCargo().getTabulador() != null)
        historialOrganismo.setCodTabulador(trabajador.getCargo().getManualCargo().getTabulador().getCodTabulador());
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Manual Cargo esta asociado a un Tabulador que no existe");
      throw error;
    }

    historialOrganismo.setNombreRegion(trabajador.getDependencia().getSede().getNombre());
    historialOrganismo.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    historialOrganismo.setDocumentoSoporte(null);
    historialOrganismo.setEstatus(estatus);
    historialOrganismo.setGrado(trabajador.getCargo().getGrado());
    historialOrganismo.setNombreOrganismo(organismo.getNombreOrganismo());
    historialOrganismo.setNombreDependencia(trabajador.getDependencia().getNombre());
    historialOrganismo.setNombreSede(trabajador.getDependencia().getSede().getNombre());
    historialOrganismo.setNumeroMovimiento(numeroMovimiento);
    historialOrganismo.setOrganismo(organismo);
    historialOrganismo.setPaso(trabajador.getPaso());
    historialOrganismo.setPersonal(trabajador.getPersonal());
    historialOrganismo.setRemesa(null);
    historialOrganismo.setSueldo(trabajador.getSueldoBasico());

    historialOrganismo.setTipoPersonal(trabajador.getCargo().getTipoCargo());

    historialOrganismo.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    historialOrganismo.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    historialOrganismo.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    historialOrganismo.setFechaMovimiento(fecha);
    historialOrganismo.setFechaRegistro(new java.util.Date());
    historialOrganismo.setOrigenMovimiento(origenMovimiento);
    historialOrganismo.setUsuario(usuario.getUsuario());
    historialOrganismo.setFechaPuntoCuenta(fechaPuntoCuenta);
    historialOrganismo.setPuntoCuenta(puntoCuenta);
    historialOrganismo.setCodConcurso(codConcurso);
    historialOrganismo.setIdHistorialOrganismo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.HistorialOrganismo"));

    return historialOrganismo;
  }

  public HistorialApn agregarHistorialApn(Trabajador trabajador, Organismo organismo, CausaMovimiento causaMovimiento, SueldoPromedio sueldoPromedio, int numeroMovimiento, String remesa, java.util.Date fechaIngreso, String estatus, String origenMovimiento, String usuario, java.util.Date fechaPuntoCuenta, String puntoCuenta, String codConcurso)
  {
    HistorialApn historialApn = new HistorialApn();
    historialApn.setAfectaSueldo("N");
    historialApn.setApellidosNombres(trabajador.getPersonal().getPrimerApellido() + " " + trabajador.getPersonal().getPrimerNombre());
    if (organismo.getAprobacionMpd().equals("S"))
      historialApn.setAprobacionMpd("S");
    else {
      historialApn.setAprobacionMpd("N");
    }
    historialApn.setCausaMovimiento(causaMovimiento);
    historialApn.setCedula(trabajador.getCedula());

    historialApn.setClasificacionPersonal(trabajador.getTipoPersonal().getClasificacionPersonal());
    historialApn.setCodOrganismo(organismo.getCodOrganismo());
    historialApn.setCodCargo(trabajador.getCargo().getCodCargo());
    historialApn.setCodCausaMovimiento(causaMovimiento.getCodCausaMovimiento());
    historialApn.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    historialApn.setCodigoNomina(trabajador.getCodigoNomina());
    historialApn.setCodManualCargo(trabajador.getCargo().getManualCargo().getCodManualCargo());
    historialApn.setCodSede(trabajador.getDependencia().getSede().getCodSede());
    historialApn.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    if (trabajador.getCargo().getManualCargo().getTabulador() != null) {
      historialApn.setCodTabulador(trabajador.getCargo().getManualCargo().getTabulador().getCodTabulador());
    }
    historialApn.setNombreRegion(trabajador.getDependencia().getSede().getNombre());
    historialApn.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    historialApn.setDocumentoSoporte(null);
    historialApn.setEstatus(estatus);
    historialApn.setGrado(trabajador.getCargo().getGrado());
    historialApn.setNombreOrganismo(organismo.getNombreOrganismo());
    historialApn.setNombreDependencia(trabajador.getDependencia().getNombre());
    historialApn.setNombreSede(trabajador.getDependencia().getSede().getNombre());
    historialApn.setNumeroMovimiento(numeroMovimiento);
    historialApn.setOrganismo(organismo);
    historialApn.setPaso(trabajador.getPaso());
    historialApn.setPersonal(trabajador.getPersonal());
    historialApn.setRemesa(remesa);
    historialApn.setSueldo(trabajador.getSueldoBasico());
    historialApn.setTipoPersonal(trabajador.getCargo().getTipoCargo());
    historialApn.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    historialApn.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    historialApn.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    historialApn.setFechaMovimiento(fechaIngreso);
    historialApn.setFechaRegistro(new java.util.Date());
    historialApn.setOrigenMovimiento(origenMovimiento);
    historialApn.setUsuario(usuario);
    historialApn.setFechaPuntoCuenta(fechaPuntoCuenta);
    historialApn.setPuntoCuenta(puntoCuenta);
    historialApn.setCodConcurso(codConcurso);
    historialApn.setIdHistorialApn(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.HistorialApn"));

    return historialApn;
  }

  public long agregarhistoricoCargos(PersistenceManager pm, Registro registro, CausaMovimiento causaMovimiento, Cargo cargo, Dependencia dependencia, int codigoNomina, String situacion, String movimiento, java.util.Date fechaMovimiento, int cedula, String primerApellido, String segundoApellido, String primerNombre, String segundoNombre, double horas)
  {
    HistoricoCargos historicoCargos = new HistoricoCargos();
    historicoCargos.setRegistro(registro);
    historicoCargos.setCausaMovimiento(causaMovimiento);
    historicoCargos.setCargo(cargo);
    historicoCargos.setDependencia(dependencia);
    historicoCargos.setCodigoNomina(codigoNomina);
    historicoCargos.setSituacion(situacion);
    historicoCargos.setMovimiento(movimiento);
    historicoCargos.setFechaMovimiento(fechaMovimiento);
    historicoCargos.setCedula(cedula);
    historicoCargos.setPrimerApellido(primerApellido);
    historicoCargos.setSegundoApellido(segundoApellido);
    historicoCargos.setPrimerNombre(primerNombre);
    historicoCargos.setSegundoNombre(segundoNombre);
    historicoCargos.setHoras(horas);

    historicoCargos.setIdHistoricoCargos(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.registroCargos.HistoricoCargos"));
    pm.makePersistent(historicoCargos);

    return historicoCargos.getIdHistoricoCargos();
  }

  public MovimientoSitp agregarMovimientoSitp(Trabajador trabajador, Organismo organismo, CausaMovimiento causaMovimiento, SueldoPromedio sueldoPromedio, int numeroMovimiento, Remesa remesa, java.util.Date fechaIngreso, java.util.Date fechaCulminacion, String estatus, String origenMovimiento, Usuario usuario, java.util.Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, int anteriorCodManualCargo, String anteriorCodCargo, String anteriorDescripcionCargo, int anteriorCodigoNomina, String anteriorCodSede, String anteriorNombreSede, String anteriorCodDependencia, String anteriorNombreDependencia, double anteriorSueldo, double anteriorCompensacion, double anteriorPrimasCargo, double anteriorPrimasTrabajador, int anteriorGrado, int anteriorPaso, String anteriorCodRegion, String anteriorNombreRegion, String observaciones, double montoJubilacion, double sueldoPromedioJubilacion, double porcentaje)
  {
    MovimientoSitp movimientoSitp = new MovimientoSitp();
    movimientoSitp.setFechaIngreso(trabajador.getFechaIngreso());
    movimientoSitp.setFechaCulminacion(fechaCulminacion);
    movimientoSitp.setTurno(trabajador.getTurno());
    movimientoSitp.setAfectaSueldo("N");
    movimientoSitp.setApellidosNombres(trabajador.getPersonal().getPrimerApellido() + " " + trabajador.getPersonal().getPrimerNombre());
    movimientoSitp.setCausaMovimiento(causaMovimiento);
    movimientoSitp.setCedula(trabajador.getCedula());
    try {
      movimientoSitp.setClasificacionPersonal(trabajador.getTipoPersonal().getClasificacionPersonal());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal esta asociado a una Clasificación de Personal que no existe");
      throw error;
    }
    movimientoSitp.setCodOrganismo(organismo.getCodOrganismo());
    movimientoSitp.setCodCargo(trabajador.getCargo().getCodCargo());
    movimientoSitp.setCodCausaMovimiento(causaMovimiento.getCodCausaMovimiento());
    movimientoSitp.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    try {
      movimientoSitp.setCodSede(trabajador.getDependencia().getSede().getCodSede());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Dependencia esta asociada a una Sede que no existe");
      throw error;
    }

    movimientoSitp.setCodigoNomina(trabajador.getCodigoNomina());
    try {
      movimientoSitp.setCodManualCargo(trabajador.getCargo().getManualCargo().getCodManualCargo());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Cargo esta asociado a un Manual Cargo que no existe ");
      throw error;
    }
    try {
      movimientoSitp.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Sede esta asociada a una Región que no existe");
      throw error;
    }

    if (trabajador.getCargo().getManualCargo().getTabulador() != null) {
      try {
        movimientoSitp.setCodTabulador(trabajador.getCargo().getManualCargo().getTabulador().getCodTabulador());
      } catch (Exception e) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("El Manual Cargo esta asociado a un Tabulador que no existe");
        throw error;
      }
    }
    movimientoSitp.setNombreRegion(trabajador.getDependencia().getSede().getNombre());
    movimientoSitp.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    movimientoSitp.setDocumentoSoporte(null);
    movimientoSitp.setEstatus(estatus);
    movimientoSitp.setGrado(trabajador.getCargo().getGrado());
    movimientoSitp.setNombreOrganismo(organismo.getNombreOrganismo());
    movimientoSitp.setNombreDependencia(trabajador.getDependencia().getNombre());
    movimientoSitp.setNombreSede(trabajador.getDependencia().getSede().getNombre());
    movimientoSitp.setNumeroMovimiento(numeroMovimiento);
    movimientoSitp.setOrganismo(organismo);
    movimientoSitp.setPaso(trabajador.getPaso());
    movimientoSitp.setPersonal(trabajador.getPersonal());
    movimientoSitp.setRemesa(remesa);
    movimientoSitp.setSueldo(trabajador.getSueldoBasico());
    movimientoSitp.setTipoPersonal(trabajador.getCargo().getTipoCargo());
    movimientoSitp.setNombreTipoPersonal(trabajador.getCargo().getTipoCargo());
    movimientoSitp.setCodOrganismoMpd(organismo.getCodigoAnteriorMpd());
    movimientoSitp.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    movimientoSitp.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    movimientoSitp.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    movimientoSitp.setFechaMovimiento(fechaIngreso);
    movimientoSitp.setFechaRegistro(new java.util.Date());
    movimientoSitp.setUsuario(usuario);
    movimientoSitp.setFechaPuntoCuenta(fechaPuntoCuenta);
    movimientoSitp.setPuntoCuenta(puntoCuenta);
    movimientoSitp.setCodConcurso(codConcurso);
    movimientoSitp.setAnio(new java.util.Date().getYear() + 1900);
    movimientoSitp.setAnteriorCodCargo(anteriorCodCargo);
    movimientoSitp.setAnteriorCodDependencia(anteriorCodDependencia);
    movimientoSitp.setAnteriorCodigoNomina(anteriorCodigoNomina);
    movimientoSitp.setAnteriorCodManualCargo(anteriorCodManualCargo);
    movimientoSitp.setAnteriorCodRegion(anteriorCodRegion);
    movimientoSitp.setAnteriorCodSede(anteriorCodSede);
    movimientoSitp.setAnteriorCompensacion(anteriorCompensacion);
    movimientoSitp.setAnteriorDescripcionCargo(anteriorDescripcionCargo);
    movimientoSitp.setAnteriorGrado(anteriorGrado);
    movimientoSitp.setAnteriorNombreDependencia(anteriorNombreDependencia);
    movimientoSitp.setAnteriorNombreRegion(anteriorNombreRegion);
    movimientoSitp.setAnteriorNombreSede(anteriorNombreSede);
    movimientoSitp.setAnteriorPaso(anteriorPaso);
    movimientoSitp.setAnteriorPrimasCargo(anteriorPrimasCargo);
    movimientoSitp.setAnteriorPrimasTrabajador(anteriorPrimasTrabajador);
    movimientoSitp.setAnteriorSueldo(anteriorSueldo);
    movimientoSitp.setObservaciones(observaciones);
    movimientoSitp.setMontoJubilacion(montoJubilacion);
    movimientoSitp.setSueldoPromedio(sueldoPromedioJubilacion);
    movimientoSitp.setPorcJubilacion(porcentaje);
    movimientoSitp.setIdMovimientoSitp(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.movimientos.MovimientoSitp"));

    this.log.error("va salir de agregar_movimientositp");
    return movimientoSitp;
  }

  public RegistroSitp agregarRegistroSitp(Trabajador trabajador, Organismo organismo, CausaMovimiento causaMovimiento, SueldoPromedio sueldoPromedio, int numeroMovimiento, Remesa remesa, java.util.Date fechaIngreso, String estatus, String origenMovimiento, Usuario usuario, java.util.Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, int anteriorCodManualCargo, String anteriorCodCargo, String anteriorDescripcionCargo, int anteriorCodigoNomina, String anteriorCodSede, String anteriorNombreSede, String anteriorCodDependencia, String anteriorNombreDependencia, double anteriorSueldo, double anteriorCompensacion, double anteriorPrimasCargo, double anteriorPrimasTrabajador, int anteriorGrado, int anteriorPaso, String anteriorCodRegion, String anteriorNombreRegion, String observaciones)
  {
    RegistroSitp registroSitp = new RegistroSitp();
    registroSitp.setFechaIngreso(trabajador.getFechaIngreso());
    registroSitp.setTurno(trabajador.getTurno());
    registroSitp.setAfectaSueldo("N");
    registroSitp.setApellidosNombres(trabajador.getPersonal().getPrimerApellido() + " " + trabajador.getPersonal().getPrimerNombre());
    registroSitp.setCausaMovimiento(causaMovimiento);
    registroSitp.setCedula(trabajador.getCedula());
    try {
      registroSitp.setClasificacionPersonal(trabajador.getTipoPersonal().getClasificacionPersonal());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Tipo de Personal esta asociado a una Clasificación de Personal que no existe");
      throw error;
    }
    registroSitp.setCodOrganismo(organismo.getCodOrganismo());
    registroSitp.setCodCargo(trabajador.getCargo().getCodCargo());
    registroSitp.setCodCausaMovimiento(causaMovimiento.getCodCausaMovimiento());
    registroSitp.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    registroSitp.setCodigoNomina(trabajador.getCodigoNomina());
    try {
      registroSitp.setCodManualCargo(trabajador.getCargo().getManualCargo().getCodManualCargo());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Cargo esta asociado a un Manual Cargo que no existe ");
      throw error;
    }
    try {
      registroSitp.setCodSede(trabajador.getDependencia().getSede().getCodSede());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Dependencia esta asociada a una Sede que no existe");
      throw error;
    }
    try {
      registroSitp.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("La Sede esta asociada a una Región que no existe");
      throw error;
    }
    try {
      if (trabajador.getCargo().getManualCargo().getTabulador() != null)
        registroSitp.setCodTabulador(trabajador.getCargo().getManualCargo().getTabulador().getCodTabulador());
    }
    catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("El Manual Cargo esta asociado a un Tabulador que no existe");
      throw error;
    }
    registroSitp.setNombreRegion(trabajador.getDependencia().getSede().getNombre());
    registroSitp.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    registroSitp.setDocumentoSoporte(null);
    registroSitp.setEstatus(estatus);
    registroSitp.setGrado(trabajador.getCargo().getGrado());
    registroSitp.setNombreOrganismo(organismo.getNombreOrganismo());
    registroSitp.setNombreDependencia(trabajador.getDependencia().getNombre());
    registroSitp.setNombreSede(trabajador.getDependencia().getSede().getNombre());
    registroSitp.setNumeroMovimiento(numeroMovimiento);
    registroSitp.setOrganismo(organismo);
    registroSitp.setPaso(trabajador.getPaso());
    registroSitp.setPersonal(trabajador.getPersonal());
    registroSitp.setRemesa(remesa);
    registroSitp.setSueldo(trabajador.getSueldoBasico());
    registroSitp.setTipoPersonal(trabajador.getCargo().getTipoCargo());
    registroSitp.setNombreTipoPersonal(trabajador.getCargo().getTipoCargo());
    registroSitp.setCodOrganismoMpd(organismo.getCodigoAnteriorMpd());
    registroSitp.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    registroSitp.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    registroSitp.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    registroSitp.setFechaMovimiento(fechaIngreso);
    registroSitp.setFechaRegistro(new java.util.Date());
    registroSitp.setUsuario(usuario);
    registroSitp.setFechaPuntoCuenta(fechaPuntoCuenta);
    registroSitp.setPuntoCuenta(puntoCuenta);
    registroSitp.setCodConcurso(codConcurso);
    registroSitp.setAnio(new java.util.Date().getYear() + 1900);
    registroSitp.setAnteriorCodCargo(anteriorCodCargo);
    registroSitp.setAnteriorCodDependencia(anteriorCodDependencia);
    registroSitp.setAnteriorCodigoNomina(anteriorCodigoNomina);
    registroSitp.setAnteriorCodManualCargo(anteriorCodManualCargo);
    registroSitp.setAnteriorCodRegion(anteriorCodRegion);
    registroSitp.setAnteriorCodSede(anteriorCodSede);
    registroSitp.setAnteriorCompensacion(anteriorCompensacion);
    registroSitp.setAnteriorDescripcionCargo(anteriorDescripcionCargo);
    registroSitp.setAnteriorGrado(anteriorGrado);
    registroSitp.setAnteriorNombreDependencia(anteriorNombreDependencia);
    registroSitp.setAnteriorNombreRegion(anteriorNombreRegion);
    registroSitp.setAnteriorNombreSede(anteriorNombreSede);
    registroSitp.setAnteriorPaso(anteriorPaso);
    registroSitp.setAnteriorPrimasCargo(anteriorPrimasCargo);
    registroSitp.setAnteriorPrimasTrabajador(anteriorPrimasTrabajador);
    registroSitp.setAnteriorSueldo(anteriorSueldo);
    registroSitp.setObservaciones(observaciones);
    registroSitp.setIdRegistroSitp(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.movimientos.RegistroSitp"));

    this.log.error("va salir de agregar_movimientositp");
    return registroSitp;
  }

  public void calcularNuevoCargoEnConceptoProyectado(long idTrabajador, double sueldo, long idRegistroCargos, String aumento, double porcentaje, int paso, long idCargo)
  {
    double compensacionAnterior = 0.0D;
    double sueldoAnterior = 0.0D;
    double totalAnterior = 0.0D;
    long idConceptoFijoSueldo = 0L;
    double sueldoBasicoTrabajador = 0.0D;
    try {
      Connection connection = Resource.getConnection();

      ResultSet rsConceptoCargo = null;
      PreparedStatement stConceptoCargo = null;

      ResultSet rsDetalleTabulador = null;
      PreparedStatement stDetalleTabulador = null;

      ResultSet rsConceptoFijo = null;
      PreparedStatement stConceptoFijo = null;

      ResultSet rsRegistroCargos = null;
      PreparedStatement stRegistroCargos = null;

      ResultSet rsConceptos = null;
      PreparedStatement stConceptos = null;

      ResultSet rsConceptoTipoPersonal = null;
      PreparedStatement stConceptoTipoPersonal = null;

      Statement stActualizar = connection.createStatement();
      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

      StringBuffer sql = new StringBuffer();

      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql = new StringBuffer();
      sql.append("delete from conceptoproyectado where id_trabajador = " + idTrabajador);
      stActualizar.execute(sql.toString());

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_tipo_personal, ");
      sql.append("  cf.id_frecuencia_tipo_personal, cf.monto");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, concepto c");
      sql.append(" where cf.id_trabajador = ?");
      sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.cod_concepto < '5000'");
      sql.append(" and c.primas_cargo <> 'S'");

      stConceptoFijo = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoFijo.setLong(1, idTrabajador);

      rsConceptoFijo = stConceptoFijo.executeQuery();

      while (rsConceptoFijo.next())
      {
        sql = new StringBuffer();
        sql.append("insert into conceptoproyectado (id_trabajador, id_concepto_tipo_personal,");
        sql.append(" id_frecuencia_tipo_personal, monto) values( " + idTrabajador + ",");
        sql.append(rsConceptoFijo.getLong("id_concepto_tipo_personal") + ",");
        sql.append(rsConceptoFijo.getLong("id_frecuencia_tipo_personal") + ",");
        sql.append(rsConceptoFijo.getDouble("monto") + ")");

        stActualizar.addBatch(sql.toString());
        this.log.error("id_concepto_tipo_personal" + rsConceptoFijo.getLong("id_concepto_tipo_personal"));
        this.log.error("monto" + rsConceptoFijo.getDouble("monto"));
      }
      stActualizar.executeBatch();

      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal, fp.cod_frecuencia_pago, ctp.id_frecuencia_tipo_personal");
      sql.append(" from concepto c, conceptotipopersonal ctp, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp");
      sql.append(" where c.cod_concepto = ?");
      sql.append(" and c.id_concepto = ctp.id_concepto");
      sql.append(" and ctp.id_tipo_personal = ?");
      sql.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");

      stConceptoTipoPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select dt.monto");
      sql.append("  ");
      sql.append(" from detalletabulador dt");
      sql.append(" ");
      sql.append(" where dt.id_tabulador = ?");
      sql.append(" and dt.grado = ?");
      sql.append(" and dt.sub_grado = ?");
      sql.append(" and dt.monto >= ?");

      stDetalleTabulador = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select t.tipo_tabulador, c.grado, c.sub_grado,  ");
      sql.append("  t.id_tabulador");
      sql.append(" from registrocargos rc, manualcargo mc, ");
      sql.append(" tabulador t, cargo c");
      sql.append(" where rc.id_registro_cargos = ?");
      sql.append(" and rc.id_cargo = c.id_cargo");
      sql.append(" and c.id_manual_cargo = mc.id_manual_cargo");
      sql.append(" and mc.id_tabulador = t.id_tabulador");
      stRegistroCargos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistroCargos.setLong(1, idRegistroCargos);
      rsRegistroCargos = stRegistroCargos.executeQuery();
      rsRegistroCargos.next();

      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal, ftp.id_frecuencia_tipo_personal  ");
      sql.append(" from conceptocargo cc, conceptotipopersonal ctp, ");
      sql.append(" frecuenciatipopersonal ftp");
      sql.append(" where cc.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and cc.id_cargo = ?");
      sql.append(" and cc.excluir = 'N'");
      sql.append(" and cc.automatico_ingreso = 'S'");

      stConceptoCargo = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoCargo.setLong(1, idRegistroCargos);
      rsConceptoCargo = stConceptoCargo.executeQuery();
      rsConceptoCargo.next();

      while (rsConceptoCargo.next())
      {
        sql = new StringBuffer();
        sql.append("insert into conceptoproyectado (id_trabajador, id_concepto_tipo_personal,");
        sql.append(" id_frecuencia_tipo_personal, monto) values( " + idTrabajador + ",");
        sql.append(rsConceptoCargo.getLong("id_concepto_tipo_personal") + ",");
        sql.append(rsConceptoCargo.getLong("id_frecuencia_tipo_personal") + ",");
        sql.append(rsConceptoCargo.getDouble("monto") + ")");

        stActualizar.addBatch(sql.toString());
      }

      stActualizar.executeBatch();

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.monto, c.cod_concepto,  fp.cod_frecuencia_pago, ");
      sql.append(" tu.jornada, c.ajuste_sueldo, t.sueldo_basico, t.id_tipo_personal, ctp.id_concepto_tipo_personal ");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, concepto c, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp,");
      sql.append(" trabajador t, turno tu");
      sql.append(" where cf.id_trabajador = ?");
      sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and cf.id_trabajador = t.id_trabajador");
      sql.append(" and t.id_turno = tu.id_turno");
      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptos.setLong(1, idTrabajador);
      rsConceptos = stConceptos.executeQuery();

      sueldoBasicoTrabajador = sueldo;

      while (rsConceptos.next())
      {
        if ((rsConceptos.getString("cod_concepto").equals("0001")) || 
          (rsConceptos.getString("cod_concepto").equals("0002")) || 
          (rsConceptos.getString("cod_concepto").equals("0003")) || 
          (rsConceptos.getString("cod_concepto").equals("0004")) || 
          (rsConceptos.getString("cod_concepto").equals("0013")))
        {
          sueldoAnterior = rsConceptos.getDouble("monto");
          idConceptoFijoSueldo = rsConceptos.getLong("id_concepto_fijo");

          if ((rsConceptos.getString("jornada").equals("C")) && 
            (!rsConceptos.getString("cod_concepto").equals("0002"))) {
            if (rsConceptos.getInt("cod_frecuencia_pago") == 3) {
              sueldoAnterior *= 2.0D;
              sueldo /= 2.0D;
            } else if (rsConceptos.getInt("cod_frecuencia_pago") == 4) {
              sueldo /= 7.0D;
              sueldoAnterior /= 7.0D;
            }

          }

          sql = new StringBuffer();
          sql.append("update conceptoproyectado set monto = " + sueldo);
          sql.append(" where id_trabajador = " + idTrabajador + " and id_concepto_tipo_personal = ");
          sql.append(rsConceptos.getLong("id_concepto_tipo_personal"));

          stActualizar.execute(sql.toString());
        }
        else if (rsConceptos.getString("cod_concepto").equals("0020")) {
          compensacionAnterior = rsConceptos.getDouble("monto");
          if (rsConceptos.getInt("cod_frecuencia_pago") == 3) {
            compensacionAnterior *= 2.0D;
          }

          sql = new StringBuffer();
        }
        else if ((rsConceptos.getString("ajuste_sueldo").equals("S")) || 
          (rsConceptos.getString("sueldo_basico").equals("S")))
        {
          if (rsConceptos.getInt("cod_frecuencia_pago") == 3) {
            totalAnterior += rsConceptos.getDouble("monto") * 2.0D;
          }
          else if (rsConceptos.getInt("cod_frecuencia_pago") == 4)
            totalAnterior += rsConceptos.getDouble("monto") / 7.0D;
          else {
            totalAnterior += rsConceptos.getDouble("monto");
          }

        }

      }

      double nuevaCompensacion = 0.0D;

      if ((aumento.equals("PO")) || (aumento.equals("NA"))) {
        if (aumento.equals("NA")) {
          porcentaje = 0.0D;
        }
        this.log.error("sueldoanterior" + sueldoAnterior);
        this.log.error("compensacionAnterior" + compensacionAnterior);
        double totalAumentar = (totalAnterior + sueldoAnterior + compensacionAnterior) * ((100.0D + porcentaje) / 100.0D);

        this.log.error("totalaumentar " + totalAumentar);
        if (totalAumentar > sueldoBasicoTrabajador)
          try {
            sql = new StringBuffer();
            sql.append("select dt.monto");
            sql.append("  ");
            sql.append(" from detalletabulador dt");
            sql.append(" ");
            sql.append(" where dt.id_tabulador = ?");
            sql.append(" and dt.grado = ?");
            sql.append(" and dt.sub_grado = ?");
            sql.append(" and dt.monto >= ? order by dt.monto limit 1");

            stDetalleTabulador = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);
            this.log.error("id_tabulador" + rsRegistroCargos.getLong("id_tabulador"));
            this.log.error("grado" + rsRegistroCargos.getLong("grado"));
            this.log.error("sub_grado" + rsRegistroCargos.getLong("sub_grado"));
            this.log.error("monto" + totalAumentar);

            stDetalleTabulador.setLong(1, rsRegistroCargos.getLong("id_tabulador"));
            stDetalleTabulador.setInt(2, rsRegistroCargos.getInt("grado"));
            stDetalleTabulador.setInt(3, rsRegistroCargos.getInt("sub_grado"));
            stDetalleTabulador.setDouble(4, totalAumentar);
            rsDetalleTabulador = stDetalleTabulador.executeQuery();
            if (rsDetalleTabulador.next()) {
              nuevaCompensacion = rsDetalleTabulador.getDouble("monto") - sueldoBasicoTrabajador;
            } else {
              sql = new StringBuffer();
              sql.append("select dt.monto, dt.paso");
              sql.append("  ");
              sql.append(" from detalletabulador dt");
              sql.append(" ");
              sql.append(" where dt.id_tabulador = ?");
              sql.append(" and dt.grado = ?");
              sql.append(" and dt.sub_grado = ?");
              sql.append(" order by paso desc limit 1");

              stDetalleTabulador = connection.prepareStatement(
                sql.toString(), 
                1003, 
                1007);
              stDetalleTabulador.setLong(1, rsRegistroCargos.getLong("id_tabulador"));
              stDetalleTabulador.setInt(2, rsRegistroCargos.getInt("grado"));
              stDetalleTabulador.setInt(3, rsRegistroCargos.getInt("sub_grado"));
              rsDetalleTabulador = stDetalleTabulador.executeQuery();

              if (rsDetalleTabulador.next()) {
                this.log.error("No consiguio el tabulador por el monto");
                nuevaCompensacion = totalAumentar - sueldoBasicoTrabajador;
                paso = rsDetalleTabulador.getInt("paso");
              }

            }

          }
          catch (Exception e)
          {
            this.log.error("Excepcion controlada:", e);
          }
      }
      else
      {
        try {
          sql = new StringBuffer();
          sql.append("select dt.monto, dt.paso");
          sql.append("  ");
          sql.append(" from detalletabulador dt");
          sql.append(" ");
          sql.append(" where dt.id_tabulador = ?");
          sql.append(" and dt.grado = ?");
          sql.append(" and dt.sub_grado = ?");
          sql.append(" and dt.paso = ?");

          stDetalleTabulador = connection.prepareStatement(
            sql.toString(), 
            1003, 
            1007);

          stDetalleTabulador.setLong(1, rsRegistroCargos.getLong("id_tabulador"));
          stDetalleTabulador.setInt(2, rsRegistroCargos.getInt("grado"));
          stDetalleTabulador.setInt(3, rsRegistroCargos.getInt("sub_grado"));
          stDetalleTabulador.setInt(4, paso);
          rsDetalleTabulador = stDetalleTabulador.executeQuery();

          if (rsDetalleTabulador.next()) {
            this.log.error("No consiguio el tabulador por el monto");
            nuevaCompensacion = rsDetalleTabulador.getDouble("monto") - sueldoBasicoTrabajador;
            paso = rsDetalleTabulador.getInt("paso");
          }
        }
        catch (Exception e)
        {
          this.log.error("Excepcion controlada:", e);
        }
      }
      stConceptoTipoPersonal.setString(1, "0020");
      stConceptoTipoPersonal.setLong(2, rsConceptos.getInt("id_tipo_personal"));

      rsConceptoTipoPersonal = stConceptoTipoPersonal.executeQuery();
      if (rsConceptoTipoPersonal.next()) {
        if (rsConceptoTipoPersonal.getInt("cod_frecuencia_pago") == 3) {
          nuevaCompensacion /= 2.0D;
        }
        if (compensacionAnterior == 0.0D) {
          this.log.error("compensacionNueva " + nuevaCompensacion);
          sql = new StringBuffer();
          sql.append("insert into conceptoproyectado (id_trabajador, id_concepto_tipo_personal,");
          sql.append(" id_frecuencia_tipo_personal, monto) values( " + idTrabajador + ",");
          sql.append(rsConceptoTipoPersonal.getLong("id_concepto_tipo_personal") + ",");
          sql.append(rsConceptoTipoPersonal.getLong("id_frecuencia_tipo_personal") + ",");
          sql.append(nuevaCompensacion + ")");

          stActualizar.execute(sql.toString());
        } else {
          sql = new StringBuffer();
          sql.append("update conceptoproyectado set monto = " + nuevaCompensacion);
          sql.append(" where id_trabajador = " + idTrabajador + " and id_concepto_tipo_personal = ");
          sql.append(rsConceptoTipoPersonal.getLong("id_concepto_tipo_personal"));

          stActualizar.execute(sql.toString());
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarConceptosFijosNuevoCargo(PersistenceManager pm, Trabajador trabajador, double sueldoBasico, RegistroCargos registroCargos, String aumento, double porcentaje, int paso) {
    try { double compensacionAnterior = 0.0D;
      double sueldoAnterior = 0.0D;
      double totalAnterior = 0.0D;

      Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoByTrabajador(trabajador.getIdTrabajador());
      Iterator iterConceptoFijo = colConceptoFijo.iterator();
      while (iterConceptoFijo.hasNext())
      {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iterConceptoFijo.next();

        this.log.error("codigo concepto " + conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto());

        if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getPrimasCargo().equals("S")) {
          conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
          pm.deletePersistent(conceptoFijo);
        } else {
          conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
          conceptoFijo.setMontoAnterior(conceptoFijo.getMonto());
          if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0001")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0003")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0004")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0002")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0013")))
          {
            sueldoAnterior = conceptoFijo.getMonto();
            conceptoFijo.setMonto(sueldoBasico);
            if (trabajador.getTurno().getJornada().equals("C")) {
              if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0001")) || 
                (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0003")) || 
                (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0004")) || 
                (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0013"))) {
                if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
                  conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 2.0D));
                  sueldoAnterior *= 2.0D;
                } else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
                  conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() * 7.0D));
                  sueldoAnterior /= 7.0D;
                }
                this.log.error("sueldobasico en cf " + conceptoFijo.getMonto());
              }
            }
            else if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0002"))
            {
              if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
                conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 2.0D / (8.0D / registroCargos.getHoras())));
              else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
                conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() * 7.0D / (8.0D / registroCargos.getHoras())));
              }
            }
          }
          else if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0020")) {
            if ((trabajador.getCausaMovimiento().getIdCausaMovimiento() == 6L) || (
              (registroCargos.getCargo().getGrado() == 99) && ((registroCargos.getCargo().getTipoCargo().equals("1")) || (registroCargos.getCargo().getTipoCargo().equals("3"))))) {
              conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
              pm.deletePersistent(conceptoFijo);
            } else {
              compensacionAnterior = conceptoFijo.getMonto();
              if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
                compensacionAnterior *= 2.0D;
            }
          }
          else if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getAjusteSueldo().equals("S")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getSueldoBasico().equals("S"))) {
            this.log.error("registrocargos is :" + registroCargos);
            this.log.error("trabajador is :" + trabajador);
            this.log.error("casuamovimiento is :" + trabajador.getCausaMovimiento());
            if ((trabajador.getCausaMovimiento().getIdCausaMovimiento() == 6L) || (
              (trabajador.getCausaMovimiento().getIdCausaMovimiento() == 5L) && (registroCargos.getCargo().getTipoCargo().equals("5")))) {
              conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
              pm.deletePersistent(conceptoFijo);
            }
            else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
              totalAnterior += conceptoFijo.getMonto() * 2.0D;
            }
            else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
              totalAnterior += conceptoFijo.getMonto() / 7.0D;
            } else {
              totalAnterior += conceptoFijo.getMonto();
            }

          }

        }

      }

      Collection colConceptoTipoPersonal = this.definicionesBusiness.findConceptoCargoByCargo(trabajador.getCargo().getIdCargo(), "N", "S");
      Iterator iterConceptoTipoPersonal = colConceptoTipoPersonal.iterator();
      this.log.error("busca concepto por cargo");
      while (iterConceptoTipoPersonal.hasNext()) {
        ConceptoTipoPersonal conceptoTipoPersonal = (ConceptoTipoPersonal)iterConceptoTipoPersonal.next();

        this.log.error(conceptoTipoPersonal.getConcepto().getDescripcion());

        this.log.error(conceptoTipoPersonal.getConcepto().getDescripcion());
        ConceptoFijo conceptoFijo = new ConceptoFijo();
        conceptoFijo.setTrabajador(trabajador);
        conceptoFijo.setDocumentoSoporte(null);
        conceptoFijo.setEstatus("A");
        conceptoFijo.setFechaComienzo(new java.util.Date());
        conceptoFijo.setFechaRegistro(new java.util.Date());
        conceptoFijo.setFechaEliminar(null);
        conceptoFijo.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
        conceptoFijo.setConceptoTipoPersonal(conceptoTipoPersonal);
        conceptoFijo.setUnidades(conceptoTipoPersonal.getUnidades());
        conceptoFijo.setMontoRestituir(0.0D);
        conceptoFijo.setUnidadesRestituir(0.0D);
        conceptoFijo.setRestituir("N");
        conceptoFijo.setMonto(0.0D);
        conceptoFijo.setMontoAnterior(0.0D);
        conceptoFijo.setIdConceptoFijo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
        pm.makePersistent(conceptoFijo);
      }

      double nuevaCompensacion = 0.0D;
      this.log.error("antes de compensacion tipo cargo = " + registroCargos.getCargo().getTipoCargo());
      DetalleTabulador detalleTabulador = new DetalleTabulador();
      if ((registroCargos.getCargo().getGrado() != 99) && ((registroCargos.getCargo().getTipoCargo().equals("1")) || (registroCargos.getCargo().getTipoCargo().equals("2"))))
      {
        if ((aumento.equals("PO")) || (aumento.equals("NA"))) {
          if (aumento.equals("NA")) {
            porcentaje = 0.0D;
          }
          this.log.error("sueldoanterior" + sueldoAnterior);
          this.log.error("compensacionAnterior" + compensacionAnterior);
          double totalAumentar = (totalAnterior + sueldoAnterior + compensacionAnterior) * ((100.0D + porcentaje) / 100.0D);

          this.log.error("totalaumentar " + totalAumentar);
          this.log.error("sueldoBasico " + sueldoBasico);
          if (totalAumentar > sueldoBasico)
            try {
              detalleTabulador = this.cargoNoGenBusiness.findDetalleTabuladorByMonto(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado(), totalAumentar);
              nuevaCompensacion = detalleTabulador.getMonto() - sueldoBasico;
              this.log.error("nuevaCompensacion " + nuevaCompensacion);
            } catch (Exception e) {
              this.log.error("No consiguio el tabulador por el monto 1");
              nuevaCompensacion = totalAumentar - sueldoBasico;
              paso = this.cargoNoGenBusiness.findDetalleTabuladorMaximoPaso(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado()).getPaso();
            }
        }
        else
        {
          try {
            this.log.error("idTabulador " + registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador());
            this.log.error("grado " + registroCargos.getCargo().getGrado());
            this.log.error("subGrado " + registroCargos.getCargo().getSubGrado());
            this.log.error("paso " + paso);

            detalleTabulador = this.cargoNoGenBusiness.findDetalleTabuladorForRegistroCargos(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado(), paso);
          } catch (Exception e) {
            this.log.error("Excepcion controlada:", e);
            ErrorSistema error = new ErrorSistema();
            error.setDescription("No se consigue el tabulador");
            throw error;
          }
          nuevaCompensacion = detalleTabulador.getMonto() - sueldoBasico;
        }

        if (trabajador.getCausaMovimiento().getIdCausaMovimiento() == 8L) nuevaCompensacion = 0.0D;

        ConceptoTipoPersonal conceptoTipoPersonal = (ConceptoTipoPersonal)this.definicionesBusiness.findConceptoTipoPersonalByCodConceptoAndIdTipoPersonal("0020", trabajador.getTipoPersonal().getIdTipoPersonal()).iterator().next();
        if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
          nuevaCompensacion /= 2.0D;
        }

        if (compensacionAnterior == 0.0D) {
          this.log.error("Se esta grabando compensacion");
          ConceptoFijo conceptoFijo = new ConceptoFijo();
          conceptoFijo.setTrabajador(trabajador);
          conceptoFijo.setDocumentoSoporte(null);
          conceptoFijo.setEstatus("A");
          conceptoFijo.setFechaComienzo(new java.util.Date());
          conceptoFijo.setFechaRegistro(new java.util.Date());
          conceptoFijo.setFechaEliminar(null);
          conceptoFijo.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
          conceptoFijo.setConceptoTipoPersonal(conceptoTipoPersonal);
          conceptoFijo.setUnidades(conceptoTipoPersonal.getUnidades());
          conceptoFijo.setMontoRestituir(0.0D);
          conceptoFijo.setUnidadesRestituir(0.0D);
          conceptoFijo.setRestituir("N");
          conceptoFijo.setMonto(nuevaCompensacion);
          conceptoFijo.setIdConceptoFijo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
          pm.makePersistent(conceptoFijo);
        } else {
          ConceptoFijo conceptoFijo = (ConceptoFijo)this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorConceptoTipoPersonalFrecuenciaTipoPersonal(trabajador.getIdTrabajador(), conceptoTipoPersonal.getIdConceptoTipoPersonal(), conceptoTipoPersonal.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()).iterator().next();
          conceptoFijo.setMonto(nuevaCompensacion);
        }

        borrarConceptosCero(trabajador.getIdTrabajador());
      }

      if (trabajador.getCausaMovimiento().getIdCausaMovimiento() == 6L) {
        paso = 1;
      }
      if (paso != 0)
        trabajador.setPaso(paso);
      else {
        trabajador.setPaso(detalleTabulador.getPaso());
      }

      if (trabajador.getCausaMovimiento().getIdCausaMovimiento() == 6L) {
        paso = 1;
        trabajador.setPaso(paso);
      }
      if ((trabajador.getCausaMovimiento().getIdCausaMovimiento() == 5L) && (trabajador.getCargo().getGrado() == 99)) {
        paso = 1;
        trabajador.setPaso(paso);
      }
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarConceptosFijosNuevoCargoNoLefp(PersistenceManager pm, Trabajador trabajador, double sueldoBasico, RegistroCargos registroCargos, String aumento, double porcentaje, int paso) {
    try {
      double compensacionAnterior = 0.0D;
      double sueldoAnterior = 0.0D;
      double totalAnterior = 0.0D;

      Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoByTrabajador(trabajador.getIdTrabajador());
      Iterator iterConceptoFijo = colConceptoFijo.iterator();
      while (iterConceptoFijo.hasNext())
      {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iterConceptoFijo.next();
        if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getPrimasCargo().equals("S")) {
          conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
          pm.deletePersistent(conceptoFijo);
        } else {
          conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
          conceptoFijo.setMontoAnterior(conceptoFijo.getMonto());
          if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0001")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0003")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0004")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0002")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0013")))
          {
            sueldoAnterior = conceptoFijo.getMonto();
            conceptoFijo.setMonto(sueldoBasico);
            if (trabajador.getTurno().getJornada().equals("C")) {
              if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0001")) || 
                (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0003")) || 
                (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0004")) || 
                (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0013"))) {
                if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
                  conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 2.0D));
                  sueldoAnterior *= 2.0D;
                } else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
                  conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() * 7.0D));
                  sueldoAnterior /= 7.0D;
                }
                this.log.error("sueldobasico en cf " + conceptoFijo.getMonto());
              }
            }
            else if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0002"))
            {
              if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
                conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 2.0D / (8.0D / registroCargos.getHoras())));
              else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
                conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() * 7.0D / (8.0D / registroCargos.getHoras())));
              }
            }
          }
          else if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getAjusteSueldo().equals("S")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getSueldoBasico().equals("S"))) {
            if ((trabajador.getCausaMovimiento().getIdCausaMovimiento() == 6L) || (
              (trabajador.getCausaMovimiento().getIdCausaMovimiento() == 5L) && (registroCargos.getCargo().getTipoCargo().equals("5")))) {
              conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
              pm.deletePersistent(conceptoFijo);
            }
            else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
              totalAnterior += conceptoFijo.getMonto() * 2.0D;
            }
            else if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
              totalAnterior += conceptoFijo.getMonto() / 7.0D;
            } else {
              totalAnterior += conceptoFijo.getMonto();
            }

          }

        }

      }

      Collection colConceptoTipoPersonal = this.definicionesBusiness.findConceptoCargoByCargo(trabajador.getCargo().getIdCargo(), "N", "S");
      Iterator iterConceptoTipoPersonal = colConceptoTipoPersonal.iterator();
      while (iterConceptoTipoPersonal.hasNext()) {
        ConceptoTipoPersonal conceptoTipoPersonal = (ConceptoTipoPersonal)iterConceptoTipoPersonal.next();
        this.log.error(conceptoTipoPersonal.getConcepto().getDescripcion());

        this.log.error(conceptoTipoPersonal.getConcepto().getDescripcion());
        ConceptoFijo conceptoFijo = new ConceptoFijo();
        conceptoFijo.setTrabajador(trabajador);
        conceptoFijo.setDocumentoSoporte(null);
        conceptoFijo.setEstatus("A");
        conceptoFijo.setFechaComienzo(new java.util.Date());
        conceptoFijo.setFechaRegistro(new java.util.Date());
        conceptoFijo.setFechaEliminar(null);
        conceptoFijo.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
        conceptoFijo.setConceptoTipoPersonal(conceptoTipoPersonal);
        conceptoFijo.setUnidades(conceptoTipoPersonal.getUnidades());
        conceptoFijo.setMontoRestituir(0.0D);
        conceptoFijo.setUnidadesRestituir(0.0D);
        conceptoFijo.setRestituir("N");
        conceptoFijo.setMonto(0.0D);
        conceptoFijo.setMontoAnterior(0.0D);
        conceptoFijo.setIdConceptoFijo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
        pm.makePersistent(conceptoFijo);
      }

      double nuevaCompensacion = 0.0D;

      if (trabajador.getCausaMovimiento().getIdCausaMovimiento() == 6L)
        paso = 1;
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public void calcularAumentoSueldoEnConceptoProyectado(long idTrabajador, long idRegistroCargos, String aumento, double porcentaje, int paso) {
    double compensacionAnterior = 0.0D;
    double sueldoAnterior = 0.0D;
    double totalAnterior = 0.0D;
    long idConceptoFijoSueldo = 0L;
    double sueldoBasicoTrabajador = 0.0D;
    try {
      Connection connection = Resource.getConnection();

      ResultSet rsDetalleTabulador = null;
      PreparedStatement stDetalleTabulador = null;

      ResultSet rsConceptoFijo = null;
      PreparedStatement stConceptoFijo = null;

      ResultSet rsRegistroCargos = null;
      PreparedStatement stRegistroCargos = null;

      ResultSet rsConceptos = null;
      PreparedStatement stConceptos = null;

      ResultSet rsConceptoTipoPersonal = null;
      PreparedStatement stConceptoTipoPersonal = null;

      Statement stActualizar = connection.createStatement();
      CalcularConceptoBeanBusiness calcularConceptoBeanBusiness = new CalcularConceptoBeanBusiness();

      StringBuffer sql = new StringBuffer();

      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql = new StringBuffer();
      sql.append("delete from conceptoproyectado where id_trabajador = " + idTrabajador);
      stActualizar.execute(sql.toString());

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_tipo_personal, ");
      sql.append("  cf.id_frecuencia_tipo_personal, cf.monto");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, concepto c");
      sql.append(" where cf.id_trabajador = ?");
      sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and c.cod_concepto < '5000'");

      stConceptoFijo = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stConceptoFijo.setLong(1, idTrabajador);

      rsConceptoFijo = stConceptoFijo.executeQuery();

      while (rsConceptoFijo.next())
      {
        sql = new StringBuffer();
        sql.append("insert into conceptoproyectado (id_trabajador, id_concepto_tipo_personal,");
        sql.append(" id_frecuencia_tipo_personal, monto) values( " + idTrabajador + ",");
        sql.append(rsConceptoFijo.getLong("id_concepto_tipo_personal") + ",");
        sql.append(rsConceptoFijo.getLong("id_frecuencia_tipo_personal") + ",");
        sql.append(rsConceptoFijo.getDouble("monto") + ")");

        stActualizar.addBatch(sql.toString());
      }

      stActualizar.executeBatch();

      sql = new StringBuffer();
      sql.append("select ctp.id_concepto_tipo_personal, fp.cod_frecuencia_pago, ftp.id_frecuencia_tipo_personal");
      sql.append(" from concepto c, conceptotipopersonal ctp, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp");
      sql.append(" where c.cod_concepto = ?");
      sql.append(" and c.id_concepto = ctp.id_concepto");
      sql.append(" and ctp.id_tipo_personal = ?");
      sql.append(" and ctp.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");

      stConceptoTipoPersonal = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select dt.monto");
      sql.append("  ");
      sql.append(" from detalletabulador dt");
      sql.append(" ");
      sql.append(" where dt.id_tabulador = ?");
      sql.append(" and dt.grado = ?");
      sql.append(" and dt.sub_grado = ?");
      sql.append(" and dt.monto >= ? order by dt.monto");

      stDetalleTabulador = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      sql = new StringBuffer();
      sql.append("select t.tipo_tabulador, c.grado, c.sub_grado,  ");
      sql.append("  t.id_tabulador");
      sql.append(" from registrocargos rc, manualcargo mc, ");
      sql.append(" tabulador t, cargo c");
      sql.append(" where rc.id_registro_cargos = ?");
      sql.append(" and rc.id_cargo = c.id_cargo");
      sql.append(" and c.id_manual_cargo = mc.id_manual_cargo");
      sql.append(" and mc.id_tabulador = t.id_tabulador");
      stRegistroCargos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistroCargos.setLong(1, idRegistroCargos);
      rsRegistroCargos = stRegistroCargos.executeQuery();
      rsRegistroCargos.next();

      sql = new StringBuffer();
      sql.append("select cf.id_concepto_fijo, cf.monto, c.cod_concepto,  fp.cod_frecuencia_pago, ");
      sql.append(" tu.jornada, c.ajuste_sueldo, t.sueldo_basico, t.id_tipo_personal ");
      sql.append(" from conceptofijo cf, conceptotipopersonal ctp, concepto c, ");
      sql.append(" frecuenciatipopersonal ftp, frecuenciapago fp,");
      sql.append(" trabajador t, turno tu");
      sql.append(" where cf.id_trabajador = ?");
      sql.append(" and cf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal");
      sql.append(" and ctp.id_concepto = c.id_concepto");
      sql.append(" and cf.id_frecuencia_tipo_personal = ftp.id_frecuencia_tipo_personal");
      sql.append(" and ftp.id_frecuencia_pago = fp.id_frecuencia_pago");
      sql.append(" and cf.id_trabajador = t.id_trabajador");
      sql.append(" and t.id_turno = tu.id_turno");
      stConceptos = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stConceptos.setLong(1, idTrabajador);
      rsConceptos = stConceptos.executeQuery();

      while (rsConceptos.next()) {
        sueldoBasicoTrabajador = rsConceptos.getDouble("sueldo_basico");

        if ((rsConceptos.getString("cod_concepto").equals("0001")) || 
          (rsConceptos.getString("cod_concepto").equals("0002")) || 
          (rsConceptos.getString("cod_concepto").equals("0003")) || 
          (rsConceptos.getString("cod_concepto").equals("0004")) || 
          (rsConceptos.getString("cod_concepto").equals("0013"))) {
          sueldoAnterior = rsConceptos.getDouble("monto");
          idConceptoFijoSueldo = rsConceptos.getLong("id_concepto_fijo");

          if ((rsConceptos.getString("jornada").equals("C")) && 
            (!rsConceptos.getString("cod_concepto").equals("0002"))) {
            if (rsConceptos.getInt("cod_frecuencia_pago") == 3)
              sueldoAnterior *= 2.0D;
            else if (rsConceptos.getInt("cod_frecuencia_pago") == 4)
            {
              sueldoAnterior /= 7.0D;
            }
          }

        }
        else if (rsConceptos.getString("cod_concepto").equals("0020")) {
          compensacionAnterior = rsConceptos.getDouble("monto");
          if (rsConceptos.getInt("cod_frecuencia_pago") == 3)
            compensacionAnterior *= 2.0D;
        }
        else if ((rsConceptos.getString("ajuste_sueldo").equals("S")) || 
          (rsConceptos.getString("sueldo_basico").equals("S")))
        {
          if (rsConceptos.getInt("cod_frecuencia_pago") == 3) {
            totalAnterior += rsConceptos.getDouble("monto") * 2.0D;
          }
          else if (rsConceptos.getInt("cod_frecuencia_pago") == 4)
            totalAnterior += rsConceptos.getDouble("monto") / 7.0D;
          else {
            totalAnterior += rsConceptos.getDouble("monto");
          }

        }

      }

      double nuevaCompensacion = 0.0D;
      if (!aumento.equals("NA")) {
        if (aumento.equals("PO")) {
          this.log.error("sueldoanterior" + sueldoAnterior);
          this.log.error("compensacionAnterior" + compensacionAnterior);
          double totalAumentar = (totalAnterior + sueldoAnterior + compensacionAnterior) * ((100.0D + porcentaje) / 100.0D);

          this.log.error("totalaumentar " + totalAumentar);
          if (totalAumentar > sueldoBasicoTrabajador)
            try {
              sql = new StringBuffer();
              sql.append("select dt.monto");
              sql.append("  ");
              sql.append(" from detalletabulador dt");
              sql.append(" ");
              sql.append(" where dt.id_tabulador = ?");
              sql.append(" and dt.grado = ?");
              sql.append(" and dt.sub_grado = ?");
              sql.append(" and dt.monto >= ? order by dt.monto limit 1");

              stDetalleTabulador = connection.prepareStatement(
                sql.toString(), 
                1003, 
                1007);

              stDetalleTabulador.setLong(1, rsRegistroCargos.getLong("id_tabulador"));
              stDetalleTabulador.setInt(2, rsRegistroCargos.getInt("grado"));
              stDetalleTabulador.setInt(3, rsRegistroCargos.getInt("sub_grado"));
              stDetalleTabulador.setDouble(4, totalAumentar);
              rsDetalleTabulador = stDetalleTabulador.executeQuery();
              if (rsDetalleTabulador.next()) {
                nuevaCompensacion = rsDetalleTabulador.getDouble("monto") - sueldoBasicoTrabajador;
              } else {
                sql = new StringBuffer();
                sql.append("select dt.monto, dt.paso");
                sql.append("  ");
                sql.append(" from detalletabulador dt");
                sql.append(" ");
                sql.append(" where dt.id_tabulador = ?");
                sql.append(" and dt.grado = ?");
                sql.append(" and dt.sub_grado = ?");
                sql.append(" order by paso desc limit 1");

                stDetalleTabulador = connection.prepareStatement(
                  sql.toString(), 
                  1003, 
                  1007);
                stDetalleTabulador.setLong(1, rsRegistroCargos.getLong("id_tabulador"));
                stDetalleTabulador.setInt(2, rsRegistroCargos.getInt("grado"));
                stDetalleTabulador.setInt(3, rsRegistroCargos.getInt("sub_grado"));
                rsDetalleTabulador = stDetalleTabulador.executeQuery();

                if (rsDetalleTabulador.next()) {
                  this.log.error("No consiguio el tabulador por el monto");
                  nuevaCompensacion = totalAumentar - sueldoBasicoTrabajador;
                  paso = rsDetalleTabulador.getInt("paso");
                }

              }

            }
            catch (Exception e)
            {
              this.log.error("Excepcion controlada:", e);
            }
        }
        else
        {
          try {
            sql = new StringBuffer();
            sql.append("select dt.monto, dt.paso");
            sql.append("  ");
            sql.append(" from detalletabulador dt");
            sql.append(" ");
            sql.append(" where dt.id_tabulador = ?");
            sql.append(" and dt.grado = ?");
            sql.append(" and dt.sub_grado = ?");
            sql.append(" and dt.paso = ?");

            stDetalleTabulador = connection.prepareStatement(
              sql.toString(), 
              1003, 
              1007);

            stDetalleTabulador.setLong(1, rsRegistroCargos.getLong("id_tabulador"));
            stDetalleTabulador.setInt(2, rsRegistroCargos.getInt("grado"));
            stDetalleTabulador.setInt(3, rsRegistroCargos.getInt("sub_grado"));
            stDetalleTabulador.setInt(4, paso);
            rsDetalleTabulador = stDetalleTabulador.executeQuery();

            if (rsDetalleTabulador.next()) {
              this.log.error("No consiguio el tabulador por el monto 2");
              nuevaCompensacion = rsDetalleTabulador.getDouble("monto") - sueldoBasicoTrabajador;
              paso = rsDetalleTabulador.getInt("paso");
            }
          }
          catch (Exception e)
          {
            this.log.error("Excepcion controlada:", e);
          }
        }
        stConceptoTipoPersonal.setString(1, "0020");
        stConceptoTipoPersonal.setLong(2, rsConceptos.getInt("id_tipo_personal"));

        rsConceptoTipoPersonal = stConceptoTipoPersonal.executeQuery();

        if (rsConceptoTipoPersonal.next()) {
          if (rsConceptoTipoPersonal.getInt("cod_frecuencia_pago") == 3) {
            nuevaCompensacion /= 2.0D;
          }
          if (compensacionAnterior == 0.0D) {
            this.log.error("CompensaNuevacion: " + nuevaCompensacion);
            sql = new StringBuffer();
            sql.append("insert into conceptoproyectado (id_trabajador, id_concepto_tipo_personal,");
            sql.append(" id_frecuencia_tipo_personal, monto) values( " + idTrabajador + ",");
            sql.append(rsConceptoTipoPersonal.getLong("id_concepto_tipo_personal") + ",");
            this.log.error("ID Concepto TP: " + rsConceptoTipoPersonal.getLong("id_concepto_tipo_personal"));
            sql.append(rsConceptoTipoPersonal.getLong("id_frecuencia_tipo_personal") + ",");
            this.log.error("ID Frecuencia TP: " + rsConceptoTipoPersonal.getLong("id_frecuencia_tipo_personal"));
            sql.append(nuevaCompensacion + ")");

            stActualizar.execute(sql.toString());
          } else {
            sql = new StringBuffer();
            sql.append("update conceptoproyectado set monto = " + nuevaCompensacion);
            sql.append(" where id_trabajador = " + idTrabajador + " and id_concepto_tipo_personal = ");
            sql.append(rsConceptoTipoPersonal.getLong("id_concepto_tipo_personal"));

            stActualizar.execute(sql.toString());
          }
        }
      }
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarConceptosFijosAumentoSueldo(PersistenceManager pm, Trabajador trabajador, RegistroCargos registroCargos, String aumento, double porcentaje, int paso) {
    try { double compensacionAnterior = 0.0D;
      double sueldoAnterior = 0.0D;
      double totalAnterior = 0.0D;
      long idConceptoFijoSueldo = 0L;

      Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoByTrabajador(trabajador.getIdTrabajador());
      Iterator iterConceptoFijo = colConceptoFijo.iterator();
      while (iterConceptoFijo.hasNext())
      {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iterConceptoFijo.next();

        conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
        conceptoFijo.setMontoAnterior(conceptoFijo.getMonto());
        if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0001")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0003")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0004")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0002")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0013")))
        {
          sueldoAnterior = conceptoFijo.getMonto();
          idConceptoFijoSueldo = conceptoFijo.getIdConceptoFijo();

          if ((trabajador.getTurno().getJornada().equals("C")) && (
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0001")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0003")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0004")) || 
            (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0013")))) {
            if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
            {
              sueldoAnterior *= 2.0D;
            } else if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4)
            {
              sueldoAnterior /= 7.0D;
            }
            this.log.error("sueldobasico en cf " + conceptoFijo.getMonto());
          }
        }
        else if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0020"))
        {
          compensacionAnterior = conceptoFijo.getMonto();
          if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
            compensacionAnterior *= 2.0D;
        }
        else if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getAjusteSueldo().equals("S")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getSueldoBasico().equals("S")))
        {
          if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
            totalAnterior += conceptoFijo.getMonto() * 2.0D;
          }
          else if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4)
            totalAnterior += conceptoFijo.getMonto() / 7.0D;
          else {
            totalAnterior += conceptoFijo.getMonto();
          }

        }

      }

      double nuevaCompensacion = 0.0D;
      if (!aumento.equals("NA")) {
        DetalleTabulador detalleTabulador = new DetalleTabulador();
        if (registroCargos.getCargo().getManualCargo().getTabulador().getTipoTabulador().equals("1"))
        {
          if (aumento.equals("PO")) {
            this.log.error("sueldoanterior" + sueldoAnterior);
            this.log.error("compensacionAnterior" + compensacionAnterior);
            double totalAumentar = (totalAnterior + sueldoAnterior + compensacionAnterior) * ((100.0D + porcentaje) / 100.0D);

            this.log.error("totalaumentar " + totalAumentar);
            if (totalAumentar > trabajador.getSueldoBasico())
              try {
                detalleTabulador = this.cargoNoGenBusiness.findDetalleTabuladorByMonto(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado(), totalAumentar);
                nuevaCompensacion = detalleTabulador.getMonto() - trabajador.getSueldoBasico();
              } catch (Exception e) {
                this.log.error("No consiguio el tabulador por el monto 3");
                nuevaCompensacion = totalAumentar - trabajador.getSueldoBasico();
                paso = this.cargoNoGenBusiness.findDetalleTabuladorMaximoPaso(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado()).getPaso();
              }
          }
          else
          {
            try {
              this.log.error("idTabulador " + registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador());
              this.log.error("grado " + registroCargos.getCargo().getGrado());
              this.log.error("subGrado " + registroCargos.getCargo().getSubGrado());
              this.log.error("paso " + paso);

              detalleTabulador = this.cargoNoGenBusiness.findDetalleTabuladorForRegistroCargos(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado(), paso);
            } catch (Exception e) {
              this.log.error("Excepcion controlada:", e);
              ErrorSistema error = new ErrorSistema();
              error.setDescription("No se consigue el tabulador");
              throw error;
            }

            nuevaCompensacion = detalleTabulador.getMonto() - trabajador.getSueldoBasico();
          }
          ConceptoTipoPersonal conceptoTipoPersonal = (ConceptoTipoPersonal)this.definicionesBusiness.findConceptoTipoPersonalByCodConceptoAndIdTipoPersonal("0020", trabajador.getTipoPersonal().getIdTipoPersonal()).iterator().next();
          if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
            nuevaCompensacion /= 2.0D;
          }
          if (compensacionAnterior == 0.0D) {
            this.log.error("Se esta grabando compensacion");
            ConceptoFijo conceptoFijo = new ConceptoFijo();
            conceptoFijo.setTrabajador(trabajador);
            conceptoFijo.setDocumentoSoporte(null);
            conceptoFijo.setEstatus("A");
            conceptoFijo.setFechaComienzo(new java.util.Date());
            conceptoFijo.setFechaRegistro(new java.util.Date());
            conceptoFijo.setFechaEliminar(null);
            conceptoFijo.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
            conceptoFijo.setConceptoTipoPersonal(conceptoTipoPersonal);
            conceptoFijo.setUnidades(conceptoTipoPersonal.getUnidades());
            conceptoFijo.setMontoRestituir(0.0D);
            conceptoFijo.setUnidadesRestituir(0.0D);
            conceptoFijo.setRestituir("N");
            conceptoFijo.setMonto(nuevaCompensacion);
            conceptoFijo.setIdConceptoFijo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
            pm.makePersistent(conceptoFijo);
          } else {
            ConceptoFijo conceptoFijo = (ConceptoFijo)this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorConceptoTipoPersonalFrecuenciaTipoPersonal(trabajador.getIdTrabajador(), conceptoTipoPersonal.getIdConceptoTipoPersonal(), conceptoTipoPersonal.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal()).iterator().next();
            conceptoFijo.setMonto(nuevaCompensacion);
          }
          if (paso != 0)
            trabajador.setPaso(paso);
          else {
            trabajador.setPaso(detalleTabulador.getPaso());
          }
          this.log.error("trabajador.paso " + trabajador.getPaso());
        } else {
          if (aumento.equals("PO")) {
            double totalAumentar = (totalAnterior + sueldoAnterior) * ((100.0D + porcentaje) / 100.0D);
            if (totalAumentar > trabajador.getSueldoBasico())
              try {
                detalleTabulador = this.cargoNoGenBusiness.findDetalleTabuladorByMonto(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado(), totalAumentar);
                trabajador.setSueldoBasico(detalleTabulador.getMonto());
                trabajador.setPaso(detalleTabulador.getPaso());
              } catch (Exception e) {
                this.log.error("No consiguio el tabulador por el monto 4");
                trabajador.setSueldoBasico(totalAumentar);
                trabajador.setPaso(this.cargoNoGenBusiness.findDetalleTabuladorMaximoPaso(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado()).getPaso());
              }
          }
          else
          {
            try
            {
              detalleTabulador = this.cargoNoGenBusiness.findDetalleTabuladorForRegistroCargos(registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), registroCargos.getCargo().getGrado(), registroCargos.getCargo().getSubGrado(), paso);
              trabajador.setSueldoBasico(detalleTabulador.getMonto());
              trabajador.setPaso(detalleTabulador.getPaso());
            } catch (Exception e) {
              this.log.error("Excepcion controlada:", e);
              ErrorSistema error = new ErrorSistema();
              error.setDescription("No se consigue el tabulador");
              throw error;
            }

          }

          ConceptoFijo conceptoFijoSueldo = new ConceptoFijo();
          conceptoFijoSueldo = this.trabajadorNoGenBusiness.findConceptoFijoById(idConceptoFijoSueldo);
          if ((conceptoFijoSueldo.getFrecuenciaTipoPersonal().getCodFrecuenciaPago() == 1) || (conceptoFijoSueldo.getFrecuenciaTipoPersonal().getCodFrecuenciaPago() == 2))
            conceptoFijoSueldo.setMonto(detalleTabulador.getMonto());
          else if (conceptoFijoSueldo.getFrecuenciaTipoPersonal().getCodFrecuenciaPago() == 3)
            conceptoFijoSueldo.setMonto(detalleTabulador.getMonto() / 2.0D);
          else if (conceptoFijoSueldo.getFrecuenciaTipoPersonal().getCodFrecuenciaPago() == 4)
            conceptoFijoSueldo.setMonto(detalleTabulador.getMonto() * 7.0D);
        }
      }
    } catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarConceptosFijosAumentoSueldoSinRegistro(PersistenceManager pm, Trabajador trabajador, double monto) {
    try {
      Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoByTrabajador(trabajador.getIdTrabajador());
      Iterator iterConceptoFijo = colConceptoFijo.iterator();
      while (iterConceptoFijo.hasNext())
      {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iterConceptoFijo.next();

        conceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoById(conceptoFijo.getIdConceptoFijo());
        conceptoFijo.setMontoAnterior(conceptoFijo.getMonto());
        if ((conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0001")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0003")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0004")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0002")) || 
          (conceptoFijo.getConceptoTipoPersonal().getConcepto().getCodConcepto().equals("0013")))
        {
          if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
            conceptoFijo.setMonto(monto / 2.0D);
          else if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
            conceptoFijo.setMonto(monto / 30.0D * 7.0D);
          }

        }

      }

      trabajador.setSueldoBasico(monto);
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public Collection agregarConceptosFijos(Trabajador trabajador, RegistroCargos registroCargos, double sueldoBasico, String pagarRetroactivo) { Collection col = new ArrayList();
    try
    {
      this.log.error("agregarConceptosFijos - entro - paso 1 ");
      this.log.error(" trabajador.getTipoPersonal().getIdTipoPersonal() " + trabajador.getTipoPersonal().getIdTipoPersonal());
      Collection colConceptoTipoPersonal = this.definicionesBusiness.findConceptoTipoPersonalForIngresoTrabajador(trabajador.getTipoPersonal().getIdTipoPersonal());
      this.log.error("agregarConceptosFijos -  paso 2 ");

      this.log.error(" trabajador.getCargo().getIdCargo() " + trabajador.getCargo().getIdCargo());
      Collection colConceptoCargo = this.definicionesBusiness.findConceptoCargoByCargo(trabajador.getCargo().getIdCargo(), "N", "S");
      this.log.error("- paso 3 ");
      colConceptoTipoPersonal.addAll(colConceptoCargo);
      Iterator iterConceptoTipoPersonal = colConceptoTipoPersonal.iterator();
      this.log.error("- paso 4 ");

      colConceptoCargo = this.definicionesBusiness.findConceptoCargoByCargo(trabajador.getCargo().getIdCargo(), "S", "S");

      this.log.error("- paso 5 ");

      Iterator iterConceptoCargo = colConceptoCargo.iterator();

      boolean excluir = false;
      ConceptoTipoPersonal conceptoTipoPersonal = null;
      this.log.error("ConceptoTipoPersonal / col CTP " + colConceptoTipoPersonal.size());
      while (iterConceptoTipoPersonal.hasNext())
      {
        Object obj = iterConceptoTipoPersonal.next();
        if ((obj instanceof ConceptoTipoPersonal)) {
          conceptoTipoPersonal = (ConceptoTipoPersonal)obj;
          this.log.error(conceptoTipoPersonal.getConcepto().getDescripcion());
        }
        else {
          conceptoTipoPersonal = ((ConceptoCargo)obj).getConceptoTipoPersonal();
          this.log.error("Es un conceptoCargo");
        }

        iterConceptoCargo = colConceptoCargo.iterator();
        while (iterConceptoCargo.hasNext()) {
          ConceptoCargo conceptoCargo = (ConceptoCargo)iterConceptoCargo.next();
          if (conceptoTipoPersonal.equals(conceptoCargo.getConceptoTipoPersonal())) {
            excluir = true;
            break;
          }
        }

        if (!excluir) {
          this.log.error("1 " + conceptoTipoPersonal.getConcepto().getDescripcion());
          this.log.error("turno " + trabajador.getTurno().getJornada());
          ConceptoFijo conceptoFijo = new ConceptoFijo();
          conceptoFijo.setTrabajador(trabajador);
          conceptoFijo.setDocumentoSoporte(null);
          conceptoFijo.setEstatus("A");
          conceptoFijo.setFechaComienzo(new java.util.Date());
          conceptoFijo.setFechaRegistro(new java.util.Date());
          conceptoFijo.setFechaEliminar(null);
          conceptoFijo.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
          conceptoFijo.setConceptoTipoPersonal(conceptoTipoPersonal);
          conceptoFijo.setUnidades(conceptoTipoPersonal.getUnidades());
          conceptoFijo.setMontoRestituir(0.0D);
          conceptoFijo.setUnidadesRestituir(0.0D);
          conceptoFijo.setRestituir("N");
          conceptoFijo.setMonto(0.0D);

          if ((conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0001")) || 
            (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0003")) || 
            (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0004")) || 
            (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0002")) || 
            (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0005")) || 
            (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0010")) || 
            (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0011")) || 
            (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0013"))) {
            this.log.error("sueldo basico " + sueldoBasico);
            conceptoFijo.setMonto(sueldoBasico);

            if (trabajador.getTurno().getJornada().equals("C")) {
              this.log.error("turno " + trabajador.getTurno().getJornada());
              this.log.error("concepto " + conceptoTipoPersonal.getConcepto().getCodConcepto());
              if ((conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0001")) || 
                (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0003")) || 
                (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0004")) || 
                (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0010")) || 
                (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0011")) || 
                (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0013"))) {
                if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
                  conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 2.0D));
                else if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
                  conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() * 7.0D));
                }
              }
            }
            else if ((conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0002")) || 
              (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0005"))) {
              if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3)
                conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 2.0D / (8.0D / registroCargos.getHoras())));
              else if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
                conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() * 7.0D / (8.0D / registroCargos.getHoras())));
              }
            }
          }

          conceptoFijo.setIdConceptoFijo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
          this.log.error("CF " + conceptoFijo);
          col.add(conceptoFijo);
        }
      }
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return null;
    }
    return col; }

  public void fraccionarConceptos(Trabajador trabajador)
  {
    try
    {
      Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorNoPersistente(trabajador.getIdTrabajador());
      Iterator iterConceptoFijo = colConceptoFijo.iterator();
      while (iterConceptoFijo.hasNext()) {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iterConceptoFijo.next();
        conceptoFijo.setMontoRestituir(conceptoFijo.getMonto());
        conceptoFijo.setRestituir("S");

        if (conceptoFijo.getConceptoTipoPersonal().getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
          if (this.dia == 16) {
            conceptoFijo.setUnidadesRestituir(15.0D);
            conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 15.0D * (30 - trabajador.getDiaIngreso() + 1)));
            conceptoFijo.setUnidades(30 - trabajador.getDiaIngreso() + 1);
          } else {
            conceptoFijo.setUnidadesRestituir(15.0D);
            conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 15.0D * (15 - trabajador.getDiaIngreso() + 1)));
            conceptoFijo.setUnidades(15 - trabajador.getDiaIngreso() + 1);
          }
        } else {
          conceptoFijo.setUnidadesRestituir(30.0D);
          conceptoFijo.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 30.0D * (30 - trabajador.getDiaIngreso() + 1)));
          conceptoFijo.setUnidades(30 - trabajador.getDiaIngreso() + 1);
        }
      }
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public Collection calcularRetroactivos(Trabajador trabajador)
  {
    Collection col = new ArrayList();
    try
    {
      int diasRetroactivo;
      int diasRetroactivo;
      if (!trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"))
      {
        int diasRetroactivo;
        if (this.anio != trabajador.getAnioIngreso())
          diasRetroactivo = 30 - (trabajador.getDiaIngreso() - 1) + 30 * (this.mes + 12 - (trabajador.getMesIngreso() + 1)) + (this.dia - 1);
        else
          diasRetroactivo = 30 - (trabajador.getDiaIngreso() - 1) + 30 * (this.mes - (trabajador.getMesIngreso() + 1)) + (this.dia - 1);
      }
      else
      {
        diasRetroactivo = this.fechaProximaNomina.compareTo(trabajador.getFechaIngreso());
        this.log.error("--------------------------fechaProximaNomina" + this.fechaProximaNomina);
        this.log.error("--------------------------fechaIngreso" + trabajador.getFechaIngreso());
        this.log.error("--------------------------dias Retractivo" + diasRetroactivo);
      }
      Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorNoPersistente(trabajador.getIdTrabajador());
      Iterator iterConceptoFijo = colConceptoFijo.iterator();
      while (iterConceptoFijo.hasNext()) {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iterConceptoFijo.next();
        if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getRetroactivo().equals("S")) {
          ConceptoVariable conceptoVariable = new ConceptoVariable();
          try {
            conceptoVariable.setConceptoTipoPersonal((ConceptoTipoPersonal)this.definicionesBusiness.findConceptoTipoPersonalByTipoPersonalAndConcepto(trabajador.getTipoPersonal().getIdTipoPersonal(), conceptoFijo.getConceptoTipoPersonal().getConcepto().getConceptoRetroactivo().getIdConcepto()).iterator().next());
          } catch (Exception e) {
            this.log.error("Excepcion controlada:", e);
            ErrorSistema error = new ErrorSistema();
            error.setDescription("Ocurrió un error buscando el concepto retroactivo para el concepto " + conceptoFijo.getConceptoTipoPersonal().getConcepto().toString());
            throw error;
          }
          this.log.error("conceptoTipoPersonal " + conceptoVariable.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
          conceptoVariable.setTrabajador(trabajador);
          conceptoVariable.setDocumentoSoporte(null);
          conceptoVariable.setEstatus("A");
          conceptoVariable.setFechaRegistro(new java.util.Date());
          conceptoVariable.setUnidades(diasRetroactivo);
          if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
            conceptoVariable.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 15.0D * diasRetroactivo));
          } else if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
            conceptoVariable.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 7.0D * diasRetroactivo));
            conceptoVariable.setFrecuenciaTipoPersonal(conceptoFijo.getFrecuenciaTipoPersonal());
          } else if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() != 2) {
            conceptoVariable.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 30.0D * diasRetroactivo));
          } else if ((conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 2) && 
            (this.dia == 16)) {
            conceptoVariable.setMonto(NumberTools.twoDecimal(conceptoFijo.getMonto() / 30.0D * (diasRetroactivo - 15)));
          }

          this.log.error("monto Concepto " + conceptoVariable.getMonto());
          this.log.error("unidades Concepto " + conceptoVariable.getUnidades());
          if (trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("Q")) {
            if (this.dia == 1)
              conceptoVariable.setFrecuenciaTipoPersonal(this.definicionesExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(1, trabajador.getTipoPersonal().getIdTipoPersonal()));
            else
              conceptoVariable.setFrecuenciaTipoPersonal(this.definicionesExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(2, trabajador.getTipoPersonal().getIdTipoPersonal()));
          }
          else if (trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("M")) {
            conceptoVariable.setFrecuenciaTipoPersonal(this.definicionesExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(2, trabajador.getTipoPersonal().getIdTipoPersonal()));
          }
          conceptoVariable.setIdConceptoVariable(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable"));
          col.add(conceptoVariable);
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
    return col;
  }

  public Collection calcularRetroactivosNuevoCargo(Trabajador trabajador, java.util.Date fechaMovimiento) {
    Collection col = new ArrayList();
    try
    {
      TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();
      int diasRetroactivo;
      int diasRetroactivo;
      if (!trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"))
      {
        int diasRetroactivo;
        if (this.anio != fechaMovimiento.getYear() + 1900)
          diasRetroactivo = 30 - (fechaMovimiento.getDate() - 1) + 30 * (this.mes + 12 - (fechaMovimiento.getMonth() + 2)) + (this.dia - 1);
        else
          diasRetroactivo = 30 - (fechaMovimiento.getDate() - 1) + 30 * (this.mes - (fechaMovimiento.getMonth() + 2)) + (this.dia - 1);
      }
      else {
        diasRetroactivo = this.fechaProximaNomina.compareTo(fechaMovimiento);
      }
      this.log.error("fecha.date" + fechaMovimiento.getDate());
      this.log.error("mes" + this.mes);
      this.log.error("dia" + this.dia);
      this.log.error("fecha.month" + fechaMovimiento.getMonth());
      this.log.error("diasRetroactivo" + diasRetroactivo);

      Trabajador trabajadorEdit = trabajadorBeanBusiness.findTrabajadorById(trabajador.getIdTrabajador());
      Collection colConceptoFijo = this.trabajadorNoGenBusiness.findConceptoFijoByTrabajadorNoPersistente(trabajador.getIdTrabajador());
      Iterator iterConceptoFijo = colConceptoFijo.iterator();
      while (iterConceptoFijo.hasNext()) {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iterConceptoFijo.next();
        if (conceptoFijo.getConceptoTipoPersonal().getConcepto().getRetroactivo().equals("S")) {
          trabajadorEdit.setHayRetroactivo("S");
          ConceptoVariable conceptoVariable = new ConceptoVariable();
          try {
            this.log.error("concepto" + conceptoFijo.getConceptoTipoPersonal().getConcepto().toString());
            conceptoVariable.setConceptoTipoPersonal((ConceptoTipoPersonal)this.definicionesBusiness.findConceptoTipoPersonalByTipoPersonalAndConcepto(trabajador.getTipoPersonal().getIdTipoPersonal(), conceptoFijo.getConceptoTipoPersonal().getConcepto().getConceptoRetroactivo().getIdConcepto()).iterator().next());
          }
          catch (Exception e) {
            this.log.error("Excepcion controlada:", e);
            ErrorSistema error = new ErrorSistema();
            error.setDescription("Ocurrió un error buscando el concepto retroactivo para el concepto " + conceptoFijo.getConceptoTipoPersonal().getConcepto().toString());
            throw error;
          }
          this.log.error("conceptoTipoPersonal " + conceptoVariable.getConceptoTipoPersonal().getIdConceptoTipoPersonal());
          conceptoVariable.setTrabajador(trabajador);
          conceptoVariable.setDocumentoSoporte(null);
          conceptoVariable.setEstatus("A");
          conceptoVariable.setFechaRegistro(new java.util.Date());
          conceptoVariable.setUnidades(diasRetroactivo);
          if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
            conceptoVariable.setMonto(NumberTools.twoDecimal((conceptoFijo.getMonto() - conceptoFijo.getMontoAnterior()) / 15.0D * diasRetroactivo));
          } else if (conceptoFijo.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 4) {
            conceptoVariable.setMonto(NumberTools.twoDecimal((conceptoFijo.getMonto() - conceptoFijo.getMontoAnterior()) / 7.0D * diasRetroactivo));
            conceptoVariable.setFrecuenciaTipoPersonal(conceptoFijo.getFrecuenciaTipoPersonal());
          } else {
            conceptoVariable.setMonto(NumberTools.twoDecimal((conceptoFijo.getMonto() - conceptoFijo.getMontoAnterior()) / 30.0D * diasRetroactivo));
          }
          this.log.error("monto Concepto " + conceptoVariable.getMonto());
          this.log.error("unidades Concepto " + conceptoVariable.getUnidades());
          if (trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("Q")) {
            if (this.dia == 1)
              conceptoVariable.setFrecuenciaTipoPersonal(this.definicionesExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(1, trabajador.getTipoPersonal().getIdTipoPersonal()));
            else
              conceptoVariable.setFrecuenciaTipoPersonal(this.definicionesExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(2, trabajador.getTipoPersonal().getIdTipoPersonal()));
          }
          else if (trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("M")) {
            conceptoVariable.setFrecuenciaTipoPersonal(this.definicionesExtend.findFrecuenciaTipoPersonalByTipoPersonalNoPersistente(2, trabajador.getTipoPersonal().getIdTipoPersonal()));
          }
          conceptoVariable.setIdConceptoVariable(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable"));
          col.add(conceptoVariable);
        }
      }
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error calculando el retroactivo");
      throw error;
    }
    return col;
  }
  public void calcularLunesFraccionados(Trabajador trabajador) {
    Calendar calendar = Calendar.getInstance();
    if ((trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("Q")) || 
      (trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("M")))
      if (trabajador.getMesIngreso() == this.mes) {
        if (trabajador.getDiaIngreso() == 16) {
          trabajador.setLunesPrimera(-1);
        } else if (trabajador.getDiaIngreso() > 16) {
          trabajador.setLunesPrimera(-1);
          java.util.Date date = new java.util.Date();
          calendar.setTime(this.fechaProximaNomina);

          calendar.set(getFechaProximaNomina().getYear() + 1900, getFechaProximaNomina().getMonth(), calendar.getActualMaximum(5));
          this.log.error("Fecha fin mes" + calendar.getTime());
          trabajador.setLunesSegunda(lunesEnPeriodo(trabajador.getFechaIngreso(), calendar.getTime()));
        }
        else if (trabajador.getDiaIngreso() > 1) {
          if (this.fechaProximaNomina.getDate() == 1) {
            calendar.set(getFechaProximaNomina().getYear() + 1900, getFechaProximaNomina().getMonth(), 15);
            trabajador.setLunesPrimera(lunesEnPeriodo(trabajador.getFechaIngreso(), calendar.getTime()));
            this.log.error("proxima nomina es 1 y graba lunes 1ra -" + lunesEnPeriodo(trabajador.getFechaIngreso(), calendar.getTime()));
            if (lunesEnPeriodo(trabajador.getFechaIngreso(), calendar.getTime()) == 0)
              trabajador.setLunesPrimera(-1);
          }
          else {
            trabajador.setLunesPrimera(-1);
            this.log.error("proxima nomina no es 1 y graba lunes 1ra menos -1");
          }

        }

      }
      else if (this.fechaProximaNomina.getDate() != 1)
        trabajador.setLunesPrimera(-1);
  }

  public void calcularLunesRetroactivo(Trabajador trabajador)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(this.fechaProximaNomina);
    calendar.add(5, -1);
    trabajador.setLunesRetroactivo(lunesEnPeriodo(trabajador.getFechaIngreso(), calendar.getTime()));
  }

  public int lunesEnPeriodo(java.util.Date desde, java.util.Date hasta) {
    int lunes = 0;
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(desde);
    while (calendar.getTime().getTime() <= hasta.getTime()) {
      if (calendar.getTime().getDay() == 1) {
        lunes++;
      }
      calendar.add(5, 1);
    }

    return lunes;
  }

  public void reversar(long idTrabajador, long idHistoricoCargos)
  {
    Connection connection = Resource.getConnection();
    try
    {
      connection.setAutoCommit(false);
      this.log.error("paso por el inicio de reversar");
      Statement stActualizar = null;
      stActualizar = connection.createStatement();
      StringBuffer sql = new StringBuffer();
      sql.append("delete from conceptofijo where id_trabajador = " + idTrabajador);
      stActualizar.addBatch(sql.toString());

      sql = new StringBuffer();
      sql.append("delete from conceptovariable where id_trabajador = " + idTrabajador);
      stActualizar.addBatch(sql.toString());

      sql = new StringBuffer();
      sql.append("delete from sueldopromedio where id_trabajador = " + idTrabajador);
      stActualizar.addBatch(sql.toString());

      sql = new StringBuffer();
      sql.append("update registroCargos set situacion ='V', id_trabajador = null where id_trabajador = " + idTrabajador);
      stActualizar.addBatch(sql.toString());

      sql = new StringBuffer();
      sql.append("delete from historicocargos where id_historico_cargos = " + idHistoricoCargos);
      stActualizar.addBatch(sql.toString());

      sql = new StringBuffer();
      sql.append("delete from trabajador where id_trabajador = " + idTrabajador);
      stActualizar.addBatch(sql.toString());

      stActualizar.executeBatch();
      this.log.error("paso por el final de reversar");
      connection.commit();
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public void borrarConceptosCero(long idTrabajador)
  {
    Connection connection = Resource.getConnection();
    try
    {
      connection.setAutoCommit(false);
      Statement stActualizar = null;
      stActualizar = connection.createStatement();
      StringBuffer sql = new StringBuffer();
      sql.append("delete from conceptofijo where monto = 0 and id_trabajador = " + idTrabajador);
      stActualizar.addBatch(sql.toString());

      sql = new StringBuffer();
      sql.append("delete from conceptovariable where monto = 0 and id_trabajador = " + idTrabajador);
      stActualizar.addBatch(sql.toString());

      stActualizar.executeBatch();
      connection.commit();
      this.log.error("borro los conceptos con valor 0");
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public void buscarProximaNomina(long idGrupoNomina)
  {
    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    ResultSet rsSeguridadOrdinaria = null;
    PreparedStatement stSeguridadOrdinaria = null;

    StringBuffer sql = new StringBuffer();
    sql.append("select max(fecha_fin) + 1 as fecha_proxima ");
    sql.append(" from seguridadordinaria ");
    sql.append("  where id_grupo_nomina = ? ");
    try
    {
      stSeguridadOrdinaria = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stSeguridadOrdinaria.setLong(1, idGrupoNomina);
      rsSeguridadOrdinaria = stSeguridadOrdinaria.executeQuery();

      int dia = 0;
      int mes = 0;
      if (rsSeguridadOrdinaria.next()) {
        this.dia = rsSeguridadOrdinaria.getDate("fecha_proxima").getDate();
        this.mes = (rsSeguridadOrdinaria.getDate("fecha_proxima").getMonth() + 1);
        this.anio = (rsSeguridadOrdinaria.getDate("fecha_proxima").getYear() + 1900);
        this.fechaProximaNomina = rsSeguridadOrdinaria.getDate("fecha_proxima");
      }
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
  }

  public int getAnio() { return this.anio; }

  public int getDia() {
    return this.dia;
  }
  public java.util.Date getFechaProximaNomina() {
    return this.fechaProximaNomina;
  }
  public int getMes() {
    return this.mes;
  }
  public void setAnio(int i) {
    this.anio = i;
  }
  public void setDia(int i) {
    this.dia = i;
  }
  public void setFechaProximaNomina(java.util.Date date) {
    this.fechaProximaNomina = date;
  }
  public void setMes(int i) {
    this.mes = i;
  }
}