package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class RegistroSitpNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findForRemesa(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo && remesa == null";

    Query query = pm.newQuery(RegistroSitp.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colRegistroSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroSitp);

    return colRegistroSitp;
  }

  public Collection findByRemesa(long idRemesa)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "remesa.idRemesa == pIdRemesa";

    Query query = pm.newQuery(RegistroSitp.class, filter);

    query.declareParameters("long pIdRemesa");
    HashMap parameters = new HashMap();

    parameters.put("pIdRemesa", new Long(idRemesa));

    Collection colRegistroSitp = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroSitp);

    return colRegistroSitp;
  }

  public int findLastNumero(long idOrganismo, int anio)
    throws Exception
  {
    ResultSet rsRegistro = null;
    PreparedStatement stRegistro = null;
    Connection connection = Resource.getConnection();
    connection.setAutoCommit(true);

    StringBuffer sql = new StringBuffer();
    sql.append("select max(numero_movimiento) as ultimo from registrositp ");
    sql.append(" where id_organismo = ? and anio = ?");
    stRegistro = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);

    stRegistro.setLong(1, idOrganismo);
    stRegistro.setInt(2, anio);
    rsRegistro = stRegistro.executeQuery();
    rsRegistro.next();
    return rsRegistro.getInt("ultimo");
  }
}