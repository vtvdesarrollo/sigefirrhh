package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.base.registro.RegistroPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.planificacion.seleccion.SeleccionNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;
import sigefirrhh.sistema.sitp.SitpFacade;

public class TrasladoArt35NoLefpDemForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TrasladoArt35NoLefpDemForm.class.getName());
  private String observaciones;
  private boolean egresado;
  private Date fechaIngreso;
  private String remesa;
  private int numeroMovimiento;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private TipoPersonal tipoPersonal;
  private Collection result;
  private boolean showData1;
  private LoginSession login;
  private boolean showReport;
  private int reportId;
  private String reportName;
  private boolean error;
  private long idMovimientoSitp;
  private long idCausaMovimiento = 18L;

  private SeleccionNoGenFacade seleccionFacade = new SeleccionNoGenFacade();
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private SitpFacade sitpFacade = new SitpFacade();
  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private Personal personal;
  private String findSelectTrabajadorIdTipoPersonal;
  private String selectCausaPersonal;
  private Collection colTipoPersonal;
  private Collection colCausaPersonal;
  private Collection colRegistroCargos;
  private int findPersonalCedula;
  private String selectIdTipoPersonal;
  private String selectIdCausaPersonal;
  private String selectIdRegistroCargos;
  private RegistroCargos registroCargos;
  private boolean showFieldsAux;
  private boolean showRegistroCargosAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private CausaPersonal causaPersonal;
  private boolean showButtonAux;
  private boolean showFields;
  private long idTrabajador;
  private String pagarRetroactivo = "S";

  public boolean isShowCausaPersonal()
  {
    return (this.colCausaPersonal != null) && (this.showData1);
  }
  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (this.showData1) && (this.showRegistroCargosAux);
  }
  public boolean isShowFields() {
    return (this.showData1) && (this.showFieldsAux);
  }

  public String redirectPersonal() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    try
    {
      externalContext.redirect("/sigefirrhh/sigefirrhh/personal/expediente/Personal.jsf");
      context.responseComplete();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    return "success";
  }

  public String runReport() {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", getLogin().getOrganismo().getNombreOrganismo());
      parameters.put("id_organismo", new Long(getLogin().getOrganismo().getIdOrganismo()));
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(getLogin().getURLLogo()));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");
      parameters.put("id_personal", new Long(getPersonal().getIdPersonal()));
      parameters.put("id_causa_movimiento", new Long(this.idCausaMovimiento));
      parameters.put("id_movimiento", new Long(this.idMovimientoSitp));

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaIngreso == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }
      if (!this.showButtonAux) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El proceso no se puede ejecutar, mientras no seleccione todos los campos requeridos", ""));
        return null;
      }

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroMovimientoSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.idMovimientoSitp = this.movimientosNoGenFacade.ingresoTrabajadorLefp(this.personal.getIdPersonal(), this.tipoPersonal.getIdTipoPersonal(), this.registroCargos.getIdRegistroCargos(), this.fechaIngreso, this.idCausaMovimiento, this.sueldo, this.numeroMovimiento, null, this.login.getIdUsuario(), this.fechaPuntoCuenta, this.puntoCuenta, 0L, this.pagarRetroactivo, this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.personal, this.personal);

      this.showReport = true;
      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      abort();
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
    }

    return "cancel";
  }

  public void changeRegistroCargos(ValueChangeEvent event) {
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();

    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
      actualizarCampos();
      this.showFieldsAux = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarCampos()
  {
    try
    {
      this.showButtonAux = true;

      this.nombreDependencia = this.registroCargos.getDependencia().toString();
      this.nombreSede = this.registroCargos.getSede().toString();
      this.nombreRegion = this.registroCargos.getSede().getRegion().toString();
      this.descripcionCargo = this.registroCargos.getCargo().toString();
      this.grado = this.registroCargos.getCargo().getGrado();

      DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), this.grado, this.registroCargos.getCargo().getSubGrado(), 1);
      this.sueldo = detalleTabulador.getMonto();
    }
    catch (Exception e)
    {
      this.nombreDependencia = null;
      this.nombreSede = null;
      this.nombreRegion = null;
      this.descripcionCargo = null;
      this.grado = 0;
      this.sueldo = 0.0D;
      this.numeroMovimiento = 0;
      this.remesa = null;
      this.fechaIngreso = null;
      this.showButtonAux = false;
    }
  }

  public void changeCausaPersonalForTipoPersonal(ValueChangeEvent event)
  {
    FacesContext context = FacesContext.getCurrentInstance();

    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    int valor = 0;
    try {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);

      valor = this.trabajadorNoGenFacade.verificarSiTrabajadorPuedeReingresar(this.personal.getIdPersonal(), this.login.getIdOrganismo(), this.tipoPersonal.getIdTipoPersonal());

      if (valor == 5) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Trabajador activo en tipo de personal restringido", ""));
        return;
      }if (valor == 2) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Trabajador ya egresado, utilizar opción de reingreso", ""));
        return;
      }if (valor == 3) {
        this.egresado = true;
        return;
      }

      llenarRegistroCargos();
      this.showRegistroCargosAux = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void continuarConEgresado()
  {
    this.egresado = false;
    llenarRegistroCargos();
  }

  public void llenarRegistroCargos()
  {
    try {
      long idClasificacionPersonal = this.tipoPersonal.getClasificacionPersonal().getIdClasificacionPersonal();

      RegistroPersonal registroPersonal = (RegistroPersonal)this.registroFacade.findRegistroPersonalByTipoPersonal(this.tipoPersonal.getIdTipoPersonal()).iterator().next();

      this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosByRegistroAndSituacionAndGrado99(registroPersonal.getRegistro().getIdRegistro(), "V", "N");

      this.showRegistroCargosAux = true;
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getColTipoPersonal() { Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col; }

  public Collection getColRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroCargos.getIdRegistroCargos()), 
        registroCargos.toString()));
    }

    return col;
  }

  public Collection getColCausaPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonal.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }
    return col;
  }

  public TrasladoArt35NoLefpDemForm() throws Exception
  {
    this.reportName = "formato01lefp";
    FacesContext context = FacesContext.getCurrentInstance();
    log.error("PASO1");
    this.colCausaPersonal = new ArrayList();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.reportName = "formato01lefp";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.showReport = false;
    this.idMovimientoSitp = 0L;

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        TrasladoArt35NoLefpDemForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public void refresh()
  {
    try
    {
      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd(
        "S", "N", this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula() { FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.error = false;

      Collection col = this.sitpFacade.findInhabilitadoByCedula(this.findPersonalCedula);

      if (!col.isEmpty()) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Esta persona se encuentra inhabilitada", ""));
        return null;
      }
      this.personal = 
        ((Personal)this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo()).iterator().next());

      this.showData1 = true;
    } catch (Exception e) {
      this.showData1 = false;
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Los datos personales no estan registrados ", ""));
      this.error = true;
    }

    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    return null;
  }

  public String abort()
  {
    this.egresado = false;
    this.showData1 = false;
    this.colCausaPersonal = null;
    this.colRegistroCargos = null;
    this.selectIdTipoPersonal = "0";
    this.selectIdRegistroCargos = "0";
    this.selectIdCausaPersonal = "0";
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaIngreso = null;
    this.sueldo = 0.0D;

    this.fechaPuntoCuenta = null;

    this.puntoCuenta = "";
    this.observaciones = "";

    return "cancel";
  }

  public boolean isShowData1() {
    return this.showData1;
  }

  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }
  public String getSelectCausaPersonal() {
    return this.selectCausaPersonal;
  }
  public void setSelectCausaPersonal(String string) {
    this.selectCausaPersonal = string;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public void setFindPersonalCedula(int findPersonalCedula) {
    this.findPersonalCedula = findPersonalCedula;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }
  public String getSelectIdCausaPersonal() {
    return this.selectIdCausaPersonal;
  }
  public void setSelectIdCausaPersonal(String selectIdCausaPersonal) {
    this.selectIdCausaPersonal = selectIdCausaPersonal;
  }
  public String getSelectIdRegistroCargos() {
    return this.selectIdRegistroCargos;
  }
  public void setSelectIdRegistroCargos(String string) {
    this.selectIdRegistroCargos = string;
  }
  public RegistroCargos getRegistroCargos() {
    return this.registroCargos;
  }
  public void setRegistroCargos(RegistroCargos cargos) {
    this.registroCargos = cargos;
  }
  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public int getGrado() {
    return this.grado;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public double getSueldo() {
    return this.sueldo;
  }
  public void setDescripcionCargo(String string) {
    this.descripcionCargo = string;
  }
  public void setGrado(int i) {
    this.grado = i;
  }
  public void setNombreDependencia(String string) {
    this.nombreDependencia = string;
  }
  public void setNombreRegion(String string) {
    this.nombreRegion = string;
  }
  public void setNombreSede(String string) {
    this.nombreSede = string;
  }
  public void setSueldo(double d) {
    this.sueldo = d;
  }
  public Date getFechaIngreso() {
    return this.fechaIngreso;
  }
  public void setFechaIngreso(Date date) {
    this.fechaIngreso = date;
  }
  public boolean isEgresado() {
    return this.egresado;
  }
  public void setEgresado(boolean b) {
    this.egresado = b;
  }
  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setFechaPuntoCuenta(Date date) {
    this.fechaPuntoCuenta = date;
  }
  public void setPuntoCuenta(String string) {
    this.puntoCuenta = string;
  }

  public boolean isShowFieldsAux() {
    return this.showFieldsAux;
  }
  public boolean isShowReport() {
    return this.showReport;
  }
  public void setShowFieldsAux(boolean showFieldsAux) {
    this.showFieldsAux = showFieldsAux;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }

  public void setPagarRetroactivo(String string) {
    this.pagarRetroactivo = string;
  }

  public boolean isError()
  {
    return this.error;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }

  public void setShowFields(boolean showFields) {
    this.showFields = showFields;
  }
  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }
}