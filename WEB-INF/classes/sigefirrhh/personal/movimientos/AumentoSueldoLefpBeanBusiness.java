package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.HistorialOrganismo;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class AumentoSueldoLefpBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(AumentoSueldoLefpBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();

  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  public long actualizar(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones) throws Exception
  {
    this.registrosBusiness.validarFechas(fechaMovimiento, fechaPuntoCuenta);

    this.txn.open();
    long id = 0L;
    PersistenceManager pm = PMThread.getPM();

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Trabajador trabajador = new Trabajador();
    trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(trabajador.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaMovimiento);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }
    this.log.error("no encontró problemas en trayectoria");

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }

    HistorialOrganismo historialOrganismo = new HistorialOrganismo();
    try
    {
      Trabajador trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

      long idRegistroCargoAnterior = trabajador.getRegistroCargos().getIdRegistroCargos();

      int anteriorCodManualCargo = trabajador.getCargo().getManualCargo().getCodManualCargo();
      String anteriorCodCargo = trabajador.getCargo().getCodCargo();
      String anteriorDescripcionCargo = trabajador.getCargo().getCodCargo();
      int anteriorCodigoNomina = trabajador.getCodigoNomina();
      String anteriorCodSede = trabajador.getDependencia().getSede().getCodSede();
      String anteriorNombreSede = trabajador.getDependencia().getSede().getNombre();
      String anteriorCodDependencia = trabajador.getDependencia().getCodDependencia();
      String anteriorNombreDependencia = trabajador.getDependencia().getNombre();
      double anteriorSueldo = trabajador.getSueldoBasico();
      double anteriorCompensacion = sueldoPromedio.getPromedioCompensacion();
      double anteriorPrimasCargo = sueldoPromedio.getPromedioPrimasc();
      double anteriorPrimasTrabajador = sueldoPromedio.getPromedioPrimast();
      int anteriorGrado = trabajador.getCargo().getGrado();
      int anteriorPaso = trabajador.getPaso();
      String anteriorCodRegion = trabajador.getDependencia().getSede().getRegion().getCodRegion();
      String anteriorNombreRegion = trabajador.getDependencia().getSede().getRegion().getNombre();

      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      trabajadorEdit.setFechaUltimoMovimiento(fechaMovimiento);

      if (organismo.getAprobacionMpd().equals("S"))
        trabajadorEdit.setMovimiento("T");
      else {
        trabajadorEdit.setMovimiento("A");
      }
      trabajadorEdit.setSituacion("1");

      this.registrosBusiness.actualizarConceptosFijosAumentoSueldo(pm, trabajadorEdit, trabajadorEdit.getRegistroCargos(), aumento, porcentaje, paso);

      this.txn.close();

      this.txn.open();
      pm = PMThread.getPM();

      Trabajador trabajador2 = new Trabajador();
      trabajador2 = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

      organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

      causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

      this.calcularSueldosPromedioBeanBusiness.calcularUnTrabajadorParaMovimientos(trabajador2, numeroMovimiento);

      this.registrosBusiness.buscarProximaNomina(trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());

      if ((this.registrosBusiness.getFechaProximaNomina().compareTo(fechaMovimiento) > 0) && (pagarRetroactivo.equals("S")))
      {
        Collection colRetroactivo = new ArrayList();

        colRetroactivo.addAll(this.registrosBusiness.calcularRetroactivosNuevoCargo(trabajador, fechaMovimiento));
        Iterator iterRetroactivo = colRetroactivo.iterator();
        while (iterRetroactivo.hasNext()) {
          this.log.error("PASO While");
          ConceptoVariable conceptoVariable = (ConceptoVariable)iterRetroactivo.next();
          pm.makePersistent(conceptoVariable);
        }

        this.log.error("CALCULO RETROACTIVOS");
      }

      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador2, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaMovimiento, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, codConcurso, observaciones, usuario.getUsuario(), "0");

      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajador2, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaMovimiento, null, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, 
        anteriorCodManualCargo, 
        anteriorCodCargo, 
        anteriorDescripcionCargo, 
        anteriorCodigoNomina, 
        anteriorCodSede, 
        anteriorNombreSede, 
        anteriorCodDependencia, 
        anteriorNombreDependencia, 
        anteriorSueldo, 
        anteriorCompensacion, 
        anteriorPrimasCargo, 
        anteriorPrimasTrabajador, 
        anteriorGrado, 
        anteriorPaso, 
        anteriorCodRegion, 
        anteriorNombreRegion, 
        observaciones, 0.0D, 0.0D, 0.0D);
      pm.makePersistent(movimientoSitp);

      id = movimientoSitp.getIdMovimientoSitp();

      this.log.error("GRABO MOVIMIENTOSITP");

      this.txn.close();

      this.registrosBusiness.borrarConceptosCero(trabajador.getIdTrabajador());
    } catch (ErrorSistema a) {
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.rollback();
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return id;
  }
}