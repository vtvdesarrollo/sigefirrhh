package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.MovimientoPersonal;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.sistema.SistemaFacade;
import sigefirrhh.sistema.Usuario;

public class MovimientoSitpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MovimientoSitpForm.class.getName());
  private MovimientoSitp movimientoSitp;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private MovimientosFacade movimientosFacade = new MovimientosFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colClasificacionPersonal;
  private Collection colTurno;
  private Collection colMovimientoPersonalForCausaMovimiento;
  private Collection colCausaMovimiento;
  private Collection colRemesa;
  private Collection colPersonal;
  private Collection colUsuario;
  private String selectClasificacionPersonal;
  private String selectTurno;
  private String selectMovimientoPersonalForCausaMovimiento;
  private String selectCausaMovimiento;
  private String selectRemesa;
  private String selectPersonal;
  private String selectUsuario;
  private Object stateResultPersonal = null;

  private Object stateResultMovimientoSitpByPersonal = null;

  public String getSelectClasificacionPersonal()
  {
    return this.selectClasificacionPersonal;
  }
  public void setSelectClasificacionPersonal(String valClasificacionPersonal) {
    Iterator iterator = this.colClasificacionPersonal.iterator();
    ClasificacionPersonal clasificacionPersonal = null;
    this.movimientoSitp.setClasificacionPersonal(null);
    while (iterator.hasNext()) {
      clasificacionPersonal = (ClasificacionPersonal)iterator.next();
      if (String.valueOf(clasificacionPersonal.getIdClasificacionPersonal()).equals(
        valClasificacionPersonal)) {
        this.movimientoSitp.setClasificacionPersonal(
          clasificacionPersonal);
        break;
      }
    }
    this.selectClasificacionPersonal = valClasificacionPersonal;
  }
  public String getSelectTurno() {
    return this.selectTurno;
  }
  public void setSelectTurno(String valTurno) {
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    this.movimientoSitp.setTurno(null);
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      if (String.valueOf(turno.getIdTurno()).equals(
        valTurno)) {
        this.movimientoSitp.setTurno(
          turno);
        break;
      }
    }
    this.selectTurno = valTurno;
  }
  public String getSelectMovimientoPersonalForCausaMovimiento() {
    return this.selectMovimientoPersonalForCausaMovimiento;
  }
  public void setSelectMovimientoPersonalForCausaMovimiento(String valMovimientoPersonalForCausaMovimiento) {
    this.selectMovimientoPersonalForCausaMovimiento = valMovimientoPersonalForCausaMovimiento;
  }
  public void changeMovimientoPersonalForCausaMovimiento(ValueChangeEvent event) {
    long idMovimientoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCausaMovimiento = null;
      if (idMovimientoPersonal > 0L)
        this.colCausaMovimiento = 
          this.registroFacade.findCausaMovimientoByMovimientoPersonal(
          idMovimientoPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowMovimientoPersonalForCausaMovimiento() { return this.colMovimientoPersonalForCausaMovimiento != null; }

  public String getSelectCausaMovimiento() {
    return this.selectCausaMovimiento;
  }
  public void setSelectCausaMovimiento(String valCausaMovimiento) {
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    this.movimientoSitp.setCausaMovimiento(null);
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      if (String.valueOf(causaMovimiento.getIdCausaMovimiento()).equals(
        valCausaMovimiento)) {
        this.movimientoSitp.setCausaMovimiento(
          causaMovimiento);
        break;
      }
    }
    this.selectCausaMovimiento = valCausaMovimiento;
  }
  public boolean isShowCausaMovimiento() {
    return this.colCausaMovimiento != null;
  }
  public String getSelectRemesa() {
    return this.selectRemesa;
  }
  public void setSelectRemesa(String valRemesa) {
    Iterator iterator = this.colRemesa.iterator();
    Remesa remesa = null;
    this.movimientoSitp.setRemesa(null);
    while (iterator.hasNext()) {
      remesa = (Remesa)iterator.next();
      if (String.valueOf(remesa.getIdRemesa()).equals(
        valRemesa)) {
        this.movimientoSitp.setRemesa(
          remesa);
        break;
      }
    }
    this.selectRemesa = valRemesa;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.movimientoSitp.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.movimientoSitp.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public String getSelectUsuario() {
    return this.selectUsuario;
  }
  public void setSelectUsuario(String valUsuario) {
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    this.movimientoSitp.setUsuario(null);
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      if (String.valueOf(usuario.getIdUsuario()).equals(
        valUsuario)) {
        this.movimientoSitp.setUsuario(
          usuario);
        break;
      }
    }
    this.selectUsuario = valUsuario;
  }
  public Collection getResult() {
    return this.result;
  }

  public MovimientoSitp getMovimientoSitp() {
    if (this.movimientoSitp == null) {
      this.movimientoSitp = new MovimientoSitp();
    }
    return this.movimientoSitp;
  }

  public MovimientoSitpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColClasificacionPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colClasificacionPersonal.iterator();
    ClasificacionPersonal clasificacionPersonal = null;
    while (iterator.hasNext()) {
      clasificacionPersonal = (ClasificacionPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(clasificacionPersonal.getIdClasificacionPersonal()), 
        clasificacionPersonal.toString()));
    }
    return col;
  }

  public Collection getColTurno()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTurno.iterator();
    Turno turno = null;
    while (iterator.hasNext()) {
      turno = (Turno)iterator.next();
      col.add(new SelectItem(
        String.valueOf(turno.getIdTurno()), 
        turno.toString()));
    }
    return col;
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();

    Iterator iterEntry = MovimientoSitp.LISTA_TIPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColMovimientoPersonalForCausaMovimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMovimientoPersonalForCausaMovimiento.iterator();
    MovimientoPersonal movimientoPersonalForCausaMovimiento = null;
    while (iterator.hasNext()) {
      movimientoPersonalForCausaMovimiento = (MovimientoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(movimientoPersonalForCausaMovimiento.getIdMovimientoPersonal()), 
        movimientoPersonalForCausaMovimiento.toString()));
    }
    return col;
  }

  public Collection getColCausaMovimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaMovimiento.getIdCausaMovimiento()), 
        causaMovimiento.toString()));
    }
    return col;
  }

  public Collection getColRemesa()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRemesa.iterator();
    Remesa remesa = null;
    while (iterator.hasNext()) {
      remesa = (Remesa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(remesa.getIdRemesa()), 
        remesa.toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public Collection getColUsuario()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colClasificacionPersonal = 
        this.definicionesFacade.findAllClasificacionPersonal();
      this.colTurno = 
        this.definicionesFacade.findTurnoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colMovimientoPersonalForCausaMovimiento = 
        this.registroFacade.findAllMovimientoPersonal();

      this.colUsuario = 
        this.sistemaFacade.findAllUsuario();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findMovimientoSitpByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding)
      {
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectMovimientoSitp()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectClasificacionPersonal = null;
    this.selectTurno = null;
    this.selectCausaMovimiento = null;
    this.selectMovimientoPersonalForCausaMovimiento = null;

    this.selectRemesa = null;
    this.selectPersonal = null;
    this.selectUsuario = null;

    long idMovimientoSitp = 
      Long.parseLong((String)requestParameterMap.get("idMovimientoSitp"));
    try
    {
      this.movimientoSitp = 
        this.movimientosFacade.findMovimientoSitpById(
        idMovimientoSitp);

      if (this.movimientoSitp.getClasificacionPersonal() != null) {
        this.selectClasificacionPersonal = 
          String.valueOf(this.movimientoSitp.getClasificacionPersonal().getIdClasificacionPersonal());
      }
      if (this.movimientoSitp.getTurno() != null) {
        this.selectTurno = 
          String.valueOf(this.movimientoSitp.getTurno().getIdTurno());
      }
      if (this.movimientoSitp.getCausaMovimiento() != null) {
        this.selectCausaMovimiento = 
          String.valueOf(this.movimientoSitp.getCausaMovimiento().getIdCausaMovimiento());
      }
      if (this.movimientoSitp.getRemesa() != null) {
        this.selectRemesa = 
          String.valueOf(this.movimientoSitp.getRemesa().getIdRemesa());
      }
      if (this.movimientoSitp.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.movimientoSitp.getPersonal().getIdPersonal());
      }
      if (this.movimientoSitp.getUsuario() != null) {
        this.selectUsuario = 
          String.valueOf(this.movimientoSitp.getUsuario().getIdUsuario());
      }

      CausaMovimiento causaMovimiento = null;
      MovimientoPersonal movimientoPersonalForCausaMovimiento = null;

      if (this.movimientoSitp.getCausaMovimiento() != null) {
        long idCausaMovimiento = 
          this.movimientoSitp.getCausaMovimiento().getIdCausaMovimiento();
        this.selectCausaMovimiento = String.valueOf(idCausaMovimiento);
        causaMovimiento = this.registroFacade.findCausaMovimientoById(
          idCausaMovimiento);
        this.colCausaMovimiento = this.registroFacade.findCausaMovimientoByMovimientoPersonal(
          causaMovimiento.getMovimientoPersonal().getIdMovimientoPersonal());

        long idMovimientoPersonalForCausaMovimiento = 
          this.movimientoSitp.getCausaMovimiento().getMovimientoPersonal().getIdMovimientoPersonal();
        this.selectMovimientoPersonalForCausaMovimiento = String.valueOf(idMovimientoPersonalForCausaMovimiento);
        movimientoPersonalForCausaMovimiento = 
          this.registroFacade.findMovimientoPersonalById(
          idMovimientoPersonalForCausaMovimiento);
        this.colMovimientoPersonalForCausaMovimiento = 
          this.registroFacade.findAllMovimientoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.movimientoSitp.getFechaIngreso() != null) && 
      (this.movimientoSitp.getFechaIngreso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Ingreso Trabajador no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.movimientoSitp.getFechaMovimiento() != null) && 
      (this.movimientoSitp.getFechaMovimiento().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Movimiento no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.movimientoSitp.getFechaRegistro() != null) && 
      (this.movimientoSitp.getFechaRegistro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Registro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.movimientoSitp.getFechaCulminacion() != null) && 
      (this.movimientoSitp.getFechaCulminacion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Culminacion no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.movimientoSitp.getFechaInicioMpd() != null) && 
      (this.movimientoSitp.getFechaInicioMpd().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Inicio Mpd no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.movimientoSitp.getFechaFinMpd() != null) && 
      (this.movimientoSitp.getFechaFinMpd().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Fin Mpd no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.movimientoSitp.getTiempoSitp() != null) && 
      (this.movimientoSitp.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.movimientoSitp.setPersonal(
          this.personal);
        this.movimientosFacade.addMovimientoSitp(
          this.movimientoSitp);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.movimientosFacade.updateMovimientoSitp(
          this.movimientoSitp);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.movimientosFacade.deleteMovimientoSitp(
        this.movimientoSitp);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectClasificacionPersonal = null;

    this.selectTurno = null;

    this.selectCausaMovimiento = null;

    this.selectMovimientoPersonalForCausaMovimiento = null;

    this.selectRemesa = null;

    this.selectPersonal = null;

    this.selectUsuario = null;

    this.movimientoSitp = new MovimientoSitp();

    this.movimientoSitp.setPersonal(this.personal);

    this.movimientoSitp.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.movimientoSitp.setIdMovimientoSitp(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.movimientos.MovimientoSitp"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.movimientoSitp = new MovimientoSitp();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.movimientoSitp = new MovimientoSitp();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }
}