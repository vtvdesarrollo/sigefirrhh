package sigefirrhh.personal.movimientos;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBusiness;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.ExpedienteBusiness;
import sigefirrhh.personal.expediente.HistorialApn;
import sigefirrhh.personal.expediente.HistorialOrganismo;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.sistema.Usuario;

public class JubilacionTrabajadorSinRegistroBeanBusiness
{
  Logger log = Logger.getLogger(JubilacionTrabajadorSinRegistroBeanBusiness.class.getName());

  private TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();
  private CargoBusiness cargoBusiness = new CargoBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private ExpedienteBusiness expedienteBusiness = new ExpedienteBusiness();
  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();
  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();

  public boolean ingresarTrabajador(long idPersonal, long idTipoPersonal, java.util.Date fechaIngreso, long idCausaPersonal, double sueldoBasico, Usuario usuario, long idDependencia, long idCargo, int codigoNomina, double porcentajeJubilacion)
    throws Exception
  {
    this.txn.open();
    PersistenceManager pm = PMThread.getPM();
    double horas = 8.0D;
    long idSueldoPromedio = 0L;
    int diaFechaIngreso = fechaIngreso.getDate();
    int mesFechaIngreso = fechaIngreso.getMonth() + 1;
    int anioFechaIngreso = fechaIngreso.getYear() + 1900;

    Cargo cargo = this.cargoBusiness.findCargoById(idCargo);
    Dependencia dependencia = this.estructuraBusiness.findDependenciaById(idDependencia);
    Personal personal = this.expedienteBusiness.findPersonalById(idPersonal);
    TipoPersonal tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    CausaPersonal causaPersonal = new CausaPersonal();
    causaPersonal = this.registroBusiness.findCausaPersonalById(idCausaPersonal);

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());

    Trabajador trabajador = new Trabajador();

    SueldoPromedio sueldoPromedio = new SueldoPromedio();

    trabajador.setAnioAntiguedad(0);
    trabajador.setAnioEgreso(0);
    trabajador.setAnioIngreso(anioFechaIngreso);
    trabajador.setAnioIngresoApn(0);
    trabajador.setAnioJubilacion(anioFechaIngreso);
    trabajador.setAnioPrestaciones(0);
    trabajador.setAnioVacaciones(0);
    trabajador.setBancoFid(null);
    trabajador.setBancoLph(null);
    trabajador.setBancoNomina(null);
    trabajador.setBaseJubilacion(0.0D);
    trabajador.setCargo(cargo);
    trabajador.setCargoReal(cargo);
    trabajador.setCausaMovimiento(causaPersonal.getCausaMovimiento());
    trabajador.setCedula(personal.getCedula());
    trabajador.setCodCargo(cargo.getCodCargo());
    trabajador.setCodigoNomina(codigoNomina);
    trabajador.setCodigoNominaReal(codigoNomina);
    trabajador.setCodTipoPersonal(tipoPersonal.getCodTipoPersonal());
    trabajador.setCotizaFju("N");
    trabajador.setCotizaLph("N");
    trabajador.setCotizaSpf("N");
    trabajador.setCotizaSso("N");
    trabajador.setCuentaFid(null);
    trabajador.setCuentaLph(null);
    trabajador.setCuentaNomina(null);
    trabajador.setDedProxNomina("S");
    trabajador.setDependencia(dependencia);
    trabajador.setDependenciaReal(dependencia);
    trabajador.setDiaAntiguedad(0);
    trabajador.setDiaEgreso(0);
    trabajador.setDiaIngreso(diaFechaIngreso);
    trabajador.setDiaIngresoApn(0);
    trabajador.setDiaJubilacion(diaFechaIngreso);
    trabajador.setDiaPrestaciones(0);
    trabajador.setDiaVacaciones(0);
    trabajador.setEstatus("A");
    trabajador.setFechaAntiguedad(null);
    trabajador.setFechaEgreso(null);
    trabajador.setFechaEntradaSig(new java.util.Date());
    trabajador.setFechaIngreso(fechaIngreso);
    trabajador.setFechaIngresoApn(null);
    trabajador.setFechaJubilacion(fechaIngreso);
    trabajador.setFechaPrestaciones(null);
    trabajador.setFechaSalidaSig(null);
    trabajador.setFechaVacaciones(null);
    trabajador.setFeVida("S");
    trabajador.setLugarPago(dependencia.getSede().getLugarPago());
    trabajador.setMesAntiguedad(0);
    trabajador.setMesEgreso(0);
    trabajador.setMesIngreso(mesFechaIngreso);
    trabajador.setMesIngresoApn(0);
    trabajador.setMesJubilacion(mesFechaIngreso);
    trabajador.setMesPrestaciones(0);
    trabajador.setMesVacaciones(0);
    if (organismo.getAprobacionMpd().equals("S"))
      trabajador.setMovimiento("T");
    else {
      trabajador.setMovimiento("A");
    }
    trabajador.setOrganismo(organismo);
    trabajador.setParProxNomina("N");
    trabajador.setPaso(1);
    trabajador.setPersonal(personal);
    trabajador.setPorcentajeIslr(0.0D);
    trabajador.setPorcentajeJubilacion(porcentajeJubilacion);
    trabajador.setRegimen("I");
    trabajador.setRegistroCargos(null);
    trabajador.setRiesgo("1");
    trabajador.setSituacion("1");
    trabajador.setSueldoBasico(sueldoBasico);
    trabajador.setTipoCtaNomina("N");
    trabajador.setTipoPersonal(tipoPersonal);
    trabajador.setTurno(dependencia.getSede().getTurno());
    trabajador.setIdTrabajador(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.Trabajador"));
    pm.makePersistent(trabajador);

    sueldoPromedio.setTrabajador(trabajador);
    this.log.error("GRABO TRABAJADOR");
    sueldoPromedio.setIdSueldoPromedio(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.SueldoPromedio"));
    pm.makePersistent(sueldoPromedio);
    idSueldoPromedio = sueldoPromedio.getIdSueldoPromedio();
    this.log.error("GRABO SUELDOPROMEDIO");

    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    ResultSet rsSeguridadOrdinaria = null;
    PreparedStatement stSeguridadOrdinaria = null;

    StringBuffer sql = new StringBuffer();
    sql.append("select max(fecha_fin) + 1 as fecha_proxima ");
    sql.append(" from seguridadordinaria ");
    sql.append("  where id_grupo_nomina = ? ");

    stSeguridadOrdinaria = connection.prepareStatement(
      sql.toString(), 
      1003, 
      1007);
    stSeguridadOrdinaria.setLong(1, trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());
    rsSeguridadOrdinaria = stSeguridadOrdinaria.executeQuery();

    int dia = 0;
    int mes = 0;
    if (rsSeguridadOrdinaria.next()) {
      dia = rsSeguridadOrdinaria.getDate("fecha_proxima").getDate();
      mes = rsSeguridadOrdinaria.getDate("fecha_proxima").getMonth() + 1;
    }

    this.log.error("BUSCO MES Y DIA");

    Collection colConceptoTipoPersonal = this.definicionesBusiness.findConceptoTipoPersonalForIngresoTrabajador(idTipoPersonal);
    Iterator iterConceptoTipoPersonal = colConceptoTipoPersonal.iterator();
    while (iterConceptoTipoPersonal.hasNext()) {
      ConceptoTipoPersonal conceptoTipoPersonal = (ConceptoTipoPersonal)iterConceptoTipoPersonal.next();
      ConceptoFijo conceptoFijo = new ConceptoFijo();
      conceptoFijo.setTrabajador(trabajador);
      conceptoFijo.setDocumentoSoporte(null);
      conceptoFijo.setEstatus("A");
      conceptoFijo.setFechaComienzo(new java.util.Date());
      conceptoFijo.setFechaRegistro(new java.util.Date());
      conceptoFijo.setFechaEliminar(null);
      conceptoFijo.setFrecuenciaTipoPersonal(conceptoTipoPersonal.getFrecuenciaTipoPersonal());
      conceptoFijo.setConceptoTipoPersonal(conceptoTipoPersonal);
      conceptoFijo.setUnidades(conceptoTipoPersonal.getUnidades());
      conceptoFijo.setMontoRestituir(0.0D);
      conceptoFijo.setUnidadesRestituir(0.0D);
      conceptoFijo.setRestituir("N");
      conceptoFijo.setMonto(0.0D);

      if ((conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0010")) || (conceptoTipoPersonal.getConcepto().getCodConcepto().equals("0011"))) {
        conceptoFijo.setMonto(sueldoBasico);

        if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
          conceptoFijo.setMonto(conceptoFijo.getMonto() / 2.0D);
        }
      }

      if ((dia != 0) && 
        (trabajador.getMesIngreso() == mes) && (trabajador.getDiaIngreso() > dia)) {
        conceptoFijo.setMontoRestituir(conceptoFijo.getMonto());
        conceptoFijo.setRestituir("S");

        if (conceptoTipoPersonal.getFrecuenciaTipoPersonal().getFrecuenciaPago().getCodFrecuenciaPago() == 3) {
          conceptoFijo.setUnidadesRestituir(15.0D);
          conceptoFijo.setMonto(conceptoFijo.getMonto() / 15.0D * (15 - (trabajador.getDiaIngreso() - 1)));
          conceptoFijo.setUnidades(15 - (trabajador.getDiaIngreso() - 1));
        } else {
          conceptoFijo.setUnidadesRestituir(30.0D);
          conceptoFijo.setMonto(conceptoFijo.getMonto() / 30.0D * (30 - (trabajador.getDiaIngreso() - 1)));
          conceptoFijo.setUnidades(30 - (trabajador.getDiaIngreso() - 1));
        }
      }

      conceptoFijo.setIdConceptoFijo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo"));
      pm.makePersistent(conceptoFijo);
      this.log.error("GRABO CONCEPTOFIJO");
    }

    this.txn.close();
    this.log.error("TERMINO TODO");
    this.calcularSueldosPromedioBeanBusiness.calcularUnTrabajador(trabajador);
    this.log.error("RECALCULO CONCEPTOS");
    pm.close();

    this.txn.open();

    pm = PMThread.getPM();
    sueldoPromedio = this.trabajadorBusiness.findSueldoPromedioById(idSueldoPromedio);
    tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);
    organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());
    causaPersonal = this.registroBusiness.findCausaPersonalById(idCausaPersonal);
    trabajador = this.trabajadorBusiness.findTrabajadorById(sueldoPromedio.getTrabajador().getIdTrabajador());

    HistorialOrganismo historialOrganismo = new HistorialOrganismo();
    historialOrganismo.setAfectaSueldo("N");
    historialOrganismo.setApellidosNombres(trabajador.getPersonal().getPrimerApellido() + " " + trabajador.getPersonal().getPrimerNombre());
    if (organismo.getAprobacionMpd().equals("S"))
      historialOrganismo.setAprobacionMpd("S");
    else {
      historialOrganismo.setAprobacionMpd("N");
    }

    historialOrganismo.setCausaMovimiento(causaPersonal.getCausaMovimiento());
    historialOrganismo.setCedula(trabajador.getCedula());
    historialOrganismo.setClasificacionPersonal(trabajador.getTipoPersonal().getClasificacionPersonal());
    historialOrganismo.setCodOrganismo(organismo.getCodOrganismo());
    historialOrganismo.setCodCargo(trabajador.getCargo().getCodCargo());
    historialOrganismo.setCodCausaMovimiento(causaPersonal.getCausaMovimiento().getCodCausaMovimiento());
    historialOrganismo.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    historialOrganismo.setCodigoNomina(trabajador.getCodigoNomina());
    historialOrganismo.setCodManualCargo(trabajador.getCargo().getManualCargo().getCodManualCargo());
    historialOrganismo.setCodSede(trabajador.getDependencia().getSede().getCodSede());
    historialOrganismo.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    if (trabajador.getCargo().getManualCargo().getTabulador() != null) {
      historialOrganismo.setCodTabulador(trabajador.getCargo().getManualCargo().getTabulador().getCodTabulador());
    }
    historialOrganismo.setNombreRegion(trabajador.getDependencia().getSede().getNombre());
    historialOrganismo.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    historialOrganismo.setDocumentoSoporte(null);
    historialOrganismo.setEstatus("4");
    historialOrganismo.setGrado(trabajador.getCargo().getGrado());
    historialOrganismo.setNombreOrganismo(organismo.getNombreOrganismo());
    historialOrganismo.setNombreDependencia(trabajador.getDependencia().getNombre());
    historialOrganismo.setNombreSede(trabajador.getDependencia().getSede().getNombre());
    historialOrganismo.setNumeroMovimiento(0);
    historialOrganismo.setOrganismo(organismo);
    historialOrganismo.setPaso(trabajador.getPaso());
    historialOrganismo.setPersonal(trabajador.getPersonal());
    historialOrganismo.setRemesa(null);
    historialOrganismo.setSueldo(trabajador.getSueldoBasico());
    historialOrganismo.setTipoPersonal(trabajador.getCargo().getTipoCargo());
    historialOrganismo.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    historialOrganismo.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    historialOrganismo.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    historialOrganismo.setFechaMovimiento(fechaIngreso);
    historialOrganismo.setFechaRegistro(new java.util.Date());
    historialOrganismo.setOrigenMovimiento("S");
    historialOrganismo.setUsuario(usuario.getUsuario());
    historialOrganismo.setIdHistorialOrganismo(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.HistorialOrganismo"));

    pm.makePersistent(historialOrganismo);
    this.log.error("GRABO HISTORICONOMINA");

    HistorialApn historialApn = new HistorialApn();
    historialApn.setAfectaSueldo("N");
    historialApn.setApellidosNombres(trabajador.getPersonal().getPrimerApellido() + " " + trabajador.getPersonal().getPrimerNombre());
    if (organismo.getAprobacionMpd().equals("S"))
      historialApn.setAprobacionMpd("S");
    else {
      historialApn.setAprobacionMpd("N");
    }
    historialApn.setCausaMovimiento(causaPersonal.getCausaMovimiento());
    historialApn.setCedula(trabajador.getCedula());
    historialApn.setClasificacionPersonal(trabajador.getTipoPersonal().getClasificacionPersonal());
    historialApn.setCodOrganismo(organismo.getCodOrganismo());
    historialApn.setCodCargo(trabajador.getCargo().getCodCargo());
    historialApn.setCodCausaMovimiento(causaPersonal.getCausaMovimiento().getCodCausaMovimiento());
    historialApn.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    historialApn.setCodigoNomina(trabajador.getCodigoNomina());
    historialApn.setCodManualCargo(trabajador.getCargo().getManualCargo().getCodManualCargo());
    historialApn.setCodSede(trabajador.getDependencia().getSede().getCodSede());
    historialApn.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    if (trabajador.getCargo().getManualCargo().getTabulador() != null) {
      historialApn.setCodTabulador(trabajador.getCargo().getManualCargo().getTabulador().getCodTabulador());
    }
    historialApn.setNombreRegion(trabajador.getDependencia().getSede().getNombre());
    historialApn.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    historialApn.setDocumentoSoporte(null);
    historialApn.setEstatus("4");
    historialApn.setGrado(trabajador.getCargo().getGrado());
    historialApn.setNombreOrganismo(organismo.getNombreOrganismo());
    historialApn.setNombreDependencia(trabajador.getDependencia().getNombre());
    historialApn.setNombreSede(trabajador.getDependencia().getSede().getNombre());
    historialApn.setNumeroMovimiento(0);
    historialApn.setOrganismo(organismo);
    historialApn.setPaso(trabajador.getPaso());
    historialApn.setPersonal(trabajador.getPersonal());
    historialApn.setRemesa(null);
    historialApn.setSueldo(trabajador.getSueldoBasico());
    historialApn.setTipoPersonal(trabajador.getCargo().getTipoCargo());
    historialApn.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    historialApn.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    historialApn.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    historialApn.setFechaMovimiento(fechaIngreso);
    historialApn.setFechaRegistro(new java.util.Date());
    historialApn.setOrigenMovimiento("S");
    historialApn.setUsuario(usuario.getUsuario());
    historialApn.setIdHistorialApn(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.HistorialApn"));
    pm.makePersistent(historialApn);
    this.log.error("GRABO HISTORIALAPN");

    MovimientoSitp movimientoSitp = new MovimientoSitp();
    movimientoSitp.setAfectaSueldo("N");
    movimientoSitp.setApellidosNombres(trabajador.getPersonal().getPrimerApellido() + " " + trabajador.getPersonal().getPrimerNombre());

    movimientoSitp.setCausaMovimiento(causaPersonal.getCausaMovimiento());
    movimientoSitp.setCedula(trabajador.getCedula());
    movimientoSitp.setClasificacionPersonal(trabajador.getTipoPersonal().getClasificacionPersonal());
    movimientoSitp.setCodOrganismo(organismo.getCodOrganismo());
    movimientoSitp.setCodCargo(trabajador.getCargo().getCodCargo());
    movimientoSitp.setCodCausaMovimiento(causaPersonal.getCausaMovimiento().getCodCausaMovimiento());
    movimientoSitp.setCodDependencia(trabajador.getDependencia().getCodDependencia());
    movimientoSitp.setCodigoNomina(trabajador.getCodigoNomina());
    movimientoSitp.setCodManualCargo(trabajador.getCargo().getManualCargo().getCodManualCargo());
    movimientoSitp.setCodSede(trabajador.getDependencia().getSede().getCodSede());
    movimientoSitp.setCodRegion(trabajador.getDependencia().getSede().getRegion().getCodRegion());
    if (trabajador.getCargo().getManualCargo().getTabulador() != null) {
      movimientoSitp.setCodTabulador(trabajador.getCargo().getManualCargo().getTabulador().getCodTabulador());
    }
    movimientoSitp.setNombreRegion(trabajador.getDependencia().getSede().getNombre());
    movimientoSitp.setDescripcionCargo(trabajador.getCargo().getDescripcionCargo());
    movimientoSitp.setDocumentoSoporte(null);
    movimientoSitp.setEstatus("4");
    movimientoSitp.setGrado(trabajador.getCargo().getGrado());
    movimientoSitp.setNombreOrganismo(organismo.getNombreOrganismo());
    movimientoSitp.setNombreDependencia(trabajador.getDependencia().getNombre());
    movimientoSitp.setNombreSede(trabajador.getDependencia().getSede().getNombre());
    movimientoSitp.setNumeroMovimiento(0);
    movimientoSitp.setOrganismo(organismo);
    movimientoSitp.setPaso(trabajador.getPaso());
    movimientoSitp.setPersonal(trabajador.getPersonal());
    movimientoSitp.setRemesa(null);
    movimientoSitp.setSueldo(trabajador.getSueldoBasico());
    movimientoSitp.setTipoPersonal(trabajador.getCargo().getTipoCargo());
    movimientoSitp.setCompensacion(sueldoPromedio.getPromedioCompensacion());
    movimientoSitp.setPrimasCargo(sueldoPromedio.getPromedioPrimasc());
    movimientoSitp.setPrimasTrabajador(sueldoPromedio.getPromedioPrimast());
    movimientoSitp.setFechaMovimiento(fechaIngreso);
    movimientoSitp.setFechaRegistro(new java.util.Date());
    movimientoSitp.setUsuario(usuario);
    movimientoSitp.setIdMovimientoSitp(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.movimientos.MovimientoSitp"));

    pm.makePersistent(movimientoSitp);
    this.log.error("GRABO MOVIMIENTOSITP");

    this.txn.close();
    return true;
  }
}