package sigefirrhh.personal.movimientos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.sistema.Usuario;

public class Remesa
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idRemesa;
  private int anio;
  private int numero;
  private Date fechaCreacion;
  private Date fechaEnvio;
  private Date fechaCierre;
  private String estatus;
  private String observaciones;
  private Organismo organismo;
  private Usuario usuario;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "estatus", "fechaCierre", "fechaCreacion", "fechaEnvio", "idRemesa", "idSitp", "numero", "observaciones", "organismo", "tiempoSitp", "usuario" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.movimientos.Remesa"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Remesa());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("A", "ABIERTA");
    LISTA_ESTATUS.put("C", "CERRADA");
    LISTA_ESTATUS.put("E", "ENVIADA");
  }

  public Remesa()
  {
    jdoSetestatus(this, "A");
  }

  public String toString()
  {
    return jdoGetanio(this) + "  -  " + 
      jdoGetnumero(this) + " - " + LISTA_ESTATUS.get(jdoGetestatus(this));
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaCreacion() {
    return jdoGetfechaCreacion(this);
  }

  public Date getFechaEnvio() {
    return jdoGetfechaEnvio(this);
  }

  public long getIdRemesa() {
    return jdoGetidRemesa(this);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public int getNumero() {
    return jdoGetnumero(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setEstatus(String string) {
    jdoSetestatus(this, string);
  }

  public void setFechaCreacion(Date date) {
    jdoSetfechaCreacion(this, date);
  }

  public void setFechaEnvio(Date date) {
    jdoSetfechaEnvio(this, date);
  }

  public void setIdRemesa(long l) {
    jdoSetidRemesa(this, l);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setNumero(int i) {
    jdoSetnumero(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int i) {
    jdoSetanio(this, i);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public Date getFechaCierre()
  {
    return jdoGetfechaCierre(this);
  }

  public void setFechaCierre(Date date)
  {
    jdoSetfechaCierre(this, date);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 12;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Remesa localRemesa = new Remesa();
    localRemesa.jdoFlags = 1;
    localRemesa.jdoStateManager = paramStateManager;
    return localRemesa;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Remesa localRemesa = new Remesa();
    localRemesa.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRemesa.jdoFlags = 1;
    localRemesa.jdoStateManager = paramStateManager;
    return localRemesa;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCierre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCreacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEnvio);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRemesa);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numero);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCierre = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCreacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEnvio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRemesa = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Remesa paramRemesa, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramRemesa.anio;
      return;
    case 1:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramRemesa.estatus;
      return;
    case 2:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCierre = paramRemesa.fechaCierre;
      return;
    case 3:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCreacion = paramRemesa.fechaCreacion;
      return;
    case 4:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEnvio = paramRemesa.fechaEnvio;
      return;
    case 5:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.idRemesa = paramRemesa.idRemesa;
      return;
    case 6:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramRemesa.idSitp;
      return;
    case 7:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramRemesa.numero;
      return;
    case 8:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramRemesa.observaciones;
      return;
    case 9:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramRemesa.organismo;
      return;
    case 10:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramRemesa.tiempoSitp;
      return;
    case 11:
      if (paramRemesa == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramRemesa.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Remesa))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Remesa localRemesa = (Remesa)paramObject;
    if (localRemesa.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRemesa, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RemesaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RemesaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RemesaPK))
      throw new IllegalArgumentException("arg1");
    RemesaPK localRemesaPK = (RemesaPK)paramObject;
    localRemesaPK.idRemesa = this.idRemesa;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RemesaPK))
      throw new IllegalArgumentException("arg1");
    RemesaPK localRemesaPK = (RemesaPK)paramObject;
    this.idRemesa = localRemesaPK.idRemesa;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RemesaPK))
      throw new IllegalArgumentException("arg2");
    RemesaPK localRemesaPK = (RemesaPK)paramObject;
    localRemesaPK.idRemesa = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RemesaPK))
      throw new IllegalArgumentException("arg2");
    RemesaPK localRemesaPK = (RemesaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localRemesaPK.idRemesa);
  }

  private static final int jdoGetanio(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.anio;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.anio;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 0))
      return paramRemesa.anio;
    return localStateManager.getIntField(paramRemesa, jdoInheritedFieldCount + 0, paramRemesa.anio);
  }

  private static final void jdoSetanio(Remesa paramRemesa, int paramInt)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramRemesa, jdoInheritedFieldCount + 0, paramRemesa.anio, paramInt);
  }

  private static final String jdoGetestatus(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.estatus;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.estatus;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 1))
      return paramRemesa.estatus;
    return localStateManager.getStringField(paramRemesa, jdoInheritedFieldCount + 1, paramRemesa.estatus);
  }

  private static final void jdoSetestatus(Remesa paramRemesa, String paramString)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramRemesa, jdoInheritedFieldCount + 1, paramRemesa.estatus, paramString);
  }

  private static final Date jdoGetfechaCierre(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.fechaCierre;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.fechaCierre;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 2))
      return paramRemesa.fechaCierre;
    return (Date)localStateManager.getObjectField(paramRemesa, jdoInheritedFieldCount + 2, paramRemesa.fechaCierre);
  }

  private static final void jdoSetfechaCierre(Remesa paramRemesa, Date paramDate)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.fechaCierre = paramDate;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.fechaCierre = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRemesa, jdoInheritedFieldCount + 2, paramRemesa.fechaCierre, paramDate);
  }

  private static final Date jdoGetfechaCreacion(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.fechaCreacion;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.fechaCreacion;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 3))
      return paramRemesa.fechaCreacion;
    return (Date)localStateManager.getObjectField(paramRemesa, jdoInheritedFieldCount + 3, paramRemesa.fechaCreacion);
  }

  private static final void jdoSetfechaCreacion(Remesa paramRemesa, Date paramDate)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.fechaCreacion = paramDate;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.fechaCreacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRemesa, jdoInheritedFieldCount + 3, paramRemesa.fechaCreacion, paramDate);
  }

  private static final Date jdoGetfechaEnvio(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.fechaEnvio;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.fechaEnvio;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 4))
      return paramRemesa.fechaEnvio;
    return (Date)localStateManager.getObjectField(paramRemesa, jdoInheritedFieldCount + 4, paramRemesa.fechaEnvio);
  }

  private static final void jdoSetfechaEnvio(Remesa paramRemesa, Date paramDate)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.fechaEnvio = paramDate;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.fechaEnvio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRemesa, jdoInheritedFieldCount + 4, paramRemesa.fechaEnvio, paramDate);
  }

  private static final long jdoGetidRemesa(Remesa paramRemesa)
  {
    return paramRemesa.idRemesa;
  }

  private static final void jdoSetidRemesa(Remesa paramRemesa, long paramLong)
  {
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.idRemesa = paramLong;
      return;
    }
    localStateManager.setLongField(paramRemesa, jdoInheritedFieldCount + 5, paramRemesa.idRemesa, paramLong);
  }

  private static final int jdoGetidSitp(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.idSitp;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.idSitp;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 6))
      return paramRemesa.idSitp;
    return localStateManager.getIntField(paramRemesa, jdoInheritedFieldCount + 6, paramRemesa.idSitp);
  }

  private static final void jdoSetidSitp(Remesa paramRemesa, int paramInt)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramRemesa, jdoInheritedFieldCount + 6, paramRemesa.idSitp, paramInt);
  }

  private static final int jdoGetnumero(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.numero;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.numero;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 7))
      return paramRemesa.numero;
    return localStateManager.getIntField(paramRemesa, jdoInheritedFieldCount + 7, paramRemesa.numero);
  }

  private static final void jdoSetnumero(Remesa paramRemesa, int paramInt)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.numero = paramInt;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.numero = paramInt;
      return;
    }
    localStateManager.setIntField(paramRemesa, jdoInheritedFieldCount + 7, paramRemesa.numero, paramInt);
  }

  private static final String jdoGetobservaciones(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.observaciones;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.observaciones;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 8))
      return paramRemesa.observaciones;
    return localStateManager.getStringField(paramRemesa, jdoInheritedFieldCount + 8, paramRemesa.observaciones);
  }

  private static final void jdoSetobservaciones(Remesa paramRemesa, String paramString)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramRemesa, jdoInheritedFieldCount + 8, paramRemesa.observaciones, paramString);
  }

  private static final Organismo jdoGetorganismo(Remesa paramRemesa)
  {
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.organismo;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 9))
      return paramRemesa.organismo;
    return (Organismo)localStateManager.getObjectField(paramRemesa, jdoInheritedFieldCount + 9, paramRemesa.organismo);
  }

  private static final void jdoSetorganismo(Remesa paramRemesa, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramRemesa, jdoInheritedFieldCount + 9, paramRemesa.organismo, paramOrganismo);
  }

  private static final Date jdoGettiempoSitp(Remesa paramRemesa)
  {
    if (paramRemesa.jdoFlags <= 0)
      return paramRemesa.tiempoSitp;
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.tiempoSitp;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 10))
      return paramRemesa.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramRemesa, jdoInheritedFieldCount + 10, paramRemesa.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Remesa paramRemesa, Date paramDate)
  {
    if (paramRemesa.jdoFlags == 0)
    {
      paramRemesa.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRemesa, jdoInheritedFieldCount + 10, paramRemesa.tiempoSitp, paramDate);
  }

  private static final Usuario jdoGetusuario(Remesa paramRemesa)
  {
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
      return paramRemesa.usuario;
    if (localStateManager.isLoaded(paramRemesa, jdoInheritedFieldCount + 11))
      return paramRemesa.usuario;
    return (Usuario)localStateManager.getObjectField(paramRemesa, jdoInheritedFieldCount + 11, paramRemesa.usuario);
  }

  private static final void jdoSetusuario(Remesa paramRemesa, Usuario paramUsuario)
  {
    StateManager localStateManager = paramRemesa.jdoStateManager;
    if (localStateManager == null)
    {
      paramRemesa.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramRemesa, jdoInheritedFieldCount + 11, paramRemesa.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}