package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class EgresoTrabajadorRegistroBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(EgresoTrabajadorRegistroBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();

  public boolean egresarTrabajador(long idTrabajador, Date fechaEgresoReal, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, String observaciones)
    throws Exception
  {
    this.txn.open();
    this.log.error("id_org" + organismo2.getIdOrganismo());
    PersistenceManager pm = PMThread.getPM();

    int diaFechaEgresoReal = fechaEgresoReal.getDate();
    int mesFechaEgresoReal = fechaEgresoReal.getMonth() + 1;
    int anioFechaEgresoReal = fechaEgresoReal.getYear() + 1900;
    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();
    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Trabajador trabajador = new Trabajador();
    trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();

    Usuario usuario = new Usuario();
    usuario = this.sistemaBusiness.findUsuarioById(idUsuario);
    try
    {
      this.log.error("PASO no 3");

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaEgresoReal, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        null, null, null, observaciones, usuario.getUsuario(), "4");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      RegistroSitp registroSitp = new RegistroSitp();
      registroSitp = this.registrosBusiness.agregarRegistroSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaEgresoReal, "0", "S", 
        usuario, null, null, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones);
      pm.makePersistent(registroSitp);

      Trabajador trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

      trabajadorEdit.setEstatus("E");
      trabajadorEdit.setFechaEgreso(fechaEgresoReal);
      trabajadorEdit.setDiaEgreso(diaFechaEgresoReal);
      trabajadorEdit.setMesEgreso(mesFechaEgresoReal);
      trabajadorEdit.setAnioEgreso(anioFechaEgresoReal);
      trabajadorEdit.setFechaSalidaSig(fechaSalidaNomina);
      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      if (trabajadorEdit.getTipoPersonal().getManejaRac().equals("S")) {
        try {
          RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(trabajador.getRegistroCargos().getIdRegistroCargos());
          registroCargos.setSituacion("V");
          registroCargos.setTrabajador(null);
        } catch (Exception e) {
          ErrorSistema error = new ErrorSistema();
          error.setDescription("El trabajador no tiene asignado un registro cargos válido");
          this.txn.close();
          throw error;
        }
      }

      this.txn.close();
    } catch (ErrorSistema a) {
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return true;
  }
}