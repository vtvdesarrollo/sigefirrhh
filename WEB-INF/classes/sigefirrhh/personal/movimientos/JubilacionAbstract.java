package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class JubilacionAbstract extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(JubilacionAbstract.class.getName());
  private int numeroMovimiento;
  private String observaciones;
  private Collection result;
  private boolean show;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();

  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private boolean showResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colTrabajador;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  public Collection listTipoPersonal;
  private Date fechaVigencia;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      if (getFechaVigencia() == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }
      if (getFechaPuntoCuenta() == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de punto de cuenta del movimiento", ""));
        return null;
      }

      ejecutar2();
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }

  public void registrarAuditoria(FacesContext context) {
    RegistrarAuditoria.registrar(context, getLogin().getUsuarioObject(), 'P', getTrabajador(), getTrabajador().getPersonal());
  }

  public String ejecutar2()
  {
    return null;
  }

  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador);
    }

    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public JubilacionAbstract()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getFindColTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("S", "S", this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() { FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null; }

  public String findTrabajadorByCodigoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String show()
  {
    try
    {
      resetResult();
      this.show = false;
      selectTrabajador();
      this.show = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;

    this.showResultTrabajador = false;
  }

  public String abort()
  {
    resetResult();
    this.fechaVigencia = null;

    this.numeroMovimiento = 0;
    this.fechaPuntoCuenta = null;

    this.puntoCuenta = "";
    this.observaciones = "";

    return "cancel";
  }

  public boolean isShow() {
    return this.show;
  }

  public boolean isShowData() {
    return (this.show) && (this.selectedTrabajador);
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }

  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }

  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }

  public void setLogin(LoginSession login) {
    this.login = login;
  }
  public MovimientosNoGenFacade getMovimientosNoGenFacade() {
    return this.movimientosNoGenFacade;
  }
  public void setShow(boolean show) {
    this.show = show;
  }
  public Date getFechaVigencia() {
    return this.fechaVigencia;
  }
  public void setFechaVigencia(Date fechaVigencia) {
    this.fechaVigencia = fechaVigencia;
  }
  public DefinicionesNoGenFacade getDefinicionesNoGenFacade() {
    return this.definicionesNoGenFacade;
  }
  public TrabajadorNoGenFacade getTrabajadorFacade() {
    return this.trabajadorFacade;
  }
}