package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class LicenciaSuspensionConSinRegistroBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(LicenciaSuspensionConSinRegistroBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();

  public boolean actualizar(long idTrabajador, Date fechaMovimiento, Date fechaCulminacion, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones)
    throws Exception
  {
    if (fechaPuntoCuenta != null)
    {
      if (fechaPuntoCuenta.compareTo(fechaMovimiento) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La fecha de Punto de cuenta no puede ser mayor a la fecha de vigencia");
        throw error;
      }

    }

    this.txn.open();

    PersistenceManager pm = PMThread.getPM();

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Trabajador trabajador = new Trabajador();
    trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }

    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaMovimiento, fechaCulminacion, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, null, observaciones, usuario.getUsuario(), "4");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      RegistroSitp registroSitp = new RegistroSitp();
      registroSitp = this.registrosBusiness.agregarRegistroSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaMovimiento, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones);
      pm.makePersistent(registroSitp);

      this.log.error("GRABO REGISTROSITP");

      Trabajador trabajadorEdit = new Trabajador();
      trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

      if (trabajadorEdit.getTipoPersonal().getManejaRac().equals("S"))
      {
        RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(trabajador.getRegistroCargos().getIdRegistroCargos());

        if (idCausaMovimiento == 11L)
          registroCargos.setCondicion("5");
        else if (idCausaMovimiento == 12L) {
          registroCargos.setCondicion("6");
        }
      }

      trabajadorEdit.setSituacion("1");
      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      if (idCausaMovimiento == 11L) {
        trabajadorEdit.setEstatus("S");
        trabajadorEdit.setSituacion("9");
      } else if (idCausaMovimiento == 12L) {
        trabajadorEdit.setSituacion("8");
      } else if (idCausaMovimiento == 13L) {
        trabajadorEdit.setEstatus("S");
        trabajadorEdit.setSituacion("13");
        this.log.error("PASO POR EL 13");
      } else if ((idCausaMovimiento == 15L) || (idCausaMovimiento == 16L)) {
        trabajadorEdit.setEstatus("A");
        trabajadorEdit.setSituacion("1");
      }

      if (organismo.getAprobacionMpd().equals("S"))
        trabajador.setMovimiento("T");
      else {
        trabajador.setMovimiento("A");
      }

      this.log.error(this.txn);
      this.log.error("PASO no 5");

      if (this.txn != null) this.txn.close(); 
    }
    catch (ErrorSistema a)
    {
      this.txn.close();
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e) {
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return true;
  }
}