package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class EgresoTrabajadorLefpBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(EgresoTrabajadorLefpBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  public long egresarTrabajador(long idTrabajador, Date fechaEgreso, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, String observaciones)
    throws Exception
  {
    this.txn.open();

    PersistenceManager pm = PMThread.getPM();

    int diaFechaEgresoReal = fechaEgreso.getDate();
    int mesFechaEgresoReal = fechaEgreso.getMonth() + 1;
    int anioFechaEgresoReal = fechaEgreso.getYear() + 1900;
    long idMovimientoSitp = 0L;

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Trabajador trabajador = new Trabajador();
    trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    this.log.error("verifica 41 o 42");
    if ((idCausaMovimiento == 41L) || (idCausaMovimiento == 42L)) {
      if (trabajador.getCargo().getGrado() == 99)
      {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("Para realizar este movimiento el trabajador no debe poseer un cargo de grado 99");
        throw error;
      }

      if ((idCausaMovimiento == 42L) && (!trabajador.getSituacion().equals("7")))
      {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("No se ha registrado un movimiento de remoción a este trabajador");
        throw error;
      }

    }

    this.log.error("verifica 43");
    if ((idCausaMovimiento == 43L) && 
      (trabajador.getCargo().getGrado() != 99))
    {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Para realizar este movimiento el trabajador debe poseer un cargo de grado 99");
      throw error;
    }

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(trabajador.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaEgreso);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }

    this.log.error("no encontró problemas en trayectoria");

    if (idCausaMovimiento == 39L) {
      Calendar cal = Calendar.getInstance();

      cal.setTime(fechaEgreso);
      cal.add(2, -3);

      if (cal.getTime().compareTo(trabajador.getFechaIngreso()) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("Para realizar este movimiento la fecha de ingreso del trabajador debe tener una antiguedad no mayor a 3 meses");
        throw error;
      }

    }

    if (idCausaMovimiento == 39L) {
      Calendar cal = Calendar.getInstance();

      cal.setTime(fechaEgreso);
      cal.add(2, -3);

      if (cal.getTime().compareTo(trabajador.getFechaIngreso()) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("Para realizar este movimiento la fecha de ingreso del trabajador debe tener una antiguedad no mayor a 3 meses");
        throw error;
      }

    }

    if (idCausaMovimiento == 42L) {
      Calendar cal = Calendar.getInstance();

      cal.setTime(fechaEgreso);
      cal.add(2, -1);

      if (cal.getTime().compareTo(trabajador.getFechaIngreso()) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("Para realizar este movimiento la fecha de ingreso del trabajador debe tener una antiguedad no mayor a 3 meses");
        throw error;
      }

    }

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }

    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaEgreso, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        null, null, null, observaciones, usuario.getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaEgreso, null, "0", "S", 
        usuario, null, null, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones, 0.0D, 0.0D, 0.0D);
      pm.makePersistent(movimientoSitp);

      idMovimientoSitp = movimientoSitp.getIdMovimientoSitp();

      this.log.error("GRABO MOVIMIENTOSITP");

      Trabajador trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

      if (idCausaMovimiento == 41L)
        trabajadorEdit.setSituacion("7");
      else {
        trabajadorEdit.setEstatus("E");
      }

      trabajadorEdit.setFechaEgreso(fechaEgreso);
      trabajadorEdit.setDiaEgreso(diaFechaEgresoReal);
      trabajadorEdit.setMesEgreso(mesFechaEgresoReal);
      trabajadorEdit.setAnioEgreso(anioFechaEgresoReal);
      trabajadorEdit.setFechaSalidaSig(fechaSalidaNomina);
      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      trabajadorEdit.setFechaUltimoMovimiento(fechaEgreso);

      RegistroCargos registroCargos = new RegistroCargos();
      if (idCausaMovimiento != 41L) {
        try {
          registroCargos = this.registroCargosBusiness.findRegistroCargosById(trabajador.getRegistroCargos().getIdRegistroCargos());
          registroCargos.setSituacion("V");
          registroCargos.setTrabajador(null);
        }
        catch (Exception e) {
          ErrorSistema error = new ErrorSistema();
          error.setDescription("El trabajador no tiene asignado un registro cargos válido");
          this.txn.close();
          throw error;
        }

      }

      this.txn.close();
    } catch (ErrorSistema a) {
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return idMovimientoSitp;
  }
}