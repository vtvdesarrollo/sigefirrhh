package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;

public class ReportRemesasForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportRemesasForm.class.getName());
  private int reportId;
  private long idRemesa;
  private long idCausaMovimiento;
  private String reportName;
  private String tipoReporte;
  private Date desde;
  private Date hasta;
  private String estatus;
  private Collection listRemesa;
  private Collection listCausaMovimiento;
  private boolean show1;
  private boolean show2;
  private boolean show3;
  private RegistroFacade registroFacade;
  private MovimientosFacade movimientosFacade;
  private LoginSession login;

  public ReportRemesasForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.registroFacade = new RegistroFacade();
    this.movimientosFacade = new MovimientosFacade();
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.reportName = "RelacionRemesas";

    this.show1 = true;

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportRemesasForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listRemesa = this.movimientosFacade.findAllRemesa();
      this.listCausaMovimiento = this.registroFacade.findAllCausaMovimiento();
    } catch (Exception e) {
      this.listRemesa = new ArrayList();
      this.listCausaMovimiento = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;
      if (this.tipoReporte.equals("1")) {
        this.reportName = "RelacionRemesas";
        this.show1 = true;
      } else if (this.tipoReporte.equals("2")) {
        this.show2 = true;
        if (this.idRemesa == 0L)
          this.reportName = "DetalleRemesas";
        else
          this.reportName = "Detalle1Remesa";
      }
      else {
        this.show3 = true;
        if (this.idCausaMovimiento == 0L)
          this.reportName = "DetalleMovimientos";
        else
          this.reportName = "DetalleMovimientosCausa";
      }
    }
    catch (Exception localException)
    {
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      this.show1 = false;
      this.show2 = false;
      this.show3 = false;

      if (this.tipoReporte.equals("1")) {
        this.reportName = "RelacionRemesas";
        this.show1 = true;
      } else if (this.tipoReporte.equals("2")) {
        this.show2 = true;
        if (this.idRemesa == 0L)
          this.reportName = "DetalleRemesas";
        else
          this.reportName = "Detalle1Remesa";
      }
      else {
        this.show3 = true;
        if (this.idCausaMovimiento == 0L)
          this.reportName = "DetalleMovimientos";
        else {
          this.reportName = "DetalleMovimientosCausa";
        }
      }
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));

      parameters.put("id_remesa", new Long(this.idRemesa));
      parameters.put("fec_ini", this.desde);
      parameters.put("fec_fin", this.hasta);

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdRemesa()
  {
    return String.valueOf(this.idRemesa);
  }
  public String getIdCausaMovimiento() {
    return String.valueOf(this.idCausaMovimiento);
  }

  public void setIdRemesa(String l)
  {
    this.idRemesa = Long.parseLong(l);
  }

  public Collection getListRemesa()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRemesa, "sigefirrhh.personal.movimientos.Remesa");
  }

  public Collection getListCausaMovimiento() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listCausaMovimiento, "sigefirrhh.base.registro.CausaMovimiento");
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public Date getDesde() {
    return this.desde;
  }
  public void setDesde(Date desde) {
    this.desde = desde;
  }
  public Date getHasta() {
    return this.hasta;
  }
  public void setHasta(Date hasta) {
    this.hasta = hasta;
  }

  public String getEstatus()
  {
    return this.estatus;
  }

  public String getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setEstatus(String string)
  {
    this.estatus = string;
  }

  public void setTipoReporte(String string)
  {
    this.tipoReporte = string;
  }

  public boolean isShow1()
  {
    return this.show1;
  }

  public boolean isShow2()
  {
    return this.show2;
  }

  public boolean isShow3()
  {
    return this.show3;
  }

  public void setIdCausaMovimiento(long idCausaMovimiento) {
    this.idCausaMovimiento = idCausaMovimiento;
  }
}