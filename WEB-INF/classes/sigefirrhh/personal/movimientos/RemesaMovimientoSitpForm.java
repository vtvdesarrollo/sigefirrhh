package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteNoGenFacade;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.sistema.RegistrarAuditoria;

public class RemesaMovimientoSitpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RemesaMovimientoSitpForm.class.getName());
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private MovimientosNoGenFacade movimientosFacade = new MovimientosNoGenFacade();
  private ExpedienteNoGenFacade expedienteFacade = new ExpedienteNoGenFacade();
  private boolean showRemesaMovimientoSitpByRemesa;
  private String findSelectRemesa;
  private Collection findColRemesa;
  private Collection colRemesa;
  private Collection colMovimientoSitp;
  private String selectRemesa;
  private String selectMovimientoSitp;
  private Object stateResultRemesaMovimientoSitpByRemesa = null;

  public String getFindSelectRemesa()
  {
    return this.findSelectRemesa;
  }
  public void setFindSelectRemesa(String valRemesa) {
    this.findSelectRemesa = valRemesa;
  }

  public Collection getFindColRemesa() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRemesa.iterator();
    Remesa remesa = null;
    while (iterator.hasNext()) {
      remesa = (Remesa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(remesa.getIdRemesa()), 
        remesa.toString()));
    }
    return col;
  }

  public String getSelectRemesa()
  {
    return this.selectRemesa;
  }
  public void setSelectRemesa(String valRemesa) {
    Iterator iterator = this.colRemesa.iterator();
    Remesa remesa = null;

    this.selectRemesa = valRemesa;
  }
  public String getSelectMovimientoSitp() {
    return this.selectMovimientoSitp;
  }

  public void setSelectMovimientoSitp(String valMovimientoSitp) {
    this.selectMovimientoSitp = valMovimientoSitp;
  }
  public Collection getResult() {
    return this.result;
  }

  public RemesaMovimientoSitpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRemesa()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRemesa.iterator();
    Remesa remesa = null;
    while (iterator.hasNext()) {
      remesa = (Remesa)iterator.next();
      col.add(new SelectItem(
        String.valueOf(remesa.getIdRemesa()), 
        remesa.toString()));
    }
    return col;
  }

  public Collection getColMovimientoSitp()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMovimientoSitp.iterator();
    MovimientoSitp movimientoSitp = null;
    while (iterator.hasNext()) {
      movimientoSitp = (MovimientoSitp)iterator.next();
      col.add(new SelectItem(
        String.valueOf(movimientoSitp.getIdMovimientoSitp()), 
        movimientoSitp.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColRemesa = 
        this.movimientosFacade.findRemesaByEstatus(
        this.login.getOrganismo().getIdOrganismo(), "A", this.login.getIdUsuario());

      this.colRemesa = 
        this.movimientosFacade.findRemesaByEstatus(
        this.login.getOrganismo().getIdOrganismo(), "A", this.login.getIdUsuario());

      this.colMovimientoSitp = 
        this.movimientosFacade.findMovimientoSitpForRemesa(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void refreshMovimientoSitp()
  {
    try {
      this.colMovimientoSitp = 
        this.movimientosFacade.findMovimientoSitpForRemesa(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRemesaMovimientoSitpByRemesa()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.movimientosFacade.findMovimientoSitpByRemesa(Long.valueOf(this.findSelectRemesa).longValue());
      this.showRemesaMovimientoSitpByRemesa = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRemesaMovimientoSitpByRemesa)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRemesa = null;

    return null;
  }

  public boolean isShowRemesaMovimientoSitpByRemesa() {
    return this.showRemesaMovimientoSitpByRemesa;
  }

  public String selectRemesaMovimientoSitp()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRemesa = null;
    this.selectMovimientoSitp = null;

    long idRemesaMovimientoSitp = 
      Long.parseLong((String)requestParameterMap.get("idRemesaMovimientoSitp"));

    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.showRemesaMovimientoSitpByRemesa = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.adding)
      {
        MovimientoSitp movimientoSitp = this.movimientosFacade.findMovimientoSitpById(Long.valueOf(this.selectMovimientoSitp).longValue());
        Remesa remesa = this.movimientosFacade.findRemesaById(Long.valueOf(this.selectRemesa).longValue());

        Trayectoria trayectoria = (Trayectoria)this.expedienteFacade.findTrayectoriaByAnioAndNumeroMovimiento(movimientoSitp.getAnio(), movimientoSitp.getNumeroMovimiento()).iterator().next();

        trayectoria.setNumeroRemesa(String.valueOf(remesa.getNumero()));

        this.expedienteFacade.updateTrayectoria(trayectoria);

        movimientoSitp.setRemesa(remesa);
        movimientoSitp.setEstatus("2");
        this.movimientosFacade.updateMovimientoSitp(movimientoSitp);

        refreshMovimientoSitp();
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', movimientoSitp);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      }

      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }

  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      Map requestParameterMap = 
        context.getExternalContext().getRequestParameterMap();

      long idMovimientoSitp = 
        Long.parseLong((String)requestParameterMap.get("idMovimientoSitp"));

      MovimientoSitp movimientoSitp = this.movimientosFacade.findMovimientoSitpById(idMovimientoSitp);
      movimientoSitp.setRemesa(null);
      movimientoSitp.setEstatus("0");
      this.movimientosFacade.updateMovimientoSitp(movimientoSitp);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', movimientoSitp);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;

    this.selectRemesa = null;

    this.selectMovimientoSitp = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();

    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}