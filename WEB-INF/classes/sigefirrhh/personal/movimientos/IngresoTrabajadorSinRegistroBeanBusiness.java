package sigefirrhh.personal.movimientos;

import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBusiness;
import sigefirrhh.base.definiciones.DefinicionesNoGenBusiness;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.ExpedienteBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.trabajador.ConceptoFijo;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class IngresoTrabajadorSinRegistroBeanBusiness
{
  Logger log = Logger.getLogger(IngresoTrabajadorSinRegistroBeanBusiness.class.getName());

  private TrabajadorNoGenBusiness trabajadorBusiness = new TrabajadorNoGenBusiness();
  private CargoBusiness cargoBusiness = new CargoBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private ExpedienteBusiness expedienteBusiness = new ExpedienteBusiness();
  private DefinicionesNoGenBusiness definicionesBusiness = new DefinicionesNoGenBusiness();
  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();

  public long ingresarTrabajador(long idPersonal, long idTipoPersonal, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long idUsuario, long idDependencia, long idCargo, int codigoNomina, double horas, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    if (fechaPuntoCuenta != null)
    {
      if (fechaPuntoCuenta.compareTo(fechaIngreso) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La fecha de Punto de cuenta no puede ser mayor a la fecha de vigencia");
        throw error;
      }
    }
    this.txn.open();
    PersistenceManager pm = PMThread.getPM();
    long idSueldoPromedio = 0L;

    Personal personal = this.expedienteBusiness.findPersonalById(idPersonal);
    TipoPersonal tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

    CausaMovimiento causaMovimiento = new CausaMovimiento();
    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

    Cargo cargo = new Cargo();
    cargo = this.cargoBusiness.findCargoById(idCargo);

    Dependencia dependencia = new Dependencia();
    dependencia = this.estructuraBusiness.findDependenciaById(idDependencia);
    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());

    Trabajador trabajador = new Trabajador();

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try
    {
      trabajador = this.registrosBusiness.agregarTrabajadorSinRegistro(personal, tipoPersonal, 
        organismo, cargo, 
        causaMovimiento, fechaIngreso, sueldoBasico, codigoNomina, dependencia);
      pm.makePersistent(trabajador);

      sueldoPromedio.setTrabajador(trabajador);
      this.log.error("GRABO TRABAJADOR");
      sueldoPromedio.setIdSueldoPromedio(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.SueldoPromedio"));
      pm.makePersistent(sueldoPromedio);
      idSueldoPromedio = sueldoPromedio.getIdSueldoPromedio();
      this.log.error("GRABO SUELDOPROMEDIO");

      this.registrosBusiness.buscarProximaNomina(trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());

      Collection col = new ArrayList();
      col.addAll(this.registrosBusiness.agregarConceptosFijos(trabajador, null, sueldoBasico, pagarRetroactivo));
      Iterator iter = col.iterator();

      while (iter.hasNext()) {
        ConceptoFijo conceptoFijo = (ConceptoFijo)iter.next();
        pm.makePersistent(conceptoFijo);
      }

      this.log.error("GRABO CONCEPTOFIJO");

      this.txn.close();
      pm.close();
    }
    catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e)
    {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }

    try
    {
      this.txn.open();

      pm = PMThread.getPM();

      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      sueldoPromedio = this.trabajadorBusiness.findSueldoPromedioById(idSueldoPromedio);
      tipoPersonal = this.definicionesBusiness.findTipoPersonalById(idTipoPersonal);

      organismo = this.estructuraBusiness.findOrganismoById(tipoPersonal.getOrganismo().getIdOrganismo());
      causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);
      trabajador = this.trabajadorBusiness.findTrabajadorById(sueldoPromedio.getTrabajador().getIdTrabajador());

      this.calcularSueldosPromedioBeanBusiness.calcularUnTrabajadorParaMovimientos(trabajador, numeroMovimiento);
      this.registrosBusiness.borrarConceptosCero(trabajador.getIdTrabajador());

      this.log.error("RECALCULO CONCEPTOS");

      if (trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) > 0) {
        this.registrosBusiness.fraccionarConceptos(trabajador);
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
        this.log.error("Trabajador Lunes Fraccionados Lunes Primera Quincena " + trabajador.getLunesPrimera());
        this.log.error("Trabajador Lunes Fraccionados Lunes Segunda Quincena " + trabajador.getLunesSegunda());
        this.log.error("FRACCIONO CONCEPTOS");
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) == 0) && (this.registrosBusiness.getDia() == 16)) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) < 0) && (trabajador.getFechaIngreso().getMonth() + 1 == this.registrosBusiness.getMes())) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }

      if ((trabajador.getFechaIngreso().compareTo(this.registrosBusiness.getFechaProximaNomina()) < 0) && 
        (trabajador.getFechaIngreso().getMonth() + 1 != this.registrosBusiness.getMes()) && 
        (this.registrosBusiness.getDia() == 16)) {
        this.registrosBusiness.calcularLunesFraccionados(trabajador);
      }
      if (!trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S"))
      {
        if ((this.registrosBusiness.getFechaProximaNomina().compareTo(trabajador.getFechaIngreso()) > 0) && (pagarRetroactivo.equals("S")))
        {
          Collection colRetroactivo = new ArrayList();

          colRetroactivo.addAll(this.registrosBusiness.calcularRetroactivos(trabajador));
          Iterator iterRetroactivo = colRetroactivo.iterator();
          while (iterRetroactivo.hasNext()) {
            ConceptoVariable conceptoVariable = (ConceptoVariable)iterRetroactivo.next();
            pm.makePersistent(conceptoVariable);
          }
          this.registrosBusiness.calcularLunesRetroactivo(trabajador);
          this.log.error("CALCULO RETROACTIVOS");

          this.log.error("Trabajador Lunes Retroactivo " + trabajador.getLunesRetroactivo());
        }

      }

      Trayectoria trayectoria = new Trayectoria();
      this.log.error("VA A TRAYECTORIA");
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaIngreso, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, codConcurso, observaciones, usuario.getUsuario(), "4");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      RegistroSitp registroSitp = new RegistroSitp();
      registroSitp = this.registrosBusiness.agregarRegistroSitp(trabajador, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaIngreso, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones);

      pm.makePersistent(registroSitp);

      this.log.error("GRABO REGISTROSITP");

      this.txn.close();
    } catch (ErrorSistema a) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e) {
      this.txn.close();
      this.registrosBusiness.reversar(trabajador.getIdTrabajador(), 0L);
      this.log.error("Excepcion controlada:", e);

      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }

    return trabajador.getIdTrabajador();
  }
}