package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.ManualPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CambioCargoSinRegistroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CambioCargoSinRegistroForm.class.getName());
  private String observaciones;
  private int numeroMovimiento;
  private String aumento = "PO";
  private double porcentaje = 10.0D;
  private int paso;
  private String selectIdManualCargo;
  private String selectIdCargo;
  private Cargo cargo;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();

  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private RegistroCargos registroCargos;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colTrabajador;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private boolean showFieldsAux;
  private boolean showButtonAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private Date fechaVigencia;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private Date fechaIngreso;
  private String selectIdRegistroCargos;
  private String pagarRetroactivo = "N";

  private Collection colManualCargo = new ArrayList();
  private Collection colCargo;

  public String getSelectIdManualCargo()
  {
    return this.selectIdManualCargo;
  }
  public void setSelectIdManualCargo(String selectIdManualCargo) {
    this.selectIdManualCargo = selectIdManualCargo;
  }
  public String getSelectIdCargo() {
    return this.selectIdCargo;
  }
  public void setSelectIdCargo(String selectIdCargo) {
    this.selectIdCargo = selectIdCargo;
  }

  public boolean isShowManualCargo() {
    return (this.colManualCargo != null) && (!this.colManualCargo.isEmpty());
  }
  public boolean isShowCargo() {
    return (this.colCargo != null) && (!this.colCargo.isEmpty());
  }
  public boolean isShowFields() {
    return this.showFieldsAux;
  }

  public void changeManualCargo(ValueChangeEvent event)
  {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;

      this.colCargo = this.cargoNoGenFacade.findCargoByManualCargo(idManualCargo);
      manualCargo = this.cargoNoGenFacade.findManualCargoById(idManualCargo);
    }
    catch (Exception e)
    {
      ManualCargo manualCargo;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCargo(ValueChangeEvent event)
  {
    long idCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (idCargo != 0L) {
        this.cargo = this.cargoNoGenFacade.findCargoById(idCargo);
        this.showFieldsAux = true;
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarCampos()
  {
    try
    {
      this.showButtonAux = true;
    }
    catch (Exception e)
    {
      this.nombreDependencia = null;
      this.nombreSede = null;
      this.nombreRegion = null;
      this.descripcionCargo = null;
      this.grado = 0;
      this.sueldo = 0.0D;
      this.numeroMovimiento = 0;

      this.fechaVigencia = null;
      this.showButtonAux = false;
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaVigencia == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }
      long idCausaPersonal = 5L;

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.movimientosNoGenFacade.cambioClasificacionSinRegistro(this.trabajador.getIdTrabajador(), this.cargo.getIdCargo(), idCausaPersonal, this.numeroMovimiento, this.fechaVigencia, this.login.getOrganismo(), this.login.getIdUsuario(), this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }

  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }

  public Collection getResult() {
    return this.result;
  }

  public CambioCargoSinRegistroForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        CambioCargoSinRegistroForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public Collection getFindColTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public Collection getColManualCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colManualCargo, "sigefirrhh.base.cargo.ManualCargo");
  }
  public Collection getColCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colCargo, "sigefirrhh.base.cargo.Cargo");
  }

  public Collection getColTrabajador() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("N", "N", this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String showCambioCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      log.error("paso0.333333333..........");
      resetResult();
      this.show = false;
      log.error("paso0.1..........");
      selectTrabajador();
      this.show = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();
    log.error("paso0..........");
    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      log.error("paso0.0000..........");
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
      log.error("paso0.111111..........");
      log.error("paso1..........");
      Collection colManualPersonal = this.cargoNoGenFacade.findManualPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal());
      log.error("paso2..........");
      Iterator iterManualPersonal = colManualPersonal.iterator();
      log.error("paso3..........");
      while (iterManualPersonal.hasNext()) {
        log.error("paso4..........");
        ManualPersonal manualPersonal = (ManualPersonal)iterManualPersonal.next();
        log.error("paso5..........");
        ManualCargo manualCargo = this.cargoNoGenFacade.findManualCargoById(manualPersonal.getManualCargo().getIdManualCargo());
        log.error("paso5..........");
        this.colManualCargo.add(manualCargo);
        log.error("paso6..........");
      }
      log.error("paso7..........");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.selectedTrabajador = true;
    log.error("selectedTrabajador" + this.selectedTrabajador);
    log.error("show" + this.show);
    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String abort()
  {
    this.selected = false;
    resetResult();

    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaVigencia = null;
    this.sueldo = 0.0D;
    this.numeroMovimiento = 0;
    this.fechaPuntoCuenta = null;
    this.puntoCuenta = "";

    this.observaciones = "";
    return "cancel";
  }

  public boolean isShow() {
    return this.show;
  }

  public boolean isShowData() {
    return (this.show) && (this.selectedTrabajador);
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }

  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public void setDescripcionCargo(String descripcionCargo) {
    this.descripcionCargo = descripcionCargo;
  }
  public int getGrado() {
    return this.grado;
  }
  public void setGrado(int grado) {
    this.grado = grado;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public void setNombreDependencia(String nombreDependencia) {
    this.nombreDependencia = nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public void setNombreRegion(String nombreRegion) {
    this.nombreRegion = nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public void setNombreSede(String nombreSede) {
    this.nombreSede = nombreSede;
  }
  public double getSueldo() {
    return this.sueldo;
  }
  public void setSueldo(double sueldo) {
    this.sueldo = sueldo;
  }

  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public Date getFechaVigencia() {
    return this.fechaVigencia;
  }
  public void setFechaVigencia(Date fechaVigencia) {
    this.fechaVigencia = fechaVigencia;
  }
}