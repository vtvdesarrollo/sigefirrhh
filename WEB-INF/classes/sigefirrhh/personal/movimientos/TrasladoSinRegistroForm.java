package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class TrasladoSinRegistroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TrasladoSinRegistroForm.class.getName());
  private String observaciones;
  private int numeroMovimiento;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private Registro registro;
  private Collection colRegion;
  private Collection colDependencia;
  private String idRegion;
  private String idDependencia;
  private String pagarRetroactivo;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private String selectCausaPersonal;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection findColTipoPersonal;
  private Date fechaMovimiento;
  private boolean showData;
  private boolean showTrabajador;
  private int codigoNomina;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public boolean isShowTrabajador()
  {
    return this.showTrabajador;
  }

  public boolean isShowDependencia()
  {
    return (this.colDependencia != null) && (!this.colDependencia.isEmpty());
  }

  public boolean isShowPosicion()
  {
    return this.registro != null;
  }

  public boolean isShowNuevaDependencia()
  {
    return this.trabajador != null;
  }

  public void changeRegion(ValueChangeEvent event)
  {
    long idRegion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idRegion != 0L) {
        this.colDependencia = this.estructuraFacade.findDependenciaByRegion(idRegion, this.login.getIdOrganismo());
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeDependencia(ValueChangeEvent event) {
    this.idDependencia = 
      ((String)event.getNewValue());
    try
    {
      this.idDependencia.equals("0");
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  private void resetResultTrabajador()
  {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String showTraslado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.show = false;

      selectTrabajador();

      this.show = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajador() {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);

      this.showTrabajador = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.selectedTrabajador = true;

    return null;
  }

  public String ejecutar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      long idCausaMovimiento = 17L;

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.movimientosNoGenFacade.trasladoSinRegistro(this.fechaMovimiento, idCausaMovimiento, this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.trabajador.getIdTrabajador(), Long.valueOf(this.idDependencia).longValue(), this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    this.showTrabajador = false;

    abort();
    return "cancel";
  }

  public Collection getFindColTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public String abort()
  {
    this.selected = false;

    this.numeroMovimiento = 0;
    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";

    this.observaciones = "";
    this.fechaMovimiento = null;

    this.colDependencia = null;

    log.error("abort");
    return "cancel";
  }

  public TrasladoSinRegistroForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegion()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegion, "sigefirrhh.base.estructura.Region");
  }

  public Collection getColDependencia() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colDependencia, "sigefirrhh.base.estructura.Dependencia");
  }

  public void refresh() {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("N", "N", this.login.getIdUsuario(), this.login.getAdministrador());

      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public int getScrollx()
  {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public Date getFechaPuntoCuenta()
  {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }

  public String getObservaciones()
  {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }

  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }

  public String getIdDependencia() {
    return this.idDependencia;
  }
  public void setIdDependencia(String idDependencia) {
    this.idDependencia = idDependencia;
  }

  public int getCodigoNomina()
  {
    return this.codigoNomina;
  }

  public void setCodigoNomina(int codigoNomina)
  {
    this.codigoNomina = codigoNomina;
  }

  public String getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(String idRegion)
  {
    this.idRegion = idRegion;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }
  public void setFindSelectTrabajador(String findSelectTrabajador) {
    this.findSelectTrabajador = findSelectTrabajador;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }

  public void setFindSelectTrabajadorIdTipoPersonal(String findSelectTrabajadorIdTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = findSelectTrabajadorIdTipoPersonal;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public void setFindTrabajadorCedula(int findTrabajadorCedula) {
    this.findTrabajadorCedula = findTrabajadorCedula;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int findTrabajadorCodigoNomina) {
    this.findTrabajadorCodigoNomina = findTrabajadorCodigoNomina;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }

  public void setFindTrabajadorPrimerApellido(String findTrabajadorPrimerApellido) {
    this.findTrabajadorPrimerApellido = findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public void setFindTrabajadorPrimerNombre(String findTrabajadorPrimerNombre) {
    this.findTrabajadorPrimerNombre = findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }

  public void setFindTrabajadorSegundoApellido(String findTrabajadorSegundoApellido) {
    this.findTrabajadorSegundoApellido = findTrabajadorSegundoApellido;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }

  public void setFindTrabajadorSegundoNombre(String findTrabajadorSegundoNombre) {
    this.findTrabajadorSegundoNombre = findTrabajadorSegundoNombre;
  }
  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public void setResultTrabajador(Collection resultTrabajador) {
    this.resultTrabajador = resultTrabajador;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowData() {
    return this.showData;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public void setTrabajador(Trabajador trabajador) {
    this.trabajador = trabajador;
  }
}