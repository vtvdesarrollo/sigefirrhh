package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.base.registro.RegistroPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class MovimientoTrabajadorRegistroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MovimientoTrabajadorRegistroForm.class.getName());
  private String observaciones;
  private boolean egresado;
  private Date fechaIngreso;
  private String remesa;
  private int numeroMovimiento;
  private int valor = 0;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private TipoPersonal tipoPersonal;
  private Collection result;
  private Collection resultTrabajador;
  private String selectProceso = "I";
  private boolean showData1;
  private LoginSession login;
  private boolean error;
  private boolean showPregunta;
  private boolean showBusquedaTodos = false;
  private boolean showBusquedaRegion = false;
  private boolean showRegion;
  private boolean showResult;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showFechaIngreso = true;
  private String seleccion;
  private String idRegion;
  private String idSede;
  private String idDependencia;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();

  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private EstructuraNoGenFacade estructuraFacade = new EstructuraNoGenFacade();
  private Personal personal;
  private Trabajador trabajador;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCedula;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private String findSelectTrabajador;
  private String selectCausaPersonal;
  private Collection colTipoPersonal;
  private Collection colCausaPersonal;
  private Collection colRegistroCargos;
  private Collection colTrabajador;
  private int findPersonalCedula;
  private Collection findColTipoPersonal;
  private String selectIdTipoPersonal;
  private String selectIdCausaPersonal;
  private String selectIdRegistroCargos;
  private RegistroCargos registroCargos;
  private boolean showFieldsAux;
  private boolean showRegistroCargosAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private CausaPersonal causaPersonal;
  private boolean showButtonAux;
  private long idTrabajador;
  private String pagarRetroactivo;
  private String tieneContinuidad;
  private int paso = 1;
  private Date fechaEgresoReal;
  private Date fechaSalidaNomina;
  private String selectTrabajador;
  private boolean selectedTrabajador;
  private Collection colRegion;
  private Collection colSede;
  private Collection colDependencia;
  private boolean show;
  private boolean selected;

  public boolean isShowCausaPersonal()
  {
    return (this.colCausaPersonal != null) && (this.showResultTrabajador);
  }

  public boolean isShowFields() {
    return true;
  }
  public boolean isShowPregunta() {
    return this.showPregunta;
  }

  public boolean isShowBusquedaTodos() {
    return this.showBusquedaTodos;
  }
  public boolean isShowBusquedaRegion() {
    return this.showBusquedaRegion;
  }
  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (!this.colRegistroCargos.isEmpty());
  }
  public boolean isShowSede() {
    return (this.colSede != null) && (!this.colSede.isEmpty());
  }
  public boolean isShowRegion() {
    return (this.colRegion != null) && (!this.colRegion.isEmpty());
  }
  public boolean isShowDependencia() {
    return (this.colDependencia != null) && (!this.colDependencia.isEmpty());
  }

  public void changeRegion(ValueChangeEvent event)
  {
    long idRegion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idRegion != 0L)
      {
        this.colSede = this.estructuraFacade.findSedeByRegion(idRegion, this.login.getIdOrganismo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeSede(ValueChangeEvent event) {
    long idSede = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idSede != 0L)
      {
        this.colDependencia = this.estructuraFacade.findDependenciaBySede(idSede, this.login.getIdOrganismo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeDependencia(ValueChangeEvent event) { this.idDependencia = 
      ((String)event.getNewValue());
    try
    {
      this.colRegistroCargos = new ArrayList();
      if (Long.valueOf(this.idDependencia).longValue() != 0L)
      {
        RegistroPersonal registroPersonal = (RegistroPersonal)this.registroFacade.findRegistroPersonalByTipoPersonal(this.tipoPersonal.getIdTipoPersonal()).iterator().next();
        this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosByRegistroDependenciaAndSituacion(registroPersonal.getRegistro().getIdRegistro(), Long.valueOf(this.idDependencia).longValue(), "V");
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTieneContinuidad(ValueChangeEvent event)
  {
    String tieneContinuidad = 
      (String)event.getNewValue();
    if (tieneContinuidad.equals("S"))
      setShowFechaIngreso(false);
  }

  public String redirectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    try
    {
      externalContext.redirect("/sigefirrhh/sigefirrhh/personal/expediente/Personal.jsf");
      context.responseComplete();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    return "success";
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      long idCausaPersonal = Long.valueOf(
        this.selectIdCausaPersonal).longValue();
      log.error("idCausaPersonal: " + idCausaPersonal);

      CausaPersonal causaPersonal = this.registroNoGenFacade.findCausaPersonalById(idCausaPersonal);

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      log.error("numeroMovimiento: " + this.numeroMovimiento);

      log.error("trabajador: " + this.trabajador);
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(this.fechaIngreso);
      calendar.add(5, -1);
      this.fechaEgresoReal = calendar.getTime();

      this.movimientosNoGenFacade.egresoTrabajadorRegistro(this.trabajador.getIdTrabajador(), this.fechaEgresoReal, this.fechaSalidaNomina, causaPersonal.getCausaMovimiento().getIdCausaMovimiento(), this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());
      log.error("Se procesó el Egreso con éxito");

      setShow(false);
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    try
    {
      if (!this.showButtonAux) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El proceso no se puede ejecutar, mientras no seleccione todos los campos requeridos", ""));
        return null;
      }

      log.error("valor de this.valor: " + this.valor);

      if (this.valor == 3) {
        int valido = this.trabajadorNoGenFacade.validarFechaIngreso(this.trabajador.getPersonal().getIdPersonal(), this.login.getIdOrganismo(), this.fechaIngreso);
        if (valido == 1) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de ingreso debe ser mayor a la última fecha de egreso del trabajador en el organismo", ""));
          return null;
        }
      }
      log.error(this.selectCausaPersonal);
      long idCausaPersonal = Long.valueOf(
        this.selectIdCausaPersonal).longValue();

      CausaPersonal causaPersonal = this.registroNoGenFacade.findCausaPersonalById(idCausaPersonal);

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.idTrabajador = this.movimientosNoGenFacade.ingresoTrabajadorRegistro(this.trabajador.getPersonal().getIdPersonal(), this.tipoPersonal.getIdTipoPersonal(), this.registroCargos.getIdRegistroCargos(), this.fechaIngreso, causaPersonal.getCausaMovimiento().getIdCausaMovimiento(), this.sueldo, this.numeroMovimiento, null, this.login.getIdUsuario(), this.fechaPuntoCuenta, this.puntoCuenta, this.codConcurso, this.pagarRetroactivo, this.observaciones, this.paso);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.personal, this.personal);

      log.error("Se procesó el Ingreso con éxito");
      context.addMessage("success_add", new FacesMessage("Se procesó el Cambio de Tipo de Personal con éxito"));

      if (this.tieneContinuidad.equals("S")) {
        this.movimientosNoGenFacade.registrarContinuidadTrabajadorRegistro(this.idTrabajador, this.trabajador.getIdTrabajador());

        log.error("Se procesó la Continuidad con éxito");
      }

    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
    }

    abort();

    return "cancel";
  }

  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;

    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador);
    }

    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public void changeSeleccion(ValueChangeEvent event)
  {
    this.seleccion = 
      ((String)event.getNewValue());

    this.showBusquedaTodos = false;
    this.showBusquedaRegion = false;
    try
    {
      if (this.seleccion.equals("T"))
        this.showBusquedaTodos = true;
      else if (this.seleccion.equals("R"))
        this.showBusquedaRegion = true;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegistroCargos(ValueChangeEvent event)
  {
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();

    if (idRegistroCargos != 0L) {
      this.showFieldsAux = true;
    }

    try
    {
      this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
      actualizarCampos();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCausaPersonal(ValueChangeEvent event)
  {
    long idCausaPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    log.error("idCausaPersonal: " + idCausaPersonal);
    if (idCausaPersonal != 0L) {
      this.showRegistroCargosAux = true;
    }

    try
    {
      this.causaPersonal = this.registroNoGenFacade.findCausaPersonalById(idCausaPersonal);
      llenarRegistroCargos();
      actualizarCampos();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String showEgresoTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();

      this.show = true;
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }
  public String findPersonalByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.error = false;
      this.personal = 
        ((Personal)this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo()).iterator().next());
      this.showData1 = true;
    } catch (Exception e) {
      this.showData1 = false;
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Los datos personales no estan registrados ", ""));
      this.error = true;
    }

    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
      long idClasificacionPersonal = this.trabajador.getTipoPersonal().getClasificacionPersonal().getIdClasificacionPersonal();

      this.colCausaPersonal = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(idClasificacionPersonal, 5L, "N");
    }
    catch (Exception e) {
      this.showData1 = false;
      log.error("Excepcion controlada:", e);
    }
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;

    this.selectedTrabajador = true;

    return null;
  }
  private void resetResult() {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  private void resetResultTrabajador()
  {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public void actualizarCampos()
  {
    try
    {
      this.showButtonAux = true;

      this.nombreDependencia = this.registroCargos.getDependencia().toString();
      this.nombreSede = this.registroCargos.getSede().toString();
      this.nombreRegion = this.registroCargos.getSede().getRegion().toString();
      this.descripcionCargo = this.registroCargos.getCargo().toString();
      this.grado = this.registroCargos.getCargo().getGrado();

      DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), this.grado, this.registroCargos.getCargo().getSubGrado(), this.paso);
      this.sueldo = 0.0D;
      this.sueldo = detalleTabulador.getMonto();
    }
    catch (Exception e)
    {
      this.nombreDependencia = null;
      this.nombreSede = null;
      this.nombreRegion = null;
      this.descripcionCargo = null;
      this.grado = 0;
      this.numeroMovimiento = 0;
      this.remesa = null;
      this.fechaIngreso = null;
      this.showButtonAux = false;
    }
  }

  public void changeCausaPersonalForTipoPersonal(ValueChangeEvent event)
  {
    FacesContext context = FacesContext.getCurrentInstance();

    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    log.error("idTipoPersonal: " + idTipoPersonal);
    log.error("selectProceso: " + this.selectProceso);
    try
    {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
      if (this.selectProceso.equals("I"))
        this.valor = this.trabajadorNoGenFacade.verificarSiTrabajadorPuedeIngresar(this.trabajador.getPersonal().getIdPersonal(), this.login.getIdOrganismo(), this.tipoPersonal.getIdTipoPersonal());
      else {
        this.valor = this.trabajadorNoGenFacade.verificarSiTrabajadorPuedeReingresar(this.trabajador.getPersonal().getIdPersonal(), this.login.getIdOrganismo(), this.tipoPersonal.getIdTipoPersonal());
      }

      long idClasificacionPersonal = this.tipoPersonal.getClasificacionPersonal().getIdClasificacionPersonal();
      this.colCausaPersonal = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimiento(idClasificacionPersonal, 3L);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void continuarConEgresado()
  {
    this.egresado = false;
    llenarRegistroCargos();
  }

  public void llenarRegistroCargos()
  {
    try {
      long idClasificacionPersonal = this.tipoPersonal.getClasificacionPersonal().getIdClasificacionPersonal();
      this.colCausaPersonal = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimiento(idClasificacionPersonal, 3L);

      RegistroPersonal registroPersonal = (RegistroPersonal)this.registroFacade.findRegistroPersonalByTipoPersonal(this.tipoPersonal.getIdTipoPersonal()).iterator().next();

      this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosByRegistroAndSituacion(registroPersonal.getRegistro().getIdRegistro(), "V");

      this.showRegistroCargosAux = true;
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getColTipoPersonal() { Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext())
    {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (tipoPersonal.getIdTipoPersonal() != this.trabajador.getTipoPersonal().getIdTipoPersonal()) {
        col.add(new SelectItem(
          String.valueOf(tipoPersonal.getIdTipoPersonal()), 
          tipoPersonal.toString()));
      }
    }
    return col; }

  public Collection getColRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroCargos.getIdRegistroCargos()), 
        registroCargos.toString()));
    }

    return col;
  }

  public Collection getColCausaPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonal.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }
    return col;
  }

  public MovimientoTrabajadorRegistroForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.colCausaPersonal = new ArrayList();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        MovimientoTrabajadorRegistroForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("S", "N", this.login.getIdUsuario(), this.login.getAdministrador());

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd(
        "S", "N", this.login.getIdUsuario(), this.login.getAdministrador());
      this.colRegion = this.estructuraFacade.findAllRegion();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getFindColTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public Collection getColTrabajador() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public String abort()
  {
    this.egresado = false;
    this.showData1 = false;
    this.colCausaPersonal = null;
    this.colRegistroCargos = null;
    this.selectIdTipoPersonal = "0";
    this.selectIdRegistroCargos = "0";
    this.selectIdCausaPersonal = "0";
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaIngreso = null;
    this.sueldo = 0.0D;
    this.numeroMovimiento = 0;
    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";
    this.observaciones = "";
    this.seleccion = null;

    return "cancel";
  }

  public boolean isShowData1() {
    return this.showData1;
  }
  public Collection getColRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegion, "sigefirrhh.base.estructura.Region");
  }
  public Collection getColSede() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colSede, "sigefirrhh.base.estructura.Sede");
  }

  public Collection getColDependencia() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colDependencia, "sigefirrhh.base.estructura.Dependencia");
  }
  public boolean isShow() {
    return this.show;
  }

  public void setShow(boolean show) {
    this.show = show;
  }

  public boolean isShowData()
  {
    return (this.show) && (this.selectedTrabajador);
  }

  public boolean isShowResult() {
    return this.showResult;
  }

  public void setShowResult(boolean showResult) {
    this.showResult = showResult;
  }

  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }

  public void setShowResultTrabajador(boolean showResultTrabajador)
  {
    this.showResultTrabajador = showResultTrabajador;
  }

  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }

  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }

  public void setFindTrabajadorCedula(int findTrabajadorCedula) {
    this.findTrabajadorCedula = findTrabajadorCedula;
  }

  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }

  public void setFindTrabajadorCodigoNomina(int findTrabajadorCodigoNomina) {
    this.findTrabajadorCodigoNomina = findTrabajadorCodigoNomina;
  }

  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }

  public void setFindTrabajadorPrimerNombre(String findTrabajadorPrimerNombre) {
    this.findTrabajadorPrimerNombre = findTrabajadorPrimerNombre;
  }

  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }

  public void setFindTrabajadorSegundoNombre(String findTrabajadorSegundoNombre) {
    this.findTrabajadorSegundoNombre = findTrabajadorSegundoNombre;
  }

  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }

  public void setFindTrabajadorPrimerApellido(String findTrabajadorPrimerApellido) {
    this.findTrabajadorPrimerApellido = findTrabajadorPrimerApellido;
  }

  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }

  public void setFindTrabajadorSegundoApellido(String findTrabajadorSegundoApellido)
  {
    this.findTrabajadorSegundoApellido = findTrabajadorSegundoApellido;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }

  public void setResultTrabajador(Collection resultTrabajador) {
    this.resultTrabajador = resultTrabajador;
  }
  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }
  public String getSelectCausaPersonal() {
    return this.selectCausaPersonal;
  }
  public void setSelectCausaPersonal(String string) {
    this.selectCausaPersonal = string;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public void setFindPersonalCedula(int findPersonalCedula) {
    this.findPersonalCedula = findPersonalCedula;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }

  public String getSelectIdCausaPersonal() {
    return this.selectIdCausaPersonal;
  }
  public void setSelectIdCausaPersonal(String selectIdCausaPersonal) {
    this.selectIdCausaPersonal = selectIdCausaPersonal;
  }
  public String getSelectIdRegistroCargos() {
    return this.selectIdRegistroCargos;
  }
  public void setSelectIdRegistroCargos(String string) {
    this.selectIdRegistroCargos = string;
  }
  public RegistroCargos getRegistroCargos() {
    return this.registroCargos;
  }
  public void setRegistroCargos(RegistroCargos cargos) {
    this.registroCargos = cargos;
  }
  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public int getGrado() {
    return this.grado;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public double getSueldo() {
    return this.sueldo;
  }
  public void setDescripcionCargo(String string) {
    this.descripcionCargo = string;
  }
  public void setGrado(int i) {
    this.grado = i;
  }
  public void setNombreDependencia(String string) {
    this.nombreDependencia = string;
  }
  public void setNombreRegion(String string) {
    this.nombreRegion = string;
  }
  public void setNombreSede(String string) {
    this.nombreSede = string;
  }
  public void setSueldo(double d) {
    this.sueldo = d;
  }
  public Date getFechaIngreso() {
    return this.fechaIngreso;
  }
  public void setFechaIngreso(Date date) {
    this.fechaIngreso = date;
  }
  public boolean isEgresado() {
    return this.egresado;
  }
  public void setEgresado(boolean b) {
    this.egresado = b;
  }
  public String getSelectProceso() {
    return this.selectProceso;
  }
  public void setSelectProceso(String selectProceso) {
    this.selectProceso = selectProceso;
  }
  public String getCodConcurso() {
    return this.codConcurso;
  }
  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setCodConcurso(String string) {
    this.codConcurso = string;
  }
  public void setFechaPuntoCuenta(Date date) {
    this.fechaPuntoCuenta = date;
  }
  public void setPuntoCuenta(String string) {
    this.puntoCuenta = string;
  }
  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }
  public void setPagarRetroactivo(String string) {
    this.pagarRetroactivo = string;
  }
  public String getTieneContinuidad() {
    return this.tieneContinuidad;
  }

  public void setTieneContinuidad(String tieneContinuidad) {
    this.tieneContinuidad = tieneContinuidad;
  }

  public boolean isError() {
    return this.error;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public int getPaso() {
    return this.paso;
  }
  public void setPaso(int paso) {
    this.paso = paso;
  }
  public Date getFechaEgresoReal() {
    return this.fechaEgresoReal;
  }

  public void setFechaEgresoReal(Date fechaEgresoReal) {
    this.fechaEgresoReal = fechaEgresoReal;
  }

  public Date getFechaSalidaNomina() {
    return this.fechaSalidaNomina;
  }

  public void setFechaSalidaNomina(Date fechaSalidaNomina) {
    this.fechaSalidaNomina = fechaSalidaNomina;
  }

  public String getSeleccion() {
    return this.seleccion;
  }
  public void setSeleccion(String seleccion) {
    this.seleccion = seleccion;
  }
  public String getIdDependencia() {
    return this.idDependencia;
  }
  public void setIdDependencia(String idDependencia) {
    this.idDependencia = idDependencia;
  }
  public String getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(String idRegion) {
    this.idRegion = idRegion;
  }
  public String getIdSede() {
    return this.idSede;
  }
  public void setIdSede(String idSede) {
    this.idSede = idSede;
  }

  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }

  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }

  public boolean isShowFechaIngreso() {
    return this.showFechaIngreso;
  }

  public void setShowFechaIngreso(boolean showFechaIngreso) {
    this.showFechaIngreso = showFechaIngreso;
  }

  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }
}