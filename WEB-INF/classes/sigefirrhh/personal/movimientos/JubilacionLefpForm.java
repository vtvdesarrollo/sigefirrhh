package sigefirrhh.personal.movimientos;

import eforserver.report.JasperForWeb;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class JubilacionLefpForm extends JubilacionAbstract
{
  static Logger log = Logger.getLogger(JubilacionLefpForm.class.getName());
  private boolean showReport;
  private int reportId;
  private String reportName;
  private long idMovimientoSitp;
  private int numeroMovimiento;
  private long idCausaMovimiento = 23L;

  private JubilacionAux jubilacionAux = new JubilacionAux();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();

  public JubilacionLefpForm() throws Exception
  {
    this.reportName = "formatojub01lefp";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.showReport = false;
    this.idMovimientoSitp = 0L;
  }

  public String validarRequisitos() {
    FacesContext context = FacesContext.getCurrentInstance();
    log.error("validar requisitos 1");
    try {
      show();
      log.error("validar requisito2");
      this.jubilacionAux = getMovimientosNoGenFacade().calcularJubilacion(getTrabajador().getIdTrabajador(), getTrabajador().getTipoPersonal().getIdTipoPersonal(), getTrabajador().getTipoPersonal().getGrupoNomina().getPeriodicidad(), getFechaVigencia(), false);
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
      abort();
    } catch (Exception e) {
      log.error("validar requisitos 3");
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
      log.error("Excepcion controlada:", e);
      abort();
    }

    return "cancel";
  }

  public JubilacionAux getJubilacionAux() {
    return this.jubilacionAux;
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", getLogin().getOrganismo().getNombreOrganismo());
      parameters.put("id_organismo", new Long(getLogin().getOrganismo().getIdOrganismo()));
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(getLogin().getURLLogo()));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");
      parameters.put("id_personal", new Long(getTrabajador().getPersonal().getIdPersonal()));
      parameters.put("id_causa_movimiento", new Long(this.idCausaMovimiento));
      parameters.put("id_movimiento", new Long(this.idMovimientoSitp));

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String ejecutar2()
  {
    log.error("ejecurar 2");
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroMovimientoSitp(getLogin().getIdOrganismo(), new Date().getYear() + 1900) + 1);
      log.error("Actualizo el numeroMovimiento");

      this.idMovimientoSitp = getMovimientosNoGenFacade().jubilacionLefp(
        getTrabajador().getIdTrabajador(), getFechaVigencia(), 
        this.idCausaMovimiento, getNumeroMovimiento(), 
        getLogin().getOrganismo(), getLogin().getIdUsuario(), 
        getFechaPuntoCuenta(), getPuntoCuenta(), getObservaciones(), this.jubilacionAux.getMontoJubilacion(), this.jubilacionAux.getPorcentaje(), this.jubilacionAux.getSueldoPromedio());

      log.error("Actualizo el idMovimientoSitp");
      this.showReport = true;

      registrarAuditoria(context);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      setShow(false);
    }
    catch (ErrorSistema a)
    {
      log.error("Error 1: " + a);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      log.error("Error 2: " + e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }

  public long getIdCausaMovimiento() {
    return this.idCausaMovimiento;
  }

  public void setIdCausaMovimiento(long idCausaMovimiento) {
    this.idCausaMovimiento = idCausaMovimiento;
  }

  public long getIdMovimientoSitp() {
    return this.idMovimientoSitp;
  }

  public void setIdMovimientoSitp(long idMovimientoSitp) {
    this.idMovimientoSitp = idMovimientoSitp;
  }

  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String reportName) {
    this.reportName = reportName;
  }

  public boolean isShowReport() {
    return this.showReport;
  }

  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }
  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
}