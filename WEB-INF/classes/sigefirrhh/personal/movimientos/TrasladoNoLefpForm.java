package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class TrasladoNoLefpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(TrasladoNoLefpForm.class.getName());
  private String observaciones;
  private int numeroMovimiento;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RegistroCargos registroCargos;
  private Registro registro;
  private Collection colRegistro;
  private Collection colRegistroCargos;
  private Collection colRegion;
  private Collection colDependencia;
  private String idRegistroCargos;
  private String idRegistro;
  private String idRegion;
  private String idDependencia;
  private String idManualCargo;
  private String idCargo;
  private String pagarRetroactivo;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private Date fechaMovimiento;
  private boolean showData;
  private boolean showPreguntaClasificacion;
  private boolean showClasificacion;
  private boolean showTrabajador;
  private int codigoNomina = 0;
  private int cedula = 0;

  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public boolean isShowTrabajador()
  {
    return this.showTrabajador;
  }
  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (!this.colRegistroCargos.isEmpty());
  }

  public boolean isShowDependencia() {
    return (this.colDependencia != null) && (!this.colDependencia.isEmpty());
  }

  public boolean isShowClasificacion()
  {
    return this.showClasificacion;
  }
  public boolean isShowPreguntaClasificacion() {
    return this.showPreguntaClasificacion;
  }
  public boolean isShowPosicion() {
    return this.registro != null;
  }
  public boolean isShowNuevaDependencia() {
    return this.registroCargos != null;
  }
  public void siClasificacion() {
    this.showClasificacion = true;
    this.showPreguntaClasificacion = false;
  }
  public void noClasificacion() {
    this.showPreguntaClasificacion = false;
  }

  public void changeRegistro(ValueChangeEvent event)
  {
    long idRegistro = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idRegistro != 0L) {
        this.registro = this.registroNoGenFacade.findRegistroById(idRegistro);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void buscarRegistroCargo() {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.codigoNomina > 0) {
        this.registroCargos = ((RegistroCargos)this.registroCargosFacade.findRegistroCargosByCodigoNominaAndRegistro(this.codigoNomina, this.registro.getIdRegistro()).iterator().next());
      }
      else if (this.cedula > 0) {
        this.registroCargos = ((RegistroCargos)this.registroCargosFacade.findRegistroCargosByCedulaAndRegistro(this.cedula, this.registro.getIdRegistro()).iterator().next());
      }

      if (this.registroCargos.getSituacion().equals("O"))
        this.showTrabajador = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegion(ValueChangeEvent event)
  {
    long idRegion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idRegion != 0L)
      {
        this.colDependencia = this.estructuraFacade.findDependenciaByRegion(idRegion, this.login.getIdOrganismo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeDependencia(ValueChangeEvent event) {
    this.idDependencia = 
      ((String)event.getNewValue());
    try
    {
      if (!this.idDependencia.equals("0"))
      {
        this.showPreguntaClasificacion = true;
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegistroCargos(ValueChangeEvent event)
  {
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.showTrabajador = false;

      if (idRegistroCargos != 0L) {
        this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
        if (this.registroCargos.getSituacion().equals("O")) {
          this.showTrabajador = true;
        }

      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      long idCausaMovimiento = 17L;

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.movimientosNoGenFacade.trasladoNoLefp(this.fechaMovimiento, idCausaMovimiento, this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.registroCargos.getIdRegistroCargos(), Long.valueOf(this.idDependencia).longValue(), this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.registroCargos);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    this.showTrabajador = false;
    abort();
    return "cancel";
  }

  public String abort()
  {
    this.selected = false;
    this.colRegistroCargos = null;

    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";
    this.registroCargos = null;
    this.observaciones = "";
    this.fechaMovimiento = null;

    this.idRegistro = "0";
    this.colDependencia = null;

    log.error("abort");
    return "cancel";
  }

  public TrasladoNoLefpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegistro() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegistro, "sigefirrhh.base.registro.Registro");
  }
  public Collection getColRegistroCargos() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegistroCargos, "sigefirrhh.personal.registroCargos.RegistroCargos");
  }
  public Collection getColRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegion, "sigefirrhh.base.estructura.Region");
  }

  public Collection getColDependencia()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colDependencia, "sigefirrhh.base.estructura.Dependencia");
  }

  public void refresh() {
    try {
      this.colRegistro = 
        this.registroNoGenFacade.findRegistroByAprobacionMpd(this.login.getIdOrganismo(), "N");
      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public int getScrollx()
  {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public Date getFechaPuntoCuenta()
  {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }

  public String getIdRegistro() {
    return this.idRegistro;
  }
  public void setIdRegistro(String idRegistro) {
    this.idRegistro = idRegistro;
  }
  public String getIdRegistroCargos() {
    return this.idRegistroCargos;
  }
  public void setIdRegistroCargos(String idRegistroCargos) {
    this.idRegistroCargos = idRegistroCargos;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public RegistroCargos getRegistroCargos() {
    return this.registroCargos;
  }
  public void setRegistroCargos(RegistroCargos registroCargos) {
    this.registroCargos = registroCargos;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }

  public String getIdDependencia() {
    return this.idDependencia;
  }
  public void setIdDependencia(String idDependencia) {
    this.idDependencia = idDependencia;
  }

  public String getIdCargo() {
    return this.idCargo;
  }
  public void setIdCargo(String idCargo) {
    this.idCargo = idCargo;
  }
  public String getIdManualCargo() {
    return this.idManualCargo;
  }
  public void setIdManualCargo(String idManualCargo) {
    this.idManualCargo = idManualCargo;
  }

  public int getCodigoNomina()
  {
    return this.codigoNomina;
  }

  public void setCodigoNomina(int codigoNomina)
  {
    this.codigoNomina = codigoNomina;
  }
  public int getCedula() {
    return this.cedula;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public String getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(String idRegion)
  {
    this.idRegion = idRegion;
  }
}