package sigefirrhh.personal.movimientos;

public class JubilacionAux
{
  private double SueldoPromedio;
  private double montoJubilacion;
  private double porcentaje;
  private double porcentajejub;
  private int aniosapn;
  private int mesesapn;
  private int diasapn;
  private int aniosorganismo;
  private int mesesorganismo;
  private int diasorganismo;
  private int aniost;
  private int mesest;
  private int diast;

  public int getAniosapn()
  {
    return this.aniosapn;
  }
  public int getAniost() {
    return this.aniost;
  }
  public void setAniost(int aniost) {
    this.aniost = aniost;
  }
  public int getDiast() {
    return this.diast;
  }
  public void setDiast(int diast) {
    this.diast = diast;
  }
  public int getMesest() {
    return this.mesest;
  }
  public void setMesest(int mesest) {
    this.mesest = mesest;
  }
  public double getPorcentajejub() {
    return this.porcentajejub;
  }
  public void setPorcentajejub(double porcentajejub) {
    this.porcentajejub = porcentajejub;
  }
  public void setAniosapn(int aniosapn) {
    this.aniosapn = aniosapn;
  }
  public int getAniosorganismo() {
    return this.aniosorganismo;
  }
  public void setAniosorganismo(int aniosorganismo) {
    this.aniosorganismo = aniosorganismo;
  }
  public int getDiasapn() {
    return this.diasapn;
  }
  public void setDiasapn(int diasapn) {
    this.diasapn = diasapn;
  }
  public int getMesesapn() {
    return this.mesesapn;
  }
  public void setMesesapn(int mesesapn) {
    this.mesesapn = mesesapn;
  }
  public int getMesesorganismo() {
    return this.mesesorganismo;
  }
  public void setMesesorganismo(int mesesorganismo) {
    this.mesesorganismo = mesesorganismo;
  }
  public double getMontoJubilacion() {
    return this.montoJubilacion;
  }
  public void setMontoJubilacion(double montoJubilacion) {
    this.montoJubilacion = montoJubilacion;
  }
  public double getSueldoPromedio() {
    return this.SueldoPromedio;
  }
  public void setSueldoPromedio(double sueldoPromedio) {
    this.SueldoPromedio = sueldoPromedio;
  }
  public double getPorcentaje() {
    return this.porcentaje;
  }
  public void setPorcentaje(double porcentaje) {
    this.porcentaje = porcentaje;
  }

  public int getDiasorganismo()
  {
    return this.diasorganismo;
  }
  public void setDiasorganismo(int diasorganismo) {
    this.diasorganismo = diasorganismo;
  }
}