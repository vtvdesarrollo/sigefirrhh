package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CambioClasificacionLefpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CambioClasificacionLefpForm.class.getName());
  private String observaciones;
  private int numeroMovimiento;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private RegistroCargos registroCargos;
  private Registro registro;
  private Collection colRegistro;
  private Collection colRegistroCargos;
  private Collection colCargo;
  private Collection colManualCargo;
  private String idRegistroCargos;
  private String idRegistro;
  private String idManualCargo;
  private String idCargo;
  private String pagarRetroactivo = "S";
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private Cargo cargo;
  private Date fechaMovimiento;
  private boolean showData;
  private boolean showReport;
  private int reportId;
  private String reportName;
  private long idMovimientoSitp;
  private long idCausaPersonal = 9L;
  private boolean showTrabajador;
  private double sueldo = 0.0D;

  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public boolean isShowTrabajador()
  {
    return this.showTrabajador;
  }
  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (!this.colRegistroCargos.isEmpty());
  }
  public boolean isShowCargo() {
    return (this.colCargo != null) && (!this.colCargo.isEmpty());
  }

  public void changeRegistro(ValueChangeEvent event)
  {
    long idRegistro = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idRegistro != 0L) {
        this.registro = this.registroNoGenFacade.findRegistroById(idRegistro);
        this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosByRegistroAndTipo12(this.registro.getIdRegistro());
        this.fechaMovimiento = this.registro.getFechaVigencia();
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeRegistroCargos(ValueChangeEvent event)
  {
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.showTrabajador = false;
      if (idRegistroCargos != 0L) {
        this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
        if (this.registroCargos.getSituacion().equals("O")) {
          this.showTrabajador = true;
        }
      }

      log.error("changeRegistroCargos Realizado");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeManualCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idManualCargo != 0L)
        this.colCargo = this.cargoNoGenFacade.findCargoByManualCargo(idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCargo(ValueChangeEvent event) { long idCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idCargo != 0L) {
        this.cargo = this.cargoNoGenFacade.findCargoById(idCargo);
        DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.cargo.getManualCargo().getTabulador().getIdTabulador(), this.cargo.getGrado(), this.cargo.getSubGrado(), 1);
        this.sueldo = detalleTabulador.getMonto();

        log.error("sueldo: " + this.sueldo);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", getLogin().getOrganismo().getNombreOrganismo());
      parameters.put("id_organismo", new Long(getLogin().getOrganismo().getIdOrganismo()));
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(getLogin().getURLLogo()));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");
      parameters.put("id_personal", new Long(this.registroCargos.getTrabajador().getPersonal().getIdPersonal()));
      parameters.put("id_causa_movimiento", new Long(this.idCausaPersonal));
      parameters.put("id_movimiento", new Long(this.idMovimientoSitp));

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaMovimiento == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }

      if (this.registro.getFechaVigencia().compareTo(this.fechaMovimiento) > 0) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de vigencia del movimiento no puede ser menor a la fecha de vigencia del registro", ""));
        return null;
      }

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroMovimientoSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.idMovimientoSitp = this.movimientosNoGenFacade.clasificacionLefp(this.fechaMovimiento, this.idCausaPersonal, this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.registroCargos.getIdRegistroCargos(), this.cargo.getIdCargo(), this.sueldo, this.observaciones, this.pagarRetroactivo, this.cargo.getCodCargo());
      this.showReport = true;

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.registroCargos);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;

      this.showTrabajador = false;
      abort();
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }

  public String abort()
  {
    this.selected = false;
    this.colRegistroCargos = null;

    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";

    this.observaciones = "";
    this.fechaMovimiento = null;
    this.idManualCargo = "0";
    this.idRegistro = "0";
    this.colCargo = null;
    this.sueldo = 0.0D;

    return "cancel";
  }

  public CambioClasificacionLefpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.reportName = "formato03lefp";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.showReport = false;
    this.idMovimientoSitp = 0L;
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegistro() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegistro, "sigefirrhh.base.registro.Registro");
  }
  public Collection getColRegistroCargos() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegistroCargos, "sigefirrhh.personal.registroCargos.RegistroCargos");
  }
  public Collection getColManualCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colManualCargo, "sigefirrhh.base.cargo.ManualCargo");
  }
  public Collection getColCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colCargo, "sigefirrhh.base.cargo.Cargo");
  }

  public void refresh()
  {
    try {
      this.colRegistro = 
        this.registroNoGenFacade.findRegistroByAprobacionMpd(this.login.getIdOrganismo(), "S");
      this.colManualCargo = 
        this.cargoNoGenFacade.findManualCargoByTipoCargo("TA", this.login.getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public int getScrollx()
  {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public Date getFechaPuntoCuenta()
  {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }

  public String getIdCargo() {
    return this.idCargo;
  }
  public void setIdCargo(String idCargo) {
    this.idCargo = idCargo;
  }
  public String getIdManualCargo() {
    return this.idManualCargo;
  }
  public void setIdManualCargo(String idManualCargo) {
    this.idManualCargo = idManualCargo;
  }
  public String getIdRegistro() {
    return this.idRegistro;
  }
  public void setIdRegistro(String idRegistro) {
    this.idRegistro = idRegistro;
  }
  public String getIdRegistroCargos() {
    return this.idRegistroCargos;
  }
  public void setIdRegistroCargos(String idRegistroCargos) {
    this.idRegistroCargos = idRegistroCargos;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public RegistroCargos getRegistroCargos() {
    return this.registroCargos;
  }
  public void setRegistroCargos(RegistroCargos registroCargos) {
    this.registroCargos = registroCargos;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
  public boolean isShowReport() {
    return this.showReport;
  }
  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }
}