package sigefirrhh.personal.movimientos;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.Collection;
import java.util.Date;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.sistema.Usuario;

public class MovimientosNoGenBusiness extends MovimientosBusiness
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  private EgresoTrabajadorLefpBeanBusiness egresoTrabajadorLefpBeanBusiness = new EgresoTrabajadorLefpBeanBusiness();

  private EgresoTrabajadorRegistroBeanBusiness egresoTrabajadorRegistroBeanBusiness = new EgresoTrabajadorRegistroBeanBusiness();

  private EgresoTrabajadorSinRegistroBeanBusiness egresoTrabajadorSinRegistroBeanBusiness = new EgresoTrabajadorSinRegistroBeanBusiness();

  private CambioClasificacionSinRegistroBeanBusiness cambioClasificacionSinRegistroBeanBusiness = new CambioClasificacionSinRegistroBeanBusiness();

  private CambioCedulaBeanBusiness cambioCedulaBeanBusiness = new CambioCedulaBeanBusiness();

  private IngresoTrabajadorLefpBeanBusiness ingresoTrabajadorLefpBeanBusiness = new IngresoTrabajadorLefpBeanBusiness();

  private ReingresoTrabajadorLefpBeanBusiness reingresoTrabajadorLefpBeanBusiness = new ReingresoTrabajadorLefpBeanBusiness();

  private IngresoTrabajadorSinRegistroBeanBusiness ingresoTrabajadorSinRegistroBeanBusiness = new IngresoTrabajadorSinRegistroBeanBusiness();

  private IngresoTrabajadorRegistroBeanBusiness ingresoTrabajadorRegistroBeanBusiness = new IngresoTrabajadorRegistroBeanBusiness();

  private JubilacionTrabajadorSinRegistroBeanBusiness jubilacionTrabajadorSinRegistroBeanBusiness = new JubilacionTrabajadorSinRegistroBeanBusiness();

  private RemesaNoGenBeanBusiness remesaNoGenBeanBusiness = new RemesaNoGenBeanBusiness();

  private RegistroSitpNoGenBeanBusiness registroSitpNoGenBeanBusiness = new RegistroSitpNoGenBeanBusiness();

  private MovimientoSitpNoGenBeanBusiness movimientoSitpNoGenBeanBusiness = new MovimientoSitpNoGenBeanBusiness();

  private CambioDesignacionAscensoLefpBeanBusiness cambioDesignacionAscensoLefpBeanBusiness = new CambioDesignacionAscensoLefpBeanBusiness();

  private CambioAscensoNoLefpBeanBusiness cambioAscensoNoLefpBeanBusiness = new CambioAscensoNoLefpBeanBusiness();

  private AumentoSueldoLefpBeanBusiness aumentoSueldoLefpBeanBusiness = new AumentoSueldoLefpBeanBusiness();

  private LicenciaSuspensionLefpBeanBusiness licenciaSuspensionLefpBeanBusiness = new LicenciaSuspensionLefpBeanBusiness();

  private AumentoSueldoConSinRegistroBeanBusiness aumentoSueldoConSinRegistroBeanBusiness = new AumentoSueldoConSinRegistroBeanBusiness();

  private LicenciaSuspensionConSinRegistroBeanBusiness licenciaSuspensionConSinRegistroBeanBusiness = new LicenciaSuspensionConSinRegistroBeanBusiness();

  private ClasificacionLefpBeanBusiness clasificacionLefpBeanBusiness = new ClasificacionLefpBeanBusiness();

  private ClasificacionNoLefpBeanBusiness clasificacionNoLefpBeanBusiness = new ClasificacionNoLefpBeanBusiness();

  private ReincorporacionLefpBeanBusiness reincorporacionLefpBeanBusiness = new ReincorporacionLefpBeanBusiness();

  private TrasladoLefpBeanBusiness trasladoLefpBeanBusiness = new TrasladoLefpBeanBusiness();

  private TrasladoNoLefpBeanBusiness trasladoNoLefpBeanBusiness = new TrasladoNoLefpBeanBusiness();

  private TrasladoMutuoNoLefpBeanBusiness trasladoMutuoNoLefpBeanBusiness = new TrasladoMutuoNoLefpBeanBusiness();

  private TrasladoSinRegistroBeanBusiness trasladoSinRegistroBeanBusiness = new TrasladoSinRegistroBeanBusiness();

  private JubilacionLefpBeanBusiness jubilacionLefpBeanBusiness = new JubilacionLefpBeanBusiness();

  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();

  public long egresoTrabajadorLefp(long idTrabajador, Date fechaEgresoReal, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, String observaciones) throws Exception
  {
    return this.egresoTrabajadorLefpBeanBusiness.egresarTrabajador(idTrabajador, fechaEgresoReal, fechaSalidaNomina, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, observaciones);
  }

  public boolean egresoTrabajadorRegistro(long idTrabajador, Date fechaEgresoReal, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, String observaciones) throws Exception {
    return this.egresoTrabajadorRegistroBeanBusiness.egresarTrabajador(idTrabajador, fechaEgresoReal, fechaSalidaNomina, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, observaciones);
  }

  public boolean egresoTrabajadorSinRegistro(long idTrabajador, Date fechaEgresoReal, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, String observaciones) throws Exception {
    return this.egresoTrabajadorSinRegistroBeanBusiness.egresarTrabajador(idTrabajador, fechaEgresoReal, fechaSalidaNomina, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, observaciones);
  }

  public boolean cambioClasificacionSinRegistro(long idTrabajador, long idCargo, long idCausaMovimiento, int numeroMovimiento, Date fechaVigencia, Organismo organismo, long idUsuario, String observaciones) throws Exception
  {
    return this.cambioClasificacionSinRegistroBeanBusiness.actualizar(idTrabajador, idCargo, idCausaMovimiento, numeroMovimiento, fechaVigencia, organismo, idUsuario, observaciones);
  }

  public boolean cambioCedula(long idTrabajador, int cedula, Organismo organismo, long idUsuario, String observaciones)
    throws Exception
  {
    return this.cambioCedulaBeanBusiness.actualizar(idTrabajador, cedula, organismo, idUsuario, observaciones);
  }

  public long ingresoTrabajadorLefp(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long usuario, Date fechaPuntoCuenta, String puntoCuenta, long idConcurso, String pagarRetroactivo, String observaciones) throws Exception
  {
    return this.ingresoTrabajadorLefpBeanBusiness.ingresarTrabajador(idPersonal, idTipoPersonal, idRegistroCargos, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, usuario, fechaPuntoCuenta, puntoCuenta, idConcurso, pagarRetroactivo, observaciones);
  }

  public long ingresoTrabajadorRegistro(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long usuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones, int paso) throws Exception {
    return this.ingresoTrabajadorRegistroBeanBusiness.ingresarTrabajador(idPersonal, idTipoPersonal, idRegistroCargos, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, pagarRetroactivo, observaciones, paso);
  }

  public long registrarContinuidadTrabajadorRegistro(long idTrabajador, long idTrabajadorEgresado) throws Exception {
    try {
      this.txn.open();
      return this.ingresoTrabajadorRegistroBeanBusiness.registrarContinuidadTrabajadorRegistro(idTrabajador, idTrabajadorEgresado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public long reingresoTrabajadorLefp(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long usuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones) throws Exception {
    return this.reingresoTrabajadorLefpBeanBusiness.ingresarTrabajador(idPersonal, idTipoPersonal, idRegistroCargos, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, pagarRetroactivo, observaciones);
  }

  public long ingresoTrabajadorSinRegistro(long idPersonal, long idTipoPersonal, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long idUsuario, long idDependencia, long idCargo, int codigoNomina, double horas, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones) throws Exception {
    return this.ingresoTrabajadorSinRegistroBeanBusiness.ingresarTrabajador(idPersonal, idTipoPersonal, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, idUsuario, idDependencia, idCargo, codigoNomina, horas, fechaPuntoCuenta, puntoCuenta, codConcurso, pagarRetroactivo, observaciones);
  }

  public boolean jubilacionTrabajadorSinRegistro(long idPersonal, long idTipoPersonal, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, Usuario usuario, long idDependencia, long idCargo, int codigoNomina, double porcentajeJubilacion) throws Exception {
    return this.jubilacionTrabajadorSinRegistroBeanBusiness.ingresarTrabajador(idPersonal, idTipoPersonal, fechaIngreso, idCausaMovimiento, sueldoBasico, usuario, idDependencia, idCargo, codigoNomina, porcentajeJubilacion);
  }

  public int findLastNumeroRemesa(long idOrganismo, int anio) throws Exception
  {
    return this.remesaNoGenBeanBusiness.findLastNumero(idOrganismo, anio);
  }

  public Collection findRemesaByEstatus(long idOrganismo, String estatus, long idUsuario) throws Exception
  {
    return this.remesaNoGenBeanBusiness.findByEstatus(idOrganismo, estatus, idUsuario);
  }

  public Collection findMovimientoSitpForRemesa(long idOrganismo) throws Exception
  {
    return this.movimientoSitpNoGenBeanBusiness.findForRemesa(idOrganismo);
  }

  public Collection findMovimientoSitpByRemesa(long idRemesa)
    throws Exception
  {
    return this.movimientoSitpNoGenBeanBusiness.findByRemesa(idRemesa);
  }

  public Collection findMovimientoSitpByRemesaAndStatus(long idRemesa, String status) throws Exception
  {
    return this.movimientoSitpNoGenBeanBusiness.findByRemesaAndStatus(idRemesa, status);
  }

  public int findLastNumeroMovimientoSitp(long idOrganismo, int anio)
    throws Exception
  {
    return this.movimientoSitpNoGenBeanBusiness.findLastNumero(idOrganismo, anio);
  }

  public int findLastNumeroRegistroSitp(long idOrganismo, int anio) throws Exception
  {
    return this.registroSitpNoGenBeanBusiness.findLastNumero(idOrganismo, anio);
  }

  public long cambioDesignacionAscensoLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, double sueldo, long idRegistroCargos, Date fechaPuntoCuenta, String puntoCuenta, long idConcursoCargo, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones) throws Exception {
    return this.cambioDesignacionAscensoLefpBeanBusiness.actualizar(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, sueldo, idRegistroCargos, fechaPuntoCuenta, puntoCuenta, idConcursoCargo, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public long aumentoSueldoLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones) throws Exception {
    return this.aumentoSueldoLefpBeanBusiness.actualizar(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, codConcurso, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public long licenciaSuspensionLefp(long idTrabajador, Date fechaMovimiento, Date fechaCulminacion, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones) throws Exception {
    return this.licenciaSuspensionLefpBeanBusiness.actualizar(idTrabajador, fechaMovimiento, fechaCulminacion, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, observaciones);
  }

  public boolean aumentoSueldoConSinRegistro(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones) throws Exception
  {
    return this.aumentoSueldoConSinRegistroBeanBusiness.actualizar(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, codConcurso, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public boolean licenciaSuspensionConSinRegistro(long idTrabajador, Date fechaMovimiento, Date fechaCulminacion, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones) throws Exception
  {
    return this.licenciaSuspensionConSinRegistroBeanBusiness.actualizar(idTrabajador, fechaMovimiento, fechaCulminacion, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, observaciones);
  }

  public boolean cambioAscensoNoLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, double sueldo, long idRegistroCargos, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones) throws Exception {
    return this.cambioAscensoNoLefpBeanBusiness.actualizar(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, sueldo, idRegistroCargos, fechaPuntoCuenta, puntoCuenta, codConcurso, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public long clasificacionLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idCargo, double sueldo, String observaciones, String pagarRetroactivo, String CodCargo) throws Exception {
    return this.clasificacionLefpBeanBusiness.actualizar(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idCargo, sueldo, observaciones, pagarRetroactivo, CodCargo);
  }

  public boolean clasificacionNoLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idCargo, double sueldo, String observaciones, String pagarRetroactivo) throws Exception {
    return this.clasificacionNoLefpBeanBusiness.actualizar(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idCargo, sueldo, observaciones, pagarRetroactivo);
  }

  public long reincorporacionLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones) throws Exception {
    return this.reincorporacionLefpBeanBusiness.actualizar(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, observaciones);
  }

  public long trasladoLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idDependencia, String observaciones) throws Exception
  {
    return this.trasladoLefpBeanBusiness.actualizar(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idDependencia, observaciones);
  }

  public long jubilacionLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, double montoJubilacion, double porcentajeJubilacion, double sueldoPromedioJubilacion)
    throws Exception
  {
    return this.jubilacionLefpBeanBusiness.actualizar(idTrabajador, fechaMovimiento, 
      idCausaMovimiento, numeroMovimiento, 
      organismo, idUsuario, fechaPuntoCuenta, 
      puntoCuenta, observaciones, 
      montoJubilacion, porcentajeJubilacion, sueldoPromedioJubilacion);
  }

  public long pensionLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, double montoJubilacion, double porcentajeJubilacion, double sueldoPromedioJubilacion)
    throws Exception
  {
    return this.jubilacionLefpBeanBusiness.ajustar_pension(idTrabajador, fechaMovimiento, 
      idCausaMovimiento, numeroMovimiento, 
      organismo, idUsuario, fechaPuntoCuenta, 
      puntoCuenta, observaciones, 
      montoJubilacion, porcentajeJubilacion, sueldoPromedioJubilacion);
  }

  public boolean trasladoNoLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idDependencia, String observaciones) throws Exception
  {
    return this.trasladoNoLefpBeanBusiness.actualizar(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idDependencia, observaciones);
  }

  public int trasladoMutuoNoLefp(long idRegistroCargos1, long idRegistroCargos2, Date fechaMovimiento, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, long idOrganismo, long idUsuario)
    throws Exception
  {
    return this.trasladoMutuoNoLefpBeanBusiness.trasladoMutuoNoLefp(idRegistroCargos1, idRegistroCargos2, fechaMovimiento, fechaPuntoCuenta, puntoCuenta, observaciones, idOrganismo, idUsuario);
  }

  public boolean trasladoSinRegistro(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idTrabajador, long idDependencia, String observaciones) throws Exception
  {
    return this.trasladoSinRegistroBeanBusiness.actualizar(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idTrabajador, idDependencia, observaciones);
  }
  public void enviarRemesa(Remesa remesa) throws Exception {
    this.remesaNoGenBeanBusiness.enviarRemesa(remesa);
  }
  public void recibirRemesa(long idOrganismo) throws Exception {
    this.remesaNoGenBeanBusiness.recibirRemesa(idOrganismo);
  }
  public JubilacionAux calcularJubilacion(long idTrabajador, long idTipoPersonal, String periodicidad, Date fechaVigencia, boolean especial) throws Exception {
    return this.registrosBusiness.calcularJubilacion(idTrabajador, idTipoPersonal, periodicidad, fechaVigencia, especial);
  }
}