package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.ManualPersonal;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.base.registro.RegistroPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ClasificacionNoLefpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ClasificacionNoLefpForm.class.getName());
  private String observaciones;
  private int numeroMovimiento;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private RegistroCargos registroCargos;
  private Registro registro;
  private Collection colRegistro;
  private Collection colRegistroCargos;
  private Collection colCargo;
  private Collection colManualCargo;
  private String idRegistroCargos;
  private String idRegistro;
  private String idManualCargo;
  private String idCargo;
  private String pagarRetroactivo = "N";
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private Cargo cargo;
  private Date fechaMovimiento;
  private boolean showData;
  private boolean showTrabajador;
  private boolean showObservaciones;
  private int codigoNomina;
  private double sueldoBasico;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public boolean isShowTrabajador()
  {
    return this.showTrabajador;
  }
  public boolean isShowObservaciones() {
    return this.showObservaciones;
  }
  public void setShowObservaciones(boolean showObservaciones) {
    this.showObservaciones = showObservaciones;
  }
  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (!this.colRegistroCargos.isEmpty());
  }
  public boolean isShowCargo() {
    return (this.colCargo != null) && (!this.colCargo.isEmpty());
  }

  public boolean isShowManualCargo() {
    return (this.colManualCargo != null) && (!this.colManualCargo.isEmpty());
  }

  public void changeRegistro(ValueChangeEvent event) {
    long idRegistro = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idRegistro != 0L) {
        this.registro = this.registroNoGenFacade.findRegistroById(idRegistro);

        this.colManualCargo = new ArrayList();
        Collection colRegistroPersonal = new ArrayList();
        ManualPersonal manualPersonal = new ManualPersonal();

        colRegistroPersonal = this.registroNoGenFacade.findRegistroPersonalByRegistro(this.registro.getIdRegistro());
        Iterator iterRegistroPersonal = colRegistroPersonal.iterator();
        Collection colManualPersonal = new ArrayList();
        Iterator iterManualPersonal;
        for (; iterRegistroPersonal.hasNext(); 
          iterManualPersonal.hasNext())
        {
          RegistroPersonal registroPersonal = (RegistroPersonal)iterRegistroPersonal.next();
          colManualPersonal = this.cargoNoGenFacade.findManualPersonalByTipoPersonal(registroPersonal.getTipoPersonal().getIdTipoPersonal());
          iterManualPersonal = colManualPersonal.iterator();
          continue;
          manualPersonal = (ManualPersonal)iterManualPersonal.next();
          this.colManualCargo.add(this.cargoNoGenFacade.findManualCargoById(manualPersonal.getManualCargo().getIdManualCargo()));
        }
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void buscarRegistroCargo() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      log.error("codigo_nomina............." + this.codigoNomina);
      this.registroCargos = ((RegistroCargos)this.registroCargosFacade.findRegistroCargosByCodigoNominaAndRegistro(this.codigoNomina, this.registro.getIdRegistro()).iterator().next());
      log.error("registro_cargos............." + this.registroCargos);
      if (this.registroCargos.getSituacion().equals("O"))
        this.showTrabajador = true;
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir un puesto de trabajo", ""));
    }
  }

  public void changeRegistroCargos(ValueChangeEvent event) {
    FacesContext context = FacesContext.getCurrentInstance();
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.showTrabajador = false;
      if (idRegistroCargos != 0L) {
        this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
        if (this.registroCargos.getSituacion().equals("O")) {
          this.showTrabajador = true;
        }
      }

    }
    catch (Exception e)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir un puesto de trabajo", ""));
    }
  }

  public void changeManualCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idManualCargo != 0L) {
        this.colCargo = this.cargoNoGenFacade.findCargoByManualCargo(idManualCargo);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCargo(ValueChangeEvent event) { long idCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      if (idCargo != 0L) {
        this.cargo = this.cargoNoGenFacade.findCargoById(idCargo);
        this.sueldoBasico = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.cargo.getManualCargo().getTabulador().getIdTabulador(), this.cargo.getGrado(), 1, 1).getMonto();
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaMovimiento == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }
      long idCausaPersonal = 8L;

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.movimientosNoGenFacade.clasificacionNoLefp(this.fechaMovimiento, idCausaPersonal, this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.registroCargos.getIdRegistroCargos(), this.cargo.getIdCargo(), this.sueldoBasico, this.observaciones, this.pagarRetroactivo);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.registroCargos);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    this.showTrabajador = false;
    abort();
    return "cancel";
  }

  public String abort()
  {
    this.selected = false;
    this.colRegistroCargos = null;

    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";
    this.registroCargos = null;
    this.observaciones = "";
    this.fechaMovimiento = null;
    this.idManualCargo = "0";
    this.idRegistro = "0";
    this.colCargo = null;

    return "cancel";
  }

  public ClasificacionNoLefpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegistro() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegistro, "sigefirrhh.base.registro.Registro");
  }
  public Collection getColRegistroCargos() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colRegistroCargos, "sigefirrhh.personal.registroCargos.RegistroCargos");
  }
  public Collection getColManualCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colManualCargo, "sigefirrhh.base.cargo.ManualCargo");
  }
  public Collection getColCargo() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.colCargo, "sigefirrhh.base.cargo.Cargo");
  }

  public void refresh()
  {
    try {
      this.colRegistro = 
        this.registroNoGenFacade.findRegistroByAprobacionMpd(this.login.getIdOrganismo(), "N");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public int getScrollx()
  {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }

  public Date getFechaPuntoCuenta()
  {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }

  public String getIdCargo() {
    return this.idCargo;
  }
  public void setIdCargo(String idCargo) {
    this.idCargo = idCargo;
  }
  public String getIdManualCargo() {
    return this.idManualCargo;
  }
  public void setIdManualCargo(String idManualCargo) {
    this.idManualCargo = idManualCargo;
  }
  public String getIdRegistro() {
    return this.idRegistro;
  }
  public void setIdRegistro(String idRegistro) {
    this.idRegistro = idRegistro;
  }
  public String getIdRegistroCargos() {
    return this.idRegistroCargos;
  }
  public void setIdRegistroCargos(String idRegistroCargos) {
    this.idRegistroCargos = idRegistroCargos;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public RegistroCargos getRegistroCargos() {
    return this.registroCargos;
  }
  public void setRegistroCargos(RegistroCargos registroCargos) {
    this.registroCargos = registroCargos;
  }
  public Date getFechaMovimiento() {
    return this.fechaMovimiento;
  }
  public void setFechaMovimiento(Date fechaMovimiento) {
    this.fechaMovimiento = fechaMovimiento;
  }
  public int getCodigoNomina() {
    return this.codigoNomina;
  }
  public void setCodigoNomina(int codigoNomina) {
    this.codigoNomina = codigoNomina;
  }
}