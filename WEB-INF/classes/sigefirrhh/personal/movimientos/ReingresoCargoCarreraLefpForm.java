package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.RelacionPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.base.registro.RegistroPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.planificacion.seleccion.ConcursoCargo;
import sigefirrhh.planificacion.seleccion.SeleccionNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ReingresoCargoCarreraLefpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReingresoCargoCarreraLefpForm.class.getName());
  private String observaciones;
  private boolean egresado;
  private Date fechaIngreso;
  private String remesa;
  private int numeroMovimiento;
  private int valor = 0;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private TipoPersonal tipoPersonal;
  private Collection result;
  private String selectProceso = "R";
  private boolean showData1;
  private LoginSession login;
  private boolean error;
  private boolean showFechaEgreso;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();

  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private SeleccionNoGenFacade seleccionFacade = new SeleccionNoGenFacade();
  private Personal personal;
  private String findSelectTrabajadorIdTipoPersonal;
  private String selectCausaPersonal;
  private Collection colTipoPersonal;
  private Collection colCausaPersonal;
  private Collection colRegistroCargos;
  private int findPersonalCedula;
  private String selectIdTipoPersonal;
  private String selectIdCausaPersonal;
  private String selectIdRegistroCargos;
  private RegistroCargos registroCargos;
  private boolean showFieldsAux;
  private boolean showRegistroCargosAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private CausaPersonal causaPersonal;
  private boolean showButtonAux;
  private String pagarRetroactivo;
  private long idMovimientoSitp;
  private Date fechaEgreso;
  private boolean mostrarRegistroCargos;
  private long idCausaPersonal;
  private boolean showReport = false;
  private int reportId;
  private String reportName;
  private ConcursoCargo concursoCargo = new ConcursoCargo();

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String reportName) {
    this.reportName = reportName;
  }

  public boolean isShowReport() {
    return this.showReport;
  }

  public void setShowReport(boolean showReport) {
    this.showReport = showReport;
  }

  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (!this.colRegistroCargos.isEmpty());
  }

  public boolean isShowCausaPersonal() {
    return (this.colCausaPersonal != null) && (this.showData1);
  }

  public boolean isShowFields() {
    return (this.showData1) && (this.showFieldsAux);
  }

  public String redirectPersonal() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    try
    {
      externalContext.redirect("/sigefirrhh/sigefirrhh/personal/expediente/Personal.jsf");
      context.responseComplete();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    return "success";
  }

  public String ejecutar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaIngreso == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }
      if (this.fechaPuntoCuenta == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de punto de cuenta del movimiento", ""));
        return null;
      }

      int valido = this.trabajadorNoGenFacade.validarFechaIngreso(this.personal.getIdPersonal(), this.login.getIdOrganismo(), this.fechaIngreso);
      if (valido == 1) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de reingreso debe ser mayor a la última fecha de egreso del trabajador en el organismo", ""));
        return null;
      }

      log.error("paso 1");
      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroMovimientoSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);
      log.error("paso 2");
      long idCausaPersonal = 3L;
      if (this.concursoCargo != null) {
        log.error("paso 3");
        this.idMovimientoSitp = this.movimientosNoGenFacade.ingresoTrabajadorLefp(this.personal.getIdPersonal(), this.tipoPersonal.getIdTipoPersonal(), this.registroCargos.getIdRegistroCargos(), this.fechaIngreso, idCausaPersonal, this.sueldo, this.numeroMovimiento, null, this.login.getIdUsuario(), this.fechaPuntoCuenta, this.puntoCuenta, this.concursoCargo.getIdConcursoCargo(), this.pagarRetroactivo, this.observaciones);
      } else {
        log.error("paso 4");
        this.idMovimientoSitp = this.movimientosNoGenFacade.ingresoTrabajadorLefp(this.personal.getIdPersonal(), this.tipoPersonal.getIdTipoPersonal(), this.registroCargos.getIdRegistroCargos(), this.fechaIngreso, idCausaPersonal, this.sueldo, this.numeroMovimiento, null, this.login.getIdUsuario(), this.fechaPuntoCuenta, this.puntoCuenta, 0L, this.pagarRetroactivo, this.observaciones);
      }
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.personal, this.personal);
      this.showReport = true;
      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error", ""));
    }

    abort();

    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/images/logo/0516.gif"));
    parameters.put("id_organismo", new Long(this.login.getIdOrganismo()));
    parameters.put("id_personal", new Long(this.personal.getIdPersonal()));
    parameters.put("id_causa_movimiento", new Long(3L));
    parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");
    parameters.put("id_movimiento", new Long(this.idMovimientoSitp));
    log.error("id_causa_movimiento = " + new Long(3L));

    this.reportName = "formato01lefp";

    JasperForWeb report = new JasperForWeb();

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public void actualizarCampos()
  {
    try
    {
      this.showButtonAux = true;

      this.nombreDependencia = this.registroCargos.getDependencia().toString();
      this.nombreSede = this.registroCargos.getSede().toString();
      this.nombreRegion = this.registroCargos.getSede().getRegion().toString();
      this.descripcionCargo = this.registroCargos.getCargo().toString();
      this.grado = this.registroCargos.getCargo().getGrado();
      DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), this.grado, this.registroCargos.getCargo().getSubGrado(), 1);
      this.sueldo = detalleTabulador.getMonto();
    }
    catch (Exception e)
    {
      this.nombreDependencia = null;
      this.nombreSede = null;
      this.nombreRegion = null;
      this.descripcionCargo = null;
      this.grado = 0;
      this.sueldo = 0.0D;
      this.numeroMovimiento = 0;
      this.remesa = null;

      this.showButtonAux = false;
    }
  }

  public Collection getColCausaPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonal.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }
    return col;
  }
  public void changeRegistroCargos(ValueChangeEvent event) {
    FacesContext context = FacesContext.getCurrentInstance();

    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (idRegistroCargos != 0L) {
        this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
        actualizarCampos();
        this.showFieldsAux = true;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    FacesContext context = FacesContext.getCurrentInstance();

    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (idTipoPersonal != 0L) {
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);
        if (this.mostrarRegistroCargos) {
          RegistroPersonal registroPersonal = (RegistroPersonal)this.registroFacade.findRegistroPersonalByTipoPersonal(this.tipoPersonal.getIdTipoPersonal()).iterator().next();
          this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosByRegistroAndSituacionAndTipo12(registroPersonal.getRegistro().getIdRegistro(), "V");
        } else {
          actualizarCampos();
          this.showFieldsAux = true;
        }
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void continuarConEgresado()
  {
    this.egresado = false;
  }

  public Collection getColRegistroCargos()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroCargos.getIdRegistroCargos()), 
        registroCargos.toString()));
    }

    return col;
  }
  public Collection getColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public ReingresoCargoCarreraLefpForm() throws Exception
  {
    this.reportName = "formato01lefp";
    FacesContext context = FacesContext.getCurrentInstance();
    this.colCausaPersonal = new ArrayList();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReingresoCargoCarreraLefpForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public void refresh()
  {
    try
    {
      this.colCausaPersonal = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefpAndGrado99(1L, 5L, "S", "N");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula() { FacesContext context = FacesContext.getCurrentInstance();
    TrayectoriaNoGenBeanBusiness trayectoria = new TrayectoriaNoGenBeanBusiness();
    if (this.fechaIngreso == null) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
      return null;
    }

    this.error = false;
    boolean esJubilado = false;
    boolean menos6Meses = false;
    boolean igual10Anios = false;
    boolean trabajadorActivo = false;

    Calendar calFechaEgreso = Calendar.getInstance();
    Calendar calFechaVigencia = Calendar.getInstance();

    log.error("fecha_ingreso........." + this.fechaIngreso);
    log.error("cedula........." + this.findPersonalCedula);
    calFechaVigencia.setTime(this.fechaIngreso);

    this.mostrarRegistroCargos = true;
    try
    {
      this.personal = 
        ((Personal)this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo()).iterator().next());
    }
    catch (Exception e) {
      this.showData1 = false;
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Los datos personales no estan registrados ", ""));
      this.error = true;
    }
    try
    {
      Collection colTrabajador = this.trabajadorFacade.findTrabajadorByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());

      Iterator iterTrabajador = colTrabajador.iterator();
      if (!colTrabajador.isEmpty()) {
        while (iterTrabajador.hasNext()) {
          Trabajador trabajador = (Trabajador)iterTrabajador.next();
          if (trabajador.getEstatus().equals("A")) {
            trabajadorActivo = true;
            context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador esta activo en otro tipo de personal, no se puede reingresar", ""));
            return null;
          }
          if (trabajador.getTipoPersonal().getClasificacionPersonal().getRelacionPersonal().getCodRelacion().equals("4")) {
            esJubilado = true;
            context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador esta jubilado, no se puede reingresar", ""));
            return null;
          }

          String mensaje = trayectoria.verificarReingreso(this.findPersonalCedula, "201", this.fechaIngreso);
          if (!mensaje.equals("true"))
          {
            if (mensaje.equals("continuar")) {
              calFechaEgreso.setTime(trabajador.getFechaEgreso());
              calFechaVigencia.setTime(this.fechaIngreso);
              calFechaVigencia.add(2, -6);

              if (calFechaEgreso.after(calFechaVigencia)) {
                menos6Meses = true;
                log.error("6 meses");
              }
              calFechaVigencia.setTime(this.fechaIngreso);
              calFechaVigencia.add(1, -10);

              if (calFechaEgreso.getTime().compareTo(calFechaVigencia.getTime()) <= 0)
                igual10Anios = true;
            }
            else {
              context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, ""));
              return null;
            }
          }
        }
        if (trabajadorActivo) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador esta activo en otro tipo de personal, no se puede reingresar", ""));
          return null;
        }
        if (esJubilado) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador esta jubilado, no se puede reingresar", ""));
          return null;
        }
        if (menos6Meses) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador tiene menos de 6 meses de egresado, no se puede reingresar", ""));
          return null;
        }

        if (igual10Anios) {
          log.error("pasa concurso");
          if (!validarConcurso(context)) {
            return null;
          }
          this.mostrarRegistroCargos = false;
        }

        this.colTipoPersonal = 
          this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd(
          "S", "S", this.login.getIdUsuario(), this.login.getAdministrador());

        this.showData1 = true;
        this.findSelectTrabajadorIdTipoPersonal = null;
      }
      else {
        this.showFechaEgreso = true;
        log.error("mostrar fecha de egreso");
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null; }

  public String validarPersonal() {
    FacesContext context = FacesContext.getCurrentInstance();
    if (this.fechaEgreso == null) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de egreso de la persona", ""));
      return null;
    }

    boolean menos6Meses = false;
    boolean igual10Anios = false;
    boolean destitucionMayor = false;

    Calendar calFechaEgreso = Calendar.getInstance();
    Calendar calFechaVigencia = Calendar.getInstance();

    this.mostrarRegistroCargos = true;

    calFechaVigencia.setTime(this.fechaIngreso);
    try
    {
      CausaPersonal causaPersonal = this.registroNoGenFacade.findCausaPersonalById(this.idCausaPersonal);

      if (causaPersonal.getCausaMovimiento().getCodCausaMovimiento().equals("502")) {
        this.mostrarRegistroCargos = false;
      } else {
        if (causaPersonal.getCausaMovimiento().getCodCausaMovimiento().equals("504")) {
          calFechaEgreso.setTime(this.fechaEgreso);
          calFechaVigencia.setTime(this.fechaIngreso);
          calFechaVigencia.add(1, -1);
          if (calFechaEgreso.after(calFechaVigencia)) {
            destitucionMayor = true;
          }
        }
        calFechaEgreso.setTime(this.fechaEgreso);

        calFechaVigencia.setTime(this.fechaIngreso);
        calFechaVigencia.add(2, -6);

        if (calFechaEgreso.after(calFechaVigencia)) {
          menos6Meses = true;
        }

        calFechaVigencia.setTime(this.fechaIngreso);
        calFechaVigencia.add(1, -10);
        if (calFechaEgreso.getTime().compareTo(calFechaVigencia.getTime()) <= 0) {
          igual10Anios = true;
        }
        if (destitucionMayor) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador posee un movimiento de destitución debe esperar por lo menos un año para poder reingresar", ""));
          return null;
        }
        if (menos6Meses) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador tiene menos de 6 meses de egresado, no se puede reingresar", ""));
          return null;
        }
        if (igual10Anios) {
          if (!validarConcurso(context)) {
            return null;
          }
          this.mostrarRegistroCargos = false;
        }
      }

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd(
        "S", "S", this.login.getIdUsuario(), this.login.getAdministrador());

      this.showData1 = true;
      this.findSelectTrabajadorIdTipoPersonal = null;
    }
    catch (Exception localException)
    {
    }

    return null;
  }

  private boolean validarConcurso(FacesContext context) {
    try {
      long idConcursoCargo = this.seleccionFacade.validarPostuladoConcurso(this.findPersonalCedula);
      if (idConcursoCargo != 0L)
      {
        this.concursoCargo = this.seleccionFacade.findConcursoCargoById(idConcursoCargo);

        this.registroCargos = this.registroCargosFacade.findRegistroCargosById(this.concursoCargo.getRegistroCargos().getIdRegistroCargos());

        if (this.registroCargos.getSituacion().equals("O")) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El cargo esta ocupado", ""));
          return false;
        }

        this.colTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalByIdRegistro(this.registroCargos.getRegistro().getIdRegistro());

        this.showData1 = true;
      }
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Persona tiene mas de 10 años de egresado y no esta registrado en concurso", ""));
        return false;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return true;
  }

  public String abort()
  {
    this.egresado = false;
    this.showData1 = false;
    this.colRegistroCargos = null;
    this.selectIdTipoPersonal = "0";
    this.selectIdRegistroCargos = "0";
    this.selectIdCausaPersonal = "0";
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaIngreso = null;
    this.sueldo = 0.0D;

    this.fechaPuntoCuenta = null;
    this.concursoCargo = null;
    this.puntoCuenta = "";
    this.observaciones = "";
    this.mostrarRegistroCargos = false;
    this.showFechaEgreso = false;
    this.fechaEgreso = null;

    return "cancel";
  }

  public boolean isShowData1() {
    return this.showData1;
  }

  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }
  public String getSelectCausaPersonal() {
    return this.selectCausaPersonal;
  }
  public void setSelectCausaPersonal(String string) {
    this.selectCausaPersonal = string;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public void setFindPersonalCedula(int findPersonalCedula) {
    this.findPersonalCedula = findPersonalCedula;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }

  public String getSelectIdCausaPersonal() {
    return this.selectIdCausaPersonal;
  }
  public void setSelectIdCausaPersonal(String selectIdCausaPersonal) {
    this.selectIdCausaPersonal = selectIdCausaPersonal;
  }
  public String getSelectIdRegistroCargos() {
    return this.selectIdRegistroCargos;
  }
  public void setSelectIdRegistroCargos(String string) {
    this.selectIdRegistroCargos = string;
  }
  public RegistroCargos getRegistroCargos() {
    return this.registroCargos;
  }
  public void setRegistroCargos(RegistroCargos cargos) {
    this.registroCargos = cargos;
  }
  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public int getGrado() {
    return this.grado;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public double getSueldo() {
    return this.sueldo;
  }

  public void setDescripcionCargo(String string) {
    this.descripcionCargo = string;
  }

  public void setGrado(int i) {
    this.grado = i;
  }

  public void setNombreDependencia(String string) {
    this.nombreDependencia = string;
  }

  public void setNombreRegion(String string) {
    this.nombreRegion = string;
  }

  public void setNombreSede(String string) {
    this.nombreSede = string;
  }

  public void setSueldo(double d) {
    this.sueldo = d;
  }

  public Date getFechaIngreso() {
    return this.fechaIngreso;
  }

  public void setFechaIngreso(Date date) {
    this.fechaIngreso = date;
  }

  public boolean isEgresado() {
    return this.egresado;
  }

  public void setEgresado(boolean b) {
    this.egresado = b;
  }

  public String getSelectProceso() {
    return this.selectProceso;
  }
  public void setSelectProceso(String selectProceso) {
    this.selectProceso = selectProceso;
  }

  public String getCodConcurso() {
    return this.codConcurso;
  }

  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }

  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }

  public void setCodConcurso(String string) {
    this.codConcurso = string;
  }

  public void setFechaPuntoCuenta(Date date) {
    this.fechaPuntoCuenta = date;
  }

  public void setPuntoCuenta(String string) {
    this.puntoCuenta = string;
  }

  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }

  public void setPagarRetroactivo(String string) {
    this.pagarRetroactivo = string;
  }

  public boolean isError() {
    return this.error;
  }

  public void setError(boolean error) {
    this.error = error;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public ConcursoCargo getConcursoCargo() {
    return this.concursoCargo;
  }
  public void setConcursoCargo(ConcursoCargo concursoCargo) {
    this.concursoCargo = concursoCargo;
  }
  public Date getFechaEgreso() {
    return this.fechaEgreso;
  }
  public void setFechaEgreso(Date fechaEgreso) {
    this.fechaEgreso = fechaEgreso;
  }
  public boolean isShowFechaEgreso() {
    return this.showFechaEgreso;
  }

  public boolean isMostrarRegistroCargos() {
    return this.mostrarRegistroCargos;
  }

  public long getIdCausaPersonal() {
    return this.idCausaPersonal;
  }

  public void setIdCausaPersonal(long idCausaPersonal) {
    this.idCausaPersonal = idCausaPersonal;
  }
}