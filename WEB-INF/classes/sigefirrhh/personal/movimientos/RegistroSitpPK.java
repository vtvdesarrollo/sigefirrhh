package sigefirrhh.personal.movimientos;

import java.io.Serializable;

public class RegistroSitpPK
  implements Serializable
{
  public long idRegistroSitp;

  public RegistroSitpPK()
  {
  }

  public RegistroSitpPK(long idRegistroSitp)
  {
    this.idRegistroSitp = idRegistroSitp;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegistroSitpPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegistroSitpPK thatPK)
  {
    return 
      this.idRegistroSitp == thatPK.idRegistroSitp;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegistroSitp)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegistroSitp);
  }
}