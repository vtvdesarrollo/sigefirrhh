package sigefirrhh.personal.movimientos;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class LicenciaConSueldoConRegistroForm extends LicenciaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(LicenciaConSueldoConRegistroForm.class.getName());

  public LicenciaConSueldoConRegistroForm()
    throws Exception
  {
  }

  public String ejecutar2()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      long idCausaPersonal = 12L;

      getMovimientosNoGenFacade().licenciaSuspensionConSinRegistro(
        getTrabajador().getIdTrabajador(), getFechaVigencia(), 
        getFechaCulminacion(), idCausaPersonal, getNumeroMovimiento(), 
        null, getLogin().getOrganismo(), getLogin().getIdUsuario(), 
        getFechaPuntoCuenta(), getPuntoCuenta(), getObservaciones());

      registrarAuditoria(context);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      setShow(false);
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }
}