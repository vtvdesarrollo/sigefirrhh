package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class MovimientosBusiness extends AbstractBusiness
  implements Serializable
{
  private MovimientoSitpBeanBusiness movimientoSitpBeanBusiness = new MovimientoSitpBeanBusiness();

  private RegistroSitpBeanBusiness registroSitpBeanBusiness = new RegistroSitpBeanBusiness();

  private RemesaBeanBusiness remesaBeanBusiness = new RemesaBeanBusiness();

  public void addMovimientoSitp(MovimientoSitp movimientoSitp)
    throws Exception
  {
    this.movimientoSitpBeanBusiness.addMovimientoSitp(movimientoSitp);
  }

  public long addMovimientoSitp(MovimientoSitp movimientoSitp, Date fechaMovimiento, String codCausaMovimiento, int numeroMovimiento, Date fechaPuntoCuenta, String puntoCuenta, String observaciones) throws Exception
  {
    return this.movimientoSitpBeanBusiness.addMovimientoSitp(movimientoSitp, fechaMovimiento, codCausaMovimiento, numeroMovimiento, fechaPuntoCuenta, puntoCuenta, observaciones);
  }

  public void updateMovimientoSitp(MovimientoSitp movimientoSitp) throws Exception
  {
    this.movimientoSitpBeanBusiness.updateMovimientoSitp(movimientoSitp);
  }

  public void deleteMovimientoSitp(MovimientoSitp movimientoSitp) throws Exception {
    this.movimientoSitpBeanBusiness.deleteMovimientoSitp(movimientoSitp);
  }

  public MovimientoSitp findMovimientoSitpById(long movimientoSitpId) throws Exception {
    return this.movimientoSitpBeanBusiness.findMovimientoSitpById(movimientoSitpId);
  }

  public Collection findAllMovimientoSitp() throws Exception {
    return this.movimientoSitpBeanBusiness.findMovimientoSitpAll();
  }

  public Collection findMovimientoSitpByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.movimientoSitpBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addRegistroSitp(RegistroSitp registroSitp)
    throws Exception
  {
    this.registroSitpBeanBusiness.addRegistroSitp(registroSitp);
  }

  public void updateRegistroSitp(RegistroSitp registroSitp) throws Exception {
    this.registroSitpBeanBusiness.updateRegistroSitp(registroSitp);
  }

  public void deleteRegistroSitp(RegistroSitp registroSitp) throws Exception {
    this.registroSitpBeanBusiness.deleteRegistroSitp(registroSitp);
  }

  public RegistroSitp findRegistroSitpById(long registroSitpId) throws Exception {
    return this.registroSitpBeanBusiness.findRegistroSitpById(registroSitpId);
  }

  public Collection findAllRegistroSitp() throws Exception {
    return this.registroSitpBeanBusiness.findRegistroSitpAll();
  }

  public Collection findRegistroSitpByPersonal(long idPersonal, long idOrganismo)
    throws Exception
  {
    return this.registroSitpBeanBusiness.findByPersonal(idPersonal, idOrganismo);
  }

  public void addRemesa(Remesa remesa)
    throws Exception
  {
    this.remesaBeanBusiness.addRemesa(remesa);
  }

  public void updateRemesa(Remesa remesa) throws Exception {
    this.remesaBeanBusiness.updateRemesa(remesa);
  }

  public void deleteRemesa(Remesa remesa) throws Exception {
    this.remesaBeanBusiness.deleteRemesa(remesa);
  }

  public Remesa findRemesaById(long remesaId) throws Exception {
    return this.remesaBeanBusiness.findRemesaById(remesaId);
  }

  public Collection findAllRemesa() throws Exception {
    return this.remesaBeanBusiness.findRemesaAll();
  }

  public Collection findRemesaByAnio(int anio, long idOrganismo)
    throws Exception
  {
    return this.remesaBeanBusiness.findByAnio(anio, idOrganismo);
  }

  public Collection findRemesaByNumero(int numero, long idOrganismo)
    throws Exception
  {
    return this.remesaBeanBusiness.findByNumero(numero, idOrganismo);
  }

  public Collection findRemesaByEstatus(String estatus, long idOrganismo)
    throws Exception
  {
    return this.remesaBeanBusiness.findByEstatus(estatus, idOrganismo);
  }
}