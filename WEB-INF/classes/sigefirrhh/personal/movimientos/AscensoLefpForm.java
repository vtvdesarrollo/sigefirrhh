package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.base.registro.RegistroPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.planificacion.seleccion.ConcursoCargo;
import sigefirrhh.planificacion.seleccion.SeleccionNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class AscensoLefpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AscensoLefpForm.class.getName());
  private String observaciones;
  private Date fechaEgresoReal;
  private Date fechaSalidaNomina;
  private String remesa;
  private int numeroMovimiento;
  private String aumento = "PO";
  private double porcentaje = 10.0D;
  private int paso;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private ConcursoCargo concursoCargo = new ConcursoCargo();
  private SeleccionNoGenFacade seleccionFacade = new SeleccionNoGenFacade();

  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();

  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private RegistroNoGenFacade registroFacade = new RegistroNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();

  private CalcularSueldosPromedioBeanBusiness calcularSueldoPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private String selectCausaPersonal;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection findColTipoPersonal;
  private Collection colRegistroCargos;
  private Collection colCausaPersonal;
  private boolean showRegistroCargosAux;
  private CausaPersonal causaPersonal;
  private boolean showFieldsAux;
  private RegistroCargos registroCargos;
  private boolean showButtonAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private Date fechaIngreso;
  private String selectIdRegistroCargos;
  private String pagarRetroactivo = "S";
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private double compensacionProyectado = 0.0D;
  private double sueldoBasicoProyectado = 0.0D;
  private double otrasRemuneracionesProyectado = 0.0D;
  private double totalRemuneracionesProyectado = 0.0D;

  private double compensacionActual = 0.0D;
  private double sueldoBasicoActual = 0.0D;
  private double otrasRemuneracionesActual = 0.0D;
  private double totalRemuneracionesActual = 0.0D;

  private boolean showNuevasRemuneraciones = false;

  private boolean showReport = false;
  private int reportId;
  private String reportName;
  private long idMovimientoSitp = 0L;

  private long idCausaPersonal = 7L;

  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  private Object stateScrollConceptoFijoByTrabajador = null;
  private Object stateResultConceptoFijoByTrabajador = null;

  public boolean isShowPorcentaje()
  {
    return this.aumento.equals("PO");
  }
  public boolean isShowPaso() {
    return this.aumento.equals("NP");
  }
  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (!this.colRegistroCargos.isEmpty());
  }
  public boolean isShowFields() {
    return this.showFieldsAux;
  }
  public String calcularNuevasRemuneraciones() {
    try {
      this.showNuevasRemuneraciones = false;

      int xPaso = this.paso + this.trabajador.getPaso();

      this.registrosBusiness.calcularNuevoCargoEnConceptoProyectado(this.trabajador.getIdTrabajador(), this.sueldo, this.registroCargos.getIdRegistroCargos(), this.aumento, this.porcentaje, xPaso, this.registroCargos.getCargo().getIdCargo());

      this.calcularSueldoPromedioBeanBusiness.calcularConceptosProyectados(this.trabajador);

      this.sueldoBasicoProyectado = this.trabajadorFacade.findMontoConceptoProyectadoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 1, "S");
      log.error("sueldo_basico_proyectado" + this.sueldoBasicoProyectado);
      this.compensacionProyectado = this.trabajadorFacade.findMontoConceptoProyectadoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 2, "S");

      this.otrasRemuneracionesProyectado = this.trabajadorFacade.findMontoConceptoProyectadoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 3, "S");
      this.totalRemuneracionesProyectado = this.trabajadorFacade.findMontoConceptoProyectadoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 4, "S");

      this.showNuevasRemuneraciones = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String calcularActualesRemuneraciones()
  {
    try {
      this.sueldoBasicoActual = this.trabajadorFacade.findMontoConceptoFijoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 1, "S");
      this.compensacionActual = this.trabajadorFacade.findMontoConceptoFijoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 2, "S");

      this.otrasRemuneracionesActual = this.trabajadorFacade.findMontoConceptoFijoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 3, "S");
      this.totalRemuneracionesActual = this.trabajadorFacade.findMontoConceptoFijoByTrabajadorQuincenal(this.trabajador.getIdTrabajador(), 4, "S");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public void changeRegistroCargos(ValueChangeEvent event) {
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();

    if (idRegistroCargos != 0L) {
      this.showFieldsAux = true;
    }

    try
    {
      this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
      actualizarCampos();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarCampos()
  {
    try {
      this.showButtonAux = true;

      this.nombreDependencia = this.registroCargos.getDependencia().toString();
      this.nombreSede = this.registroCargos.getSede().toString();
      this.nombreRegion = this.registroCargos.getSede().getRegion().toString();
      this.descripcionCargo = this.registroCargos.getCargo().toString();
      this.grado = this.registroCargos.getCargo().getGrado();

      DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), this.grado, this.registroCargos.getCargo().getSubGrado(), 1);
      this.sueldo = detalleTabulador.getMonto();

      log.error("sueldo" + this.sueldo);
    }
    catch (Exception e)
    {
      this.nombreDependencia = null;
      this.nombreSede = null;
      this.nombreRegion = null;
      this.descripcionCargo = null;
      this.grado = 0;
      this.sueldo = 0.0D;
      this.numeroMovimiento = 0;
      this.remesa = null;
      this.fechaIngreso = null;
      this.showButtonAux = false;
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaIngreso == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }
      if (this.fechaPuntoCuenta == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de punto de cuenta del movimiento", ""));
        return null;
      }
      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroMovimientoSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.idMovimientoSitp = this.movimientosNoGenFacade.cambioDesignacionAscensoLefp(this.trabajador.getIdTrabajador(), this.fechaIngreso, this.idCausaPersonal, this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.sueldo, this.registroCargos.getIdRegistroCargos(), this.fechaPuntoCuenta, this.puntoCuenta, this.concursoCargo.getIdConcursoCargo(), this.aumento, this.porcentaje, this.paso, this.pagarRetroactivo, this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());
      this.showReport = true;
      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;
      abort();
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    return "cancel";
  }

  public Collection getResult()
  {
    return this.result;
  }

  public AscensoLefpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    log.error("PASO1");
    this.colCausaPersonal = new ArrayList();

    this.reportName = "formato03lefp";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.showReport = false;
    this.idMovimientoSitp = 0L;

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        AscensoLefpForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public String runReport() {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/images/logo/0516.gif"));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");
      parameters.put("id_personal", new Long(this.trabajador.getPersonal().getIdPersonal()));
      parameters.put("id_causa_movimiento", new Long(this.idCausaPersonal));
      parameters.put("id_movimiento", new Long(this.idMovimientoSitp));

      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/personal/movimientos");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getFindColTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("S", "S", this.login.getIdUsuario(), this.login.getAdministrador());

      log.error("findColTipoPersonal " + this.findColTipoPersonal.size());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String showAscenso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();
      this.show = false;
      selectTrabajador();
      if ((this.trabajador.getCargo().getTipoCargo().equals("0")) || (this.trabajador.getCargo().getTipoCargo().equals("3")))
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Movimiento no permitido para cargo de alto nivel o cargo no clasificado", ""));
      else if (this.trabajador.getEstatus().equals("E")) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Movimiento no permitido para personal egresado", ""));
      }
      else
      {
        this.show = true;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
      try
      {
        long idConcursoCargo = this.seleccionFacade.validarPostuladoConcurso(this.trabajador.getCedula());

        RegistroPersonal registroPersonal = (RegistroPersonal)this.registroFacade.findRegistroPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal()).iterator().next();

        this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosForAscenso(registroPersonal.getRegistro().getIdRegistro(), "V", this.trabajador.getCargo().getGrado());
      }
      catch (Exception e) {
        log.error("Excepcion controlada:", e);
      }

      calcularActualesRemuneraciones();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }
  public Collection getColRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroCargos.getIdRegistroCargos()), 
        registroCargos.toString()));
    }

    return col;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;

    this.showResultTrabajador = false;
  }

  public String Ejecutar()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    return "cancel";
  }

  public String abort()
  {
    this.selected = false;
    resetResult();
    this.colCausaPersonal = null;
    this.colRegistroCargos = null;
    this.selectIdRegistroCargos = "0";
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaIngreso = null;
    this.sueldo = 0.0D;

    this.fechaPuntoCuenta = null;
    this.concursoCargo = new ConcursoCargo();
    this.puntoCuenta = "";
    this.registroCargos = null;
    this.observaciones = "";
    return "cancel";
  }

  public String abortUpdate()
  {
    this.selected = false;
    this.result = null;
    this.showResult = false;

    return "cancel";
  }

  public boolean isShow() {
    return this.show;
  }

  public boolean isShowData() {
    return (this.show) && (this.selectedTrabajador);
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }

  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }

  public Date getFechaEgresoReal() {
    return this.fechaEgresoReal;
  }
  public void setFechaEgresoReal(Date fechaEgresoReal) {
    this.fechaEgresoReal = fechaEgresoReal;
  }
  public Date getFechaSalidaNomina() {
    return this.fechaSalidaNomina;
  }
  public void setFechaSalidaNomina(Date fechaSalidaNomina) {
    this.fechaSalidaNomina = fechaSalidaNomina;
  }
  public String getSelectCausaPersonal() {
    return this.selectCausaPersonal;
  }
  public void setSelectCausaPersonal(String string) {
    this.selectCausaPersonal = string;
  }
  public CausaPersonal getCausaPersonal() {
    return this.causaPersonal;
  }
  public void setCausaPersonal(CausaPersonal causaPersonal) {
    this.causaPersonal = causaPersonal;
  }
  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public void setDescripcionCargo(String descripcionCargo) {
    this.descripcionCargo = descripcionCargo;
  }
  public Date getFechaIngreso() {
    return this.fechaIngreso;
  }
  public void setFechaIngreso(Date fechaIngreso) {
    this.fechaIngreso = fechaIngreso;
  }
  public int getGrado() {
    return this.grado;
  }
  public void setGrado(int grado) {
    this.grado = grado;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public void setNombreDependencia(String nombreDependencia) {
    this.nombreDependencia = nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public void setNombreRegion(String nombreRegion) {
    this.nombreRegion = nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public void setNombreSede(String nombreSede) {
    this.nombreSede = nombreSede;
  }
  public double getSueldo() {
    return this.sueldo;
  }
  public void setSueldo(double sueldo) {
    this.sueldo = sueldo;
  }
  public String getSelectIdRegistroCargos() {
    return this.selectIdRegistroCargos;
  }
  public void setSelectIdRegistroCargos(String selectIdRegistroCargos) {
    this.selectIdRegistroCargos = selectIdRegistroCargos;
  }
  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }
  public void setPagarRetroactivo(String pagarRetroactivo) {
    this.pagarRetroactivo = pagarRetroactivo;
  }
  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public String getAumento() {
    return this.aumento;
  }
  public void setAumento(String aumento) {
    this.aumento = aumento;
  }
  public int getPaso() {
    return this.paso;
  }
  public void setPaso(int paso) {
    this.paso = paso;
  }
  public double getPorcentaje() {
    return this.porcentaje;
  }
  public void setPorcentaje(double porcentaje) {
    this.porcentaje = porcentaje;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
  public double getCompensacionActual() {
    return this.compensacionActual;
  }
  public double getCompensacionProyectado() {
    return this.compensacionProyectado;
  }
  public double getOtrasRemuneracionesActual() {
    return this.otrasRemuneracionesActual;
  }
  public double getOtrasRemuneracionesProyectado() {
    return this.otrasRemuneracionesProyectado;
  }
  public double getSueldoBasicoActual() {
    return this.sueldoBasicoActual;
  }
  public double getSueldoBasicoProyectado() {
    return this.sueldoBasicoProyectado;
  }
  public double getTotalRemuneracionesActual() {
    return this.totalRemuneracionesActual;
  }
  public double getTotalRemuneracionesProyectado() {
    return this.totalRemuneracionesProyectado;
  }
  public boolean isShowNuevasRemuneraciones() {
    return this.showNuevasRemuneraciones;
  }
  public boolean isShowReport() {
    return this.showReport;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
}