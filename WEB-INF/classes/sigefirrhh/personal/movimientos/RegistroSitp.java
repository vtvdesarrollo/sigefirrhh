package sigefirrhh.personal.movimientos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.Turno;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.MovimientoPersonal;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.sistema.Usuario;

public class RegistroSitp
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_LOCALIDAD;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_TIPO;
  private long idRegistroSitp;
  private int cedula;
  private String apellidosNombres;
  private ClasificacionPersonal clasificacionPersonal;
  private Turno turno;
  private String tipoPersonal;
  private String nombreTipoPersonal;
  private CausaMovimiento causaMovimiento;
  private String codCausaMovimiento;
  private Date fechaIngreso;
  private Date fechaMovimiento;
  private Date fechaRegistro;
  private int codManualCargo;
  private String codCargo;
  private String codTabulador;
  private String descripcionCargo;
  private int codigoNomina;
  private String codSede;
  private String nombreSede;
  private String codDependencia;
  private String nombreDependencia;
  private double sueldo;
  private double compensacion;
  private double primasCargo;
  private double primasTrabajador;
  private int grado;
  private int paso;
  private Remesa remesa;
  private int numeroMovimiento;
  private String afectaSueldo;
  private String localidad;
  private String estatus;
  private String documentoSoporte;
  private Personal personal;
  private Organismo organismo;
  private String codOrganismo;
  private String nombreOrganismo;
  private String codOrganismoMpd;
  private String codRegion;
  private String nombreRegion;
  private String observaciones;
  private Usuario usuario;
  private String estatusMpd;
  private String codigoDevolucion;
  private String puntoCuenta;
  private Date fechaPuntoCuenta;
  private String codConcurso;
  private String analistaMpd;
  private int idAnalistaMpd;
  private Date fechaInicioMpd;
  private Date fechaFinMpd;
  private String observacionesMpd;
  private int anio;
  private int anteriorCodManualCargo;
  private String anteriorCodCargo;
  private String anteriorDescripcionCargo;
  private int anteriorCodigoNomina;
  private String anteriorCodSede;
  private String anteriorNombreSede;
  private String anteriorCodDependencia;
  private String anteriorNombreDependencia;
  private double anteriorSueldo;
  private double anteriorCompensacion;
  private double anteriorPrimasCargo;
  private double anteriorPrimasTrabajador;
  private int anteriorGrado;
  private int anteriorPaso;
  private String anteriorCodRegion;
  private String anteriorNombreRegion;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "afectaSueldo", "analistaMpd", "anio", "anteriorCodCargo", "anteriorCodDependencia", "anteriorCodManualCargo", "anteriorCodRegion", "anteriorCodSede", "anteriorCodigoNomina", "anteriorCompensacion", "anteriorDescripcionCargo", "anteriorGrado", "anteriorNombreDependencia", "anteriorNombreRegion", "anteriorNombreSede", "anteriorPaso", "anteriorPrimasCargo", "anteriorPrimasTrabajador", "anteriorSueldo", "apellidosNombres", "causaMovimiento", "cedula", "clasificacionPersonal", "codCargo", "codCausaMovimiento", "codConcurso", "codDependencia", "codManualCargo", "codOrganismo", "codOrganismoMpd", "codRegion", "codSede", "codTabulador", "codigoDevolucion", "codigoNomina", "compensacion", "descripcionCargo", "documentoSoporte", "estatus", "estatusMpd", "fechaFinMpd", "fechaIngreso", "fechaInicioMpd", "fechaMovimiento", "fechaPuntoCuenta", "fechaRegistro", "grado", "idAnalistaMpd", "idRegistroSitp", "idSitp", "localidad", "nombreDependencia", "nombreOrganismo", "nombreRegion", "nombreSede", "nombreTipoPersonal", "numeroMovimiento", "observaciones", "observacionesMpd", "organismo", "paso", "personal", "primasCargo", "primasTrabajador", "puntoCuenta", "remesa", "sueldo", "tiempoSitp", "tipoPersonal", "turno", "usuario" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.ClasificacionPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.movimientos.Remesa"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Turno"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 26, 21, 21, 21, 26, 21, 21, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.personal.movimientos.RegistroSitp"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RegistroSitp());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_LOCALIDAD = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_LOCALIDAD.put("C", "NIVEL CENTRAL");
    LISTA_LOCALIDAD.put("R", "NIVEL REGIONAL");
    LISTA_ESTATUS.put("0", "SIN REMESA");
    LISTA_ESTATUS.put("1", "CON REMESA");
    LISTA_ESTATUS.put("2", "ENVIADO MPD");
    LISTA_ESTATUS.put("3", "ANALISIS");
    LISTA_ESTATUS.put("4", "APROBADO");
    LISTA_ESTATUS.put("5", "DEVUELTO");
    LISTA_TIPO.put("0", "ALTO NIVEL");
    LISTA_TIPO.put("1", "ADMINISTRATIVO");
    LISTA_TIPO.put("2", "PROFESIONAL Y TECNICO");
    LISTA_TIPO.put("3", "NO CLASIFICADO");
    LISTA_TIPO.put("4", "OBRERO");
  }

  public RegistroSitp()
  {
    jdoSetcedula(this, 0);

    jdoSettipoPersonal(this, "1");

    jdoSetcodigoNomina(this, 0);

    jdoSetsueldo(this, 0.0D);

    jdoSetcompensacion(this, 0.0D);

    jdoSetprimasCargo(this, 0.0D);

    jdoSetprimasTrabajador(this, 0.0D);

    jdoSetgrado(this, 1);

    jdoSetpaso(this, 1);

    jdoSetafectaSueldo(this, "S");

    jdoSetlocalidad(this, "C");

    jdoSetestatus(this, "0");

    jdoSetanteriorCodigoNomina(this, 0);

    jdoSetanteriorSueldo(this, 0.0D);

    jdoSetanteriorCompensacion(this, 0.0D);

    jdoSetanteriorPrimasCargo(this, 0.0D);

    jdoSetanteriorPrimasTrabajador(this, 0.0D);

    jdoSetanteriorGrado(this, 1);

    jdoSetanteriorPaso(this, 1);
  }

  public String toString()
  {
    return 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaMovimiento(this)) + " - " + 
      jdoGetcausaMovimiento(this).getMovimientoPersonal().getDescripcion() + " - " + 
      jdoGetapellidosNombres(this);
  }

  public String getAfectaSueldo()
  {
    return jdoGetafectaSueldo(this);
  }

  public String getAnalistaMpd()
  {
    return jdoGetanalistaMpd(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public String getApellidosNombres()
  {
    return jdoGetapellidosNombres(this);
  }

  public CausaMovimiento getCausaMovimiento()
  {
    return jdoGetcausaMovimiento(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }

  public ClasificacionPersonal getClasificacionPersonal()
  {
    return jdoGetclasificacionPersonal(this);
  }

  public String getCodCargo()
  {
    return jdoGetcodCargo(this);
  }

  public String getCodCausaMovimiento()
  {
    return jdoGetcodCausaMovimiento(this);
  }

  public String getCodConcurso()
  {
    return jdoGetcodConcurso(this);
  }

  public String getCodDependencia()
  {
    return jdoGetcodDependencia(this);
  }

  public String getCodigoDevolucion()
  {
    return jdoGetcodigoDevolucion(this);
  }

  public int getCodigoNomina()
  {
    return jdoGetcodigoNomina(this);
  }

  public int getCodManualCargo()
  {
    return jdoGetcodManualCargo(this);
  }

  public String getCodOrganismo()
  {
    return jdoGetcodOrganismo(this);
  }

  public String getCodOrganismoMpd()
  {
    return jdoGetcodOrganismoMpd(this);
  }

  public String getCodRegion()
  {
    return jdoGetcodRegion(this);
  }

  public String getCodSede()
  {
    return jdoGetcodSede(this);
  }

  public String getCodTabulador()
  {
    return jdoGetcodTabulador(this);
  }

  public double getCompensacion()
  {
    return jdoGetcompensacion(this);
  }

  public String getDescripcionCargo()
  {
    return jdoGetdescripcionCargo(this);
  }

  public String getDocumentoSoporte()
  {
    return jdoGetdocumentoSoporte(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public String getEstatusMpd()
  {
    return jdoGetestatusMpd(this);
  }

  public Date getFechaFinMpd()
  {
    return jdoGetfechaFinMpd(this);
  }

  public Date getFechaInicioMpd()
  {
    return jdoGetfechaInicioMpd(this);
  }

  public Date getFechaMovimiento()
  {
    return jdoGetfechaMovimiento(this);
  }

  public Date getFechaPuntoCuenta()
  {
    return jdoGetfechaPuntoCuenta(this);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public int getGrado()
  {
    return jdoGetgrado(this);
  }

  public int getIdAnalistaMpd()
  {
    return jdoGetidAnalistaMpd(this);
  }

  public long getIdRegistroSitp()
  {
    return jdoGetidRegistroSitp(this);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public String getLocalidad()
  {
    return jdoGetlocalidad(this);
  }

  public String getNombreDependencia()
  {
    return jdoGetnombreDependencia(this);
  }

  public String getNombreOrganismo()
  {
    return jdoGetnombreOrganismo(this);
  }

  public String getNombreRegion()
  {
    return jdoGetnombreRegion(this);
  }

  public String getNombreSede()
  {
    return jdoGetnombreSede(this);
  }

  public String getNombreTipoPersonal()
  {
    return jdoGetnombreTipoPersonal(this);
  }

  public int getNumeroMovimiento()
  {
    return jdoGetnumeroMovimiento(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public String getObservacionesMpd()
  {
    return jdoGetobservacionesMpd(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public int getPaso()
  {
    return jdoGetpaso(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public double getPrimasCargo()
  {
    return jdoGetprimasCargo(this);
  }

  public double getPrimasTrabajador()
  {
    return jdoGetprimasTrabajador(this);
  }

  public String getPuntoCuenta()
  {
    return jdoGetpuntoCuenta(this);
  }

  public Remesa getRemesa()
  {
    return jdoGetremesa(this);
  }

  public double getSueldo()
  {
    return jdoGetsueldo(this);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public String getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setAfectaSueldo(String string)
  {
    jdoSetafectaSueldo(this, string);
  }

  public void setAnalistaMpd(String string)
  {
    jdoSetanalistaMpd(this, string);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setApellidosNombres(String string)
  {
    jdoSetapellidosNombres(this, string);
  }

  public void setCausaMovimiento(CausaMovimiento movimiento)
  {
    jdoSetcausaMovimiento(this, movimiento);
  }

  public void setCedula(int i)
  {
    jdoSetcedula(this, i);
  }

  public void setClasificacionPersonal(ClasificacionPersonal personal)
  {
    jdoSetclasificacionPersonal(this, personal);
  }

  public void setCodCargo(String string)
  {
    jdoSetcodCargo(this, string);
  }

  public void setCodCausaMovimiento(String string)
  {
    jdoSetcodCausaMovimiento(this, string);
  }

  public void setCodConcurso(String string)
  {
    jdoSetcodConcurso(this, string);
  }

  public void setCodDependencia(String string)
  {
    jdoSetcodDependencia(this, string);
  }

  public void setCodigoDevolucion(String string)
  {
    jdoSetcodigoDevolucion(this, string);
  }

  public void setCodigoNomina(int i)
  {
    jdoSetcodigoNomina(this, i);
  }

  public void setCodManualCargo(int i)
  {
    jdoSetcodManualCargo(this, i);
  }

  public void setCodOrganismo(String string)
  {
    jdoSetcodOrganismo(this, string);
  }

  public void setCodOrganismoMpd(String string)
  {
    jdoSetcodOrganismoMpd(this, string);
  }

  public void setCodRegion(String string)
  {
    jdoSetcodRegion(this, string);
  }

  public void setCodSede(String string)
  {
    jdoSetcodSede(this, string);
  }

  public void setCodTabulador(String string)
  {
    jdoSetcodTabulador(this, string);
  }

  public void setCompensacion(double d)
  {
    jdoSetcompensacion(this, d);
  }

  public void setDescripcionCargo(String string)
  {
    jdoSetdescripcionCargo(this, string);
  }

  public void setDocumentoSoporte(String string)
  {
    jdoSetdocumentoSoporte(this, string);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setEstatusMpd(String string)
  {
    jdoSetestatusMpd(this, string);
  }

  public void setFechaFinMpd(Date date)
  {
    jdoSetfechaFinMpd(this, date);
  }

  public void setFechaInicioMpd(Date date)
  {
    jdoSetfechaInicioMpd(this, date);
  }

  public void setFechaMovimiento(Date date)
  {
    jdoSetfechaMovimiento(this, date);
  }

  public void setFechaPuntoCuenta(Date date)
  {
    jdoSetfechaPuntoCuenta(this, date);
  }

  public void setFechaRegistro(Date date)
  {
    jdoSetfechaRegistro(this, date);
  }

  public void setGrado(int i)
  {
    jdoSetgrado(this, i);
  }

  public void setIdAnalistaMpd(int i)
  {
    jdoSetidAnalistaMpd(this, i);
  }

  public void setIdRegistroSitp(long l)
  {
    jdoSetidRegistroSitp(this, l);
  }

  public void setIdSitp(int i)
  {
    jdoSetidSitp(this, i);
  }

  public void setLocalidad(String string)
  {
    jdoSetlocalidad(this, string);
  }

  public void setNombreDependencia(String string)
  {
    jdoSetnombreDependencia(this, string);
  }

  public void setNombreOrganismo(String string)
  {
    jdoSetnombreOrganismo(this, string);
  }

  public void setNombreRegion(String string)
  {
    jdoSetnombreRegion(this, string);
  }

  public void setNombreSede(String string)
  {
    jdoSetnombreSede(this, string);
  }

  public void setNombreTipoPersonal(String string)
  {
    jdoSetnombreTipoPersonal(this, string);
  }

  public void setNumeroMovimiento(int i)
  {
    jdoSetnumeroMovimiento(this, i);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setObservacionesMpd(String string)
  {
    jdoSetobservacionesMpd(this, string);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setPaso(int i)
  {
    jdoSetpaso(this, i);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPrimasCargo(double d)
  {
    jdoSetprimasCargo(this, d);
  }

  public void setPrimasTrabajador(double d)
  {
    jdoSetprimasTrabajador(this, d);
  }

  public void setPuntoCuenta(String string)
  {
    jdoSetpuntoCuenta(this, string);
  }

  public void setRemesa(Remesa remesa)
  {
    jdoSetremesa(this, remesa);
  }

  public void setSueldo(double d)
  {
    jdoSetsueldo(this, d);
  }

  public void setTiempoSitp(Date date)
  {
    jdoSettiempoSitp(this, date);
  }

  public void setTipoPersonal(String string)
  {
    jdoSettipoPersonal(this, string);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public String getAnteriorCodCargo() {
    return jdoGetanteriorCodCargo(this);
  }
  public void setAnteriorCodCargo(String anteriorCodCargo) {
    jdoSetanteriorCodCargo(this, anteriorCodCargo);
  }
  public String getAnteriorCodDependencia() {
    return jdoGetanteriorCodDependencia(this);
  }
  public void setAnteriorCodDependencia(String anteriorCodDependencia) {
    jdoSetanteriorCodDependencia(this, anteriorCodDependencia);
  }
  public int getAnteriorCodigoNomina() {
    return jdoGetanteriorCodigoNomina(this);
  }
  public void setAnteriorCodigoNomina(int anteriorCodigoNomina) {
    jdoSetanteriorCodigoNomina(this, anteriorCodigoNomina);
  }
  public int getAnteriorCodManualCargo() {
    return jdoGetanteriorCodManualCargo(this);
  }
  public void setAnteriorCodManualCargo(int anteriorCodManualCargo) {
    jdoSetanteriorCodManualCargo(this, anteriorCodManualCargo);
  }
  public String getAnteriorCodRegion() {
    return jdoGetanteriorCodRegion(this);
  }
  public void setAnteriorCodRegion(String anteriorCodRegion) {
    jdoSetanteriorCodRegion(this, anteriorCodRegion);
  }
  public String getAnteriorCodSede() {
    return jdoGetanteriorCodSede(this);
  }
  public void setAnteriorCodSede(String anteriorCodSede) {
    jdoSetanteriorCodSede(this, anteriorCodSede);
  }
  public double getAnteriorCompensacion() {
    return jdoGetanteriorCompensacion(this);
  }
  public void setAnteriorCompensacion(double anteriorCompensacion) {
    jdoSetanteriorCompensacion(this, anteriorCompensacion);
  }
  public String getAnteriorDescripcionCargo() {
    return jdoGetanteriorDescripcionCargo(this);
  }
  public void setAnteriorDescripcionCargo(String anteriorDescripcionCargo) {
    jdoSetanteriorDescripcionCargo(this, anteriorDescripcionCargo);
  }
  public int getAnteriorGrado() {
    return jdoGetanteriorGrado(this);
  }
  public void setAnteriorGrado(int anteriorGrado) {
    jdoSetanteriorGrado(this, anteriorGrado);
  }
  public String getAnteriorNombreDependencia() {
    return jdoGetanteriorNombreDependencia(this);
  }
  public void setAnteriorNombreDependencia(String anteriorNombreDependencia) {
    jdoSetanteriorNombreDependencia(this, anteriorNombreDependencia);
  }
  public String getAnteriorNombreRegion() {
    return jdoGetanteriorNombreRegion(this);
  }
  public void setAnteriorNombreRegion(String anteriorNombreRegion) {
    jdoSetanteriorNombreRegion(this, anteriorNombreRegion);
  }
  public String getAnteriorNombreSede() {
    return jdoGetanteriorNombreSede(this);
  }
  public void setAnteriorNombreSede(String anteriorNombreSede) {
    jdoSetanteriorNombreSede(this, anteriorNombreSede);
  }
  public int getAnteriorPaso() {
    return jdoGetanteriorPaso(this);
  }
  public void setAnteriorPaso(int anteriorPaso) {
    jdoSetanteriorPaso(this, anteriorPaso);
  }
  public double getAnteriorPrimasCargo() {
    return jdoGetanteriorPrimasCargo(this);
  }
  public void setAnteriorPrimasCargo(double anteriorPrimasCargo) {
    jdoSetanteriorPrimasCargo(this, anteriorPrimasCargo);
  }
  public double getAnteriorPrimasTrabajador() {
    return jdoGetanteriorPrimasTrabajador(this);
  }
  public void setAnteriorPrimasTrabajador(double anteriorPrimasTrabajador) {
    jdoSetanteriorPrimasTrabajador(this, anteriorPrimasTrabajador);
  }
  public double getAnteriorSueldo() {
    return jdoGetanteriorSueldo(this);
  }
  public void setAnteriorSueldo(double anteriorSueldo) {
    jdoSetanteriorSueldo(this, anteriorSueldo);
  }

  public Date getFechaIngreso() {
    return jdoGetfechaIngreso(this);
  }

  public void setFechaIngreso(Date fechaIngreso) {
    jdoSetfechaIngreso(this, fechaIngreso);
  }

  public Turno getTurno() {
    return jdoGetturno(this);
  }

  public void setTurno(Turno turno) {
    jdoSetturno(this, turno);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 71;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RegistroSitp localRegistroSitp = new RegistroSitp();
    localRegistroSitp.jdoFlags = 1;
    localRegistroSitp.jdoStateManager = paramStateManager;
    return localRegistroSitp;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RegistroSitp localRegistroSitp = new RegistroSitp();
    localRegistroSitp.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegistroSitp.jdoFlags = 1;
    localRegistroSitp.jdoStateManager = paramStateManager;
    return localRegistroSitp;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.afectaSueldo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.analistaMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodCargo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodDependencia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorCodManualCargo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodRegion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorCodSede);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorCodigoNomina);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorCompensacion);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorDescripcionCargo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorGrado);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorNombreDependencia);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorNombreRegion);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.anteriorNombreSede);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anteriorPaso);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorPrimasCargo);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorPrimasTrabajador);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.anteriorSueldo);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.apellidosNombres);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.clasificacionPersonal);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCargo);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codCausaMovimiento);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcurso);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codManualCargo);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismo);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codOrganismoMpd);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codRegion);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSede);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTabulador);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoDevolucion);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacion);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionCargo);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.documentoSoporte);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatusMpd);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFinMpd);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicioMpd);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaMovimiento);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPuntoCuenta);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idAnalistaMpd);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegistroSitp);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.localidad);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDependencia);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreOrganismo);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreRegion);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSede);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreTipoPersonal);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroMovimiento);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observacionesMpd);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.paso);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasCargo);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasTrabajador);
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.puntoCuenta);
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.remesa);
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldo);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoPersonal);
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.turno);
      return;
    case 70:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.afectaSueldo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.analistaMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodManualCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCodigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorCompensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorDescripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorGrado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorNombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorNombreRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorNombreSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorPaso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorPrimasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorPrimasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anteriorSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.apellidosNombres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.clasificacionPersonal = ((ClasificacionPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codCausaMovimiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codManualCargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codOrganismoMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTabulador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoDevolucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.documentoSoporte = localStateManager.replacingStringField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatusMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFinMpd = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicioMpd = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaMovimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPuntoCuenta = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAnalistaMpd = localStateManager.replacingIntField(this, paramInt);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegistroSitp = localStateManager.replacingLongField(this, paramInt);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.localidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreRegion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSede = localStateManager.replacingStringField(this, paramInt);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreTipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroMovimiento = localStateManager.replacingIntField(this, paramInt);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 58:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observacionesMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 59:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 60:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 61:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 62:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasCargo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 63:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 64:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.puntoCuenta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 65:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.remesa = ((Remesa)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 66:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 67:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 68:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 69:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.turno = ((Turno)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 70:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RegistroSitp paramRegistroSitp, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.afectaSueldo = paramRegistroSitp.afectaSueldo;
      return;
    case 1:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.analistaMpd = paramRegistroSitp.analistaMpd;
      return;
    case 2:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramRegistroSitp.anio;
      return;
    case 3:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodCargo = paramRegistroSitp.anteriorCodCargo;
      return;
    case 4:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodDependencia = paramRegistroSitp.anteriorCodDependencia;
      return;
    case 5:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodManualCargo = paramRegistroSitp.anteriorCodManualCargo;
      return;
    case 6:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodRegion = paramRegistroSitp.anteriorCodRegion;
      return;
    case 7:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodSede = paramRegistroSitp.anteriorCodSede;
      return;
    case 8:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCodigoNomina = paramRegistroSitp.anteriorCodigoNomina;
      return;
    case 9:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorCompensacion = paramRegistroSitp.anteriorCompensacion;
      return;
    case 10:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorDescripcionCargo = paramRegistroSitp.anteriorDescripcionCargo;
      return;
    case 11:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorGrado = paramRegistroSitp.anteriorGrado;
      return;
    case 12:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorNombreDependencia = paramRegistroSitp.anteriorNombreDependencia;
      return;
    case 13:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorNombreRegion = paramRegistroSitp.anteriorNombreRegion;
      return;
    case 14:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorNombreSede = paramRegistroSitp.anteriorNombreSede;
      return;
    case 15:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorPaso = paramRegistroSitp.anteriorPaso;
      return;
    case 16:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorPrimasCargo = paramRegistroSitp.anteriorPrimasCargo;
      return;
    case 17:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorPrimasTrabajador = paramRegistroSitp.anteriorPrimasTrabajador;
      return;
    case 18:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.anteriorSueldo = paramRegistroSitp.anteriorSueldo;
      return;
    case 19:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.apellidosNombres = paramRegistroSitp.apellidosNombres;
      return;
    case 20:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramRegistroSitp.causaMovimiento;
      return;
    case 21:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramRegistroSitp.cedula;
      return;
    case 22:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.clasificacionPersonal = paramRegistroSitp.clasificacionPersonal;
      return;
    case 23:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codCargo = paramRegistroSitp.codCargo;
      return;
    case 24:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codCausaMovimiento = paramRegistroSitp.codCausaMovimiento;
      return;
    case 25:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codConcurso = paramRegistroSitp.codConcurso;
      return;
    case 26:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramRegistroSitp.codDependencia;
      return;
    case 27:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codManualCargo = paramRegistroSitp.codManualCargo;
      return;
    case 28:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismo = paramRegistroSitp.codOrganismo;
      return;
    case 29:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codOrganismoMpd = paramRegistroSitp.codOrganismoMpd;
      return;
    case 30:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codRegion = paramRegistroSitp.codRegion;
      return;
    case 31:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codSede = paramRegistroSitp.codSede;
      return;
    case 32:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codTabulador = paramRegistroSitp.codTabulador;
      return;
    case 33:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codigoDevolucion = paramRegistroSitp.codigoDevolucion;
      return;
    case 34:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramRegistroSitp.codigoNomina;
      return;
    case 35:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.compensacion = paramRegistroSitp.compensacion;
      return;
    case 36:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionCargo = paramRegistroSitp.descripcionCargo;
      return;
    case 37:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.documentoSoporte = paramRegistroSitp.documentoSoporte;
      return;
    case 38:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramRegistroSitp.estatus;
      return;
    case 39:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.estatusMpd = paramRegistroSitp.estatusMpd;
      return;
    case 40:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFinMpd = paramRegistroSitp.fechaFinMpd;
      return;
    case 41:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramRegistroSitp.fechaIngreso;
      return;
    case 42:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicioMpd = paramRegistroSitp.fechaInicioMpd;
      return;
    case 43:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaMovimiento = paramRegistroSitp.fechaMovimiento;
      return;
    case 44:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPuntoCuenta = paramRegistroSitp.fechaPuntoCuenta;
      return;
    case 45:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramRegistroSitp.fechaRegistro;
      return;
    case 46:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramRegistroSitp.grado;
      return;
    case 47:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.idAnalistaMpd = paramRegistroSitp.idAnalistaMpd;
      return;
    case 48:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.idRegistroSitp = paramRegistroSitp.idRegistroSitp;
      return;
    case 49:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramRegistroSitp.idSitp;
      return;
    case 50:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.localidad = paramRegistroSitp.localidad;
      return;
    case 51:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDependencia = paramRegistroSitp.nombreDependencia;
      return;
    case 52:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreOrganismo = paramRegistroSitp.nombreOrganismo;
      return;
    case 53:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreRegion = paramRegistroSitp.nombreRegion;
      return;
    case 54:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSede = paramRegistroSitp.nombreSede;
      return;
    case 55:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.nombreTipoPersonal = paramRegistroSitp.nombreTipoPersonal;
      return;
    case 56:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.numeroMovimiento = paramRegistroSitp.numeroMovimiento;
      return;
    case 57:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramRegistroSitp.observaciones;
      return;
    case 58:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.observacionesMpd = paramRegistroSitp.observacionesMpd;
      return;
    case 59:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramRegistroSitp.organismo;
      return;
    case 60:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.paso = paramRegistroSitp.paso;
      return;
    case 61:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramRegistroSitp.personal;
      return;
    case 62:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.primasCargo = paramRegistroSitp.primasCargo;
      return;
    case 63:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.primasTrabajador = paramRegistroSitp.primasTrabajador;
      return;
    case 64:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.puntoCuenta = paramRegistroSitp.puntoCuenta;
      return;
    case 65:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.remesa = paramRegistroSitp.remesa;
      return;
    case 66:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.sueldo = paramRegistroSitp.sueldo;
      return;
    case 67:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramRegistroSitp.tiempoSitp;
      return;
    case 68:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramRegistroSitp.tipoPersonal;
      return;
    case 69:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.turno = paramRegistroSitp.turno;
      return;
    case 70:
      if (paramRegistroSitp == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramRegistroSitp.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RegistroSitp))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RegistroSitp localRegistroSitp = (RegistroSitp)paramObject;
    if (localRegistroSitp.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegistroSitp, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegistroSitpPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegistroSitpPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroSitpPK))
      throw new IllegalArgumentException("arg1");
    RegistroSitpPK localRegistroSitpPK = (RegistroSitpPK)paramObject;
    localRegistroSitpPK.idRegistroSitp = this.idRegistroSitp;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroSitpPK))
      throw new IllegalArgumentException("arg1");
    RegistroSitpPK localRegistroSitpPK = (RegistroSitpPK)paramObject;
    this.idRegistroSitp = localRegistroSitpPK.idRegistroSitp;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroSitpPK))
      throw new IllegalArgumentException("arg2");
    RegistroSitpPK localRegistroSitpPK = (RegistroSitpPK)paramObject;
    localRegistroSitpPK.idRegistroSitp = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 48);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroSitpPK))
      throw new IllegalArgumentException("arg2");
    RegistroSitpPK localRegistroSitpPK = (RegistroSitpPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 48, localRegistroSitpPK.idRegistroSitp);
  }

  private static final String jdoGetafectaSueldo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.afectaSueldo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.afectaSueldo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 0))
      return paramRegistroSitp.afectaSueldo;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 0, paramRegistroSitp.afectaSueldo);
  }

  private static final void jdoSetafectaSueldo(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.afectaSueldo = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.afectaSueldo = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 0, paramRegistroSitp.afectaSueldo, paramString);
  }

  private static final String jdoGetanalistaMpd(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.analistaMpd;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.analistaMpd;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 1))
      return paramRegistroSitp.analistaMpd;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 1, paramRegistroSitp.analistaMpd);
  }

  private static final void jdoSetanalistaMpd(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.analistaMpd = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.analistaMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 1, paramRegistroSitp.analistaMpd, paramString);
  }

  private static final int jdoGetanio(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anio;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anio;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 2))
      return paramRegistroSitp.anio;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 2, paramRegistroSitp.anio);
  }

  private static final void jdoSetanio(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 2, paramRegistroSitp.anio, paramInt);
  }

  private static final String jdoGetanteriorCodCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorCodCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorCodCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 3))
      return paramRegistroSitp.anteriorCodCargo;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 3, paramRegistroSitp.anteriorCodCargo);
  }

  private static final void jdoSetanteriorCodCargo(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorCodCargo = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorCodCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 3, paramRegistroSitp.anteriorCodCargo, paramString);
  }

  private static final String jdoGetanteriorCodDependencia(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorCodDependencia;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorCodDependencia;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 4))
      return paramRegistroSitp.anteriorCodDependencia;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 4, paramRegistroSitp.anteriorCodDependencia);
  }

  private static final void jdoSetanteriorCodDependencia(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorCodDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorCodDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 4, paramRegistroSitp.anteriorCodDependencia, paramString);
  }

  private static final int jdoGetanteriorCodManualCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorCodManualCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorCodManualCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 5))
      return paramRegistroSitp.anteriorCodManualCargo;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 5, paramRegistroSitp.anteriorCodManualCargo);
  }

  private static final void jdoSetanteriorCodManualCargo(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorCodManualCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorCodManualCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 5, paramRegistroSitp.anteriorCodManualCargo, paramInt);
  }

  private static final String jdoGetanteriorCodRegion(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorCodRegion;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorCodRegion;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 6))
      return paramRegistroSitp.anteriorCodRegion;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 6, paramRegistroSitp.anteriorCodRegion);
  }

  private static final void jdoSetanteriorCodRegion(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorCodRegion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorCodRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 6, paramRegistroSitp.anteriorCodRegion, paramString);
  }

  private static final String jdoGetanteriorCodSede(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorCodSede;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorCodSede;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 7))
      return paramRegistroSitp.anteriorCodSede;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 7, paramRegistroSitp.anteriorCodSede);
  }

  private static final void jdoSetanteriorCodSede(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorCodSede = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorCodSede = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 7, paramRegistroSitp.anteriorCodSede, paramString);
  }

  private static final int jdoGetanteriorCodigoNomina(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorCodigoNomina;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorCodigoNomina;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 8))
      return paramRegistroSitp.anteriorCodigoNomina;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 8, paramRegistroSitp.anteriorCodigoNomina);
  }

  private static final void jdoSetanteriorCodigoNomina(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorCodigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorCodigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 8, paramRegistroSitp.anteriorCodigoNomina, paramInt);
  }

  private static final double jdoGetanteriorCompensacion(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorCompensacion;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorCompensacion;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 9))
      return paramRegistroSitp.anteriorCompensacion;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 9, paramRegistroSitp.anteriorCompensacion);
  }

  private static final void jdoSetanteriorCompensacion(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorCompensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorCompensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 9, paramRegistroSitp.anteriorCompensacion, paramDouble);
  }

  private static final String jdoGetanteriorDescripcionCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorDescripcionCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorDescripcionCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 10))
      return paramRegistroSitp.anteriorDescripcionCargo;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 10, paramRegistroSitp.anteriorDescripcionCargo);
  }

  private static final void jdoSetanteriorDescripcionCargo(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorDescripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorDescripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 10, paramRegistroSitp.anteriorDescripcionCargo, paramString);
  }

  private static final int jdoGetanteriorGrado(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorGrado;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorGrado;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 11))
      return paramRegistroSitp.anteriorGrado;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 11, paramRegistroSitp.anteriorGrado);
  }

  private static final void jdoSetanteriorGrado(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorGrado = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorGrado = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 11, paramRegistroSitp.anteriorGrado, paramInt);
  }

  private static final String jdoGetanteriorNombreDependencia(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorNombreDependencia;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorNombreDependencia;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 12))
      return paramRegistroSitp.anteriorNombreDependencia;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 12, paramRegistroSitp.anteriorNombreDependencia);
  }

  private static final void jdoSetanteriorNombreDependencia(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorNombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorNombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 12, paramRegistroSitp.anteriorNombreDependencia, paramString);
  }

  private static final String jdoGetanteriorNombreRegion(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorNombreRegion;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorNombreRegion;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 13))
      return paramRegistroSitp.anteriorNombreRegion;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 13, paramRegistroSitp.anteriorNombreRegion);
  }

  private static final void jdoSetanteriorNombreRegion(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorNombreRegion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorNombreRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 13, paramRegistroSitp.anteriorNombreRegion, paramString);
  }

  private static final String jdoGetanteriorNombreSede(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorNombreSede;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorNombreSede;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 14))
      return paramRegistroSitp.anteriorNombreSede;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 14, paramRegistroSitp.anteriorNombreSede);
  }

  private static final void jdoSetanteriorNombreSede(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorNombreSede = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorNombreSede = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 14, paramRegistroSitp.anteriorNombreSede, paramString);
  }

  private static final int jdoGetanteriorPaso(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorPaso;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorPaso;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 15))
      return paramRegistroSitp.anteriorPaso;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 15, paramRegistroSitp.anteriorPaso);
  }

  private static final void jdoSetanteriorPaso(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorPaso = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorPaso = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 15, paramRegistroSitp.anteriorPaso, paramInt);
  }

  private static final double jdoGetanteriorPrimasCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorPrimasCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorPrimasCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 16))
      return paramRegistroSitp.anteriorPrimasCargo;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 16, paramRegistroSitp.anteriorPrimasCargo);
  }

  private static final void jdoSetanteriorPrimasCargo(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorPrimasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorPrimasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 16, paramRegistroSitp.anteriorPrimasCargo, paramDouble);
  }

  private static final double jdoGetanteriorPrimasTrabajador(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorPrimasTrabajador;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorPrimasTrabajador;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 17))
      return paramRegistroSitp.anteriorPrimasTrabajador;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 17, paramRegistroSitp.anteriorPrimasTrabajador);
  }

  private static final void jdoSetanteriorPrimasTrabajador(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorPrimasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorPrimasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 17, paramRegistroSitp.anteriorPrimasTrabajador, paramDouble);
  }

  private static final double jdoGetanteriorSueldo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.anteriorSueldo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.anteriorSueldo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 18))
      return paramRegistroSitp.anteriorSueldo;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 18, paramRegistroSitp.anteriorSueldo);
  }

  private static final void jdoSetanteriorSueldo(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.anteriorSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.anteriorSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 18, paramRegistroSitp.anteriorSueldo, paramDouble);
  }

  private static final String jdoGetapellidosNombres(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.apellidosNombres;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.apellidosNombres;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 19))
      return paramRegistroSitp.apellidosNombres;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 19, paramRegistroSitp.apellidosNombres);
  }

  private static final void jdoSetapellidosNombres(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.apellidosNombres = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.apellidosNombres = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 19, paramRegistroSitp.apellidosNombres, paramString);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(RegistroSitp paramRegistroSitp)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.causaMovimiento;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 20))
      return paramRegistroSitp.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 20, paramRegistroSitp.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(RegistroSitp paramRegistroSitp, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 20, paramRegistroSitp.causaMovimiento, paramCausaMovimiento);
  }

  private static final int jdoGetcedula(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.cedula;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.cedula;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 21))
      return paramRegistroSitp.cedula;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 21, paramRegistroSitp.cedula);
  }

  private static final void jdoSetcedula(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 21, paramRegistroSitp.cedula, paramInt);
  }

  private static final ClasificacionPersonal jdoGetclasificacionPersonal(RegistroSitp paramRegistroSitp)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.clasificacionPersonal;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 22))
      return paramRegistroSitp.clasificacionPersonal;
    return (ClasificacionPersonal)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 22, paramRegistroSitp.clasificacionPersonal);
  }

  private static final void jdoSetclasificacionPersonal(RegistroSitp paramRegistroSitp, ClasificacionPersonal paramClasificacionPersonal)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.clasificacionPersonal = paramClasificacionPersonal;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 22, paramRegistroSitp.clasificacionPersonal, paramClasificacionPersonal);
  }

  private static final String jdoGetcodCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 23))
      return paramRegistroSitp.codCargo;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 23, paramRegistroSitp.codCargo);
  }

  private static final void jdoSetcodCargo(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codCargo = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 23, paramRegistroSitp.codCargo, paramString);
  }

  private static final String jdoGetcodCausaMovimiento(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codCausaMovimiento;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codCausaMovimiento;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 24))
      return paramRegistroSitp.codCausaMovimiento;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 24, paramRegistroSitp.codCausaMovimiento);
  }

  private static final void jdoSetcodCausaMovimiento(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codCausaMovimiento = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codCausaMovimiento = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 24, paramRegistroSitp.codCausaMovimiento, paramString);
  }

  private static final String jdoGetcodConcurso(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codConcurso;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codConcurso;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 25))
      return paramRegistroSitp.codConcurso;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 25, paramRegistroSitp.codConcurso);
  }

  private static final void jdoSetcodConcurso(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codConcurso = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codConcurso = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 25, paramRegistroSitp.codConcurso, paramString);
  }

  private static final String jdoGetcodDependencia(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codDependencia;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codDependencia;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 26))
      return paramRegistroSitp.codDependencia;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 26, paramRegistroSitp.codDependencia);
  }

  private static final void jdoSetcodDependencia(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 26, paramRegistroSitp.codDependencia, paramString);
  }

  private static final int jdoGetcodManualCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codManualCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codManualCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 27))
      return paramRegistroSitp.codManualCargo;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 27, paramRegistroSitp.codManualCargo);
  }

  private static final void jdoSetcodManualCargo(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codManualCargo = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codManualCargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 27, paramRegistroSitp.codManualCargo, paramInt);
  }

  private static final String jdoGetcodOrganismo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codOrganismo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codOrganismo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 28))
      return paramRegistroSitp.codOrganismo;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 28, paramRegistroSitp.codOrganismo);
  }

  private static final void jdoSetcodOrganismo(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 28, paramRegistroSitp.codOrganismo, paramString);
  }

  private static final String jdoGetcodOrganismoMpd(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codOrganismoMpd;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codOrganismoMpd;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 29))
      return paramRegistroSitp.codOrganismoMpd;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 29, paramRegistroSitp.codOrganismoMpd);
  }

  private static final void jdoSetcodOrganismoMpd(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codOrganismoMpd = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codOrganismoMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 29, paramRegistroSitp.codOrganismoMpd, paramString);
  }

  private static final String jdoGetcodRegion(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codRegion;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codRegion;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 30))
      return paramRegistroSitp.codRegion;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 30, paramRegistroSitp.codRegion);
  }

  private static final void jdoSetcodRegion(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codRegion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 30, paramRegistroSitp.codRegion, paramString);
  }

  private static final String jdoGetcodSede(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codSede;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codSede;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 31))
      return paramRegistroSitp.codSede;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 31, paramRegistroSitp.codSede);
  }

  private static final void jdoSetcodSede(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codSede = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codSede = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 31, paramRegistroSitp.codSede, paramString);
  }

  private static final String jdoGetcodTabulador(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codTabulador;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codTabulador;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 32))
      return paramRegistroSitp.codTabulador;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 32, paramRegistroSitp.codTabulador);
  }

  private static final void jdoSetcodTabulador(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codTabulador = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codTabulador = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 32, paramRegistroSitp.codTabulador, paramString);
  }

  private static final String jdoGetcodigoDevolucion(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codigoDevolucion;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codigoDevolucion;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 33))
      return paramRegistroSitp.codigoDevolucion;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 33, paramRegistroSitp.codigoDevolucion);
  }

  private static final void jdoSetcodigoDevolucion(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codigoDevolucion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codigoDevolucion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 33, paramRegistroSitp.codigoDevolucion, paramString);
  }

  private static final int jdoGetcodigoNomina(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.codigoNomina;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.codigoNomina;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 34))
      return paramRegistroSitp.codigoNomina;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 34, paramRegistroSitp.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 34, paramRegistroSitp.codigoNomina, paramInt);
  }

  private static final double jdoGetcompensacion(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.compensacion;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.compensacion;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 35))
      return paramRegistroSitp.compensacion;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 35, paramRegistroSitp.compensacion);
  }

  private static final void jdoSetcompensacion(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.compensacion = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.compensacion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 35, paramRegistroSitp.compensacion, paramDouble);
  }

  private static final String jdoGetdescripcionCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.descripcionCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.descripcionCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 36))
      return paramRegistroSitp.descripcionCargo;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 36, paramRegistroSitp.descripcionCargo);
  }

  private static final void jdoSetdescripcionCargo(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.descripcionCargo = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.descripcionCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 36, paramRegistroSitp.descripcionCargo, paramString);
  }

  private static final String jdoGetdocumentoSoporte(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.documentoSoporte;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.documentoSoporte;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 37))
      return paramRegistroSitp.documentoSoporte;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 37, paramRegistroSitp.documentoSoporte);
  }

  private static final void jdoSetdocumentoSoporte(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.documentoSoporte = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.documentoSoporte = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 37, paramRegistroSitp.documentoSoporte, paramString);
  }

  private static final String jdoGetestatus(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.estatus;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.estatus;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 38))
      return paramRegistroSitp.estatus;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 38, paramRegistroSitp.estatus);
  }

  private static final void jdoSetestatus(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 38, paramRegistroSitp.estatus, paramString);
  }

  private static final String jdoGetestatusMpd(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.estatusMpd;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.estatusMpd;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 39))
      return paramRegistroSitp.estatusMpd;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 39, paramRegistroSitp.estatusMpd);
  }

  private static final void jdoSetestatusMpd(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.estatusMpd = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.estatusMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 39, paramRegistroSitp.estatusMpd, paramString);
  }

  private static final Date jdoGetfechaFinMpd(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.fechaFinMpd;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.fechaFinMpd;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 40))
      return paramRegistroSitp.fechaFinMpd;
    return (Date)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 40, paramRegistroSitp.fechaFinMpd);
  }

  private static final void jdoSetfechaFinMpd(RegistroSitp paramRegistroSitp, Date paramDate)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.fechaFinMpd = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.fechaFinMpd = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 40, paramRegistroSitp.fechaFinMpd, paramDate);
  }

  private static final Date jdoGetfechaIngreso(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.fechaIngreso;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.fechaIngreso;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 41))
      return paramRegistroSitp.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 41, paramRegistroSitp.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(RegistroSitp paramRegistroSitp, Date paramDate)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 41, paramRegistroSitp.fechaIngreso, paramDate);
  }

  private static final Date jdoGetfechaInicioMpd(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.fechaInicioMpd;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.fechaInicioMpd;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 42))
      return paramRegistroSitp.fechaInicioMpd;
    return (Date)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 42, paramRegistroSitp.fechaInicioMpd);
  }

  private static final void jdoSetfechaInicioMpd(RegistroSitp paramRegistroSitp, Date paramDate)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.fechaInicioMpd = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.fechaInicioMpd = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 42, paramRegistroSitp.fechaInicioMpd, paramDate);
  }

  private static final Date jdoGetfechaMovimiento(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.fechaMovimiento;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.fechaMovimiento;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 43))
      return paramRegistroSitp.fechaMovimiento;
    return (Date)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 43, paramRegistroSitp.fechaMovimiento);
  }

  private static final void jdoSetfechaMovimiento(RegistroSitp paramRegistroSitp, Date paramDate)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.fechaMovimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.fechaMovimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 43, paramRegistroSitp.fechaMovimiento, paramDate);
  }

  private static final Date jdoGetfechaPuntoCuenta(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.fechaPuntoCuenta;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.fechaPuntoCuenta;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 44))
      return paramRegistroSitp.fechaPuntoCuenta;
    return (Date)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 44, paramRegistroSitp.fechaPuntoCuenta);
  }

  private static final void jdoSetfechaPuntoCuenta(RegistroSitp paramRegistroSitp, Date paramDate)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.fechaPuntoCuenta = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.fechaPuntoCuenta = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 44, paramRegistroSitp.fechaPuntoCuenta, paramDate);
  }

  private static final Date jdoGetfechaRegistro(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.fechaRegistro;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.fechaRegistro;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 45))
      return paramRegistroSitp.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 45, paramRegistroSitp.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(RegistroSitp paramRegistroSitp, Date paramDate)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 45, paramRegistroSitp.fechaRegistro, paramDate);
  }

  private static final int jdoGetgrado(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.grado;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.grado;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 46))
      return paramRegistroSitp.grado;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 46, paramRegistroSitp.grado);
  }

  private static final void jdoSetgrado(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 46, paramRegistroSitp.grado, paramInt);
  }

  private static final int jdoGetidAnalistaMpd(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.idAnalistaMpd;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.idAnalistaMpd;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 47))
      return paramRegistroSitp.idAnalistaMpd;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 47, paramRegistroSitp.idAnalistaMpd);
  }

  private static final void jdoSetidAnalistaMpd(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.idAnalistaMpd = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.idAnalistaMpd = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 47, paramRegistroSitp.idAnalistaMpd, paramInt);
  }

  private static final long jdoGetidRegistroSitp(RegistroSitp paramRegistroSitp)
  {
    return paramRegistroSitp.idRegistroSitp;
  }

  private static final void jdoSetidRegistroSitp(RegistroSitp paramRegistroSitp, long paramLong)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.idRegistroSitp = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegistroSitp, jdoInheritedFieldCount + 48, paramRegistroSitp.idRegistroSitp, paramLong);
  }

  private static final int jdoGetidSitp(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.idSitp;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.idSitp;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 49))
      return paramRegistroSitp.idSitp;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 49, paramRegistroSitp.idSitp);
  }

  private static final void jdoSetidSitp(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 49, paramRegistroSitp.idSitp, paramInt);
  }

  private static final String jdoGetlocalidad(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.localidad;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.localidad;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 50))
      return paramRegistroSitp.localidad;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 50, paramRegistroSitp.localidad);
  }

  private static final void jdoSetlocalidad(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.localidad = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.localidad = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 50, paramRegistroSitp.localidad, paramString);
  }

  private static final String jdoGetnombreDependencia(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.nombreDependencia;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.nombreDependencia;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 51))
      return paramRegistroSitp.nombreDependencia;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 51, paramRegistroSitp.nombreDependencia);
  }

  private static final void jdoSetnombreDependencia(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.nombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.nombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 51, paramRegistroSitp.nombreDependencia, paramString);
  }

  private static final String jdoGetnombreOrganismo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.nombreOrganismo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.nombreOrganismo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 52))
      return paramRegistroSitp.nombreOrganismo;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 52, paramRegistroSitp.nombreOrganismo);
  }

  private static final void jdoSetnombreOrganismo(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.nombreOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.nombreOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 52, paramRegistroSitp.nombreOrganismo, paramString);
  }

  private static final String jdoGetnombreRegion(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.nombreRegion;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.nombreRegion;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 53))
      return paramRegistroSitp.nombreRegion;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 53, paramRegistroSitp.nombreRegion);
  }

  private static final void jdoSetnombreRegion(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.nombreRegion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.nombreRegion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 53, paramRegistroSitp.nombreRegion, paramString);
  }

  private static final String jdoGetnombreSede(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.nombreSede;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.nombreSede;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 54))
      return paramRegistroSitp.nombreSede;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 54, paramRegistroSitp.nombreSede);
  }

  private static final void jdoSetnombreSede(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.nombreSede = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.nombreSede = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 54, paramRegistroSitp.nombreSede, paramString);
  }

  private static final String jdoGetnombreTipoPersonal(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.nombreTipoPersonal;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.nombreTipoPersonal;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 55))
      return paramRegistroSitp.nombreTipoPersonal;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 55, paramRegistroSitp.nombreTipoPersonal);
  }

  private static final void jdoSetnombreTipoPersonal(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.nombreTipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.nombreTipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 55, paramRegistroSitp.nombreTipoPersonal, paramString);
  }

  private static final int jdoGetnumeroMovimiento(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.numeroMovimiento;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.numeroMovimiento;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 56))
      return paramRegistroSitp.numeroMovimiento;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 56, paramRegistroSitp.numeroMovimiento);
  }

  private static final void jdoSetnumeroMovimiento(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.numeroMovimiento = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.numeroMovimiento = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 56, paramRegistroSitp.numeroMovimiento, paramInt);
  }

  private static final String jdoGetobservaciones(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.observaciones;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.observaciones;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 57))
      return paramRegistroSitp.observaciones;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 57, paramRegistroSitp.observaciones);
  }

  private static final void jdoSetobservaciones(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 57, paramRegistroSitp.observaciones, paramString);
  }

  private static final String jdoGetobservacionesMpd(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.observacionesMpd;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.observacionesMpd;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 58))
      return paramRegistroSitp.observacionesMpd;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 58, paramRegistroSitp.observacionesMpd);
  }

  private static final void jdoSetobservacionesMpd(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.observacionesMpd = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.observacionesMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 58, paramRegistroSitp.observacionesMpd, paramString);
  }

  private static final Organismo jdoGetorganismo(RegistroSitp paramRegistroSitp)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.organismo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 59))
      return paramRegistroSitp.organismo;
    return (Organismo)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 59, paramRegistroSitp.organismo);
  }

  private static final void jdoSetorganismo(RegistroSitp paramRegistroSitp, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 59, paramRegistroSitp.organismo, paramOrganismo);
  }

  private static final int jdoGetpaso(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.paso;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.paso;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 60))
      return paramRegistroSitp.paso;
    return localStateManager.getIntField(paramRegistroSitp, jdoInheritedFieldCount + 60, paramRegistroSitp.paso);
  }

  private static final void jdoSetpaso(RegistroSitp paramRegistroSitp, int paramInt)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.paso = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.paso = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroSitp, jdoInheritedFieldCount + 60, paramRegistroSitp.paso, paramInt);
  }

  private static final Personal jdoGetpersonal(RegistroSitp paramRegistroSitp)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.personal;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 61))
      return paramRegistroSitp.personal;
    return (Personal)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 61, paramRegistroSitp.personal);
  }

  private static final void jdoSetpersonal(RegistroSitp paramRegistroSitp, Personal paramPersonal)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 61, paramRegistroSitp.personal, paramPersonal);
  }

  private static final double jdoGetprimasCargo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.primasCargo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.primasCargo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 62))
      return paramRegistroSitp.primasCargo;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 62, paramRegistroSitp.primasCargo);
  }

  private static final void jdoSetprimasCargo(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.primasCargo = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.primasCargo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 62, paramRegistroSitp.primasCargo, paramDouble);
  }

  private static final double jdoGetprimasTrabajador(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.primasTrabajador;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.primasTrabajador;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 63))
      return paramRegistroSitp.primasTrabajador;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 63, paramRegistroSitp.primasTrabajador);
  }

  private static final void jdoSetprimasTrabajador(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.primasTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.primasTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 63, paramRegistroSitp.primasTrabajador, paramDouble);
  }

  private static final String jdoGetpuntoCuenta(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.puntoCuenta;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.puntoCuenta;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 64))
      return paramRegistroSitp.puntoCuenta;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 64, paramRegistroSitp.puntoCuenta);
  }

  private static final void jdoSetpuntoCuenta(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.puntoCuenta = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.puntoCuenta = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 64, paramRegistroSitp.puntoCuenta, paramString);
  }

  private static final Remesa jdoGetremesa(RegistroSitp paramRegistroSitp)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.remesa;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 65))
      return paramRegistroSitp.remesa;
    return (Remesa)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 65, paramRegistroSitp.remesa);
  }

  private static final void jdoSetremesa(RegistroSitp paramRegistroSitp, Remesa paramRemesa)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.remesa = paramRemesa;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 65, paramRegistroSitp.remesa, paramRemesa);
  }

  private static final double jdoGetsueldo(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.sueldo;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.sueldo;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 66))
      return paramRegistroSitp.sueldo;
    return localStateManager.getDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 66, paramRegistroSitp.sueldo);
  }

  private static final void jdoSetsueldo(RegistroSitp paramRegistroSitp, double paramDouble)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.sueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.sueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroSitp, jdoInheritedFieldCount + 66, paramRegistroSitp.sueldo, paramDouble);
  }

  private static final Date jdoGettiempoSitp(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.tiempoSitp;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.tiempoSitp;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 67))
      return paramRegistroSitp.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 67, paramRegistroSitp.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(RegistroSitp paramRegistroSitp, Date paramDate)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 67, paramRegistroSitp.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoPersonal(RegistroSitp paramRegistroSitp)
  {
    if (paramRegistroSitp.jdoFlags <= 0)
      return paramRegistroSitp.tipoPersonal;
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.tipoPersonal;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 68))
      return paramRegistroSitp.tipoPersonal;
    return localStateManager.getStringField(paramRegistroSitp, jdoInheritedFieldCount + 68, paramRegistroSitp.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(RegistroSitp paramRegistroSitp, String paramString)
  {
    if (paramRegistroSitp.jdoFlags == 0)
    {
      paramRegistroSitp.tipoPersonal = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.tipoPersonal = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroSitp, jdoInheritedFieldCount + 68, paramRegistroSitp.tipoPersonal, paramString);
  }

  private static final Turno jdoGetturno(RegistroSitp paramRegistroSitp)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.turno;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 69))
      return paramRegistroSitp.turno;
    return (Turno)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 69, paramRegistroSitp.turno);
  }

  private static final void jdoSetturno(RegistroSitp paramRegistroSitp, Turno paramTurno)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.turno = paramTurno;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 69, paramRegistroSitp.turno, paramTurno);
  }

  private static final Usuario jdoGetusuario(RegistroSitp paramRegistroSitp)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroSitp.usuario;
    if (localStateManager.isLoaded(paramRegistroSitp, jdoInheritedFieldCount + 70))
      return paramRegistroSitp.usuario;
    return (Usuario)localStateManager.getObjectField(paramRegistroSitp, jdoInheritedFieldCount + 70, paramRegistroSitp.usuario);
  }

  private static final void jdoSetusuario(RegistroSitp paramRegistroSitp, Usuario paramUsuario)
  {
    StateManager localStateManager = paramRegistroSitp.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroSitp.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramRegistroSitp, jdoInheritedFieldCount + 70, paramRegistroSitp.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}