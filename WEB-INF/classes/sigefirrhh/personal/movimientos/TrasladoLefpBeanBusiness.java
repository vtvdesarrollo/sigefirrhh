package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class TrasladoLefpBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(TrasladoLefpBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();

  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  public long actualizar(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idDependencia, String observaciones)
    throws Exception
  {
    try
    {
      this.txn.open();

      PersistenceManager pm = PMThread.getPM();

      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Organismo organismo = new Organismo();
      organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

      CausaMovimiento causaMovimiento = new CausaMovimiento();
      causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

      Dependencia dependencia = this.estructuraBusiness.findDependenciaById(idDependencia);

      RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(idRegistroCargos);

      registroCargos.setDependencia(dependencia);
      registroCargos.setSede(dependencia.getSede());
      long idMovimientoSitp = 0L;

      if (registroCargos.getSituacion().equals("O"))
      {
        long idTrabajador = registroCargos.getTrabajador().getIdTrabajador();

        Trabajador trabajador = new Trabajador();
        trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

        String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(trabajador.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaMovimiento);
        if (mensaje != null) {
          ErrorSistema error = new ErrorSistema();
          error.setDescription(mensaje);
          throw error;
        }
        this.log.error("no encontró problemas en trayectoria");

        trabajador.setCausaMovimiento(causaMovimiento);
        trabajador.setFechaUltimoMovimiento(fechaMovimiento);
        trabajador.setDependencia(dependencia);

        SueldoPromedio sueldoPromedio = new SueldoPromedio();
        try {
          sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
        } catch (Exception e) {
          ErrorSistema error = new ErrorSistema();
          error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
          throw error;
        }

        int anteriorCodManualCargo = trabajador.getCargo().getManualCargo().getCodManualCargo();
        String anteriorCodCargo = trabajador.getCargo().getCodCargo();
        String anteriorDescripcionCargo = trabajador.getCargo().getCodCargo();
        int anteriorCodigoNomina = trabajador.getCodigoNomina();
        String anteriorCodSede = trabajador.getDependencia().getSede().getCodSede();
        String anteriorNombreSede = trabajador.getDependencia().getSede().getNombre();
        String anteriorCodDependencia = trabajador.getDependencia().getCodDependencia();
        String anteriorNombreDependencia = trabajador.getDependencia().getNombre();
        double anteriorSueldo = trabajador.getSueldoBasico();
        double anteriorCompensacion = sueldoPromedio.getPromedioCompensacion();
        double anteriorPrimasCargo = sueldoPromedio.getPromedioPrimasc();
        double anteriorPrimasTrabajador = sueldoPromedio.getPromedioPrimast();
        int anteriorGrado = trabajador.getCargo().getGrado();
        int anteriorPaso = trabajador.getPaso();
        String anteriorCodRegion = trabajador.getDependencia().getSede().getRegion().getCodRegion();
        String anteriorNombreRegion = trabajador.getDependencia().getSede().getRegion().getNombre();

        long idHistoricoCargos = this.registrosBusiness.agregarhistoricoCargos(pm, 
          registroCargos.getRegistro(), 
          causaMovimiento, 
          registroCargos.getCargo(), 
          trabajador.getDependencia(), 
          registroCargos.getCodigoNomina(), 
          registroCargos.getSituacion(), 
          "2", fechaMovimiento, 
          trabajador.getCedula(), 
          trabajador.getPersonal().getPrimerApellido(), 
          trabajador.getPersonal().getSegundoApellido(), 
          trabajador.getPersonal().getPrimerNombre(), 
          trabajador.getPersonal().getSegundoNombre(), 
          registroCargos.getHoras());

        Trabajador trabajador2 = new Trabajador();
        trabajador2 = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

        organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

        causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

        usuario = this.sistemaBusiness.findUsuarioById(idUsuario);
        try
        {
          sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
        } catch (Exception e) {
          ErrorSistema error = new ErrorSistema();
          error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
          throw error;
        }
        this.registrosBusiness.buscarProximaNomina(trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());

        Trayectoria trayectoria = new Trayectoria();
        trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador2, sueldoPromedio, 
          new Date(), numeroMovimiento, fechaMovimiento, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
          null, null, null, observaciones, usuario.getUsuario(), "0");
        pm.makePersistent(trayectoria);
        this.log.error("GRABO TRAYECTORIA");

        MovimientoSitp movimientoSitp = new MovimientoSitp();
        movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajador2, organismo, 
          causaMovimiento, sueldoPromedio, 
          numeroMovimiento, null, 
          fechaMovimiento, null, "0", "S", 
          usuario, null, null, null, 
          anteriorCodManualCargo, 
          anteriorCodCargo, 
          anteriorDescripcionCargo, 
          anteriorCodigoNomina, 
          anteriorCodSede, 
          anteriorNombreSede, 
          anteriorCodDependencia, 
          anteriorNombreDependencia, 
          anteriorSueldo, 
          anteriorCompensacion, 
          anteriorPrimasCargo, 
          anteriorPrimasTrabajador, 
          anteriorGrado, 
          anteriorPaso, 
          anteriorCodRegion, 
          anteriorNombreRegion, 
          observaciones, 0.0D, 0.0D, 0.0D);

        idMovimientoSitp = movimientoSitp.getIdMovimientoSitp();

        pm.makePersistent(movimientoSitp);

        this.log.error("GRABO MOVIMIENTOSITP");

        this.log.error("va terminar el proceso");
        this.txn.close();
        return idMovimientoSitp;
      }

      long idHistoricoCargos = this.registrosBusiness.agregarhistoricoCargos(pm, 
        registroCargos.getRegistro(), 
        causaMovimiento, 
        registroCargos.getCargo(), 
        registroCargos.getDependencia(), 
        registroCargos.getCodigoNomina(), 
        registroCargos.getSituacion(), 
        "2", fechaMovimiento, 
        0, 
        null, 
        null, 
        null, 
        null, 
        0.0D);

      this.txn.close();
      return idMovimientoSitp;
    }
    catch (ErrorSistema a)
    {
      this.txn.close();
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
  }
}