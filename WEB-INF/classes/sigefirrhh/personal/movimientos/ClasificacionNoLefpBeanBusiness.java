package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBusiness;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.procesoNomina.CalcularSueldosPromedioBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.ConceptoVariable;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ClasificacionNoLefpBeanBusiness extends AbstractBeanBusiness
{
  Logger log = Logger.getLogger(ClasificacionNoLefpBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private CargoBusiness cargoBusiness = new CargoBusiness();

  private CalcularSueldosPromedioBeanBusiness calcularSueldosPromedioBeanBusiness = new CalcularSueldosPromedioBeanBusiness();

  public boolean actualizar(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idCargo, double sueldo, String observaciones, String pagarRetroactivo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.log.error("1");

      PersistenceManager pm = PMThread.getPM();

      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Organismo organismo = new Organismo();
      organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

      CausaMovimiento causaMovimiento = new CausaMovimiento();
      causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

      Cargo cargo = this.cargoBusiness.findCargoById(idCargo);

      RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(idRegistroCargos);

      registroCargos.setCargo(cargo);

      if (registroCargos.getSituacion().equals("O")) {
        this.log.error("ocupado");

        long idTrabajador = registroCargos.getTrabajador().getIdTrabajador();

        Trabajador trabajador = new Trabajador();
        trabajador = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

        trabajador.setCargo(cargo);
        trabajador.setSueldoBasico(sueldo);
        trabajador.setCausaMovimiento(causaMovimiento);

        SueldoPromedio sueldoPromedio = new SueldoPromedio();
        try {
          sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();

          this.log.error("leyo sueldo promedio");
        }
        catch (Exception e) {
          ErrorSistema error = new ErrorSistema();
          error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
          throw error;
        }

        int anteriorCodManualCargo = trabajador.getCargo().getManualCargo().getCodManualCargo();
        String anteriorCodCargo = trabajador.getCargo().getCodCargo();
        String anteriorDescripcionCargo = trabajador.getCargo().getCodCargo();
        int anteriorCodigoNomina = trabajador.getCodigoNomina();
        String anteriorCodSede = trabajador.getDependencia().getSede().getCodSede();
        String anteriorNombreSede = trabajador.getDependencia().getSede().getNombre();
        String anteriorCodDependencia = trabajador.getDependencia().getCodDependencia();
        String anteriorNombreDependencia = trabajador.getDependencia().getNombre();
        double anteriorSueldo = trabajador.getSueldoBasico();
        double anteriorCompensacion = sueldoPromedio.getPromedioCompensacion();
        double anteriorPrimasCargo = sueldoPromedio.getPromedioPrimasc();
        double anteriorPrimasTrabajador = sueldoPromedio.getPromedioPrimast();
        int anteriorGrado = trabajador.getCargo().getGrado();
        int anteriorPaso = trabajador.getPaso();
        String anteriorCodRegion = trabajador.getDependencia().getSede().getRegion().getCodRegion();
        String anteriorNombreRegion = trabajador.getDependencia().getSede().getRegion().getNombre();

        long idHistoricoCargos = this.registrosBusiness.agregarhistoricoCargos(pm, 
          registroCargos.getRegistro(), 
          causaMovimiento, 
          registroCargos.getCargo(), 
          trabajador.getDependencia(), 
          registroCargos.getCodigoNomina(), 
          registroCargos.getSituacion(), 
          "2", fechaMovimiento, 
          trabajador.getCedula(), 
          trabajador.getPersonal().getPrimerApellido(), 
          trabajador.getPersonal().getSegundoApellido(), 
          trabajador.getPersonal().getPrimerNombre(), 
          trabajador.getPersonal().getSegundoNombre(), 
          registroCargos.getHoras());
        this.log.error("antes de concepto fijo");

        this.registrosBusiness.actualizarConceptosFijosNuevoCargo(pm, trabajador, sueldo, registroCargos, "NA", 0.0D, 0);
        this.log.error("salio de concepto fijo");

        this.txn.close();

        this.calcularSueldosPromedioBeanBusiness.calcularUnTrabajadorParaMovimientos(trabajador, numeroMovimiento);

        this.txn.open();
        pm = PMThread.getPM();

        Trabajador trabajador2 = new Trabajador();
        trabajador2 = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

        organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

        causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);

        usuario = this.sistemaBusiness.findUsuarioById(idUsuario);
        try
        {
          sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajador.getIdTrabajador()).iterator().next();
        } catch (Exception e) {
          ErrorSistema error = new ErrorSistema();
          error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
          throw error;
        }
        this.registrosBusiness.buscarProximaNomina(trabajador.getTipoPersonal().getGrupoNomina().getIdGrupoNomina());
        if ((!trabajador.getTipoPersonal().getGrupoNomina().getPeriodicidad().equals("S")) && 
          (this.registrosBusiness.getFechaProximaNomina().compareTo(fechaMovimiento) > 0) && (pagarRetroactivo.equals("S")))
        {
          Collection colRetroactivo = new ArrayList();

          colRetroactivo.addAll(this.registrosBusiness.calcularRetroactivosNuevoCargo(trabajador2, fechaMovimiento));
          Iterator iterRetroactivo = colRetroactivo.iterator();
          while (iterRetroactivo.hasNext()) {
            ConceptoVariable conceptoVariable = (ConceptoVariable)iterRetroactivo.next();
            pm.makePersistent(conceptoVariable);
          }
          this.log.error("CALCULO RETROACTIVOS");
        }

        Trayectoria trayectoria = new Trayectoria();
        trayectoria = this.registrosBusiness.agregarTrayectoria(trabajador2, sueldoPromedio, 
          new Date(), numeroMovimiento, fechaMovimiento, null, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
          null, null, null, observaciones, usuario.getUsuario(), "4");
        pm.makePersistent(trayectoria);
        this.log.error("GRABO TRAYECTORIA");

        RegistroSitp registroSitp = new RegistroSitp();
        registroSitp = this.registrosBusiness.agregarRegistroSitp(trabajador2, organismo, 
          causaMovimiento, sueldoPromedio, 
          numeroMovimiento, null, 
          fechaMovimiento, "4", "S", 
          usuario, null, null, null, 
          anteriorCodManualCargo, 
          anteriorCodCargo, 
          anteriorDescripcionCargo, 
          anteriorCodigoNomina, 
          anteriorCodSede, 
          anteriorNombreSede, 
          anteriorCodDependencia, 
          anteriorNombreDependencia, 
          anteriorSueldo, 
          anteriorCompensacion, 
          anteriorPrimasCargo, 
          anteriorPrimasTrabajador, 
          anteriorGrado, 
          anteriorPaso, 
          anteriorCodRegion, 
          anteriorNombreRegion, 
          observaciones);
        pm.makePersistent(registroSitp);

        this.log.error("GRABO REGISTROSITP");

        this.txn.close();
        return true;
      }

      long idHistoricoCargos = this.registrosBusiness.agregarhistoricoCargos(pm, 
        registroCargos.getRegistro(), 
        causaMovimiento, 
        registroCargos.getCargo(), 
        registroCargos.getDependencia(), 
        registroCargos.getCodigoNomina(), 
        registroCargos.getSituacion(), 
        "2", fechaMovimiento, 
        0, 
        null, 
        null, 
        null, 
        null, 
        0.0D);

      this.txn.close();
      return true;
    }
    catch (ErrorSistema a)
    {
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
  }
}