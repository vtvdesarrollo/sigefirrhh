package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.base.registro.RegistroPersonal;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class CambioCargoNoLefpForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CambioCargoNoLefpForm.class.getName());
  private String observaciones;
  private Date fechaEgresoReal;
  private Date fechaSalidaNomina;
  private String remesa;
  private int numeroMovimiento;
  private String aumento = "PO";
  private double porcentaje = 10.0D;
  private int paso;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();

  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colConceptoTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colTrabajador;
  private String selectConceptoTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Collection colRegistroCargos;
  private Collection colCausaPersonal;
  private boolean showRegistroCargosAux;
  private boolean showFieldsAux;
  private RegistroCargos registroCargos;
  private boolean showButtonAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private Date fechaIngreso;
  private String selectIdRegistroCargos;
  private String pagarRetroactivo = "S";
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private boolean showData;
  private boolean showAumento;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public boolean isShowPorcentaje()
  {
    return (this.showAumento) && (this.aumento.equals("PO"));
  }
  public boolean isShowPaso() {
    return (this.showAumento) && (this.aumento.equals("NP"));
  }
  public boolean isShowAumento() {
    this.showAumento = false;

    if (this.showFieldsAux) {
      this.showAumento = true;
      return !this.registroCargos.getCargo().getTipoCargo().equals("5");
    }
    return false;
  }

  public boolean isShowRegistroCargos() {
    return (this.colRegistroCargos != null) && (this.showRegistroCargosAux);
  }
  public boolean isShowFields() {
    return this.showFieldsAux;
  }
  public Collection getColRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    while (iterator.hasNext()) {
      registroCargos = (RegistroCargos)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroCargos.getIdRegistroCargos()), 
        registroCargos.toString()));
    }

    return col;
  }

  public void changeRegistroCargos(ValueChangeEvent event) {
    long idRegistroCargos = Long.valueOf(
      (String)event.getNewValue()).longValue();

    if (idRegistroCargos != 0L) {
      this.showFieldsAux = true;
    }

    try
    {
      this.registroCargos = this.registroCargosFacade.findRegistroCargosById(idRegistroCargos);
      actualizarCampos();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarCampos()
  {
    try {
      this.showButtonAux = true;

      this.nombreDependencia = this.registroCargos.getDependencia().toString();
      this.nombreSede = this.registroCargos.getSede().toString();
      this.nombreRegion = this.registroCargos.getSede().getRegion().toString();
      this.descripcionCargo = this.registroCargos.getCargo().toString();
      this.grado = this.registroCargos.getCargo().getGrado();

      DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.registroCargos.getCargo().getManualCargo().getTabulador().getIdTabulador(), this.grado, this.registroCargos.getCargo().getSubGrado(), 1);
      this.sueldo = detalleTabulador.getMonto();
    }
    catch (Exception e)
    {
      this.nombreDependencia = null;
      this.nombreSede = null;
      this.nombreRegion = null;
      this.descripcionCargo = null;
      this.grado = 0;
      this.sueldo = 0.0D;
      this.numeroMovimiento = 0;
      this.remesa = null;
      this.fechaIngreso = null;
      this.showButtonAux = false;
    }
  }

  public void llenarRegistroCargos()
  {
    try
    {
      RegistroPersonal registroPersonal = (RegistroPersonal)this.registroNoGenFacade.findRegistroPersonalByTipoPersonal(this.trabajador.getTipoPersonal().getIdTipoPersonal()).iterator().next();

      this.colRegistroCargos = this.registroCargosFacade.findRegistroCargosByRegistroAndSituacion(registroPersonal.getRegistro().getIdRegistro(), "V");

      this.showRegistroCargosAux = true;
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String ejecutar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaIngreso == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }
      if (this.fechaPuntoCuenta == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de punto de cuenta del movimiento", ""));
        return null;
      }
      long idCausaPersonal = 5L;

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);
      this.movimientosNoGenFacade.cambioAscensoNoLefp(this.trabajador.getIdTrabajador(), this.fechaIngreso, idCausaPersonal, this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.sueldo, this.registroCargos.getIdRegistroCargos(), this.fechaPuntoCuenta, this.puntoCuenta, null, this.aumento, this.porcentaje, this.paso, this.pagarRetroactivo, this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
      this.show = false;
    }
    catch (ErrorSistema a)
    {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    abort();

    return "cancel";
  }

  public Collection getColCausaPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonal.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }
    log.error("paso 2");
    return col;
  }

  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;

    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador);
    }

    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public CambioCargoNoLefpForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.colCausaPersonal = new ArrayList();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        CambioCargoNoLefpForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public Collection getFindColTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.findColTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public Collection getColTrabajador() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd("S", "N", this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String showCambioCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();
      this.show = false;
      selectTrabajador();
      llenarRegistroCargos();
      this.show = true;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);

      long idClasificacionPersonal = this.trabajador.getTipoPersonal().getClasificacionPersonal().getIdClasificacionPersonal();

      this.colCausaPersonal = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(idClasificacionPersonal, 5L, "N");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;

    this.showResultTrabajador = false;
  }

  public String abort()
  {
    this.selected = false;
    resetResult();
    this.colCausaPersonal = null;
    this.colRegistroCargos = null;
    this.selectIdRegistroCargos = "0";
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaIngreso = null;
    this.sueldo = 0.0D;

    this.fechaPuntoCuenta = null;

    this.puntoCuenta = "";
    this.registroCargos = null;
    this.observaciones = "";
    return "cancel";
  }

  public boolean isShow() {
    return this.show;
  }

  public boolean isShowData() {
    return (this.show) && (this.selectedTrabajador);
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }

  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }

  public Date getFechaEgresoReal() {
    return this.fechaEgresoReal;
  }
  public void setFechaEgresoReal(Date fechaEgresoReal) {
    this.fechaEgresoReal = fechaEgresoReal;
  }
  public Date getFechaSalidaNomina() {
    return this.fechaSalidaNomina;
  }
  public void setFechaSalidaNomina(Date fechaSalidaNomina) {
    this.fechaSalidaNomina = fechaSalidaNomina;
  }

  public String getDescripcionCargo() {
    return this.descripcionCargo;
  }
  public void setDescripcionCargo(String descripcionCargo) {
    this.descripcionCargo = descripcionCargo;
  }
  public Date getFechaIngreso() {
    return this.fechaIngreso;
  }
  public void setFechaIngreso(Date fechaIngreso) {
    this.fechaIngreso = fechaIngreso;
  }
  public int getGrado() {
    return this.grado;
  }
  public void setGrado(int grado) {
    this.grado = grado;
  }
  public String getNombreDependencia() {
    return this.nombreDependencia;
  }
  public void setNombreDependencia(String nombreDependencia) {
    this.nombreDependencia = nombreDependencia;
  }
  public String getNombreRegion() {
    return this.nombreRegion;
  }
  public void setNombreRegion(String nombreRegion) {
    this.nombreRegion = nombreRegion;
  }
  public String getNombreSede() {
    return this.nombreSede;
  }
  public void setNombreSede(String nombreSede) {
    this.nombreSede = nombreSede;
  }
  public double getSueldo() {
    return this.sueldo;
  }
  public void setSueldo(double sueldo) {
    this.sueldo = sueldo;
  }
  public String getSelectIdRegistroCargos() {
    return this.selectIdRegistroCargos;
  }
  public void setSelectIdRegistroCargos(String selectIdRegistroCargos) {
    this.selectIdRegistroCargos = selectIdRegistroCargos;
  }
  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }
  public void setPagarRetroactivo(String pagarRetroactivo) {
    this.pagarRetroactivo = pagarRetroactivo;
  }

  public Date getFechaPuntoCuenta() {
    return this.fechaPuntoCuenta;
  }
  public void setFechaPuntoCuenta(Date fechaPuntoCuenta) {
    this.fechaPuntoCuenta = fechaPuntoCuenta;
  }
  public String getPuntoCuenta() {
    return this.puntoCuenta;
  }
  public void setPuntoCuenta(String puntoCuenta) {
    this.puntoCuenta = puntoCuenta;
  }
  public String getAumento() {
    return this.aumento;
  }
  public void setAumento(String aumento) {
    this.aumento = aumento;
  }
  public int getPaso() {
    return this.paso;
  }
  public void setPaso(int paso) {
    this.paso = paso;
  }
  public double getPorcentaje() {
    return this.porcentaje;
  }
  public void setPorcentaje(double porcentaje) {
    this.porcentaje = porcentaje;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
}