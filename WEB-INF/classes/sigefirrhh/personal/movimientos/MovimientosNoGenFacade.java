package sigefirrhh.personal.movimientos;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.sistema.Usuario;

public class MovimientosNoGenFacade extends MovimientosFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private MovimientosNoGenBusiness movimientosNoGenBusiness = new MovimientosNoGenBusiness();

  public long egresoTrabajadorLefp(long idTrabajador, Date fechaEgresoReal, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.egresoTrabajadorLefp(idTrabajador, fechaEgresoReal, fechaSalidaNomina, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, observaciones);
  }

  public boolean egresoTrabajadorRegistro(long idTrabajador, Date fechaEgresoReal, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.egresoTrabajadorRegistro(idTrabajador, fechaEgresoReal, fechaSalidaNomina, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, observaciones);
  }

  public boolean egresoTrabajadorSinRegistro(long idTrabajador, Date fechaEgresoReal, Date fechaSalidaNomina, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.egresoTrabajadorSinRegistro(idTrabajador, fechaEgresoReal, fechaSalidaNomina, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, observaciones);
  }

  public boolean cambioClasificacionSinRegistro(long idTrabajador, long idCargo, long idCausaMovimiento, int numeroMovimiento, Date fechaVigencia, Organismo organismo, long idUsuario, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.cambioClasificacionSinRegistro(idTrabajador, idCargo, idCausaMovimiento, numeroMovimiento, fechaVigencia, organismo, idUsuario, observaciones);
  }

  public boolean cambioCedula(long idTrabajador, int cedula, Organismo organismo, long idUsuario, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.cambioCedula(idTrabajador, cedula, organismo, idUsuario, observaciones);
  }

  public long ingresoTrabajadorLefp(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long usuario, Date fechaPuntoCuenta, String puntoCuenta, long idConcurso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.ingresoTrabajadorLefp(idPersonal, idTipoPersonal, idRegistroCargos, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, usuario, fechaPuntoCuenta, puntoCuenta, idConcurso, pagarRetroactivo, observaciones);
  }

  public long ingresoTrabajadorRegistro(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long usuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones, int paso)
    throws Exception
  {
    return this.movimientosNoGenBusiness.ingresoTrabajadorRegistro(idPersonal, idTipoPersonal, idRegistroCargos, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, pagarRetroactivo, observaciones, paso);
  }

  public long registrarContinuidadTrabajadorRegistro(long idTrabajador, long idTrabajadorEgresado)
    throws Exception
  {
    return this.movimientosNoGenBusiness.registrarContinuidadTrabajadorRegistro(idTrabajador, idTrabajadorEgresado);
  }

  public long reingresoTrabajadorLefp(long idPersonal, long idTipoPersonal, long idRegistroCargos, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long usuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.reingresoTrabajadorLefp(idPersonal, idTipoPersonal, idRegistroCargos, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, usuario, fechaPuntoCuenta, puntoCuenta, codConcurso, pagarRetroactivo, observaciones);
  }

  public long ingresoTrabajadorSinRegistro(long idPersonal, long idTipoPersonal, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, int numeroMovimiento, Remesa remesa, long idUsuario, long idDependencia, long idCargo, int codigoNomina, double horas, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.ingresoTrabajadorSinRegistro(idPersonal, idTipoPersonal, fechaIngreso, idCausaMovimiento, sueldoBasico, numeroMovimiento, remesa, idUsuario, idDependencia, idCargo, codigoNomina, horas, fechaPuntoCuenta, puntoCuenta, codConcurso, pagarRetroactivo, observaciones);
  }

  public boolean jubilacionTrabajadorSinRegistro(long idPersonal, long idTipoPersonal, Date fechaIngreso, long idCausaMovimiento, double sueldoBasico, Usuario usuario, long idDependencia, long idCargo, int codigoNomina, double porcentajeJubilacion)
    throws Exception
  {
    return this.movimientosNoGenBusiness.jubilacionTrabajadorSinRegistro(idPersonal, idTipoPersonal, fechaIngreso, idCausaMovimiento, sueldoBasico, usuario, idDependencia, idCargo, codigoNomina, porcentajeJubilacion);
  }

  public int findLastNumeroRemesa(long idOrganismo, int anio)
    throws Exception
  {
    return this.movimientosNoGenBusiness.findLastNumeroRemesa(idOrganismo, anio);
  }

  public Collection findRemesaByEstatus(long idOrganismo, String estatus, long idUsuario) throws Exception
  {
    try {
      this.txn.open();
      return this.movimientosNoGenBusiness.findRemesaByEstatus(idOrganismo, estatus, idUsuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientoSitpForRemesa(long idOrganismo) throws Exception
  {
    try { this.txn.open();
      return this.movimientosNoGenBusiness.findMovimientoSitpForRemesa(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientoSitpByRemesa(long idRemesa)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.movimientosNoGenBusiness.findMovimientoSitpByRemesa(idRemesa);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientoSitpByRemesaAndStatus(long idRemesa, String status) throws Exception
  {
    try {
      this.txn.open();
      return this.movimientosNoGenBusiness.findMovimientoSitpByRemesaAndStatus(idRemesa, status);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public int findLastNumeroMovimientoSitp(long idOrganismo, int anio)
    throws Exception
  {
    return this.movimientosNoGenBusiness.findLastNumeroMovimientoSitp(idOrganismo, anio);
  }

  public int findLastNumeroRegistroSitp(long idOrganismo, int anio)
    throws Exception
  {
    return this.movimientosNoGenBusiness.findLastNumeroRegistroSitp(idOrganismo, anio);
  }

  public long cambioDesignacionAscensoLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, double sueldo, long idRegistroCargos, Date fechaPuntoCuenta, String puntoCuenta, long idConcursoCargo, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.cambioDesignacionAscensoLefp(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, sueldo, idRegistroCargos, fechaPuntoCuenta, puntoCuenta, idConcursoCargo, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public long licenciaSuspensionLefp(long idTrabajador, Date fechaMovimiento, Date fechaCulminacion, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.licenciaSuspensionLefp(idTrabajador, fechaMovimiento, fechaCulminacion, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, observaciones);
  }

  public long aumentoSueldoLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.aumentoSueldoLefp(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, codConcurso, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public boolean aumentoSueldoConSinRegistro(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.aumentoSueldoConSinRegistro(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, codConcurso, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public boolean licenciaSuspensionConSinRegistro(long idTrabajador, Date fechaMovimiento, Date fechaCulminacion, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.licenciaSuspensionConSinRegistro(idTrabajador, fechaMovimiento, fechaCulminacion, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, observaciones);
  }

  public boolean cambioAscensoNoLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, double sueldo, long idRegistroCargos, Date fechaPuntoCuenta, String puntoCuenta, String codConcurso, String aumento, double porcentaje, int paso, String pagarRetroactivo, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.cambioAscensoNoLefp(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, sueldo, idRegistroCargos, fechaPuntoCuenta, puntoCuenta, codConcurso, aumento, porcentaje, paso, pagarRetroactivo, observaciones);
  }

  public long clasificacionLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idCargo, double sueldo, String observaciones, String pagarRetroactivo, String CodCargo)
    throws Exception
  {
    return this.movimientosNoGenBusiness.clasificacionLefp(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idCargo, sueldo, observaciones, pagarRetroactivo, CodCargo);
  }

  public boolean clasificacionNoLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idCargo, double sueldo, String observaciones, String pagarRetroactivo)
    throws Exception
  {
    return this.movimientosNoGenBusiness.clasificacionNoLefp(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idCargo, sueldo, observaciones, pagarRetroactivo);
  }

  public long reincorporacionLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.reincorporacionLefp(idTrabajador, fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo, idUsuario, fechaPuntoCuenta, puntoCuenta, observaciones);
  }

  public long trasladoLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idDependencia, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.trasladoLefp(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idDependencia, observaciones);
  }

  public boolean trasladoNoLefp(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idRegistroCargos, long idDependencia, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.trasladoNoLefp(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idRegistroCargos, idDependencia, observaciones);
  }

  public int trasladoMutuoNoLefp(long idRegistroCargos1, long idRegistroCargos2, Date fechaMovimiento, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, long idOrganismo, long idUsuario)
    throws Exception
  {
    return this.movimientosNoGenBusiness.trasladoMutuoNoLefp(idRegistroCargos1, idRegistroCargos2, fechaMovimiento, fechaPuntoCuenta, puntoCuenta, observaciones, idOrganismo, idUsuario);
  }

  public boolean trasladoSinRegistro(Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, long idTrabajador, long idDependencia, String observaciones)
    throws Exception
  {
    return this.movimientosNoGenBusiness.trasladoSinRegistro(fechaMovimiento, idCausaMovimiento, numeroMovimiento, remesa, organismo2, idUsuario, idTrabajador, idDependencia, observaciones);
  }

  public void enviarRemesa(Remesa remesa) throws Exception
  {
    this.movimientosNoGenBusiness.enviarRemesa(remesa);
  }

  public void recibirRemesa(long idOrganismo) throws Exception {
    this.movimientosNoGenBusiness.recibirRemesa(idOrganismo);
  }

  public JubilacionAux calcularJubilacion(long idTrabajador, long idTipoPersonal, String periodicidad, Date fechaVigencia, boolean especial) throws Exception {
    return this.movimientosNoGenBusiness.calcularJubilacion(idTrabajador, idTipoPersonal, periodicidad, fechaVigencia, especial);
  }

  public long jubilacionLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, double montoJubilacion, double porcentajeJubilacion, double sueldoPromedioJubilacion)
    throws Exception
  {
    return this.movimientosNoGenBusiness.jubilacionLefp(idTrabajador, fechaMovimiento, 
      idCausaMovimiento, numeroMovimiento, 
      organismo, idUsuario, fechaPuntoCuenta, 
      puntoCuenta, observaciones, 
      montoJubilacion, porcentajeJubilacion, sueldoPromedioJubilacion);
  }

  public long pensionLefp(long idTrabajador, Date fechaMovimiento, long idCausaMovimiento, int numeroMovimiento, Organismo organismo, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones, double montoJubilacion, double porcentajeJubilacion, double sueldoPromedioJubilacion)
    throws Exception
  {
    return this.movimientosNoGenBusiness.pensionLefp(idTrabajador, fechaMovimiento, 
      idCausaMovimiento, numeroMovimiento, 
      organismo, idUsuario, fechaPuntoCuenta, 
      puntoCuenta, observaciones, 
      montoJubilacion, porcentajeJubilacion, sueldoPromedioJubilacion);
  }
}