package sigefirrhh.personal.movimientos;

import java.io.Serializable;

public class RemesaPK
  implements Serializable
{
  public long idRemesa;

  public RemesaPK()
  {
  }

  public RemesaPK(long idRemesa)
  {
    this.idRemesa = idRemesa;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RemesaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RemesaPK thatPK)
  {
    return 
      this.idRemesa == thatPK.idRemesa;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRemesa)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRemesa);
  }
}