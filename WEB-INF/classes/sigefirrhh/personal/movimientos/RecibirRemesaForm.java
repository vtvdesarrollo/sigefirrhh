package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import java.io.Serializable;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class RecibirRemesaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RecibirRemesaForm.class.getName());
  private LoginSession login;
  private MovimientosNoGenFacade movimientosFacade = new MovimientosNoGenFacade();

  private boolean show2 = false;

  public RecibirRemesaForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public void refresh()
  {
  }

  public String recibir()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      try
      {
        this.movimientosFacade.recibirRemesa(this.login.getIdOrganismo());
        refresh();
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

        context.addMessage("success_add", new FacesMessage("Se recibieron con éxito"));
      }
      catch (Exception e) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se pudieron recibir las remesas", ""));
        log.error("Excepcion controlada:", e);
      }

      this.show2 = false;
      return null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }return null;
  }

  public String generar()
  {
    this.show2 = true;
    return null;
  }

  public String abort() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.show2 = false;
    return null;
  }

  public boolean isShow2() {
    return this.show2;
  }
}