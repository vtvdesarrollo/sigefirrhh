package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class EgresoTrabajadorJubiladoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(EgresoTrabajadorJubiladoForm.class.getName());
  private String observaciones;
  private Date fechaEgresoReal;
  private Date fechaSalidaNomina;
  private String remesa;
  private int numeroMovimiento;
  private Collection result;
  private boolean show;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private FrecuenciaTipoPersonal frecuenciaTipoPersonal;
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private String selectCausaPersonal;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colConceptoTipoPersonal;
  private Collection colFrecuenciaTipoPersonal;
  private Collection colTrabajador;
  private String selectConceptoTipoPersonal;
  private String selectFrecuenciaTipoPersonal;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Collection colCausaPersonal;
  private Object stateScrollTrabajador = null;
  private Object stateResultTrabajador = null;

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      long idCausaPersonal = Long.valueOf(
        this.selectCausaPersonal).longValue();

      CausaPersonal causaPersonal = this.registroNoGenFacade.findCausaPersonalById(idCausaPersonal);

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      boolean estado = this.movimientosNoGenFacade.egresoTrabajadorSinRegistro(this.trabajador.getIdTrabajador(), this.fechaEgresoReal, this.fechaSalidaNomina, causaPersonal.getCausaMovimiento().getIdCausaMovimiento(), this.numeroMovimiento, null, this.login.getOrganismo(), this.login.getIdUsuario(), this.observaciones);
      if ((estado = 1) != 0) {
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.trabajador, this.trabajador.getPersonal());

        context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
        this.show = false;
      }
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error", ""));
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public Collection getColCausaPersonal()
  {
    log.error("size CausaPersonal" + this.colCausaPersonal.size());
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonal.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }
    log.error("paso 2");
    return col;
  }

  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;

    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador);
    }

    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public EgresoTrabajadorJubiladoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    log.error("PASO1");
    this.colCausaPersonal = new ArrayList();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getColTrabajador() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColTipoPersonal = 
        this.definicionesNoGenFacade.findTipoPersonalPensionadoJubilado();
      log.error("-----------------------------------------size " + this.findColTipoPersonal.size());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonalAndEstatus(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNominaAndTipoPersonalAndEstatus(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina, "A");
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String showEgresoTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();

      this.show = true;
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
      long idClasificacionPersonal = this.trabajador.getTipoPersonal().getClasificacionPersonal().getIdClasificacionPersonal();

      this.colCausaPersonal = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimientoAndSujetoLefp(idClasificacionPersonal, 5L, "N");
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String Ejecutar()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    return "cancel";
  }

  public String abort()
  {
    this.selected = false;
    resetResult();

    return "cancel";
  }

  public String abortUpdate()
  {
    this.selected = false;
    this.result = null;
    this.showResult = false;

    return "cancel";
  }

  public boolean isShow() {
    return this.show;
  }

  public boolean isShowData() {
    return (this.show) && (this.selectedTrabajador);
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }

  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento()
  {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }

  public Date getFechaEgresoReal() {
    return this.fechaEgresoReal;
  }
  public void setFechaEgresoReal(Date fechaEgresoReal) {
    this.fechaEgresoReal = fechaEgresoReal;
  }
  public Date getFechaSalidaNomina() {
    return this.fechaSalidaNomina;
  }
  public void setFechaSalidaNomina(Date fechaSalidaNomina) {
    this.fechaSalidaNomina = fechaSalidaNomina;
  }

  public String getSelectCausaPersonal()
  {
    return this.selectCausaPersonal;
  }

  public void setSelectCausaPersonal(String string)
  {
    this.selectCausaPersonal = string;
  }

  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
}