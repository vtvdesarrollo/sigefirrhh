package sigefirrhh.personal.movimientos;

import eforserver.presentation.Form;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.DetalleTabulador;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.cargo.ManualPersonal;
import sigefirrhh.base.cargo.Tabulador;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaPersonal;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.base.registro.RegistroNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ReingresoTrabajadorSinRegistroForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReingresoTrabajadorSinRegistroForm.class.getName());
  private String observaciones;
  private String pagarRetroactivo;
  private boolean egresado;
  private Date fechaIngreso;
  private String remesa;
  private int numeroMovimiento;
  private int valor = 0;
  private Date fechaPuntoCuenta;
  private String puntoCuenta;
  private String codConcurso;
  private TipoPersonal tipoPersonal;
  private int codigoNomina;
  private double horas;
  private Collection result;
  private boolean error;
  private boolean showData1;
  private LoginSession login;
  private DefinicionesFacadeExtend definicionesFacade = new DefinicionesFacadeExtend();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();

  private TrabajadorNoGenFacade trabajadorNoGenFacade = new TrabajadorNoGenFacade();
  private RegistroNoGenFacade registroNoGenFacade = new RegistroNoGenFacade();
  private MovimientosNoGenFacade movimientosNoGenFacade = new MovimientosNoGenFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private CargoNoGenFacade cargoNoGenFacade = new CargoNoGenFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private Personal personal;
  private Tabulador tabulador;
  private Cargo cargo;
  private String findSelectTrabajadorIdTipoPersonal;
  private String selectCausaPersonal;
  private Collection colTipoPersonal;
  private Collection colCausaPersonal;
  private Collection colRegion;
  private Collection colDependencia;
  private Collection colSede;
  private Collection colManualCargo = new ArrayList();
  private Collection colCargo;
  private int findPersonalCedula;
  private String selectIdTipoPersonal;
  private String selectIdCausaPersonal;
  private String selectIdRegistroCargos;
  private String selectIdRegion;
  private String selectIdSede;
  private String selectIdDependencia;
  private String selectIdManualCargo;
  private String selectIdCargo;
  private RegistroCargos registroCargos;
  private boolean showFieldsAux;
  private boolean showRegistroCargosAux;
  private String nombreSede;
  private String nombreDependencia;
  private String descripcionCargo;
  private int grado;
  private String nombreRegion;
  private double sueldo;
  private CausaPersonal causaPersonal;
  private boolean showButtonAux;

  public String getSelectIdCargo()
  {
    return this.selectIdCargo;
  }
  public void setSelectIdCargo(String selectIdCargo) {
    this.selectIdCargo = selectIdCargo;
  }
  public String getSelectIdDependencia() {
    return this.selectIdDependencia;
  }
  public void setSelectIdDependencia(String selectIdDependencia) {
    this.selectIdDependencia = selectIdDependencia;
  }
  public String getSelectIdManualCargo() {
    return this.selectIdManualCargo;
  }
  public void setSelectIdManualCargo(String selectIdManualCargo) {
    this.selectIdManualCargo = selectIdManualCargo;
  }
  public String getSelectIdRegion() {
    return this.selectIdRegion;
  }
  public void setSelectIdRegion(String selectIdRegion) {
    this.selectIdRegion = selectIdRegion;
  }
  public String getSelectIdSede() {
    return this.selectIdSede;
  }
  public void setSelectIdSede(String selectIdSede) {
    this.selectIdSede = selectIdSede;
  }

  public boolean isShowRegion()
  {
    return this.showData1;
  }
  public boolean isShowSede() {
    return this.colSede != null;
  }
  public boolean isShowDependencia() {
    return this.colDependencia != null;
  }
  public boolean isShowManualCargo() {
    return this.colManualCargo != null;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public boolean isShowCausaPersonal() {
    return (this.colCausaPersonal != null) && (this.showData1);
  }
  public boolean isShowFields() {
    return (this.showData1) && (this.showFieldsAux);
  }
  public boolean isShowButton() {
    return this.showButtonAux;
  }

  public String redirectPersonal() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    try
    {
      externalContext.redirect("/sigefirrhh/sigefirrhh/personal/expediente/Personal.jsf");
      context.responseComplete();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }
    return "success";
  }
  public Collection getColRegion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public String ejecutar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      if (this.fechaIngreso == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir la fecha de vigencia del movimiento", ""));
        return null;
      }

      int valido = this.trabajadorNoGenFacade.validarFechaIngreso(this.personal.getIdPersonal(), this.login.getIdOrganismo(), this.fechaIngreso);
      if (valido == 1) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha de reingreso debe ser mayor a la última fecha de egreso del trabajador en el organismo", ""));
        return null;
      }

      long idCausaPersonal = Long.valueOf(
        this.selectIdCausaPersonal).longValue();

      CausaPersonal causaPersonal = this.registroNoGenFacade.findCausaPersonalById(idCausaPersonal);

      this.numeroMovimiento = (this.movimientosNoGenFacade.findLastNumeroRegistroSitp(this.login.getIdOrganismo(), new Date().getYear() + 1900) + 1);

      this.movimientosNoGenFacade.ingresoTrabajadorSinRegistro(this.personal.getIdPersonal(), this.tipoPersonal.getIdTipoPersonal(), this.fechaIngreso, causaPersonal.getCausaMovimiento().getIdCausaMovimiento(), this.sueldo, this.numeroMovimiento, null, this.login.getIdUsuario(), Long.valueOf(this.selectIdDependencia).longValue(), Long.valueOf(this.selectIdCargo).longValue(), this.codigoNomina, this.horas, this.fechaPuntoCuenta, this.puntoCuenta, this.codConcurso, this.pagarRetroactivo, this.observaciones);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P', this.personal, this.personal);

      context.addMessage("success_add", new FacesMessage("Se procesó con éxito"));
    }
    catch (ErrorSistema a) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, a.getDescription(), ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
    }

    abort();

    return "cancel";
  }

  public void changeRegion(ValueChangeEvent event)
  {
    long idRegion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colSede = this.estructuraFacade.findSedeByRegion(idRegion, this.login.getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeSede(ValueChangeEvent event)
  {
    long idSede = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colDependencia = this.estructuraFacade.findDependenciaBySede(idSede, this.login.getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeManualCargo(ValueChangeEvent event)
  {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;

      this.colCargo = this.cargoNoGenFacade.findCargoByManualCargo(idManualCargo);
      ManualCargo manualCargo = this.cargoNoGenFacade.findManualCargoById(idManualCargo);

      this.tabulador = this.cargoNoGenFacade.findTabuladorById(manualCargo.getTabulador().getIdTabulador());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeCargo(ValueChangeEvent event)
  {
    long idCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if ((idCargo != 0L) && 
        (this.tabulador != null))
      {
        this.cargo = this.cargoNoGenFacade.findCargoById(idCargo);
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  private void llenarCausaPersonal() {
    try {
      Collection colManualPersonal = this.cargoNoGenFacade.findManualPersonalByTipoPersonal(this.tipoPersonal.getIdTipoPersonal());
      Iterator iterManualPersonal = colManualPersonal.iterator();
      while (iterManualPersonal.hasNext()) {
        ManualPersonal manualPersonal = (ManualPersonal)iterManualPersonal.next();

        ManualCargo manualCargo = this.cargoNoGenFacade.findManualCargoById(manualPersonal.getManualCargo().getIdManualCargo());
        this.colManualCargo.add(manualCargo);
      }

      long idClasificacionPersonal = this.tipoPersonal.getClasificacionPersonal().getIdClasificacionPersonal();
      this.colCausaPersonal = this.registroNoGenFacade.findCausaPersonalByClasificacionPersonalAndCausaMovimiento(idClasificacionPersonal, 2L);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void actualizarCampos()
  {
    try {
      this.codigoNomina = (this.trabajadorNoGenFacade.buscarUltimoCodigoNomina(this.tipoPersonal.getIdTipoPersonal()) + 1);
      DetalleTabulador detalleTabulador = this.cargoNoGenFacade.findDetalleTabuladorForRegistroCargos(this.tabulador.getIdTabulador(), this.cargo.getGrado(), this.cargo.getSubGrado(), 1);
      this.sueldo = 0.0D;
      this.sueldo = detalleTabulador.getMonto();
    }
    catch (Exception localException) {
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    FacesContext context = FacesContext.getCurrentInstance();

    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(idTipoPersonal);

      this.valor = this.trabajadorNoGenFacade.verificarSiTrabajadorPuedeReingresar(this.personal.getIdPersonal(), this.login.getIdOrganismo(), this.tipoPersonal.getIdTipoPersonal());
      if (this.valor == 1) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Trabajador activo en tipo de personal restringido", ""));
        return;
      }if (this.valor == 2) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Trabajador ya egresado, utilizar opción de reingreso", ""));
        return;
      }if (this.valor == 3) {
        this.egresado = true;
        return;
      }
      llenarCausaPersonal();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void continuarConEgresado()
  {
    this.egresado = false;
    llenarCausaPersonal();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }
  public Collection getColDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }

    return col;
  }
  public Collection getColSede() {
    Collection col = new ArrayList();
    Iterator iterator = this.colSede.iterator();
    Sede sede = null;
    while (iterator.hasNext()) {
      sede = (Sede)iterator.next();
      col.add(new SelectItem(
        String.valueOf(sede.getIdSede()), 
        sede.toString()));
    }

    return col;
  }
  public Collection getColManualCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargo.iterator();
    ManualCargo manualCargo = null;
    while (iterator.hasNext()) {
      manualCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargo.getIdManualCargo()), 
        manualCargo.toString()));
    }

    return col;
  }
  public Collection getColCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }

    return col;
  }
  public Collection getColCausaPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaPersonal.iterator();
    CausaPersonal causaPersonal = null;
    while (iterator.hasNext()) {
      causaPersonal = (CausaPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaPersonal.getIdCausaPersonal()), 
        causaPersonal.toString()));
    }
    return col;
  }

  public ReingresoTrabajadorSinRegistroForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.colCausaPersonal = new ArrayList();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReingresoTrabajadorSinRegistroForm.this.actualizarCampos();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
    refresh();
  }

  public void refresh()
  {
    try
    {
      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByManejaRacAndAprobacionMpd(
        "N", "N", this.login.getIdUsuario(), this.login.getAdministrador());

      this.colRegion = 
        this.estructuraFacade.findAllRegion();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula() { FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.error = false;
      this.personal = 
        ((Personal)this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo()).iterator().next());
      this.showData1 = true;
    } catch (Exception e) {
      this.showData1 = false;
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Los datos personales no estan registrados ", ""));
      this.error = true;
    }

    this.findSelectTrabajadorIdTipoPersonal = null;
    this.selectCausaPersonal = null;
    return null;
  }

  public String abort()
  {
    this.egresado = false;
    this.showData1 = false;
    this.colCausaPersonal = null;
    this.selectIdTipoPersonal = "0";
    this.selectIdRegistroCargos = "0";
    this.selectIdCausaPersonal = "0";
    this.descripcionCargo = null;
    this.showFieldsAux = false;
    this.nombreRegion = null;
    this.nombreSede = null;
    this.nombreDependencia = null;
    this.grado = 0;
    this.fechaIngreso = null;
    this.sueldo = 0.0D;
    this.numeroMovimiento = 0;
    this.fechaPuntoCuenta = null;
    this.codConcurso = null;
    this.puntoCuenta = "";
    this.observaciones = "";
    return "cancel";
  }

  public boolean isShowData1() {
    return this.showData1;
  }

  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getNumeroMovimiento() {
    return this.numeroMovimiento;
  }
  public void setNumeroMovimiento(int numeroMovimiento) {
    this.numeroMovimiento = numeroMovimiento;
  }
  public String getRemesa() {
    return this.remesa;
  }
  public void setRemesa(String remesa) {
    this.remesa = remesa;
  }

  public String getSelectCausaPersonal()
  {
    return this.selectCausaPersonal;
  }

  public void setSelectCausaPersonal(String string)
  {
    this.selectCausaPersonal = string;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public void setFindPersonalCedula(int findPersonalCedula) {
    this.findPersonalCedula = findPersonalCedula;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }

  public String getSelectIdCausaPersonal() {
    return this.selectIdCausaPersonal;
  }
  public void setSelectIdCausaPersonal(String selectIdCausaPersonal) {
    this.selectIdCausaPersonal = selectIdCausaPersonal;
  }

  public String getSelectIdRegistroCargos()
  {
    return this.selectIdRegistroCargos;
  }

  public void setSelectIdRegistroCargos(String string)
  {
    this.selectIdRegistroCargos = string;
  }

  public RegistroCargos getRegistroCargos()
  {
    return this.registroCargos;
  }

  public void setRegistroCargos(RegistroCargos cargos)
  {
    this.registroCargos = cargos;
  }

  public String getDescripcionCargo()
  {
    return this.descripcionCargo;
  }

  public int getGrado()
  {
    return this.grado;
  }

  public String getNombreDependencia()
  {
    return this.nombreDependencia;
  }

  public String getNombreRegion()
  {
    return this.nombreRegion;
  }

  public String getNombreSede()
  {
    return this.nombreSede;
  }

  public double getSueldo()
  {
    return this.sueldo;
  }

  public void setDescripcionCargo(String string)
  {
    this.descripcionCargo = string;
  }

  public void setGrado(int i)
  {
    this.grado = i;
  }

  public void setNombreDependencia(String string)
  {
    this.nombreDependencia = string;
  }

  public void setNombreRegion(String string)
  {
    this.nombreRegion = string;
  }

  public void setNombreSede(String string)
  {
    this.nombreSede = string;
  }

  public void setSueldo(double d)
  {
    this.sueldo = d;
  }

  public Date getFechaIngreso()
  {
    return this.fechaIngreso;
  }

  public void setFechaIngreso(Date date)
  {
    this.fechaIngreso = date;
  }

  public boolean isEgresado()
  {
    return this.egresado;
  }

  public void setEgresado(boolean b)
  {
    this.egresado = b;
  }

  public double getHoras()
  {
    return this.horas;
  }

  public void setHoras(double d)
  {
    this.horas = d;
  }

  public int getCodigoNomina()
  {
    return this.codigoNomina;
  }

  public void setCodigoNomina(int i)
  {
    this.codigoNomina = i;
  }

  public String getCodConcurso()
  {
    return this.codConcurso;
  }

  public Date getFechaPuntoCuenta()
  {
    return this.fechaPuntoCuenta;
  }

  public String getPuntoCuenta()
  {
    return this.puntoCuenta;
  }

  public void setCodConcurso(String string)
  {
    this.codConcurso = string;
  }

  public void setFechaPuntoCuenta(Date date)
  {
    this.fechaPuntoCuenta = date;
  }

  public void setPuntoCuenta(String string)
  {
    this.puntoCuenta = string;
  }

  public String getPagarRetroactivo() {
    return this.pagarRetroactivo;
  }
  public void setPagarRetroactivo(String pagarRetroactivo) {
    this.pagarRetroactivo = pagarRetroactivo;
  }
  public boolean isError() {
    return this.error;
  }
  public String getObservaciones() {
    return this.observaciones;
  }
  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }
}