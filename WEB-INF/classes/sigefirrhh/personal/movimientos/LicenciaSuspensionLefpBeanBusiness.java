package sigefirrhh.personal.movimientos;

import eforserver.business.AbstractBusiness;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.RegistroxBusiness;
import sigefirrhh.personal.expediente.Trayectoria;
import sigefirrhh.personal.expediente.TrayectoriaNoGenBeanBusiness;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBusiness;
import sigefirrhh.personal.trabajador.SueldoPromedio;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBusiness;
import sigefirrhh.sistema.SistemaBusiness;
import sigefirrhh.sistema.Usuario;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class LicenciaSuspensionLefpBeanBusiness extends AbstractBusiness
{
  Logger log = Logger.getLogger(LicenciaSuspensionLefpBeanBusiness.class.getName());

  private TrabajadorBusiness trabajadorBusiness = new TrabajadorBusiness();
  private RegistroCargosBusiness registroCargosBusiness = new RegistroCargosBusiness();
  private RegistroxBusiness registroBusiness = new RegistroxBusiness();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EstructuraBusiness estructuraBusiness = new EstructuraBusiness();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();
  private RegistrosBusiness registrosBusiness = new RegistrosBusiness();
  private TrayectoriaNoGenBeanBusiness trayectoriaBeanBusiness = new TrayectoriaNoGenBeanBusiness();

  public long actualizar(long idTrabajador, Date fechaMovimiento, Date fechaCulminacion, long idCausaMovimiento, int numeroMovimiento, Remesa remesa, Organismo organismo2, long idUsuario, Date fechaPuntoCuenta, String puntoCuenta, String observaciones)
    throws Exception
  {
    long idMovimientoSitp = 0L;
    this.registrosBusiness.validarFechas(fechaMovimiento, fechaPuntoCuenta);

    if ((idCausaMovimiento == 11L) || (idCausaMovimiento == 12L))
    {
      if (fechaMovimiento.compareTo(fechaCulminacion) > 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La fecha de Culminación no puede ser menor a la fecha de vigencia");
        throw error;
      }
      Calendar calFechaMovimiento = Calendar.getInstance();
      calFechaMovimiento.setTime(fechaMovimiento);
      calFechaMovimiento.add(1, 3);
      if (calFechaMovimiento.getTime().compareTo(fechaCulminacion) < 0) {
        ErrorSistema error = new ErrorSistema();
        error.setDescription("La fecha de Culminacion no puede ser mayor a 3 años");
        throw error;
      }
    }
    this.txn.open();

    PersistenceManager pm = PMThread.getPM();

    Organismo organismo = new Organismo();
    organismo = this.estructuraBusiness.findOrganismoById(organismo2.getIdOrganismo());

    CausaMovimiento causaMovimiento = new CausaMovimiento();

    causaMovimiento = this.registroBusiness.findCausaMovimientoById(idCausaMovimiento);
    this.log.error("id_causa_movimiento................................" + idCausaMovimiento);
    this.log.error("causa_movimiento..................................." + causaMovimiento);
    Trabajador trabajadorEdit = new Trabajador();
    trabajadorEdit = this.trabajadorBusiness.findTrabajadorById(idTrabajador);

    String mensaje = this.trayectoriaBeanBusiness.verificarTrayectoria(trabajadorEdit.getCedula(), causaMovimiento.getCodCausaMovimiento(), fechaMovimiento);
    if (mensaje != null) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription(mensaje);
      throw error;
    }
    this.log.error("no encontró problemas en trayectoria");

    SueldoPromedio sueldoPromedio = new SueldoPromedio();
    try {
      sueldoPromedio = (SueldoPromedio)this.trabajadorBusiness.findSueldoPromedioByTrabajador(trabajadorEdit.getIdTrabajador()).iterator().next();
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("No se consiguió el registro de Sueldo Promedio para este trabajador");
      throw error;
    }

    try
    {
      Usuario usuario = new Usuario();
      usuario = this.sistemaBusiness.findUsuarioById(idUsuario);

      Trayectoria trayectoria = new Trayectoria();
      trayectoria = this.registrosBusiness.agregarTrayectoria(trabajadorEdit, sueldoPromedio, 
        new Date(), numeroMovimiento, fechaMovimiento, fechaCulminacion, causaMovimiento, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 
        puntoCuenta, fechaPuntoCuenta, null, observaciones, usuario.getUsuario(), "0");
      pm.makePersistent(trayectoria);
      this.log.error("GRABO TRAYECTORIA");

      MovimientoSitp movimientoSitp = new MovimientoSitp();
      movimientoSitp = this.registrosBusiness.agregarMovimientoSitp(trabajadorEdit, organismo, 
        causaMovimiento, sueldoPromedio, 
        numeroMovimiento, remesa, 
        fechaMovimiento, fechaCulminacion, "0", "S", 
        usuario, fechaPuntoCuenta, puntoCuenta, null, 
        0, null, null, 0, null, null, null, null, 0.0D, 0.0D, 0.0D, 0.0D, 0, 0, null, null, observaciones, 0.0D, 0.0D, 0.0D);
      pm.makePersistent(movimientoSitp);

      idMovimientoSitp = movimientoSitp.getIdMovimientoSitp();

      this.log.error("GRABO MOVIMIENTOSITP");

      RegistroCargos registroCargos = this.registroCargosBusiness.findRegistroCargosById(trabajadorEdit.getRegistroCargos().getIdRegistroCargos());

      if (idCausaMovimiento == 11L)
        registroCargos.setCondicion("5");
      else if (idCausaMovimiento == 12L)
        registroCargos.setCondicion("6");
      else if (idCausaMovimiento == 13L) {
        registroCargos.setCondicion("3");
      }

      this.log.error("ACTUALIZO REGISTROCARGOS");
      trabajadorEdit.setCausaMovimiento(causaMovimiento);
      trabajadorEdit.setFechaUltimoMovimiento(fechaMovimiento);

      if (idCausaMovimiento == 11L) {
        trabajadorEdit.setEstatus("P");
        trabajadorEdit.setSituacion("9");
      } else if (idCausaMovimiento == 12L) {
        trabajadorEdit.setEstatus("A");
        trabajadorEdit.setSituacion("8");
      } else if (idCausaMovimiento == 13L) {
        trabajadorEdit.setEstatus("S");
        trabajadorEdit.setSituacion("12");
      }

      if (organismo.getAprobacionMpd().equals("S"))
        trabajadorEdit.setMovimiento("T");
      else {
        trabajadorEdit.setMovimiento("A");
      }

      this.log.error("PASO no 5");

      if (this.txn != null) this.txn.close(); 
    }
    catch (ErrorSistema a)
    {
      this.txn.close();
      a.printStackTrace();
      ErrorSistema error = new ErrorSistema();
      error.setDescription(a.getDescription());
      throw error;
    }
    catch (Exception e) {
      this.txn.close();
      this.log.error("Excepcion controlada:", e);
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Ocurrió un error");
      throw error;
    }
    return idMovimientoSitp;
  }
}