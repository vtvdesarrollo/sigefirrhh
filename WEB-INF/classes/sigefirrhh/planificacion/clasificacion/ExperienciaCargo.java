package sigefirrhh.planificacion.clasificacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;

public class ExperienciaCargo
  implements Serializable, PersistenceCapable
{
  private long idExperienciaCargo;
  private Cargo cargo;
  private Cargo cargoRequerido;
  private double peso;
  private int aniosExperiencia;
  private int mesesExperiencia;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosExperiencia", "cargo", "cargoRequerido", "idExperienciaCargo", "idSitp", "mesesExperiencia", "peso", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Long.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 26, 26, 24, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ExperienciaCargo()
  {
    jdoSetpeso(this, 0.0D);

    jdoSetaniosExperiencia(this, 0);

    jdoSetmesesExperiencia(this, 0);
  }

  public String toString()
  {
    return jdoGetcargoRequerido(this).getDescripcionCargo() + " - " + jdoGetpeso(this);
  }

  public int getAniosExperiencia()
  {
    return jdoGetaniosExperiencia(this);
  }

  public void setAniosExperiencia(int aniosExperiencia)
  {
    jdoSetaniosExperiencia(this, aniosExperiencia);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public Cargo getCargoRequerido()
  {
    return jdoGetcargoRequerido(this);
  }

  public void setCargoRequerido(Cargo cargoRequerido)
  {
    jdoSetcargoRequerido(this, cargoRequerido);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public int getMesesExperiencia()
  {
    return jdoGetmesesExperiencia(this);
  }

  public void setMesesExperiencia(int mesesExperiencia)
  {
    jdoSetmesesExperiencia(this, mesesExperiencia);
  }

  public double getPeso()
  {
    return jdoGetpeso(this);
  }

  public void setPeso(double peso)
  {
    jdoSetpeso(this, peso);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public long getIdExperienciaCargo()
  {
    return jdoGetidExperienciaCargo(this);
  }

  public void setIdExperienciaCargo(long l)
  {
    jdoSetidExperienciaCargo(this, l);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.clasificacion.ExperienciaCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ExperienciaCargo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ExperienciaCargo localExperienciaCargo = new ExperienciaCargo();
    localExperienciaCargo.jdoFlags = 1;
    localExperienciaCargo.jdoStateManager = paramStateManager;
    return localExperienciaCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ExperienciaCargo localExperienciaCargo = new ExperienciaCargo();
    localExperienciaCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localExperienciaCargo.jdoFlags = 1;
    localExperienciaCargo.jdoStateManager = paramStateManager;
    return localExperienciaCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosExperiencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargoRequerido);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idExperienciaCargo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesExperiencia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.peso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoRequerido = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idExperienciaCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.peso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ExperienciaCargo paramExperienciaCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.aniosExperiencia = paramExperienciaCargo.aniosExperiencia;
      return;
    case 1:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramExperienciaCargo.cargo;
      return;
    case 2:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargoRequerido = paramExperienciaCargo.cargoRequerido;
      return;
    case 3:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idExperienciaCargo = paramExperienciaCargo.idExperienciaCargo;
      return;
    case 4:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramExperienciaCargo.idSitp;
      return;
    case 5:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.mesesExperiencia = paramExperienciaCargo.mesesExperiencia;
      return;
    case 6:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.peso = paramExperienciaCargo.peso;
      return;
    case 7:
      if (paramExperienciaCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramExperienciaCargo.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ExperienciaCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ExperienciaCargo localExperienciaCargo = (ExperienciaCargo)paramObject;
    if (localExperienciaCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localExperienciaCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ExperienciaCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ExperienciaCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExperienciaCargoPK))
      throw new IllegalArgumentException("arg1");
    ExperienciaCargoPK localExperienciaCargoPK = (ExperienciaCargoPK)paramObject;
    localExperienciaCargoPK.idExperienciaCargo = this.idExperienciaCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExperienciaCargoPK))
      throw new IllegalArgumentException("arg1");
    ExperienciaCargoPK localExperienciaCargoPK = (ExperienciaCargoPK)paramObject;
    this.idExperienciaCargo = localExperienciaCargoPK.idExperienciaCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExperienciaCargoPK))
      throw new IllegalArgumentException("arg2");
    ExperienciaCargoPK localExperienciaCargoPK = (ExperienciaCargoPK)paramObject;
    localExperienciaCargoPK.idExperienciaCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExperienciaCargoPK))
      throw new IllegalArgumentException("arg2");
    ExperienciaCargoPK localExperienciaCargoPK = (ExperienciaCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localExperienciaCargoPK.idExperienciaCargo);
  }

  private static final int jdoGetaniosExperiencia(ExperienciaCargo paramExperienciaCargo)
  {
    if (paramExperienciaCargo.jdoFlags <= 0)
      return paramExperienciaCargo.aniosExperiencia;
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaCargo.aniosExperiencia;
    if (localStateManager.isLoaded(paramExperienciaCargo, jdoInheritedFieldCount + 0))
      return paramExperienciaCargo.aniosExperiencia;
    return localStateManager.getIntField(paramExperienciaCargo, jdoInheritedFieldCount + 0, paramExperienciaCargo.aniosExperiencia);
  }

  private static final void jdoSetaniosExperiencia(ExperienciaCargo paramExperienciaCargo, int paramInt)
  {
    if (paramExperienciaCargo.jdoFlags == 0)
    {
      paramExperienciaCargo.aniosExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.aniosExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramExperienciaCargo, jdoInheritedFieldCount + 0, paramExperienciaCargo.aniosExperiencia, paramInt);
  }

  private static final Cargo jdoGetcargo(ExperienciaCargo paramExperienciaCargo)
  {
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaCargo.cargo;
    if (localStateManager.isLoaded(paramExperienciaCargo, jdoInheritedFieldCount + 1))
      return paramExperienciaCargo.cargo;
    return (Cargo)localStateManager.getObjectField(paramExperienciaCargo, jdoInheritedFieldCount + 1, paramExperienciaCargo.cargo);
  }

  private static final void jdoSetcargo(ExperienciaCargo paramExperienciaCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramExperienciaCargo, jdoInheritedFieldCount + 1, paramExperienciaCargo.cargo, paramCargo);
  }

  private static final Cargo jdoGetcargoRequerido(ExperienciaCargo paramExperienciaCargo)
  {
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaCargo.cargoRequerido;
    if (localStateManager.isLoaded(paramExperienciaCargo, jdoInheritedFieldCount + 2))
      return paramExperienciaCargo.cargoRequerido;
    return (Cargo)localStateManager.getObjectField(paramExperienciaCargo, jdoInheritedFieldCount + 2, paramExperienciaCargo.cargoRequerido);
  }

  private static final void jdoSetcargoRequerido(ExperienciaCargo paramExperienciaCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.cargoRequerido = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramExperienciaCargo, jdoInheritedFieldCount + 2, paramExperienciaCargo.cargoRequerido, paramCargo);
  }

  private static final long jdoGetidExperienciaCargo(ExperienciaCargo paramExperienciaCargo)
  {
    return paramExperienciaCargo.idExperienciaCargo;
  }

  private static final void jdoSetidExperienciaCargo(ExperienciaCargo paramExperienciaCargo, long paramLong)
  {
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.idExperienciaCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramExperienciaCargo, jdoInheritedFieldCount + 3, paramExperienciaCargo.idExperienciaCargo, paramLong);
  }

  private static final int jdoGetidSitp(ExperienciaCargo paramExperienciaCargo)
  {
    if (paramExperienciaCargo.jdoFlags <= 0)
      return paramExperienciaCargo.idSitp;
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaCargo.idSitp;
    if (localStateManager.isLoaded(paramExperienciaCargo, jdoInheritedFieldCount + 4))
      return paramExperienciaCargo.idSitp;
    return localStateManager.getIntField(paramExperienciaCargo, jdoInheritedFieldCount + 4, paramExperienciaCargo.idSitp);
  }

  private static final void jdoSetidSitp(ExperienciaCargo paramExperienciaCargo, int paramInt)
  {
    if (paramExperienciaCargo.jdoFlags == 0)
    {
      paramExperienciaCargo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramExperienciaCargo, jdoInheritedFieldCount + 4, paramExperienciaCargo.idSitp, paramInt);
  }

  private static final int jdoGetmesesExperiencia(ExperienciaCargo paramExperienciaCargo)
  {
    if (paramExperienciaCargo.jdoFlags <= 0)
      return paramExperienciaCargo.mesesExperiencia;
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaCargo.mesesExperiencia;
    if (localStateManager.isLoaded(paramExperienciaCargo, jdoInheritedFieldCount + 5))
      return paramExperienciaCargo.mesesExperiencia;
    return localStateManager.getIntField(paramExperienciaCargo, jdoInheritedFieldCount + 5, paramExperienciaCargo.mesesExperiencia);
  }

  private static final void jdoSetmesesExperiencia(ExperienciaCargo paramExperienciaCargo, int paramInt)
  {
    if (paramExperienciaCargo.jdoFlags == 0)
    {
      paramExperienciaCargo.mesesExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.mesesExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramExperienciaCargo, jdoInheritedFieldCount + 5, paramExperienciaCargo.mesesExperiencia, paramInt);
  }

  private static final double jdoGetpeso(ExperienciaCargo paramExperienciaCargo)
  {
    if (paramExperienciaCargo.jdoFlags <= 0)
      return paramExperienciaCargo.peso;
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaCargo.peso;
    if (localStateManager.isLoaded(paramExperienciaCargo, jdoInheritedFieldCount + 6))
      return paramExperienciaCargo.peso;
    return localStateManager.getDoubleField(paramExperienciaCargo, jdoInheritedFieldCount + 6, paramExperienciaCargo.peso);
  }

  private static final void jdoSetpeso(ExperienciaCargo paramExperienciaCargo, double paramDouble)
  {
    if (paramExperienciaCargo.jdoFlags == 0)
    {
      paramExperienciaCargo.peso = paramDouble;
      return;
    }
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.peso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramExperienciaCargo, jdoInheritedFieldCount + 6, paramExperienciaCargo.peso, paramDouble);
  }

  private static final Date jdoGettiempoSitp(ExperienciaCargo paramExperienciaCargo)
  {
    if (paramExperienciaCargo.jdoFlags <= 0)
      return paramExperienciaCargo.tiempoSitp;
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
      return paramExperienciaCargo.tiempoSitp;
    if (localStateManager.isLoaded(paramExperienciaCargo, jdoInheritedFieldCount + 7))
      return paramExperienciaCargo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramExperienciaCargo, jdoInheritedFieldCount + 7, paramExperienciaCargo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ExperienciaCargo paramExperienciaCargo, Date paramDate)
  {
    if (paramExperienciaCargo.jdoFlags == 0)
    {
      paramExperienciaCargo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramExperienciaCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramExperienciaCargo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramExperienciaCargo, jdoInheritedFieldCount + 7, paramExperienciaCargo.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}