package sigefirrhh.planificacion.clasificacion;

import java.io.Serializable;

public class HabilidadCargoPK
  implements Serializable
{
  public long idHabilidadCargo;

  public HabilidadCargoPK()
  {
  }

  public HabilidadCargoPK(long idHabilidadCargo)
  {
    this.idHabilidadCargo = idHabilidadCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HabilidadCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HabilidadCargoPK thatPK)
  {
    return 
      this.idHabilidadCargo == thatPK.idHabilidadCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHabilidadCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHabilidadCargo);
  }
}