package sigefirrhh.planificacion.clasificacion;

import java.io.Serializable;

public class ProfesionCargoPK
  implements Serializable
{
  public long idProfesionCargo;

  public ProfesionCargoPK()
  {
  }

  public ProfesionCargoPK(long idProfesionCargo)
  {
    this.idProfesionCargo = idProfesionCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProfesionCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProfesionCargoPK thatPK)
  {
    return 
      this.idProfesionCargo == thatPK.idProfesionCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProfesionCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProfesionCargo);
  }
}