package sigefirrhh.planificacion.clasificacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.TipoHabilidad;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class HabilidadCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(HabilidadCargoForm.class.getName());
  private HabilidadCargo habilidadCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private PersonalFacade personalFacade = new PersonalFacade();
  private ClasificacionFacade clasificacionFacade = new ClasificacionFacade();
  private boolean showHabilidadCargoByCargo;
  private String findSelectManualCargoForCargo;
  private String findSelectCargo;
  private Collection findColManualCargoForCargo;
  private Collection findColCargo;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colTipoHabilidad;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectTipoHabilidad;
  private Object stateResultHabilidadCargoByCargo = null;

  public Collection getFindColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }
  public String getFindSelectManualCargoForCargo() {
    return this.findSelectManualCargoForCargo;
  }
  public void setFindSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.findSelectManualCargoForCargo = valManualCargoForCargo;
  }
  public void findChangeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColCargo = null;
      if (idManualCargo > 0L)
        this.findColCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowManualCargoForCargo() { return this.findColManualCargoForCargo != null; }

  public String getFindSelectCargo() {
    return this.findSelectCargo;
  }
  public void setFindSelectCargo(String valCargo) {
    this.findSelectCargo = valCargo;
  }

  public Collection getFindColCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }
  public boolean isFindShowCargo() {
    return this.findColCargo != null;
  }

  public String getSelectManualCargoForCargo()
  {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.habilidadCargo.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.habilidadCargo.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.habilidadCargo.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.habilidadCargo.setCargo(
          cargo);
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectTipoHabilidad() {
    return this.selectTipoHabilidad;
  }
  public void setSelectTipoHabilidad(String valTipoHabilidad) {
    Iterator iterator = this.colTipoHabilidad.iterator();
    TipoHabilidad tipoHabilidad = null;
    this.habilidadCargo.setTipoHabilidad(null);
    while (iterator.hasNext()) {
      tipoHabilidad = (TipoHabilidad)iterator.next();
      if (String.valueOf(tipoHabilidad.getIdTipoHabilidad()).equals(
        valTipoHabilidad)) {
        this.habilidadCargo.setTipoHabilidad(
          tipoHabilidad);
      }
    }
    this.selectTipoHabilidad = valTipoHabilidad;
  }
  public Collection getResult() {
    return this.result;
  }

  public HabilidadCargo getHabilidadCargo() {
    if (this.habilidadCargo == null) {
      this.habilidadCargo = new HabilidadCargo();
    }
    return this.habilidadCargo;
  }

  public HabilidadCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColTipoHabilidad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoHabilidad.iterator();
    TipoHabilidad tipoHabilidad = null;
    while (iterator.hasNext()) {
      tipoHabilidad = (TipoHabilidad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoHabilidad.getIdTipoHabilidad()), 
        tipoHabilidad.toString()));
    }
    return col;
  }

  public Collection getListNivel() {
    Collection col = new ArrayList();

    Iterator iterEntry = HabilidadCargo.LISTA_NIVEL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColManualCargoForCargo = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());

      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());

      this.colTipoHabilidad = 
        this.personalFacade.findAllTipoHabilidad();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findHabilidadCargoByCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.clasificacionFacade.findHabilidadCargoByCargo(Long.valueOf(this.findSelectCargo).longValue());
      this.showHabilidadCargoByCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showHabilidadCargoByCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;
    return null;
  }

  public boolean isShowHabilidadCargoByCargo() {
    return this.showHabilidadCargoByCargo;
  }

  public String selectHabilidadCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectTipoHabilidad = null;

    long idHabilidadCargo = 
      Long.parseLong((String)requestParameterMap.get("idHabilidadCargo"));
    try
    {
      this.habilidadCargo = 
        this.clasificacionFacade.findHabilidadCargoById(
        idHabilidadCargo);
      if (this.habilidadCargo.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.habilidadCargo.getCargo().getIdCargo());
      }
      if (this.habilidadCargo.getTipoHabilidad() != null) {
        this.selectTipoHabilidad = 
          String.valueOf(this.habilidadCargo.getTipoHabilidad().getIdTipoHabilidad());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;

      if (this.habilidadCargo.getCargo() != null) {
        long idCargo = 
          this.habilidadCargo.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.habilidadCargo.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.habilidadCargo = null;
    this.showHabilidadCargoByCargo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.habilidadCargo.getTiempoSitp() != null) && 
      (this.habilidadCargo.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.clasificacionFacade.addHabilidadCargo(
          this.habilidadCargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.habilidadCargo);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.clasificacionFacade.updateHabilidadCargo(
          this.habilidadCargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.habilidadCargo);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.clasificacionFacade.deleteHabilidadCargo(
        this.habilidadCargo);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.habilidadCargo);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.habilidadCargo = new HabilidadCargo();

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    this.selectTipoHabilidad = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.habilidadCargo.setIdHabilidadCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.clasificacion.HabilidadCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.habilidadCargo = new HabilidadCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}