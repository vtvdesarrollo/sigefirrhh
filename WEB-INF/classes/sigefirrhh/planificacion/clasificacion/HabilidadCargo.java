package sigefirrhh.planificacion.clasificacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.personal.TipoHabilidad;

public class HabilidadCargo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_NIVEL;
  private long idHabilidadCargo;
  private Cargo cargo;
  private TipoHabilidad tipoHabilidad;
  private String nivel;
  private double peso;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cargo", "idHabilidadCargo", "idSitp", "nivel", "peso", "tiempoSitp", "tipoHabilidad" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.TipoHabilidad") }; private static final byte[] jdoFieldFlags = { 26, 24, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.clasificacion.HabilidadCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HabilidadCargo());

    LISTA_NIVEL = 
      new LinkedHashMap();

    LISTA_NIVEL.put("A", "AVANZADO");
    LISTA_NIVEL.put("M", "MEDIO");
    LISTA_NIVEL.put("B", "BAJO");
  }

  public HabilidadCargo()
  {
    jdoSetnivel(this, "A");

    jdoSetpeso(this, 0.0D);

    jdoSetidSitp(this, 0);
  }

  public String toString()
  {
    return jdoGettipoHabilidad(this).getDescripcion() + " - " + jdoGetpeso(this);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public long getIdHabilidadCargo()
  {
    return jdoGetidHabilidadCargo(this);
  }

  public void setIdHabilidadCargo(long idHabilidadCargo)
  {
    jdoSetidHabilidadCargo(this, idHabilidadCargo);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNivel()
  {
    return jdoGetnivel(this);
  }

  public void setNivel(String nivel)
  {
    jdoSetnivel(this, nivel);
  }

  public double getPeso()
  {
    return jdoGetpeso(this);
  }

  public void setPeso(double peso)
  {
    jdoSetpeso(this, peso);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public TipoHabilidad getTipoHabilidad()
  {
    return jdoGettipoHabilidad(this);
  }

  public void setTipoHabilidad(TipoHabilidad tipoHabilidad)
  {
    jdoSettipoHabilidad(this, tipoHabilidad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HabilidadCargo localHabilidadCargo = new HabilidadCargo();
    localHabilidadCargo.jdoFlags = 1;
    localHabilidadCargo.jdoStateManager = paramStateManager;
    return localHabilidadCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HabilidadCargo localHabilidadCargo = new HabilidadCargo();
    localHabilidadCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHabilidadCargo.jdoFlags = 1;
    localHabilidadCargo.jdoStateManager = paramStateManager;
    return localHabilidadCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHabilidadCargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivel);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.peso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoHabilidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHabilidadCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.peso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoHabilidad = ((TipoHabilidad)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HabilidadCargo paramHabilidadCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHabilidadCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramHabilidadCargo.cargo;
      return;
    case 1:
      if (paramHabilidadCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idHabilidadCargo = paramHabilidadCargo.idHabilidadCargo;
      return;
    case 2:
      if (paramHabilidadCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramHabilidadCargo.idSitp;
      return;
    case 3:
      if (paramHabilidadCargo == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramHabilidadCargo.nivel;
      return;
    case 4:
      if (paramHabilidadCargo == null)
        throw new IllegalArgumentException("arg1");
      this.peso = paramHabilidadCargo.peso;
      return;
    case 5:
      if (paramHabilidadCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramHabilidadCargo.tiempoSitp;
      return;
    case 6:
      if (paramHabilidadCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoHabilidad = paramHabilidadCargo.tipoHabilidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HabilidadCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HabilidadCargo localHabilidadCargo = (HabilidadCargo)paramObject;
    if (localHabilidadCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHabilidadCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HabilidadCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HabilidadCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HabilidadCargoPK))
      throw new IllegalArgumentException("arg1");
    HabilidadCargoPK localHabilidadCargoPK = (HabilidadCargoPK)paramObject;
    localHabilidadCargoPK.idHabilidadCargo = this.idHabilidadCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HabilidadCargoPK))
      throw new IllegalArgumentException("arg1");
    HabilidadCargoPK localHabilidadCargoPK = (HabilidadCargoPK)paramObject;
    this.idHabilidadCargo = localHabilidadCargoPK.idHabilidadCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HabilidadCargoPK))
      throw new IllegalArgumentException("arg2");
    HabilidadCargoPK localHabilidadCargoPK = (HabilidadCargoPK)paramObject;
    localHabilidadCargoPK.idHabilidadCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HabilidadCargoPK))
      throw new IllegalArgumentException("arg2");
    HabilidadCargoPK localHabilidadCargoPK = (HabilidadCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localHabilidadCargoPK.idHabilidadCargo);
  }

  private static final Cargo jdoGetcargo(HabilidadCargo paramHabilidadCargo)
  {
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidadCargo.cargo;
    if (localStateManager.isLoaded(paramHabilidadCargo, jdoInheritedFieldCount + 0))
      return paramHabilidadCargo.cargo;
    return (Cargo)localStateManager.getObjectField(paramHabilidadCargo, jdoInheritedFieldCount + 0, paramHabilidadCargo.cargo);
  }

  private static final void jdoSetcargo(HabilidadCargo paramHabilidadCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidadCargo.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramHabilidadCargo, jdoInheritedFieldCount + 0, paramHabilidadCargo.cargo, paramCargo);
  }

  private static final long jdoGetidHabilidadCargo(HabilidadCargo paramHabilidadCargo)
  {
    return paramHabilidadCargo.idHabilidadCargo;
  }

  private static final void jdoSetidHabilidadCargo(HabilidadCargo paramHabilidadCargo, long paramLong)
  {
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidadCargo.idHabilidadCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramHabilidadCargo, jdoInheritedFieldCount + 1, paramHabilidadCargo.idHabilidadCargo, paramLong);
  }

  private static final int jdoGetidSitp(HabilidadCargo paramHabilidadCargo)
  {
    if (paramHabilidadCargo.jdoFlags <= 0)
      return paramHabilidadCargo.idSitp;
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidadCargo.idSitp;
    if (localStateManager.isLoaded(paramHabilidadCargo, jdoInheritedFieldCount + 2))
      return paramHabilidadCargo.idSitp;
    return localStateManager.getIntField(paramHabilidadCargo, jdoInheritedFieldCount + 2, paramHabilidadCargo.idSitp);
  }

  private static final void jdoSetidSitp(HabilidadCargo paramHabilidadCargo, int paramInt)
  {
    if (paramHabilidadCargo.jdoFlags == 0)
    {
      paramHabilidadCargo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidadCargo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramHabilidadCargo, jdoInheritedFieldCount + 2, paramHabilidadCargo.idSitp, paramInt);
  }

  private static final String jdoGetnivel(HabilidadCargo paramHabilidadCargo)
  {
    if (paramHabilidadCargo.jdoFlags <= 0)
      return paramHabilidadCargo.nivel;
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidadCargo.nivel;
    if (localStateManager.isLoaded(paramHabilidadCargo, jdoInheritedFieldCount + 3))
      return paramHabilidadCargo.nivel;
    return localStateManager.getStringField(paramHabilidadCargo, jdoInheritedFieldCount + 3, paramHabilidadCargo.nivel);
  }

  private static final void jdoSetnivel(HabilidadCargo paramHabilidadCargo, String paramString)
  {
    if (paramHabilidadCargo.jdoFlags == 0)
    {
      paramHabilidadCargo.nivel = paramString;
      return;
    }
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidadCargo.nivel = paramString;
      return;
    }
    localStateManager.setStringField(paramHabilidadCargo, jdoInheritedFieldCount + 3, paramHabilidadCargo.nivel, paramString);
  }

  private static final double jdoGetpeso(HabilidadCargo paramHabilidadCargo)
  {
    if (paramHabilidadCargo.jdoFlags <= 0)
      return paramHabilidadCargo.peso;
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidadCargo.peso;
    if (localStateManager.isLoaded(paramHabilidadCargo, jdoInheritedFieldCount + 4))
      return paramHabilidadCargo.peso;
    return localStateManager.getDoubleField(paramHabilidadCargo, jdoInheritedFieldCount + 4, paramHabilidadCargo.peso);
  }

  private static final void jdoSetpeso(HabilidadCargo paramHabilidadCargo, double paramDouble)
  {
    if (paramHabilidadCargo.jdoFlags == 0)
    {
      paramHabilidadCargo.peso = paramDouble;
      return;
    }
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidadCargo.peso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHabilidadCargo, jdoInheritedFieldCount + 4, paramHabilidadCargo.peso, paramDouble);
  }

  private static final Date jdoGettiempoSitp(HabilidadCargo paramHabilidadCargo)
  {
    if (paramHabilidadCargo.jdoFlags <= 0)
      return paramHabilidadCargo.tiempoSitp;
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidadCargo.tiempoSitp;
    if (localStateManager.isLoaded(paramHabilidadCargo, jdoInheritedFieldCount + 5))
      return paramHabilidadCargo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramHabilidadCargo, jdoInheritedFieldCount + 5, paramHabilidadCargo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(HabilidadCargo paramHabilidadCargo, Date paramDate)
  {
    if (paramHabilidadCargo.jdoFlags == 0)
    {
      paramHabilidadCargo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidadCargo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHabilidadCargo, jdoInheritedFieldCount + 5, paramHabilidadCargo.tiempoSitp, paramDate);
  }

  private static final TipoHabilidad jdoGettipoHabilidad(HabilidadCargo paramHabilidadCargo)
  {
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
      return paramHabilidadCargo.tipoHabilidad;
    if (localStateManager.isLoaded(paramHabilidadCargo, jdoInheritedFieldCount + 6))
      return paramHabilidadCargo.tipoHabilidad;
    return (TipoHabilidad)localStateManager.getObjectField(paramHabilidadCargo, jdoInheritedFieldCount + 6, paramHabilidadCargo.tipoHabilidad);
  }

  private static final void jdoSettipoHabilidad(HabilidadCargo paramHabilidadCargo, TipoHabilidad paramTipoHabilidad)
  {
    StateManager localStateManager = paramHabilidadCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramHabilidadCargo.tipoHabilidad = paramTipoHabilidad;
      return;
    }
    localStateManager.setObjectField(paramHabilidadCargo, jdoInheritedFieldCount + 6, paramHabilidadCargo.tipoHabilidad, paramTipoHabilidad);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}