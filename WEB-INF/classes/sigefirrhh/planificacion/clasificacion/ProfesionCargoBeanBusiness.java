package sigefirrhh.planificacion.clasificacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.personal.Profesion;
import sigefirrhh.base.personal.ProfesionBeanBusiness;

public class ProfesionCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addProfesionCargo(ProfesionCargo profesionCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ProfesionCargo profesionCargoNew = 
      (ProfesionCargo)BeanUtils.cloneBean(
      profesionCargo);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (profesionCargoNew.getCargo() != null) {
      profesionCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        profesionCargoNew.getCargo().getIdCargo()));
    }

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (profesionCargoNew.getProfesion() != null) {
      profesionCargoNew.setProfesion(
        profesionBeanBusiness.findProfesionById(
        profesionCargoNew.getProfesion().getIdProfesion()));
    }
    pm.makePersistent(profesionCargoNew);
  }

  public void updateProfesionCargo(ProfesionCargo profesionCargo) throws Exception
  {
    ProfesionCargo profesionCargoModify = 
      findProfesionCargoById(profesionCargo.getIdProfesionCargo());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (profesionCargo.getCargo() != null) {
      profesionCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        profesionCargo.getCargo().getIdCargo()));
    }

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (profesionCargo.getProfesion() != null) {
      profesionCargo.setProfesion(
        profesionBeanBusiness.findProfesionById(
        profesionCargo.getProfesion().getIdProfesion()));
    }

    BeanUtils.copyProperties(profesionCargoModify, profesionCargo);
  }

  public void deleteProfesionCargo(ProfesionCargo profesionCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ProfesionCargo profesionCargoDelete = 
      findProfesionCargoById(profesionCargo.getIdProfesionCargo());
    pm.deletePersistent(profesionCargoDelete);
  }

  public ProfesionCargo findProfesionCargoById(long idProfesionCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idProfesionCargo == pIdProfesionCargo";
    Query query = pm.newQuery(ProfesionCargo.class, filter);

    query.declareParameters("long pIdProfesionCargo");

    parameters.put("pIdProfesionCargo", new Long(idProfesionCargo));

    Collection colProfesionCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colProfesionCargo.iterator();
    return (ProfesionCargo)iterator.next();
  }

  public Collection findProfesionCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent profesionCargoExtent = pm.getExtent(
      ProfesionCargo.class, true);
    Query query = pm.newQuery(profesionCargoExtent);
    query.setOrdering("peso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(ProfesionCargo.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("peso ascending");

    Collection colProfesionCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProfesionCargo);

    return colProfesionCargo;
  }
}