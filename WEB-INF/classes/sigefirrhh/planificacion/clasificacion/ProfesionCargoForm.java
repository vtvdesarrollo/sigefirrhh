package sigefirrhh.planificacion.clasificacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.Profesion;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ProfesionCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProfesionCargoForm.class.getName());
  private ProfesionCargo profesionCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private PersonalFacade personalFacade = new PersonalFacade();
  private ClasificacionFacade clasificacionFacade = new ClasificacionFacade();
  private boolean showProfesionCargoByCargo;
  private String findSelectManualCargoForCargo;
  private String findSelectCargo;
  private Collection findColManualCargoForCargo;
  private Collection findColCargo;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colProfesion;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectProfesion;
  private Object stateResultProfesionCargoByCargo = null;

  public Collection getFindColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }
  public String getFindSelectManualCargoForCargo() {
    return this.findSelectManualCargoForCargo;
  }
  public void setFindSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.findSelectManualCargoForCargo = valManualCargoForCargo;
  }
  public void findChangeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColCargo = null;
      if (idManualCargo > 0L)
        this.findColCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowManualCargoForCargo() { return this.findColManualCargoForCargo != null; }

  public String getFindSelectCargo() {
    return this.findSelectCargo;
  }
  public void setFindSelectCargo(String valCargo) {
    this.findSelectCargo = valCargo;
  }

  public Collection getFindColCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }
  public boolean isFindShowCargo() {
    return this.findColCargo != null;
  }

  public String getSelectManualCargoForCargo()
  {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.profesionCargo.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.profesionCargo.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.profesionCargo.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.profesionCargo.setCargo(
          cargo);
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectProfesion() {
    return this.selectProfesion;
  }
  public void setSelectProfesion(String valProfesion) {
    Iterator iterator = this.colProfesion.iterator();
    Profesion profesion = null;
    this.profesionCargo.setProfesion(null);
    while (iterator.hasNext()) {
      profesion = (Profesion)iterator.next();
      if (String.valueOf(profesion.getIdProfesion()).equals(
        valProfesion)) {
        this.profesionCargo.setProfesion(
          profesion);
      }
    }
    this.selectProfesion = valProfesion;
  }
  public Collection getResult() {
    return this.result;
  }

  public ProfesionCargo getProfesionCargo() {
    if (this.profesionCargo == null) {
      this.profesionCargo = new ProfesionCargo();
    }
    return this.profesionCargo;
  }

  public ProfesionCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColProfesion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colProfesion.iterator();
    Profesion profesion = null;
    while (iterator.hasNext()) {
      profesion = (Profesion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(profesion.getIdProfesion()), 
        profesion.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColManualCargoForCargo = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());

      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());

      this.colProfesion = 
        this.personalFacade.findAllProfesion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findProfesionCargoByCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.clasificacionFacade.findProfesionCargoByCargo(Long.valueOf(this.findSelectCargo).longValue());
      this.showProfesionCargoByCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProfesionCargoByCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;
    return null;
  }

  public boolean isShowProfesionCargoByCargo() {
    return this.showProfesionCargoByCargo;
  }

  public String selectProfesionCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectProfesion = null;

    long idProfesionCargo = 
      Long.parseLong((String)requestParameterMap.get("idProfesionCargo"));
    try
    {
      this.profesionCargo = 
        this.clasificacionFacade.findProfesionCargoById(
        idProfesionCargo);
      if (this.profesionCargo.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.profesionCargo.getCargo().getIdCargo());
      }
      if (this.profesionCargo.getProfesion() != null) {
        this.selectProfesion = 
          String.valueOf(this.profesionCargo.getProfesion().getIdProfesion());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;

      if (this.profesionCargo.getCargo() != null) {
        long idCargo = 
          this.profesionCargo.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.profesionCargo.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.profesionCargo = null;
    this.showProfesionCargoByCargo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.profesionCargo.getTiempoSitp() != null) && 
      (this.profesionCargo.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.clasificacionFacade.addProfesionCargo(
          this.profesionCargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.profesionCargo);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.clasificacionFacade.updateProfesionCargo(
          this.profesionCargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.profesionCargo);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.clasificacionFacade.deleteProfesionCargo(
        this.profesionCargo);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.profesionCargo);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.profesionCargo = new ProfesionCargo();

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    this.selectProfesion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.profesionCargo.setIdProfesionCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.clasificacion.ProfesionCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.profesionCargo = new ProfesionCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}