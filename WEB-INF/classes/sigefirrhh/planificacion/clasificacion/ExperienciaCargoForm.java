package sigefirrhh.planificacion.clasificacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ExperienciaCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ExperienciaCargoForm.class.getName());
  private ExperienciaCargo experienciaCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private ClasificacionFacade clasificacionFacade = new ClasificacionFacade();
  private boolean showExperienciaCargoByCargo;
  private boolean showExperienciaCargoByCargoRequerido;
  private String findSelectManualCargoForCargo;
  private String findSelectCargo;
  private String findSelectManualCargoForCargoRequerido;
  private String findSelectCargoRequerido;
  private Collection findColManualCargoForCargo;
  private Collection findColCargo;
  private Collection findColManualCargoForCargoRequerido;
  private Collection findColCargoRequerido;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colManualCargoForCargoRequerido;
  private Collection colCargoRequerido;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectManualCargoForCargoRequerido;
  private String selectCargoRequerido;
  private Object stateResultExperienciaCargoByCargo = null;

  private Object stateResultExperienciaCargoByCargoRequerido = null;

  public Collection getFindColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }
  public String getFindSelectManualCargoForCargo() {
    return this.findSelectManualCargoForCargo;
  }
  public void setFindSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.findSelectManualCargoForCargo = valManualCargoForCargo;
  }
  public void findChangeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColCargo = null;
      if (idManualCargo > 0L)
        this.findColCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowManualCargoForCargo() { return this.findColManualCargoForCargo != null; }

  public String getFindSelectCargo() {
    return this.findSelectCargo;
  }
  public void setFindSelectCargo(String valCargo) {
    this.findSelectCargo = valCargo;
  }

  public Collection getFindColCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }
  public boolean isFindShowCargo() {
    return this.findColCargo != null;
  }
  public Collection getFindColManualCargoForCargoRequerido() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargoForCargoRequerido.iterator();
    ManualCargo manualCargoForCargoRequerido = null;
    while (iterator.hasNext()) {
      manualCargoForCargoRequerido = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargoRequerido.getIdManualCargo()), 
        manualCargoForCargoRequerido.toString()));
    }
    return col;
  }
  public String getFindSelectManualCargoForCargoRequerido() {
    return this.findSelectManualCargoForCargoRequerido;
  }
  public void setFindSelectManualCargoForCargoRequerido(String valManualCargoForCargoRequerido) {
    this.findSelectManualCargoForCargoRequerido = valManualCargoForCargoRequerido;
  }
  public void findChangeManualCargoForCargoRequerido(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColCargoRequerido = null;
      if (idManualCargo > 0L)
        this.findColCargoRequerido = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowManualCargoForCargoRequerido() { return this.findColManualCargoForCargoRequerido != null; }

  public String getFindSelectCargoRequerido() {
    return this.findSelectCargoRequerido;
  }
  public void setFindSelectCargoRequerido(String valCargoRequerido) {
    this.findSelectCargoRequerido = valCargoRequerido;
  }

  public Collection getFindColCargoRequerido() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCargoRequerido.iterator();
    Cargo cargoRequerido = null;
    while (iterator.hasNext()) {
      cargoRequerido = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargoRequerido.getIdCargo()), 
        cargoRequerido.toString()));
    }
    return col;
  }
  public boolean isFindShowCargoRequerido() {
    return this.findColCargoRequerido != null;
  }

  public String getSelectManualCargoForCargo()
  {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.experienciaCargo.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.experienciaCargo.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.experienciaCargo.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.experienciaCargo.setCargo(
          cargo);
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectManualCargoForCargoRequerido() {
    return this.selectManualCargoForCargoRequerido;
  }
  public void setSelectManualCargoForCargoRequerido(String valManualCargoForCargoRequerido) {
    this.selectManualCargoForCargoRequerido = valManualCargoForCargoRequerido;
  }
  public void changeManualCargoForCargoRequerido(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargoRequerido = null;
      if (idManualCargo > 0L) {
        this.colCargoRequerido = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargoRequerido = null;
        this.experienciaCargo.setCargoRequerido(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargoRequerido = null;
      this.experienciaCargo.setCargoRequerido(
        null);
    }
  }

  public boolean isShowManualCargoForCargoRequerido() { return this.colManualCargoForCargoRequerido != null; }

  public String getSelectCargoRequerido() {
    return this.selectCargoRequerido;
  }
  public void setSelectCargoRequerido(String valCargoRequerido) {
    Iterator iterator = this.colCargoRequerido.iterator();
    Cargo cargoRequerido = null;
    this.experienciaCargo.setCargoRequerido(null);
    while (iterator.hasNext()) {
      cargoRequerido = (Cargo)iterator.next();
      if (String.valueOf(cargoRequerido.getIdCargo()).equals(
        valCargoRequerido)) {
        this.experienciaCargo.setCargoRequerido(
          cargoRequerido);
      }
    }
    this.selectCargoRequerido = valCargoRequerido;
  }
  public boolean isShowCargoRequerido() {
    return this.colCargoRequerido != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ExperienciaCargo getExperienciaCargo() {
    if (this.experienciaCargo == null) {
      this.experienciaCargo = new ExperienciaCargo();
    }
    return this.experienciaCargo;
  }

  public ExperienciaCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColManualCargoForCargoRequerido() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargoRequerido.iterator();
    ManualCargo manualCargoForCargoRequerido = null;
    while (iterator.hasNext()) {
      manualCargoForCargoRequerido = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargoRequerido.getIdManualCargo()), 
        manualCargoForCargoRequerido.toString()));
    }
    return col;
  }

  public Collection getColCargoRequerido()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargoRequerido.iterator();
    Cargo cargoRequerido = null;
    while (iterator.hasNext()) {
      cargoRequerido = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargoRequerido.getIdCargo()), 
        cargoRequerido.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColManualCargoForCargo = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());

      this.findColManualCargoForCargoRequerido = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());

      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());

      this.colManualCargoForCargoRequerido = 
        this.cargoFacade.findManualCargoByProcesoSeleccion(
        "S", this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findExperienciaCargoByCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.clasificacionFacade.findExperienciaCargoByCargo(Long.valueOf(this.findSelectCargo).longValue());
      this.showExperienciaCargoByCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showExperienciaCargoByCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;
    this.findSelectManualCargoForCargoRequerido = null;
    this.findSelectCargoRequerido = null;

    return null;
  }

  public String findExperienciaCargoByCargoRequerido()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.clasificacionFacade.findExperienciaCargoByCargoRequerido(Long.valueOf(this.findSelectCargoRequerido).longValue());
      this.showExperienciaCargoByCargoRequerido = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showExperienciaCargoByCargoRequerido)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;
    this.findSelectManualCargoForCargoRequerido = null;
    this.findSelectCargoRequerido = null;

    return null;
  }

  public boolean isShowExperienciaCargoByCargo() {
    return this.showExperienciaCargoByCargo;
  }
  public boolean isShowExperienciaCargoByCargoRequerido() {
    return this.showExperienciaCargoByCargoRequerido;
  }

  public String selectExperienciaCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectCargoRequerido = null;
    this.selectManualCargoForCargoRequerido = null;

    long idExperienciaCargo = 
      Long.parseLong((String)requestParameterMap.get("idExperienciaCargo"));
    try
    {
      this.experienciaCargo = 
        this.clasificacionFacade.findExperienciaCargoById(
        idExperienciaCargo);
      if (this.experienciaCargo.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.experienciaCargo.getCargo().getIdCargo());
      }
      if (this.experienciaCargo.getCargoRequerido() != null) {
        this.selectCargoRequerido = 
          String.valueOf(this.experienciaCargo.getCargoRequerido().getIdCargo());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;
      Cargo cargoRequerido = null;
      ManualCargo manualCargoForCargoRequerido = null;

      if (this.experienciaCargo.getCargo() != null) {
        long idCargo = 
          this.experienciaCargo.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.experienciaCargo.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
      if (this.experienciaCargo.getCargoRequerido() != null) {
        long idCargoRequerido = 
          this.experienciaCargo.getCargoRequerido().getIdCargo();
        this.selectCargoRequerido = String.valueOf(idCargoRequerido);
        cargoRequerido = this.cargoFacade.findCargoById(
          idCargoRequerido);
        this.colCargoRequerido = this.cargoFacade.findCargoByManualCargo(
          cargoRequerido.getManualCargo().getIdManualCargo());

        long idManualCargoForCargoRequerido = 
          this.experienciaCargo.getCargoRequerido().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargoRequerido = String.valueOf(idManualCargoForCargoRequerido);
        manualCargoForCargoRequerido = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargoRequerido);
        this.colManualCargoForCargoRequerido = 
          this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.experienciaCargo = null;
    this.showExperienciaCargoByCargo = false;
    this.showExperienciaCargoByCargoRequerido = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.experienciaCargo.getTiempoSitp() != null) && 
      (this.experienciaCargo.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.clasificacionFacade.addExperienciaCargo(
          this.experienciaCargo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.clasificacionFacade.updateExperienciaCargo(
          this.experienciaCargo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.clasificacionFacade.deleteExperienciaCargo(
        this.experienciaCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.experienciaCargo = new ExperienciaCargo();

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    this.selectCargoRequerido = null;

    this.selectManualCargoForCargoRequerido = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.experienciaCargo.setIdExperienciaCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.clasificacion.ExperienciaCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.experienciaCargo = new ExperienciaCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}