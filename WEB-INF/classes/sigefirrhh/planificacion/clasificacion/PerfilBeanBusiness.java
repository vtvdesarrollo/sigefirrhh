package sigefirrhh.planificacion.clasificacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;

public class PerfilBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPerfil(Perfil perfil)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Perfil perfilNew = 
      (Perfil)BeanUtils.cloneBean(
      perfil);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (perfilNew.getCargo() != null) {
      perfilNew.setCargo(
        cargoBeanBusiness.findCargoById(
        perfilNew.getCargo().getIdCargo()));
    }
    pm.makePersistent(perfilNew);
  }

  public void updatePerfil(Perfil perfil) throws Exception
  {
    Perfil perfilModify = 
      findPerfilById(perfil.getIdPerfil());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (perfil.getCargo() != null) {
      perfil.setCargo(
        cargoBeanBusiness.findCargoById(
        perfil.getCargo().getIdCargo()));
    }

    BeanUtils.copyProperties(perfilModify, perfil);
  }

  public void deletePerfil(Perfil perfil) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Perfil perfilDelete = 
      findPerfilById(perfil.getIdPerfil());
    pm.deletePersistent(perfilDelete);
  }

  public Perfil findPerfilById(long idPerfil) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPerfil == pIdPerfil";
    Query query = pm.newQuery(Perfil.class, filter);

    query.declareParameters("long pIdPerfil");

    parameters.put("pIdPerfil", new Long(idPerfil));

    Collection colPerfil = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPerfil.iterator();
    return (Perfil)iterator.next();
  }

  public Collection findPerfilAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent perfilExtent = pm.getExtent(
      Perfil.class, true);
    Query query = pm.newQuery(perfilExtent);
    query.setOrdering("cargo.codCargo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(Perfil.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("cargo.codCargo ascending");

    Collection colPerfil = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPerfil);

    return colPerfil;
  }
}