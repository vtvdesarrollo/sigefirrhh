package sigefirrhh.planificacion.clasificacion;

import java.io.Serializable;

public class PerfilPK
  implements Serializable
{
  public long idPerfil;

  public PerfilPK()
  {
  }

  public PerfilPK(long idPerfil)
  {
    this.idPerfil = idPerfil;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PerfilPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PerfilPK thatPK)
  {
    return 
      this.idPerfil == thatPK.idPerfil;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPerfil)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPerfil);
  }
}