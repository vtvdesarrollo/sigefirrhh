package sigefirrhh.planificacion.clasificacion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class ClasificacionFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ClasificacionBusiness clasificacionBusiness = new ClasificacionBusiness();

  public void addAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.clasificacionBusiness.addAdiestramientoCargo(adiestramientoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.updateAdiestramientoCargo(adiestramientoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.deleteAdiestramientoCargo(adiestramientoCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AdiestramientoCargo findAdiestramientoCargoById(long adiestramientoCargoId) throws Exception
  {
    try { this.txn.open();
      AdiestramientoCargo adiestramientoCargo = 
        this.clasificacionBusiness.findAdiestramientoCargoById(adiestramientoCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(adiestramientoCargo);
      return adiestramientoCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAdiestramientoCargo() throws Exception
  {
    try { this.txn.open();
      return this.clasificacionBusiness.findAllAdiestramientoCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAdiestramientoCargoByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.clasificacionBusiness.findAdiestramientoCargoByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addExperienciaCargo(ExperienciaCargo experienciaCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.clasificacionBusiness.addExperienciaCargo(experienciaCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateExperienciaCargo(ExperienciaCargo experienciaCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.updateExperienciaCargo(experienciaCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteExperienciaCargo(ExperienciaCargo experienciaCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.deleteExperienciaCargo(experienciaCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ExperienciaCargo findExperienciaCargoById(long experienciaCargoId) throws Exception
  {
    try { this.txn.open();
      ExperienciaCargo experienciaCargo = 
        this.clasificacionBusiness.findExperienciaCargoById(experienciaCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(experienciaCargo);
      return experienciaCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllExperienciaCargo() throws Exception
  {
    try { this.txn.open();
      return this.clasificacionBusiness.findAllExperienciaCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExperienciaCargoByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.clasificacionBusiness.findExperienciaCargoByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExperienciaCargoByCargoRequerido(long idCargoRequerido)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.clasificacionBusiness.findExperienciaCargoByCargoRequerido(idCargoRequerido);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHabilidadCargo(HabilidadCargo habilidadCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.clasificacionBusiness.addHabilidadCargo(habilidadCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHabilidadCargo(HabilidadCargo habilidadCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.updateHabilidadCargo(habilidadCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHabilidadCargo(HabilidadCargo habilidadCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.deleteHabilidadCargo(habilidadCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HabilidadCargo findHabilidadCargoById(long habilidadCargoId) throws Exception
  {
    try { this.txn.open();
      HabilidadCargo habilidadCargo = 
        this.clasificacionBusiness.findHabilidadCargoById(habilidadCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(habilidadCargo);
      return habilidadCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHabilidadCargo() throws Exception
  {
    try { this.txn.open();
      return this.clasificacionBusiness.findAllHabilidadCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHabilidadCargoByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.clasificacionBusiness.findHabilidadCargoByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPerfil(Perfil perfil)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.clasificacionBusiness.addPerfil(perfil);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePerfil(Perfil perfil) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.updatePerfil(perfil);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePerfil(Perfil perfil) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.deletePerfil(perfil);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Perfil findPerfilById(long perfilId) throws Exception
  {
    try { this.txn.open();
      Perfil perfil = 
        this.clasificacionBusiness.findPerfilById(perfilId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(perfil);
      return perfil;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPerfil() throws Exception
  {
    try { this.txn.open();
      return this.clasificacionBusiness.findAllPerfil();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPerfilByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.clasificacionBusiness.findPerfilByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addProfesionCargo(ProfesionCargo profesionCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.clasificacionBusiness.addProfesionCargo(profesionCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateProfesionCargo(ProfesionCargo profesionCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.updateProfesionCargo(profesionCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteProfesionCargo(ProfesionCargo profesionCargo) throws Exception
  {
    try { this.txn.open();
      this.clasificacionBusiness.deleteProfesionCargo(profesionCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ProfesionCargo findProfesionCargoById(long profesionCargoId) throws Exception
  {
    try { this.txn.open();
      ProfesionCargo profesionCargo = 
        this.clasificacionBusiness.findProfesionCargoById(profesionCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(profesionCargo);
      return profesionCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllProfesionCargo() throws Exception
  {
    try { this.txn.open();
      return this.clasificacionBusiness.findAllProfesionCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProfesionCargoByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.clasificacionBusiness.findProfesionCargoByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}