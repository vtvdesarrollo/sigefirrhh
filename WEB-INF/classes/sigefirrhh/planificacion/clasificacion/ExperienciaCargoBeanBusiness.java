package sigefirrhh.planificacion.clasificacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;

public class ExperienciaCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addExperienciaCargo(ExperienciaCargo experienciaCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ExperienciaCargo experienciaCargoNew = 
      (ExperienciaCargo)BeanUtils.cloneBean(
      experienciaCargo);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (experienciaCargoNew.getCargo() != null) {
      experienciaCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        experienciaCargoNew.getCargo().getIdCargo()));
    }

    CargoBeanBusiness cargoRequeridoBeanBusiness = new CargoBeanBusiness();

    if (experienciaCargoNew.getCargoRequerido() != null) {
      experienciaCargoNew.setCargoRequerido(
        cargoRequeridoBeanBusiness.findCargoById(
        experienciaCargoNew.getCargoRequerido().getIdCargo()));
    }
    pm.makePersistent(experienciaCargoNew);
  }

  public void updateExperienciaCargo(ExperienciaCargo experienciaCargo) throws Exception
  {
    ExperienciaCargo experienciaCargoModify = 
      findExperienciaCargoById(experienciaCargo.getIdExperienciaCargo());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (experienciaCargo.getCargo() != null) {
      experienciaCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        experienciaCargo.getCargo().getIdCargo()));
    }

    CargoBeanBusiness cargoRequeridoBeanBusiness = new CargoBeanBusiness();

    if (experienciaCargo.getCargoRequerido() != null) {
      experienciaCargo.setCargoRequerido(
        cargoRequeridoBeanBusiness.findCargoById(
        experienciaCargo.getCargoRequerido().getIdCargo()));
    }

    BeanUtils.copyProperties(experienciaCargoModify, experienciaCargo);
  }

  public void deleteExperienciaCargo(ExperienciaCargo experienciaCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ExperienciaCargo experienciaCargoDelete = 
      findExperienciaCargoById(experienciaCargo.getIdExperienciaCargo());
    pm.deletePersistent(experienciaCargoDelete);
  }

  public ExperienciaCargo findExperienciaCargoById(long idExperienciaCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idExperienciaCargo == pIdExperienciaCargo";
    Query query = pm.newQuery(ExperienciaCargo.class, filter);

    query.declareParameters("long pIdExperienciaCargo");

    parameters.put("pIdExperienciaCargo", new Long(idExperienciaCargo));

    Collection colExperienciaCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colExperienciaCargo.iterator();
    return (ExperienciaCargo)iterator.next();
  }

  public Collection findExperienciaCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent experienciaCargoExtent = pm.getExtent(
      ExperienciaCargo.class, true);
    Query query = pm.newQuery(experienciaCargoExtent);
    query.setOrdering("peso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(ExperienciaCargo.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("peso ascending");

    Collection colExperienciaCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExperienciaCargo);

    return colExperienciaCargo;
  }

  public Collection findByCargoRequerido(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargoRequerido.idCargo == pIdCargo";

    Query query = pm.newQuery(ExperienciaCargo.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("peso ascending");

    Collection colExperienciaCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExperienciaCargo);

    return colExperienciaCargo;
  }
}