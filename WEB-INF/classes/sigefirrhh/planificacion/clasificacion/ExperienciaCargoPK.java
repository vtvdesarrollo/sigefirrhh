package sigefirrhh.planificacion.clasificacion;

import java.io.Serializable;

public class ExperienciaCargoPK
  implements Serializable
{
  public long idExperienciaCargo;

  public ExperienciaCargoPK()
  {
  }

  public ExperienciaCargoPK(long idExperienciaCargo)
  {
    this.idExperienciaCargo = idExperienciaCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ExperienciaCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ExperienciaCargoPK thatPK)
  {
    return 
      this.idExperienciaCargo == thatPK.idExperienciaCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idExperienciaCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idExperienciaCargo);
  }
}