package sigefirrhh.planificacion.clasificacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.AreaConocimientoBeanBusiness;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;

public class AdiestramientoCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AdiestramientoCargo adiestramientoCargoNew = 
      (AdiestramientoCargo)BeanUtils.cloneBean(
      adiestramientoCargo);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (adiestramientoCargoNew.getCargo() != null) {
      adiestramientoCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        adiestramientoCargoNew.getCargo().getIdCargo()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (adiestramientoCargoNew.getAreaConocimiento() != null) {
      adiestramientoCargoNew.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        adiestramientoCargoNew.getAreaConocimiento().getIdAreaConocimiento()));
    }
    pm.makePersistent(adiestramientoCargoNew);
  }

  public void updateAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo) throws Exception
  {
    AdiestramientoCargo adiestramientoCargoModify = 
      findAdiestramientoCargoById(adiestramientoCargo.getIdAdiestramientoCargo());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (adiestramientoCargo.getCargo() != null) {
      adiestramientoCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        adiestramientoCargo.getCargo().getIdCargo()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (adiestramientoCargo.getAreaConocimiento() != null) {
      adiestramientoCargo.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        adiestramientoCargo.getAreaConocimiento().getIdAreaConocimiento()));
    }

    BeanUtils.copyProperties(adiestramientoCargoModify, adiestramientoCargo);
  }

  public void deleteAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AdiestramientoCargo adiestramientoCargoDelete = 
      findAdiestramientoCargoById(adiestramientoCargo.getIdAdiestramientoCargo());
    pm.deletePersistent(adiestramientoCargoDelete);
  }

  public AdiestramientoCargo findAdiestramientoCargoById(long idAdiestramientoCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAdiestramientoCargo == pIdAdiestramientoCargo";
    Query query = pm.newQuery(AdiestramientoCargo.class, filter);

    query.declareParameters("long pIdAdiestramientoCargo");

    parameters.put("pIdAdiestramientoCargo", new Long(idAdiestramientoCargo));

    Collection colAdiestramientoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAdiestramientoCargo.iterator();
    return (AdiestramientoCargo)iterator.next();
  }

  public Collection findAdiestramientoCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent adiestramientoCargoExtent = pm.getExtent(
      AdiestramientoCargo.class, true);
    Query query = pm.newQuery(adiestramientoCargoExtent);
    query.setOrdering("peso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(AdiestramientoCargo.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("peso ascending");

    Collection colAdiestramientoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAdiestramientoCargo);

    return colAdiestramientoCargo;
  }
}