package sigefirrhh.planificacion.clasificacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.personal.TipoHabilidad;
import sigefirrhh.base.personal.TipoHabilidadBeanBusiness;

public class HabilidadCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHabilidadCargo(HabilidadCargo habilidadCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HabilidadCargo habilidadCargoNew = 
      (HabilidadCargo)BeanUtils.cloneBean(
      habilidadCargo);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (habilidadCargoNew.getCargo() != null) {
      habilidadCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        habilidadCargoNew.getCargo().getIdCargo()));
    }

    TipoHabilidadBeanBusiness tipoHabilidadBeanBusiness = new TipoHabilidadBeanBusiness();

    if (habilidadCargoNew.getTipoHabilidad() != null) {
      habilidadCargoNew.setTipoHabilidad(
        tipoHabilidadBeanBusiness.findTipoHabilidadById(
        habilidadCargoNew.getTipoHabilidad().getIdTipoHabilidad()));
    }
    pm.makePersistent(habilidadCargoNew);
  }

  public void updateHabilidadCargo(HabilidadCargo habilidadCargo) throws Exception
  {
    HabilidadCargo habilidadCargoModify = 
      findHabilidadCargoById(habilidadCargo.getIdHabilidadCargo());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (habilidadCargo.getCargo() != null) {
      habilidadCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        habilidadCargo.getCargo().getIdCargo()));
    }

    TipoHabilidadBeanBusiness tipoHabilidadBeanBusiness = new TipoHabilidadBeanBusiness();

    if (habilidadCargo.getTipoHabilidad() != null) {
      habilidadCargo.setTipoHabilidad(
        tipoHabilidadBeanBusiness.findTipoHabilidadById(
        habilidadCargo.getTipoHabilidad().getIdTipoHabilidad()));
    }

    BeanUtils.copyProperties(habilidadCargoModify, habilidadCargo);
  }

  public void deleteHabilidadCargo(HabilidadCargo habilidadCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HabilidadCargo habilidadCargoDelete = 
      findHabilidadCargoById(habilidadCargo.getIdHabilidadCargo());
    pm.deletePersistent(habilidadCargoDelete);
  }

  public HabilidadCargo findHabilidadCargoById(long idHabilidadCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHabilidadCargo == pIdHabilidadCargo";
    Query query = pm.newQuery(HabilidadCargo.class, filter);

    query.declareParameters("long pIdHabilidadCargo");

    parameters.put("pIdHabilidadCargo", new Long(idHabilidadCargo));

    Collection colHabilidadCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHabilidadCargo.iterator();
    return (HabilidadCargo)iterator.next();
  }

  public Collection findHabilidadCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent habilidadCargoExtent = pm.getExtent(
      HabilidadCargo.class, true);
    Query query = pm.newQuery(habilidadCargoExtent);
    query.setOrdering("peso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(HabilidadCargo.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("peso ascending");

    Collection colHabilidadCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHabilidadCargo);

    return colHabilidadCargo;
  }
}