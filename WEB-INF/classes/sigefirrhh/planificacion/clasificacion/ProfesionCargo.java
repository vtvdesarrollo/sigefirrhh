package sigefirrhh.planificacion.clasificacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.personal.Profesion;

public class ProfesionCargo
  implements Serializable, PersistenceCapable
{
  private long idProfesionCargo;
  private Cargo cargo;
  private Profesion profesion;
  private double peso;
  private int aniosGraduado;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "aniosGraduado", "cargo", "idProfesionCargo", "idSitp", "peso", "profesion", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Long.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.Profesion"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ProfesionCargo()
  {
    jdoSetpeso(this, 0.0D);

    jdoSetaniosGraduado(this, 0);
  }

  public String toString()
  {
    return jdoGetprofesion(this).getNombre() + " - " + jdoGetpeso(this);
  }

  public int getAniosGraduado()
  {
    return jdoGetaniosGraduado(this);
  }

  public void setAniosGraduado(int aniosGraduado)
  {
    jdoSetaniosGraduado(this, aniosGraduado);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public long getIdProfesionCargo()
  {
    return jdoGetidProfesionCargo(this);
  }

  public void setIdProfesionCargo(long idProfesionCargo)
  {
    jdoSetidProfesionCargo(this, idProfesionCargo);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public double getPeso()
  {
    return jdoGetpeso(this);
  }

  public void setPeso(double peso)
  {
    jdoSetpeso(this, peso);
  }

  public Profesion getProfesion()
  {
    return jdoGetprofesion(this);
  }

  public void setProfesion(Profesion profesion)
  {
    jdoSetprofesion(this, profesion);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.clasificacion.ProfesionCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ProfesionCargo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ProfesionCargo localProfesionCargo = new ProfesionCargo();
    localProfesionCargo.jdoFlags = 1;
    localProfesionCargo.jdoStateManager = paramStateManager;
    return localProfesionCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ProfesionCargo localProfesionCargo = new ProfesionCargo();
    localProfesionCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProfesionCargo.jdoFlags = 1;
    localProfesionCargo.jdoStateManager = paramStateManager;
    return localProfesionCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosGraduado);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProfesionCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.peso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.profesion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosGraduado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProfesionCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.peso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.profesion = ((Profesion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ProfesionCargo paramProfesionCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProfesionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.aniosGraduado = paramProfesionCargo.aniosGraduado;
      return;
    case 1:
      if (paramProfesionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramProfesionCargo.cargo;
      return;
    case 2:
      if (paramProfesionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idProfesionCargo = paramProfesionCargo.idProfesionCargo;
      return;
    case 3:
      if (paramProfesionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramProfesionCargo.idSitp;
      return;
    case 4:
      if (paramProfesionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.peso = paramProfesionCargo.peso;
      return;
    case 5:
      if (paramProfesionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.profesion = paramProfesionCargo.profesion;
      return;
    case 6:
      if (paramProfesionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramProfesionCargo.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ProfesionCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ProfesionCargo localProfesionCargo = (ProfesionCargo)paramObject;
    if (localProfesionCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProfesionCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProfesionCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProfesionCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProfesionCargoPK))
      throw new IllegalArgumentException("arg1");
    ProfesionCargoPK localProfesionCargoPK = (ProfesionCargoPK)paramObject;
    localProfesionCargoPK.idProfesionCargo = this.idProfesionCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProfesionCargoPK))
      throw new IllegalArgumentException("arg1");
    ProfesionCargoPK localProfesionCargoPK = (ProfesionCargoPK)paramObject;
    this.idProfesionCargo = localProfesionCargoPK.idProfesionCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProfesionCargoPK))
      throw new IllegalArgumentException("arg2");
    ProfesionCargoPK localProfesionCargoPK = (ProfesionCargoPK)paramObject;
    localProfesionCargoPK.idProfesionCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProfesionCargoPK))
      throw new IllegalArgumentException("arg2");
    ProfesionCargoPK localProfesionCargoPK = (ProfesionCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localProfesionCargoPK.idProfesionCargo);
  }

  private static final int jdoGetaniosGraduado(ProfesionCargo paramProfesionCargo)
  {
    if (paramProfesionCargo.jdoFlags <= 0)
      return paramProfesionCargo.aniosGraduado;
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionCargo.aniosGraduado;
    if (localStateManager.isLoaded(paramProfesionCargo, jdoInheritedFieldCount + 0))
      return paramProfesionCargo.aniosGraduado;
    return localStateManager.getIntField(paramProfesionCargo, jdoInheritedFieldCount + 0, paramProfesionCargo.aniosGraduado);
  }

  private static final void jdoSetaniosGraduado(ProfesionCargo paramProfesionCargo, int paramInt)
  {
    if (paramProfesionCargo.jdoFlags == 0)
    {
      paramProfesionCargo.aniosGraduado = paramInt;
      return;
    }
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionCargo.aniosGraduado = paramInt;
      return;
    }
    localStateManager.setIntField(paramProfesionCargo, jdoInheritedFieldCount + 0, paramProfesionCargo.aniosGraduado, paramInt);
  }

  private static final Cargo jdoGetcargo(ProfesionCargo paramProfesionCargo)
  {
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionCargo.cargo;
    if (localStateManager.isLoaded(paramProfesionCargo, jdoInheritedFieldCount + 1))
      return paramProfesionCargo.cargo;
    return (Cargo)localStateManager.getObjectField(paramProfesionCargo, jdoInheritedFieldCount + 1, paramProfesionCargo.cargo);
  }

  private static final void jdoSetcargo(ProfesionCargo paramProfesionCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionCargo.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramProfesionCargo, jdoInheritedFieldCount + 1, paramProfesionCargo.cargo, paramCargo);
  }

  private static final long jdoGetidProfesionCargo(ProfesionCargo paramProfesionCargo)
  {
    return paramProfesionCargo.idProfesionCargo;
  }

  private static final void jdoSetidProfesionCargo(ProfesionCargo paramProfesionCargo, long paramLong)
  {
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionCargo.idProfesionCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramProfesionCargo, jdoInheritedFieldCount + 2, paramProfesionCargo.idProfesionCargo, paramLong);
  }

  private static final int jdoGetidSitp(ProfesionCargo paramProfesionCargo)
  {
    if (paramProfesionCargo.jdoFlags <= 0)
      return paramProfesionCargo.idSitp;
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionCargo.idSitp;
    if (localStateManager.isLoaded(paramProfesionCargo, jdoInheritedFieldCount + 3))
      return paramProfesionCargo.idSitp;
    return localStateManager.getIntField(paramProfesionCargo, jdoInheritedFieldCount + 3, paramProfesionCargo.idSitp);
  }

  private static final void jdoSetidSitp(ProfesionCargo paramProfesionCargo, int paramInt)
  {
    if (paramProfesionCargo.jdoFlags == 0)
    {
      paramProfesionCargo.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionCargo.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramProfesionCargo, jdoInheritedFieldCount + 3, paramProfesionCargo.idSitp, paramInt);
  }

  private static final double jdoGetpeso(ProfesionCargo paramProfesionCargo)
  {
    if (paramProfesionCargo.jdoFlags <= 0)
      return paramProfesionCargo.peso;
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionCargo.peso;
    if (localStateManager.isLoaded(paramProfesionCargo, jdoInheritedFieldCount + 4))
      return paramProfesionCargo.peso;
    return localStateManager.getDoubleField(paramProfesionCargo, jdoInheritedFieldCount + 4, paramProfesionCargo.peso);
  }

  private static final void jdoSetpeso(ProfesionCargo paramProfesionCargo, double paramDouble)
  {
    if (paramProfesionCargo.jdoFlags == 0)
    {
      paramProfesionCargo.peso = paramDouble;
      return;
    }
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionCargo.peso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramProfesionCargo, jdoInheritedFieldCount + 4, paramProfesionCargo.peso, paramDouble);
  }

  private static final Profesion jdoGetprofesion(ProfesionCargo paramProfesionCargo)
  {
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionCargo.profesion;
    if (localStateManager.isLoaded(paramProfesionCargo, jdoInheritedFieldCount + 5))
      return paramProfesionCargo.profesion;
    return (Profesion)localStateManager.getObjectField(paramProfesionCargo, jdoInheritedFieldCount + 5, paramProfesionCargo.profesion);
  }

  private static final void jdoSetprofesion(ProfesionCargo paramProfesionCargo, Profesion paramProfesion)
  {
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionCargo.profesion = paramProfesion;
      return;
    }
    localStateManager.setObjectField(paramProfesionCargo, jdoInheritedFieldCount + 5, paramProfesionCargo.profesion, paramProfesion);
  }

  private static final Date jdoGettiempoSitp(ProfesionCargo paramProfesionCargo)
  {
    if (paramProfesionCargo.jdoFlags <= 0)
      return paramProfesionCargo.tiempoSitp;
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramProfesionCargo.tiempoSitp;
    if (localStateManager.isLoaded(paramProfesionCargo, jdoInheritedFieldCount + 6))
      return paramProfesionCargo.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramProfesionCargo, jdoInheritedFieldCount + 6, paramProfesionCargo.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ProfesionCargo paramProfesionCargo, Date paramDate)
  {
    if (paramProfesionCargo.jdoFlags == 0)
    {
      paramProfesionCargo.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramProfesionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramProfesionCargo.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramProfesionCargo, jdoInheritedFieldCount + 6, paramProfesionCargo.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}