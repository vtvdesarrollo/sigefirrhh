package sigefirrhh.planificacion.clasificacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;

public class Perfil
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_TIPO_CARGO;
  private long idPerfil;
  private Cargo cargo;
  private String tipoCargo;
  private String naturaleza;
  private String propositos;
  private String responsabilidades;
  private String aprobacionMpd;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aprobacionMpd", "cargo", "idPerfil", "idSitp", "naturaleza", "propositos", "responsabilidades", "tiempoSitp", "tipoCargo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 26, 24, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.clasificacion.Perfil"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Perfil());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_TIPO_CARGO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_TIPO_CARGO.put("1", "ALTO NIVEL");
    LISTA_TIPO_CARGO.put("2", "ADMINISTRATIVO");
    LISTA_TIPO_CARGO.put("3", "TECNICO-PROFESIONAL");
    LISTA_TIPO_CARGO.put("4", "OBRERO");
    LISTA_TIPO_CARGO.put("5", "NO CLASIFICADO");
  }

  public Perfil()
  {
    jdoSettipoCargo(this, "1");
  }

  public String toString()
  {
    return jdoGetcargo(this).getDescripcionCargo();
  }

  public String getTipoCargo()
  {
    return jdoGettipoCargo(this);
  }

  public void setTipoCargo(String tipoCargo)
  {
    jdoSettipoCargo(this, tipoCargo);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public long getIdPerfil()
  {
    return jdoGetidPerfil(this);
  }

  public String getNaturaleza()
  {
    return jdoGetnaturaleza(this);
  }

  public String getPropositos()
  {
    return jdoGetpropositos(this);
  }

  public String getResponsabilidades()
  {
    return jdoGetresponsabilidades(this);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public void setIdPerfil(long l)
  {
    jdoSetidPerfil(this, l);
  }

  public void setNaturaleza(String string)
  {
    jdoSetnaturaleza(this, string);
  }

  public void setPropositos(String string)
  {
    jdoSetpropositos(this, string);
  }

  public void setResponsabilidades(String string)
  {
    jdoSetresponsabilidades(this, string);
  }

  public int getIdSitp() {
    return jdoGetidSitp(this);
  }

  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }

  public void setIdSitp(int i) {
    jdoSetidSitp(this, i);
  }

  public void setTiempoSitp(Date date) {
    jdoSettiempoSitp(this, date);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Perfil localPerfil = new Perfil();
    localPerfil.jdoFlags = 1;
    localPerfil.jdoStateManager = paramStateManager;
    return localPerfil;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Perfil localPerfil = new Perfil();
    localPerfil.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPerfil.jdoFlags = 1;
    localPerfil.jdoStateManager = paramStateManager;
    return localPerfil;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPerfil);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.naturaleza);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.propositos);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.responsabilidades);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCargo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPerfil = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.naturaleza = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.propositos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.responsabilidades = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Perfil paramPerfil, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramPerfil.aprobacionMpd;
      return;
    case 1:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramPerfil.cargo;
      return;
    case 2:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.idPerfil = paramPerfil.idPerfil;
      return;
    case 3:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramPerfil.idSitp;
      return;
    case 4:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.naturaleza = paramPerfil.naturaleza;
      return;
    case 5:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.propositos = paramPerfil.propositos;
      return;
    case 6:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.responsabilidades = paramPerfil.responsabilidades;
      return;
    case 7:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramPerfil.tiempoSitp;
      return;
    case 8:
      if (paramPerfil == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCargo = paramPerfil.tipoCargo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Perfil))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Perfil localPerfil = (Perfil)paramObject;
    if (localPerfil.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPerfil, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PerfilPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PerfilPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PerfilPK))
      throw new IllegalArgumentException("arg1");
    PerfilPK localPerfilPK = (PerfilPK)paramObject;
    localPerfilPK.idPerfil = this.idPerfil;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PerfilPK))
      throw new IllegalArgumentException("arg1");
    PerfilPK localPerfilPK = (PerfilPK)paramObject;
    this.idPerfil = localPerfilPK.idPerfil;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PerfilPK))
      throw new IllegalArgumentException("arg2");
    PerfilPK localPerfilPK = (PerfilPK)paramObject;
    localPerfilPK.idPerfil = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PerfilPK))
      throw new IllegalArgumentException("arg2");
    PerfilPK localPerfilPK = (PerfilPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localPerfilPK.idPerfil);
  }

  private static final String jdoGetaprobacionMpd(Perfil paramPerfil)
  {
    if (paramPerfil.jdoFlags <= 0)
      return paramPerfil.aprobacionMpd;
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.aprobacionMpd;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 0))
      return paramPerfil.aprobacionMpd;
    return localStateManager.getStringField(paramPerfil, jdoInheritedFieldCount + 0, paramPerfil.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(Perfil paramPerfil, String paramString)
  {
    if (paramPerfil.jdoFlags == 0)
    {
      paramPerfil.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramPerfil, jdoInheritedFieldCount + 0, paramPerfil.aprobacionMpd, paramString);
  }

  private static final Cargo jdoGetcargo(Perfil paramPerfil)
  {
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.cargo;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 1))
      return paramPerfil.cargo;
    return (Cargo)localStateManager.getObjectField(paramPerfil, jdoInheritedFieldCount + 1, paramPerfil.cargo);
  }

  private static final void jdoSetcargo(Perfil paramPerfil, Cargo paramCargo)
  {
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramPerfil, jdoInheritedFieldCount + 1, paramPerfil.cargo, paramCargo);
  }

  private static final long jdoGetidPerfil(Perfil paramPerfil)
  {
    return paramPerfil.idPerfil;
  }

  private static final void jdoSetidPerfil(Perfil paramPerfil, long paramLong)
  {
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.idPerfil = paramLong;
      return;
    }
    localStateManager.setLongField(paramPerfil, jdoInheritedFieldCount + 2, paramPerfil.idPerfil, paramLong);
  }

  private static final int jdoGetidSitp(Perfil paramPerfil)
  {
    if (paramPerfil.jdoFlags <= 0)
      return paramPerfil.idSitp;
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.idSitp;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 3))
      return paramPerfil.idSitp;
    return localStateManager.getIntField(paramPerfil, jdoInheritedFieldCount + 3, paramPerfil.idSitp);
  }

  private static final void jdoSetidSitp(Perfil paramPerfil, int paramInt)
  {
    if (paramPerfil.jdoFlags == 0)
    {
      paramPerfil.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramPerfil, jdoInheritedFieldCount + 3, paramPerfil.idSitp, paramInt);
  }

  private static final String jdoGetnaturaleza(Perfil paramPerfil)
  {
    if (paramPerfil.jdoFlags <= 0)
      return paramPerfil.naturaleza;
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.naturaleza;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 4))
      return paramPerfil.naturaleza;
    return localStateManager.getStringField(paramPerfil, jdoInheritedFieldCount + 4, paramPerfil.naturaleza);
  }

  private static final void jdoSetnaturaleza(Perfil paramPerfil, String paramString)
  {
    if (paramPerfil.jdoFlags == 0)
    {
      paramPerfil.naturaleza = paramString;
      return;
    }
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.naturaleza = paramString;
      return;
    }
    localStateManager.setStringField(paramPerfil, jdoInheritedFieldCount + 4, paramPerfil.naturaleza, paramString);
  }

  private static final String jdoGetpropositos(Perfil paramPerfil)
  {
    if (paramPerfil.jdoFlags <= 0)
      return paramPerfil.propositos;
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.propositos;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 5))
      return paramPerfil.propositos;
    return localStateManager.getStringField(paramPerfil, jdoInheritedFieldCount + 5, paramPerfil.propositos);
  }

  private static final void jdoSetpropositos(Perfil paramPerfil, String paramString)
  {
    if (paramPerfil.jdoFlags == 0)
    {
      paramPerfil.propositos = paramString;
      return;
    }
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.propositos = paramString;
      return;
    }
    localStateManager.setStringField(paramPerfil, jdoInheritedFieldCount + 5, paramPerfil.propositos, paramString);
  }

  private static final String jdoGetresponsabilidades(Perfil paramPerfil)
  {
    if (paramPerfil.jdoFlags <= 0)
      return paramPerfil.responsabilidades;
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.responsabilidades;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 6))
      return paramPerfil.responsabilidades;
    return localStateManager.getStringField(paramPerfil, jdoInheritedFieldCount + 6, paramPerfil.responsabilidades);
  }

  private static final void jdoSetresponsabilidades(Perfil paramPerfil, String paramString)
  {
    if (paramPerfil.jdoFlags == 0)
    {
      paramPerfil.responsabilidades = paramString;
      return;
    }
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.responsabilidades = paramString;
      return;
    }
    localStateManager.setStringField(paramPerfil, jdoInheritedFieldCount + 6, paramPerfil.responsabilidades, paramString);
  }

  private static final Date jdoGettiempoSitp(Perfil paramPerfil)
  {
    if (paramPerfil.jdoFlags <= 0)
      return paramPerfil.tiempoSitp;
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.tiempoSitp;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 7))
      return paramPerfil.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramPerfil, jdoInheritedFieldCount + 7, paramPerfil.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Perfil paramPerfil, Date paramDate)
  {
    if (paramPerfil.jdoFlags == 0)
    {
      paramPerfil.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPerfil, jdoInheritedFieldCount + 7, paramPerfil.tiempoSitp, paramDate);
  }

  private static final String jdoGettipoCargo(Perfil paramPerfil)
  {
    if (paramPerfil.jdoFlags <= 0)
      return paramPerfil.tipoCargo;
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
      return paramPerfil.tipoCargo;
    if (localStateManager.isLoaded(paramPerfil, jdoInheritedFieldCount + 8))
      return paramPerfil.tipoCargo;
    return localStateManager.getStringField(paramPerfil, jdoInheritedFieldCount + 8, paramPerfil.tipoCargo);
  }

  private static final void jdoSettipoCargo(Perfil paramPerfil, String paramString)
  {
    if (paramPerfil.jdoFlags == 0)
    {
      paramPerfil.tipoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramPerfil.jdoStateManager;
    if (localStateManager == null)
    {
      paramPerfil.tipoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramPerfil, jdoInheritedFieldCount + 8, paramPerfil.tipoCargo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}