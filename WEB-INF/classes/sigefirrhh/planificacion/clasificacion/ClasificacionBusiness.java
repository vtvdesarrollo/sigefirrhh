package sigefirrhh.planificacion.clasificacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class ClasificacionBusiness extends AbstractBusiness
  implements Serializable
{
  private AdiestramientoCargoBeanBusiness adiestramientoCargoBeanBusiness = new AdiestramientoCargoBeanBusiness();

  private ExperienciaCargoBeanBusiness experienciaCargoBeanBusiness = new ExperienciaCargoBeanBusiness();

  private HabilidadCargoBeanBusiness habilidadCargoBeanBusiness = new HabilidadCargoBeanBusiness();

  private PerfilBeanBusiness perfilBeanBusiness = new PerfilBeanBusiness();

  private ProfesionCargoBeanBusiness profesionCargoBeanBusiness = new ProfesionCargoBeanBusiness();

  public void addAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo)
    throws Exception
  {
    this.adiestramientoCargoBeanBusiness.addAdiestramientoCargo(adiestramientoCargo);
  }

  public void updateAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo) throws Exception {
    this.adiestramientoCargoBeanBusiness.updateAdiestramientoCargo(adiestramientoCargo);
  }

  public void deleteAdiestramientoCargo(AdiestramientoCargo adiestramientoCargo) throws Exception {
    this.adiestramientoCargoBeanBusiness.deleteAdiestramientoCargo(adiestramientoCargo);
  }

  public AdiestramientoCargo findAdiestramientoCargoById(long adiestramientoCargoId) throws Exception {
    return this.adiestramientoCargoBeanBusiness.findAdiestramientoCargoById(adiestramientoCargoId);
  }

  public Collection findAllAdiestramientoCargo() throws Exception {
    return this.adiestramientoCargoBeanBusiness.findAdiestramientoCargoAll();
  }

  public Collection findAdiestramientoCargoByCargo(long idCargo)
    throws Exception
  {
    return this.adiestramientoCargoBeanBusiness.findByCargo(idCargo);
  }

  public void addExperienciaCargo(ExperienciaCargo experienciaCargo)
    throws Exception
  {
    this.experienciaCargoBeanBusiness.addExperienciaCargo(experienciaCargo);
  }

  public void updateExperienciaCargo(ExperienciaCargo experienciaCargo) throws Exception {
    this.experienciaCargoBeanBusiness.updateExperienciaCargo(experienciaCargo);
  }

  public void deleteExperienciaCargo(ExperienciaCargo experienciaCargo) throws Exception {
    this.experienciaCargoBeanBusiness.deleteExperienciaCargo(experienciaCargo);
  }

  public ExperienciaCargo findExperienciaCargoById(long experienciaCargoId) throws Exception {
    return this.experienciaCargoBeanBusiness.findExperienciaCargoById(experienciaCargoId);
  }

  public Collection findAllExperienciaCargo() throws Exception {
    return this.experienciaCargoBeanBusiness.findExperienciaCargoAll();
  }

  public Collection findExperienciaCargoByCargo(long idCargo)
    throws Exception
  {
    return this.experienciaCargoBeanBusiness.findByCargo(idCargo);
  }

  public Collection findExperienciaCargoByCargoRequerido(long idCargoRequerido)
    throws Exception
  {
    return this.experienciaCargoBeanBusiness.findByCargoRequerido(idCargoRequerido);
  }

  public void addHabilidadCargo(HabilidadCargo habilidadCargo)
    throws Exception
  {
    this.habilidadCargoBeanBusiness.addHabilidadCargo(habilidadCargo);
  }

  public void updateHabilidadCargo(HabilidadCargo habilidadCargo) throws Exception {
    this.habilidadCargoBeanBusiness.updateHabilidadCargo(habilidadCargo);
  }

  public void deleteHabilidadCargo(HabilidadCargo habilidadCargo) throws Exception {
    this.habilidadCargoBeanBusiness.deleteHabilidadCargo(habilidadCargo);
  }

  public HabilidadCargo findHabilidadCargoById(long habilidadCargoId) throws Exception {
    return this.habilidadCargoBeanBusiness.findHabilidadCargoById(habilidadCargoId);
  }

  public Collection findAllHabilidadCargo() throws Exception {
    return this.habilidadCargoBeanBusiness.findHabilidadCargoAll();
  }

  public Collection findHabilidadCargoByCargo(long idCargo)
    throws Exception
  {
    return this.habilidadCargoBeanBusiness.findByCargo(idCargo);
  }

  public void addPerfil(Perfil perfil)
    throws Exception
  {
    this.perfilBeanBusiness.addPerfil(perfil);
  }

  public void updatePerfil(Perfil perfil) throws Exception {
    this.perfilBeanBusiness.updatePerfil(perfil);
  }

  public void deletePerfil(Perfil perfil) throws Exception {
    this.perfilBeanBusiness.deletePerfil(perfil);
  }

  public Perfil findPerfilById(long perfilId) throws Exception {
    return this.perfilBeanBusiness.findPerfilById(perfilId);
  }

  public Collection findAllPerfil() throws Exception {
    return this.perfilBeanBusiness.findPerfilAll();
  }

  public Collection findPerfilByCargo(long idCargo)
    throws Exception
  {
    return this.perfilBeanBusiness.findByCargo(idCargo);
  }

  public void addProfesionCargo(ProfesionCargo profesionCargo)
    throws Exception
  {
    this.profesionCargoBeanBusiness.addProfesionCargo(profesionCargo);
  }

  public void updateProfesionCargo(ProfesionCargo profesionCargo) throws Exception {
    this.profesionCargoBeanBusiness.updateProfesionCargo(profesionCargo);
  }

  public void deleteProfesionCargo(ProfesionCargo profesionCargo) throws Exception {
    this.profesionCargoBeanBusiness.deleteProfesionCargo(profesionCargo);
  }

  public ProfesionCargo findProfesionCargoById(long profesionCargoId) throws Exception {
    return this.profesionCargoBeanBusiness.findProfesionCargoById(profesionCargoId);
  }

  public Collection findAllProfesionCargo() throws Exception {
    return this.profesionCargoBeanBusiness.findProfesionCargoAll();
  }

  public Collection findProfesionCargoByCargo(long idCargo)
    throws Exception
  {
    return this.profesionCargoBeanBusiness.findByCargo(idCargo);
  }
}