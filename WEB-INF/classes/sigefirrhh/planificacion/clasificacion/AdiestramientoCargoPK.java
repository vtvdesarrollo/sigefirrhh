package sigefirrhh.planificacion.clasificacion;

import java.io.Serializable;

public class AdiestramientoCargoPK
  implements Serializable
{
  public long idAdiestramientoCargo;

  public AdiestramientoCargoPK()
  {
  }

  public AdiestramientoCargoPK(long idAdiestramientoCargo)
  {
    this.idAdiestramientoCargo = idAdiestramientoCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AdiestramientoCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AdiestramientoCargoPK thatPK)
  {
    return 
      this.idAdiestramientoCargo == thatPK.idAdiestramientoCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAdiestramientoCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAdiestramientoCargo);
  }
}