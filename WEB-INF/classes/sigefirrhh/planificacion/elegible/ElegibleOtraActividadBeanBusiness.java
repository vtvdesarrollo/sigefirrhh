package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoOtraActividad;
import sigefirrhh.base.personal.TipoOtraActividadBeanBusiness;

public class ElegibleOtraActividadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleOtraActividad elegibleOtraActividadNew = 
      (ElegibleOtraActividad)BeanUtils.cloneBean(
      elegibleOtraActividad);

    TipoOtraActividadBeanBusiness tipoOtraActividadBeanBusiness = new TipoOtraActividadBeanBusiness();

    if (elegibleOtraActividadNew.getTipoOtraActividad() != null) {
      elegibleOtraActividadNew.setTipoOtraActividad(
        tipoOtraActividadBeanBusiness.findTipoOtraActividadById(
        elegibleOtraActividadNew.getTipoOtraActividad().getIdTipoOtraActividad()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleOtraActividadNew.getElegible() != null) {
      elegibleOtraActividadNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleOtraActividadNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleOtraActividadNew);
  }

  public void updateElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad) throws Exception
  {
    ElegibleOtraActividad elegibleOtraActividadModify = 
      findElegibleOtraActividadById(elegibleOtraActividad.getIdElegibleOtraActividad());

    TipoOtraActividadBeanBusiness tipoOtraActividadBeanBusiness = new TipoOtraActividadBeanBusiness();

    if (elegibleOtraActividad.getTipoOtraActividad() != null) {
      elegibleOtraActividad.setTipoOtraActividad(
        tipoOtraActividadBeanBusiness.findTipoOtraActividadById(
        elegibleOtraActividad.getTipoOtraActividad().getIdTipoOtraActividad()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleOtraActividad.getElegible() != null) {
      elegibleOtraActividad.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleOtraActividad.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleOtraActividadModify, elegibleOtraActividad);
  }

  public void deleteElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleOtraActividad elegibleOtraActividadDelete = 
      findElegibleOtraActividadById(elegibleOtraActividad.getIdElegibleOtraActividad());
    pm.deletePersistent(elegibleOtraActividadDelete);
  }

  public ElegibleOtraActividad findElegibleOtraActividadById(long idElegibleOtraActividad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleOtraActividad == pIdElegibleOtraActividad";
    Query query = pm.newQuery(ElegibleOtraActividad.class, filter);

    query.declareParameters("long pIdElegibleOtraActividad");

    parameters.put("pIdElegibleOtraActividad", new Long(idElegibleOtraActividad));

    Collection colElegibleOtraActividad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleOtraActividad.iterator();
    return (ElegibleOtraActividad)iterator.next();
  }

  public Collection findElegibleOtraActividadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleOtraActividadExtent = pm.getExtent(
      ElegibleOtraActividad.class, true);
    Query query = pm.newQuery(elegibleOtraActividadExtent);
    query.setOrdering("tipoOtraActividad.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleOtraActividad.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("tipoOtraActividad.descripcion ascending");

    Collection colElegibleOtraActividad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleOtraActividad);

    return colElegibleOtraActividad;
  }
}