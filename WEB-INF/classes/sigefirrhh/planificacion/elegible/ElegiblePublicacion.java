package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ElegiblePublicacion
  implements Serializable, PersistenceCapable
{
  private long idElegiblePublicacion;
  private int anioPublicacion;
  private String titulo;
  private String editorial;
  private String propiedadIntelectual;
  private Elegible elegible;
  private String observaciones;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anioPublicacion", "editorial", "elegible", "idElegiblePublicacion", "idSitp", "observaciones", "propiedadIntelectual", "tiempoSitp", "titulo" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 26, 24, 21, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return String.valueOf(jdoGetanioPublicacion(this));
  }

  public int getAnioPublicacion() {
    return jdoGetanioPublicacion(this);
  }
  public void setAnioPublicacion(int anioPublicacion) {
    jdoSetanioPublicacion(this, anioPublicacion);
  }
  public String getEditorial() {
    return jdoGeteditorial(this);
  }
  public void setEditorial(String editorial) {
    jdoSeteditorial(this, editorial);
  }
  public Elegible getElegible() {
    return jdoGetelegible(this);
  }
  public void setElegible(Elegible elegible) {
    jdoSetelegible(this, elegible);
  }
  public long getIdElegiblePublicacion() {
    return jdoGetidElegiblePublicacion(this);
  }
  public void setIdElegiblePublicacion(long idElegiblePublicacion) {
    jdoSetidElegiblePublicacion(this, idElegiblePublicacion);
  }
  public int getIdSitp() {
    return jdoGetidSitp(this);
  }
  public void setIdSitp(int idSitp) {
    jdoSetidSitp(this, idSitp);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public String getPropiedadIntelectual() {
    return jdoGetpropiedadIntelectual(this);
  }
  public void setPropiedadIntelectual(String propiedadIntelectual) {
    jdoSetpropiedadIntelectual(this, propiedadIntelectual);
  }
  public Date getTiempoSitp() {
    return jdoGettiempoSitp(this);
  }
  public void setTiempoSitp(Date tiempoSitp) {
    jdoSettiempoSitp(this, tiempoSitp);
  }
  public String getTitulo() {
    return jdoGettitulo(this);
  }
  public void setTitulo(String titulo) {
    jdoSettitulo(this, titulo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegiblePublicacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegiblePublicacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegiblePublicacion localElegiblePublicacion = new ElegiblePublicacion();
    localElegiblePublicacion.jdoFlags = 1;
    localElegiblePublicacion.jdoStateManager = paramStateManager;
    return localElegiblePublicacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegiblePublicacion localElegiblePublicacion = new ElegiblePublicacion();
    localElegiblePublicacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegiblePublicacion.jdoFlags = 1;
    localElegiblePublicacion.jdoStateManager = paramStateManager;
    return localElegiblePublicacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioPublicacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.editorial);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegiblePublicacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.propiedadIntelectual);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.titulo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioPublicacion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.editorial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegiblePublicacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.propiedadIntelectual = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.titulo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegiblePublicacion paramElegiblePublicacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.anioPublicacion = paramElegiblePublicacion.anioPublicacion;
      return;
    case 1:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.editorial = paramElegiblePublicacion.editorial;
      return;
    case 2:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegiblePublicacion.elegible;
      return;
    case 3:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.idElegiblePublicacion = paramElegiblePublicacion.idElegiblePublicacion;
      return;
    case 4:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegiblePublicacion.idSitp;
      return;
    case 5:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramElegiblePublicacion.observaciones;
      return;
    case 6:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.propiedadIntelectual = paramElegiblePublicacion.propiedadIntelectual;
      return;
    case 7:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegiblePublicacion.tiempoSitp;
      return;
    case 8:
      if (paramElegiblePublicacion == null)
        throw new IllegalArgumentException("arg1");
      this.titulo = paramElegiblePublicacion.titulo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegiblePublicacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegiblePublicacion localElegiblePublicacion = (ElegiblePublicacion)paramObject;
    if (localElegiblePublicacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegiblePublicacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegiblePublicacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegiblePublicacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegiblePublicacionPK))
      throw new IllegalArgumentException("arg1");
    ElegiblePublicacionPK localElegiblePublicacionPK = (ElegiblePublicacionPK)paramObject;
    localElegiblePublicacionPK.idElegiblePublicacion = this.idElegiblePublicacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegiblePublicacionPK))
      throw new IllegalArgumentException("arg1");
    ElegiblePublicacionPK localElegiblePublicacionPK = (ElegiblePublicacionPK)paramObject;
    this.idElegiblePublicacion = localElegiblePublicacionPK.idElegiblePublicacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegiblePublicacionPK))
      throw new IllegalArgumentException("arg2");
    ElegiblePublicacionPK localElegiblePublicacionPK = (ElegiblePublicacionPK)paramObject;
    localElegiblePublicacionPK.idElegiblePublicacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegiblePublicacionPK))
      throw new IllegalArgumentException("arg2");
    ElegiblePublicacionPK localElegiblePublicacionPK = (ElegiblePublicacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localElegiblePublicacionPK.idElegiblePublicacion);
  }

  private static final int jdoGetanioPublicacion(ElegiblePublicacion paramElegiblePublicacion)
  {
    if (paramElegiblePublicacion.jdoFlags <= 0)
      return paramElegiblePublicacion.anioPublicacion;
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.anioPublicacion;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 0))
      return paramElegiblePublicacion.anioPublicacion;
    return localStateManager.getIntField(paramElegiblePublicacion, jdoInheritedFieldCount + 0, paramElegiblePublicacion.anioPublicacion);
  }

  private static final void jdoSetanioPublicacion(ElegiblePublicacion paramElegiblePublicacion, int paramInt)
  {
    if (paramElegiblePublicacion.jdoFlags == 0)
    {
      paramElegiblePublicacion.anioPublicacion = paramInt;
      return;
    }
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.anioPublicacion = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegiblePublicacion, jdoInheritedFieldCount + 0, paramElegiblePublicacion.anioPublicacion, paramInt);
  }

  private static final String jdoGeteditorial(ElegiblePublicacion paramElegiblePublicacion)
  {
    if (paramElegiblePublicacion.jdoFlags <= 0)
      return paramElegiblePublicacion.editorial;
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.editorial;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 1))
      return paramElegiblePublicacion.editorial;
    return localStateManager.getStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 1, paramElegiblePublicacion.editorial);
  }

  private static final void jdoSeteditorial(ElegiblePublicacion paramElegiblePublicacion, String paramString)
  {
    if (paramElegiblePublicacion.jdoFlags == 0)
    {
      paramElegiblePublicacion.editorial = paramString;
      return;
    }
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.editorial = paramString;
      return;
    }
    localStateManager.setStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 1, paramElegiblePublicacion.editorial, paramString);
  }

  private static final Elegible jdoGetelegible(ElegiblePublicacion paramElegiblePublicacion)
  {
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.elegible;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 2))
      return paramElegiblePublicacion.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegiblePublicacion, jdoInheritedFieldCount + 2, paramElegiblePublicacion.elegible);
  }

  private static final void jdoSetelegible(ElegiblePublicacion paramElegiblePublicacion, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegiblePublicacion, jdoInheritedFieldCount + 2, paramElegiblePublicacion.elegible, paramElegible);
  }

  private static final long jdoGetidElegiblePublicacion(ElegiblePublicacion paramElegiblePublicacion)
  {
    return paramElegiblePublicacion.idElegiblePublicacion;
  }

  private static final void jdoSetidElegiblePublicacion(ElegiblePublicacion paramElegiblePublicacion, long paramLong)
  {
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.idElegiblePublicacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegiblePublicacion, jdoInheritedFieldCount + 3, paramElegiblePublicacion.idElegiblePublicacion, paramLong);
  }

  private static final int jdoGetidSitp(ElegiblePublicacion paramElegiblePublicacion)
  {
    if (paramElegiblePublicacion.jdoFlags <= 0)
      return paramElegiblePublicacion.idSitp;
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.idSitp;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 4))
      return paramElegiblePublicacion.idSitp;
    return localStateManager.getIntField(paramElegiblePublicacion, jdoInheritedFieldCount + 4, paramElegiblePublicacion.idSitp);
  }

  private static final void jdoSetidSitp(ElegiblePublicacion paramElegiblePublicacion, int paramInt)
  {
    if (paramElegiblePublicacion.jdoFlags == 0)
    {
      paramElegiblePublicacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegiblePublicacion, jdoInheritedFieldCount + 4, paramElegiblePublicacion.idSitp, paramInt);
  }

  private static final String jdoGetobservaciones(ElegiblePublicacion paramElegiblePublicacion)
  {
    if (paramElegiblePublicacion.jdoFlags <= 0)
      return paramElegiblePublicacion.observaciones;
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.observaciones;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 5))
      return paramElegiblePublicacion.observaciones;
    return localStateManager.getStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 5, paramElegiblePublicacion.observaciones);
  }

  private static final void jdoSetobservaciones(ElegiblePublicacion paramElegiblePublicacion, String paramString)
  {
    if (paramElegiblePublicacion.jdoFlags == 0)
    {
      paramElegiblePublicacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 5, paramElegiblePublicacion.observaciones, paramString);
  }

  private static final String jdoGetpropiedadIntelectual(ElegiblePublicacion paramElegiblePublicacion)
  {
    if (paramElegiblePublicacion.jdoFlags <= 0)
      return paramElegiblePublicacion.propiedadIntelectual;
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.propiedadIntelectual;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 6))
      return paramElegiblePublicacion.propiedadIntelectual;
    return localStateManager.getStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 6, paramElegiblePublicacion.propiedadIntelectual);
  }

  private static final void jdoSetpropiedadIntelectual(ElegiblePublicacion paramElegiblePublicacion, String paramString)
  {
    if (paramElegiblePublicacion.jdoFlags == 0)
    {
      paramElegiblePublicacion.propiedadIntelectual = paramString;
      return;
    }
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.propiedadIntelectual = paramString;
      return;
    }
    localStateManager.setStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 6, paramElegiblePublicacion.propiedadIntelectual, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegiblePublicacion paramElegiblePublicacion)
  {
    if (paramElegiblePublicacion.jdoFlags <= 0)
      return paramElegiblePublicacion.tiempoSitp;
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.tiempoSitp;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 7))
      return paramElegiblePublicacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegiblePublicacion, jdoInheritedFieldCount + 7, paramElegiblePublicacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegiblePublicacion paramElegiblePublicacion, Date paramDate)
  {
    if (paramElegiblePublicacion.jdoFlags == 0)
    {
      paramElegiblePublicacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegiblePublicacion, jdoInheritedFieldCount + 7, paramElegiblePublicacion.tiempoSitp, paramDate);
  }

  private static final String jdoGettitulo(ElegiblePublicacion paramElegiblePublicacion)
  {
    if (paramElegiblePublicacion.jdoFlags <= 0)
      return paramElegiblePublicacion.titulo;
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegiblePublicacion.titulo;
    if (localStateManager.isLoaded(paramElegiblePublicacion, jdoInheritedFieldCount + 8))
      return paramElegiblePublicacion.titulo;
    return localStateManager.getStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 8, paramElegiblePublicacion.titulo);
  }

  private static final void jdoSettitulo(ElegiblePublicacion paramElegiblePublicacion, String paramString)
  {
    if (paramElegiblePublicacion.jdoFlags == 0)
    {
      paramElegiblePublicacion.titulo = paramString;
      return;
    }
    StateManager localStateManager = paramElegiblePublicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegiblePublicacion.titulo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegiblePublicacion, jdoInheritedFieldCount + 8, paramElegiblePublicacion.titulo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}