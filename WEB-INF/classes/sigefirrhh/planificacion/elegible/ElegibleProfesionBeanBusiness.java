package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Profesion;
import sigefirrhh.base.personal.ProfesionBeanBusiness;

public class ElegibleProfesionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleProfesion(ElegibleProfesion elegibleProfesion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleProfesion elegibleProfesionNew = 
      (ElegibleProfesion)BeanUtils.cloneBean(
      elegibleProfesion);

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (elegibleProfesionNew.getProfesion() != null) {
      elegibleProfesionNew.setProfesion(
        profesionBeanBusiness.findProfesionById(
        elegibleProfesionNew.getProfesion().getIdProfesion()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleProfesionNew.getElegible() != null) {
      elegibleProfesionNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleProfesionNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleProfesionNew);
  }

  public void updateElegibleProfesion(ElegibleProfesion elegibleProfesion) throws Exception
  {
    ElegibleProfesion elegibleProfesionModify = 
      findElegibleProfesionById(elegibleProfesion.getIdElegibleProfesion());

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (elegibleProfesion.getProfesion() != null) {
      elegibleProfesion.setProfesion(
        profesionBeanBusiness.findProfesionById(
        elegibleProfesion.getProfesion().getIdProfesion()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleProfesion.getElegible() != null) {
      elegibleProfesion.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleProfesion.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleProfesionModify, elegibleProfesion);
  }

  public void deleteElegibleProfesion(ElegibleProfesion elegibleProfesion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleProfesion elegibleProfesionDelete = 
      findElegibleProfesionById(elegibleProfesion.getIdElegibleProfesion());
    pm.deletePersistent(elegibleProfesionDelete);
  }

  public ElegibleProfesion findElegibleProfesionById(long idElegibleProfesion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleProfesion == pIdElegibleProfesion";
    Query query = pm.newQuery(ElegibleProfesion.class, filter);

    query.declareParameters("long pIdElegibleProfesion");

    parameters.put("pIdElegibleProfesion", new Long(idElegibleProfesion));

    Collection colElegibleProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleProfesion.iterator();
    return (ElegibleProfesion)iterator.next();
  }

  public Collection findElegibleProfesionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleProfesionExtent = pm.getExtent(
      ElegibleProfesion.class, true);
    Query query = pm.newQuery(elegibleProfesionExtent);
    query.setOrdering("profesion.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleProfesion.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("profesion.nombre ascending");

    Collection colElegibleProfesion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleProfesion);

    return colElegibleProfesion;
  }
}