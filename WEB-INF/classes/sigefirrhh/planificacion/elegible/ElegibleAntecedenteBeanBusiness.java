package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ElegibleAntecedenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleAntecedente(ElegibleAntecedente elegibleAntecedente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleAntecedente elegibleAntecedenteNew = 
      (ElegibleAntecedente)BeanUtils.cloneBean(
      elegibleAntecedente);

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleAntecedenteNew.getElegible() != null) {
      elegibleAntecedenteNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleAntecedenteNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleAntecedenteNew);
  }

  public void updateElegibleAntecedente(ElegibleAntecedente elegibleAntecedente) throws Exception
  {
    ElegibleAntecedente elegibleAntecedenteModify = 
      findElegibleAntecedenteById(elegibleAntecedente.getIdElegibleAntecedente());

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleAntecedente.getElegible() != null) {
      elegibleAntecedente.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleAntecedente.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleAntecedenteModify, elegibleAntecedente);
  }

  public void deleteElegibleAntecedente(ElegibleAntecedente elegibleAntecedente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleAntecedente elegibleAntecedenteDelete = 
      findElegibleAntecedenteById(elegibleAntecedente.getIdElegibleAntecedente());
    pm.deletePersistent(elegibleAntecedenteDelete);
  }

  public ElegibleAntecedente findElegibleAntecedenteById(long idElegibleAntecedente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleAntecedente == pIdElegibleAntecedente";
    Query query = pm.newQuery(ElegibleAntecedente.class, filter);

    query.declareParameters("long pIdElegibleAntecedente");

    parameters.put("pIdElegibleAntecedente", new Long(idElegibleAntecedente));

    Collection colElegibleAntecedente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleAntecedente.iterator();
    return (ElegibleAntecedente)iterator.next();
  }

  public Collection findElegibleAntecedenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleAntecedenteExtent = pm.getExtent(
      ElegibleAntecedente.class, true);
    Query query = pm.newQuery(elegibleAntecedenteExtent);
    query.setOrdering("nombreInstitucion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleAntecedente.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("nombreInstitucion ascending");

    Collection colElegibleAntecedente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleAntecedente);

    return colElegibleAntecedente;
  }
}