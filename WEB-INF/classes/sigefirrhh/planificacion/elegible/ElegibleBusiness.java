package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class ElegibleBusiness extends AbstractBusiness
  implements Serializable
{
  private ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

  private ElegibleActividadDocenteBeanBusiness elegibleActividadDocenteBeanBusiness = new ElegibleActividadDocenteBeanBusiness();

  private ElegibleAfiliacionBeanBusiness elegibleAfiliacionBeanBusiness = new ElegibleAfiliacionBeanBusiness();

  private ElegibleAntecedenteBeanBusiness elegibleAntecedenteBeanBusiness = new ElegibleAntecedenteBeanBusiness();

  private ElegibleCertificacionBeanBusiness elegibleCertificacionBeanBusiness = new ElegibleCertificacionBeanBusiness();

  private ElegibleEducacionBeanBusiness elegibleEducacionBeanBusiness = new ElegibleEducacionBeanBusiness();

  private ElegibleEstudioBeanBusiness elegibleEstudioBeanBusiness = new ElegibleEstudioBeanBusiness();

  private ElegibleExperienciaBeanBusiness elegibleExperienciaBeanBusiness = new ElegibleExperienciaBeanBusiness();

  private ElegibleFamiliarBeanBusiness elegibleFamiliarBeanBusiness = new ElegibleFamiliarBeanBusiness();

  private ElegibleHabilidadBeanBusiness elegibleHabilidadBeanBusiness = new ElegibleHabilidadBeanBusiness();

  private ElegibleIdiomaBeanBusiness elegibleIdiomaBeanBusiness = new ElegibleIdiomaBeanBusiness();

  private ElegibleOrganismoBeanBusiness elegibleOrganismoBeanBusiness = new ElegibleOrganismoBeanBusiness();

  private ElegibleOtraActividadBeanBusiness elegibleOtraActividadBeanBusiness = new ElegibleOtraActividadBeanBusiness();

  private ElegibleProfesionBeanBusiness elegibleProfesionBeanBusiness = new ElegibleProfesionBeanBusiness();

  private ElegiblePublicacionBeanBusiness elegiblePublicacionBeanBusiness = new ElegiblePublicacionBeanBusiness();

  public void updateElegible(Elegible elegible)
    throws Exception
  {
    this.elegibleBeanBusiness.updateElegible(elegible);
  }

  public Elegible findElegibleById(long elegibleId)
    throws Exception
  {
    return this.elegibleBeanBusiness.findElegibleById(elegibleId);
  }

  public Collection findAllElegible() throws Exception {
    return this.elegibleBeanBusiness.findElegibleAll();
  }

  public Collection findElegibleByCedula(int cedula)
    throws Exception
  {
    return this.elegibleBeanBusiness.findByCedula(cedula);
  }

  public Collection findElegibleByPrimerApellido(String primerApellido)
    throws Exception
  {
    return this.elegibleBeanBusiness.findByPrimerApellido(primerApellido);
  }

  public Collection findElegibleByPrimerNombre(String primerNombre)
    throws Exception
  {
    return this.elegibleBeanBusiness.findByPrimerNombre(primerNombre);
  }

  public Collection findElegibleByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    return this.elegibleBeanBusiness.findByNombresApellidos(
      primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo);
  }

  public Collection findElegibleByCedula(int cedula, long idOrganismo) throws Exception
  {
    return this.elegibleBeanBusiness.findByCedula(cedula, idOrganismo);
  }

  public void addElegible(Elegible elegible, long idOrganismo) throws Exception
  {
    this.elegibleBeanBusiness.addElegible(elegible, idOrganismo);
  }

  public void deleteElegible(Elegible elegible, long idOrganismo) throws Exception {
    this.elegibleBeanBusiness.deleteElegible(elegible, idOrganismo);
  }

  public void addElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente)
    throws Exception
  {
    this.elegibleActividadDocenteBeanBusiness.addElegibleActividadDocente(elegibleActividadDocente);
  }

  public void updateElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente) throws Exception {
    this.elegibleActividadDocenteBeanBusiness.updateElegibleActividadDocente(elegibleActividadDocente);
  }

  public void deleteElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente) throws Exception {
    this.elegibleActividadDocenteBeanBusiness.deleteElegibleActividadDocente(elegibleActividadDocente);
  }

  public ElegibleActividadDocente findElegibleActividadDocenteById(long elegibleActividadDocenteId) throws Exception {
    return this.elegibleActividadDocenteBeanBusiness.findElegibleActividadDocenteById(elegibleActividadDocenteId);
  }

  public Collection findAllElegibleActividadDocente() throws Exception {
    return this.elegibleActividadDocenteBeanBusiness.findElegibleActividadDocenteAll();
  }

  public Collection findElegibleActividadDocenteByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleActividadDocenteBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion)
    throws Exception
  {
    this.elegibleAfiliacionBeanBusiness.addElegibleAfiliacion(elegibleAfiliacion);
  }

  public void updateElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion) throws Exception {
    this.elegibleAfiliacionBeanBusiness.updateElegibleAfiliacion(elegibleAfiliacion);
  }

  public void deleteElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion) throws Exception {
    this.elegibleAfiliacionBeanBusiness.deleteElegibleAfiliacion(elegibleAfiliacion);
  }

  public ElegibleAfiliacion findElegibleAfiliacionById(long elegibleAfiliacionId) throws Exception {
    return this.elegibleAfiliacionBeanBusiness.findElegibleAfiliacionById(elegibleAfiliacionId);
  }

  public Collection findAllElegibleAfiliacion() throws Exception {
    return this.elegibleAfiliacionBeanBusiness.findElegibleAfiliacionAll();
  }

  public Collection findElegibleAfiliacionByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleAfiliacionBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleAntecedente(ElegibleAntecedente elegibleAntecedente)
    throws Exception
  {
    this.elegibleAntecedenteBeanBusiness.addElegibleAntecedente(elegibleAntecedente);
  }

  public void updateElegibleAntecedente(ElegibleAntecedente elegibleAntecedente) throws Exception {
    this.elegibleAntecedenteBeanBusiness.updateElegibleAntecedente(elegibleAntecedente);
  }

  public void deleteElegibleAntecedente(ElegibleAntecedente elegibleAntecedente) throws Exception {
    this.elegibleAntecedenteBeanBusiness.deleteElegibleAntecedente(elegibleAntecedente);
  }

  public ElegibleAntecedente findElegibleAntecedenteById(long elegibleAntecedenteId) throws Exception {
    return this.elegibleAntecedenteBeanBusiness.findElegibleAntecedenteById(elegibleAntecedenteId);
  }

  public Collection findAllElegibleAntecedente() throws Exception {
    return this.elegibleAntecedenteBeanBusiness.findElegibleAntecedenteAll();
  }

  public Collection findElegibleAntecedenteByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleAntecedenteBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleCertificacion(ElegibleCertificacion elegibleCertificacion)
    throws Exception
  {
    this.elegibleCertificacionBeanBusiness.addElegibleCertificacion(elegibleCertificacion);
  }

  public void updateElegibleCertificacion(ElegibleCertificacion elegibleCertificacion) throws Exception {
    this.elegibleCertificacionBeanBusiness.updateElegibleCertificacion(elegibleCertificacion);
  }

  public void deleteElegibleCertificacion(ElegibleCertificacion elegibleCertificacion) throws Exception {
    this.elegibleCertificacionBeanBusiness.deleteElegibleCertificacion(elegibleCertificacion);
  }

  public ElegibleCertificacion findElegibleCertificacionById(long elegibleCertificacionId) throws Exception {
    return this.elegibleCertificacionBeanBusiness.findElegibleCertificacionById(elegibleCertificacionId);
  }

  public Collection findAllElegibleCertificacion() throws Exception {
    return this.elegibleCertificacionBeanBusiness.findElegibleCertificacionAll();
  }

  public Collection findElegibleCertificacionByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    return this.elegibleCertificacionBeanBusiness.findByAreaConocimiento(idAreaConocimiento);
  }

  public Collection findElegibleCertificacionByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleCertificacionBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleEducacion(ElegibleEducacion elegibleEducacion)
    throws Exception
  {
    this.elegibleEducacionBeanBusiness.addElegibleEducacion(elegibleEducacion);
  }

  public void updateElegibleEducacion(ElegibleEducacion elegibleEducacion) throws Exception {
    this.elegibleEducacionBeanBusiness.updateElegibleEducacion(elegibleEducacion);
  }

  public void deleteElegibleEducacion(ElegibleEducacion elegibleEducacion) throws Exception {
    this.elegibleEducacionBeanBusiness.deleteElegibleEducacion(elegibleEducacion);
  }

  public ElegibleEducacion findElegibleEducacionById(long elegibleEducacionId) throws Exception {
    return this.elegibleEducacionBeanBusiness.findElegibleEducacionById(elegibleEducacionId);
  }

  public Collection findAllElegibleEducacion() throws Exception {
    return this.elegibleEducacionBeanBusiness.findElegibleEducacionAll();
  }

  public Collection findElegibleEducacionByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleEducacionBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleEstudio(ElegibleEstudio elegibleEstudio)
    throws Exception
  {
    this.elegibleEstudioBeanBusiness.addElegibleEstudio(elegibleEstudio);
  }

  public void updateElegibleEstudio(ElegibleEstudio elegibleEstudio) throws Exception {
    this.elegibleEstudioBeanBusiness.updateElegibleEstudio(elegibleEstudio);
  }

  public void deleteElegibleEstudio(ElegibleEstudio elegibleEstudio) throws Exception {
    this.elegibleEstudioBeanBusiness.deleteElegibleEstudio(elegibleEstudio);
  }

  public ElegibleEstudio findElegibleEstudioById(long elegibleEstudioId) throws Exception {
    return this.elegibleEstudioBeanBusiness.findElegibleEstudioById(elegibleEstudioId);
  }

  public Collection findAllElegibleEstudio() throws Exception {
    return this.elegibleEstudioBeanBusiness.findElegibleEstudioAll();
  }

  public Collection findElegibleEstudioByTipoCurso(long idTipoCurso)
    throws Exception
  {
    return this.elegibleEstudioBeanBusiness.findByTipoCurso(idTipoCurso);
  }

  public Collection findElegibleEstudioByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    return this.elegibleEstudioBeanBusiness.findByAreaConocimiento(idAreaConocimiento);
  }

  public Collection findElegibleEstudioByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleEstudioBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleExperiencia(ElegibleExperiencia elegibleExperiencia)
    throws Exception
  {
    this.elegibleExperienciaBeanBusiness.addElegibleExperiencia(elegibleExperiencia);
  }

  public void updateElegibleExperiencia(ElegibleExperiencia elegibleExperiencia) throws Exception {
    this.elegibleExperienciaBeanBusiness.updateElegibleExperiencia(elegibleExperiencia);
  }

  public void deleteElegibleExperiencia(ElegibleExperiencia elegibleExperiencia) throws Exception {
    this.elegibleExperienciaBeanBusiness.deleteElegibleExperiencia(elegibleExperiencia);
  }

  public ElegibleExperiencia findElegibleExperienciaById(long elegibleExperienciaId) throws Exception {
    return this.elegibleExperienciaBeanBusiness.findElegibleExperienciaById(elegibleExperienciaId);
  }

  public Collection findAllElegibleExperiencia() throws Exception {
    return this.elegibleExperienciaBeanBusiness.findElegibleExperienciaAll();
  }

  public Collection findElegibleExperienciaByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleExperienciaBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleFamiliar(ElegibleFamiliar elegibleFamiliar)
    throws Exception
  {
    this.elegibleFamiliarBeanBusiness.addElegibleFamiliar(elegibleFamiliar);
  }

  public void updateElegibleFamiliar(ElegibleFamiliar elegibleFamiliar) throws Exception {
    this.elegibleFamiliarBeanBusiness.updateElegibleFamiliar(elegibleFamiliar);
  }

  public void deleteElegibleFamiliar(ElegibleFamiliar elegibleFamiliar) throws Exception {
    this.elegibleFamiliarBeanBusiness.deleteElegibleFamiliar(elegibleFamiliar);
  }

  public ElegibleFamiliar findElegibleFamiliarById(long elegibleFamiliarId) throws Exception {
    return this.elegibleFamiliarBeanBusiness.findElegibleFamiliarById(elegibleFamiliarId);
  }

  public Collection findAllElegibleFamiliar() throws Exception {
    return this.elegibleFamiliarBeanBusiness.findElegibleFamiliarAll();
  }

  public Collection findElegibleFamiliarByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleFamiliarBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleHabilidad(ElegibleHabilidad elegibleHabilidad)
    throws Exception
  {
    this.elegibleHabilidadBeanBusiness.addElegibleHabilidad(elegibleHabilidad);
  }

  public void updateElegibleHabilidad(ElegibleHabilidad elegibleHabilidad) throws Exception {
    this.elegibleHabilidadBeanBusiness.updateElegibleHabilidad(elegibleHabilidad);
  }

  public void deleteElegibleHabilidad(ElegibleHabilidad elegibleHabilidad) throws Exception {
    this.elegibleHabilidadBeanBusiness.deleteElegibleHabilidad(elegibleHabilidad);
  }

  public ElegibleHabilidad findElegibleHabilidadById(long elegibleHabilidadId) throws Exception {
    return this.elegibleHabilidadBeanBusiness.findElegibleHabilidadById(elegibleHabilidadId);
  }

  public Collection findAllElegibleHabilidad() throws Exception {
    return this.elegibleHabilidadBeanBusiness.findElegibleHabilidadAll();
  }

  public Collection findElegibleHabilidadByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleHabilidadBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleIdioma(ElegibleIdioma elegibleIdioma)
    throws Exception
  {
    this.elegibleIdiomaBeanBusiness.addElegibleIdioma(elegibleIdioma);
  }

  public void updateElegibleIdioma(ElegibleIdioma elegibleIdioma) throws Exception {
    this.elegibleIdiomaBeanBusiness.updateElegibleIdioma(elegibleIdioma);
  }

  public void deleteElegibleIdioma(ElegibleIdioma elegibleIdioma) throws Exception {
    this.elegibleIdiomaBeanBusiness.deleteElegibleIdioma(elegibleIdioma);
  }

  public ElegibleIdioma findElegibleIdiomaById(long elegibleIdiomaId) throws Exception {
    return this.elegibleIdiomaBeanBusiness.findElegibleIdiomaById(elegibleIdiomaId);
  }

  public Collection findAllElegibleIdioma() throws Exception {
    return this.elegibleIdiomaBeanBusiness.findElegibleIdiomaAll();
  }

  public Collection findElegibleIdiomaByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleIdiomaBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleOrganismo(ElegibleOrganismo elegibleOrganismo)
    throws Exception
  {
    this.elegibleOrganismoBeanBusiness.addElegibleOrganismo(elegibleOrganismo);
  }

  public void updateElegibleOrganismo(ElegibleOrganismo elegibleOrganismo) throws Exception {
    this.elegibleOrganismoBeanBusiness.updateElegibleOrganismo(elegibleOrganismo);
  }

  public void deleteElegibleOrganismo(ElegibleOrganismo elegibleOrganismo) throws Exception {
    this.elegibleOrganismoBeanBusiness.deleteElegibleOrganismo(elegibleOrganismo);
  }

  public ElegibleOrganismo findElegibleOrganismoById(long elegibleOrganismoId) throws Exception {
    return this.elegibleOrganismoBeanBusiness.findElegibleOrganismoById(elegibleOrganismoId);
  }

  public Collection findAllElegibleOrganismo() throws Exception {
    return this.elegibleOrganismoBeanBusiness.findElegibleOrganismoAll();
  }

  public void addElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad)
    throws Exception
  {
    this.elegibleOtraActividadBeanBusiness.addElegibleOtraActividad(elegibleOtraActividad);
  }

  public void updateElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad) throws Exception {
    this.elegibleOtraActividadBeanBusiness.updateElegibleOtraActividad(elegibleOtraActividad);
  }

  public void deleteElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad) throws Exception {
    this.elegibleOtraActividadBeanBusiness.deleteElegibleOtraActividad(elegibleOtraActividad);
  }

  public ElegibleOtraActividad findElegibleOtraActividadById(long elegibleOtraActividadId) throws Exception {
    return this.elegibleOtraActividadBeanBusiness.findElegibleOtraActividadById(elegibleOtraActividadId);
  }

  public Collection findAllElegibleOtraActividad() throws Exception {
    return this.elegibleOtraActividadBeanBusiness.findElegibleOtraActividadAll();
  }

  public Collection findElegibleOtraActividadByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleOtraActividadBeanBusiness.findByElegible(idElegible);
  }

  public void addElegibleProfesion(ElegibleProfesion elegibleProfesion)
    throws Exception
  {
    this.elegibleProfesionBeanBusiness.addElegibleProfesion(elegibleProfesion);
  }

  public void updateElegibleProfesion(ElegibleProfesion elegibleProfesion) throws Exception {
    this.elegibleProfesionBeanBusiness.updateElegibleProfesion(elegibleProfesion);
  }

  public void deleteElegibleProfesion(ElegibleProfesion elegibleProfesion) throws Exception {
    this.elegibleProfesionBeanBusiness.deleteElegibleProfesion(elegibleProfesion);
  }

  public ElegibleProfesion findElegibleProfesionById(long elegibleProfesionId) throws Exception {
    return this.elegibleProfesionBeanBusiness.findElegibleProfesionById(elegibleProfesionId);
  }

  public Collection findAllElegibleProfesion() throws Exception {
    return this.elegibleProfesionBeanBusiness.findElegibleProfesionAll();
  }

  public Collection findElegibleProfesionByElegible(long idElegible)
    throws Exception
  {
    return this.elegibleProfesionBeanBusiness.findByElegible(idElegible);
  }

  public void addElegiblePublicacion(ElegiblePublicacion elegiblePublicacion)
    throws Exception
  {
    this.elegiblePublicacionBeanBusiness.addElegiblePublicacion(elegiblePublicacion);
  }

  public void updateElegiblePublicacion(ElegiblePublicacion elegiblePublicacion) throws Exception {
    this.elegiblePublicacionBeanBusiness.updateElegiblePublicacion(elegiblePublicacion);
  }

  public void deleteElegiblePublicacion(ElegiblePublicacion elegiblePublicacion) throws Exception {
    this.elegiblePublicacionBeanBusiness.deleteElegiblePublicacion(elegiblePublicacion);
  }

  public ElegiblePublicacion findElegiblePublicacionById(long elegiblePublicacionId) throws Exception {
    return this.elegiblePublicacionBeanBusiness.findElegiblePublicacionById(elegiblePublicacionId);
  }

  public Collection findAllElegiblePublicacion() throws Exception {
    return this.elegiblePublicacionBeanBusiness.findElegiblePublicacionAll();
  }

  public Collection findElegiblePublicacionByElegible(long idElegible)
    throws Exception
  {
    return this.elegiblePublicacionBeanBusiness.findByElegible(idElegible);
  }
}