package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleOtraActividadPK
  implements Serializable
{
  public long idElegibleOtraActividad;

  public ElegibleOtraActividadPK()
  {
  }

  public ElegibleOtraActividadPK(long idElegibleOtraActividad)
  {
    this.idElegibleOtraActividad = idElegibleOtraActividad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleOtraActividadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleOtraActividadPK thatPK)
  {
    return 
      this.idElegibleOtraActividad == thatPK.idElegibleOtraActividad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleOtraActividad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleOtraActividad);
  }
}