package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class ElegibleOrganismo
  implements Serializable, PersistenceCapable
{
  private long idElegibleOrganismo;
  private Elegible elegible;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "elegible", "idElegibleOrganismo", "organismo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public long getIdElegibleOrganismo()
  {
    return jdoGetidElegibleOrganismo(this);
  }

  public void setIdElegibleOrganismo(long idElegibleOrganismo)
  {
    jdoSetidElegibleOrganismo(this, idElegibleOrganismo);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleOrganismo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleOrganismo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleOrganismo localElegibleOrganismo = new ElegibleOrganismo();
    localElegibleOrganismo.jdoFlags = 1;
    localElegibleOrganismo.jdoStateManager = paramStateManager;
    return localElegibleOrganismo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleOrganismo localElegibleOrganismo = new ElegibleOrganismo();
    localElegibleOrganismo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleOrganismo.jdoFlags = 1;
    localElegibleOrganismo.jdoStateManager = paramStateManager;
    return localElegibleOrganismo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleOrganismo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleOrganismo paramElegibleOrganismo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleOrganismo.elegible;
      return;
    case 1:
      if (paramElegibleOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleOrganismo = paramElegibleOrganismo.idElegibleOrganismo;
      return;
    case 2:
      if (paramElegibleOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramElegibleOrganismo.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleOrganismo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleOrganismo localElegibleOrganismo = (ElegibleOrganismo)paramObject;
    if (localElegibleOrganismo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleOrganismo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleOrganismoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleOrganismoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleOrganismoPK))
      throw new IllegalArgumentException("arg1");
    ElegibleOrganismoPK localElegibleOrganismoPK = (ElegibleOrganismoPK)paramObject;
    localElegibleOrganismoPK.idElegibleOrganismo = this.idElegibleOrganismo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleOrganismoPK))
      throw new IllegalArgumentException("arg1");
    ElegibleOrganismoPK localElegibleOrganismoPK = (ElegibleOrganismoPK)paramObject;
    this.idElegibleOrganismo = localElegibleOrganismoPK.idElegibleOrganismo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleOrganismoPK))
      throw new IllegalArgumentException("arg2");
    ElegibleOrganismoPK localElegibleOrganismoPK = (ElegibleOrganismoPK)paramObject;
    localElegibleOrganismoPK.idElegibleOrganismo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleOrganismoPK))
      throw new IllegalArgumentException("arg2");
    ElegibleOrganismoPK localElegibleOrganismoPK = (ElegibleOrganismoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localElegibleOrganismoPK.idElegibleOrganismo);
  }

  private static final Elegible jdoGetelegible(ElegibleOrganismo paramElegibleOrganismo)
  {
    StateManager localStateManager = paramElegibleOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleOrganismo.elegible;
    if (localStateManager.isLoaded(paramElegibleOrganismo, jdoInheritedFieldCount + 0))
      return paramElegibleOrganismo.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleOrganismo, jdoInheritedFieldCount + 0, paramElegibleOrganismo.elegible);
  }

  private static final void jdoSetelegible(ElegibleOrganismo paramElegibleOrganismo, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleOrganismo.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleOrganismo, jdoInheritedFieldCount + 0, paramElegibleOrganismo.elegible, paramElegible);
  }

  private static final long jdoGetidElegibleOrganismo(ElegibleOrganismo paramElegibleOrganismo)
  {
    return paramElegibleOrganismo.idElegibleOrganismo;
  }

  private static final void jdoSetidElegibleOrganismo(ElegibleOrganismo paramElegibleOrganismo, long paramLong)
  {
    StateManager localStateManager = paramElegibleOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleOrganismo.idElegibleOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleOrganismo, jdoInheritedFieldCount + 1, paramElegibleOrganismo.idElegibleOrganismo, paramLong);
  }

  private static final Organismo jdoGetorganismo(ElegibleOrganismo paramElegibleOrganismo)
  {
    StateManager localStateManager = paramElegibleOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleOrganismo.organismo;
    if (localStateManager.isLoaded(paramElegibleOrganismo, jdoInheritedFieldCount + 2))
      return paramElegibleOrganismo.organismo;
    return (Organismo)localStateManager.getObjectField(paramElegibleOrganismo, jdoInheritedFieldCount + 2, paramElegibleOrganismo.organismo);
  }

  private static final void jdoSetorganismo(ElegibleOrganismo paramElegibleOrganismo, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramElegibleOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleOrganismo.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramElegibleOrganismo, jdoInheritedFieldCount + 2, paramElegibleOrganismo.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}