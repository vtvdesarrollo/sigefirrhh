package sigefirrhh.planificacion.elegible;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.PersonalFacade;
import sigefirrhh.base.personal.Titulo;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class ElegibleEducacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ElegibleEducacionForm.class.getName());
  private ElegibleEducacion elegibleEducacion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private PersonalFacade personalFacade = new PersonalFacade();
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private ElegibleFacade elegibleFacade = new ElegibleFacade();
  private Collection resultElegible;
  private Elegible elegible;
  private boolean selectedElegible;
  private int findElegibleCedula;
  private String findElegiblePrimerNombre;
  private String findElegibleSegundoNombre;
  private String findElegiblePrimerApellido;
  private String findElegibleSegundoApellido;
  private boolean showResultElegible;
  private boolean showAddResultElegible;
  private boolean showResult;
  private String findSelectElegible;
  private Collection colNivelEducativo;
  private Collection colCarrera;
  private Collection colNivelEducativoForTitulo;
  private Collection colTitulo;
  private Collection colPaisForCiudad;
  private Collection colEstadoForCiudad;
  private Collection colCiudad;
  private Collection colElegible;
  private String selectNivelEducativo;
  private String selectCarrera;
  private String selectNivelEducativoForTitulo;
  private String selectTitulo;
  private String selectPaisForCiudad;
  private String selectEstadoForCiudad;
  private String selectCiudad;
  private String selectElegible;
  private Object stateScrollElegible = null;
  private Object stateResultElegible = null;

  private Object stateScrollElegibleEducacionByElegible = null;
  private Object stateResultElegibleEducacionByElegible = null;

  public String getSelectNivelEducativo()
  {
    return this.selectNivelEducativo;
  }
  public void setSelectNivelEducativo(String valNivelEducativo) {
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    this.elegibleEducacion.setNivelEducativo(null);
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      if (String.valueOf(nivelEducativo.getIdNivelEducativo()).equals(
        valNivelEducativo)) {
        this.elegibleEducacion.setNivelEducativo(
          nivelEducativo);
      }
    }
    this.selectNivelEducativo = valNivelEducativo;
  }
  public String getSelectCarrera() {
    return this.selectCarrera;
  }
  public void setSelectCarrera(String valCarrera) {
    Iterator iterator = this.colCarrera.iterator();
    Carrera carrera = null;
    this.elegibleEducacion.setCarrera(null);
    while (iterator.hasNext()) {
      carrera = (Carrera)iterator.next();
      if (String.valueOf(carrera.getIdCarrera()).equals(
        valCarrera)) {
        this.elegibleEducacion.setCarrera(
          carrera);
      }
    }
    this.selectCarrera = valCarrera;
  }
  public String getSelectNivelEducativoForTitulo() {
    return this.selectNivelEducativoForTitulo;
  }
  public void setSelectNivelEducativoForTitulo(String valNivelEducativoForTitulo) {
    this.selectNivelEducativoForTitulo = valNivelEducativoForTitulo;
  }
  public void changeNivelEducativoForTitulo(ValueChangeEvent event) {
    long idNivelEducativo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colTitulo = null;
      if (idNivelEducativo > 0L)
        this.colTitulo = 
          this.personalFacade.findTituloByNivelEducativo(
          idNivelEducativo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowNivelEducativoForTitulo() { return this.colNivelEducativoForTitulo != null; }

  public String getSelectTitulo() {
    return this.selectTitulo;
  }
  public void setSelectTitulo(String valTitulo) {
    Iterator iterator = this.colTitulo.iterator();
    Titulo titulo = null;
    this.elegibleEducacion.setTitulo(null);
    while (iterator.hasNext()) {
      titulo = (Titulo)iterator.next();
      if (String.valueOf(titulo.getIdTitulo()).equals(
        valTitulo)) {
        this.elegibleEducacion.setTitulo(
          titulo);
      }
    }
    this.selectTitulo = valTitulo;
  }
  public boolean isShowTitulo() {
    return this.colTitulo != null;
  }
  public String getSelectPaisForCiudad() {
    return this.selectPaisForCiudad;
  }
  public void setSelectPaisForCiudad(String valPaisForCiudad) {
    this.selectPaisForCiudad = valPaisForCiudad;
  }
  public void changePaisForCiudad(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      this.colEstadoForCiudad = null;
      if (idPais > 0L)
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowPaisForCiudad() { return this.colPaisForCiudad != null; }

  public String getSelectEstadoForCiudad() {
    return this.selectEstadoForCiudad;
  }
  public void setSelectEstadoForCiudad(String valEstadoForCiudad) {
    this.selectEstadoForCiudad = valEstadoForCiudad;
  }
  public void changeEstadoForCiudad(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudad = null;
      if (idEstado > 0L)
        this.colCiudad = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowEstadoForCiudad() { return this.colEstadoForCiudad != null; }

  public String getSelectCiudad() {
    return this.selectCiudad;
  }
  public void setSelectCiudad(String valCiudad) {
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    this.elegibleEducacion.setCiudad(null);
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      if (String.valueOf(ciudad.getIdCiudad()).equals(
        valCiudad)) {
        this.elegibleEducacion.setCiudad(
          ciudad);
      }
    }
    this.selectCiudad = valCiudad;
  }
  public boolean isShowCiudad() {
    return this.colCiudad != null;
  }
  public String getSelectElegible() {
    return this.selectElegible;
  }
  public void setSelectElegible(String valElegible) {
    Iterator iterator = this.colElegible.iterator();
    Elegible elegible = null;
    this.elegibleEducacion.setElegible(null);
    while (iterator.hasNext()) {
      elegible = (Elegible)iterator.next();
      if (String.valueOf(elegible.getIdElegible()).equals(
        valElegible)) {
        this.elegibleEducacion.setElegible(
          elegible);
      }
    }
    this.selectElegible = valElegible;
  }
  public Collection getResult() {
    return this.result;
  }

  public ElegibleEducacion getElegibleEducacion() {
    if (this.elegibleEducacion == null) {
      this.elegibleEducacion = new ElegibleEducacion();
    }
    return this.elegibleEducacion;
  }

  public ElegibleEducacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColNivelEducativo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativo.iterator();
    NivelEducativo nivelEducativo = null;
    while (iterator.hasNext()) {
      nivelEducativo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativo.getIdNivelEducativo()), 
        nivelEducativo.toString()));
    }
    return col;
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleEducacion.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColCarrera()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCarrera.iterator();
    Carrera carrera = null;
    while (iterator.hasNext()) {
      carrera = (Carrera)iterator.next();
      col.add(new SelectItem(
        String.valueOf(carrera.getIdCarrera()), 
        carrera.toString()));
    }
    return col;
  }

  public Collection getColNivelEducativoForTitulo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colNivelEducativoForTitulo.iterator();
    NivelEducativo nivelEducativoForTitulo = null;
    while (iterator.hasNext()) {
      nivelEducativoForTitulo = (NivelEducativo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(nivelEducativoForTitulo.getIdNivelEducativo()), 
        nivelEducativoForTitulo.toString()));
    }
    return col;
  }

  public Collection getColTitulo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTitulo.iterator();
    Titulo titulo = null;
    while (iterator.hasNext()) {
      titulo = (Titulo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(titulo.getIdTitulo()), 
        titulo.toString()));
    }
    return col;
  }

  public Collection getListRegistroTitulo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleEducacion.LISTA_REGISTRO_TITULO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudad.iterator();
    Pais paisForCiudad = null;
    while (iterator.hasNext()) {
      paisForCiudad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudad.getIdPais()), 
        paisForCiudad.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudad.iterator();
    Estado estadoForCiudad = null;
    while (iterator.hasNext()) {
      estadoForCiudad = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudad.getIdEstado()), 
        estadoForCiudad.toString()));
    }
    return col;
  }

  public Collection getColCiudad()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudad.iterator();
    Ciudad ciudad = null;
    while (iterator.hasNext()) {
      ciudad = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudad.getIdCiudad()), 
        ciudad.toString()));
    }
    return col;
  }

  public Collection getListSector() {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleEducacion.LISTA_SECTOR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListBecado() {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleEducacion.LISTA_BECADO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListReembolso()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleEducacion.LISTA_REEMBOLSO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColElegible()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colElegible.iterator();
    Elegible elegible = null;
    while (iterator.hasNext()) {
      elegible = (Elegible)iterator.next();
      col.add(new SelectItem(
        String.valueOf(elegible.getIdElegible()), 
        elegible.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colNivelEducativo = 
        this.personalFacade.findAllNivelEducativo();
      this.colCarrera = 
        this.personalFacade.findAllCarrera();
      this.colNivelEducativoForTitulo = 
        this.personalFacade.findAllNivelEducativo();
      this.colPaisForCiudad = 
        this.ubicacionFacade.findAllPais();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findElegibleByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultElegible();

      this.result = null;
      this.showResult = false;

      this.resultElegible = 
        this.elegibleFacade.findElegibleByCedula(this.findElegibleCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultElegible = 
        ((this.resultElegible != null) && (!this.resultElegible.isEmpty()));

      if (!this.showResultElegible)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findElegibleCedula = 0;
    this.findElegiblePrimerNombre = null;
    this.findElegibleSegundoNombre = null;
    this.findElegiblePrimerApellido = null;
    this.findElegibleSegundoApellido = null;
    return null;
  }

  public String findElegibleByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultElegible();

      this.result = null;
      this.showResult = false;

      if (((this.findElegiblePrimerNombre == null) || (this.findElegiblePrimerNombre.equals(""))) && 
        ((this.findElegibleSegundoNombre == null) || (this.findElegibleSegundoNombre.equals(""))) && 
        ((this.findElegiblePrimerApellido == null) || (this.findElegiblePrimerApellido.equals(""))) && (
        (this.findElegibleSegundoApellido == null) || (this.findElegibleSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultElegible = 
          this.elegibleFacade.findElegibleByNombresApellidos(
          this.findElegiblePrimerNombre, 
          this.findElegibleSegundoNombre, 
          this.findElegiblePrimerApellido, 
          this.findElegibleSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultElegible = 
          ((this.resultElegible != null) && (!this.resultElegible.isEmpty()));
        if (!this.showResultElegible)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findElegibleCedula = 0;
    this.findElegiblePrimerNombre = null;
    this.findElegibleSegundoNombre = null;
    this.findElegiblePrimerApellido = null;
    this.findElegibleSegundoApellido = null;

    return null;
  }

  public String findElegibleEducacionByElegible()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectElegible();
      if (!this.adding) {
        this.result = 
          this.elegibleFacade.findElegibleEducacionByElegible(
          this.elegible.getIdElegible());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectElegibleEducacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectNivelEducativo = null;
    this.selectCarrera = null;
    this.selectTitulo = null;
    this.selectNivelEducativoForTitulo = null;

    this.selectCiudad = null;
    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    this.selectElegible = null;

    long idElegibleEducacion = 
      Long.parseLong((String)requestParameterMap.get("idElegibleEducacion"));
    try
    {
      this.elegibleEducacion = 
        this.elegibleFacade.findElegibleEducacionById(
        idElegibleEducacion);

      if (this.elegibleEducacion.getNivelEducativo() != null) {
        this.selectNivelEducativo = 
          String.valueOf(this.elegibleEducacion.getNivelEducativo().getIdNivelEducativo());
      }
      if (this.elegibleEducacion.getCarrera() != null) {
        this.selectCarrera = 
          String.valueOf(this.elegibleEducacion.getCarrera().getIdCarrera());
      }
      if (this.elegibleEducacion.getTitulo() != null) {
        this.selectTitulo = 
          String.valueOf(this.elegibleEducacion.getTitulo().getIdTitulo());
      }
      if (this.elegibleEducacion.getCiudad() != null) {
        this.selectCiudad = 
          String.valueOf(this.elegibleEducacion.getCiudad().getIdCiudad());
      }
      if (this.elegibleEducacion.getElegible() != null) {
        this.selectElegible = 
          String.valueOf(this.elegibleEducacion.getElegible().getIdElegible());
      }

      Titulo titulo = null;
      NivelEducativo nivelEducativoForTitulo = null;
      Ciudad ciudad = null;
      Estado estadoForCiudad = null;
      Pais paisForCiudad = null;

      if (this.elegibleEducacion.getTitulo() != null) {
        long idTitulo = 
          this.elegibleEducacion.getTitulo().getIdTitulo();
        this.selectTitulo = String.valueOf(idTitulo);
        titulo = this.personalFacade.findTituloById(
          idTitulo);
        this.colTitulo = this.personalFacade.findTituloByNivelEducativo(
          titulo.getNivelEducativo().getIdNivelEducativo());

        long idNivelEducativoForTitulo = 
          this.elegibleEducacion.getTitulo().getNivelEducativo().getIdNivelEducativo();
        this.selectNivelEducativoForTitulo = String.valueOf(idNivelEducativoForTitulo);
        nivelEducativoForTitulo = 
          this.personalFacade.findNivelEducativoById(
          idNivelEducativoForTitulo);
        this.colNivelEducativoForTitulo = 
          this.personalFacade.findAllNivelEducativo();
      }
      if (this.elegibleEducacion.getCiudad() != null) {
        long idCiudad = 
          this.elegibleEducacion.getCiudad().getIdCiudad();
        this.selectCiudad = String.valueOf(idCiudad);
        ciudad = this.ubicacionFacade.findCiudadById(
          idCiudad);
        this.colCiudad = this.ubicacionFacade.findCiudadByEstado(
          ciudad.getEstado().getIdEstado());

        long idEstadoForCiudad = 
          this.elegibleEducacion.getCiudad().getEstado().getIdEstado();
        this.selectEstadoForCiudad = String.valueOf(idEstadoForCiudad);
        estadoForCiudad = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudad);
        this.colEstadoForCiudad = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudad.getPais().getIdPais());
        long idPaisForCiudad = 
          estadoForCiudad.getPais().getIdPais();
        this.selectPaisForCiudad = String.valueOf(idPaisForCiudad);
        paisForCiudad = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudad);
        this.colPaisForCiudad = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectElegible()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idElegible = 
      Long.parseLong((String)requestParameterMap.get("idElegible"));
    try
    {
      this.elegible = 
        this.elegibleFacade.findElegibleById(
        idElegible);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedElegible = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultElegible();
  }

  private void resetResultElegible() {
    this.resultElegible = null;
    this.selectedElegible = false;
    this.elegible = null;

    this.showResultElegible = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    if ((!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("T")) && (!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("U")) && (!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("S"))) {
      this.elegibleEducacion.setCarrera(null);
    }
    if ((!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("E")) && (!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("M")) && (!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("C")) && (!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("R")) && (!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("G")) && (!this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("L"))) {
      this.elegibleEducacion.setNombrePostgrado(null);
    }
    if ((this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("I")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("B")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("P"))) {
      this.elegibleEducacion.setRegistroTitulo("N");
      this.elegibleEducacion.setFechaRegistro(null);
    }

    if ((this.elegibleEducacion.getFechaRegistro() != null) && 
      (this.elegibleEducacion.getFechaRegistro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Registro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.elegibleEducacion.getTiempoSitp() != null) && 
      (this.elegibleEducacion.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }
    try
    {
      if (this.elegibleEducacion.getAnioInicio() <= 0) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año Inicio tiene un valor no valido", ""));
        error = true;
      }
      if (this.elegibleEducacion.getAnioInicio() > new Date().getYear() + 1900) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año Inicio tiene un valor no valido", ""));
        error = true;
      }
      if ((this.elegibleEducacion.getAnioFin() != 0) && ((this.elegibleEducacion.getAnioInicio() >= this.elegibleEducacion.getAnioFin()) || (this.elegibleEducacion.getAnioFin() > new Date().getYear() + 1900))) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año Fin tiene un valor no valido", ""));
        error = true;
      }
      if ((this.elegibleEducacion.getFechaRegistro() != null) && (this.elegibleEducacion.getFechaRegistro().getYear() + 1900 < this.elegibleEducacion.getAnioFin())) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha Registro tiene un valor no valido", ""));
        error = true;
      }
    }
    catch (Exception localException1) {
    }
    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.elegibleEducacion.setElegible(
          this.elegible);
        this.elegibleFacade.addElegibleEducacion(
          this.elegibleEducacion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.elegibleFacade.updateElegibleEducacion(
          this.elegibleEducacion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }

      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.elegibleFacade.deleteElegibleEducacion(this.elegibleEducacion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedElegible = true;

    this.selectNivelEducativo = null;

    this.selectCarrera = null;

    this.selectTitulo = null;

    this.selectNivelEducativoForTitulo = null;

    this.selectCiudad = null;

    this.selectPaisForCiudad = null;

    this.selectEstadoForCiudad = null;

    this.selectElegible = null;

    this.elegibleEducacion = new ElegibleEducacion();

    this.elegibleEducacion.setElegible(this.elegible);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.elegibleEducacion.setIdElegibleEducacion(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.elegible.ElegibleEducacion"));

    return null;
  }

  public boolean isShowCarreraAux() {
    try {
      return (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("T")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("S")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTituloAux() {
    try {
      return (this.elegibleEducacion.getEstatus().equals("F")) && ((this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("T")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("S")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("U")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("D")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("H"))); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowRegistroTituloAux() {
    try {
      return this.elegibleEducacion.getEstatus().equals("F"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFechaRegistroAux() {
    try {
      return this.elegibleEducacion.getRegistroTitulo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNombrePostgradoAux() {
    try {
      return (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("E")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("M")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("C")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("R")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("G")) || (this.elegibleEducacion.getNivelEducativo().getCodNivelEducativo().equals("L")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowOrganizacionBecariaAux() {
    try {
      return this.elegibleEducacion.getBecado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.elegibleEducacion = new ElegibleEducacion();
    return "cancel";
  }
  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.elegibleEducacion = new ElegibleEducacion();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedElegible));
  }

  public Collection getResultElegible() {
    return this.resultElegible;
  }
  public Elegible getElegible() {
    return this.elegible;
  }
  public boolean isSelectedElegible() {
    return this.selectedElegible;
  }
  public int getFindElegibleCedula() {
    return this.findElegibleCedula;
  }
  public String getFindElegiblePrimerNombre() {
    return this.findElegiblePrimerNombre;
  }
  public String getFindElegibleSegundoNombre() {
    return this.findElegibleSegundoNombre;
  }
  public String getFindElegiblePrimerApellido() {
    return this.findElegiblePrimerApellido;
  }
  public String getFindElegibleSegundoApellido() {
    return this.findElegibleSegundoApellido;
  }
  public void setFindElegibleCedula(int cedula) {
    this.findElegibleCedula = cedula;
  }
  public void setFindElegiblePrimerNombre(String nombre) {
    this.findElegiblePrimerNombre = nombre;
  }
  public void setFindElegibleSegundoNombre(String nombre) {
    this.findElegibleSegundoNombre = nombre;
  }
  public void setFindElegiblePrimerApellido(String nombre) {
    this.findElegiblePrimerApellido = nombre;
  }
  public void setFindElegibleSegundoApellido(String nombre) {
    this.findElegibleSegundoApellido = nombre;
  }
  public boolean isShowResultElegible() {
    return this.showResultElegible;
  }
  public boolean isShowAddResultElegible() {
    return this.showAddResultElegible;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedElegible);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectElegible() {
    return this.findSelectElegible;
  }

  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }
  public LoginSession getLogin() {
    return this.login;
  }
}