package sigefirrhh.planificacion.elegible;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.Municipio;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.Parroquia;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class ElegibleForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ElegibleForm.class.getName());
  private Elegible elegible;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int scrollx;
  private int scrolly;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private ElegibleFacade elegibleFacade = new ElegibleFacade();
  private boolean showElegibleByCedula;
  private boolean showElegibleByPrimerApellido;
  private boolean showElegibleByPrimerNombre;
  private int findCedula;
  private String findPrimerApellido;
  private String findPrimerNombre;
  private int findElegibleCedula;
  private String findElegiblePrimerNombre;
  private String findElegibleSegundoNombre;
  private String findElegiblePrimerApellido;
  private String findElegibleSegundoApellido;
  private Collection colPaisForCiudadNacimiento;
  private Collection colEstadoForCiudadNacimiento;
  private Collection colCiudadNacimiento;
  private Collection colPaisNacionalidad;
  private Collection colPaisForCiudadResidencia;
  private Collection colEstadoForCiudadResidencia;
  private Collection colCiudadResidencia;
  private Collection colPaisForParroquia;
  private Collection colEstadoForParroquia;
  private Collection colMunicipioForParroquia;
  private Collection colParroquia;
  private Collection colEstablecimientoSalud;
  private String selectPaisForCiudadNacimiento;
  private String selectEstadoForCiudadNacimiento;
  private String selectCiudadNacimiento;
  private String selectPaisNacionalidad;
  private String selectPaisForCiudadResidencia;
  private String selectEstadoForCiudadResidencia;
  private String selectCiudadResidencia;
  private String selectPaisForParroquia;
  private String selectEstadoForParroquia;
  private String selectMunicipioForParroquia;
  private String selectParroquia;
  private String selectEstablecimientoSalud;
  private Object stateScrollElegibleByPrimerApellido = null;
  private Object stateResultElegibleByPrimerApellido = null;

  private Object stateScrollElegibleByPrimerNombre = null;
  private Object stateResultElegibleByPrimerNombre = null;

  private Object stateScrollElegible = null;
  private Object stateResultElegible = null;

  public int getFindCedula()
  {
    return this.findCedula;
  }
  public void setFindCedula(int findCedula) {
    this.findCedula = findCedula;
  }
  public String getFindPrimerApellido() {
    return this.findPrimerApellido;
  }
  public void setFindPrimerApellido(String findPrimerApellido) {
    this.findPrimerApellido = findPrimerApellido;
  }
  public String getFindPrimerNombre() {
    return this.findPrimerNombre;
  }
  public void setFindPrimerNombre(String findPrimerNombre) {
    this.findPrimerNombre = findPrimerNombre;
  }

  public String getSelectPaisForCiudadNacimiento()
  {
    return this.selectPaisForCiudadNacimiento;
  }
  public void setSelectPaisForCiudadNacimiento(String valPaisForCiudadNacimiento) {
    this.selectPaisForCiudadNacimiento = valPaisForCiudadNacimiento;
  }
  public void changePaisForCiudadNacimiento(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadNacimiento = null;
      this.colEstadoForCiudadNacimiento = null;
      if (idPais > 0L) {
        this.colEstadoForCiudadNacimiento = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectCiudadNacimiento = null;
        this.elegible.setCiudadNacimiento(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadNacimiento = null;
      this.elegible.setCiudadNacimiento(
        null);
    }
  }

  public boolean isShowPaisForCiudadNacimiento() { return this.colPaisForCiudadNacimiento != null; }

  public String getSelectEstadoForCiudadNacimiento() {
    return this.selectEstadoForCiudadNacimiento;
  }
  public void setSelectEstadoForCiudadNacimiento(String valEstadoForCiudadNacimiento) {
    this.selectEstadoForCiudadNacimiento = valEstadoForCiudadNacimiento;
  }
  public void changeEstadoForCiudadNacimiento(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadNacimiento = null;
      if (idEstado > 0L) {
        this.colCiudadNacimiento = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
      } else {
        this.selectCiudadNacimiento = null;
        this.elegible.setCiudadNacimiento(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadNacimiento = null;
      this.elegible.setCiudadNacimiento(
        null);
    }
  }

  public boolean isShowEstadoForCiudadNacimiento() { return this.colEstadoForCiudadNacimiento != null; }

  public String getSelectCiudadNacimiento() {
    return this.selectCiudadNacimiento;
  }
  public void setSelectCiudadNacimiento(String valCiudadNacimiento) {
    Iterator iterator = this.colCiudadNacimiento.iterator();
    Ciudad ciudadNacimiento = null;
    this.elegible.setCiudadNacimiento(null);
    while (iterator.hasNext()) {
      ciudadNacimiento = (Ciudad)iterator.next();
      if (String.valueOf(ciudadNacimiento.getIdCiudad()).equals(
        valCiudadNacimiento)) {
        this.elegible.setCiudadNacimiento(
          ciudadNacimiento);
      }
    }
    this.selectCiudadNacimiento = valCiudadNacimiento;
  }
  public boolean isShowCiudadNacimiento() {
    return this.colCiudadNacimiento != null;
  }
  public String getSelectPaisNacionalidad() {
    return this.selectPaisNacionalidad;
  }
  public void setSelectPaisNacionalidad(String valPaisNacionalidad) {
    Iterator iterator = this.colPaisNacionalidad.iterator();
    Pais paisNacionalidad = null;
    this.elegible.setPaisNacionalidad(null);
    while (iterator.hasNext()) {
      paisNacionalidad = (Pais)iterator.next();
      if (String.valueOf(paisNacionalidad.getIdPais()).equals(
        valPaisNacionalidad)) {
        this.elegible.setPaisNacionalidad(
          paisNacionalidad);
      }
    }
    this.selectPaisNacionalidad = valPaisNacionalidad;
  }
  public String getSelectPaisForCiudadResidencia() {
    return this.selectPaisForCiudadResidencia;
  }
  public void setSelectPaisForCiudadResidencia(String valPaisForCiudadResidencia) {
    this.selectPaisForCiudadResidencia = valPaisForCiudadResidencia;
  }
  public void changePaisForCiudadResidencia(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadResidencia = null;
      this.colEstadoForCiudadResidencia = null;
      this.colMunicipioForParroquia = null;
      if (idPais > 0L) {
        this.colEstadoForCiudadResidencia = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectCiudadResidencia = null;
        this.selectMunicipioForParroquia = null;
        this.selectParroquia = null;
        this.elegible.setParroquia(null);
        this.elegible.setCiudadResidencia(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadResidencia = null;
      this.selectMunicipioForParroquia = null;
      this.selectParroquia = null;
      this.elegible.setParroquia(null);
      this.elegible.setCiudadResidencia(
        null);
    }
  }

  public boolean isShowPaisForCiudadResidencia() { return this.colPaisForCiudadResidencia != null; }

  public String getSelectEstadoForCiudadResidencia() {
    return this.selectEstadoForCiudadResidencia;
  }
  public void setSelectEstadoForCiudadResidencia(String valEstadoForCiudadResidencia) {
    this.selectEstadoForCiudadResidencia = valEstadoForCiudadResidencia;
  }
  public void changeEstadoForCiudadResidencia(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCiudadResidencia = null;
      this.colMunicipioForParroquia = null;
      if (idEstado > 0L) {
        this.colCiudadResidencia = 
          this.ubicacionFacade.findCiudadByEstado(
          idEstado);
        this.colMunicipioForParroquia = 
          this.ubicacionFacade.findMunicipioByEstado(
          idEstado);
      } else {
        this.selectCiudadResidencia = null;
        this.selectParroquia = null;
        this.elegible.setParroquia(null);
        this.elegible.setCiudadResidencia(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCiudadResidencia = null;
      this.selectParroquia = null;
      this.elegible.setParroquia(null);
      this.elegible.setCiudadResidencia(
        null);
    }
  }

  public boolean isShowEstadoForCiudadResidencia() { return this.colEstadoForCiudadResidencia != null; }

  public String getSelectCiudadResidencia() {
    return this.selectCiudadResidencia;
  }
  public void setSelectCiudadResidencia(String valCiudadResidencia) {
    Iterator iterator = this.colCiudadResidencia.iterator();
    Ciudad ciudadResidencia = null;
    this.elegible.setCiudadResidencia(null);
    while (iterator.hasNext()) {
      ciudadResidencia = (Ciudad)iterator.next();
      if (String.valueOf(ciudadResidencia.getIdCiudad()).equals(
        valCiudadResidencia)) {
        this.elegible.setCiudadResidencia(
          ciudadResidencia);
      }
    }
    this.selectCiudadResidencia = valCiudadResidencia;
  }
  public boolean isShowCiudadResidencia() {
    return this.colCiudadResidencia != null;
  }
  public String getSelectPaisForParroquia() {
    return this.selectPaisForParroquia;
  }
  public void setSelectPaisForParroquia(String valPaisForParroquia) {
    this.selectPaisForParroquia = valPaisForParroquia;
  }
  public void changePaisForParroquia(ValueChangeEvent event) {
    long idPais = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colParroquia = null;
      this.colMunicipioForParroquia = null;
      this.colEstadoForParroquia = null;
      if (idPais > 0L) {
        this.colEstadoForParroquia = 
          this.ubicacionFacade.findEstadoByPais(
          idPais);
      } else {
        this.selectParroquia = null;
        this.elegible.setParroquia(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectParroquia = null;
      this.elegible.setParroquia(
        null);
    }
  }

  public boolean isShowPaisForParroquia() { return this.colPaisForParroquia != null; }

  public String getSelectEstadoForParroquia() {
    return this.selectEstadoForParroquia;
  }
  public void setSelectEstadoForParroquia(String valEstadoForParroquia) {
    this.selectEstadoForParroquia = valEstadoForParroquia;
  }
  public void changeEstadoForParroquia(ValueChangeEvent event) {
    long idEstado = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colParroquia = null;
      this.colMunicipioForParroquia = null;
      if (idEstado > 0L) {
        this.colMunicipioForParroquia = 
          this.ubicacionFacade.findMunicipioByEstado(
          idEstado);
      } else {
        this.selectParroquia = null;
        this.elegible.setParroquia(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectParroquia = null;
      this.elegible.setParroquia(
        null);
    }
  }

  public boolean isShowEstadoForParroquia() { return this.colEstadoForParroquia != null; }

  public String getSelectMunicipioForParroquia() {
    return this.selectMunicipioForParroquia;
  }
  public void setSelectMunicipioForParroquia(String valMunicipioForParroquia) {
    this.selectMunicipioForParroquia = valMunicipioForParroquia;
  }
  public void changeMunicipioForParroquia(ValueChangeEvent event) {
    long idMunicipio = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colParroquia = null;
      if (idMunicipio > 0L) {
        this.colParroquia = 
          this.ubicacionFacade.findParroquiaByMunicipio(
          idMunicipio);
      } else {
        this.selectParroquia = null;
        this.elegible.setParroquia(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectParroquia = null;
      this.elegible.setParroquia(
        null);
    }
  }

  public boolean isShowMunicipioForParroquia() { return this.colMunicipioForParroquia != null; }

  public String getSelectParroquia() {
    return this.selectParroquia;
  }
  public void setSelectParroquia(String valParroquia) {
    Iterator iterator = this.colParroquia.iterator();
    Parroquia parroquia = null;
    this.elegible.setParroquia(null);
    while (iterator.hasNext()) {
      parroquia = (Parroquia)iterator.next();
      if (String.valueOf(parroquia.getIdParroquia()).equals(
        valParroquia)) {
        this.elegible.setParroquia(
          parroquia);
      }
    }
    this.selectParroquia = valParroquia;
  }
  public boolean isShowParroquia() {
    return this.colParroquia != null;
  }

  public Collection getResult() {
    return this.result;
  }

  public Elegible getElegible() {
    if (this.elegible == null) {
      this.elegible = new Elegible();
    }
    return this.elegible;
  }

  public ElegibleForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListSexo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SEXO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudadNacimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudadNacimiento.iterator();
    Pais paisForCiudadNacimiento = null;
    while (iterator.hasNext()) {
      paisForCiudadNacimiento = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudadNacimiento.getIdPais()), 
        paisForCiudadNacimiento.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudadNacimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudadNacimiento.iterator();
    Estado estadoForCiudadNacimiento = null;
    while (iterator.hasNext()) {
      estadoForCiudadNacimiento = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudadNacimiento.getIdEstado()), 
        estadoForCiudadNacimiento.toString()));
    }
    return col;
  }

  public Collection getColCiudadNacimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudadNacimiento.iterator();
    Ciudad ciudadNacimiento = null;
    while (iterator.hasNext()) {
      ciudadNacimiento = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudadNacimiento.getIdCiudad()), 
        ciudadNacimiento.toString()));
    }
    return col;
  }

  public Collection getListEstadoCivil() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_ESTADO_CIVIL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListNivelEducativo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_NIVEL_EDUCATIVO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListNacionalidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_NACIONALIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDobleNacionalidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListTrabajandoActualmente() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPaisNacionalidad() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisNacionalidad.iterator();
    Pais paisNacionalidad = null;
    while (iterator.hasNext()) {
      paisNacionalidad = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisNacionalidad.getIdPais()), 
        paisNacionalidad.toString()));
    }
    return col;
  }

  public Collection getListNacionalizado() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPaisForCiudadResidencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForCiudadResidencia.iterator();
    Pais paisForCiudadResidencia = null;
    while (iterator.hasNext()) {
      paisForCiudadResidencia = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForCiudadResidencia.getIdPais()), 
        paisForCiudadResidencia.toString()));
    }
    return col;
  }

  public Collection getColEstadoForCiudadResidencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForCiudadResidencia.iterator();
    Estado estadoForCiudadResidencia = null;
    while (iterator.hasNext()) {
      estadoForCiudadResidencia = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForCiudadResidencia.getIdEstado()), 
        estadoForCiudadResidencia.toString()));
    }
    return col;
  }

  public Collection getColCiudadResidencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCiudadResidencia.iterator();
    Ciudad ciudadResidencia = null;
    while (iterator.hasNext()) {
      ciudadResidencia = (Ciudad)iterator.next();
      col.add(new SelectItem(
        String.valueOf(ciudadResidencia.getIdCiudad()), 
        ciudadResidencia.toString()));
    }
    return col;
  }

  public Collection getColPaisForParroquia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPaisForParroquia.iterator();
    Pais paisForParroquia = null;
    while (iterator.hasNext()) {
      paisForParroquia = (Pais)iterator.next();
      col.add(new SelectItem(
        String.valueOf(paisForParroquia.getIdPais()), 
        paisForParroquia.toString()));
    }
    return col;
  }

  public Collection getColEstadoForParroquia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstadoForParroquia.iterator();
    Estado estadoForParroquia = null;
    while (iterator.hasNext()) {
      estadoForParroquia = (Estado)iterator.next();
      col.add(new SelectItem(
        String.valueOf(estadoForParroquia.getIdEstado()), 
        estadoForParroquia.toString()));
    }
    return col;
  }

  public Collection getColMunicipioForParroquia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMunicipioForParroquia.iterator();
    Municipio municipioForParroquia = null;
    while (iterator.hasNext()) {
      municipioForParroquia = (Municipio)iterator.next();
      col.add(new SelectItem(
        String.valueOf(municipioForParroquia.getIdMunicipio()), 
        municipioForParroquia.toString()));
    }
    return col;
  }

  public Collection getColParroquia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colParroquia.iterator();
    Parroquia parroquia = null;
    while (iterator.hasNext()) {
      parroquia = (Parroquia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(parroquia.getIdParroquia()), 
        parroquia.toString()));
    }
    return col;
  }

  public Collection getListSectorTrabajoConyugue()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SECTOR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMismoOrganismoConyugue() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTieneHijos() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoVivienda() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_TIPO_VIVIENDA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTenenciaVivienda() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_TENENCIA_VIVIENDA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListManeja() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTieneVehiculo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDiestralidad()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_DIESTRALIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListGrupoSanguineo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Elegible.LISTA_GRUPO_SANGUINEO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColEstablecimientoSalud()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstablecimientoSalud.iterator();
    EstablecimientoSalud establecimientoSalud = null;
    while (iterator.hasNext()) {
      establecimientoSalud = (EstablecimientoSalud)iterator.next();
      col.add(new SelectItem(
        String.valueOf(establecimientoSalud.getIdEstablecimientoSalud()), 
        establecimientoSalud.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colPaisForCiudadNacimiento = 
        this.ubicacionFacade.findAllPais();
      log.error("PASO  No11");
      this.colPaisNacionalidad = 
        this.ubicacionFacade.findAllPais();
      log.error("PASO  No12");
      this.colPaisForCiudadResidencia = 
        this.ubicacionFacade.findAllPais();
      log.error("PASO  No13");
      this.colPaisForParroquia = 
        this.ubicacionFacade.findAllPais();
      log.error("PASO  No14");
      this.colEstablecimientoSalud = 
        this.bienestarFacade.findAllEstablecimientoSalud();
      log.error("PASO  No15");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findElegibleByPrimerApellido()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();

      this.showElegibleByPrimerApellido = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showElegibleByPrimerApellido)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCedula = 0;
    this.findPrimerApellido = null;
    this.findPrimerNombre = null;

    return null;
  }

  public String findElegibleByPrimerNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();

      this.showElegibleByPrimerNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showElegibleByPrimerNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCedula = 0;
    this.findPrimerApellido = null;
    this.findPrimerNombre = null;

    return null;
  }

  public boolean isShowElegibleByCedula() {
    return this.showElegibleByCedula;
  }
  public boolean isShowElegibleByPrimerApellido() {
    return this.showElegibleByPrimerApellido;
  }
  public boolean isShowElegibleByPrimerNombre() {
    return this.showElegibleByPrimerNombre;
  }

  public String selectElegible()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCiudadNacimiento = null;
    this.selectPaisForCiudadNacimiento = null;

    this.selectEstadoForCiudadNacimiento = null;

    this.selectPaisNacionalidad = null;
    this.selectCiudadResidencia = null;
    this.selectPaisForCiudadResidencia = null;

    this.selectEstadoForCiudadResidencia = null;

    this.selectParroquia = null;
    this.selectPaisForParroquia = null;

    this.selectEstadoForParroquia = null;

    this.selectMunicipioForParroquia = null;

    long idElegible = 
      Long.parseLong((String)requestParameterMap.get("idElegible"));
    try
    {
      this.elegible = 
        this.elegibleFacade.findElegibleById(
        idElegible);
      if (this.elegible.getCiudadNacimiento() != null) {
        this.selectCiudadNacimiento = 
          String.valueOf(this.elegible.getCiudadNacimiento().getIdCiudad());
      }
      if (this.elegible.getPaisNacionalidad() != null) {
        this.selectPaisNacionalidad = 
          String.valueOf(this.elegible.getPaisNacionalidad().getIdPais());
      }
      if (this.elegible.getCiudadResidencia() != null) {
        this.selectCiudadResidencia = 
          String.valueOf(this.elegible.getCiudadResidencia().getIdCiudad());
      }
      if (this.elegible.getParroquia() != null) {
        this.selectParroquia = 
          String.valueOf(this.elegible.getParroquia().getIdParroquia());
      }

      Ciudad ciudadNacimiento = null;
      Estado estadoForCiudadNacimiento = null;
      Pais paisForCiudadNacimiento = null;
      Ciudad ciudadResidencia = null;
      Estado estadoForCiudadResidencia = null;
      Pais paisForCiudadResidencia = null;
      Parroquia parroquia = null;
      Municipio municipioForParroquia = null;
      Estado estadoForParroquia = null;
      Pais paisForParroquia = null;

      if (this.elegible.getCiudadNacimiento() != null) {
        long idCiudadNacimiento = 
          this.elegible.getCiudadNacimiento().getIdCiudad();
        this.selectCiudadNacimiento = String.valueOf(idCiudadNacimiento);
        ciudadNacimiento = this.ubicacionFacade.findCiudadById(
          idCiudadNacimiento);
        this.colCiudadNacimiento = this.ubicacionFacade.findCiudadByEstado(
          ciudadNacimiento.getEstado().getIdEstado());

        long idEstadoForCiudadNacimiento = 
          this.elegible.getCiudadNacimiento().getEstado().getIdEstado();
        this.selectEstadoForCiudadNacimiento = String.valueOf(idEstadoForCiudadNacimiento);
        estadoForCiudadNacimiento = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudadNacimiento);
        this.colEstadoForCiudadNacimiento = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudadNacimiento.getPais().getIdPais());

        long idPaisForCiudadNacimiento = 
          estadoForCiudadNacimiento.getPais().getIdPais();
        this.selectPaisForCiudadNacimiento = String.valueOf(idPaisForCiudadNacimiento);
        paisForCiudadNacimiento = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudadNacimiento);
        this.colPaisForCiudadNacimiento = 
          this.ubicacionFacade.findAllPais();
      }
      if (this.elegible.getCiudadResidencia() != null) {
        long idCiudadResidencia = 
          this.elegible.getCiudadResidencia().getIdCiudad();
        this.selectCiudadResidencia = String.valueOf(idCiudadResidencia);
        ciudadResidencia = this.ubicacionFacade.findCiudadById(
          idCiudadResidencia);
        this.colCiudadResidencia = this.ubicacionFacade.findCiudadByEstado(
          ciudadResidencia.getEstado().getIdEstado());

        long idEstadoForCiudadResidencia = 
          this.elegible.getCiudadResidencia().getEstado().getIdEstado();
        this.selectEstadoForCiudadResidencia = String.valueOf(idEstadoForCiudadResidencia);
        estadoForCiudadResidencia = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForCiudadResidencia);
        this.colEstadoForCiudadResidencia = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForCiudadResidencia.getPais().getIdPais());

        long idPaisForCiudadResidencia = 
          estadoForCiudadResidencia.getPais().getIdPais();
        this.selectPaisForCiudadResidencia = String.valueOf(idPaisForCiudadResidencia);
        paisForCiudadResidencia = 
          this.ubicacionFacade.findPaisById(
          idPaisForCiudadResidencia);
        this.colPaisForCiudadResidencia = 
          this.ubicacionFacade.findAllPais();
      }
      if (this.elegible.getParroquia() != null) {
        long idParroquia = 
          this.elegible.getParroquia().getIdParroquia();
        this.selectParroquia = String.valueOf(idParroquia);
        parroquia = this.ubicacionFacade.findParroquiaById(
          idParroquia);
        this.colParroquia = this.ubicacionFacade.findParroquiaByMunicipio(
          parroquia.getMunicipio().getIdMunicipio());

        long idMunicipioForParroquia = 
          this.elegible.getParroquia().getMunicipio().getIdMunicipio();
        this.selectMunicipioForParroquia = String.valueOf(idMunicipioForParroquia);
        municipioForParroquia = 
          this.ubicacionFacade.findMunicipioById(
          idMunicipioForParroquia);
        this.colMunicipioForParroquia = 
          this.ubicacionFacade.findMunicipioByEstado(
          municipioForParroquia.getEstado().getIdEstado());

        long idEstadoForParroquia = 
          municipioForParroquia.getEstado().getIdEstado();
        this.selectEstadoForParroquia = String.valueOf(idEstadoForParroquia);
        estadoForParroquia = 
          this.ubicacionFacade.findEstadoById(
          idEstadoForParroquia);
        this.colEstadoForParroquia = 
          this.ubicacionFacade.findEstadoByPais(
          estadoForParroquia.getPais().getIdPais());

        long idPaisForParroquia = 
          estadoForParroquia.getPais().getIdPais();
        this.selectPaisForParroquia = String.valueOf(idPaisForParroquia);
        paisForParroquia = 
          this.ubicacionFacade.findPaisById(
          idPaisForParroquia);
        this.colPaisForParroquia = 
          this.ubicacionFacade.findAllPais();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.elegible = null;
    this.showElegibleByCedula = false;
    this.showElegibleByPrimerApellido = false;
    this.showElegibleByPrimerNombre = false;
  }

  public String edit()
  {
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    if ((!this.elegible.getEstadoCivil().equals("C")) && (!this.elegible.getEstadoCivil().equals("U"))) {
      this.elegible.setCedulaConyugue(0);
      this.elegible.setNombreConyugue(null);
      this.elegible.setSectorTrabajoConyugue("N");
      this.elegible.setMismoOrganismoConyugue("N");
    }
    if (this.elegible.getNacionalizado().equals("N")) {
      this.elegible.setFechaNacionalizacion(null);
      this.elegible.setGacetaNacionalizacion(null);
      this.elegible.setOtraNormativaNac(null);
    }
    if (this.elegible.getDobleNacionalidad().equals("N")) {
      this.elegible.setPaisNacionalidad(null);
    }
    if (this.elegible.getTieneVehiculo().equals("N")) {
      this.elegible.setMarcaVehiculo(null);
      this.elegible.setPlacaVehiculo(null);
    }

    if ((this.elegible.getFechaNacimiento() != null) && 
      (this.elegible.getFechaNacimiento().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Nacimiento no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.elegible.getFechaNacionalizacion() != null) && 
      (this.elegible.getFechaNacionalizacion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Nacionalización no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.elegible.setFechaRegistro(new Date());
        this.elegibleFacade.addElegible(
          this.elegible, this.login.getOrganismo().getIdOrganismo());
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.elegibleFacade.updateElegible(
          this.elegible);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.elegibleFacade.deleteElegible(
        this.elegible, this.login.getOrganismo().getIdOrganismo());
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.scrollx = 0;
    this.scrolly = 0;
    this.elegible = new Elegible();

    this.selectCiudadNacimiento = null;

    this.selectPaisForCiudadNacimiento = null;

    this.selectEstadoForCiudadNacimiento = null;

    this.selectPaisNacionalidad = null;

    this.selectCiudadResidencia = null;

    this.selectPaisForCiudadResidencia = null;

    this.selectEstadoForCiudadResidencia = null;

    this.selectParroquia = null;

    this.selectPaisForParroquia = null;

    this.selectEstadoForParroquia = null;

    this.selectMunicipioForParroquia = null;

    this.selectEstablecimientoSalud = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.elegible.setIdElegible(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.elegible.Elegible"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.scrollx = 0;
    this.scrolly = 0;
    resetResult();
    this.elegible = new Elegible();
    return "cancel";
  }

  public String abortUpdate() {
    this.showElegibleByCedula = true;
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.elegible = new Elegible();
    return "cancel";
  }

  public boolean isShowPaisNacionalidadAux() {
    try {
      return this.elegible.getDobleNacionalidad().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowFechaNacionalizacionAux() {
    try {
      return this.elegible.getNacionalizado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowGacetaNacionalizacionAux() {
    try {
      return this.elegible.getNacionalizado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowOtraNormativaNacAux() {
    try {
      return this.elegible.getNacionalizado().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowCedulaConyugueAux() {
    try {
      return (this.elegible.getEstadoCivil().equals("C")) || (this.elegible.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNombreConyugueAux() {
    try {
      return (this.elegible.getEstadoCivil().equals("C")) || (this.elegible.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowSectorTrabajoConyugueAux() {
    try {
      return (this.elegible.getEstadoCivil().equals("C")) || (this.elegible.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowMismoOrganismoConyugueAux() {
    try {
      return (this.elegible.getEstadoCivil().equals("C")) || (this.elegible.getEstadoCivil().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowMarcaVehiculoAux() {
    try {
      return this.elegible.getTieneVehiculo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowModeloVehiculoAux() {
    try {
      return this.elegible.getTieneVehiculo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowPlacaVehiculoAux() {
    try {
      return this.elegible.getTieneVehiculo().equals("S"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }
  public int getScrollx() {
    return this.scrollx;
  }
  public int getScrolly() {
    return this.scrolly;
  }
  public void setScrollx(int scrollx) {
    this.scrollx = scrollx;
  }
  public void setScrolly(int scrolly) {
    this.scrolly = scrolly;
  }

  public String findElegibleByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      resetResult();

      this.result = null;
      this.showElegibleByCedula = false;

      this.result = 
        this.elegibleFacade.findElegibleByCedula(this.findElegibleCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showElegibleByCedula = 
        ((this.result != null) && (!this.result.isEmpty()));

      if (!this.showElegibleByCedula)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findElegibleCedula = 0;
    this.findElegiblePrimerNombre = null;
    this.findElegibleSegundoNombre = null;
    this.findElegiblePrimerApellido = null;
    this.findElegibleSegundoApellido = null;
    return null;
  }

  public String findElegibleByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      this.result = null;
      this.showElegibleByCedula = false;

      if (((this.findElegiblePrimerNombre == null) || (this.findElegiblePrimerNombre.equals(""))) && 
        ((this.findElegibleSegundoNombre == null) || (this.findElegibleSegundoNombre.equals(""))) && 
        ((this.findElegiblePrimerApellido == null) || (this.findElegiblePrimerApellido.equals(""))) && (
        (this.findElegibleSegundoApellido == null) || (this.findElegibleSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.result = 
          this.elegibleFacade.findElegibleByNombresApellidos(
          this.findElegiblePrimerNombre, 
          this.findElegibleSegundoNombre, 
          this.findElegiblePrimerApellido, 
          this.findElegibleSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showElegibleByCedula = 
          ((this.result != null) && (!this.result.isEmpty()));
        if (!this.showElegibleByCedula)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findElegibleCedula = 0;
    this.findElegiblePrimerNombre = null;
    this.findElegibleSegundoNombre = null;
    this.findElegiblePrimerApellido = null;
    this.findElegibleSegundoApellido = null;

    return null;
  }
  public int getFindElegibleCedula() {
    return this.findElegibleCedula;
  }

  public String getFindElegiblePrimerApellido() {
    return this.findElegiblePrimerApellido;
  }

  public String getFindElegiblePrimerNombre() {
    return this.findElegiblePrimerNombre;
  }

  public String getFindElegibleSegundoApellido() {
    return this.findElegibleSegundoApellido;
  }

  public String getFindElegibleSegundoNombre() {
    return this.findElegibleSegundoNombre;
  }

  public void setFindElegibleCedula(int i) {
    this.findElegibleCedula = i;
  }

  public void setFindElegiblePrimerApellido(String string) {
    this.findElegiblePrimerApellido = string;
  }

  public void setFindElegiblePrimerNombre(String string) {
    this.findElegiblePrimerNombre = string;
  }

  public void setFindElegibleSegundoApellido(String string) {
    this.findElegibleSegundoApellido = string;
  }

  public void setFindElegibleSegundoNombre(String string) {
    this.findElegibleSegundoNombre = string;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}