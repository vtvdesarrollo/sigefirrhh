package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleEstudioPK
  implements Serializable
{
  public long idElegibleEstudio;

  public ElegibleEstudioPK()
  {
  }

  public ElegibleEstudioPK(long idElegibleEstudio)
  {
    this.idElegibleEstudio = idElegibleEstudio;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleEstudioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleEstudioPK thatPK)
  {
    return 
      this.idElegibleEstudio == thatPK.idElegibleEstudio;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleEstudio)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleEstudio);
  }
}