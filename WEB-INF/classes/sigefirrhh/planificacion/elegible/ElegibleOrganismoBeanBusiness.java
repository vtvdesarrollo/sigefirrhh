package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class ElegibleOrganismoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleOrganismo(ElegibleOrganismo elegibleOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleOrganismo elegibleOrganismoNew = 
      (ElegibleOrganismo)BeanUtils.cloneBean(
      elegibleOrganismo);

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleOrganismoNew.getElegible() != null) {
      elegibleOrganismoNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleOrganismoNew.getElegible().getIdElegible()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (elegibleOrganismoNew.getOrganismo() != null) {
      elegibleOrganismoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        elegibleOrganismoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(elegibleOrganismoNew);
  }

  public void updateElegibleOrganismo(ElegibleOrganismo elegibleOrganismo) throws Exception
  {
    ElegibleOrganismo elegibleOrganismoModify = 
      findElegibleOrganismoById(elegibleOrganismo.getIdElegibleOrganismo());

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleOrganismo.getElegible() != null) {
      elegibleOrganismo.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleOrganismo.getElegible().getIdElegible()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (elegibleOrganismo.getOrganismo() != null) {
      elegibleOrganismo.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        elegibleOrganismo.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(elegibleOrganismoModify, elegibleOrganismo);
  }

  public void deleteElegibleOrganismo(ElegibleOrganismo elegibleOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleOrganismo elegibleOrganismoDelete = 
      findElegibleOrganismoById(elegibleOrganismo.getIdElegibleOrganismo());
    pm.deletePersistent(elegibleOrganismoDelete);
  }

  public ElegibleOrganismo findElegibleOrganismoById(long idElegibleOrganismo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleOrganismo == pIdElegibleOrganismo";
    Query query = pm.newQuery(ElegibleOrganismo.class, filter);

    query.declareParameters("long pIdElegibleOrganismo");

    parameters.put("pIdElegibleOrganismo", new Long(idElegibleOrganismo));

    Collection colElegibleOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleOrganismo.iterator();
    return (ElegibleOrganismo)iterator.next();
  }

  public Collection findElegibleOrganismoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleOrganismoExtent = pm.getExtent(
      ElegibleOrganismo.class, true);
    Query query = pm.newQuery(elegibleOrganismoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}