package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleFamiliarPK
  implements Serializable
{
  public long idElegibleFamiliar;

  public ElegibleFamiliarPK()
  {
  }

  public ElegibleFamiliarPK(long idElegibleFamiliar)
  {
    this.idElegibleFamiliar = idElegibleFamiliar;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleFamiliarPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleFamiliarPK thatPK)
  {
    return 
      this.idElegibleFamiliar == thatPK.idElegibleFamiliar;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleFamiliar)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleFamiliar);
  }
}