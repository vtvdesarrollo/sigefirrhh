package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoHabilidad;

public class ElegibleHabilidad
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_NIVEL;
  private long idElegibleHabilidad;
  private TipoHabilidad tipoHabilidad;
  private String nivel;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "elegible", "idElegibleHabilidad", "idSitp", "nivel", "tiempoSitp", "tipoHabilidad" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.TipoHabilidad") }; private static final byte[] jdoFieldFlags = { 26, 24, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleHabilidad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleHabilidad());

    LISTA_NIVEL = 
      new LinkedHashMap();
    LISTA_NIVEL.put("A", "ALTO");
    LISTA_NIVEL.put("M", "MEDIO");
    LISTA_NIVEL.put("B", "BAJO");
  }

  public ElegibleHabilidad()
  {
    jdoSetnivel(this, "A");
  }

  public String toString()
  {
    return jdoGettipoHabilidad(this).getDescripcion();
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public long getIdElegibleHabilidad()
  {
    return jdoGetidElegibleHabilidad(this);
  }

  public void setIdElegibleHabilidad(long idElegibleHabilidad)
  {
    jdoSetidElegibleHabilidad(this, idElegibleHabilidad);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNivel()
  {
    return jdoGetnivel(this);
  }

  public void setNivel(String nivel)
  {
    jdoSetnivel(this, nivel);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public TipoHabilidad getTipoHabilidad()
  {
    return jdoGettipoHabilidad(this);
  }

  public void setTipoHabilidad(TipoHabilidad tipoHabilidad)
  {
    jdoSettipoHabilidad(this, tipoHabilidad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleHabilidad localElegibleHabilidad = new ElegibleHabilidad();
    localElegibleHabilidad.jdoFlags = 1;
    localElegibleHabilidad.jdoStateManager = paramStateManager;
    return localElegibleHabilidad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleHabilidad localElegibleHabilidad = new ElegibleHabilidad();
    localElegibleHabilidad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleHabilidad.jdoFlags = 1;
    localElegibleHabilidad.jdoStateManager = paramStateManager;
    return localElegibleHabilidad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleHabilidad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivel);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoHabilidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleHabilidad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoHabilidad = ((TipoHabilidad)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleHabilidad paramElegibleHabilidad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleHabilidad.elegible;
      return;
    case 1:
      if (paramElegibleHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleHabilidad = paramElegibleHabilidad.idElegibleHabilidad;
      return;
    case 2:
      if (paramElegibleHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleHabilidad.idSitp;
      return;
    case 3:
      if (paramElegibleHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramElegibleHabilidad.nivel;
      return;
    case 4:
      if (paramElegibleHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleHabilidad.tiempoSitp;
      return;
    case 5:
      if (paramElegibleHabilidad == null)
        throw new IllegalArgumentException("arg1");
      this.tipoHabilidad = paramElegibleHabilidad.tipoHabilidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleHabilidad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleHabilidad localElegibleHabilidad = (ElegibleHabilidad)paramObject;
    if (localElegibleHabilidad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleHabilidad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleHabilidadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleHabilidadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleHabilidadPK))
      throw new IllegalArgumentException("arg1");
    ElegibleHabilidadPK localElegibleHabilidadPK = (ElegibleHabilidadPK)paramObject;
    localElegibleHabilidadPK.idElegibleHabilidad = this.idElegibleHabilidad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleHabilidadPK))
      throw new IllegalArgumentException("arg1");
    ElegibleHabilidadPK localElegibleHabilidadPK = (ElegibleHabilidadPK)paramObject;
    this.idElegibleHabilidad = localElegibleHabilidadPK.idElegibleHabilidad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleHabilidadPK))
      throw new IllegalArgumentException("arg2");
    ElegibleHabilidadPK localElegibleHabilidadPK = (ElegibleHabilidadPK)paramObject;
    localElegibleHabilidadPK.idElegibleHabilidad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleHabilidadPK))
      throw new IllegalArgumentException("arg2");
    ElegibleHabilidadPK localElegibleHabilidadPK = (ElegibleHabilidadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localElegibleHabilidadPK.idElegibleHabilidad);
  }

  private static final Elegible jdoGetelegible(ElegibleHabilidad paramElegibleHabilidad)
  {
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleHabilidad.elegible;
    if (localStateManager.isLoaded(paramElegibleHabilidad, jdoInheritedFieldCount + 0))
      return paramElegibleHabilidad.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleHabilidad, jdoInheritedFieldCount + 0, paramElegibleHabilidad.elegible);
  }

  private static final void jdoSetelegible(ElegibleHabilidad paramElegibleHabilidad, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleHabilidad.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleHabilidad, jdoInheritedFieldCount + 0, paramElegibleHabilidad.elegible, paramElegible);
  }

  private static final long jdoGetidElegibleHabilidad(ElegibleHabilidad paramElegibleHabilidad)
  {
    return paramElegibleHabilidad.idElegibleHabilidad;
  }

  private static final void jdoSetidElegibleHabilidad(ElegibleHabilidad paramElegibleHabilidad, long paramLong)
  {
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleHabilidad.idElegibleHabilidad = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleHabilidad, jdoInheritedFieldCount + 1, paramElegibleHabilidad.idElegibleHabilidad, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleHabilidad paramElegibleHabilidad)
  {
    if (paramElegibleHabilidad.jdoFlags <= 0)
      return paramElegibleHabilidad.idSitp;
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleHabilidad.idSitp;
    if (localStateManager.isLoaded(paramElegibleHabilidad, jdoInheritedFieldCount + 2))
      return paramElegibleHabilidad.idSitp;
    return localStateManager.getIntField(paramElegibleHabilidad, jdoInheritedFieldCount + 2, paramElegibleHabilidad.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleHabilidad paramElegibleHabilidad, int paramInt)
  {
    if (paramElegibleHabilidad.jdoFlags == 0)
    {
      paramElegibleHabilidad.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleHabilidad.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleHabilidad, jdoInheritedFieldCount + 2, paramElegibleHabilidad.idSitp, paramInt);
  }

  private static final String jdoGetnivel(ElegibleHabilidad paramElegibleHabilidad)
  {
    if (paramElegibleHabilidad.jdoFlags <= 0)
      return paramElegibleHabilidad.nivel;
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleHabilidad.nivel;
    if (localStateManager.isLoaded(paramElegibleHabilidad, jdoInheritedFieldCount + 3))
      return paramElegibleHabilidad.nivel;
    return localStateManager.getStringField(paramElegibleHabilidad, jdoInheritedFieldCount + 3, paramElegibleHabilidad.nivel);
  }

  private static final void jdoSetnivel(ElegibleHabilidad paramElegibleHabilidad, String paramString)
  {
    if (paramElegibleHabilidad.jdoFlags == 0)
    {
      paramElegibleHabilidad.nivel = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleHabilidad.nivel = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleHabilidad, jdoInheritedFieldCount + 3, paramElegibleHabilidad.nivel, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleHabilidad paramElegibleHabilidad)
  {
    if (paramElegibleHabilidad.jdoFlags <= 0)
      return paramElegibleHabilidad.tiempoSitp;
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleHabilidad.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleHabilidad, jdoInheritedFieldCount + 4))
      return paramElegibleHabilidad.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleHabilidad, jdoInheritedFieldCount + 4, paramElegibleHabilidad.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleHabilidad paramElegibleHabilidad, Date paramDate)
  {
    if (paramElegibleHabilidad.jdoFlags == 0)
    {
      paramElegibleHabilidad.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleHabilidad.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleHabilidad, jdoInheritedFieldCount + 4, paramElegibleHabilidad.tiempoSitp, paramDate);
  }

  private static final TipoHabilidad jdoGettipoHabilidad(ElegibleHabilidad paramElegibleHabilidad)
  {
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleHabilidad.tipoHabilidad;
    if (localStateManager.isLoaded(paramElegibleHabilidad, jdoInheritedFieldCount + 5))
      return paramElegibleHabilidad.tipoHabilidad;
    return (TipoHabilidad)localStateManager.getObjectField(paramElegibleHabilidad, jdoInheritedFieldCount + 5, paramElegibleHabilidad.tipoHabilidad);
  }

  private static final void jdoSettipoHabilidad(ElegibleHabilidad paramElegibleHabilidad, TipoHabilidad paramTipoHabilidad)
  {
    StateManager localStateManager = paramElegibleHabilidad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleHabilidad.tipoHabilidad = paramTipoHabilidad;
      return;
    }
    localStateManager.setObjectField(paramElegibleHabilidad, jdoInheritedFieldCount + 5, paramElegibleHabilidad.tipoHabilidad, paramTipoHabilidad);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}