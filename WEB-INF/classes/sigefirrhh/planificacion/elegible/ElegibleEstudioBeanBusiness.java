package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.AreaConocimientoBeanBusiness;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.adiestramiento.TipoCursoBeanBusiness;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.PaisBeanBusiness;

public class ElegibleEstudioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleEstudio(ElegibleEstudio elegibleEstudio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleEstudio elegibleEstudioNew = 
      (ElegibleEstudio)BeanUtils.cloneBean(
      elegibleEstudio);

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (elegibleEstudioNew.getTipoCurso() != null) {
      elegibleEstudioNew.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        elegibleEstudioNew.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (elegibleEstudioNew.getAreaConocimiento() != null) {
      elegibleEstudioNew.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        elegibleEstudioNew.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PaisBeanBusiness paisBeanBusiness = new PaisBeanBusiness();

    if (elegibleEstudioNew.getPais() != null) {
      elegibleEstudioNew.setPais(
        paisBeanBusiness.findPaisById(
        elegibleEstudioNew.getPais().getIdPais()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleEstudioNew.getElegible() != null) {
      elegibleEstudioNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleEstudioNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleEstudioNew);
  }

  public void updateElegibleEstudio(ElegibleEstudio elegibleEstudio) throws Exception
  {
    ElegibleEstudio elegibleEstudioModify = 
      findElegibleEstudioById(elegibleEstudio.getIdElegibleEstudio());

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (elegibleEstudio.getTipoCurso() != null) {
      elegibleEstudio.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        elegibleEstudio.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (elegibleEstudio.getAreaConocimiento() != null) {
      elegibleEstudio.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        elegibleEstudio.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PaisBeanBusiness paisBeanBusiness = new PaisBeanBusiness();

    if (elegibleEstudio.getPais() != null) {
      elegibleEstudio.setPais(
        paisBeanBusiness.findPaisById(
        elegibleEstudio.getPais().getIdPais()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleEstudio.getElegible() != null) {
      elegibleEstudio.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleEstudio.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleEstudioModify, elegibleEstudio);
  }

  public void deleteElegibleEstudio(ElegibleEstudio elegibleEstudio) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleEstudio elegibleEstudioDelete = 
      findElegibleEstudioById(elegibleEstudio.getIdElegibleEstudio());
    pm.deletePersistent(elegibleEstudioDelete);
  }

  public ElegibleEstudio findElegibleEstudioById(long idElegibleEstudio) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleEstudio == pIdElegibleEstudio";
    Query query = pm.newQuery(ElegibleEstudio.class, filter);

    query.declareParameters("long pIdElegibleEstudio");

    parameters.put("pIdElegibleEstudio", new Long(idElegibleEstudio));

    Collection colElegibleEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleEstudio.iterator();
    return (ElegibleEstudio)iterator.next();
  }

  public Collection findElegibleEstudioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleEstudioExtent = pm.getExtent(
      ElegibleEstudio.class, true);
    Query query = pm.newQuery(elegibleEstudioExtent);
    query.setOrdering("anio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoCurso(long idTipoCurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoCurso.idTipoCurso == pIdTipoCurso";

    Query query = pm.newQuery(ElegibleEstudio.class, filter);

    query.declareParameters("long pIdTipoCurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoCurso", new Long(idTipoCurso));

    query.setOrdering("anio ascending");

    Collection colElegibleEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleEstudio);

    return colElegibleEstudio;
  }

  public Collection findByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "areaConocimiento.idAreaConocimiento == pIdAreaConocimiento";

    Query query = pm.newQuery(ElegibleEstudio.class, filter);

    query.declareParameters("long pIdAreaConocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdAreaConocimiento", new Long(idAreaConocimiento));

    query.setOrdering("anio ascending");

    Collection colElegibleEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleEstudio);

    return colElegibleEstudio;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleEstudio.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("anio ascending");

    Collection colElegibleEstudio = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleEstudio);

    return colElegibleEstudio;
  }
}