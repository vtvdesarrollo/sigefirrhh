package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.sistema.exceptions.ErrorSistema;

public class ElegibleFamiliarBeanBusiness extends AbstractBeanBusiness
{
  public void addElegibleFamiliar(ElegibleFamiliar familiar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleFamiliar familiarNew = 
      (ElegibleFamiliar)BeanUtils.cloneBean(
      familiar);

    if (((familiarNew.getParentesco().equals("P")) || (familiarNew.getParentesco().equals("M")) || (familiarNew.getParentesco().equals("C"))) && (countParentesco(familiarNew.getElegible().getIdElegible(), familiarNew.getParentesco(), 0L) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo elegible");
      throw error;
    }

    if ((familiarNew.getParentesco().equals("S")) && (countParentesco(familiarNew.getElegible().getIdElegible(), familiarNew.getParentesco(), familiarNew.getSexo(), 0L) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo elegible");
      throw error;
    }

    if ((familiarNew.getCedulaFamiliar() != 0) && (searchCedulaFamiliar(familiarNew.getElegible().getIdElegible(), familiarNew.getCedulaFamiliar(), 0L) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este cédula ya esta registrado en otro familiar para el mismo trabajador");
      throw error;
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (familiarNew.getElegible() != null) {
      familiarNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        familiarNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(familiarNew);
  }

  public void updateElegibleFamiliar(ElegibleFamiliar familiar) throws Exception
  {
    ElegibleFamiliar familiarModify = 
      findElegibleFamiliarById(familiar.getIdElegibleFamiliar());

    if (((familiar.getParentesco().equals("P")) || (familiar.getParentesco().equals("M")) || (familiar.getParentesco().equals("C"))) && (countParentesco(familiar.getElegible().getIdElegible(), familiar.getParentesco(), familiarModify.getIdElegibleFamiliar()) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo elegible");
      throw error;
    }

    if (((familiar.getParentesco().equals("A")) || (familiar.getParentesco().equals("S"))) && (countParentesco(familiar.getElegible().getIdElegible(), familiar.getParentesco(), familiar.getSexo(), familiarModify.getIdElegibleFamiliar()) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este parentesco no puede estar registrado más de una vez para el mismo elegible");
      throw error;
    }

    if ((familiar.getCedulaFamiliar() != 0) && (searchCedulaFamiliar(familiar.getElegible().getIdElegible(), familiar.getCedulaFamiliar(), familiarModify.getIdElegibleFamiliar()) != 0)) {
      ErrorSistema error = new ErrorSistema();
      error.setDescription("Este cédula ya esta registrado en otro familiar para el mismo trabajador");
      throw error;
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (familiar.getElegible() != null) {
      familiar.setElegible(
        elegibleBeanBusiness.findElegibleById(
        familiar.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(familiarModify, familiar);
  }

  public void deleteElegibleFamiliar(ElegibleFamiliar familiar) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleFamiliar familiarDelete = 
      findElegibleFamiliarById(familiar.getIdElegibleFamiliar());
    pm.deletePersistent(familiarDelete);
  }

  public ElegibleFamiliar findElegibleFamiliarById(long idElegibleFamiliar) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleFamiliar == pIdElegibleFamiliar";
    Query query = pm.newQuery(ElegibleFamiliar.class, filter);

    query.declareParameters("long pIdElegibleFamiliar");

    parameters.put("pIdElegibleFamiliar", new Long(idElegibleFamiliar));

    Collection colElegibleFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleFamiliar.iterator();
    return (ElegibleFamiliar)iterator.next();
  }

  public Collection findElegibleFamiliarAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent familiarExtent = pm.getExtent(
      ElegibleFamiliar.class, true);
    Query query = pm.newQuery(familiarExtent);
    query.setOrdering("primerApellido ascending, primerNombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleFamiliar.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("primerApellido ascending, primerNombre ascending");

    Collection colElegibleFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleFamiliar);

    return colElegibleFamiliar;
  }

  public int countParentesco(long idElegible, String parentesco, long idElegibleFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible && parentesco == pParentesco && idElegibleFamiliar != pIdElegibleFamiliar";

    Query query = pm.newQuery(ElegibleFamiliar.class, filter);

    query.declareParameters("long pIdElegible, String pParentesco, long pIdElegibleFamiliar");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));
    parameters.put("pParentesco", new String(parentesco));
    parameters.put("pIdElegibleFamiliar", new Long(idElegibleFamiliar));

    Collection colElegibleFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleFamiliar);

    return colElegibleFamiliar.size();
  }

  public int countParentesco(long idElegible, String parentesco, String sexo, long idElegibleFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible && parentesco == pParentesco && sexo == pSexo && idElegibleFamiliar != pIdElegibleFamiliar";

    Query query = pm.newQuery(ElegibleFamiliar.class, filter);

    query.declareParameters("long pIdElegible, String pParentesco, String pSexo, long pIdElegibleFamiliar");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));
    parameters.put("pParentesco", new String(parentesco));
    parameters.put("pSexo", new String(sexo));
    parameters.put("pIdElegibleFamiliar", new Long(idElegibleFamiliar));

    Collection colElegibleFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleFamiliar);

    return colElegibleFamiliar.size();
  }

  public int searchCedulaFamiliar(long idElegible, int cedulaFamiliar, long idElegibleFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible && cedulaFamiliar == pCedulaFamiliar && idElegibleFamiliar != pIdElegibleFamiliar";

    Query query = pm.newQuery(ElegibleFamiliar.class, filter);

    query.declareParameters("long pIdElegible, int pCedulaFamiliar, long pIdElegibleFamiliar");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));
    parameters.put("pCedulaFamiliar", new Integer(cedulaFamiliar));
    parameters.put("pIdElegibleFamiliar", new Long(idElegibleFamiliar));

    Collection colElegibleFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleFamiliar);

    return colElegibleFamiliar.size();
  }
}