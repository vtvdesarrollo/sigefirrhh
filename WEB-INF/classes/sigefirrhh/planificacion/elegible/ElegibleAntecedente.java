package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ElegibleAntecedente
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_VAC_PENDIENTES;
  protected static final Map LISTA_PRE_PENDIENTES;
  private long idElegibleAntecedente;
  private String nombreInstitucion;
  private String personalIngreso;
  private Date fechaIngreso;
  private String cargoIngreso;
  private String descargoIngreso;
  private int codracIngreso;
  private double sueldoIngreso;
  private double compensacionIngreso;
  private double primasIngreso;
  private String personalEgreso;
  private Date fechaEgreso;
  private String cargoEgreso;
  private String descargoEgreso;
  private String causaEgreso;
  private int codracEgreso;
  private double sueldoEgreso;
  private double compensacionEgreso;
  private double primasEgreso;
  private String prestacionesPendientes;
  private String vacacionesPendientes;
  private int diasVacaciones;
  private String observaciones;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cargoEgreso", "cargoIngreso", "causaEgreso", "codracEgreso", "codracIngreso", "compensacionEgreso", "compensacionIngreso", "descargoEgreso", "descargoIngreso", "diasVacaciones", "elegible", "fechaEgreso", "fechaIngreso", "idElegibleAntecedente", "idSitp", "nombreInstitucion", "observaciones", "personalEgreso", "personalIngreso", "prestacionesPendientes", "primasEgreso", "primasIngreso", "sueldoEgreso", "sueldoIngreso", "tiempoSitp", "vacacionesPendientes" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleAntecedente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleAntecedente());

    LISTA_VAC_PENDIENTES = 
      new LinkedHashMap();
    LISTA_PRE_PENDIENTES = 
      new LinkedHashMap();

    LISTA_VAC_PENDIENTES.put("S", "SI");
    LISTA_VAC_PENDIENTES.put("N", "NO");
    LISTA_PRE_PENDIENTES.put("S", "SI");
    LISTA_PRE_PENDIENTES.put("N", "NO");
  }

  public ElegibleAntecedente()
  {
    jdoSetcodracIngreso(this, 0);

    jdoSetsueldoIngreso(this, 0.0D);

    jdoSetcompensacionIngreso(this, 0.0D);

    jdoSetprimasIngreso(this, 0.0D);

    jdoSetcodracEgreso(this, 0);

    jdoSetsueldoEgreso(this, 0.0D);

    jdoSetcompensacionEgreso(this, 0.0D);

    jdoSetprimasEgreso(this, 0.0D);

    jdoSetprestacionesPendientes(this, "N");

    jdoSetvacacionesPendientes(this, "N");

    jdoSetdiasVacaciones(this, 0);
  }

  public String toString()
  {
    return jdoGetnombreInstitucion(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaIngreso(this)) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaEgreso(this));
  }

  public String getCargoEgreso()
  {
    return jdoGetcargoEgreso(this);
  }

  public void setCargoEgreso(String cargoEgreso)
  {
    jdoSetcargoEgreso(this, cargoEgreso);
  }

  public String getCargoIngreso()
  {
    return jdoGetcargoIngreso(this);
  }

  public void setCargoIngreso(String cargoIngreso)
  {
    jdoSetcargoIngreso(this, cargoIngreso);
  }

  public String getCausaEgreso()
  {
    return jdoGetcausaEgreso(this);
  }

  public void setCausaEgreso(String causaEgreso)
  {
    jdoSetcausaEgreso(this, causaEgreso);
  }

  public int getCodracEgreso()
  {
    return jdoGetcodracEgreso(this);
  }

  public void setCodracEgreso(int codracEgreso)
  {
    jdoSetcodracEgreso(this, codracEgreso);
  }

  public int getCodracIngreso()
  {
    return jdoGetcodracIngreso(this);
  }

  public void setCodracIngreso(int codracIngreso)
  {
    jdoSetcodracIngreso(this, codracIngreso);
  }

  public double getCompensacionEgreso()
  {
    return jdoGetcompensacionEgreso(this);
  }

  public void setCompensacionEgreso(double compensacionEgreso)
  {
    jdoSetcompensacionEgreso(this, compensacionEgreso);
  }

  public double getCompensacionIngreso()
  {
    return jdoGetcompensacionIngreso(this);
  }

  public void setCompensacionIngreso(double compensacionIngreso)
  {
    jdoSetcompensacionIngreso(this, compensacionIngreso);
  }

  public String getDescargoEgreso()
  {
    return jdoGetdescargoEgreso(this);
  }

  public void setDescargoEgreso(String descargoEgreso)
  {
    jdoSetdescargoEgreso(this, descargoEgreso);
  }

  public String getDescargoIngreso()
  {
    return jdoGetdescargoIngreso(this);
  }

  public void setDescargoIngreso(String descargoIngreso)
  {
    jdoSetdescargoIngreso(this, descargoIngreso);
  }

  public int getDiasVacaciones()
  {
    return jdoGetdiasVacaciones(this);
  }

  public void setDiasVacaciones(int diasVacaciones)
  {
    jdoSetdiasVacaciones(this, diasVacaciones);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public Date getFechaEgreso()
  {
    return jdoGetfechaEgreso(this);
  }

  public void setFechaEgreso(Date fechaEgreso)
  {
    jdoSetfechaEgreso(this, fechaEgreso);
  }

  public Date getFechaIngreso()
  {
    return jdoGetfechaIngreso(this);
  }

  public void setFechaIngreso(Date fechaIngreso)
  {
    jdoSetfechaIngreso(this, fechaIngreso);
  }

  public long getIdElegibleAntecedente()
  {
    return jdoGetidElegibleAntecedente(this);
  }

  public void setIdElegibleAntecedente(long idElegibleAntecedente)
  {
    jdoSetidElegibleAntecedente(this, idElegibleAntecedente);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNombreInstitucion()
  {
    return jdoGetnombreInstitucion(this);
  }

  public void setNombreInstitucion(String nombreInstitucion)
  {
    jdoSetnombreInstitucion(this, nombreInstitucion);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public String getPersonalEgreso()
  {
    return jdoGetpersonalEgreso(this);
  }

  public void setPersonalEgreso(String personalEgreso)
  {
    jdoSetpersonalEgreso(this, personalEgreso);
  }

  public String getPersonalIngreso()
  {
    return jdoGetpersonalIngreso(this);
  }

  public void setPersonalIngreso(String personalIngreso)
  {
    jdoSetpersonalIngreso(this, personalIngreso);
  }

  public String getPrestacionesPendientes()
  {
    return jdoGetprestacionesPendientes(this);
  }

  public void setPrestacionesPendientes(String prestacionesPendientes)
  {
    jdoSetprestacionesPendientes(this, prestacionesPendientes);
  }

  public double getPrimasEgreso()
  {
    return jdoGetprimasEgreso(this);
  }

  public void setPrimasEgreso(double primasEgreso)
  {
    jdoSetprimasEgreso(this, primasEgreso);
  }

  public double getPrimasIngreso()
  {
    return jdoGetprimasIngreso(this);
  }

  public void setPrimasIngreso(double primasIngreso)
  {
    jdoSetprimasIngreso(this, primasIngreso);
  }

  public double getSueldoEgreso()
  {
    return jdoGetsueldoEgreso(this);
  }

  public void setSueldoEgreso(double sueldoEgreso)
  {
    jdoSetsueldoEgreso(this, sueldoEgreso);
  }

  public double getSueldoIngreso()
  {
    return jdoGetsueldoIngreso(this);
  }

  public void setSueldoIngreso(double sueldoIngreso)
  {
    jdoSetsueldoIngreso(this, sueldoIngreso);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public String getVacacionesPendientes()
  {
    return jdoGetvacacionesPendientes(this);
  }

  public void setVacacionesPendientes(String vacacionesPendientes)
  {
    jdoSetvacacionesPendientes(this, vacacionesPendientes);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 26;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleAntecedente localElegibleAntecedente = new ElegibleAntecedente();
    localElegibleAntecedente.jdoFlags = 1;
    localElegibleAntecedente.jdoStateManager = paramStateManager;
    return localElegibleAntecedente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleAntecedente localElegibleAntecedente = new ElegibleAntecedente();
    localElegibleAntecedente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleAntecedente.jdoFlags = 1;
    localElegibleAntecedente.jdoStateManager = paramStateManager;
    return localElegibleAntecedente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoEgreso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoIngreso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.causaEgreso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codracEgreso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codracIngreso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacionEgreso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.compensacionIngreso);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descargoEgreso);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descargoIngreso);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.diasVacaciones);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEgreso);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleAntecedente);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInstitucion);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personalEgreso);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personalIngreso);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.prestacionesPendientes);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasEgreso);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primasIngreso);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoEgreso);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoIngreso);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vacacionesPendientes);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codracEgreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codracIngreso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacionEgreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.compensacionIngreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descargoEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descargoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diasVacaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEgreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleAntecedente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personalEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personalIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.prestacionesPendientes = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasEgreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasIngreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoEgreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoIngreso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vacacionesPendientes = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleAntecedente paramElegibleAntecedente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.cargoEgreso = paramElegibleAntecedente.cargoEgreso;
      return;
    case 1:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.cargoIngreso = paramElegibleAntecedente.cargoIngreso;
      return;
    case 2:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.causaEgreso = paramElegibleAntecedente.causaEgreso;
      return;
    case 3:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.codracEgreso = paramElegibleAntecedente.codracEgreso;
      return;
    case 4:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.codracIngreso = paramElegibleAntecedente.codracIngreso;
      return;
    case 5:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.compensacionEgreso = paramElegibleAntecedente.compensacionEgreso;
      return;
    case 6:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.compensacionIngreso = paramElegibleAntecedente.compensacionIngreso;
      return;
    case 7:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.descargoEgreso = paramElegibleAntecedente.descargoEgreso;
      return;
    case 8:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.descargoIngreso = paramElegibleAntecedente.descargoIngreso;
      return;
    case 9:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.diasVacaciones = paramElegibleAntecedente.diasVacaciones;
      return;
    case 10:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleAntecedente.elegible;
      return;
    case 11:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEgreso = paramElegibleAntecedente.fechaEgreso;
      return;
    case 12:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramElegibleAntecedente.fechaIngreso;
      return;
    case 13:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleAntecedente = paramElegibleAntecedente.idElegibleAntecedente;
      return;
    case 14:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleAntecedente.idSitp;
      return;
    case 15:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInstitucion = paramElegibleAntecedente.nombreInstitucion;
      return;
    case 16:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramElegibleAntecedente.observaciones;
      return;
    case 17:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.personalEgreso = paramElegibleAntecedente.personalEgreso;
      return;
    case 18:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.personalIngreso = paramElegibleAntecedente.personalIngreso;
      return;
    case 19:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.prestacionesPendientes = paramElegibleAntecedente.prestacionesPendientes;
      return;
    case 20:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.primasEgreso = paramElegibleAntecedente.primasEgreso;
      return;
    case 21:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.primasIngreso = paramElegibleAntecedente.primasIngreso;
      return;
    case 22:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoEgreso = paramElegibleAntecedente.sueldoEgreso;
      return;
    case 23:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoIngreso = paramElegibleAntecedente.sueldoIngreso;
      return;
    case 24:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleAntecedente.tiempoSitp;
      return;
    case 25:
      if (paramElegibleAntecedente == null)
        throw new IllegalArgumentException("arg1");
      this.vacacionesPendientes = paramElegibleAntecedente.vacacionesPendientes;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleAntecedente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleAntecedente localElegibleAntecedente = (ElegibleAntecedente)paramObject;
    if (localElegibleAntecedente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleAntecedente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleAntecedentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleAntecedentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleAntecedentePK))
      throw new IllegalArgumentException("arg1");
    ElegibleAntecedentePK localElegibleAntecedentePK = (ElegibleAntecedentePK)paramObject;
    localElegibleAntecedentePK.idElegibleAntecedente = this.idElegibleAntecedente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleAntecedentePK))
      throw new IllegalArgumentException("arg1");
    ElegibleAntecedentePK localElegibleAntecedentePK = (ElegibleAntecedentePK)paramObject;
    this.idElegibleAntecedente = localElegibleAntecedentePK.idElegibleAntecedente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleAntecedentePK))
      throw new IllegalArgumentException("arg2");
    ElegibleAntecedentePK localElegibleAntecedentePK = (ElegibleAntecedentePK)paramObject;
    localElegibleAntecedentePK.idElegibleAntecedente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 13);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleAntecedentePK))
      throw new IllegalArgumentException("arg2");
    ElegibleAntecedentePK localElegibleAntecedentePK = (ElegibleAntecedentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 13, localElegibleAntecedentePK.idElegibleAntecedente);
  }

  private static final String jdoGetcargoEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.cargoEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.cargoEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 0))
      return paramElegibleAntecedente.cargoEgreso;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 0, paramElegibleAntecedente.cargoEgreso);
  }

  private static final void jdoSetcargoEgreso(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.cargoEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.cargoEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 0, paramElegibleAntecedente.cargoEgreso, paramString);
  }

  private static final String jdoGetcargoIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.cargoIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.cargoIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 1))
      return paramElegibleAntecedente.cargoIngreso;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 1, paramElegibleAntecedente.cargoIngreso);
  }

  private static final void jdoSetcargoIngreso(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.cargoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.cargoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 1, paramElegibleAntecedente.cargoIngreso, paramString);
  }

  private static final String jdoGetcausaEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.causaEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.causaEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 2))
      return paramElegibleAntecedente.causaEgreso;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 2, paramElegibleAntecedente.causaEgreso);
  }

  private static final void jdoSetcausaEgreso(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.causaEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.causaEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 2, paramElegibleAntecedente.causaEgreso, paramString);
  }

  private static final int jdoGetcodracEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.codracEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.codracEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 3))
      return paramElegibleAntecedente.codracEgreso;
    return localStateManager.getIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 3, paramElegibleAntecedente.codracEgreso);
  }

  private static final void jdoSetcodracEgreso(ElegibleAntecedente paramElegibleAntecedente, int paramInt)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.codracEgreso = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.codracEgreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 3, paramElegibleAntecedente.codracEgreso, paramInt);
  }

  private static final int jdoGetcodracIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.codracIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.codracIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 4))
      return paramElegibleAntecedente.codracIngreso;
    return localStateManager.getIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 4, paramElegibleAntecedente.codracIngreso);
  }

  private static final void jdoSetcodracIngreso(ElegibleAntecedente paramElegibleAntecedente, int paramInt)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.codracIngreso = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.codracIngreso = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 4, paramElegibleAntecedente.codracIngreso, paramInt);
  }

  private static final double jdoGetcompensacionEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.compensacionEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.compensacionEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 5))
      return paramElegibleAntecedente.compensacionEgreso;
    return localStateManager.getDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 5, paramElegibleAntecedente.compensacionEgreso);
  }

  private static final void jdoSetcompensacionEgreso(ElegibleAntecedente paramElegibleAntecedente, double paramDouble)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.compensacionEgreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.compensacionEgreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 5, paramElegibleAntecedente.compensacionEgreso, paramDouble);
  }

  private static final double jdoGetcompensacionIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.compensacionIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.compensacionIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 6))
      return paramElegibleAntecedente.compensacionIngreso;
    return localStateManager.getDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 6, paramElegibleAntecedente.compensacionIngreso);
  }

  private static final void jdoSetcompensacionIngreso(ElegibleAntecedente paramElegibleAntecedente, double paramDouble)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.compensacionIngreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.compensacionIngreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 6, paramElegibleAntecedente.compensacionIngreso, paramDouble);
  }

  private static final String jdoGetdescargoEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.descargoEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.descargoEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 7))
      return paramElegibleAntecedente.descargoEgreso;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 7, paramElegibleAntecedente.descargoEgreso);
  }

  private static final void jdoSetdescargoEgreso(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.descargoEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.descargoEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 7, paramElegibleAntecedente.descargoEgreso, paramString);
  }

  private static final String jdoGetdescargoIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.descargoIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.descargoIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 8))
      return paramElegibleAntecedente.descargoIngreso;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 8, paramElegibleAntecedente.descargoIngreso);
  }

  private static final void jdoSetdescargoIngreso(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.descargoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.descargoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 8, paramElegibleAntecedente.descargoIngreso, paramString);
  }

  private static final int jdoGetdiasVacaciones(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.diasVacaciones;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.diasVacaciones;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 9))
      return paramElegibleAntecedente.diasVacaciones;
    return localStateManager.getIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 9, paramElegibleAntecedente.diasVacaciones);
  }

  private static final void jdoSetdiasVacaciones(ElegibleAntecedente paramElegibleAntecedente, int paramInt)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.diasVacaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.diasVacaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 9, paramElegibleAntecedente.diasVacaciones, paramInt);
  }

  private static final Elegible jdoGetelegible(ElegibleAntecedente paramElegibleAntecedente)
  {
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.elegible;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 10))
      return paramElegibleAntecedente.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 10, paramElegibleAntecedente.elegible);
  }

  private static final void jdoSetelegible(ElegibleAntecedente paramElegibleAntecedente, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 10, paramElegibleAntecedente.elegible, paramElegible);
  }

  private static final Date jdoGetfechaEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.fechaEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.fechaEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 11))
      return paramElegibleAntecedente.fechaEgreso;
    return (Date)localStateManager.getObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 11, paramElegibleAntecedente.fechaEgreso);
  }

  private static final void jdoSetfechaEgreso(ElegibleAntecedente paramElegibleAntecedente, Date paramDate)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.fechaEgreso = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.fechaEgreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 11, paramElegibleAntecedente.fechaEgreso, paramDate);
  }

  private static final Date jdoGetfechaIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.fechaIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.fechaIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 12))
      return paramElegibleAntecedente.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 12, paramElegibleAntecedente.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(ElegibleAntecedente paramElegibleAntecedente, Date paramDate)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 12, paramElegibleAntecedente.fechaIngreso, paramDate);
  }

  private static final long jdoGetidElegibleAntecedente(ElegibleAntecedente paramElegibleAntecedente)
  {
    return paramElegibleAntecedente.idElegibleAntecedente;
  }

  private static final void jdoSetidElegibleAntecedente(ElegibleAntecedente paramElegibleAntecedente, long paramLong)
  {
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.idElegibleAntecedente = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleAntecedente, jdoInheritedFieldCount + 13, paramElegibleAntecedente.idElegibleAntecedente, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.idSitp;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.idSitp;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 14))
      return paramElegibleAntecedente.idSitp;
    return localStateManager.getIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 14, paramElegibleAntecedente.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleAntecedente paramElegibleAntecedente, int paramInt)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleAntecedente, jdoInheritedFieldCount + 14, paramElegibleAntecedente.idSitp, paramInt);
  }

  private static final String jdoGetnombreInstitucion(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.nombreInstitucion;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.nombreInstitucion;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 15))
      return paramElegibleAntecedente.nombreInstitucion;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 15, paramElegibleAntecedente.nombreInstitucion);
  }

  private static final void jdoSetnombreInstitucion(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.nombreInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.nombreInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 15, paramElegibleAntecedente.nombreInstitucion, paramString);
  }

  private static final String jdoGetobservaciones(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.observaciones;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.observaciones;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 16))
      return paramElegibleAntecedente.observaciones;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 16, paramElegibleAntecedente.observaciones);
  }

  private static final void jdoSetobservaciones(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 16, paramElegibleAntecedente.observaciones, paramString);
  }

  private static final String jdoGetpersonalEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.personalEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.personalEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 17))
      return paramElegibleAntecedente.personalEgreso;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 17, paramElegibleAntecedente.personalEgreso);
  }

  private static final void jdoSetpersonalEgreso(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.personalEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.personalEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 17, paramElegibleAntecedente.personalEgreso, paramString);
  }

  private static final String jdoGetpersonalIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.personalIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.personalIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 18))
      return paramElegibleAntecedente.personalIngreso;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 18, paramElegibleAntecedente.personalIngreso);
  }

  private static final void jdoSetpersonalIngreso(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.personalIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.personalIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 18, paramElegibleAntecedente.personalIngreso, paramString);
  }

  private static final String jdoGetprestacionesPendientes(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.prestacionesPendientes;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.prestacionesPendientes;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 19))
      return paramElegibleAntecedente.prestacionesPendientes;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 19, paramElegibleAntecedente.prestacionesPendientes);
  }

  private static final void jdoSetprestacionesPendientes(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.prestacionesPendientes = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.prestacionesPendientes = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 19, paramElegibleAntecedente.prestacionesPendientes, paramString);
  }

  private static final double jdoGetprimasEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.primasEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.primasEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 20))
      return paramElegibleAntecedente.primasEgreso;
    return localStateManager.getDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 20, paramElegibleAntecedente.primasEgreso);
  }

  private static final void jdoSetprimasEgreso(ElegibleAntecedente paramElegibleAntecedente, double paramDouble)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.primasEgreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.primasEgreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 20, paramElegibleAntecedente.primasEgreso, paramDouble);
  }

  private static final double jdoGetprimasIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.primasIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.primasIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 21))
      return paramElegibleAntecedente.primasIngreso;
    return localStateManager.getDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 21, paramElegibleAntecedente.primasIngreso);
  }

  private static final void jdoSetprimasIngreso(ElegibleAntecedente paramElegibleAntecedente, double paramDouble)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.primasIngreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.primasIngreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 21, paramElegibleAntecedente.primasIngreso, paramDouble);
  }

  private static final double jdoGetsueldoEgreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.sueldoEgreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.sueldoEgreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 22))
      return paramElegibleAntecedente.sueldoEgreso;
    return localStateManager.getDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 22, paramElegibleAntecedente.sueldoEgreso);
  }

  private static final void jdoSetsueldoEgreso(ElegibleAntecedente paramElegibleAntecedente, double paramDouble)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.sueldoEgreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.sueldoEgreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 22, paramElegibleAntecedente.sueldoEgreso, paramDouble);
  }

  private static final double jdoGetsueldoIngreso(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.sueldoIngreso;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.sueldoIngreso;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 23))
      return paramElegibleAntecedente.sueldoIngreso;
    return localStateManager.getDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 23, paramElegibleAntecedente.sueldoIngreso);
  }

  private static final void jdoSetsueldoIngreso(ElegibleAntecedente paramElegibleAntecedente, double paramDouble)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.sueldoIngreso = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.sueldoIngreso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegibleAntecedente, jdoInheritedFieldCount + 23, paramElegibleAntecedente.sueldoIngreso, paramDouble);
  }

  private static final Date jdoGettiempoSitp(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.tiempoSitp;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 24))
      return paramElegibleAntecedente.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 24, paramElegibleAntecedente.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleAntecedente paramElegibleAntecedente, Date paramDate)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleAntecedente, jdoInheritedFieldCount + 24, paramElegibleAntecedente.tiempoSitp, paramDate);
  }

  private static final String jdoGetvacacionesPendientes(ElegibleAntecedente paramElegibleAntecedente)
  {
    if (paramElegibleAntecedente.jdoFlags <= 0)
      return paramElegibleAntecedente.vacacionesPendientes;
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAntecedente.vacacionesPendientes;
    if (localStateManager.isLoaded(paramElegibleAntecedente, jdoInheritedFieldCount + 25))
      return paramElegibleAntecedente.vacacionesPendientes;
    return localStateManager.getStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 25, paramElegibleAntecedente.vacacionesPendientes);
  }

  private static final void jdoSetvacacionesPendientes(ElegibleAntecedente paramElegibleAntecedente, String paramString)
  {
    if (paramElegibleAntecedente.jdoFlags == 0)
    {
      paramElegibleAntecedente.vacacionesPendientes = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAntecedente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAntecedente.vacacionesPendientes = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAntecedente, jdoInheritedFieldCount + 25, paramElegibleAntecedente.vacacionesPendientes, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}