package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.ubicacion.Pais;

public class ElegibleEstudio
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_UNIDAD_TIEMPO;
  protected static final Map LISTA_ORIGEN_CURSO;
  protected static final Map LISTA_PARTICIPACION;
  protected static final Map LISTA_CERTIFICO;
  protected static final Map LISTA_DIPLOMA;
  protected static final Map LISTA_BECADO;
  protected static final Map LISTA_FINANCIAMIENTO;
  private long idElegibleEstudio;
  private TipoCurso tipoCurso;
  private AreaConocimiento areaConocimiento;
  private String nombreCurso;
  private int anio;
  private String nombreEntidad;
  private int duracion;
  private String unidadTiempo;
  private String origenCurso;
  private String participacion;
  private String certifico;
  private Pais pais;
  private String becado;
  private String financiamiento;
  private int aniosExperiencia;
  private int mesesExperiencia;
  private String observaciones;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "aniosExperiencia", "areaConocimiento", "becado", "certifico", "duracion", "elegible", "financiamiento", "idElegibleEstudio", "idSitp", "mesesExperiencia", "nombreCurso", "nombreEntidad", "observaciones", "origenCurso", "pais", "participacion", "tiempoSitp", "tipoCurso", "unidadTiempo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.adiestramiento.AreaConocimiento"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Pais"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.adiestramiento.TipoCurso"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 26, 21, 21, 21, 26, 21, 24, 21, 21, 21, 21, 21, 21, 26, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleEstudio"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleEstudio());

    LISTA_UNIDAD_TIEMPO = 
      new LinkedHashMap();
    LISTA_ORIGEN_CURSO = 
      new LinkedHashMap();
    LISTA_PARTICIPACION = 
      new LinkedHashMap();
    LISTA_CERTIFICO = 
      new LinkedHashMap();
    LISTA_DIPLOMA = 
      new LinkedHashMap();
    LISTA_BECADO = 
      new LinkedHashMap();
    LISTA_FINANCIAMIENTO = 
      new LinkedHashMap();

    LISTA_UNIDAD_TIEMPO.put("H", "HORAS");
    LISTA_UNIDAD_TIEMPO.put("D", "DIAS");
    LISTA_UNIDAD_TIEMPO.put("S", "SEMANAS");
    LISTA_UNIDAD_TIEMPO.put("M", "MESES");
    LISTA_UNIDAD_TIEMPO.put("A", "AÑOS");
    LISTA_ORIGEN_CURSO.put("I", "INICIATIVA PROPIA");
    LISTA_ORIGEN_CURSO.put("O", "ORGANISMO");
    LISTA_PARTICIPACION.put("P", "PARTICIPANTE");
    LISTA_PARTICIPACION.put("I", "INSTRUCTOR");
    LISTA_PARTICIPACION.put("O", "OYENTE");
    LISTA_PARTICIPACION.put("N", "PONENTE");
    LISTA_PARTICIPACION.put("C", "COORDINADOR");
    LISTA_CERTIFICO.put("S", "SI");
    LISTA_CERTIFICO.put("N", "NO");
    LISTA_DIPLOMA.put("S", "SI");
    LISTA_DIPLOMA.put("N", "NO");
    LISTA_BECADO.put("S", "SI");
    LISTA_BECADO.put("N", "NO");
    LISTA_FINANCIAMIENTO.put("O", "ORGANISMO");
    LISTA_FINANCIAMIENTO.put("E", "FINANCIAMIENTO EXTERNO");
    LISTA_FINANCIAMIENTO.put("P", "FINANCIAMIENTO PARCIAL");
    LISTA_FINANCIAMIENTO.put("N", "NINGUNO");
  }

  public ElegibleEstudio()
  {
    jdoSetanio(this, 0);

    jdoSetduracion(this, 0);

    jdoSetunidadTiempo(this, "H");

    jdoSetorigenCurso(this, "I");

    jdoSetparticipacion(this, "P");

    jdoSetcertifico(this, "N");

    jdoSetbecado(this, "N");

    jdoSetfinanciamiento(this, "N");
  }

  public String toString()
  {
    return jdoGetnombreCurso(this) + "  - " + 
      jdoGetanio(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public int getAniosExperiencia()
  {
    return jdoGetaniosExperiencia(this);
  }

  public void setAniosExperiencia(int aniosExperiencia)
  {
    jdoSetaniosExperiencia(this, aniosExperiencia);
  }

  public AreaConocimiento getAreaConocimiento()
  {
    return jdoGetareaConocimiento(this);
  }

  public void setAreaConocimiento(AreaConocimiento areaConocimiento)
  {
    jdoSetareaConocimiento(this, areaConocimiento);
  }

  public String getBecado()
  {
    return jdoGetbecado(this);
  }

  public void setBecado(String becado)
  {
    jdoSetbecado(this, becado);
  }

  public String getCertifico()
  {
    return jdoGetcertifico(this);
  }

  public void setCertifico(String certifico)
  {
    jdoSetcertifico(this, certifico);
  }

  public int getDuracion()
  {
    return jdoGetduracion(this);
  }

  public void setDuracion(int duracion)
  {
    jdoSetduracion(this, duracion);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public String getFinanciamiento()
  {
    return jdoGetfinanciamiento(this);
  }

  public void setFinanciamiento(String financiamiento)
  {
    jdoSetfinanciamiento(this, financiamiento);
  }

  public long getIdElegibleEstudio()
  {
    return jdoGetidElegibleEstudio(this);
  }

  public void setIdElegibleEstudio(long idElegibleEstudio)
  {
    jdoSetidElegibleEstudio(this, idElegibleEstudio);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public int getMesesExperiencia()
  {
    return jdoGetmesesExperiencia(this);
  }

  public void setMesesExperiencia(int mesesExperiencia)
  {
    jdoSetmesesExperiencia(this, mesesExperiencia);
  }

  public String getNombreCurso()
  {
    return jdoGetnombreCurso(this);
  }

  public void setNombreCurso(String nombreCurso)
  {
    jdoSetnombreCurso(this, nombreCurso);
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public void setNombreEntidad(String nombreEntidad)
  {
    jdoSetnombreEntidad(this, nombreEntidad);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public String getOrigenCurso()
  {
    return jdoGetorigenCurso(this);
  }

  public void setOrigenCurso(String origenCurso)
  {
    jdoSetorigenCurso(this, origenCurso);
  }

  public Pais getPais()
  {
    return jdoGetpais(this);
  }

  public void setPais(Pais pais)
  {
    jdoSetpais(this, pais);
  }

  public String getParticipacion()
  {
    return jdoGetparticipacion(this);
  }

  public void setParticipacion(String participacion)
  {
    jdoSetparticipacion(this, participacion);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public TipoCurso getTipoCurso()
  {
    return jdoGettipoCurso(this);
  }

  public void setTipoCurso(TipoCurso tipoCurso)
  {
    jdoSettipoCurso(this, tipoCurso);
  }

  public String getUnidadTiempo()
  {
    return jdoGetunidadTiempo(this);
  }

  public void setUnidadTiempo(String unidadTiempo)
  {
    jdoSetunidadTiempo(this, unidadTiempo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 20;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleEstudio localElegibleEstudio = new ElegibleEstudio();
    localElegibleEstudio.jdoFlags = 1;
    localElegibleEstudio.jdoStateManager = paramStateManager;
    return localElegibleEstudio;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleEstudio localElegibleEstudio = new ElegibleEstudio();
    localElegibleEstudio.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleEstudio.jdoFlags = 1;
    localElegibleEstudio.jdoStateManager = paramStateManager;
    return localElegibleEstudio;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosExperiencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaConocimiento);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.becado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.certifico);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.duracion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.financiamiento);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleEstudio);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesExperiencia);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCurso);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origenCurso);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.pais);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.participacion);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoCurso);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.unidadTiempo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaConocimiento = ((AreaConocimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.becado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.certifico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.duracion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.financiamiento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleEstudio = localStateManager.replacingLongField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origenCurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pais = ((Pais)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.participacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCurso = ((TipoCurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadTiempo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleEstudio paramElegibleEstudio, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramElegibleEstudio.anio;
      return;
    case 1:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.aniosExperiencia = paramElegibleEstudio.aniosExperiencia;
      return;
    case 2:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.areaConocimiento = paramElegibleEstudio.areaConocimiento;
      return;
    case 3:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.becado = paramElegibleEstudio.becado;
      return;
    case 4:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.certifico = paramElegibleEstudio.certifico;
      return;
    case 5:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.duracion = paramElegibleEstudio.duracion;
      return;
    case 6:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleEstudio.elegible;
      return;
    case 7:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.financiamiento = paramElegibleEstudio.financiamiento;
      return;
    case 8:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleEstudio = paramElegibleEstudio.idElegibleEstudio;
      return;
    case 9:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleEstudio.idSitp;
      return;
    case 10:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.mesesExperiencia = paramElegibleEstudio.mesesExperiencia;
      return;
    case 11:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCurso = paramElegibleEstudio.nombreCurso;
      return;
    case 12:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramElegibleEstudio.nombreEntidad;
      return;
    case 13:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramElegibleEstudio.observaciones;
      return;
    case 14:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.origenCurso = paramElegibleEstudio.origenCurso;
      return;
    case 15:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.pais = paramElegibleEstudio.pais;
      return;
    case 16:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.participacion = paramElegibleEstudio.participacion;
      return;
    case 17:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleEstudio.tiempoSitp;
      return;
    case 18:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCurso = paramElegibleEstudio.tipoCurso;
      return;
    case 19:
      if (paramElegibleEstudio == null)
        throw new IllegalArgumentException("arg1");
      this.unidadTiempo = paramElegibleEstudio.unidadTiempo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleEstudio))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleEstudio localElegibleEstudio = (ElegibleEstudio)paramObject;
    if (localElegibleEstudio.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleEstudio, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleEstudioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleEstudioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleEstudioPK))
      throw new IllegalArgumentException("arg1");
    ElegibleEstudioPK localElegibleEstudioPK = (ElegibleEstudioPK)paramObject;
    localElegibleEstudioPK.idElegibleEstudio = this.idElegibleEstudio;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleEstudioPK))
      throw new IllegalArgumentException("arg1");
    ElegibleEstudioPK localElegibleEstudioPK = (ElegibleEstudioPK)paramObject;
    this.idElegibleEstudio = localElegibleEstudioPK.idElegibleEstudio;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleEstudioPK))
      throw new IllegalArgumentException("arg2");
    ElegibleEstudioPK localElegibleEstudioPK = (ElegibleEstudioPK)paramObject;
    localElegibleEstudioPK.idElegibleEstudio = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 8);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleEstudioPK))
      throw new IllegalArgumentException("arg2");
    ElegibleEstudioPK localElegibleEstudioPK = (ElegibleEstudioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 8, localElegibleEstudioPK.idElegibleEstudio);
  }

  private static final int jdoGetanio(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.anio;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.anio;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 0))
      return paramElegibleEstudio.anio;
    return localStateManager.getIntField(paramElegibleEstudio, jdoInheritedFieldCount + 0, paramElegibleEstudio.anio);
  }

  private static final void jdoSetanio(ElegibleEstudio paramElegibleEstudio, int paramInt)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEstudio, jdoInheritedFieldCount + 0, paramElegibleEstudio.anio, paramInt);
  }

  private static final int jdoGetaniosExperiencia(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.aniosExperiencia;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.aniosExperiencia;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 1))
      return paramElegibleEstudio.aniosExperiencia;
    return localStateManager.getIntField(paramElegibleEstudio, jdoInheritedFieldCount + 1, paramElegibleEstudio.aniosExperiencia);
  }

  private static final void jdoSetaniosExperiencia(ElegibleEstudio paramElegibleEstudio, int paramInt)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.aniosExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.aniosExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEstudio, jdoInheritedFieldCount + 1, paramElegibleEstudio.aniosExperiencia, paramInt);
  }

  private static final AreaConocimiento jdoGetareaConocimiento(ElegibleEstudio paramElegibleEstudio)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.areaConocimiento;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 2))
      return paramElegibleEstudio.areaConocimiento;
    return (AreaConocimiento)localStateManager.getObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 2, paramElegibleEstudio.areaConocimiento);
  }

  private static final void jdoSetareaConocimiento(ElegibleEstudio paramElegibleEstudio, AreaConocimiento paramAreaConocimiento)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.areaConocimiento = paramAreaConocimiento;
      return;
    }
    localStateManager.setObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 2, paramElegibleEstudio.areaConocimiento, paramAreaConocimiento);
  }

  private static final String jdoGetbecado(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.becado;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.becado;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 3))
      return paramElegibleEstudio.becado;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 3, paramElegibleEstudio.becado);
  }

  private static final void jdoSetbecado(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.becado = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.becado = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 3, paramElegibleEstudio.becado, paramString);
  }

  private static final String jdoGetcertifico(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.certifico;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.certifico;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 4))
      return paramElegibleEstudio.certifico;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 4, paramElegibleEstudio.certifico);
  }

  private static final void jdoSetcertifico(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.certifico = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.certifico = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 4, paramElegibleEstudio.certifico, paramString);
  }

  private static final int jdoGetduracion(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.duracion;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.duracion;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 5))
      return paramElegibleEstudio.duracion;
    return localStateManager.getIntField(paramElegibleEstudio, jdoInheritedFieldCount + 5, paramElegibleEstudio.duracion);
  }

  private static final void jdoSetduracion(ElegibleEstudio paramElegibleEstudio, int paramInt)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.duracion = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.duracion = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEstudio, jdoInheritedFieldCount + 5, paramElegibleEstudio.duracion, paramInt);
  }

  private static final Elegible jdoGetelegible(ElegibleEstudio paramElegibleEstudio)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.elegible;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 6))
      return paramElegibleEstudio.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 6, paramElegibleEstudio.elegible);
  }

  private static final void jdoSetelegible(ElegibleEstudio paramElegibleEstudio, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 6, paramElegibleEstudio.elegible, paramElegible);
  }

  private static final String jdoGetfinanciamiento(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.financiamiento;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.financiamiento;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 7))
      return paramElegibleEstudio.financiamiento;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 7, paramElegibleEstudio.financiamiento);
  }

  private static final void jdoSetfinanciamiento(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.financiamiento = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.financiamiento = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 7, paramElegibleEstudio.financiamiento, paramString);
  }

  private static final long jdoGetidElegibleEstudio(ElegibleEstudio paramElegibleEstudio)
  {
    return paramElegibleEstudio.idElegibleEstudio;
  }

  private static final void jdoSetidElegibleEstudio(ElegibleEstudio paramElegibleEstudio, long paramLong)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.idElegibleEstudio = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleEstudio, jdoInheritedFieldCount + 8, paramElegibleEstudio.idElegibleEstudio, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.idSitp;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.idSitp;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 9))
      return paramElegibleEstudio.idSitp;
    return localStateManager.getIntField(paramElegibleEstudio, jdoInheritedFieldCount + 9, paramElegibleEstudio.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleEstudio paramElegibleEstudio, int paramInt)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEstudio, jdoInheritedFieldCount + 9, paramElegibleEstudio.idSitp, paramInt);
  }

  private static final int jdoGetmesesExperiencia(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.mesesExperiencia;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.mesesExperiencia;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 10))
      return paramElegibleEstudio.mesesExperiencia;
    return localStateManager.getIntField(paramElegibleEstudio, jdoInheritedFieldCount + 10, paramElegibleEstudio.mesesExperiencia);
  }

  private static final void jdoSetmesesExperiencia(ElegibleEstudio paramElegibleEstudio, int paramInt)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.mesesExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.mesesExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEstudio, jdoInheritedFieldCount + 10, paramElegibleEstudio.mesesExperiencia, paramInt);
  }

  private static final String jdoGetnombreCurso(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.nombreCurso;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.nombreCurso;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 11))
      return paramElegibleEstudio.nombreCurso;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 11, paramElegibleEstudio.nombreCurso);
  }

  private static final void jdoSetnombreCurso(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.nombreCurso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.nombreCurso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 11, paramElegibleEstudio.nombreCurso, paramString);
  }

  private static final String jdoGetnombreEntidad(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.nombreEntidad;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.nombreEntidad;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 12))
      return paramElegibleEstudio.nombreEntidad;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 12, paramElegibleEstudio.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 12, paramElegibleEstudio.nombreEntidad, paramString);
  }

  private static final String jdoGetobservaciones(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.observaciones;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.observaciones;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 13))
      return paramElegibleEstudio.observaciones;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 13, paramElegibleEstudio.observaciones);
  }

  private static final void jdoSetobservaciones(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 13, paramElegibleEstudio.observaciones, paramString);
  }

  private static final String jdoGetorigenCurso(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.origenCurso;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.origenCurso;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 14))
      return paramElegibleEstudio.origenCurso;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 14, paramElegibleEstudio.origenCurso);
  }

  private static final void jdoSetorigenCurso(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.origenCurso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.origenCurso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 14, paramElegibleEstudio.origenCurso, paramString);
  }

  private static final Pais jdoGetpais(ElegibleEstudio paramElegibleEstudio)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.pais;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 15))
      return paramElegibleEstudio.pais;
    return (Pais)localStateManager.getObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 15, paramElegibleEstudio.pais);
  }

  private static final void jdoSetpais(ElegibleEstudio paramElegibleEstudio, Pais paramPais)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.pais = paramPais;
      return;
    }
    localStateManager.setObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 15, paramElegibleEstudio.pais, paramPais);
  }

  private static final String jdoGetparticipacion(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.participacion;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.participacion;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 16))
      return paramElegibleEstudio.participacion;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 16, paramElegibleEstudio.participacion);
  }

  private static final void jdoSetparticipacion(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.participacion = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.participacion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 16, paramElegibleEstudio.participacion, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.tiempoSitp;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 17))
      return paramElegibleEstudio.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 17, paramElegibleEstudio.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleEstudio paramElegibleEstudio, Date paramDate)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 17, paramElegibleEstudio.tiempoSitp, paramDate);
  }

  private static final TipoCurso jdoGettipoCurso(ElegibleEstudio paramElegibleEstudio)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.tipoCurso;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 18))
      return paramElegibleEstudio.tipoCurso;
    return (TipoCurso)localStateManager.getObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 18, paramElegibleEstudio.tipoCurso);
  }

  private static final void jdoSettipoCurso(ElegibleEstudio paramElegibleEstudio, TipoCurso paramTipoCurso)
  {
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.tipoCurso = paramTipoCurso;
      return;
    }
    localStateManager.setObjectField(paramElegibleEstudio, jdoInheritedFieldCount + 18, paramElegibleEstudio.tipoCurso, paramTipoCurso);
  }

  private static final String jdoGetunidadTiempo(ElegibleEstudio paramElegibleEstudio)
  {
    if (paramElegibleEstudio.jdoFlags <= 0)
      return paramElegibleEstudio.unidadTiempo;
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEstudio.unidadTiempo;
    if (localStateManager.isLoaded(paramElegibleEstudio, jdoInheritedFieldCount + 19))
      return paramElegibleEstudio.unidadTiempo;
    return localStateManager.getStringField(paramElegibleEstudio, jdoInheritedFieldCount + 19, paramElegibleEstudio.unidadTiempo);
  }

  private static final void jdoSetunidadTiempo(ElegibleEstudio paramElegibleEstudio, String paramString)
  {
    if (paramElegibleEstudio.jdoFlags == 0)
    {
      paramElegibleEstudio.unidadTiempo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEstudio.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEstudio.unidadTiempo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEstudio, jdoInheritedFieldCount + 19, paramElegibleEstudio.unidadTiempo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}