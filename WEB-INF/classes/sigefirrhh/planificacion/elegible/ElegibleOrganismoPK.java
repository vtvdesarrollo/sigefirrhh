package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleOrganismoPK
  implements Serializable
{
  public long idElegibleOrganismo;

  public ElegibleOrganismoPK()
  {
  }

  public ElegibleOrganismoPK(long idElegibleOrganismo)
  {
    this.idElegibleOrganismo = idElegibleOrganismo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleOrganismoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleOrganismoPK thatPK)
  {
    return 
      this.idElegibleOrganismo == thatPK.idElegibleOrganismo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleOrganismo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleOrganismo);
  }
}