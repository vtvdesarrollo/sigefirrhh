package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.Parroquia;

public class Elegible
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTADO_CIVIL;
  protected static final Map LISTA_SEXO;
  protected static final Map LISTA_SECTOR;
  protected static final Map LISTA_TIPO_VIVIENDA;
  protected static final Map LISTA_TENENCIA_VIVIENDA;
  protected static final Map LISTA_DIESTRALIDAD;
  protected static final Map LISTA_NACIONALIDAD;
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_GRUPO_SANGUINEO;
  private long idElegible;
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private String sexo;
  private Date fechaNacimiento;
  private Ciudad ciudadNacimiento;
  private String estadoCivil;
  private String nivelEducativo;
  private String nacionalidad;
  private String dobleNacionalidad;
  private Pais paisNacionalidad;
  private String nacionalizado;
  private Date fechaNacionalizacion;
  private String gacetaNacionalizacion;
  private String otraNormativaNac;
  private String direccionResidencia;
  private String zonaPostalResidencia;
  private Ciudad ciudadResidencia;
  private Parroquia parroquia;
  private String telefonoResidencia;
  private String telefonoCelular;
  private String telefonoOficina;
  private String email;
  private Date fechaRegistro;
  private String cargoAspira;
  private double sueldoAspira;
  private String disponibilidad;
  private String trabajandoActualmente;
  private String trabajoActual;
  private String cargoActual;
  private double sueldoActual;
  private String motivoRetiro;
  private int cedulaConyugue;
  private String nombreConyugue;
  private String sectorTrabajoConyugue;
  private String mismoOrganismoConyugue;
  private String tieneHijos;
  private String tipoVivienda;
  private String tenenciaVivienda;
  private String maneja;
  private int gradoLicencia;
  private String tieneVehiculo;
  private String marcaVehiculo;
  private String modeloVehiculo;
  private String placaVehiculo;
  private String numeroSso;
  private String numeroRif;
  private String numeroLibretaMilitar;
  private double estatura;
  private double peso;
  private String diestralidad;
  private String grupoSanguineo;
  private int aniosServicioApn;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aniosServicioApn", "cargoActual", "cargoAspira", "cedula", "cedulaConyugue", "ciudadNacimiento", "ciudadResidencia", "diestralidad", "direccionResidencia", "disponibilidad", "dobleNacionalidad", "email", "estadoCivil", "estatura", "fechaNacimiento", "fechaNacionalizacion", "fechaRegistro", "gacetaNacionalizacion", "gradoLicencia", "grupoSanguineo", "idElegible", "idSitp", "maneja", "marcaVehiculo", "mismoOrganismoConyugue", "modeloVehiculo", "motivoRetiro", "nacionalidad", "nacionalizado", "nivelEducativo", "nombreConyugue", "numeroLibretaMilitar", "numeroRif", "numeroSso", "otraNormativaNac", "paisNacionalidad", "parroquia", "peso", "placaVehiculo", "primerApellido", "primerNombre", "sectorTrabajoConyugue", "segundoApellido", "segundoNombre", "sexo", "sueldoActual", "sueldoAspira", "telefonoCelular", "telefonoOficina", "telefonoResidencia", "tenenciaVivienda", "tiempoSitp", "tieneHijos", "tieneVehiculo", "tipoVivienda", "trabajandoActualmente", "trabajoActual", "zonaPostalResidencia" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Pais"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Parroquia"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 26, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Elegible());

    LISTA_ESTADO_CIVIL = 
      new LinkedHashMap();
    LISTA_SEXO = 
      new LinkedHashMap();
    LISTA_SECTOR = 
      new LinkedHashMap();
    LISTA_TIPO_VIVIENDA = 
      new LinkedHashMap();
    LISTA_TENENCIA_VIVIENDA = 
      new LinkedHashMap();
    LISTA_DIESTRALIDAD = 
      new LinkedHashMap();
    LISTA_NACIONALIDAD = 
      new LinkedHashMap();
    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_GRUPO_SANGUINEO = 
      new LinkedHashMap();

    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
    LISTA_ESTADO_CIVIL.put("D", "DIVORCIADO(A)");
    LISTA_ESTADO_CIVIL.put("V", "VIUDO(A)");
    LISTA_ESTADO_CIVIL.put("U", "UNIDO(A)");
    LISTA_ESTADO_CIVIL.put("O", "OTRO");
    LISTA_SEXO.put("F", "FEMENINO");
    LISTA_SEXO.put("M", "MASCULINO");
    LISTA_SECTOR.put("P", "PRIVADO");
    LISTA_SECTOR.put("U", "PUBLICO");
    LISTA_SECTOR.put("N", "NINGUNO");
    LISTA_NACIONALIDAD.put("V", "VENEZOLANO(A)");
    LISTA_NACIONALIDAD.put("E", "EXTRANJERO(A)");
    LISTA_DIESTRALIDAD.put("D", "DERECHO(A)");
    LISTA_DIESTRALIDAD.put("Z", "ZURDO(A)");
    LISTA_DIESTRALIDAD.put("A", "AMBIDIESTRO");
    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("N", "NINGUNO");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_GRUPO_SANGUINEO.put("A+", "A+");
    LISTA_GRUPO_SANGUINEO.put("B+", "B+");
    LISTA_GRUPO_SANGUINEO.put("O+", "O+");
    LISTA_GRUPO_SANGUINEO.put("A-", "A-");
    LISTA_GRUPO_SANGUINEO.put("B-", "B-");
    LISTA_GRUPO_SANGUINEO.put("O-", "O-");
    LISTA_GRUPO_SANGUINEO.put("AB+", "AB+");
    LISTA_GRUPO_SANGUINEO.put("AB-", "AB-");
    LISTA_GRUPO_SANGUINEO.put("NO", "NO");
    LISTA_TIPO_VIVIENDA.put("C", "CASA");
    LISTA_TIPO_VIVIENDA.put("A", "APARTAMENTO");
    LISTA_TIPO_VIVIENDA.put("B", "HABITACION");
    LISTA_TIPO_VIVIENDA.put("H", "HOTEL");
    LISTA_TENENCIA_VIVIENDA.put("A", "ALQUILADA");
    LISTA_TENENCIA_VIVIENDA.put("P", "PROPIA");
    LISTA_TENENCIA_VIVIENDA.put("H", "PAGANDO");
    LISTA_TENENCIA_VIVIENDA.put("F", "FAMILIAR");
  }

  public Elegible()
  {
    jdoSetestadoCivil(this, "S");

    jdoSetnivelEducativo(this, "N");

    jdoSetnacionalidad(this, "V");

    jdoSetdobleNacionalidad(this, "N");

    jdoSetnacionalizado(this, "N");

    jdoSetsueldoAspira(this, 0.0D);

    jdoSettrabajandoActualmente(this, "N");

    jdoSetsectorTrabajoConyugue(this, "N");

    jdoSetmismoOrganismoConyugue(this, "N");

    jdoSettieneHijos(this, "N");

    jdoSettipoVivienda(this, "C");

    jdoSettenenciaVivienda(this, "P");

    jdoSetmaneja(this, "N");

    jdoSettieneVehiculo(this, "N");

    jdoSetdiestralidad(this, "D");

    jdoSetgrupoSanguineo(this, "NO");

    jdoSetaniosServicioApn(this, 0);
  }

  public String toString()
  {
    return jdoGetprimerApellido(this) + " " + 
      jdoGetsegundoApellido(this) + ", " + 
      jdoGetprimerNombre(this) + " " + 
      jdoGetsegundoNombre(this) + "  -  " + 
      jdoGetcedula(this);
  }

  public int getAniosServicioApn()
  {
    return jdoGetaniosServicioApn(this);
  }

  public void setAniosServicioApn(int aniosServicioApn)
  {
    jdoSetaniosServicioApn(this, aniosServicioApn);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }

  public void setCedula(int cedula)
  {
    jdoSetcedula(this, cedula);
  }

  public int getCedulaConyugue()
  {
    return jdoGetcedulaConyugue(this);
  }

  public void setCedulaConyugue(int cedulaConyugue)
  {
    jdoSetcedulaConyugue(this, cedulaConyugue);
  }

  public Ciudad getCiudadNacimiento()
  {
    return jdoGetciudadNacimiento(this);
  }

  public void setCiudadNacimiento(Ciudad ciudadNacimiento)
  {
    jdoSetciudadNacimiento(this, ciudadNacimiento);
  }

  public Ciudad getCiudadResidencia()
  {
    return jdoGetciudadResidencia(this);
  }

  public void setCiudadResidencia(Ciudad ciudadResidencia)
  {
    jdoSetciudadResidencia(this, ciudadResidencia);
  }

  public String getDiestralidad()
  {
    return jdoGetdiestralidad(this);
  }

  public void setDiestralidad(String diestralidad)
  {
    jdoSetdiestralidad(this, diestralidad);
  }

  public String getDireccionResidencia()
  {
    return jdoGetdireccionResidencia(this);
  }

  public void setDireccionResidencia(String direccionResidencia)
  {
    jdoSetdireccionResidencia(this, direccionResidencia);
  }

  public String getDobleNacionalidad()
  {
    return jdoGetdobleNacionalidad(this);
  }

  public void setDobleNacionalidad(String dobleNacionalidad)
  {
    jdoSetdobleNacionalidad(this, dobleNacionalidad);
  }

  public String getEmail()
  {
    return jdoGetemail(this);
  }

  public void setEmail(String email)
  {
    jdoSetemail(this, email);
  }

  public String getEstadoCivil()
  {
    return jdoGetestadoCivil(this);
  }

  public void setEstadoCivil(String estadoCivil)
  {
    jdoSetestadoCivil(this, estadoCivil);
  }

  public double getEstatura()
  {
    return jdoGetestatura(this);
  }

  public void setEstatura(double estatura)
  {
    jdoSetestatura(this, estatura);
  }

  public Date getFechaNacimiento()
  {
    return jdoGetfechaNacimiento(this);
  }

  public void setFechaNacimiento(Date fechaNacimiento)
  {
    jdoSetfechaNacimiento(this, fechaNacimiento);
  }

  public Date getFechaNacionalizacion()
  {
    return jdoGetfechaNacionalizacion(this);
  }

  public void setFechaNacionalizacion(Date fechaNacionalizacion)
  {
    jdoSetfechaNacionalizacion(this, fechaNacionalizacion);
  }

  public String getGacetaNacionalizacion()
  {
    return jdoGetgacetaNacionalizacion(this);
  }

  public void setGacetaNacionalizacion(String gacetaNacionalizacion)
  {
    jdoSetgacetaNacionalizacion(this, gacetaNacionalizacion);
  }

  public int getGradoLicencia()
  {
    return jdoGetgradoLicencia(this);
  }

  public void setGradoLicencia(int gradoLicencia)
  {
    jdoSetgradoLicencia(this, gradoLicencia);
  }

  public String getGrupoSanguineo()
  {
    return jdoGetgrupoSanguineo(this);
  }

  public void setGrupoSanguineo(String grupoSanguineo)
  {
    jdoSetgrupoSanguineo(this, grupoSanguineo);
  }

  public long getIdElegible()
  {
    return jdoGetidElegible(this);
  }

  public void setIdElegible(long idElegible)
  {
    jdoSetidElegible(this, idElegible);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getManeja()
  {
    return jdoGetmaneja(this);
  }

  public void setManeja(String maneja)
  {
    jdoSetmaneja(this, maneja);
  }

  public String getMarcaVehiculo()
  {
    return jdoGetmarcaVehiculo(this);
  }

  public void setMarcaVehiculo(String marcaVehiculo)
  {
    jdoSetmarcaVehiculo(this, marcaVehiculo);
  }

  public String getMismoOrganismoConyugue()
  {
    return jdoGetmismoOrganismoConyugue(this);
  }

  public void setMismoOrganismoConyugue(String mismoOrganismoConyugue)
  {
    jdoSetmismoOrganismoConyugue(this, mismoOrganismoConyugue);
  }

  public String getModeloVehiculo()
  {
    return jdoGetmodeloVehiculo(this);
  }

  public void setModeloVehiculo(String modeloVehiculo)
  {
    jdoSetmodeloVehiculo(this, modeloVehiculo);
  }

  public String getNacionalidad()
  {
    return jdoGetnacionalidad(this);
  }

  public void setNacionalidad(String nacionalidad)
  {
    jdoSetnacionalidad(this, nacionalidad);
  }

  public String getNacionalizado()
  {
    return jdoGetnacionalizado(this);
  }

  public void setNacionalizado(String nacionalizado)
  {
    jdoSetnacionalizado(this, nacionalizado);
  }

  public String getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public void setNivelEducativo(String nivelEducativo)
  {
    jdoSetnivelEducativo(this, nivelEducativo);
  }

  public String getNombreConyugue()
  {
    return jdoGetnombreConyugue(this);
  }

  public void setNombreConyugue(String nombreConyugue)
  {
    jdoSetnombreConyugue(this, nombreConyugue);
  }

  public String getNumeroLibretaMilitar()
  {
    return jdoGetnumeroLibretaMilitar(this);
  }

  public void setNumeroLibretaMilitar(String numeroLibretaMilitar)
  {
    jdoSetnumeroLibretaMilitar(this, numeroLibretaMilitar);
  }

  public String getNumeroRif()
  {
    return jdoGetnumeroRif(this);
  }

  public void setNumeroRif(String numeroRif)
  {
    jdoSetnumeroRif(this, numeroRif);
  }

  public String getNumeroSso()
  {
    return jdoGetnumeroSso(this);
  }

  public void setNumeroSso(String numeroSso)
  {
    jdoSetnumeroSso(this, numeroSso);
  }

  public String getOtraNormativaNac()
  {
    return jdoGetotraNormativaNac(this);
  }

  public void setOtraNormativaNac(String otraNormativaNac)
  {
    jdoSetotraNormativaNac(this, otraNormativaNac);
  }

  public Pais getPaisNacionalidad()
  {
    return jdoGetpaisNacionalidad(this);
  }

  public void setPaisNacionalidad(Pais paisNacionalidad)
  {
    jdoSetpaisNacionalidad(this, paisNacionalidad);
  }

  public Parroquia getParroquia()
  {
    return jdoGetparroquia(this);
  }

  public void setParroquia(Parroquia parroquia)
  {
    jdoSetparroquia(this, parroquia);
  }

  public double getPeso()
  {
    return jdoGetpeso(this);
  }

  public void setPeso(double peso)
  {
    jdoSetpeso(this, peso);
  }

  public String getPlacaVehiculo()
  {
    return jdoGetplacaVehiculo(this);
  }

  public void setPlacaVehiculo(String placaVehiculo)
  {
    jdoSetplacaVehiculo(this, placaVehiculo);
  }

  public String getPrimerApellido()
  {
    return jdoGetprimerApellido(this);
  }

  public void setPrimerApellido(String primerApellido)
  {
    jdoSetprimerApellido(this, primerApellido);
  }

  public String getPrimerNombre()
  {
    return jdoGetprimerNombre(this);
  }

  public void setPrimerNombre(String primerNombre)
  {
    jdoSetprimerNombre(this, primerNombre);
  }

  public String getSectorTrabajoConyugue()
  {
    return jdoGetsectorTrabajoConyugue(this);
  }

  public void setSectorTrabajoConyugue(String sectorTrabajoConyugue)
  {
    jdoSetsectorTrabajoConyugue(this, sectorTrabajoConyugue);
  }

  public String getSegundoApellido()
  {
    return jdoGetsegundoApellido(this);
  }

  public void setSegundoApellido(String segundoApellido)
  {
    jdoSetsegundoApellido(this, segundoApellido);
  }

  public String getSegundoNombre()
  {
    return jdoGetsegundoNombre(this);
  }

  public void setSegundoNombre(String segundoNombre)
  {
    jdoSetsegundoNombre(this, segundoNombre);
  }

  public String getSexo()
  {
    return jdoGetsexo(this);
  }

  public void setSexo(String sexo)
  {
    jdoSetsexo(this, sexo);
  }

  public String getTelefonoCelular()
  {
    return jdoGettelefonoCelular(this);
  }

  public void setTelefonoCelular(String telefonoCelular)
  {
    jdoSettelefonoCelular(this, telefonoCelular);
  }

  public String getTelefonoOficina()
  {
    return jdoGettelefonoOficina(this);
  }

  public void setTelefonoOficina(String telefonoOficina)
  {
    jdoSettelefonoOficina(this, telefonoOficina);
  }

  public String getTelefonoResidencia()
  {
    return jdoGettelefonoResidencia(this);
  }

  public void setTelefonoResidencia(String telefonoResidencia)
  {
    jdoSettelefonoResidencia(this, telefonoResidencia);
  }

  public String getTenenciaVivienda()
  {
    return jdoGettenenciaVivienda(this);
  }

  public void setTenenciaVivienda(String tenenciaVivienda)
  {
    jdoSettenenciaVivienda(this, tenenciaVivienda);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public String getTieneHijos()
  {
    return jdoGettieneHijos(this);
  }

  public void setTieneHijos(String tieneHijos)
  {
    jdoSettieneHijos(this, tieneHijos);
  }

  public String getTieneVehiculo()
  {
    return jdoGettieneVehiculo(this);
  }

  public void setTieneVehiculo(String tieneVehiculo)
  {
    jdoSettieneVehiculo(this, tieneVehiculo);
  }

  public String getTipoVivienda()
  {
    return jdoGettipoVivienda(this);
  }

  public void setTipoVivienda(String tipoVivienda)
  {
    jdoSettipoVivienda(this, tipoVivienda);
  }

  public String getZonaPostalResidencia()
  {
    return jdoGetzonaPostalResidencia(this);
  }

  public void setZonaPostalResidencia(String zonaPostalResidencia)
  {
    jdoSetzonaPostalResidencia(this, zonaPostalResidencia);
  }
  public String getCargoActual() {
    return jdoGetcargoActual(this);
  }
  public void setCargoActual(String cargoActual) {
    jdoSetcargoActual(this, cargoActual);
  }
  public String getCargoAspira() {
    return jdoGetcargoAspira(this);
  }
  public void setCargoAspira(String cargoAspira) {
    jdoSetcargoAspira(this, cargoAspira);
  }
  public String getDisponibilidad() {
    return jdoGetdisponibilidad(this);
  }
  public void setDisponibilidad(String disponibilidad) {
    jdoSetdisponibilidad(this, disponibilidad);
  }
  public Date getFechaRegistro() {
    return jdoGetfechaRegistro(this);
  }
  public void setFechaRegistro(Date fechaRegistro) {
    jdoSetfechaRegistro(this, fechaRegistro);
  }
  public String getMotivoRetiro() {
    return jdoGetmotivoRetiro(this);
  }
  public void setMotivoRetiro(String motivoRetiro) {
    jdoSetmotivoRetiro(this, motivoRetiro);
  }
  public double getSueldoActual() {
    return jdoGetsueldoActual(this);
  }
  public void setSueldoActual(double sueldoActual) {
    jdoSetsueldoActual(this, sueldoActual);
  }
  public double getSueldoAspira() {
    return jdoGetsueldoAspira(this);
  }
  public void setSueldoAspira(double sueldoAspira) {
    jdoSetsueldoAspira(this, sueldoAspira);
  }
  public String getTrabajandoActualmente() {
    return jdoGettrabajandoActualmente(this);
  }
  public void setTrabajandoActualmente(String trabajandoActualmente) {
    jdoSettrabajandoActualmente(this, trabajandoActualmente);
  }
  public String getTrabajoActual() {
    return jdoGettrabajoActual(this);
  }
  public void setTrabajoActual(String trabajoActual) {
    jdoSettrabajoActual(this, trabajoActual);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 58;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Elegible localElegible = new Elegible();
    localElegible.jdoFlags = 1;
    localElegible.jdoStateManager = paramStateManager;
    return localElegible;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Elegible localElegible = new Elegible();
    localElegible.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegible.jdoFlags = 1;
    localElegible.jdoStateManager = paramStateManager;
    return localElegible;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosServicioApn);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoActual);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoAspira);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaConyugue);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudadNacimiento);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudadResidencia);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.diestralidad);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccionResidencia);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.disponibilidad);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.dobleNacionalidad);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.email);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estadoCivil);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.estatura);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaNacimiento);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaNacionalizacion);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gacetaNacionalizacion);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.gradoLicencia);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.grupoSanguineo);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegible);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.maneja);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.marcaVehiculo);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mismoOrganismoConyugue);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.modeloVehiculo);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.motivoRetiro);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nacionalidad);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nacionalizado);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreConyugue);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroLibretaMilitar);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroRif);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroSso);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.otraNormativaNac);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.paisNacionalidad);
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.parroquia);
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.peso);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.placaVehiculo);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sectorTrabajoConyugue);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sexo);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoActual);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.sueldoAspira);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoCelular);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoOficina);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefonoResidencia);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tenenciaVivienda);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tieneHijos);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tieneVehiculo);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoVivienda);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.trabajandoActualmente);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.trabajoActual);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.zonaPostalResidencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosServicioApn = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoActual = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoAspira = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaConyugue = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudadNacimiento = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudadResidencia = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diestralidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccionResidencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.disponibilidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dobleNacionalidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.email = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estadoCivil = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatura = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaNacimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaNacionalizacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gacetaNacionalizacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gradoLicencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoSanguineo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegible = localStateManager.replacingLongField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.maneja = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.marcaVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mismoOrganismoConyugue = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.modeloVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.motivoRetiro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nacionalidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nacionalizado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreConyugue = localStateManager.replacingStringField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroLibretaMilitar = localStateManager.replacingStringField(this, paramInt);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroRif = localStateManager.replacingStringField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroSso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otraNormativaNac = localStateManager.replacingStringField(this, paramInt);
      return;
    case 35:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.paisNacionalidad = ((Pais)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 36:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parroquia = ((Parroquia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 37:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.peso = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 38:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.placaVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 39:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 40:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 41:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sectorTrabajoConyugue = localStateManager.replacingStringField(this, paramInt);
      return;
    case 42:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 43:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 44:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sexo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 45:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoActual = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 46:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sueldoAspira = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 47:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoCelular = localStateManager.replacingStringField(this, paramInt);
      return;
    case 48:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoOficina = localStateManager.replacingStringField(this, paramInt);
      return;
    case 49:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefonoResidencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 50:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tenenciaVivienda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 51:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 52:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tieneHijos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 53:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tieneVehiculo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 54:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoVivienda = localStateManager.replacingStringField(this, paramInt);
      return;
    case 55:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajandoActualmente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 56:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajoActual = localStateManager.replacingStringField(this, paramInt);
      return;
    case 57:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.zonaPostalResidencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Elegible paramElegible, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.aniosServicioApn = paramElegible.aniosServicioApn;
      return;
    case 1:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.cargoActual = paramElegible.cargoActual;
      return;
    case 2:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.cargoAspira = paramElegible.cargoAspira;
      return;
    case 3:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramElegible.cedula;
      return;
    case 4:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaConyugue = paramElegible.cedulaConyugue;
      return;
    case 5:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.ciudadNacimiento = paramElegible.ciudadNacimiento;
      return;
    case 6:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.ciudadResidencia = paramElegible.ciudadResidencia;
      return;
    case 7:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.diestralidad = paramElegible.diestralidad;
      return;
    case 8:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.direccionResidencia = paramElegible.direccionResidencia;
      return;
    case 9:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.disponibilidad = paramElegible.disponibilidad;
      return;
    case 10:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.dobleNacionalidad = paramElegible.dobleNacionalidad;
      return;
    case 11:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.email = paramElegible.email;
      return;
    case 12:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.estadoCivil = paramElegible.estadoCivil;
      return;
    case 13:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.estatura = paramElegible.estatura;
      return;
    case 14:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.fechaNacimiento = paramElegible.fechaNacimiento;
      return;
    case 15:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.fechaNacionalizacion = paramElegible.fechaNacionalizacion;
      return;
    case 16:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramElegible.fechaRegistro;
      return;
    case 17:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.gacetaNacionalizacion = paramElegible.gacetaNacionalizacion;
      return;
    case 18:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.gradoLicencia = paramElegible.gradoLicencia;
      return;
    case 19:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.grupoSanguineo = paramElegible.grupoSanguineo;
      return;
    case 20:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.idElegible = paramElegible.idElegible;
      return;
    case 21:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegible.idSitp;
      return;
    case 22:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.maneja = paramElegible.maneja;
      return;
    case 23:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.marcaVehiculo = paramElegible.marcaVehiculo;
      return;
    case 24:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.mismoOrganismoConyugue = paramElegible.mismoOrganismoConyugue;
      return;
    case 25:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.modeloVehiculo = paramElegible.modeloVehiculo;
      return;
    case 26:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.motivoRetiro = paramElegible.motivoRetiro;
      return;
    case 27:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.nacionalidad = paramElegible.nacionalidad;
      return;
    case 28:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.nacionalizado = paramElegible.nacionalizado;
      return;
    case 29:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramElegible.nivelEducativo;
      return;
    case 30:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.nombreConyugue = paramElegible.nombreConyugue;
      return;
    case 31:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.numeroLibretaMilitar = paramElegible.numeroLibretaMilitar;
      return;
    case 32:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.numeroRif = paramElegible.numeroRif;
      return;
    case 33:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.numeroSso = paramElegible.numeroSso;
      return;
    case 34:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.otraNormativaNac = paramElegible.otraNormativaNac;
      return;
    case 35:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.paisNacionalidad = paramElegible.paisNacionalidad;
      return;
    case 36:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.parroquia = paramElegible.parroquia;
      return;
    case 37:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.peso = paramElegible.peso;
      return;
    case 38:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.placaVehiculo = paramElegible.placaVehiculo;
      return;
    case 39:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramElegible.primerApellido;
      return;
    case 40:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramElegible.primerNombre;
      return;
    case 41:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.sectorTrabajoConyugue = paramElegible.sectorTrabajoConyugue;
      return;
    case 42:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramElegible.segundoApellido;
      return;
    case 43:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramElegible.segundoNombre;
      return;
    case 44:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.sexo = paramElegible.sexo;
      return;
    case 45:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoActual = paramElegible.sueldoActual;
      return;
    case 46:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.sueldoAspira = paramElegible.sueldoAspira;
      return;
    case 47:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoCelular = paramElegible.telefonoCelular;
      return;
    case 48:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoOficina = paramElegible.telefonoOficina;
      return;
    case 49:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.telefonoResidencia = paramElegible.telefonoResidencia;
      return;
    case 50:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.tenenciaVivienda = paramElegible.tenenciaVivienda;
      return;
    case 51:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegible.tiempoSitp;
      return;
    case 52:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.tieneHijos = paramElegible.tieneHijos;
      return;
    case 53:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.tieneVehiculo = paramElegible.tieneVehiculo;
      return;
    case 54:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.tipoVivienda = paramElegible.tipoVivienda;
      return;
    case 55:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.trabajandoActualmente = paramElegible.trabajandoActualmente;
      return;
    case 56:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.trabajoActual = paramElegible.trabajoActual;
      return;
    case 57:
      if (paramElegible == null)
        throw new IllegalArgumentException("arg1");
      this.zonaPostalResidencia = paramElegible.zonaPostalResidencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Elegible))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Elegible localElegible = (Elegible)paramObject;
    if (localElegible.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegible, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegiblePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegiblePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegiblePK))
      throw new IllegalArgumentException("arg1");
    ElegiblePK localElegiblePK = (ElegiblePK)paramObject;
    localElegiblePK.idElegible = this.idElegible;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegiblePK))
      throw new IllegalArgumentException("arg1");
    ElegiblePK localElegiblePK = (ElegiblePK)paramObject;
    this.idElegible = localElegiblePK.idElegible;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegiblePK))
      throw new IllegalArgumentException("arg2");
    ElegiblePK localElegiblePK = (ElegiblePK)paramObject;
    localElegiblePK.idElegible = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 20);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegiblePK))
      throw new IllegalArgumentException("arg2");
    ElegiblePK localElegiblePK = (ElegiblePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 20, localElegiblePK.idElegible);
  }

  private static final int jdoGetaniosServicioApn(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.aniosServicioApn;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.aniosServicioApn;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 0))
      return paramElegible.aniosServicioApn;
    return localStateManager.getIntField(paramElegible, jdoInheritedFieldCount + 0, paramElegible.aniosServicioApn);
  }

  private static final void jdoSetaniosServicioApn(Elegible paramElegible, int paramInt)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.aniosServicioApn = paramInt;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.aniosServicioApn = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegible, jdoInheritedFieldCount + 0, paramElegible.aniosServicioApn, paramInt);
  }

  private static final String jdoGetcargoActual(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.cargoActual;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.cargoActual;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 1))
      return paramElegible.cargoActual;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 1, paramElegible.cargoActual);
  }

  private static final void jdoSetcargoActual(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.cargoActual = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.cargoActual = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 1, paramElegible.cargoActual, paramString);
  }

  private static final String jdoGetcargoAspira(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.cargoAspira;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.cargoAspira;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 2))
      return paramElegible.cargoAspira;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 2, paramElegible.cargoAspira);
  }

  private static final void jdoSetcargoAspira(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.cargoAspira = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.cargoAspira = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 2, paramElegible.cargoAspira, paramString);
  }

  private static final int jdoGetcedula(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.cedula;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.cedula;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 3))
      return paramElegible.cedula;
    return localStateManager.getIntField(paramElegible, jdoInheritedFieldCount + 3, paramElegible.cedula);
  }

  private static final void jdoSetcedula(Elegible paramElegible, int paramInt)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegible, jdoInheritedFieldCount + 3, paramElegible.cedula, paramInt);
  }

  private static final int jdoGetcedulaConyugue(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.cedulaConyugue;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.cedulaConyugue;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 4))
      return paramElegible.cedulaConyugue;
    return localStateManager.getIntField(paramElegible, jdoInheritedFieldCount + 4, paramElegible.cedulaConyugue);
  }

  private static final void jdoSetcedulaConyugue(Elegible paramElegible, int paramInt)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.cedulaConyugue = paramInt;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.cedulaConyugue = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegible, jdoInheritedFieldCount + 4, paramElegible.cedulaConyugue, paramInt);
  }

  private static final Ciudad jdoGetciudadNacimiento(Elegible paramElegible)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.ciudadNacimiento;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 5))
      return paramElegible.ciudadNacimiento;
    return (Ciudad)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 5, paramElegible.ciudadNacimiento);
  }

  private static final void jdoSetciudadNacimiento(Elegible paramElegible, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.ciudadNacimiento = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 5, paramElegible.ciudadNacimiento, paramCiudad);
  }

  private static final Ciudad jdoGetciudadResidencia(Elegible paramElegible)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.ciudadResidencia;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 6))
      return paramElegible.ciudadResidencia;
    return (Ciudad)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 6, paramElegible.ciudadResidencia);
  }

  private static final void jdoSetciudadResidencia(Elegible paramElegible, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.ciudadResidencia = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 6, paramElegible.ciudadResidencia, paramCiudad);
  }

  private static final String jdoGetdiestralidad(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.diestralidad;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.diestralidad;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 7))
      return paramElegible.diestralidad;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 7, paramElegible.diestralidad);
  }

  private static final void jdoSetdiestralidad(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.diestralidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.diestralidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 7, paramElegible.diestralidad, paramString);
  }

  private static final String jdoGetdireccionResidencia(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.direccionResidencia;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.direccionResidencia;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 8))
      return paramElegible.direccionResidencia;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 8, paramElegible.direccionResidencia);
  }

  private static final void jdoSetdireccionResidencia(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.direccionResidencia = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.direccionResidencia = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 8, paramElegible.direccionResidencia, paramString);
  }

  private static final String jdoGetdisponibilidad(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.disponibilidad;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.disponibilidad;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 9))
      return paramElegible.disponibilidad;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 9, paramElegible.disponibilidad);
  }

  private static final void jdoSetdisponibilidad(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.disponibilidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.disponibilidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 9, paramElegible.disponibilidad, paramString);
  }

  private static final String jdoGetdobleNacionalidad(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.dobleNacionalidad;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.dobleNacionalidad;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 10))
      return paramElegible.dobleNacionalidad;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 10, paramElegible.dobleNacionalidad);
  }

  private static final void jdoSetdobleNacionalidad(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.dobleNacionalidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.dobleNacionalidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 10, paramElegible.dobleNacionalidad, paramString);
  }

  private static final String jdoGetemail(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.email;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.email;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 11))
      return paramElegible.email;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 11, paramElegible.email);
  }

  private static final void jdoSetemail(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.email = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.email = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 11, paramElegible.email, paramString);
  }

  private static final String jdoGetestadoCivil(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.estadoCivil;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.estadoCivil;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 12))
      return paramElegible.estadoCivil;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 12, paramElegible.estadoCivil);
  }

  private static final void jdoSetestadoCivil(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.estadoCivil = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.estadoCivil = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 12, paramElegible.estadoCivil, paramString);
  }

  private static final double jdoGetestatura(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.estatura;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.estatura;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 13))
      return paramElegible.estatura;
    return localStateManager.getDoubleField(paramElegible, jdoInheritedFieldCount + 13, paramElegible.estatura);
  }

  private static final void jdoSetestatura(Elegible paramElegible, double paramDouble)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.estatura = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.estatura = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegible, jdoInheritedFieldCount + 13, paramElegible.estatura, paramDouble);
  }

  private static final Date jdoGetfechaNacimiento(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.fechaNacimiento;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.fechaNacimiento;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 14))
      return paramElegible.fechaNacimiento;
    return (Date)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 14, paramElegible.fechaNacimiento);
  }

  private static final void jdoSetfechaNacimiento(Elegible paramElegible, Date paramDate)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.fechaNacimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.fechaNacimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 14, paramElegible.fechaNacimiento, paramDate);
  }

  private static final Date jdoGetfechaNacionalizacion(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.fechaNacionalizacion;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.fechaNacionalizacion;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 15))
      return paramElegible.fechaNacionalizacion;
    return (Date)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 15, paramElegible.fechaNacionalizacion);
  }

  private static final void jdoSetfechaNacionalizacion(Elegible paramElegible, Date paramDate)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.fechaNacionalizacion = paramDate;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.fechaNacionalizacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 15, paramElegible.fechaNacionalizacion, paramDate);
  }

  private static final Date jdoGetfechaRegistro(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.fechaRegistro;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.fechaRegistro;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 16))
      return paramElegible.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 16, paramElegible.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(Elegible paramElegible, Date paramDate)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 16, paramElegible.fechaRegistro, paramDate);
  }

  private static final String jdoGetgacetaNacionalizacion(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.gacetaNacionalizacion;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.gacetaNacionalizacion;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 17))
      return paramElegible.gacetaNacionalizacion;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 17, paramElegible.gacetaNacionalizacion);
  }

  private static final void jdoSetgacetaNacionalizacion(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.gacetaNacionalizacion = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.gacetaNacionalizacion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 17, paramElegible.gacetaNacionalizacion, paramString);
  }

  private static final int jdoGetgradoLicencia(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.gradoLicencia;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.gradoLicencia;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 18))
      return paramElegible.gradoLicencia;
    return localStateManager.getIntField(paramElegible, jdoInheritedFieldCount + 18, paramElegible.gradoLicencia);
  }

  private static final void jdoSetgradoLicencia(Elegible paramElegible, int paramInt)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.gradoLicencia = paramInt;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.gradoLicencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegible, jdoInheritedFieldCount + 18, paramElegible.gradoLicencia, paramInt);
  }

  private static final String jdoGetgrupoSanguineo(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.grupoSanguineo;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.grupoSanguineo;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 19))
      return paramElegible.grupoSanguineo;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 19, paramElegible.grupoSanguineo);
  }

  private static final void jdoSetgrupoSanguineo(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.grupoSanguineo = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.grupoSanguineo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 19, paramElegible.grupoSanguineo, paramString);
  }

  private static final long jdoGetidElegible(Elegible paramElegible)
  {
    return paramElegible.idElegible;
  }

  private static final void jdoSetidElegible(Elegible paramElegible, long paramLong)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.idElegible = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegible, jdoInheritedFieldCount + 20, paramElegible.idElegible, paramLong);
  }

  private static final int jdoGetidSitp(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.idSitp;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.idSitp;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 21))
      return paramElegible.idSitp;
    return localStateManager.getIntField(paramElegible, jdoInheritedFieldCount + 21, paramElegible.idSitp);
  }

  private static final void jdoSetidSitp(Elegible paramElegible, int paramInt)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegible, jdoInheritedFieldCount + 21, paramElegible.idSitp, paramInt);
  }

  private static final String jdoGetmaneja(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.maneja;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.maneja;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 22))
      return paramElegible.maneja;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 22, paramElegible.maneja);
  }

  private static final void jdoSetmaneja(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.maneja = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.maneja = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 22, paramElegible.maneja, paramString);
  }

  private static final String jdoGetmarcaVehiculo(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.marcaVehiculo;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.marcaVehiculo;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 23))
      return paramElegible.marcaVehiculo;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 23, paramElegible.marcaVehiculo);
  }

  private static final void jdoSetmarcaVehiculo(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.marcaVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.marcaVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 23, paramElegible.marcaVehiculo, paramString);
  }

  private static final String jdoGetmismoOrganismoConyugue(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.mismoOrganismoConyugue;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.mismoOrganismoConyugue;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 24))
      return paramElegible.mismoOrganismoConyugue;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 24, paramElegible.mismoOrganismoConyugue);
  }

  private static final void jdoSetmismoOrganismoConyugue(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.mismoOrganismoConyugue = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.mismoOrganismoConyugue = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 24, paramElegible.mismoOrganismoConyugue, paramString);
  }

  private static final String jdoGetmodeloVehiculo(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.modeloVehiculo;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.modeloVehiculo;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 25))
      return paramElegible.modeloVehiculo;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 25, paramElegible.modeloVehiculo);
  }

  private static final void jdoSetmodeloVehiculo(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.modeloVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.modeloVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 25, paramElegible.modeloVehiculo, paramString);
  }

  private static final String jdoGetmotivoRetiro(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.motivoRetiro;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.motivoRetiro;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 26))
      return paramElegible.motivoRetiro;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 26, paramElegible.motivoRetiro);
  }

  private static final void jdoSetmotivoRetiro(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.motivoRetiro = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.motivoRetiro = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 26, paramElegible.motivoRetiro, paramString);
  }

  private static final String jdoGetnacionalidad(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.nacionalidad;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.nacionalidad;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 27))
      return paramElegible.nacionalidad;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 27, paramElegible.nacionalidad);
  }

  private static final void jdoSetnacionalidad(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.nacionalidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.nacionalidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 27, paramElegible.nacionalidad, paramString);
  }

  private static final String jdoGetnacionalizado(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.nacionalizado;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.nacionalizado;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 28))
      return paramElegible.nacionalizado;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 28, paramElegible.nacionalizado);
  }

  private static final void jdoSetnacionalizado(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.nacionalizado = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.nacionalizado = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 28, paramElegible.nacionalizado, paramString);
  }

  private static final String jdoGetnivelEducativo(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.nivelEducativo;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.nivelEducativo;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 29))
      return paramElegible.nivelEducativo;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 29, paramElegible.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 29, paramElegible.nivelEducativo, paramString);
  }

  private static final String jdoGetnombreConyugue(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.nombreConyugue;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.nombreConyugue;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 30))
      return paramElegible.nombreConyugue;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 30, paramElegible.nombreConyugue);
  }

  private static final void jdoSetnombreConyugue(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.nombreConyugue = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.nombreConyugue = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 30, paramElegible.nombreConyugue, paramString);
  }

  private static final String jdoGetnumeroLibretaMilitar(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.numeroLibretaMilitar;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.numeroLibretaMilitar;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 31))
      return paramElegible.numeroLibretaMilitar;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 31, paramElegible.numeroLibretaMilitar);
  }

  private static final void jdoSetnumeroLibretaMilitar(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.numeroLibretaMilitar = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.numeroLibretaMilitar = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 31, paramElegible.numeroLibretaMilitar, paramString);
  }

  private static final String jdoGetnumeroRif(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.numeroRif;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.numeroRif;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 32))
      return paramElegible.numeroRif;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 32, paramElegible.numeroRif);
  }

  private static final void jdoSetnumeroRif(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.numeroRif = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.numeroRif = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 32, paramElegible.numeroRif, paramString);
  }

  private static final String jdoGetnumeroSso(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.numeroSso;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.numeroSso;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 33))
      return paramElegible.numeroSso;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 33, paramElegible.numeroSso);
  }

  private static final void jdoSetnumeroSso(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.numeroSso = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.numeroSso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 33, paramElegible.numeroSso, paramString);
  }

  private static final String jdoGetotraNormativaNac(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.otraNormativaNac;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.otraNormativaNac;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 34))
      return paramElegible.otraNormativaNac;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 34, paramElegible.otraNormativaNac);
  }

  private static final void jdoSetotraNormativaNac(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.otraNormativaNac = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.otraNormativaNac = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 34, paramElegible.otraNormativaNac, paramString);
  }

  private static final Pais jdoGetpaisNacionalidad(Elegible paramElegible)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.paisNacionalidad;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 35))
      return paramElegible.paisNacionalidad;
    return (Pais)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 35, paramElegible.paisNacionalidad);
  }

  private static final void jdoSetpaisNacionalidad(Elegible paramElegible, Pais paramPais)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.paisNacionalidad = paramPais;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 35, paramElegible.paisNacionalidad, paramPais);
  }

  private static final Parroquia jdoGetparroquia(Elegible paramElegible)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.parroquia;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 36))
      return paramElegible.parroquia;
    return (Parroquia)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 36, paramElegible.parroquia);
  }

  private static final void jdoSetparroquia(Elegible paramElegible, Parroquia paramParroquia)
  {
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.parroquia = paramParroquia;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 36, paramElegible.parroquia, paramParroquia);
  }

  private static final double jdoGetpeso(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.peso;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.peso;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 37))
      return paramElegible.peso;
    return localStateManager.getDoubleField(paramElegible, jdoInheritedFieldCount + 37, paramElegible.peso);
  }

  private static final void jdoSetpeso(Elegible paramElegible, double paramDouble)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.peso = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.peso = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegible, jdoInheritedFieldCount + 37, paramElegible.peso, paramDouble);
  }

  private static final String jdoGetplacaVehiculo(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.placaVehiculo;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.placaVehiculo;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 38))
      return paramElegible.placaVehiculo;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 38, paramElegible.placaVehiculo);
  }

  private static final void jdoSetplacaVehiculo(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.placaVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.placaVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 38, paramElegible.placaVehiculo, paramString);
  }

  private static final String jdoGetprimerApellido(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.primerApellido;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.primerApellido;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 39))
      return paramElegible.primerApellido;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 39, paramElegible.primerApellido);
  }

  private static final void jdoSetprimerApellido(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 39, paramElegible.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.primerNombre;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.primerNombre;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 40))
      return paramElegible.primerNombre;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 40, paramElegible.primerNombre);
  }

  private static final void jdoSetprimerNombre(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 40, paramElegible.primerNombre, paramString);
  }

  private static final String jdoGetsectorTrabajoConyugue(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.sectorTrabajoConyugue;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.sectorTrabajoConyugue;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 41))
      return paramElegible.sectorTrabajoConyugue;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 41, paramElegible.sectorTrabajoConyugue);
  }

  private static final void jdoSetsectorTrabajoConyugue(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.sectorTrabajoConyugue = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.sectorTrabajoConyugue = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 41, paramElegible.sectorTrabajoConyugue, paramString);
  }

  private static final String jdoGetsegundoApellido(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.segundoApellido;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.segundoApellido;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 42))
      return paramElegible.segundoApellido;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 42, paramElegible.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 42, paramElegible.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.segundoNombre;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.segundoNombre;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 43))
      return paramElegible.segundoNombre;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 43, paramElegible.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 43, paramElegible.segundoNombre, paramString);
  }

  private static final String jdoGetsexo(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.sexo;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.sexo;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 44))
      return paramElegible.sexo;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 44, paramElegible.sexo);
  }

  private static final void jdoSetsexo(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.sexo = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.sexo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 44, paramElegible.sexo, paramString);
  }

  private static final double jdoGetsueldoActual(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.sueldoActual;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.sueldoActual;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 45))
      return paramElegible.sueldoActual;
    return localStateManager.getDoubleField(paramElegible, jdoInheritedFieldCount + 45, paramElegible.sueldoActual);
  }

  private static final void jdoSetsueldoActual(Elegible paramElegible, double paramDouble)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.sueldoActual = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.sueldoActual = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegible, jdoInheritedFieldCount + 45, paramElegible.sueldoActual, paramDouble);
  }

  private static final double jdoGetsueldoAspira(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.sueldoAspira;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.sueldoAspira;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 46))
      return paramElegible.sueldoAspira;
    return localStateManager.getDoubleField(paramElegible, jdoInheritedFieldCount + 46, paramElegible.sueldoAspira);
  }

  private static final void jdoSetsueldoAspira(Elegible paramElegible, double paramDouble)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.sueldoAspira = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.sueldoAspira = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegible, jdoInheritedFieldCount + 46, paramElegible.sueldoAspira, paramDouble);
  }

  private static final String jdoGettelefonoCelular(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.telefonoCelular;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.telefonoCelular;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 47))
      return paramElegible.telefonoCelular;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 47, paramElegible.telefonoCelular);
  }

  private static final void jdoSettelefonoCelular(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.telefonoCelular = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.telefonoCelular = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 47, paramElegible.telefonoCelular, paramString);
  }

  private static final String jdoGettelefonoOficina(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.telefonoOficina;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.telefonoOficina;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 48))
      return paramElegible.telefonoOficina;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 48, paramElegible.telefonoOficina);
  }

  private static final void jdoSettelefonoOficina(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.telefonoOficina = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.telefonoOficina = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 48, paramElegible.telefonoOficina, paramString);
  }

  private static final String jdoGettelefonoResidencia(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.telefonoResidencia;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.telefonoResidencia;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 49))
      return paramElegible.telefonoResidencia;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 49, paramElegible.telefonoResidencia);
  }

  private static final void jdoSettelefonoResidencia(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.telefonoResidencia = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.telefonoResidencia = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 49, paramElegible.telefonoResidencia, paramString);
  }

  private static final String jdoGettenenciaVivienda(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.tenenciaVivienda;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.tenenciaVivienda;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 50))
      return paramElegible.tenenciaVivienda;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 50, paramElegible.tenenciaVivienda);
  }

  private static final void jdoSettenenciaVivienda(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.tenenciaVivienda = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.tenenciaVivienda = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 50, paramElegible.tenenciaVivienda, paramString);
  }

  private static final Date jdoGettiempoSitp(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.tiempoSitp;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.tiempoSitp;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 51))
      return paramElegible.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegible, jdoInheritedFieldCount + 51, paramElegible.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(Elegible paramElegible, Date paramDate)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegible, jdoInheritedFieldCount + 51, paramElegible.tiempoSitp, paramDate);
  }

  private static final String jdoGettieneHijos(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.tieneHijos;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.tieneHijos;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 52))
      return paramElegible.tieneHijos;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 52, paramElegible.tieneHijos);
  }

  private static final void jdoSettieneHijos(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.tieneHijos = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.tieneHijos = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 52, paramElegible.tieneHijos, paramString);
  }

  private static final String jdoGettieneVehiculo(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.tieneVehiculo;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.tieneVehiculo;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 53))
      return paramElegible.tieneVehiculo;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 53, paramElegible.tieneVehiculo);
  }

  private static final void jdoSettieneVehiculo(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.tieneVehiculo = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.tieneVehiculo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 53, paramElegible.tieneVehiculo, paramString);
  }

  private static final String jdoGettipoVivienda(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.tipoVivienda;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.tipoVivienda;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 54))
      return paramElegible.tipoVivienda;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 54, paramElegible.tipoVivienda);
  }

  private static final void jdoSettipoVivienda(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.tipoVivienda = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.tipoVivienda = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 54, paramElegible.tipoVivienda, paramString);
  }

  private static final String jdoGettrabajandoActualmente(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.trabajandoActualmente;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.trabajandoActualmente;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 55))
      return paramElegible.trabajandoActualmente;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 55, paramElegible.trabajandoActualmente);
  }

  private static final void jdoSettrabajandoActualmente(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.trabajandoActualmente = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.trabajandoActualmente = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 55, paramElegible.trabajandoActualmente, paramString);
  }

  private static final String jdoGettrabajoActual(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.trabajoActual;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.trabajoActual;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 56))
      return paramElegible.trabajoActual;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 56, paramElegible.trabajoActual);
  }

  private static final void jdoSettrabajoActual(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.trabajoActual = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.trabajoActual = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 56, paramElegible.trabajoActual, paramString);
  }

  private static final String jdoGetzonaPostalResidencia(Elegible paramElegible)
  {
    if (paramElegible.jdoFlags <= 0)
      return paramElegible.zonaPostalResidencia;
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
      return paramElegible.zonaPostalResidencia;
    if (localStateManager.isLoaded(paramElegible, jdoInheritedFieldCount + 57))
      return paramElegible.zonaPostalResidencia;
    return localStateManager.getStringField(paramElegible, jdoInheritedFieldCount + 57, paramElegible.zonaPostalResidencia);
  }

  private static final void jdoSetzonaPostalResidencia(Elegible paramElegible, String paramString)
  {
    if (paramElegible.jdoFlags == 0)
    {
      paramElegible.zonaPostalResidencia = paramString;
      return;
    }
    StateManager localStateManager = paramElegible.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegible.zonaPostalResidencia = paramString;
      return;
    }
    localStateManager.setStringField(paramElegible, jdoInheritedFieldCount + 57, paramElegible.zonaPostalResidencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}