package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegiblePK
  implements Serializable
{
  public long idElegible;

  public ElegiblePK()
  {
  }

  public ElegiblePK(long idElegible)
  {
    this.idElegible = idElegible;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegiblePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegiblePK thatPK)
  {
    return 
      this.idElegible == thatPK.idElegible;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegible)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegible);
  }
}