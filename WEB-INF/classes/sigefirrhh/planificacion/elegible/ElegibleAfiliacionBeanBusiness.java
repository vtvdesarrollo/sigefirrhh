package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Gremio;
import sigefirrhh.base.personal.GremioBeanBusiness;

public class ElegibleAfiliacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleAfiliacion elegibleAfiliacionNew = 
      (ElegibleAfiliacion)BeanUtils.cloneBean(
      elegibleAfiliacion);

    GremioBeanBusiness gremioBeanBusiness = new GremioBeanBusiness();

    if (elegibleAfiliacionNew.getGremio() != null) {
      elegibleAfiliacionNew.setGremio(
        gremioBeanBusiness.findGremioById(
        elegibleAfiliacionNew.getGremio().getIdGremio()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleAfiliacionNew.getElegible() != null) {
      elegibleAfiliacionNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleAfiliacionNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleAfiliacionNew);
  }

  public void updateElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion) throws Exception
  {
    ElegibleAfiliacion elegibleAfiliacionModify = 
      findElegibleAfiliacionById(elegibleAfiliacion.getIdElegibleAfiliacion());

    GremioBeanBusiness gremioBeanBusiness = new GremioBeanBusiness();

    if (elegibleAfiliacion.getGremio() != null) {
      elegibleAfiliacion.setGremio(
        gremioBeanBusiness.findGremioById(
        elegibleAfiliacion.getGremio().getIdGremio()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleAfiliacion.getElegible() != null) {
      elegibleAfiliacion.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleAfiliacion.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleAfiliacionModify, elegibleAfiliacion);
  }

  public void deleteElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleAfiliacion elegibleAfiliacionDelete = 
      findElegibleAfiliacionById(elegibleAfiliacion.getIdElegibleAfiliacion());
    pm.deletePersistent(elegibleAfiliacionDelete);
  }

  public ElegibleAfiliacion findElegibleAfiliacionById(long idElegibleAfiliacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleAfiliacion == pIdElegibleAfiliacion";
    Query query = pm.newQuery(ElegibleAfiliacion.class, filter);

    query.declareParameters("long pIdElegibleAfiliacion");

    parameters.put("pIdElegibleAfiliacion", new Long(idElegibleAfiliacion));

    Collection colElegibleAfiliacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleAfiliacion.iterator();
    return (ElegibleAfiliacion)iterator.next();
  }

  public Collection findElegibleAfiliacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleAfiliacionExtent = pm.getExtent(
      ElegibleAfiliacion.class, true);
    Query query = pm.newQuery(elegibleAfiliacionExtent);
    query.setOrdering("gremio.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleAfiliacion.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("gremio.nombre ascending");

    Collection colElegibleAfiliacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleAfiliacion);

    return colElegibleAfiliacion;
  }
}