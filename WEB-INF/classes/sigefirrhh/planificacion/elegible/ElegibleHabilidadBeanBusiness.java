package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoHabilidad;
import sigefirrhh.base.personal.TipoHabilidadBeanBusiness;

public class ElegibleHabilidadBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleHabilidad(ElegibleHabilidad elegibleHabilidad)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleHabilidad elegibleHabilidadNew = 
      (ElegibleHabilidad)BeanUtils.cloneBean(
      elegibleHabilidad);

    TipoHabilidadBeanBusiness tipoHabilidadBeanBusiness = new TipoHabilidadBeanBusiness();

    if (elegibleHabilidadNew.getTipoHabilidad() != null) {
      elegibleHabilidadNew.setTipoHabilidad(
        tipoHabilidadBeanBusiness.findTipoHabilidadById(
        elegibleHabilidadNew.getTipoHabilidad().getIdTipoHabilidad()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleHabilidadNew.getElegible() != null) {
      elegibleHabilidadNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleHabilidadNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleHabilidadNew);
  }

  public void updateElegibleHabilidad(ElegibleHabilidad elegibleHabilidad) throws Exception
  {
    ElegibleHabilidad elegibleHabilidadModify = 
      findElegibleHabilidadById(elegibleHabilidad.getIdElegibleHabilidad());

    TipoHabilidadBeanBusiness tipoHabilidadBeanBusiness = new TipoHabilidadBeanBusiness();

    if (elegibleHabilidad.getTipoHabilidad() != null) {
      elegibleHabilidad.setTipoHabilidad(
        tipoHabilidadBeanBusiness.findTipoHabilidadById(
        elegibleHabilidad.getTipoHabilidad().getIdTipoHabilidad()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleHabilidad.getElegible() != null) {
      elegibleHabilidad.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleHabilidad.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleHabilidadModify, elegibleHabilidad);
  }

  public void deleteElegibleHabilidad(ElegibleHabilidad elegibleHabilidad) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleHabilidad elegibleHabilidadDelete = 
      findElegibleHabilidadById(elegibleHabilidad.getIdElegibleHabilidad());
    pm.deletePersistent(elegibleHabilidadDelete);
  }

  public ElegibleHabilidad findElegibleHabilidadById(long idElegibleHabilidad) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleHabilidad == pIdElegibleHabilidad";
    Query query = pm.newQuery(ElegibleHabilidad.class, filter);

    query.declareParameters("long pIdElegibleHabilidad");

    parameters.put("pIdElegibleHabilidad", new Long(idElegibleHabilidad));

    Collection colElegibleHabilidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleHabilidad.iterator();
    return (ElegibleHabilidad)iterator.next();
  }

  public Collection findElegibleHabilidadAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleHabilidadExtent = pm.getExtent(
      ElegibleHabilidad.class, true);
    Query query = pm.newQuery(elegibleHabilidadExtent);
    query.setOrdering("tipoHabilidad.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleHabilidad.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("tipoHabilidad.descripcion ascending");

    Collection colElegibleHabilidad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleHabilidad);

    return colElegibleHabilidad;
  }
}