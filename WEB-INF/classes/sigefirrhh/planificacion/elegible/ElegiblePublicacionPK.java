package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegiblePublicacionPK
  implements Serializable
{
  public long idElegiblePublicacion;

  public ElegiblePublicacionPK()
  {
  }

  public ElegiblePublicacionPK(long idElegiblePublicacion)
  {
    this.idElegiblePublicacion = idElegiblePublicacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegiblePublicacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegiblePublicacionPK thatPK)
  {
    return 
      this.idElegiblePublicacion == thatPK.idElegiblePublicacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegiblePublicacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegiblePublicacion);
  }
}