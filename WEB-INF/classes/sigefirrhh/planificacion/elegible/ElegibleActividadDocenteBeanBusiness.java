package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.CarreraBeanBusiness;

public class ElegibleActividadDocenteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleActividadDocente elegibleActividadDocenteNew = 
      (ElegibleActividadDocente)BeanUtils.cloneBean(
      elegibleActividadDocente);

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (elegibleActividadDocenteNew.getCarrera() != null) {
      elegibleActividadDocenteNew.setCarrera(
        carreraBeanBusiness.findCarreraById(
        elegibleActividadDocenteNew.getCarrera().getIdCarrera()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleActividadDocenteNew.getElegible() != null) {
      elegibleActividadDocenteNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleActividadDocenteNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleActividadDocenteNew);
  }

  public void updateElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente) throws Exception
  {
    ElegibleActividadDocente elegibleActividadDocenteModify = 
      findElegibleActividadDocenteById(elegibleActividadDocente.getIdElegibleActividadDocente());

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (elegibleActividadDocente.getCarrera() != null) {
      elegibleActividadDocente.setCarrera(
        carreraBeanBusiness.findCarreraById(
        elegibleActividadDocente.getCarrera().getIdCarrera()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleActividadDocente.getElegible() != null) {
      elegibleActividadDocente.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleActividadDocente.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleActividadDocenteModify, elegibleActividadDocente);
  }

  public void deleteElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleActividadDocente elegibleActividadDocenteDelete = 
      findElegibleActividadDocenteById(elegibleActividadDocente.getIdElegibleActividadDocente());
    pm.deletePersistent(elegibleActividadDocenteDelete);
  }

  public ElegibleActividadDocente findElegibleActividadDocenteById(long idElegibleActividadDocente) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleActividadDocente == pIdElegibleActividadDocente";
    Query query = pm.newQuery(ElegibleActividadDocente.class, filter);

    query.declareParameters("long pIdElegibleActividadDocente");

    parameters.put("pIdElegibleActividadDocente", new Long(idElegibleActividadDocente));

    Collection colElegibleActividadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleActividadDocente.iterator();
    return (ElegibleActividadDocente)iterator.next();
  }

  public Collection findElegibleActividadDocenteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleActividadDocenteExtent = pm.getExtent(
      ElegibleActividadDocente.class, true);
    Query query = pm.newQuery(elegibleActividadDocenteExtent);
    query.setOrdering("anioInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleActividadDocente.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("anioInicio ascending");

    Collection colElegibleActividadDocente = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleActividadDocente);

    return colElegibleActividadDocente;
  }
}