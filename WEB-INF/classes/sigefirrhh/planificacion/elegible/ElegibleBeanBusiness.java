package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.EstablecimientoSaludBeanBusiness;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;
import sigefirrhh.base.ubicacion.Pais;
import sigefirrhh.base.ubicacion.PaisBeanBusiness;
import sigefirrhh.base.ubicacion.Parroquia;
import sigefirrhh.base.ubicacion.ParroquiaBeanBusiness;

public class ElegibleBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegible(Elegible elegible, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Elegible elegibleNew = 
      (Elegible)BeanUtils.cloneBean(
      elegible);

    CiudadBeanBusiness ciudadNacimientoBeanBusiness = new CiudadBeanBusiness();

    if (elegibleNew.getCiudadNacimiento() != null) {
      elegibleNew.setCiudadNacimiento(
        ciudadNacimientoBeanBusiness.findCiudadById(
        elegibleNew.getCiudadNacimiento().getIdCiudad()));
    }

    PaisBeanBusiness paisNacionalidadBeanBusiness = new PaisBeanBusiness();

    if (elegibleNew.getPaisNacionalidad() != null) {
      elegibleNew.setPaisNacionalidad(
        paisNacionalidadBeanBusiness.findPaisById(
        elegibleNew.getPaisNacionalidad().getIdPais()));
    }

    CiudadBeanBusiness ciudadResidenciaBeanBusiness = new CiudadBeanBusiness();

    if (elegibleNew.getCiudadResidencia() != null) {
      elegibleNew.setCiudadResidencia(
        ciudadResidenciaBeanBusiness.findCiudadById(
        elegibleNew.getCiudadResidencia().getIdCiudad()));
    }

    ParroquiaBeanBusiness parroquiaBeanBusiness = new ParroquiaBeanBusiness();

    if (elegibleNew.getParroquia() != null) {
      elegibleNew.setParroquia(
        parroquiaBeanBusiness.findParroquiaById(
        elegibleNew.getParroquia().getIdParroquia()));
    }

    pm.makePersistent(elegibleNew);

    ElegibleOrganismo elegibleOrganismo = new ElegibleOrganismo();

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();
    Organismo organismonew = organismoBeanBusiness.findOrganismoById(idOrganismo);

    elegibleOrganismo.setElegible(elegibleNew);
    elegibleOrganismo.setOrganismo(organismonew);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    elegibleOrganismo.setIdElegibleOrganismo(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.elegible.ElegibleOrganismo"));

    pm.makePersistent(elegibleOrganismo);
  }

  public void updateElegible(Elegible elegible) throws Exception
  {
    Elegible elegibleModify = 
      findElegibleById(elegible.getIdElegible());

    CiudadBeanBusiness ciudadNacimientoBeanBusiness = new CiudadBeanBusiness();

    if (elegible.getCiudadNacimiento() != null) {
      elegible.setCiudadNacimiento(
        ciudadNacimientoBeanBusiness.findCiudadById(
        elegible.getCiudadNacimiento().getIdCiudad()));
    }

    PaisBeanBusiness paisNacionalidadBeanBusiness = new PaisBeanBusiness();

    if (elegible.getPaisNacionalidad() != null) {
      elegible.setPaisNacionalidad(
        paisNacionalidadBeanBusiness.findPaisById(
        elegible.getPaisNacionalidad().getIdPais()));
    }

    CiudadBeanBusiness ciudadResidenciaBeanBusiness = new CiudadBeanBusiness();

    if (elegible.getCiudadResidencia() != null) {
      elegible.setCiudadResidencia(
        ciudadResidenciaBeanBusiness.findCiudadById(
        elegible.getCiudadResidencia().getIdCiudad()));
    }

    ParroquiaBeanBusiness parroquiaBeanBusiness = new ParroquiaBeanBusiness();

    if (elegible.getParroquia() != null) {
      elegible.setParroquia(
        parroquiaBeanBusiness.findParroquiaById(
        elegible.getParroquia().getIdParroquia()));
    }

    EstablecimientoSaludBeanBusiness establecimientoSaludBeanBusiness = new EstablecimientoSaludBeanBusiness();

    BeanUtils.copyProperties(elegibleModify, elegible);
  }

  public void deleteElegible(Elegible elegible, long idOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Elegible elegibleDelete = 
      findElegibleById(elegible.getIdElegible());

    pm.deletePersistent(elegibleDelete);
  }

  public Elegible findElegibleById(long idElegible)
    throws Exception
  {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegible == pIdElegible";
    Query query = pm.newQuery(Elegible.class, filter);

    query.declareParameters("long pIdElegible");

    parameters.put("pIdElegible", new Long(idElegible));

    Collection colElegible = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegible.iterator();
    return (Elegible)iterator.next();
  }

  public Collection findElegibleAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleExtent = pm.getExtent(
      Elegible.class, true);
    Query query = pm.newQuery(elegibleExtent);
    query.setOrdering("primerApellido ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCedula(int cedula)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cedula == pCedula";

    Query query = pm.newQuery(Elegible.class, filter);

    query.declareParameters("int pCedula");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));

    query.setOrdering("primerApellido ascending");

    Collection colElegible = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegible);

    return colElegible;
  }

  public Collection findByPrimerApellido(String primerApellido)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primerApellido.startsWith(pPrimerApellido)";

    Query query = pm.newQuery(Elegible.class, filter);

    query.declareParameters("java.lang.String pPrimerApellido");
    HashMap parameters = new HashMap();

    parameters.put("pPrimerApellido", new String(primerApellido));

    query.setOrdering("primerApellido ascending");

    Collection colElegible = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegible);

    return colElegible;
  }

  public Collection findByPrimerNombre(String primerNombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primerNombre.startsWith(pPrimerNombre)";

    Query query = pm.newQuery(Elegible.class, filter);

    query.declareParameters("java.lang.String pPrimerNombre");
    HashMap parameters = new HashMap();

    parameters.put("pPrimerNombre", new String(primerNombre));

    query.setOrdering("primerApellido ascending");

    Collection colElegible = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegible);

    return colElegible;
  }

  public Collection findByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    StringBuffer filter = new StringBuffer();
    if (primerNombre != null) {
      filter.append("primerNombre.startsWith(pPrimerNombre) && ");
    }
    if (segundoNombre != null) {
      filter.append("segundoNombre.startsWith(pSegundoNombre) && ");
    }
    if (primerApellido != null) {
      filter.append("primerApellido.startsWith(pPrimerApellido) && ");
    }
    if (segundoApellido != null) {
      filter.append("segundoApellido.startsWith(pSegundoApellido) && ");
    }
    filter.append("po.elegible.idElegible == idElegible && po.organismo.idOrganismo == pIdOrganismo");

    Query query = pm.newQuery(Elegible.class, filter.toString());

    StringBuffer declare = new StringBuffer();

    if (primerNombre != null) {
      declare.append("String pPrimerNombre, ");
    }
    if (segundoNombre != null) {
      declare.append("String pSegundoNombre, ");
    }
    if (primerApellido != null) {
      declare.append("String pPrimerApellido, ");
    }
    if (segundoApellido != null) {
      declare.append("String pSegundoApellido, ");
    }
    declare.append("long pIdOrganismo");

    query.declareImports("import sigefirrhh.planificacion.elegible.ElegibleOrganismo");
    query.declareVariables("ElegibleOrganismo po");

    query.declareParameters(declare.toString());
    query.setOrdering("primerApellido ascending, primerNombre ascending");
    HashMap parameters = new HashMap();

    if (primerNombre != null) {
      parameters.put("pPrimerNombre", new String(primerNombre));
    }
    if (segundoNombre != null) {
      parameters.put("pSegundoNombre", new String(segundoNombre));
    }
    if (primerApellido != null) {
      parameters.put("pPrimerApellido", new String(primerApellido));
    }
    if (segundoApellido != null) {
      parameters.put("pSegundoApellido", new String(segundoApellido));
    }
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    Collection colElegible = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegible);

    return colElegible;
  }

  public Collection findByCedula(int cedula, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    String filter = "cedula == pCedula && po.elegible.idElegible == idElegible && po.organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Elegible.class, filter);
    query.declareImports("import sigefirrhh.planificacion.elegible.ElegibleOrganismo");
    query.declareVariables("ElegibleOrganismo po");

    query.declareParameters("int pCedula, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("primerApellido ascending");

    Collection colElegible = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegible);

    return colElegible;
  }
}