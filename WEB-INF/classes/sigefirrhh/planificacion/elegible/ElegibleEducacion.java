package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.Titulo;
import sigefirrhh.base.ubicacion.Ciudad;

public class ElegibleEducacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_REGISTRO_TITULO;
  protected static final Map LISTA_BECADO;
  protected static final Map LISTA_SECTOR;
  protected static final Map LISTA_REEMBOLSO;
  private long idElegibleEducacion;
  private NivelEducativo nivelEducativo;
  private int anioInicio;
  private int anioFin;
  private String estatus;
  private Carrera carrera;
  private Titulo titulo;
  private String registroTitulo;
  private Date fechaRegistro;
  private String nombreEntidad;
  private String nombrePostgrado;
  private Ciudad ciudad;
  private String sector;
  private String escala;
  private String calificacion;
  private String mencion;
  private String becado;
  private String organizacionBecaria;
  private String reembolso;
  private String observaciones;
  private int aniosExperiencia;
  private int mesesExperiencia;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anioFin", "anioInicio", "aniosExperiencia", "becado", "calificacion", "carrera", "ciudad", "elegible", "escala", "estatus", "fechaRegistro", "idElegibleEducacion", "idSitp", "mencion", "mesesExperiencia", "nivelEducativo", "nombreEntidad", "nombrePostgrado", "observaciones", "organizacionBecaria", "reembolso", "registroTitulo", "sector", "tiempoSitp", "titulo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.Carrera"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.NivelEducativo"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.Titulo") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 26, 26, 26, 21, 21, 21, 24, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleEducacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleEducacion());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_REGISTRO_TITULO = 
      new LinkedHashMap();
    LISTA_BECADO = 
      new LinkedHashMap();
    LISTA_SECTOR = 
      new LinkedHashMap();
    LISTA_REEMBOLSO = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("E", "ESTUDIANDO");
    LISTA_ESTATUS.put("F", "FINALIZO");
    LISTA_ESTATUS.put("N", "NO TERMINO");
    LISTA_REGISTRO_TITULO.put("S", "SI");
    LISTA_REGISTRO_TITULO.put("N", "NO");
    LISTA_BECADO.put("N", "NO");
    LISTA_BECADO.put("S", "SI");
    LISTA_SECTOR.put("P", "PRIVADO");
    LISTA_SECTOR.put("U", "PUBLICO");
    LISTA_SECTOR.put("E", "EXTERIOR");
    LISTA_REEMBOLSO.put("S", "SI");
    LISTA_REEMBOLSO.put("N", "NO");
  }

  public ElegibleEducacion()
  {
    jdoSetestatus(this, "F");

    jdoSetregistroTitulo(this, "S");

    jdoSetsector(this, "P");

    jdoSetbecado(this, "N");

    jdoSetreembolso(this, "N");

    jdoSetaniosExperiencia(this, 0);

    jdoSetmesesExperiencia(this, 0);
  }

  public String toString()
  {
    if (jdoGetcarrera(this) != null) {
      return jdoGetnivelEducativo(this).getDescripcion() + " - " + 
        LISTA_ESTATUS.get(jdoGetestatus(this)) + " - " + 
        jdoGetanioInicio(this) + " - " + 
        jdoGetanioFin(this) + " - " + 
        jdoGetcarrera(this).getNombre();
    }
    return jdoGetnivelEducativo(this).getDescripcion() + " - " + 
      LISTA_ESTATUS.get(jdoGetestatus(this)) + " - " + 
      jdoGetanioInicio(this) + " - " + 
      jdoGetanioFin(this);
  }

  public int getAnioFin()
  {
    return jdoGetanioFin(this);
  }

  public void setAnioFin(int anioFin)
  {
    jdoSetanioFin(this, anioFin);
  }

  public int getAnioInicio()
  {
    return jdoGetanioInicio(this);
  }

  public void setAnioInicio(int anioInicio)
  {
    jdoSetanioInicio(this, anioInicio);
  }

  public int getAniosExperiencia()
  {
    return jdoGetaniosExperiencia(this);
  }

  public void setAniosExperiencia(int aniosExperiencia)
  {
    jdoSetaniosExperiencia(this, aniosExperiencia);
  }

  public String getBecado()
  {
    return jdoGetbecado(this);
  }

  public void setBecado(String becado)
  {
    jdoSetbecado(this, becado);
  }

  public Carrera getCarrera()
  {
    return jdoGetcarrera(this);
  }

  public void setCarrera(Carrera carrera)
  {
    jdoSetcarrera(this, carrera);
  }

  public Ciudad getCiudad()
  {
    return jdoGetciudad(this);
  }

  public void setCiudad(Ciudad ciudad)
  {
    jdoSetciudad(this, ciudad);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus)
  {
    jdoSetestatus(this, estatus);
  }

  public Date getFechaRegistro()
  {
    return jdoGetfechaRegistro(this);
  }

  public void setFechaRegistro(Date fechaRegistro)
  {
    jdoSetfechaRegistro(this, fechaRegistro);
  }

  public long getIdElegibleEducacion()
  {
    return jdoGetidElegibleEducacion(this);
  }

  public void setIdElegibleEducacion(long idElegibleEducacion)
  {
    jdoSetidElegibleEducacion(this, idElegibleEducacion);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public int getMesesExperiencia()
  {
    return jdoGetmesesExperiencia(this);
  }

  public void setMesesExperiencia(int mesesExperiencia)
  {
    jdoSetmesesExperiencia(this, mesesExperiencia);
  }

  public NivelEducativo getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public void setNivelEducativo(NivelEducativo nivelEducativo)
  {
    jdoSetnivelEducativo(this, nivelEducativo);
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public void setNombreEntidad(String nombreEntidad)
  {
    jdoSetnombreEntidad(this, nombreEntidad);
  }

  public String getNombrePostgrado()
  {
    return jdoGetnombrePostgrado(this);
  }

  public void setNombrePostgrado(String nombrePostgrado)
  {
    jdoSetnombrePostgrado(this, nombrePostgrado);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public String getOrganizacionBecaria()
  {
    return jdoGetorganizacionBecaria(this);
  }

  public void setOrganizacionBecaria(String organizacionBecaria)
  {
    jdoSetorganizacionBecaria(this, organizacionBecaria);
  }

  public String getReembolso()
  {
    return jdoGetreembolso(this);
  }

  public void setReembolso(String reembolso)
  {
    jdoSetreembolso(this, reembolso);
  }

  public String getRegistroTitulo()
  {
    return jdoGetregistroTitulo(this);
  }

  public void setRegistroTitulo(String registroTitulo)
  {
    jdoSetregistroTitulo(this, registroTitulo);
  }

  public String getSector()
  {
    return jdoGetsector(this);
  }

  public void setSector(String sector)
  {
    jdoSetsector(this, sector);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public Titulo getTitulo()
  {
    return jdoGettitulo(this);
  }

  public void setTitulo(Titulo titulo)
  {
    jdoSettitulo(this, titulo);
  }
  public String getCalificacion() {
    return jdoGetcalificacion(this);
  }
  public void setCalificacion(String calificacion) {
    jdoSetcalificacion(this, calificacion);
  }
  public String getEscala() {
    return jdoGetescala(this);
  }
  public void setEscala(String escala) {
    jdoSetescala(this, escala);
  }
  public String getMencion() {
    return jdoGetmencion(this);
  }
  public void setMencion(String mencion) {
    jdoSetmencion(this, mencion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 25;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleEducacion localElegibleEducacion = new ElegibleEducacion();
    localElegibleEducacion.jdoFlags = 1;
    localElegibleEducacion.jdoStateManager = paramStateManager;
    return localElegibleEducacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleEducacion localElegibleEducacion = new ElegibleEducacion();
    localElegibleEducacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleEducacion.jdoFlags = 1;
    localElegibleEducacion.jdoStateManager = paramStateManager;
    return localElegibleEducacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioFin);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioInicio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.aniosExperiencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.becado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.calificacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.carrera);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.escala);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleEducacion);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mencion);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesesExperiencia);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelEducativo);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombrePostgrado);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.organizacionBecaria);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.reembolso);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.registroTitulo);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sector);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.titulo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioFin = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioInicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aniosExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.becado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.calificacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.carrera = ((Carrera)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.escala = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleEducacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mencion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesesExperiencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = ((NivelEducativo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombrePostgrado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organizacionBecaria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reembolso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroTitulo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.titulo = ((Titulo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleEducacion paramElegibleEducacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.anioFin = paramElegibleEducacion.anioFin;
      return;
    case 1:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.anioInicio = paramElegibleEducacion.anioInicio;
      return;
    case 2:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.aniosExperiencia = paramElegibleEducacion.aniosExperiencia;
      return;
    case 3:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.becado = paramElegibleEducacion.becado;
      return;
    case 4:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.calificacion = paramElegibleEducacion.calificacion;
      return;
    case 5:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.carrera = paramElegibleEducacion.carrera;
      return;
    case 6:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramElegibleEducacion.ciudad;
      return;
    case 7:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleEducacion.elegible;
      return;
    case 8:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.escala = paramElegibleEducacion.escala;
      return;
    case 9:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramElegibleEducacion.estatus;
      return;
    case 10:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramElegibleEducacion.fechaRegistro;
      return;
    case 11:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleEducacion = paramElegibleEducacion.idElegibleEducacion;
      return;
    case 12:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleEducacion.idSitp;
      return;
    case 13:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.mencion = paramElegibleEducacion.mencion;
      return;
    case 14:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.mesesExperiencia = paramElegibleEducacion.mesesExperiencia;
      return;
    case 15:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramElegibleEducacion.nivelEducativo;
      return;
    case 16:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramElegibleEducacion.nombreEntidad;
      return;
    case 17:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombrePostgrado = paramElegibleEducacion.nombrePostgrado;
      return;
    case 18:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramElegibleEducacion.observaciones;
      return;
    case 19:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.organizacionBecaria = paramElegibleEducacion.organizacionBecaria;
      return;
    case 20:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.reembolso = paramElegibleEducacion.reembolso;
      return;
    case 21:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.registroTitulo = paramElegibleEducacion.registroTitulo;
      return;
    case 22:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.sector = paramElegibleEducacion.sector;
      return;
    case 23:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleEducacion.tiempoSitp;
      return;
    case 24:
      if (paramElegibleEducacion == null)
        throw new IllegalArgumentException("arg1");
      this.titulo = paramElegibleEducacion.titulo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleEducacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleEducacion localElegibleEducacion = (ElegibleEducacion)paramObject;
    if (localElegibleEducacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleEducacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleEducacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleEducacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleEducacionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleEducacionPK localElegibleEducacionPK = (ElegibleEducacionPK)paramObject;
    localElegibleEducacionPK.idElegibleEducacion = this.idElegibleEducacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleEducacionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleEducacionPK localElegibleEducacionPK = (ElegibleEducacionPK)paramObject;
    this.idElegibleEducacion = localElegibleEducacionPK.idElegibleEducacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleEducacionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleEducacionPK localElegibleEducacionPK = (ElegibleEducacionPK)paramObject;
    localElegibleEducacionPK.idElegibleEducacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 11);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleEducacionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleEducacionPK localElegibleEducacionPK = (ElegibleEducacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 11, localElegibleEducacionPK.idElegibleEducacion);
  }

  private static final int jdoGetanioFin(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.anioFin;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.anioFin;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 0))
      return paramElegibleEducacion.anioFin;
    return localStateManager.getIntField(paramElegibleEducacion, jdoInheritedFieldCount + 0, paramElegibleEducacion.anioFin);
  }

  private static final void jdoSetanioFin(ElegibleEducacion paramElegibleEducacion, int paramInt)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.anioFin = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.anioFin = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEducacion, jdoInheritedFieldCount + 0, paramElegibleEducacion.anioFin, paramInt);
  }

  private static final int jdoGetanioInicio(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.anioInicio;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.anioInicio;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 1))
      return paramElegibleEducacion.anioInicio;
    return localStateManager.getIntField(paramElegibleEducacion, jdoInheritedFieldCount + 1, paramElegibleEducacion.anioInicio);
  }

  private static final void jdoSetanioInicio(ElegibleEducacion paramElegibleEducacion, int paramInt)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.anioInicio = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.anioInicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEducacion, jdoInheritedFieldCount + 1, paramElegibleEducacion.anioInicio, paramInt);
  }

  private static final int jdoGetaniosExperiencia(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.aniosExperiencia;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.aniosExperiencia;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 2))
      return paramElegibleEducacion.aniosExperiencia;
    return localStateManager.getIntField(paramElegibleEducacion, jdoInheritedFieldCount + 2, paramElegibleEducacion.aniosExperiencia);
  }

  private static final void jdoSetaniosExperiencia(ElegibleEducacion paramElegibleEducacion, int paramInt)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.aniosExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.aniosExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEducacion, jdoInheritedFieldCount + 2, paramElegibleEducacion.aniosExperiencia, paramInt);
  }

  private static final String jdoGetbecado(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.becado;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.becado;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 3))
      return paramElegibleEducacion.becado;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 3, paramElegibleEducacion.becado);
  }

  private static final void jdoSetbecado(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.becado = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.becado = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 3, paramElegibleEducacion.becado, paramString);
  }

  private static final String jdoGetcalificacion(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.calificacion;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.calificacion;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 4))
      return paramElegibleEducacion.calificacion;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 4, paramElegibleEducacion.calificacion);
  }

  private static final void jdoSetcalificacion(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.calificacion = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.calificacion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 4, paramElegibleEducacion.calificacion, paramString);
  }

  private static final Carrera jdoGetcarrera(ElegibleEducacion paramElegibleEducacion)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.carrera;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 5))
      return paramElegibleEducacion.carrera;
    return (Carrera)localStateManager.getObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 5, paramElegibleEducacion.carrera);
  }

  private static final void jdoSetcarrera(ElegibleEducacion paramElegibleEducacion, Carrera paramCarrera)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.carrera = paramCarrera;
      return;
    }
    localStateManager.setObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 5, paramElegibleEducacion.carrera, paramCarrera);
  }

  private static final Ciudad jdoGetciudad(ElegibleEducacion paramElegibleEducacion)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.ciudad;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 6))
      return paramElegibleEducacion.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 6, paramElegibleEducacion.ciudad);
  }

  private static final void jdoSetciudad(ElegibleEducacion paramElegibleEducacion, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 6, paramElegibleEducacion.ciudad, paramCiudad);
  }

  private static final Elegible jdoGetelegible(ElegibleEducacion paramElegibleEducacion)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.elegible;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 7))
      return paramElegibleEducacion.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 7, paramElegibleEducacion.elegible);
  }

  private static final void jdoSetelegible(ElegibleEducacion paramElegibleEducacion, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 7, paramElegibleEducacion.elegible, paramElegible);
  }

  private static final String jdoGetescala(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.escala;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.escala;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 8))
      return paramElegibleEducacion.escala;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 8, paramElegibleEducacion.escala);
  }

  private static final void jdoSetescala(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.escala = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.escala = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 8, paramElegibleEducacion.escala, paramString);
  }

  private static final String jdoGetestatus(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.estatus;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.estatus;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 9))
      return paramElegibleEducacion.estatus;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 9, paramElegibleEducacion.estatus);
  }

  private static final void jdoSetestatus(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 9, paramElegibleEducacion.estatus, paramString);
  }

  private static final Date jdoGetfechaRegistro(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.fechaRegistro;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.fechaRegistro;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 10))
      return paramElegibleEducacion.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 10, paramElegibleEducacion.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(ElegibleEducacion paramElegibleEducacion, Date paramDate)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 10, paramElegibleEducacion.fechaRegistro, paramDate);
  }

  private static final long jdoGetidElegibleEducacion(ElegibleEducacion paramElegibleEducacion)
  {
    return paramElegibleEducacion.idElegibleEducacion;
  }

  private static final void jdoSetidElegibleEducacion(ElegibleEducacion paramElegibleEducacion, long paramLong)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.idElegibleEducacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleEducacion, jdoInheritedFieldCount + 11, paramElegibleEducacion.idElegibleEducacion, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.idSitp;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.idSitp;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 12))
      return paramElegibleEducacion.idSitp;
    return localStateManager.getIntField(paramElegibleEducacion, jdoInheritedFieldCount + 12, paramElegibleEducacion.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleEducacion paramElegibleEducacion, int paramInt)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEducacion, jdoInheritedFieldCount + 12, paramElegibleEducacion.idSitp, paramInt);
  }

  private static final String jdoGetmencion(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.mencion;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.mencion;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 13))
      return paramElegibleEducacion.mencion;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 13, paramElegibleEducacion.mencion);
  }

  private static final void jdoSetmencion(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.mencion = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.mencion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 13, paramElegibleEducacion.mencion, paramString);
  }

  private static final int jdoGetmesesExperiencia(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.mesesExperiencia;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.mesesExperiencia;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 14))
      return paramElegibleEducacion.mesesExperiencia;
    return localStateManager.getIntField(paramElegibleEducacion, jdoInheritedFieldCount + 14, paramElegibleEducacion.mesesExperiencia);
  }

  private static final void jdoSetmesesExperiencia(ElegibleEducacion paramElegibleEducacion, int paramInt)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.mesesExperiencia = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.mesesExperiencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleEducacion, jdoInheritedFieldCount + 14, paramElegibleEducacion.mesesExperiencia, paramInt);
  }

  private static final NivelEducativo jdoGetnivelEducativo(ElegibleEducacion paramElegibleEducacion)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.nivelEducativo;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 15))
      return paramElegibleEducacion.nivelEducativo;
    return (NivelEducativo)localStateManager.getObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 15, paramElegibleEducacion.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(ElegibleEducacion paramElegibleEducacion, NivelEducativo paramNivelEducativo)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.nivelEducativo = paramNivelEducativo;
      return;
    }
    localStateManager.setObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 15, paramElegibleEducacion.nivelEducativo, paramNivelEducativo);
  }

  private static final String jdoGetnombreEntidad(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.nombreEntidad;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.nombreEntidad;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 16))
      return paramElegibleEducacion.nombreEntidad;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 16, paramElegibleEducacion.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 16, paramElegibleEducacion.nombreEntidad, paramString);
  }

  private static final String jdoGetnombrePostgrado(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.nombrePostgrado;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.nombrePostgrado;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 17))
      return paramElegibleEducacion.nombrePostgrado;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 17, paramElegibleEducacion.nombrePostgrado);
  }

  private static final void jdoSetnombrePostgrado(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.nombrePostgrado = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.nombrePostgrado = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 17, paramElegibleEducacion.nombrePostgrado, paramString);
  }

  private static final String jdoGetobservaciones(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.observaciones;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.observaciones;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 18))
      return paramElegibleEducacion.observaciones;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 18, paramElegibleEducacion.observaciones);
  }

  private static final void jdoSetobservaciones(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 18, paramElegibleEducacion.observaciones, paramString);
  }

  private static final String jdoGetorganizacionBecaria(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.organizacionBecaria;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.organizacionBecaria;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 19))
      return paramElegibleEducacion.organizacionBecaria;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 19, paramElegibleEducacion.organizacionBecaria);
  }

  private static final void jdoSetorganizacionBecaria(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.organizacionBecaria = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.organizacionBecaria = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 19, paramElegibleEducacion.organizacionBecaria, paramString);
  }

  private static final String jdoGetreembolso(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.reembolso;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.reembolso;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 20))
      return paramElegibleEducacion.reembolso;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 20, paramElegibleEducacion.reembolso);
  }

  private static final void jdoSetreembolso(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.reembolso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.reembolso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 20, paramElegibleEducacion.reembolso, paramString);
  }

  private static final String jdoGetregistroTitulo(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.registroTitulo;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.registroTitulo;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 21))
      return paramElegibleEducacion.registroTitulo;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 21, paramElegibleEducacion.registroTitulo);
  }

  private static final void jdoSetregistroTitulo(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.registroTitulo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.registroTitulo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 21, paramElegibleEducacion.registroTitulo, paramString);
  }

  private static final String jdoGetsector(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.sector;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.sector;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 22))
      return paramElegibleEducacion.sector;
    return localStateManager.getStringField(paramElegibleEducacion, jdoInheritedFieldCount + 22, paramElegibleEducacion.sector);
  }

  private static final void jdoSetsector(ElegibleEducacion paramElegibleEducacion, String paramString)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.sector = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.sector = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleEducacion, jdoInheritedFieldCount + 22, paramElegibleEducacion.sector, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleEducacion paramElegibleEducacion)
  {
    if (paramElegibleEducacion.jdoFlags <= 0)
      return paramElegibleEducacion.tiempoSitp;
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 23))
      return paramElegibleEducacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 23, paramElegibleEducacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleEducacion paramElegibleEducacion, Date paramDate)
  {
    if (paramElegibleEducacion.jdoFlags == 0)
    {
      paramElegibleEducacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 23, paramElegibleEducacion.tiempoSitp, paramDate);
  }

  private static final Titulo jdoGettitulo(ElegibleEducacion paramElegibleEducacion)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleEducacion.titulo;
    if (localStateManager.isLoaded(paramElegibleEducacion, jdoInheritedFieldCount + 24))
      return paramElegibleEducacion.titulo;
    return (Titulo)localStateManager.getObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 24, paramElegibleEducacion.titulo);
  }

  private static final void jdoSettitulo(ElegibleEducacion paramElegibleEducacion, Titulo paramTitulo)
  {
    StateManager localStateManager = paramElegibleEducacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleEducacion.titulo = paramTitulo;
      return;
    }
    localStateManager.setObjectField(paramElegibleEducacion, jdoInheritedFieldCount + 24, paramElegibleEducacion.titulo, paramTitulo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}