package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleEducacionPK
  implements Serializable
{
  public long idElegibleEducacion;

  public ElegibleEducacionPK()
  {
  }

  public ElegibleEducacionPK(long idElegibleEducacion)
  {
    this.idElegibleEducacion = idElegibleEducacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleEducacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleEducacionPK thatPK)
  {
    return 
      this.idElegibleEducacion == thatPK.idElegibleEducacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleEducacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleEducacion);
  }
}