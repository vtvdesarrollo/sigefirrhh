package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ElegibleExperiencia
  implements Serializable, PersistenceCapable
{
  private long idElegibleExperiencia;
  private String nombreInstitucion;
  private Date fechaIngreso;
  private Date fechaEgreso;
  private String cargoIngreso;
  private String cargoEgreso;
  private String jefe;
  private String telefono;
  private String causaRetiro;
  private double ultimoSueldo;
  private String observaciones;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargoEgreso", "cargoIngreso", "causaRetiro", "elegible", "fechaEgreso", "fechaIngreso", "idElegibleExperiencia", "idSitp", "jefe", "nombreInstitucion", "observaciones", "telefono", "tiempoSitp", "ultimoSueldo" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombreInstitucion(this) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaIngreso(this)) + " " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaEgreso(this));
  }

  public String getCargoEgreso()
  {
    return jdoGetcargoEgreso(this);
  }

  public void setCargoEgreso(String cargoEgreso)
  {
    jdoSetcargoEgreso(this, cargoEgreso);
  }

  public String getCargoIngreso()
  {
    return jdoGetcargoIngreso(this);
  }

  public void setCargoIngreso(String cargoIngreso)
  {
    jdoSetcargoIngreso(this, cargoIngreso);
  }

  public String getCausaRetiro()
  {
    return jdoGetcausaRetiro(this);
  }

  public void setCausaRetiro(String causaRetiro)
  {
    jdoSetcausaRetiro(this, causaRetiro);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public Date getFechaEgreso()
  {
    return jdoGetfechaEgreso(this);
  }

  public void setFechaEgreso(Date fechaEgreso)
  {
    jdoSetfechaEgreso(this, fechaEgreso);
  }

  public Date getFechaIngreso()
  {
    return jdoGetfechaIngreso(this);
  }

  public void setFechaIngreso(Date fechaIngreso)
  {
    jdoSetfechaIngreso(this, fechaIngreso);
  }

  public long getIdElegibleExperiencia()
  {
    return jdoGetidElegibleExperiencia(this);
  }

  public void setIdElegibleExperiencia(long idElegibleExperiencia)
  {
    jdoSetidElegibleExperiencia(this, idElegibleExperiencia);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getJefe()
  {
    return jdoGetjefe(this);
  }

  public void setJefe(String jefe)
  {
    jdoSetjefe(this, jefe);
  }

  public String getNombreInstitucion()
  {
    return jdoGetnombreInstitucion(this);
  }

  public void setNombreInstitucion(String nombreInstitucion)
  {
    jdoSetnombreInstitucion(this, nombreInstitucion);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public String getTelefono()
  {
    return jdoGettelefono(this);
  }

  public void setTelefono(String telefono)
  {
    jdoSettelefono(this, telefono);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public double getUltimoSueldo()
  {
    return jdoGetultimoSueldo(this);
  }

  public void setUltimoSueldo(double ultimoSueldo)
  {
    jdoSetultimoSueldo(this, ultimoSueldo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleExperiencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleExperiencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleExperiencia localElegibleExperiencia = new ElegibleExperiencia();
    localElegibleExperiencia.jdoFlags = 1;
    localElegibleExperiencia.jdoStateManager = paramStateManager;
    return localElegibleExperiencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleExperiencia localElegibleExperiencia = new ElegibleExperiencia();
    localElegibleExperiencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleExperiencia.jdoFlags = 1;
    localElegibleExperiencia.jdoStateManager = paramStateManager;
    return localElegibleExperiencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoEgreso);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargoIngreso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.causaRetiro);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaEgreso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaIngreso);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleExperiencia);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.jefe);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreInstitucion);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.ultimoSueldo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoEgreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargoIngreso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaRetiro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaEgreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaIngreso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleExperiencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.jefe = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreInstitucion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ultimoSueldo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleExperiencia paramElegibleExperiencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.cargoEgreso = paramElegibleExperiencia.cargoEgreso;
      return;
    case 1:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.cargoIngreso = paramElegibleExperiencia.cargoIngreso;
      return;
    case 2:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.causaRetiro = paramElegibleExperiencia.causaRetiro;
      return;
    case 3:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleExperiencia.elegible;
      return;
    case 4:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaEgreso = paramElegibleExperiencia.fechaEgreso;
      return;
    case 5:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.fechaIngreso = paramElegibleExperiencia.fechaIngreso;
      return;
    case 6:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleExperiencia = paramElegibleExperiencia.idElegibleExperiencia;
      return;
    case 7:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleExperiencia.idSitp;
      return;
    case 8:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.jefe = paramElegibleExperiencia.jefe;
      return;
    case 9:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.nombreInstitucion = paramElegibleExperiencia.nombreInstitucion;
      return;
    case 10:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramElegibleExperiencia.observaciones;
      return;
    case 11:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.telefono = paramElegibleExperiencia.telefono;
      return;
    case 12:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleExperiencia.tiempoSitp;
      return;
    case 13:
      if (paramElegibleExperiencia == null)
        throw new IllegalArgumentException("arg1");
      this.ultimoSueldo = paramElegibleExperiencia.ultimoSueldo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleExperiencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleExperiencia localElegibleExperiencia = (ElegibleExperiencia)paramObject;
    if (localElegibleExperiencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleExperiencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleExperienciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleExperienciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleExperienciaPK))
      throw new IllegalArgumentException("arg1");
    ElegibleExperienciaPK localElegibleExperienciaPK = (ElegibleExperienciaPK)paramObject;
    localElegibleExperienciaPK.idElegibleExperiencia = this.idElegibleExperiencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleExperienciaPK))
      throw new IllegalArgumentException("arg1");
    ElegibleExperienciaPK localElegibleExperienciaPK = (ElegibleExperienciaPK)paramObject;
    this.idElegibleExperiencia = localElegibleExperienciaPK.idElegibleExperiencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleExperienciaPK))
      throw new IllegalArgumentException("arg2");
    ElegibleExperienciaPK localElegibleExperienciaPK = (ElegibleExperienciaPK)paramObject;
    localElegibleExperienciaPK.idElegibleExperiencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleExperienciaPK))
      throw new IllegalArgumentException("arg2");
    ElegibleExperienciaPK localElegibleExperienciaPK = (ElegibleExperienciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localElegibleExperienciaPK.idElegibleExperiencia);
  }

  private static final String jdoGetcargoEgreso(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.cargoEgreso;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.cargoEgreso;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 0))
      return paramElegibleExperiencia.cargoEgreso;
    return localStateManager.getStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 0, paramElegibleExperiencia.cargoEgreso);
  }

  private static final void jdoSetcargoEgreso(ElegibleExperiencia paramElegibleExperiencia, String paramString)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.cargoEgreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.cargoEgreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 0, paramElegibleExperiencia.cargoEgreso, paramString);
  }

  private static final String jdoGetcargoIngreso(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.cargoIngreso;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.cargoIngreso;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 1))
      return paramElegibleExperiencia.cargoIngreso;
    return localStateManager.getStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 1, paramElegibleExperiencia.cargoIngreso);
  }

  private static final void jdoSetcargoIngreso(ElegibleExperiencia paramElegibleExperiencia, String paramString)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.cargoIngreso = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.cargoIngreso = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 1, paramElegibleExperiencia.cargoIngreso, paramString);
  }

  private static final String jdoGetcausaRetiro(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.causaRetiro;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.causaRetiro;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 2))
      return paramElegibleExperiencia.causaRetiro;
    return localStateManager.getStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 2, paramElegibleExperiencia.causaRetiro);
  }

  private static final void jdoSetcausaRetiro(ElegibleExperiencia paramElegibleExperiencia, String paramString)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.causaRetiro = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.causaRetiro = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 2, paramElegibleExperiencia.causaRetiro, paramString);
  }

  private static final Elegible jdoGetelegible(ElegibleExperiencia paramElegibleExperiencia)
  {
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.elegible;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 3))
      return paramElegibleExperiencia.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 3, paramElegibleExperiencia.elegible);
  }

  private static final void jdoSetelegible(ElegibleExperiencia paramElegibleExperiencia, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 3, paramElegibleExperiencia.elegible, paramElegible);
  }

  private static final Date jdoGetfechaEgreso(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.fechaEgreso;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.fechaEgreso;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 4))
      return paramElegibleExperiencia.fechaEgreso;
    return (Date)localStateManager.getObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 4, paramElegibleExperiencia.fechaEgreso);
  }

  private static final void jdoSetfechaEgreso(ElegibleExperiencia paramElegibleExperiencia, Date paramDate)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.fechaEgreso = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.fechaEgreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 4, paramElegibleExperiencia.fechaEgreso, paramDate);
  }

  private static final Date jdoGetfechaIngreso(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.fechaIngreso;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.fechaIngreso;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 5))
      return paramElegibleExperiencia.fechaIngreso;
    return (Date)localStateManager.getObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 5, paramElegibleExperiencia.fechaIngreso);
  }

  private static final void jdoSetfechaIngreso(ElegibleExperiencia paramElegibleExperiencia, Date paramDate)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.fechaIngreso = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.fechaIngreso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 5, paramElegibleExperiencia.fechaIngreso, paramDate);
  }

  private static final long jdoGetidElegibleExperiencia(ElegibleExperiencia paramElegibleExperiencia)
  {
    return paramElegibleExperiencia.idElegibleExperiencia;
  }

  private static final void jdoSetidElegibleExperiencia(ElegibleExperiencia paramElegibleExperiencia, long paramLong)
  {
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.idElegibleExperiencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleExperiencia, jdoInheritedFieldCount + 6, paramElegibleExperiencia.idElegibleExperiencia, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.idSitp;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.idSitp;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 7))
      return paramElegibleExperiencia.idSitp;
    return localStateManager.getIntField(paramElegibleExperiencia, jdoInheritedFieldCount + 7, paramElegibleExperiencia.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleExperiencia paramElegibleExperiencia, int paramInt)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleExperiencia, jdoInheritedFieldCount + 7, paramElegibleExperiencia.idSitp, paramInt);
  }

  private static final String jdoGetjefe(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.jefe;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.jefe;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 8))
      return paramElegibleExperiencia.jefe;
    return localStateManager.getStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 8, paramElegibleExperiencia.jefe);
  }

  private static final void jdoSetjefe(ElegibleExperiencia paramElegibleExperiencia, String paramString)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.jefe = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.jefe = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 8, paramElegibleExperiencia.jefe, paramString);
  }

  private static final String jdoGetnombreInstitucion(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.nombreInstitucion;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.nombreInstitucion;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 9))
      return paramElegibleExperiencia.nombreInstitucion;
    return localStateManager.getStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 9, paramElegibleExperiencia.nombreInstitucion);
  }

  private static final void jdoSetnombreInstitucion(ElegibleExperiencia paramElegibleExperiencia, String paramString)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.nombreInstitucion = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.nombreInstitucion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 9, paramElegibleExperiencia.nombreInstitucion, paramString);
  }

  private static final String jdoGetobservaciones(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.observaciones;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.observaciones;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 10))
      return paramElegibleExperiencia.observaciones;
    return localStateManager.getStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 10, paramElegibleExperiencia.observaciones);
  }

  private static final void jdoSetobservaciones(ElegibleExperiencia paramElegibleExperiencia, String paramString)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 10, paramElegibleExperiencia.observaciones, paramString);
  }

  private static final String jdoGettelefono(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.telefono;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.telefono;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 11))
      return paramElegibleExperiencia.telefono;
    return localStateManager.getStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 11, paramElegibleExperiencia.telefono);
  }

  private static final void jdoSettelefono(ElegibleExperiencia paramElegibleExperiencia, String paramString)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.telefono = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.telefono = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleExperiencia, jdoInheritedFieldCount + 11, paramElegibleExperiencia.telefono, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.tiempoSitp;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 12))
      return paramElegibleExperiencia.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 12, paramElegibleExperiencia.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleExperiencia paramElegibleExperiencia, Date paramDate)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleExperiencia, jdoInheritedFieldCount + 12, paramElegibleExperiencia.tiempoSitp, paramDate);
  }

  private static final double jdoGetultimoSueldo(ElegibleExperiencia paramElegibleExperiencia)
  {
    if (paramElegibleExperiencia.jdoFlags <= 0)
      return paramElegibleExperiencia.ultimoSueldo;
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleExperiencia.ultimoSueldo;
    if (localStateManager.isLoaded(paramElegibleExperiencia, jdoInheritedFieldCount + 13))
      return paramElegibleExperiencia.ultimoSueldo;
    return localStateManager.getDoubleField(paramElegibleExperiencia, jdoInheritedFieldCount + 13, paramElegibleExperiencia.ultimoSueldo);
  }

  private static final void jdoSetultimoSueldo(ElegibleExperiencia paramElegibleExperiencia, double paramDouble)
  {
    if (paramElegibleExperiencia.jdoFlags == 0)
    {
      paramElegibleExperiencia.ultimoSueldo = paramDouble;
      return;
    }
    StateManager localStateManager = paramElegibleExperiencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleExperiencia.ultimoSueldo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramElegibleExperiencia, jdoInheritedFieldCount + 13, paramElegibleExperiencia.ultimoSueldo, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}