package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleAfiliacionPK
  implements Serializable
{
  public long idElegibleAfiliacion;

  public ElegibleAfiliacionPK()
  {
  }

  public ElegibleAfiliacionPK(long idElegibleAfiliacion)
  {
    this.idElegibleAfiliacion = idElegibleAfiliacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleAfiliacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleAfiliacionPK thatPK)
  {
    return 
      this.idElegibleAfiliacion == thatPK.idElegibleAfiliacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleAfiliacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleAfiliacion);
  }
}