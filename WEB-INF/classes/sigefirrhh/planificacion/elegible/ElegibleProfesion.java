package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Profesion;

public class ElegibleProfesion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idElegibleProfesion;
  private Profesion profesion;
  private Elegible elegible;
  private String actualmente;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "actualmente", "elegible", "idElegibleProfesion", "idSitp", "profesion", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.personal.Profesion"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 26, 24, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleProfesion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleProfesion());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ElegibleProfesion()
  {
    jdoSetactualmente(this, "N");
  }

  public String toString()
  {
    return jdoGetprofesion(this).getNombre();
  }

  public String getActualmente()
  {
    return jdoGetactualmente(this);
  }

  public void setActualmente(String actualmente)
  {
    jdoSetactualmente(this, actualmente);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public long getIdElegibleProfesion()
  {
    return jdoGetidElegibleProfesion(this);
  }

  public void setIdElegibleProfesion(long idElegibleProfesion)
  {
    jdoSetidElegibleProfesion(this, idElegibleProfesion);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public Profesion getProfesion()
  {
    return jdoGetprofesion(this);
  }

  public void setProfesion(Profesion profesion)
  {
    jdoSetprofesion(this, profesion);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleProfesion localElegibleProfesion = new ElegibleProfesion();
    localElegibleProfesion.jdoFlags = 1;
    localElegibleProfesion.jdoStateManager = paramStateManager;
    return localElegibleProfesion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleProfesion localElegibleProfesion = new ElegibleProfesion();
    localElegibleProfesion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleProfesion.jdoFlags = 1;
    localElegibleProfesion.jdoStateManager = paramStateManager;
    return localElegibleProfesion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.actualmente);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleProfesion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.profesion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.actualmente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleProfesion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.profesion = ((Profesion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleProfesion paramElegibleProfesion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.actualmente = paramElegibleProfesion.actualmente;
      return;
    case 1:
      if (paramElegibleProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleProfesion.elegible;
      return;
    case 2:
      if (paramElegibleProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleProfesion = paramElegibleProfesion.idElegibleProfesion;
      return;
    case 3:
      if (paramElegibleProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleProfesion.idSitp;
      return;
    case 4:
      if (paramElegibleProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.profesion = paramElegibleProfesion.profesion;
      return;
    case 5:
      if (paramElegibleProfesion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleProfesion.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleProfesion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleProfesion localElegibleProfesion = (ElegibleProfesion)paramObject;
    if (localElegibleProfesion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleProfesion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleProfesionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleProfesionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleProfesionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleProfesionPK localElegibleProfesionPK = (ElegibleProfesionPK)paramObject;
    localElegibleProfesionPK.idElegibleProfesion = this.idElegibleProfesion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleProfesionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleProfesionPK localElegibleProfesionPK = (ElegibleProfesionPK)paramObject;
    this.idElegibleProfesion = localElegibleProfesionPK.idElegibleProfesion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleProfesionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleProfesionPK localElegibleProfesionPK = (ElegibleProfesionPK)paramObject;
    localElegibleProfesionPK.idElegibleProfesion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleProfesionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleProfesionPK localElegibleProfesionPK = (ElegibleProfesionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localElegibleProfesionPK.idElegibleProfesion);
  }

  private static final String jdoGetactualmente(ElegibleProfesion paramElegibleProfesion)
  {
    if (paramElegibleProfesion.jdoFlags <= 0)
      return paramElegibleProfesion.actualmente;
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleProfesion.actualmente;
    if (localStateManager.isLoaded(paramElegibleProfesion, jdoInheritedFieldCount + 0))
      return paramElegibleProfesion.actualmente;
    return localStateManager.getStringField(paramElegibleProfesion, jdoInheritedFieldCount + 0, paramElegibleProfesion.actualmente);
  }

  private static final void jdoSetactualmente(ElegibleProfesion paramElegibleProfesion, String paramString)
  {
    if (paramElegibleProfesion.jdoFlags == 0)
    {
      paramElegibleProfesion.actualmente = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleProfesion.actualmente = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleProfesion, jdoInheritedFieldCount + 0, paramElegibleProfesion.actualmente, paramString);
  }

  private static final Elegible jdoGetelegible(ElegibleProfesion paramElegibleProfesion)
  {
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleProfesion.elegible;
    if (localStateManager.isLoaded(paramElegibleProfesion, jdoInheritedFieldCount + 1))
      return paramElegibleProfesion.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleProfesion, jdoInheritedFieldCount + 1, paramElegibleProfesion.elegible);
  }

  private static final void jdoSetelegible(ElegibleProfesion paramElegibleProfesion, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleProfesion.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleProfesion, jdoInheritedFieldCount + 1, paramElegibleProfesion.elegible, paramElegible);
  }

  private static final long jdoGetidElegibleProfesion(ElegibleProfesion paramElegibleProfesion)
  {
    return paramElegibleProfesion.idElegibleProfesion;
  }

  private static final void jdoSetidElegibleProfesion(ElegibleProfesion paramElegibleProfesion, long paramLong)
  {
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleProfesion.idElegibleProfesion = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleProfesion, jdoInheritedFieldCount + 2, paramElegibleProfesion.idElegibleProfesion, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleProfesion paramElegibleProfesion)
  {
    if (paramElegibleProfesion.jdoFlags <= 0)
      return paramElegibleProfesion.idSitp;
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleProfesion.idSitp;
    if (localStateManager.isLoaded(paramElegibleProfesion, jdoInheritedFieldCount + 3))
      return paramElegibleProfesion.idSitp;
    return localStateManager.getIntField(paramElegibleProfesion, jdoInheritedFieldCount + 3, paramElegibleProfesion.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleProfesion paramElegibleProfesion, int paramInt)
  {
    if (paramElegibleProfesion.jdoFlags == 0)
    {
      paramElegibleProfesion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleProfesion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleProfesion, jdoInheritedFieldCount + 3, paramElegibleProfesion.idSitp, paramInt);
  }

  private static final Profesion jdoGetprofesion(ElegibleProfesion paramElegibleProfesion)
  {
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleProfesion.profesion;
    if (localStateManager.isLoaded(paramElegibleProfesion, jdoInheritedFieldCount + 4))
      return paramElegibleProfesion.profesion;
    return (Profesion)localStateManager.getObjectField(paramElegibleProfesion, jdoInheritedFieldCount + 4, paramElegibleProfesion.profesion);
  }

  private static final void jdoSetprofesion(ElegibleProfesion paramElegibleProfesion, Profesion paramProfesion)
  {
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleProfesion.profesion = paramProfesion;
      return;
    }
    localStateManager.setObjectField(paramElegibleProfesion, jdoInheritedFieldCount + 4, paramElegibleProfesion.profesion, paramProfesion);
  }

  private static final Date jdoGettiempoSitp(ElegibleProfesion paramElegibleProfesion)
  {
    if (paramElegibleProfesion.jdoFlags <= 0)
      return paramElegibleProfesion.tiempoSitp;
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleProfesion.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleProfesion, jdoInheritedFieldCount + 5))
      return paramElegibleProfesion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleProfesion, jdoInheritedFieldCount + 5, paramElegibleProfesion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleProfesion paramElegibleProfesion, Date paramDate)
  {
    if (paramElegibleProfesion.jdoFlags == 0)
    {
      paramElegibleProfesion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleProfesion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleProfesion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleProfesion, jdoInheritedFieldCount + 5, paramElegibleProfesion.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}