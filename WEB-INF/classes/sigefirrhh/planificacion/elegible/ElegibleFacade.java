package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class ElegibleFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private ElegibleBusiness elegibleBusiness = new ElegibleBusiness();

  public void addElegible(Elegible elegible)
    throws Exception
  {
    try
    {
      this.txn.open();
    }
    finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegible(Elegible elegible) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegible(elegible);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegible(Elegible elegible) throws Exception
  {
    try { this.txn.open();
    } finally
    {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Elegible findElegibleById(long elegibleId) throws Exception
  {
    try { this.txn.open();
      Elegible elegible = 
        this.elegibleBusiness.findElegibleById(elegibleId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegible);
      return elegible;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegible() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegible();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleByCedula(int cedula)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleByCedula(cedula);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleByPrimerApellido(String primerApellido)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleByPrimerApellido(primerApellido);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleByPrimerNombre(String primerNombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleByPrimerNombre(primerNombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findElegibleByNombresApellidos(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleByNombresApellidos(
        primerNombre, segundoNombre, primerApellido, segundoApellido, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleByCedula(int cedula, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      return this.elegibleBusiness.findElegibleByCedula(cedula, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addElegible(Elegible elegible, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      this.elegibleBusiness.addElegible(elegible, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegible(Elegible elegible, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      this.elegibleBusiness.deleteElegible(elegible, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleActividadDocente(elegibleActividadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleActividadDocente(elegibleActividadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleActividadDocente(ElegibleActividadDocente elegibleActividadDocente) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleActividadDocente(elegibleActividadDocente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleActividadDocente findElegibleActividadDocenteById(long elegibleActividadDocenteId) throws Exception
  {
    try { this.txn.open();
      ElegibleActividadDocente elegibleActividadDocente = 
        this.elegibleBusiness.findElegibleActividadDocenteById(elegibleActividadDocenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleActividadDocente);
      return elegibleActividadDocente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleActividadDocente() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleActividadDocente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleActividadDocenteByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleActividadDocenteByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleAfiliacion(elegibleAfiliacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleAfiliacion(elegibleAfiliacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleAfiliacion(ElegibleAfiliacion elegibleAfiliacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleAfiliacion(elegibleAfiliacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleAfiliacion findElegibleAfiliacionById(long elegibleAfiliacionId) throws Exception
  {
    try { this.txn.open();
      ElegibleAfiliacion elegibleAfiliacion = 
        this.elegibleBusiness.findElegibleAfiliacionById(elegibleAfiliacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleAfiliacion);
      return elegibleAfiliacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleAfiliacion() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleAfiliacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleAfiliacionByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleAfiliacionByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleAntecedente(ElegibleAntecedente elegibleAntecedente)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleAntecedente(elegibleAntecedente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleAntecedente(ElegibleAntecedente elegibleAntecedente) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleAntecedente(elegibleAntecedente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleAntecedente(ElegibleAntecedente elegibleAntecedente) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleAntecedente(elegibleAntecedente);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleAntecedente findElegibleAntecedenteById(long elegibleAntecedenteId) throws Exception
  {
    try { this.txn.open();
      ElegibleAntecedente elegibleAntecedente = 
        this.elegibleBusiness.findElegibleAntecedenteById(elegibleAntecedenteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleAntecedente);
      return elegibleAntecedente;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleAntecedente() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleAntecedente();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleAntecedenteByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleAntecedenteByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleCertificacion(ElegibleCertificacion elegibleCertificacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleCertificacion(elegibleCertificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleCertificacion(ElegibleCertificacion elegibleCertificacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleCertificacion(elegibleCertificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleCertificacion(ElegibleCertificacion elegibleCertificacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleCertificacion(elegibleCertificacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleCertificacion findElegibleCertificacionById(long elegibleCertificacionId) throws Exception
  {
    try { this.txn.open();
      ElegibleCertificacion elegibleCertificacion = 
        this.elegibleBusiness.findElegibleCertificacionById(elegibleCertificacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleCertificacion);
      return elegibleCertificacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleCertificacion() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleCertificacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleCertificacionByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleCertificacionByAreaConocimiento(idAreaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleCertificacionByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleCertificacionByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleEducacion(ElegibleEducacion elegibleEducacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleEducacion(elegibleEducacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleEducacion(ElegibleEducacion elegibleEducacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleEducacion(elegibleEducacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleEducacion(ElegibleEducacion elegibleEducacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleEducacion(elegibleEducacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleEducacion findElegibleEducacionById(long elegibleEducacionId) throws Exception
  {
    try { this.txn.open();
      ElegibleEducacion elegibleEducacion = 
        this.elegibleBusiness.findElegibleEducacionById(elegibleEducacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleEducacion);
      return elegibleEducacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleEducacion() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleEducacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleEducacionByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleEducacionByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleEstudio(ElegibleEstudio elegibleEstudio)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleEstudio(elegibleEstudio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleEstudio(ElegibleEstudio elegibleEstudio) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleEstudio(elegibleEstudio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleEstudio(ElegibleEstudio elegibleEstudio) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleEstudio(elegibleEstudio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleEstudio findElegibleEstudioById(long elegibleEstudioId) throws Exception
  {
    try { this.txn.open();
      ElegibleEstudio elegibleEstudio = 
        this.elegibleBusiness.findElegibleEstudioById(elegibleEstudioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleEstudio);
      return elegibleEstudio;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleEstudio() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleEstudio();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleEstudioByTipoCurso(long idTipoCurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleEstudioByTipoCurso(idTipoCurso);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleEstudioByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleEstudioByAreaConocimiento(idAreaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleEstudioByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleEstudioByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleExperiencia(ElegibleExperiencia elegibleExperiencia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleExperiencia(elegibleExperiencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleExperiencia(ElegibleExperiencia elegibleExperiencia) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleExperiencia(elegibleExperiencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleExperiencia(ElegibleExperiencia elegibleExperiencia) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleExperiencia(elegibleExperiencia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleExperiencia findElegibleExperienciaById(long elegibleExperienciaId) throws Exception
  {
    try { this.txn.open();
      ElegibleExperiencia elegibleExperiencia = 
        this.elegibleBusiness.findElegibleExperienciaById(elegibleExperienciaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleExperiencia);
      return elegibleExperiencia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleExperiencia() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleExperiencia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleExperienciaByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleExperienciaByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleFamiliar(ElegibleFamiliar elegibleFamiliar)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleFamiliar(elegibleFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleFamiliar(ElegibleFamiliar elegibleFamiliar) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleFamiliar(elegibleFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleFamiliar(ElegibleFamiliar elegibleFamiliar) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleFamiliar(elegibleFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleFamiliar findElegibleFamiliarById(long elegibleFamiliarId) throws Exception
  {
    try { this.txn.open();
      ElegibleFamiliar elegibleFamiliar = 
        this.elegibleBusiness.findElegibleFamiliarById(elegibleFamiliarId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleFamiliar);
      return elegibleFamiliar;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleFamiliar() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleFamiliar();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleFamiliarByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleFamiliarByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleHabilidad(ElegibleHabilidad elegibleHabilidad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleHabilidad(elegibleHabilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleHabilidad(ElegibleHabilidad elegibleHabilidad) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleHabilidad(elegibleHabilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleHabilidad(ElegibleHabilidad elegibleHabilidad) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleHabilidad(elegibleHabilidad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleHabilidad findElegibleHabilidadById(long elegibleHabilidadId) throws Exception
  {
    try { this.txn.open();
      ElegibleHabilidad elegibleHabilidad = 
        this.elegibleBusiness.findElegibleHabilidadById(elegibleHabilidadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleHabilidad);
      return elegibleHabilidad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleHabilidad() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleHabilidad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleHabilidadByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleHabilidadByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleIdioma(ElegibleIdioma elegibleIdioma)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleIdioma(elegibleIdioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleIdioma(ElegibleIdioma elegibleIdioma) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleIdioma(elegibleIdioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleIdioma(ElegibleIdioma elegibleIdioma) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleIdioma(elegibleIdioma);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleIdioma findElegibleIdiomaById(long elegibleIdiomaId) throws Exception
  {
    try { this.txn.open();
      ElegibleIdioma elegibleIdioma = 
        this.elegibleBusiness.findElegibleIdiomaById(elegibleIdiomaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleIdioma);
      return elegibleIdioma;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleIdioma() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleIdioma();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleIdiomaByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleIdiomaByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleOrganismo(ElegibleOrganismo elegibleOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleOrganismo(elegibleOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleOrganismo(ElegibleOrganismo elegibleOrganismo) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleOrganismo(elegibleOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleOrganismo(ElegibleOrganismo elegibleOrganismo) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleOrganismo(elegibleOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleOrganismo findElegibleOrganismoById(long elegibleOrganismoId) throws Exception
  {
    try { this.txn.open();
      ElegibleOrganismo elegibleOrganismo = 
        this.elegibleBusiness.findElegibleOrganismoById(elegibleOrganismoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleOrganismo);
      return elegibleOrganismo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleOrganismo() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleOrganismo();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleOtraActividad(elegibleOtraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleOtraActividad(elegibleOtraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleOtraActividad(ElegibleOtraActividad elegibleOtraActividad) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleOtraActividad(elegibleOtraActividad);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleOtraActividad findElegibleOtraActividadById(long elegibleOtraActividadId) throws Exception
  {
    try { this.txn.open();
      ElegibleOtraActividad elegibleOtraActividad = 
        this.elegibleBusiness.findElegibleOtraActividadById(elegibleOtraActividadId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleOtraActividad);
      return elegibleOtraActividad;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleOtraActividad() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleOtraActividad();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleOtraActividadByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleOtraActividadByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegibleProfesion(ElegibleProfesion elegibleProfesion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegibleProfesion(elegibleProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegibleProfesion(ElegibleProfesion elegibleProfesion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegibleProfesion(elegibleProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegibleProfesion(ElegibleProfesion elegibleProfesion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegibleProfesion(elegibleProfesion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegibleProfesion findElegibleProfesionById(long elegibleProfesionId) throws Exception
  {
    try { this.txn.open();
      ElegibleProfesion elegibleProfesion = 
        this.elegibleBusiness.findElegibleProfesionById(elegibleProfesionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegibleProfesion);
      return elegibleProfesion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegibleProfesion() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegibleProfesion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegibleProfesionByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegibleProfesionByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addElegiblePublicacion(ElegiblePublicacion elegiblePublicacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.elegibleBusiness.addElegiblePublicacion(elegiblePublicacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateElegiblePublicacion(ElegiblePublicacion elegiblePublicacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.updateElegiblePublicacion(elegiblePublicacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteElegiblePublicacion(ElegiblePublicacion elegiblePublicacion) throws Exception
  {
    try { this.txn.open();
      this.elegibleBusiness.deleteElegiblePublicacion(elegiblePublicacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ElegiblePublicacion findElegiblePublicacionById(long elegiblePublicacionId) throws Exception
  {
    try { this.txn.open();
      ElegiblePublicacion elegiblePublicacion = 
        this.elegibleBusiness.findElegiblePublicacionById(elegiblePublicacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(elegiblePublicacion);
      return elegiblePublicacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllElegiblePublicacion() throws Exception
  {
    try { this.txn.open();
      return this.elegibleBusiness.findAllElegiblePublicacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findElegiblePublicacionByElegible(long idElegible)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.elegibleBusiness.findElegiblePublicacionByElegible(idElegible);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}