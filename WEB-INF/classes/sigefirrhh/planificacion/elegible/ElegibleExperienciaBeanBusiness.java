package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ElegibleExperienciaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleExperiencia(ElegibleExperiencia elegibleExperiencia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleExperiencia elegibleExperienciaNew = 
      (ElegibleExperiencia)BeanUtils.cloneBean(
      elegibleExperiencia);

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleExperienciaNew.getElegible() != null) {
      elegibleExperienciaNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleExperienciaNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleExperienciaNew);
  }

  public void updateElegibleExperiencia(ElegibleExperiencia elegibleExperiencia) throws Exception
  {
    ElegibleExperiencia elegibleExperienciaModify = 
      findElegibleExperienciaById(elegibleExperiencia.getIdElegibleExperiencia());

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleExperiencia.getElegible() != null) {
      elegibleExperiencia.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleExperiencia.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleExperienciaModify, elegibleExperiencia);
  }

  public void deleteElegibleExperiencia(ElegibleExperiencia elegibleExperiencia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleExperiencia elegibleExperienciaDelete = 
      findElegibleExperienciaById(elegibleExperiencia.getIdElegibleExperiencia());
    pm.deletePersistent(elegibleExperienciaDelete);
  }

  public ElegibleExperiencia findElegibleExperienciaById(long idElegibleExperiencia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleExperiencia == pIdElegibleExperiencia";
    Query query = pm.newQuery(ElegibleExperiencia.class, filter);

    query.declareParameters("long pIdElegibleExperiencia");

    parameters.put("pIdElegibleExperiencia", new Long(idElegibleExperiencia));

    Collection colElegibleExperiencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleExperiencia.iterator();
    return (ElegibleExperiencia)iterator.next();
  }

  public Collection findElegibleExperienciaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleExperienciaExtent = pm.getExtent(
      ElegibleExperiencia.class, true);
    Query query = pm.newQuery(elegibleExperienciaExtent);
    query.setOrdering("fechaIngreso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleExperiencia.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("fechaIngreso ascending");

    Collection colElegibleExperiencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleExperiencia);

    return colElegibleExperiencia;
  }
}