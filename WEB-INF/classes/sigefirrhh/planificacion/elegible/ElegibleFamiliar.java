package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ElegibleFamiliar
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTADO_CIVIL;
  protected static final Map LISTA_SEXO;
  protected static final Map LISTA_PARENTESCO;
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_GRUPO_SANGUINEO;
  private long idElegibleFamiliar;
  private String primerNombre;
  private String segundoNombre;
  private String primerApellido;
  private String segundoApellido;
  private String estadoCivil;
  private String sexo;
  private int cedulaFamiliar;
  private Date fechaNacimiento;
  private String parentesco;
  private String nivelEducativo;
  private String grado;
  private String promedioNota;
  private String mismoOrganismo;
  private String ninoExcepcional;
  private String tallaFranela;
  private String tallaPantalon;
  private String tallaGorra;
  private String gozaPrimaPorHijo;
  private String gozaUtiles;
  private String gozaSeguro;
  private String gozaBeca;
  private String grupoSanguineo;
  private String alergico;
  private String alergias;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "alergias", "alergico", "cedulaFamiliar", "elegible", "estadoCivil", "fechaNacimiento", "gozaBeca", "gozaPrimaPorHijo", "gozaSeguro", "gozaUtiles", "grado", "grupoSanguineo", "idElegibleFamiliar", "idSitp", "mismoOrganismo", "ninoExcepcional", "nivelEducativo", "parentesco", "primerApellido", "primerNombre", "promedioNota", "segundoApellido", "segundoNombre", "sexo", "tallaFranela", "tallaGorra", "tallaPantalon", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleFamiliar"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleFamiliar());

    LISTA_ESTADO_CIVIL = 
      new LinkedHashMap();
    LISTA_SEXO = 
      new LinkedHashMap();
    LISTA_PARENTESCO = 
      new LinkedHashMap();
    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_GRUPO_SANGUINEO = 
      new LinkedHashMap();

    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
    LISTA_ESTADO_CIVIL.put("D", "DIVORCIADO(A)");
    LISTA_ESTADO_CIVIL.put("V", "VIUDO(A)");
    LISTA_ESTADO_CIVIL.put("U", "UNIDO(A)");
    LISTA_ESTADO_CIVIL.put("O", "OTRO");
    LISTA_SEXO.put("F", "FEMENINO");
    LISTA_SEXO.put("M", "MASCULINO");
    LISTA_PARENTESCO.put("C", "CONYUGUE");
    LISTA_PARENTESCO.put("M", "MADRE");
    LISTA_PARENTESCO.put("P", "PADRE");
    LISTA_PARENTESCO.put("H", "HIJO(A)");
    LISTA_PARENTESCO.put("E", "HERMANO(A)");
    LISTA_PARENTESCO.put("S", "SUEGRO(A)");
    LISTA_PARENTESCO.put("A", "ABUELO(A)");
    LISTA_PARENTESCO.put("B", "SOBRINO(A)");
    LISTA_PARENTESCO.put("I", "TIO(A)");
    LISTA_PARENTESCO.put("U", "TUTELADO(A)");
    LISTA_PARENTESCO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("I", "PRIMARIA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("H", "BACHILLERATO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("E", "ESPECIALIZACION");
    LISTA_NIVEL_EDUCATIVO.put("M", "MAESTRIA");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("R", "POST DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("L", "DIPLOMADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("N", "NINGUNO");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_GRUPO_SANGUINEO.put("A+", "A+");
    LISTA_GRUPO_SANGUINEO.put("B+", "B+");
    LISTA_GRUPO_SANGUINEO.put("O+", "O+");
    LISTA_GRUPO_SANGUINEO.put("A-", "A-");
    LISTA_GRUPO_SANGUINEO.put("B-", "B-");
    LISTA_GRUPO_SANGUINEO.put("O-", "O-");
    LISTA_GRUPO_SANGUINEO.put("AB+", "AB+");
    LISTA_GRUPO_SANGUINEO.put("AB-", "AB-");
    LISTA_GRUPO_SANGUINEO.put("NO", "NO");
  }

  public ElegibleFamiliar()
  {
    jdoSetcedulaFamiliar(this, 0);

    jdoSetnivelEducativo(this, "N");

    jdoSetmismoOrganismo(this, "N");

    jdoSetninoExcepcional(this, "N");

    jdoSetgozaPrimaPorHijo(this, "N");

    jdoSetgozaUtiles(this, "N");

    jdoSetgozaSeguro(this, "N");

    jdoSetgozaBeca(this, "N");

    jdoSetgrupoSanguineo(this, "NO");

    jdoSetalergico(this, "N");
  }

  public String toString()
  {
    return jdoGetprimerApellido(this) + " " + 
      jdoGetprimerNombre(this) + " - " + 
      LISTA_PARENTESCO.get(jdoGetparentesco(this)) + " - " + 
      new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaNacimiento(this));
  }

  public String getAlergias()
  {
    return jdoGetalergias(this);
  }

  public void setAlergias(String alergias)
  {
    jdoSetalergias(this, alergias);
  }

  public String getAlergico()
  {
    return jdoGetalergico(this);
  }

  public void setAlergico(String alergico)
  {
    jdoSetalergico(this, alergico);
  }

  public int getCedulaFamiliar()
  {
    return jdoGetcedulaFamiliar(this);
  }

  public void setCedulaFamiliar(int cedulaFamiliar)
  {
    jdoSetcedulaFamiliar(this, cedulaFamiliar);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public String getEstadoCivil()
  {
    return jdoGetestadoCivil(this);
  }

  public void setEstadoCivil(String estadoCivil)
  {
    jdoSetestadoCivil(this, estadoCivil);
  }

  public Date getFechaNacimiento()
  {
    return jdoGetfechaNacimiento(this);
  }

  public void setFechaNacimiento(Date fechaNacimiento)
  {
    jdoSetfechaNacimiento(this, fechaNacimiento);
  }

  public String getGozaBeca()
  {
    return jdoGetgozaBeca(this);
  }

  public void setGozaBeca(String gozaBeca)
  {
    jdoSetgozaBeca(this, gozaBeca);
  }

  public String getGozaPrimaPorHijo()
  {
    return jdoGetgozaPrimaPorHijo(this);
  }

  public void setGozaPrimaPorHijo(String gozaPrimaPorHijo)
  {
    jdoSetgozaPrimaPorHijo(this, gozaPrimaPorHijo);
  }

  public String getGozaSeguro()
  {
    return jdoGetgozaSeguro(this);
  }

  public void setGozaSeguro(String gozaSeguro)
  {
    jdoSetgozaSeguro(this, gozaSeguro);
  }

  public String getGozaUtiles()
  {
    return jdoGetgozaUtiles(this);
  }

  public void setGozaUtiles(String gozaUtiles)
  {
    jdoSetgozaUtiles(this, gozaUtiles);
  }

  public String getGrado()
  {
    return jdoGetgrado(this);
  }

  public void setGrado(String grado)
  {
    jdoSetgrado(this, grado);
  }

  public String getGrupoSanguineo()
  {
    return jdoGetgrupoSanguineo(this);
  }

  public void setGrupoSanguineo(String grupoSanguineo)
  {
    jdoSetgrupoSanguineo(this, grupoSanguineo);
  }

  public long getIdElegibleFamiliar()
  {
    return jdoGetidElegibleFamiliar(this);
  }

  public void setIdElegibleFamiliar(long idElegibleFamiliar)
  {
    jdoSetidElegibleFamiliar(this, idElegibleFamiliar);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getMismoOrganismo()
  {
    return jdoGetmismoOrganismo(this);
  }

  public void setMismoOrganismo(String mismoOrganismo)
  {
    jdoSetmismoOrganismo(this, mismoOrganismo);
  }

  public String getNinoExcepcional()
  {
    return jdoGetninoExcepcional(this);
  }

  public void setNinoExcepcional(String ninoExcepcional)
  {
    jdoSetninoExcepcional(this, ninoExcepcional);
  }

  public String getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public void setNivelEducativo(String nivelEducativo)
  {
    jdoSetnivelEducativo(this, nivelEducativo);
  }

  public String getParentesco()
  {
    return jdoGetparentesco(this);
  }

  public void setParentesco(String parentesco)
  {
    jdoSetparentesco(this, parentesco);
  }

  public String getPrimerApellido()
  {
    return jdoGetprimerApellido(this);
  }

  public void setPrimerApellido(String primerApellido)
  {
    jdoSetprimerApellido(this, primerApellido);
  }

  public String getPrimerNombre()
  {
    return jdoGetprimerNombre(this);
  }

  public void setPrimerNombre(String primerNombre)
  {
    jdoSetprimerNombre(this, primerNombre);
  }

  public String getPromedioNota()
  {
    return jdoGetpromedioNota(this);
  }

  public void setPromedioNota(String promedioNota)
  {
    jdoSetpromedioNota(this, promedioNota);
  }

  public String getSegundoApellido()
  {
    return jdoGetsegundoApellido(this);
  }

  public void setSegundoApellido(String segundoApellido)
  {
    jdoSetsegundoApellido(this, segundoApellido);
  }

  public String getSegundoNombre()
  {
    return jdoGetsegundoNombre(this);
  }

  public void setSegundoNombre(String segundoNombre)
  {
    jdoSetsegundoNombre(this, segundoNombre);
  }

  public String getSexo()
  {
    return jdoGetsexo(this);
  }

  public void setSexo(String sexo)
  {
    jdoSetsexo(this, sexo);
  }

  public String getTallaFranela()
  {
    return jdoGettallaFranela(this);
  }

  public void setTallaFranela(String tallaFranela)
  {
    jdoSettallaFranela(this, tallaFranela);
  }

  public String getTallaGorra()
  {
    return jdoGettallaGorra(this);
  }

  public void setTallaGorra(String tallaGorra)
  {
    jdoSettallaGorra(this, tallaGorra);
  }

  public String getTallaPantalon()
  {
    return jdoGettallaPantalon(this);
  }

  public void setTallaPantalon(String tallaPantalon)
  {
    jdoSettallaPantalon(this, tallaPantalon);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 28;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleFamiliar localElegibleFamiliar = new ElegibleFamiliar();
    localElegibleFamiliar.jdoFlags = 1;
    localElegibleFamiliar.jdoStateManager = paramStateManager;
    return localElegibleFamiliar;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleFamiliar localElegibleFamiliar = new ElegibleFamiliar();
    localElegibleFamiliar.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleFamiliar.jdoFlags = 1;
    localElegibleFamiliar.jdoStateManager = paramStateManager;
    return localElegibleFamiliar;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alergias);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.alergico);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaFamiliar);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estadoCivil);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaNacimiento);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaBeca);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaPrimaPorHijo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaSeguro);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.gozaUtiles);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.grado);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.grupoSanguineo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleFamiliar);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mismoOrganismo);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ninoExcepcional);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.parentesco);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.promedioNota);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sexo);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tallaFranela);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tallaGorra);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tallaPantalon);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alergias = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.alergico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaFamiliar = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estadoCivil = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaNacimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaBeca = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaPrimaPorHijo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaSeguro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gozaUtiles = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoSanguineo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleFamiliar = localStateManager.replacingLongField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mismoOrganismo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ninoExcepcional = localStateManager.replacingStringField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.parentesco = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioNota = localStateManager.replacingStringField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sexo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tallaFranela = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tallaGorra = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tallaPantalon = localStateManager.replacingStringField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleFamiliar paramElegibleFamiliar, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.alergias = paramElegibleFamiliar.alergias;
      return;
    case 1:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.alergico = paramElegibleFamiliar.alergico;
      return;
    case 2:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaFamiliar = paramElegibleFamiliar.cedulaFamiliar;
      return;
    case 3:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleFamiliar.elegible;
      return;
    case 4:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.estadoCivil = paramElegibleFamiliar.estadoCivil;
      return;
    case 5:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.fechaNacimiento = paramElegibleFamiliar.fechaNacimiento;
      return;
    case 6:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaBeca = paramElegibleFamiliar.gozaBeca;
      return;
    case 7:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaPrimaPorHijo = paramElegibleFamiliar.gozaPrimaPorHijo;
      return;
    case 8:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaSeguro = paramElegibleFamiliar.gozaSeguro;
      return;
    case 9:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.gozaUtiles = paramElegibleFamiliar.gozaUtiles;
      return;
    case 10:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramElegibleFamiliar.grado;
      return;
    case 11:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.grupoSanguineo = paramElegibleFamiliar.grupoSanguineo;
      return;
    case 12:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleFamiliar = paramElegibleFamiliar.idElegibleFamiliar;
      return;
    case 13:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleFamiliar.idSitp;
      return;
    case 14:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.mismoOrganismo = paramElegibleFamiliar.mismoOrganismo;
      return;
    case 15:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.ninoExcepcional = paramElegibleFamiliar.ninoExcepcional;
      return;
    case 16:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramElegibleFamiliar.nivelEducativo;
      return;
    case 17:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.parentesco = paramElegibleFamiliar.parentesco;
      return;
    case 18:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramElegibleFamiliar.primerApellido;
      return;
    case 19:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramElegibleFamiliar.primerNombre;
      return;
    case 20:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.promedioNota = paramElegibleFamiliar.promedioNota;
      return;
    case 21:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramElegibleFamiliar.segundoApellido;
      return;
    case 22:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramElegibleFamiliar.segundoNombre;
      return;
    case 23:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.sexo = paramElegibleFamiliar.sexo;
      return;
    case 24:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tallaFranela = paramElegibleFamiliar.tallaFranela;
      return;
    case 25:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tallaGorra = paramElegibleFamiliar.tallaGorra;
      return;
    case 26:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tallaPantalon = paramElegibleFamiliar.tallaPantalon;
      return;
    case 27:
      if (paramElegibleFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleFamiliar.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleFamiliar))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleFamiliar localElegibleFamiliar = (ElegibleFamiliar)paramObject;
    if (localElegibleFamiliar.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleFamiliar, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleFamiliarPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleFamiliarPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleFamiliarPK))
      throw new IllegalArgumentException("arg1");
    ElegibleFamiliarPK localElegibleFamiliarPK = (ElegibleFamiliarPK)paramObject;
    localElegibleFamiliarPK.idElegibleFamiliar = this.idElegibleFamiliar;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleFamiliarPK))
      throw new IllegalArgumentException("arg1");
    ElegibleFamiliarPK localElegibleFamiliarPK = (ElegibleFamiliarPK)paramObject;
    this.idElegibleFamiliar = localElegibleFamiliarPK.idElegibleFamiliar;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleFamiliarPK))
      throw new IllegalArgumentException("arg2");
    ElegibleFamiliarPK localElegibleFamiliarPK = (ElegibleFamiliarPK)paramObject;
    localElegibleFamiliarPK.idElegibleFamiliar = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 12);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleFamiliarPK))
      throw new IllegalArgumentException("arg2");
    ElegibleFamiliarPK localElegibleFamiliarPK = (ElegibleFamiliarPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 12, localElegibleFamiliarPK.idElegibleFamiliar);
  }

  private static final String jdoGetalergias(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.alergias;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.alergias;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 0))
      return paramElegibleFamiliar.alergias;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 0, paramElegibleFamiliar.alergias);
  }

  private static final void jdoSetalergias(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.alergias = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.alergias = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 0, paramElegibleFamiliar.alergias, paramString);
  }

  private static final String jdoGetalergico(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.alergico;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.alergico;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 1))
      return paramElegibleFamiliar.alergico;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 1, paramElegibleFamiliar.alergico);
  }

  private static final void jdoSetalergico(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.alergico = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.alergico = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 1, paramElegibleFamiliar.alergico, paramString);
  }

  private static final int jdoGetcedulaFamiliar(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.cedulaFamiliar;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.cedulaFamiliar;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 2))
      return paramElegibleFamiliar.cedulaFamiliar;
    return localStateManager.getIntField(paramElegibleFamiliar, jdoInheritedFieldCount + 2, paramElegibleFamiliar.cedulaFamiliar);
  }

  private static final void jdoSetcedulaFamiliar(ElegibleFamiliar paramElegibleFamiliar, int paramInt)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.cedulaFamiliar = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.cedulaFamiliar = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleFamiliar, jdoInheritedFieldCount + 2, paramElegibleFamiliar.cedulaFamiliar, paramInt);
  }

  private static final Elegible jdoGetelegible(ElegibleFamiliar paramElegibleFamiliar)
  {
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.elegible;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 3))
      return paramElegibleFamiliar.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleFamiliar, jdoInheritedFieldCount + 3, paramElegibleFamiliar.elegible);
  }

  private static final void jdoSetelegible(ElegibleFamiliar paramElegibleFamiliar, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleFamiliar, jdoInheritedFieldCount + 3, paramElegibleFamiliar.elegible, paramElegible);
  }

  private static final String jdoGetestadoCivil(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.estadoCivil;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.estadoCivil;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 4))
      return paramElegibleFamiliar.estadoCivil;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 4, paramElegibleFamiliar.estadoCivil);
  }

  private static final void jdoSetestadoCivil(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.estadoCivil = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.estadoCivil = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 4, paramElegibleFamiliar.estadoCivil, paramString);
  }

  private static final Date jdoGetfechaNacimiento(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.fechaNacimiento;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.fechaNacimiento;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 5))
      return paramElegibleFamiliar.fechaNacimiento;
    return (Date)localStateManager.getObjectField(paramElegibleFamiliar, jdoInheritedFieldCount + 5, paramElegibleFamiliar.fechaNacimiento);
  }

  private static final void jdoSetfechaNacimiento(ElegibleFamiliar paramElegibleFamiliar, Date paramDate)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.fechaNacimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.fechaNacimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleFamiliar, jdoInheritedFieldCount + 5, paramElegibleFamiliar.fechaNacimiento, paramDate);
  }

  private static final String jdoGetgozaBeca(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.gozaBeca;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.gozaBeca;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 6))
      return paramElegibleFamiliar.gozaBeca;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 6, paramElegibleFamiliar.gozaBeca);
  }

  private static final void jdoSetgozaBeca(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.gozaBeca = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.gozaBeca = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 6, paramElegibleFamiliar.gozaBeca, paramString);
  }

  private static final String jdoGetgozaPrimaPorHijo(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.gozaPrimaPorHijo;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.gozaPrimaPorHijo;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 7))
      return paramElegibleFamiliar.gozaPrimaPorHijo;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 7, paramElegibleFamiliar.gozaPrimaPorHijo);
  }

  private static final void jdoSetgozaPrimaPorHijo(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.gozaPrimaPorHijo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.gozaPrimaPorHijo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 7, paramElegibleFamiliar.gozaPrimaPorHijo, paramString);
  }

  private static final String jdoGetgozaSeguro(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.gozaSeguro;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.gozaSeguro;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 8))
      return paramElegibleFamiliar.gozaSeguro;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 8, paramElegibleFamiliar.gozaSeguro);
  }

  private static final void jdoSetgozaSeguro(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.gozaSeguro = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.gozaSeguro = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 8, paramElegibleFamiliar.gozaSeguro, paramString);
  }

  private static final String jdoGetgozaUtiles(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.gozaUtiles;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.gozaUtiles;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 9))
      return paramElegibleFamiliar.gozaUtiles;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 9, paramElegibleFamiliar.gozaUtiles);
  }

  private static final void jdoSetgozaUtiles(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.gozaUtiles = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.gozaUtiles = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 9, paramElegibleFamiliar.gozaUtiles, paramString);
  }

  private static final String jdoGetgrado(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.grado;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.grado;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 10))
      return paramElegibleFamiliar.grado;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 10, paramElegibleFamiliar.grado);
  }

  private static final void jdoSetgrado(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.grado = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.grado = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 10, paramElegibleFamiliar.grado, paramString);
  }

  private static final String jdoGetgrupoSanguineo(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.grupoSanguineo;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.grupoSanguineo;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 11))
      return paramElegibleFamiliar.grupoSanguineo;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 11, paramElegibleFamiliar.grupoSanguineo);
  }

  private static final void jdoSetgrupoSanguineo(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.grupoSanguineo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.grupoSanguineo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 11, paramElegibleFamiliar.grupoSanguineo, paramString);
  }

  private static final long jdoGetidElegibleFamiliar(ElegibleFamiliar paramElegibleFamiliar)
  {
    return paramElegibleFamiliar.idElegibleFamiliar;
  }

  private static final void jdoSetidElegibleFamiliar(ElegibleFamiliar paramElegibleFamiliar, long paramLong)
  {
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.idElegibleFamiliar = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleFamiliar, jdoInheritedFieldCount + 12, paramElegibleFamiliar.idElegibleFamiliar, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.idSitp;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.idSitp;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 13))
      return paramElegibleFamiliar.idSitp;
    return localStateManager.getIntField(paramElegibleFamiliar, jdoInheritedFieldCount + 13, paramElegibleFamiliar.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleFamiliar paramElegibleFamiliar, int paramInt)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleFamiliar, jdoInheritedFieldCount + 13, paramElegibleFamiliar.idSitp, paramInt);
  }

  private static final String jdoGetmismoOrganismo(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.mismoOrganismo;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.mismoOrganismo;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 14))
      return paramElegibleFamiliar.mismoOrganismo;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 14, paramElegibleFamiliar.mismoOrganismo);
  }

  private static final void jdoSetmismoOrganismo(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.mismoOrganismo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.mismoOrganismo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 14, paramElegibleFamiliar.mismoOrganismo, paramString);
  }

  private static final String jdoGetninoExcepcional(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.ninoExcepcional;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.ninoExcepcional;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 15))
      return paramElegibleFamiliar.ninoExcepcional;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 15, paramElegibleFamiliar.ninoExcepcional);
  }

  private static final void jdoSetninoExcepcional(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.ninoExcepcional = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.ninoExcepcional = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 15, paramElegibleFamiliar.ninoExcepcional, paramString);
  }

  private static final String jdoGetnivelEducativo(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.nivelEducativo;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.nivelEducativo;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 16))
      return paramElegibleFamiliar.nivelEducativo;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 16, paramElegibleFamiliar.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 16, paramElegibleFamiliar.nivelEducativo, paramString);
  }

  private static final String jdoGetparentesco(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.parentesco;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.parentesco;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 17))
      return paramElegibleFamiliar.parentesco;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 17, paramElegibleFamiliar.parentesco);
  }

  private static final void jdoSetparentesco(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.parentesco = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.parentesco = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 17, paramElegibleFamiliar.parentesco, paramString);
  }

  private static final String jdoGetprimerApellido(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.primerApellido;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.primerApellido;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 18))
      return paramElegibleFamiliar.primerApellido;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 18, paramElegibleFamiliar.primerApellido);
  }

  private static final void jdoSetprimerApellido(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 18, paramElegibleFamiliar.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.primerNombre;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.primerNombre;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 19))
      return paramElegibleFamiliar.primerNombre;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 19, paramElegibleFamiliar.primerNombre);
  }

  private static final void jdoSetprimerNombre(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 19, paramElegibleFamiliar.primerNombre, paramString);
  }

  private static final String jdoGetpromedioNota(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.promedioNota;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.promedioNota;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 20))
      return paramElegibleFamiliar.promedioNota;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 20, paramElegibleFamiliar.promedioNota);
  }

  private static final void jdoSetpromedioNota(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.promedioNota = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.promedioNota = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 20, paramElegibleFamiliar.promedioNota, paramString);
  }

  private static final String jdoGetsegundoApellido(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.segundoApellido;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.segundoApellido;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 21))
      return paramElegibleFamiliar.segundoApellido;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 21, paramElegibleFamiliar.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 21, paramElegibleFamiliar.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.segundoNombre;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.segundoNombre;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 22))
      return paramElegibleFamiliar.segundoNombre;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 22, paramElegibleFamiliar.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 22, paramElegibleFamiliar.segundoNombre, paramString);
  }

  private static final String jdoGetsexo(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.sexo;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.sexo;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 23))
      return paramElegibleFamiliar.sexo;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 23, paramElegibleFamiliar.sexo);
  }

  private static final void jdoSetsexo(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.sexo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.sexo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 23, paramElegibleFamiliar.sexo, paramString);
  }

  private static final String jdoGettallaFranela(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.tallaFranela;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.tallaFranela;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 24))
      return paramElegibleFamiliar.tallaFranela;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 24, paramElegibleFamiliar.tallaFranela);
  }

  private static final void jdoSettallaFranela(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.tallaFranela = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.tallaFranela = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 24, paramElegibleFamiliar.tallaFranela, paramString);
  }

  private static final String jdoGettallaGorra(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.tallaGorra;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.tallaGorra;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 25))
      return paramElegibleFamiliar.tallaGorra;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 25, paramElegibleFamiliar.tallaGorra);
  }

  private static final void jdoSettallaGorra(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.tallaGorra = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.tallaGorra = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 25, paramElegibleFamiliar.tallaGorra, paramString);
  }

  private static final String jdoGettallaPantalon(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.tallaPantalon;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.tallaPantalon;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 26))
      return paramElegibleFamiliar.tallaPantalon;
    return localStateManager.getStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 26, paramElegibleFamiliar.tallaPantalon);
  }

  private static final void jdoSettallaPantalon(ElegibleFamiliar paramElegibleFamiliar, String paramString)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.tallaPantalon = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.tallaPantalon = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleFamiliar, jdoInheritedFieldCount + 26, paramElegibleFamiliar.tallaPantalon, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleFamiliar paramElegibleFamiliar)
  {
    if (paramElegibleFamiliar.jdoFlags <= 0)
      return paramElegibleFamiliar.tiempoSitp;
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleFamiliar.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleFamiliar, jdoInheritedFieldCount + 27))
      return paramElegibleFamiliar.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleFamiliar, jdoInheritedFieldCount + 27, paramElegibleFamiliar.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleFamiliar paramElegibleFamiliar, Date paramDate)
  {
    if (paramElegibleFamiliar.jdoFlags == 0)
    {
      paramElegibleFamiliar.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleFamiliar.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleFamiliar, jdoInheritedFieldCount + 27, paramElegibleFamiliar.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}