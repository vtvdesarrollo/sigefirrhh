package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.adiestramiento.AreaConocimiento;

public class ElegibleCertificacion
  implements Serializable, PersistenceCapable
{
  private long idElegibleCertificacion;
  private AreaConocimiento areaConocimiento;
  private String nombreCertificacion;
  private String nombreEntidad;
  private Date fechaCertificacion;
  private String vigencia;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "areaConocimiento", "elegible", "fechaCertificacion", "idElegibleCertificacion", "idSitp", "nombreCertificacion", "nombreEntidad", "tiempoSitp", "vigencia" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.adiestramiento.AreaConocimiento"), sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 26, 26, 21, 24, 21, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaCertificacion(this)) + " - " + 
      jdoGetnombreCertificacion(this);
  }

  public AreaConocimiento getAreaConocimiento()
  {
    return jdoGetareaConocimiento(this);
  }

  public void setAreaConocimiento(AreaConocimiento areaConocimiento)
  {
    jdoSetareaConocimiento(this, areaConocimiento);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public Date getFechaCertificacion()
  {
    return jdoGetfechaCertificacion(this);
  }

  public void setFechaCertificacion(Date fechaCertificacion)
  {
    jdoSetfechaCertificacion(this, fechaCertificacion);
  }

  public long getIdElegibleCertificacion()
  {
    return jdoGetidElegibleCertificacion(this);
  }

  public void setIdElegibleCertificacion(long idElegibleCertificacion)
  {
    jdoSetidElegibleCertificacion(this, idElegibleCertificacion);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNombreCertificacion()
  {
    return jdoGetnombreCertificacion(this);
  }

  public void setNombreCertificacion(String nombreCertificacion)
  {
    jdoSetnombreCertificacion(this, nombreCertificacion);
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public void setNombreEntidad(String nombreEntidad)
  {
    jdoSetnombreEntidad(this, nombreEntidad);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public String getVigencia()
  {
    return jdoGetvigencia(this);
  }

  public void setVigencia(String vigencia)
  {
    jdoSetvigencia(this, vigencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleCertificacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleCertificacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleCertificacion localElegibleCertificacion = new ElegibleCertificacion();
    localElegibleCertificacion.jdoFlags = 1;
    localElegibleCertificacion.jdoStateManager = paramStateManager;
    return localElegibleCertificacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleCertificacion localElegibleCertificacion = new ElegibleCertificacion();
    localElegibleCertificacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleCertificacion.jdoFlags = 1;
    localElegibleCertificacion.jdoStateManager = paramStateManager;
    return localElegibleCertificacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaConocimiento);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCertificacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleCertificacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCertificacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.vigencia);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaConocimiento = ((AreaConocimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCertificacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleCertificacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCertificacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.vigencia = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleCertificacion paramElegibleCertificacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.areaConocimiento = paramElegibleCertificacion.areaConocimiento;
      return;
    case 1:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleCertificacion.elegible;
      return;
    case 2:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCertificacion = paramElegibleCertificacion.fechaCertificacion;
      return;
    case 3:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleCertificacion = paramElegibleCertificacion.idElegibleCertificacion;
      return;
    case 4:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleCertificacion.idSitp;
      return;
    case 5:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCertificacion = paramElegibleCertificacion.nombreCertificacion;
      return;
    case 6:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramElegibleCertificacion.nombreEntidad;
      return;
    case 7:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleCertificacion.tiempoSitp;
      return;
    case 8:
      if (paramElegibleCertificacion == null)
        throw new IllegalArgumentException("arg1");
      this.vigencia = paramElegibleCertificacion.vigencia;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleCertificacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleCertificacion localElegibleCertificacion = (ElegibleCertificacion)paramObject;
    if (localElegibleCertificacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleCertificacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleCertificacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleCertificacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleCertificacionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleCertificacionPK localElegibleCertificacionPK = (ElegibleCertificacionPK)paramObject;
    localElegibleCertificacionPK.idElegibleCertificacion = this.idElegibleCertificacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleCertificacionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleCertificacionPK localElegibleCertificacionPK = (ElegibleCertificacionPK)paramObject;
    this.idElegibleCertificacion = localElegibleCertificacionPK.idElegibleCertificacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleCertificacionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleCertificacionPK localElegibleCertificacionPK = (ElegibleCertificacionPK)paramObject;
    localElegibleCertificacionPK.idElegibleCertificacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleCertificacionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleCertificacionPK localElegibleCertificacionPK = (ElegibleCertificacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localElegibleCertificacionPK.idElegibleCertificacion);
  }

  private static final AreaConocimiento jdoGetareaConocimiento(ElegibleCertificacion paramElegibleCertificacion)
  {
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.areaConocimiento;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 0))
      return paramElegibleCertificacion.areaConocimiento;
    return (AreaConocimiento)localStateManager.getObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 0, paramElegibleCertificacion.areaConocimiento);
  }

  private static final void jdoSetareaConocimiento(ElegibleCertificacion paramElegibleCertificacion, AreaConocimiento paramAreaConocimiento)
  {
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.areaConocimiento = paramAreaConocimiento;
      return;
    }
    localStateManager.setObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 0, paramElegibleCertificacion.areaConocimiento, paramAreaConocimiento);
  }

  private static final Elegible jdoGetelegible(ElegibleCertificacion paramElegibleCertificacion)
  {
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.elegible;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 1))
      return paramElegibleCertificacion.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 1, paramElegibleCertificacion.elegible);
  }

  private static final void jdoSetelegible(ElegibleCertificacion paramElegibleCertificacion, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 1, paramElegibleCertificacion.elegible, paramElegible);
  }

  private static final Date jdoGetfechaCertificacion(ElegibleCertificacion paramElegibleCertificacion)
  {
    if (paramElegibleCertificacion.jdoFlags <= 0)
      return paramElegibleCertificacion.fechaCertificacion;
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.fechaCertificacion;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 2))
      return paramElegibleCertificacion.fechaCertificacion;
    return (Date)localStateManager.getObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 2, paramElegibleCertificacion.fechaCertificacion);
  }

  private static final void jdoSetfechaCertificacion(ElegibleCertificacion paramElegibleCertificacion, Date paramDate)
  {
    if (paramElegibleCertificacion.jdoFlags == 0)
    {
      paramElegibleCertificacion.fechaCertificacion = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.fechaCertificacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 2, paramElegibleCertificacion.fechaCertificacion, paramDate);
  }

  private static final long jdoGetidElegibleCertificacion(ElegibleCertificacion paramElegibleCertificacion)
  {
    return paramElegibleCertificacion.idElegibleCertificacion;
  }

  private static final void jdoSetidElegibleCertificacion(ElegibleCertificacion paramElegibleCertificacion, long paramLong)
  {
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.idElegibleCertificacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleCertificacion, jdoInheritedFieldCount + 3, paramElegibleCertificacion.idElegibleCertificacion, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleCertificacion paramElegibleCertificacion)
  {
    if (paramElegibleCertificacion.jdoFlags <= 0)
      return paramElegibleCertificacion.idSitp;
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.idSitp;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 4))
      return paramElegibleCertificacion.idSitp;
    return localStateManager.getIntField(paramElegibleCertificacion, jdoInheritedFieldCount + 4, paramElegibleCertificacion.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleCertificacion paramElegibleCertificacion, int paramInt)
  {
    if (paramElegibleCertificacion.jdoFlags == 0)
    {
      paramElegibleCertificacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleCertificacion, jdoInheritedFieldCount + 4, paramElegibleCertificacion.idSitp, paramInt);
  }

  private static final String jdoGetnombreCertificacion(ElegibleCertificacion paramElegibleCertificacion)
  {
    if (paramElegibleCertificacion.jdoFlags <= 0)
      return paramElegibleCertificacion.nombreCertificacion;
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.nombreCertificacion;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 5))
      return paramElegibleCertificacion.nombreCertificacion;
    return localStateManager.getStringField(paramElegibleCertificacion, jdoInheritedFieldCount + 5, paramElegibleCertificacion.nombreCertificacion);
  }

  private static final void jdoSetnombreCertificacion(ElegibleCertificacion paramElegibleCertificacion, String paramString)
  {
    if (paramElegibleCertificacion.jdoFlags == 0)
    {
      paramElegibleCertificacion.nombreCertificacion = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.nombreCertificacion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleCertificacion, jdoInheritedFieldCount + 5, paramElegibleCertificacion.nombreCertificacion, paramString);
  }

  private static final String jdoGetnombreEntidad(ElegibleCertificacion paramElegibleCertificacion)
  {
    if (paramElegibleCertificacion.jdoFlags <= 0)
      return paramElegibleCertificacion.nombreEntidad;
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.nombreEntidad;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 6))
      return paramElegibleCertificacion.nombreEntidad;
    return localStateManager.getStringField(paramElegibleCertificacion, jdoInheritedFieldCount + 6, paramElegibleCertificacion.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(ElegibleCertificacion paramElegibleCertificacion, String paramString)
  {
    if (paramElegibleCertificacion.jdoFlags == 0)
    {
      paramElegibleCertificacion.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleCertificacion, jdoInheritedFieldCount + 6, paramElegibleCertificacion.nombreEntidad, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleCertificacion paramElegibleCertificacion)
  {
    if (paramElegibleCertificacion.jdoFlags <= 0)
      return paramElegibleCertificacion.tiempoSitp;
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 7))
      return paramElegibleCertificacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 7, paramElegibleCertificacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleCertificacion paramElegibleCertificacion, Date paramDate)
  {
    if (paramElegibleCertificacion.jdoFlags == 0)
    {
      paramElegibleCertificacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleCertificacion, jdoInheritedFieldCount + 7, paramElegibleCertificacion.tiempoSitp, paramDate);
  }

  private static final String jdoGetvigencia(ElegibleCertificacion paramElegibleCertificacion)
  {
    if (paramElegibleCertificacion.jdoFlags <= 0)
      return paramElegibleCertificacion.vigencia;
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleCertificacion.vigencia;
    if (localStateManager.isLoaded(paramElegibleCertificacion, jdoInheritedFieldCount + 8))
      return paramElegibleCertificacion.vigencia;
    return localStateManager.getStringField(paramElegibleCertificacion, jdoInheritedFieldCount + 8, paramElegibleCertificacion.vigencia);
  }

  private static final void jdoSetvigencia(ElegibleCertificacion paramElegibleCertificacion, String paramString)
  {
    if (paramElegibleCertificacion.jdoFlags == 0)
    {
      paramElegibleCertificacion.vigencia = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleCertificacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleCertificacion.vigencia = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleCertificacion, jdoInheritedFieldCount + 8, paramElegibleCertificacion.vigencia, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}