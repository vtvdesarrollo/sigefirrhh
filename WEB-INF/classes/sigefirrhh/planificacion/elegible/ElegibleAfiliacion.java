package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Gremio;

public class ElegibleAfiliacion
  implements Serializable, PersistenceCapable
{
  private long idElegibleAfiliacion;
  private Gremio gremio;
  private Date fecha;
  private String numeroAfiliacion;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "elegible", "fecha", "gremio", "idElegibleAfiliacion", "idSitp", "numeroAfiliacion", "tiempoSitp" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.Gremio"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetgremio(this).getNombre() + " " + 
      jdoGetnumeroAfiliacion(this);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public void setFecha(Date fecha)
  {
    jdoSetfecha(this, fecha);
  }

  public Gremio getGremio()
  {
    return jdoGetgremio(this);
  }

  public void setGremio(Gremio gremio)
  {
    jdoSetgremio(this, gremio);
  }

  public long getIdElegibleAfiliacion()
  {
    return jdoGetidElegibleAfiliacion(this);
  }

  public void setIdElegibleAfiliacion(long idElegibleAfiliacion)
  {
    jdoSetidElegibleAfiliacion(this, idElegibleAfiliacion);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNumeroAfiliacion()
  {
    return jdoGetnumeroAfiliacion(this);
  }

  public void setNumeroAfiliacion(String numeroAfiliacion)
  {
    jdoSetnumeroAfiliacion(this, numeroAfiliacion);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleAfiliacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleAfiliacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleAfiliacion localElegibleAfiliacion = new ElegibleAfiliacion();
    localElegibleAfiliacion.jdoFlags = 1;
    localElegibleAfiliacion.jdoStateManager = paramStateManager;
    return localElegibleAfiliacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleAfiliacion localElegibleAfiliacion = new ElegibleAfiliacion();
    localElegibleAfiliacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleAfiliacion.jdoFlags = 1;
    localElegibleAfiliacion.jdoStateManager = paramStateManager;
    return localElegibleAfiliacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.gremio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleAfiliacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroAfiliacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gremio = ((Gremio)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleAfiliacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroAfiliacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleAfiliacion paramElegibleAfiliacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleAfiliacion.elegible;
      return;
    case 1:
      if (paramElegibleAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramElegibleAfiliacion.fecha;
      return;
    case 2:
      if (paramElegibleAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.gremio = paramElegibleAfiliacion.gremio;
      return;
    case 3:
      if (paramElegibleAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleAfiliacion = paramElegibleAfiliacion.idElegibleAfiliacion;
      return;
    case 4:
      if (paramElegibleAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleAfiliacion.idSitp;
      return;
    case 5:
      if (paramElegibleAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.numeroAfiliacion = paramElegibleAfiliacion.numeroAfiliacion;
      return;
    case 6:
      if (paramElegibleAfiliacion == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleAfiliacion.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleAfiliacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleAfiliacion localElegibleAfiliacion = (ElegibleAfiliacion)paramObject;
    if (localElegibleAfiliacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleAfiliacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleAfiliacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleAfiliacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleAfiliacionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleAfiliacionPK localElegibleAfiliacionPK = (ElegibleAfiliacionPK)paramObject;
    localElegibleAfiliacionPK.idElegibleAfiliacion = this.idElegibleAfiliacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleAfiliacionPK))
      throw new IllegalArgumentException("arg1");
    ElegibleAfiliacionPK localElegibleAfiliacionPK = (ElegibleAfiliacionPK)paramObject;
    this.idElegibleAfiliacion = localElegibleAfiliacionPK.idElegibleAfiliacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleAfiliacionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleAfiliacionPK localElegibleAfiliacionPK = (ElegibleAfiliacionPK)paramObject;
    localElegibleAfiliacionPK.idElegibleAfiliacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleAfiliacionPK))
      throw new IllegalArgumentException("arg2");
    ElegibleAfiliacionPK localElegibleAfiliacionPK = (ElegibleAfiliacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localElegibleAfiliacionPK.idElegibleAfiliacion);
  }

  private static final Elegible jdoGetelegible(ElegibleAfiliacion paramElegibleAfiliacion)
  {
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAfiliacion.elegible;
    if (localStateManager.isLoaded(paramElegibleAfiliacion, jdoInheritedFieldCount + 0))
      return paramElegibleAfiliacion.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 0, paramElegibleAfiliacion.elegible);
  }

  private static final void jdoSetelegible(ElegibleAfiliacion paramElegibleAfiliacion, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAfiliacion.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 0, paramElegibleAfiliacion.elegible, paramElegible);
  }

  private static final Date jdoGetfecha(ElegibleAfiliacion paramElegibleAfiliacion)
  {
    if (paramElegibleAfiliacion.jdoFlags <= 0)
      return paramElegibleAfiliacion.fecha;
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAfiliacion.fecha;
    if (localStateManager.isLoaded(paramElegibleAfiliacion, jdoInheritedFieldCount + 1))
      return paramElegibleAfiliacion.fecha;
    return (Date)localStateManager.getObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 1, paramElegibleAfiliacion.fecha);
  }

  private static final void jdoSetfecha(ElegibleAfiliacion paramElegibleAfiliacion, Date paramDate)
  {
    if (paramElegibleAfiliacion.jdoFlags == 0)
    {
      paramElegibleAfiliacion.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAfiliacion.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 1, paramElegibleAfiliacion.fecha, paramDate);
  }

  private static final Gremio jdoGetgremio(ElegibleAfiliacion paramElegibleAfiliacion)
  {
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAfiliacion.gremio;
    if (localStateManager.isLoaded(paramElegibleAfiliacion, jdoInheritedFieldCount + 2))
      return paramElegibleAfiliacion.gremio;
    return (Gremio)localStateManager.getObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 2, paramElegibleAfiliacion.gremio);
  }

  private static final void jdoSetgremio(ElegibleAfiliacion paramElegibleAfiliacion, Gremio paramGremio)
  {
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAfiliacion.gremio = paramGremio;
      return;
    }
    localStateManager.setObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 2, paramElegibleAfiliacion.gremio, paramGremio);
  }

  private static final long jdoGetidElegibleAfiliacion(ElegibleAfiliacion paramElegibleAfiliacion)
  {
    return paramElegibleAfiliacion.idElegibleAfiliacion;
  }

  private static final void jdoSetidElegibleAfiliacion(ElegibleAfiliacion paramElegibleAfiliacion, long paramLong)
  {
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAfiliacion.idElegibleAfiliacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleAfiliacion, jdoInheritedFieldCount + 3, paramElegibleAfiliacion.idElegibleAfiliacion, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleAfiliacion paramElegibleAfiliacion)
  {
    if (paramElegibleAfiliacion.jdoFlags <= 0)
      return paramElegibleAfiliacion.idSitp;
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAfiliacion.idSitp;
    if (localStateManager.isLoaded(paramElegibleAfiliacion, jdoInheritedFieldCount + 4))
      return paramElegibleAfiliacion.idSitp;
    return localStateManager.getIntField(paramElegibleAfiliacion, jdoInheritedFieldCount + 4, paramElegibleAfiliacion.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleAfiliacion paramElegibleAfiliacion, int paramInt)
  {
    if (paramElegibleAfiliacion.jdoFlags == 0)
    {
      paramElegibleAfiliacion.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAfiliacion.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleAfiliacion, jdoInheritedFieldCount + 4, paramElegibleAfiliacion.idSitp, paramInt);
  }

  private static final String jdoGetnumeroAfiliacion(ElegibleAfiliacion paramElegibleAfiliacion)
  {
    if (paramElegibleAfiliacion.jdoFlags <= 0)
      return paramElegibleAfiliacion.numeroAfiliacion;
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAfiliacion.numeroAfiliacion;
    if (localStateManager.isLoaded(paramElegibleAfiliacion, jdoInheritedFieldCount + 5))
      return paramElegibleAfiliacion.numeroAfiliacion;
    return localStateManager.getStringField(paramElegibleAfiliacion, jdoInheritedFieldCount + 5, paramElegibleAfiliacion.numeroAfiliacion);
  }

  private static final void jdoSetnumeroAfiliacion(ElegibleAfiliacion paramElegibleAfiliacion, String paramString)
  {
    if (paramElegibleAfiliacion.jdoFlags == 0)
    {
      paramElegibleAfiliacion.numeroAfiliacion = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAfiliacion.numeroAfiliacion = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleAfiliacion, jdoInheritedFieldCount + 5, paramElegibleAfiliacion.numeroAfiliacion, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleAfiliacion paramElegibleAfiliacion)
  {
    if (paramElegibleAfiliacion.jdoFlags <= 0)
      return paramElegibleAfiliacion.tiempoSitp;
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleAfiliacion.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleAfiliacion, jdoInheritedFieldCount + 6))
      return paramElegibleAfiliacion.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 6, paramElegibleAfiliacion.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleAfiliacion paramElegibleAfiliacion, Date paramDate)
  {
    if (paramElegibleAfiliacion.jdoFlags == 0)
    {
      paramElegibleAfiliacion.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleAfiliacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleAfiliacion.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleAfiliacion, jdoInheritedFieldCount + 6, paramElegibleAfiliacion.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}