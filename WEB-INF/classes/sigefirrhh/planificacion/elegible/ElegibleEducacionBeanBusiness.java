package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.Carrera;
import sigefirrhh.base.personal.CarreraBeanBusiness;
import sigefirrhh.base.personal.NivelEducativo;
import sigefirrhh.base.personal.NivelEducativoBeanBusiness;
import sigefirrhh.base.personal.Titulo;
import sigefirrhh.base.personal.TituloBeanBusiness;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class ElegibleEducacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleEducacion(ElegibleEducacion elegibleEducacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleEducacion elegibleEducacionNew = 
      (ElegibleEducacion)BeanUtils.cloneBean(
      elegibleEducacion);

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (elegibleEducacionNew.getNivelEducativo() != null) {
      elegibleEducacionNew.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        elegibleEducacionNew.getNivelEducativo().getIdNivelEducativo()));
    }

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (elegibleEducacionNew.getCarrera() != null) {
      elegibleEducacionNew.setCarrera(
        carreraBeanBusiness.findCarreraById(
        elegibleEducacionNew.getCarrera().getIdCarrera()));
    }

    TituloBeanBusiness tituloBeanBusiness = new TituloBeanBusiness();

    if (elegibleEducacionNew.getTitulo() != null) {
      elegibleEducacionNew.setTitulo(
        tituloBeanBusiness.findTituloById(
        elegibleEducacionNew.getTitulo().getIdTitulo()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (elegibleEducacionNew.getCiudad() != null) {
      elegibleEducacionNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        elegibleEducacionNew.getCiudad().getIdCiudad()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleEducacionNew.getElegible() != null) {
      elegibleEducacionNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleEducacionNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleEducacionNew);
  }

  public void updateElegibleEducacion(ElegibleEducacion elegibleEducacion) throws Exception
  {
    ElegibleEducacion elegibleEducacionModify = 
      findElegibleEducacionById(elegibleEducacion.getIdElegibleEducacion());

    NivelEducativoBeanBusiness nivelEducativoBeanBusiness = new NivelEducativoBeanBusiness();

    if (elegibleEducacion.getNivelEducativo() != null) {
      elegibleEducacion.setNivelEducativo(
        nivelEducativoBeanBusiness.findNivelEducativoById(
        elegibleEducacion.getNivelEducativo().getIdNivelEducativo()));
    }

    CarreraBeanBusiness carreraBeanBusiness = new CarreraBeanBusiness();

    if (elegibleEducacion.getCarrera() != null) {
      elegibleEducacion.setCarrera(
        carreraBeanBusiness.findCarreraById(
        elegibleEducacion.getCarrera().getIdCarrera()));
    }

    TituloBeanBusiness tituloBeanBusiness = new TituloBeanBusiness();

    if (elegibleEducacion.getTitulo() != null) {
      elegibleEducacion.setTitulo(
        tituloBeanBusiness.findTituloById(
        elegibleEducacion.getTitulo().getIdTitulo()));
    }

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (elegibleEducacion.getCiudad() != null) {
      elegibleEducacion.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        elegibleEducacion.getCiudad().getIdCiudad()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleEducacion.getElegible() != null) {
      elegibleEducacion.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleEducacion.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleEducacionModify, elegibleEducacion);
  }

  public void deleteElegibleEducacion(ElegibleEducacion elegibleEducacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleEducacion elegibleEducacionDelete = 
      findElegibleEducacionById(elegibleEducacion.getIdElegibleEducacion());
    pm.deletePersistent(elegibleEducacionDelete);
  }

  public ElegibleEducacion findElegibleEducacionById(long idElegibleEducacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleEducacion == pIdElegibleEducacion";
    Query query = pm.newQuery(ElegibleEducacion.class, filter);

    query.declareParameters("long pIdElegibleEducacion");

    parameters.put("pIdElegibleEducacion", new Long(idElegibleEducacion));

    Collection colElegibleEducacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleEducacion.iterator();
    return (ElegibleEducacion)iterator.next();
  }

  public Collection findElegibleEducacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleEducacionExtent = pm.getExtent(
      ElegibleEducacion.class, true);
    Query query = pm.newQuery(elegibleEducacionExtent);
    query.setOrdering("anioInicio ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleEducacion.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("anioInicio ascending");

    Collection colElegibleEducacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleEducacion);

    return colElegibleEducacion;
  }
}