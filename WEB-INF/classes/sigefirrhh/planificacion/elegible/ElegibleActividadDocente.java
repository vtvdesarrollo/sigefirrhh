package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.Carrera;

public class ElegibleActividadDocente
  implements Serializable, PersistenceCapable
{
  private long idElegibleActividadDocente;
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SECTOR;
  private String nivelEducativo;
  private int anioInicio;
  private int anioFin;
  private String estatus;
  private String sector;
  private String nombreEntidad;
  private String asignatura;
  private String relacionLaboral;
  private Carrera carrera;
  private String observaciones;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anioFin", "anioInicio", "asignatura", "carrera", "elegible", "estatus", "idElegibleActividadDocente", "idSitp", "nivelEducativo", "nombreEntidad", "observaciones", "relacionLaboral", "sector", "tiempoSitp" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.Carrera"), sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 26, 21, 24, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleActividadDocente"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleActividadDocente());

    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_SECTOR = 
      new LinkedHashMap();

    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TECNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TECNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
    LISTA_NIVEL_EDUCATIVO.put("N", "NINGUNO");
    LISTA_ESTATUS.put("S", "SI");
    LISTA_ESTATUS.put("N", "NO");
    LISTA_SECTOR.put("P", "PUBLICO");
    LISTA_SECTOR.put("U", "PRIVADO");
    LISTA_SECTOR.put("E", "EXTERIOR");
  }

  public ElegibleActividadDocente()
  {
    jdoSetnivelEducativo(this, "N");

    jdoSetestatus(this, "N");

    jdoSetsector(this, "P");
  }

  public String toString()
  {
    return jdoGetnombreEntidad(this) + " " + 
      jdoGetestatus(this);
  }

  public int getAnioFin()
  {
    return jdoGetanioFin(this);
  }

  public void setAnioFin(int anioFin)
  {
    jdoSetanioFin(this, anioFin);
  }

  public int getAnioInicio()
  {
    return jdoGetanioInicio(this);
  }

  public void setAnioInicio(int anioInicio)
  {
    jdoSetanioInicio(this, anioInicio);
  }

  public String getAsignatura()
  {
    return jdoGetasignatura(this);
  }

  public void setAsignatura(String asignatura)
  {
    jdoSetasignatura(this, asignatura);
  }

  public Carrera getCarrera()
  {
    return jdoGetcarrera(this);
  }

  public void setCarrera(Carrera carrera)
  {
    jdoSetcarrera(this, carrera);
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus)
  {
    jdoSetestatus(this, estatus);
  }

  public long getIdElegibleActividadDocente()
  {
    return jdoGetidElegibleActividadDocente(this);
  }

  public void setIdElegibleActividadDocente(long idElegibleActividadDocente)
  {
    jdoSetidElegibleActividadDocente(this, idElegibleActividadDocente);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public void setNivelEducativo(String nivelEducativo)
  {
    jdoSetnivelEducativo(this, nivelEducativo);
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public void setNombreEntidad(String nombreEntidad)
  {
    jdoSetnombreEntidad(this, nombreEntidad);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public String getRelacionLaboral()
  {
    return jdoGetrelacionLaboral(this);
  }

  public void setRelacionLaboral(String relacionLaboral)
  {
    jdoSetrelacionLaboral(this, relacionLaboral);
  }

  public String getSector()
  {
    return jdoGetsector(this);
  }

  public void setSector(String sector)
  {
    jdoSetsector(this, sector);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleActividadDocente localElegibleActividadDocente = new ElegibleActividadDocente();
    localElegibleActividadDocente.jdoFlags = 1;
    localElegibleActividadDocente.jdoStateManager = paramStateManager;
    return localElegibleActividadDocente;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleActividadDocente localElegibleActividadDocente = new ElegibleActividadDocente();
    localElegibleActividadDocente.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleActividadDocente.jdoFlags = 1;
    localElegibleActividadDocente.jdoStateManager = paramStateManager;
    return localElegibleActividadDocente;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioFin);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioInicio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.asignatura);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.carrera);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleActividadDocente);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.relacionLaboral);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sector);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioFin = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioInicio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asignatura = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.carrera = ((Carrera)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleActividadDocente = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.relacionLaboral = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sector = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleActividadDocente paramElegibleActividadDocente, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.anioFin = paramElegibleActividadDocente.anioFin;
      return;
    case 1:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.anioInicio = paramElegibleActividadDocente.anioInicio;
      return;
    case 2:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.asignatura = paramElegibleActividadDocente.asignatura;
      return;
    case 3:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.carrera = paramElegibleActividadDocente.carrera;
      return;
    case 4:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleActividadDocente.elegible;
      return;
    case 5:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramElegibleActividadDocente.estatus;
      return;
    case 6:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleActividadDocente = paramElegibleActividadDocente.idElegibleActividadDocente;
      return;
    case 7:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleActividadDocente.idSitp;
      return;
    case 8:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramElegibleActividadDocente.nivelEducativo;
      return;
    case 9:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramElegibleActividadDocente.nombreEntidad;
      return;
    case 10:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramElegibleActividadDocente.observaciones;
      return;
    case 11:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.relacionLaboral = paramElegibleActividadDocente.relacionLaboral;
      return;
    case 12:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.sector = paramElegibleActividadDocente.sector;
      return;
    case 13:
      if (paramElegibleActividadDocente == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleActividadDocente.tiempoSitp;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleActividadDocente))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleActividadDocente localElegibleActividadDocente = (ElegibleActividadDocente)paramObject;
    if (localElegibleActividadDocente.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleActividadDocente, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleActividadDocentePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleActividadDocentePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleActividadDocentePK))
      throw new IllegalArgumentException("arg1");
    ElegibleActividadDocentePK localElegibleActividadDocentePK = (ElegibleActividadDocentePK)paramObject;
    localElegibleActividadDocentePK.idElegibleActividadDocente = this.idElegibleActividadDocente;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleActividadDocentePK))
      throw new IllegalArgumentException("arg1");
    ElegibleActividadDocentePK localElegibleActividadDocentePK = (ElegibleActividadDocentePK)paramObject;
    this.idElegibleActividadDocente = localElegibleActividadDocentePK.idElegibleActividadDocente;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleActividadDocentePK))
      throw new IllegalArgumentException("arg2");
    ElegibleActividadDocentePK localElegibleActividadDocentePK = (ElegibleActividadDocentePK)paramObject;
    localElegibleActividadDocentePK.idElegibleActividadDocente = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleActividadDocentePK))
      throw new IllegalArgumentException("arg2");
    ElegibleActividadDocentePK localElegibleActividadDocentePK = (ElegibleActividadDocentePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localElegibleActividadDocentePK.idElegibleActividadDocente);
  }

  private static final int jdoGetanioFin(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.anioFin;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.anioFin;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 0))
      return paramElegibleActividadDocente.anioFin;
    return localStateManager.getIntField(paramElegibleActividadDocente, jdoInheritedFieldCount + 0, paramElegibleActividadDocente.anioFin);
  }

  private static final void jdoSetanioFin(ElegibleActividadDocente paramElegibleActividadDocente, int paramInt)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.anioFin = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.anioFin = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleActividadDocente, jdoInheritedFieldCount + 0, paramElegibleActividadDocente.anioFin, paramInt);
  }

  private static final int jdoGetanioInicio(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.anioInicio;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.anioInicio;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 1))
      return paramElegibleActividadDocente.anioInicio;
    return localStateManager.getIntField(paramElegibleActividadDocente, jdoInheritedFieldCount + 1, paramElegibleActividadDocente.anioInicio);
  }

  private static final void jdoSetanioInicio(ElegibleActividadDocente paramElegibleActividadDocente, int paramInt)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.anioInicio = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.anioInicio = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleActividadDocente, jdoInheritedFieldCount + 1, paramElegibleActividadDocente.anioInicio, paramInt);
  }

  private static final String jdoGetasignatura(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.asignatura;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.asignatura;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 2))
      return paramElegibleActividadDocente.asignatura;
    return localStateManager.getStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 2, paramElegibleActividadDocente.asignatura);
  }

  private static final void jdoSetasignatura(ElegibleActividadDocente paramElegibleActividadDocente, String paramString)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.asignatura = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.asignatura = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 2, paramElegibleActividadDocente.asignatura, paramString);
  }

  private static final Carrera jdoGetcarrera(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.carrera;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 3))
      return paramElegibleActividadDocente.carrera;
    return (Carrera)localStateManager.getObjectField(paramElegibleActividadDocente, jdoInheritedFieldCount + 3, paramElegibleActividadDocente.carrera);
  }

  private static final void jdoSetcarrera(ElegibleActividadDocente paramElegibleActividadDocente, Carrera paramCarrera)
  {
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.carrera = paramCarrera;
      return;
    }
    localStateManager.setObjectField(paramElegibleActividadDocente, jdoInheritedFieldCount + 3, paramElegibleActividadDocente.carrera, paramCarrera);
  }

  private static final Elegible jdoGetelegible(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.elegible;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 4))
      return paramElegibleActividadDocente.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleActividadDocente, jdoInheritedFieldCount + 4, paramElegibleActividadDocente.elegible);
  }

  private static final void jdoSetelegible(ElegibleActividadDocente paramElegibleActividadDocente, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleActividadDocente, jdoInheritedFieldCount + 4, paramElegibleActividadDocente.elegible, paramElegible);
  }

  private static final String jdoGetestatus(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.estatus;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.estatus;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 5))
      return paramElegibleActividadDocente.estatus;
    return localStateManager.getStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 5, paramElegibleActividadDocente.estatus);
  }

  private static final void jdoSetestatus(ElegibleActividadDocente paramElegibleActividadDocente, String paramString)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 5, paramElegibleActividadDocente.estatus, paramString);
  }

  private static final long jdoGetidElegibleActividadDocente(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    return paramElegibleActividadDocente.idElegibleActividadDocente;
  }

  private static final void jdoSetidElegibleActividadDocente(ElegibleActividadDocente paramElegibleActividadDocente, long paramLong)
  {
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.idElegibleActividadDocente = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleActividadDocente, jdoInheritedFieldCount + 6, paramElegibleActividadDocente.idElegibleActividadDocente, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.idSitp;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.idSitp;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 7))
      return paramElegibleActividadDocente.idSitp;
    return localStateManager.getIntField(paramElegibleActividadDocente, jdoInheritedFieldCount + 7, paramElegibleActividadDocente.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleActividadDocente paramElegibleActividadDocente, int paramInt)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleActividadDocente, jdoInheritedFieldCount + 7, paramElegibleActividadDocente.idSitp, paramInt);
  }

  private static final String jdoGetnivelEducativo(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.nivelEducativo;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.nivelEducativo;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 8))
      return paramElegibleActividadDocente.nivelEducativo;
    return localStateManager.getStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 8, paramElegibleActividadDocente.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(ElegibleActividadDocente paramElegibleActividadDocente, String paramString)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 8, paramElegibleActividadDocente.nivelEducativo, paramString);
  }

  private static final String jdoGetnombreEntidad(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.nombreEntidad;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.nombreEntidad;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 9))
      return paramElegibleActividadDocente.nombreEntidad;
    return localStateManager.getStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 9, paramElegibleActividadDocente.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(ElegibleActividadDocente paramElegibleActividadDocente, String paramString)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 9, paramElegibleActividadDocente.nombreEntidad, paramString);
  }

  private static final String jdoGetobservaciones(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.observaciones;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.observaciones;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 10))
      return paramElegibleActividadDocente.observaciones;
    return localStateManager.getStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 10, paramElegibleActividadDocente.observaciones);
  }

  private static final void jdoSetobservaciones(ElegibleActividadDocente paramElegibleActividadDocente, String paramString)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 10, paramElegibleActividadDocente.observaciones, paramString);
  }

  private static final String jdoGetrelacionLaboral(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.relacionLaboral;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.relacionLaboral;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 11))
      return paramElegibleActividadDocente.relacionLaboral;
    return localStateManager.getStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 11, paramElegibleActividadDocente.relacionLaboral);
  }

  private static final void jdoSetrelacionLaboral(ElegibleActividadDocente paramElegibleActividadDocente, String paramString)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.relacionLaboral = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.relacionLaboral = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 11, paramElegibleActividadDocente.relacionLaboral, paramString);
  }

  private static final String jdoGetsector(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.sector;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.sector;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 12))
      return paramElegibleActividadDocente.sector;
    return localStateManager.getStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 12, paramElegibleActividadDocente.sector);
  }

  private static final void jdoSetsector(ElegibleActividadDocente paramElegibleActividadDocente, String paramString)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.sector = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.sector = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleActividadDocente, jdoInheritedFieldCount + 12, paramElegibleActividadDocente.sector, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleActividadDocente paramElegibleActividadDocente)
  {
    if (paramElegibleActividadDocente.jdoFlags <= 0)
      return paramElegibleActividadDocente.tiempoSitp;
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleActividadDocente.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleActividadDocente, jdoInheritedFieldCount + 13))
      return paramElegibleActividadDocente.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleActividadDocente, jdoInheritedFieldCount + 13, paramElegibleActividadDocente.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleActividadDocente paramElegibleActividadDocente, Date paramDate)
  {
    if (paramElegibleActividadDocente.jdoFlags == 0)
    {
      paramElegibleActividadDocente.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleActividadDocente.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleActividadDocente.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleActividadDocente, jdoInheritedFieldCount + 13, paramElegibleActividadDocente.tiempoSitp, paramDate);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}