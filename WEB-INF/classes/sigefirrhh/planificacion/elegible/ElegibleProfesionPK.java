package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleProfesionPK
  implements Serializable
{
  public long idElegibleProfesion;

  public ElegibleProfesionPK()
  {
  }

  public ElegibleProfesionPK(long idElegibleProfesion)
  {
    this.idElegibleProfesion = idElegibleProfesion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleProfesionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleProfesionPK thatPK)
  {
    return 
      this.idElegibleProfesion == thatPK.idElegibleProfesion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleProfesion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleProfesion);
  }
}