package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleAntecedentePK
  implements Serializable
{
  public long idElegibleAntecedente;

  public ElegibleAntecedentePK()
  {
  }

  public ElegibleAntecedentePK(long idElegibleAntecedente)
  {
    this.idElegibleAntecedente = idElegibleAntecedente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleAntecedentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleAntecedentePK thatPK)
  {
    return 
      this.idElegibleAntecedente == thatPK.idElegibleAntecedente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleAntecedente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleAntecedente);
  }
}