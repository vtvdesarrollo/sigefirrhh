package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleActividadDocentePK
  implements Serializable
{
  public long idElegibleActividadDocente;

  public ElegibleActividadDocentePK()
  {
  }

  public ElegibleActividadDocentePK(long idElegibleActividadDocente)
  {
    this.idElegibleActividadDocente = idElegibleActividadDocente;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleActividadDocentePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleActividadDocentePK thatPK)
  {
    return 
      this.idElegibleActividadDocente == thatPK.idElegibleActividadDocente;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleActividadDocente)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleActividadDocente);
  }
}