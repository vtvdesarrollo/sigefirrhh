package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class ElegiblePublicacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegiblePublicacion(ElegiblePublicacion elegiblePublicacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegiblePublicacion elegiblePublicacionNew = 
      (ElegiblePublicacion)BeanUtils.cloneBean(
      elegiblePublicacion);

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegiblePublicacionNew.getElegible() != null) {
      elegiblePublicacionNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegiblePublicacionNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegiblePublicacionNew);
  }

  public void updateElegiblePublicacion(ElegiblePublicacion elegiblePublicacion) throws Exception
  {
    ElegiblePublicacion elegiblePublicacionModify = 
      findElegiblePublicacionById(elegiblePublicacion.getIdElegiblePublicacion());

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegiblePublicacion.getElegible() != null) {
      elegiblePublicacion.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegiblePublicacion.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegiblePublicacionModify, elegiblePublicacion);
  }

  public void deleteElegiblePublicacion(ElegiblePublicacion elegiblePublicacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegiblePublicacion elegiblePublicacionDelete = 
      findElegiblePublicacionById(elegiblePublicacion.getIdElegiblePublicacion());
    pm.deletePersistent(elegiblePublicacionDelete);
  }

  public ElegiblePublicacion findElegiblePublicacionById(long idElegiblePublicacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegiblePublicacion == pIdElegiblePublicacion";
    Query query = pm.newQuery(ElegiblePublicacion.class, filter);

    query.declareParameters("long pIdElegiblePublicacion");

    parameters.put("pIdElegiblePublicacion", new Long(idElegiblePublicacion));

    Collection colElegiblePublicacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegiblePublicacion.iterator();
    return (ElegiblePublicacion)iterator.next();
  }

  public Collection findElegiblePublicacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegiblePublicacionExtent = pm.getExtent(
      ElegiblePublicacion.class, true);
    Query query = pm.newQuery(elegiblePublicacionExtent);
    query.setOrdering("anioPublicacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegiblePublicacion.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("anioPublicacion ascending");

    Collection colElegiblePublicacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegiblePublicacion);

    return colElegiblePublicacion;
  }
}