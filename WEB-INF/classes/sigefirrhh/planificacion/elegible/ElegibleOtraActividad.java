package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoOtraActividad;

public class ElegibleOtraActividad
  implements Serializable, PersistenceCapable
{
  private long idElegibleOtraActividad;
  private TipoOtraActividad tipoOtraActividad;
  private String observaciones;
  private Elegible elegible;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "elegible", "idElegibleOtraActividad", "observaciones", "tipoOtraActividad" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.TipoOtraActividad") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGettipoOtraActividad(this).getDescripcion();
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public long getIdElegibleOtraActividad()
  {
    return jdoGetidElegibleOtraActividad(this);
  }

  public void setIdElegibleOtraActividad(long idElegibleOtraActividad)
  {
    jdoSetidElegibleOtraActividad(this, idElegibleOtraActividad);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public TipoOtraActividad getTipoOtraActividad()
  {
    return jdoGettipoOtraActividad(this);
  }

  public void setTipoOtraActividad(TipoOtraActividad tipoOtraActividad)
  {
    jdoSettipoOtraActividad(this, tipoOtraActividad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleOtraActividad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleOtraActividad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleOtraActividad localElegibleOtraActividad = new ElegibleOtraActividad();
    localElegibleOtraActividad.jdoFlags = 1;
    localElegibleOtraActividad.jdoStateManager = paramStateManager;
    return localElegibleOtraActividad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleOtraActividad localElegibleOtraActividad = new ElegibleOtraActividad();
    localElegibleOtraActividad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleOtraActividad.jdoFlags = 1;
    localElegibleOtraActividad.jdoStateManager = paramStateManager;
    return localElegibleOtraActividad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleOtraActividad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoOtraActividad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleOtraActividad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoOtraActividad = ((TipoOtraActividad)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleOtraActividad paramElegibleOtraActividad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleOtraActividad.elegible;
      return;
    case 1:
      if (paramElegibleOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleOtraActividad = paramElegibleOtraActividad.idElegibleOtraActividad;
      return;
    case 2:
      if (paramElegibleOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramElegibleOtraActividad.observaciones;
      return;
    case 3:
      if (paramElegibleOtraActividad == null)
        throw new IllegalArgumentException("arg1");
      this.tipoOtraActividad = paramElegibleOtraActividad.tipoOtraActividad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleOtraActividad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleOtraActividad localElegibleOtraActividad = (ElegibleOtraActividad)paramObject;
    if (localElegibleOtraActividad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleOtraActividad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleOtraActividadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleOtraActividadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleOtraActividadPK))
      throw new IllegalArgumentException("arg1");
    ElegibleOtraActividadPK localElegibleOtraActividadPK = (ElegibleOtraActividadPK)paramObject;
    localElegibleOtraActividadPK.idElegibleOtraActividad = this.idElegibleOtraActividad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleOtraActividadPK))
      throw new IllegalArgumentException("arg1");
    ElegibleOtraActividadPK localElegibleOtraActividadPK = (ElegibleOtraActividadPK)paramObject;
    this.idElegibleOtraActividad = localElegibleOtraActividadPK.idElegibleOtraActividad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleOtraActividadPK))
      throw new IllegalArgumentException("arg2");
    ElegibleOtraActividadPK localElegibleOtraActividadPK = (ElegibleOtraActividadPK)paramObject;
    localElegibleOtraActividadPK.idElegibleOtraActividad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleOtraActividadPK))
      throw new IllegalArgumentException("arg2");
    ElegibleOtraActividadPK localElegibleOtraActividadPK = (ElegibleOtraActividadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localElegibleOtraActividadPK.idElegibleOtraActividad);
  }

  private static final Elegible jdoGetelegible(ElegibleOtraActividad paramElegibleOtraActividad)
  {
    StateManager localStateManager = paramElegibleOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleOtraActividad.elegible;
    if (localStateManager.isLoaded(paramElegibleOtraActividad, jdoInheritedFieldCount + 0))
      return paramElegibleOtraActividad.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleOtraActividad, jdoInheritedFieldCount + 0, paramElegibleOtraActividad.elegible);
  }

  private static final void jdoSetelegible(ElegibleOtraActividad paramElegibleOtraActividad, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleOtraActividad.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleOtraActividad, jdoInheritedFieldCount + 0, paramElegibleOtraActividad.elegible, paramElegible);
  }

  private static final long jdoGetidElegibleOtraActividad(ElegibleOtraActividad paramElegibleOtraActividad)
  {
    return paramElegibleOtraActividad.idElegibleOtraActividad;
  }

  private static final void jdoSetidElegibleOtraActividad(ElegibleOtraActividad paramElegibleOtraActividad, long paramLong)
  {
    StateManager localStateManager = paramElegibleOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleOtraActividad.idElegibleOtraActividad = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleOtraActividad, jdoInheritedFieldCount + 1, paramElegibleOtraActividad.idElegibleOtraActividad, paramLong);
  }

  private static final String jdoGetobservaciones(ElegibleOtraActividad paramElegibleOtraActividad)
  {
    if (paramElegibleOtraActividad.jdoFlags <= 0)
      return paramElegibleOtraActividad.observaciones;
    StateManager localStateManager = paramElegibleOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleOtraActividad.observaciones;
    if (localStateManager.isLoaded(paramElegibleOtraActividad, jdoInheritedFieldCount + 2))
      return paramElegibleOtraActividad.observaciones;
    return localStateManager.getStringField(paramElegibleOtraActividad, jdoInheritedFieldCount + 2, paramElegibleOtraActividad.observaciones);
  }

  private static final void jdoSetobservaciones(ElegibleOtraActividad paramElegibleOtraActividad, String paramString)
  {
    if (paramElegibleOtraActividad.jdoFlags == 0)
    {
      paramElegibleOtraActividad.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleOtraActividad.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleOtraActividad, jdoInheritedFieldCount + 2, paramElegibleOtraActividad.observaciones, paramString);
  }

  private static final TipoOtraActividad jdoGettipoOtraActividad(ElegibleOtraActividad paramElegibleOtraActividad)
  {
    StateManager localStateManager = paramElegibleOtraActividad.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleOtraActividad.tipoOtraActividad;
    if (localStateManager.isLoaded(paramElegibleOtraActividad, jdoInheritedFieldCount + 3))
      return paramElegibleOtraActividad.tipoOtraActividad;
    return (TipoOtraActividad)localStateManager.getObjectField(paramElegibleOtraActividad, jdoInheritedFieldCount + 3, paramElegibleOtraActividad.tipoOtraActividad);
  }

  private static final void jdoSettipoOtraActividad(ElegibleOtraActividad paramElegibleOtraActividad, TipoOtraActividad paramTipoOtraActividad)
  {
    StateManager localStateManager = paramElegibleOtraActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleOtraActividad.tipoOtraActividad = paramTipoOtraActividad;
      return;
    }
    localStateManager.setObjectField(paramElegibleOtraActividad, jdoInheritedFieldCount + 3, paramElegibleOtraActividad.tipoOtraActividad, paramTipoOtraActividad);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}