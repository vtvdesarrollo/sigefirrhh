package sigefirrhh.planificacion.elegible;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ElegibleFamiliarForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ElegibleFamiliarForm.class.getName());
  private ElegibleFamiliar elegibleFamiliar;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ElegibleFacade elegibleFacade = new ElegibleFacade();
  private Collection resultElegible;
  private Elegible elegible;
  private boolean selectedElegible;
  private int findElegibleCedula;
  private String findElegiblePrimerNombre;
  private String findElegibleSegundoNombre;
  private String findElegiblePrimerApellido;
  private String findElegibleSegundoApellido;
  private boolean showResultElegible;
  private boolean showAddResultElegible;
  private boolean showResult;
  private String findSelectElegible;
  private Collection colElegible;
  private String selectElegible;
  private Object stateResultElegible = null;

  private Object stateResultElegibleFamiliarByElegible = null;

  public String getSelectElegible()
  {
    return this.selectElegible;
  }
  public void setSelectElegible(String valElegible) {
    Iterator iterator = this.colElegible.iterator();
    Elegible elegible = null;
    this.elegibleFamiliar.setElegible(null);
    while (iterator.hasNext()) {
      elegible = (Elegible)iterator.next();
      if (String.valueOf(elegible.getIdElegible()).equals(
        valElegible)) {
        this.elegibleFamiliar.setElegible(
          elegible);
        break;
      }
    }
    this.selectElegible = valElegible;
  }
  public Collection getResult() {
    return this.result;
  }

  public ElegibleFamiliar getElegibleFamiliar() {
    if (this.elegibleFamiliar == null) {
      this.elegibleFamiliar = new ElegibleFamiliar();
    }
    return this.elegibleFamiliar;
  }

  public ElegibleFamiliarForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListEstadoCivil()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_ESTADO_CIVIL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListSexo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_SEXO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListParentesco()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_PARENTESCO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListNivelEducativo() {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_NIVEL_EDUCATIVO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListMismoOrganismo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListNinoExcepcional() {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListGrupoSanguineo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_GRUPO_SANGUINEO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAlergico() {
    Collection col = new ArrayList();

    Iterator iterEntry = ElegibleFamiliar.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColElegible()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colElegible.iterator();
    Elegible elegible = null;
    while (iterator.hasNext()) {
      elegible = (Elegible)iterator.next();
      col.add(new SelectItem(
        String.valueOf(elegible.getIdElegible()), 
        elegible.toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findElegibleByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultElegible();

      this.result = null;
      this.showResult = false;

      this.resultElegible = 
        this.elegibleFacade.findElegibleByCedula(this.findElegibleCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultElegible = 
        ((this.resultElegible != null) && (!this.resultElegible.isEmpty()));

      if (!this.showResultElegible)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findElegibleCedula = 0;
    this.findElegiblePrimerNombre = null;
    this.findElegibleSegundoNombre = null;
    this.findElegiblePrimerApellido = null;
    this.findElegibleSegundoApellido = null;
    return null;
  }

  public String findElegibleByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultElegible();

      this.result = null;
      this.showResult = false;

      if (((this.findElegiblePrimerNombre == null) || (this.findElegiblePrimerNombre.equals(""))) && 
        ((this.findElegibleSegundoNombre == null) || (this.findElegibleSegundoNombre.equals(""))) && 
        ((this.findElegiblePrimerApellido == null) || (this.findElegiblePrimerApellido.equals(""))) && (
        (this.findElegibleSegundoApellido == null) || (this.findElegibleSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultElegible = 
          this.elegibleFacade.findElegibleByNombresApellidos(
          this.findElegiblePrimerNombre, 
          this.findElegibleSegundoNombre, 
          this.findElegiblePrimerApellido, 
          this.findElegibleSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultElegible = 
          ((this.resultElegible != null) && (!this.resultElegible.isEmpty()));
        if (!this.showResultElegible)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findElegibleCedula = 0;
    this.findElegiblePrimerNombre = null;
    this.findElegibleSegundoNombre = null;
    this.findElegiblePrimerApellido = null;
    this.findElegibleSegundoApellido = null;

    return null;
  }

  public String findElegibleFamiliarByElegible()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectElegible();
      if (!this.adding) {
        this.result = 
          this.elegibleFacade.findElegibleFamiliarByElegible(
          this.elegible.getIdElegible());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectElegibleFamiliar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectElegible = null;

    long idElegibleFamiliar = 
      Long.parseLong((String)requestParameterMap.get("idElegibleFamiliar"));
    try
    {
      this.elegibleFamiliar = 
        this.elegibleFacade.findElegibleFamiliarById(
        idElegibleFamiliar);

      if (this.elegibleFamiliar.getElegible() != null) {
        this.selectElegible = 
          String.valueOf(this.elegibleFamiliar.getElegible().getIdElegible());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectElegible()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idElegible = 
      Long.parseLong((String)requestParameterMap.get("idElegible"));
    try
    {
      this.elegible = 
        this.elegibleFacade.findElegibleById(
        idElegible);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedElegible = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultElegible();
  }

  private void resetResultElegible() {
    this.resultElegible = null;
    this.selectedElegible = false;
    this.elegible = null;

    this.showResultElegible = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.elegibleFamiliar.getFechaNacimiento() != null) && 
      (this.elegibleFamiliar.getFechaNacimiento().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Nacimiento no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.elegibleFamiliar.getTiempoSitp() != null) && 
      (this.elegibleFamiliar.getTiempoSitp().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Tiempo Sitp no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.elegibleFamiliar.setElegible(
          this.elegible);
        this.elegibleFacade.addElegibleFamiliar(
          this.elegibleFamiliar);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.elegibleFacade.updateElegibleFamiliar(
          this.elegibleFamiliar);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.elegibleFacade.deleteElegibleFamiliar(
        this.elegibleFamiliar);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedElegible = true;

    this.selectElegible = null;

    this.elegibleFamiliar = new ElegibleFamiliar();

    this.elegibleFamiliar.setElegible(this.elegible);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.elegibleFamiliar.setIdElegibleFamiliar(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.elegible.ElegibleFamiliar"));

    return null;
  }

  public boolean isShowGradoAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowPromedioNotaAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowNinoExcepcionalAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTallaFranelaAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTallaPantalonAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowTallaGorraAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowAlergicoAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowAlergiasAux() {
    try {
      return (this.elegibleFamiliar.getParentesco().equals("H")) || (this.elegibleFamiliar.getParentesco().equals("U")); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.elegibleFamiliar = new ElegibleFamiliar();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.elegibleFamiliar = new ElegibleFamiliar();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedElegible));
  }

  public Collection getResultElegible() {
    return this.resultElegible;
  }
  public Elegible getElegible() {
    return this.elegible;
  }
  public boolean isSelectedElegible() {
    return this.selectedElegible;
  }
  public int getFindElegibleCedula() {
    return this.findElegibleCedula;
  }
  public String getFindElegiblePrimerNombre() {
    return this.findElegiblePrimerNombre;
  }
  public String getFindElegibleSegundoNombre() {
    return this.findElegibleSegundoNombre;
  }
  public String getFindElegiblePrimerApellido() {
    return this.findElegiblePrimerApellido;
  }
  public String getFindElegibleSegundoApellido() {
    return this.findElegibleSegundoApellido;
  }
  public void setFindElegibleCedula(int cedula) {
    this.findElegibleCedula = cedula;
  }
  public void setFindElegiblePrimerNombre(String nombre) {
    this.findElegiblePrimerNombre = nombre;
  }
  public void setFindElegibleSegundoNombre(String nombre) {
    this.findElegibleSegundoNombre = nombre;
  }
  public void setFindElegiblePrimerApellido(String nombre) {
    this.findElegiblePrimerApellido = nombre;
  }
  public void setFindElegibleSegundoApellido(String nombre) {
    this.findElegibleSegundoApellido = nombre;
  }
  public boolean isShowResultElegible() {
    return this.showResultElegible;
  }
  public boolean isShowAddResultElegible() {
    return this.showAddResultElegible;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedElegible);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectElegible() {
    return this.findSelectElegible;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }
}