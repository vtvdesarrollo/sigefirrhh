package sigefirrhh.planificacion.elegible;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.personal.TipoIdioma;

public class ElegibleIdioma
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  protected static final Map LISTA_IDIOMA;
  private long idElegibleIdioma;
  private TipoIdioma tipoIdioma;
  private String habla;
  private String lee;
  private String escribe;
  private String nombreEntidad;
  private String examenSuficiencia;
  private String entidadSuficiencia;
  private Date fechaSuficiencia;
  private String examenAcademico;
  private String entidadAcademica;
  private Date fechaAcademica;
  private Elegible elegible;
  private int idSitp;
  private Date tiempoSitp;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "elegible", "entidadAcademica", "entidadSuficiencia", "escribe", "examenAcademico", "examenSuficiencia", "fechaAcademica", "fechaSuficiencia", "habla", "idElegibleIdioma", "idSitp", "lee", "nombreEntidad", "tiempoSitp", "tipoIdioma" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.TipoIdioma") }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.elegible.ElegibleIdioma"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ElegibleIdioma());

    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_IDIOMA = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
    LISTA_IDIOMA.put("B", "BASICO");
    LISTA_IDIOMA.put("I", "INTERMEDIO");
    LISTA_IDIOMA.put("A", "AVANZADO");
    LISTA_IDIOMA.put("N", "NO");
  }

  public ElegibleIdioma()
  {
    jdoSethabla(this, "N");

    jdoSetlee(this, "N");

    jdoSetescribe(this, "N");

    jdoSetexamenSuficiencia(this, "N");

    jdoSetexamenAcademico(this, "N");
  }

  public String toString()
  {
    return jdoGettipoIdioma(this).getDescripcion();
  }

  public Elegible getElegible()
  {
    return jdoGetelegible(this);
  }

  public void setElegible(Elegible elegible)
  {
    jdoSetelegible(this, elegible);
  }

  public String getEntidadAcademica()
  {
    return jdoGetentidadAcademica(this);
  }

  public void setEntidadAcademica(String entidadAcademica)
  {
    jdoSetentidadAcademica(this, entidadAcademica);
  }

  public String getEntidadSuficiencia()
  {
    return jdoGetentidadSuficiencia(this);
  }

  public void setEntidadSuficiencia(String entidadSuficiencia)
  {
    jdoSetentidadSuficiencia(this, entidadSuficiencia);
  }

  public String getEscribe()
  {
    return jdoGetescribe(this);
  }

  public void setEscribe(String escribe)
  {
    jdoSetescribe(this, escribe);
  }

  public String getExamenAcademico()
  {
    return jdoGetexamenAcademico(this);
  }

  public void setExamenAcademico(String examenAcademico)
  {
    jdoSetexamenAcademico(this, examenAcademico);
  }

  public String getExamenSuficiencia()
  {
    return jdoGetexamenSuficiencia(this);
  }

  public void setExamenSuficiencia(String examenSuficiencia)
  {
    jdoSetexamenSuficiencia(this, examenSuficiencia);
  }

  public Date getFechaAcademica()
  {
    return jdoGetfechaAcademica(this);
  }

  public void setFechaAcademica(Date fechaAcademica)
  {
    jdoSetfechaAcademica(this, fechaAcademica);
  }

  public Date getFechaSuficiencia()
  {
    return jdoGetfechaSuficiencia(this);
  }

  public void setFechaSuficiencia(Date fechaSuficiencia)
  {
    jdoSetfechaSuficiencia(this, fechaSuficiencia);
  }

  public String getHabla()
  {
    return jdoGethabla(this);
  }

  public void setHabla(String habla)
  {
    jdoSethabla(this, habla);
  }

  public long getIdElegibleIdioma()
  {
    return jdoGetidElegibleIdioma(this);
  }

  public void setIdElegibleIdioma(long idElegibleIdioma)
  {
    jdoSetidElegibleIdioma(this, idElegibleIdioma);
  }

  public int getIdSitp()
  {
    return jdoGetidSitp(this);
  }

  public void setIdSitp(int idSitp)
  {
    jdoSetidSitp(this, idSitp);
  }

  public String getLee()
  {
    return jdoGetlee(this);
  }

  public void setLee(String lee)
  {
    jdoSetlee(this, lee);
  }

  public String getNombreEntidad()
  {
    return jdoGetnombreEntidad(this);
  }

  public void setNombreEntidad(String nombreEntidad)
  {
    jdoSetnombreEntidad(this, nombreEntidad);
  }

  public Date getTiempoSitp()
  {
    return jdoGettiempoSitp(this);
  }

  public void setTiempoSitp(Date tiempoSitp)
  {
    jdoSettiempoSitp(this, tiempoSitp);
  }

  public TipoIdioma getTipoIdioma()
  {
    return jdoGettipoIdioma(this);
  }

  public void setTipoIdioma(TipoIdioma tipoIdioma)
  {
    jdoSettipoIdioma(this, tipoIdioma);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ElegibleIdioma localElegibleIdioma = new ElegibleIdioma();
    localElegibleIdioma.jdoFlags = 1;
    localElegibleIdioma.jdoStateManager = paramStateManager;
    return localElegibleIdioma;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ElegibleIdioma localElegibleIdioma = new ElegibleIdioma();
    localElegibleIdioma.jdoCopyKeyFieldsFromObjectId(paramObject);
    localElegibleIdioma.jdoFlags = 1;
    localElegibleIdioma.jdoStateManager = paramStateManager;
    return localElegibleIdioma;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.entidadAcademica);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.entidadSuficiencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.escribe);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.examenAcademico);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.examenSuficiencia);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaAcademica);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaSuficiencia);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.habla);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idElegibleIdioma);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.idSitp);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.lee);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tiempoSitp);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoIdioma);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entidadAcademica = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entidadSuficiencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.escribe = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.examenAcademico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.examenSuficiencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaAcademica = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaSuficiencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.habla = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idElegibleIdioma = localStateManager.replacingLongField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSitp = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.lee = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoSitp = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoIdioma = ((TipoIdioma)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ElegibleIdioma paramElegibleIdioma, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramElegibleIdioma.elegible;
      return;
    case 1:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.entidadAcademica = paramElegibleIdioma.entidadAcademica;
      return;
    case 2:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.entidadSuficiencia = paramElegibleIdioma.entidadSuficiencia;
      return;
    case 3:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.escribe = paramElegibleIdioma.escribe;
      return;
    case 4:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.examenAcademico = paramElegibleIdioma.examenAcademico;
      return;
    case 5:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.examenSuficiencia = paramElegibleIdioma.examenSuficiencia;
      return;
    case 6:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.fechaAcademica = paramElegibleIdioma.fechaAcademica;
      return;
    case 7:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.fechaSuficiencia = paramElegibleIdioma.fechaSuficiencia;
      return;
    case 8:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.habla = paramElegibleIdioma.habla;
      return;
    case 9:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.idElegibleIdioma = paramElegibleIdioma.idElegibleIdioma;
      return;
    case 10:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.idSitp = paramElegibleIdioma.idSitp;
      return;
    case 11:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.lee = paramElegibleIdioma.lee;
      return;
    case 12:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramElegibleIdioma.nombreEntidad;
      return;
    case 13:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoSitp = paramElegibleIdioma.tiempoSitp;
      return;
    case 14:
      if (paramElegibleIdioma == null)
        throw new IllegalArgumentException("arg1");
      this.tipoIdioma = paramElegibleIdioma.tipoIdioma;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ElegibleIdioma))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ElegibleIdioma localElegibleIdioma = (ElegibleIdioma)paramObject;
    if (localElegibleIdioma.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localElegibleIdioma, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ElegibleIdiomaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ElegibleIdiomaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleIdiomaPK))
      throw new IllegalArgumentException("arg1");
    ElegibleIdiomaPK localElegibleIdiomaPK = (ElegibleIdiomaPK)paramObject;
    localElegibleIdiomaPK.idElegibleIdioma = this.idElegibleIdioma;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ElegibleIdiomaPK))
      throw new IllegalArgumentException("arg1");
    ElegibleIdiomaPK localElegibleIdiomaPK = (ElegibleIdiomaPK)paramObject;
    this.idElegibleIdioma = localElegibleIdiomaPK.idElegibleIdioma;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleIdiomaPK))
      throw new IllegalArgumentException("arg2");
    ElegibleIdiomaPK localElegibleIdiomaPK = (ElegibleIdiomaPK)paramObject;
    localElegibleIdiomaPK.idElegibleIdioma = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 9);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ElegibleIdiomaPK))
      throw new IllegalArgumentException("arg2");
    ElegibleIdiomaPK localElegibleIdiomaPK = (ElegibleIdiomaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 9, localElegibleIdiomaPK.idElegibleIdioma);
  }

  private static final Elegible jdoGetelegible(ElegibleIdioma paramElegibleIdioma)
  {
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.elegible;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 0))
      return paramElegibleIdioma.elegible;
    return (Elegible)localStateManager.getObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 0, paramElegibleIdioma.elegible);
  }

  private static final void jdoSetelegible(ElegibleIdioma paramElegibleIdioma, Elegible paramElegible)
  {
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 0, paramElegibleIdioma.elegible, paramElegible);
  }

  private static final String jdoGetentidadAcademica(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.entidadAcademica;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.entidadAcademica;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 1))
      return paramElegibleIdioma.entidadAcademica;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 1, paramElegibleIdioma.entidadAcademica);
  }

  private static final void jdoSetentidadAcademica(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.entidadAcademica = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.entidadAcademica = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 1, paramElegibleIdioma.entidadAcademica, paramString);
  }

  private static final String jdoGetentidadSuficiencia(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.entidadSuficiencia;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.entidadSuficiencia;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 2))
      return paramElegibleIdioma.entidadSuficiencia;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 2, paramElegibleIdioma.entidadSuficiencia);
  }

  private static final void jdoSetentidadSuficiencia(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.entidadSuficiencia = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.entidadSuficiencia = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 2, paramElegibleIdioma.entidadSuficiencia, paramString);
  }

  private static final String jdoGetescribe(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.escribe;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.escribe;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 3))
      return paramElegibleIdioma.escribe;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 3, paramElegibleIdioma.escribe);
  }

  private static final void jdoSetescribe(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.escribe = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.escribe = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 3, paramElegibleIdioma.escribe, paramString);
  }

  private static final String jdoGetexamenAcademico(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.examenAcademico;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.examenAcademico;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 4))
      return paramElegibleIdioma.examenAcademico;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 4, paramElegibleIdioma.examenAcademico);
  }

  private static final void jdoSetexamenAcademico(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.examenAcademico = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.examenAcademico = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 4, paramElegibleIdioma.examenAcademico, paramString);
  }

  private static final String jdoGetexamenSuficiencia(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.examenSuficiencia;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.examenSuficiencia;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 5))
      return paramElegibleIdioma.examenSuficiencia;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 5, paramElegibleIdioma.examenSuficiencia);
  }

  private static final void jdoSetexamenSuficiencia(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.examenSuficiencia = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.examenSuficiencia = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 5, paramElegibleIdioma.examenSuficiencia, paramString);
  }

  private static final Date jdoGetfechaAcademica(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.fechaAcademica;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.fechaAcademica;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 6))
      return paramElegibleIdioma.fechaAcademica;
    return (Date)localStateManager.getObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 6, paramElegibleIdioma.fechaAcademica);
  }

  private static final void jdoSetfechaAcademica(ElegibleIdioma paramElegibleIdioma, Date paramDate)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.fechaAcademica = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.fechaAcademica = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 6, paramElegibleIdioma.fechaAcademica, paramDate);
  }

  private static final Date jdoGetfechaSuficiencia(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.fechaSuficiencia;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.fechaSuficiencia;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 7))
      return paramElegibleIdioma.fechaSuficiencia;
    return (Date)localStateManager.getObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 7, paramElegibleIdioma.fechaSuficiencia);
  }

  private static final void jdoSetfechaSuficiencia(ElegibleIdioma paramElegibleIdioma, Date paramDate)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.fechaSuficiencia = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.fechaSuficiencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 7, paramElegibleIdioma.fechaSuficiencia, paramDate);
  }

  private static final String jdoGethabla(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.habla;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.habla;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 8))
      return paramElegibleIdioma.habla;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 8, paramElegibleIdioma.habla);
  }

  private static final void jdoSethabla(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.habla = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.habla = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 8, paramElegibleIdioma.habla, paramString);
  }

  private static final long jdoGetidElegibleIdioma(ElegibleIdioma paramElegibleIdioma)
  {
    return paramElegibleIdioma.idElegibleIdioma;
  }

  private static final void jdoSetidElegibleIdioma(ElegibleIdioma paramElegibleIdioma, long paramLong)
  {
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.idElegibleIdioma = paramLong;
      return;
    }
    localStateManager.setLongField(paramElegibleIdioma, jdoInheritedFieldCount + 9, paramElegibleIdioma.idElegibleIdioma, paramLong);
  }

  private static final int jdoGetidSitp(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.idSitp;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.idSitp;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 10))
      return paramElegibleIdioma.idSitp;
    return localStateManager.getIntField(paramElegibleIdioma, jdoInheritedFieldCount + 10, paramElegibleIdioma.idSitp);
  }

  private static final void jdoSetidSitp(ElegibleIdioma paramElegibleIdioma, int paramInt)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.idSitp = paramInt;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.idSitp = paramInt;
      return;
    }
    localStateManager.setIntField(paramElegibleIdioma, jdoInheritedFieldCount + 10, paramElegibleIdioma.idSitp, paramInt);
  }

  private static final String jdoGetlee(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.lee;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.lee;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 11))
      return paramElegibleIdioma.lee;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 11, paramElegibleIdioma.lee);
  }

  private static final void jdoSetlee(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.lee = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.lee = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 11, paramElegibleIdioma.lee, paramString);
  }

  private static final String jdoGetnombreEntidad(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.nombreEntidad;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.nombreEntidad;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 12))
      return paramElegibleIdioma.nombreEntidad;
    return localStateManager.getStringField(paramElegibleIdioma, jdoInheritedFieldCount + 12, paramElegibleIdioma.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(ElegibleIdioma paramElegibleIdioma, String paramString)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramElegibleIdioma, jdoInheritedFieldCount + 12, paramElegibleIdioma.nombreEntidad, paramString);
  }

  private static final Date jdoGettiempoSitp(ElegibleIdioma paramElegibleIdioma)
  {
    if (paramElegibleIdioma.jdoFlags <= 0)
      return paramElegibleIdioma.tiempoSitp;
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.tiempoSitp;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 13))
      return paramElegibleIdioma.tiempoSitp;
    return (Date)localStateManager.getObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 13, paramElegibleIdioma.tiempoSitp);
  }

  private static final void jdoSettiempoSitp(ElegibleIdioma paramElegibleIdioma, Date paramDate)
  {
    if (paramElegibleIdioma.jdoFlags == 0)
    {
      paramElegibleIdioma.tiempoSitp = paramDate;
      return;
    }
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.tiempoSitp = paramDate;
      return;
    }
    localStateManager.setObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 13, paramElegibleIdioma.tiempoSitp, paramDate);
  }

  private static final TipoIdioma jdoGettipoIdioma(ElegibleIdioma paramElegibleIdioma)
  {
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
      return paramElegibleIdioma.tipoIdioma;
    if (localStateManager.isLoaded(paramElegibleIdioma, jdoInheritedFieldCount + 14))
      return paramElegibleIdioma.tipoIdioma;
    return (TipoIdioma)localStateManager.getObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 14, paramElegibleIdioma.tipoIdioma);
  }

  private static final void jdoSettipoIdioma(ElegibleIdioma paramElegibleIdioma, TipoIdioma paramTipoIdioma)
  {
    StateManager localStateManager = paramElegibleIdioma.jdoStateManager;
    if (localStateManager == null)
    {
      paramElegibleIdioma.tipoIdioma = paramTipoIdioma;
      return;
    }
    localStateManager.setObjectField(paramElegibleIdioma, jdoInheritedFieldCount + 14, paramElegibleIdioma.tipoIdioma, paramTipoIdioma);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}