package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleExperienciaPK
  implements Serializable
{
  public long idElegibleExperiencia;

  public ElegibleExperienciaPK()
  {
  }

  public ElegibleExperienciaPK(long idElegibleExperiencia)
  {
    this.idElegibleExperiencia = idElegibleExperiencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleExperienciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleExperienciaPK thatPK)
  {
    return 
      this.idElegibleExperiencia == thatPK.idElegibleExperiencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleExperiencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleExperiencia);
  }
}