package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleIdiomaPK
  implements Serializable
{
  public long idElegibleIdioma;

  public ElegibleIdiomaPK()
  {
  }

  public ElegibleIdiomaPK(long idElegibleIdioma)
  {
    this.idElegibleIdioma = idElegibleIdioma;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleIdiomaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleIdiomaPK thatPK)
  {
    return 
      this.idElegibleIdioma == thatPK.idElegibleIdioma;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleIdioma)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleIdioma);
  }
}