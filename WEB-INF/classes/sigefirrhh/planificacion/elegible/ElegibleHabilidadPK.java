package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleHabilidadPK
  implements Serializable
{
  public long idElegibleHabilidad;

  public ElegibleHabilidadPK()
  {
  }

  public ElegibleHabilidadPK(long idElegibleHabilidad)
  {
    this.idElegibleHabilidad = idElegibleHabilidad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleHabilidadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleHabilidadPK thatPK)
  {
    return 
      this.idElegibleHabilidad == thatPK.idElegibleHabilidad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleHabilidad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleHabilidad);
  }
}