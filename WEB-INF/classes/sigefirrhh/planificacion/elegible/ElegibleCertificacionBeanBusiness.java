package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.AreaConocimientoBeanBusiness;

public class ElegibleCertificacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleCertificacion(ElegibleCertificacion elegibleCertificacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleCertificacion elegibleCertificacionNew = 
      (ElegibleCertificacion)BeanUtils.cloneBean(
      elegibleCertificacion);

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (elegibleCertificacionNew.getAreaConocimiento() != null) {
      elegibleCertificacionNew.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        elegibleCertificacionNew.getAreaConocimiento().getIdAreaConocimiento()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleCertificacionNew.getElegible() != null) {
      elegibleCertificacionNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleCertificacionNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleCertificacionNew);
  }

  public void updateElegibleCertificacion(ElegibleCertificacion elegibleCertificacion) throws Exception
  {
    ElegibleCertificacion elegibleCertificacionModify = 
      findElegibleCertificacionById(elegibleCertificacion.getIdElegibleCertificacion());

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (elegibleCertificacion.getAreaConocimiento() != null) {
      elegibleCertificacion.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        elegibleCertificacion.getAreaConocimiento().getIdAreaConocimiento()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleCertificacion.getElegible() != null) {
      elegibleCertificacion.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleCertificacion.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleCertificacionModify, elegibleCertificacion);
  }

  public void deleteElegibleCertificacion(ElegibleCertificacion elegibleCertificacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleCertificacion elegibleCertificacionDelete = 
      findElegibleCertificacionById(elegibleCertificacion.getIdElegibleCertificacion());
    pm.deletePersistent(elegibleCertificacionDelete);
  }

  public ElegibleCertificacion findElegibleCertificacionById(long idElegibleCertificacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleCertificacion == pIdElegibleCertificacion";
    Query query = pm.newQuery(ElegibleCertificacion.class, filter);

    query.declareParameters("long pIdElegibleCertificacion");

    parameters.put("pIdElegibleCertificacion", new Long(idElegibleCertificacion));

    Collection colElegibleCertificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleCertificacion.iterator();
    return (ElegibleCertificacion)iterator.next();
  }

  public Collection findElegibleCertificacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleCertificacionExtent = pm.getExtent(
      ElegibleCertificacion.class, true);
    Query query = pm.newQuery(elegibleCertificacionExtent);
    query.setOrdering("fechaCertificacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "areaConocimiento.idAreaConocimiento == pIdAreaConocimiento";

    Query query = pm.newQuery(ElegibleCertificacion.class, filter);

    query.declareParameters("long pIdAreaConocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdAreaConocimiento", new Long(idAreaConocimiento));

    query.setOrdering("fechaCertificacion ascending");

    Collection colElegibleCertificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleCertificacion);

    return colElegibleCertificacion;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleCertificacion.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("fechaCertificacion ascending");

    Collection colElegibleCertificacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleCertificacion);

    return colElegibleCertificacion;
  }
}