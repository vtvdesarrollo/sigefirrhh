package sigefirrhh.planificacion.elegible;

import java.io.Serializable;

public class ElegibleCertificacionPK
  implements Serializable
{
  public long idElegibleCertificacion;

  public ElegibleCertificacionPK()
  {
  }

  public ElegibleCertificacionPK(long idElegibleCertificacion)
  {
    this.idElegibleCertificacion = idElegibleCertificacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ElegibleCertificacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ElegibleCertificacionPK thatPK)
  {
    return 
      this.idElegibleCertificacion == thatPK.idElegibleCertificacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idElegibleCertificacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idElegibleCertificacion);
  }
}