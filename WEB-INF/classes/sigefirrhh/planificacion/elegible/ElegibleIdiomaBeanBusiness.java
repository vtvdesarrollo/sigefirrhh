package sigefirrhh.planificacion.elegible;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.personal.TipoIdioma;
import sigefirrhh.base.personal.TipoIdiomaBeanBusiness;

public class ElegibleIdiomaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addElegibleIdioma(ElegibleIdioma elegibleIdioma)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ElegibleIdioma elegibleIdiomaNew = 
      (ElegibleIdioma)BeanUtils.cloneBean(
      elegibleIdioma);

    TipoIdiomaBeanBusiness tipoIdiomaBeanBusiness = new TipoIdiomaBeanBusiness();

    if (elegibleIdiomaNew.getTipoIdioma() != null) {
      elegibleIdiomaNew.setTipoIdioma(
        tipoIdiomaBeanBusiness.findTipoIdiomaById(
        elegibleIdiomaNew.getTipoIdioma().getIdTipoIdioma()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleIdiomaNew.getElegible() != null) {
      elegibleIdiomaNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleIdiomaNew.getElegible().getIdElegible()));
    }
    pm.makePersistent(elegibleIdiomaNew);
  }

  public void updateElegibleIdioma(ElegibleIdioma elegibleIdioma) throws Exception
  {
    ElegibleIdioma elegibleIdiomaModify = 
      findElegibleIdiomaById(elegibleIdioma.getIdElegibleIdioma());

    TipoIdiomaBeanBusiness tipoIdiomaBeanBusiness = new TipoIdiomaBeanBusiness();

    if (elegibleIdioma.getTipoIdioma() != null) {
      elegibleIdioma.setTipoIdioma(
        tipoIdiomaBeanBusiness.findTipoIdiomaById(
        elegibleIdioma.getTipoIdioma().getIdTipoIdioma()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (elegibleIdioma.getElegible() != null) {
      elegibleIdioma.setElegible(
        elegibleBeanBusiness.findElegibleById(
        elegibleIdioma.getElegible().getIdElegible()));
    }

    BeanUtils.copyProperties(elegibleIdiomaModify, elegibleIdioma);
  }

  public void deleteElegibleIdioma(ElegibleIdioma elegibleIdioma) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ElegibleIdioma elegibleIdiomaDelete = 
      findElegibleIdiomaById(elegibleIdioma.getIdElegibleIdioma());
    pm.deletePersistent(elegibleIdiomaDelete);
  }

  public ElegibleIdioma findElegibleIdiomaById(long idElegibleIdioma) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idElegibleIdioma == pIdElegibleIdioma";
    Query query = pm.newQuery(ElegibleIdioma.class, filter);

    query.declareParameters("long pIdElegibleIdioma");

    parameters.put("pIdElegibleIdioma", new Long(idElegibleIdioma));

    Collection colElegibleIdioma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colElegibleIdioma.iterator();
    return (ElegibleIdioma)iterator.next();
  }

  public Collection findElegibleIdiomaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent elegibleIdiomaExtent = pm.getExtent(
      ElegibleIdioma.class, true);
    Query query = pm.newQuery(elegibleIdiomaExtent);
    query.setOrdering("tipoIdioma.descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByElegible(long idElegible)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "elegible.idElegible == pIdElegible";

    Query query = pm.newQuery(ElegibleIdioma.class, filter);

    query.declareParameters("long pIdElegible");
    HashMap parameters = new HashMap();

    parameters.put("pIdElegible", new Long(idElegible));

    query.setOrdering("tipoIdioma.descripcion ascending");

    Collection colElegibleIdioma = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colElegibleIdioma);

    return colElegibleIdioma;
  }
}