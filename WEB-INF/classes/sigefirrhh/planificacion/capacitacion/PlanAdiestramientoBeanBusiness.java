package sigefirrhh.planificacion.capacitacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.AreaConocimientoBeanBusiness;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.adiestramiento.TipoCursoBeanBusiness;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.base.estructura.UnidadFuncionalBeanBusiness;
import sigefirrhh.planificacion.plan.PlanPersonal;
import sigefirrhh.planificacion.plan.PlanPersonalBeanBusiness;

public class PlanAdiestramientoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPlanAdiestramiento(PlanAdiestramiento planAdiestramiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PlanAdiestramiento planAdiestramientoNew = 
      (PlanAdiestramiento)BeanUtils.cloneBean(
      planAdiestramiento);

    UnidadFuncionalBeanBusiness unidadFuncionalBeanBusiness = new UnidadFuncionalBeanBusiness();

    if (planAdiestramientoNew.getUnidadFuncional() != null) {
      planAdiestramientoNew.setUnidadFuncional(
        unidadFuncionalBeanBusiness.findUnidadFuncionalById(
        planAdiestramientoNew.getUnidadFuncional().getIdUnidadFuncional()));
    }

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (planAdiestramientoNew.getTipoCurso() != null) {
      planAdiestramientoNew.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        planAdiestramientoNew.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (planAdiestramientoNew.getAreaConocimiento() != null) {
      planAdiestramientoNew.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        planAdiestramientoNew.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (planAdiestramientoNew.getPlanPersonal() != null) {
      planAdiestramientoNew.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        planAdiestramientoNew.getPlanPersonal().getIdPlanPersonal()));
    }
    pm.makePersistent(planAdiestramientoNew);
  }

  public void updatePlanAdiestramiento(PlanAdiestramiento planAdiestramiento) throws Exception
  {
    PlanAdiestramiento planAdiestramientoModify = 
      findPlanAdiestramientoById(planAdiestramiento.getIdPlanAdiestramiento());

    UnidadFuncionalBeanBusiness unidadFuncionalBeanBusiness = new UnidadFuncionalBeanBusiness();

    if (planAdiestramiento.getUnidadFuncional() != null) {
      planAdiestramiento.setUnidadFuncional(
        unidadFuncionalBeanBusiness.findUnidadFuncionalById(
        planAdiestramiento.getUnidadFuncional().getIdUnidadFuncional()));
    }

    TipoCursoBeanBusiness tipoCursoBeanBusiness = new TipoCursoBeanBusiness();

    if (planAdiestramiento.getTipoCurso() != null) {
      planAdiestramiento.setTipoCurso(
        tipoCursoBeanBusiness.findTipoCursoById(
        planAdiestramiento.getTipoCurso().getIdTipoCurso()));
    }

    AreaConocimientoBeanBusiness areaConocimientoBeanBusiness = new AreaConocimientoBeanBusiness();

    if (planAdiestramiento.getAreaConocimiento() != null) {
      planAdiestramiento.setAreaConocimiento(
        areaConocimientoBeanBusiness.findAreaConocimientoById(
        planAdiestramiento.getAreaConocimiento().getIdAreaConocimiento()));
    }

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (planAdiestramiento.getPlanPersonal() != null) {
      planAdiestramiento.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        planAdiestramiento.getPlanPersonal().getIdPlanPersonal()));
    }

    BeanUtils.copyProperties(planAdiestramientoModify, planAdiestramiento);
  }

  public void deletePlanAdiestramiento(PlanAdiestramiento planAdiestramiento) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PlanAdiestramiento planAdiestramientoDelete = 
      findPlanAdiestramientoById(planAdiestramiento.getIdPlanAdiestramiento());
    pm.deletePersistent(planAdiestramientoDelete);
  }

  public PlanAdiestramiento findPlanAdiestramientoById(long idPlanAdiestramiento) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPlanAdiestramiento == pIdPlanAdiestramiento";
    Query query = pm.newQuery(PlanAdiestramiento.class, filter);

    query.declareParameters("long pIdPlanAdiestramiento");

    parameters.put("pIdPlanAdiestramiento", new Long(idPlanAdiestramiento));

    Collection colPlanAdiestramiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPlanAdiestramiento.iterator();
    return (PlanAdiestramiento)iterator.next();
  }

  public Collection findPlanAdiestramientoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent planAdiestramientoExtent = pm.getExtent(
      PlanAdiestramiento.class, true);
    Query query = pm.newQuery(planAdiestramientoExtent);
    query.setOrdering("anio descending,unidadFuncional.codUnidadFuncional ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUnidadFuncional(long idUnidadFuncional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadFuncional.idUnidadFuncional == pIdUnidadFuncional";

    Query query = pm.newQuery(PlanAdiestramiento.class, filter);

    query.declareParameters("long pIdUnidadFuncional");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadFuncional", new Long(idUnidadFuncional));

    query.setOrdering("anio descending,unidadFuncional.codUnidadFuncional ascending");

    Collection colPlanAdiestramiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanAdiestramiento);

    return colPlanAdiestramiento;
  }

  public Collection findByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "areaConocimiento.idAreaConocimiento == pIdAreaConocimiento";

    Query query = pm.newQuery(PlanAdiestramiento.class, filter);

    query.declareParameters("long pIdAreaConocimiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdAreaConocimiento", new Long(idAreaConocimiento));

    query.setOrdering("anio descending,unidadFuncional.codUnidadFuncional ascending");

    Collection colPlanAdiestramiento = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanAdiestramiento);

    return colPlanAdiestramiento;
  }
}