package sigefirrhh.planificacion.capacitacion;

import java.io.Serializable;

public class ParticipantePK
  implements Serializable
{
  public long idParticipante;

  public ParticipantePK()
  {
  }

  public ParticipantePK(long idParticipante)
  {
    this.idParticipante = idParticipante;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParticipantePK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParticipantePK thatPK)
  {
    return 
      this.idParticipante == thatPK.idParticipante;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idParticipante)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idParticipante);
  }
}