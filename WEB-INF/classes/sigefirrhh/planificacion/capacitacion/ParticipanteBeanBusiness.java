package sigefirrhh.planificacion.capacitacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class ParticipanteBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addParticipante(Participante participante)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Participante participanteNew = 
      (Participante)BeanUtils.cloneBean(
      participante);

    PlanAdiestramientoBeanBusiness planAdiestramientoBeanBusiness = new PlanAdiestramientoBeanBusiness();

    if (participanteNew.getPlanAdiestramiento() != null) {
      participanteNew.setPlanAdiestramiento(
        planAdiestramientoBeanBusiness.findPlanAdiestramientoById(
        participanteNew.getPlanAdiestramiento().getIdPlanAdiestramiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (participanteNew.getPersonal() != null) {
      participanteNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        participanteNew.getPersonal().getIdPersonal()));
    }
    pm.makePersistent(participanteNew);
  }

  public void updateParticipante(Participante participante) throws Exception
  {
    Participante participanteModify = 
      findParticipanteById(participante.getIdParticipante());

    PlanAdiestramientoBeanBusiness planAdiestramientoBeanBusiness = new PlanAdiestramientoBeanBusiness();

    if (participante.getPlanAdiestramiento() != null) {
      participante.setPlanAdiestramiento(
        planAdiestramientoBeanBusiness.findPlanAdiestramientoById(
        participante.getPlanAdiestramiento().getIdPlanAdiestramiento()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (participante.getPersonal() != null) {
      participante.setPersonal(
        personalBeanBusiness.findPersonalById(
        participante.getPersonal().getIdPersonal()));
    }

    BeanUtils.copyProperties(participanteModify, participante);
  }

  public void deleteParticipante(Participante participante) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Participante participanteDelete = 
      findParticipanteById(participante.getIdParticipante());
    pm.deletePersistent(participanteDelete);
  }

  public Participante findParticipanteById(long idParticipante) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idParticipante == pIdParticipante";
    Query query = pm.newQuery(Participante.class, filter);

    query.declareParameters("long pIdParticipante");

    parameters.put("pIdParticipante", new Long(idParticipante));

    Collection colParticipante = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colParticipante.iterator();
    return (Participante)iterator.next();
  }

  public Collection findParticipanteAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent participanteExtent = pm.getExtent(
      Participante.class, true);
    Query query = pm.newQuery(participanteExtent);
    query.setOrdering("trabajador.cedula ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPlanAdiestramiento(long idPlanAdiestramiento)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planAdiestramiento.idPlanAdiestramiento == pIdPlanAdiestramiento";

    Query query = pm.newQuery(Participante.class, filter);

    query.declareParameters("long pIdPlanAdiestramiento");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanAdiestramiento", new Long(idPlanAdiestramiento));

    query.setOrdering("trabajador.cedula ascending");

    Collection colParticipante = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colParticipante);

    return colParticipante;
  }
}