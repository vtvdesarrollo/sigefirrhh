package sigefirrhh.planificacion.capacitacion;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.adiestramiento.AdiestramientoFacade;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.login.LoginSession;
import sigefirrhh.planificacion.plan.PlanFacade;
import sigefirrhh.planificacion.plan.PlanPersonal;

public class ReportePlanAdiestramientoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportePlanAdiestramientoForm.class.getName());
  private PlanAdiestramiento planAdiestramiento;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private boolean showReport = false;
  private int reportId;
  private String reportName;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private PlanFacade planFacade = new PlanFacade();
  private CapacitacionFacade capacitacionFacade = new CapacitacionFacade();
  private boolean showPlanAdiestramientoByUnidadFuncional;
  private boolean showPlanAdiestramientoByAreaConocimiento;
  private String findSelectUnidadFuncional;
  private String findSelectAreaConocimiento;
  private Collection findColUnidadFuncional;
  private Collection findColAreaConocimiento;
  private Collection colUnidadFuncional;
  private Collection colTipoCurso;
  private Collection colAreaConocimiento;
  private Collection colPlanPersonal;
  private String selectUnidadFuncional;
  private String selectTipoCurso;
  private String selectAreaConocimiento;
  private String selectPlanPersonal;
  private Object stateResultPlanAdiestramientoByUnidadFuncional = null;

  private Object stateResultPlanAdiestramientoByAreaConocimiento = null;

  public int getReportId()
  {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
      parameters.put("id_unidad_funcional", new Long(this.planAdiestramiento.getUnidadFuncional().getIdUnidadFuncional()));
      parameters.put("anio", new Integer(this.planAdiestramiento.getAnio()));
      parameters.put("numero_plan", new Integer(this.planAdiestramiento.getNumeroPlan()));
      parameters.put("logo", 
        ((ServletContext)context.getExternalContext().getContext()).getRealPath(
        this.login.getURLLogo()));
      parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/capacitacion");
      this.reportName = "planAdiestramiento";
      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/capacitacion");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getFindSelectUnidadFuncional()
  {
    return this.findSelectUnidadFuncional;
  }
  public void setFindSelectUnidadFuncional(String valUnidadFuncional) {
    this.findSelectUnidadFuncional = valUnidadFuncional;
  }

  public Collection getFindColUnidadFuncional() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }
  public String getFindSelectAreaConocimiento() {
    return this.findSelectAreaConocimiento;
  }
  public void setFindSelectAreaConocimiento(String valAreaConocimiento) {
    this.findSelectAreaConocimiento = valAreaConocimiento;
  }

  public Collection getFindColAreaConocimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public String getSelectUnidadFuncional()
  {
    return this.selectUnidadFuncional;
  }
  public void setSelectUnidadFuncional(String valUnidadFuncional) {
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    this.planAdiestramiento.setUnidadFuncional(null);
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      if (String.valueOf(unidadFuncional.getIdUnidadFuncional()).equals(
        valUnidadFuncional)) {
        this.planAdiestramiento.setUnidadFuncional(
          unidadFuncional);
        break;
      }
    }
    this.selectUnidadFuncional = valUnidadFuncional;
  }
  public String getSelectTipoCurso() {
    return this.selectTipoCurso;
  }
  public void setSelectTipoCurso(String valTipoCurso) {
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    this.planAdiestramiento.setTipoCurso(null);
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      if (String.valueOf(tipoCurso.getIdTipoCurso()).equals(
        valTipoCurso)) {
        this.planAdiestramiento.setTipoCurso(
          tipoCurso);
        break;
      }
    }
    this.selectTipoCurso = valTipoCurso;
  }
  public String getSelectAreaConocimiento() {
    return this.selectAreaConocimiento;
  }
  public void setSelectAreaConocimiento(String valAreaConocimiento) {
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    this.planAdiestramiento.setAreaConocimiento(null);
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      if (String.valueOf(areaConocimiento.getIdAreaConocimiento()).equals(
        valAreaConocimiento)) {
        this.planAdiestramiento.setAreaConocimiento(
          areaConocimiento);
        break;
      }
    }
    this.selectAreaConocimiento = valAreaConocimiento;
  }
  public String getSelectPlanPersonal() {
    return this.selectPlanPersonal;
  }
  public void setSelectPlanPersonal(String valPlanPersonal) {
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    this.planAdiestramiento.setPlanPersonal(null);
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      if (String.valueOf(planPersonal.getIdPlanPersonal()).equals(
        valPlanPersonal)) {
        this.planAdiestramiento.setPlanPersonal(
          planPersonal);
        break;
      }
    }
    this.selectPlanPersonal = valPlanPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public PlanAdiestramiento getPlanAdiestramiento() {
    if (this.planAdiestramiento == null) {
      this.planAdiestramiento = new PlanAdiestramiento();
    }
    return this.planAdiestramiento;
  }

  public ReportePlanAdiestramientoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUnidadFuncional()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }

  public Collection getColTipoCurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCurso.getIdTipoCurso()), 
        tipoCurso.toString()));
    }
    return col;
  }

  public Collection getColAreaConocimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public Collection getListUnidadTiempo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_UNIDAD_TIEMPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoCargo() {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_TIPO_CARGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListNivel() {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_NIVEL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListInternoExterno()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_LUGAR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAprobacionMpd()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_APROBACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPlanPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColAreaConocimiento = 
        this.adiestramientoFacade.findAllAreaConocimiento();

      this.colUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoCurso = 
        this.adiestramientoFacade.findAllTipoCurso();
      this.colAreaConocimiento = 
        this.adiestramientoFacade.findAllAreaConocimiento();
      this.colPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPlanAdiestramientoByUnidadFuncional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.capacitacionFacade.findPlanAdiestramientoByUnidadFuncional(Long.valueOf(this.findSelectUnidadFuncional).longValue());
      this.showPlanAdiestramientoByUnidadFuncional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanAdiestramientoByUnidadFuncional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUnidadFuncional = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public String findPlanAdiestramientoByAreaConocimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.capacitacionFacade.findPlanAdiestramientoByAreaConocimiento(Long.valueOf(this.findSelectAreaConocimiento).longValue());
      this.showPlanAdiestramientoByAreaConocimiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanAdiestramientoByAreaConocimiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUnidadFuncional = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public boolean isShowPlanAdiestramientoByUnidadFuncional() {
    return this.showPlanAdiestramientoByUnidadFuncional;
  }
  public boolean isShowPlanAdiestramientoByAreaConocimiento() {
    return this.showPlanAdiestramientoByAreaConocimiento;
  }

  public String selectPlanAdiestramiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUnidadFuncional = null;
    this.selectTipoCurso = null;
    this.selectAreaConocimiento = null;
    this.selectPlanPersonal = null;

    long idPlanAdiestramiento = 
      Long.parseLong((String)requestParameterMap.get("idPlanAdiestramiento"));
    try
    {
      this.planAdiestramiento = 
        this.capacitacionFacade.findPlanAdiestramientoById(
        idPlanAdiestramiento);
      if (this.planAdiestramiento.getUnidadFuncional() != null) {
        this.selectUnidadFuncional = 
          String.valueOf(this.planAdiestramiento.getUnidadFuncional().getIdUnidadFuncional());
      }
      if (this.planAdiestramiento.getTipoCurso() != null) {
        this.selectTipoCurso = 
          String.valueOf(this.planAdiestramiento.getTipoCurso().getIdTipoCurso());
      }
      if (this.planAdiestramiento.getAreaConocimiento() != null) {
        this.selectAreaConocimiento = 
          String.valueOf(this.planAdiestramiento.getAreaConocimiento().getIdAreaConocimiento());
      }
      if (this.planAdiestramiento.getPlanPersonal() != null) {
        this.selectPlanPersonal = 
          String.valueOf(this.planAdiestramiento.getPlanPersonal().getIdPlanPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.planAdiestramiento = null;
    this.showPlanAdiestramientoByUnidadFuncional = false;
    this.showPlanAdiestramientoByAreaConocimiento = false;
    this.showReport = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    return null;
  }

  public String add() {
    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.planAdiestramiento = new PlanAdiestramiento();
    this.showReport = false;
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public boolean isShowReport() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}