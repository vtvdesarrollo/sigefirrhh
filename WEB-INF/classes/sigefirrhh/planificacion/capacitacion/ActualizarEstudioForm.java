package sigefirrhh.planificacion.capacitacion;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.login.LoginSession;

public class ActualizarEstudioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarEstudioForm.class.getName());
  private Collection listPlanAdiestramiento;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private CapacitacionFacade capacitacionFacade;
  private long idPlanAdiestramiento;
  private String selectIdPlanAdiestramiento;
  private String selectIdTipoDotacion;
  private int cedula;
  private String estatus;
  private String desempenio;
  private String evaluacionInstructor;
  private String evaluacionContenido;
  private String calificacion;
  private int cedulaFinal;
  private String estatusFinal;
  private String desempenioFinal;
  private String evaluacionFinalContenido;
  private String evaluacionFinalInstructor;
  private String calificacionFinal;
  private String nombre;
  private String apellido;
  private long idParticipante = 0L;
  private PlanAdiestramiento planAdiestramiento;
  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  StringBuffer sql = new StringBuffer();
  private Participante participante;
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private ResultSet rsRegistroTrabajador = null;
  private PreparedStatement stRegistroTrabajador = null;

  private ResultSet rsRegistroParticipante = null;
  private PreparedStatement stRegistroParticipante = null;

  private ResultSet rsRegistroEstudio = null;
  private PreparedStatement stRegistroEstudio = null;

  public ActualizarEstudioForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.capacitacionFacade = new CapacitacionFacade();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.connection.setAutoCommit(true);
      this.connection = Resource.getConnection();

      this.sql = new StringBuffer();
      this.sql.append("select p.id_personal, p.primer_nombre, p.primer_apellido ");
      this.sql.append(" from personal p ");
      this.sql.append(" where p.cedula = ? ");

      this.stRegistroTrabajador = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());

      this.sql = new StringBuffer();
      this.sql.append("select * ");
      this.sql.append(" from participante p ");
      this.sql.append(" where  p.id_personal = ? ");
      this.sql.append(" and  p.id_plan_adiestramiento = ? ");

      this.stRegistroParticipante = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());

      this.sql = new StringBuffer();
      this.sql.append("select * ");
      this.sql.append(" from estudio e ");
      this.sql.append(" where  e.id_personal = ? ");
      this.sql.append(" and  e.nombre_curso = ? ");
      this.sql.append(" and  e.anio = ? ");

      this.stRegistroEstudio = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return;
    }

    refresh();
  }

  public void refresh()
  {
    try
    {
      this.listPlanAdiestramiento = this.capacitacionFacade.findAllPlanAdiestramiento();
    }
    catch (Exception e)
    {
      this.listPlanAdiestramiento = new ArrayList();
      log.error("Excepcion controlada:", e);
    }
  }

  public void changePlanAdiestramiento(ValueChangeEvent event) {
    this.idPlanAdiestramiento = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdPlanAdiestramiento = String.valueOf(this.idPlanAdiestramiento);
    try {
      if (this.idPlanAdiestramiento > 0L)
        this.planAdiestramiento = this.capacitacionFacade.findPlanAdiestramientoById(this.idPlanAdiestramiento);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.nombre = "";
    this.apellido = "";
    this.cedulaFinal = 0;
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.stRegistroTrabajador.setInt(1, this.cedula);
      this.rsRegistroTrabajador = this.stRegistroTrabajador.executeQuery();

      if (this.rsRegistroTrabajador.next()) {
        this.nombre = this.rsRegistroTrabajador.getString("primer_nombre");
        this.apellido = this.rsRegistroTrabajador.getString("primer_apellido");
      }
      else {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona no esta registrada", ""));
        return null;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras se buscaba a la persona", ""));
      return null;
    }
    try
    {
      this.stRegistroParticipante.setLong(1, this.rsRegistroTrabajador.getLong("id_personal"));
      this.stRegistroParticipante.setLong(2, this.idPlanAdiestramiento);
      this.rsRegistroParticipante = this.stRegistroParticipante.executeQuery();

      this.sql = new StringBuffer();
      this.stExecute = this.connection.createStatement();
      if (this.rsRegistroParticipante.next())
      {
        this.sql.append("update participante set  estatus = '" + this.estatus + "', ");
        this.sql.append("  desempenio = '" + this.desempenio + "', ");
        this.sql.append("  evaluacion_instructor = '" + this.evaluacionInstructor + "', ");
        this.sql.append("  evaluacion_contenido = '" + this.evaluacionContenido + "' ");
        this.sql.append(" where id_participante = " + this.rsRegistroParticipante.getLong("id_participante"));
        this.stExecute.addBatch(this.sql.toString());
        this.stExecute.executeBatch();

        this.stExecute.close();
      }
      else
      {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona no esta registrada para este curso", ""));
        return null;
      }

      this.stRegistroEstudio.setLong(1, this.rsRegistroTrabajador.getLong("id_personal"));
      this.stRegistroEstudio.setString(2, this.planAdiestramiento.getNombreCurso());
      this.stRegistroEstudio.setInt(3, this.planAdiestramiento.getAnio());
      this.rsRegistroEstudio = this.stRegistroEstudio.executeQuery();

      this.sql = new StringBuffer();
      this.stExecute = this.connection.createStatement();
      if (this.rsRegistroEstudio.next()) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ya se actualizó la informacion para esta Persona", ""));
        return null;
      }

      this.sql.append("insert into estudio (id_personal, anios_experiencia, certifico, duracion, meses_experiencia, ");
      this.sql.append("origen_curso, participacion, unidad_tiempo, becado, financiamiento, id_pais, id_tipo_curso, ");
      this.sql.append("id_area_conocimiento, observaciones, nombre_curso, nombre_entidad, anio, escala, calificacion, id_estudio) values(");
      this.sql.append(this.rsRegistroTrabajador.getLong("id_personal") + ", 0, 'N', " + this.planAdiestramiento.getDuracion() + ", 0, 'O', '");
      this.sql.append(this.rsRegistroParticipante.getString("asistencia") + "', '" + this.planAdiestramiento.getUnidadTiempo() + "', 'N', 'N', 1, ");
      this.sql.append(this.planAdiestramiento.getTipoCurso().getIdTipoCurso() + ", " + this.planAdiestramiento.getAreaConocimiento().getIdAreaConocimiento() + ",");
      this.sql.append("null, '" + this.planAdiestramiento.getNombreCurso() + "', '" + this.planAdiestramiento.getNombreEntidad() + "', ");
      this.sql.append(this.planAdiestramiento.getAnio() + ", null, '" + this.calificacion + "', ");
      this.sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Estudio") + ")");
      this.stExecute.addBatch(this.sql.toString());
      this.stExecute.executeBatch();

      this.stExecute.close();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras se buscaba en la tabla participante o actualizaba la tabla estudio", ""));
      return null;
    }

    this.cedulaFinal = this.cedula;
    this.estatusFinal = this.estatus;
    this.desempenioFinal = this.desempenio;
    this.evaluacionFinalContenido = this.evaluacionContenido;
    this.evaluacionFinalInstructor = this.evaluacionInstructor;
    this.calificacionFinal = this.calificacion;

    this.cedula = 0;

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListPlanAdiestramiento() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listPlanAdiestramiento, "sigefirrhh.planificacion.capacitacion.PlanAdiestramiento");
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Participante.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListEvaluacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = Participante.LISTA_DESEMPENIO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListDesempenio() {
    Collection col = new ArrayList();

    Iterator iterEntry = Participante.LISTA_DESEMPENIO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public int getCedula() {
    return this.cedula;
  }

  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public int getCedulaFinal() {
    return this.cedulaFinal;
  }
  public void setCedulaFinal(int cedulaFinal) {
    this.cedulaFinal = cedulaFinal;
  }
  public String getApellido() {
    return this.apellido;
  }
  public void setApellido(String apellido) {
    this.apellido = apellido;
  }
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getSelectIdTipoDotacion()
  {
    return this.selectIdTipoDotacion;
  }
  public void setSelectIdTipoDotacion(String selectIdTipoDotacion) {
    this.selectIdTipoDotacion = selectIdTipoDotacion;
  }

  public String getSelectIdPlanAdiestramiento() {
    return this.selectIdPlanAdiestramiento;
  }
  public void setSelectIdPlanAdiestramiento(String selectIdPlanAdiestramiento) {
    this.selectIdPlanAdiestramiento = selectIdPlanAdiestramiento;
  }

  public String getCalificacion() {
    return this.calificacion;
  }

  public void setCalificacion(String calificacion) {
    this.calificacion = calificacion;
  }

  public String getCalificacionFinal() {
    return this.calificacionFinal;
  }

  public void setCalificacionFinal(String calificacionFinal) {
    this.calificacionFinal = calificacionFinal;
  }

  public String getDesempenio() {
    return this.desempenio;
  }

  public void setDesempenio(String desempenio) {
    this.desempenio = desempenio;
  }

  public String getEstatus() {
    return this.estatus;
  }

  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }

  public String getEstatusFinal() {
    return this.estatusFinal;
  }

  public void setEstatusFinal(String estatusFinal) {
    this.estatusFinal = estatusFinal;
  }

  public String getEvaluacionContenido() {
    return this.evaluacionContenido;
  }

  public void setEvaluacionContenido(String evaluacionContenido) {
    this.evaluacionContenido = evaluacionContenido;
  }

  public String getEvaluacionInstructor() {
    return this.evaluacionInstructor;
  }

  public void setEvaluacionInstructor(String evaluacionInstructor) {
    this.evaluacionInstructor = evaluacionInstructor;
  }

  public String getEvaluacionFinalContenido() {
    return this.evaluacionFinalContenido;
  }

  public void setEvaluacionFinalContenido(String evaluacionFinalContenido) {
    this.evaluacionFinalContenido = evaluacionFinalContenido;
  }

  public String getDesempenioFinal() {
    return this.desempenioFinal;
  }

  public void setDesempenioFinal(String desempenioFinal) {
    this.desempenioFinal = desempenioFinal;
  }

  public String getEvaluacionFinalInstructor() {
    return this.evaluacionFinalInstructor;
  }

  public void setEvaluacionFinalInstructor(String evaluacionFinalInstructor) {
    this.evaluacionFinalInstructor = evaluacionFinalInstructor;
  }
}