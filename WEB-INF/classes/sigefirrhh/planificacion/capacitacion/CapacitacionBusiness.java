package sigefirrhh.planificacion.capacitacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class CapacitacionBusiness extends AbstractBusiness
  implements Serializable
{
  private ParticipanteBeanBusiness participanteBeanBusiness = new ParticipanteBeanBusiness();

  private PlanAdiestramientoBeanBusiness planAdiestramientoBeanBusiness = new PlanAdiestramientoBeanBusiness();

  public void addParticipante(Participante participante)
    throws Exception
  {
    this.participanteBeanBusiness.addParticipante(participante);
  }

  public void updateParticipante(Participante participante) throws Exception {
    this.participanteBeanBusiness.updateParticipante(participante);
  }

  public void deleteParticipante(Participante participante) throws Exception {
    this.participanteBeanBusiness.deleteParticipante(participante);
  }

  public Participante findParticipanteById(long participanteId) throws Exception {
    return this.participanteBeanBusiness.findParticipanteById(participanteId);
  }

  public Collection findAllParticipante() throws Exception {
    return this.participanteBeanBusiness.findParticipanteAll();
  }

  public Collection findParticipanteByPlanAdiestramiento(long idPlanAdiestramiento)
    throws Exception
  {
    return this.participanteBeanBusiness.findByPlanAdiestramiento(idPlanAdiestramiento);
  }

  public void addPlanAdiestramiento(PlanAdiestramiento planAdiestramiento)
    throws Exception
  {
    this.planAdiestramientoBeanBusiness.addPlanAdiestramiento(planAdiestramiento);
  }

  public void updatePlanAdiestramiento(PlanAdiestramiento planAdiestramiento) throws Exception {
    this.planAdiestramientoBeanBusiness.updatePlanAdiestramiento(planAdiestramiento);
  }

  public void deletePlanAdiestramiento(PlanAdiestramiento planAdiestramiento) throws Exception {
    this.planAdiestramientoBeanBusiness.deletePlanAdiestramiento(planAdiestramiento);
  }

  public PlanAdiestramiento findPlanAdiestramientoById(long planAdiestramientoId) throws Exception {
    return this.planAdiestramientoBeanBusiness.findPlanAdiestramientoById(planAdiestramientoId);
  }

  public Collection findAllPlanAdiestramiento() throws Exception {
    return this.planAdiestramientoBeanBusiness.findPlanAdiestramientoAll();
  }

  public Collection findPlanAdiestramientoByUnidadFuncional(long idUnidadFuncional)
    throws Exception
  {
    return this.planAdiestramientoBeanBusiness.findByUnidadFuncional(idUnidadFuncional);
  }

  public Collection findPlanAdiestramientoByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    return this.planAdiestramientoBeanBusiness.findByAreaConocimiento(idAreaConocimiento);
  }
}