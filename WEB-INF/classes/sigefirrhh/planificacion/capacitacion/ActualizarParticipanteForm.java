package sigefirrhh.planificacion.capacitacion;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.login.LoginSession;

public class ActualizarParticipanteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarParticipanteForm.class.getName());
  private Collection listPlanAdiestramiento;
  private Collection listTipoDotacion;
  private Collection listSubtipoDotacion;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private CapacitacionFacade capacitacionFacade;
  private long idTipoDotacion;
  private long idSubtipoDotacion;
  private long idPlanAdiestramiento;
  private String selectIdPlanAdiestramiento;
  private String selectIdTipoDotacion;
  private int cedula;
  private String asistencia;
  private int cedulaFinal;
  private String asistenciaFinal;
  private String nombre;
  private String apellido;
  private long idParticipante = 0L;
  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  StringBuffer sql = new StringBuffer();
  private Participante participante;
  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private ResultSet rsRegistroTrabajador = null;
  private PreparedStatement stRegistroTrabajador = null;

  private ResultSet rsRegistroParticipante = null;
  private PreparedStatement stRegistroParticipante = null;

  public boolean isShowTipoDotacion() {
    return (this.listTipoDotacion != null) && (!this.listTipoDotacion.isEmpty());
  }
  public boolean isShowSubtipoDotacion() {
    return (this.listSubtipoDotacion != null) && (!this.listSubtipoDotacion.isEmpty());
  }

  public ActualizarParticipanteForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.capacitacionFacade = new CapacitacionFacade();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.connection.setAutoCommit(true);
      this.connection = Resource.getConnection();

      this.sql = new StringBuffer();
      this.sql.append("select p.id_personal, p.primer_nombre, p.primer_apellido ");
      this.sql.append(" from personal p ");
      this.sql.append(" where p.cedula = ? ");

      this.stRegistroTrabajador = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());

      this.sql = new StringBuffer();
      this.sql.append("select * ");
      this.sql.append(" from participante p ");
      this.sql.append(" where  p.id_personal = ? ");
      this.sql.append(" and  p.id_plan_adiestramiento = ? ");

      this.stRegistroParticipante = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return;
    }

    refresh();
  }

  public void refresh()
  {
    try
    {
      this.listPlanAdiestramiento = this.capacitacionFacade.findAllPlanAdiestramiento();
    }
    catch (Exception e)
    {
      this.listPlanAdiestramiento = new ArrayList();
      log.error("Excepcion controlada:", e);
    }
  }

  public void changePlanAdiestramiento(ValueChangeEvent event) {
    this.idPlanAdiestramiento = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdPlanAdiestramiento = String.valueOf(this.idPlanAdiestramiento);

    this.nombre = "";
    this.apellido = "";
    this.cedulaFinal = 0;
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.stRegistroTrabajador.setInt(1, this.cedula);
      this.rsRegistroTrabajador = this.stRegistroTrabajador.executeQuery();

      if (this.rsRegistroTrabajador.next()) {
        this.nombre = this.rsRegistroTrabajador.getString("primer_nombre");
        this.apellido = this.rsRegistroTrabajador.getString("primer_apellido");
      }
      else {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona no esta registrada", ""));
        return null;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras a la persona", ""));
      return null;
    }
    try
    {
      this.stRegistroParticipante.setLong(1, this.rsRegistroTrabajador.getLong("id_personal"));
      this.stRegistroParticipante.setLong(2, this.idPlanAdiestramiento);
      this.rsRegistroParticipante = this.stRegistroParticipante.executeQuery();

      this.sql = new StringBuffer();
      this.stExecute = this.connection.createStatement();
      if (this.rsRegistroParticipante.next())
      {
        this.sql.append("update participante set  asistencia = '" + this.asistencia + "' ");
        this.sql.append(" where id_participante = " + this.rsRegistroParticipante.getLong("id_participante"));
        this.stExecute.addBatch(this.sql.toString());
        this.stExecute.executeBatch();

        this.stExecute.close();
      }
      else
      {
        this.sql.append("insert into participante (id_personal, id_plan_adiestramiento, desempenio, ");
        this.sql.append("evaluacion_contenido, evaluacion_instructor, estatus, asistencia, id_participante) values (");
        this.sql.append(this.rsRegistroTrabajador.getLong("id_personal") + ", " + this.idPlanAdiestramiento);
        this.sql.append(", '0', '0', '0', '0', '" + this.asistencia + "', ");
        this.sql.append(this.identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.capacitacion.Participante") + ")");
        this.stExecute.addBatch(this.sql.toString());
        this.stExecute.executeBatch();

        this.stExecute.close();
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras se buscaba en la tabla participante", ""));
      return null;
    }

    this.cedulaFinal = this.cedula;
    this.asistenciaFinal = this.asistencia;

    this.cedula = 0;

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListPlanAdiestramiento() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listPlanAdiestramiento, "sigefirrhh.planificacion.capacitacion.PlanAdiestramiento");
  }

  public Collection getListTipoDotacion()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoDotacion, "sigefirrhh.base.bienestar.TipoDotacion");
  }

  public Collection getListSubtipoDotacion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listSubtipoDotacion, "sigefirrhh.base.bienestar.SubtipoDotacion");
  }

  public Collection getListAsistencia()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Participante.LISTA_ASISTENCIA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public int getCedula() {
    return this.cedula;
  }

  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public int getCedulaFinal() {
    return this.cedulaFinal;
  }
  public void setCedulaFinal(int cedulaFinal) {
    this.cedulaFinal = cedulaFinal;
  }
  public String getApellido() {
    return this.apellido;
  }
  public void setApellido(String apellido) {
    this.apellido = apellido;
  }
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public long getIdSubtipoDotacion() {
    return this.idSubtipoDotacion;
  }
  public void setIdSubtipoDotacion(long idSubtipoDotacion) {
    this.idSubtipoDotacion = idSubtipoDotacion;
  }
  public long getIdTipoDotacion() {
    return this.idTipoDotacion;
  }
  public void setIdTipoDotacion(long idTipoDotacion) {
    this.idTipoDotacion = idTipoDotacion;
  }

  public String getSelectIdTipoDotacion() {
    return this.selectIdTipoDotacion;
  }
  public void setSelectIdTipoDotacion(String selectIdTipoDotacion) {
    this.selectIdTipoDotacion = selectIdTipoDotacion;
  }
  public String getAsistencia() {
    return this.asistencia;
  }
  public void setAsistencia(String asistencia) {
    this.asistencia = asistencia;
  }
  public String getAsistenciaFinal() {
    return this.asistenciaFinal;
  }
  public void setAsistenciaFinal(String asistenciaFinal) {
    this.asistenciaFinal = asistenciaFinal;
  }
  public String getSelectIdPlanAdiestramiento() {
    return this.selectIdPlanAdiestramiento;
  }
  public void setSelectIdPlanAdiestramiento(String selectIdPlanAdiestramiento) {
    this.selectIdPlanAdiestramiento = selectIdPlanAdiestramiento;
  }
}