package sigefirrhh.planificacion.capacitacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ParticipanteForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ParticipanteForm.class.getName());
  private Participante participante;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private CapacitacionFacade capacitacionFacade = new CapacitacionFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colUnidadFuncionalForPlanAdiestramiento;
  private Collection colPlanAdiestramiento;
  private Collection colUnidadFuncionalForTrabajador;
  private Collection colDependenciaForTrabajador;
  private Collection colTrabajador;
  private String selectUnidadFuncionalForPlanAdiestramiento;
  private String selectPlanAdiestramiento;
  private String selectUnidadFuncionalForTrabajador;
  private String selectDependenciaForTrabajador;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Object stateResultParticipanteByTrabajador = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectUnidadFuncionalForPlanAdiestramiento()
  {
    return this.selectUnidadFuncionalForPlanAdiestramiento;
  }
  public void setSelectUnidadFuncionalForPlanAdiestramiento(String valUnidadFuncionalForPlanAdiestramiento) {
    this.selectUnidadFuncionalForPlanAdiestramiento = valUnidadFuncionalForPlanAdiestramiento;
  }
  public void changeUnidadFuncionalForPlanAdiestramiento(ValueChangeEvent event) {
    long idUnidadFuncional = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPlanAdiestramiento = null;
      if (idUnidadFuncional > 0L)
        this.colPlanAdiestramiento = 
          this.capacitacionFacade.findPlanAdiestramientoByUnidadFuncional(
          idUnidadFuncional);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changePlanAdiestramiento(ValueChangeEvent event) {
    this.selectPlanAdiestramiento = 
      ((String)event.getNewValue());
  }

  public boolean isShowUnidadFuncionalForPlanAdiestramiento()
  {
    return this.colUnidadFuncionalForPlanAdiestramiento != null;
  }
  public String getSelectPlanAdiestramiento() {
    return this.selectPlanAdiestramiento;
  }
  public void setSelectPlanAdiestramiento(String valPlanAdiestramiento) {
    log.error("paso 0 ");
    Iterator iterator = this.colPlanAdiestramiento.iterator();
    PlanAdiestramiento planAdiestramiento = null;
    this.participante.setPlanAdiestramiento(null);
    while (iterator.hasNext()) {
      planAdiestramiento = (PlanAdiestramiento)iterator.next();
      if (String.valueOf(planAdiestramiento.getIdPlanAdiestramiento()).equals(
        valPlanAdiestramiento)) {
        this.participante.setPlanAdiestramiento(
          planAdiestramiento);
        log.error("paso 1 ");
        break;
      }
    }
    this.selectPlanAdiestramiento = valPlanAdiestramiento;
  }
  public boolean isShowPlanAdiestramiento() {
    return this.colPlanAdiestramiento != null;
  }
  public String getSelectUnidadFuncionalForTrabajador() {
    return this.selectUnidadFuncionalForTrabajador;
  }
  public void setSelectUnidadFuncionalForTrabajador(String valUnidadFuncionalForTrabajador) {
    this.selectUnidadFuncionalForTrabajador = valUnidadFuncionalForTrabajador;
  }
  public void changeUnidadFuncionalForTrabajador(ValueChangeEvent event) {
    long idUnidadFuncional = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colTrabajador = null;
      this.colDependenciaForTrabajador = null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowUnidadFuncionalForTrabajador() { return this.colUnidadFuncionalForTrabajador != null; }

  public String getSelectDependenciaForTrabajador() {
    return this.selectDependenciaForTrabajador;
  }
  public void setSelectDependenciaForTrabajador(String valDependenciaForTrabajador) {
    this.selectDependenciaForTrabajador = valDependenciaForTrabajador;
  }
  public void changeDependenciaForTrabajador(ValueChangeEvent event) {
    long idDependencia = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colTrabajador = null;
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowDependenciaForTrabajador() { return this.colDependenciaForTrabajador != null; }

  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;

    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador))
      {
        break;
      }
    }

    this.selectTrabajador = valTrabajador;
  }
  public boolean isShowTrabajador() {
    return this.colTrabajador != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public Participante getParticipante() {
    if (this.participante == null) {
      this.participante = new Participante();
    }
    return this.participante;
  }

  public ParticipanteForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    log.error("PASO 0000000 ");
    refresh();
  }

  public Collection getColUnidadFuncionalForPlanAdiestramiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncionalForPlanAdiestramiento.iterator();
    UnidadFuncional unidadFuncionalForPlanAdiestramiento = null;
    while (iterator.hasNext()) {
      unidadFuncionalForPlanAdiestramiento = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncionalForPlanAdiestramiento.getIdUnidadFuncional()), 
        unidadFuncionalForPlanAdiestramiento.toString()));
    }
    return col;
  }

  public Collection getColPlanAdiestramiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanAdiestramiento.iterator();
    PlanAdiestramiento planAdiestramiento = null;
    while (iterator.hasNext()) {
      planAdiestramiento = (PlanAdiestramiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planAdiestramiento.getIdPlanAdiestramiento()), 
        planAdiestramiento.toString()));
    }
    return col;
  }

  public Collection getListAsistencia() {
    Collection col = new ArrayList();

    Iterator iterEntry = Participante.LISTA_ASISTENCIA.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEstatus() {
    Collection col = new ArrayList();

    Iterator iterEntry = Participante.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListDesempenio() {
    Collection col = new ArrayList();

    Iterator iterEntry = Participante.LISTA_DESEMPENIO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColUnidadFuncionalForTrabajador() {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncionalForTrabajador.iterator();
    UnidadFuncional unidadFuncionalForTrabajador = null;
    while (iterator.hasNext()) {
      unidadFuncionalForTrabajador = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncionalForTrabajador.getIdUnidadFuncional()), 
        unidadFuncionalForTrabajador.toString()));
    }
    return col;
  }

  public Collection getColDependenciaForTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependenciaForTrabajador.iterator();
    Dependencia dependenciaForTrabajador = null;
    while (iterator.hasNext()) {
      dependenciaForTrabajador = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependenciaForTrabajador.getIdDependencia()), 
        dependenciaForTrabajador.toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        new DefinicionesFacade().findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colUnidadFuncionalForPlanAdiestramiento = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colUnidadFuncionalForTrabajador = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findParticipanteByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding)
      {
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectParticipante()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPlanAdiestramiento = null;
    this.selectUnidadFuncionalForPlanAdiestramiento = null;

    this.selectTrabajador = null;
    this.selectUnidadFuncionalForTrabajador = null;

    this.selectDependenciaForTrabajador = null;

    long idParticipante = 
      Long.parseLong((String)requestParameterMap.get("idParticipante"));
    try
    {
      this.participante = 
        this.capacitacionFacade.findParticipanteById(
        idParticipante);

      if (this.participante.getPlanAdiestramiento() != null) {
        this.selectPlanAdiestramiento = 
          String.valueOf(this.participante.getPlanAdiestramiento().getIdPlanAdiestramiento());
      }

      PlanAdiestramiento planAdiestramiento = null;
      UnidadFuncional unidadFuncionalForPlanAdiestramiento = null;
      Trabajador trabajador = null;
      Dependencia dependenciaForTrabajador = null;
      UnidadFuncional unidadFuncionalForTrabajador = null;

      if (this.participante.getPlanAdiestramiento() != null) {
        long idPlanAdiestramiento = 
          this.participante.getPlanAdiestramiento().getIdPlanAdiestramiento();
        this.selectPlanAdiestramiento = String.valueOf(idPlanAdiestramiento);
        planAdiestramiento = this.capacitacionFacade.findPlanAdiestramientoById(
          idPlanAdiestramiento);
        this.colPlanAdiestramiento = this.capacitacionFacade.findPlanAdiestramientoByUnidadFuncional(
          planAdiestramiento.getUnidadFuncional().getIdUnidadFuncional());

        long idUnidadFuncionalForPlanAdiestramiento = 
          this.participante.getPlanAdiestramiento().getUnidadFuncional().getIdUnidadFuncional();
        this.selectUnidadFuncionalForPlanAdiestramiento = String.valueOf(idUnidadFuncionalForPlanAdiestramiento);
        unidadFuncionalForPlanAdiestramiento = 
          this.estructuraFacade.findUnidadFuncionalById(
          idUnidadFuncionalForPlanAdiestramiento);
        this.colUnidadFuncionalForPlanAdiestramiento = 
          this.estructuraFacade.findAllUnidadFuncional();
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    log.error("PASO 1 ");
    boolean error = false;

    if (error) {
      return null;
    }
    try
    {
      if (this.adding)
      {
        this.capacitacionFacade.addParticipante(
          this.participante);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.participante, this.trabajador.getPersonal());

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.capacitacionFacade.updateParticipante(
          this.participante);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.participante, this.trabajador.getPersonal());

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.capacitacionFacade.deleteParticipante(
        this.participante);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.participante, this.trabajador.getPersonal());

      context.addMessage("success_delete", new FacesMessage("Se eliminï¿½ con ï¿½xito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectTrabajador = null;

    this.selectUnidadFuncionalForTrabajador = null;

    this.selectDependenciaForTrabajador = null;

    this.participante = new Participante();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.participante.setIdParticipante(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.capacitacion.Participante"));

    return null;
  }

  public boolean isShowDesempenioAux() {
    try {
      return this.participante.getEstatus().equals("A"); } catch (Exception e) {
    }
    return false;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.participante = new Participante();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.participante = new Participante();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}