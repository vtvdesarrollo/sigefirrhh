package sigefirrhh.planificacion.capacitacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Personal;

public class Participante
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ASISTENCIA;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_DESEMPENIO;
  private long idParticipante;
  private PlanAdiestramiento planAdiestramiento;
  private String asistencia;
  private String estatus;
  private String desempenio;
  private String evaluacionInstructor;
  private String evaluacionContenido;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "asistencia", "desempenio", "estatus", "evaluacionContenido", "evaluacionInstructor", "idParticipante", "personal", "planAdiestramiento" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.planificacion.capacitacion.PlanAdiestramiento") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.capacitacion.Participante"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Participante());

    LISTA_ASISTENCIA = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_DESEMPENIO = 
      new LinkedHashMap();
    LISTA_ASISTENCIA.put("P", "PARTICIPANTE");
    LISTA_ASISTENCIA.put("I", "INSTRUCTOR");
    LISTA_ASISTENCIA.put("O", "OYENTE");
    LISTA_ESTATUS.put("0", "INSCRITO");
    LISTA_ESTATUS.put("A", "ASISTIO");
    LISTA_ESTATUS.put("N", "NO ASISTIO");
    LISTA_ESTATUS.put("R", "NO ASISTIO POR REPOSO");
    LISTA_ESTATUS.put("P", "NO ASISTIO POR PERMISO");
    LISTA_ESTATUS.put("V", "NO ASISTIO POR VACACIONES");
    LISTA_DESEMPENIO.put("0", "POR EVALUAR");
    LISTA_DESEMPENIO.put("E", "EXCELENTE");
    LISTA_DESEMPENIO.put("N", "NORMAL");
    LISTA_DESEMPENIO.put("R", "REGULAR");
    LISTA_DESEMPENIO.put("D", "DEFICIENTE");
  }

  public Participante()
  {
    jdoSetasistencia(this, "P");

    jdoSetestatus(this, "I");

    jdoSetdesempenio(this, "N");

    jdoSetevaluacionInstructor(this, "N");

    jdoSetevaluacionContenido(this, "N");
  }

  public String toString()
  {
    return jdoGetpersonal(this).getPrimerApellido() + ", " + 
      jdoGetpersonal(this).getPrimerNombre() + " - " + jdoGetplanAdiestramiento(this).toString();
  }

  public String getAsistencia() {
    return jdoGetasistencia(this);
  }
  public void setAsistencia(String asistencia) {
    jdoSetasistencia(this, asistencia);
  }
  public String getDesempenio() {
    return jdoGetdesempenio(this);
  }
  public void setDesempenio(String desempenio) {
    jdoSetdesempenio(this, desempenio);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public long getIdParticipante() {
    return jdoGetidParticipante(this);
  }
  public void setIdParticipante(long idParticipante) {
    jdoSetidParticipante(this, idParticipante);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public PlanAdiestramiento getPlanAdiestramiento() {
    return jdoGetplanAdiestramiento(this);
  }
  public void setPlanAdiestramiento(PlanAdiestramiento planAdiestramiento) {
    jdoSetplanAdiestramiento(this, planAdiestramiento);
  }
  public String getEvaluacionContenido() {
    return jdoGetevaluacionContenido(this);
  }
  public void setEvaluacionContenido(String evaluacionContenido) {
    jdoSetevaluacionContenido(this, evaluacionContenido);
  }
  public String getEvaluacionInstructor() {
    return jdoGetevaluacionInstructor(this);
  }
  public void setEvaluacionInstructor(String evaluacionInstructor) {
    jdoSetevaluacionInstructor(this, evaluacionInstructor);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Participante localParticipante = new Participante();
    localParticipante.jdoFlags = 1;
    localParticipante.jdoStateManager = paramStateManager;
    return localParticipante;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Participante localParticipante = new Participante();
    localParticipante.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParticipante.jdoFlags = 1;
    localParticipante.jdoStateManager = paramStateManager;
    return localParticipante;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.asistencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.desempenio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.evaluacionContenido);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.evaluacionInstructor);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idParticipante);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planAdiestramiento);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.asistencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.desempenio = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.evaluacionContenido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.evaluacionInstructor = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idParticipante = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planAdiestramiento = ((PlanAdiestramiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Participante paramParticipante, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.asistencia = paramParticipante.asistencia;
      return;
    case 1:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.desempenio = paramParticipante.desempenio;
      return;
    case 2:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramParticipante.estatus;
      return;
    case 3:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.evaluacionContenido = paramParticipante.evaluacionContenido;
      return;
    case 4:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.evaluacionInstructor = paramParticipante.evaluacionInstructor;
      return;
    case 5:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.idParticipante = paramParticipante.idParticipante;
      return;
    case 6:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramParticipante.personal;
      return;
    case 7:
      if (paramParticipante == null)
        throw new IllegalArgumentException("arg1");
      this.planAdiestramiento = paramParticipante.planAdiestramiento;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Participante))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Participante localParticipante = (Participante)paramObject;
    if (localParticipante.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParticipante, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParticipantePK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParticipantePK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParticipantePK))
      throw new IllegalArgumentException("arg1");
    ParticipantePK localParticipantePK = (ParticipantePK)paramObject;
    localParticipantePK.idParticipante = this.idParticipante;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParticipantePK))
      throw new IllegalArgumentException("arg1");
    ParticipantePK localParticipantePK = (ParticipantePK)paramObject;
    this.idParticipante = localParticipantePK.idParticipante;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParticipantePK))
      throw new IllegalArgumentException("arg2");
    ParticipantePK localParticipantePK = (ParticipantePK)paramObject;
    localParticipantePK.idParticipante = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParticipantePK))
      throw new IllegalArgumentException("arg2");
    ParticipantePK localParticipantePK = (ParticipantePK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localParticipantePK.idParticipante);
  }

  private static final String jdoGetasistencia(Participante paramParticipante)
  {
    if (paramParticipante.jdoFlags <= 0)
      return paramParticipante.asistencia;
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
      return paramParticipante.asistencia;
    if (localStateManager.isLoaded(paramParticipante, jdoInheritedFieldCount + 0))
      return paramParticipante.asistencia;
    return localStateManager.getStringField(paramParticipante, jdoInheritedFieldCount + 0, paramParticipante.asistencia);
  }

  private static final void jdoSetasistencia(Participante paramParticipante, String paramString)
  {
    if (paramParticipante.jdoFlags == 0)
    {
      paramParticipante.asistencia = paramString;
      return;
    }
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.asistencia = paramString;
      return;
    }
    localStateManager.setStringField(paramParticipante, jdoInheritedFieldCount + 0, paramParticipante.asistencia, paramString);
  }

  private static final String jdoGetdesempenio(Participante paramParticipante)
  {
    if (paramParticipante.jdoFlags <= 0)
      return paramParticipante.desempenio;
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
      return paramParticipante.desempenio;
    if (localStateManager.isLoaded(paramParticipante, jdoInheritedFieldCount + 1))
      return paramParticipante.desempenio;
    return localStateManager.getStringField(paramParticipante, jdoInheritedFieldCount + 1, paramParticipante.desempenio);
  }

  private static final void jdoSetdesempenio(Participante paramParticipante, String paramString)
  {
    if (paramParticipante.jdoFlags == 0)
    {
      paramParticipante.desempenio = paramString;
      return;
    }
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.desempenio = paramString;
      return;
    }
    localStateManager.setStringField(paramParticipante, jdoInheritedFieldCount + 1, paramParticipante.desempenio, paramString);
  }

  private static final String jdoGetestatus(Participante paramParticipante)
  {
    if (paramParticipante.jdoFlags <= 0)
      return paramParticipante.estatus;
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
      return paramParticipante.estatus;
    if (localStateManager.isLoaded(paramParticipante, jdoInheritedFieldCount + 2))
      return paramParticipante.estatus;
    return localStateManager.getStringField(paramParticipante, jdoInheritedFieldCount + 2, paramParticipante.estatus);
  }

  private static final void jdoSetestatus(Participante paramParticipante, String paramString)
  {
    if (paramParticipante.jdoFlags == 0)
    {
      paramParticipante.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramParticipante, jdoInheritedFieldCount + 2, paramParticipante.estatus, paramString);
  }

  private static final String jdoGetevaluacionContenido(Participante paramParticipante)
  {
    if (paramParticipante.jdoFlags <= 0)
      return paramParticipante.evaluacionContenido;
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
      return paramParticipante.evaluacionContenido;
    if (localStateManager.isLoaded(paramParticipante, jdoInheritedFieldCount + 3))
      return paramParticipante.evaluacionContenido;
    return localStateManager.getStringField(paramParticipante, jdoInheritedFieldCount + 3, paramParticipante.evaluacionContenido);
  }

  private static final void jdoSetevaluacionContenido(Participante paramParticipante, String paramString)
  {
    if (paramParticipante.jdoFlags == 0)
    {
      paramParticipante.evaluacionContenido = paramString;
      return;
    }
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.evaluacionContenido = paramString;
      return;
    }
    localStateManager.setStringField(paramParticipante, jdoInheritedFieldCount + 3, paramParticipante.evaluacionContenido, paramString);
  }

  private static final String jdoGetevaluacionInstructor(Participante paramParticipante)
  {
    if (paramParticipante.jdoFlags <= 0)
      return paramParticipante.evaluacionInstructor;
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
      return paramParticipante.evaluacionInstructor;
    if (localStateManager.isLoaded(paramParticipante, jdoInheritedFieldCount + 4))
      return paramParticipante.evaluacionInstructor;
    return localStateManager.getStringField(paramParticipante, jdoInheritedFieldCount + 4, paramParticipante.evaluacionInstructor);
  }

  private static final void jdoSetevaluacionInstructor(Participante paramParticipante, String paramString)
  {
    if (paramParticipante.jdoFlags == 0)
    {
      paramParticipante.evaluacionInstructor = paramString;
      return;
    }
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.evaluacionInstructor = paramString;
      return;
    }
    localStateManager.setStringField(paramParticipante, jdoInheritedFieldCount + 4, paramParticipante.evaluacionInstructor, paramString);
  }

  private static final long jdoGetidParticipante(Participante paramParticipante)
  {
    return paramParticipante.idParticipante;
  }

  private static final void jdoSetidParticipante(Participante paramParticipante, long paramLong)
  {
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.idParticipante = paramLong;
      return;
    }
    localStateManager.setLongField(paramParticipante, jdoInheritedFieldCount + 5, paramParticipante.idParticipante, paramLong);
  }

  private static final Personal jdoGetpersonal(Participante paramParticipante)
  {
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
      return paramParticipante.personal;
    if (localStateManager.isLoaded(paramParticipante, jdoInheritedFieldCount + 6))
      return paramParticipante.personal;
    return (Personal)localStateManager.getObjectField(paramParticipante, jdoInheritedFieldCount + 6, paramParticipante.personal);
  }

  private static final void jdoSetpersonal(Participante paramParticipante, Personal paramPersonal)
  {
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramParticipante, jdoInheritedFieldCount + 6, paramParticipante.personal, paramPersonal);
  }

  private static final PlanAdiestramiento jdoGetplanAdiestramiento(Participante paramParticipante)
  {
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
      return paramParticipante.planAdiestramiento;
    if (localStateManager.isLoaded(paramParticipante, jdoInheritedFieldCount + 7))
      return paramParticipante.planAdiestramiento;
    return (PlanAdiestramiento)localStateManager.getObjectField(paramParticipante, jdoInheritedFieldCount + 7, paramParticipante.planAdiestramiento);
  }

  private static final void jdoSetplanAdiestramiento(Participante paramParticipante, PlanAdiestramiento paramPlanAdiestramiento)
  {
    StateManager localStateManager = paramParticipante.jdoStateManager;
    if (localStateManager == null)
    {
      paramParticipante.planAdiestramiento = paramPlanAdiestramiento;
      return;
    }
    localStateManager.setObjectField(paramParticipante, jdoInheritedFieldCount + 7, paramParticipante.planAdiestramiento, paramPlanAdiestramiento);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}