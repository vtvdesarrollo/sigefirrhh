package sigefirrhh.planificacion.capacitacion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class CapacitacionFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private CapacitacionBusiness capacitacionBusiness = new CapacitacionBusiness();

  public void addParticipante(Participante participante)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.capacitacionBusiness.addParticipante(participante);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateParticipante(Participante participante) throws Exception
  {
    try { this.txn.open();
      this.capacitacionBusiness.updateParticipante(participante);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteParticipante(Participante participante) throws Exception
  {
    try { this.txn.open();
      this.capacitacionBusiness.deleteParticipante(participante);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Participante findParticipanteById(long participanteId) throws Exception
  {
    try { this.txn.open();
      Participante participante = 
        this.capacitacionBusiness.findParticipanteById(participanteId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(participante);
      return participante;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllParticipante() throws Exception
  {
    try { this.txn.open();
      return this.capacitacionBusiness.findAllParticipante();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findParticipanteByPlanAdiestramiento(long idPlanAdiestramiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.capacitacionBusiness.findParticipanteByPlanAdiestramiento(idPlanAdiestramiento);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPlanAdiestramiento(PlanAdiestramiento planAdiestramiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.capacitacionBusiness.addPlanAdiestramiento(planAdiestramiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePlanAdiestramiento(PlanAdiestramiento planAdiestramiento) throws Exception
  {
    try { this.txn.open();
      this.capacitacionBusiness.updatePlanAdiestramiento(planAdiestramiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePlanAdiestramiento(PlanAdiestramiento planAdiestramiento) throws Exception
  {
    try { this.txn.open();
      this.capacitacionBusiness.deletePlanAdiestramiento(planAdiestramiento);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PlanAdiestramiento findPlanAdiestramientoById(long planAdiestramientoId) throws Exception
  {
    try { this.txn.open();
      PlanAdiestramiento planAdiestramiento = 
        this.capacitacionBusiness.findPlanAdiestramientoById(planAdiestramientoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(planAdiestramiento);
      return planAdiestramiento;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPlanAdiestramiento() throws Exception
  {
    try { this.txn.open();
      return this.capacitacionBusiness.findAllPlanAdiestramiento();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanAdiestramientoByUnidadFuncional(long idUnidadFuncional)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.capacitacionBusiness.findPlanAdiestramientoByUnidadFuncional(idUnidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanAdiestramientoByAreaConocimiento(long idAreaConocimiento)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.capacitacionBusiness.findPlanAdiestramientoByAreaConocimiento(idAreaConocimiento);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}