package sigefirrhh.planificacion.capacitacion;

import java.io.Serializable;

public class PlanAdiestramientoPK
  implements Serializable
{
  public long idPlanAdiestramiento;

  public PlanAdiestramientoPK()
  {
  }

  public PlanAdiestramientoPK(long idPlanAdiestramiento)
  {
    this.idPlanAdiestramiento = idPlanAdiestramiento;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanAdiestramientoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanAdiestramientoPK thatPK)
  {
    return 
      this.idPlanAdiestramiento == thatPK.idPlanAdiestramiento;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanAdiestramiento)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanAdiestramiento);
  }
}