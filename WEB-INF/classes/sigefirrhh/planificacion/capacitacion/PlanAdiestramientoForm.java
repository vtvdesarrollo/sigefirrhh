package sigefirrhh.planificacion.capacitacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.adiestramiento.AdiestramientoFacade;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.login.LoginSession;
import sigefirrhh.planificacion.plan.PlanFacade;
import sigefirrhh.planificacion.plan.PlanPersonal;

public class PlanAdiestramientoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PlanAdiestramientoForm.class.getName());
  private PlanAdiestramiento planAdiestramiento;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private AdiestramientoFacade adiestramientoFacade = new AdiestramientoFacade();
  private PlanFacade planFacade = new PlanFacade();
  private CapacitacionFacade capacitacionFacade = new CapacitacionFacade();
  private boolean showPlanAdiestramientoByUnidadFuncional;
  private boolean showPlanAdiestramientoByAreaConocimiento;
  private String findSelectUnidadFuncional;
  private String findSelectAreaConocimiento;
  private Collection findColUnidadFuncional;
  private Collection findColAreaConocimiento;
  private Collection colUnidadFuncional;
  private Collection colTipoCurso;
  private Collection colAreaConocimiento;
  private Collection colPlanPersonal;
  private String selectUnidadFuncional;
  private String selectTipoCurso;
  private String selectAreaConocimiento;
  private String selectPlanPersonal;
  private Object stateResultPlanAdiestramientoByUnidadFuncional = null;

  private Object stateResultPlanAdiestramientoByAreaConocimiento = null;

  public String getFindSelectUnidadFuncional()
  {
    return this.findSelectUnidadFuncional;
  }
  public void setFindSelectUnidadFuncional(String valUnidadFuncional) {
    this.findSelectUnidadFuncional = valUnidadFuncional;
  }

  public Collection getFindColUnidadFuncional() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }
  public String getFindSelectAreaConocimiento() {
    return this.findSelectAreaConocimiento;
  }
  public void setFindSelectAreaConocimiento(String valAreaConocimiento) {
    this.findSelectAreaConocimiento = valAreaConocimiento;
  }

  public Collection getFindColAreaConocimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public String getSelectUnidadFuncional()
  {
    return this.selectUnidadFuncional;
  }
  public void setSelectUnidadFuncional(String valUnidadFuncional) {
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    this.planAdiestramiento.setUnidadFuncional(null);
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      if (String.valueOf(unidadFuncional.getIdUnidadFuncional()).equals(
        valUnidadFuncional)) {
        this.planAdiestramiento.setUnidadFuncional(
          unidadFuncional);
        break;
      }
    }
    this.selectUnidadFuncional = valUnidadFuncional;
  }
  public String getSelectTipoCurso() {
    return this.selectTipoCurso;
  }
  public void setSelectTipoCurso(String valTipoCurso) {
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    this.planAdiestramiento.setTipoCurso(null);
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      if (String.valueOf(tipoCurso.getIdTipoCurso()).equals(
        valTipoCurso)) {
        this.planAdiestramiento.setTipoCurso(
          tipoCurso);
        break;
      }
    }
    this.selectTipoCurso = valTipoCurso;
  }
  public String getSelectAreaConocimiento() {
    return this.selectAreaConocimiento;
  }
  public void setSelectAreaConocimiento(String valAreaConocimiento) {
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    this.planAdiestramiento.setAreaConocimiento(null);
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      if (String.valueOf(areaConocimiento.getIdAreaConocimiento()).equals(
        valAreaConocimiento)) {
        this.planAdiestramiento.setAreaConocimiento(
          areaConocimiento);
        break;
      }
    }
    this.selectAreaConocimiento = valAreaConocimiento;
  }
  public String getSelectPlanPersonal() {
    return this.selectPlanPersonal;
  }
  public void setSelectPlanPersonal(String valPlanPersonal) {
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    this.planAdiestramiento.setPlanPersonal(null);
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      if (String.valueOf(planPersonal.getIdPlanPersonal()).equals(
        valPlanPersonal)) {
        this.planAdiestramiento.setPlanPersonal(
          planPersonal);
        break;
      }
    }
    this.selectPlanPersonal = valPlanPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public PlanAdiestramiento getPlanAdiestramiento() {
    if (this.planAdiestramiento == null) {
      this.planAdiestramiento = new PlanAdiestramiento();
    }
    return this.planAdiestramiento;
  }

  public PlanAdiestramientoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUnidadFuncional()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }

  public Collection getColTipoCurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoCurso.iterator();
    TipoCurso tipoCurso = null;
    while (iterator.hasNext()) {
      tipoCurso = (TipoCurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoCurso.getIdTipoCurso()), 
        tipoCurso.toString()));
    }
    return col;
  }

  public Collection getColAreaConocimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAreaConocimiento.iterator();
    AreaConocimiento areaConocimiento = null;
    while (iterator.hasNext()) {
      areaConocimiento = (AreaConocimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(areaConocimiento.getIdAreaConocimiento()), 
        areaConocimiento.toString()));
    }
    return col;
  }

  public Collection getListUnidadTiempo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_UNIDAD_TIEMPO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListTipoCargo() {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_TIPO_CARGO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListNivel() {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_NIVEL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListInternoExterno()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_LUGAR.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAprobacionMpd()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanAdiestramiento.LISTA_APROBACION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPlanPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColAreaConocimiento = 
        this.adiestramientoFacade.findAllAreaConocimiento();

      this.colUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colTipoCurso = 
        this.adiestramientoFacade.findAllTipoCurso();
      this.colAreaConocimiento = 
        this.adiestramientoFacade.findAllAreaConocimiento();
      this.colPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPlanAdiestramientoByUnidadFuncional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.capacitacionFacade.findPlanAdiestramientoByUnidadFuncional(Long.valueOf(this.findSelectUnidadFuncional).longValue());
      this.showPlanAdiestramientoByUnidadFuncional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanAdiestramientoByUnidadFuncional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUnidadFuncional = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public String findPlanAdiestramientoByAreaConocimiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.capacitacionFacade.findPlanAdiestramientoByAreaConocimiento(Long.valueOf(this.findSelectAreaConocimiento).longValue());
      this.showPlanAdiestramientoByAreaConocimiento = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanAdiestramientoByAreaConocimiento)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUnidadFuncional = null;
    this.findSelectAreaConocimiento = null;

    return null;
  }

  public boolean isShowPlanAdiestramientoByUnidadFuncional() {
    return this.showPlanAdiestramientoByUnidadFuncional;
  }
  public boolean isShowPlanAdiestramientoByAreaConocimiento() {
    return this.showPlanAdiestramientoByAreaConocimiento;
  }

  public String selectPlanAdiestramiento()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUnidadFuncional = null;
    this.selectTipoCurso = null;
    this.selectAreaConocimiento = null;
    this.selectPlanPersonal = null;

    long idPlanAdiestramiento = 
      Long.parseLong((String)requestParameterMap.get("idPlanAdiestramiento"));
    try
    {
      this.planAdiestramiento = 
        this.capacitacionFacade.findPlanAdiestramientoById(
        idPlanAdiestramiento);
      if (this.planAdiestramiento.getUnidadFuncional() != null) {
        this.selectUnidadFuncional = 
          String.valueOf(this.planAdiestramiento.getUnidadFuncional().getIdUnidadFuncional());
      }
      if (this.planAdiestramiento.getTipoCurso() != null) {
        this.selectTipoCurso = 
          String.valueOf(this.planAdiestramiento.getTipoCurso().getIdTipoCurso());
      }
      if (this.planAdiestramiento.getAreaConocimiento() != null) {
        this.selectAreaConocimiento = 
          String.valueOf(this.planAdiestramiento.getAreaConocimiento().getIdAreaConocimiento());
      }
      if (this.planAdiestramiento.getPlanPersonal() != null) {
        this.selectPlanPersonal = 
          String.valueOf(this.planAdiestramiento.getPlanPersonal().getIdPlanPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.planAdiestramiento = null;
    this.showPlanAdiestramientoByUnidadFuncional = false;
    this.showPlanAdiestramientoByAreaConocimiento = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try {
      if (this.planAdiestramiento.getAnio() == 0) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año tiene un valor no valido", ""));
        error = true;
      }
    }
    catch (Exception localException1)
    {
    }
    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.capacitacionFacade.addPlanAdiestramiento(
          this.planAdiestramiento);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.capacitacionFacade.updatePlanAdiestramiento(
          this.planAdiestramiento);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.capacitacionFacade.deletePlanAdiestramiento(
        this.planAdiestramiento);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.planAdiestramiento = new PlanAdiestramiento();

    this.selectUnidadFuncional = null;

    this.selectTipoCurso = null;

    this.selectAreaConocimiento = null;

    this.selectPlanPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.planAdiestramiento.setIdPlanAdiestramiento(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.capacitacion.PlanAdiestramiento"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.planAdiestramiento = new PlanAdiestramiento();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}