package sigefirrhh.planificacion.capacitacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.adiestramiento.AreaConocimiento;
import sigefirrhh.base.adiestramiento.TipoCurso;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.planificacion.plan.PlanPersonal;

public class PlanAdiestramiento
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_UNIDAD_TIEMPO;
  protected static final Map LISTA_TIPO_CARGO;
  protected static final Map LISTA_LUGAR;
  protected static final Map LISTA_NIVEL;
  protected static final Map LISTA_APROBACION;
  private long idPlanAdiestramiento;
  private UnidadFuncional unidadFuncional;
  private int anio;
  private int numeroPlan;
  private TipoCurso tipoCurso;
  private AreaConocimiento areaConocimiento;
  private String nombreCurso;
  private int numeroParticipantes;
  private int duracion;
  private String unidadTiempo;
  private String tipoCargo;
  private String nivel;
  private double costoEstimado;
  private double costoAprobado;
  private double costoReal;
  private int trimestre;
  private String estatus;
  private Date fechaInicio;
  private String internoExterno;
  private String nombreEntidad;
  private String aprobacionMpd;
  private PlanPersonal planPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "aprobacionMpd", "areaConocimiento", "costoAprobado", "costoEstimado", "costoReal", "duracion", "estatus", "fechaInicio", "idPlanAdiestramiento", "internoExterno", "nivel", "nombreCurso", "nombreEntidad", "numeroParticipantes", "numeroPlan", "planPersonal", "tipoCargo", "tipoCurso", "trimestre", "unidadFuncional", "unidadTiempo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.adiestramiento.AreaConocimiento"), Double.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.adiestramiento.TipoCurso"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadFuncional"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 26, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 26, 21, 26, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.capacitacion.PlanAdiestramiento"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanAdiestramiento());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_UNIDAD_TIEMPO = 
      new LinkedHashMap();
    LISTA_TIPO_CARGO = 
      new LinkedHashMap();
    LISTA_LUGAR = 
      new LinkedHashMap();
    LISTA_NIVEL = 
      new LinkedHashMap();
    LISTA_APROBACION = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("P", "PROPUESTO");
    LISTA_ESTATUS.put("A", "APROBADO");
    LISTA_ESTATUS.put("I", "INSCRIPCION");
    LISTA_ESTATUS.put("E", "EJECUCION");
    LISTA_ESTATUS.put("C", "CERRADO");
    LISTA_UNIDAD_TIEMPO.put("H", "HORAS");
    LISTA_UNIDAD_TIEMPO.put("D", "DIAS");
    LISTA_UNIDAD_TIEMPO.put("S", "SEMANAS");
    LISTA_UNIDAD_TIEMPO.put("M", "MESES");
    LISTA_UNIDAD_TIEMPO.put("A", "AÑOS");
    LISTA_TIPO_CARGO.put("1", "ALTO NIVEL");
    LISTA_TIPO_CARGO.put("2", "ADMINISTRATIVO");
    LISTA_TIPO_CARGO.put("3", "TECNICO-PROFESIONAL");
    LISTA_TIPO_CARGO.put("4", "OBRERO");
    LISTA_TIPO_CARGO.put("5", "NO CLASIFICADO");
    LISTA_TIPO_CARGO.put("9", "NO APLICA");
    LISTA_LUGAR.put("I", "EN ORGANISMO");
    LISTA_LUGAR.put("E", "FUERA DEL ORGANISMO");
    LISTA_APROBACION.put("S", "SI");
    LISTA_APROBACION.put("N", "NO");
    LISTA_NIVEL.put("1", "SUPERVISORIO");
    LISTA_NIVEL.put("2", "SUPERVISADO");
  }

  public PlanAdiestramiento()
  {
    jdoSetnumeroPlan(this, 1);

    jdoSetnumeroParticipantes(this, 1);

    jdoSetduracion(this, 1);

    jdoSetunidadTiempo(this, "H");

    jdoSettipoCargo(this, "9");

    jdoSetnivel(this, "1");

    jdoSetcostoEstimado(this, 0.0D);

    jdoSetcostoAprobado(this, 0.0D);

    jdoSetcostoReal(this, 0.0D);

    jdoSettrimestre(this, 1);

    jdoSetestatus(this, "P");

    jdoSetinternoExterno(this, "I");

    jdoSetaprobacionMpd(this, "N");
  }

  public String toString()
  {
    return jdoGetunidadFuncional(this).getNombre() + "-" + jdoGetanio(this) + jdoGetnumeroPlan(this);
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public String getAprobacionMpd() {
    return jdoGetaprobacionMpd(this);
  }

  public void setAprobacionMpd(String aprobacionMpd) {
    jdoSetaprobacionMpd(this, aprobacionMpd);
  }

  public AreaConocimiento getAreaConocimiento() {
    return jdoGetareaConocimiento(this);
  }

  public void setAreaConocimiento(AreaConocimiento areaConocimiento) {
    jdoSetareaConocimiento(this, areaConocimiento);
  }

  public double getCostoAprobado() {
    return jdoGetcostoAprobado(this);
  }

  public void setCostoAprobado(double costoAprobado) {
    jdoSetcostoAprobado(this, costoAprobado);
  }

  public double getCostoEstimado() {
    return jdoGetcostoEstimado(this);
  }

  public void setCostoEstimado(double costoEstimado) {
    jdoSetcostoEstimado(this, costoEstimado);
  }

  public double getCostoReal() {
    return jdoGetcostoReal(this);
  }

  public void setCostoReal(double costoReal) {
    jdoSetcostoReal(this, costoReal);
  }

  public int getDuracion() {
    return jdoGetduracion(this);
  }

  public void setDuracion(int duracion) {
    jdoSetduracion(this, duracion);
  }

  public String getEstatus() {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }

  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }

  public void setFechaInicio(Date fechaInicio) {
    jdoSetfechaInicio(this, fechaInicio);
  }

  public long getIdPlanAdiestramiento() {
    return jdoGetidPlanAdiestramiento(this);
  }

  public void setIdPlanAdiestramiento(long idPlanAdiestramiento) {
    jdoSetidPlanAdiestramiento(this, idPlanAdiestramiento);
  }

  public String getInternoExterno() {
    return jdoGetinternoExterno(this);
  }

  public void setInternoExterno(String internoExterno) {
    jdoSetinternoExterno(this, internoExterno);
  }

  public String getNombreCurso() {
    return jdoGetnombreCurso(this);
  }

  public void setNombreCurso(String nombreCurso) {
    jdoSetnombreCurso(this, nombreCurso);
  }

  public String getNombreEntidad() {
    return jdoGetnombreEntidad(this);
  }

  public void setNombreEntidad(String nombreEntidad) {
    jdoSetnombreEntidad(this, nombreEntidad);
  }

  public int getNumeroParticipantes() {
    return jdoGetnumeroParticipantes(this);
  }

  public void setNumeroParticipantes(int numeroParticipantes) {
    jdoSetnumeroParticipantes(this, numeroParticipantes);
  }

  public int getNumeroPlan() {
    return jdoGetnumeroPlan(this);
  }

  public void setNumeroPlan(int numeroPlan) {
    jdoSetnumeroPlan(this, numeroPlan);
  }

  public PlanPersonal getPlanPersonal() {
    return jdoGetplanPersonal(this);
  }

  public void setPlanPersonal(PlanPersonal planPersonal) {
    jdoSetplanPersonal(this, planPersonal);
  }

  public String getTipoCargo() {
    return jdoGettipoCargo(this);
  }

  public void setTipoCargo(String tipoCargo) {
    jdoSettipoCargo(this, tipoCargo);
  }

  public TipoCurso getTipoCurso() {
    return jdoGettipoCurso(this);
  }

  public void setTipoCurso(TipoCurso tipoCurso) {
    jdoSettipoCurso(this, tipoCurso);
  }

  public int getTrimestre() {
    return jdoGettrimestre(this);
  }

  public void setTrimestre(int trimestre) {
    jdoSettrimestre(this, trimestre);
  }

  public UnidadFuncional getUnidadFuncional() {
    return jdoGetunidadFuncional(this);
  }

  public void setUnidadFuncional(UnidadFuncional unidadFuncional) {
    jdoSetunidadFuncional(this, unidadFuncional);
  }

  public String getUnidadTiempo() {
    return jdoGetunidadTiempo(this);
  }

  public void setUnidadTiempo(String unidadTiempo) {
    jdoSetunidadTiempo(this, unidadTiempo);
  }

  public String getNivel() {
    return jdoGetnivel(this);
  }

  public void setNivel(String nivel) {
    jdoSetnivel(this, nivel);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 22;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanAdiestramiento localPlanAdiestramiento = new PlanAdiestramiento();
    localPlanAdiestramiento.jdoFlags = 1;
    localPlanAdiestramiento.jdoStateManager = paramStateManager;
    return localPlanAdiestramiento;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanAdiestramiento localPlanAdiestramiento = new PlanAdiestramiento();
    localPlanAdiestramiento.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanAdiestramiento.jdoFlags = 1;
    localPlanAdiestramiento.jdoStateManager = paramStateManager;
    return localPlanAdiestramiento;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.areaConocimiento);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoAprobado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoEstimado);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoReal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.duracion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanAdiestramiento);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.internoExterno);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivel);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCurso);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreEntidad);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroParticipantes);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroPlan);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPersonal);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCargo);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoCurso);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.trimestre);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadFuncional);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.unidadTiempo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.areaConocimiento = ((AreaConocimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoAprobado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoEstimado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoReal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.duracion = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanAdiestramiento = localStateManager.replacingLongField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.internoExterno = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreEntidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroParticipantes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroPlan = localStateManager.replacingIntField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPersonal = ((PlanPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCurso = ((TipoCurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trimestre = localStateManager.replacingIntField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadFuncional = ((UnidadFuncional)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadTiempo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanAdiestramiento paramPlanAdiestramiento, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPlanAdiestramiento.anio;
      return;
    case 1:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramPlanAdiestramiento.aprobacionMpd;
      return;
    case 2:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.areaConocimiento = paramPlanAdiestramiento.areaConocimiento;
      return;
    case 3:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.costoAprobado = paramPlanAdiestramiento.costoAprobado;
      return;
    case 4:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.costoEstimado = paramPlanAdiestramiento.costoEstimado;
      return;
    case 5:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.costoReal = paramPlanAdiestramiento.costoReal;
      return;
    case 6:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.duracion = paramPlanAdiestramiento.duracion;
      return;
    case 7:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramPlanAdiestramiento.estatus;
      return;
    case 8:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramPlanAdiestramiento.fechaInicio;
      return;
    case 9:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanAdiestramiento = paramPlanAdiestramiento.idPlanAdiestramiento;
      return;
    case 10:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.internoExterno = paramPlanAdiestramiento.internoExterno;
      return;
    case 11:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramPlanAdiestramiento.nivel;
      return;
    case 12:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCurso = paramPlanAdiestramiento.nombreCurso;
      return;
    case 13:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.nombreEntidad = paramPlanAdiestramiento.nombreEntidad;
      return;
    case 14:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.numeroParticipantes = paramPlanAdiestramiento.numeroParticipantes;
      return;
    case 15:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.numeroPlan = paramPlanAdiestramiento.numeroPlan;
      return;
    case 16:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.planPersonal = paramPlanAdiestramiento.planPersonal;
      return;
    case 17:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCargo = paramPlanAdiestramiento.tipoCargo;
      return;
    case 18:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCurso = paramPlanAdiestramiento.tipoCurso;
      return;
    case 19:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.trimestre = paramPlanAdiestramiento.trimestre;
      return;
    case 20:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.unidadFuncional = paramPlanAdiestramiento.unidadFuncional;
      return;
    case 21:
      if (paramPlanAdiestramiento == null)
        throw new IllegalArgumentException("arg1");
      this.unidadTiempo = paramPlanAdiestramiento.unidadTiempo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanAdiestramiento))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanAdiestramiento localPlanAdiestramiento = (PlanAdiestramiento)paramObject;
    if (localPlanAdiestramiento.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanAdiestramiento, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanAdiestramientoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanAdiestramientoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanAdiestramientoPK))
      throw new IllegalArgumentException("arg1");
    PlanAdiestramientoPK localPlanAdiestramientoPK = (PlanAdiestramientoPK)paramObject;
    localPlanAdiestramientoPK.idPlanAdiestramiento = this.idPlanAdiestramiento;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanAdiestramientoPK))
      throw new IllegalArgumentException("arg1");
    PlanAdiestramientoPK localPlanAdiestramientoPK = (PlanAdiestramientoPK)paramObject;
    this.idPlanAdiestramiento = localPlanAdiestramientoPK.idPlanAdiestramiento;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanAdiestramientoPK))
      throw new IllegalArgumentException("arg2");
    PlanAdiestramientoPK localPlanAdiestramientoPK = (PlanAdiestramientoPK)paramObject;
    localPlanAdiestramientoPK.idPlanAdiestramiento = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 9);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanAdiestramientoPK))
      throw new IllegalArgumentException("arg2");
    PlanAdiestramientoPK localPlanAdiestramientoPK = (PlanAdiestramientoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 9, localPlanAdiestramientoPK.idPlanAdiestramiento);
  }

  private static final int jdoGetanio(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.anio;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.anio;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 0))
      return paramPlanAdiestramiento.anio;
    return localStateManager.getIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 0, paramPlanAdiestramiento.anio);
  }

  private static final void jdoSetanio(PlanAdiestramiento paramPlanAdiestramiento, int paramInt)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 0, paramPlanAdiestramiento.anio, paramInt);
  }

  private static final String jdoGetaprobacionMpd(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.aprobacionMpd;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.aprobacionMpd;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 1))
      return paramPlanAdiestramiento.aprobacionMpd;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 1, paramPlanAdiestramiento.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 1, paramPlanAdiestramiento.aprobacionMpd, paramString);
  }

  private static final AreaConocimiento jdoGetareaConocimiento(PlanAdiestramiento paramPlanAdiestramiento)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.areaConocimiento;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 2))
      return paramPlanAdiestramiento.areaConocimiento;
    return (AreaConocimiento)localStateManager.getObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 2, paramPlanAdiestramiento.areaConocimiento);
  }

  private static final void jdoSetareaConocimiento(PlanAdiestramiento paramPlanAdiestramiento, AreaConocimiento paramAreaConocimiento)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.areaConocimiento = paramAreaConocimiento;
      return;
    }
    localStateManager.setObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 2, paramPlanAdiestramiento.areaConocimiento, paramAreaConocimiento);
  }

  private static final double jdoGetcostoAprobado(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.costoAprobado;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.costoAprobado;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 3))
      return paramPlanAdiestramiento.costoAprobado;
    return localStateManager.getDoubleField(paramPlanAdiestramiento, jdoInheritedFieldCount + 3, paramPlanAdiestramiento.costoAprobado);
  }

  private static final void jdoSetcostoAprobado(PlanAdiestramiento paramPlanAdiestramiento, double paramDouble)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.costoAprobado = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.costoAprobado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanAdiestramiento, jdoInheritedFieldCount + 3, paramPlanAdiestramiento.costoAprobado, paramDouble);
  }

  private static final double jdoGetcostoEstimado(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.costoEstimado;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.costoEstimado;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 4))
      return paramPlanAdiestramiento.costoEstimado;
    return localStateManager.getDoubleField(paramPlanAdiestramiento, jdoInheritedFieldCount + 4, paramPlanAdiestramiento.costoEstimado);
  }

  private static final void jdoSetcostoEstimado(PlanAdiestramiento paramPlanAdiestramiento, double paramDouble)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.costoEstimado = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.costoEstimado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanAdiestramiento, jdoInheritedFieldCount + 4, paramPlanAdiestramiento.costoEstimado, paramDouble);
  }

  private static final double jdoGetcostoReal(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.costoReal;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.costoReal;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 5))
      return paramPlanAdiestramiento.costoReal;
    return localStateManager.getDoubleField(paramPlanAdiestramiento, jdoInheritedFieldCount + 5, paramPlanAdiestramiento.costoReal);
  }

  private static final void jdoSetcostoReal(PlanAdiestramiento paramPlanAdiestramiento, double paramDouble)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.costoReal = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.costoReal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanAdiestramiento, jdoInheritedFieldCount + 5, paramPlanAdiestramiento.costoReal, paramDouble);
  }

  private static final int jdoGetduracion(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.duracion;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.duracion;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 6))
      return paramPlanAdiestramiento.duracion;
    return localStateManager.getIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 6, paramPlanAdiestramiento.duracion);
  }

  private static final void jdoSetduracion(PlanAdiestramiento paramPlanAdiestramiento, int paramInt)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.duracion = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.duracion = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 6, paramPlanAdiestramiento.duracion, paramInt);
  }

  private static final String jdoGetestatus(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.estatus;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.estatus;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 7))
      return paramPlanAdiestramiento.estatus;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 7, paramPlanAdiestramiento.estatus);
  }

  private static final void jdoSetestatus(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 7, paramPlanAdiestramiento.estatus, paramString);
  }

  private static final Date jdoGetfechaInicio(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.fechaInicio;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.fechaInicio;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 8))
      return paramPlanAdiestramiento.fechaInicio;
    return (Date)localStateManager.getObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 8, paramPlanAdiestramiento.fechaInicio);
  }

  private static final void jdoSetfechaInicio(PlanAdiestramiento paramPlanAdiestramiento, Date paramDate)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 8, paramPlanAdiestramiento.fechaInicio, paramDate);
  }

  private static final long jdoGetidPlanAdiestramiento(PlanAdiestramiento paramPlanAdiestramiento)
  {
    return paramPlanAdiestramiento.idPlanAdiestramiento;
  }

  private static final void jdoSetidPlanAdiestramiento(PlanAdiestramiento paramPlanAdiestramiento, long paramLong)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.idPlanAdiestramiento = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanAdiestramiento, jdoInheritedFieldCount + 9, paramPlanAdiestramiento.idPlanAdiestramiento, paramLong);
  }

  private static final String jdoGetinternoExterno(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.internoExterno;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.internoExterno;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 10))
      return paramPlanAdiestramiento.internoExterno;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 10, paramPlanAdiestramiento.internoExterno);
  }

  private static final void jdoSetinternoExterno(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.internoExterno = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.internoExterno = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 10, paramPlanAdiestramiento.internoExterno, paramString);
  }

  private static final String jdoGetnivel(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.nivel;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.nivel;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 11))
      return paramPlanAdiestramiento.nivel;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 11, paramPlanAdiestramiento.nivel);
  }

  private static final void jdoSetnivel(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.nivel = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.nivel = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 11, paramPlanAdiestramiento.nivel, paramString);
  }

  private static final String jdoGetnombreCurso(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.nombreCurso;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.nombreCurso;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 12))
      return paramPlanAdiestramiento.nombreCurso;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 12, paramPlanAdiestramiento.nombreCurso);
  }

  private static final void jdoSetnombreCurso(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.nombreCurso = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.nombreCurso = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 12, paramPlanAdiestramiento.nombreCurso, paramString);
  }

  private static final String jdoGetnombreEntidad(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.nombreEntidad;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.nombreEntidad;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 13))
      return paramPlanAdiestramiento.nombreEntidad;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 13, paramPlanAdiestramiento.nombreEntidad);
  }

  private static final void jdoSetnombreEntidad(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.nombreEntidad = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.nombreEntidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 13, paramPlanAdiestramiento.nombreEntidad, paramString);
  }

  private static final int jdoGetnumeroParticipantes(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.numeroParticipantes;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.numeroParticipantes;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 14))
      return paramPlanAdiestramiento.numeroParticipantes;
    return localStateManager.getIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 14, paramPlanAdiestramiento.numeroParticipantes);
  }

  private static final void jdoSetnumeroParticipantes(PlanAdiestramiento paramPlanAdiestramiento, int paramInt)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.numeroParticipantes = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.numeroParticipantes = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 14, paramPlanAdiestramiento.numeroParticipantes, paramInt);
  }

  private static final int jdoGetnumeroPlan(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.numeroPlan;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.numeroPlan;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 15))
      return paramPlanAdiestramiento.numeroPlan;
    return localStateManager.getIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 15, paramPlanAdiestramiento.numeroPlan);
  }

  private static final void jdoSetnumeroPlan(PlanAdiestramiento paramPlanAdiestramiento, int paramInt)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.numeroPlan = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.numeroPlan = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 15, paramPlanAdiestramiento.numeroPlan, paramInt);
  }

  private static final PlanPersonal jdoGetplanPersonal(PlanAdiestramiento paramPlanAdiestramiento)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.planPersonal;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 16))
      return paramPlanAdiestramiento.planPersonal;
    return (PlanPersonal)localStateManager.getObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 16, paramPlanAdiestramiento.planPersonal);
  }

  private static final void jdoSetplanPersonal(PlanAdiestramiento paramPlanAdiestramiento, PlanPersonal paramPlanPersonal)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.planPersonal = paramPlanPersonal;
      return;
    }
    localStateManager.setObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 16, paramPlanAdiestramiento.planPersonal, paramPlanPersonal);
  }

  private static final String jdoGettipoCargo(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.tipoCargo;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.tipoCargo;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 17))
      return paramPlanAdiestramiento.tipoCargo;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 17, paramPlanAdiestramiento.tipoCargo);
  }

  private static final void jdoSettipoCargo(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.tipoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.tipoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 17, paramPlanAdiestramiento.tipoCargo, paramString);
  }

  private static final TipoCurso jdoGettipoCurso(PlanAdiestramiento paramPlanAdiestramiento)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.tipoCurso;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 18))
      return paramPlanAdiestramiento.tipoCurso;
    return (TipoCurso)localStateManager.getObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 18, paramPlanAdiestramiento.tipoCurso);
  }

  private static final void jdoSettipoCurso(PlanAdiestramiento paramPlanAdiestramiento, TipoCurso paramTipoCurso)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.tipoCurso = paramTipoCurso;
      return;
    }
    localStateManager.setObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 18, paramPlanAdiestramiento.tipoCurso, paramTipoCurso);
  }

  private static final int jdoGettrimestre(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.trimestre;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.trimestre;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 19))
      return paramPlanAdiestramiento.trimestre;
    return localStateManager.getIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 19, paramPlanAdiestramiento.trimestre);
  }

  private static final void jdoSettrimestre(PlanAdiestramiento paramPlanAdiestramiento, int paramInt)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.trimestre = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.trimestre = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanAdiestramiento, jdoInheritedFieldCount + 19, paramPlanAdiestramiento.trimestre, paramInt);
  }

  private static final UnidadFuncional jdoGetunidadFuncional(PlanAdiestramiento paramPlanAdiestramiento)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.unidadFuncional;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 20))
      return paramPlanAdiestramiento.unidadFuncional;
    return (UnidadFuncional)localStateManager.getObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 20, paramPlanAdiestramiento.unidadFuncional);
  }

  private static final void jdoSetunidadFuncional(PlanAdiestramiento paramPlanAdiestramiento, UnidadFuncional paramUnidadFuncional)
  {
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.unidadFuncional = paramUnidadFuncional;
      return;
    }
    localStateManager.setObjectField(paramPlanAdiestramiento, jdoInheritedFieldCount + 20, paramPlanAdiestramiento.unidadFuncional, paramUnidadFuncional);
  }

  private static final String jdoGetunidadTiempo(PlanAdiestramiento paramPlanAdiestramiento)
  {
    if (paramPlanAdiestramiento.jdoFlags <= 0)
      return paramPlanAdiestramiento.unidadTiempo;
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
      return paramPlanAdiestramiento.unidadTiempo;
    if (localStateManager.isLoaded(paramPlanAdiestramiento, jdoInheritedFieldCount + 21))
      return paramPlanAdiestramiento.unidadTiempo;
    return localStateManager.getStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 21, paramPlanAdiestramiento.unidadTiempo);
  }

  private static final void jdoSetunidadTiempo(PlanAdiestramiento paramPlanAdiestramiento, String paramString)
  {
    if (paramPlanAdiestramiento.jdoFlags == 0)
    {
      paramPlanAdiestramiento.unidadTiempo = paramString;
      return;
    }
    StateManager localStateManager = paramPlanAdiestramiento.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanAdiestramiento.unidadTiempo = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanAdiestramiento, jdoInheritedFieldCount + 21, paramPlanAdiestramiento.unidadTiempo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}