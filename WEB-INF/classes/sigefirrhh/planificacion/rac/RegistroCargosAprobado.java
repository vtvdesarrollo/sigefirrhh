package sigefirrhh.planificacion.rac;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.planificacion.plan.PlanPersonal;

public class RegistroCargosAprobado
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_ACCION;
  private long idRegistroCargosAprobado;
  private PlanPersonal planPersonal;
  private Registro registro;
  private String accion;
  private String estatus;
  private Date fechaAccion;
  private int codigoNomina;
  private Cargo cargo;
  private Dependencia dependencia;
  private Region region;
  private double horas;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "accion", "cargo", "codigoNomina", "dependencia", "estatus", "fechaAccion", "horas", "idRegistroCargosAprobado", "planPersonal", "region", "registro" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Double.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanPersonal"), sunjdo$classForName$("sigefirrhh.base.estructura.Region"), sunjdo$classForName$("sigefirrhh.base.registro.Registro") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 26, 21, 21, 21, 24, 26, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.rac.RegistroCargosAprobado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RegistroCargosAprobado());

    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_ACCION = 
      new LinkedHashMap();

    LISTA_ACCION.put("1", "CREACION");
    LISTA_ACCION.put("2", "ELIMINACION");
    LISTA_ACCION.put("3", "CLASIFICACION");
    LISTA_ACCION.put("4", "CAMBIO CLASIFICACION");
    LISTA_ACCION.put("5", "TRASLADO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public RegistroCargosAprobado()
  {
    jdoSetaccion(this, "1");

    jdoSetestatus(this, "N");

    jdoSethoras(this, 8.0D);
  }

  public String toString()
  {
    return jdoGetaccion(this) + " - " + jdoGetcodigoNomina(this) + " - " + 
      jdoGetcargo(this).getDescripcionCargo();
  }

  public String getAccion()
  {
    return jdoGetaccion(this);
  }
  public void setAccion(String accion) {
    jdoSetaccion(this, accion);
  }
  public Cargo getCargo() {
    return jdoGetcargo(this);
  }
  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }
  public int getCodigoNomina() {
    return jdoGetcodigoNomina(this);
  }
  public void setCodigoNomina(int codigoNomina) {
    jdoSetcodigoNomina(this, codigoNomina);
  }
  public Dependencia getDependencia() {
    return jdoGetdependencia(this);
  }
  public void setDependencia(Dependencia dependencia) {
    jdoSetdependencia(this, dependencia);
  }
  public double getHoras() {
    return jdoGethoras(this);
  }
  public void setHoras(double horas) {
    jdoSethoras(this, horas);
  }
  public long getIdRegistroCargosAprobado() {
    return jdoGetidRegistroCargosAprobado(this);
  }
  public void setIdRegistroCargosAprobado(long idRegistroCargosAprobado) {
    jdoSetidRegistroCargosAprobado(this, idRegistroCargosAprobado);
  }
  public PlanPersonal getPlanPersonal() {
    return jdoGetplanPersonal(this);
  }
  public void setPlanPersonal(PlanPersonal planPersonal) {
    jdoSetplanPersonal(this, planPersonal);
  }
  public Registro getRegistro() {
    return jdoGetregistro(this);
  }
  public void setRegistro(Registro registro) {
    jdoSetregistro(this, registro);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public Date getFechaAccion() {
    return jdoGetfechaAccion(this);
  }
  public void setFechaAccion(Date fechaAccion) {
    jdoSetfechaAccion(this, fechaAccion);
  }
  public Region getRegion() {
    return jdoGetregion(this);
  }
  public void setRegion(Region region) {
    jdoSetregion(this, region);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RegistroCargosAprobado localRegistroCargosAprobado = new RegistroCargosAprobado();
    localRegistroCargosAprobado.jdoFlags = 1;
    localRegistroCargosAprobado.jdoStateManager = paramStateManager;
    return localRegistroCargosAprobado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RegistroCargosAprobado localRegistroCargosAprobado = new RegistroCargosAprobado();
    localRegistroCargosAprobado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRegistroCargosAprobado.jdoFlags = 1;
    localRegistroCargosAprobado.jdoStateManager = paramStateManager;
    return localRegistroCargosAprobado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.accion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoNomina);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaAccion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.horas);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRegistroCargosAprobado);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPersonal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.region);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registro);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoNomina = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaAccion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRegistroCargosAprobado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPersonal = ((PlanPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.region = ((Region)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registro = ((Registro)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RegistroCargosAprobado paramRegistroCargosAprobado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.accion = paramRegistroCargosAprobado.accion;
      return;
    case 1:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramRegistroCargosAprobado.cargo;
      return;
    case 2:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.codigoNomina = paramRegistroCargosAprobado.codigoNomina;
      return;
    case 3:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramRegistroCargosAprobado.dependencia;
      return;
    case 4:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramRegistroCargosAprobado.estatus;
      return;
    case 5:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.fechaAccion = paramRegistroCargosAprobado.fechaAccion;
      return;
    case 6:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramRegistroCargosAprobado.horas;
      return;
    case 7:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.idRegistroCargosAprobado = paramRegistroCargosAprobado.idRegistroCargosAprobado;
      return;
    case 8:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.planPersonal = paramRegistroCargosAprobado.planPersonal;
      return;
    case 9:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.region = paramRegistroCargosAprobado.region;
      return;
    case 10:
      if (paramRegistroCargosAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.registro = paramRegistroCargosAprobado.registro;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RegistroCargosAprobado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RegistroCargosAprobado localRegistroCargosAprobado = (RegistroCargosAprobado)paramObject;
    if (localRegistroCargosAprobado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRegistroCargosAprobado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RegistroCargosAprobadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RegistroCargosAprobadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroCargosAprobadoPK))
      throw new IllegalArgumentException("arg1");
    RegistroCargosAprobadoPK localRegistroCargosAprobadoPK = (RegistroCargosAprobadoPK)paramObject;
    localRegistroCargosAprobadoPK.idRegistroCargosAprobado = this.idRegistroCargosAprobado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RegistroCargosAprobadoPK))
      throw new IllegalArgumentException("arg1");
    RegistroCargosAprobadoPK localRegistroCargosAprobadoPK = (RegistroCargosAprobadoPK)paramObject;
    this.idRegistroCargosAprobado = localRegistroCargosAprobadoPK.idRegistroCargosAprobado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroCargosAprobadoPK))
      throw new IllegalArgumentException("arg2");
    RegistroCargosAprobadoPK localRegistroCargosAprobadoPK = (RegistroCargosAprobadoPK)paramObject;
    localRegistroCargosAprobadoPK.idRegistroCargosAprobado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RegistroCargosAprobadoPK))
      throw new IllegalArgumentException("arg2");
    RegistroCargosAprobadoPK localRegistroCargosAprobadoPK = (RegistroCargosAprobadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localRegistroCargosAprobadoPK.idRegistroCargosAprobado);
  }

  private static final String jdoGetaccion(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    if (paramRegistroCargosAprobado.jdoFlags <= 0)
      return paramRegistroCargosAprobado.accion;
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.accion;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 0))
      return paramRegistroCargosAprobado.accion;
    return localStateManager.getStringField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 0, paramRegistroCargosAprobado.accion);
  }

  private static final void jdoSetaccion(RegistroCargosAprobado paramRegistroCargosAprobado, String paramString)
  {
    if (paramRegistroCargosAprobado.jdoFlags == 0)
    {
      paramRegistroCargosAprobado.accion = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.accion = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 0, paramRegistroCargosAprobado.accion, paramString);
  }

  private static final Cargo jdoGetcargo(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.cargo;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 1))
      return paramRegistroCargosAprobado.cargo;
    return (Cargo)localStateManager.getObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 1, paramRegistroCargosAprobado.cargo);
  }

  private static final void jdoSetcargo(RegistroCargosAprobado paramRegistroCargosAprobado, Cargo paramCargo)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 1, paramRegistroCargosAprobado.cargo, paramCargo);
  }

  private static final int jdoGetcodigoNomina(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    if (paramRegistroCargosAprobado.jdoFlags <= 0)
      return paramRegistroCargosAprobado.codigoNomina;
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.codigoNomina;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 2))
      return paramRegistroCargosAprobado.codigoNomina;
    return localStateManager.getIntField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 2, paramRegistroCargosAprobado.codigoNomina);
  }

  private static final void jdoSetcodigoNomina(RegistroCargosAprobado paramRegistroCargosAprobado, int paramInt)
  {
    if (paramRegistroCargosAprobado.jdoFlags == 0)
    {
      paramRegistroCargosAprobado.codigoNomina = paramInt;
      return;
    }
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.codigoNomina = paramInt;
      return;
    }
    localStateManager.setIntField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 2, paramRegistroCargosAprobado.codigoNomina, paramInt);
  }

  private static final Dependencia jdoGetdependencia(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.dependencia;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 3))
      return paramRegistroCargosAprobado.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 3, paramRegistroCargosAprobado.dependencia);
  }

  private static final void jdoSetdependencia(RegistroCargosAprobado paramRegistroCargosAprobado, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 3, paramRegistroCargosAprobado.dependencia, paramDependencia);
  }

  private static final String jdoGetestatus(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    if (paramRegistroCargosAprobado.jdoFlags <= 0)
      return paramRegistroCargosAprobado.estatus;
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.estatus;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 4))
      return paramRegistroCargosAprobado.estatus;
    return localStateManager.getStringField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 4, paramRegistroCargosAprobado.estatus);
  }

  private static final void jdoSetestatus(RegistroCargosAprobado paramRegistroCargosAprobado, String paramString)
  {
    if (paramRegistroCargosAprobado.jdoFlags == 0)
    {
      paramRegistroCargosAprobado.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 4, paramRegistroCargosAprobado.estatus, paramString);
  }

  private static final Date jdoGetfechaAccion(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    if (paramRegistroCargosAprobado.jdoFlags <= 0)
      return paramRegistroCargosAprobado.fechaAccion;
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.fechaAccion;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 5))
      return paramRegistroCargosAprobado.fechaAccion;
    return (Date)localStateManager.getObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 5, paramRegistroCargosAprobado.fechaAccion);
  }

  private static final void jdoSetfechaAccion(RegistroCargosAprobado paramRegistroCargosAprobado, Date paramDate)
  {
    if (paramRegistroCargosAprobado.jdoFlags == 0)
    {
      paramRegistroCargosAprobado.fechaAccion = paramDate;
      return;
    }
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.fechaAccion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 5, paramRegistroCargosAprobado.fechaAccion, paramDate);
  }

  private static final double jdoGethoras(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    if (paramRegistroCargosAprobado.jdoFlags <= 0)
      return paramRegistroCargosAprobado.horas;
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.horas;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 6))
      return paramRegistroCargosAprobado.horas;
    return localStateManager.getDoubleField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 6, paramRegistroCargosAprobado.horas);
  }

  private static final void jdoSethoras(RegistroCargosAprobado paramRegistroCargosAprobado, double paramDouble)
  {
    if (paramRegistroCargosAprobado.jdoFlags == 0)
    {
      paramRegistroCargosAprobado.horas = paramDouble;
      return;
    }
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.horas = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 6, paramRegistroCargosAprobado.horas, paramDouble);
  }

  private static final long jdoGetidRegistroCargosAprobado(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    return paramRegistroCargosAprobado.idRegistroCargosAprobado;
  }

  private static final void jdoSetidRegistroCargosAprobado(RegistroCargosAprobado paramRegistroCargosAprobado, long paramLong)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.idRegistroCargosAprobado = paramLong;
      return;
    }
    localStateManager.setLongField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 7, paramRegistroCargosAprobado.idRegistroCargosAprobado, paramLong);
  }

  private static final PlanPersonal jdoGetplanPersonal(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.planPersonal;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 8))
      return paramRegistroCargosAprobado.planPersonal;
    return (PlanPersonal)localStateManager.getObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 8, paramRegistroCargosAprobado.planPersonal);
  }

  private static final void jdoSetplanPersonal(RegistroCargosAprobado paramRegistroCargosAprobado, PlanPersonal paramPlanPersonal)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.planPersonal = paramPlanPersonal;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 8, paramRegistroCargosAprobado.planPersonal, paramPlanPersonal);
  }

  private static final Region jdoGetregion(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.region;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 9))
      return paramRegistroCargosAprobado.region;
    return (Region)localStateManager.getObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 9, paramRegistroCargosAprobado.region);
  }

  private static final void jdoSetregion(RegistroCargosAprobado paramRegistroCargosAprobado, Region paramRegion)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.region = paramRegion;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 9, paramRegistroCargosAprobado.region, paramRegion);
  }

  private static final Registro jdoGetregistro(RegistroCargosAprobado paramRegistroCargosAprobado)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRegistroCargosAprobado.registro;
    if (localStateManager.isLoaded(paramRegistroCargosAprobado, jdoInheritedFieldCount + 10))
      return paramRegistroCargosAprobado.registro;
    return (Registro)localStateManager.getObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 10, paramRegistroCargosAprobado.registro);
  }

  private static final void jdoSetregistro(RegistroCargosAprobado paramRegistroCargosAprobado, Registro paramRegistro)
  {
    StateManager localStateManager = paramRegistroCargosAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRegistroCargosAprobado.registro = paramRegistro;
      return;
    }
    localStateManager.setObjectField(paramRegistroCargosAprobado, jdoInheritedFieldCount + 10, paramRegistroCargosAprobado.registro, paramRegistro);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}