package sigefirrhh.planificacion.rac;

import java.io.Serializable;

public class RegistroCargosAprobadoPK
  implements Serializable
{
  public long idRegistroCargosAprobado;

  public RegistroCargosAprobadoPK()
  {
  }

  public RegistroCargosAprobadoPK(long idRegistroCargosAprobado)
  {
    this.idRegistroCargosAprobado = idRegistroCargosAprobado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RegistroCargosAprobadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RegistroCargosAprobadoPK thatPK)
  {
    return 
      this.idRegistroCargosAprobado == thatPK.idRegistroCargosAprobado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRegistroCargosAprobado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRegistroCargosAprobado);
  }
}