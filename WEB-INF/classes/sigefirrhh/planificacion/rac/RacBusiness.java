package sigefirrhh.planificacion.rac;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class RacBusiness extends AbstractBusiness
  implements Serializable
{
  private RegistroCargosAprobadoBeanBusiness registroCargosAprobadoBeanBusiness = new RegistroCargosAprobadoBeanBusiness();

  public void addRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado)
    throws Exception
  {
    this.registroCargosAprobadoBeanBusiness.addRegistroCargosAprobado(registroCargosAprobado);
  }

  public void updateRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado) throws Exception {
    this.registroCargosAprobadoBeanBusiness.updateRegistroCargosAprobado(registroCargosAprobado);
  }

  public void deleteRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado) throws Exception {
    this.registroCargosAprobadoBeanBusiness.deleteRegistroCargosAprobado(registroCargosAprobado);
  }

  public RegistroCargosAprobado findRegistroCargosAprobadoById(long registroCargosAprobadoId) throws Exception {
    return this.registroCargosAprobadoBeanBusiness.findRegistroCargosAprobadoById(registroCargosAprobadoId);
  }

  public Collection findAllRegistroCargosAprobado() throws Exception {
    return this.registroCargosAprobadoBeanBusiness.findRegistroCargosAprobadoAll();
  }

  public Collection findRegistroCargosAprobadoByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    return this.registroCargosAprobadoBeanBusiness.findByPlanPersonal(idPlanPersonal);
  }

  public Collection findRegistroCargosAprobadoByRegistro(long idRegistro)
    throws Exception
  {
    return this.registroCargosAprobadoBeanBusiness.findByRegistro(idRegistro);
  }

  public Collection findRegistroCargosAprobadoByCodigoNomina(int codigoNomina)
    throws Exception
  {
    return this.registroCargosAprobadoBeanBusiness.findByCodigoNomina(codigoNomina);
  }
}