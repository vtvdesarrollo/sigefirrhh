package sigefirrhh.planificacion.rac;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.Sede;
import sigefirrhh.base.personal.Gremio;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.personal.trabajador.Trabajador;

public class RacAprobado
  implements Serializable, PersistenceCapable
{
  private long idRacAprobado;
  private Registro registro;
  private int codigoRac;
  private String estatus;
  private String situacion;
  private Date fechaCreacion;
  private Cargo cargo;
  private Gremio gremio;
  private Dependencia dependencia;
  private Sede sede;
  private int horas;
  private int reportaRac;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargo", "codigoRac", "dependencia", "estatus", "fechaCreacion", "gremio", "horas", "idRacAprobado", "registro", "reportaRac", "sede", "situacion", "trabajador" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.personal.Gremio"), Integer.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.registro.Registro"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Sede"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 21, 21, 26, 21, 24, 26, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public int getCodigoRac()
  {
    return jdoGetcodigoRac(this);
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Date getFechaCreacion()
  {
    return jdoGetfechaCreacion(this);
  }

  public Gremio getGremio()
  {
    return jdoGetgremio(this);
  }

  public int getHoras()
  {
    return jdoGethoras(this);
  }

  public long getIdRacAprobado()
  {
    return jdoGetidRacAprobado(this);
  }

  public Registro getRegistro()
  {
    return jdoGetregistro(this);
  }

  public int getReportaRac()
  {
    return jdoGetreportaRac(this);
  }

  public Sede getSede()
  {
    return jdoGetsede(this);
  }

  public String getSituacion()
  {
    return jdoGetsituacion(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public void setCodigoRac(int i)
  {
    jdoSetcodigoRac(this, i);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFechaCreacion(Date date)
  {
    jdoSetfechaCreacion(this, date);
  }

  public void setGremio(Gremio gremio)
  {
    jdoSetgremio(this, gremio);
  }

  public void setHoras(int i)
  {
    jdoSethoras(this, i);
  }

  public void setIdRacAprobado(long l)
  {
    jdoSetidRacAprobado(this, l);
  }

  public void setRegistro(Registro registro)
  {
    jdoSetregistro(this, registro);
  }

  public void setReportaRac(int i)
  {
    jdoSetreportaRac(this, i);
  }

  public void setSede(Sede sede)
  {
    jdoSetsede(this, sede);
  }

  public void setSituacion(String string)
  {
    jdoSetsituacion(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.rac.RacAprobado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RacAprobado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RacAprobado localRacAprobado = new RacAprobado();
    localRacAprobado.jdoFlags = 1;
    localRacAprobado.jdoStateManager = paramStateManager;
    return localRacAprobado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RacAprobado localRacAprobado = new RacAprobado();
    localRacAprobado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRacAprobado.jdoFlags = 1;
    localRacAprobado.jdoStateManager = paramStateManager;
    return localRacAprobado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codigoRac);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCreacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.gremio);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.horas);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRacAprobado);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registro);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.reportaRac);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.sede);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.situacion);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoRac = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCreacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.gremio = ((Gremio)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.horas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRacAprobado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registro = ((Registro)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.reportaRac = localStateManager.replacingIntField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sede = ((Sede)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.situacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RacAprobado paramRacAprobado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramRacAprobado.cargo;
      return;
    case 1:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.codigoRac = paramRacAprobado.codigoRac;
      return;
    case 2:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramRacAprobado.dependencia;
      return;
    case 3:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramRacAprobado.estatus;
      return;
    case 4:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCreacion = paramRacAprobado.fechaCreacion;
      return;
    case 5:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.gremio = paramRacAprobado.gremio;
      return;
    case 6:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.horas = paramRacAprobado.horas;
      return;
    case 7:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.idRacAprobado = paramRacAprobado.idRacAprobado;
      return;
    case 8:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.registro = paramRacAprobado.registro;
      return;
    case 9:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.reportaRac = paramRacAprobado.reportaRac;
      return;
    case 10:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.sede = paramRacAprobado.sede;
      return;
    case 11:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.situacion = paramRacAprobado.situacion;
      return;
    case 12:
      if (paramRacAprobado == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramRacAprobado.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RacAprobado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RacAprobado localRacAprobado = (RacAprobado)paramObject;
    if (localRacAprobado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRacAprobado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RacAprobadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RacAprobadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RacAprobadoPK))
      throw new IllegalArgumentException("arg1");
    RacAprobadoPK localRacAprobadoPK = (RacAprobadoPK)paramObject;
    localRacAprobadoPK.idRacAprobado = this.idRacAprobado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RacAprobadoPK))
      throw new IllegalArgumentException("arg1");
    RacAprobadoPK localRacAprobadoPK = (RacAprobadoPK)paramObject;
    this.idRacAprobado = localRacAprobadoPK.idRacAprobado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RacAprobadoPK))
      throw new IllegalArgumentException("arg2");
    RacAprobadoPK localRacAprobadoPK = (RacAprobadoPK)paramObject;
    localRacAprobadoPK.idRacAprobado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RacAprobadoPK))
      throw new IllegalArgumentException("arg2");
    RacAprobadoPK localRacAprobadoPK = (RacAprobadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localRacAprobadoPK.idRacAprobado);
  }

  private static final Cargo jdoGetcargo(RacAprobado paramRacAprobado)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.cargo;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 0))
      return paramRacAprobado.cargo;
    return (Cargo)localStateManager.getObjectField(paramRacAprobado, jdoInheritedFieldCount + 0, paramRacAprobado.cargo);
  }

  private static final void jdoSetcargo(RacAprobado paramRacAprobado, Cargo paramCargo)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramRacAprobado, jdoInheritedFieldCount + 0, paramRacAprobado.cargo, paramCargo);
  }

  private static final int jdoGetcodigoRac(RacAprobado paramRacAprobado)
  {
    if (paramRacAprobado.jdoFlags <= 0)
      return paramRacAprobado.codigoRac;
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.codigoRac;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 1))
      return paramRacAprobado.codigoRac;
    return localStateManager.getIntField(paramRacAprobado, jdoInheritedFieldCount + 1, paramRacAprobado.codigoRac);
  }

  private static final void jdoSetcodigoRac(RacAprobado paramRacAprobado, int paramInt)
  {
    if (paramRacAprobado.jdoFlags == 0)
    {
      paramRacAprobado.codigoRac = paramInt;
      return;
    }
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.codigoRac = paramInt;
      return;
    }
    localStateManager.setIntField(paramRacAprobado, jdoInheritedFieldCount + 1, paramRacAprobado.codigoRac, paramInt);
  }

  private static final Dependencia jdoGetdependencia(RacAprobado paramRacAprobado)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.dependencia;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 2))
      return paramRacAprobado.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramRacAprobado, jdoInheritedFieldCount + 2, paramRacAprobado.dependencia);
  }

  private static final void jdoSetdependencia(RacAprobado paramRacAprobado, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramRacAprobado, jdoInheritedFieldCount + 2, paramRacAprobado.dependencia, paramDependencia);
  }

  private static final String jdoGetestatus(RacAprobado paramRacAprobado)
  {
    if (paramRacAprobado.jdoFlags <= 0)
      return paramRacAprobado.estatus;
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.estatus;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 3))
      return paramRacAprobado.estatus;
    return localStateManager.getStringField(paramRacAprobado, jdoInheritedFieldCount + 3, paramRacAprobado.estatus);
  }

  private static final void jdoSetestatus(RacAprobado paramRacAprobado, String paramString)
  {
    if (paramRacAprobado.jdoFlags == 0)
    {
      paramRacAprobado.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramRacAprobado, jdoInheritedFieldCount + 3, paramRacAprobado.estatus, paramString);
  }

  private static final Date jdoGetfechaCreacion(RacAprobado paramRacAprobado)
  {
    if (paramRacAprobado.jdoFlags <= 0)
      return paramRacAprobado.fechaCreacion;
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.fechaCreacion;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 4))
      return paramRacAprobado.fechaCreacion;
    return (Date)localStateManager.getObjectField(paramRacAprobado, jdoInheritedFieldCount + 4, paramRacAprobado.fechaCreacion);
  }

  private static final void jdoSetfechaCreacion(RacAprobado paramRacAprobado, Date paramDate)
  {
    if (paramRacAprobado.jdoFlags == 0)
    {
      paramRacAprobado.fechaCreacion = paramDate;
      return;
    }
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.fechaCreacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramRacAprobado, jdoInheritedFieldCount + 4, paramRacAprobado.fechaCreacion, paramDate);
  }

  private static final Gremio jdoGetgremio(RacAprobado paramRacAprobado)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.gremio;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 5))
      return paramRacAprobado.gremio;
    return (Gremio)localStateManager.getObjectField(paramRacAprobado, jdoInheritedFieldCount + 5, paramRacAprobado.gremio);
  }

  private static final void jdoSetgremio(RacAprobado paramRacAprobado, Gremio paramGremio)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.gremio = paramGremio;
      return;
    }
    localStateManager.setObjectField(paramRacAprobado, jdoInheritedFieldCount + 5, paramRacAprobado.gremio, paramGremio);
  }

  private static final int jdoGethoras(RacAprobado paramRacAprobado)
  {
    if (paramRacAprobado.jdoFlags <= 0)
      return paramRacAprobado.horas;
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.horas;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 6))
      return paramRacAprobado.horas;
    return localStateManager.getIntField(paramRacAprobado, jdoInheritedFieldCount + 6, paramRacAprobado.horas);
  }

  private static final void jdoSethoras(RacAprobado paramRacAprobado, int paramInt)
  {
    if (paramRacAprobado.jdoFlags == 0)
    {
      paramRacAprobado.horas = paramInt;
      return;
    }
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.horas = paramInt;
      return;
    }
    localStateManager.setIntField(paramRacAprobado, jdoInheritedFieldCount + 6, paramRacAprobado.horas, paramInt);
  }

  private static final long jdoGetidRacAprobado(RacAprobado paramRacAprobado)
  {
    return paramRacAprobado.idRacAprobado;
  }

  private static final void jdoSetidRacAprobado(RacAprobado paramRacAprobado, long paramLong)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.idRacAprobado = paramLong;
      return;
    }
    localStateManager.setLongField(paramRacAprobado, jdoInheritedFieldCount + 7, paramRacAprobado.idRacAprobado, paramLong);
  }

  private static final Registro jdoGetregistro(RacAprobado paramRacAprobado)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.registro;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 8))
      return paramRacAprobado.registro;
    return (Registro)localStateManager.getObjectField(paramRacAprobado, jdoInheritedFieldCount + 8, paramRacAprobado.registro);
  }

  private static final void jdoSetregistro(RacAprobado paramRacAprobado, Registro paramRegistro)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.registro = paramRegistro;
      return;
    }
    localStateManager.setObjectField(paramRacAprobado, jdoInheritedFieldCount + 8, paramRacAprobado.registro, paramRegistro);
  }

  private static final int jdoGetreportaRac(RacAprobado paramRacAprobado)
  {
    if (paramRacAprobado.jdoFlags <= 0)
      return paramRacAprobado.reportaRac;
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.reportaRac;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 9))
      return paramRacAprobado.reportaRac;
    return localStateManager.getIntField(paramRacAprobado, jdoInheritedFieldCount + 9, paramRacAprobado.reportaRac);
  }

  private static final void jdoSetreportaRac(RacAprobado paramRacAprobado, int paramInt)
  {
    if (paramRacAprobado.jdoFlags == 0)
    {
      paramRacAprobado.reportaRac = paramInt;
      return;
    }
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.reportaRac = paramInt;
      return;
    }
    localStateManager.setIntField(paramRacAprobado, jdoInheritedFieldCount + 9, paramRacAprobado.reportaRac, paramInt);
  }

  private static final Sede jdoGetsede(RacAprobado paramRacAprobado)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.sede;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 10))
      return paramRacAprobado.sede;
    return (Sede)localStateManager.getObjectField(paramRacAprobado, jdoInheritedFieldCount + 10, paramRacAprobado.sede);
  }

  private static final void jdoSetsede(RacAprobado paramRacAprobado, Sede paramSede)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.sede = paramSede;
      return;
    }
    localStateManager.setObjectField(paramRacAprobado, jdoInheritedFieldCount + 10, paramRacAprobado.sede, paramSede);
  }

  private static final String jdoGetsituacion(RacAprobado paramRacAprobado)
  {
    if (paramRacAprobado.jdoFlags <= 0)
      return paramRacAprobado.situacion;
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.situacion;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 11))
      return paramRacAprobado.situacion;
    return localStateManager.getStringField(paramRacAprobado, jdoInheritedFieldCount + 11, paramRacAprobado.situacion);
  }

  private static final void jdoSetsituacion(RacAprobado paramRacAprobado, String paramString)
  {
    if (paramRacAprobado.jdoFlags == 0)
    {
      paramRacAprobado.situacion = paramString;
      return;
    }
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.situacion = paramString;
      return;
    }
    localStateManager.setStringField(paramRacAprobado, jdoInheritedFieldCount + 11, paramRacAprobado.situacion, paramString);
  }

  private static final Trabajador jdoGettrabajador(RacAprobado paramRacAprobado)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
      return paramRacAprobado.trabajador;
    if (localStateManager.isLoaded(paramRacAprobado, jdoInheritedFieldCount + 12))
      return paramRacAprobado.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramRacAprobado, jdoInheritedFieldCount + 12, paramRacAprobado.trabajador);
  }

  private static final void jdoSettrabajador(RacAprobado paramRacAprobado, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramRacAprobado.jdoStateManager;
    if (localStateManager == null)
    {
      paramRacAprobado.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramRacAprobado, jdoInheritedFieldCount + 12, paramRacAprobado.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}