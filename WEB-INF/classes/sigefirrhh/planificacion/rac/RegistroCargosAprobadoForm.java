package sigefirrhh.planificacion.rac;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.planificacion.plan.PlanFacade;
import sigefirrhh.planificacion.plan.PlanPersonal;

public class RegistroCargosAprobadoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RegistroCargosAprobadoForm.class.getName());
  private RegistroCargosAprobado registroCargosAprobado;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PlanFacade planFacade = new PlanFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private RacFacade racFacade = new RacFacade();
  private boolean showRegistroCargosAprobadoByPlanPersonal;
  private boolean showRegistroCargosAprobadoByRegistro;
  private boolean showRegistroCargosAprobadoByCodigoNomina;
  private String findSelectPlanPersonal;
  private String findSelectRegistro;
  private int findCodigoNomina;
  private Collection findColPlanPersonal;
  private Collection findColRegistro;
  private Collection colPlanPersonal;
  private Collection colRegistro;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colDependencia;
  private Collection colRegion;
  private String selectPlanPersonal;
  private String selectRegistro;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectDependencia;
  private String selectRegion;
  private Object stateResultRegistroCargosAprobadoByPlanPersonal = null;

  private Object stateResultRegistroCargosAprobadoByRegistro = null;

  private Object stateResultRegistroCargosAprobadoByCodigoNomina = null;

  public String getFindSelectPlanPersonal()
  {
    return this.findSelectPlanPersonal;
  }
  public void setFindSelectPlanPersonal(String valPlanPersonal) {
    this.findSelectPlanPersonal = valPlanPersonal;
  }

  public Collection getFindColPlanPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }
  public String getFindSelectRegistro() {
    return this.findSelectRegistro;
  }
  public void setFindSelectRegistro(String valRegistro) {
    this.findSelectRegistro = valRegistro;
  }

  public Collection getFindColRegistro() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }
  public int getFindCodigoNomina() {
    return this.findCodigoNomina;
  }
  public void setFindCodigoNomina(int findCodigoNomina) {
    this.findCodigoNomina = findCodigoNomina;
  }

  public String getSelectPlanPersonal()
  {
    return this.selectPlanPersonal;
  }
  public void setSelectPlanPersonal(String valPlanPersonal) {
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    this.registroCargosAprobado.setPlanPersonal(null);
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      if (String.valueOf(planPersonal.getIdPlanPersonal()).equals(
        valPlanPersonal)) {
        this.registroCargosAprobado.setPlanPersonal(
          planPersonal);
        break;
      }
    }
    this.selectPlanPersonal = valPlanPersonal;
  }
  public String getSelectRegistro() {
    return this.selectRegistro;
  }
  public void setSelectRegistro(String valRegistro) {
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    this.registroCargosAprobado.setRegistro(null);
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      if (String.valueOf(registro.getIdRegistro()).equals(
        valRegistro)) {
        this.registroCargosAprobado.setRegistro(
          registro);
        break;
      }
    }
    this.selectRegistro = valRegistro;
  }
  public String getSelectManualCargoForCargo() {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.registroCargosAprobado.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.registroCargosAprobado.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.registroCargosAprobado.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.registroCargosAprobado.setCargo(
          cargo);
        break;
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectDependencia() {
    return this.selectDependencia;
  }
  public void setSelectDependencia(String valDependencia) {
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    this.registroCargosAprobado.setDependencia(null);
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      if (String.valueOf(dependencia.getIdDependencia()).equals(
        valDependencia)) {
        this.registroCargosAprobado.setDependencia(
          dependencia);
        break;
      }
    }
    this.selectDependencia = valDependencia;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String valRegion) {
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    this.registroCargosAprobado.setRegion(null);
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      if (String.valueOf(region.getIdRegion()).equals(
        valRegion)) {
        this.registroCargosAprobado.setRegion(
          region);
        break;
      }
    }
    this.selectRegion = valRegion;
  }
  public Collection getResult() {
    return this.result;
  }

  public RegistroCargosAprobado getRegistroCargosAprobado() {
    if (this.registroCargosAprobado == null) {
      this.registroCargosAprobado = new RegistroCargosAprobado();
    }
    return this.registroCargosAprobado;
  }

  public RegistroCargosAprobadoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPlanPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public Collection getColRegistro()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistro.iterator();
    Registro registro = null;
    while (iterator.hasNext()) {
      registro = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registro.getIdRegistro()), 
        registro.toString()));
    }
    return col;
  }

  public Collection getListAccion() {
    Collection col = new ArrayList();

    Iterator iterEntry = RegistroCargosAprobado.LISTA_ACCION.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEstatus() {
    Collection col = new ArrayList();

    Iterator iterEntry = RegistroCargosAprobado.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColDependencia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.findColRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colRegistro = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colDependencia = 
        this.estructuraFacade.findDependenciaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRegistroCargosAprobadoByPlanPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.racFacade.findRegistroCargosAprobadoByPlanPersonal(Long.valueOf(this.findSelectPlanPersonal).longValue());
      this.showRegistroCargosAprobadoByPlanPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegistroCargosAprobadoByPlanPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;
    this.findSelectRegistro = null;
    this.findCodigoNomina = 0;

    return null;
  }

  public String findRegistroCargosAprobadoByRegistro()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.racFacade.findRegistroCargosAprobadoByRegistro(Long.valueOf(this.findSelectRegistro).longValue());
      this.showRegistroCargosAprobadoByRegistro = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegistroCargosAprobadoByRegistro)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;
    this.findSelectRegistro = null;
    this.findCodigoNomina = 0;

    return null;
  }

  public String findRegistroCargosAprobadoByCodigoNomina()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.racFacade.findRegistroCargosAprobadoByCodigoNomina(this.findCodigoNomina);
      this.showRegistroCargosAprobadoByCodigoNomina = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRegistroCargosAprobadoByCodigoNomina)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;
    this.findSelectRegistro = null;
    this.findCodigoNomina = 0;

    return null;
  }

  public boolean isShowRegistroCargosAprobadoByPlanPersonal() {
    return this.showRegistroCargosAprobadoByPlanPersonal;
  }
  public boolean isShowRegistroCargosAprobadoByRegistro() {
    return this.showRegistroCargosAprobadoByRegistro;
  }
  public boolean isShowRegistroCargosAprobadoByCodigoNomina() {
    return this.showRegistroCargosAprobadoByCodigoNomina;
  }

  public String selectRegistroCargosAprobado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPlanPersonal = null;
    this.selectRegistro = null;
    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectDependencia = null;
    this.selectRegion = null;

    long idRegistroCargosAprobado = 
      Long.parseLong((String)requestParameterMap.get("idRegistroCargosAprobado"));
    try
    {
      this.registroCargosAprobado = 
        this.racFacade.findRegistroCargosAprobadoById(
        idRegistroCargosAprobado);
      if (this.registroCargosAprobado.getPlanPersonal() != null) {
        this.selectPlanPersonal = 
          String.valueOf(this.registroCargosAprobado.getPlanPersonal().getIdPlanPersonal());
      }
      if (this.registroCargosAprobado.getRegistro() != null) {
        this.selectRegistro = 
          String.valueOf(this.registroCargosAprobado.getRegistro().getIdRegistro());
      }
      if (this.registroCargosAprobado.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.registroCargosAprobado.getCargo().getIdCargo());
      }
      if (this.registroCargosAprobado.getDependencia() != null) {
        this.selectDependencia = 
          String.valueOf(this.registroCargosAprobado.getDependencia().getIdDependencia());
      }
      if (this.registroCargosAprobado.getRegion() != null) {
        this.selectRegion = 
          String.valueOf(this.registroCargosAprobado.getRegion().getIdRegion());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;

      if (this.registroCargosAprobado.getCargo() != null) {
        long idCargo = 
          this.registroCargosAprobado.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.registroCargosAprobado.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.registroCargosAprobado = null;
    this.showRegistroCargosAprobadoByPlanPersonal = false;
    this.showRegistroCargosAprobadoByRegistro = false;
    this.showRegistroCargosAprobadoByCodigoNomina = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.racFacade.addRegistroCargosAprobado(
          this.registroCargosAprobado);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.racFacade.updateRegistroCargosAprobado(
          this.registroCargosAprobado);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.racFacade.deleteRegistroCargosAprobado(
        this.registroCargosAprobado);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.registroCargosAprobado = new RegistroCargosAprobado();

    this.selectPlanPersonal = null;

    this.selectRegistro = null;

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    this.selectDependencia = null;

    this.selectRegion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.registroCargosAprobado.setIdRegistroCargosAprobado(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.rac.RegistroCargosAprobado"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.registroCargosAprobado = new RegistroCargosAprobado();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}