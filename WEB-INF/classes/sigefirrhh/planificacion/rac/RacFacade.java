package sigefirrhh.planificacion.rac;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class RacFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private RacBusiness racBusiness = new RacBusiness();

  public void addRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.racBusiness.addRegistroCargosAprobado(registroCargosAprobado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado) throws Exception
  {
    try { this.txn.open();
      this.racBusiness.updateRegistroCargosAprobado(registroCargosAprobado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado) throws Exception
  {
    try { this.txn.open();
      this.racBusiness.deleteRegistroCargosAprobado(registroCargosAprobado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RegistroCargosAprobado findRegistroCargosAprobadoById(long registroCargosAprobadoId) throws Exception
  {
    try { this.txn.open();
      RegistroCargosAprobado registroCargosAprobado = 
        this.racBusiness.findRegistroCargosAprobadoById(registroCargosAprobadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(registroCargosAprobado);
      return registroCargosAprobado;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRegistroCargosAprobado() throws Exception
  {
    try { this.txn.open();
      return this.racBusiness.findAllRegistroCargosAprobado();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosAprobadoByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.racBusiness.findRegistroCargosAprobadoByPlanPersonal(idPlanPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosAprobadoByRegistro(long idRegistro)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.racBusiness.findRegistroCargosAprobadoByRegistro(idRegistro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRegistroCargosAprobadoByCodigoNomina(int codigoNomina)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.racBusiness.findRegistroCargosAprobadoByCodigoNomina(codigoNomina);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}