package sigefirrhh.planificacion.rac;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.RegionBeanBusiness;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroBeanBusiness;
import sigefirrhh.planificacion.plan.PlanPersonal;
import sigefirrhh.planificacion.plan.PlanPersonalBeanBusiness;

public class RegistroCargosAprobadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RegistroCargosAprobado registroCargosAprobadoNew = 
      (RegistroCargosAprobado)BeanUtils.cloneBean(
      registroCargosAprobado);

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (registroCargosAprobadoNew.getPlanPersonal() != null) {
      registroCargosAprobadoNew.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        registroCargosAprobadoNew.getPlanPersonal().getIdPlanPersonal()));
    }

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (registroCargosAprobadoNew.getRegistro() != null) {
      registroCargosAprobadoNew.setRegistro(
        registroBeanBusiness.findRegistroById(
        registroCargosAprobadoNew.getRegistro().getIdRegistro()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (registroCargosAprobadoNew.getCargo() != null) {
      registroCargosAprobadoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        registroCargosAprobadoNew.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (registroCargosAprobadoNew.getDependencia() != null) {
      registroCargosAprobadoNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        registroCargosAprobadoNew.getDependencia().getIdDependencia()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (registroCargosAprobadoNew.getRegion() != null) {
      registroCargosAprobadoNew.setRegion(
        regionBeanBusiness.findRegionById(
        registroCargosAprobadoNew.getRegion().getIdRegion()));
    }
    pm.makePersistent(registroCargosAprobadoNew);
  }

  public void updateRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado) throws Exception
  {
    RegistroCargosAprobado registroCargosAprobadoModify = 
      findRegistroCargosAprobadoById(registroCargosAprobado.getIdRegistroCargosAprobado());

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (registroCargosAprobado.getPlanPersonal() != null) {
      registroCargosAprobado.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        registroCargosAprobado.getPlanPersonal().getIdPlanPersonal()));
    }

    RegistroBeanBusiness registroBeanBusiness = new RegistroBeanBusiness();

    if (registroCargosAprobado.getRegistro() != null) {
      registroCargosAprobado.setRegistro(
        registroBeanBusiness.findRegistroById(
        registroCargosAprobado.getRegistro().getIdRegistro()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (registroCargosAprobado.getCargo() != null) {
      registroCargosAprobado.setCargo(
        cargoBeanBusiness.findCargoById(
        registroCargosAprobado.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (registroCargosAprobado.getDependencia() != null) {
      registroCargosAprobado.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        registroCargosAprobado.getDependencia().getIdDependencia()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (registroCargosAprobado.getRegion() != null) {
      registroCargosAprobado.setRegion(
        regionBeanBusiness.findRegionById(
        registroCargosAprobado.getRegion().getIdRegion()));
    }

    BeanUtils.copyProperties(registroCargosAprobadoModify, registroCargosAprobado);
  }

  public void deleteRegistroCargosAprobado(RegistroCargosAprobado registroCargosAprobado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RegistroCargosAprobado registroCargosAprobadoDelete = 
      findRegistroCargosAprobadoById(registroCargosAprobado.getIdRegistroCargosAprobado());
    pm.deletePersistent(registroCargosAprobadoDelete);
  }

  public RegistroCargosAprobado findRegistroCargosAprobadoById(long idRegistroCargosAprobado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRegistroCargosAprobado == pIdRegistroCargosAprobado";
    Query query = pm.newQuery(RegistroCargosAprobado.class, filter);

    query.declareParameters("long pIdRegistroCargosAprobado");

    parameters.put("pIdRegistroCargosAprobado", new Long(idRegistroCargosAprobado));

    Collection colRegistroCargosAprobado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRegistroCargosAprobado.iterator();
    return (RegistroCargosAprobado)iterator.next();
  }

  public Collection findRegistroCargosAprobadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent registroCargosAprobadoExtent = pm.getExtent(
      RegistroCargosAprobado.class, true);
    Query query = pm.newQuery(registroCargosAprobadoExtent);
    query.setOrdering("codigoNomina ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPersonal.idPlanPersonal == pIdPlanPersonal";

    Query query = pm.newQuery(RegistroCargosAprobado.class, filter);

    query.declareParameters("long pIdPlanPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPersonal", new Long(idPlanPersonal));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargosAprobado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargosAprobado);

    return colRegistroCargosAprobado;
  }

  public Collection findByRegistro(long idRegistro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "registro.idRegistro == pIdRegistro";

    Query query = pm.newQuery(RegistroCargosAprobado.class, filter);

    query.declareParameters("long pIdRegistro");
    HashMap parameters = new HashMap();

    parameters.put("pIdRegistro", new Long(idRegistro));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargosAprobado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargosAprobado);

    return colRegistroCargosAprobado;
  }

  public Collection findByCodigoNomina(int codigoNomina)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigoNomina == pCodigoNomina";

    Query query = pm.newQuery(RegistroCargosAprobado.class, filter);

    query.declareParameters("int pCodigoNomina");
    HashMap parameters = new HashMap();

    parameters.put("pCodigoNomina", new Integer(codigoNomina));

    query.setOrdering("codigoNomina ascending");

    Collection colRegistroCargosAprobado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRegistroCargosAprobado);

    return colRegistroCargosAprobado;
  }
}