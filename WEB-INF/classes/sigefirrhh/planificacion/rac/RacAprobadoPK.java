package sigefirrhh.planificacion.rac;

import java.io.Serializable;

public class RacAprobadoPK
  implements Serializable
{
  public long idRacAprobado;

  public RacAprobadoPK()
  {
  }

  public RacAprobadoPK(long idRacAprobado)
  {
    this.idRacAprobado = idRacAprobado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RacAprobadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RacAprobadoPK thatPK)
  {
    return 
      this.idRacAprobado == thatPK.idRacAprobado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRacAprobado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRacAprobado);
  }
}