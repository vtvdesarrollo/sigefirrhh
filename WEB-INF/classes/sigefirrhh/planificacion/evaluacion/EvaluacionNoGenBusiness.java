package sigefirrhh.planificacion.evaluacion;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Collection;

public class EvaluacionNoGenBusiness extends EvaluacionBusiness
  implements Serializable
{
  private ResultadoEvaluacionNoGenBeanBusiness resultadoEvaluacionNoGenBeanBusiness = new ResultadoEvaluacionNoGenBeanBusiness();

  public Collection findResultadoEvaluacionByTipoPersonalAndRango(long idTipoPersonal, double puntos)
    throws Exception
  {
    return this.resultadoEvaluacionNoGenBeanBusiness.findByTipoPersonalAndRango(idTipoPersonal, puntos);
  }

  public Collection findResultadoEvaluacionByTipoPersonalAndAnio(long idTipoPersonal, int anio) throws Exception {
    return this.resultadoEvaluacionNoGenBeanBusiness.findByTipoPersonalAndAnio(idTipoPersonal, anio);
  }

  public ResultSet findResultadoEvaluacionByIdTipoPersonalAnioRango(long idTipoPersonal, int anio, double puntos) throws Exception {
    return this.resultadoEvaluacionNoGenBeanBusiness.findByIdTipoPersonalAnioRango(idTipoPersonal, anio, puntos);
  }
}