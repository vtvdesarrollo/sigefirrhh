package sigefirrhh.planificacion.evaluacion;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.CategoriaPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteNoGenFacade;
import sigefirrhh.personal.expediente.Personal;

public class RegistrarEvaluacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RegistrarEvaluacionForm.class.getName());

  protected static final Map LISTA_NIVEL_EMPLEADO = new LinkedHashMap();

  protected static final Map LISTA_NIVEL_OBRERO = new LinkedHashMap();
  private Collection listTipoPersonal;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private ExpedienteNoGenFacade expedienteNoGenFacade;
  private EvaluacionNoGenFacade evaluacionNoGenFacade;
  private EvaluacionFacade evaluacionFacade;
  private long idTipoPersonal;
  private long id_evaluacion;
  private String selectIdTipoPersonal;
  private int anio;
  private int mes;
  private int cedula;
  private double competencias = 0.0D;
  private double objetivos = 0.0D;

  private double manejoBienes = 0.0D;
  private double habitoSeguridad = 0.0D;
  private double calidadTrabajo = 0.0D;
  private double cumplimientoNormas = 0.0D;
  private double atencionPublico = 0.0D;
  private double interesTrabajo = 0.0D;
  private double cooperacion = 0.0D;
  private double cantidadTrabajo = 0.0D;
  private double tomaDecisiones = 0.0D;
  private double comunicacion = 0.0D;
  private double capacidadMando = 0.0D;
  private double coordinacion = 0.0D;
  private int cedulaSupervisor;
  private String nombreSupervisor;
  private int cedulaJefe;
  private String nombreJefe;
  private String nivelEmpleado;
  private String nivelObrero;
  private int cedulaFinal;
  private String nombre;
  private String apellido;
  private TipoPersonal tipoPersonal;
  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  StringBuffer sql = new StringBuffer();

  private IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
  private ResultSet rsRegistroTrabajador = null;
  private PreparedStatement stRegistroTrabajador = null;

  private ResultSet rsRegistroEvaluacion = null;
  private PreparedStatement stRegistroEvaluacion = null;
  private boolean showEmpleados;
  private boolean showObreros;
  Personal personal = new Personal();
  Evaluacion evaluacion = new Evaluacion();

  static
  {
    LISTA_NIVEL_EMPLEADO.put("1", "ADMINISTRATIVO");
    LISTA_NIVEL_EMPLEADO.put("2", "TECNICO PROFESIONAL");
    LISTA_NIVEL_EMPLEADO.put("3", "SUPERVISORIO");
    LISTA_NIVEL_OBRERO.put("1", "CALIFICADO/NO CALIFICADO");
    LISTA_NIVEL_OBRERO.put("2", "SUPERVISORIO");
  }

  public RegistrarEvaluacionForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.evaluacionNoGenFacade = new EvaluacionNoGenFacade();
    this.expedienteNoGenFacade = new ExpedienteNoGenFacade();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.connection.setAutoCommit(true);
      this.connection = Resource.getConnection();

      this.sql = new StringBuffer();
      this.sql.append("select p.id_personal, p.primer_nombre, p.primer_apellido, t.id_tipo_personal, ");
      this.sql.append(" d.id_dependencia, d.nombre as nombre_dependencia, ");
      this.sql.append(" c.id_cargo, c.descripcion_cargo as nombre_cargo ");
      this.sql.append(" from personal p, trabajador t, dependencia d, cargo c ");
      this.sql.append(" where p.cedula = ? ");
      this.sql.append(" and p.id_personal = t.id_personal ");
      this.sql.append(" and t.id_tipo_personal = ? ");
      this.sql.append(" and t.id_dependencia = d.id_dependencia");
      this.sql.append(" and t.id_cargo = c.id_cargo");
      this.stRegistroTrabajador = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      this.sql = new StringBuffer();
      this.sql.append("select * ");
      this.sql.append(" from evaluacion e ");
      this.sql.append(" where  e.id_personal = ? ");
      this.sql.append(" and  e.id_tipo_personal = ? ");
      this.sql.append(" and  e.anio = ? ");
      this.sql.append(" and  e.mes = ? ");

      this.stRegistroEvaluacion = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      return;
    }

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), " tp.aumento_evaluacion = 'S' ");
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(this.idTipoPersonal);
    try
    {
      this.showEmpleados = false;
      this.showObreros = false;

      if (this.idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(this.idTipoPersonal);
        if (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2"))
          this.showObreros = true;
        else
          this.showEmpleados = true;
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.nombre = "";
    this.apellido = "";
    this.cedulaFinal = 0;
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.stRegistroTrabajador.setInt(1, this.cedula);
      this.stRegistroTrabajador.setLong(2, this.idTipoPersonal);
      this.rsRegistroTrabajador = this.stRegistroTrabajador.executeQuery();

      if (this.rsRegistroTrabajador.next()) {
        this.nombre = this.rsRegistroTrabajador.getString("primer_nombre");
        this.apellido = this.rsRegistroTrabajador.getString("primer_apellido");
      }
      else
      {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La persona no esta registrada", ""));
        return null;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras se buscaba a la persona", ""));
      return null;
    }
    try
    {
      this.stRegistroEvaluacion.setLong(1, this.rsRegistroTrabajador.getLong("id_personal"));
      this.stRegistroEvaluacion.setLong(2, this.idTipoPersonal);
      this.stRegistroEvaluacion.setInt(3, this.anio);
      this.stRegistroEvaluacion.setInt(4, this.mes);
      this.rsRegistroEvaluacion = this.stRegistroEvaluacion.executeQuery();

      this.sql = new StringBuffer();
      this.stExecute = this.connection.createStatement();
      double puntos = 0.0D;
      if (!this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2")) {
        puntos = this.competencias + this.objetivos;
        if ((this.competencias > 250.0D) || (this.objetivos > 250.0D)) {
          context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Competencia u Objetivos es mayor a 250 puntos", ""));
          return null;
        }
      } else {
        puntos = this.manejoBienes + this.habitoSeguridad + this.calidadTrabajo + this.cumplimientoNormas + 
          this.atencionPublico + this.interesTrabajo + this.cooperacion + this.cantidadTrabajo + 
          this.tomaDecisiones + this.comunicacion + this.capacidadMando + this.coordinacion;
      }
      ResultSet resultado = this.evaluacionNoGenFacade.findResultadoEvaluacionByIdTipoPersonalAnioRango(this.idTipoPersonal, this.anio, puntos);

      if (resultado != null) {
        log.error("************0*************");
        if (this.rsRegistroEvaluacion.next()) {
          context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Esa cedula ya esta registrada", ""));
          return null;
        }

        if (!this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2")) {
          this.sql.append(" insert into evaluacion (id_personal, id_resultado_evaluacion, resultado_competencias, resultado_objetivos, ");
        } else {
          this.sql.append(" insert into evaluacion (id_personal, id_resultado_evaluacion, habito_seguridad, calidad_trabajo, ");
          this.sql.append(" cumplimiento_normas, atencion_publico, interes_trabajo, cooperacion, cantidad_trabajo, ");
          this.sql.append(" toma_decisiones, comunicacion, capacidad_mando, coordinacion, manejo_bienes,resultado_competencias, ");
        }
        this.sql.append(" cedula_supervisor, cedula_jefe, nombre_supervisor, nombre_jefe, monto_aumentar, ");
        this.sql.append(" porcentaje_aumento, monto_unico, numero_pasos, anio, mes, id_tipo_personal, nivel, ");
        this.sql.append(" id_cargo, id_dependencia, nombre_cargo, nombre_dependencia, id_evaluacion) values(");
        this.sql.append(this.rsRegistroTrabajador.getLong("id_personal") + ", " + resultado.getLong("id_resultado_evaluacion") + ", ");
        if (!this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2")) {
          this.sql.append(this.competencias + ", " + this.objetivos + ", ");
        } else {
          this.sql.append(this.habitoSeguridad + ", " + this.calidadTrabajo + ", ");
          this.sql.append(this.cumplimientoNormas + ", " + this.atencionPublico + ", ");
          this.sql.append(this.interesTrabajo + ", " + this.cooperacion + ", ");
          this.sql.append(this.cantidadTrabajo + ", " + this.tomaDecisiones + ", ");
          this.sql.append(this.comunicacion + ", " + this.capacidadMando + ", ");
          this.sql.append(this.coordinacion + ", " + this.manejoBienes + ", ");
          this.sql.append(puntos + ", ");
        }

        this.sql.append(this.cedulaSupervisor + ", " + this.cedulaJefe + ", '");
        this.sql.append(this.nombreSupervisor + "', '" + this.nombreJefe + "', " + resultado.getDouble("monto_aumentar") + ", ");
        this.sql.append(resultado.getDouble("porcentaje_aumento") + ", " + resultado.getDouble("monto_unico") + ", ");
        this.sql.append(resultado.getInt("numero_pasos") + ", " + this.anio + ", " + this.mes + ", ");
        this.sql.append(this.rsRegistroTrabajador.getLong("id_tipo_personal") + ", '");
        if (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2"))
          this.sql.append(this.nivelObrero + "', ");
        else {
          this.sql.append(this.nivelEmpleado + "', ");
        }
        this.sql.append(this.rsRegistroTrabajador.getLong("id_cargo") + ", " + this.rsRegistroTrabajador.getLong("id_dependencia") + ", '");
        this.sql.append(this.rsRegistroTrabajador.getString("nombre_cargo") + "', '" + this.rsRegistroTrabajador.getString("nombre_dependencia") + "', ");
        this.id_evaluacion = this.identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.evaluacion.Evaluacion");
        this.sql.append(this.id_evaluacion + ")");

        this.stExecute.addBatch(this.sql.toString());
        this.stExecute.executeBatch();

        this.stExecute.close();
        log.error("************1*************");

        log.error("************regreso *************");
      }
      else
      {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Resultado evaluación no esta registrado para este rango o este tipo personal", ""));
        return null;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error", ""));
      return null;
    }

    this.cedulaFinal = this.cedula;
    this.manejoBienes = 0.0D;
    this.habitoSeguridad = 0.0D;
    this.calidadTrabajo = 0.0D;
    this.cumplimientoNormas = 0.0D;
    this.atencionPublico = 0.0D;
    this.interesTrabajo = 0.0D;
    this.cooperacion = 0.0D;
    this.cantidadTrabajo = 0.0D;
    this.tomaDecisiones = 0.0D;
    this.comunicacion = 0.0D;
    this.capacidadMando = 0.0D;
    this.coordinacion = 0.0D;
    this.cedula = 0;
    this.competencias = 0.0D;
    this.objetivos = 0.0D;

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));

    return null;
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListNivelEmpleado() {
    Collection col = new ArrayList();

    Iterator iterEntry = Evaluacion.LISTA_NIVEL_EMPLEADO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public Collection getListNivelObrero() {
    Collection col = new ArrayList();

    Iterator iterEntry = Evaluacion.LISTA_NIVEL_OBRERO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }
  public int getCedula() {
    return this.cedula;
  }

  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public int getCedulaFinal() {
    return this.cedulaFinal;
  }
  public void setCedulaFinal(int cedulaFinal) {
    this.cedulaFinal = cedulaFinal;
  }
  public String getApellido() {
    return this.apellido;
  }
  public void setApellido(String apellido) {
    this.apellido = apellido;
  }
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public int getAnio()
  {
    return this.anio;
  }

  public void setAnio(int anio) {
    this.anio = anio;
  }

  public int getMes() {
    return this.mes;
  }

  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getNombreJefe() {
    return this.nombreJefe;
  }

  public void setNombreJefe(String nombreJefe) {
    this.nombreJefe = nombreJefe;
  }

  public String getNombreSupervisor() {
    return this.nombreSupervisor;
  }

  public void setNombreSupervisor(String nombreSupervisor) {
    this.nombreSupervisor = nombreSupervisor;
  }

  public String getSelectIdTipoPersonal()
  {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }
  public int getCedulaJefe() {
    return this.cedulaJefe;
  }

  public void setCedulaJefe(int cedulaJefe) {
    this.cedulaJefe = cedulaJefe;
  }

  public int getCedulaSupervisor() {
    return this.cedulaSupervisor;
  }

  public void setCedulaSupervisor(int cedulaSupervisor) {
    this.cedulaSupervisor = cedulaSupervisor;
  }

  public boolean isShowEmpleados()
  {
    return this.showEmpleados;
  }

  public boolean isShowObreros() {
    return this.showObreros;
  }

  public String getNivelEmpleado()
  {
    return this.nivelEmpleado;
  }

  public void setNivelEmpleado(String nivelEmpleado) {
    this.nivelEmpleado = nivelEmpleado;
  }

  public String getNivelObrero() {
    return this.nivelObrero;
  }

  public void setNivelObrero(String nivelObrero) {
    this.nivelObrero = nivelObrero;
  }

  public double getAtencionPublico() {
    return this.atencionPublico;
  }

  public void setAtencionPublico(double atencionPublico) {
    this.atencionPublico = atencionPublico;
  }

  public double getCalidadTrabajo() {
    return this.calidadTrabajo;
  }

  public void setCalidadTrabajo(double calidadTrabajo) {
    this.calidadTrabajo = calidadTrabajo;
  }

  public double getCantidadTrabajo() {
    return this.cantidadTrabajo;
  }

  public void setCantidadTrabajo(double cantidadTrabajo) {
    this.cantidadTrabajo = cantidadTrabajo;
  }

  public double getCapacidadMando() {
    return this.capacidadMando;
  }

  public void setCapacidadMando(double capacidadMando) {
    this.capacidadMando = capacidadMando;
  }

  public double getCompetencias() {
    return this.competencias;
  }

  public void setCompetencias(double competencias) {
    this.competencias = competencias;
  }

  public double getComunicacion() {
    return this.comunicacion;
  }

  public void setComunicacion(double comunicacion) {
    this.comunicacion = comunicacion;
  }

  public double getCooperacion() {
    return this.cooperacion;
  }

  public void setCooperacion(double cooperacion) {
    this.cooperacion = cooperacion;
  }

  public double getCoordinacion() {
    return this.coordinacion;
  }

  public void setCoordinacion(double coordinacion) {
    this.coordinacion = coordinacion;
  }

  public double getCumplimientoNormas() {
    return this.cumplimientoNormas;
  }

  public void setCumplimientoNormas(double cumplimientoNormas) {
    this.cumplimientoNormas = cumplimientoNormas;
  }

  public double getHabitoSeguridad() {
    return this.habitoSeguridad;
  }

  public void setHabitoSeguridad(double habitoSeguridad) {
    this.habitoSeguridad = habitoSeguridad;
  }

  public double getInteresTrabajo() {
    return this.interesTrabajo;
  }

  public void setInteresTrabajo(double interesTrabajo) {
    this.interesTrabajo = interesTrabajo;
  }

  public double getManejoBienes() {
    return this.manejoBienes;
  }

  public void setManejoBienes(double manejoBienes) {
    this.manejoBienes = manejoBienes;
  }

  public double getObjetivos() {
    return this.objetivos;
  }

  public void setObjetivos(double objetivos) {
    this.objetivos = objetivos;
  }

  public double getTomaDecisiones() {
    return this.tomaDecisiones;
  }

  public void setTomaDecisiones(double tomaDecisiones) {
    this.tomaDecisiones = tomaDecisiones;
  }
}