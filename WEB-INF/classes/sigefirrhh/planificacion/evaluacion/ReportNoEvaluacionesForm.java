package sigefirrhh.planificacion.evaluacion;

import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportNoEvaluacionesForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportNoEvaluacionesForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private Collection colTipoPersonal;
  private String idTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private EvaluacionNoGenFacade evaluacionFacade = new EvaluacionNoGenFacade();
  private LoginSession login;
  private int anio;
  private int mes;
  private String orden = "1";
  private String motivo = "0";
  private String formato = "1";

  public ReportNoEvaluacionesForm() {
    this.reportName = "noevaluacionesdeta";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.definicionesFacade = new DefinicionesNoGenFacade();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.anio = (new Date().getYear() + 1900);
    this.mes = (new Date().getMonth() + 1);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportNoEvaluacionesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    String idTipoPersonal = 
      (String)event.getNewValue();
    this.idTipoPersonal = idTipoPersonal;
    try
    {
      this.idTipoPersonal.equals("0");
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  private void cambiarNombreAReporte() {
    this.reportName = "noevaluacionesdeta";
    if (this.orden.equals("1")) {
      this.reportName += "alf";
    }
    if (this.orden.equals("2")) {
      this.reportName += "ced";
    }
    if (this.orden.equals("3")) {
      this.reportName += "cod";
    }
    if (this.formato.equals("2"))
      this.reportName = ("a_" + this.reportName);
  }

  public void refresh()
  {
    try
    {
      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), " tp.aumento_evaluacion = 'S' ");
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);

      this.colTipoPersonal = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();

    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
    parameters.put("anio", new Integer(this.anio));
    parameters.put("mes", new Integer(this.mes));
    parameters.put("motivo", this.motivo);

    this.reportName = "noevaluacionesdeta";
    if (this.orden.equals("1")) {
      this.reportName += "alf";
    }
    if (this.orden.equals("2")) {
      this.reportName += "ced";
    }
    if (this.orden.equals("3")) {
      this.reportName += "cod";
    }

    if (this.formato.equals("2")) {
      this.reportName = ("a_" + this.reportName);
    }

    JasperForWeb report = new JasperForWeb();

    report.setType(1);

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/evaluacion");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public int getAnio()
  {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(String idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }
  public String getMotivo() {
    return this.motivo;
  }
  public void setMotivo(String motivo) {
    this.motivo = motivo;
  }

  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
}