package sigefirrhh.planificacion.evaluacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ResultadoEvaluacion
  implements Serializable, PersistenceCapable
{
  private long idResultadoEvaluacion;
  private TipoPersonal tipoPersonal;
  private int anio;
  private String codResultadoEvaluacion;
  private String descripcion;
  private double rangoMinimo;
  private double rangoMaximo;
  private double porcentajeAumento;
  private int numeroPasos;
  private double montoAumentar;
  private double montoUnico;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "codResultadoEvaluacion", "descripcion", "idResultadoEvaluacion", "montoAumentar", "montoUnico", "numeroPasos", "porcentajeAumento", "rangoMaximo", "rangoMinimo", "tipoPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Double.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ResultadoEvaluacion()
  {
    jdoSetanio(this, 0);

    jdoSetrangoMinimo(this, 0.0D);

    jdoSetrangoMaximo(this, 0.0D);

    jdoSetporcentajeAumento(this, 0.0D);

    jdoSetnumeroPasos(this, 0);

    jdoSetmontoAumentar(this, 0.0D);

    jdoSetmontoUnico(this, 0.0D);
  }
  public String toString() { return jdoGetdescripcion(this) + "  -  Entre: " + 
      jdoGetrangoMinimo(this) + " y " + jdoGetrangoMaximo(this);
  }

  public String getCodResultadoEvaluacion()
  {
    return jdoGetcodResultadoEvaluacion(this);
  }

  public void setCodResultadoEvaluacion(String codResultadoEvaluacion)
  {
    jdoSetcodResultadoEvaluacion(this, codResultadoEvaluacion);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion)
  {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdResultadoEvaluacion()
  {
    return jdoGetidResultadoEvaluacion(this);
  }

  public void setIdResultadoEvaluacion(long idResultadoEvaluacion)
  {
    jdoSetidResultadoEvaluacion(this, idResultadoEvaluacion);
  }

  public double getMontoAumentar()
  {
    return jdoGetmontoAumentar(this);
  }

  public void setMontoAumentar(double montoAumentar)
  {
    jdoSetmontoAumentar(this, montoAumentar);
  }

  public double getMontoUnico()
  {
    return jdoGetmontoUnico(this);
  }

  public void setMontoUnico(double montoUnico)
  {
    jdoSetmontoUnico(this, montoUnico);
  }

  public int getNumeroPasos()
  {
    return jdoGetnumeroPasos(this);
  }

  public void setNumeroPasos(int numeroPasos)
  {
    jdoSetnumeroPasos(this, numeroPasos);
  }

  public double getPorcentajeAumento()
  {
    return jdoGetporcentajeAumento(this);
  }

  public void setPorcentajeAumento(double porcentajeAumento)
  {
    jdoSetporcentajeAumento(this, porcentajeAumento);
  }

  public double getRangoMaximo()
  {
    return jdoGetrangoMaximo(this);
  }

  public void setRangoMaximo(double rangoMaximo)
  {
    jdoSetrangoMaximo(this, rangoMaximo);
  }

  public double getRangoMinimo()
  {
    return jdoGetrangoMinimo(this);
  }

  public void setRangoMinimo(double rangoMinimo)
  {
    jdoSetrangoMinimo(this, rangoMinimo);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal)
  {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.ResultadoEvaluacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ResultadoEvaluacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ResultadoEvaluacion localResultadoEvaluacion = new ResultadoEvaluacion();
    localResultadoEvaluacion.jdoFlags = 1;
    localResultadoEvaluacion.jdoStateManager = paramStateManager;
    return localResultadoEvaluacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ResultadoEvaluacion localResultadoEvaluacion = new ResultadoEvaluacion();
    localResultadoEvaluacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localResultadoEvaluacion.jdoFlags = 1;
    localResultadoEvaluacion.jdoStateManager = paramStateManager;
    return localResultadoEvaluacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codResultadoEvaluacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idResultadoEvaluacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAumentar);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoUnico);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroPasos);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeAumento);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.rangoMaximo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.rangoMinimo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codResultadoEvaluacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idResultadoEvaluacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAumentar = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoUnico = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroPasos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeAumento = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rangoMaximo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rangoMinimo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ResultadoEvaluacion paramResultadoEvaluacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramResultadoEvaluacion.anio;
      return;
    case 1:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.codResultadoEvaluacion = paramResultadoEvaluacion.codResultadoEvaluacion;
      return;
    case 2:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramResultadoEvaluacion.descripcion;
      return;
    case 3:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.idResultadoEvaluacion = paramResultadoEvaluacion.idResultadoEvaluacion;
      return;
    case 4:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.montoAumentar = paramResultadoEvaluacion.montoAumentar;
      return;
    case 5:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.montoUnico = paramResultadoEvaluacion.montoUnico;
      return;
    case 6:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.numeroPasos = paramResultadoEvaluacion.numeroPasos;
      return;
    case 7:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeAumento = paramResultadoEvaluacion.porcentajeAumento;
      return;
    case 8:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.rangoMaximo = paramResultadoEvaluacion.rangoMaximo;
      return;
    case 9:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.rangoMinimo = paramResultadoEvaluacion.rangoMinimo;
      return;
    case 10:
      if (paramResultadoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramResultadoEvaluacion.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ResultadoEvaluacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ResultadoEvaluacion localResultadoEvaluacion = (ResultadoEvaluacion)paramObject;
    if (localResultadoEvaluacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localResultadoEvaluacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ResultadoEvaluacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ResultadoEvaluacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResultadoEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    ResultadoEvaluacionPK localResultadoEvaluacionPK = (ResultadoEvaluacionPK)paramObject;
    localResultadoEvaluacionPK.idResultadoEvaluacion = this.idResultadoEvaluacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ResultadoEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    ResultadoEvaluacionPK localResultadoEvaluacionPK = (ResultadoEvaluacionPK)paramObject;
    this.idResultadoEvaluacion = localResultadoEvaluacionPK.idResultadoEvaluacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResultadoEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    ResultadoEvaluacionPK localResultadoEvaluacionPK = (ResultadoEvaluacionPK)paramObject;
    localResultadoEvaluacionPK.idResultadoEvaluacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ResultadoEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    ResultadoEvaluacionPK localResultadoEvaluacionPK = (ResultadoEvaluacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localResultadoEvaluacionPK.idResultadoEvaluacion);
  }

  private static final int jdoGetanio(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.anio;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.anio;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 0))
      return paramResultadoEvaluacion.anio;
    return localStateManager.getIntField(paramResultadoEvaluacion, jdoInheritedFieldCount + 0, paramResultadoEvaluacion.anio);
  }

  private static final void jdoSetanio(ResultadoEvaluacion paramResultadoEvaluacion, int paramInt)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramResultadoEvaluacion, jdoInheritedFieldCount + 0, paramResultadoEvaluacion.anio, paramInt);
  }

  private static final String jdoGetcodResultadoEvaluacion(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.codResultadoEvaluacion;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.codResultadoEvaluacion;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 1))
      return paramResultadoEvaluacion.codResultadoEvaluacion;
    return localStateManager.getStringField(paramResultadoEvaluacion, jdoInheritedFieldCount + 1, paramResultadoEvaluacion.codResultadoEvaluacion);
  }

  private static final void jdoSetcodResultadoEvaluacion(ResultadoEvaluacion paramResultadoEvaluacion, String paramString)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.codResultadoEvaluacion = paramString;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.codResultadoEvaluacion = paramString;
      return;
    }
    localStateManager.setStringField(paramResultadoEvaluacion, jdoInheritedFieldCount + 1, paramResultadoEvaluacion.codResultadoEvaluacion, paramString);
  }

  private static final String jdoGetdescripcion(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.descripcion;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.descripcion;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 2))
      return paramResultadoEvaluacion.descripcion;
    return localStateManager.getStringField(paramResultadoEvaluacion, jdoInheritedFieldCount + 2, paramResultadoEvaluacion.descripcion);
  }

  private static final void jdoSetdescripcion(ResultadoEvaluacion paramResultadoEvaluacion, String paramString)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramResultadoEvaluacion, jdoInheritedFieldCount + 2, paramResultadoEvaluacion.descripcion, paramString);
  }

  private static final long jdoGetidResultadoEvaluacion(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    return paramResultadoEvaluacion.idResultadoEvaluacion;
  }

  private static final void jdoSetidResultadoEvaluacion(ResultadoEvaluacion paramResultadoEvaluacion, long paramLong)
  {
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.idResultadoEvaluacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramResultadoEvaluacion, jdoInheritedFieldCount + 3, paramResultadoEvaluacion.idResultadoEvaluacion, paramLong);
  }

  private static final double jdoGetmontoAumentar(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.montoAumentar;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.montoAumentar;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 4))
      return paramResultadoEvaluacion.montoAumentar;
    return localStateManager.getDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 4, paramResultadoEvaluacion.montoAumentar);
  }

  private static final void jdoSetmontoAumentar(ResultadoEvaluacion paramResultadoEvaluacion, double paramDouble)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.montoAumentar = paramDouble;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.montoAumentar = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 4, paramResultadoEvaluacion.montoAumentar, paramDouble);
  }

  private static final double jdoGetmontoUnico(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.montoUnico;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.montoUnico;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 5))
      return paramResultadoEvaluacion.montoUnico;
    return localStateManager.getDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 5, paramResultadoEvaluacion.montoUnico);
  }

  private static final void jdoSetmontoUnico(ResultadoEvaluacion paramResultadoEvaluacion, double paramDouble)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.montoUnico = paramDouble;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.montoUnico = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 5, paramResultadoEvaluacion.montoUnico, paramDouble);
  }

  private static final int jdoGetnumeroPasos(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.numeroPasos;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.numeroPasos;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 6))
      return paramResultadoEvaluacion.numeroPasos;
    return localStateManager.getIntField(paramResultadoEvaluacion, jdoInheritedFieldCount + 6, paramResultadoEvaluacion.numeroPasos);
  }

  private static final void jdoSetnumeroPasos(ResultadoEvaluacion paramResultadoEvaluacion, int paramInt)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.numeroPasos = paramInt;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.numeroPasos = paramInt;
      return;
    }
    localStateManager.setIntField(paramResultadoEvaluacion, jdoInheritedFieldCount + 6, paramResultadoEvaluacion.numeroPasos, paramInt);
  }

  private static final double jdoGetporcentajeAumento(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.porcentajeAumento;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.porcentajeAumento;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 7))
      return paramResultadoEvaluacion.porcentajeAumento;
    return localStateManager.getDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 7, paramResultadoEvaluacion.porcentajeAumento);
  }

  private static final void jdoSetporcentajeAumento(ResultadoEvaluacion paramResultadoEvaluacion, double paramDouble)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.porcentajeAumento = paramDouble;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.porcentajeAumento = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 7, paramResultadoEvaluacion.porcentajeAumento, paramDouble);
  }

  private static final double jdoGetrangoMaximo(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.rangoMaximo;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.rangoMaximo;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 8))
      return paramResultadoEvaluacion.rangoMaximo;
    return localStateManager.getDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 8, paramResultadoEvaluacion.rangoMaximo);
  }

  private static final void jdoSetrangoMaximo(ResultadoEvaluacion paramResultadoEvaluacion, double paramDouble)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.rangoMaximo = paramDouble;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.rangoMaximo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 8, paramResultadoEvaluacion.rangoMaximo, paramDouble);
  }

  private static final double jdoGetrangoMinimo(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    if (paramResultadoEvaluacion.jdoFlags <= 0)
      return paramResultadoEvaluacion.rangoMinimo;
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.rangoMinimo;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 9))
      return paramResultadoEvaluacion.rangoMinimo;
    return localStateManager.getDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 9, paramResultadoEvaluacion.rangoMinimo);
  }

  private static final void jdoSetrangoMinimo(ResultadoEvaluacion paramResultadoEvaluacion, double paramDouble)
  {
    if (paramResultadoEvaluacion.jdoFlags == 0)
    {
      paramResultadoEvaluacion.rangoMinimo = paramDouble;
      return;
    }
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.rangoMinimo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramResultadoEvaluacion, jdoInheritedFieldCount + 9, paramResultadoEvaluacion.rangoMinimo, paramDouble);
  }

  private static final TipoPersonal jdoGettipoPersonal(ResultadoEvaluacion paramResultadoEvaluacion)
  {
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramResultadoEvaluacion.tipoPersonal;
    if (localStateManager.isLoaded(paramResultadoEvaluacion, jdoInheritedFieldCount + 10))
      return paramResultadoEvaluacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramResultadoEvaluacion, jdoInheritedFieldCount + 10, paramResultadoEvaluacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ResultadoEvaluacion paramResultadoEvaluacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramResultadoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramResultadoEvaluacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramResultadoEvaluacion, jdoInheritedFieldCount + 10, paramResultadoEvaluacion.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}