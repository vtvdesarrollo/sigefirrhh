package sigefirrhh.planificacion.evaluacion;

import java.io.Serializable;

public class AccionEvaluacionPK
  implements Serializable
{
  public long idAccionEvaluacion;

  public AccionEvaluacionPK()
  {
  }

  public AccionEvaluacionPK(long idAccionEvaluacion)
  {
    this.idAccionEvaluacion = idAccionEvaluacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AccionEvaluacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AccionEvaluacionPK thatPK)
  {
    return 
      this.idAccionEvaluacion == thatPK.idAccionEvaluacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAccionEvaluacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAccionEvaluacion);
  }
}