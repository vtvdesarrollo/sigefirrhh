package sigefirrhh.planificacion.evaluacion;

import java.io.Serializable;

public class ConceptoEvaluacionPK
  implements Serializable
{
  public long idConceptoEvaluacion;

  public ConceptoEvaluacionPK()
  {
  }

  public ConceptoEvaluacionPK(long idConceptoEvaluacion)
  {
    this.idConceptoEvaluacion = idConceptoEvaluacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConceptoEvaluacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConceptoEvaluacionPK thatPK)
  {
    return 
      this.idConceptoEvaluacion == thatPK.idConceptoEvaluacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConceptoEvaluacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConceptoEvaluacion);
  }
}