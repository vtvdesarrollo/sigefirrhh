package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class ApelacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addApelacion(Apelacion apelacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Apelacion apelacionNew = 
      (Apelacion)BeanUtils.cloneBean(
      apelacion);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (apelacionNew.getPersonal() != null) {
      apelacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        apelacionNew.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    pm.makePersistent(apelacionNew);
  }

  public void updateApelacion(Apelacion apelacion) throws Exception
  {
    Apelacion apelacionModify = 
      findApelacionById(apelacion.getIdApelacion());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (apelacion.getPersonal() != null) {
      apelacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        apelacion.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    BeanUtils.copyProperties(apelacionModify, apelacion);
  }

  public void deleteApelacion(Apelacion apelacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Apelacion apelacionDelete = 
      findApelacionById(apelacion.getIdApelacion());
    pm.deletePersistent(apelacionDelete);
  }

  public Apelacion findApelacionById(long idApelacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idApelacion == pIdApelacion";
    Query query = pm.newQuery(Apelacion.class, filter);

    query.declareParameters("long pIdApelacion");

    parameters.put("pIdApelacion", new Long(idApelacion));

    Collection colApelacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colApelacion.iterator();
    return (Apelacion)iterator.next();
  }

  public Collection findApelacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent apelacionExtent = pm.getExtent(
      Apelacion.class, true);
    Query query = pm.newQuery(apelacionExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Apelacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colApelacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colApelacion);

    return colApelacion;
  }
}