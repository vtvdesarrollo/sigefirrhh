package sigefirrhh.planificacion.evaluacion;

import java.io.Serializable;

public class ApelacionPK
  implements Serializable
{
  public long idApelacion;

  public ApelacionPK()
  {
  }

  public ApelacionPK(long idApelacion)
  {
    this.idApelacion = idApelacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ApelacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ApelacionPK thatPK)
  {
    return 
      this.idApelacion == thatPK.idApelacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idApelacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idApelacion);
  }
}