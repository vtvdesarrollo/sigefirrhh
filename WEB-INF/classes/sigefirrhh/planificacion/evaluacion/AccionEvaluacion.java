package sigefirrhh.planificacion.evaluacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AccionEvaluacion
  implements Serializable, PersistenceCapable
{
  private long idAccionEvaluacion;
  private String codAccionEvaluacion;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codAccionEvaluacion", "descripcion", "idAccionEvaluacion" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this);
  }

  public String getCodAccionEvaluacion()
  {
    return jdoGetcodAccionEvaluacion(this);
  }

  public void setCodAccionEvaluacion(String codAccionEvaluacion)
  {
    jdoSetcodAccionEvaluacion(this, codAccionEvaluacion);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion)
  {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdAccionEvaluacion()
  {
    return jdoGetidAccionEvaluacion(this);
  }

  public void setIdAccionEvaluacion(long idAccionEvaluacion)
  {
    jdoSetidAccionEvaluacion(this, idAccionEvaluacion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.AccionEvaluacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AccionEvaluacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AccionEvaluacion localAccionEvaluacion = new AccionEvaluacion();
    localAccionEvaluacion.jdoFlags = 1;
    localAccionEvaluacion.jdoStateManager = paramStateManager;
    return localAccionEvaluacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AccionEvaluacion localAccionEvaluacion = new AccionEvaluacion();
    localAccionEvaluacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAccionEvaluacion.jdoFlags = 1;
    localAccionEvaluacion.jdoStateManager = paramStateManager;
    return localAccionEvaluacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codAccionEvaluacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAccionEvaluacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codAccionEvaluacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAccionEvaluacion = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AccionEvaluacion paramAccionEvaluacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAccionEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.codAccionEvaluacion = paramAccionEvaluacion.codAccionEvaluacion;
      return;
    case 1:
      if (paramAccionEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramAccionEvaluacion.descripcion;
      return;
    case 2:
      if (paramAccionEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.idAccionEvaluacion = paramAccionEvaluacion.idAccionEvaluacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AccionEvaluacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AccionEvaluacion localAccionEvaluacion = (AccionEvaluacion)paramObject;
    if (localAccionEvaluacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAccionEvaluacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AccionEvaluacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AccionEvaluacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    AccionEvaluacionPK localAccionEvaluacionPK = (AccionEvaluacionPK)paramObject;
    localAccionEvaluacionPK.idAccionEvaluacion = this.idAccionEvaluacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    AccionEvaluacionPK localAccionEvaluacionPK = (AccionEvaluacionPK)paramObject;
    this.idAccionEvaluacion = localAccionEvaluacionPK.idAccionEvaluacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    AccionEvaluacionPK localAccionEvaluacionPK = (AccionEvaluacionPK)paramObject;
    localAccionEvaluacionPK.idAccionEvaluacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    AccionEvaluacionPK localAccionEvaluacionPK = (AccionEvaluacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localAccionEvaluacionPK.idAccionEvaluacion);
  }

  private static final String jdoGetcodAccionEvaluacion(AccionEvaluacion paramAccionEvaluacion)
  {
    if (paramAccionEvaluacion.jdoFlags <= 0)
      return paramAccionEvaluacion.codAccionEvaluacion;
    StateManager localStateManager = paramAccionEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEvaluacion.codAccionEvaluacion;
    if (localStateManager.isLoaded(paramAccionEvaluacion, jdoInheritedFieldCount + 0))
      return paramAccionEvaluacion.codAccionEvaluacion;
    return localStateManager.getStringField(paramAccionEvaluacion, jdoInheritedFieldCount + 0, paramAccionEvaluacion.codAccionEvaluacion);
  }

  private static final void jdoSetcodAccionEvaluacion(AccionEvaluacion paramAccionEvaluacion, String paramString)
  {
    if (paramAccionEvaluacion.jdoFlags == 0)
    {
      paramAccionEvaluacion.codAccionEvaluacion = paramString;
      return;
    }
    StateManager localStateManager = paramAccionEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEvaluacion.codAccionEvaluacion = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionEvaluacion, jdoInheritedFieldCount + 0, paramAccionEvaluacion.codAccionEvaluacion, paramString);
  }

  private static final String jdoGetdescripcion(AccionEvaluacion paramAccionEvaluacion)
  {
    if (paramAccionEvaluacion.jdoFlags <= 0)
      return paramAccionEvaluacion.descripcion;
    StateManager localStateManager = paramAccionEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramAccionEvaluacion.descripcion;
    if (localStateManager.isLoaded(paramAccionEvaluacion, jdoInheritedFieldCount + 1))
      return paramAccionEvaluacion.descripcion;
    return localStateManager.getStringField(paramAccionEvaluacion, jdoInheritedFieldCount + 1, paramAccionEvaluacion.descripcion);
  }

  private static final void jdoSetdescripcion(AccionEvaluacion paramAccionEvaluacion, String paramString)
  {
    if (paramAccionEvaluacion.jdoFlags == 0)
    {
      paramAccionEvaluacion.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramAccionEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEvaluacion.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionEvaluacion, jdoInheritedFieldCount + 1, paramAccionEvaluacion.descripcion, paramString);
  }

  private static final long jdoGetidAccionEvaluacion(AccionEvaluacion paramAccionEvaluacion)
  {
    return paramAccionEvaluacion.idAccionEvaluacion;
  }

  private static final void jdoSetidAccionEvaluacion(AccionEvaluacion paramAccionEvaluacion, long paramLong)
  {
    StateManager localStateManager = paramAccionEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionEvaluacion.idAccionEvaluacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramAccionEvaluacion, jdoInheritedFieldCount + 2, paramAccionEvaluacion.idAccionEvaluacion, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}