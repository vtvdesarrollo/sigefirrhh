package sigefirrhh.planificacion.evaluacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.expediente.Personal;

public class NoEvaluacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_MOTIVO;
  private long idNoEvaluacion;
  private int anio;
  private int mes;
  private String motivo;
  private int cedulaSupervisor;
  private String nombreSupervisor;
  private String observaciones;
  private Personal personal;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "cedulaSupervisor", "idNoEvaluacion", "mes", "motivo", "nombreSupervisor", "observaciones", "personal", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.NoEvaluacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new NoEvaluacion());

    LISTA_MOTIVO = 
      new LinkedHashMap();

    LISTA_MOTIVO.put("1", "REPOSO");
    LISTA_MOTIVO.put("2", "PERMISO");
    LISTA_MOTIVO.put("3", "VACACIONES");
    LISTA_MOTIVO.put("4", "OTROS");
  }

  public NoEvaluacion()
  {
    jdoSetcedulaSupervisor(this, 0);
  }

  public String toString()
  {
    return jdoGetpersonal(this).toString();
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public int getCedulaSupervisor()
  {
    return jdoGetcedulaSupervisor(this);
  }

  public void setCedulaSupervisor(int cedulaSupervisor)
  {
    jdoSetcedulaSupervisor(this, cedulaSupervisor);
  }

  public long getIdNoEvaluacion()
  {
    return jdoGetidNoEvaluacion(this);
  }

  public void setIdNoEvaluacion(long idNoEvaluacion)
  {
    jdoSetidNoEvaluacion(this, idNoEvaluacion);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int mes)
  {
    jdoSetmes(this, mes);
  }

  public String getMotivo()
  {
    return jdoGetmotivo(this);
  }

  public void setMotivo(String motivo)
  {
    jdoSetmotivo(this, motivo);
  }

  public String getNombreSupervisor()
  {
    return jdoGetnombreSupervisor(this);
  }

  public void setNombreSupervisor(String nombreSupervisor)
  {
    jdoSetnombreSupervisor(this, nombreSupervisor);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal)
  {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    NoEvaluacion localNoEvaluacion = new NoEvaluacion();
    localNoEvaluacion.jdoFlags = 1;
    localNoEvaluacion.jdoStateManager = paramStateManager;
    return localNoEvaluacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    NoEvaluacion localNoEvaluacion = new NoEvaluacion();
    localNoEvaluacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localNoEvaluacion.jdoFlags = 1;
    localNoEvaluacion.jdoStateManager = paramStateManager;
    return localNoEvaluacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaSupervisor);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idNoEvaluacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.motivo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSupervisor);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaSupervisor = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idNoEvaluacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.motivo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSupervisor = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(NoEvaluacion paramNoEvaluacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramNoEvaluacion.anio;
      return;
    case 1:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaSupervisor = paramNoEvaluacion.cedulaSupervisor;
      return;
    case 2:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.idNoEvaluacion = paramNoEvaluacion.idNoEvaluacion;
      return;
    case 3:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramNoEvaluacion.mes;
      return;
    case 4:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.motivo = paramNoEvaluacion.motivo;
      return;
    case 5:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSupervisor = paramNoEvaluacion.nombreSupervisor;
      return;
    case 6:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramNoEvaluacion.observaciones;
      return;
    case 7:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramNoEvaluacion.personal;
      return;
    case 8:
      if (paramNoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramNoEvaluacion.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof NoEvaluacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    NoEvaluacion localNoEvaluacion = (NoEvaluacion)paramObject;
    if (localNoEvaluacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localNoEvaluacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new NoEvaluacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new NoEvaluacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NoEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    NoEvaluacionPK localNoEvaluacionPK = (NoEvaluacionPK)paramObject;
    localNoEvaluacionPK.idNoEvaluacion = this.idNoEvaluacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof NoEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    NoEvaluacionPK localNoEvaluacionPK = (NoEvaluacionPK)paramObject;
    this.idNoEvaluacion = localNoEvaluacionPK.idNoEvaluacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NoEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    NoEvaluacionPK localNoEvaluacionPK = (NoEvaluacionPK)paramObject;
    localNoEvaluacionPK.idNoEvaluacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof NoEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    NoEvaluacionPK localNoEvaluacionPK = (NoEvaluacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localNoEvaluacionPK.idNoEvaluacion);
  }

  private static final int jdoGetanio(NoEvaluacion paramNoEvaluacion)
  {
    if (paramNoEvaluacion.jdoFlags <= 0)
      return paramNoEvaluacion.anio;
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.anio;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 0))
      return paramNoEvaluacion.anio;
    return localStateManager.getIntField(paramNoEvaluacion, jdoInheritedFieldCount + 0, paramNoEvaluacion.anio);
  }

  private static final void jdoSetanio(NoEvaluacion paramNoEvaluacion, int paramInt)
  {
    if (paramNoEvaluacion.jdoFlags == 0)
    {
      paramNoEvaluacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramNoEvaluacion, jdoInheritedFieldCount + 0, paramNoEvaluacion.anio, paramInt);
  }

  private static final int jdoGetcedulaSupervisor(NoEvaluacion paramNoEvaluacion)
  {
    if (paramNoEvaluacion.jdoFlags <= 0)
      return paramNoEvaluacion.cedulaSupervisor;
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.cedulaSupervisor;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 1))
      return paramNoEvaluacion.cedulaSupervisor;
    return localStateManager.getIntField(paramNoEvaluacion, jdoInheritedFieldCount + 1, paramNoEvaluacion.cedulaSupervisor);
  }

  private static final void jdoSetcedulaSupervisor(NoEvaluacion paramNoEvaluacion, int paramInt)
  {
    if (paramNoEvaluacion.jdoFlags == 0)
    {
      paramNoEvaluacion.cedulaSupervisor = paramInt;
      return;
    }
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.cedulaSupervisor = paramInt;
      return;
    }
    localStateManager.setIntField(paramNoEvaluacion, jdoInheritedFieldCount + 1, paramNoEvaluacion.cedulaSupervisor, paramInt);
  }

  private static final long jdoGetidNoEvaluacion(NoEvaluacion paramNoEvaluacion)
  {
    return paramNoEvaluacion.idNoEvaluacion;
  }

  private static final void jdoSetidNoEvaluacion(NoEvaluacion paramNoEvaluacion, long paramLong)
  {
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.idNoEvaluacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramNoEvaluacion, jdoInheritedFieldCount + 2, paramNoEvaluacion.idNoEvaluacion, paramLong);
  }

  private static final int jdoGetmes(NoEvaluacion paramNoEvaluacion)
  {
    if (paramNoEvaluacion.jdoFlags <= 0)
      return paramNoEvaluacion.mes;
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.mes;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 3))
      return paramNoEvaluacion.mes;
    return localStateManager.getIntField(paramNoEvaluacion, jdoInheritedFieldCount + 3, paramNoEvaluacion.mes);
  }

  private static final void jdoSetmes(NoEvaluacion paramNoEvaluacion, int paramInt)
  {
    if (paramNoEvaluacion.jdoFlags == 0)
    {
      paramNoEvaluacion.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramNoEvaluacion, jdoInheritedFieldCount + 3, paramNoEvaluacion.mes, paramInt);
  }

  private static final String jdoGetmotivo(NoEvaluacion paramNoEvaluacion)
  {
    if (paramNoEvaluacion.jdoFlags <= 0)
      return paramNoEvaluacion.motivo;
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.motivo;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 4))
      return paramNoEvaluacion.motivo;
    return localStateManager.getStringField(paramNoEvaluacion, jdoInheritedFieldCount + 4, paramNoEvaluacion.motivo);
  }

  private static final void jdoSetmotivo(NoEvaluacion paramNoEvaluacion, String paramString)
  {
    if (paramNoEvaluacion.jdoFlags == 0)
    {
      paramNoEvaluacion.motivo = paramString;
      return;
    }
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.motivo = paramString;
      return;
    }
    localStateManager.setStringField(paramNoEvaluacion, jdoInheritedFieldCount + 4, paramNoEvaluacion.motivo, paramString);
  }

  private static final String jdoGetnombreSupervisor(NoEvaluacion paramNoEvaluacion)
  {
    if (paramNoEvaluacion.jdoFlags <= 0)
      return paramNoEvaluacion.nombreSupervisor;
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.nombreSupervisor;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 5))
      return paramNoEvaluacion.nombreSupervisor;
    return localStateManager.getStringField(paramNoEvaluacion, jdoInheritedFieldCount + 5, paramNoEvaluacion.nombreSupervisor);
  }

  private static final void jdoSetnombreSupervisor(NoEvaluacion paramNoEvaluacion, String paramString)
  {
    if (paramNoEvaluacion.jdoFlags == 0)
    {
      paramNoEvaluacion.nombreSupervisor = paramString;
      return;
    }
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.nombreSupervisor = paramString;
      return;
    }
    localStateManager.setStringField(paramNoEvaluacion, jdoInheritedFieldCount + 5, paramNoEvaluacion.nombreSupervisor, paramString);
  }

  private static final String jdoGetobservaciones(NoEvaluacion paramNoEvaluacion)
  {
    if (paramNoEvaluacion.jdoFlags <= 0)
      return paramNoEvaluacion.observaciones;
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.observaciones;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 6))
      return paramNoEvaluacion.observaciones;
    return localStateManager.getStringField(paramNoEvaluacion, jdoInheritedFieldCount + 6, paramNoEvaluacion.observaciones);
  }

  private static final void jdoSetobservaciones(NoEvaluacion paramNoEvaluacion, String paramString)
  {
    if (paramNoEvaluacion.jdoFlags == 0)
    {
      paramNoEvaluacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramNoEvaluacion, jdoInheritedFieldCount + 6, paramNoEvaluacion.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(NoEvaluacion paramNoEvaluacion)
  {
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.personal;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 7))
      return paramNoEvaluacion.personal;
    return (Personal)localStateManager.getObjectField(paramNoEvaluacion, jdoInheritedFieldCount + 7, paramNoEvaluacion.personal);
  }

  private static final void jdoSetpersonal(NoEvaluacion paramNoEvaluacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramNoEvaluacion, jdoInheritedFieldCount + 7, paramNoEvaluacion.personal, paramPersonal);
  }

  private static final TipoPersonal jdoGettipoPersonal(NoEvaluacion paramNoEvaluacion)
  {
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramNoEvaluacion.tipoPersonal;
    if (localStateManager.isLoaded(paramNoEvaluacion, jdoInheritedFieldCount + 8))
      return paramNoEvaluacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramNoEvaluacion, jdoInheritedFieldCount + 8, paramNoEvaluacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(NoEvaluacion paramNoEvaluacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramNoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramNoEvaluacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramNoEvaluacion, jdoInheritedFieldCount + 8, paramNoEvaluacion.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}