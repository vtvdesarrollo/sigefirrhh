package sigefirrhh.planificacion.evaluacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class AccionEvaluacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(AccionEvaluacionForm.class.getName());
  private AccionEvaluacion accionEvaluacion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EvaluacionFacade evaluacionFacade = new EvaluacionFacade();
  private boolean showAccionEvaluacionByCodAccionEvaluacion;
  private boolean showAccionEvaluacionByDescripcion;
  private String findCodAccionEvaluacion;
  private String findDescripcion;
  private Object stateResultAccionEvaluacionByCodAccionEvaluacion = null;

  private Object stateResultAccionEvaluacionByDescripcion = null;

  public String getFindCodAccionEvaluacion()
  {
    return this.findCodAccionEvaluacion;
  }
  public void setFindCodAccionEvaluacion(String findCodAccionEvaluacion) {
    this.findCodAccionEvaluacion = findCodAccionEvaluacion;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public AccionEvaluacion getAccionEvaluacion() {
    if (this.accionEvaluacion == null) {
      this.accionEvaluacion = new AccionEvaluacion();
    }
    return this.accionEvaluacion;
  }

  public AccionEvaluacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findAccionEvaluacionByCodAccionEvaluacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.evaluacionFacade.findAccionEvaluacionByCodAccionEvaluacion(this.findCodAccionEvaluacion);
      this.showAccionEvaluacionByCodAccionEvaluacion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAccionEvaluacionByCodAccionEvaluacion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodAccionEvaluacion = null;
    this.findDescripcion = null;

    return null;
  }

  public String findAccionEvaluacionByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.evaluacionFacade.findAccionEvaluacionByDescripcion(this.findDescripcion);
      this.showAccionEvaluacionByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showAccionEvaluacionByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodAccionEvaluacion = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowAccionEvaluacionByCodAccionEvaluacion() {
    return this.showAccionEvaluacionByCodAccionEvaluacion;
  }
  public boolean isShowAccionEvaluacionByDescripcion() {
    return this.showAccionEvaluacionByDescripcion;
  }

  public String selectAccionEvaluacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idAccionEvaluacion = 
      Long.parseLong((String)requestParameterMap.get("idAccionEvaluacion"));
    try
    {
      this.accionEvaluacion = 
        this.evaluacionFacade.findAccionEvaluacionById(
        idAccionEvaluacion);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.accionEvaluacion = null;
    this.showAccionEvaluacionByCodAccionEvaluacion = false;
    this.showAccionEvaluacionByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.evaluacionFacade.addAccionEvaluacion(
          this.accionEvaluacion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.evaluacionFacade.updateAccionEvaluacion(
          this.accionEvaluacion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.evaluacionFacade.deleteAccionEvaluacion(
        this.accionEvaluacion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.accionEvaluacion = new AccionEvaluacion();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.accionEvaluacion.setIdAccionEvaluacion(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.evaluacion.AccionEvaluacion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.accionEvaluacion = new AccionEvaluacion();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}