package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ConceptoEvaluacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConceptoEvaluacion conceptoEvaluacionNew = 
      (ConceptoEvaluacion)BeanUtils.cloneBean(
      conceptoEvaluacion);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoEvaluacionNew.getTipoPersonal() != null) {
      conceptoEvaluacionNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoEvaluacionNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoEvaluacionNew.getConceptoTipoPersonal() != null) {
      conceptoEvaluacionNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoEvaluacionNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(conceptoEvaluacionNew);
  }

  public void updateConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion) throws Exception
  {
    ConceptoEvaluacion conceptoEvaluacionModify = 
      findConceptoEvaluacionById(conceptoEvaluacion.getIdConceptoEvaluacion());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (conceptoEvaluacion.getTipoPersonal() != null) {
      conceptoEvaluacion.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        conceptoEvaluacion.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (conceptoEvaluacion.getConceptoTipoPersonal() != null) {
      conceptoEvaluacion.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        conceptoEvaluacion.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(conceptoEvaluacionModify, conceptoEvaluacion);
  }

  public void deleteConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConceptoEvaluacion conceptoEvaluacionDelete = 
      findConceptoEvaluacionById(conceptoEvaluacion.getIdConceptoEvaluacion());
    pm.deletePersistent(conceptoEvaluacionDelete);
  }

  public ConceptoEvaluacion findConceptoEvaluacionById(long idConceptoEvaluacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConceptoEvaluacion == pIdConceptoEvaluacion";
    Query query = pm.newQuery(ConceptoEvaluacion.class, filter);

    query.declareParameters("long pIdConceptoEvaluacion");

    parameters.put("pIdConceptoEvaluacion", new Long(idConceptoEvaluacion));

    Collection colConceptoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConceptoEvaluacion.iterator();
    return (ConceptoEvaluacion)iterator.next();
  }

  public Collection findConceptoEvaluacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent conceptoEvaluacionExtent = pm.getExtent(
      ConceptoEvaluacion.class, true);
    Query query = pm.newQuery(conceptoEvaluacionExtent);
    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ConceptoEvaluacion.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("conceptoTipoPersonal.concepto.codConcepto ascending");

    Collection colConceptoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConceptoEvaluacion);

    return colConceptoEvaluacion;
  }
}