package sigefirrhh.planificacion.evaluacion;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ActualizarEvaluacionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarEvaluacionForm.class.getName());

  protected static final Map LISTA_NIVEL_EMPLEADO = new LinkedHashMap();

  protected static final Map LISTA_NIVEL_OBRERO = new LinkedHashMap();
  private Evaluacion evaluacion_update;
  private Evaluacion evaluacion;
  private Collection result;
  private long idTipoPersonal;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private Connection connection = Resource.getConnection();

  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private EvaluacionNoGenFacade evaluacionFacade = new EvaluacionNoGenFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private EstructuraNoGenFacade estructuraFacade = new EstructuraNoGenFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private boolean showEmpleados;
  private boolean showObreros;
  private ResultadoEvaluacion resultadoEvaluacionAnterior;
  Statement stExecute = null;
  StringBuffer sql = new StringBuffer();
  private int anio;
  private int mes;
  private int cedula;
  private float competencias = 0.0F;
  private float objetivos = 0.0F;

  private double manejoBienes = 0.0D;
  private double habitoSeguridad = 0.0D;
  private double calidadTrabajo = 0.0D;
  private double cumplimientoNormas = 0.0D;
  private double atencionPublico = 0.0D;
  private double interesTrabajo = 0.0D;
  private double cooperacion = 0.0D;
  private double cantidadTrabajo = 0.0D;
  private double tomaDecisiones = 0.0D;
  private double comunicacion = 0.0D;
  private double capacidadMando = 0.0D;
  private double coordinacion = 0.0D;
  private int cedulaSupervisor;
  private String nombreSupervisor;
  private int cedulaJefe;
  private String nombreJefe;
  private String nivelEmpleado;
  private String nivelObrero;
  private int cedulaFinal;
  private String nombre;
  private String apellido;
  private ResultSet rsRegistroTrabajador = null;
  private PreparedStatement stRegistroTrabajador = null;

  private ResultSet rsRegistroEvaluacion = null;
  private PreparedStatement stRegistroEvaluacion = null;

  private Object stateResultPersonal = null;

  private Object stateResultEvaluacionByPersonal = null;

  static
  {
    LISTA_NIVEL_EMPLEADO.put("1", "ADMINISTRATIVO");
    LISTA_NIVEL_EMPLEADO.put("2", "TECNICO PROFESIONAL");
    LISTA_NIVEL_EMPLEADO.put("3", "SUPERVISORIO");
    LISTA_NIVEL_OBRERO.put("1", "CALIFICADO/NO CALIFICADO");
    LISTA_NIVEL_OBRERO.put("2", "SUPERVISORIO");
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Evaluacion getEvaluacion() {
    if (this.evaluacion == null) {
      this.evaluacion = new Evaluacion();
    }
    return this.evaluacion;
  }

  public void setEvaluacion(Evaluacion evaluacion) {
    this.evaluacion = evaluacion;
  }

  public ActualizarEvaluacionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.connection.setAutoCommit(true);
      this.connection = Resource.getConnection();

      this.sql = new StringBuffer();
      this.sql.append("select p.id_personal, p.primer_nombre, p.primer_apellido, t.id_tipo_personal, ");
      this.sql.append(" d.id_dependencia, d.nombre as nombre_dependencia, ");
      this.sql.append(" c.id_cargo, c.descripcion_cargo as nombre_cargo ");
      this.sql.append(" from personal p, trabajador t, dependencia d, cargo c ");
      this.sql.append(" where p.cedula = ? ");
      this.sql.append(" and p.id_personal = t.id_personal ");
      this.sql.append(" and t.id_tipo_personal = ? ");
      this.sql.append(" and t.id_dependencia = d.id_dependencia");
      this.sql.append(" and t.id_cargo = c.id_cargo");
      this.stRegistroTrabajador = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());

      this.sql = new StringBuffer();
      this.sql.append("select * ");
      this.sql.append(" from evaluacion e ");
      this.sql.append(" where  e.id_personal = ? ");
      this.sql.append(" and  e.id_tipo_personal = ? ");
      this.sql.append(" and  e.anio = ? ");
      this.sql.append(" and  e.mes = ? ");

      this.stRegistroEvaluacion = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      return;
    }

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public void refresh()
  {
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findEvaluacionByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.evaluacionFacade.findEvaluacionByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectEvaluacion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idEvaluacionEmpleado = 
      Long.parseLong((String)requestParameterMap.get("idEvaluacion"));
    try
    {
      this.evaluacion = 
        this.evaluacionFacade.findEvaluacionById(idEvaluacionEmpleado);
      this.showObreros = false;
      this.showEmpleados = false;
      this.showObreros = this.evaluacion.getTipoPersonal().getGrupoNomina().getPeriodicidad().equalsIgnoreCase("S");
      this.showEmpleados = (!this.showObreros);
      edit();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  public String save()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    if ((this.evaluacion.getResultadoCompetencias() > 250.0F) || (this.evaluacion.getResultadoObjetivos() > 250.0F)) {
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Puntaje mayor a 250", ""));
      return null;
    }
    this.evaluacionFacade.updateEvaluacion(this.evaluacion);

    this.evaluacion_update = this.evaluacionFacade.findEvaluacionById(this.evaluacion.getIdEvaluacion());

    context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));

    log.error("3");

    return "true";
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;
    this.resultadoEvaluacionAnterior = this.evaluacion.getResultadoEvaluacion();

    return null;
  }

  public String delete()
  {
    this.deleting = true;

    return null;
  }

  public String deleteOk()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.evaluacionFacade.deleteEvaluacion(
        this.evaluacion);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.evaluacion);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.evaluacion = new Evaluacion();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.evaluacion = new Evaluacion();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public boolean isShowEmpleados() {
    return this.showEmpleados;
  }

  public boolean isShowObreros() {
    return this.showObreros;
  }

  public float getCompetencias() {
    this.competencias = this.evaluacion.getResultadoCompetencias();
    return this.competencias;
  }
  public void setCompetencias(float competencias) {
    this.competencias = competencias;
    this.evaluacion.setResultadoCompetencias(this.competencias);
  }
  public float getObjetivos() {
    this.objetivos = this.evaluacion.getResultadoObjetivos();
    return this.objetivos;
  }
  public void setObjetivos(float objetivos) {
    this.objetivos = objetivos;
    this.evaluacion.setResultadoObjetivos(this.objetivos);
  }
}