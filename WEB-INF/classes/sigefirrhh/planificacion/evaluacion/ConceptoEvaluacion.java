package sigefirrhh.planificacion.evaluacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;

public class ConceptoEvaluacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idConceptoEvaluacion;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private String eliminarDespuesAumento;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "conceptoTipoPersonal", "eliminarDespuesAumento", "idConceptoEvaluacion", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 26, 21, 24, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.ConceptoEvaluacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConceptoEvaluacion());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public ConceptoEvaluacion()
  {
    jdoSeteliminarDespuesAumento(this, "N");
  }

  public String toString()
  {
    return jdoGetconceptoTipoPersonal(this).getConcepto().getDescripcion();
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public long getIdConceptoEvaluacion() {
    return jdoGetidConceptoEvaluacion(this);
  }
  public void setIdConceptoEvaluacion(long idConceptoEvaluacion) {
    jdoSetidConceptoEvaluacion(this, idConceptoEvaluacion);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public String getEliminarDespuesAumento() {
    return jdoGeteliminarDespuesAumento(this);
  }
  public void setEliminarDespuesAumento(String eliminarDespuesAumento) {
    jdoSeteliminarDespuesAumento(this, eliminarDespuesAumento);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConceptoEvaluacion localConceptoEvaluacion = new ConceptoEvaluacion();
    localConceptoEvaluacion.jdoFlags = 1;
    localConceptoEvaluacion.jdoStateManager = paramStateManager;
    return localConceptoEvaluacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConceptoEvaluacion localConceptoEvaluacion = new ConceptoEvaluacion();
    localConceptoEvaluacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConceptoEvaluacion.jdoFlags = 1;
    localConceptoEvaluacion.jdoStateManager = paramStateManager;
    return localConceptoEvaluacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.eliminarDespuesAumento);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConceptoEvaluacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.eliminarDespuesAumento = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConceptoEvaluacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConceptoEvaluacion paramConceptoEvaluacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConceptoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramConceptoEvaluacion.conceptoTipoPersonal;
      return;
    case 1:
      if (paramConceptoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.eliminarDespuesAumento = paramConceptoEvaluacion.eliminarDespuesAumento;
      return;
    case 2:
      if (paramConceptoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.idConceptoEvaluacion = paramConceptoEvaluacion.idConceptoEvaluacion;
      return;
    case 3:
      if (paramConceptoEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramConceptoEvaluacion.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConceptoEvaluacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConceptoEvaluacion localConceptoEvaluacion = (ConceptoEvaluacion)paramObject;
    if (localConceptoEvaluacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConceptoEvaluacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConceptoEvaluacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConceptoEvaluacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    ConceptoEvaluacionPK localConceptoEvaluacionPK = (ConceptoEvaluacionPK)paramObject;
    localConceptoEvaluacionPK.idConceptoEvaluacion = this.idConceptoEvaluacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConceptoEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    ConceptoEvaluacionPK localConceptoEvaluacionPK = (ConceptoEvaluacionPK)paramObject;
    this.idConceptoEvaluacion = localConceptoEvaluacionPK.idConceptoEvaluacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    ConceptoEvaluacionPK localConceptoEvaluacionPK = (ConceptoEvaluacionPK)paramObject;
    localConceptoEvaluacionPK.idConceptoEvaluacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConceptoEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    ConceptoEvaluacionPK localConceptoEvaluacionPK = (ConceptoEvaluacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localConceptoEvaluacionPK.idConceptoEvaluacion);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(ConceptoEvaluacion paramConceptoEvaluacion)
  {
    StateManager localStateManager = paramConceptoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoEvaluacion.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramConceptoEvaluacion, jdoInheritedFieldCount + 0))
      return paramConceptoEvaluacion.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramConceptoEvaluacion, jdoInheritedFieldCount + 0, paramConceptoEvaluacion.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(ConceptoEvaluacion paramConceptoEvaluacion, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramConceptoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEvaluacion.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoEvaluacion, jdoInheritedFieldCount + 0, paramConceptoEvaluacion.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGeteliminarDespuesAumento(ConceptoEvaluacion paramConceptoEvaluacion)
  {
    if (paramConceptoEvaluacion.jdoFlags <= 0)
      return paramConceptoEvaluacion.eliminarDespuesAumento;
    StateManager localStateManager = paramConceptoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoEvaluacion.eliminarDespuesAumento;
    if (localStateManager.isLoaded(paramConceptoEvaluacion, jdoInheritedFieldCount + 1))
      return paramConceptoEvaluacion.eliminarDespuesAumento;
    return localStateManager.getStringField(paramConceptoEvaluacion, jdoInheritedFieldCount + 1, paramConceptoEvaluacion.eliminarDespuesAumento);
  }

  private static final void jdoSeteliminarDespuesAumento(ConceptoEvaluacion paramConceptoEvaluacion, String paramString)
  {
    if (paramConceptoEvaluacion.jdoFlags == 0)
    {
      paramConceptoEvaluacion.eliminarDespuesAumento = paramString;
      return;
    }
    StateManager localStateManager = paramConceptoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEvaluacion.eliminarDespuesAumento = paramString;
      return;
    }
    localStateManager.setStringField(paramConceptoEvaluacion, jdoInheritedFieldCount + 1, paramConceptoEvaluacion.eliminarDespuesAumento, paramString);
  }

  private static final long jdoGetidConceptoEvaluacion(ConceptoEvaluacion paramConceptoEvaluacion)
  {
    return paramConceptoEvaluacion.idConceptoEvaluacion;
  }

  private static final void jdoSetidConceptoEvaluacion(ConceptoEvaluacion paramConceptoEvaluacion, long paramLong)
  {
    StateManager localStateManager = paramConceptoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEvaluacion.idConceptoEvaluacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramConceptoEvaluacion, jdoInheritedFieldCount + 2, paramConceptoEvaluacion.idConceptoEvaluacion, paramLong);
  }

  private static final TipoPersonal jdoGettipoPersonal(ConceptoEvaluacion paramConceptoEvaluacion)
  {
    StateManager localStateManager = paramConceptoEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramConceptoEvaluacion.tipoPersonal;
    if (localStateManager.isLoaded(paramConceptoEvaluacion, jdoInheritedFieldCount + 3))
      return paramConceptoEvaluacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramConceptoEvaluacion, jdoInheritedFieldCount + 3, paramConceptoEvaluacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(ConceptoEvaluacion paramConceptoEvaluacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramConceptoEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramConceptoEvaluacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramConceptoEvaluacion, jdoInheritedFieldCount + 3, paramConceptoEvaluacion.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}