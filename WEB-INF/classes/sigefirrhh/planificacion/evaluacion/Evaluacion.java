package sigefirrhh.planificacion.evaluacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.personal.expediente.Personal;

public class Evaluacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_NIVEL_EMPLEADO;
  protected static final Map LISTA_NIVEL_OBRERO;
  private long idEvaluacion;
  private int anio;
  private int mes;
  private float resultadoCompetencias;
  private float resultadoObjetivos;
  private float manejoBienes;
  private float habitoSeguridad;
  private float calidadTrabajo;
  private float cumplimientoNormas;
  private float atencionPublico;
  private float interesTrabajo;
  private float cooperacion;
  private float cantidadTrabajo;
  private float tomaDecisiones;
  private float comunicacion;
  private float capacidadMando;
  private float coordinacion;
  private String nivel;
  private ResultadoEvaluacion resultadoEvaluacion;
  private AccionEvaluacion accionEvaluacion;
  private int cedulaSupervisor;
  private String nombreSupervisor;
  private int cedulaJefe;
  private String nombreJefe;
  private double porcentajeAumento;
  private int numeroPasos;
  private double montoAumentar;
  private double montoUnico;
  private String observaciones;
  private Personal personal;
  private TipoPersonal tipoPersonal;
  private Cargo cargo;
  private Dependencia dependencia;
  private String nombreCargo;
  private String nombreDependencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "accionEvaluacion", "anio", "atencionPublico", "calidadTrabajo", "cantidadTrabajo", "capacidadMando", "cargo", "cedulaJefe", "cedulaSupervisor", "comunicacion", "cooperacion", "coordinacion", "cumplimientoNormas", "dependencia", "habitoSeguridad", "idEvaluacion", "interesTrabajo", "manejoBienes", "mes", "montoAumentar", "montoUnico", "nivel", "nombreCargo", "nombreDependencia", "nombreJefe", "nombreSupervisor", "numeroPasos", "observaciones", "personal", "porcentajeAumento", "resultadoCompetencias", "resultadoEvaluacion", "resultadoObjetivos", "tipoPersonal", "tomaDecisiones" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.AccionEvaluacion"), Integer.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Integer.TYPE, Integer.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, Float.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Float.TYPE, Long.TYPE, Float.TYPE, Float.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Double.TYPE, Float.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.ResultadoEvaluacion"), Float.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Float.TYPE }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 21, 21, 26, 21, 21, 21, 21, 21, 21, 26, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 26, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.Evaluacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Evaluacion());

    LISTA_NIVEL_EMPLEADO = 
      new LinkedHashMap();
    LISTA_NIVEL_OBRERO = 
      new LinkedHashMap();

    LISTA_NIVEL_EMPLEADO.put("1", "ADMINISTRATIVO");
    LISTA_NIVEL_EMPLEADO.put("2", "TECNICO PROFESIONAL");
    LISTA_NIVEL_EMPLEADO.put("3", "SUPERVISOR");
    LISTA_NIVEL_OBRERO.put("1", "CALIFICADO/NO CALIFICADO");
    LISTA_NIVEL_OBRERO.put("2", "SUPERVISOR");
  }

  public Evaluacion()
  {
    jdoSetresultadoCompetencias(this, 0.0F);

    jdoSetresultadoObjetivos(this, 0.0F);

    jdoSetmanejoBienes(this, 0.0F);

    jdoSethabitoSeguridad(this, 0.0F);

    jdoSetcalidadTrabajo(this, 0.0F);

    jdoSetcumplimientoNormas(this, 0.0F);

    jdoSetatencionPublico(this, 0.0F);

    jdoSetinteresTrabajo(this, 0.0F);

    jdoSetcooperacion(this, 0.0F);

    jdoSetcantidadTrabajo(this, 0.0F);

    jdoSettomaDecisiones(this, 0.0F);

    jdoSetcomunicacion(this, 0.0F);

    jdoSetcapacidadMando(this, 0.0F);

    jdoSetcoordinacion(this, 0.0F);

    jdoSetcedulaSupervisor(this, 0);

    jdoSetcedulaJefe(this, 0);

    jdoSetporcentajeAumento(this, 0.0D);

    jdoSetnumeroPasos(this, 0);

    jdoSetmontoAumentar(this, 0.0D);

    jdoSetmontoUnico(this, 0.0D);
  }

  public String toString()
  {
    double total = jdoGetresultadoCompetencias(this) + jdoGetresultadoObjetivos(this);
    return jdoGetanio(this) + " - " + jdoGetmes(this) + " - " + total + " - " + jdoGetresultadoEvaluacion(this).getDescripcion();
  }

  public AccionEvaluacion getAccionEvaluacion() {
    return jdoGetaccionEvaluacion(this);
  }
  public void setAccionEvaluacion(AccionEvaluacion accionEvaluacion) {
    jdoSetaccionEvaluacion(this, accionEvaluacion);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public Cargo getCargo() {
    return jdoGetcargo(this);
  }
  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }
  public int getCedulaJefe() {
    return jdoGetcedulaJefe(this);
  }
  public void setCedulaJefe(int cedulaJefe) {
    jdoSetcedulaJefe(this, cedulaJefe);
  }
  public int getCedulaSupervisor() {
    return jdoGetcedulaSupervisor(this);
  }
  public void setCedulaSupervisor(int cedulaSupervisor) {
    jdoSetcedulaSupervisor(this, cedulaSupervisor);
  }

  public Dependencia getDependencia() {
    return jdoGetdependencia(this);
  }
  public void setDependencia(Dependencia dependencia) {
    jdoSetdependencia(this, dependencia);
  }

  public long getIdEvaluacion() {
    return jdoGetidEvaluacion(this);
  }
  public void setIdEvaluacion(long idEvaluacion) {
    jdoSetidEvaluacion(this, idEvaluacion);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public String getNombreJefe() {
    return jdoGetnombreJefe(this);
  }
  public void setNombreJefe(String nombreJefe) {
    jdoSetnombreJefe(this, nombreJefe);
  }
  public String getNombreSupervisor() {
    return jdoGetnombreSupervisor(this);
  }
  public void setNombreSupervisor(String nombreSupervisor) {
    jdoSetnombreSupervisor(this, nombreSupervisor);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }
  public ResultadoEvaluacion getResultadoEvaluacion() {
    return jdoGetresultadoEvaluacion(this);
  }
  public void setResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion) {
    jdoSetresultadoEvaluacion(this, resultadoEvaluacion);
  }
  public double getMontoAumentar() {
    return jdoGetmontoAumentar(this);
  }
  public void setMontoAumentar(double montoAumentar) {
    jdoSetmontoAumentar(this, montoAumentar);
  }
  public double getMontoUnico() {
    return jdoGetmontoUnico(this);
  }
  public void setMontoUnico(double montoUnico) {
    jdoSetmontoUnico(this, montoUnico);
  }
  public int getNumeroPasos() {
    return jdoGetnumeroPasos(this);
  }
  public void setNumeroPasos(int numeroPasos) {
    jdoSetnumeroPasos(this, numeroPasos);
  }
  public double getPorcentajeAumento() {
    return jdoGetporcentajeAumento(this);
  }
  public void setPorcentajeAumento(double porcentajeAumento) {
    jdoSetporcentajeAumento(this, porcentajeAumento);
  }
  public String getNombreCargo() {
    return jdoGetnombreCargo(this);
  }
  public void setNombreCargo(String nombreCargo) {
    jdoSetnombreCargo(this, nombreCargo);
  }

  public String getNombreDependencia() {
    return jdoGetnombreDependencia(this);
  }
  public void setNombreDependencia(String nombreDependencia) {
    jdoSetnombreDependencia(this, nombreDependencia);
  }
  public float getResultadoCompetencias() {
    return jdoGetresultadoCompetencias(this);
  }
  public void setResultadoCompetencias(float resultadoCompetencias) {
    jdoSetresultadoCompetencias(this, resultadoCompetencias);
  }
  public float getResultadoObjetivos() {
    return jdoGetresultadoObjetivos(this);
  }
  public void setResultadoObjetivos(float resultadoObjetivos) {
    jdoSetresultadoObjetivos(this, resultadoObjetivos);
  }

  public float getAtencionPublico() {
    return jdoGetatencionPublico(this);
  }

  public void setAtencionPublico(float atencionPublico) {
    jdoSetatencionPublico(this, atencionPublico);
  }

  public float getCalidadTrabajo() {
    return jdoGetcalidadTrabajo(this);
  }

  public void setCalidadTrabajo(float calidadTrabajo) {
    jdoSetcalidadTrabajo(this, calidadTrabajo);
  }

  public float getCantidadTrabajo() {
    return jdoGetcantidadTrabajo(this);
  }

  public void setCantidadTrabajo(float cantidadTrabajo) {
    jdoSetcantidadTrabajo(this, cantidadTrabajo);
  }

  public float getCapacidadMando() {
    return jdoGetcapacidadMando(this);
  }

  public void setCapacidadMando(float capacidadMando) {
    jdoSetcapacidadMando(this, capacidadMando);
  }

  public float getComunicacion() {
    return jdoGetcomunicacion(this);
  }

  public void setComunicacion(float comunicacion) {
    jdoSetcomunicacion(this, comunicacion);
  }

  public float getCooperacion() {
    return jdoGetcooperacion(this);
  }

  public void setCooperacion(float cooperacion) {
    jdoSetcooperacion(this, cooperacion);
  }

  public float getCoordinacion() {
    return jdoGetcoordinacion(this);
  }

  public void setCoordinacion(float coordinacion) {
    jdoSetcoordinacion(this, coordinacion);
  }

  public float getCumplimientoNormas() {
    return jdoGetcumplimientoNormas(this);
  }

  public void setCumplimientoNormas(float cumplimientoNormas) {
    jdoSetcumplimientoNormas(this, cumplimientoNormas);
  }

  public float getHabitoSeguridad() {
    return jdoGethabitoSeguridad(this);
  }

  public void setHabitoSeguridad(float habitoSeguridad) {
    jdoSethabitoSeguridad(this, habitoSeguridad);
  }

  public float getInteresTrabajo() {
    return jdoGetinteresTrabajo(this);
  }

  public void setInteresTrabajo(float interesTrabajo) {
    jdoSetinteresTrabajo(this, interesTrabajo);
  }

  public float getManejoBienes() {
    return jdoGetmanejoBienes(this);
  }

  public void setManejoBienes(float manejoBienes) {
    jdoSetmanejoBienes(this, manejoBienes);
  }

  public float getTomaDecisiones() {
    return jdoGettomaDecisiones(this);
  }

  public void setTomaDecisiones(float tomaDecisiones) {
    jdoSettomaDecisiones(this, tomaDecisiones);
  }

  public String getNivel() {
    return jdoGetnivel(this);
  }

  public void setNivel(String nivel) {
    jdoSetnivel(this, nivel);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 35;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Evaluacion localEvaluacion = new Evaluacion();
    localEvaluacion.jdoFlags = 1;
    localEvaluacion.jdoStateManager = paramStateManager;
    return localEvaluacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Evaluacion localEvaluacion = new Evaluacion();
    localEvaluacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localEvaluacion.jdoFlags = 1;
    localEvaluacion.jdoStateManager = paramStateManager;
    return localEvaluacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.accionEvaluacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.atencionPublico);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.calidadTrabajo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.cantidadTrabajo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.capacidadMando);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaJefe);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaSupervisor);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.comunicacion);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.cooperacion);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.coordinacion);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.cumplimientoNormas);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.habitoSeguridad);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idEvaluacion);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.interesTrabajo);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.manejoBienes);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoAumentar);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoUnico);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivel);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreCargo);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDependencia);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreJefe);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSupervisor);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroPasos);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeAumento);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.resultadoCompetencias);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.resultadoEvaluacion);
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.resultadoObjetivos);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.tomaDecisiones);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accionEvaluacion = ((AccionEvaluacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.atencionPublico = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.calidadTrabajo = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadTrabajo = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.capacidadMando = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaJefe = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaSupervisor = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.comunicacion = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cooperacion = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.coordinacion = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cumplimientoNormas = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.habitoSeguridad = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idEvaluacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.interesTrabajo = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.manejoBienes = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoAumentar = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoUnico = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivel = localStateManager.replacingStringField(this, paramInt);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreJefe = localStateManager.replacingStringField(this, paramInt);
      return;
    case 25:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSupervisor = localStateManager.replacingStringField(this, paramInt);
      return;
    case 26:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroPasos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 27:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 28:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 29:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeAumento = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 30:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultadoCompetencias = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 31:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultadoEvaluacion = ((ResultadoEvaluacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 32:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultadoObjetivos = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 33:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 34:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tomaDecisiones = localStateManager.replacingFloatField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Evaluacion paramEvaluacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.accionEvaluacion = paramEvaluacion.accionEvaluacion;
      return;
    case 1:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramEvaluacion.anio;
      return;
    case 2:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.atencionPublico = paramEvaluacion.atencionPublico;
      return;
    case 3:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.calidadTrabajo = paramEvaluacion.calidadTrabajo;
      return;
    case 4:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadTrabajo = paramEvaluacion.cantidadTrabajo;
      return;
    case 5:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.capacidadMando = paramEvaluacion.capacidadMando;
      return;
    case 6:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramEvaluacion.cargo;
      return;
    case 7:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaJefe = paramEvaluacion.cedulaJefe;
      return;
    case 8:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaSupervisor = paramEvaluacion.cedulaSupervisor;
      return;
    case 9:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.comunicacion = paramEvaluacion.comunicacion;
      return;
    case 10:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.cooperacion = paramEvaluacion.cooperacion;
      return;
    case 11:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.coordinacion = paramEvaluacion.coordinacion;
      return;
    case 12:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.cumplimientoNormas = paramEvaluacion.cumplimientoNormas;
      return;
    case 13:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramEvaluacion.dependencia;
      return;
    case 14:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.habitoSeguridad = paramEvaluacion.habitoSeguridad;
      return;
    case 15:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.idEvaluacion = paramEvaluacion.idEvaluacion;
      return;
    case 16:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.interesTrabajo = paramEvaluacion.interesTrabajo;
      return;
    case 17:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.manejoBienes = paramEvaluacion.manejoBienes;
      return;
    case 18:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramEvaluacion.mes;
      return;
    case 19:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.montoAumentar = paramEvaluacion.montoAumentar;
      return;
    case 20:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.montoUnico = paramEvaluacion.montoUnico;
      return;
    case 21:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.nivel = paramEvaluacion.nivel;
      return;
    case 22:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreCargo = paramEvaluacion.nombreCargo;
      return;
    case 23:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDependencia = paramEvaluacion.nombreDependencia;
      return;
    case 24:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreJefe = paramEvaluacion.nombreJefe;
      return;
    case 25:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSupervisor = paramEvaluacion.nombreSupervisor;
      return;
    case 26:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.numeroPasos = paramEvaluacion.numeroPasos;
      return;
    case 27:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramEvaluacion.observaciones;
      return;
    case 28:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramEvaluacion.personal;
      return;
    case 29:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeAumento = paramEvaluacion.porcentajeAumento;
      return;
    case 30:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.resultadoCompetencias = paramEvaluacion.resultadoCompetencias;
      return;
    case 31:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.resultadoEvaluacion = paramEvaluacion.resultadoEvaluacion;
      return;
    case 32:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.resultadoObjetivos = paramEvaluacion.resultadoObjetivos;
      return;
    case 33:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramEvaluacion.tipoPersonal;
      return;
    case 34:
      if (paramEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.tomaDecisiones = paramEvaluacion.tomaDecisiones;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Evaluacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Evaluacion localEvaluacion = (Evaluacion)paramObject;
    if (localEvaluacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localEvaluacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new EvaluacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new EvaluacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EvaluacionPK))
      throw new IllegalArgumentException("arg1");
    EvaluacionPK localEvaluacionPK = (EvaluacionPK)paramObject;
    localEvaluacionPK.idEvaluacion = this.idEvaluacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof EvaluacionPK))
      throw new IllegalArgumentException("arg1");
    EvaluacionPK localEvaluacionPK = (EvaluacionPK)paramObject;
    this.idEvaluacion = localEvaluacionPK.idEvaluacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EvaluacionPK))
      throw new IllegalArgumentException("arg2");
    EvaluacionPK localEvaluacionPK = (EvaluacionPK)paramObject;
    localEvaluacionPK.idEvaluacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 15);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof EvaluacionPK))
      throw new IllegalArgumentException("arg2");
    EvaluacionPK localEvaluacionPK = (EvaluacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 15, localEvaluacionPK.idEvaluacion);
  }

  private static final AccionEvaluacion jdoGetaccionEvaluacion(Evaluacion paramEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.accionEvaluacion;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 0))
      return paramEvaluacion.accionEvaluacion;
    return (AccionEvaluacion)localStateManager.getObjectField(paramEvaluacion, jdoInheritedFieldCount + 0, paramEvaluacion.accionEvaluacion);
  }

  private static final void jdoSetaccionEvaluacion(Evaluacion paramEvaluacion, AccionEvaluacion paramAccionEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.accionEvaluacion = paramAccionEvaluacion;
      return;
    }
    localStateManager.setObjectField(paramEvaluacion, jdoInheritedFieldCount + 0, paramEvaluacion.accionEvaluacion, paramAccionEvaluacion);
  }

  private static final int jdoGetanio(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.anio;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.anio;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 1))
      return paramEvaluacion.anio;
    return localStateManager.getIntField(paramEvaluacion, jdoInheritedFieldCount + 1, paramEvaluacion.anio);
  }

  private static final void jdoSetanio(Evaluacion paramEvaluacion, int paramInt)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramEvaluacion, jdoInheritedFieldCount + 1, paramEvaluacion.anio, paramInt);
  }

  private static final float jdoGetatencionPublico(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.atencionPublico;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.atencionPublico;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 2))
      return paramEvaluacion.atencionPublico;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 2, paramEvaluacion.atencionPublico);
  }

  private static final void jdoSetatencionPublico(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.atencionPublico = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.atencionPublico = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 2, paramEvaluacion.atencionPublico, paramFloat);
  }

  private static final float jdoGetcalidadTrabajo(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.calidadTrabajo;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.calidadTrabajo;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 3))
      return paramEvaluacion.calidadTrabajo;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 3, paramEvaluacion.calidadTrabajo);
  }

  private static final void jdoSetcalidadTrabajo(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.calidadTrabajo = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.calidadTrabajo = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 3, paramEvaluacion.calidadTrabajo, paramFloat);
  }

  private static final float jdoGetcantidadTrabajo(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.cantidadTrabajo;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.cantidadTrabajo;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 4))
      return paramEvaluacion.cantidadTrabajo;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 4, paramEvaluacion.cantidadTrabajo);
  }

  private static final void jdoSetcantidadTrabajo(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.cantidadTrabajo = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.cantidadTrabajo = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 4, paramEvaluacion.cantidadTrabajo, paramFloat);
  }

  private static final float jdoGetcapacidadMando(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.capacidadMando;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.capacidadMando;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 5))
      return paramEvaluacion.capacidadMando;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 5, paramEvaluacion.capacidadMando);
  }

  private static final void jdoSetcapacidadMando(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.capacidadMando = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.capacidadMando = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 5, paramEvaluacion.capacidadMando, paramFloat);
  }

  private static final Cargo jdoGetcargo(Evaluacion paramEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.cargo;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 6))
      return paramEvaluacion.cargo;
    return (Cargo)localStateManager.getObjectField(paramEvaluacion, jdoInheritedFieldCount + 6, paramEvaluacion.cargo);
  }

  private static final void jdoSetcargo(Evaluacion paramEvaluacion, Cargo paramCargo)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramEvaluacion, jdoInheritedFieldCount + 6, paramEvaluacion.cargo, paramCargo);
  }

  private static final int jdoGetcedulaJefe(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.cedulaJefe;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.cedulaJefe;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 7))
      return paramEvaluacion.cedulaJefe;
    return localStateManager.getIntField(paramEvaluacion, jdoInheritedFieldCount + 7, paramEvaluacion.cedulaJefe);
  }

  private static final void jdoSetcedulaJefe(Evaluacion paramEvaluacion, int paramInt)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.cedulaJefe = paramInt;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.cedulaJefe = paramInt;
      return;
    }
    localStateManager.setIntField(paramEvaluacion, jdoInheritedFieldCount + 7, paramEvaluacion.cedulaJefe, paramInt);
  }

  private static final int jdoGetcedulaSupervisor(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.cedulaSupervisor;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.cedulaSupervisor;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 8))
      return paramEvaluacion.cedulaSupervisor;
    return localStateManager.getIntField(paramEvaluacion, jdoInheritedFieldCount + 8, paramEvaluacion.cedulaSupervisor);
  }

  private static final void jdoSetcedulaSupervisor(Evaluacion paramEvaluacion, int paramInt)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.cedulaSupervisor = paramInt;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.cedulaSupervisor = paramInt;
      return;
    }
    localStateManager.setIntField(paramEvaluacion, jdoInheritedFieldCount + 8, paramEvaluacion.cedulaSupervisor, paramInt);
  }

  private static final float jdoGetcomunicacion(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.comunicacion;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.comunicacion;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 9))
      return paramEvaluacion.comunicacion;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 9, paramEvaluacion.comunicacion);
  }

  private static final void jdoSetcomunicacion(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.comunicacion = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.comunicacion = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 9, paramEvaluacion.comunicacion, paramFloat);
  }

  private static final float jdoGetcooperacion(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.cooperacion;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.cooperacion;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 10))
      return paramEvaluacion.cooperacion;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 10, paramEvaluacion.cooperacion);
  }

  private static final void jdoSetcooperacion(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.cooperacion = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.cooperacion = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 10, paramEvaluacion.cooperacion, paramFloat);
  }

  private static final float jdoGetcoordinacion(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.coordinacion;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.coordinacion;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 11))
      return paramEvaluacion.coordinacion;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 11, paramEvaluacion.coordinacion);
  }

  private static final void jdoSetcoordinacion(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.coordinacion = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.coordinacion = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 11, paramEvaluacion.coordinacion, paramFloat);
  }

  private static final float jdoGetcumplimientoNormas(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.cumplimientoNormas;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.cumplimientoNormas;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 12))
      return paramEvaluacion.cumplimientoNormas;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 12, paramEvaluacion.cumplimientoNormas);
  }

  private static final void jdoSetcumplimientoNormas(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.cumplimientoNormas = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.cumplimientoNormas = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 12, paramEvaluacion.cumplimientoNormas, paramFloat);
  }

  private static final Dependencia jdoGetdependencia(Evaluacion paramEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.dependencia;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 13))
      return paramEvaluacion.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramEvaluacion, jdoInheritedFieldCount + 13, paramEvaluacion.dependencia);
  }

  private static final void jdoSetdependencia(Evaluacion paramEvaluacion, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramEvaluacion, jdoInheritedFieldCount + 13, paramEvaluacion.dependencia, paramDependencia);
  }

  private static final float jdoGethabitoSeguridad(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.habitoSeguridad;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.habitoSeguridad;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 14))
      return paramEvaluacion.habitoSeguridad;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 14, paramEvaluacion.habitoSeguridad);
  }

  private static final void jdoSethabitoSeguridad(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.habitoSeguridad = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.habitoSeguridad = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 14, paramEvaluacion.habitoSeguridad, paramFloat);
  }

  private static final long jdoGetidEvaluacion(Evaluacion paramEvaluacion)
  {
    return paramEvaluacion.idEvaluacion;
  }

  private static final void jdoSetidEvaluacion(Evaluacion paramEvaluacion, long paramLong)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.idEvaluacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramEvaluacion, jdoInheritedFieldCount + 15, paramEvaluacion.idEvaluacion, paramLong);
  }

  private static final float jdoGetinteresTrabajo(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.interesTrabajo;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.interesTrabajo;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 16))
      return paramEvaluacion.interesTrabajo;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 16, paramEvaluacion.interesTrabajo);
  }

  private static final void jdoSetinteresTrabajo(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.interesTrabajo = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.interesTrabajo = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 16, paramEvaluacion.interesTrabajo, paramFloat);
  }

  private static final float jdoGetmanejoBienes(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.manejoBienes;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.manejoBienes;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 17))
      return paramEvaluacion.manejoBienes;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 17, paramEvaluacion.manejoBienes);
  }

  private static final void jdoSetmanejoBienes(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.manejoBienes = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.manejoBienes = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 17, paramEvaluacion.manejoBienes, paramFloat);
  }

  private static final int jdoGetmes(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.mes;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.mes;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 18))
      return paramEvaluacion.mes;
    return localStateManager.getIntField(paramEvaluacion, jdoInheritedFieldCount + 18, paramEvaluacion.mes);
  }

  private static final void jdoSetmes(Evaluacion paramEvaluacion, int paramInt)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramEvaluacion, jdoInheritedFieldCount + 18, paramEvaluacion.mes, paramInt);
  }

  private static final double jdoGetmontoAumentar(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.montoAumentar;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.montoAumentar;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 19))
      return paramEvaluacion.montoAumentar;
    return localStateManager.getDoubleField(paramEvaluacion, jdoInheritedFieldCount + 19, paramEvaluacion.montoAumentar);
  }

  private static final void jdoSetmontoAumentar(Evaluacion paramEvaluacion, double paramDouble)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.montoAumentar = paramDouble;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.montoAumentar = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEvaluacion, jdoInheritedFieldCount + 19, paramEvaluacion.montoAumentar, paramDouble);
  }

  private static final double jdoGetmontoUnico(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.montoUnico;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.montoUnico;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 20))
      return paramEvaluacion.montoUnico;
    return localStateManager.getDoubleField(paramEvaluacion, jdoInheritedFieldCount + 20, paramEvaluacion.montoUnico);
  }

  private static final void jdoSetmontoUnico(Evaluacion paramEvaluacion, double paramDouble)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.montoUnico = paramDouble;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.montoUnico = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEvaluacion, jdoInheritedFieldCount + 20, paramEvaluacion.montoUnico, paramDouble);
  }

  private static final String jdoGetnivel(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.nivel;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.nivel;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 21))
      return paramEvaluacion.nivel;
    return localStateManager.getStringField(paramEvaluacion, jdoInheritedFieldCount + 21, paramEvaluacion.nivel);
  }

  private static final void jdoSetnivel(Evaluacion paramEvaluacion, String paramString)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.nivel = paramString;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.nivel = paramString;
      return;
    }
    localStateManager.setStringField(paramEvaluacion, jdoInheritedFieldCount + 21, paramEvaluacion.nivel, paramString);
  }

  private static final String jdoGetnombreCargo(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.nombreCargo;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.nombreCargo;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 22))
      return paramEvaluacion.nombreCargo;
    return localStateManager.getStringField(paramEvaluacion, jdoInheritedFieldCount + 22, paramEvaluacion.nombreCargo);
  }

  private static final void jdoSetnombreCargo(Evaluacion paramEvaluacion, String paramString)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.nombreCargo = paramString;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.nombreCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramEvaluacion, jdoInheritedFieldCount + 22, paramEvaluacion.nombreCargo, paramString);
  }

  private static final String jdoGetnombreDependencia(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.nombreDependencia;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.nombreDependencia;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 23))
      return paramEvaluacion.nombreDependencia;
    return localStateManager.getStringField(paramEvaluacion, jdoInheritedFieldCount + 23, paramEvaluacion.nombreDependencia);
  }

  private static final void jdoSetnombreDependencia(Evaluacion paramEvaluacion, String paramString)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.nombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.nombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramEvaluacion, jdoInheritedFieldCount + 23, paramEvaluacion.nombreDependencia, paramString);
  }

  private static final String jdoGetnombreJefe(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.nombreJefe;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.nombreJefe;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 24))
      return paramEvaluacion.nombreJefe;
    return localStateManager.getStringField(paramEvaluacion, jdoInheritedFieldCount + 24, paramEvaluacion.nombreJefe);
  }

  private static final void jdoSetnombreJefe(Evaluacion paramEvaluacion, String paramString)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.nombreJefe = paramString;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.nombreJefe = paramString;
      return;
    }
    localStateManager.setStringField(paramEvaluacion, jdoInheritedFieldCount + 24, paramEvaluacion.nombreJefe, paramString);
  }

  private static final String jdoGetnombreSupervisor(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.nombreSupervisor;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.nombreSupervisor;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 25))
      return paramEvaluacion.nombreSupervisor;
    return localStateManager.getStringField(paramEvaluacion, jdoInheritedFieldCount + 25, paramEvaluacion.nombreSupervisor);
  }

  private static final void jdoSetnombreSupervisor(Evaluacion paramEvaluacion, String paramString)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.nombreSupervisor = paramString;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.nombreSupervisor = paramString;
      return;
    }
    localStateManager.setStringField(paramEvaluacion, jdoInheritedFieldCount + 25, paramEvaluacion.nombreSupervisor, paramString);
  }

  private static final int jdoGetnumeroPasos(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.numeroPasos;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.numeroPasos;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 26))
      return paramEvaluacion.numeroPasos;
    return localStateManager.getIntField(paramEvaluacion, jdoInheritedFieldCount + 26, paramEvaluacion.numeroPasos);
  }

  private static final void jdoSetnumeroPasos(Evaluacion paramEvaluacion, int paramInt)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.numeroPasos = paramInt;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.numeroPasos = paramInt;
      return;
    }
    localStateManager.setIntField(paramEvaluacion, jdoInheritedFieldCount + 26, paramEvaluacion.numeroPasos, paramInt);
  }

  private static final String jdoGetobservaciones(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.observaciones;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.observaciones;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 27))
      return paramEvaluacion.observaciones;
    return localStateManager.getStringField(paramEvaluacion, jdoInheritedFieldCount + 27, paramEvaluacion.observaciones);
  }

  private static final void jdoSetobservaciones(Evaluacion paramEvaluacion, String paramString)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramEvaluacion, jdoInheritedFieldCount + 27, paramEvaluacion.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Evaluacion paramEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.personal;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 28))
      return paramEvaluacion.personal;
    return (Personal)localStateManager.getObjectField(paramEvaluacion, jdoInheritedFieldCount + 28, paramEvaluacion.personal);
  }

  private static final void jdoSetpersonal(Evaluacion paramEvaluacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramEvaluacion, jdoInheritedFieldCount + 28, paramEvaluacion.personal, paramPersonal);
  }

  private static final double jdoGetporcentajeAumento(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.porcentajeAumento;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.porcentajeAumento;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 29))
      return paramEvaluacion.porcentajeAumento;
    return localStateManager.getDoubleField(paramEvaluacion, jdoInheritedFieldCount + 29, paramEvaluacion.porcentajeAumento);
  }

  private static final void jdoSetporcentajeAumento(Evaluacion paramEvaluacion, double paramDouble)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.porcentajeAumento = paramDouble;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.porcentajeAumento = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramEvaluacion, jdoInheritedFieldCount + 29, paramEvaluacion.porcentajeAumento, paramDouble);
  }

  private static final float jdoGetresultadoCompetencias(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.resultadoCompetencias;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.resultadoCompetencias;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 30))
      return paramEvaluacion.resultadoCompetencias;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 30, paramEvaluacion.resultadoCompetencias);
  }

  private static final void jdoSetresultadoCompetencias(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.resultadoCompetencias = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.resultadoCompetencias = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 30, paramEvaluacion.resultadoCompetencias, paramFloat);
  }

  private static final ResultadoEvaluacion jdoGetresultadoEvaluacion(Evaluacion paramEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.resultadoEvaluacion;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 31))
      return paramEvaluacion.resultadoEvaluacion;
    return (ResultadoEvaluacion)localStateManager.getObjectField(paramEvaluacion, jdoInheritedFieldCount + 31, paramEvaluacion.resultadoEvaluacion);
  }

  private static final void jdoSetresultadoEvaluacion(Evaluacion paramEvaluacion, ResultadoEvaluacion paramResultadoEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.resultadoEvaluacion = paramResultadoEvaluacion;
      return;
    }
    localStateManager.setObjectField(paramEvaluacion, jdoInheritedFieldCount + 31, paramEvaluacion.resultadoEvaluacion, paramResultadoEvaluacion);
  }

  private static final float jdoGetresultadoObjetivos(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.resultadoObjetivos;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.resultadoObjetivos;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 32))
      return paramEvaluacion.resultadoObjetivos;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 32, paramEvaluacion.resultadoObjetivos);
  }

  private static final void jdoSetresultadoObjetivos(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.resultadoObjetivos = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.resultadoObjetivos = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 32, paramEvaluacion.resultadoObjetivos, paramFloat);
  }

  private static final TipoPersonal jdoGettipoPersonal(Evaluacion paramEvaluacion)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.tipoPersonal;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 33))
      return paramEvaluacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramEvaluacion, jdoInheritedFieldCount + 33, paramEvaluacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Evaluacion paramEvaluacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramEvaluacion, jdoInheritedFieldCount + 33, paramEvaluacion.tipoPersonal, paramTipoPersonal);
  }

  private static final float jdoGettomaDecisiones(Evaluacion paramEvaluacion)
  {
    if (paramEvaluacion.jdoFlags <= 0)
      return paramEvaluacion.tomaDecisiones;
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramEvaluacion.tomaDecisiones;
    if (localStateManager.isLoaded(paramEvaluacion, jdoInheritedFieldCount + 34))
      return paramEvaluacion.tomaDecisiones;
    return localStateManager.getFloatField(paramEvaluacion, jdoInheritedFieldCount + 34, paramEvaluacion.tomaDecisiones);
  }

  private static final void jdoSettomaDecisiones(Evaluacion paramEvaluacion, float paramFloat)
  {
    if (paramEvaluacion.jdoFlags == 0)
    {
      paramEvaluacion.tomaDecisiones = paramFloat;
      return;
    }
    StateManager localStateManager = paramEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramEvaluacion.tomaDecisiones = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramEvaluacion, jdoInheritedFieldCount + 34, paramEvaluacion.tomaDecisiones, paramFloat);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}