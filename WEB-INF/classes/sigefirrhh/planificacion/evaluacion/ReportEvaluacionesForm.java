package sigefirrhh.planificacion.evaluacion;

import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.CategoriaPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.login.LoginSession;

public class ReportEvaluacionesForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportEvaluacionesForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String formato = "1";
  private String selectIdTipoPersonal;
  private String orden = "A";
  private String tipo = "R";
  private Collection colRegion;
  private Collection colDependencia;
  private Collection colResultadoEvaluacion;
  private Collection colUnidadFuncional;
  private Collection listTipoPersonal;
  private long idTipoPersonal;
  private long idRegion;
  private long idDependencia;
  private long idResultadoEvaluacion;
  private long idUnidadFuncional;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private EvaluacionNoGenFacade evaluacionFacade = new EvaluacionNoGenFacade();
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private String seleccionUbicacion = "T";

  private boolean showRegion = false;
  private boolean showDependencia = false;
  private boolean showUnidadFuncional = false;
  private boolean showOrdenado = false;
  private int anio;
  private int mes;
  private TipoPersonal tipoPersonal;

  public boolean isShowUnidadFuncional()
  {
    return this.showUnidadFuncional;
  }
  public boolean isShowDependencia() {
    return this.showDependencia;
  }
  public boolean isShowRegion() {
    return this.showRegion;
  }
  public boolean isShowOrdenado() {
    return this.showOrdenado;
  }
  public boolean isShowResultadoEvaluacion() {
    return (this.colResultadoEvaluacion != null) && (!this.colResultadoEvaluacion.isEmpty());
  }
  public ReportEvaluacionesForm() {
    this.reportName = "evaluacionesdetaalf";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.definicionesFacade = new DefinicionesNoGenFacade();
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.anio = (new Date().getYear() + 1900);
    this.mes = (new Date().getMonth() + 1);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportEvaluacionesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeSeleccionUbicacion(ValueChangeEvent event) {
    String seleccionUbicacion = 
      (String)event.getNewValue();
    this.seleccionUbicacion = seleccionUbicacion;
    try {
      this.showRegion = false;
      this.showDependencia = false;
      this.showUnidadFuncional = false;
      if (seleccionUbicacion.equals("R"))
        this.showRegion = true;
      else if (seleccionUbicacion.equals("D"))
        this.showDependencia = true;
      else if (seleccionUbicacion.equals("U"))
        this.showUnidadFuncional = true;
    }
    catch (Exception e)
    {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(this.idTipoPersonal);
    try {
      this.showOrdenado = false;
      if (this.idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
        this.showOrdenado = true;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }
  public Collection getColUnidadFuncional() {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }

  public Collection getColDependencia() {
    Collection col = new ArrayList();
    Iterator iterator = this.colDependencia.iterator();
    Dependencia dependencia = null;
    while (iterator.hasNext()) {
      dependencia = (Dependencia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(dependencia.getIdDependencia()), 
        dependencia.toString()));
    }
    return col;
  }
  public Collection getColResultadoEvaluacion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colResultadoEvaluacion.iterator();
    ResultadoEvaluacion resultadoEvaluacion = null;
    while (iterator.hasNext()) {
      resultadoEvaluacion = (ResultadoEvaluacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(resultadoEvaluacion.getIdResultadoEvaluacion()), 
        resultadoEvaluacion.toString()));
    }
    return col;
  }

  private void cambiarNombreAReporte()
  {
    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }
    this.reportName += "evaldeta";

    if (this.seleccionUbicacion.equals("R"))
      this.reportName += "reg";
    else if (this.seleccionUbicacion.equals("D"))
      this.reportName += "dep";
    else if (this.seleccionUbicacion.equals("U")) {
      this.reportName += "uni";
    }
    if (this.tipo.equals("R")) {
      this.reportName += "real";
    }

    if (this.idResultadoEvaluacion != 0L) {
      this.reportName += "res";
    }
    if (this.orden.equals("A"))
      this.reportName += "alf";
    else if (this.orden.equals("C"))
      this.reportName += "ced";
    else if (this.orden.equals("N"))
      this.reportName += "cod";
    else if (this.orden.equals("U")) {
      this.reportName += "_ua";
    }

    if ((this.tipoPersonal != null) && 
      (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2")))
      this.reportName += "obr";
  }

  public void refresh()
  {
    try
    {
      try
      {
        this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), " tp.aumento_evaluacion = 'S' ");
      }
      catch (Exception e) {
        this.listTipoPersonal = new ArrayList();
        log.error("Excepcion controlada:", e);
      }

      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(this.login.getIdOrganismo());

      this.colDependencia = 
        this.estructuraFacade.findDependenciaByOrganismo(this.login.getIdOrganismo());

      this.colUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.colDependencia = new ArrayList();

      this.colRegion = new ArrayList();
      this.colUnidadFuncional = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
    parameters.put("anio", new Integer(this.anio));
    parameters.put("mes", new Integer(this.mes));
    log.error("idTP " + new Long(this.idTipoPersonal));

    this.reportName = "";
    if (this.formato.equals("2")) {
      this.reportName = "a_";
    }
    this.reportName += "evaldeta";

    if (this.seleccionUbicacion.equals("R")) {
      this.reportName += "reg";
      parameters.put("id_region", new Long(this.idRegion));
    }
    else if (this.seleccionUbicacion.equals("D")) {
      this.reportName += "dep";
      parameters.put("id_dependencia", new Long(this.idDependencia));
    } else if (this.seleccionUbicacion.equals("U")) {
      this.reportName += "uni";
      parameters.put("id_unidad_funcional", new Long(this.idUnidadFuncional));
    }
    if (this.tipo.equals("R")) {
      this.reportName += "real";
    }

    if (this.idResultadoEvaluacion != 0L) {
      this.reportName += "res";
      parameters.put("id_resultado_evaluacion", new Long(this.idResultadoEvaluacion));
    }
    if (this.orden.equals("A"))
      this.reportName += "alf";
    else if (this.orden.equals("C"))
      this.reportName += "ced";
    else if (this.orden.equals("N"))
      this.reportName += "cod";
    else if (this.orden.equals("U")) {
      this.reportName += "_ua";
    }

    if ((this.tipoPersonal != null) && 
      (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2"))) {
      this.reportName += "obr";
    }

    JasperForWeb report = new JasperForWeb();

    if (this.formato.equals("2"))
      report.setType(3);
    else {
      report.setType(1);
    }
    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/evaluacion");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string)
  {
    this.formato = string;
  }

  public String getSeleccionUbicacion()
  {
    return this.seleccionUbicacion;
  }
  public void setSeleccionUbicacion(String seleccionUbicacion) {
    this.seleccionUbicacion = seleccionUbicacion;
  }

  public long getIdDependencia() {
    return this.idDependencia;
  }
  public void setIdDependencia(long idDependencia) {
    this.idDependencia = idDependencia;
  }

  public long getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(long idRegion) {
    this.idRegion = idRegion;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getOrden() {
    return this.orden;
  }
  public void setOrden(String orden) {
    this.orden = orden;
  }
  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  public long getIdResultadoEvaluacion() {
    return this.idResultadoEvaluacion;
  }
  public void setIdResultadoEvaluacion(long idResultadoEvaluacion) {
    this.idResultadoEvaluacion = idResultadoEvaluacion;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public TipoPersonal getTipoPersonal() {
    return this.tipoPersonal;
  }
  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    this.tipoPersonal = tipoPersonal;
  }
  public long getIdUnidadFuncional() {
    return this.idUnidadFuncional;
  }
  public void setIdUnidadFuncional(long idUnidadFuncional) {
    this.idUnidadFuncional = idUnidadFuncional;
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }
}