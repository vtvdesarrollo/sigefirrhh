package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class AccionEvaluacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addAccionEvaluacion(AccionEvaluacion accionEvaluacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    AccionEvaluacion accionEvaluacionNew = 
      (AccionEvaluacion)BeanUtils.cloneBean(
      accionEvaluacion);

    pm.makePersistent(accionEvaluacionNew);
  }

  public void updateAccionEvaluacion(AccionEvaluacion accionEvaluacion) throws Exception
  {
    AccionEvaluacion accionEvaluacionModify = 
      findAccionEvaluacionById(accionEvaluacion.getIdAccionEvaluacion());

    BeanUtils.copyProperties(accionEvaluacionModify, accionEvaluacion);
  }

  public void deleteAccionEvaluacion(AccionEvaluacion accionEvaluacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    AccionEvaluacion accionEvaluacionDelete = 
      findAccionEvaluacionById(accionEvaluacion.getIdAccionEvaluacion());
    pm.deletePersistent(accionEvaluacionDelete);
  }

  public AccionEvaluacion findAccionEvaluacionById(long idAccionEvaluacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idAccionEvaluacion == pIdAccionEvaluacion";
    Query query = pm.newQuery(AccionEvaluacion.class, filter);

    query.declareParameters("long pIdAccionEvaluacion");

    parameters.put("pIdAccionEvaluacion", new Long(idAccionEvaluacion));

    Collection colAccionEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colAccionEvaluacion.iterator();
    return (AccionEvaluacion)iterator.next();
  }

  public Collection findAccionEvaluacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent accionEvaluacionExtent = pm.getExtent(
      AccionEvaluacion.class, true);
    Query query = pm.newQuery(accionEvaluacionExtent);
    query.setOrdering("codAccionEvaluacion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodAccionEvaluacion(String codAccionEvaluacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codAccionEvaluacion == pCodAccionEvaluacion";

    Query query = pm.newQuery(AccionEvaluacion.class, filter);

    query.declareParameters("java.lang.String pCodAccionEvaluacion");
    HashMap parameters = new HashMap();

    parameters.put("pCodAccionEvaluacion", new String(codAccionEvaluacion));

    query.setOrdering("codAccionEvaluacion ascending");

    Collection colAccionEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionEvaluacion);

    return colAccionEvaluacion;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(AccionEvaluacion.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("codAccionEvaluacion ascending");

    Collection colAccionEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colAccionEvaluacion);

    return colAccionEvaluacion;
  }
}