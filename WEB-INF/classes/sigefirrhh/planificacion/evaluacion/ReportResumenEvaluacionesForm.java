package sigefirrhh.planificacion.evaluacion;

import eforserver.common.Resource;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.login.LoginSession;

public class ReportResumenEvaluacionesForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportResumenEvaluacionesForm.class.getName());
  private int reportId;
  private String reportName;
  private Collection colRegion;
  private Collection colTipoPersonal;
  private long idRegion;
  private String idTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private EvaluacionNoGenFacade evaluacionFacade = new EvaluacionNoGenFacade();
  private LoginSession login;
  private String seleccionUbicacion = "T";

  private boolean showRegion = false;
  private boolean showDependencia = false;
  private boolean showUnidadFuncional = false;
  private int anio;
  private int mes;
  private String clasificacion = "1";
  private int cantidadTrabajadores = 0;
  private int cantidadEvaluados = 0;

  public boolean isShowUnidadFuncional() {
    return this.showUnidadFuncional;
  }
  public boolean isShowDependencia() {
    return this.showDependencia;
  }
  public boolean isShowRegion() {
    return this.showRegion;
  }

  public ReportResumenEvaluacionesForm() {
    this.reportName = "resuevaluaciones";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.definicionesFacade = new DefinicionesNoGenFacade();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.anio = (new Date().getYear() + 1900);
    this.mes = (new Date().getMonth() + 1);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportResumenEvaluacionesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void calcular() {
    Collection col = new ArrayList();
    Connection connection = Resource.getConnection();
    connection = Resource.getConnection();
    StringBuffer sql = new StringBuffer();
    try {
      connection.setAutoCommit(true);

      ResultSet rsRegistros = null;
      PreparedStatement stRegistros = null;
      if (this.seleccionUbicacion.equals("R")) {
        sql.append("select count(t.id_trabajador) as cantidad");
        sql.append(" from personal p, trabajador t , dependencia d");
        sql.append(" where p.id_personal = t.id_personal");
        sql.append(" and t.id_tipo_personal = ?");
        sql.append(" and t.estatus = 'A'");
        sql.append(" and t.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_region = ?");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, Long.valueOf(this.idTipoPersonal).longValue());
        stRegistros.setLong(2, this.idRegion);
      }
      else {
        sql.append("select count(t.id_trabajador) as cantidad");
        sql.append(" from trabajador t, personal p");
        sql.append(" where p.id_personal = t.id_personal");
        sql.append(" and t.id_tipo_personal = ?");
        sql.append(" and t.estatus = 'A'");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, Long.valueOf(this.idTipoPersonal).longValue());
      }

      rsRegistros = stRegistros.executeQuery();
      rsRegistros.next();
      this.cantidadTrabajadores = rsRegistros.getInt("cantidad");

      sql = new StringBuffer();

      if (this.seleccionUbicacion.equals("R")) {
        sql.append("select count(e.id_personal) as cantidad");
        sql.append(" from evaluacion e , dependencia d");
        sql.append(" where e.id_tipo_personal = ?");
        sql.append(" and e.anio = ?");
        sql.append(" and e.mes = ?");
        sql.append(" and e.id_dependencia = d.id_dependencia");
        sql.append(" and d.id_region = ?");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, Long.valueOf(this.idTipoPersonal).longValue());
        stRegistros.setInt(2, this.anio);
        stRegistros.setInt(3, this.mes);
        stRegistros.setLong(4, this.idRegion);
      }
      else {
        sql.append("select count(e.id_personal) as cantidad");
        sql.append(" from evaluacion e");
        sql.append(" where e.id_tipo_personal = ?");
        sql.append(" and e.anio = ?");
        sql.append(" and e.mes = ?");

        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, Long.valueOf(this.idTipoPersonal).longValue());
        stRegistros.setInt(2, this.anio);
        stRegistros.setInt(3, this.mes);
      }

      rsRegistros = stRegistros.executeQuery();
      rsRegistros.next();
      this.cantidadEvaluados = rsRegistros.getInt("cantidad");
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeSeleccionUbicacion(ValueChangeEvent event)
  {
    String seleccionUbicacion = 
      (String)event.getNewValue();
    this.seleccionUbicacion = seleccionUbicacion;
    try {
      this.showRegion = false;
      this.showDependencia = false;
      this.showUnidadFuncional = false;
      if (seleccionUbicacion.equals("R"))
        this.showRegion = true;
      else if (seleccionUbicacion.equals("D"))
        this.showDependencia = true;
      else if (seleccionUbicacion.equals("U")) {
        this.showUnidadFuncional = true;
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    String idTipoPersonal = 
      (String)event.getNewValue();
    this.idTipoPersonal = idTipoPersonal;
    try
    {
      this.idTipoPersonal.equals("0");
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  private void cambiarNombreAReporte() {
    this.reportName = "resuevaluaciones";
    if (this.clasificacion.equals("2")) {
      this.reportName += "niv";
    }
    if (this.seleccionUbicacion.equals("R"))
      this.reportName += "reg";
  }

  public void refresh()
  {
    try
    {
      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), " tp.aumento_evaluacion = 'S' ");

      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);

      this.colTipoPersonal = new ArrayList();
      this.colRegion = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();

    calcular();

    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
    parameters.put("anio", new Integer(this.anio));
    parameters.put("mes", new Integer(this.mes));
    parameters.put("cantidad_trabajadores", new Integer(this.cantidadTrabajadores));
    parameters.put("cantidad_evaluados", new Integer(this.cantidadEvaluados));
    log.error("cantidad_trabajadores" + this.cantidadTrabajadores);
    log.error("cantidad_evaluados" + this.cantidadEvaluados);
    this.reportName = "resuevaluaciones";
    if (this.clasificacion.equals("2")) {
      this.reportName += "niv";
    }
    if (this.seleccionUbicacion.equals("R")) {
      this.reportName += "reg";
      parameters.put("id_region", new Long(this.idRegion));
    }

    JasperForWeb report = new JasperForWeb();

    report.setType(1);

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/evaluacion");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public String getSeleccionUbicacion()
  {
    return this.seleccionUbicacion;
  }
  public void setSeleccionUbicacion(String seleccionUbicacion) {
    this.seleccionUbicacion = seleccionUbicacion;
  }

  public long getIdRegion()
  {
    return this.idRegion;
  }
  public void setIdRegion(long idRegion) {
    this.idRegion = idRegion;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(String idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getClasificacion() {
    return this.clasificacion;
  }
  public void setClasificacion(String clasificacion) {
    this.clasificacion = clasificacion;
  }
}