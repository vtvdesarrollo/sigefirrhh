package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class NoEvaluacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addNoEvaluacion(NoEvaluacion noEvaluacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    NoEvaluacion noEvaluacionNew = 
      (NoEvaluacion)BeanUtils.cloneBean(
      noEvaluacion);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (noEvaluacionNew.getPersonal() != null) {
      noEvaluacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        noEvaluacionNew.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (noEvaluacionNew.getTipoPersonal() != null) {
      noEvaluacionNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        noEvaluacionNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(noEvaluacionNew);
  }

  public void updateNoEvaluacion(NoEvaluacion noEvaluacion) throws Exception
  {
    NoEvaluacion noEvaluacionModify = 
      findNoEvaluacionById(noEvaluacion.getIdNoEvaluacion());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (noEvaluacion.getPersonal() != null) {
      noEvaluacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        noEvaluacion.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (noEvaluacion.getTipoPersonal() != null) {
      noEvaluacion.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        noEvaluacion.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(noEvaluacionModify, noEvaluacion);
  }

  public void deleteNoEvaluacion(NoEvaluacion noEvaluacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    NoEvaluacion noEvaluacionDelete = 
      findNoEvaluacionById(noEvaluacion.getIdNoEvaluacion());
    pm.deletePersistent(noEvaluacionDelete);
  }

  public NoEvaluacion findNoEvaluacionById(long idNoEvaluacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idNoEvaluacion == pIdNoEvaluacion";
    Query query = pm.newQuery(NoEvaluacion.class, filter);

    query.declareParameters("long pIdNoEvaluacion");

    parameters.put("pIdNoEvaluacion", new Long(idNoEvaluacion));

    Collection colNoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colNoEvaluacion.iterator();
    return (NoEvaluacion)iterator.next();
  }

  public Collection findNoEvaluacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent noEvaluacionExtent = pm.getExtent(
      NoEvaluacion.class, true);
    Query query = pm.newQuery(noEvaluacionExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(NoEvaluacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colNoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colNoEvaluacion);

    return colNoEvaluacion;
  }
}