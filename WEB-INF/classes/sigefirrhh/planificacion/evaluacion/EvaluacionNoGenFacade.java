package sigefirrhh.planificacion.evaluacion;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Collection;

public class EvaluacionNoGenFacade extends EvaluacionFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EvaluacionNoGenBusiness evaluacionNoGenBusiness = new EvaluacionNoGenBusiness();

  public Collection findResultadoEvaluacionByTipoPersonalAndRango(long idTipoPersonal, double puntos) throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionNoGenBusiness.findResultadoEvaluacionByTipoPersonalAndRango(idTipoPersonal, puntos);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResultadoEvaluacionByTipoPersonalAndAnio(long idTipoPersonal, int anio) throws Exception
  {
    try { this.txn.open();
      return this.evaluacionNoGenBusiness.findResultadoEvaluacionByTipoPersonalAndAnio(idTipoPersonal, anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ResultSet findResultadoEvaluacionByIdTipoPersonalAnioRango(long idTipoPersonal, int anio, double puntos)
    throws Exception
  {
    return this.evaluacionNoGenBusiness.findResultadoEvaluacionByIdTipoPersonalAnioRango(idTipoPersonal, anio, puntos);
  }
}