package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class EvaluacionBusiness extends AbstractBusiness
  implements Serializable
{
  private ConceptoEvaluacionBeanBusiness conceptoEvaluacionBeanBusiness = new ConceptoEvaluacionBeanBusiness();

  private AccionEvaluacionBeanBusiness accionEvaluacionBeanBusiness = new AccionEvaluacionBeanBusiness();

  private EvaluacionBeanBusiness evaluacionBeanBusiness = new EvaluacionBeanBusiness();

  private ResultadoEvaluacionBeanBusiness resultadoEvaluacionBeanBusiness = new ResultadoEvaluacionBeanBusiness();

  private NoEvaluacionBeanBusiness noEvaluacionBeanBusiness = new NoEvaluacionBeanBusiness();

  private ApelacionBeanBusiness apelacionBeanBusiness = new ApelacionBeanBusiness();

  public void addConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion)
    throws Exception
  {
    this.conceptoEvaluacionBeanBusiness.addConceptoEvaluacion(conceptoEvaluacion);
  }

  public void updateConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion) throws Exception {
    this.conceptoEvaluacionBeanBusiness.updateConceptoEvaluacion(conceptoEvaluacion);
  }

  public void deleteConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion) throws Exception {
    this.conceptoEvaluacionBeanBusiness.deleteConceptoEvaluacion(conceptoEvaluacion);
  }

  public ConceptoEvaluacion findConceptoEvaluacionById(long conceptoEvaluacionId) throws Exception {
    return this.conceptoEvaluacionBeanBusiness.findConceptoEvaluacionById(conceptoEvaluacionId);
  }

  public Collection findAllConceptoEvaluacion() throws Exception {
    return this.conceptoEvaluacionBeanBusiness.findConceptoEvaluacionAll();
  }

  public Collection findConceptoEvaluacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.conceptoEvaluacionBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addAccionEvaluacion(AccionEvaluacion accionEvaluacion)
    throws Exception
  {
    this.accionEvaluacionBeanBusiness.addAccionEvaluacion(accionEvaluacion);
  }

  public void updateAccionEvaluacion(AccionEvaluacion accionEvaluacion) throws Exception {
    this.accionEvaluacionBeanBusiness.updateAccionEvaluacion(accionEvaluacion);
  }

  public void deleteAccionEvaluacion(AccionEvaluacion accionEvaluacion) throws Exception {
    this.accionEvaluacionBeanBusiness.deleteAccionEvaluacion(accionEvaluacion);
  }

  public AccionEvaluacion findAccionEvaluacionById(long accionEvaluacionId) throws Exception {
    return this.accionEvaluacionBeanBusiness.findAccionEvaluacionById(accionEvaluacionId);
  }

  public Collection findAllAccionEvaluacion() throws Exception {
    return this.accionEvaluacionBeanBusiness.findAccionEvaluacionAll();
  }

  public Collection findAccionEvaluacionByCodAccionEvaluacion(String codAccionEvaluacion)
    throws Exception
  {
    return this.accionEvaluacionBeanBusiness.findByCodAccionEvaluacion(codAccionEvaluacion);
  }

  public Collection findAccionEvaluacionByDescripcion(String descripcion)
    throws Exception
  {
    return this.accionEvaluacionBeanBusiness.findByDescripcion(descripcion);
  }

  public void addEvaluacion(Evaluacion evaluacion)
    throws Exception
  {
    this.evaluacionBeanBusiness.addEvaluacion(evaluacion);
  }

  public void updateEvaluacion(Evaluacion evaluacion) throws Exception {
    this.evaluacionBeanBusiness.updateEvaluacion(evaluacion);
  }

  public void deleteEvaluacion(Evaluacion evaluacion) throws Exception {
    this.evaluacionBeanBusiness.deleteEvaluacion(evaluacion);
  }

  public Evaluacion findEvaluacionById(long evaluacionId) throws Exception {
    return this.evaluacionBeanBusiness.findEvaluacionById(evaluacionId);
  }

  public Collection findAllEvaluacion() throws Exception {
    return this.evaluacionBeanBusiness.findEvaluacionAll();
  }

  public Collection findEvaluacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.evaluacionBeanBusiness.findByPersonal(idPersonal);
  }

  public void addResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion)
    throws Exception
  {
    this.resultadoEvaluacionBeanBusiness.addResultadoEvaluacion(resultadoEvaluacion);
  }

  public void updateResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion) throws Exception {
    this.resultadoEvaluacionBeanBusiness.updateResultadoEvaluacion(resultadoEvaluacion);
  }

  public void deleteResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion) throws Exception {
    this.resultadoEvaluacionBeanBusiness.deleteResultadoEvaluacion(resultadoEvaluacion);
  }

  public ResultadoEvaluacion findResultadoEvaluacionById(long resultadoEvaluacionId) throws Exception {
    return this.resultadoEvaluacionBeanBusiness.findResultadoEvaluacionById(resultadoEvaluacionId);
  }

  public Collection findAllResultadoEvaluacion() throws Exception {
    return this.resultadoEvaluacionBeanBusiness.findResultadoEvaluacionAll();
  }

  public Collection findResultadoEvaluacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.resultadoEvaluacionBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addNoEvaluacion(NoEvaluacion noEvaluacion)
    throws Exception
  {
    this.noEvaluacionBeanBusiness.addNoEvaluacion(noEvaluacion);
  }

  public void updateNoEvaluacion(NoEvaluacion noEvaluacion) throws Exception {
    this.noEvaluacionBeanBusiness.updateNoEvaluacion(noEvaluacion);
  }

  public void deleteNoEvaluacion(NoEvaluacion noEvaluacion) throws Exception {
    this.noEvaluacionBeanBusiness.deleteNoEvaluacion(noEvaluacion);
  }

  public NoEvaluacion findNoEvaluacionById(long noEvaluacionId) throws Exception {
    return this.noEvaluacionBeanBusiness.findNoEvaluacionById(noEvaluacionId);
  }

  public Collection findAllNoEvaluacion() throws Exception {
    return this.noEvaluacionBeanBusiness.findNoEvaluacionAll();
  }

  public Collection findNoEvaluacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.noEvaluacionBeanBusiness.findByPersonal(idPersonal);
  }

  public void addApelacion(Apelacion apelacion)
    throws Exception
  {
    this.apelacionBeanBusiness.addApelacion(apelacion);
  }

  public void updateApelacion(Apelacion apelacion) throws Exception {
    this.apelacionBeanBusiness.updateApelacion(apelacion);
  }

  public void deleteApelacion(Apelacion apelacion) throws Exception {
    this.apelacionBeanBusiness.deleteApelacion(apelacion);
  }

  public Apelacion findApelacionById(long apelacionId) throws Exception {
    return this.apelacionBeanBusiness.findApelacionById(apelacionId);
  }

  public Collection findAllApelacion() throws Exception {
    return this.apelacionBeanBusiness.findApelacionAll();
  }

  public Collection findApelacionByPersonal(long idPersonal)
    throws Exception
  {
    return this.apelacionBeanBusiness.findByPersonal(idPersonal);
  }
}