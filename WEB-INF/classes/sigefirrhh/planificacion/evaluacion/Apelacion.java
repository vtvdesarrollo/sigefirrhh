package sigefirrhh.planificacion.evaluacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Personal;

public class Apelacion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_RESULTADO;
  private long idApelacion;
  private int anio;
  private int mes;
  private Date fecha;
  private String resultado;
  private int cedulaSupervisor;
  private String nombreSupervisor;
  private String observaciones;
  private Personal personal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "cedulaSupervisor", "fecha", "idApelacion", "mes", "nombreSupervisor", "observaciones", "personal", "resultado" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.evaluacion.Apelacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Apelacion());

    LISTA_RESULTADO = 
      new LinkedHashMap();

    LISTA_RESULTADO.put("1", "RATIFICADO");
    LISTA_RESULTADO.put("2", "MODIFICADO");
  }

  public Apelacion()
  {
    jdoSetcedulaSupervisor(this, 0);
  }

  public String toString()
  {
    return jdoGetpersonal(this).toString();
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public int getCedulaSupervisor()
  {
    return jdoGetcedulaSupervisor(this);
  }

  public void setCedulaSupervisor(int cedulaSupervisor)
  {
    jdoSetcedulaSupervisor(this, cedulaSupervisor);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public void setFecha(Date fecha)
  {
    jdoSetfecha(this, fecha);
  }

  public long getIdApelacion()
  {
    return jdoGetidApelacion(this);
  }

  public void setIdApelacion(long idApelacion)
  {
    jdoSetidApelacion(this, idApelacion);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int mes)
  {
    jdoSetmes(this, mes);
  }

  public String getNombreSupervisor()
  {
    return jdoGetnombreSupervisor(this);
  }

  public void setNombreSupervisor(String nombreSupervisor)
  {
    jdoSetnombreSupervisor(this, nombreSupervisor);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public String getResultado()
  {
    return jdoGetresultado(this);
  }

  public void setResultado(String resultado)
  {
    jdoSetresultado(this, resultado);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Apelacion localApelacion = new Apelacion();
    localApelacion.jdoFlags = 1;
    localApelacion.jdoStateManager = paramStateManager;
    return localApelacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Apelacion localApelacion = new Apelacion();
    localApelacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localApelacion.jdoFlags = 1;
    localApelacion.jdoStateManager = paramStateManager;
    return localApelacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedulaSupervisor);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idApelacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreSupervisor);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.resultado);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedulaSupervisor = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idApelacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreSupervisor = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultado = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Apelacion paramApelacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramApelacion.anio;
      return;
    case 1:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.cedulaSupervisor = paramApelacion.cedulaSupervisor;
      return;
    case 2:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramApelacion.fecha;
      return;
    case 3:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.idApelacion = paramApelacion.idApelacion;
      return;
    case 4:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramApelacion.mes;
      return;
    case 5:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.nombreSupervisor = paramApelacion.nombreSupervisor;
      return;
    case 6:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramApelacion.observaciones;
      return;
    case 7:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramApelacion.personal;
      return;
    case 8:
      if (paramApelacion == null)
        throw new IllegalArgumentException("arg1");
      this.resultado = paramApelacion.resultado;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Apelacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Apelacion localApelacion = (Apelacion)paramObject;
    if (localApelacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localApelacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ApelacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ApelacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ApelacionPK))
      throw new IllegalArgumentException("arg1");
    ApelacionPK localApelacionPK = (ApelacionPK)paramObject;
    localApelacionPK.idApelacion = this.idApelacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ApelacionPK))
      throw new IllegalArgumentException("arg1");
    ApelacionPK localApelacionPK = (ApelacionPK)paramObject;
    this.idApelacion = localApelacionPK.idApelacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ApelacionPK))
      throw new IllegalArgumentException("arg2");
    ApelacionPK localApelacionPK = (ApelacionPK)paramObject;
    localApelacionPK.idApelacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ApelacionPK))
      throw new IllegalArgumentException("arg2");
    ApelacionPK localApelacionPK = (ApelacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localApelacionPK.idApelacion);
  }

  private static final int jdoGetanio(Apelacion paramApelacion)
  {
    if (paramApelacion.jdoFlags <= 0)
      return paramApelacion.anio;
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.anio;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 0))
      return paramApelacion.anio;
    return localStateManager.getIntField(paramApelacion, jdoInheritedFieldCount + 0, paramApelacion.anio);
  }

  private static final void jdoSetanio(Apelacion paramApelacion, int paramInt)
  {
    if (paramApelacion.jdoFlags == 0)
    {
      paramApelacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramApelacion, jdoInheritedFieldCount + 0, paramApelacion.anio, paramInt);
  }

  private static final int jdoGetcedulaSupervisor(Apelacion paramApelacion)
  {
    if (paramApelacion.jdoFlags <= 0)
      return paramApelacion.cedulaSupervisor;
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.cedulaSupervisor;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 1))
      return paramApelacion.cedulaSupervisor;
    return localStateManager.getIntField(paramApelacion, jdoInheritedFieldCount + 1, paramApelacion.cedulaSupervisor);
  }

  private static final void jdoSetcedulaSupervisor(Apelacion paramApelacion, int paramInt)
  {
    if (paramApelacion.jdoFlags == 0)
    {
      paramApelacion.cedulaSupervisor = paramInt;
      return;
    }
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.cedulaSupervisor = paramInt;
      return;
    }
    localStateManager.setIntField(paramApelacion, jdoInheritedFieldCount + 1, paramApelacion.cedulaSupervisor, paramInt);
  }

  private static final Date jdoGetfecha(Apelacion paramApelacion)
  {
    if (paramApelacion.jdoFlags <= 0)
      return paramApelacion.fecha;
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.fecha;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 2))
      return paramApelacion.fecha;
    return (Date)localStateManager.getObjectField(paramApelacion, jdoInheritedFieldCount + 2, paramApelacion.fecha);
  }

  private static final void jdoSetfecha(Apelacion paramApelacion, Date paramDate)
  {
    if (paramApelacion.jdoFlags == 0)
    {
      paramApelacion.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramApelacion, jdoInheritedFieldCount + 2, paramApelacion.fecha, paramDate);
  }

  private static final long jdoGetidApelacion(Apelacion paramApelacion)
  {
    return paramApelacion.idApelacion;
  }

  private static final void jdoSetidApelacion(Apelacion paramApelacion, long paramLong)
  {
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.idApelacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramApelacion, jdoInheritedFieldCount + 3, paramApelacion.idApelacion, paramLong);
  }

  private static final int jdoGetmes(Apelacion paramApelacion)
  {
    if (paramApelacion.jdoFlags <= 0)
      return paramApelacion.mes;
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.mes;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 4))
      return paramApelacion.mes;
    return localStateManager.getIntField(paramApelacion, jdoInheritedFieldCount + 4, paramApelacion.mes);
  }

  private static final void jdoSetmes(Apelacion paramApelacion, int paramInt)
  {
    if (paramApelacion.jdoFlags == 0)
    {
      paramApelacion.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramApelacion, jdoInheritedFieldCount + 4, paramApelacion.mes, paramInt);
  }

  private static final String jdoGetnombreSupervisor(Apelacion paramApelacion)
  {
    if (paramApelacion.jdoFlags <= 0)
      return paramApelacion.nombreSupervisor;
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.nombreSupervisor;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 5))
      return paramApelacion.nombreSupervisor;
    return localStateManager.getStringField(paramApelacion, jdoInheritedFieldCount + 5, paramApelacion.nombreSupervisor);
  }

  private static final void jdoSetnombreSupervisor(Apelacion paramApelacion, String paramString)
  {
    if (paramApelacion.jdoFlags == 0)
    {
      paramApelacion.nombreSupervisor = paramString;
      return;
    }
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.nombreSupervisor = paramString;
      return;
    }
    localStateManager.setStringField(paramApelacion, jdoInheritedFieldCount + 5, paramApelacion.nombreSupervisor, paramString);
  }

  private static final String jdoGetobservaciones(Apelacion paramApelacion)
  {
    if (paramApelacion.jdoFlags <= 0)
      return paramApelacion.observaciones;
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.observaciones;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 6))
      return paramApelacion.observaciones;
    return localStateManager.getStringField(paramApelacion, jdoInheritedFieldCount + 6, paramApelacion.observaciones);
  }

  private static final void jdoSetobservaciones(Apelacion paramApelacion, String paramString)
  {
    if (paramApelacion.jdoFlags == 0)
    {
      paramApelacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramApelacion, jdoInheritedFieldCount + 6, paramApelacion.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Apelacion paramApelacion)
  {
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.personal;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 7))
      return paramApelacion.personal;
    return (Personal)localStateManager.getObjectField(paramApelacion, jdoInheritedFieldCount + 7, paramApelacion.personal);
  }

  private static final void jdoSetpersonal(Apelacion paramApelacion, Personal paramPersonal)
  {
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramApelacion, jdoInheritedFieldCount + 7, paramApelacion.personal, paramPersonal);
  }

  private static final String jdoGetresultado(Apelacion paramApelacion)
  {
    if (paramApelacion.jdoFlags <= 0)
      return paramApelacion.resultado;
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
      return paramApelacion.resultado;
    if (localStateManager.isLoaded(paramApelacion, jdoInheritedFieldCount + 8))
      return paramApelacion.resultado;
    return localStateManager.getStringField(paramApelacion, jdoInheritedFieldCount + 8, paramApelacion.resultado);
  }

  private static final void jdoSetresultado(Apelacion paramApelacion, String paramString)
  {
    if (paramApelacion.jdoFlags == 0)
    {
      paramApelacion.resultado = paramString;
      return;
    }
    StateManager localStateManager = paramApelacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramApelacion.resultado = paramString;
      return;
    }
    localStateManager.setStringField(paramApelacion, jdoInheritedFieldCount + 8, paramApelacion.resultado, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}