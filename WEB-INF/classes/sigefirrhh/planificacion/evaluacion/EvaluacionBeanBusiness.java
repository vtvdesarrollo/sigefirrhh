package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class EvaluacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(EvaluacionBeanBusiness.class.getName());

  public void addEvaluacion(Evaluacion evaluacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Evaluacion evaluacionNew = 
      (Evaluacion)BeanUtils.cloneBean(
      evaluacion);

    ResultadoEvaluacionBeanBusiness resultadoEvaluacionBeanBusiness = new ResultadoEvaluacionBeanBusiness();

    if (evaluacionNew.getResultadoEvaluacion() != null) {
      evaluacionNew.setResultadoEvaluacion(
        resultadoEvaluacionBeanBusiness.findResultadoEvaluacionById(
        evaluacionNew.getResultadoEvaluacion().getIdResultadoEvaluacion()));
    }

    AccionEvaluacionBeanBusiness accionEvaluacionBeanBusiness = new AccionEvaluacionBeanBusiness();

    if (evaluacionNew.getAccionEvaluacion() != null) {
      evaluacionNew.setAccionEvaluacion(
        accionEvaluacionBeanBusiness.findAccionEvaluacionById(
        evaluacionNew.getAccionEvaluacion().getIdAccionEvaluacion()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (evaluacionNew.getPersonal() != null) {
      evaluacionNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        evaluacionNew.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (evaluacionNew.getTipoPersonal() != null) {
      evaluacionNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        evaluacionNew.getTipoPersonal().getIdTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (evaluacionNew.getCargo() != null) {
      evaluacionNew.setCargo(
        cargoBeanBusiness.findCargoById(
        evaluacionNew.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (evaluacionNew.getDependencia() != null) {
      evaluacionNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        evaluacionNew.getDependencia().getIdDependencia()));
    }
    pm.makePersistent(evaluacionNew);
  }

  public void updateEvaluacion(Evaluacion evaluacion) throws Exception
  {
    Evaluacion evaluacionModify = 
      findEvaluacionById(evaluacion.getIdEvaluacion());

    ResultadoEvaluacionBeanBusiness resultadoEvaluacionBeanBusiness = new ResultadoEvaluacionBeanBusiness();

    if (evaluacion.getResultadoEvaluacion() != null) {
      evaluacion.setResultadoEvaluacion(
        resultadoEvaluacionBeanBusiness.findResultadoEvaluacionById(
        evaluacion.getResultadoEvaluacion().getIdResultadoEvaluacion()));
    }

    AccionEvaluacionBeanBusiness accionEvaluacionBeanBusiness = new AccionEvaluacionBeanBusiness();

    if (evaluacion.getAccionEvaluacion() != null) {
      evaluacion.setAccionEvaluacion(
        accionEvaluacionBeanBusiness.findAccionEvaluacionById(
        evaluacion.getAccionEvaluacion().getIdAccionEvaluacion()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (evaluacion.getPersonal() != null) {
      evaluacion.setPersonal(
        personalBeanBusiness.findPersonalById(
        evaluacion.getPersonal().getIdPersonal()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (evaluacion.getTipoPersonal() != null) {
      evaluacion.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        evaluacion.getTipoPersonal().getIdTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (evaluacion.getCargo() != null) {
      evaluacion.setCargo(
        cargoBeanBusiness.findCargoById(
        evaluacion.getCargo().getIdCargo()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (evaluacion.getDependencia() != null) {
      evaluacion.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        evaluacion.getDependencia().getIdDependencia()));
    }

    BeanUtils.copyProperties(evaluacionModify, evaluacion);
  }

  public void deleteEvaluacion(Evaluacion evaluacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Evaluacion evaluacionDelete = 
      findEvaluacionById(evaluacion.getIdEvaluacion());
    pm.deletePersistent(evaluacionDelete);
  }

  public Evaluacion findEvaluacionById(long idEvaluacion) throws Exception {
    HashMap parameters = new HashMap();
    this.log.error("************1 en business*************");
    PersistenceManager pm = PMThread.getPM();
    String filter = "idEvaluacion == pIdEvaluacion";
    Query query = pm.newQuery(Evaluacion.class, filter);

    query.declareParameters("long pIdEvaluacion");

    parameters.put("pIdEvaluacion", new Long(idEvaluacion));

    Collection colEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));
    this.log.error("************2 en business*************");
    Iterator iterator = colEvaluacion.iterator();
    return (Evaluacion)iterator.next();
  }

  public Collection findEvaluacionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent evaluacionExtent = pm.getExtent(
      Evaluacion.class, true);
    Query query = pm.newQuery(evaluacionExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Evaluacion.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colEvaluacion);

    return colEvaluacion;
  }
}