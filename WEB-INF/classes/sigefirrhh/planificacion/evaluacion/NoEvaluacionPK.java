package sigefirrhh.planificacion.evaluacion;

import java.io.Serializable;

public class NoEvaluacionPK
  implements Serializable
{
  public long idNoEvaluacion;

  public NoEvaluacionPK()
  {
  }

  public NoEvaluacionPK(long idNoEvaluacion)
  {
    this.idNoEvaluacion = idNoEvaluacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((NoEvaluacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(NoEvaluacionPK thatPK)
  {
    return 
      this.idNoEvaluacion == thatPK.idNoEvaluacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idNoEvaluacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idNoEvaluacion);
  }
}