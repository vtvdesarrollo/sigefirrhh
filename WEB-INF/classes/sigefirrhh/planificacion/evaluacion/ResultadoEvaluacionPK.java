package sigefirrhh.planificacion.evaluacion;

import java.io.Serializable;

public class ResultadoEvaluacionPK
  implements Serializable
{
  public long idResultadoEvaluacion;

  public ResultadoEvaluacionPK()
  {
  }

  public ResultadoEvaluacionPK(long idResultadoEvaluacion)
  {
    this.idResultadoEvaluacion = idResultadoEvaluacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ResultadoEvaluacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ResultadoEvaluacionPK thatPK)
  {
    return 
      this.idResultadoEvaluacion == thatPK.idResultadoEvaluacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idResultadoEvaluacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idResultadoEvaluacion);
  }
}