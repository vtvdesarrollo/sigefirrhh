package sigefirrhh.planificacion.evaluacion;

import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.CategoriaPersonal;
import sigefirrhh.base.definiciones.ClasificacionPersonal;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.login.LoginSession;

public class ReportNotificacionEvaluacionesForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportNotificacionEvaluacionesForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private Collection colRegion;
  private long idRegion;
  private String tipo = "R";
  private String selectIdTipoPersonal;
  private Collection listTipoPersonal;
  private long idTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private EvaluacionNoGenFacade evaluacionFacade = new EvaluacionNoGenFacade();
  private LoginSession login;
  private boolean show;
  private boolean auxShow;
  private Date fechaEmision;
  private int anio;
  private int mes;
  private TipoPersonal tipoPersonal;

  public ReportNotificacionEvaluacionesForm()
  {
    this.reportName = "notificacionempleado";
    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.definicionesFacade = new DefinicionesNoGenFacade();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.anio = (new Date().getYear() + 1900);
    this.mes = (new Date().getMonth() + 1);
    this.fechaEmision = new Date();

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportNotificacionEvaluacionesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void changeTipoPersonal(ValueChangeEvent event)
  {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(this.idTipoPersonal);
    try {
      if (this.idTipoPersonal > 0L)
        this.tipoPersonal = this.definicionesFacade.findTipoPersonalById(this.idTipoPersonal);
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  private void cambiarNombreAReporte()
  {
    if (this.tipoPersonal != null) {
      if (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2"))
        this.reportName = "notificacionobrero";
      else {
        this.reportName = "notificacionempleado";
      }
    }
    if (this.idRegion != 0L) {
      this.reportName += "reg";
    }
    if (this.tipo.equals("R"))
      this.reportName += "real";
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), " tp.aumento_evaluacion = 'S' ");

      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(this.login.getIdOrganismo());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);

      this.listTipoPersonal = new ArrayList();
      this.colRegion = new ArrayList();
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
    parameters.put("anio_periodo", new Integer(this.anio));
    parameters.put("mes_periodo", new Integer(this.mes));
    parameters.put("dia", new Integer(this.fechaEmision.getDate()));
    parameters.put("mes", new Integer(this.fechaEmision.getMonth() + 1));
    parameters.put("anio", new Integer(this.fechaEmision.getYear() + 1900));
    if (this.mes == 12)
      parameters.put("periodo", "SEGUNDO SEMESTRE");
    else {
      parameters.put("periodo", "PRIMER SEMESTRE");
    }
    if (this.tipoPersonal != null) {
      if (this.tipoPersonal.getClasificacionPersonal().getCategoriaPersonal().getCodCategoria().equals("2"))
        this.reportName = "notificacionobrero";
      else {
        this.reportName = "notificacionempleado";
      }
    }
    if (this.idRegion != 0L) {
      this.reportName += "reg";
      parameters.put("id_region", new Long(this.idRegion));
    }

    if (this.tipo.equals("R")) {
      this.reportName += "real";
    }

    JasperForWeb report = new JasperForWeb();

    report.setType(1);

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/evaluacion");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public boolean isShow()
  {
    return this.auxShow;
  }

  public int getTipoReporte()
  {
    return this.tipoReporte;
  }

  public void setTipoReporte(int i)
  {
    this.tipoReporte = i;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }
  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public long getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(long idRegion) {
    this.idRegion = idRegion;
  }

  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }

  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public Date getFechaEmision() {
    return this.fechaEmision;
  }

  public void setFechaEmision(Date fechaEmision) {
    this.fechaEmision = fechaEmision;
  }
  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectIdTipoPersonal()
  {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }
}