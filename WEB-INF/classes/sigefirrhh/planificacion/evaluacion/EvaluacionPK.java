package sigefirrhh.planificacion.evaluacion;

import java.io.Serializable;

public class EvaluacionPK
  implements Serializable
{
  public long idEvaluacion;

  public EvaluacionPK()
  {
  }

  public EvaluacionPK(long idEvaluacion)
  {
    this.idEvaluacion = idEvaluacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((EvaluacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(EvaluacionPK thatPK)
  {
    return 
      this.idEvaluacion == thatPK.idEvaluacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idEvaluacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idEvaluacion);
  }
}