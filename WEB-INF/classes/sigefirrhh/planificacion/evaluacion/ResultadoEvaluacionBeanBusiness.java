package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class ResultadoEvaluacionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ResultadoEvaluacion resultadoEvaluacionNew = 
      (ResultadoEvaluacion)BeanUtils.cloneBean(
      resultadoEvaluacion);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (resultadoEvaluacionNew.getTipoPersonal() != null) {
      resultadoEvaluacionNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        resultadoEvaluacionNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(resultadoEvaluacionNew);
  }

  public void updateResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion) throws Exception
  {
    ResultadoEvaluacion resultadoEvaluacionModify = 
      findResultadoEvaluacionById(resultadoEvaluacion.getIdResultadoEvaluacion());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (resultadoEvaluacion.getTipoPersonal() != null) {
      resultadoEvaluacion.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        resultadoEvaluacion.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(resultadoEvaluacionModify, resultadoEvaluacion);
  }

  public void deleteResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ResultadoEvaluacion resultadoEvaluacionDelete = 
      findResultadoEvaluacionById(resultadoEvaluacion.getIdResultadoEvaluacion());
    pm.deletePersistent(resultadoEvaluacionDelete);
  }

  public ResultadoEvaluacion findResultadoEvaluacionById(long idResultadoEvaluacion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idResultadoEvaluacion == pIdResultadoEvaluacion";
    Query query = pm.newQuery(ResultadoEvaluacion.class, filter);

    query.declareParameters("long pIdResultadoEvaluacion");

    parameters.put("pIdResultadoEvaluacion", new Long(idResultadoEvaluacion));

    Collection colResultadoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colResultadoEvaluacion.iterator();
    ResultadoEvaluacion result = iterator.hasNext() ? (ResultadoEvaluacion)iterator.next() : null;
    return result;
  }

  public Collection findResultadoEvaluacionAll() throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Extent resultadoEvaluacionExtent = pm.getExtent(
      ResultadoEvaluacion.class, true);
    Query query = pm.newQuery(resultadoEvaluacionExtent);
    query.setOrdering("rangoMinimo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(ResultadoEvaluacion.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("rangoMinimo ascending");

    Collection colResultadoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResultadoEvaluacion);

    return colResultadoEvaluacion;
  }
}