package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;
import org.apache.log4j.Logger;

public class EvaluacionFacade extends AbstractFacade
  implements Serializable
{
  static Logger log = Logger.getLogger(EvaluacionFacade.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private EvaluacionBusiness evaluacionBusiness = new EvaluacionBusiness();

  public void addConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.evaluacionBusiness.addConceptoEvaluacion(conceptoEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.updateConceptoEvaluacion(conceptoEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteConceptoEvaluacion(ConceptoEvaluacion conceptoEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.deleteConceptoEvaluacion(conceptoEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ConceptoEvaluacion findConceptoEvaluacionById(long conceptoEvaluacionId) throws Exception
  {
    try { this.txn.open();
      ConceptoEvaluacion conceptoEvaluacion = 
        this.evaluacionBusiness.findConceptoEvaluacionById(conceptoEvaluacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(conceptoEvaluacion);
      return conceptoEvaluacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllConceptoEvaluacion() throws Exception
  {
    try { this.txn.open();
      return this.evaluacionBusiness.findAllConceptoEvaluacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConceptoEvaluacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionBusiness.findConceptoEvaluacionByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addAccionEvaluacion(AccionEvaluacion accionEvaluacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.evaluacionBusiness.addAccionEvaluacion(accionEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateAccionEvaluacion(AccionEvaluacion accionEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.updateAccionEvaluacion(accionEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteAccionEvaluacion(AccionEvaluacion accionEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.deleteAccionEvaluacion(accionEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public AccionEvaluacion findAccionEvaluacionById(long accionEvaluacionId) throws Exception
  {
    try { this.txn.open();
      AccionEvaluacion accionEvaluacion = 
        this.evaluacionBusiness.findAccionEvaluacionById(accionEvaluacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(accionEvaluacion);
      return accionEvaluacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllAccionEvaluacion() throws Exception
  {
    try { this.txn.open();
      return this.evaluacionBusiness.findAllAccionEvaluacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionEvaluacionByCodAccionEvaluacion(String codAccionEvaluacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionBusiness.findAccionEvaluacionByCodAccionEvaluacion(codAccionEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAccionEvaluacionByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionBusiness.findAccionEvaluacionByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addEvaluacion(Evaluacion evaluacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.evaluacionBusiness.addEvaluacion(evaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateEvaluacion(Evaluacion evaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.updateEvaluacion(evaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteEvaluacion(Evaluacion evaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.deleteEvaluacion(evaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Evaluacion findEvaluacionById(long evaluacionId) throws Exception
  {
    try { this.txn.open();
      Evaluacion evaluacion = this.evaluacionBusiness.findEvaluacionById(evaluacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(evaluacion);
      log.error("************adentro 1*************");
      return evaluacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllEvaluacion() throws Exception
  {
    try { this.txn.open();
      return this.evaluacionBusiness.findAllEvaluacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findEvaluacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionBusiness.findEvaluacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.evaluacionBusiness.addResultadoEvaluacion(resultadoEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.updateResultadoEvaluacion(resultadoEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteResultadoEvaluacion(ResultadoEvaluacion resultadoEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.deleteResultadoEvaluacion(resultadoEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ResultadoEvaluacion findResultadoEvaluacionById(long resultadoEvaluacionId) throws Exception
  {
    try { this.txn.open();
      ResultadoEvaluacion resultadoEvaluacion = 
        this.evaluacionBusiness.findResultadoEvaluacionById(resultadoEvaluacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(resultadoEvaluacion);
      return resultadoEvaluacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllResultadoEvaluacion() throws Exception
  {
    try { this.txn.open();
      return this.evaluacionBusiness.findAllResultadoEvaluacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findResultadoEvaluacionByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionBusiness.findResultadoEvaluacionByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addNoEvaluacion(NoEvaluacion noEvaluacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.evaluacionBusiness.addNoEvaluacion(noEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateNoEvaluacion(NoEvaluacion noEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.updateNoEvaluacion(noEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteNoEvaluacion(NoEvaluacion noEvaluacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.deleteNoEvaluacion(noEvaluacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public NoEvaluacion findNoEvaluacionById(long noEvaluacionId) throws Exception
  {
    try { this.txn.open();
      NoEvaluacion noEvaluacion = 
        this.evaluacionBusiness.findNoEvaluacionById(noEvaluacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(noEvaluacion);
      return noEvaluacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllNoEvaluacion() throws Exception
  {
    try { this.txn.open();
      return this.evaluacionBusiness.findAllNoEvaluacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findNoEvaluacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionBusiness.findNoEvaluacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addApelacion(Apelacion apelacion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.evaluacionBusiness.addApelacion(apelacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateApelacion(Apelacion apelacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.updateApelacion(apelacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteApelacion(Apelacion apelacion) throws Exception
  {
    try { this.txn.open();
      this.evaluacionBusiness.deleteApelacion(apelacion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Apelacion findApelacionById(long apelacionId) throws Exception
  {
    try { this.txn.open();
      Apelacion apelacion = 
        this.evaluacionBusiness.findApelacionById(apelacionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(apelacion);
      return apelacion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllApelacion() throws Exception
  {
    try { this.txn.open();
      return this.evaluacionBusiness.findAllApelacion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findApelacionByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.evaluacionBusiness.findApelacionByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}