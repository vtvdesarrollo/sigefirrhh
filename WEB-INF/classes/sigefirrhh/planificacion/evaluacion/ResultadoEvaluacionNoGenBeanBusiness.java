package sigefirrhh.planificacion.evaluacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class ResultadoEvaluacionNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByTipoPersonalAndRango(long idTipoPersonal, double puntos)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && rangoMinimo <= pPuntos && rangoMaximo >= pPuntos";

    Query query = pm.newQuery(ResultadoEvaluacion.class, filter);

    query.declareParameters("long pIdTipoPersonal, double pPuntos");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pPuntos", new Double(puntos));

    query.setOrdering("rangoMinimo ascending");

    Collection colResultadoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResultadoEvaluacion);

    return colResultadoEvaluacion;
  }

  public Collection findByTipoPersonalAndAnio(long idTipoPersonal, int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal && anio == pAnio";

    Query query = pm.newQuery(ResultadoEvaluacion.class, filter);

    query.declareParameters("long pIdTipoPersonal, int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));
    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("rangoMinimo ascending");

    Collection colResultadoEvaluacion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colResultadoEvaluacion);

    return colResultadoEvaluacion;
  }

  public ResultSet findByIdTipoPersonalAnioRango(long idTipoPersonal, int anio, double puntos)
    throws Exception
  {
    Connection connection = null;

    StringBuffer sql = new StringBuffer();

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select *   ");
      sql.append(" from resultadoevaluacion ");
      sql.append(" where id_tipo_personal = ?");
      sql.append(" and rango_minimo <= ? and rango_maximo >= ? limit 1");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idTipoPersonal);
      stRegistros.setDouble(2, puntos);
      stRegistros.setDouble(3, puntos);
      rsRegistros = stRegistros.executeQuery();

      if (rsRegistros.next()) {
        return rsRegistros;
      }
      return null;
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException6) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException7) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException8)
        {
        }
    }
  }
}