package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class PlanJubilacionPK
  implements Serializable
{
  public long idPlanJubilacion;

  public PlanJubilacionPK()
  {
  }

  public PlanJubilacionPK(long idPlanJubilacion)
  {
    this.idPlanJubilacion = idPlanJubilacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanJubilacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanJubilacionPK thatPK)
  {
    return 
      this.idPlanJubilacion == thatPK.idPlanJubilacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanJubilacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanJubilacion);
  }
}