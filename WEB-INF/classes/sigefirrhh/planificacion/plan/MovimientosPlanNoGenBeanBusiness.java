package sigefirrhh.planificacion.plan;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MovimientosPlanNoGenBeanBusiness extends MovimientosPlanBeanBusiness
  implements Serializable
{
  public long validarMovimiento(long idCausaMovimiento)
    throws Exception
  {
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select *  ");
      sql.append(" from movimientosplan mp, planpersonal pp");
      sql.append(" where mp.id_causa_movimiento = ?");
      sql.append(" and mp.id_plan_personal = pp.id_plan_personal");
      sql.append(" and pp.estatus = '3'");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idCausaMovimiento);
      rsRegistros = stRegistros.executeQuery();

      if (rsRegistros.next()) {
        int cantidad = 0;
        cantidad = rsRegistros.getInt("cantidad_planificados") - (rsRegistros.getInt("cantidad_realizados") - rsRegistros.getInt("cantidad_devueltos"));
        if (cantidad > 0)
          return rsRegistros.getLong("id_movimientos_plan");
        int cantidad;
        return 0L;
      }

      return 0L;
    }
    finally
    {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException9) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException10) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException11)
        {
        }
    }
  }
}