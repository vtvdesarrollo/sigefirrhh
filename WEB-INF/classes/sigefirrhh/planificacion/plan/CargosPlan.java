package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.registro.MovimientoCargo;

public class CargosPlan
  implements Serializable, PersistenceCapable
{
  private long idCargosPlan;
  private PlanPersonal planPersonal;
  private MovimientoCargo movimientoCargo;
  private int cantidadPlanificados;
  private int cantidadRealizados;
  private int cantidadAprobados;
  private int cantidadDevueltos;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cantidadAprobados", "cantidadDevueltos", "cantidadPlanificados", "cantidadRealizados", "idCargosPlan", "movimientoCargo", "planPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.registro.MovimientoCargo"), sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public CargosPlan()
  {
    jdoSetcantidadPlanificados(this, 0);

    jdoSetcantidadRealizados(this, 0);

    jdoSetcantidadAprobados(this, 0);

    jdoSetcantidadDevueltos(this, 0);
  }

  public String toString() {
    return jdoGetmovimientoCargo(this) + " Planificados: " + jdoGetcantidadPlanificados(this) + " Realizados: " + jdoGetcantidadRealizados(this);
  }

  public int getCantidadAprobados()
  {
    return jdoGetcantidadAprobados(this);
  }

  public void setCantidadAprobados(int cantidadAprobados)
  {
    jdoSetcantidadAprobados(this, cantidadAprobados);
  }

  public int getCantidadDevueltos()
  {
    return jdoGetcantidadDevueltos(this);
  }

  public void setCantidadDevueltos(int cantidadDevueltos)
  {
    jdoSetcantidadDevueltos(this, cantidadDevueltos);
  }

  public int getCantidadPlanificados()
  {
    return jdoGetcantidadPlanificados(this);
  }

  public void setCantidadPlanificados(int cantidadPlanificados)
  {
    jdoSetcantidadPlanificados(this, cantidadPlanificados);
  }

  public int getCantidadRealizados()
  {
    return jdoGetcantidadRealizados(this);
  }

  public void setCantidadRealizados(int cantidadRealizados)
  {
    jdoSetcantidadRealizados(this, cantidadRealizados);
  }

  public long getIdCargosPlan()
  {
    return jdoGetidCargosPlan(this);
  }

  public void setIdCargosPlan(long idCargosPlan)
  {
    jdoSetidCargosPlan(this, idCargosPlan);
  }

  public MovimientoCargo getMovimientoCargo()
  {
    return jdoGetmovimientoCargo(this);
  }

  public void setMovimientoCargo(MovimientoCargo movimientoCargo)
  {
    jdoSetmovimientoCargo(this, movimientoCargo);
  }

  public PlanPersonal getPlanPersonal()
  {
    return jdoGetplanPersonal(this);
  }

  public void setPlanPersonal(PlanPersonal planPersonal)
  {
    jdoSetplanPersonal(this, planPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.CargosPlan"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CargosPlan());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CargosPlan localCargosPlan = new CargosPlan();
    localCargosPlan.jdoFlags = 1;
    localCargosPlan.jdoStateManager = paramStateManager;
    return localCargosPlan;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CargosPlan localCargosPlan = new CargosPlan();
    localCargosPlan.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCargosPlan.jdoFlags = 1;
    localCargosPlan.jdoStateManager = paramStateManager;
    return localCargosPlan;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadAprobados);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadDevueltos);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadPlanificados);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadRealizados);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCargosPlan);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.movimientoCargo);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadAprobados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadDevueltos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadPlanificados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadRealizados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCargosPlan = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.movimientoCargo = ((MovimientoCargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPersonal = ((PlanPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CargosPlan paramCargosPlan, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCargosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadAprobados = paramCargosPlan.cantidadAprobados;
      return;
    case 1:
      if (paramCargosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadDevueltos = paramCargosPlan.cantidadDevueltos;
      return;
    case 2:
      if (paramCargosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadPlanificados = paramCargosPlan.cantidadPlanificados;
      return;
    case 3:
      if (paramCargosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadRealizados = paramCargosPlan.cantidadRealizados;
      return;
    case 4:
      if (paramCargosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.idCargosPlan = paramCargosPlan.idCargosPlan;
      return;
    case 5:
      if (paramCargosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.movimientoCargo = paramCargosPlan.movimientoCargo;
      return;
    case 6:
      if (paramCargosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.planPersonal = paramCargosPlan.planPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CargosPlan))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CargosPlan localCargosPlan = (CargosPlan)paramObject;
    if (localCargosPlan.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCargosPlan, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CargosPlanPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CargosPlanPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CargosPlanPK))
      throw new IllegalArgumentException("arg1");
    CargosPlanPK localCargosPlanPK = (CargosPlanPK)paramObject;
    localCargosPlanPK.idCargosPlan = this.idCargosPlan;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CargosPlanPK))
      throw new IllegalArgumentException("arg1");
    CargosPlanPK localCargosPlanPK = (CargosPlanPK)paramObject;
    this.idCargosPlan = localCargosPlanPK.idCargosPlan;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CargosPlanPK))
      throw new IllegalArgumentException("arg2");
    CargosPlanPK localCargosPlanPK = (CargosPlanPK)paramObject;
    localCargosPlanPK.idCargosPlan = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CargosPlanPK))
      throw new IllegalArgumentException("arg2");
    CargosPlanPK localCargosPlanPK = (CargosPlanPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localCargosPlanPK.idCargosPlan);
  }

  private static final int jdoGetcantidadAprobados(CargosPlan paramCargosPlan)
  {
    if (paramCargosPlan.jdoFlags <= 0)
      return paramCargosPlan.cantidadAprobados;
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramCargosPlan.cantidadAprobados;
    if (localStateManager.isLoaded(paramCargosPlan, jdoInheritedFieldCount + 0))
      return paramCargosPlan.cantidadAprobados;
    return localStateManager.getIntField(paramCargosPlan, jdoInheritedFieldCount + 0, paramCargosPlan.cantidadAprobados);
  }

  private static final void jdoSetcantidadAprobados(CargosPlan paramCargosPlan, int paramInt)
  {
    if (paramCargosPlan.jdoFlags == 0)
    {
      paramCargosPlan.cantidadAprobados = paramInt;
      return;
    }
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargosPlan.cantidadAprobados = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargosPlan, jdoInheritedFieldCount + 0, paramCargosPlan.cantidadAprobados, paramInt);
  }

  private static final int jdoGetcantidadDevueltos(CargosPlan paramCargosPlan)
  {
    if (paramCargosPlan.jdoFlags <= 0)
      return paramCargosPlan.cantidadDevueltos;
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramCargosPlan.cantidadDevueltos;
    if (localStateManager.isLoaded(paramCargosPlan, jdoInheritedFieldCount + 1))
      return paramCargosPlan.cantidadDevueltos;
    return localStateManager.getIntField(paramCargosPlan, jdoInheritedFieldCount + 1, paramCargosPlan.cantidadDevueltos);
  }

  private static final void jdoSetcantidadDevueltos(CargosPlan paramCargosPlan, int paramInt)
  {
    if (paramCargosPlan.jdoFlags == 0)
    {
      paramCargosPlan.cantidadDevueltos = paramInt;
      return;
    }
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargosPlan.cantidadDevueltos = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargosPlan, jdoInheritedFieldCount + 1, paramCargosPlan.cantidadDevueltos, paramInt);
  }

  private static final int jdoGetcantidadPlanificados(CargosPlan paramCargosPlan)
  {
    if (paramCargosPlan.jdoFlags <= 0)
      return paramCargosPlan.cantidadPlanificados;
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramCargosPlan.cantidadPlanificados;
    if (localStateManager.isLoaded(paramCargosPlan, jdoInheritedFieldCount + 2))
      return paramCargosPlan.cantidadPlanificados;
    return localStateManager.getIntField(paramCargosPlan, jdoInheritedFieldCount + 2, paramCargosPlan.cantidadPlanificados);
  }

  private static final void jdoSetcantidadPlanificados(CargosPlan paramCargosPlan, int paramInt)
  {
    if (paramCargosPlan.jdoFlags == 0)
    {
      paramCargosPlan.cantidadPlanificados = paramInt;
      return;
    }
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargosPlan.cantidadPlanificados = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargosPlan, jdoInheritedFieldCount + 2, paramCargosPlan.cantidadPlanificados, paramInt);
  }

  private static final int jdoGetcantidadRealizados(CargosPlan paramCargosPlan)
  {
    if (paramCargosPlan.jdoFlags <= 0)
      return paramCargosPlan.cantidadRealizados;
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramCargosPlan.cantidadRealizados;
    if (localStateManager.isLoaded(paramCargosPlan, jdoInheritedFieldCount + 3))
      return paramCargosPlan.cantidadRealizados;
    return localStateManager.getIntField(paramCargosPlan, jdoInheritedFieldCount + 3, paramCargosPlan.cantidadRealizados);
  }

  private static final void jdoSetcantidadRealizados(CargosPlan paramCargosPlan, int paramInt)
  {
    if (paramCargosPlan.jdoFlags == 0)
    {
      paramCargosPlan.cantidadRealizados = paramInt;
      return;
    }
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargosPlan.cantidadRealizados = paramInt;
      return;
    }
    localStateManager.setIntField(paramCargosPlan, jdoInheritedFieldCount + 3, paramCargosPlan.cantidadRealizados, paramInt);
  }

  private static final long jdoGetidCargosPlan(CargosPlan paramCargosPlan)
  {
    return paramCargosPlan.idCargosPlan;
  }

  private static final void jdoSetidCargosPlan(CargosPlan paramCargosPlan, long paramLong)
  {
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargosPlan.idCargosPlan = paramLong;
      return;
    }
    localStateManager.setLongField(paramCargosPlan, jdoInheritedFieldCount + 4, paramCargosPlan.idCargosPlan, paramLong);
  }

  private static final MovimientoCargo jdoGetmovimientoCargo(CargosPlan paramCargosPlan)
  {
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramCargosPlan.movimientoCargo;
    if (localStateManager.isLoaded(paramCargosPlan, jdoInheritedFieldCount + 5))
      return paramCargosPlan.movimientoCargo;
    return (MovimientoCargo)localStateManager.getObjectField(paramCargosPlan, jdoInheritedFieldCount + 5, paramCargosPlan.movimientoCargo);
  }

  private static final void jdoSetmovimientoCargo(CargosPlan paramCargosPlan, MovimientoCargo paramMovimientoCargo)
  {
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargosPlan.movimientoCargo = paramMovimientoCargo;
      return;
    }
    localStateManager.setObjectField(paramCargosPlan, jdoInheritedFieldCount + 5, paramCargosPlan.movimientoCargo, paramMovimientoCargo);
  }

  private static final PlanPersonal jdoGetplanPersonal(CargosPlan paramCargosPlan)
  {
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramCargosPlan.planPersonal;
    if (localStateManager.isLoaded(paramCargosPlan, jdoInheritedFieldCount + 6))
      return paramCargosPlan.planPersonal;
    return (PlanPersonal)localStateManager.getObjectField(paramCargosPlan, jdoInheritedFieldCount + 6, paramCargosPlan.planPersonal);
  }

  private static final void jdoSetplanPersonal(CargosPlan paramCargosPlan, PlanPersonal paramPlanPersonal)
  {
    StateManager localStateManager = paramCargosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramCargosPlan.planPersonal = paramPlanPersonal;
      return;
    }
    localStateManager.setObjectField(paramCargosPlan, jdoInheritedFieldCount + 6, paramCargosPlan.planPersonal, paramPlanPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}