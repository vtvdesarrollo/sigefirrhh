package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Actividad
  implements Serializable, PersistenceCapable
{
  private long idActividad;
  private AccionObjetivo accionObjetivo;
  private int numero;
  private String enunciado;
  private String descripcion;
  private Date fechaInicio;
  private Date fechaFin;
  private double costoRrhh;
  private double costoMateriales;
  private double costoFinanciero;
  private double primerTrimPlan;
  private double segundoTrimPlan;
  private double tercerTrimPlan;
  private double cuartoTrimPlan;
  private double primerTrimReal;
  private double segundoTrimReal;
  private double tercerTrimReal;
  private double cuartoTrimReal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "accionObjetivo", "costoFinanciero", "costoMateriales", "costoRrhh", "cuartoTrimPlan", "cuartoTrimReal", "descripcion", "enunciado", "fechaFin", "fechaInicio", "idActividad", "numero", "primerTrimPlan", "primerTrimReal", "segundoTrimPlan", "segundoTrimReal", "tercerTrimPlan", "tercerTrimReal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.plan.AccionObjetivo"), Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, Double.TYPE };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Actividad()
  {
    jdoSetnumero(this, 1);

    jdoSetcostoRrhh(this, 0.0D);

    jdoSetcostoMateriales(this, 0.0D);

    jdoSetcostoFinanciero(this, 0.0D);

    jdoSetprimerTrimPlan(this, 0.0D);

    jdoSetsegundoTrimPlan(this, 0.0D);

    jdoSettercerTrimPlan(this, 0.0D);

    jdoSetcuartoTrimPlan(this, 0.0D);

    jdoSetprimerTrimReal(this, 0.0D);

    jdoSetsegundoTrimReal(this, 0.0D);

    jdoSettercerTrimReal(this, 0.0D);

    jdoSetcuartoTrimReal(this, 0.0D);
  }

  public String toString() {
    return jdoGetenunciado(this);
  }

  public AccionObjetivo getAccionObjetivo() {
    return jdoGetaccionObjetivo(this);
  }
  public void setAccionObjetivo(AccionObjetivo accionObjetivo) {
    jdoSetaccionObjetivo(this, accionObjetivo);
  }
  public double getCostoFinanciero() {
    return jdoGetcostoFinanciero(this);
  }
  public void setCostoFinanciero(double costoFinanciero) {
    jdoSetcostoFinanciero(this, costoFinanciero);
  }
  public double getCostoMateriales() {
    return jdoGetcostoMateriales(this);
  }
  public void setCostoMateriales(double costoMateriales) {
    jdoSetcostoMateriales(this, costoMateriales);
  }
  public double getCostoRrhh() {
    return jdoGetcostoRrhh(this);
  }
  public void setCostoRrhh(double costoRrhh) {
    jdoSetcostoRrhh(this, costoRrhh);
  }
  public double getCuartoTrimPlan() {
    return jdoGetcuartoTrimPlan(this);
  }
  public void setCuartoTrimPlan(double cuartoTrimPlan) {
    jdoSetcuartoTrimPlan(this, cuartoTrimPlan);
  }
  public double getCuartoTrimReal() {
    return jdoGetcuartoTrimReal(this);
  }
  public void setCuartoTrimReal(double cuartoTrimReal) {
    jdoSetcuartoTrimReal(this, cuartoTrimReal);
  }
  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }
  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }
  public String getEnunciado() {
    return jdoGetenunciado(this);
  }
  public void setEnunciado(String enunciado) {
    jdoSetenunciado(this, enunciado);
  }
  public Date getFechaFin() {
    return jdoGetfechaFin(this);
  }
  public void setFechaFin(Date fechaFin) {
    jdoSetfechaFin(this, fechaFin);
  }
  public Date getFechaInicio() {
    return jdoGetfechaInicio(this);
  }
  public void setFechaInicio(Date fechaInicio) {
    jdoSetfechaInicio(this, fechaInicio);
  }
  public long getIdActividad() {
    return jdoGetidActividad(this);
  }
  public void setIdActividad(long idActividad) {
    jdoSetidActividad(this, idActividad);
  }
  public int getNumero() {
    return jdoGetnumero(this);
  }
  public void setNumero(int numero) {
    jdoSetnumero(this, numero);
  }
  public double getPrimerTrimPlan() {
    return jdoGetprimerTrimPlan(this);
  }
  public void setPrimerTrimPlan(double primerTrimPlan) {
    jdoSetprimerTrimPlan(this, primerTrimPlan);
  }
  public double getPrimerTrimReal() {
    return jdoGetprimerTrimReal(this);
  }
  public void setPrimerTrimReal(double primerTrimReal) {
    jdoSetprimerTrimReal(this, primerTrimReal);
  }
  public double getSegundoTrimPlan() {
    return jdoGetsegundoTrimPlan(this);
  }
  public void setSegundoTrimPlan(double segundoTrimPlan) {
    jdoSetsegundoTrimPlan(this, segundoTrimPlan);
  }
  public double getSegundoTrimReal() {
    return jdoGetsegundoTrimReal(this);
  }
  public void setSegundoTrimReal(double segundoTrimReal) {
    jdoSetsegundoTrimReal(this, segundoTrimReal);
  }
  public double getTercerTrimPlan() {
    return jdoGettercerTrimPlan(this);
  }
  public void setTercerTrimPlan(double tercerTrimPlan) {
    jdoSettercerTrimPlan(this, tercerTrimPlan);
  }
  public double getTercerTrimReal() {
    return jdoGettercerTrimReal(this);
  }
  public void setTercerTrimReal(double tercerTrimReal) {
    jdoSettercerTrimReal(this, tercerTrimReal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 18;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.Actividad"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Actividad());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Actividad localActividad = new Actividad();
    localActividad.jdoFlags = 1;
    localActividad.jdoStateManager = paramStateManager;
    return localActividad;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Actividad localActividad = new Actividad();
    localActividad.jdoCopyKeyFieldsFromObjectId(paramObject);
    localActividad.jdoFlags = 1;
    localActividad.jdoStateManager = paramStateManager;
    return localActividad;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.accionObjetivo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoFinanciero);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoMateriales);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoRrhh);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.cuartoTrimPlan);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.cuartoTrimReal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.enunciado);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idActividad);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numero);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primerTrimPlan);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primerTrimReal);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.segundoTrimPlan);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.segundoTrimReal);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tercerTrimPlan);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.tercerTrimReal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accionObjetivo = ((AccionObjetivo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoFinanciero = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoMateriales = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoRrhh = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuartoTrimPlan = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cuartoTrimReal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.enunciado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idActividad = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerTrimPlan = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerTrimReal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoTrimPlan = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoTrimReal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tercerTrimPlan = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tercerTrimReal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Actividad paramActividad, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.accionObjetivo = paramActividad.accionObjetivo;
      return;
    case 1:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.costoFinanciero = paramActividad.costoFinanciero;
      return;
    case 2:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.costoMateriales = paramActividad.costoMateriales;
      return;
    case 3:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.costoRrhh = paramActividad.costoRrhh;
      return;
    case 4:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.cuartoTrimPlan = paramActividad.cuartoTrimPlan;
      return;
    case 5:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.cuartoTrimReal = paramActividad.cuartoTrimReal;
      return;
    case 6:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramActividad.descripcion;
      return;
    case 7:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.enunciado = paramActividad.enunciado;
      return;
    case 8:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramActividad.fechaFin;
      return;
    case 9:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramActividad.fechaInicio;
      return;
    case 10:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.idActividad = paramActividad.idActividad;
      return;
    case 11:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramActividad.numero;
      return;
    case 12:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.primerTrimPlan = paramActividad.primerTrimPlan;
      return;
    case 13:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.primerTrimReal = paramActividad.primerTrimReal;
      return;
    case 14:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.segundoTrimPlan = paramActividad.segundoTrimPlan;
      return;
    case 15:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.segundoTrimReal = paramActividad.segundoTrimReal;
      return;
    case 16:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.tercerTrimPlan = paramActividad.tercerTrimPlan;
      return;
    case 17:
      if (paramActividad == null)
        throw new IllegalArgumentException("arg1");
      this.tercerTrimReal = paramActividad.tercerTrimReal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Actividad))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Actividad localActividad = (Actividad)paramObject;
    if (localActividad.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localActividad, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ActividadPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ActividadPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ActividadPK))
      throw new IllegalArgumentException("arg1");
    ActividadPK localActividadPK = (ActividadPK)paramObject;
    localActividadPK.idActividad = this.idActividad;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ActividadPK))
      throw new IllegalArgumentException("arg1");
    ActividadPK localActividadPK = (ActividadPK)paramObject;
    this.idActividad = localActividadPK.idActividad;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ActividadPK))
      throw new IllegalArgumentException("arg2");
    ActividadPK localActividadPK = (ActividadPK)paramObject;
    localActividadPK.idActividad = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ActividadPK))
      throw new IllegalArgumentException("arg2");
    ActividadPK localActividadPK = (ActividadPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localActividadPK.idActividad);
  }

  private static final AccionObjetivo jdoGetaccionObjetivo(Actividad paramActividad)
  {
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.accionObjetivo;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 0))
      return paramActividad.accionObjetivo;
    return (AccionObjetivo)localStateManager.getObjectField(paramActividad, jdoInheritedFieldCount + 0, paramActividad.accionObjetivo);
  }

  private static final void jdoSetaccionObjetivo(Actividad paramActividad, AccionObjetivo paramAccionObjetivo)
  {
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.accionObjetivo = paramAccionObjetivo;
      return;
    }
    localStateManager.setObjectField(paramActividad, jdoInheritedFieldCount + 0, paramActividad.accionObjetivo, paramAccionObjetivo);
  }

  private static final double jdoGetcostoFinanciero(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.costoFinanciero;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.costoFinanciero;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 1))
      return paramActividad.costoFinanciero;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 1, paramActividad.costoFinanciero);
  }

  private static final void jdoSetcostoFinanciero(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.costoFinanciero = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.costoFinanciero = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 1, paramActividad.costoFinanciero, paramDouble);
  }

  private static final double jdoGetcostoMateriales(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.costoMateriales;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.costoMateriales;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 2))
      return paramActividad.costoMateriales;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 2, paramActividad.costoMateriales);
  }

  private static final void jdoSetcostoMateriales(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.costoMateriales = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.costoMateriales = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 2, paramActividad.costoMateriales, paramDouble);
  }

  private static final double jdoGetcostoRrhh(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.costoRrhh;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.costoRrhh;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 3))
      return paramActividad.costoRrhh;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 3, paramActividad.costoRrhh);
  }

  private static final void jdoSetcostoRrhh(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.costoRrhh = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.costoRrhh = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 3, paramActividad.costoRrhh, paramDouble);
  }

  private static final double jdoGetcuartoTrimPlan(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.cuartoTrimPlan;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.cuartoTrimPlan;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 4))
      return paramActividad.cuartoTrimPlan;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 4, paramActividad.cuartoTrimPlan);
  }

  private static final void jdoSetcuartoTrimPlan(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.cuartoTrimPlan = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.cuartoTrimPlan = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 4, paramActividad.cuartoTrimPlan, paramDouble);
  }

  private static final double jdoGetcuartoTrimReal(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.cuartoTrimReal;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.cuartoTrimReal;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 5))
      return paramActividad.cuartoTrimReal;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 5, paramActividad.cuartoTrimReal);
  }

  private static final void jdoSetcuartoTrimReal(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.cuartoTrimReal = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.cuartoTrimReal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 5, paramActividad.cuartoTrimReal, paramDouble);
  }

  private static final String jdoGetdescripcion(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.descripcion;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.descripcion;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 6))
      return paramActividad.descripcion;
    return localStateManager.getStringField(paramActividad, jdoInheritedFieldCount + 6, paramActividad.descripcion);
  }

  private static final void jdoSetdescripcion(Actividad paramActividad, String paramString)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramActividad, jdoInheritedFieldCount + 6, paramActividad.descripcion, paramString);
  }

  private static final String jdoGetenunciado(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.enunciado;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.enunciado;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 7))
      return paramActividad.enunciado;
    return localStateManager.getStringField(paramActividad, jdoInheritedFieldCount + 7, paramActividad.enunciado);
  }

  private static final void jdoSetenunciado(Actividad paramActividad, String paramString)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.enunciado = paramString;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.enunciado = paramString;
      return;
    }
    localStateManager.setStringField(paramActividad, jdoInheritedFieldCount + 7, paramActividad.enunciado, paramString);
  }

  private static final Date jdoGetfechaFin(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.fechaFin;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.fechaFin;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 8))
      return paramActividad.fechaFin;
    return (Date)localStateManager.getObjectField(paramActividad, jdoInheritedFieldCount + 8, paramActividad.fechaFin);
  }

  private static final void jdoSetfechaFin(Actividad paramActividad, Date paramDate)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramActividad, jdoInheritedFieldCount + 8, paramActividad.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.fechaInicio;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.fechaInicio;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 9))
      return paramActividad.fechaInicio;
    return (Date)localStateManager.getObjectField(paramActividad, jdoInheritedFieldCount + 9, paramActividad.fechaInicio);
  }

  private static final void jdoSetfechaInicio(Actividad paramActividad, Date paramDate)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramActividad, jdoInheritedFieldCount + 9, paramActividad.fechaInicio, paramDate);
  }

  private static final long jdoGetidActividad(Actividad paramActividad)
  {
    return paramActividad.idActividad;
  }

  private static final void jdoSetidActividad(Actividad paramActividad, long paramLong)
  {
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.idActividad = paramLong;
      return;
    }
    localStateManager.setLongField(paramActividad, jdoInheritedFieldCount + 10, paramActividad.idActividad, paramLong);
  }

  private static final int jdoGetnumero(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.numero;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.numero;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 11))
      return paramActividad.numero;
    return localStateManager.getIntField(paramActividad, jdoInheritedFieldCount + 11, paramActividad.numero);
  }

  private static final void jdoSetnumero(Actividad paramActividad, int paramInt)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.numero = paramInt;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.numero = paramInt;
      return;
    }
    localStateManager.setIntField(paramActividad, jdoInheritedFieldCount + 11, paramActividad.numero, paramInt);
  }

  private static final double jdoGetprimerTrimPlan(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.primerTrimPlan;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.primerTrimPlan;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 12))
      return paramActividad.primerTrimPlan;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 12, paramActividad.primerTrimPlan);
  }

  private static final void jdoSetprimerTrimPlan(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.primerTrimPlan = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.primerTrimPlan = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 12, paramActividad.primerTrimPlan, paramDouble);
  }

  private static final double jdoGetprimerTrimReal(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.primerTrimReal;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.primerTrimReal;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 13))
      return paramActividad.primerTrimReal;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 13, paramActividad.primerTrimReal);
  }

  private static final void jdoSetprimerTrimReal(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.primerTrimReal = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.primerTrimReal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 13, paramActividad.primerTrimReal, paramDouble);
  }

  private static final double jdoGetsegundoTrimPlan(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.segundoTrimPlan;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.segundoTrimPlan;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 14))
      return paramActividad.segundoTrimPlan;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 14, paramActividad.segundoTrimPlan);
  }

  private static final void jdoSetsegundoTrimPlan(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.segundoTrimPlan = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.segundoTrimPlan = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 14, paramActividad.segundoTrimPlan, paramDouble);
  }

  private static final double jdoGetsegundoTrimReal(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.segundoTrimReal;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.segundoTrimReal;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 15))
      return paramActividad.segundoTrimReal;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 15, paramActividad.segundoTrimReal);
  }

  private static final void jdoSetsegundoTrimReal(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.segundoTrimReal = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.segundoTrimReal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 15, paramActividad.segundoTrimReal, paramDouble);
  }

  private static final double jdoGettercerTrimPlan(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.tercerTrimPlan;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.tercerTrimPlan;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 16))
      return paramActividad.tercerTrimPlan;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 16, paramActividad.tercerTrimPlan);
  }

  private static final void jdoSettercerTrimPlan(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.tercerTrimPlan = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.tercerTrimPlan = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 16, paramActividad.tercerTrimPlan, paramDouble);
  }

  private static final double jdoGettercerTrimReal(Actividad paramActividad)
  {
    if (paramActividad.jdoFlags <= 0)
      return paramActividad.tercerTrimReal;
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
      return paramActividad.tercerTrimReal;
    if (localStateManager.isLoaded(paramActividad, jdoInheritedFieldCount + 17))
      return paramActividad.tercerTrimReal;
    return localStateManager.getDoubleField(paramActividad, jdoInheritedFieldCount + 17, paramActividad.tercerTrimReal);
  }

  private static final void jdoSettercerTrimReal(Actividad paramActividad, double paramDouble)
  {
    if (paramActividad.jdoFlags == 0)
    {
      paramActividad.tercerTrimReal = paramDouble;
      return;
    }
    StateManager localStateManager = paramActividad.jdoStateManager;
    if (localStateManager == null)
    {
      paramActividad.tercerTrimReal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramActividad, jdoInheritedFieldCount + 17, paramActividad.tercerTrimReal, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}