package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class PlanPersonalPK
  implements Serializable
{
  public long idPlanPersonal;

  public PlanPersonalPK()
  {
  }

  public PlanPersonalPK(long idPlanPersonal)
  {
    this.idPlanPersonal = idPlanPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanPersonalPK thatPK)
  {
    return 
      this.idPlanPersonal == thatPK.idPlanPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanPersonal);
  }
}