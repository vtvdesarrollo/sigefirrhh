package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class PlanFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PlanBusiness planBusiness = new PlanBusiness();

  public void addContratosPlan(ContratosPlan contratosPlan)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.planBusiness.addContratosPlan(contratosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateContratosPlan(ContratosPlan contratosPlan) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.updateContratosPlan(contratosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteContratosPlan(ContratosPlan contratosPlan) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.deleteContratosPlan(contratosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ContratosPlan findContratosPlanById(long contratosPlanId) throws Exception
  {
    try { this.txn.open();
      ContratosPlan contratosPlan = 
        this.planBusiness.findContratosPlanById(contratosPlanId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(contratosPlan);
      return contratosPlan;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllContratosPlan() throws Exception
  {
    try { this.txn.open();
      return this.planBusiness.findAllContratosPlan();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findContratosPlanByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findContratosPlanByPlanPersonal(idPlanPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addMovimientosPlan(MovimientosPlan movimientosPlan)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.planBusiness.addMovimientosPlan(movimientosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateMovimientosPlan(MovimientosPlan movimientosPlan) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.updateMovimientosPlan(movimientosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteMovimientosPlan(MovimientosPlan movimientosPlan) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.deleteMovimientosPlan(movimientosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public MovimientosPlan findMovimientosPlanById(long movimientosPlanId) throws Exception
  {
    try { this.txn.open();
      MovimientosPlan movimientosPlan = 
        this.planBusiness.findMovimientosPlanById(movimientosPlanId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(movimientosPlan);
      return movimientosPlan;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllMovimientosPlan() throws Exception
  {
    try { this.txn.open();
      return this.planBusiness.findAllMovimientosPlan();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findMovimientosPlanByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findMovimientosPlanByPlanPersonal(idPlanPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPlanPersonal(PlanPersonal planPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.planBusiness.addPlanPersonal(planPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePlanPersonal(PlanPersonal planPersonal) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.updatePlanPersonal(planPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePlanPersonal(PlanPersonal planPersonal) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.deletePlanPersonal(planPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PlanPersonal findPlanPersonalById(long planPersonalId) throws Exception
  {
    try { this.txn.open();
      PlanPersonal planPersonal = 
        this.planBusiness.findPlanPersonalById(planPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(planPersonal);
      return planPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPlanPersonal() throws Exception
  {
    try { this.txn.open();
      return this.planBusiness.findAllPlanPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanPersonalByAnio(int anio, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findPlanPersonalByAnio(anio, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPlanPersonalByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findPlanPersonalByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addProyectoPersonal(ProyectoPersonal proyectoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.planBusiness.addProyectoPersonal(proyectoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateProyectoPersonal(ProyectoPersonal proyectoPersonal) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.updateProyectoPersonal(proyectoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteProyectoPersonal(ProyectoPersonal proyectoPersonal) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.deleteProyectoPersonal(proyectoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ProyectoPersonal findProyectoPersonalById(long proyectoPersonalId) throws Exception
  {
    try { this.txn.open();
      ProyectoPersonal proyectoPersonal = 
        this.planBusiness.findProyectoPersonalById(proyectoPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(proyectoPersonal);
      return proyectoPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllProyectoPersonal() throws Exception
  {
    try { this.txn.open();
      return this.planBusiness.findAllProyectoPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProyectoPersonalByPlanPersonal(long idPlanPersonal, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findProyectoPersonalByPlanPersonal(idPlanPersonal, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProyectoPersonalByNumero(int numero, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findProyectoPersonalByNumero(numero, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProyectoPersonalByEnunciado(String enunciado, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findProyectoPersonalByEnunciado(enunciado, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findProyectoPersonalByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findProyectoPersonalByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSubsistema(Subsistema subsistema)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.planBusiness.addSubsistema(subsistema);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSubsistema(Subsistema subsistema) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.updateSubsistema(subsistema);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSubsistema(Subsistema subsistema) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.deleteSubsistema(subsistema);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Subsistema findSubsistemaById(long subsistemaId) throws Exception
  {
    try { this.txn.open();
      Subsistema subsistema = 
        this.planBusiness.findSubsistemaById(subsistemaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(subsistema);
      return subsistema;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSubsistema() throws Exception
  {
    try { this.txn.open();
      return this.planBusiness.findAllSubsistema();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSubsistemaByCodSubsistema(String codSubsistema)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findSubsistemaByCodSubsistema(codSubsistema);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSubsistemaByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findSubsistemaByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addCargosPlan(CargosPlan cargosPlan)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.planBusiness.addCargosPlan(cargosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateCargosPlan(CargosPlan cargosPlan) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.updateCargosPlan(cargosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteCargosPlan(CargosPlan cargosPlan) throws Exception
  {
    try { this.txn.open();
      this.planBusiness.deleteCargosPlan(cargosPlan);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public CargosPlan findCargosPlanById(long cargosPlanId) throws Exception
  {
    try { this.txn.open();
      CargosPlan cargosPlan = 
        this.planBusiness.findCargosPlanById(cargosPlanId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(cargosPlan);
      return cargosPlan;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllCargosPlan() throws Exception
  {
    try { this.txn.open();
      return this.planBusiness.findAllCargosPlan();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findCargosPlanByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.planBusiness.findCargosPlanByPlanPersonal(idPlanPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}