package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class PlanBusiness extends AbstractBusiness
  implements Serializable
{
  private ContratosPlanBeanBusiness contratosPlanBeanBusiness = new ContratosPlanBeanBusiness();

  private MovimientosPlanBeanBusiness movimientosPlanBeanBusiness = new MovimientosPlanBeanBusiness();

  private PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

  private ProyectoPersonalBeanBusiness proyectoPersonalBeanBusiness = new ProyectoPersonalBeanBusiness();

  private SubsistemaBeanBusiness subsistemaBeanBusiness = new SubsistemaBeanBusiness();

  private CargosPlanBeanBusiness cargosPlanBeanBusiness = new CargosPlanBeanBusiness();

  public void addContratosPlan(ContratosPlan contratosPlan)
    throws Exception
  {
    this.contratosPlanBeanBusiness.addContratosPlan(contratosPlan);
  }

  public void updateContratosPlan(ContratosPlan contratosPlan) throws Exception {
    this.contratosPlanBeanBusiness.updateContratosPlan(contratosPlan);
  }

  public void deleteContratosPlan(ContratosPlan contratosPlan) throws Exception {
    this.contratosPlanBeanBusiness.deleteContratosPlan(contratosPlan);
  }

  public ContratosPlan findContratosPlanById(long contratosPlanId) throws Exception {
    return this.contratosPlanBeanBusiness.findContratosPlanById(contratosPlanId);
  }

  public Collection findAllContratosPlan() throws Exception {
    return this.contratosPlanBeanBusiness.findContratosPlanAll();
  }

  public Collection findContratosPlanByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    return this.contratosPlanBeanBusiness.findByPlanPersonal(idPlanPersonal);
  }

  public void addMovimientosPlan(MovimientosPlan movimientosPlan)
    throws Exception
  {
    this.movimientosPlanBeanBusiness.addMovimientosPlan(movimientosPlan);
  }

  public void updateMovimientosPlan(MovimientosPlan movimientosPlan) throws Exception {
    this.movimientosPlanBeanBusiness.updateMovimientosPlan(movimientosPlan);
  }

  public void deleteMovimientosPlan(MovimientosPlan movimientosPlan) throws Exception {
    this.movimientosPlanBeanBusiness.deleteMovimientosPlan(movimientosPlan);
  }

  public MovimientosPlan findMovimientosPlanById(long movimientosPlanId) throws Exception {
    return this.movimientosPlanBeanBusiness.findMovimientosPlanById(movimientosPlanId);
  }

  public Collection findAllMovimientosPlan() throws Exception {
    return this.movimientosPlanBeanBusiness.findMovimientosPlanAll();
  }

  public Collection findMovimientosPlanByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    return this.movimientosPlanBeanBusiness.findByPlanPersonal(idPlanPersonal);
  }

  public void addPlanPersonal(PlanPersonal planPersonal)
    throws Exception
  {
    this.planPersonalBeanBusiness.addPlanPersonal(planPersonal);
  }

  public void updatePlanPersonal(PlanPersonal planPersonal) throws Exception {
    this.planPersonalBeanBusiness.updatePlanPersonal(planPersonal);
  }

  public void deletePlanPersonal(PlanPersonal planPersonal) throws Exception {
    this.planPersonalBeanBusiness.deletePlanPersonal(planPersonal);
  }

  public PlanPersonal findPlanPersonalById(long planPersonalId) throws Exception {
    return this.planPersonalBeanBusiness.findPlanPersonalById(planPersonalId);
  }

  public Collection findAllPlanPersonal() throws Exception {
    return this.planPersonalBeanBusiness.findPlanPersonalAll();
  }

  public Collection findPlanPersonalByAnio(int anio, long idOrganismo)
    throws Exception
  {
    return this.planPersonalBeanBusiness.findByAnio(anio, idOrganismo);
  }

  public Collection findPlanPersonalByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.planPersonalBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addProyectoPersonal(ProyectoPersonal proyectoPersonal)
    throws Exception
  {
    this.proyectoPersonalBeanBusiness.addProyectoPersonal(proyectoPersonal);
  }

  public void updateProyectoPersonal(ProyectoPersonal proyectoPersonal) throws Exception {
    this.proyectoPersonalBeanBusiness.updateProyectoPersonal(proyectoPersonal);
  }

  public void deleteProyectoPersonal(ProyectoPersonal proyectoPersonal) throws Exception {
    this.proyectoPersonalBeanBusiness.deleteProyectoPersonal(proyectoPersonal);
  }

  public ProyectoPersonal findProyectoPersonalById(long proyectoPersonalId) throws Exception {
    return this.proyectoPersonalBeanBusiness.findProyectoPersonalById(proyectoPersonalId);
  }

  public Collection findAllProyectoPersonal() throws Exception {
    return this.proyectoPersonalBeanBusiness.findProyectoPersonalAll();
  }

  public Collection findProyectoPersonalByPlanPersonal(long idPlanPersonal, long idOrganismo)
    throws Exception
  {
    return this.proyectoPersonalBeanBusiness.findByPlanPersonal(idPlanPersonal, idOrganismo);
  }

  public Collection findProyectoPersonalByNumero(int numero, long idOrganismo)
    throws Exception
  {
    return this.proyectoPersonalBeanBusiness.findByNumero(numero, idOrganismo);
  }

  public Collection findProyectoPersonalByEnunciado(String enunciado, long idOrganismo)
    throws Exception
  {
    return this.proyectoPersonalBeanBusiness.findByEnunciado(enunciado, idOrganismo);
  }

  public Collection findProyectoPersonalByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.proyectoPersonalBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addSubsistema(Subsistema subsistema)
    throws Exception
  {
    this.subsistemaBeanBusiness.addSubsistema(subsistema);
  }

  public void updateSubsistema(Subsistema subsistema) throws Exception {
    this.subsistemaBeanBusiness.updateSubsistema(subsistema);
  }

  public void deleteSubsistema(Subsistema subsistema) throws Exception {
    this.subsistemaBeanBusiness.deleteSubsistema(subsistema);
  }

  public Subsistema findSubsistemaById(long subsistemaId) throws Exception {
    return this.subsistemaBeanBusiness.findSubsistemaById(subsistemaId);
  }

  public Collection findAllSubsistema() throws Exception {
    return this.subsistemaBeanBusiness.findSubsistemaAll();
  }

  public Collection findSubsistemaByCodSubsistema(String codSubsistema)
    throws Exception
  {
    return this.subsistemaBeanBusiness.findByCodSubsistema(codSubsistema);
  }

  public Collection findSubsistemaByDescripcion(String descripcion)
    throws Exception
  {
    return this.subsistemaBeanBusiness.findByDescripcion(descripcion);
  }

  public void addCargosPlan(CargosPlan cargosPlan)
    throws Exception
  {
    this.cargosPlanBeanBusiness.addCargosPlan(cargosPlan);
  }

  public void updateCargosPlan(CargosPlan cargosPlan) throws Exception {
    this.cargosPlanBeanBusiness.updateCargosPlan(cargosPlan);
  }

  public void deleteCargosPlan(CargosPlan cargosPlan) throws Exception {
    this.cargosPlanBeanBusiness.deleteCargosPlan(cargosPlan);
  }

  public CargosPlan findCargosPlanById(long cargosPlanId) throws Exception {
    return this.cargosPlanBeanBusiness.findCargosPlanById(cargosPlanId);
  }

  public Collection findAllCargosPlan() throws Exception {
    return this.cargosPlanBeanBusiness.findCargosPlanAll();
  }

  public Collection findCargosPlanByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    return this.cargosPlanBeanBusiness.findByPlanPersonal(idPlanPersonal);
  }
}