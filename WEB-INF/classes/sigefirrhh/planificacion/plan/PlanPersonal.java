package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class PlanPersonal
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SINO;
  private long idPlanPersonal;
  private int anio;
  private int version;
  private Date fechaPreparacion;
  private Date fechaVigencia;
  private String estatus;
  private Date fechaFinalizacion;
  private String aproboRrhh;
  private String aproboPlanificacion;
  private String aproboPresupuesto;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "aproboPlanificacion", "aproboPresupuesto", "aproboRrhh", "estatus", "fechaFinalizacion", "fechaPreparacion", "fechaVigencia", "idPlanPersonal", "organismo", "version" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), Integer.TYPE }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 24, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanPersonal());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("0", "PROPUESTO");
    LISTA_ESTATUS.put("1", "APROBADO ORGANISMO");
    LISTA_ESTATUS.put("2", "APROBADO MPD");
    LISTA_ESTATUS.put("3", "EN EJECUCION");
    LISTA_ESTATUS.put("4", "EJECUTADO");
    LISTA_ESTATUS.put("5", "MODIFICADO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public PlanPersonal()
  {
    jdoSetversion(this, 1);

    jdoSetestatus(this, "0");

    jdoSetaproboRrhh(this, "N");

    jdoSetaproboPlanificacion(this, "N");

    jdoSetaproboPresupuesto(this, "N");
  }

  public String toString()
  {
    return jdoGetanio(this) + "  -  " + jdoGetversion(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public String getAproboPlanificacion() {
    return jdoGetaproboPlanificacion(this);
  }
  public void setAproboPlanificacion(String aproboPlanificacion) {
    jdoSetaproboPlanificacion(this, aproboPlanificacion);
  }
  public String getAproboPresupuesto() {
    return jdoGetaproboPresupuesto(this);
  }
  public void setAproboPresupuesto(String aproboPresupuesto) {
    jdoSetaproboPresupuesto(this, aproboPresupuesto);
  }
  public String getAproboRrhh() {
    return jdoGetaproboRrhh(this);
  }
  public void setAproboRrhh(String aproboRrhh) {
    jdoSetaproboRrhh(this, aproboRrhh);
  }

  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public Date getFechaFinalizacion() {
    return jdoGetfechaFinalizacion(this);
  }
  public void setFechaFinalizacion(Date fechaFinalizacion) {
    jdoSetfechaFinalizacion(this, fechaFinalizacion);
  }
  public Date getFechaPreparacion() {
    return jdoGetfechaPreparacion(this);
  }
  public void setFechaPreparacion(Date fechaPreparacion) {
    jdoSetfechaPreparacion(this, fechaPreparacion);
  }
  public Date getFechaVigencia() {
    return jdoGetfechaVigencia(this);
  }
  public void setFechaVigencia(Date fechaVigencia) {
    jdoSetfechaVigencia(this, fechaVigencia);
  }
  public long getIdPlanPersonal() {
    return jdoGetidPlanPersonal(this);
  }
  public void setIdPlanPersonal(long idPlanPersonal) {
    jdoSetidPlanPersonal(this, idPlanPersonal);
  }
  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }
  public int getVersion() {
    return jdoGetversion(this);
  }
  public void setVersion(int version) {
    jdoSetversion(this, version);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 11;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanPersonal localPlanPersonal = new PlanPersonal();
    localPlanPersonal.jdoFlags = 1;
    localPlanPersonal.jdoStateManager = paramStateManager;
    return localPlanPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanPersonal localPlanPersonal = new PlanPersonal();
    localPlanPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanPersonal.jdoFlags = 1;
    localPlanPersonal.jdoStateManager = paramStateManager;
    return localPlanPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aproboPlanificacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aproboPresupuesto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aproboRrhh);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFinalizacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaPreparacion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVigencia);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanPersonal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.version);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aproboPlanificacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aproboPresupuesto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aproboRrhh = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFinalizacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaPreparacion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVigencia = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.version = localStateManager.replacingIntField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanPersonal paramPlanPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPlanPersonal.anio;
      return;
    case 1:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.aproboPlanificacion = paramPlanPersonal.aproboPlanificacion;
      return;
    case 2:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.aproboPresupuesto = paramPlanPersonal.aproboPresupuesto;
      return;
    case 3:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.aproboRrhh = paramPlanPersonal.aproboRrhh;
      return;
    case 4:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramPlanPersonal.estatus;
      return;
    case 5:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFinalizacion = paramPlanPersonal.fechaFinalizacion;
      return;
    case 6:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.fechaPreparacion = paramPlanPersonal.fechaPreparacion;
      return;
    case 7:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVigencia = paramPlanPersonal.fechaVigencia;
      return;
    case 8:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanPersonal = paramPlanPersonal.idPlanPersonal;
      return;
    case 9:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramPlanPersonal.organismo;
      return;
    case 10:
      if (paramPlanPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.version = paramPlanPersonal.version;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanPersonal localPlanPersonal = (PlanPersonal)paramObject;
    if (localPlanPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanPersonalPK))
      throw new IllegalArgumentException("arg1");
    PlanPersonalPK localPlanPersonalPK = (PlanPersonalPK)paramObject;
    localPlanPersonalPK.idPlanPersonal = this.idPlanPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanPersonalPK))
      throw new IllegalArgumentException("arg1");
    PlanPersonalPK localPlanPersonalPK = (PlanPersonalPK)paramObject;
    this.idPlanPersonal = localPlanPersonalPK.idPlanPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanPersonalPK))
      throw new IllegalArgumentException("arg2");
    PlanPersonalPK localPlanPersonalPK = (PlanPersonalPK)paramObject;
    localPlanPersonalPK.idPlanPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 8);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanPersonalPK))
      throw new IllegalArgumentException("arg2");
    PlanPersonalPK localPlanPersonalPK = (PlanPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 8, localPlanPersonalPK.idPlanPersonal);
  }

  private static final int jdoGetanio(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.anio;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.anio;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 0))
      return paramPlanPersonal.anio;
    return localStateManager.getIntField(paramPlanPersonal, jdoInheritedFieldCount + 0, paramPlanPersonal.anio);
  }

  private static final void jdoSetanio(PlanPersonal paramPlanPersonal, int paramInt)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanPersonal, jdoInheritedFieldCount + 0, paramPlanPersonal.anio, paramInt);
  }

  private static final String jdoGetaproboPlanificacion(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.aproboPlanificacion;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.aproboPlanificacion;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 1))
      return paramPlanPersonal.aproboPlanificacion;
    return localStateManager.getStringField(paramPlanPersonal, jdoInheritedFieldCount + 1, paramPlanPersonal.aproboPlanificacion);
  }

  private static final void jdoSetaproboPlanificacion(PlanPersonal paramPlanPersonal, String paramString)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.aproboPlanificacion = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.aproboPlanificacion = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPersonal, jdoInheritedFieldCount + 1, paramPlanPersonal.aproboPlanificacion, paramString);
  }

  private static final String jdoGetaproboPresupuesto(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.aproboPresupuesto;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.aproboPresupuesto;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 2))
      return paramPlanPersonal.aproboPresupuesto;
    return localStateManager.getStringField(paramPlanPersonal, jdoInheritedFieldCount + 2, paramPlanPersonal.aproboPresupuesto);
  }

  private static final void jdoSetaproboPresupuesto(PlanPersonal paramPlanPersonal, String paramString)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.aproboPresupuesto = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.aproboPresupuesto = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPersonal, jdoInheritedFieldCount + 2, paramPlanPersonal.aproboPresupuesto, paramString);
  }

  private static final String jdoGetaproboRrhh(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.aproboRrhh;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.aproboRrhh;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 3))
      return paramPlanPersonal.aproboRrhh;
    return localStateManager.getStringField(paramPlanPersonal, jdoInheritedFieldCount + 3, paramPlanPersonal.aproboRrhh);
  }

  private static final void jdoSetaproboRrhh(PlanPersonal paramPlanPersonal, String paramString)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.aproboRrhh = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.aproboRrhh = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPersonal, jdoInheritedFieldCount + 3, paramPlanPersonal.aproboRrhh, paramString);
  }

  private static final String jdoGetestatus(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.estatus;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.estatus;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 4))
      return paramPlanPersonal.estatus;
    return localStateManager.getStringField(paramPlanPersonal, jdoInheritedFieldCount + 4, paramPlanPersonal.estatus);
  }

  private static final void jdoSetestatus(PlanPersonal paramPlanPersonal, String paramString)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanPersonal, jdoInheritedFieldCount + 4, paramPlanPersonal.estatus, paramString);
  }

  private static final Date jdoGetfechaFinalizacion(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.fechaFinalizacion;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.fechaFinalizacion;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 5))
      return paramPlanPersonal.fechaFinalizacion;
    return (Date)localStateManager.getObjectField(paramPlanPersonal, jdoInheritedFieldCount + 5, paramPlanPersonal.fechaFinalizacion);
  }

  private static final void jdoSetfechaFinalizacion(PlanPersonal paramPlanPersonal, Date paramDate)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.fechaFinalizacion = paramDate;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.fechaFinalizacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPlanPersonal, jdoInheritedFieldCount + 5, paramPlanPersonal.fechaFinalizacion, paramDate);
  }

  private static final Date jdoGetfechaPreparacion(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.fechaPreparacion;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.fechaPreparacion;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 6))
      return paramPlanPersonal.fechaPreparacion;
    return (Date)localStateManager.getObjectField(paramPlanPersonal, jdoInheritedFieldCount + 6, paramPlanPersonal.fechaPreparacion);
  }

  private static final void jdoSetfechaPreparacion(PlanPersonal paramPlanPersonal, Date paramDate)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.fechaPreparacion = paramDate;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.fechaPreparacion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPlanPersonal, jdoInheritedFieldCount + 6, paramPlanPersonal.fechaPreparacion, paramDate);
  }

  private static final Date jdoGetfechaVigencia(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.fechaVigencia;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.fechaVigencia;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 7))
      return paramPlanPersonal.fechaVigencia;
    return (Date)localStateManager.getObjectField(paramPlanPersonal, jdoInheritedFieldCount + 7, paramPlanPersonal.fechaVigencia);
  }

  private static final void jdoSetfechaVigencia(PlanPersonal paramPlanPersonal, Date paramDate)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.fechaVigencia = paramDate;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.fechaVigencia = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPlanPersonal, jdoInheritedFieldCount + 7, paramPlanPersonal.fechaVigencia, paramDate);
  }

  private static final long jdoGetidPlanPersonal(PlanPersonal paramPlanPersonal)
  {
    return paramPlanPersonal.idPlanPersonal;
  }

  private static final void jdoSetidPlanPersonal(PlanPersonal paramPlanPersonal, long paramLong)
  {
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.idPlanPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanPersonal, jdoInheritedFieldCount + 8, paramPlanPersonal.idPlanPersonal, paramLong);
  }

  private static final Organismo jdoGetorganismo(PlanPersonal paramPlanPersonal)
  {
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.organismo;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 9))
      return paramPlanPersonal.organismo;
    return (Organismo)localStateManager.getObjectField(paramPlanPersonal, jdoInheritedFieldCount + 9, paramPlanPersonal.organismo);
  }

  private static final void jdoSetorganismo(PlanPersonal paramPlanPersonal, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramPlanPersonal, jdoInheritedFieldCount + 9, paramPlanPersonal.organismo, paramOrganismo);
  }

  private static final int jdoGetversion(PlanPersonal paramPlanPersonal)
  {
    if (paramPlanPersonal.jdoFlags <= 0)
      return paramPlanPersonal.version;
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramPlanPersonal.version;
    if (localStateManager.isLoaded(paramPlanPersonal, jdoInheritedFieldCount + 10))
      return paramPlanPersonal.version;
    return localStateManager.getIntField(paramPlanPersonal, jdoInheritedFieldCount + 10, paramPlanPersonal.version);
  }

  private static final void jdoSetversion(PlanPersonal paramPlanPersonal, int paramInt)
  {
    if (paramPlanPersonal.jdoFlags == 0)
    {
      paramPlanPersonal.version = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanPersonal.version = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanPersonal, jdoInheritedFieldCount + 10, paramPlanPersonal.version, paramInt);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}