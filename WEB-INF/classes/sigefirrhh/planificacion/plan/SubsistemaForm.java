package sigefirrhh.planificacion.plan;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class SubsistemaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SubsistemaForm.class.getName());
  private Subsistema subsistema;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PlanFacade planFacade = new PlanFacade();
  private boolean showSubsistemaByCodSubsistema;
  private boolean showSubsistemaByDescripcion;
  private String findCodSubsistema;
  private String findDescripcion;
  private Object stateResultSubsistemaByCodSubsistema = null;

  private Object stateResultSubsistemaByDescripcion = null;

  public String getFindCodSubsistema()
  {
    return this.findCodSubsistema;
  }
  public void setFindCodSubsistema(String findCodSubsistema) {
    this.findCodSubsistema = findCodSubsistema;
  }
  public String getFindDescripcion() {
    return this.findDescripcion;
  }
  public void setFindDescripcion(String findDescripcion) {
    this.findDescripcion = findDescripcion;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Subsistema getSubsistema() {
    if (this.subsistema == null) {
      this.subsistema = new Subsistema();
    }
    return this.subsistema;
  }

  public SubsistemaForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findSubsistemaByCodSubsistema()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.planFacade.findSubsistemaByCodSubsistema(this.findCodSubsistema);
      this.showSubsistemaByCodSubsistema = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSubsistemaByCodSubsistema)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSubsistema = null;
    this.findDescripcion = null;

    return null;
  }

  public String findSubsistemaByDescripcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.planFacade.findSubsistemaByDescripcion(this.findDescripcion);
      this.showSubsistemaByDescripcion = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSubsistemaByDescripcion)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodSubsistema = null;
    this.findDescripcion = null;

    return null;
  }

  public boolean isShowSubsistemaByCodSubsistema() {
    return this.showSubsistemaByCodSubsistema;
  }
  public boolean isShowSubsistemaByDescripcion() {
    return this.showSubsistemaByDescripcion;
  }

  public String selectSubsistema()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idSubsistema = 
      Long.parseLong((String)requestParameterMap.get("idSubsistema"));
    try
    {
      this.subsistema = 
        this.planFacade.findSubsistemaById(
        idSubsistema);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.subsistema = null;
    this.showSubsistemaByCodSubsistema = false;
    this.showSubsistemaByDescripcion = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.planFacade.addSubsistema(
          this.subsistema);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.planFacade.updateSubsistema(
          this.subsistema);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.planFacade.deleteSubsistema(
        this.subsistema);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.subsistema = new Subsistema();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.subsistema.setIdSubsistema(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.plan.Subsistema"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.subsistema = new Subsistema();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}