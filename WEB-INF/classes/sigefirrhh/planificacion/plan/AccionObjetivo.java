package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AccionObjetivo
  implements Serializable, PersistenceCapable
{
  private long idAccionObjetivo;
  private ProyectoPersonal proyectoPersonal;
  private int numero;
  private String enunciado;
  private String descripcion;
  private String resultadoParcial;
  private String responsable;
  private int cedula;
  private String cargo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargo", "cedula", "descripcion", "enunciado", "idAccionObjetivo", "numero", "proyectoPersonal", "responsable", "resultadoParcial" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.plan.ProyectoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 26, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public AccionObjetivo()
  {
    jdoSetnumero(this, 1);
  }

  public String toString()
  {
    return jdoGetenunciado(this);
  }
  public String getCargo() {
    return jdoGetcargo(this);
  }
  public void setCargo(String cargo) {
    jdoSetcargo(this, cargo);
  }
  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }
  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }
  public String getEnunciado() {
    return jdoGetenunciado(this);
  }
  public void setEnunciado(String enunciado) {
    jdoSetenunciado(this, enunciado);
  }
  public long getIdAccionObjetivo() {
    return jdoGetidAccionObjetivo(this);
  }
  public void setIdAccionObjetivo(long idAccionObjetivo) {
    jdoSetidAccionObjetivo(this, idAccionObjetivo);
  }
  public int getNumero() {
    return jdoGetnumero(this);
  }
  public void setNumero(int numero) {
    jdoSetnumero(this, numero);
  }
  public ProyectoPersonal getProyectoPersonal() {
    return jdoGetproyectoPersonal(this);
  }
  public void setProyectoPersonal(ProyectoPersonal proyectoPersonal) {
    jdoSetproyectoPersonal(this, proyectoPersonal);
  }
  public String getResponsable() {
    return jdoGetresponsable(this);
  }
  public void setResponsable(String responsable) {
    jdoSetresponsable(this, responsable);
  }
  public String getResultadoParcial() {
    return jdoGetresultadoParcial(this);
  }
  public void setResultadoParcial(String resultadoParcial) {
    jdoSetresultadoParcial(this, resultadoParcial);
  }
  public int getCedula() {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.AccionObjetivo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AccionObjetivo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AccionObjetivo localAccionObjetivo = new AccionObjetivo();
    localAccionObjetivo.jdoFlags = 1;
    localAccionObjetivo.jdoStateManager = paramStateManager;
    return localAccionObjetivo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AccionObjetivo localAccionObjetivo = new AccionObjetivo();
    localAccionObjetivo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAccionObjetivo.jdoFlags = 1;
    localAccionObjetivo.jdoStateManager = paramStateManager;
    return localAccionObjetivo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.enunciado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAccionObjetivo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numero);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proyectoPersonal);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.responsable);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.resultadoParcial);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.enunciado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAccionObjetivo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proyectoPersonal = ((ProyectoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.responsable = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultadoParcial = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AccionObjetivo paramAccionObjetivo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramAccionObjetivo.cargo;
      return;
    case 1:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramAccionObjetivo.cedula;
      return;
    case 2:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramAccionObjetivo.descripcion;
      return;
    case 3:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.enunciado = paramAccionObjetivo.enunciado;
      return;
    case 4:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.idAccionObjetivo = paramAccionObjetivo.idAccionObjetivo;
      return;
    case 5:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramAccionObjetivo.numero;
      return;
    case 6:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.proyectoPersonal = paramAccionObjetivo.proyectoPersonal;
      return;
    case 7:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.responsable = paramAccionObjetivo.responsable;
      return;
    case 8:
      if (paramAccionObjetivo == null)
        throw new IllegalArgumentException("arg1");
      this.resultadoParcial = paramAccionObjetivo.resultadoParcial;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AccionObjetivo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AccionObjetivo localAccionObjetivo = (AccionObjetivo)paramObject;
    if (localAccionObjetivo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAccionObjetivo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AccionObjetivoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AccionObjetivoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionObjetivoPK))
      throw new IllegalArgumentException("arg1");
    AccionObjetivoPK localAccionObjetivoPK = (AccionObjetivoPK)paramObject;
    localAccionObjetivoPK.idAccionObjetivo = this.idAccionObjetivo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionObjetivoPK))
      throw new IllegalArgumentException("arg1");
    AccionObjetivoPK localAccionObjetivoPK = (AccionObjetivoPK)paramObject;
    this.idAccionObjetivo = localAccionObjetivoPK.idAccionObjetivo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionObjetivoPK))
      throw new IllegalArgumentException("arg2");
    AccionObjetivoPK localAccionObjetivoPK = (AccionObjetivoPK)paramObject;
    localAccionObjetivoPK.idAccionObjetivo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionObjetivoPK))
      throw new IllegalArgumentException("arg2");
    AccionObjetivoPK localAccionObjetivoPK = (AccionObjetivoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localAccionObjetivoPK.idAccionObjetivo);
  }

  private static final String jdoGetcargo(AccionObjetivo paramAccionObjetivo)
  {
    if (paramAccionObjetivo.jdoFlags <= 0)
      return paramAccionObjetivo.cargo;
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.cargo;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 0))
      return paramAccionObjetivo.cargo;
    return localStateManager.getStringField(paramAccionObjetivo, jdoInheritedFieldCount + 0, paramAccionObjetivo.cargo);
  }

  private static final void jdoSetcargo(AccionObjetivo paramAccionObjetivo, String paramString)
  {
    if (paramAccionObjetivo.jdoFlags == 0)
    {
      paramAccionObjetivo.cargo = paramString;
      return;
    }
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.cargo = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionObjetivo, jdoInheritedFieldCount + 0, paramAccionObjetivo.cargo, paramString);
  }

  private static final int jdoGetcedula(AccionObjetivo paramAccionObjetivo)
  {
    if (paramAccionObjetivo.jdoFlags <= 0)
      return paramAccionObjetivo.cedula;
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.cedula;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 1))
      return paramAccionObjetivo.cedula;
    return localStateManager.getIntField(paramAccionObjetivo, jdoInheritedFieldCount + 1, paramAccionObjetivo.cedula);
  }

  private static final void jdoSetcedula(AccionObjetivo paramAccionObjetivo, int paramInt)
  {
    if (paramAccionObjetivo.jdoFlags == 0)
    {
      paramAccionObjetivo.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramAccionObjetivo, jdoInheritedFieldCount + 1, paramAccionObjetivo.cedula, paramInt);
  }

  private static final String jdoGetdescripcion(AccionObjetivo paramAccionObjetivo)
  {
    if (paramAccionObjetivo.jdoFlags <= 0)
      return paramAccionObjetivo.descripcion;
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.descripcion;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 2))
      return paramAccionObjetivo.descripcion;
    return localStateManager.getStringField(paramAccionObjetivo, jdoInheritedFieldCount + 2, paramAccionObjetivo.descripcion);
  }

  private static final void jdoSetdescripcion(AccionObjetivo paramAccionObjetivo, String paramString)
  {
    if (paramAccionObjetivo.jdoFlags == 0)
    {
      paramAccionObjetivo.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionObjetivo, jdoInheritedFieldCount + 2, paramAccionObjetivo.descripcion, paramString);
  }

  private static final String jdoGetenunciado(AccionObjetivo paramAccionObjetivo)
  {
    if (paramAccionObjetivo.jdoFlags <= 0)
      return paramAccionObjetivo.enunciado;
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.enunciado;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 3))
      return paramAccionObjetivo.enunciado;
    return localStateManager.getStringField(paramAccionObjetivo, jdoInheritedFieldCount + 3, paramAccionObjetivo.enunciado);
  }

  private static final void jdoSetenunciado(AccionObjetivo paramAccionObjetivo, String paramString)
  {
    if (paramAccionObjetivo.jdoFlags == 0)
    {
      paramAccionObjetivo.enunciado = paramString;
      return;
    }
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.enunciado = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionObjetivo, jdoInheritedFieldCount + 3, paramAccionObjetivo.enunciado, paramString);
  }

  private static final long jdoGetidAccionObjetivo(AccionObjetivo paramAccionObjetivo)
  {
    return paramAccionObjetivo.idAccionObjetivo;
  }

  private static final void jdoSetidAccionObjetivo(AccionObjetivo paramAccionObjetivo, long paramLong)
  {
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.idAccionObjetivo = paramLong;
      return;
    }
    localStateManager.setLongField(paramAccionObjetivo, jdoInheritedFieldCount + 4, paramAccionObjetivo.idAccionObjetivo, paramLong);
  }

  private static final int jdoGetnumero(AccionObjetivo paramAccionObjetivo)
  {
    if (paramAccionObjetivo.jdoFlags <= 0)
      return paramAccionObjetivo.numero;
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.numero;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 5))
      return paramAccionObjetivo.numero;
    return localStateManager.getIntField(paramAccionObjetivo, jdoInheritedFieldCount + 5, paramAccionObjetivo.numero);
  }

  private static final void jdoSetnumero(AccionObjetivo paramAccionObjetivo, int paramInt)
  {
    if (paramAccionObjetivo.jdoFlags == 0)
    {
      paramAccionObjetivo.numero = paramInt;
      return;
    }
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.numero = paramInt;
      return;
    }
    localStateManager.setIntField(paramAccionObjetivo, jdoInheritedFieldCount + 5, paramAccionObjetivo.numero, paramInt);
  }

  private static final ProyectoPersonal jdoGetproyectoPersonal(AccionObjetivo paramAccionObjetivo)
  {
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.proyectoPersonal;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 6))
      return paramAccionObjetivo.proyectoPersonal;
    return (ProyectoPersonal)localStateManager.getObjectField(paramAccionObjetivo, jdoInheritedFieldCount + 6, paramAccionObjetivo.proyectoPersonal);
  }

  private static final void jdoSetproyectoPersonal(AccionObjetivo paramAccionObjetivo, ProyectoPersonal paramProyectoPersonal)
  {
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.proyectoPersonal = paramProyectoPersonal;
      return;
    }
    localStateManager.setObjectField(paramAccionObjetivo, jdoInheritedFieldCount + 6, paramAccionObjetivo.proyectoPersonal, paramProyectoPersonal);
  }

  private static final String jdoGetresponsable(AccionObjetivo paramAccionObjetivo)
  {
    if (paramAccionObjetivo.jdoFlags <= 0)
      return paramAccionObjetivo.responsable;
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.responsable;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 7))
      return paramAccionObjetivo.responsable;
    return localStateManager.getStringField(paramAccionObjetivo, jdoInheritedFieldCount + 7, paramAccionObjetivo.responsable);
  }

  private static final void jdoSetresponsable(AccionObjetivo paramAccionObjetivo, String paramString)
  {
    if (paramAccionObjetivo.jdoFlags == 0)
    {
      paramAccionObjetivo.responsable = paramString;
      return;
    }
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.responsable = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionObjetivo, jdoInheritedFieldCount + 7, paramAccionObjetivo.responsable, paramString);
  }

  private static final String jdoGetresultadoParcial(AccionObjetivo paramAccionObjetivo)
  {
    if (paramAccionObjetivo.jdoFlags <= 0)
      return paramAccionObjetivo.resultadoParcial;
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
      return paramAccionObjetivo.resultadoParcial;
    if (localStateManager.isLoaded(paramAccionObjetivo, jdoInheritedFieldCount + 8))
      return paramAccionObjetivo.resultadoParcial;
    return localStateManager.getStringField(paramAccionObjetivo, jdoInheritedFieldCount + 8, paramAccionObjetivo.resultadoParcial);
  }

  private static final void jdoSetresultadoParcial(AccionObjetivo paramAccionObjetivo, String paramString)
  {
    if (paramAccionObjetivo.jdoFlags == 0)
    {
      paramAccionObjetivo.resultadoParcial = paramString;
      return;
    }
    StateManager localStateManager = paramAccionObjetivo.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionObjetivo.resultadoParcial = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionObjetivo, jdoInheritedFieldCount + 8, paramAccionObjetivo.resultadoParcial, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}