package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.personal.sigecof.AccionCentralizada;
import sigefirrhh.personal.sigecof.Proyecto;

public class ProyectoPersonal
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_COMPONENTE;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SINO;
  private long idProyectoPersonal;
  private PlanPersonal planPersonal;
  private Subsistema subsistema;
  private int numero;
  private String enunciado;
  private String denominacion;
  private String objetivo;
  private String productoFinal;
  private String localizacion;
  private String componente;
  private Proyecto proyecto;
  private AccionCentralizada accionCentralizada;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "accionCentralizada", "componente", "denominacion", "enunciado", "idProyectoPersonal", "localizacion", "numero", "objetivo", "organismo", "planPersonal", "productoFinal", "proyecto", "subsistema" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.sigecof.AccionCentralizada"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.sigecof.Proyecto"), sunjdo$classForName$("sigefirrhh.planificacion.plan.Subsistema") }; private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 24, 21, 21, 21, 26, 26, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.ProyectoPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ProyectoPersonal());

    LISTA_COMPONENTE = 
      new LinkedHashMap();
    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_COMPONENTE.put("P", "PROYECTO");
    LISTA_COMPONENTE.put("A", "ACCION CENTRALIZADA");
    LISTA_COMPONENTE.put("N", "NO APLICA");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ProyectoPersonal()
  {
    jdoSetcomponente(this, "A");
  }

  public String toString()
  {
    return jdoGetsubsistema(this).getDescripcion() + "  -  " + jdoGetnumero(this) + " - " + jdoGetenunciado(this);
  }

  public AccionCentralizada getAccionCentralizada() {
    return jdoGetaccionCentralizada(this);
  }
  public void setAccionCentralizada(AccionCentralizada accionCentralizada) {
    jdoSetaccionCentralizada(this, accionCentralizada);
  }
  public String getComponente() {
    return jdoGetcomponente(this);
  }
  public void setComponente(String componente) {
    jdoSetcomponente(this, componente);
  }
  public String getDenominacion() {
    return jdoGetdenominacion(this);
  }
  public void setDenominacion(String denominacion) {
    jdoSetdenominacion(this, denominacion);
  }
  public String getEnunciado() {
    return jdoGetenunciado(this);
  }
  public void setEnunciado(String enunciado) {
    jdoSetenunciado(this, enunciado);
  }
  public long getIdProyectoPersonal() {
    return jdoGetidProyectoPersonal(this);
  }
  public void setIdProyectoPersonal(long idProyectoPersonal) {
    jdoSetidProyectoPersonal(this, idProyectoPersonal);
  }
  public String getLocalizacion() {
    return jdoGetlocalizacion(this);
  }
  public void setLocalizacion(String localizacion) {
    jdoSetlocalizacion(this, localizacion);
  }
  public int getNumero() {
    return jdoGetnumero(this);
  }
  public void setNumero(int numero) {
    jdoSetnumero(this, numero);
  }
  public String getObjetivo() {
    return jdoGetobjetivo(this);
  }
  public void setObjetivo(String objetivo) {
    jdoSetobjetivo(this, objetivo);
  }
  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }
  public PlanPersonal getPlanPersonal() {
    return jdoGetplanPersonal(this);
  }
  public void setPlanPersonal(PlanPersonal planPersonal) {
    jdoSetplanPersonal(this, planPersonal);
  }
  public String getProductoFinal() {
    return jdoGetproductoFinal(this);
  }
  public void setProductoFinal(String productoFinal) {
    jdoSetproductoFinal(this, productoFinal);
  }
  public Proyecto getProyecto() {
    return jdoGetproyecto(this);
  }
  public void setProyecto(Proyecto proyecto) {
    jdoSetproyecto(this, proyecto);
  }
  public Subsistema getSubsistema() {
    return jdoGetsubsistema(this);
  }
  public void setSubsistema(Subsistema subsistema) {
    jdoSetsubsistema(this, subsistema);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ProyectoPersonal localProyectoPersonal = new ProyectoPersonal();
    localProyectoPersonal.jdoFlags = 1;
    localProyectoPersonal.jdoStateManager = paramStateManager;
    return localProyectoPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ProyectoPersonal localProyectoPersonal = new ProyectoPersonal();
    localProyectoPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProyectoPersonal.jdoFlags = 1;
    localProyectoPersonal.jdoStateManager = paramStateManager;
    return localProyectoPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.accionCentralizada);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.componente);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.denominacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.enunciado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProyectoPersonal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.localizacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numero);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.objetivo);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPersonal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.productoFinal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proyecto);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.subsistema);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accionCentralizada = ((AccionCentralizada)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.componente = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.denominacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.enunciado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProyectoPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.localizacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.objetivo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPersonal = ((PlanPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.productoFinal = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proyecto = ((Proyecto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subsistema = ((Subsistema)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ProyectoPersonal paramProyectoPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.accionCentralizada = paramProyectoPersonal.accionCentralizada;
      return;
    case 1:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.componente = paramProyectoPersonal.componente;
      return;
    case 2:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.denominacion = paramProyectoPersonal.denominacion;
      return;
    case 3:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.enunciado = paramProyectoPersonal.enunciado;
      return;
    case 4:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idProyectoPersonal = paramProyectoPersonal.idProyectoPersonal;
      return;
    case 5:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.localizacion = paramProyectoPersonal.localizacion;
      return;
    case 6:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramProyectoPersonal.numero;
      return;
    case 7:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.objetivo = paramProyectoPersonal.objetivo;
      return;
    case 8:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramProyectoPersonal.organismo;
      return;
    case 9:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.planPersonal = paramProyectoPersonal.planPersonal;
      return;
    case 10:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.productoFinal = paramProyectoPersonal.productoFinal;
      return;
    case 11:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.proyecto = paramProyectoPersonal.proyecto;
      return;
    case 12:
      if (paramProyectoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.subsistema = paramProyectoPersonal.subsistema;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ProyectoPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ProyectoPersonal localProyectoPersonal = (ProyectoPersonal)paramObject;
    if (localProyectoPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProyectoPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProyectoPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProyectoPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProyectoPersonalPK))
      throw new IllegalArgumentException("arg1");
    ProyectoPersonalPK localProyectoPersonalPK = (ProyectoPersonalPK)paramObject;
    localProyectoPersonalPK.idProyectoPersonal = this.idProyectoPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProyectoPersonalPK))
      throw new IllegalArgumentException("arg1");
    ProyectoPersonalPK localProyectoPersonalPK = (ProyectoPersonalPK)paramObject;
    this.idProyectoPersonal = localProyectoPersonalPK.idProyectoPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProyectoPersonalPK))
      throw new IllegalArgumentException("arg2");
    ProyectoPersonalPK localProyectoPersonalPK = (ProyectoPersonalPK)paramObject;
    localProyectoPersonalPK.idProyectoPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProyectoPersonalPK))
      throw new IllegalArgumentException("arg2");
    ProyectoPersonalPK localProyectoPersonalPK = (ProyectoPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localProyectoPersonalPK.idProyectoPersonal);
  }

  private static final AccionCentralizada jdoGetaccionCentralizada(ProyectoPersonal paramProyectoPersonal)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.accionCentralizada;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 0))
      return paramProyectoPersonal.accionCentralizada;
    return (AccionCentralizada)localStateManager.getObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 0, paramProyectoPersonal.accionCentralizada);
  }

  private static final void jdoSetaccionCentralizada(ProyectoPersonal paramProyectoPersonal, AccionCentralizada paramAccionCentralizada)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.accionCentralizada = paramAccionCentralizada;
      return;
    }
    localStateManager.setObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 0, paramProyectoPersonal.accionCentralizada, paramAccionCentralizada);
  }

  private static final String jdoGetcomponente(ProyectoPersonal paramProyectoPersonal)
  {
    if (paramProyectoPersonal.jdoFlags <= 0)
      return paramProyectoPersonal.componente;
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.componente;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 1))
      return paramProyectoPersonal.componente;
    return localStateManager.getStringField(paramProyectoPersonal, jdoInheritedFieldCount + 1, paramProyectoPersonal.componente);
  }

  private static final void jdoSetcomponente(ProyectoPersonal paramProyectoPersonal, String paramString)
  {
    if (paramProyectoPersonal.jdoFlags == 0)
    {
      paramProyectoPersonal.componente = paramString;
      return;
    }
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.componente = paramString;
      return;
    }
    localStateManager.setStringField(paramProyectoPersonal, jdoInheritedFieldCount + 1, paramProyectoPersonal.componente, paramString);
  }

  private static final String jdoGetdenominacion(ProyectoPersonal paramProyectoPersonal)
  {
    if (paramProyectoPersonal.jdoFlags <= 0)
      return paramProyectoPersonal.denominacion;
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.denominacion;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 2))
      return paramProyectoPersonal.denominacion;
    return localStateManager.getStringField(paramProyectoPersonal, jdoInheritedFieldCount + 2, paramProyectoPersonal.denominacion);
  }

  private static final void jdoSetdenominacion(ProyectoPersonal paramProyectoPersonal, String paramString)
  {
    if (paramProyectoPersonal.jdoFlags == 0)
    {
      paramProyectoPersonal.denominacion = paramString;
      return;
    }
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.denominacion = paramString;
      return;
    }
    localStateManager.setStringField(paramProyectoPersonal, jdoInheritedFieldCount + 2, paramProyectoPersonal.denominacion, paramString);
  }

  private static final String jdoGetenunciado(ProyectoPersonal paramProyectoPersonal)
  {
    if (paramProyectoPersonal.jdoFlags <= 0)
      return paramProyectoPersonal.enunciado;
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.enunciado;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 3))
      return paramProyectoPersonal.enunciado;
    return localStateManager.getStringField(paramProyectoPersonal, jdoInheritedFieldCount + 3, paramProyectoPersonal.enunciado);
  }

  private static final void jdoSetenunciado(ProyectoPersonal paramProyectoPersonal, String paramString)
  {
    if (paramProyectoPersonal.jdoFlags == 0)
    {
      paramProyectoPersonal.enunciado = paramString;
      return;
    }
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.enunciado = paramString;
      return;
    }
    localStateManager.setStringField(paramProyectoPersonal, jdoInheritedFieldCount + 3, paramProyectoPersonal.enunciado, paramString);
  }

  private static final long jdoGetidProyectoPersonal(ProyectoPersonal paramProyectoPersonal)
  {
    return paramProyectoPersonal.idProyectoPersonal;
  }

  private static final void jdoSetidProyectoPersonal(ProyectoPersonal paramProyectoPersonal, long paramLong)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.idProyectoPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramProyectoPersonal, jdoInheritedFieldCount + 4, paramProyectoPersonal.idProyectoPersonal, paramLong);
  }

  private static final String jdoGetlocalizacion(ProyectoPersonal paramProyectoPersonal)
  {
    if (paramProyectoPersonal.jdoFlags <= 0)
      return paramProyectoPersonal.localizacion;
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.localizacion;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 5))
      return paramProyectoPersonal.localizacion;
    return localStateManager.getStringField(paramProyectoPersonal, jdoInheritedFieldCount + 5, paramProyectoPersonal.localizacion);
  }

  private static final void jdoSetlocalizacion(ProyectoPersonal paramProyectoPersonal, String paramString)
  {
    if (paramProyectoPersonal.jdoFlags == 0)
    {
      paramProyectoPersonal.localizacion = paramString;
      return;
    }
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.localizacion = paramString;
      return;
    }
    localStateManager.setStringField(paramProyectoPersonal, jdoInheritedFieldCount + 5, paramProyectoPersonal.localizacion, paramString);
  }

  private static final int jdoGetnumero(ProyectoPersonal paramProyectoPersonal)
  {
    if (paramProyectoPersonal.jdoFlags <= 0)
      return paramProyectoPersonal.numero;
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.numero;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 6))
      return paramProyectoPersonal.numero;
    return localStateManager.getIntField(paramProyectoPersonal, jdoInheritedFieldCount + 6, paramProyectoPersonal.numero);
  }

  private static final void jdoSetnumero(ProyectoPersonal paramProyectoPersonal, int paramInt)
  {
    if (paramProyectoPersonal.jdoFlags == 0)
    {
      paramProyectoPersonal.numero = paramInt;
      return;
    }
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.numero = paramInt;
      return;
    }
    localStateManager.setIntField(paramProyectoPersonal, jdoInheritedFieldCount + 6, paramProyectoPersonal.numero, paramInt);
  }

  private static final String jdoGetobjetivo(ProyectoPersonal paramProyectoPersonal)
  {
    if (paramProyectoPersonal.jdoFlags <= 0)
      return paramProyectoPersonal.objetivo;
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.objetivo;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 7))
      return paramProyectoPersonal.objetivo;
    return localStateManager.getStringField(paramProyectoPersonal, jdoInheritedFieldCount + 7, paramProyectoPersonal.objetivo);
  }

  private static final void jdoSetobjetivo(ProyectoPersonal paramProyectoPersonal, String paramString)
  {
    if (paramProyectoPersonal.jdoFlags == 0)
    {
      paramProyectoPersonal.objetivo = paramString;
      return;
    }
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.objetivo = paramString;
      return;
    }
    localStateManager.setStringField(paramProyectoPersonal, jdoInheritedFieldCount + 7, paramProyectoPersonal.objetivo, paramString);
  }

  private static final Organismo jdoGetorganismo(ProyectoPersonal paramProyectoPersonal)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.organismo;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 8))
      return paramProyectoPersonal.organismo;
    return (Organismo)localStateManager.getObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 8, paramProyectoPersonal.organismo);
  }

  private static final void jdoSetorganismo(ProyectoPersonal paramProyectoPersonal, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 8, paramProyectoPersonal.organismo, paramOrganismo);
  }

  private static final PlanPersonal jdoGetplanPersonal(ProyectoPersonal paramProyectoPersonal)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.planPersonal;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 9))
      return paramProyectoPersonal.planPersonal;
    return (PlanPersonal)localStateManager.getObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 9, paramProyectoPersonal.planPersonal);
  }

  private static final void jdoSetplanPersonal(ProyectoPersonal paramProyectoPersonal, PlanPersonal paramPlanPersonal)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.planPersonal = paramPlanPersonal;
      return;
    }
    localStateManager.setObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 9, paramProyectoPersonal.planPersonal, paramPlanPersonal);
  }

  private static final String jdoGetproductoFinal(ProyectoPersonal paramProyectoPersonal)
  {
    if (paramProyectoPersonal.jdoFlags <= 0)
      return paramProyectoPersonal.productoFinal;
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.productoFinal;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 10))
      return paramProyectoPersonal.productoFinal;
    return localStateManager.getStringField(paramProyectoPersonal, jdoInheritedFieldCount + 10, paramProyectoPersonal.productoFinal);
  }

  private static final void jdoSetproductoFinal(ProyectoPersonal paramProyectoPersonal, String paramString)
  {
    if (paramProyectoPersonal.jdoFlags == 0)
    {
      paramProyectoPersonal.productoFinal = paramString;
      return;
    }
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.productoFinal = paramString;
      return;
    }
    localStateManager.setStringField(paramProyectoPersonal, jdoInheritedFieldCount + 10, paramProyectoPersonal.productoFinal, paramString);
  }

  private static final Proyecto jdoGetproyecto(ProyectoPersonal paramProyectoPersonal)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.proyecto;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 11))
      return paramProyectoPersonal.proyecto;
    return (Proyecto)localStateManager.getObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 11, paramProyectoPersonal.proyecto);
  }

  private static final void jdoSetproyecto(ProyectoPersonal paramProyectoPersonal, Proyecto paramProyecto)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.proyecto = paramProyecto;
      return;
    }
    localStateManager.setObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 11, paramProyectoPersonal.proyecto, paramProyecto);
  }

  private static final Subsistema jdoGetsubsistema(ProyectoPersonal paramProyectoPersonal)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramProyectoPersonal.subsistema;
    if (localStateManager.isLoaded(paramProyectoPersonal, jdoInheritedFieldCount + 12))
      return paramProyectoPersonal.subsistema;
    return (Subsistema)localStateManager.getObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 12, paramProyectoPersonal.subsistema);
  }

  private static final void jdoSetsubsistema(ProyectoPersonal paramProyectoPersonal, Subsistema paramSubsistema)
  {
    StateManager localStateManager = paramProyectoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramProyectoPersonal.subsistema = paramSubsistema;
      return;
    }
    localStateManager.setObjectField(paramProyectoPersonal, jdoInheritedFieldCount + 12, paramProyectoPersonal.subsistema, paramSubsistema);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}