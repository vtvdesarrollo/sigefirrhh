package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.UnidadFuncional;

public class PlanEvaluacion
  implements Serializable, PersistenceCapable
{
  private long idPlanEvaluacion;
  private int anio;
  private int mes;
  private int trabajadoresAprobado;
  private int trabajadoresEvaluados;
  private String tipoCargo;
  private TipoPersonal tipoPersonal;
  private UnidadFuncional unidadFuncional;
  private String aprobacionMpd;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "aprobacionMpd", "idPlanEvaluacion", "mes", "tipoCargo", "tipoPersonal", "trabajadoresAprobado", "trabajadoresEvaluados", "unidadFuncional" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadFuncional") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 26, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public long getIdPlanEvaluacion()
  {
    return jdoGetidPlanEvaluacion(this);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public String getTipoCargo()
  {
    return jdoGettipoCargo(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public int getTrabajadoresAprobado()
  {
    return jdoGettrabajadoresAprobado(this);
  }

  public int getTrabajadoresEvaluados()
  {
    return jdoGettrabajadoresEvaluados(this);
  }

  public UnidadFuncional getUnidadFuncional()
  {
    return jdoGetunidadFuncional(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setIdPlanEvaluacion(long l)
  {
    jdoSetidPlanEvaluacion(this, l);
  }

  public void setMes(int i)
  {
    jdoSetmes(this, i);
  }

  public void setTipoCargo(String string)
  {
    jdoSettipoCargo(this, string);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public void setTrabajadoresAprobado(int i)
  {
    jdoSettrabajadoresAprobado(this, i);
  }

  public void setTrabajadoresEvaluados(int i)
  {
    jdoSettrabajadoresEvaluados(this, i);
  }

  public void setUnidadFuncional(UnidadFuncional funcional)
  {
    jdoSetunidadFuncional(this, funcional);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanEvaluacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanEvaluacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanEvaluacion localPlanEvaluacion = new PlanEvaluacion();
    localPlanEvaluacion.jdoFlags = 1;
    localPlanEvaluacion.jdoStateManager = paramStateManager;
    return localPlanEvaluacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanEvaluacion localPlanEvaluacion = new PlanEvaluacion();
    localPlanEvaluacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanEvaluacion.jdoFlags = 1;
    localPlanEvaluacion.jdoStateManager = paramStateManager;
    return localPlanEvaluacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanEvaluacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCargo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.trabajadoresAprobado);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.trabajadoresEvaluados);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadFuncional);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanEvaluacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajadoresAprobado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajadoresEvaluados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadFuncional = ((UnidadFuncional)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanEvaluacion paramPlanEvaluacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPlanEvaluacion.anio;
      return;
    case 1:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramPlanEvaluacion.aprobacionMpd;
      return;
    case 2:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanEvaluacion = paramPlanEvaluacion.idPlanEvaluacion;
      return;
    case 3:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramPlanEvaluacion.mes;
      return;
    case 4:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCargo = paramPlanEvaluacion.tipoCargo;
      return;
    case 5:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPlanEvaluacion.tipoPersonal;
      return;
    case 6:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.trabajadoresAprobado = paramPlanEvaluacion.trabajadoresAprobado;
      return;
    case 7:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.trabajadoresEvaluados = paramPlanEvaluacion.trabajadoresEvaluados;
      return;
    case 8:
      if (paramPlanEvaluacion == null)
        throw new IllegalArgumentException("arg1");
      this.unidadFuncional = paramPlanEvaluacion.unidadFuncional;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanEvaluacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanEvaluacion localPlanEvaluacion = (PlanEvaluacion)paramObject;
    if (localPlanEvaluacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanEvaluacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanEvaluacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanEvaluacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    PlanEvaluacionPK localPlanEvaluacionPK = (PlanEvaluacionPK)paramObject;
    localPlanEvaluacionPK.idPlanEvaluacion = this.idPlanEvaluacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanEvaluacionPK))
      throw new IllegalArgumentException("arg1");
    PlanEvaluacionPK localPlanEvaluacionPK = (PlanEvaluacionPK)paramObject;
    this.idPlanEvaluacion = localPlanEvaluacionPK.idPlanEvaluacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    PlanEvaluacionPK localPlanEvaluacionPK = (PlanEvaluacionPK)paramObject;
    localPlanEvaluacionPK.idPlanEvaluacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanEvaluacionPK))
      throw new IllegalArgumentException("arg2");
    PlanEvaluacionPK localPlanEvaluacionPK = (PlanEvaluacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localPlanEvaluacionPK.idPlanEvaluacion);
  }

  private static final int jdoGetanio(PlanEvaluacion paramPlanEvaluacion)
  {
    if (paramPlanEvaluacion.jdoFlags <= 0)
      return paramPlanEvaluacion.anio;
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.anio;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 0))
      return paramPlanEvaluacion.anio;
    return localStateManager.getIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 0, paramPlanEvaluacion.anio);
  }

  private static final void jdoSetanio(PlanEvaluacion paramPlanEvaluacion, int paramInt)
  {
    if (paramPlanEvaluacion.jdoFlags == 0)
    {
      paramPlanEvaluacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 0, paramPlanEvaluacion.anio, paramInt);
  }

  private static final String jdoGetaprobacionMpd(PlanEvaluacion paramPlanEvaluacion)
  {
    if (paramPlanEvaluacion.jdoFlags <= 0)
      return paramPlanEvaluacion.aprobacionMpd;
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.aprobacionMpd;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 1))
      return paramPlanEvaluacion.aprobacionMpd;
    return localStateManager.getStringField(paramPlanEvaluacion, jdoInheritedFieldCount + 1, paramPlanEvaluacion.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(PlanEvaluacion paramPlanEvaluacion, String paramString)
  {
    if (paramPlanEvaluacion.jdoFlags == 0)
    {
      paramPlanEvaluacion.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanEvaluacion, jdoInheritedFieldCount + 1, paramPlanEvaluacion.aprobacionMpd, paramString);
  }

  private static final long jdoGetidPlanEvaluacion(PlanEvaluacion paramPlanEvaluacion)
  {
    return paramPlanEvaluacion.idPlanEvaluacion;
  }

  private static final void jdoSetidPlanEvaluacion(PlanEvaluacion paramPlanEvaluacion, long paramLong)
  {
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.idPlanEvaluacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanEvaluacion, jdoInheritedFieldCount + 2, paramPlanEvaluacion.idPlanEvaluacion, paramLong);
  }

  private static final int jdoGetmes(PlanEvaluacion paramPlanEvaluacion)
  {
    if (paramPlanEvaluacion.jdoFlags <= 0)
      return paramPlanEvaluacion.mes;
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.mes;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 3))
      return paramPlanEvaluacion.mes;
    return localStateManager.getIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 3, paramPlanEvaluacion.mes);
  }

  private static final void jdoSetmes(PlanEvaluacion paramPlanEvaluacion, int paramInt)
  {
    if (paramPlanEvaluacion.jdoFlags == 0)
    {
      paramPlanEvaluacion.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 3, paramPlanEvaluacion.mes, paramInt);
  }

  private static final String jdoGettipoCargo(PlanEvaluacion paramPlanEvaluacion)
  {
    if (paramPlanEvaluacion.jdoFlags <= 0)
      return paramPlanEvaluacion.tipoCargo;
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.tipoCargo;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 4))
      return paramPlanEvaluacion.tipoCargo;
    return localStateManager.getStringField(paramPlanEvaluacion, jdoInheritedFieldCount + 4, paramPlanEvaluacion.tipoCargo);
  }

  private static final void jdoSettipoCargo(PlanEvaluacion paramPlanEvaluacion, String paramString)
  {
    if (paramPlanEvaluacion.jdoFlags == 0)
    {
      paramPlanEvaluacion.tipoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.tipoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanEvaluacion, jdoInheritedFieldCount + 4, paramPlanEvaluacion.tipoCargo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(PlanEvaluacion paramPlanEvaluacion)
  {
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.tipoPersonal;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 5))
      return paramPlanEvaluacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPlanEvaluacion, jdoInheritedFieldCount + 5, paramPlanEvaluacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PlanEvaluacion paramPlanEvaluacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPlanEvaluacion, jdoInheritedFieldCount + 5, paramPlanEvaluacion.tipoPersonal, paramTipoPersonal);
  }

  private static final int jdoGettrabajadoresAprobado(PlanEvaluacion paramPlanEvaluacion)
  {
    if (paramPlanEvaluacion.jdoFlags <= 0)
      return paramPlanEvaluacion.trabajadoresAprobado;
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.trabajadoresAprobado;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 6))
      return paramPlanEvaluacion.trabajadoresAprobado;
    return localStateManager.getIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 6, paramPlanEvaluacion.trabajadoresAprobado);
  }

  private static final void jdoSettrabajadoresAprobado(PlanEvaluacion paramPlanEvaluacion, int paramInt)
  {
    if (paramPlanEvaluacion.jdoFlags == 0)
    {
      paramPlanEvaluacion.trabajadoresAprobado = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.trabajadoresAprobado = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 6, paramPlanEvaluacion.trabajadoresAprobado, paramInt);
  }

  private static final int jdoGettrabajadoresEvaluados(PlanEvaluacion paramPlanEvaluacion)
  {
    if (paramPlanEvaluacion.jdoFlags <= 0)
      return paramPlanEvaluacion.trabajadoresEvaluados;
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.trabajadoresEvaluados;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 7))
      return paramPlanEvaluacion.trabajadoresEvaluados;
    return localStateManager.getIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 7, paramPlanEvaluacion.trabajadoresEvaluados);
  }

  private static final void jdoSettrabajadoresEvaluados(PlanEvaluacion paramPlanEvaluacion, int paramInt)
  {
    if (paramPlanEvaluacion.jdoFlags == 0)
    {
      paramPlanEvaluacion.trabajadoresEvaluados = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.trabajadoresEvaluados = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanEvaluacion, jdoInheritedFieldCount + 7, paramPlanEvaluacion.trabajadoresEvaluados, paramInt);
  }

  private static final UnidadFuncional jdoGetunidadFuncional(PlanEvaluacion paramPlanEvaluacion)
  {
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanEvaluacion.unidadFuncional;
    if (localStateManager.isLoaded(paramPlanEvaluacion, jdoInheritedFieldCount + 8))
      return paramPlanEvaluacion.unidadFuncional;
    return (UnidadFuncional)localStateManager.getObjectField(paramPlanEvaluacion, jdoInheritedFieldCount + 8, paramPlanEvaluacion.unidadFuncional);
  }

  private static final void jdoSetunidadFuncional(PlanEvaluacion paramPlanEvaluacion, UnidadFuncional paramUnidadFuncional)
  {
    StateManager localStateManager = paramPlanEvaluacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanEvaluacion.unidadFuncional = paramUnidadFuncional;
      return;
    }
    localStateManager.setObjectField(paramPlanEvaluacion, jdoInheritedFieldCount + 8, paramPlanEvaluacion.unidadFuncional, paramUnidadFuncional);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}