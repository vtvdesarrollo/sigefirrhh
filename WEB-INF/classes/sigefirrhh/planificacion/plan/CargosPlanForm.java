package sigefirrhh.planificacion.plan;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.MovimientoCargo;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;

public class CargosPlanForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(CargosPlanForm.class.getName());
  private CargosPlan cargosPlan;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PlanFacade planFacade = new PlanFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showCargosPlanByPlanPersonal;
  private String findSelectPlanPersonal;
  private Collection findColPlanPersonal;
  private Collection colPlanPersonal;
  private Collection colMovimientoCargo;
  private String selectPlanPersonal;
  private String selectMovimientoCargo;
  private Object stateResultCargosPlanByPlanPersonal = null;

  public String getFindSelectPlanPersonal()
  {
    return this.findSelectPlanPersonal;
  }
  public void setFindSelectPlanPersonal(String valPlanPersonal) {
    this.findSelectPlanPersonal = valPlanPersonal;
  }

  public Collection getFindColPlanPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public String getSelectPlanPersonal()
  {
    return this.selectPlanPersonal;
  }
  public void setSelectPlanPersonal(String valPlanPersonal) {
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    this.cargosPlan.setPlanPersonal(null);
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      if (String.valueOf(planPersonal.getIdPlanPersonal()).equals(
        valPlanPersonal)) {
        this.cargosPlan.setPlanPersonal(
          planPersonal);
        break;
      }
    }
    this.selectPlanPersonal = valPlanPersonal;
  }
  public String getSelectMovimientoCargo() {
    return this.selectMovimientoCargo;
  }
  public void setSelectMovimientoCargo(String valMovimientoCargo) {
    Iterator iterator = this.colMovimientoCargo.iterator();
    MovimientoCargo movimientoCargo = null;
    this.cargosPlan.setMovimientoCargo(null);
    while (iterator.hasNext()) {
      movimientoCargo = (MovimientoCargo)iterator.next();
      if (String.valueOf(movimientoCargo.getIdMovimientoCargo()).equals(
        valMovimientoCargo)) {
        this.cargosPlan.setMovimientoCargo(
          movimientoCargo);
        break;
      }
    }
    this.selectMovimientoCargo = valMovimientoCargo;
  }
  public Collection getResult() {
    return this.result;
  }

  public CargosPlan getCargosPlan() {
    if (this.cargosPlan == null) {
      this.cargosPlan = new CargosPlan();
    }
    return this.cargosPlan;
  }

  public CargosPlanForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPlanPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public Collection getColMovimientoCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colMovimientoCargo.iterator();
    MovimientoCargo movimientoCargo = null;
    while (iterator.hasNext()) {
      movimientoCargo = (MovimientoCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(movimientoCargo.getIdMovimientoCargo()), 
        movimientoCargo.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colMovimientoCargo = 
        this.registroFacade.findAllMovimientoCargo();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findCargosPlanByPlanPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.planFacade.findCargosPlanByPlanPersonal(Long.valueOf(this.findSelectPlanPersonal).longValue());
      this.showCargosPlanByPlanPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showCargosPlanByPlanPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;

    return null;
  }

  public boolean isShowCargosPlanByPlanPersonal() {
    return this.showCargosPlanByPlanPersonal;
  }

  public String selectCargosPlan()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPlanPersonal = null;
    this.selectMovimientoCargo = null;

    long idCargosPlan = 
      Long.parseLong((String)requestParameterMap.get("idCargosPlan"));
    try
    {
      this.cargosPlan = 
        this.planFacade.findCargosPlanById(
        idCargosPlan);
      if (this.cargosPlan.getPlanPersonal() != null) {
        this.selectPlanPersonal = 
          String.valueOf(this.cargosPlan.getPlanPersonal().getIdPlanPersonal());
      }
      if (this.cargosPlan.getMovimientoCargo() != null) {
        this.selectMovimientoCargo = 
          String.valueOf(this.cargosPlan.getMovimientoCargo().getIdMovimientoCargo());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.cargosPlan = null;
    this.showCargosPlanByPlanPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.cargosPlan.getCantidadRealizados() > this.cargosPlan.getCantidadAprobados()) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cantidad de movimientos realizados no puede ser mayor que los movimientos aprobados", ""));
        return null;
      }
      if (this.cargosPlan.getCantidadDevueltos() > this.cargosPlan.getCantidadAprobados()) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cantidad de movimientos devueltos no puede ser mayor que los movimientos aprobados", ""));
        return null;
      }
      if (this.adding) {
        this.planFacade.addCargosPlan(
          this.cargosPlan);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.planFacade.updateCargosPlan(
          this.cargosPlan);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.planFacade.deleteCargosPlan(
        this.cargosPlan);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.cargosPlan = new CargosPlan();

    this.selectPlanPersonal = null;

    this.selectMovimientoCargo = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.cargosPlan.setIdCargosPlan(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.plan.CargosPlan"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.cargosPlan = new CargosPlan();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}