package sigefirrhh.planificacion.plan;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.MovimientoPersonal;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;

public class MovimientosPlanForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(MovimientosPlanForm.class.getName());
  private MovimientosPlan movimientosPlan;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PlanFacade planFacade = new PlanFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showMovimientosPlanByPlanPersonal;
  private String findSelectPlanPersonal;
  private Collection findColPlanPersonal;
  private Collection colPlanPersonal;
  private Collection colMovimientoPersonalForCausaMovimiento;
  private Collection colCausaMovimiento;
  private String selectPlanPersonal;
  private String selectMovimientoPersonalForCausaMovimiento;
  private String selectCausaMovimiento;
  private Object stateResultMovimientosPlanByPlanPersonal = null;

  public String getFindSelectPlanPersonal()
  {
    return this.findSelectPlanPersonal;
  }
  public void setFindSelectPlanPersonal(String valPlanPersonal) {
    this.findSelectPlanPersonal = valPlanPersonal;
  }

  public Collection getFindColPlanPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public String getSelectPlanPersonal()
  {
    return this.selectPlanPersonal;
  }
  public void setSelectPlanPersonal(String valPlanPersonal) {
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    this.movimientosPlan.setPlanPersonal(null);
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      if (String.valueOf(planPersonal.getIdPlanPersonal()).equals(
        valPlanPersonal)) {
        this.movimientosPlan.setPlanPersonal(
          planPersonal);
        break;
      }
    }
    this.selectPlanPersonal = valPlanPersonal;
  }
  public String getSelectMovimientoPersonalForCausaMovimiento() {
    return this.selectMovimientoPersonalForCausaMovimiento;
  }
  public void setSelectMovimientoPersonalForCausaMovimiento(String valMovimientoPersonalForCausaMovimiento) {
    this.selectMovimientoPersonalForCausaMovimiento = valMovimientoPersonalForCausaMovimiento;
  }
  public void changeMovimientoPersonalForCausaMovimiento(ValueChangeEvent event) {
    long idMovimientoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCausaMovimiento = null;
      if (idMovimientoPersonal > 0L) {
        this.colCausaMovimiento = 
          this.registroFacade.findCausaMovimientoByMovimientoPersonal(
          idMovimientoPersonal);
      } else {
        this.selectCausaMovimiento = null;
        this.movimientosPlan.setCausaMovimiento(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCausaMovimiento = null;
      this.movimientosPlan.setCausaMovimiento(
        null);
    }
  }

  public boolean isShowMovimientoPersonalForCausaMovimiento() { return this.colMovimientoPersonalForCausaMovimiento != null; }

  public String getSelectCausaMovimiento() {
    return this.selectCausaMovimiento;
  }
  public void setSelectCausaMovimiento(String valCausaMovimiento) {
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    this.movimientosPlan.setCausaMovimiento(null);
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      if (String.valueOf(causaMovimiento.getIdCausaMovimiento()).equals(
        valCausaMovimiento)) {
        this.movimientosPlan.setCausaMovimiento(
          causaMovimiento);
        break;
      }
    }
    this.selectCausaMovimiento = valCausaMovimiento;
  }
  public boolean isShowCausaMovimiento() {
    return this.colCausaMovimiento != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public MovimientosPlan getMovimientosPlan() {
    if (this.movimientosPlan == null) {
      this.movimientosPlan = new MovimientosPlan();
    }
    return this.movimientosPlan;
  }

  public MovimientosPlanForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPlanPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public Collection getColMovimientoPersonalForCausaMovimiento() {
    Collection col = new ArrayList();
    Iterator iterator = this.colMovimientoPersonalForCausaMovimiento.iterator();
    MovimientoPersonal movimientoPersonalForCausaMovimiento = null;
    while (iterator.hasNext()) {
      movimientoPersonalForCausaMovimiento = (MovimientoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(movimientoPersonalForCausaMovimiento.getIdMovimientoPersonal()), 
        movimientoPersonalForCausaMovimiento.toString()));
    }
    return col;
  }

  public Collection getColCausaMovimiento()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCausaMovimiento.iterator();
    CausaMovimiento causaMovimiento = null;
    while (iterator.hasNext()) {
      causaMovimiento = (CausaMovimiento)iterator.next();
      col.add(new SelectItem(
        String.valueOf(causaMovimiento.getIdCausaMovimiento()), 
        causaMovimiento.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colMovimientoPersonalForCausaMovimiento = 
        this.registroFacade.findAllMovimientoPersonal();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findMovimientosPlanByPlanPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.planFacade.findMovimientosPlanByPlanPersonal(Long.valueOf(this.findSelectPlanPersonal).longValue());
      this.showMovimientosPlanByPlanPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showMovimientosPlanByPlanPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;

    return null;
  }

  public boolean isShowMovimientosPlanByPlanPersonal() {
    return this.showMovimientosPlanByPlanPersonal;
  }

  public String selectMovimientosPlan()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPlanPersonal = null;
    this.selectCausaMovimiento = null;
    this.selectMovimientoPersonalForCausaMovimiento = null;

    long idMovimientosPlan = 
      Long.parseLong((String)requestParameterMap.get("idMovimientosPlan"));
    try
    {
      this.movimientosPlan = 
        this.planFacade.findMovimientosPlanById(
        idMovimientosPlan);
      if (this.movimientosPlan.getPlanPersonal() != null) {
        this.selectPlanPersonal = 
          String.valueOf(this.movimientosPlan.getPlanPersonal().getIdPlanPersonal());
      }
      if (this.movimientosPlan.getCausaMovimiento() != null) {
        this.selectCausaMovimiento = 
          String.valueOf(this.movimientosPlan.getCausaMovimiento().getIdCausaMovimiento());
      }

      CausaMovimiento causaMovimiento = null;
      MovimientoPersonal movimientoPersonalForCausaMovimiento = null;

      if (this.movimientosPlan.getCausaMovimiento() != null) {
        long idCausaMovimiento = 
          this.movimientosPlan.getCausaMovimiento().getIdCausaMovimiento();
        this.selectCausaMovimiento = String.valueOf(idCausaMovimiento);
        causaMovimiento = this.registroFacade.findCausaMovimientoById(
          idCausaMovimiento);
        this.colCausaMovimiento = this.registroFacade.findCausaMovimientoByMovimientoPersonal(
          causaMovimiento.getMovimientoPersonal().getIdMovimientoPersonal());

        long idMovimientoPersonalForCausaMovimiento = 
          this.movimientosPlan.getCausaMovimiento().getMovimientoPersonal().getIdMovimientoPersonal();
        this.selectMovimientoPersonalForCausaMovimiento = String.valueOf(idMovimientoPersonalForCausaMovimiento);
        movimientoPersonalForCausaMovimiento = 
          this.registroFacade.findMovimientoPersonalById(
          idMovimientoPersonalForCausaMovimiento);
        this.colMovimientoPersonalForCausaMovimiento = 
          this.registroFacade.findAllMovimientoPersonal();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.movimientosPlan = null;
    this.showMovimientosPlanByPlanPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.movimientosPlan.getCantidadRealizados() > this.movimientosPlan.getCantidadAprobados()) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cantidad de movimientos realizados no puede ser mayor que los movimientos aprobados", ""));
        return null;
      }
      if (this.movimientosPlan.getCantidadDevueltos() > this.movimientosPlan.getCantidadAprobados()) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La cantidad de movimientos devueltos no puede ser mayor que los movimientos aprobados", ""));
        return null;
      }
      if (this.adding) {
        this.planFacade.addMovimientosPlan(
          this.movimientosPlan);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.planFacade.updateMovimientosPlan(
          this.movimientosPlan);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.planFacade.deleteMovimientosPlan(
        this.movimientosPlan);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.movimientosPlan = new MovimientosPlan();

    this.selectPlanPersonal = null;

    this.selectCausaMovimiento = null;

    this.selectMovimientoPersonalForCausaMovimiento = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.movimientosPlan.setIdMovimientosPlan(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.plan.MovimientosPlan"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.movimientosPlan = new MovimientosPlan();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}