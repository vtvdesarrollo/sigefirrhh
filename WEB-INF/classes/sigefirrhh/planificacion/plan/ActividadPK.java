package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class ActividadPK
  implements Serializable
{
  public long idActividad;

  public ActividadPK()
  {
  }

  public ActividadPK(long idActividad)
  {
    this.idActividad = idActividad;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ActividadPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ActividadPK thatPK)
  {
    return 
      this.idActividad == thatPK.idActividad;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idActividad)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idActividad);
  }
}