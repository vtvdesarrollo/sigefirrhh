package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class MetaPK
  implements Serializable
{
  public long idMeta;

  public MetaPK()
  {
  }

  public MetaPK(long idMeta)
  {
    this.idMeta = idMeta;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MetaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MetaPK thatPK)
  {
    return 
      this.idMeta == thatPK.idMeta;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMeta)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMeta);
  }
}