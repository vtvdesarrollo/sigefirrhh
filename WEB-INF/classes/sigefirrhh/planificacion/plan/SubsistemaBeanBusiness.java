package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SubsistemaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSubsistema(Subsistema subsistema)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Subsistema subsistemaNew = 
      (Subsistema)BeanUtils.cloneBean(
      subsistema);

    pm.makePersistent(subsistemaNew);
  }

  public void updateSubsistema(Subsistema subsistema) throws Exception
  {
    Subsistema subsistemaModify = 
      findSubsistemaById(subsistema.getIdSubsistema());

    BeanUtils.copyProperties(subsistemaModify, subsistema);
  }

  public void deleteSubsistema(Subsistema subsistema) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Subsistema subsistemaDelete = 
      findSubsistemaById(subsistema.getIdSubsistema());
    pm.deletePersistent(subsistemaDelete);
  }

  public Subsistema findSubsistemaById(long idSubsistema) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSubsistema == pIdSubsistema";
    Query query = pm.newQuery(Subsistema.class, filter);

    query.declareParameters("long pIdSubsistema");

    parameters.put("pIdSubsistema", new Long(idSubsistema));

    Collection colSubsistema = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSubsistema.iterator();
    return (Subsistema)iterator.next();
  }

  public Collection findSubsistemaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent subsistemaExtent = pm.getExtent(
      Subsistema.class, true);
    Query query = pm.newQuery(subsistemaExtent);
    query.setOrdering("codSubsistema ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodSubsistema(String codSubsistema)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codSubsistema == pCodSubsistema";

    Query query = pm.newQuery(Subsistema.class, filter);

    query.declareParameters("java.lang.String pCodSubsistema");
    HashMap parameters = new HashMap();

    parameters.put("pCodSubsistema", new String(codSubsistema));

    query.setOrdering("codSubsistema ascending");

    Collection colSubsistema = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSubsistema);

    return colSubsistema;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(Subsistema.class, filter);

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    query.setOrdering("codSubsistema ascending");

    Collection colSubsistema = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSubsistema);

    return colSubsistema;
  }
}