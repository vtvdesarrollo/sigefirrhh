package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Subsistema
  implements Serializable, PersistenceCapable
{
  private long idSubsistema;
  private String codSubsistema;
  private String descripcion;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codSubsistema", "descripcion", "idSubsistema" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetdescripcion(this) + "  -  " + jdoGetcodSubsistema(this);
  }

  public String getCodSubsistema()
  {
    return jdoGetcodSubsistema(this);
  }

  public void setCodSubsistema(String codSubsistema)
  {
    jdoSetcodSubsistema(this, codSubsistema);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion)
  {
    jdoSetdescripcion(this, descripcion);
  }

  public long getIdSubsistema()
  {
    return jdoGetidSubsistema(this);
  }

  public void setIdSubsistema(long idSubsistema)
  {
    jdoSetidSubsistema(this, idSubsistema);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.Subsistema"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Subsistema());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Subsistema localSubsistema = new Subsistema();
    localSubsistema.jdoFlags = 1;
    localSubsistema.jdoStateManager = paramStateManager;
    return localSubsistema;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Subsistema localSubsistema = new Subsistema();
    localSubsistema.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSubsistema.jdoFlags = 1;
    localSubsistema.jdoStateManager = paramStateManager;
    return localSubsistema;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codSubsistema);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSubsistema);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codSubsistema = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSubsistema = localStateManager.replacingLongField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Subsistema paramSubsistema, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSubsistema == null)
        throw new IllegalArgumentException("arg1");
      this.codSubsistema = paramSubsistema.codSubsistema;
      return;
    case 1:
      if (paramSubsistema == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramSubsistema.descripcion;
      return;
    case 2:
      if (paramSubsistema == null)
        throw new IllegalArgumentException("arg1");
      this.idSubsistema = paramSubsistema.idSubsistema;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Subsistema))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Subsistema localSubsistema = (Subsistema)paramObject;
    if (localSubsistema.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSubsistema, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SubsistemaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SubsistemaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubsistemaPK))
      throw new IllegalArgumentException("arg1");
    SubsistemaPK localSubsistemaPK = (SubsistemaPK)paramObject;
    localSubsistemaPK.idSubsistema = this.idSubsistema;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SubsistemaPK))
      throw new IllegalArgumentException("arg1");
    SubsistemaPK localSubsistemaPK = (SubsistemaPK)paramObject;
    this.idSubsistema = localSubsistemaPK.idSubsistema;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubsistemaPK))
      throw new IllegalArgumentException("arg2");
    SubsistemaPK localSubsistemaPK = (SubsistemaPK)paramObject;
    localSubsistemaPK.idSubsistema = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SubsistemaPK))
      throw new IllegalArgumentException("arg2");
    SubsistemaPK localSubsistemaPK = (SubsistemaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSubsistemaPK.idSubsistema);
  }

  private static final String jdoGetcodSubsistema(Subsistema paramSubsistema)
  {
    if (paramSubsistema.jdoFlags <= 0)
      return paramSubsistema.codSubsistema;
    StateManager localStateManager = paramSubsistema.jdoStateManager;
    if (localStateManager == null)
      return paramSubsistema.codSubsistema;
    if (localStateManager.isLoaded(paramSubsistema, jdoInheritedFieldCount + 0))
      return paramSubsistema.codSubsistema;
    return localStateManager.getStringField(paramSubsistema, jdoInheritedFieldCount + 0, paramSubsistema.codSubsistema);
  }

  private static final void jdoSetcodSubsistema(Subsistema paramSubsistema, String paramString)
  {
    if (paramSubsistema.jdoFlags == 0)
    {
      paramSubsistema.codSubsistema = paramString;
      return;
    }
    StateManager localStateManager = paramSubsistema.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubsistema.codSubsistema = paramString;
      return;
    }
    localStateManager.setStringField(paramSubsistema, jdoInheritedFieldCount + 0, paramSubsistema.codSubsistema, paramString);
  }

  private static final String jdoGetdescripcion(Subsistema paramSubsistema)
  {
    if (paramSubsistema.jdoFlags <= 0)
      return paramSubsistema.descripcion;
    StateManager localStateManager = paramSubsistema.jdoStateManager;
    if (localStateManager == null)
      return paramSubsistema.descripcion;
    if (localStateManager.isLoaded(paramSubsistema, jdoInheritedFieldCount + 1))
      return paramSubsistema.descripcion;
    return localStateManager.getStringField(paramSubsistema, jdoInheritedFieldCount + 1, paramSubsistema.descripcion);
  }

  private static final void jdoSetdescripcion(Subsistema paramSubsistema, String paramString)
  {
    if (paramSubsistema.jdoFlags == 0)
    {
      paramSubsistema.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramSubsistema.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubsistema.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramSubsistema, jdoInheritedFieldCount + 1, paramSubsistema.descripcion, paramString);
  }

  private static final long jdoGetidSubsistema(Subsistema paramSubsistema)
  {
    return paramSubsistema.idSubsistema;
  }

  private static final void jdoSetidSubsistema(Subsistema paramSubsistema, long paramLong)
  {
    StateManager localStateManager = paramSubsistema.jdoStateManager;
    if (localStateManager == null)
    {
      paramSubsistema.idSubsistema = paramLong;
      return;
    }
    localStateManager.setLongField(paramSubsistema, jdoInheritedFieldCount + 2, paramSubsistema.idSubsistema, paramLong);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}