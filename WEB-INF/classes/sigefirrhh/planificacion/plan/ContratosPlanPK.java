package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class ContratosPlanPK
  implements Serializable
{
  public long idContratosPlan;

  public ContratosPlanPK()
  {
  }

  public ContratosPlanPK(long idContratosPlan)
  {
    this.idContratosPlan = idContratosPlan;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ContratosPlanPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ContratosPlanPK thatPK)
  {
    return 
      this.idContratosPlan == thatPK.idContratosPlan;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idContratosPlan)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idContratosPlan);
  }
}