package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class SubsistemaPK
  implements Serializable
{
  public long idSubsistema;

  public SubsistemaPK()
  {
  }

  public SubsistemaPK(long idSubsistema)
  {
    this.idSubsistema = idSubsistema;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SubsistemaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SubsistemaPK thatPK)
  {
    return 
      this.idSubsistema == thatPK.idSubsistema;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSubsistema)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSubsistema);
  }
}