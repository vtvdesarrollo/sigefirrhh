package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class CargosPlanPK
  implements Serializable
{
  public long idCargosPlan;

  public CargosPlanPK()
  {
  }

  public CargosPlanPK(long idCargosPlan)
  {
    this.idCargosPlan = idCargosPlan;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CargosPlanPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CargosPlanPK thatPK)
  {
    return 
      this.idCargosPlan == thatPK.idCargosPlan;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCargosPlan)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCargosPlan);
  }
}