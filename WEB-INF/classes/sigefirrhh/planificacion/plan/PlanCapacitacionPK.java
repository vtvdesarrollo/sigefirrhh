package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class PlanCapacitacionPK
  implements Serializable
{
  public long idPlanCapacitacion;

  public PlanCapacitacionPK()
  {
  }

  public PlanCapacitacionPK(long idPlanCapacitacion)
  {
    this.idPlanCapacitacion = idPlanCapacitacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanCapacitacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanCapacitacionPK thatPK)
  {
    return 
      this.idPlanCapacitacion == thatPK.idPlanCapacitacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanCapacitacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanCapacitacion);
  }
}