package sigefirrhh.planificacion.plan;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PlanPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PlanPersonalForm.class.getName());
  private PlanPersonal planPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private PlanFacade planFacade = new PlanFacade();
  private boolean showPlanPersonalByAnio;
  private int findAnio;
  private Object stateResultPlanPersonalByAnio = null;

  public int getFindAnio()
  {
    return this.findAnio;
  }
  public void setFindAnio(int findAnio) {
    this.findAnio = findAnio;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public PlanPersonal getPlanPersonal() {
    if (this.planPersonal == null) {
      this.planPersonal = new PlanPersonal();
    }
    return this.planPersonal;
  }

  public PlanPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanPersonal.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAproboRrhh()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanPersonal.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAproboPlanificacion() {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanPersonal.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAproboPresupuesto() {
    Collection col = new ArrayList();

    Iterator iterEntry = PlanPersonal.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findPlanPersonalByAnio()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.planFacade.findPlanPersonalByAnio(this.findAnio, idOrganismo);
      this.showPlanPersonalByAnio = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPlanPersonalByAnio)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findAnio = 0;

    return null;
  }

  public boolean isShowPlanPersonalByAnio() {
    return this.showPlanPersonalByAnio;
  }

  public String selectPlanPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPlanPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPlanPersonal"));
    try
    {
      this.planPersonal = 
        this.planFacade.findPlanPersonalById(
        idPlanPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.planPersonal = null;
    this.showPlanPersonalByAnio = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try {
      if (this.planPersonal.getAnio() == 0) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Año tiene un valor no valido", ""));
        error = true;
      }

      if ((this.planPersonal.getFechaPreparacion() != null) && (this.planPersonal.getFechaVigencia() != null) && 
        (this.planPersonal.getFechaPreparacion().compareTo(this.planPersonal.getFechaVigencia()) >= 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha de Vigencia no puede ser menor a la Fecha de Preparación", ""));
        error = true;
      }
      if ((this.planPersonal.getFechaFinalizacion() != null) && (this.planPersonal.getFechaVigencia() != null) && 
        (this.planPersonal.getFechaVigencia().compareTo(this.planPersonal.getFechaFinalizacion()) >= 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El campo Fecha de Finalización no puede ser menor a la Fecha de Vigencia", ""));
        error = true;
      }
    }
    catch (Exception localException1)
    {
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.planFacade.addPlanPersonal(
          this.planPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.planFacade.updatePlanPersonal(
          this.planPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.planFacade.deletePlanPersonal(
        this.planPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.planPersonal = new PlanPersonal();

    this.planPersonal.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.planPersonal.setIdPlanPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.plan.PlanPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.planPersonal = new PlanPersonal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}