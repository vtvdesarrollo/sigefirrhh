package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.RegionBeanBusiness;

public class ContratosPlanBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addContratosPlan(ContratosPlan contratosPlan)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ContratosPlan contratosPlanNew = 
      (ContratosPlan)BeanUtils.cloneBean(
      contratosPlan);

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (contratosPlanNew.getPlanPersonal() != null) {
      contratosPlanNew.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        contratosPlanNew.getPlanPersonal().getIdPlanPersonal()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (contratosPlanNew.getRegion() != null) {
      contratosPlanNew.setRegion(
        regionBeanBusiness.findRegionById(
        contratosPlanNew.getRegion().getIdRegion()));
    }
    pm.makePersistent(contratosPlanNew);
  }

  public void updateContratosPlan(ContratosPlan contratosPlan) throws Exception
  {
    ContratosPlan contratosPlanModify = 
      findContratosPlanById(contratosPlan.getIdContratosPlan());

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (contratosPlan.getPlanPersonal() != null) {
      contratosPlan.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        contratosPlan.getPlanPersonal().getIdPlanPersonal()));
    }

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (contratosPlan.getRegion() != null) {
      contratosPlan.setRegion(
        regionBeanBusiness.findRegionById(
        contratosPlan.getRegion().getIdRegion()));
    }

    BeanUtils.copyProperties(contratosPlanModify, contratosPlan);
  }

  public void deleteContratosPlan(ContratosPlan contratosPlan) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ContratosPlan contratosPlanDelete = 
      findContratosPlanById(contratosPlan.getIdContratosPlan());
    pm.deletePersistent(contratosPlanDelete);
  }

  public ContratosPlan findContratosPlanById(long idContratosPlan) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idContratosPlan == pIdContratosPlan";
    Query query = pm.newQuery(ContratosPlan.class, filter);

    query.declareParameters("long pIdContratosPlan");

    parameters.put("pIdContratosPlan", new Long(idContratosPlan));

    Collection colContratosPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colContratosPlan.iterator();
    return (ContratosPlan)iterator.next();
  }

  public Collection findContratosPlanAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent contratosPlanExtent = pm.getExtent(
      ContratosPlan.class, true);
    Query query = pm.newQuery(contratosPlanExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPersonal.idPlanPersonal == pIdPlanPersonal";

    Query query = pm.newQuery(ContratosPlan.class, filter);

    query.declareParameters("long pIdPlanPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPersonal", new Long(idPlanPersonal));

    Collection colContratosPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colContratosPlan);

    return colContratosPlan;
  }
}