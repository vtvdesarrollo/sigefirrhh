package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class PlanPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPlanPersonal(PlanPersonal planPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PlanPersonal planPersonalNew = 
      (PlanPersonal)BeanUtils.cloneBean(
      planPersonal);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (planPersonalNew.getOrganismo() != null) {
      planPersonalNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        planPersonalNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(planPersonalNew);
  }

  public void updatePlanPersonal(PlanPersonal planPersonal) throws Exception
  {
    PlanPersonal planPersonalModify = 
      findPlanPersonalById(planPersonal.getIdPlanPersonal());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (planPersonal.getOrganismo() != null) {
      planPersonal.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        planPersonal.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(planPersonalModify, planPersonal);
  }

  public void deletePlanPersonal(PlanPersonal planPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PlanPersonal planPersonalDelete = 
      findPlanPersonalById(planPersonal.getIdPlanPersonal());
    pm.deletePersistent(planPersonalDelete);
  }

  public PlanPersonal findPlanPersonalById(long idPlanPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPlanPersonal == pIdPlanPersonal";
    Query query = pm.newQuery(PlanPersonal.class, filter);

    query.declareParameters("long pIdPlanPersonal");

    parameters.put("pIdPlanPersonal", new Long(idPlanPersonal));

    Collection colPlanPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPlanPersonal.iterator();
    return (PlanPersonal)iterator.next();
  }

  public Collection findPlanPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent planPersonalExtent = pm.getExtent(
      PlanPersonal.class, true);
    Query query = pm.newQuery(planPersonalExtent);
    query.setOrdering("anio descending, version ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(PlanPersonal.class, filter);

    query.declareParameters("int pAnio, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio descending, version ascending");

    Collection colPlanPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanPersonal);

    return colPlanPersonal;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(PlanPersonal.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("anio descending, version ascending");

    Collection colPlanPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPlanPersonal);

    return colPlanPersonal;
  }
}