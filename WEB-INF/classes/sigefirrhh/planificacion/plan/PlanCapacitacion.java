package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.adiestramiento.Curso;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.planificacion.capacitacion.PlanAdiestramiento;

public class PlanCapacitacion
  implements Serializable, PersistenceCapable
{
  private long idPlanCapacitacion;
  private int anio;
  private Curso curso;
  private int participantesAprobado;
  private int participantesReal;
  private String tipoCargo;
  private double costoAprobado;
  private double costoReal;
  private int trimestreAprobado;
  private int trimestreReal;
  private String partidaPresupuestaria;
  private String estatus;
  private UnidadFuncional unidadFuncional;
  private PlanAdiestramiento planAdiestramiento;
  private String aprobacionMpd;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "aprobacionMpd", "costoAprobado", "costoReal", "curso", "estatus", "idPlanCapacitacion", "participantesAprobado", "participantesReal", "partidaPresupuestaria", "planAdiestramiento", "tipoCargo", "trimestreAprobado", "trimestreReal", "unidadFuncional" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.adiestramiento.Curso"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.planificacion.capacitacion.PlanAdiestramiento"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadFuncional") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 26, 21, 24, 21, 21, 21, 26, 21, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public void setAprobacionMpd(String aprobacionMpd)
  {
    jdoSetaprobacionMpd(this, aprobacionMpd);
  }

  public double getCostoAprobado()
  {
    return jdoGetcostoAprobado(this);
  }

  public void setCostoAprobado(double costoAprobado)
  {
    jdoSetcostoAprobado(this, costoAprobado);
  }

  public double getCostoReal()
  {
    return jdoGetcostoReal(this);
  }

  public void setCostoReal(double costoReal)
  {
    jdoSetcostoReal(this, costoReal);
  }

  public Curso getCurso()
  {
    return jdoGetcurso(this);
  }

  public void setCurso(Curso curso)
  {
    jdoSetcurso(this, curso);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus)
  {
    jdoSetestatus(this, estatus);
  }

  public long getIdPlanCapacitacion()
  {
    return jdoGetidPlanCapacitacion(this);
  }

  public void setIdPlanCapacitacion(long idPlanCapacitacion)
  {
    jdoSetidPlanCapacitacion(this, idPlanCapacitacion);
  }

  public int getParticipantesAprobado()
  {
    return jdoGetparticipantesAprobado(this);
  }

  public void setParticipantesAprobado(int participantesAprobado)
  {
    jdoSetparticipantesAprobado(this, participantesAprobado);
  }

  public int getParticipantesReal()
  {
    return jdoGetparticipantesReal(this);
  }

  public void setParticipantesReal(int participantesReal)
  {
    jdoSetparticipantesReal(this, participantesReal);
  }

  public String getPartidaPresupuestaria()
  {
    return jdoGetpartidaPresupuestaria(this);
  }

  public void setPartidaPresupuestaria(String partidaPresupuestaria)
  {
    jdoSetpartidaPresupuestaria(this, partidaPresupuestaria);
  }

  public PlanAdiestramiento getPlanAdiestramiento()
  {
    return jdoGetplanAdiestramiento(this);
  }

  public void setPlanAdiestramiento(PlanAdiestramiento planAdiestramiento)
  {
    jdoSetplanAdiestramiento(this, planAdiestramiento);
  }

  public String getTipoCargo()
  {
    return jdoGettipoCargo(this);
  }

  public void setTipoCargo(String tipoCargo)
  {
    jdoSettipoCargo(this, tipoCargo);
  }

  public int getTrimestreAprobado()
  {
    return jdoGettrimestreAprobado(this);
  }

  public void setTrimestreAprobado(int trimestreAprobado)
  {
    jdoSettrimestreAprobado(this, trimestreAprobado);
  }

  public int getTrimestreReal()
  {
    return jdoGettrimestreReal(this);
  }

  public void setTrimestreReal(int trimestreReal)
  {
    jdoSettrimestreReal(this, trimestreReal);
  }

  public UnidadFuncional getUnidadFuncional()
  {
    return jdoGetunidadFuncional(this);
  }

  public void setUnidadFuncional(UnidadFuncional unidadFuncional)
  {
    jdoSetunidadFuncional(this, unidadFuncional);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanCapacitacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanCapacitacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanCapacitacion localPlanCapacitacion = new PlanCapacitacion();
    localPlanCapacitacion.jdoFlags = 1;
    localPlanCapacitacion.jdoStateManager = paramStateManager;
    return localPlanCapacitacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanCapacitacion localPlanCapacitacion = new PlanCapacitacion();
    localPlanCapacitacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanCapacitacion.jdoFlags = 1;
    localPlanCapacitacion.jdoStateManager = paramStateManager;
    return localPlanCapacitacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoAprobado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.costoReal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.curso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanCapacitacion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.participantesAprobado);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.participantesReal);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.partidaPresupuestaria);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planAdiestramiento);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCargo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.trimestreAprobado);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.trimestreReal);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadFuncional);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoAprobado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.costoReal = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.curso = ((Curso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanCapacitacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.participantesAprobado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.participantesReal = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.partidaPresupuestaria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planAdiestramiento = ((PlanAdiestramiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trimestreAprobado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trimestreReal = localStateManager.replacingIntField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadFuncional = ((UnidadFuncional)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanCapacitacion paramPlanCapacitacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPlanCapacitacion.anio;
      return;
    case 1:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramPlanCapacitacion.aprobacionMpd;
      return;
    case 2:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.costoAprobado = paramPlanCapacitacion.costoAprobado;
      return;
    case 3:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.costoReal = paramPlanCapacitacion.costoReal;
      return;
    case 4:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.curso = paramPlanCapacitacion.curso;
      return;
    case 5:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramPlanCapacitacion.estatus;
      return;
    case 6:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanCapacitacion = paramPlanCapacitacion.idPlanCapacitacion;
      return;
    case 7:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.participantesAprobado = paramPlanCapacitacion.participantesAprobado;
      return;
    case 8:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.participantesReal = paramPlanCapacitacion.participantesReal;
      return;
    case 9:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.partidaPresupuestaria = paramPlanCapacitacion.partidaPresupuestaria;
      return;
    case 10:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.planAdiestramiento = paramPlanCapacitacion.planAdiestramiento;
      return;
    case 11:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCargo = paramPlanCapacitacion.tipoCargo;
      return;
    case 12:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.trimestreAprobado = paramPlanCapacitacion.trimestreAprobado;
      return;
    case 13:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.trimestreReal = paramPlanCapacitacion.trimestreReal;
      return;
    case 14:
      if (paramPlanCapacitacion == null)
        throw new IllegalArgumentException("arg1");
      this.unidadFuncional = paramPlanCapacitacion.unidadFuncional;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanCapacitacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanCapacitacion localPlanCapacitacion = (PlanCapacitacion)paramObject;
    if (localPlanCapacitacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanCapacitacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanCapacitacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanCapacitacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanCapacitacionPK))
      throw new IllegalArgumentException("arg1");
    PlanCapacitacionPK localPlanCapacitacionPK = (PlanCapacitacionPK)paramObject;
    localPlanCapacitacionPK.idPlanCapacitacion = this.idPlanCapacitacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanCapacitacionPK))
      throw new IllegalArgumentException("arg1");
    PlanCapacitacionPK localPlanCapacitacionPK = (PlanCapacitacionPK)paramObject;
    this.idPlanCapacitacion = localPlanCapacitacionPK.idPlanCapacitacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanCapacitacionPK))
      throw new IllegalArgumentException("arg2");
    PlanCapacitacionPK localPlanCapacitacionPK = (PlanCapacitacionPK)paramObject;
    localPlanCapacitacionPK.idPlanCapacitacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanCapacitacionPK))
      throw new IllegalArgumentException("arg2");
    PlanCapacitacionPK localPlanCapacitacionPK = (PlanCapacitacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localPlanCapacitacionPK.idPlanCapacitacion);
  }

  private static final int jdoGetanio(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.anio;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.anio;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 0))
      return paramPlanCapacitacion.anio;
    return localStateManager.getIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 0, paramPlanCapacitacion.anio);
  }

  private static final void jdoSetanio(PlanCapacitacion paramPlanCapacitacion, int paramInt)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 0, paramPlanCapacitacion.anio, paramInt);
  }

  private static final String jdoGetaprobacionMpd(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.aprobacionMpd;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.aprobacionMpd;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 1))
      return paramPlanCapacitacion.aprobacionMpd;
    return localStateManager.getStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 1, paramPlanCapacitacion.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(PlanCapacitacion paramPlanCapacitacion, String paramString)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 1, paramPlanCapacitacion.aprobacionMpd, paramString);
  }

  private static final double jdoGetcostoAprobado(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.costoAprobado;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.costoAprobado;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 2))
      return paramPlanCapacitacion.costoAprobado;
    return localStateManager.getDoubleField(paramPlanCapacitacion, jdoInheritedFieldCount + 2, paramPlanCapacitacion.costoAprobado);
  }

  private static final void jdoSetcostoAprobado(PlanCapacitacion paramPlanCapacitacion, double paramDouble)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.costoAprobado = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.costoAprobado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanCapacitacion, jdoInheritedFieldCount + 2, paramPlanCapacitacion.costoAprobado, paramDouble);
  }

  private static final double jdoGetcostoReal(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.costoReal;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.costoReal;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 3))
      return paramPlanCapacitacion.costoReal;
    return localStateManager.getDoubleField(paramPlanCapacitacion, jdoInheritedFieldCount + 3, paramPlanCapacitacion.costoReal);
  }

  private static final void jdoSetcostoReal(PlanCapacitacion paramPlanCapacitacion, double paramDouble)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.costoReal = paramDouble;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.costoReal = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPlanCapacitacion, jdoInheritedFieldCount + 3, paramPlanCapacitacion.costoReal, paramDouble);
  }

  private static final Curso jdoGetcurso(PlanCapacitacion paramPlanCapacitacion)
  {
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.curso;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 4))
      return paramPlanCapacitacion.curso;
    return (Curso)localStateManager.getObjectField(paramPlanCapacitacion, jdoInheritedFieldCount + 4, paramPlanCapacitacion.curso);
  }

  private static final void jdoSetcurso(PlanCapacitacion paramPlanCapacitacion, Curso paramCurso)
  {
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.curso = paramCurso;
      return;
    }
    localStateManager.setObjectField(paramPlanCapacitacion, jdoInheritedFieldCount + 4, paramPlanCapacitacion.curso, paramCurso);
  }

  private static final String jdoGetestatus(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.estatus;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.estatus;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 5))
      return paramPlanCapacitacion.estatus;
    return localStateManager.getStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 5, paramPlanCapacitacion.estatus);
  }

  private static final void jdoSetestatus(PlanCapacitacion paramPlanCapacitacion, String paramString)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 5, paramPlanCapacitacion.estatus, paramString);
  }

  private static final long jdoGetidPlanCapacitacion(PlanCapacitacion paramPlanCapacitacion)
  {
    return paramPlanCapacitacion.idPlanCapacitacion;
  }

  private static final void jdoSetidPlanCapacitacion(PlanCapacitacion paramPlanCapacitacion, long paramLong)
  {
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.idPlanCapacitacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanCapacitacion, jdoInheritedFieldCount + 6, paramPlanCapacitacion.idPlanCapacitacion, paramLong);
  }

  private static final int jdoGetparticipantesAprobado(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.participantesAprobado;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.participantesAprobado;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 7))
      return paramPlanCapacitacion.participantesAprobado;
    return localStateManager.getIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 7, paramPlanCapacitacion.participantesAprobado);
  }

  private static final void jdoSetparticipantesAprobado(PlanCapacitacion paramPlanCapacitacion, int paramInt)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.participantesAprobado = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.participantesAprobado = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 7, paramPlanCapacitacion.participantesAprobado, paramInt);
  }

  private static final int jdoGetparticipantesReal(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.participantesReal;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.participantesReal;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 8))
      return paramPlanCapacitacion.participantesReal;
    return localStateManager.getIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 8, paramPlanCapacitacion.participantesReal);
  }

  private static final void jdoSetparticipantesReal(PlanCapacitacion paramPlanCapacitacion, int paramInt)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.participantesReal = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.participantesReal = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 8, paramPlanCapacitacion.participantesReal, paramInt);
  }

  private static final String jdoGetpartidaPresupuestaria(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.partidaPresupuestaria;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.partidaPresupuestaria;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 9))
      return paramPlanCapacitacion.partidaPresupuestaria;
    return localStateManager.getStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 9, paramPlanCapacitacion.partidaPresupuestaria);
  }

  private static final void jdoSetpartidaPresupuestaria(PlanCapacitacion paramPlanCapacitacion, String paramString)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.partidaPresupuestaria = paramString;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.partidaPresupuestaria = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 9, paramPlanCapacitacion.partidaPresupuestaria, paramString);
  }

  private static final PlanAdiestramiento jdoGetplanAdiestramiento(PlanCapacitacion paramPlanCapacitacion)
  {
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.planAdiestramiento;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 10))
      return paramPlanCapacitacion.planAdiestramiento;
    return (PlanAdiestramiento)localStateManager.getObjectField(paramPlanCapacitacion, jdoInheritedFieldCount + 10, paramPlanCapacitacion.planAdiestramiento);
  }

  private static final void jdoSetplanAdiestramiento(PlanCapacitacion paramPlanCapacitacion, PlanAdiestramiento paramPlanAdiestramiento)
  {
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.planAdiestramiento = paramPlanAdiestramiento;
      return;
    }
    localStateManager.setObjectField(paramPlanCapacitacion, jdoInheritedFieldCount + 10, paramPlanCapacitacion.planAdiestramiento, paramPlanAdiestramiento);
  }

  private static final String jdoGettipoCargo(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.tipoCargo;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.tipoCargo;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 11))
      return paramPlanCapacitacion.tipoCargo;
    return localStateManager.getStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 11, paramPlanCapacitacion.tipoCargo);
  }

  private static final void jdoSettipoCargo(PlanCapacitacion paramPlanCapacitacion, String paramString)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.tipoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.tipoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanCapacitacion, jdoInheritedFieldCount + 11, paramPlanCapacitacion.tipoCargo, paramString);
  }

  private static final int jdoGettrimestreAprobado(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.trimestreAprobado;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.trimestreAprobado;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 12))
      return paramPlanCapacitacion.trimestreAprobado;
    return localStateManager.getIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 12, paramPlanCapacitacion.trimestreAprobado);
  }

  private static final void jdoSettrimestreAprobado(PlanCapacitacion paramPlanCapacitacion, int paramInt)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.trimestreAprobado = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.trimestreAprobado = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 12, paramPlanCapacitacion.trimestreAprobado, paramInt);
  }

  private static final int jdoGettrimestreReal(PlanCapacitacion paramPlanCapacitacion)
  {
    if (paramPlanCapacitacion.jdoFlags <= 0)
      return paramPlanCapacitacion.trimestreReal;
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.trimestreReal;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 13))
      return paramPlanCapacitacion.trimestreReal;
    return localStateManager.getIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 13, paramPlanCapacitacion.trimestreReal);
  }

  private static final void jdoSettrimestreReal(PlanCapacitacion paramPlanCapacitacion, int paramInt)
  {
    if (paramPlanCapacitacion.jdoFlags == 0)
    {
      paramPlanCapacitacion.trimestreReal = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.trimestreReal = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanCapacitacion, jdoInheritedFieldCount + 13, paramPlanCapacitacion.trimestreReal, paramInt);
  }

  private static final UnidadFuncional jdoGetunidadFuncional(PlanCapacitacion paramPlanCapacitacion)
  {
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanCapacitacion.unidadFuncional;
    if (localStateManager.isLoaded(paramPlanCapacitacion, jdoInheritedFieldCount + 14))
      return paramPlanCapacitacion.unidadFuncional;
    return (UnidadFuncional)localStateManager.getObjectField(paramPlanCapacitacion, jdoInheritedFieldCount + 14, paramPlanCapacitacion.unidadFuncional);
  }

  private static final void jdoSetunidadFuncional(PlanCapacitacion paramPlanCapacitacion, UnidadFuncional paramUnidadFuncional)
  {
    StateManager localStateManager = paramPlanCapacitacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanCapacitacion.unidadFuncional = paramUnidadFuncional;
      return;
    }
    localStateManager.setObjectField(paramPlanCapacitacion, jdoInheritedFieldCount + 14, paramPlanCapacitacion.unidadFuncional, paramUnidadFuncional);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}