package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Region;

public class ContratosPlan
  implements Serializable, PersistenceCapable
{
  private long idContratosPlan;
  private PlanPersonal planPersonal;
  private Region region;
  private int cantidadAprobados;
  private int cantidadRealizados;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cantidadAprobados", "cantidadRealizados", "idContratosPlan", "planPersonal", "region" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanPersonal"), sunjdo$classForName$("sigefirrhh.base.estructura.Region") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public ContratosPlan()
  {
    jdoSetcantidadAprobados(this, 0);

    jdoSetcantidadRealizados(this, 0);
  }

  public String toString() {
    return jdoGetregion(this).getNombre() + " Aprobados: " + jdoGetcantidadAprobados(this) + " Realizados: " + jdoGetcantidadRealizados(this);
  }

  public int getCantidadAprobados() {
    return jdoGetcantidadAprobados(this);
  }
  public void setCantidadAprobados(int cantidadAprobados) {
    jdoSetcantidadAprobados(this, cantidadAprobados);
  }
  public int getCantidadRealizados() {
    return jdoGetcantidadRealizados(this);
  }
  public void setCantidadRealizados(int cantidadRealizados) {
    jdoSetcantidadRealizados(this, cantidadRealizados);
  }
  public long getIdContratosPlan() {
    return jdoGetidContratosPlan(this);
  }
  public void setIdContratosPlan(long idContratosPlan) {
    jdoSetidContratosPlan(this, idContratosPlan);
  }
  public PlanPersonal getPlanPersonal() {
    return jdoGetplanPersonal(this);
  }
  public void setPlanPersonal(PlanPersonal planPersonal) {
    jdoSetplanPersonal(this, planPersonal);
  }
  public Region getRegion() {
    return jdoGetregion(this);
  }
  public void setRegion(Region region) {
    jdoSetregion(this, region);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.ContratosPlan"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ContratosPlan());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ContratosPlan localContratosPlan = new ContratosPlan();
    localContratosPlan.jdoFlags = 1;
    localContratosPlan.jdoStateManager = paramStateManager;
    return localContratosPlan;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ContratosPlan localContratosPlan = new ContratosPlan();
    localContratosPlan.jdoCopyKeyFieldsFromObjectId(paramObject);
    localContratosPlan.jdoFlags = 1;
    localContratosPlan.jdoStateManager = paramStateManager;
    return localContratosPlan;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadAprobados);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadRealizados);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idContratosPlan);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPersonal);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.region);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadAprobados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadRealizados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idContratosPlan = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPersonal = ((PlanPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.region = ((Region)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ContratosPlan paramContratosPlan, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramContratosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadAprobados = paramContratosPlan.cantidadAprobados;
      return;
    case 1:
      if (paramContratosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadRealizados = paramContratosPlan.cantidadRealizados;
      return;
    case 2:
      if (paramContratosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.idContratosPlan = paramContratosPlan.idContratosPlan;
      return;
    case 3:
      if (paramContratosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.planPersonal = paramContratosPlan.planPersonal;
      return;
    case 4:
      if (paramContratosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.region = paramContratosPlan.region;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ContratosPlan))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ContratosPlan localContratosPlan = (ContratosPlan)paramObject;
    if (localContratosPlan.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localContratosPlan, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ContratosPlanPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ContratosPlanPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContratosPlanPK))
      throw new IllegalArgumentException("arg1");
    ContratosPlanPK localContratosPlanPK = (ContratosPlanPK)paramObject;
    localContratosPlanPK.idContratosPlan = this.idContratosPlan;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ContratosPlanPK))
      throw new IllegalArgumentException("arg1");
    ContratosPlanPK localContratosPlanPK = (ContratosPlanPK)paramObject;
    this.idContratosPlan = localContratosPlanPK.idContratosPlan;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContratosPlanPK))
      throw new IllegalArgumentException("arg2");
    ContratosPlanPK localContratosPlanPK = (ContratosPlanPK)paramObject;
    localContratosPlanPK.idContratosPlan = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ContratosPlanPK))
      throw new IllegalArgumentException("arg2");
    ContratosPlanPK localContratosPlanPK = (ContratosPlanPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localContratosPlanPK.idContratosPlan);
  }

  private static final int jdoGetcantidadAprobados(ContratosPlan paramContratosPlan)
  {
    if (paramContratosPlan.jdoFlags <= 0)
      return paramContratosPlan.cantidadAprobados;
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramContratosPlan.cantidadAprobados;
    if (localStateManager.isLoaded(paramContratosPlan, jdoInheritedFieldCount + 0))
      return paramContratosPlan.cantidadAprobados;
    return localStateManager.getIntField(paramContratosPlan, jdoInheritedFieldCount + 0, paramContratosPlan.cantidadAprobados);
  }

  private static final void jdoSetcantidadAprobados(ContratosPlan paramContratosPlan, int paramInt)
  {
    if (paramContratosPlan.jdoFlags == 0)
    {
      paramContratosPlan.cantidadAprobados = paramInt;
      return;
    }
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratosPlan.cantidadAprobados = paramInt;
      return;
    }
    localStateManager.setIntField(paramContratosPlan, jdoInheritedFieldCount + 0, paramContratosPlan.cantidadAprobados, paramInt);
  }

  private static final int jdoGetcantidadRealizados(ContratosPlan paramContratosPlan)
  {
    if (paramContratosPlan.jdoFlags <= 0)
      return paramContratosPlan.cantidadRealizados;
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramContratosPlan.cantidadRealizados;
    if (localStateManager.isLoaded(paramContratosPlan, jdoInheritedFieldCount + 1))
      return paramContratosPlan.cantidadRealizados;
    return localStateManager.getIntField(paramContratosPlan, jdoInheritedFieldCount + 1, paramContratosPlan.cantidadRealizados);
  }

  private static final void jdoSetcantidadRealizados(ContratosPlan paramContratosPlan, int paramInt)
  {
    if (paramContratosPlan.jdoFlags == 0)
    {
      paramContratosPlan.cantidadRealizados = paramInt;
      return;
    }
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratosPlan.cantidadRealizados = paramInt;
      return;
    }
    localStateManager.setIntField(paramContratosPlan, jdoInheritedFieldCount + 1, paramContratosPlan.cantidadRealizados, paramInt);
  }

  private static final long jdoGetidContratosPlan(ContratosPlan paramContratosPlan)
  {
    return paramContratosPlan.idContratosPlan;
  }

  private static final void jdoSetidContratosPlan(ContratosPlan paramContratosPlan, long paramLong)
  {
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratosPlan.idContratosPlan = paramLong;
      return;
    }
    localStateManager.setLongField(paramContratosPlan, jdoInheritedFieldCount + 2, paramContratosPlan.idContratosPlan, paramLong);
  }

  private static final PlanPersonal jdoGetplanPersonal(ContratosPlan paramContratosPlan)
  {
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramContratosPlan.planPersonal;
    if (localStateManager.isLoaded(paramContratosPlan, jdoInheritedFieldCount + 3))
      return paramContratosPlan.planPersonal;
    return (PlanPersonal)localStateManager.getObjectField(paramContratosPlan, jdoInheritedFieldCount + 3, paramContratosPlan.planPersonal);
  }

  private static final void jdoSetplanPersonal(ContratosPlan paramContratosPlan, PlanPersonal paramPlanPersonal)
  {
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratosPlan.planPersonal = paramPlanPersonal;
      return;
    }
    localStateManager.setObjectField(paramContratosPlan, jdoInheritedFieldCount + 3, paramContratosPlan.planPersonal, paramPlanPersonal);
  }

  private static final Region jdoGetregion(ContratosPlan paramContratosPlan)
  {
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramContratosPlan.region;
    if (localStateManager.isLoaded(paramContratosPlan, jdoInheritedFieldCount + 4))
      return paramContratosPlan.region;
    return (Region)localStateManager.getObjectField(paramContratosPlan, jdoInheritedFieldCount + 4, paramContratosPlan.region);
  }

  private static final void jdoSetregion(ContratosPlan paramContratosPlan, Region paramRegion)
  {
    StateManager localStateManager = paramContratosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramContratosPlan.region = paramRegion;
      return;
    }
    localStateManager.setObjectField(paramContratosPlan, jdoInheritedFieldCount + 4, paramContratosPlan.region, paramRegion);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}