package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.UnidadFuncional;

public class PlanJubilacion
  implements Serializable, PersistenceCapable
{
  private long idPlanJubilacion;
  private int anio;
  private int trabajadoresAprobado;
  private int trabajadoresJubilados;
  private String tipoCargo;
  private TipoPersonal tipoPersonal;
  private UnidadFuncional unidadFuncional;
  private String aprobacionMpd;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "aprobacionMpd", "idPlanJubilacion", "tipoCargo", "tipoPersonal", "trabajadoresAprobado", "trabajadoresJubilados", "unidadFuncional" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadFuncional") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public int getAnio()
  {
    return jdoGetanio(this);
  }

  public String getAprobacionMpd()
  {
    return jdoGetaprobacionMpd(this);
  }

  public long getIdPlanJubilacion()
  {
    return jdoGetidPlanJubilacion(this);
  }

  public String getTipoCargo()
  {
    return jdoGettipoCargo(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public int getTrabajadoresAprobado()
  {
    return jdoGettrabajadoresAprobado(this);
  }

  public int getTrabajadoresJubilados()
  {
    return jdoGettrabajadoresJubilados(this);
  }

  public UnidadFuncional getUnidadFuncional()
  {
    return jdoGetunidadFuncional(this);
  }

  public void setAnio(int i)
  {
    jdoSetanio(this, i);
  }

  public void setAprobacionMpd(String string)
  {
    jdoSetaprobacionMpd(this, string);
  }

  public void setIdPlanJubilacion(long l)
  {
    jdoSetidPlanJubilacion(this, l);
  }

  public void setTipoCargo(String string)
  {
    jdoSettipoCargo(this, string);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public void setTrabajadoresAprobado(int i)
  {
    jdoSettrabajadoresAprobado(this, i);
  }

  public void setTrabajadoresJubilados(int i)
  {
    jdoSettrabajadoresJubilados(this, i);
  }

  public void setUnidadFuncional(UnidadFuncional funcional)
  {
    jdoSetunidadFuncional(this, funcional);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanJubilacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PlanJubilacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PlanJubilacion localPlanJubilacion = new PlanJubilacion();
    localPlanJubilacion.jdoFlags = 1;
    localPlanJubilacion.jdoStateManager = paramStateManager;
    return localPlanJubilacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PlanJubilacion localPlanJubilacion = new PlanJubilacion();
    localPlanJubilacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPlanJubilacion.jdoFlags = 1;
    localPlanJubilacion.jdoStateManager = paramStateManager;
    return localPlanJubilacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobacionMpd);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPlanJubilacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoCargo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.trabajadoresAprobado);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.trabajadoresJubilados);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadFuncional);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobacionMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPlanJubilacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoCargo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajadoresAprobado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajadoresJubilados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadFuncional = ((UnidadFuncional)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PlanJubilacion paramPlanJubilacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramPlanJubilacion.anio;
      return;
    case 1:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.aprobacionMpd = paramPlanJubilacion.aprobacionMpd;
      return;
    case 2:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.idPlanJubilacion = paramPlanJubilacion.idPlanJubilacion;
      return;
    case 3:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoCargo = paramPlanJubilacion.tipoCargo;
      return;
    case 4:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramPlanJubilacion.tipoPersonal;
      return;
    case 5:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.trabajadoresAprobado = paramPlanJubilacion.trabajadoresAprobado;
      return;
    case 6:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.trabajadoresJubilados = paramPlanJubilacion.trabajadoresJubilados;
      return;
    case 7:
      if (paramPlanJubilacion == null)
        throw new IllegalArgumentException("arg1");
      this.unidadFuncional = paramPlanJubilacion.unidadFuncional;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PlanJubilacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PlanJubilacion localPlanJubilacion = (PlanJubilacion)paramObject;
    if (localPlanJubilacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPlanJubilacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PlanJubilacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PlanJubilacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanJubilacionPK))
      throw new IllegalArgumentException("arg1");
    PlanJubilacionPK localPlanJubilacionPK = (PlanJubilacionPK)paramObject;
    localPlanJubilacionPK.idPlanJubilacion = this.idPlanJubilacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PlanJubilacionPK))
      throw new IllegalArgumentException("arg1");
    PlanJubilacionPK localPlanJubilacionPK = (PlanJubilacionPK)paramObject;
    this.idPlanJubilacion = localPlanJubilacionPK.idPlanJubilacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanJubilacionPK))
      throw new IllegalArgumentException("arg2");
    PlanJubilacionPK localPlanJubilacionPK = (PlanJubilacionPK)paramObject;
    localPlanJubilacionPK.idPlanJubilacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PlanJubilacionPK))
      throw new IllegalArgumentException("arg2");
    PlanJubilacionPK localPlanJubilacionPK = (PlanJubilacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localPlanJubilacionPK.idPlanJubilacion);
  }

  private static final int jdoGetanio(PlanJubilacion paramPlanJubilacion)
  {
    if (paramPlanJubilacion.jdoFlags <= 0)
      return paramPlanJubilacion.anio;
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanJubilacion.anio;
    if (localStateManager.isLoaded(paramPlanJubilacion, jdoInheritedFieldCount + 0))
      return paramPlanJubilacion.anio;
    return localStateManager.getIntField(paramPlanJubilacion, jdoInheritedFieldCount + 0, paramPlanJubilacion.anio);
  }

  private static final void jdoSetanio(PlanJubilacion paramPlanJubilacion, int paramInt)
  {
    if (paramPlanJubilacion.jdoFlags == 0)
    {
      paramPlanJubilacion.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanJubilacion, jdoInheritedFieldCount + 0, paramPlanJubilacion.anio, paramInt);
  }

  private static final String jdoGetaprobacionMpd(PlanJubilacion paramPlanJubilacion)
  {
    if (paramPlanJubilacion.jdoFlags <= 0)
      return paramPlanJubilacion.aprobacionMpd;
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanJubilacion.aprobacionMpd;
    if (localStateManager.isLoaded(paramPlanJubilacion, jdoInheritedFieldCount + 1))
      return paramPlanJubilacion.aprobacionMpd;
    return localStateManager.getStringField(paramPlanJubilacion, jdoInheritedFieldCount + 1, paramPlanJubilacion.aprobacionMpd);
  }

  private static final void jdoSetaprobacionMpd(PlanJubilacion paramPlanJubilacion, String paramString)
  {
    if (paramPlanJubilacion.jdoFlags == 0)
    {
      paramPlanJubilacion.aprobacionMpd = paramString;
      return;
    }
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.aprobacionMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanJubilacion, jdoInheritedFieldCount + 1, paramPlanJubilacion.aprobacionMpd, paramString);
  }

  private static final long jdoGetidPlanJubilacion(PlanJubilacion paramPlanJubilacion)
  {
    return paramPlanJubilacion.idPlanJubilacion;
  }

  private static final void jdoSetidPlanJubilacion(PlanJubilacion paramPlanJubilacion, long paramLong)
  {
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.idPlanJubilacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramPlanJubilacion, jdoInheritedFieldCount + 2, paramPlanJubilacion.idPlanJubilacion, paramLong);
  }

  private static final String jdoGettipoCargo(PlanJubilacion paramPlanJubilacion)
  {
    if (paramPlanJubilacion.jdoFlags <= 0)
      return paramPlanJubilacion.tipoCargo;
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanJubilacion.tipoCargo;
    if (localStateManager.isLoaded(paramPlanJubilacion, jdoInheritedFieldCount + 3))
      return paramPlanJubilacion.tipoCargo;
    return localStateManager.getStringField(paramPlanJubilacion, jdoInheritedFieldCount + 3, paramPlanJubilacion.tipoCargo);
  }

  private static final void jdoSettipoCargo(PlanJubilacion paramPlanJubilacion, String paramString)
  {
    if (paramPlanJubilacion.jdoFlags == 0)
    {
      paramPlanJubilacion.tipoCargo = paramString;
      return;
    }
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.tipoCargo = paramString;
      return;
    }
    localStateManager.setStringField(paramPlanJubilacion, jdoInheritedFieldCount + 3, paramPlanJubilacion.tipoCargo, paramString);
  }

  private static final TipoPersonal jdoGettipoPersonal(PlanJubilacion paramPlanJubilacion)
  {
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanJubilacion.tipoPersonal;
    if (localStateManager.isLoaded(paramPlanJubilacion, jdoInheritedFieldCount + 4))
      return paramPlanJubilacion.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramPlanJubilacion, jdoInheritedFieldCount + 4, paramPlanJubilacion.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(PlanJubilacion paramPlanJubilacion, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramPlanJubilacion, jdoInheritedFieldCount + 4, paramPlanJubilacion.tipoPersonal, paramTipoPersonal);
  }

  private static final int jdoGettrabajadoresAprobado(PlanJubilacion paramPlanJubilacion)
  {
    if (paramPlanJubilacion.jdoFlags <= 0)
      return paramPlanJubilacion.trabajadoresAprobado;
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanJubilacion.trabajadoresAprobado;
    if (localStateManager.isLoaded(paramPlanJubilacion, jdoInheritedFieldCount + 5))
      return paramPlanJubilacion.trabajadoresAprobado;
    return localStateManager.getIntField(paramPlanJubilacion, jdoInheritedFieldCount + 5, paramPlanJubilacion.trabajadoresAprobado);
  }

  private static final void jdoSettrabajadoresAprobado(PlanJubilacion paramPlanJubilacion, int paramInt)
  {
    if (paramPlanJubilacion.jdoFlags == 0)
    {
      paramPlanJubilacion.trabajadoresAprobado = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.trabajadoresAprobado = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanJubilacion, jdoInheritedFieldCount + 5, paramPlanJubilacion.trabajadoresAprobado, paramInt);
  }

  private static final int jdoGettrabajadoresJubilados(PlanJubilacion paramPlanJubilacion)
  {
    if (paramPlanJubilacion.jdoFlags <= 0)
      return paramPlanJubilacion.trabajadoresJubilados;
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanJubilacion.trabajadoresJubilados;
    if (localStateManager.isLoaded(paramPlanJubilacion, jdoInheritedFieldCount + 6))
      return paramPlanJubilacion.trabajadoresJubilados;
    return localStateManager.getIntField(paramPlanJubilacion, jdoInheritedFieldCount + 6, paramPlanJubilacion.trabajadoresJubilados);
  }

  private static final void jdoSettrabajadoresJubilados(PlanJubilacion paramPlanJubilacion, int paramInt)
  {
    if (paramPlanJubilacion.jdoFlags == 0)
    {
      paramPlanJubilacion.trabajadoresJubilados = paramInt;
      return;
    }
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.trabajadoresJubilados = paramInt;
      return;
    }
    localStateManager.setIntField(paramPlanJubilacion, jdoInheritedFieldCount + 6, paramPlanJubilacion.trabajadoresJubilados, paramInt);
  }

  private static final UnidadFuncional jdoGetunidadFuncional(PlanJubilacion paramPlanJubilacion)
  {
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
      return paramPlanJubilacion.unidadFuncional;
    if (localStateManager.isLoaded(paramPlanJubilacion, jdoInheritedFieldCount + 7))
      return paramPlanJubilacion.unidadFuncional;
    return (UnidadFuncional)localStateManager.getObjectField(paramPlanJubilacion, jdoInheritedFieldCount + 7, paramPlanJubilacion.unidadFuncional);
  }

  private static final void jdoSetunidadFuncional(PlanJubilacion paramPlanJubilacion, UnidadFuncional paramUnidadFuncional)
  {
    StateManager localStateManager = paramPlanJubilacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPlanJubilacion.unidadFuncional = paramUnidadFuncional;
      return;
    }
    localStateManager.setObjectField(paramPlanJubilacion, jdoInheritedFieldCount + 7, paramPlanJubilacion.unidadFuncional, paramUnidadFuncional);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}