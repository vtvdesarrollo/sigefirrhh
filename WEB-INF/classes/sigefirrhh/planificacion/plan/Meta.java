package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Meta
  implements Serializable, PersistenceCapable
{
  private long idMeta;
  private AccionObjetivo accionObjetivo;
  private int numero;
  private String enunciado;
  private String descripcion;
  private double cantidad;
  private String unidades;
  private String tiempoEstimado;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "accionObjetivo", "cantidad", "descripcion", "enunciado", "idMeta", "numero", "tiempoEstimado", "unidades" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.planificacion.plan.AccionObjetivo"), Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 26, 21, 21, 21, 24, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Meta()
  {
    jdoSetnumero(this, 1);

    jdoSetcantidad(this, 0.0D);
  }

  public String toString()
  {
    return jdoGetdescripcion(this);
  }

  public AccionObjetivo getAccionObjetivo()
  {
    return jdoGetaccionObjetivo(this);
  }
  public void setAccionObjetivo(AccionObjetivo accionObjetivo) {
    jdoSetaccionObjetivo(this, accionObjetivo);
  }
  public double getCantidad() {
    return jdoGetcantidad(this);
  }
  public void setCantidad(double cantidad) {
    jdoSetcantidad(this, cantidad);
  }
  public String getDescripcion() {
    return jdoGetdescripcion(this);
  }
  public void setDescripcion(String descripcion) {
    jdoSetdescripcion(this, descripcion);
  }
  public String getEnunciado() {
    return jdoGetenunciado(this);
  }
  public void setEnunciado(String enunciado) {
    jdoSetenunciado(this, enunciado);
  }
  public long getIdMeta() {
    return jdoGetidMeta(this);
  }
  public void setIdMeta(long idMeta) {
    jdoSetidMeta(this, idMeta);
  }
  public int getNumero() {
    return jdoGetnumero(this);
  }
  public void setNumero(int numero) {
    jdoSetnumero(this, numero);
  }
  public String getTiempoEstimado() {
    return jdoGettiempoEstimado(this);
  }
  public void setTiempoEstimado(String tiempoEstimado) {
    jdoSettiempoEstimado(this, tiempoEstimado);
  }
  public String getUnidades() {
    return jdoGetunidades(this);
  }
  public void setUnidades(String unidades) {
    jdoSetunidades(this, unidades);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.Meta"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Meta());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Meta localMeta = new Meta();
    localMeta.jdoFlags = 1;
    localMeta.jdoStateManager = paramStateManager;
    return localMeta;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Meta localMeta = new Meta();
    localMeta.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMeta.jdoFlags = 1;
    localMeta.jdoStateManager = paramStateManager;
    return localMeta;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.accionObjetivo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.cantidad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.enunciado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMeta);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numero);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tiempoEstimado);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.unidades);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accionObjetivo = ((AccionObjetivo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidad = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.enunciado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMeta = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numero = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tiempoEstimado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidades = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Meta paramMeta, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.accionObjetivo = paramMeta.accionObjetivo;
      return;
    case 1:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.cantidad = paramMeta.cantidad;
      return;
    case 2:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramMeta.descripcion;
      return;
    case 3:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.enunciado = paramMeta.enunciado;
      return;
    case 4:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.idMeta = paramMeta.idMeta;
      return;
    case 5:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.numero = paramMeta.numero;
      return;
    case 6:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.tiempoEstimado = paramMeta.tiempoEstimado;
      return;
    case 7:
      if (paramMeta == null)
        throw new IllegalArgumentException("arg1");
      this.unidades = paramMeta.unidades;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Meta))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Meta localMeta = (Meta)paramObject;
    if (localMeta.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMeta, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MetaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MetaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MetaPK))
      throw new IllegalArgumentException("arg1");
    MetaPK localMetaPK = (MetaPK)paramObject;
    localMetaPK.idMeta = this.idMeta;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MetaPK))
      throw new IllegalArgumentException("arg1");
    MetaPK localMetaPK = (MetaPK)paramObject;
    this.idMeta = localMetaPK.idMeta;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MetaPK))
      throw new IllegalArgumentException("arg2");
    MetaPK localMetaPK = (MetaPK)paramObject;
    localMetaPK.idMeta = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MetaPK))
      throw new IllegalArgumentException("arg2");
    MetaPK localMetaPK = (MetaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localMetaPK.idMeta);
  }

  private static final AccionObjetivo jdoGetaccionObjetivo(Meta paramMeta)
  {
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
      return paramMeta.accionObjetivo;
    if (localStateManager.isLoaded(paramMeta, jdoInheritedFieldCount + 0))
      return paramMeta.accionObjetivo;
    return (AccionObjetivo)localStateManager.getObjectField(paramMeta, jdoInheritedFieldCount + 0, paramMeta.accionObjetivo);
  }

  private static final void jdoSetaccionObjetivo(Meta paramMeta, AccionObjetivo paramAccionObjetivo)
  {
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.accionObjetivo = paramAccionObjetivo;
      return;
    }
    localStateManager.setObjectField(paramMeta, jdoInheritedFieldCount + 0, paramMeta.accionObjetivo, paramAccionObjetivo);
  }

  private static final double jdoGetcantidad(Meta paramMeta)
  {
    if (paramMeta.jdoFlags <= 0)
      return paramMeta.cantidad;
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
      return paramMeta.cantidad;
    if (localStateManager.isLoaded(paramMeta, jdoInheritedFieldCount + 1))
      return paramMeta.cantidad;
    return localStateManager.getDoubleField(paramMeta, jdoInheritedFieldCount + 1, paramMeta.cantidad);
  }

  private static final void jdoSetcantidad(Meta paramMeta, double paramDouble)
  {
    if (paramMeta.jdoFlags == 0)
    {
      paramMeta.cantidad = paramDouble;
      return;
    }
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.cantidad = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramMeta, jdoInheritedFieldCount + 1, paramMeta.cantidad, paramDouble);
  }

  private static final String jdoGetdescripcion(Meta paramMeta)
  {
    if (paramMeta.jdoFlags <= 0)
      return paramMeta.descripcion;
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
      return paramMeta.descripcion;
    if (localStateManager.isLoaded(paramMeta, jdoInheritedFieldCount + 2))
      return paramMeta.descripcion;
    return localStateManager.getStringField(paramMeta, jdoInheritedFieldCount + 2, paramMeta.descripcion);
  }

  private static final void jdoSetdescripcion(Meta paramMeta, String paramString)
  {
    if (paramMeta.jdoFlags == 0)
    {
      paramMeta.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramMeta, jdoInheritedFieldCount + 2, paramMeta.descripcion, paramString);
  }

  private static final String jdoGetenunciado(Meta paramMeta)
  {
    if (paramMeta.jdoFlags <= 0)
      return paramMeta.enunciado;
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
      return paramMeta.enunciado;
    if (localStateManager.isLoaded(paramMeta, jdoInheritedFieldCount + 3))
      return paramMeta.enunciado;
    return localStateManager.getStringField(paramMeta, jdoInheritedFieldCount + 3, paramMeta.enunciado);
  }

  private static final void jdoSetenunciado(Meta paramMeta, String paramString)
  {
    if (paramMeta.jdoFlags == 0)
    {
      paramMeta.enunciado = paramString;
      return;
    }
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.enunciado = paramString;
      return;
    }
    localStateManager.setStringField(paramMeta, jdoInheritedFieldCount + 3, paramMeta.enunciado, paramString);
  }

  private static final long jdoGetidMeta(Meta paramMeta)
  {
    return paramMeta.idMeta;
  }

  private static final void jdoSetidMeta(Meta paramMeta, long paramLong)
  {
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.idMeta = paramLong;
      return;
    }
    localStateManager.setLongField(paramMeta, jdoInheritedFieldCount + 4, paramMeta.idMeta, paramLong);
  }

  private static final int jdoGetnumero(Meta paramMeta)
  {
    if (paramMeta.jdoFlags <= 0)
      return paramMeta.numero;
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
      return paramMeta.numero;
    if (localStateManager.isLoaded(paramMeta, jdoInheritedFieldCount + 5))
      return paramMeta.numero;
    return localStateManager.getIntField(paramMeta, jdoInheritedFieldCount + 5, paramMeta.numero);
  }

  private static final void jdoSetnumero(Meta paramMeta, int paramInt)
  {
    if (paramMeta.jdoFlags == 0)
    {
      paramMeta.numero = paramInt;
      return;
    }
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.numero = paramInt;
      return;
    }
    localStateManager.setIntField(paramMeta, jdoInheritedFieldCount + 5, paramMeta.numero, paramInt);
  }

  private static final String jdoGettiempoEstimado(Meta paramMeta)
  {
    if (paramMeta.jdoFlags <= 0)
      return paramMeta.tiempoEstimado;
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
      return paramMeta.tiempoEstimado;
    if (localStateManager.isLoaded(paramMeta, jdoInheritedFieldCount + 6))
      return paramMeta.tiempoEstimado;
    return localStateManager.getStringField(paramMeta, jdoInheritedFieldCount + 6, paramMeta.tiempoEstimado);
  }

  private static final void jdoSettiempoEstimado(Meta paramMeta, String paramString)
  {
    if (paramMeta.jdoFlags == 0)
    {
      paramMeta.tiempoEstimado = paramString;
      return;
    }
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.tiempoEstimado = paramString;
      return;
    }
    localStateManager.setStringField(paramMeta, jdoInheritedFieldCount + 6, paramMeta.tiempoEstimado, paramString);
  }

  private static final String jdoGetunidades(Meta paramMeta)
  {
    if (paramMeta.jdoFlags <= 0)
      return paramMeta.unidades;
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
      return paramMeta.unidades;
    if (localStateManager.isLoaded(paramMeta, jdoInheritedFieldCount + 7))
      return paramMeta.unidades;
    return localStateManager.getStringField(paramMeta, jdoInheritedFieldCount + 7, paramMeta.unidades);
  }

  private static final void jdoSetunidades(Meta paramMeta, String paramString)
  {
    if (paramMeta.jdoFlags == 0)
    {
      paramMeta.unidades = paramString;
      return;
    }
    StateManager localStateManager = paramMeta.jdoStateManager;
    if (localStateManager == null)
    {
      paramMeta.unidades = paramString;
      return;
    }
    localStateManager.setStringField(paramMeta, jdoInheritedFieldCount + 7, paramMeta.unidades, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}