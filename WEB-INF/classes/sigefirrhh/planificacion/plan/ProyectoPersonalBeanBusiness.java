package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;
import sigefirrhh.personal.sigecof.AccionCentralizada;
import sigefirrhh.personal.sigecof.AccionCentralizadaBeanBusiness;
import sigefirrhh.personal.sigecof.Proyecto;
import sigefirrhh.personal.sigecof.ProyectoBeanBusiness;

public class ProyectoPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addProyectoPersonal(ProyectoPersonal proyectoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ProyectoPersonal proyectoPersonalNew = 
      (ProyectoPersonal)BeanUtils.cloneBean(
      proyectoPersonal);

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (proyectoPersonalNew.getPlanPersonal() != null) {
      proyectoPersonalNew.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        proyectoPersonalNew.getPlanPersonal().getIdPlanPersonal()));
    }

    SubsistemaBeanBusiness subsistemaBeanBusiness = new SubsistemaBeanBusiness();

    if (proyectoPersonalNew.getSubsistema() != null) {
      proyectoPersonalNew.setSubsistema(
        subsistemaBeanBusiness.findSubsistemaById(
        proyectoPersonalNew.getSubsistema().getIdSubsistema()));
    }

    ProyectoBeanBusiness proyectoBeanBusiness = new ProyectoBeanBusiness();

    if (proyectoPersonalNew.getProyecto() != null) {
      proyectoPersonalNew.setProyecto(
        proyectoBeanBusiness.findProyectoById(
        proyectoPersonalNew.getProyecto().getIdProyecto()));
    }

    AccionCentralizadaBeanBusiness accionCentralizadaBeanBusiness = new AccionCentralizadaBeanBusiness();

    if (proyectoPersonalNew.getAccionCentralizada() != null) {
      proyectoPersonalNew.setAccionCentralizada(
        accionCentralizadaBeanBusiness.findAccionCentralizadaById(
        proyectoPersonalNew.getAccionCentralizada().getIdAccionCentralizada()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (proyectoPersonalNew.getOrganismo() != null) {
      proyectoPersonalNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        proyectoPersonalNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(proyectoPersonalNew);
  }

  public void updateProyectoPersonal(ProyectoPersonal proyectoPersonal) throws Exception
  {
    ProyectoPersonal proyectoPersonalModify = 
      findProyectoPersonalById(proyectoPersonal.getIdProyectoPersonal());

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (proyectoPersonal.getPlanPersonal() != null) {
      proyectoPersonal.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        proyectoPersonal.getPlanPersonal().getIdPlanPersonal()));
    }

    SubsistemaBeanBusiness subsistemaBeanBusiness = new SubsistemaBeanBusiness();

    if (proyectoPersonal.getSubsistema() != null) {
      proyectoPersonal.setSubsistema(
        subsistemaBeanBusiness.findSubsistemaById(
        proyectoPersonal.getSubsistema().getIdSubsistema()));
    }

    ProyectoBeanBusiness proyectoBeanBusiness = new ProyectoBeanBusiness();

    if (proyectoPersonal.getProyecto() != null) {
      proyectoPersonal.setProyecto(
        proyectoBeanBusiness.findProyectoById(
        proyectoPersonal.getProyecto().getIdProyecto()));
    }

    AccionCentralizadaBeanBusiness accionCentralizadaBeanBusiness = new AccionCentralizadaBeanBusiness();

    if (proyectoPersonal.getAccionCentralizada() != null) {
      proyectoPersonal.setAccionCentralizada(
        accionCentralizadaBeanBusiness.findAccionCentralizadaById(
        proyectoPersonal.getAccionCentralizada().getIdAccionCentralizada()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (proyectoPersonal.getOrganismo() != null) {
      proyectoPersonal.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        proyectoPersonal.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(proyectoPersonalModify, proyectoPersonal);
  }

  public void deleteProyectoPersonal(ProyectoPersonal proyectoPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ProyectoPersonal proyectoPersonalDelete = 
      findProyectoPersonalById(proyectoPersonal.getIdProyectoPersonal());
    pm.deletePersistent(proyectoPersonalDelete);
  }

  public ProyectoPersonal findProyectoPersonalById(long idProyectoPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idProyectoPersonal == pIdProyectoPersonal";
    Query query = pm.newQuery(ProyectoPersonal.class, filter);

    query.declareParameters("long pIdProyectoPersonal");

    parameters.put("pIdProyectoPersonal", new Long(idProyectoPersonal));

    Collection colProyectoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colProyectoPersonal.iterator();
    return (ProyectoPersonal)iterator.next();
  }

  public Collection findProyectoPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent proyectoPersonalExtent = pm.getExtent(
      ProyectoPersonal.class, true);
    Query query = pm.newQuery(proyectoPersonalExtent);
    query.setOrdering("subsistema.codSubsistema ascending, numero ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPlanPersonal(long idPlanPersonal, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPersonal.idPlanPersonal == pIdPlanPersonal &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ProyectoPersonal.class, filter);

    query.declareParameters("long pIdPlanPersonal, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPersonal", new Long(idPlanPersonal));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("subsistema.codSubsistema ascending, numero ascending");

    Collection colProyectoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProyectoPersonal);

    return colProyectoPersonal;
  }

  public Collection findByNumero(int numero, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "numero == pNumero &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ProyectoPersonal.class, filter);

    query.declareParameters("int pNumero, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNumero", new Integer(numero));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("subsistema.codSubsistema ascending, numero ascending");

    Collection colProyectoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProyectoPersonal);

    return colProyectoPersonal;
  }

  public Collection findByEnunciado(String enunciado, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "enunciado.startsWith(pEnunciado) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ProyectoPersonal.class, filter);

    query.declareParameters("java.lang.String pEnunciado, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pEnunciado", new String(enunciado));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("subsistema.codSubsistema ascending, numero ascending");

    Collection colProyectoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProyectoPersonal);

    return colProyectoPersonal;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(ProyectoPersonal.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("subsistema.codSubsistema ascending, numero ascending");

    Collection colProyectoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colProyectoPersonal);

    return colProyectoPersonal;
  }
}