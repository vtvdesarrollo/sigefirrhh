package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.registro.MovimientoCargo;
import sigefirrhh.base.registro.MovimientoCargoBeanBusiness;

public class CargosPlanBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addCargosPlan(CargosPlan cargosPlan)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    CargosPlan cargosPlanNew = 
      (CargosPlan)BeanUtils.cloneBean(
      cargosPlan);

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (cargosPlanNew.getPlanPersonal() != null) {
      cargosPlanNew.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        cargosPlanNew.getPlanPersonal().getIdPlanPersonal()));
    }

    MovimientoCargoBeanBusiness movimientoCargoBeanBusiness = new MovimientoCargoBeanBusiness();

    if (cargosPlanNew.getMovimientoCargo() != null) {
      cargosPlanNew.setMovimientoCargo(
        movimientoCargoBeanBusiness.findMovimientoCargoById(
        cargosPlanNew.getMovimientoCargo().getIdMovimientoCargo()));
    }
    pm.makePersistent(cargosPlanNew);
  }

  public void updateCargosPlan(CargosPlan cargosPlan) throws Exception
  {
    CargosPlan cargosPlanModify = 
      findCargosPlanById(cargosPlan.getIdCargosPlan());

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (cargosPlan.getPlanPersonal() != null) {
      cargosPlan.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        cargosPlan.getPlanPersonal().getIdPlanPersonal()));
    }

    MovimientoCargoBeanBusiness movimientoCargoBeanBusiness = new MovimientoCargoBeanBusiness();

    if (cargosPlan.getMovimientoCargo() != null) {
      cargosPlan.setMovimientoCargo(
        movimientoCargoBeanBusiness.findMovimientoCargoById(
        cargosPlan.getMovimientoCargo().getIdMovimientoCargo()));
    }

    BeanUtils.copyProperties(cargosPlanModify, cargosPlan);
  }

  public void deleteCargosPlan(CargosPlan cargosPlan) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    CargosPlan cargosPlanDelete = 
      findCargosPlanById(cargosPlan.getIdCargosPlan());
    pm.deletePersistent(cargosPlanDelete);
  }

  public CargosPlan findCargosPlanById(long idCargosPlan) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idCargosPlan == pIdCargosPlan";
    Query query = pm.newQuery(CargosPlan.class, filter);

    query.declareParameters("long pIdCargosPlan");

    parameters.put("pIdCargosPlan", new Long(idCargosPlan));

    Collection colCargosPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colCargosPlan.iterator();
    return (CargosPlan)iterator.next();
  }

  public Collection findCargosPlanAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent cargosPlanExtent = pm.getExtent(
      CargosPlan.class, true);
    Query query = pm.newQuery(cargosPlanExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPersonal.idPlanPersonal == pIdPlanPersonal";

    Query query = pm.newQuery(CargosPlan.class, filter);

    query.declareParameters("long pIdPlanPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPersonal", new Long(idPlanPersonal));

    Collection colCargosPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colCargosPlan);

    return colCargosPlan;
  }
}