package sigefirrhh.planificacion.plan;

import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPlanPersonalForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPlanPersonalForm.class.getName());
  private int reportId;
  private String reportName;
  private String reporte = "1";
  private String subsistema = "1";
  private LoginSession login;
  private int anio;

  public ReportPlanPersonalForm()
  {
    this.reportName = "ppevaluaciones";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.anio = (new Date().getYear() + 1900);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPlanPersonalForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public boolean isShowSubsistema() {
    return this.reporte.equals("2");
  }

  private void cambiarNombreAReporte() {
    this.reportName = "";

    if (this.reporte.equals("10"))
      this.reportName = "ppevaluaciones";
    else if (this.reporte.equals("20"))
      this.reportName = "ppadiestramiento";
    else if (this.reporte.equals("30"))
      this.reportName = "ppjubilaciones";
    else if (this.reporte.equals("40"))
      this.reportName = "ppreccategoria";
  }

  public void refresh()
  {
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getIdOrganismo()));
    parameters.put("anio", new Integer(this.anio));
    parameters.put("mes", new Integer(new Date().getMonth() + 1));
    parameters.put("fecha_tope", new Date());

    this.reportName = "";
    if (this.reporte.equals("10"))
      this.reportName = "ppevaluaciones";
    else if (this.reporte.equals("20"))
      this.reportName = "ppadiestramiento";
    else if (this.reporte.equals("30"))
      this.reportName = "ppjubilaciones";
    else if (this.reporte.equals("40")) {
      this.reportName = "ppreccategoria";
    }

    JasperForWeb report = new JasperForWeb();

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/planificacion/plan");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }

  public int getReportId()
  {
    return this.reportId;
  }

  public void setReportId(int i)
  {
    this.reportId = i;
  }

  public String getReportName()
  {
    return this.reportName;
  }

  public void setReportName(String string)
  {
    this.reportName = string;
  }

  public int getAnio()
  {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }

  public String getReporte() {
    return this.reporte;
  }
  public void setReporte(String reporte) {
    this.reporte = reporte;
  }
  public String getSubsistema() {
    return this.subsistema;
  }
  public void setSubsistema(String subsistema) {
    this.subsistema = subsistema;
  }
}