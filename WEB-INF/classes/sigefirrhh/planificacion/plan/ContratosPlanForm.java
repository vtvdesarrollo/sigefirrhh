package sigefirrhh.planificacion.plan;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.login.LoginSession;

public class ContratosPlanForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ContratosPlanForm.class.getName());
  private ContratosPlan contratosPlan;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PlanFacade planFacade = new PlanFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showContratosPlanByPlanPersonal;
  private String findSelectPlanPersonal;
  private Collection findColPlanPersonal;
  private Collection colPlanPersonal;
  private Collection colRegion;
  private String selectPlanPersonal;
  private String selectRegion;
  private Object stateResultContratosPlanByPlanPersonal = null;

  public String getFindSelectPlanPersonal()
  {
    return this.findSelectPlanPersonal;
  }
  public void setFindSelectPlanPersonal(String valPlanPersonal) {
    this.findSelectPlanPersonal = valPlanPersonal;
  }

  public Collection getFindColPlanPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public String getSelectPlanPersonal()
  {
    return this.selectPlanPersonal;
  }
  public void setSelectPlanPersonal(String valPlanPersonal) {
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    this.contratosPlan.setPlanPersonal(null);
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      if (String.valueOf(planPersonal.getIdPlanPersonal()).equals(
        valPlanPersonal)) {
        this.contratosPlan.setPlanPersonal(
          planPersonal);
        break;
      }
    }
    this.selectPlanPersonal = valPlanPersonal;
  }
  public String getSelectRegion() {
    return this.selectRegion;
  }
  public void setSelectRegion(String valRegion) {
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    this.contratosPlan.setRegion(null);
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      if (String.valueOf(region.getIdRegion()).equals(
        valRegion)) {
        this.contratosPlan.setRegion(
          region);
        break;
      }
    }
    this.selectRegion = valRegion;
  }
  public Collection getResult() {
    return this.result;
  }

  public ContratosPlan getContratosPlan() {
    if (this.contratosPlan == null) {
      this.contratosPlan = new ContratosPlan();
    }
    return this.contratosPlan;
  }

  public ContratosPlanForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPlanPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findContratosPlanByPlanPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.planFacade.findContratosPlanByPlanPersonal(Long.valueOf(this.findSelectPlanPersonal).longValue());
      this.showContratosPlanByPlanPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showContratosPlanByPlanPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;

    return null;
  }

  public boolean isShowContratosPlanByPlanPersonal() {
    return this.showContratosPlanByPlanPersonal;
  }

  public String selectContratosPlan()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPlanPersonal = null;
    this.selectRegion = null;

    long idContratosPlan = 
      Long.parseLong((String)requestParameterMap.get("idContratosPlan"));
    try
    {
      this.contratosPlan = 
        this.planFacade.findContratosPlanById(
        idContratosPlan);
      if (this.contratosPlan.getPlanPersonal() != null) {
        this.selectPlanPersonal = 
          String.valueOf(this.contratosPlan.getPlanPersonal().getIdPlanPersonal());
      }
      if (this.contratosPlan.getRegion() != null) {
        this.selectRegion = 
          String.valueOf(this.contratosPlan.getRegion().getIdRegion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.contratosPlan = null;
    this.showContratosPlanByPlanPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.planFacade.addContratosPlan(
          this.contratosPlan);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.planFacade.updateContratosPlan(
          this.contratosPlan);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.planFacade.deleteContratosPlan(
        this.contratosPlan);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.contratosPlan = new ContratosPlan();

    this.selectPlanPersonal = null;

    this.selectRegion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.contratosPlan.setIdContratosPlan(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.plan.ContratosPlan"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.contratosPlan = new ContratosPlan();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}