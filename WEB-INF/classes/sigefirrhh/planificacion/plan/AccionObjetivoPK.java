package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class AccionObjetivoPK
  implements Serializable
{
  public long idAccionObjetivo;

  public AccionObjetivoPK()
  {
  }

  public AccionObjetivoPK(long idAccionObjetivo)
  {
    this.idAccionObjetivo = idAccionObjetivo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AccionObjetivoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AccionObjetivoPK thatPK)
  {
    return 
      this.idAccionObjetivo == thatPK.idAccionObjetivo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAccionObjetivo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAccionObjetivo);
  }
}