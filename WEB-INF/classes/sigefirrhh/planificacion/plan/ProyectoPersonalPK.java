package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class ProyectoPersonalPK
  implements Serializable
{
  public long idProyectoPersonal;

  public ProyectoPersonalPK()
  {
  }

  public ProyectoPersonalPK(long idProyectoPersonal)
  {
    this.idProyectoPersonal = idProyectoPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProyectoPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProyectoPersonalPK thatPK)
  {
    return 
      this.idProyectoPersonal == thatPK.idProyectoPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProyectoPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProyectoPersonal);
  }
}