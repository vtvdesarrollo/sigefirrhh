package sigefirrhh.planificacion.plan;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.registro.CausaMovimiento;

public class MovimientosPlan
  implements Serializable, PersistenceCapable
{
  private long idMovimientosPlan;
  private PlanPersonal planPersonal;
  private CausaMovimiento causaMovimiento;
  private int cantidadPlanificados;
  private int cantidadRealizados;
  private int cantidadAprobados;
  private int cantidadDevueltos;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cantidadAprobados", "cantidadDevueltos", "cantidadPlanificados", "cantidadRealizados", "causaMovimiento", "idMovimientosPlan", "planPersonal" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.registro.CausaMovimiento"), Long.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.plan.PlanPersonal") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public MovimientosPlan()
  {
    jdoSetcantidadPlanificados(this, 0);

    jdoSetcantidadRealizados(this, 0);

    jdoSetcantidadAprobados(this, 0);

    jdoSetcantidadDevueltos(this, 0);
  }

  public String toString() {
    return jdoGetcausaMovimiento(this) + " Planificados: " + jdoGetcantidadPlanificados(this) + " Realizados: " + jdoGetcantidadRealizados(this);
  }

  public int getCantidadAprobados() {
    return jdoGetcantidadAprobados(this);
  }
  public void setCantidadAprobados(int cantidadAprobados) {
    jdoSetcantidadAprobados(this, cantidadAprobados);
  }
  public int getCantidadRealizados() {
    return jdoGetcantidadRealizados(this);
  }
  public void setCantidadRealizados(int cantidadRealizados) {
    jdoSetcantidadRealizados(this, cantidadRealizados);
  }
  public CausaMovimiento getCausaMovimiento() {
    return jdoGetcausaMovimiento(this);
  }
  public void setCausaMovimiento(CausaMovimiento causaMovimiento) {
    jdoSetcausaMovimiento(this, causaMovimiento);
  }
  public long getIdMovimientosPlan() {
    return jdoGetidMovimientosPlan(this);
  }
  public void setIdMovimientosPlan(long idMovimientosPlan) {
    jdoSetidMovimientosPlan(this, idMovimientosPlan);
  }
  public PlanPersonal getPlanPersonal() {
    return jdoGetplanPersonal(this);
  }
  public void setPlanPersonal(PlanPersonal planPersonal) {
    jdoSetplanPersonal(this, planPersonal);
  }
  public int getCantidadDevueltos() {
    return jdoGetcantidadDevueltos(this);
  }
  public void setCantidadDevueltos(int cantidadDevueltos) {
    jdoSetcantidadDevueltos(this, cantidadDevueltos);
  }
  public int getCantidadPlanificados() {
    return jdoGetcantidadPlanificados(this);
  }
  public void setCantidadPlanificados(int cantidadPlanificados) {
    jdoSetcantidadPlanificados(this, cantidadPlanificados);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.plan.MovimientosPlan"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new MovimientosPlan());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    MovimientosPlan localMovimientosPlan = new MovimientosPlan();
    localMovimientosPlan.jdoFlags = 1;
    localMovimientosPlan.jdoStateManager = paramStateManager;
    return localMovimientosPlan;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    MovimientosPlan localMovimientosPlan = new MovimientosPlan();
    localMovimientosPlan.jdoCopyKeyFieldsFromObjectId(paramObject);
    localMovimientosPlan.jdoFlags = 1;
    localMovimientosPlan.jdoStateManager = paramStateManager;
    return localMovimientosPlan;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadAprobados);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadDevueltos);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadPlanificados);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadRealizados);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.causaMovimiento);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idMovimientosPlan);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadAprobados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadDevueltos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadPlanificados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadRealizados = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.causaMovimiento = ((CausaMovimiento)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idMovimientosPlan = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPersonal = ((PlanPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(MovimientosPlan paramMovimientosPlan, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramMovimientosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadAprobados = paramMovimientosPlan.cantidadAprobados;
      return;
    case 1:
      if (paramMovimientosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadDevueltos = paramMovimientosPlan.cantidadDevueltos;
      return;
    case 2:
      if (paramMovimientosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadPlanificados = paramMovimientosPlan.cantidadPlanificados;
      return;
    case 3:
      if (paramMovimientosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadRealizados = paramMovimientosPlan.cantidadRealizados;
      return;
    case 4:
      if (paramMovimientosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.causaMovimiento = paramMovimientosPlan.causaMovimiento;
      return;
    case 5:
      if (paramMovimientosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.idMovimientosPlan = paramMovimientosPlan.idMovimientosPlan;
      return;
    case 6:
      if (paramMovimientosPlan == null)
        throw new IllegalArgumentException("arg1");
      this.planPersonal = paramMovimientosPlan.planPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof MovimientosPlan))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    MovimientosPlan localMovimientosPlan = (MovimientosPlan)paramObject;
    if (localMovimientosPlan.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localMovimientosPlan, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new MovimientosPlanPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new MovimientosPlanPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientosPlanPK))
      throw new IllegalArgumentException("arg1");
    MovimientosPlanPK localMovimientosPlanPK = (MovimientosPlanPK)paramObject;
    localMovimientosPlanPK.idMovimientosPlan = this.idMovimientosPlan;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof MovimientosPlanPK))
      throw new IllegalArgumentException("arg1");
    MovimientosPlanPK localMovimientosPlanPK = (MovimientosPlanPK)paramObject;
    this.idMovimientosPlan = localMovimientosPlanPK.idMovimientosPlan;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientosPlanPK))
      throw new IllegalArgumentException("arg2");
    MovimientosPlanPK localMovimientosPlanPK = (MovimientosPlanPK)paramObject;
    localMovimientosPlanPK.idMovimientosPlan = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof MovimientosPlanPK))
      throw new IllegalArgumentException("arg2");
    MovimientosPlanPK localMovimientosPlanPK = (MovimientosPlanPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localMovimientosPlanPK.idMovimientosPlan);
  }

  private static final int jdoGetcantidadAprobados(MovimientosPlan paramMovimientosPlan)
  {
    if (paramMovimientosPlan.jdoFlags <= 0)
      return paramMovimientosPlan.cantidadAprobados;
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientosPlan.cantidadAprobados;
    if (localStateManager.isLoaded(paramMovimientosPlan, jdoInheritedFieldCount + 0))
      return paramMovimientosPlan.cantidadAprobados;
    return localStateManager.getIntField(paramMovimientosPlan, jdoInheritedFieldCount + 0, paramMovimientosPlan.cantidadAprobados);
  }

  private static final void jdoSetcantidadAprobados(MovimientosPlan paramMovimientosPlan, int paramInt)
  {
    if (paramMovimientosPlan.jdoFlags == 0)
    {
      paramMovimientosPlan.cantidadAprobados = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientosPlan.cantidadAprobados = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientosPlan, jdoInheritedFieldCount + 0, paramMovimientosPlan.cantidadAprobados, paramInt);
  }

  private static final int jdoGetcantidadDevueltos(MovimientosPlan paramMovimientosPlan)
  {
    if (paramMovimientosPlan.jdoFlags <= 0)
      return paramMovimientosPlan.cantidadDevueltos;
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientosPlan.cantidadDevueltos;
    if (localStateManager.isLoaded(paramMovimientosPlan, jdoInheritedFieldCount + 1))
      return paramMovimientosPlan.cantidadDevueltos;
    return localStateManager.getIntField(paramMovimientosPlan, jdoInheritedFieldCount + 1, paramMovimientosPlan.cantidadDevueltos);
  }

  private static final void jdoSetcantidadDevueltos(MovimientosPlan paramMovimientosPlan, int paramInt)
  {
    if (paramMovimientosPlan.jdoFlags == 0)
    {
      paramMovimientosPlan.cantidadDevueltos = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientosPlan.cantidadDevueltos = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientosPlan, jdoInheritedFieldCount + 1, paramMovimientosPlan.cantidadDevueltos, paramInt);
  }

  private static final int jdoGetcantidadPlanificados(MovimientosPlan paramMovimientosPlan)
  {
    if (paramMovimientosPlan.jdoFlags <= 0)
      return paramMovimientosPlan.cantidadPlanificados;
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientosPlan.cantidadPlanificados;
    if (localStateManager.isLoaded(paramMovimientosPlan, jdoInheritedFieldCount + 2))
      return paramMovimientosPlan.cantidadPlanificados;
    return localStateManager.getIntField(paramMovimientosPlan, jdoInheritedFieldCount + 2, paramMovimientosPlan.cantidadPlanificados);
  }

  private static final void jdoSetcantidadPlanificados(MovimientosPlan paramMovimientosPlan, int paramInt)
  {
    if (paramMovimientosPlan.jdoFlags == 0)
    {
      paramMovimientosPlan.cantidadPlanificados = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientosPlan.cantidadPlanificados = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientosPlan, jdoInheritedFieldCount + 2, paramMovimientosPlan.cantidadPlanificados, paramInt);
  }

  private static final int jdoGetcantidadRealizados(MovimientosPlan paramMovimientosPlan)
  {
    if (paramMovimientosPlan.jdoFlags <= 0)
      return paramMovimientosPlan.cantidadRealizados;
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientosPlan.cantidadRealizados;
    if (localStateManager.isLoaded(paramMovimientosPlan, jdoInheritedFieldCount + 3))
      return paramMovimientosPlan.cantidadRealizados;
    return localStateManager.getIntField(paramMovimientosPlan, jdoInheritedFieldCount + 3, paramMovimientosPlan.cantidadRealizados);
  }

  private static final void jdoSetcantidadRealizados(MovimientosPlan paramMovimientosPlan, int paramInt)
  {
    if (paramMovimientosPlan.jdoFlags == 0)
    {
      paramMovimientosPlan.cantidadRealizados = paramInt;
      return;
    }
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientosPlan.cantidadRealizados = paramInt;
      return;
    }
    localStateManager.setIntField(paramMovimientosPlan, jdoInheritedFieldCount + 3, paramMovimientosPlan.cantidadRealizados, paramInt);
  }

  private static final CausaMovimiento jdoGetcausaMovimiento(MovimientosPlan paramMovimientosPlan)
  {
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientosPlan.causaMovimiento;
    if (localStateManager.isLoaded(paramMovimientosPlan, jdoInheritedFieldCount + 4))
      return paramMovimientosPlan.causaMovimiento;
    return (CausaMovimiento)localStateManager.getObjectField(paramMovimientosPlan, jdoInheritedFieldCount + 4, paramMovimientosPlan.causaMovimiento);
  }

  private static final void jdoSetcausaMovimiento(MovimientosPlan paramMovimientosPlan, CausaMovimiento paramCausaMovimiento)
  {
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientosPlan.causaMovimiento = paramCausaMovimiento;
      return;
    }
    localStateManager.setObjectField(paramMovimientosPlan, jdoInheritedFieldCount + 4, paramMovimientosPlan.causaMovimiento, paramCausaMovimiento);
  }

  private static final long jdoGetidMovimientosPlan(MovimientosPlan paramMovimientosPlan)
  {
    return paramMovimientosPlan.idMovimientosPlan;
  }

  private static final void jdoSetidMovimientosPlan(MovimientosPlan paramMovimientosPlan, long paramLong)
  {
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientosPlan.idMovimientosPlan = paramLong;
      return;
    }
    localStateManager.setLongField(paramMovimientosPlan, jdoInheritedFieldCount + 5, paramMovimientosPlan.idMovimientosPlan, paramLong);
  }

  private static final PlanPersonal jdoGetplanPersonal(MovimientosPlan paramMovimientosPlan)
  {
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
      return paramMovimientosPlan.planPersonal;
    if (localStateManager.isLoaded(paramMovimientosPlan, jdoInheritedFieldCount + 6))
      return paramMovimientosPlan.planPersonal;
    return (PlanPersonal)localStateManager.getObjectField(paramMovimientosPlan, jdoInheritedFieldCount + 6, paramMovimientosPlan.planPersonal);
  }

  private static final void jdoSetplanPersonal(MovimientosPlan paramMovimientosPlan, PlanPersonal paramPlanPersonal)
  {
    StateManager localStateManager = paramMovimientosPlan.jdoStateManager;
    if (localStateManager == null)
    {
      paramMovimientosPlan.planPersonal = paramPlanPersonal;
      return;
    }
    localStateManager.setObjectField(paramMovimientosPlan, jdoInheritedFieldCount + 6, paramMovimientosPlan.planPersonal, paramPlanPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}