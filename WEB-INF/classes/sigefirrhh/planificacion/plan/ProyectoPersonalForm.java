package sigefirrhh.planificacion.plan;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.sigecof.AccionCentralizada;
import sigefirrhh.personal.sigecof.Proyecto;
import sigefirrhh.personal.sigecof.SigecofFacade;

public class ProyectoPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ProyectoPersonalForm.class.getName());
  private ProyectoPersonal proyectoPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private PlanFacade planFacade = new PlanFacade();
  private SigecofFacade sigecofFacade = new SigecofFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showProyectoPersonalByPlanPersonal;
  private boolean showProyectoPersonalByNumero;
  private boolean showProyectoPersonalByEnunciado;
  private String findSelectPlanPersonal;
  private int findNumero;
  private String findEnunciado;
  private Collection findColPlanPersonal;
  private Collection colPlanPersonal;
  private Collection colSubsistema;
  private Collection colProyecto;
  private Collection colAccionCentralizada;
  private String selectPlanPersonal;
  private String selectSubsistema;
  private String selectProyecto;
  private String selectAccionCentralizada;
  private Object stateResultProyectoPersonalByPlanPersonal = null;

  private Object stateResultProyectoPersonalByNumero = null;

  private Object stateResultProyectoPersonalByEnunciado = null;

  public String getFindSelectPlanPersonal()
  {
    return this.findSelectPlanPersonal;
  }
  public void setFindSelectPlanPersonal(String valPlanPersonal) {
    this.findSelectPlanPersonal = valPlanPersonal;
  }

  public Collection getFindColPlanPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }
  public int getFindNumero() {
    return this.findNumero;
  }
  public void setFindNumero(int findNumero) {
    this.findNumero = findNumero;
  }
  public String getFindEnunciado() {
    return this.findEnunciado;
  }
  public void setFindEnunciado(String findEnunciado) {
    this.findEnunciado = findEnunciado;
  }

  public String getSelectPlanPersonal()
  {
    return this.selectPlanPersonal;
  }
  public void setSelectPlanPersonal(String valPlanPersonal) {
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    this.proyectoPersonal.setPlanPersonal(null);
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      if (String.valueOf(planPersonal.getIdPlanPersonal()).equals(
        valPlanPersonal)) {
        this.proyectoPersonal.setPlanPersonal(
          planPersonal);
        break;
      }
    }
    this.selectPlanPersonal = valPlanPersonal;
  }
  public String getSelectSubsistema() {
    return this.selectSubsistema;
  }
  public void setSelectSubsistema(String valSubsistema) {
    Iterator iterator = this.colSubsistema.iterator();
    Subsistema subsistema = null;
    this.proyectoPersonal.setSubsistema(null);
    while (iterator.hasNext()) {
      subsistema = (Subsistema)iterator.next();
      if (String.valueOf(subsistema.getIdSubsistema()).equals(
        valSubsistema)) {
        this.proyectoPersonal.setSubsistema(
          subsistema);
        break;
      }
    }
    this.selectSubsistema = valSubsistema;
  }
  public String getSelectProyecto() {
    return this.selectProyecto;
  }
  public void setSelectProyecto(String valProyecto) {
    Iterator iterator = this.colProyecto.iterator();
    Proyecto proyecto = null;
    this.proyectoPersonal.setProyecto(null);
    while (iterator.hasNext()) {
      proyecto = (Proyecto)iterator.next();
      if (String.valueOf(proyecto.getIdProyecto()).equals(
        valProyecto)) {
        this.proyectoPersonal.setProyecto(
          proyecto);
        break;
      }
    }
    this.selectProyecto = valProyecto;
  }
  public String getSelectAccionCentralizada() {
    return this.selectAccionCentralizada;
  }
  public void setSelectAccionCentralizada(String valAccionCentralizada) {
    Iterator iterator = this.colAccionCentralizada.iterator();
    AccionCentralizada accionCentralizada = null;
    this.proyectoPersonal.setAccionCentralizada(null);
    while (iterator.hasNext()) {
      accionCentralizada = (AccionCentralizada)iterator.next();
      if (String.valueOf(accionCentralizada.getIdAccionCentralizada()).equals(
        valAccionCentralizada)) {
        this.proyectoPersonal.setAccionCentralizada(
          accionCentralizada);
        break;
      }
    }
    this.selectAccionCentralizada = valAccionCentralizada;
  }
  public Collection getResult() {
    return this.result;
  }

  public ProyectoPersonal getProyectoPersonal() {
    if (this.proyectoPersonal == null) {
      this.proyectoPersonal = new ProyectoPersonal();
    }
    return this.proyectoPersonal;
  }

  public ProyectoPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColPlanPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPersonal.iterator();
    PlanPersonal planPersonal = null;
    while (iterator.hasNext()) {
      planPersonal = (PlanPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPersonal.getIdPlanPersonal()), 
        planPersonal.toString()));
    }
    return col;
  }

  public Collection getColSubsistema()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSubsistema.iterator();
    Subsistema subsistema = null;
    while (iterator.hasNext()) {
      subsistema = (Subsistema)iterator.next();
      col.add(new SelectItem(
        String.valueOf(subsistema.getIdSubsistema()), 
        subsistema.toString()));
    }
    return col;
  }

  public Collection getListComponente()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = ProyectoPersonal.LISTA_COMPONENTE.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColProyecto()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colProyecto.iterator();
    Proyecto proyecto = null;
    while (iterator.hasNext()) {
      proyecto = (Proyecto)iterator.next();
      col.add(new SelectItem(
        String.valueOf(proyecto.getIdProyecto()), 
        proyecto.toString()));
    }
    return col;
  }

  public Collection getColAccionCentralizada()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colAccionCentralizada.iterator();
    AccionCentralizada accionCentralizada = null;
    while (iterator.hasNext()) {
      accionCentralizada = (AccionCentralizada)iterator.next();
      col.add(new SelectItem(
        String.valueOf(accionCentralizada.getIdAccionCentralizada()), 
        accionCentralizada.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colPlanPersonal = 
        this.planFacade.findPlanPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colSubsistema = 
        this.planFacade.findAllSubsistema();
      this.colProyecto = 
        this.sigecofFacade.findProyectoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colAccionCentralizada = 
        this.sigecofFacade.findAccionCentralizadaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findProyectoPersonalByPlanPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.planFacade.findProyectoPersonalByPlanPersonal(Long.valueOf(this.findSelectPlanPersonal).longValue(), idOrganismo);
      this.showProyectoPersonalByPlanPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProyectoPersonalByPlanPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;
    this.findNumero = 0;
    this.findEnunciado = null;

    return null;
  }

  public String findProyectoPersonalByNumero()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.planFacade.findProyectoPersonalByNumero(this.findNumero, idOrganismo);
      this.showProyectoPersonalByNumero = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProyectoPersonalByNumero)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;
    this.findNumero = 0;
    this.findEnunciado = null;

    return null;
  }

  public String findProyectoPersonalByEnunciado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.planFacade.findProyectoPersonalByEnunciado(this.findEnunciado, idOrganismo);
      this.showProyectoPersonalByEnunciado = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showProyectoPersonalByEnunciado)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPlanPersonal = null;
    this.findNumero = 0;
    this.findEnunciado = null;

    return null;
  }

  public boolean isShowProyectoPersonalByPlanPersonal() {
    return this.showProyectoPersonalByPlanPersonal;
  }
  public boolean isShowProyectoPersonalByNumero() {
    return this.showProyectoPersonalByNumero;
  }
  public boolean isShowProyectoPersonalByEnunciado() {
    return this.showProyectoPersonalByEnunciado;
  }

  public String selectProyectoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPlanPersonal = null;
    this.selectSubsistema = null;
    this.selectProyecto = null;
    this.selectAccionCentralizada = null;

    long idProyectoPersonal = 
      Long.parseLong((String)requestParameterMap.get("idProyectoPersonal"));
    try
    {
      this.proyectoPersonal = 
        this.planFacade.findProyectoPersonalById(
        idProyectoPersonal);
      if (this.proyectoPersonal.getPlanPersonal() != null) {
        this.selectPlanPersonal = 
          String.valueOf(this.proyectoPersonal.getPlanPersonal().getIdPlanPersonal());
      }
      if (this.proyectoPersonal.getSubsistema() != null) {
        this.selectSubsistema = 
          String.valueOf(this.proyectoPersonal.getSubsistema().getIdSubsistema());
      }
      if (this.proyectoPersonal.getProyecto() != null) {
        this.selectProyecto = 
          String.valueOf(this.proyectoPersonal.getProyecto().getIdProyecto());
      }
      if (this.proyectoPersonal.getAccionCentralizada() != null) {
        this.selectAccionCentralizada = 
          String.valueOf(this.proyectoPersonal.getAccionCentralizada().getIdAccionCentralizada());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.proyectoPersonal = null;
    this.showProyectoPersonalByPlanPersonal = false;
    this.showProyectoPersonalByNumero = false;
    this.showProyectoPersonalByEnunciado = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.planFacade.addProyectoPersonal(
          this.proyectoPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.planFacade.updateProyectoPersonal(
          this.proyectoPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.planFacade.deleteProyectoPersonal(
        this.proyectoPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.proyectoPersonal = new ProyectoPersonal();

    this.selectPlanPersonal = null;

    this.selectSubsistema = null;

    this.selectProyecto = null;

    this.selectAccionCentralizada = null;

    this.proyectoPersonal.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.proyectoPersonal.setIdProyectoPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.plan.ProyectoPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.proyectoPersonal = new ProyectoPersonal();
    return "cancel";
  }

  public boolean isShowProyectoAux()
  {
    try
    {
      return this.proyectoPersonal.getComponente().equals("P"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isShowAccionCentralizadaAux()
  {
    try {
      return this.proyectoPersonal.getComponente().equals("A"); } catch (Exception e) {
    }
    return false;
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}