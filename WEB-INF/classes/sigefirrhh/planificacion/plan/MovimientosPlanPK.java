package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class MovimientosPlanPK
  implements Serializable
{
  public long idMovimientosPlan;

  public MovimientosPlanPK()
  {
  }

  public MovimientosPlanPK(long idMovimientosPlan)
  {
    this.idMovimientosPlan = idMovimientosPlan;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((MovimientosPlanPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(MovimientosPlanPK thatPK)
  {
    return 
      this.idMovimientosPlan == thatPK.idMovimientosPlan;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idMovimientosPlan)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idMovimientosPlan);
  }
}