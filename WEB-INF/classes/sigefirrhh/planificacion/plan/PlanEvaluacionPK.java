package sigefirrhh.planificacion.plan;

import java.io.Serializable;

public class PlanEvaluacionPK
  implements Serializable
{
  public long idPlanEvaluacion;

  public PlanEvaluacionPK()
  {
  }

  public PlanEvaluacionPK(long idPlanEvaluacion)
  {
    this.idPlanEvaluacion = idPlanEvaluacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PlanEvaluacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PlanEvaluacionPK thatPK)
  {
    return 
      this.idPlanEvaluacion == thatPK.idPlanEvaluacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPlanEvaluacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPlanEvaluacion);
  }
}