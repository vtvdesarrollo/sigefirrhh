package sigefirrhh.planificacion.plan;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.registro.CausaMovimiento;
import sigefirrhh.base.registro.CausaMovimientoBeanBusiness;

public class MovimientosPlanBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addMovimientosPlan(MovimientosPlan movimientosPlan)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    MovimientosPlan movimientosPlanNew = 
      (MovimientosPlan)BeanUtils.cloneBean(
      movimientosPlan);

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (movimientosPlanNew.getPlanPersonal() != null) {
      movimientosPlanNew.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        movimientosPlanNew.getPlanPersonal().getIdPlanPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (movimientosPlanNew.getCausaMovimiento() != null) {
      movimientosPlanNew.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        movimientosPlanNew.getCausaMovimiento().getIdCausaMovimiento()));
    }
    pm.makePersistent(movimientosPlanNew);
  }

  public void updateMovimientosPlan(MovimientosPlan movimientosPlan) throws Exception
  {
    MovimientosPlan movimientosPlanModify = 
      findMovimientosPlanById(movimientosPlan.getIdMovimientosPlan());

    PlanPersonalBeanBusiness planPersonalBeanBusiness = new PlanPersonalBeanBusiness();

    if (movimientosPlan.getPlanPersonal() != null) {
      movimientosPlan.setPlanPersonal(
        planPersonalBeanBusiness.findPlanPersonalById(
        movimientosPlan.getPlanPersonal().getIdPlanPersonal()));
    }

    CausaMovimientoBeanBusiness causaMovimientoBeanBusiness = new CausaMovimientoBeanBusiness();

    if (movimientosPlan.getCausaMovimiento() != null) {
      movimientosPlan.setCausaMovimiento(
        causaMovimientoBeanBusiness.findCausaMovimientoById(
        movimientosPlan.getCausaMovimiento().getIdCausaMovimiento()));
    }

    BeanUtils.copyProperties(movimientosPlanModify, movimientosPlan);
  }

  public void deleteMovimientosPlan(MovimientosPlan movimientosPlan) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    MovimientosPlan movimientosPlanDelete = 
      findMovimientosPlanById(movimientosPlan.getIdMovimientosPlan());
    pm.deletePersistent(movimientosPlanDelete);
  }

  public MovimientosPlan findMovimientosPlanById(long idMovimientosPlan) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idMovimientosPlan == pIdMovimientosPlan";
    Query query = pm.newQuery(MovimientosPlan.class, filter);

    query.declareParameters("long pIdMovimientosPlan");

    parameters.put("pIdMovimientosPlan", new Long(idMovimientosPlan));

    Collection colMovimientosPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colMovimientosPlan.iterator();
    return (MovimientosPlan)iterator.next();
  }

  public Collection findMovimientosPlanAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent movimientosPlanExtent = pm.getExtent(
      MovimientosPlan.class, true);
    Query query = pm.newQuery(movimientosPlanExtent);
    query.setOrdering("causaMovimiento.codCausaMovimiento ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPlanPersonal(long idPlanPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPersonal.idPlanPersonal == pIdPlanPersonal";

    Query query = pm.newQuery(MovimientosPlan.class, filter);

    query.declareParameters("long pIdPlanPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPersonal", new Long(idPlanPersonal));

    query.setOrdering("causaMovimiento.codCausaMovimiento ascending");

    Collection colMovimientosPlan = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colMovimientosPlan);

    return colMovimientosPlan;
  }
}