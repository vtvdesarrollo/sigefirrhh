package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class SeleccionBusiness extends AbstractBusiness
  implements Serializable
{
  private ConcursoBeanBusiness concursoBeanBusiness = new ConcursoBeanBusiness();

  private ConcursoCargoBeanBusiness concursoCargoBeanBusiness = new ConcursoCargoBeanBusiness();

  private PruebaSeleccionBeanBusiness pruebaSeleccionBeanBusiness = new PruebaSeleccionBeanBusiness();

  private BaremoSeleccionBeanBusiness baremoSeleccionBeanBusiness = new BaremoSeleccionBeanBusiness();

  private PostuladoConcursoBeanBusiness postuladoConcursoBeanBusiness = new PostuladoConcursoBeanBusiness();

  private PostuladoExternoBeanBusiness postuladoExternoBeanBusiness = new PostuladoExternoBeanBusiness();

  private PruebaPreseleccionadoBeanBusiness pruebaPreseleccionadoBeanBusiness = new PruebaPreseleccionadoBeanBusiness();

  private BaremoPreseleccionadoBeanBusiness baremoPreseleccionadoBeanBusiness = new BaremoPreseleccionadoBeanBusiness();

  private VaremosBeanBusiness varemosBeanBusiness = new VaremosBeanBusiness();

  public void addConcurso(Concurso concurso)
    throws Exception
  {
    this.concursoBeanBusiness.addConcurso(concurso);
  }

  public void updateConcurso(Concurso concurso) throws Exception {
    this.concursoBeanBusiness.updateConcurso(concurso);
  }

  public void deleteConcurso(Concurso concurso) throws Exception {
    this.concursoBeanBusiness.deleteConcurso(concurso);
  }

  public Concurso findConcursoById(long concursoId) throws Exception {
    return this.concursoBeanBusiness.findConcursoById(concursoId);
  }

  public Collection findAllConcurso() throws Exception {
    return this.concursoBeanBusiness.findConcursoAll();
  }

  public Collection findConcursoByCodConcurso(String codConcurso, long idOrganismo) throws Exception
  {
    return this.concursoBeanBusiness.findByCodConcurso(codConcurso, idOrganismo);
  }

  public Collection findConcursoByNombre(String nombre, long idOrganismo) throws Exception
  {
    return this.concursoBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findConcursoByEstatus(String estatus, long idOrganismo) throws Exception
  {
    return this.concursoBeanBusiness.findByEstatus(estatus, idOrganismo);
  }

  public Collection findConcursoTodos(long idOrganismo) throws Exception
  {
    return this.concursoBeanBusiness.findTodos(idOrganismo);
  }

  public Collection findConcursoByOrganismo(long idOrganismo) throws Exception
  {
    return this.concursoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addConcursoCargo(ConcursoCargo concursoCargo) throws Exception {
    this.concursoCargoBeanBusiness.addConcursoCargo(concursoCargo);
  }

  public void updateConcursoCargo(ConcursoCargo concursoCargo) throws Exception
  {
    this.concursoCargoBeanBusiness.updateConcursoCargo(concursoCargo);
  }

  public void deleteConcursoCargo(ConcursoCargo concursoCargo) throws Exception
  {
    this.concursoCargoBeanBusiness.deleteConcursoCargo(concursoCargo);
  }

  public ConcursoCargo findConcursoCargoById(long concursoCargoId) throws Exception
  {
    return this.concursoCargoBeanBusiness.findConcursoCargoById(concursoCargoId);
  }

  public Collection findAllConcursoCargo() throws Exception {
    return this.concursoCargoBeanBusiness.findConcursoCargoAll();
  }

  public Collection findConcursoCargoByConcurso(long idConcurso) throws Exception
  {
    return this.concursoCargoBeanBusiness.findByConcurso(idConcurso);
  }

  public void addPruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception
  {
    this.pruebaSeleccionBeanBusiness.addPruebaSeleccion(pruebaSeleccion);
  }

  public void addBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception
  {
    this.baremoSeleccionBeanBusiness.addBaremoSeleccion(baremoSeleccion);
  }

  public void addVaremos(Varemos varemos) throws Exception {
    this.varemosBeanBusiness.addVaremos(varemos);
  }

  public void updatePruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception
  {
    this.pruebaSeleccionBeanBusiness.updatePruebaSeleccion(pruebaSeleccion);
  }

  public void updateBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception
  {
    this.baremoSeleccionBeanBusiness.updateBaremoSeleccion(baremoSeleccion);
  }

  public void updateVaremos(Varemos varemos) throws Exception {
    this.varemosBeanBusiness.updateVaremos(varemos);
  }

  public void deletePruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception
  {
    this.pruebaSeleccionBeanBusiness.deletePruebaSeleccion(pruebaSeleccion);
  }

  public void deleteBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception
  {
    this.baremoSeleccionBeanBusiness.deleteBaremoSeleccion(baremoSeleccion);
  }

  public void deleteVaremos(Varemos varemos) throws Exception {
    this.varemosBeanBusiness.deleteVaremos(varemos);
  }

  public PruebaSeleccion findPruebaSeleccionById(long pruebaSeleccionId) throws Exception
  {
    return this.pruebaSeleccionBeanBusiness
      .findPruebaSeleccionById(pruebaSeleccionId);
  }

  public BaremoSeleccion findBaremoSeleccionById(long baremoSeleccionId) throws Exception
  {
    return this.baremoSeleccionBeanBusiness
      .findBaremoSeleccionById(baremoSeleccionId);
  }

  public Varemos findVaremosById(long varemosId) throws Exception {
    return this.varemosBeanBusiness.findVaremosById(varemosId);
  }

  public Collection findAllPruebaSeleccion() throws Exception {
    return this.pruebaSeleccionBeanBusiness.findPruebaSeleccionAll();
  }

  public Collection findAllBaremoSeleccion() throws Exception {
    return this.baremoSeleccionBeanBusiness.findBaremoSeleccionAll();
  }

  public Collection findAllVaremos() throws Exception {
    return this.varemosBeanBusiness.findVaremosAll();
  }

  public Collection findPruebaSeleccionByCodPrueba(String codPrueba) throws Exception
  {
    return this.pruebaSeleccionBeanBusiness.findByCodPrueba(codPrueba);
  }

  public Collection findBaremoSeleccionByCodBaremo(String codBaremo) throws Exception
  {
    return this.baremoSeleccionBeanBusiness.findByCodBaremo(codBaremo);
  }

  public Collection findVaremosByCodVaremos(String codVaremos) throws Exception
  {
    return this.varemosBeanBusiness.findByCodVaremos(codVaremos);
  }

  public Collection findPruebaSeleccionByNombre(String nombre) throws Exception
  {
    return this.pruebaSeleccionBeanBusiness.findByNombre(nombre);
  }

  public Collection findBaremoSeleccionByNombre(String nombre) throws Exception
  {
    return this.baremoSeleccionBeanBusiness.findByNombre(nombre);
  }

  public Collection findVaremosByNombre(String nombre) throws Exception
  {
    return this.varemosBeanBusiness.findByNombre(nombre);
  }

  public void addPostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception
  {
    this.postuladoConcursoBeanBusiness.addPostuladoConcurso(postuladoConcurso);
  }

  public void updatePostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception
  {
    this.postuladoConcursoBeanBusiness
      .updatePostuladoConcurso(postuladoConcurso);
  }

  public void deletePostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception
  {
    this.postuladoConcursoBeanBusiness
      .deletePostuladoConcurso(postuladoConcurso);
  }

  public PostuladoConcurso findPostuladoConcursoById(long postuladoConcursoId) throws Exception
  {
    return this.postuladoConcursoBeanBusiness
      .findPostuladoConcursoById(postuladoConcursoId);
  }

  public Collection findAllPostuladoConcurso() throws Exception {
    return this.postuladoConcursoBeanBusiness.findPostuladoConcursoAll();
  }

  public Collection findPostuladoConcursoByConcursoCargo(long idConcursoCargo) throws Exception
  {
    return this.postuladoConcursoBeanBusiness
      .findByConcursoCargo(idConcursoCargo);
  }

  public void addPostuladoExterno(PostuladoExterno postuladoExterno) throws Exception
  {
    this.postuladoExternoBeanBusiness.addPostuladoExterno(postuladoExterno);
  }

  public void updatePostuladoExterno(PostuladoExterno postuladoExterno) throws Exception
  {
    this.postuladoExternoBeanBusiness.updatePostuladoExterno(postuladoExterno);
  }

  public void deletePostuladoExterno(PostuladoExterno postuladoExterno) throws Exception
  {
    this.postuladoExternoBeanBusiness.deletePostuladoExterno(postuladoExterno);
  }

  public PostuladoExterno findPostuladoExternoById(long postuladoExternoId) throws Exception
  {
    return this.postuladoExternoBeanBusiness
      .findPostuladoExternoById(postuladoExternoId);
  }

  public Collection findAllPostuladoExterno() throws Exception {
    return this.postuladoExternoBeanBusiness.findPostuladoExternoAll();
  }

  public Collection findPostuladoExternoByCedula(int cedula) throws Exception {
    return this.postuladoExternoBeanBusiness.findByCedula(cedula);
  }

  public Collection findPostuladoExternoByPrimerApellido(String primerApellido) throws Exception
  {
    return this.postuladoExternoBeanBusiness
      .findByPrimerApellido(primerApellido);
  }

  public void addPruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception
  {
    this.pruebaPreseleccionadoBeanBusiness
      .addPruebaPreseleccionado(pruebaPreseleccionado);
  }

  public void updatePruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception
  {
    this.pruebaPreseleccionadoBeanBusiness
      .updatePruebaPreseleccionado(pruebaPreseleccionado);
  }

  public void deletePruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception
  {
    this.pruebaPreseleccionadoBeanBusiness
      .deletePruebaPreseleccionado(pruebaPreseleccionado);
  }

  public PruebaPreseleccionado findPruebaPreseleccionadoById(long pruebaPreseleccionadoId) throws Exception
  {
    return this.pruebaPreseleccionadoBeanBusiness
      .findPruebaPreseleccionadoById(pruebaPreseleccionadoId);
  }

  public Collection findAllPruebaPreseleccionado() throws Exception {
    return this.pruebaPreseleccionadoBeanBusiness.findPruebaPreseleccionadoAll();
  }

  public Collection findPruebaPreseleccionadoByPostuladoConcurso(long idPostuladoConcurso) throws Exception
  {
    return this.pruebaPreseleccionadoBeanBusiness
      .findByPostuladoConcurso(idPostuladoConcurso);
  }

  public void addBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado)
    throws Exception
  {
    this.baremoPreseleccionadoBeanBusiness
      .addBaremoPreseleccionado(baremoPreseleccionado);
  }

  public void updateBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado) throws Exception
  {
    this.baremoPreseleccionadoBeanBusiness
      .updateBaremoPreseleccionado(baremoPreseleccionado);
  }

  public void deleteBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado) throws Exception
  {
    this.baremoPreseleccionadoBeanBusiness
      .deleteBaremoPreseleccionado(baremoPreseleccionado);
  }

  public BaremoPreseleccionado findBaremoPreseleccionadoById(long baremoPreseleccionadoId) throws Exception
  {
    return this.baremoPreseleccionadoBeanBusiness
      .findBaremoPreseleccionadoById(baremoPreseleccionadoId);
  }

  public Collection findAllBaremoPreseleccionado() throws Exception {
    return this.baremoPreseleccionadoBeanBusiness.findBaremoPreseleccionadoAll();
  }

  public Collection findBaremoPreseleccionadoByPostuladoConcurso(long idPostuladoConcurso) throws Exception
  {
    return this.baremoPreseleccionadoBeanBusiness
      .findByPostuladoConcurso(idPostuladoConcurso);
  }
}