package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class PostuladoExternoPK
  implements Serializable
{
  public long idPostuladoExterno;

  public PostuladoExternoPK()
  {
  }

  public PostuladoExternoPK(long idPostuladoExterno)
  {
    this.idPostuladoExterno = idPostuladoExterno;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PostuladoExternoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PostuladoExternoPK thatPK)
  {
    return 
      this.idPostuladoExterno == thatPK.idPostuladoExterno;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPostuladoExterno)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPostuladoExterno);
  }
}