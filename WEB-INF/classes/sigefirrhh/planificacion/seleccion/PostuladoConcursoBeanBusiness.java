package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;
import sigefirrhh.planificacion.elegible.Elegible;
import sigefirrhh.planificacion.elegible.ElegibleBeanBusiness;

public class PostuladoConcursoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPostuladoConcurso(PostuladoConcurso postuladoConcurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PostuladoConcurso postuladoConcursoNew = 
      (PostuladoConcurso)BeanUtils.cloneBean(
      postuladoConcurso);

    ConcursoCargoBeanBusiness concursoCargoBeanBusiness = new ConcursoCargoBeanBusiness();

    if (postuladoConcursoNew.getConcursoCargo() != null) {
      postuladoConcursoNew.setConcursoCargo(
        concursoCargoBeanBusiness.findConcursoCargoById(
        postuladoConcursoNew.getConcursoCargo().getIdConcursoCargo()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (postuladoConcursoNew.getPersonal() != null) {
      postuladoConcursoNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        postuladoConcursoNew.getPersonal().getIdPersonal()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (postuladoConcursoNew.getElegible() != null) {
      postuladoConcursoNew.setElegible(
        elegibleBeanBusiness.findElegibleById(
        postuladoConcursoNew.getElegible().getIdElegible()));
    }

    PostuladoExternoBeanBusiness postuladoExternoBeanBusiness = new PostuladoExternoBeanBusiness();

    if (postuladoConcursoNew.getPostuladoExterno() != null) {
      postuladoConcursoNew.setPostuladoExterno(
        postuladoExternoBeanBusiness.findPostuladoExternoById(
        postuladoConcursoNew.getPostuladoExterno().getIdPostuladoExterno()));
    }
    pm.makePersistent(postuladoConcursoNew);
  }

  public void updatePostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception
  {
    PostuladoConcurso postuladoConcursoModify = 
      findPostuladoConcursoById(postuladoConcurso.getIdPostuladoConcurso());

    ConcursoCargoBeanBusiness concursoCargoBeanBusiness = new ConcursoCargoBeanBusiness();

    if (postuladoConcurso.getConcursoCargo() != null) {
      postuladoConcurso.setConcursoCargo(
        concursoCargoBeanBusiness.findConcursoCargoById(
        postuladoConcurso.getConcursoCargo().getIdConcursoCargo()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (postuladoConcurso.getPersonal() != null) {
      postuladoConcurso.setPersonal(
        personalBeanBusiness.findPersonalById(
        postuladoConcurso.getPersonal().getIdPersonal()));
    }

    ElegibleBeanBusiness elegibleBeanBusiness = new ElegibleBeanBusiness();

    if (postuladoConcurso.getElegible() != null) {
      postuladoConcurso.setElegible(
        elegibleBeanBusiness.findElegibleById(
        postuladoConcurso.getElegible().getIdElegible()));
    }

    PostuladoExternoBeanBusiness postuladoExternoBeanBusiness = new PostuladoExternoBeanBusiness();

    if (postuladoConcurso.getPostuladoExterno() != null) {
      postuladoConcurso.setPostuladoExterno(
        postuladoExternoBeanBusiness.findPostuladoExternoById(
        postuladoConcurso.getPostuladoExterno().getIdPostuladoExterno()));
    }

    BeanUtils.copyProperties(postuladoConcursoModify, postuladoConcurso);
  }

  public void deletePostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PostuladoConcurso postuladoConcursoDelete = 
      findPostuladoConcursoById(postuladoConcurso.getIdPostuladoConcurso());
    pm.deletePersistent(postuladoConcursoDelete);
  }

  public PostuladoConcurso findPostuladoConcursoById(long idPostuladoConcurso) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPostuladoConcurso == pIdPostuladoConcurso";
    Query query = pm.newQuery(PostuladoConcurso.class, filter);

    query.declareParameters("long pIdPostuladoConcurso");

    parameters.put("pIdPostuladoConcurso", new Long(idPostuladoConcurso));

    Collection colPostuladoConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPostuladoConcurso.iterator();
    return (PostuladoConcurso)iterator.next();
  }

  public Collection findPostuladoConcursoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent postuladoConcursoExtent = pm.getExtent(
      PostuladoConcurso.class, true);
    Query query = pm.newQuery(postuladoConcursoExtent);
    query.setOrdering("resultado ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConcursoCargo(long idConcursoCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "concursoCargo.idConcursoCargo == pIdConcursoCargo";

    Query query = pm.newQuery(PostuladoConcurso.class, filter);

    query.declareParameters("long pIdConcursoCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdConcursoCargo", new Long(idConcursoCargo));

    query.setOrdering("resultado ascending");

    Collection colPostuladoConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPostuladoConcurso);

    return colPostuladoConcurso;
  }
}