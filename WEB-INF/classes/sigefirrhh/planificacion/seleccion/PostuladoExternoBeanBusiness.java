package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PostuladoExternoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPostuladoExterno(PostuladoExterno postuladoExterno)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PostuladoExterno postuladoExternoNew = 
      (PostuladoExterno)BeanUtils.cloneBean(
      postuladoExterno);

    pm.makePersistent(postuladoExternoNew);
  }

  public void updatePostuladoExterno(PostuladoExterno postuladoExterno) throws Exception
  {
    PostuladoExterno postuladoExternoModify = 
      findPostuladoExternoById(postuladoExterno.getIdPostuladoExterno());

    BeanUtils.copyProperties(postuladoExternoModify, postuladoExterno);
  }

  public void deletePostuladoExterno(PostuladoExterno postuladoExterno) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PostuladoExterno postuladoExternoDelete = 
      findPostuladoExternoById(postuladoExterno.getIdPostuladoExterno());
    pm.deletePersistent(postuladoExternoDelete);
  }

  public PostuladoExterno findPostuladoExternoById(long idPostuladoExterno) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPostuladoExterno == pIdPostuladoExterno";
    Query query = pm.newQuery(PostuladoExterno.class, filter);

    query.declareParameters("long pIdPostuladoExterno");

    parameters.put("pIdPostuladoExterno", new Long(idPostuladoExterno));

    Collection colPostuladoExterno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPostuladoExterno.iterator();
    return (PostuladoExterno)iterator.next();
  }

  public Collection findPostuladoExternoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent postuladoExternoExtent = pm.getExtent(
      PostuladoExterno.class, true);
    Query query = pm.newQuery(postuladoExternoExtent);
    query.setOrdering("primerApellido ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCedula(int cedula)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cedula == pCedula";

    Query query = pm.newQuery(PostuladoExterno.class, filter);

    query.declareParameters("int pCedula");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));

    query.setOrdering("primerApellido ascending");

    Collection colPostuladoExterno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPostuladoExterno);

    return colPostuladoExterno;
  }

  public Collection findByPrimerApellido(String primerApellido)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primerApellido.startsWith(pPrimerApellido)";

    Query query = pm.newQuery(PostuladoExterno.class, filter);

    query.declareParameters("java.lang.String pPrimerApellido");
    HashMap parameters = new HashMap();

    parameters.put("pPrimerApellido", new String(primerApellido));

    query.setOrdering("primerApellido ascending");

    Collection colPostuladoExterno = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPostuladoExterno);

    return colPostuladoExterno;
  }
}