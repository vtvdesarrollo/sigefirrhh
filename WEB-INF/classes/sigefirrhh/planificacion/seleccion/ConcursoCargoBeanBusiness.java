package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosBeanBusiness;

public class ConcursoCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConcursoCargo(ConcursoCargo concursoCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ConcursoCargo concursoCargoNew = 
      (ConcursoCargo)BeanUtils.cloneBean(
      concursoCargo);

    ConcursoBeanBusiness concursoBeanBusiness = new ConcursoBeanBusiness();

    if (concursoCargoNew.getConcurso() != null) {
      concursoCargoNew.setConcurso(
        concursoBeanBusiness.findConcursoById(
        concursoCargoNew.getConcurso().getIdConcurso()));
    }

    RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();

    if (concursoCargoNew.getRegistroCargos() != null) {
      concursoCargoNew.setRegistroCargos(
        registroCargosBeanBusiness.findRegistroCargosById(
        concursoCargoNew.getRegistroCargos().getIdRegistroCargos()));
    }

    PostuladoConcursoBeanBusiness postuladoConcursoBeanBusiness = new PostuladoConcursoBeanBusiness();

    if (concursoCargoNew.getPostuladoConcurso() != null) {
      concursoCargoNew.setPostuladoConcurso(
        postuladoConcursoBeanBusiness.findPostuladoConcursoById(
        concursoCargoNew.getPostuladoConcurso().getIdPostuladoConcurso()));
    }
    pm.makePersistent(concursoCargoNew);
  }

  public void updateConcursoCargo(ConcursoCargo concursoCargo) throws Exception
  {
    ConcursoCargo concursoCargoModify = 
      findConcursoCargoById(concursoCargo.getIdConcursoCargo());

    ConcursoBeanBusiness concursoBeanBusiness = new ConcursoBeanBusiness();

    if (concursoCargo.getConcurso() != null) {
      concursoCargo.setConcurso(
        concursoBeanBusiness.findConcursoById(
        concursoCargo.getConcurso().getIdConcurso()));
    }

    RegistroCargosBeanBusiness registroCargosBeanBusiness = new RegistroCargosBeanBusiness();

    if (concursoCargo.getRegistroCargos() != null) {
      concursoCargo.setRegistroCargos(
        registroCargosBeanBusiness.findRegistroCargosById(
        concursoCargo.getRegistroCargos().getIdRegistroCargos()));
    }

    PostuladoConcursoBeanBusiness postuladoConcursoBeanBusiness = new PostuladoConcursoBeanBusiness();

    if (concursoCargo.getPostuladoConcurso() != null) {
      concursoCargo.setPostuladoConcurso(
        postuladoConcursoBeanBusiness.findPostuladoConcursoById(
        concursoCargo.getPostuladoConcurso().getIdPostuladoConcurso()));
    }

    BeanUtils.copyProperties(concursoCargoModify, concursoCargo);
  }

  public void deleteConcursoCargo(ConcursoCargo concursoCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ConcursoCargo concursoCargoDelete = 
      findConcursoCargoById(concursoCargo.getIdConcursoCargo());
    pm.deletePersistent(concursoCargoDelete);
  }

  public ConcursoCargo findConcursoCargoById(long idConcursoCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConcursoCargo == pIdConcursoCargo";
    Query query = pm.newQuery(ConcursoCargo.class, filter);

    query.declareParameters("long pIdConcursoCargo");

    parameters.put("pIdConcursoCargo", new Long(idConcursoCargo));

    Collection colConcursoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConcursoCargo.iterator();
    return (ConcursoCargo)iterator.next();
  }

  public Collection findConcursoCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent concursoCargoExtent = pm.getExtent(
      ConcursoCargo.class, true);
    Query query = pm.newQuery(concursoCargoExtent);
    query.setOrdering("registroCargos.cargo.descripcionCargo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByConcurso(long idConcurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "concurso.idConcurso == pIdConcurso";

    Query query = pm.newQuery(ConcursoCargo.class, filter);

    query.declareParameters("long pIdConcurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdConcurso", new Long(idConcurso));

    query.setOrdering("registroCargos.cargo.descripcionCargo ascending");

    Collection colConcursoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcursoCargo);

    return colConcursoCargo;
  }
}