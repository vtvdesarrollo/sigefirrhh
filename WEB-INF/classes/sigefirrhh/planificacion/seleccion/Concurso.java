package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class Concurso
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_SINO;
  private long idConcurso;
  private String codConcurso;
  private String nombre;
  private String descripcion;
  private String estatus;
  private String aprobadoMpd;
  private Date fechaApertura;
  private Date inicioConvocatoria;
  private Date finConvocatoria;
  private Date inicioRetiro;
  private Date finRetiro;
  private Date inicioInscripcion;
  private Date finInscripcion;
  private Date iniPublicacionInsc;
  private Date finPublicacionInsc;
  private Date iniExamenes;
  private Date finExamenes;
  private Date iniPublicacionExam;
  private Date finPublicacionExam;
  private Date iniEntrevistas;
  private Date finEntrevistas;
  private Date iniPublicacionEntr;
  private Date finPublicacionEntr;
  private Date entregaInforme;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "aprobadoMpd", "codConcurso", "descripcion", "entregaInforme", "estatus", "fechaApertura", "finConvocatoria", "finEntrevistas", "finExamenes", "finInscripcion", "finPublicacionEntr", "finPublicacionExam", "finPublicacionInsc", "finRetiro", "idConcurso", "iniEntrevistas", "iniExamenes", "iniPublicacionEntr", "iniPublicacionExam", "iniPublicacionInsc", "inicioConvocatoria", "inicioInscripcion", "inicioRetiro", "nombre", "organismo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.Concurso"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Concurso());

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("0", "PLANIFICADO");
    LISTA_ESTATUS.put("1", "EN PROCESO");
    LISTA_ESTATUS.put("2", "CERRADO");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public Concurso()
  {
    jdoSetestatus(this, "1");

    jdoSetaprobadoMpd(this, "S");
  }

  public String toString()
  {
    return jdoGetnombre(this) + " - " + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaApertura(this));
  }

  public String getCodConcurso()
  {
    return jdoGetcodConcurso(this);
  }

  public void setCodConcurso(String codConcurso)
  {
    jdoSetcodConcurso(this, codConcurso);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public void setDescripcion(String descripcion)
  {
    jdoSetdescripcion(this, descripcion);
  }

  public Date getEntregaInforme()
  {
    return jdoGetentregaInforme(this);
  }

  public void setEntregaInforme(Date entregaInforme)
  {
    jdoSetentregaInforme(this, entregaInforme);
  }

  public Date getFechaApertura()
  {
    return jdoGetfechaApertura(this);
  }

  public void setFechaApertura(Date fechaApertura)
  {
    jdoSetfechaApertura(this, fechaApertura);
  }

  public Date getFinConvocatoria()
  {
    return jdoGetfinConvocatoria(this);
  }

  public void setFinConvocatoria(Date finConvocatoria)
  {
    jdoSetfinConvocatoria(this, finConvocatoria);
  }

  public Date getFinEntrevistas()
  {
    return jdoGetfinEntrevistas(this);
  }

  public void setFinEntrevistas(Date finEntrevistas)
  {
    jdoSetfinEntrevistas(this, finEntrevistas);
  }

  public Date getFinExamenes()
  {
    return jdoGetfinExamenes(this);
  }

  public void setFinExamenes(Date finExamenes)
  {
    jdoSetfinExamenes(this, finExamenes);
  }

  public Date getFinInscripcion()
  {
    return jdoGetfinInscripcion(this);
  }

  public void setFinInscripcion(Date finInscripcion)
  {
    jdoSetfinInscripcion(this, finInscripcion);
  }

  public Date getFinPublicacionEntr()
  {
    return jdoGetfinPublicacionEntr(this);
  }

  public void setFinPublicacionEntr(Date finPublicacionEntr)
  {
    jdoSetfinPublicacionEntr(this, finPublicacionEntr);
  }

  public Date getFinPublicacionExam()
  {
    return jdoGetfinPublicacionExam(this);
  }

  public void setFinPublicacionExam(Date finPublicacionExam)
  {
    jdoSetfinPublicacionExam(this, finPublicacionExam);
  }

  public Date getFinPublicacionInsc()
  {
    return jdoGetfinPublicacionInsc(this);
  }

  public void setFinPublicacionInsc(Date finPublicacionInsc)
  {
    jdoSetfinPublicacionInsc(this, finPublicacionInsc);
  }

  public Date getFinRetiro()
  {
    return jdoGetfinRetiro(this);
  }

  public void setFinRetiro(Date finRetiro)
  {
    jdoSetfinRetiro(this, finRetiro);
  }

  public long getIdConcurso()
  {
    return jdoGetidConcurso(this);
  }

  public void setIdConcurso(long idConcurso)
  {
    jdoSetidConcurso(this, idConcurso);
  }

  public Date getInicioConvocatoria()
  {
    return jdoGetinicioConvocatoria(this);
  }

  public void setInicioConvocatoria(Date inicioConvocatoria)
  {
    jdoSetinicioConvocatoria(this, inicioConvocatoria);
  }

  public Date getInicioInscripcion()
  {
    return jdoGetinicioInscripcion(this);
  }

  public void setInicioInscripcion(Date inicioInscripcion)
  {
    jdoSetinicioInscripcion(this, inicioInscripcion);
  }

  public Date getInicioRetiro()
  {
    return jdoGetinicioRetiro(this);
  }

  public void setInicioRetiro(Date inicioRetiro)
  {
    jdoSetinicioRetiro(this, inicioRetiro);
  }

  public Date getIniEntrevistas()
  {
    return jdoGetiniEntrevistas(this);
  }

  public void setIniEntrevistas(Date iniEntrevistas)
  {
    jdoSetiniEntrevistas(this, iniEntrevistas);
  }

  public Date getIniExamenes()
  {
    return jdoGetiniExamenes(this);
  }

  public void setIniExamenes(Date iniExamenes)
  {
    jdoSetiniExamenes(this, iniExamenes);
  }

  public Date getIniPublicacionEntr()
  {
    return jdoGetiniPublicacionEntr(this);
  }

  public void setIniPublicacionEntr(Date iniPublicacionEntr)
  {
    jdoSetiniPublicacionEntr(this, iniPublicacionEntr);
  }

  public Date getIniPublicacionExam()
  {
    return jdoGetiniPublicacionExam(this);
  }

  public void setIniPublicacionExam(Date iniPublicacionExam)
  {
    jdoSetiniPublicacionExam(this, iniPublicacionExam);
  }

  public Date getIniPublicacionInsc()
  {
    return jdoGetiniPublicacionInsc(this);
  }

  public void setIniPublicacionInsc(Date iniPublicacionInsc)
  {
    jdoSetiniPublicacionInsc(this, iniPublicacionInsc);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus)
  {
    jdoSetestatus(this, estatus);
  }
  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }
  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }
  public String getAprobadoMpd() {
    return jdoGetaprobadoMpd(this);
  }
  public void setAprobadoMpd(String aprobadoMpd) {
    jdoSetaprobadoMpd(this, aprobadoMpd);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 25;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Concurso localConcurso = new Concurso();
    localConcurso.jdoFlags = 1;
    localConcurso.jdoStateManager = paramStateManager;
    return localConcurso;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Concurso localConcurso = new Concurso();
    localConcurso.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConcurso.jdoFlags = 1;
    localConcurso.jdoStateManager = paramStateManager;
    return localConcurso;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.aprobadoMpd);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codConcurso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.entregaInforme);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaApertura);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finConvocatoria);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finEntrevistas);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finExamenes);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finInscripcion);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finPublicacionEntr);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finPublicacionExam);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finPublicacionInsc);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.finRetiro);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConcurso);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.iniEntrevistas);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.iniExamenes);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.iniPublicacionEntr);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.iniPublicacionExam);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.iniPublicacionInsc);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.inicioConvocatoria);
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.inicioInscripcion);
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.inicioRetiro);
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.aprobadoMpd = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codConcurso = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entregaInforme = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaApertura = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finConvocatoria = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finEntrevistas = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finExamenes = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finInscripcion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finPublicacionEntr = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finPublicacionExam = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finPublicacionInsc = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.finRetiro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConcurso = localStateManager.replacingLongField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.iniEntrevistas = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.iniExamenes = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.iniPublicacionEntr = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.iniPublicacionExam = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.iniPublicacionInsc = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inicioConvocatoria = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 21:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inicioInscripcion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 22:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.inicioRetiro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 23:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 24:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Concurso paramConcurso, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.aprobadoMpd = paramConcurso.aprobadoMpd;
      return;
    case 1:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.codConcurso = paramConcurso.codConcurso;
      return;
    case 2:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramConcurso.descripcion;
      return;
    case 3:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.entregaInforme = paramConcurso.entregaInforme;
      return;
    case 4:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramConcurso.estatus;
      return;
    case 5:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.fechaApertura = paramConcurso.fechaApertura;
      return;
    case 6:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finConvocatoria = paramConcurso.finConvocatoria;
      return;
    case 7:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finEntrevistas = paramConcurso.finEntrevistas;
      return;
    case 8:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finExamenes = paramConcurso.finExamenes;
      return;
    case 9:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finInscripcion = paramConcurso.finInscripcion;
      return;
    case 10:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finPublicacionEntr = paramConcurso.finPublicacionEntr;
      return;
    case 11:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finPublicacionExam = paramConcurso.finPublicacionExam;
      return;
    case 12:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finPublicacionInsc = paramConcurso.finPublicacionInsc;
      return;
    case 13:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.finRetiro = paramConcurso.finRetiro;
      return;
    case 14:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.idConcurso = paramConcurso.idConcurso;
      return;
    case 15:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.iniEntrevistas = paramConcurso.iniEntrevistas;
      return;
    case 16:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.iniExamenes = paramConcurso.iniExamenes;
      return;
    case 17:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.iniPublicacionEntr = paramConcurso.iniPublicacionEntr;
      return;
    case 18:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.iniPublicacionExam = paramConcurso.iniPublicacionExam;
      return;
    case 19:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.iniPublicacionInsc = paramConcurso.iniPublicacionInsc;
      return;
    case 20:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.inicioConvocatoria = paramConcurso.inicioConvocatoria;
      return;
    case 21:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.inicioInscripcion = paramConcurso.inicioInscripcion;
      return;
    case 22:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.inicioRetiro = paramConcurso.inicioRetiro;
      return;
    case 23:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramConcurso.nombre;
      return;
    case 24:
      if (paramConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramConcurso.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Concurso))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Concurso localConcurso = (Concurso)paramObject;
    if (localConcurso.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConcurso, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConcursoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConcursoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConcursoPK))
      throw new IllegalArgumentException("arg1");
    ConcursoPK localConcursoPK = (ConcursoPK)paramObject;
    localConcursoPK.idConcurso = this.idConcurso;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConcursoPK))
      throw new IllegalArgumentException("arg1");
    ConcursoPK localConcursoPK = (ConcursoPK)paramObject;
    this.idConcurso = localConcursoPK.idConcurso;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConcursoPK))
      throw new IllegalArgumentException("arg2");
    ConcursoPK localConcursoPK = (ConcursoPK)paramObject;
    localConcursoPK.idConcurso = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 14);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConcursoPK))
      throw new IllegalArgumentException("arg2");
    ConcursoPK localConcursoPK = (ConcursoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 14, localConcursoPK.idConcurso);
  }

  private static final String jdoGetaprobadoMpd(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.aprobadoMpd;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.aprobadoMpd;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 0))
      return paramConcurso.aprobadoMpd;
    return localStateManager.getStringField(paramConcurso, jdoInheritedFieldCount + 0, paramConcurso.aprobadoMpd);
  }

  private static final void jdoSetaprobadoMpd(Concurso paramConcurso, String paramString)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.aprobadoMpd = paramString;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.aprobadoMpd = paramString;
      return;
    }
    localStateManager.setStringField(paramConcurso, jdoInheritedFieldCount + 0, paramConcurso.aprobadoMpd, paramString);
  }

  private static final String jdoGetcodConcurso(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.codConcurso;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.codConcurso;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 1))
      return paramConcurso.codConcurso;
    return localStateManager.getStringField(paramConcurso, jdoInheritedFieldCount + 1, paramConcurso.codConcurso);
  }

  private static final void jdoSetcodConcurso(Concurso paramConcurso, String paramString)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.codConcurso = paramString;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.codConcurso = paramString;
      return;
    }
    localStateManager.setStringField(paramConcurso, jdoInheritedFieldCount + 1, paramConcurso.codConcurso, paramString);
  }

  private static final String jdoGetdescripcion(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.descripcion;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.descripcion;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 2))
      return paramConcurso.descripcion;
    return localStateManager.getStringField(paramConcurso, jdoInheritedFieldCount + 2, paramConcurso.descripcion);
  }

  private static final void jdoSetdescripcion(Concurso paramConcurso, String paramString)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramConcurso, jdoInheritedFieldCount + 2, paramConcurso.descripcion, paramString);
  }

  private static final Date jdoGetentregaInforme(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.entregaInforme;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.entregaInforme;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 3))
      return paramConcurso.entregaInforme;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 3, paramConcurso.entregaInforme);
  }

  private static final void jdoSetentregaInforme(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.entregaInforme = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.entregaInforme = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 3, paramConcurso.entregaInforme, paramDate);
  }

  private static final String jdoGetestatus(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.estatus;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.estatus;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 4))
      return paramConcurso.estatus;
    return localStateManager.getStringField(paramConcurso, jdoInheritedFieldCount + 4, paramConcurso.estatus);
  }

  private static final void jdoSetestatus(Concurso paramConcurso, String paramString)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramConcurso, jdoInheritedFieldCount + 4, paramConcurso.estatus, paramString);
  }

  private static final Date jdoGetfechaApertura(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.fechaApertura;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.fechaApertura;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 5))
      return paramConcurso.fechaApertura;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 5, paramConcurso.fechaApertura);
  }

  private static final void jdoSetfechaApertura(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.fechaApertura = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.fechaApertura = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 5, paramConcurso.fechaApertura, paramDate);
  }

  private static final Date jdoGetfinConvocatoria(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finConvocatoria;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finConvocatoria;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 6))
      return paramConcurso.finConvocatoria;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 6, paramConcurso.finConvocatoria);
  }

  private static final void jdoSetfinConvocatoria(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finConvocatoria = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finConvocatoria = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 6, paramConcurso.finConvocatoria, paramDate);
  }

  private static final Date jdoGetfinEntrevistas(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finEntrevistas;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finEntrevistas;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 7))
      return paramConcurso.finEntrevistas;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 7, paramConcurso.finEntrevistas);
  }

  private static final void jdoSetfinEntrevistas(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finEntrevistas = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finEntrevistas = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 7, paramConcurso.finEntrevistas, paramDate);
  }

  private static final Date jdoGetfinExamenes(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finExamenes;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finExamenes;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 8))
      return paramConcurso.finExamenes;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 8, paramConcurso.finExamenes);
  }

  private static final void jdoSetfinExamenes(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finExamenes = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finExamenes = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 8, paramConcurso.finExamenes, paramDate);
  }

  private static final Date jdoGetfinInscripcion(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finInscripcion;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finInscripcion;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 9))
      return paramConcurso.finInscripcion;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 9, paramConcurso.finInscripcion);
  }

  private static final void jdoSetfinInscripcion(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finInscripcion = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finInscripcion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 9, paramConcurso.finInscripcion, paramDate);
  }

  private static final Date jdoGetfinPublicacionEntr(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finPublicacionEntr;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finPublicacionEntr;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 10))
      return paramConcurso.finPublicacionEntr;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 10, paramConcurso.finPublicacionEntr);
  }

  private static final void jdoSetfinPublicacionEntr(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finPublicacionEntr = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finPublicacionEntr = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 10, paramConcurso.finPublicacionEntr, paramDate);
  }

  private static final Date jdoGetfinPublicacionExam(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finPublicacionExam;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finPublicacionExam;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 11))
      return paramConcurso.finPublicacionExam;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 11, paramConcurso.finPublicacionExam);
  }

  private static final void jdoSetfinPublicacionExam(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finPublicacionExam = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finPublicacionExam = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 11, paramConcurso.finPublicacionExam, paramDate);
  }

  private static final Date jdoGetfinPublicacionInsc(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finPublicacionInsc;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finPublicacionInsc;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 12))
      return paramConcurso.finPublicacionInsc;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 12, paramConcurso.finPublicacionInsc);
  }

  private static final void jdoSetfinPublicacionInsc(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finPublicacionInsc = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finPublicacionInsc = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 12, paramConcurso.finPublicacionInsc, paramDate);
  }

  private static final Date jdoGetfinRetiro(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.finRetiro;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.finRetiro;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 13))
      return paramConcurso.finRetiro;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 13, paramConcurso.finRetiro);
  }

  private static final void jdoSetfinRetiro(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.finRetiro = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.finRetiro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 13, paramConcurso.finRetiro, paramDate);
  }

  private static final long jdoGetidConcurso(Concurso paramConcurso)
  {
    return paramConcurso.idConcurso;
  }

  private static final void jdoSetidConcurso(Concurso paramConcurso, long paramLong)
  {
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.idConcurso = paramLong;
      return;
    }
    localStateManager.setLongField(paramConcurso, jdoInheritedFieldCount + 14, paramConcurso.idConcurso, paramLong);
  }

  private static final Date jdoGetiniEntrevistas(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.iniEntrevistas;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.iniEntrevistas;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 15))
      return paramConcurso.iniEntrevistas;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 15, paramConcurso.iniEntrevistas);
  }

  private static final void jdoSetiniEntrevistas(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.iniEntrevistas = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.iniEntrevistas = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 15, paramConcurso.iniEntrevistas, paramDate);
  }

  private static final Date jdoGetiniExamenes(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.iniExamenes;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.iniExamenes;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 16))
      return paramConcurso.iniExamenes;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 16, paramConcurso.iniExamenes);
  }

  private static final void jdoSetiniExamenes(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.iniExamenes = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.iniExamenes = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 16, paramConcurso.iniExamenes, paramDate);
  }

  private static final Date jdoGetiniPublicacionEntr(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.iniPublicacionEntr;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.iniPublicacionEntr;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 17))
      return paramConcurso.iniPublicacionEntr;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 17, paramConcurso.iniPublicacionEntr);
  }

  private static final void jdoSetiniPublicacionEntr(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.iniPublicacionEntr = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.iniPublicacionEntr = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 17, paramConcurso.iniPublicacionEntr, paramDate);
  }

  private static final Date jdoGetiniPublicacionExam(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.iniPublicacionExam;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.iniPublicacionExam;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 18))
      return paramConcurso.iniPublicacionExam;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 18, paramConcurso.iniPublicacionExam);
  }

  private static final void jdoSetiniPublicacionExam(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.iniPublicacionExam = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.iniPublicacionExam = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 18, paramConcurso.iniPublicacionExam, paramDate);
  }

  private static final Date jdoGetiniPublicacionInsc(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.iniPublicacionInsc;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.iniPublicacionInsc;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 19))
      return paramConcurso.iniPublicacionInsc;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 19, paramConcurso.iniPublicacionInsc);
  }

  private static final void jdoSetiniPublicacionInsc(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.iniPublicacionInsc = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.iniPublicacionInsc = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 19, paramConcurso.iniPublicacionInsc, paramDate);
  }

  private static final Date jdoGetinicioConvocatoria(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.inicioConvocatoria;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.inicioConvocatoria;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 20))
      return paramConcurso.inicioConvocatoria;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 20, paramConcurso.inicioConvocatoria);
  }

  private static final void jdoSetinicioConvocatoria(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.inicioConvocatoria = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.inicioConvocatoria = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 20, paramConcurso.inicioConvocatoria, paramDate);
  }

  private static final Date jdoGetinicioInscripcion(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.inicioInscripcion;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.inicioInscripcion;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 21))
      return paramConcurso.inicioInscripcion;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 21, paramConcurso.inicioInscripcion);
  }

  private static final void jdoSetinicioInscripcion(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.inicioInscripcion = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.inicioInscripcion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 21, paramConcurso.inicioInscripcion, paramDate);
  }

  private static final Date jdoGetinicioRetiro(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.inicioRetiro;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.inicioRetiro;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 22))
      return paramConcurso.inicioRetiro;
    return (Date)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 22, paramConcurso.inicioRetiro);
  }

  private static final void jdoSetinicioRetiro(Concurso paramConcurso, Date paramDate)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.inicioRetiro = paramDate;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.inicioRetiro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 22, paramConcurso.inicioRetiro, paramDate);
  }

  private static final String jdoGetnombre(Concurso paramConcurso)
  {
    if (paramConcurso.jdoFlags <= 0)
      return paramConcurso.nombre;
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.nombre;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 23))
      return paramConcurso.nombre;
    return localStateManager.getStringField(paramConcurso, jdoInheritedFieldCount + 23, paramConcurso.nombre);
  }

  private static final void jdoSetnombre(Concurso paramConcurso, String paramString)
  {
    if (paramConcurso.jdoFlags == 0)
    {
      paramConcurso.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramConcurso, jdoInheritedFieldCount + 23, paramConcurso.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(Concurso paramConcurso)
  {
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramConcurso.organismo;
    if (localStateManager.isLoaded(paramConcurso, jdoInheritedFieldCount + 24))
      return paramConcurso.organismo;
    return (Organismo)localStateManager.getObjectField(paramConcurso, jdoInheritedFieldCount + 24, paramConcurso.organismo);
  }

  private static final void jdoSetorganismo(Concurso paramConcurso, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcurso.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramConcurso, jdoInheritedFieldCount + 24, paramConcurso.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}