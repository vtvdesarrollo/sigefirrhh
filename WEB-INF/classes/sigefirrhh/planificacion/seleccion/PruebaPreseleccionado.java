package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PruebaPreseleccionado
  implements Serializable, PersistenceCapable
{
  private long idPruebaPreseleccionado;
  private PostuladoConcurso postuladoConcurso;
  private PruebaSeleccion pruebaSeleccion;
  private float resultado;
  private String entrevistador;
  private Date fecha;
  private String observaciones;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "entrevistador", "fecha", "idPruebaPreseleccionado", "observaciones", "postuladoConcurso", "pruebaSeleccion", "resultado" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PostuladoConcurso"), sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PruebaSeleccion"), Float.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetresultado(this));

    return jdoGetpruebaSeleccion(this).getNombre() + " - " + a;
  }

  public String getEntrevistador() {
    return jdoGetentrevistador(this);
  }

  public void setEntrevistador(String entrevistador)
  {
    jdoSetentrevistador(this, entrevistador);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public void setFecha(Date fecha)
  {
    jdoSetfecha(this, fecha);
  }

  public long getIdPruebaPreseleccionado()
  {
    return jdoGetidPruebaPreseleccionado(this);
  }

  public void setIdPruebaPreseleccionado(long idPruebaPreseleccionado)
  {
    jdoSetidPruebaPreseleccionado(this, idPruebaPreseleccionado);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public PostuladoConcurso getPostuladoConcurso()
  {
    return jdoGetpostuladoConcurso(this);
  }

  public void setPostuladoConcurso(PostuladoConcurso postuladoConcurso)
  {
    jdoSetpostuladoConcurso(this, postuladoConcurso);
  }

  public PruebaSeleccion getPruebaSeleccion()
  {
    return jdoGetpruebaSeleccion(this);
  }

  public void setPruebaSeleccion(PruebaSeleccion pruebaSeleccion)
  {
    jdoSetpruebaSeleccion(this, pruebaSeleccion);
  }

  public float getResultado()
  {
    return jdoGetresultado(this);
  }

  public void setResultado(float resultado)
  {
    jdoSetresultado(this, resultado);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PruebaPreseleccionado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PruebaPreseleccionado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PruebaPreseleccionado localPruebaPreseleccionado = new PruebaPreseleccionado();
    localPruebaPreseleccionado.jdoFlags = 1;
    localPruebaPreseleccionado.jdoStateManager = paramStateManager;
    return localPruebaPreseleccionado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PruebaPreseleccionado localPruebaPreseleccionado = new PruebaPreseleccionado();
    localPruebaPreseleccionado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPruebaPreseleccionado.jdoFlags = 1;
    localPruebaPreseleccionado.jdoStateManager = paramStateManager;
    return localPruebaPreseleccionado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.entrevistador);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPruebaPreseleccionado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.postuladoConcurso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.pruebaSeleccion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.resultado);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entrevistador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPruebaPreseleccionado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.postuladoConcurso = ((PostuladoConcurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pruebaSeleccion = ((PruebaSeleccion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultado = localStateManager.replacingFloatField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PruebaPreseleccionado paramPruebaPreseleccionado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPruebaPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.entrevistador = paramPruebaPreseleccionado.entrevistador;
      return;
    case 1:
      if (paramPruebaPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramPruebaPreseleccionado.fecha;
      return;
    case 2:
      if (paramPruebaPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.idPruebaPreseleccionado = paramPruebaPreseleccionado.idPruebaPreseleccionado;
      return;
    case 3:
      if (paramPruebaPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramPruebaPreseleccionado.observaciones;
      return;
    case 4:
      if (paramPruebaPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.postuladoConcurso = paramPruebaPreseleccionado.postuladoConcurso;
      return;
    case 5:
      if (paramPruebaPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.pruebaSeleccion = paramPruebaPreseleccionado.pruebaSeleccion;
      return;
    case 6:
      if (paramPruebaPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.resultado = paramPruebaPreseleccionado.resultado;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PruebaPreseleccionado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PruebaPreseleccionado localPruebaPreseleccionado = (PruebaPreseleccionado)paramObject;
    if (localPruebaPreseleccionado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPruebaPreseleccionado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PruebaPreseleccionadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PruebaPreseleccionadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PruebaPreseleccionadoPK))
      throw new IllegalArgumentException("arg1");
    PruebaPreseleccionadoPK localPruebaPreseleccionadoPK = (PruebaPreseleccionadoPK)paramObject;
    localPruebaPreseleccionadoPK.idPruebaPreseleccionado = this.idPruebaPreseleccionado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PruebaPreseleccionadoPK))
      throw new IllegalArgumentException("arg1");
    PruebaPreseleccionadoPK localPruebaPreseleccionadoPK = (PruebaPreseleccionadoPK)paramObject;
    this.idPruebaPreseleccionado = localPruebaPreseleccionadoPK.idPruebaPreseleccionado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PruebaPreseleccionadoPK))
      throw new IllegalArgumentException("arg2");
    PruebaPreseleccionadoPK localPruebaPreseleccionadoPK = (PruebaPreseleccionadoPK)paramObject;
    localPruebaPreseleccionadoPK.idPruebaPreseleccionado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PruebaPreseleccionadoPK))
      throw new IllegalArgumentException("arg2");
    PruebaPreseleccionadoPK localPruebaPreseleccionadoPK = (PruebaPreseleccionadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localPruebaPreseleccionadoPK.idPruebaPreseleccionado);
  }

  private static final String jdoGetentrevistador(PruebaPreseleccionado paramPruebaPreseleccionado)
  {
    if (paramPruebaPreseleccionado.jdoFlags <= 0)
      return paramPruebaPreseleccionado.entrevistador;
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaPreseleccionado.entrevistador;
    if (localStateManager.isLoaded(paramPruebaPreseleccionado, jdoInheritedFieldCount + 0))
      return paramPruebaPreseleccionado.entrevistador;
    return localStateManager.getStringField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 0, paramPruebaPreseleccionado.entrevistador);
  }

  private static final void jdoSetentrevistador(PruebaPreseleccionado paramPruebaPreseleccionado, String paramString)
  {
    if (paramPruebaPreseleccionado.jdoFlags == 0)
    {
      paramPruebaPreseleccionado.entrevistador = paramString;
      return;
    }
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaPreseleccionado.entrevistador = paramString;
      return;
    }
    localStateManager.setStringField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 0, paramPruebaPreseleccionado.entrevistador, paramString);
  }

  private static final Date jdoGetfecha(PruebaPreseleccionado paramPruebaPreseleccionado)
  {
    if (paramPruebaPreseleccionado.jdoFlags <= 0)
      return paramPruebaPreseleccionado.fecha;
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaPreseleccionado.fecha;
    if (localStateManager.isLoaded(paramPruebaPreseleccionado, jdoInheritedFieldCount + 1))
      return paramPruebaPreseleccionado.fecha;
    return (Date)localStateManager.getObjectField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 1, paramPruebaPreseleccionado.fecha);
  }

  private static final void jdoSetfecha(PruebaPreseleccionado paramPruebaPreseleccionado, Date paramDate)
  {
    if (paramPruebaPreseleccionado.jdoFlags == 0)
    {
      paramPruebaPreseleccionado.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaPreseleccionado.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 1, paramPruebaPreseleccionado.fecha, paramDate);
  }

  private static final long jdoGetidPruebaPreseleccionado(PruebaPreseleccionado paramPruebaPreseleccionado)
  {
    return paramPruebaPreseleccionado.idPruebaPreseleccionado;
  }

  private static final void jdoSetidPruebaPreseleccionado(PruebaPreseleccionado paramPruebaPreseleccionado, long paramLong)
  {
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaPreseleccionado.idPruebaPreseleccionado = paramLong;
      return;
    }
    localStateManager.setLongField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 2, paramPruebaPreseleccionado.idPruebaPreseleccionado, paramLong);
  }

  private static final String jdoGetobservaciones(PruebaPreseleccionado paramPruebaPreseleccionado)
  {
    if (paramPruebaPreseleccionado.jdoFlags <= 0)
      return paramPruebaPreseleccionado.observaciones;
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaPreseleccionado.observaciones;
    if (localStateManager.isLoaded(paramPruebaPreseleccionado, jdoInheritedFieldCount + 3))
      return paramPruebaPreseleccionado.observaciones;
    return localStateManager.getStringField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 3, paramPruebaPreseleccionado.observaciones);
  }

  private static final void jdoSetobservaciones(PruebaPreseleccionado paramPruebaPreseleccionado, String paramString)
  {
    if (paramPruebaPreseleccionado.jdoFlags == 0)
    {
      paramPruebaPreseleccionado.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaPreseleccionado.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 3, paramPruebaPreseleccionado.observaciones, paramString);
  }

  private static final PostuladoConcurso jdoGetpostuladoConcurso(PruebaPreseleccionado paramPruebaPreseleccionado)
  {
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaPreseleccionado.postuladoConcurso;
    if (localStateManager.isLoaded(paramPruebaPreseleccionado, jdoInheritedFieldCount + 4))
      return paramPruebaPreseleccionado.postuladoConcurso;
    return (PostuladoConcurso)localStateManager.getObjectField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 4, paramPruebaPreseleccionado.postuladoConcurso);
  }

  private static final void jdoSetpostuladoConcurso(PruebaPreseleccionado paramPruebaPreseleccionado, PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaPreseleccionado.postuladoConcurso = paramPostuladoConcurso;
      return;
    }
    localStateManager.setObjectField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 4, paramPruebaPreseleccionado.postuladoConcurso, paramPostuladoConcurso);
  }

  private static final PruebaSeleccion jdoGetpruebaSeleccion(PruebaPreseleccionado paramPruebaPreseleccionado)
  {
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaPreseleccionado.pruebaSeleccion;
    if (localStateManager.isLoaded(paramPruebaPreseleccionado, jdoInheritedFieldCount + 5))
      return paramPruebaPreseleccionado.pruebaSeleccion;
    return (PruebaSeleccion)localStateManager.getObjectField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 5, paramPruebaPreseleccionado.pruebaSeleccion);
  }

  private static final void jdoSetpruebaSeleccion(PruebaPreseleccionado paramPruebaPreseleccionado, PruebaSeleccion paramPruebaSeleccion)
  {
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaPreseleccionado.pruebaSeleccion = paramPruebaSeleccion;
      return;
    }
    localStateManager.setObjectField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 5, paramPruebaPreseleccionado.pruebaSeleccion, paramPruebaSeleccion);
  }

  private static final float jdoGetresultado(PruebaPreseleccionado paramPruebaPreseleccionado)
  {
    if (paramPruebaPreseleccionado.jdoFlags <= 0)
      return paramPruebaPreseleccionado.resultado;
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaPreseleccionado.resultado;
    if (localStateManager.isLoaded(paramPruebaPreseleccionado, jdoInheritedFieldCount + 6))
      return paramPruebaPreseleccionado.resultado;
    return localStateManager.getFloatField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 6, paramPruebaPreseleccionado.resultado);
  }

  private static final void jdoSetresultado(PruebaPreseleccionado paramPruebaPreseleccionado, float paramFloat)
  {
    if (paramPruebaPreseleccionado.jdoFlags == 0)
    {
      paramPruebaPreseleccionado.resultado = paramFloat;
      return;
    }
    StateManager localStateManager = paramPruebaPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaPreseleccionado.resultado = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramPruebaPreseleccionado, jdoInheritedFieldCount + 6, paramPruebaPreseleccionado.resultado, paramFloat);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}