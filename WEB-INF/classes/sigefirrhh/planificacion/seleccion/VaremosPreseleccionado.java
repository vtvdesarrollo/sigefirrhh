package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class VaremosPreseleccionado
  implements Serializable, PersistenceCapable
{
  private long idVaremosPreseleccionado;
  private PostuladoConcurso postuladoConcurso;
  private Varemos varemos;
  private double resultado;
  private String observaciones;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idVaremosPreseleccionado", "observaciones", "postuladoConcurso", "resultado", "varemos" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PostuladoConcurso"), Double.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.seleccion.Varemos") };
  private static final byte[] jdoFieldFlags = { 24, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public VaremosPreseleccionado()
  {
    jdoSetresultado(this, 0.0D);
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetresultado(this));
    return jdoGetvaremos(this).getNombre() + " - " + a;
  }

  public long getIdVaremosPreseleccionado()
  {
    return jdoGetidVaremosPreseleccionado(this);
  }
  public void setIdVaremosPreseleccionado(long idVaremosPreseleccionado) {
    jdoSetidVaremosPreseleccionado(this, idVaremosPreseleccionado);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public PostuladoConcurso getPostuladoConcurso() {
    return jdoGetpostuladoConcurso(this);
  }
  public void setPostuladoConcurso(PostuladoConcurso postuladoConcurso) {
    jdoSetpostuladoConcurso(this, postuladoConcurso);
  }
  public double getResultado() {
    return jdoGetresultado(this);
  }
  public void setResultado(double resultado) {
    jdoSetresultado(this, resultado);
  }
  public Varemos getVaremos() {
    return jdoGetvaremos(this);
  }
  public void setVaremos(Varemos varemos) {
    jdoSetvaremos(this, varemos);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.VaremosPreseleccionado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new VaremosPreseleccionado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    VaremosPreseleccionado localVaremosPreseleccionado = new VaremosPreseleccionado();
    localVaremosPreseleccionado.jdoFlags = 1;
    localVaremosPreseleccionado.jdoStateManager = paramStateManager;
    return localVaremosPreseleccionado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    VaremosPreseleccionado localVaremosPreseleccionado = new VaremosPreseleccionado();
    localVaremosPreseleccionado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localVaremosPreseleccionado.jdoFlags = 1;
    localVaremosPreseleccionado.jdoStateManager = paramStateManager;
    return localVaremosPreseleccionado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idVaremosPreseleccionado);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.postuladoConcurso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.resultado);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.varemos);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idVaremosPreseleccionado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.postuladoConcurso = ((PostuladoConcurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.varemos = ((Varemos)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(VaremosPreseleccionado paramVaremosPreseleccionado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramVaremosPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.idVaremosPreseleccionado = paramVaremosPreseleccionado.idVaremosPreseleccionado;
      return;
    case 1:
      if (paramVaremosPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramVaremosPreseleccionado.observaciones;
      return;
    case 2:
      if (paramVaremosPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.postuladoConcurso = paramVaremosPreseleccionado.postuladoConcurso;
      return;
    case 3:
      if (paramVaremosPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.resultado = paramVaremosPreseleccionado.resultado;
      return;
    case 4:
      if (paramVaremosPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.varemos = paramVaremosPreseleccionado.varemos;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof VaremosPreseleccionado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    VaremosPreseleccionado localVaremosPreseleccionado = (VaremosPreseleccionado)paramObject;
    if (localVaremosPreseleccionado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localVaremosPreseleccionado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new VaremosPreseleccionadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new VaremosPreseleccionadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VaremosPreseleccionadoPK))
      throw new IllegalArgumentException("arg1");
    VaremosPreseleccionadoPK localVaremosPreseleccionadoPK = (VaremosPreseleccionadoPK)paramObject;
    localVaremosPreseleccionadoPK.idVaremosPreseleccionado = this.idVaremosPreseleccionado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VaremosPreseleccionadoPK))
      throw new IllegalArgumentException("arg1");
    VaremosPreseleccionadoPK localVaremosPreseleccionadoPK = (VaremosPreseleccionadoPK)paramObject;
    this.idVaremosPreseleccionado = localVaremosPreseleccionadoPK.idVaremosPreseleccionado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VaremosPreseleccionadoPK))
      throw new IllegalArgumentException("arg2");
    VaremosPreseleccionadoPK localVaremosPreseleccionadoPK = (VaremosPreseleccionadoPK)paramObject;
    localVaremosPreseleccionadoPK.idVaremosPreseleccionado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VaremosPreseleccionadoPK))
      throw new IllegalArgumentException("arg2");
    VaremosPreseleccionadoPK localVaremosPreseleccionadoPK = (VaremosPreseleccionadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localVaremosPreseleccionadoPK.idVaremosPreseleccionado);
  }

  private static final long jdoGetidVaremosPreseleccionado(VaremosPreseleccionado paramVaremosPreseleccionado)
  {
    return paramVaremosPreseleccionado.idVaremosPreseleccionado;
  }

  private static final void jdoSetidVaremosPreseleccionado(VaremosPreseleccionado paramVaremosPreseleccionado, long paramLong)
  {
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremosPreseleccionado.idVaremosPreseleccionado = paramLong;
      return;
    }
    localStateManager.setLongField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 0, paramVaremosPreseleccionado.idVaremosPreseleccionado, paramLong);
  }

  private static final String jdoGetobservaciones(VaremosPreseleccionado paramVaremosPreseleccionado)
  {
    if (paramVaremosPreseleccionado.jdoFlags <= 0)
      return paramVaremosPreseleccionado.observaciones;
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramVaremosPreseleccionado.observaciones;
    if (localStateManager.isLoaded(paramVaremosPreseleccionado, jdoInheritedFieldCount + 1))
      return paramVaremosPreseleccionado.observaciones;
    return localStateManager.getStringField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 1, paramVaremosPreseleccionado.observaciones);
  }

  private static final void jdoSetobservaciones(VaremosPreseleccionado paramVaremosPreseleccionado, String paramString)
  {
    if (paramVaremosPreseleccionado.jdoFlags == 0)
    {
      paramVaremosPreseleccionado.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremosPreseleccionado.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 1, paramVaremosPreseleccionado.observaciones, paramString);
  }

  private static final PostuladoConcurso jdoGetpostuladoConcurso(VaremosPreseleccionado paramVaremosPreseleccionado)
  {
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramVaremosPreseleccionado.postuladoConcurso;
    if (localStateManager.isLoaded(paramVaremosPreseleccionado, jdoInheritedFieldCount + 2))
      return paramVaremosPreseleccionado.postuladoConcurso;
    return (PostuladoConcurso)localStateManager.getObjectField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 2, paramVaremosPreseleccionado.postuladoConcurso);
  }

  private static final void jdoSetpostuladoConcurso(VaremosPreseleccionado paramVaremosPreseleccionado, PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremosPreseleccionado.postuladoConcurso = paramPostuladoConcurso;
      return;
    }
    localStateManager.setObjectField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 2, paramVaremosPreseleccionado.postuladoConcurso, paramPostuladoConcurso);
  }

  private static final double jdoGetresultado(VaremosPreseleccionado paramVaremosPreseleccionado)
  {
    if (paramVaremosPreseleccionado.jdoFlags <= 0)
      return paramVaremosPreseleccionado.resultado;
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramVaremosPreseleccionado.resultado;
    if (localStateManager.isLoaded(paramVaremosPreseleccionado, jdoInheritedFieldCount + 3))
      return paramVaremosPreseleccionado.resultado;
    return localStateManager.getDoubleField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 3, paramVaremosPreseleccionado.resultado);
  }

  private static final void jdoSetresultado(VaremosPreseleccionado paramVaremosPreseleccionado, double paramDouble)
  {
    if (paramVaremosPreseleccionado.jdoFlags == 0)
    {
      paramVaremosPreseleccionado.resultado = paramDouble;
      return;
    }
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremosPreseleccionado.resultado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 3, paramVaremosPreseleccionado.resultado, paramDouble);
  }

  private static final Varemos jdoGetvaremos(VaremosPreseleccionado paramVaremosPreseleccionado)
  {
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramVaremosPreseleccionado.varemos;
    if (localStateManager.isLoaded(paramVaremosPreseleccionado, jdoInheritedFieldCount + 4))
      return paramVaremosPreseleccionado.varemos;
    return (Varemos)localStateManager.getObjectField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 4, paramVaremosPreseleccionado.varemos);
  }

  private static final void jdoSetvaremos(VaremosPreseleccionado paramVaremosPreseleccionado, Varemos paramVaremos)
  {
    StateManager localStateManager = paramVaremosPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremosPreseleccionado.varemos = paramVaremos;
      return;
    }
    localStateManager.setObjectField(paramVaremosPreseleccionado, jdoInheritedFieldCount + 4, paramVaremosPreseleccionado.varemos, paramVaremos);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}