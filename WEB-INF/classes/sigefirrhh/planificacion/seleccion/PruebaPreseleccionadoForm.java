package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PruebaPreseleccionadoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PruebaPreseleccionadoForm.class.getName());
  private PruebaPreseleccionado pruebaPreseleccionado;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionFacade seleccionFacade = new SeleccionFacade();
  private boolean showPruebaPreseleccionadoByPostuladoConcurso;
  private String findSelectConcursoForPostuladoConcurso;
  private String findSelectConcursoCargoForPostuladoConcurso;
  private String findSelectPostuladoConcurso;
  private Collection findColConcursoForPostuladoConcurso;
  private Collection findColConcursoCargoForPostuladoConcurso;
  private Collection findColPostuladoConcurso;
  private Collection colConcursoForPostuladoConcurso;
  private Collection colConcursoCargoForPostuladoConcurso;
  private Collection colPostuladoConcurso;
  private Collection colPruebaSeleccion;
  private String selectConcursoForPostuladoConcurso;
  private String selectConcursoCargoForPostuladoConcurso;
  private String selectPostuladoConcurso;
  private String selectPruebaSeleccion;
  private Object stateResultPruebaPreseleccionadoByPostuladoConcurso = null;

  public Collection getFindColConcursoForPostuladoConcurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConcursoForPostuladoConcurso.iterator();
    Concurso concursoForPostuladoConcurso = null;
    while (iterator.hasNext()) {
      concursoForPostuladoConcurso = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concursoForPostuladoConcurso.getIdConcurso()), 
        concursoForPostuladoConcurso.toString()));
    }
    return col;
  }
  public String getFindSelectConcursoForPostuladoConcurso() {
    return this.findSelectConcursoForPostuladoConcurso;
  }
  public void setFindSelectConcursoForPostuladoConcurso(String valConcursoForPostuladoConcurso) {
    this.findSelectConcursoForPostuladoConcurso = valConcursoForPostuladoConcurso;
  }
  public void findChangeConcursoForPostuladoConcurso(ValueChangeEvent event) {
    long idConcurso = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColPostuladoConcurso = null;
      this.findColConcursoCargoForPostuladoConcurso = null;
      if (idConcurso > 0L)
        this.findColConcursoCargoForPostuladoConcurso = 
          this.seleccionFacade.findConcursoCargoByConcurso(
          idConcurso);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowConcursoForPostuladoConcurso() { return this.findColConcursoForPostuladoConcurso != null; }

  public Collection getFindColConcursoCargoForPostuladoConcurso() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConcursoCargoForPostuladoConcurso.iterator();
    ConcursoCargo concursoCargoForPostuladoConcurso = null;
    while (iterator.hasNext()) {
      concursoCargoForPostuladoConcurso = (ConcursoCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concursoCargoForPostuladoConcurso.getIdConcursoCargo()), 
        concursoCargoForPostuladoConcurso.toString()));
    }
    return col;
  }
  public String getFindSelectConcursoCargoForPostuladoConcurso() {
    return this.findSelectConcursoCargoForPostuladoConcurso;
  }
  public void setFindSelectConcursoCargoForPostuladoConcurso(String valConcursoCargoForPostuladoConcurso) {
    this.findSelectConcursoCargoForPostuladoConcurso = valConcursoCargoForPostuladoConcurso;
  }
  public void findChangeConcursoCargoForPostuladoConcurso(ValueChangeEvent event) {
    long idConcursoCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColPostuladoConcurso = null;
      if (idConcursoCargo > 0L)
        this.findColPostuladoConcurso = 
          this.seleccionFacade.findPostuladoConcursoByConcursoCargo(
          idConcursoCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowConcursoCargoForPostuladoConcurso() { return this.findColConcursoCargoForPostuladoConcurso != null; }

  public String getFindSelectPostuladoConcurso() {
    return this.findSelectPostuladoConcurso;
  }
  public void setFindSelectPostuladoConcurso(String valPostuladoConcurso) {
    this.findSelectPostuladoConcurso = valPostuladoConcurso;
  }

  public Collection getFindColPostuladoConcurso() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPostuladoConcurso.iterator();
    PostuladoConcurso postuladoConcurso = null;
    while (iterator.hasNext()) {
      postuladoConcurso = (PostuladoConcurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(postuladoConcurso.getIdPostuladoConcurso()), 
        postuladoConcurso.toString()));
    }
    return col;
  }
  public boolean isFindShowPostuladoConcurso() {
    return this.findColPostuladoConcurso != null;
  }

  public String getSelectConcursoForPostuladoConcurso()
  {
    return this.selectConcursoForPostuladoConcurso;
  }
  public void setSelectConcursoForPostuladoConcurso(String valConcursoForPostuladoConcurso) {
    this.selectConcursoForPostuladoConcurso = valConcursoForPostuladoConcurso;
  }
  public void changeConcursoForPostuladoConcurso(ValueChangeEvent event) {
    long idConcurso = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPostuladoConcurso = null;
      this.colConcursoCargoForPostuladoConcurso = null;
      if (idConcurso > 0L) {
        this.colConcursoCargoForPostuladoConcurso = 
          this.seleccionFacade.findConcursoCargoByConcurso(
          idConcurso);
      } else {
        this.selectPostuladoConcurso = null;
        this.pruebaPreseleccionado.setPostuladoConcurso(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectPostuladoConcurso = null;
      this.pruebaPreseleccionado.setPostuladoConcurso(
        null);
    }
  }

  public boolean isShowConcursoForPostuladoConcurso() { return this.colConcursoForPostuladoConcurso != null; }

  public String getSelectConcursoCargoForPostuladoConcurso() {
    return this.selectConcursoCargoForPostuladoConcurso;
  }
  public void setSelectConcursoCargoForPostuladoConcurso(String valConcursoCargoForPostuladoConcurso) {
    this.selectConcursoCargoForPostuladoConcurso = valConcursoCargoForPostuladoConcurso;
  }
  public void changeConcursoCargoForPostuladoConcurso(ValueChangeEvent event) {
    long idConcursoCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPostuladoConcurso = null;
      if (idConcursoCargo > 0L) {
        this.colPostuladoConcurso = 
          this.seleccionFacade.findPostuladoConcursoByConcursoCargo(
          idConcursoCargo);
      } else {
        this.selectPostuladoConcurso = null;
        this.pruebaPreseleccionado.setPostuladoConcurso(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectPostuladoConcurso = null;
      this.pruebaPreseleccionado.setPostuladoConcurso(
        null);
    }
  }

  public boolean isShowConcursoCargoForPostuladoConcurso() { return this.colConcursoCargoForPostuladoConcurso != null; }

  public String getSelectPostuladoConcurso() {
    return this.selectPostuladoConcurso;
  }
  public void setSelectPostuladoConcurso(String valPostuladoConcurso) {
    Iterator iterator = this.colPostuladoConcurso.iterator();
    PostuladoConcurso postuladoConcurso = null;
    this.pruebaPreseleccionado.setPostuladoConcurso(null);
    while (iterator.hasNext()) {
      postuladoConcurso = (PostuladoConcurso)iterator.next();
      if (String.valueOf(postuladoConcurso.getIdPostuladoConcurso()).equals(
        valPostuladoConcurso)) {
        this.pruebaPreseleccionado.setPostuladoConcurso(
          postuladoConcurso);
        break;
      }
    }
    this.selectPostuladoConcurso = valPostuladoConcurso;
  }
  public boolean isShowPostuladoConcurso() {
    return this.colPostuladoConcurso != null;
  }
  public String getSelectPruebaSeleccion() {
    return this.selectPruebaSeleccion;
  }
  public void setSelectPruebaSeleccion(String valPruebaSeleccion) {
    Iterator iterator = this.colPruebaSeleccion.iterator();
    PruebaSeleccion pruebaSeleccion = null;
    this.pruebaPreseleccionado.setPruebaSeleccion(null);
    while (iterator.hasNext()) {
      pruebaSeleccion = (PruebaSeleccion)iterator.next();
      if (String.valueOf(pruebaSeleccion.getIdPruebaSeleccion()).equals(
        valPruebaSeleccion)) {
        this.pruebaPreseleccionado.setPruebaSeleccion(
          pruebaSeleccion);
        break;
      }
    }
    this.selectPruebaSeleccion = valPruebaSeleccion;
  }
  public Collection getResult() {
    return this.result;
  }

  public PruebaPreseleccionado getPruebaPreseleccionado() {
    if (this.pruebaPreseleccionado == null) {
      this.pruebaPreseleccionado = new PruebaPreseleccionado();
    }
    return this.pruebaPreseleccionado;
  }

  public PruebaPreseleccionadoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColConcursoForPostuladoConcurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcursoForPostuladoConcurso.iterator();
    Concurso concursoForPostuladoConcurso = null;
    while (iterator.hasNext()) {
      concursoForPostuladoConcurso = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concursoForPostuladoConcurso.getIdConcurso()), 
        concursoForPostuladoConcurso.toString()));
    }
    return col;
  }

  public Collection getColConcursoCargoForPostuladoConcurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcursoCargoForPostuladoConcurso.iterator();
    ConcursoCargo concursoCargoForPostuladoConcurso = null;
    while (iterator.hasNext()) {
      concursoCargoForPostuladoConcurso = (ConcursoCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concursoCargoForPostuladoConcurso.getIdConcursoCargo()), 
        concursoCargoForPostuladoConcurso.toString()));
    }
    return col;
  }

  public Collection getColPostuladoConcurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPostuladoConcurso.iterator();
    PostuladoConcurso postuladoConcurso = null;
    while (iterator.hasNext()) {
      postuladoConcurso = (PostuladoConcurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(postuladoConcurso.getIdPostuladoConcurso()), 
        postuladoConcurso.toString()));
    }
    return col;
  }

  public Collection getColPruebaSeleccion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPruebaSeleccion.iterator();
    PruebaSeleccion pruebaSeleccion = null;
    while (iterator.hasNext()) {
      pruebaSeleccion = (PruebaSeleccion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(pruebaSeleccion.getIdPruebaSeleccion()), 
        pruebaSeleccion.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColConcursoForPostuladoConcurso = 
        this.seleccionFacade.findConcursoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colConcursoForPostuladoConcurso = 
        this.seleccionFacade.findConcursoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colPruebaSeleccion = 
        this.seleccionFacade.findAllPruebaSeleccion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPruebaPreseleccionadoByPostuladoConcurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findPruebaPreseleccionadoByPostuladoConcurso(Long.valueOf(this.findSelectPostuladoConcurso).longValue());
      this.showPruebaPreseleccionadoByPostuladoConcurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPruebaPreseleccionadoByPostuladoConcurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectConcursoForPostuladoConcurso = null;
    this.findSelectConcursoCargoForPostuladoConcurso = null;
    this.findSelectPostuladoConcurso = null;

    return null;
  }

  public boolean isShowPruebaPreseleccionadoByPostuladoConcurso() {
    return this.showPruebaPreseleccionadoByPostuladoConcurso;
  }

  public String selectPruebaPreseleccionado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPostuladoConcurso = null;
    this.selectConcursoForPostuladoConcurso = null;

    this.selectConcursoCargoForPostuladoConcurso = null;

    this.selectPruebaSeleccion = null;

    long idPruebaPreseleccionado = 
      Long.parseLong((String)requestParameterMap.get("idPruebaPreseleccionado"));
    try
    {
      this.pruebaPreseleccionado = 
        this.seleccionFacade.findPruebaPreseleccionadoById(
        idPruebaPreseleccionado);
      if (this.pruebaPreseleccionado.getPostuladoConcurso() != null) {
        this.selectPostuladoConcurso = 
          String.valueOf(this.pruebaPreseleccionado.getPostuladoConcurso().getIdPostuladoConcurso());
      }
      if (this.pruebaPreseleccionado.getPruebaSeleccion() != null) {
        this.selectPruebaSeleccion = 
          String.valueOf(this.pruebaPreseleccionado.getPruebaSeleccion().getIdPruebaSeleccion());
      }

      PostuladoConcurso postuladoConcurso = null;
      ConcursoCargo concursoCargoForPostuladoConcurso = null;
      Concurso concursoForPostuladoConcurso = null;

      if (this.pruebaPreseleccionado.getPostuladoConcurso() != null) {
        long idPostuladoConcurso = 
          this.pruebaPreseleccionado.getPostuladoConcurso().getIdPostuladoConcurso();
        this.selectPostuladoConcurso = String.valueOf(idPostuladoConcurso);
        postuladoConcurso = this.seleccionFacade.findPostuladoConcursoById(
          idPostuladoConcurso);
        this.colPostuladoConcurso = this.seleccionFacade.findPostuladoConcursoByConcursoCargo(
          postuladoConcurso.getConcursoCargo().getIdConcursoCargo());

        long idConcursoCargoForPostuladoConcurso = 
          this.pruebaPreseleccionado.getPostuladoConcurso().getConcursoCargo().getIdConcursoCargo();
        this.selectConcursoCargoForPostuladoConcurso = String.valueOf(idConcursoCargoForPostuladoConcurso);
        concursoCargoForPostuladoConcurso = 
          this.seleccionFacade.findConcursoCargoById(
          idConcursoCargoForPostuladoConcurso);
        this.colConcursoCargoForPostuladoConcurso = 
          this.seleccionFacade.findConcursoCargoByConcurso(
          concursoCargoForPostuladoConcurso.getConcurso().getIdConcurso());

        long idConcursoForPostuladoConcurso = 
          concursoCargoForPostuladoConcurso.getConcurso().getIdConcurso();
        this.selectConcursoForPostuladoConcurso = String.valueOf(idConcursoForPostuladoConcurso);
        concursoForPostuladoConcurso = 
          this.seleccionFacade.findConcursoById(
          idConcursoForPostuladoConcurso);
        this.colConcursoForPostuladoConcurso = 
          this.seleccionFacade.findAllConcurso();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.pruebaPreseleccionado = null;
    this.showPruebaPreseleccionadoByPostuladoConcurso = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.pruebaPreseleccionado.getFecha() != null) && 
      (this.pruebaPreseleccionado.getFecha().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.seleccionFacade.addPruebaPreseleccionado(
          this.pruebaPreseleccionado);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.seleccionFacade.updatePruebaPreseleccionado(
          this.pruebaPreseleccionado);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deletePruebaPreseleccionado(
        this.pruebaPreseleccionado);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.pruebaPreseleccionado = new PruebaPreseleccionado();

    this.selectPostuladoConcurso = null;

    this.selectConcursoForPostuladoConcurso = null;

    this.selectConcursoCargoForPostuladoConcurso = null;

    this.selectPruebaSeleccion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.pruebaPreseleccionado.setIdPruebaPreseleccionado(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.PruebaPreseleccionado"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.pruebaPreseleccionado = new PruebaPreseleccionado();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}