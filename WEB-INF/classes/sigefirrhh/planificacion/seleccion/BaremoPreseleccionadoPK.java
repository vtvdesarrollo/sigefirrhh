package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class BaremoPreseleccionadoPK
  implements Serializable
{
  public long idBaremoPreseleccionado;

  public BaremoPreseleccionadoPK()
  {
  }

  public BaremoPreseleccionadoPK(long idBaremoPreseleccionado)
  {
    this.idBaremoPreseleccionado = idBaremoPreseleccionado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((BaremoPreseleccionadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(BaremoPreseleccionadoPK thatPK)
  {
    return 
      this.idBaremoPreseleccionado == thatPK.idBaremoPreseleccionado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idBaremoPreseleccionado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idBaremoPreseleccionado);
  }
}