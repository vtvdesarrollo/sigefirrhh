package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorNoGenFacade;
import sigefirrhh.planificacion.elegible.Elegible;
import sigefirrhh.planificacion.elegible.ElegibleFacade;
import sigefirrhh.sistema.RegistrarAuditoria;
import sigefirrhh.sistema.sitp.SitpFacade;

public class PostuladoConcursoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PostuladoConcursoForm.class.getName());
  private PostuladoConcurso postuladoConcurso;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionNoGenFacade seleccionFacade = new SeleccionNoGenFacade();
  private boolean showPostuladoConcursoByConcursoCargo;
  private String findSelectConcursoForConcursoCargo;
  private String findSelectConcursoCargo;
  private Collection findColConcursoForConcursoCargo;
  private Collection findColConcursoCargo;
  private Collection colConcursoForConcursoCargo;
  private Collection colConcursoCargo;
  private String selectConcursoForConcursoCargo;
  private String selectConcursoCargo;
  private int findCedula;
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private SitpFacade sitpFacade = new SitpFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private ElegibleFacade elegibleFacade = new ElegibleFacade();
  private TrabajadorNoGenFacade trabajadorFacade = new TrabajadorNoGenFacade();
  private ConcursoCargo concursoCargo;
  Personal personal;
  Elegible elegible;
  PostuladoExterno postuladoExterno;
  private boolean showBusquedaTrabajador;
  private Object stateResultPostuladoConcursoByConcursoCargo = null;

  public boolean isShowBusquedaTrabajador()
  {
    return this.showBusquedaTrabajador;
  }

  public String findPostuladoByCedula() {
    this.primerApellido = "";
    this.segundoApellido = "";
    this.primerNombre = "";
    this.segundoNombre = "";
    this.cedula = 0;
    this.personal = null;
    this.elegible = null;
    this.postuladoExterno = null;
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      Collection colInhabilitado = this.sitpFacade.findInhabilitadoByCedula(this.findCedula);

      if (!colInhabilitado.isEmpty()) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Esta persona se encuentra inhabilitada", ""));
        return null;
      }

      this.personal = ((Personal)this.expedienteFacade.findPersonalByCedula(this.findCedula, this.login.getIdOrganismo()).iterator().next());
      Collection col = this.trabajadorFacade.findTrabajadorByPersonalAndEstatus(this.personal.getIdPersonal(), "A", this.login.getIdOrganismo());
      if (!col.isEmpty()) {
        Trabajador trabajador = (Trabajador)col.iterator().next();
        if (this.postuladoConcurso.getConcursoCargo().getRegistroCargos().getCargo().getGrado() < trabajador.getCargo().getGrado()) {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador posee un cargo de grado mayor al cargo que esta postulando", ""));
          return "cancel";
        }

      }

      this.primerApellido = this.personal.getPrimerApellido();
      this.segundoApellido = this.personal.getSegundoApellido();
      this.primerNombre = this.personal.getPrimerNombre();
      this.segundoNombre = this.personal.getSegundoNombre();
      this.cedula = this.findCedula;
      this.postuladoConcurso.setOrigen("I");
    }
    catch (Exception ei) {
      try {
        this.elegible = ((Elegible)this.elegibleFacade.findElegibleByCedula(this.findCedula, this.login.getIdOrganismo()).iterator().next());
        this.primerApellido = this.elegible.getPrimerApellido();
        this.segundoApellido = this.elegible.getSegundoApellido();
        this.primerNombre = this.elegible.getPrimerNombre();
        this.segundoNombre = this.elegible.getSegundoNombre();
        this.cedula = this.findCedula;
        this.postuladoConcurso.setOrigen("E");
      } catch (Exception ee) {
        try {
          this.postuladoExterno = ((PostuladoExterno)this.seleccionFacade.findPostuladoExternoByCedula(this.findCedula).iterator().next());
          this.primerApellido = this.postuladoExterno.getPrimerApellido();
          this.segundoApellido = this.postuladoExterno.getSegundoApellido();
          this.primerNombre = this.postuladoExterno.getPrimerNombre();
          this.segundoNombre = this.postuladoExterno.getSegundoNombre();
          this.cedula = this.findCedula;
          this.postuladoConcurso.setOrigen("X");
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cédula no registrada en el sistema", ""));
          return "cancel";
        }
      }

    }

    this.postuladoConcurso.setCedula(this.cedula);
    return "cancel";
  }

  public Collection getFindColConcursoForConcursoCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConcursoForConcursoCargo.iterator();
    Concurso concursoForConcursoCargo = null;
    while (iterator.hasNext()) {
      concursoForConcursoCargo = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concursoForConcursoCargo.getIdConcurso()), 
        concursoForConcursoCargo.toString()));
    }
    return col;
  }
  public String getFindSelectConcursoForConcursoCargo() {
    return this.findSelectConcursoForConcursoCargo;
  }
  public void setFindSelectConcursoForConcursoCargo(String valConcursoForConcursoCargo) {
    this.findSelectConcursoForConcursoCargo = valConcursoForConcursoCargo;
  }
  public void findChangeConcursoForConcursoCargo(ValueChangeEvent event) {
    long idConcurso = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColConcursoCargo = null;
      if (idConcurso > 0L)
        this.findColConcursoCargo = 
          this.seleccionFacade.findConcursoCargoByIdConcurso(
          idConcurso);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowConcursoForConcursoCargo() { return this.findColConcursoForConcursoCargo != null; }

  public String getFindSelectConcursoCargo() {
    return this.findSelectConcursoCargo;
  }
  public void setFindSelectConcursoCargo(String valConcursoCargo) {
    this.findSelectConcursoCargo = valConcursoCargo;
  }

  public Collection getFindColConcursoCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConcursoCargo.iterator();
    ConcursoCargo concursoCargo = null;

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public boolean isFindShowConcursoCargo() {
    return this.findColConcursoCargo != null;
  }

  public String getSelectConcursoForConcursoCargo()
  {
    return this.selectConcursoForConcursoCargo;
  }
  public void setSelectConcursoForConcursoCargo(String valConcursoForConcursoCargo) {
    this.selectConcursoForConcursoCargo = valConcursoForConcursoCargo;
  }
  public void changeConcursoForConcursoCargo(ValueChangeEvent event) {
    long idConcurso = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colConcursoCargo = null;
      if (idConcurso > 0L) {
        this.colConcursoCargo = 
          this.seleccionFacade.findConcursoCargoByIdConcurso(
          idConcurso);
        this.showBusquedaTrabajador = false;
      } else {
        this.selectConcursoCargo = null;
        this.postuladoConcurso.setConcursoCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConcursoCargo = null;
      this.postuladoConcurso.setConcursoCargo(
        null);
    }
  }

  public void changeConcursoCargo(ValueChangeEvent event) { long idConcursoCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.showBusquedaTrabajador = false;
    try
    {
      if (idConcursoCargo > 0L)
      {
        this.postuladoConcurso.setConcursoCargo(this.seleccionFacade.findConcursoCargoById(idConcursoCargo));
        this.showBusquedaTrabajador = true;
      } else {
        this.selectConcursoCargo = null;
        this.postuladoConcurso.setConcursoCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectConcursoCargo = null;
      this.postuladoConcurso.setConcursoCargo(
        null);
    } }

  public boolean isShowConcursoForConcursoCargo() {
    return this.colConcursoForConcursoCargo != null;
  }
  public String getSelectConcursoCargo() {
    return this.selectConcursoCargo;
  }
  public void setSelectConcursoCargo(String valConcursoCargo) {
    Iterator iterator = this.colConcursoCargo.iterator();
    ConcursoCargo concursoCargo = null;
    this.postuladoConcurso.setConcursoCargo(null);

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valConcursoCargo)) {
        try {
          this.postuladoConcurso.setConcursoCargo(this.seleccionFacade.findConcursoCargoById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }
    this.selectConcursoCargo = valConcursoCargo;
  }
  public boolean isShowConcursoCargo() {
    return this.colConcursoCargo != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public PostuladoConcurso getPostuladoConcurso() {
    if (this.postuladoConcurso == null) {
      this.postuladoConcurso = new PostuladoConcurso();
    }
    return this.postuladoConcurso;
  }

  public PostuladoConcursoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColConcursoForConcursoCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcursoForConcursoCargo.iterator();
    Concurso concursoForConcursoCargo = null;
    while (iterator.hasNext()) {
      concursoForConcursoCargo = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concursoForConcursoCargo.getIdConcurso()), 
        concursoForConcursoCargo.toString()));
    }
    return col;
  }

  public Collection getColConcursoCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcursoCargo.iterator();
    ConcursoCargo concursoCargo = null;

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getListOrigen()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PostuladoConcurso.LISTA_ORIGEN.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEstatus() {
    Collection col = new ArrayList();

    Iterator iterEntry = PostuladoConcurso.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColConcursoForConcursoCargo = 
        this.seleccionFacade.findConcursoByEstatus("1", this.login.getIdOrganismo());

      this.colConcursoForConcursoCargo = 
        this.seleccionFacade.findConcursoByEstatus("1", this.login.getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPostuladoConcursoByConcursoCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findPostuladoConcursoByConcursoCargo(Long.valueOf(this.findSelectConcursoCargo).longValue());
      this.showPostuladoConcursoByConcursoCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPostuladoConcursoByConcursoCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectConcursoForConcursoCargo = null;
    this.findSelectConcursoCargo = null;

    return null;
  }

  public boolean isShowPostuladoConcursoByConcursoCargo() {
    return this.showPostuladoConcursoByConcursoCargo;
  }

  public String selectPostuladoConcurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectConcursoCargo = null;
    this.selectConcursoForConcursoCargo = null;

    long idPostuladoConcurso = 
      Long.parseLong((String)requestParameterMap.get("idPostuladoConcurso"));
    try
    {
      this.postuladoConcurso = 
        this.seleccionFacade.findPostuladoConcursoById(
        idPostuladoConcurso);
      if (this.postuladoConcurso.getConcursoCargo() != null) {
        this.selectConcursoCargo = 
          String.valueOf(this.postuladoConcurso.getConcursoCargo().getIdConcursoCargo());
      }

      ConcursoCargo concursoCargo = null;
      Concurso concursoForConcursoCargo = null;

      if (this.postuladoConcurso.getConcursoCargo() != null) {
        long idConcursoCargo = 
          this.postuladoConcurso.getConcursoCargo().getIdConcursoCargo();
        this.selectConcursoCargo = String.valueOf(idConcursoCargo);
        concursoCargo = this.seleccionFacade.findConcursoCargoById(
          idConcursoCargo);
        this.colConcursoCargo = this.seleccionFacade.findConcursoCargoByIdConcurso(
          concursoCargo.getConcurso().getIdConcurso());

        long idConcursoForConcursoCargo = 
          this.postuladoConcurso.getConcursoCargo().getConcurso().getIdConcurso();
        this.selectConcursoForConcursoCargo = String.valueOf(idConcursoForConcursoCargo);
        concursoForConcursoCargo = 
          this.seleccionFacade.findConcursoById(
          idConcursoForConcursoCargo);
        this.colConcursoForConcursoCargo = 
          this.seleccionFacade.findAllConcurso();
      }

      if (this.postuladoConcurso.getPersonal() != null)
      {
        this.primerApellido = this.postuladoConcurso.getPersonal().getPrimerApellido();
        this.segundoApellido = this.postuladoConcurso.getPersonal().getSegundoApellido();
        this.primerNombre = this.postuladoConcurso.getPersonal().getPrimerNombre();
        this.segundoNombre = this.postuladoConcurso.getPersonal().getSegundoNombre();
        this.cedula = this.postuladoConcurso.getPersonal().getCedula();
      }
      else if (this.postuladoConcurso.getElegible() != null)
      {
        this.primerApellido = this.postuladoConcurso.getElegible().getPrimerApellido();
        this.segundoApellido = this.postuladoConcurso.getElegible().getSegundoApellido();
        this.primerNombre = this.postuladoConcurso.getElegible().getPrimerNombre();
        this.segundoNombre = this.postuladoConcurso.getElegible().getSegundoNombre();
        this.cedula = this.postuladoConcurso.getElegible().getCedula();
      }
      else if (this.postuladoConcurso.getPostuladoExterno() != null)
      {
        this.primerApellido = this.postuladoConcurso.getPostuladoExterno().getPrimerApellido();
        this.segundoApellido = this.postuladoConcurso.getPostuladoExterno().getSegundoApellido();
        this.primerNombre = this.postuladoConcurso.getPostuladoExterno().getPrimerNombre();
        this.segundoNombre = this.postuladoConcurso.getPostuladoExterno().getSegundoNombre();
        this.cedula = this.postuladoConcurso.getPostuladoExterno().getCedula();
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.postuladoConcurso = null;
    this.showPostuladoConcursoByConcursoCargo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        if (this.personal != null) {
          this.postuladoConcurso.setPersonal(this.personal);
        }
        else if (this.elegible != null) {
          this.postuladoConcurso.setElegible(this.elegible);
        }
        else if (this.postuladoExterno != null) {
          this.postuladoConcurso.setPostuladoExterno(this.postuladoExterno);
        }
        else {
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No ha seleccionado una persona", ""));

          return "cancel";
        }
        this.seleccionFacade.addPostuladoConcurso(
          this.postuladoConcurso);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.postuladoConcurso, this.personal);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.seleccionFacade.updatePostuladoConcurso(
          this.postuladoConcurso);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.postuladoConcurso, this.personal);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deletePostuladoConcurso(
        this.postuladoConcurso);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.postuladoConcurso, this.personal);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.postuladoConcurso = new PostuladoConcurso();

    this.postuladoExterno = null;
    this.personal = null;
    this.elegible = null;
    this.primerApellido = null;
    this.segundoApellido = null;
    this.primerNombre = null;
    this.segundoNombre = null;
    this.cedula = 0;
    this.postuladoConcurso.setOrigen(null);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.postuladoConcurso.setIdPostuladoConcurso(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.PostuladoConcurso"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.postuladoConcurso = new PostuladoConcurso();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }

  public int getCedula()
  {
    return this.cedula;
  }
  public void setCedula(int cedula) {
    this.cedula = cedula;
  }
  public String getPrimerApellido() {
    return this.primerApellido;
  }
  public void setPrimerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
  }
  public String getPrimerNombre() {
    return this.primerNombre;
  }
  public void setPrimerNombre(String primerNombre) {
    this.primerNombre = primerNombre;
  }
  public String getSegundoApellido() {
    return this.segundoApellido;
  }
  public void setSegundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }
  public String getSegundoNombre() {
    return this.segundoNombre;
  }
  public void setSegundoNombre(String segundoNombre) {
    this.segundoNombre = segundoNombre;
  }
  public int getFindCedula() {
    return this.findCedula;
  }
  public void setFindCedula(int findCedula) {
    this.findCedula = findCedula;
  }
}