package sigefirrhh.planificacion.seleccion;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

public class PostuladoConcursoNoGenBeanBusiness extends PostuladoConcursoBeanBusiness
  implements Serializable
{
  public Collection findByIdConcursoCargo(long idConcursoCargo)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select id, cedula, descripcion from (");
      sql.append(" select pc.id_postulado_concurso as id, p.cedula as cedula, (p.cedula || ' - ' || p.primer_apellido || ', ' || p.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, personal p");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_personal = p.id_personal");
      sql.append(" and pc.cedula not in");
      sql.append(" (select cedula from concursocargo where estatus = '2')");
      sql.append("\tunion ");
      sql.append(" select pc.id_postulado_concurso, e.cedula as cedula, (e.cedula || ' - ' || e.primer_apellido || ', ' || e.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, elegible e");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_elegible = e.id_elegible");
      sql.append(" and pc.cedula not in");
      sql.append(" (select cedula from concursocargo where estatus = '2')");
      sql.append(" union");
      sql.append(" select pc.id_postulado_concurso, x.cedula as cedula, (x.cedula || ' - ' || x.primer_apellido || ', ' || x.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, postuladoexterno x");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_postulado_externo = x.id_postulado_externo");
      sql.append(" and pc.cedula not in");
      sql.append(" (select cedula from concursocargo where estatus = '2')");
      sql.append(" ) as postulados order by cedula");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConcursoCargo);
      stRegistros.setLong(2, idConcursoCargo);
      stRegistros.setLong(3, idConcursoCargo);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }

  public Collection findByIdConcursoCargo(long idConcursoCargo, String estatus) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select id, cedula, descripcion from (");
      sql.append(" select pc.id_postulado_concurso as id, p.cedula as cedula, (p.cedula || ' - ' || p.primer_apellido || ', ' || p.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, personal p");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_personal = p.id_personal");
      sql.append(" and pc.estatus = ?");
      sql.append("\tunion ");
      sql.append(" select pc.id_postulado_concurso, e.cedula as cedula, (e.cedula || ' - ' || e.primer_apellido || ', ' || e.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, elegible e");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_elegible = e.id_elegible");
      sql.append(" and pc.estatus = ?");
      sql.append(" union");
      sql.append(" select pc.id_postulado_concurso, x.cedula as cedula, (x.cedula || ' - ' || x.primer_apellido || ', ' || x.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, postuladoexterno x");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_postulado_externo = x.id_postulado_externo");
      sql.append(" and pc.estatus = ?");
      sql.append(" ) as postulados order by cedula");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConcursoCargo);
      stRegistros.setString(2, estatus);
      stRegistros.setLong(3, idConcursoCargo);
      stRegistros.setString(4, estatus);
      stRegistros.setLong(5, idConcursoCargo);
      stRegistros.setString(6, estatus);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }

  public Collection findByIdConcursoCargoTodos(long idConcursoCargo)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;

    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append("select id, cedula, descripcion from (");
      sql.append(" select pc.id_postulado_concurso as id, p.cedula as cedula, (p.cedula || ' - ' || p.primer_apellido || ', ' || p.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, personal p");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_personal = p.id_personal");
      sql.append("\tunion ");
      sql.append(" select pc.id_postulado_concurso, e.cedula as cedula, (e.cedula || ' - ' || e.primer_apellido || ', ' || e.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, elegible e");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_elegible = e.id_elegible");
      sql.append(" union");
      sql.append(" select pc.id_postulado_concurso, x.cedula as cedula, (x.cedula || ' - ' || x.primer_apellido || ', ' || x.primer_nombre)  as descripcion");
      sql.append(" from postuladoconcurso pc, postuladoexterno x");
      sql.append(" where pc.id_concurso_cargo = ?");
      sql.append(" and pc.id_postulado_externo = x.id_postulado_externo");
      sql.append(" ) as postulados order by cedula");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConcursoCargo);
      stRegistros.setLong(2, idConcursoCargo);
      stRegistros.setLong(3, idConcursoCargo);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next()) {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }
}