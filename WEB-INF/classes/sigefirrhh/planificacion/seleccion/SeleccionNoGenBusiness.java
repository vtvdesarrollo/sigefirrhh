package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;
import java.util.Collection;

public class SeleccionNoGenBusiness extends SeleccionBusiness
  implements Serializable
{
  private ConcursoCargoNoGenBeanBusiness concursoCargoNoGenBeanBusiness = new ConcursoCargoNoGenBeanBusiness();

  private PostuladoConcursoNoGenBeanBusiness postuladoConcursoNoGenBeanBusiness = new PostuladoConcursoNoGenBeanBusiness();

  public long validarPostuladoConcurso(int cedula)
    throws Exception
  {
    return this.concursoCargoNoGenBeanBusiness.validarPostuladoConcurso(cedula);
  }

  public Collection findPostuladoConcursoByIdConcursoCargo(long idConcursoCargo) throws Exception
  {
    return this.postuladoConcursoNoGenBeanBusiness.findByIdConcursoCargo(idConcursoCargo);
  }

  public Collection findPostuladoConcursoByIdConcursoCargo(long idConcursoCargo, String estatus) throws Exception {
    return this.postuladoConcursoNoGenBeanBusiness.findByIdConcursoCargo(idConcursoCargo, estatus);
  }

  public Collection findPostuladoConcursoByIdConcursoCargoTodos(long idConcursoCargo)
    throws Exception
  {
    return this.postuladoConcursoNoGenBeanBusiness.findByIdConcursoCargoTodos(idConcursoCargo);
  }

  public Collection findConcursoCargoByIdConcurso(long idConcurso)
    throws Exception
  {
    return this.concursoCargoNoGenBeanBusiness.findByIdConcurso(idConcurso);
  }

  public Collection findConcursoCargoByIdConcursoTodos(long idConcurso) throws Exception
  {
    return this.concursoCargoNoGenBeanBusiness.findByIdConcursoTodos(idConcurso);
  }

  public Collection findConcursoCargoByConcursoNoEstatus(long idConcurso, String estatus) throws Exception
  {
    return this.concursoCargoNoGenBeanBusiness.findByConcursoNoEstatus(idConcurso, estatus);
  }

  public Collection findConcursoCargoByConcursoTodos(long idConcurso) throws Exception
  {
    return this.concursoCargoNoGenBeanBusiness.findByConcursoTodos(idConcurso);
  }
}