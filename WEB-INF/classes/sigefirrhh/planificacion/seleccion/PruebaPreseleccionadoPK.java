package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class PruebaPreseleccionadoPK
  implements Serializable
{
  public long idPruebaPreseleccionado;

  public PruebaPreseleccionadoPK()
  {
  }

  public PruebaPreseleccionadoPK(long idPruebaPreseleccionado)
  {
    this.idPruebaPreseleccionado = idPruebaPreseleccionado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PruebaPreseleccionadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PruebaPreseleccionadoPK thatPK)
  {
    return 
      this.idPruebaPreseleccionado == thatPK.idPruebaPreseleccionado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPruebaPreseleccionado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPruebaPreseleccionado);
  }
}