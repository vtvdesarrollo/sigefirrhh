package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PruebaSeleccion
  implements Serializable, PersistenceCapable
{
  private long idPruebaSeleccion;
  private String codPrueba;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codPrueba", "idPruebaSeleccion", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public String getCodPrueba()
  {
    return jdoGetcodPrueba(this);
  }

  public void setCodPrueba(String codPrueba)
  {
    jdoSetcodPrueba(this, codPrueba);
  }

  public long getIdPruebaSeleccion()
  {
    return jdoGetidPruebaSeleccion(this);
  }

  public void setIdPruebaSeleccion(long idPruebaSeleccion)
  {
    jdoSetidPruebaSeleccion(this, idPruebaSeleccion);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PruebaSeleccion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PruebaSeleccion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PruebaSeleccion localPruebaSeleccion = new PruebaSeleccion();
    localPruebaSeleccion.jdoFlags = 1;
    localPruebaSeleccion.jdoStateManager = paramStateManager;
    return localPruebaSeleccion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PruebaSeleccion localPruebaSeleccion = new PruebaSeleccion();
    localPruebaSeleccion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPruebaSeleccion.jdoFlags = 1;
    localPruebaSeleccion.jdoStateManager = paramStateManager;
    return localPruebaSeleccion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codPrueba);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPruebaSeleccion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codPrueba = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPruebaSeleccion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PruebaSeleccion paramPruebaSeleccion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPruebaSeleccion == null)
        throw new IllegalArgumentException("arg1");
      this.codPrueba = paramPruebaSeleccion.codPrueba;
      return;
    case 1:
      if (paramPruebaSeleccion == null)
        throw new IllegalArgumentException("arg1");
      this.idPruebaSeleccion = paramPruebaSeleccion.idPruebaSeleccion;
      return;
    case 2:
      if (paramPruebaSeleccion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramPruebaSeleccion.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PruebaSeleccion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PruebaSeleccion localPruebaSeleccion = (PruebaSeleccion)paramObject;
    if (localPruebaSeleccion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPruebaSeleccion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PruebaSeleccionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PruebaSeleccionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PruebaSeleccionPK))
      throw new IllegalArgumentException("arg1");
    PruebaSeleccionPK localPruebaSeleccionPK = (PruebaSeleccionPK)paramObject;
    localPruebaSeleccionPK.idPruebaSeleccion = this.idPruebaSeleccion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PruebaSeleccionPK))
      throw new IllegalArgumentException("arg1");
    PruebaSeleccionPK localPruebaSeleccionPK = (PruebaSeleccionPK)paramObject;
    this.idPruebaSeleccion = localPruebaSeleccionPK.idPruebaSeleccion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PruebaSeleccionPK))
      throw new IllegalArgumentException("arg2");
    PruebaSeleccionPK localPruebaSeleccionPK = (PruebaSeleccionPK)paramObject;
    localPruebaSeleccionPK.idPruebaSeleccion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PruebaSeleccionPK))
      throw new IllegalArgumentException("arg2");
    PruebaSeleccionPK localPruebaSeleccionPK = (PruebaSeleccionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localPruebaSeleccionPK.idPruebaSeleccion);
  }

  private static final String jdoGetcodPrueba(PruebaSeleccion paramPruebaSeleccion)
  {
    if (paramPruebaSeleccion.jdoFlags <= 0)
      return paramPruebaSeleccion.codPrueba;
    StateManager localStateManager = paramPruebaSeleccion.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaSeleccion.codPrueba;
    if (localStateManager.isLoaded(paramPruebaSeleccion, jdoInheritedFieldCount + 0))
      return paramPruebaSeleccion.codPrueba;
    return localStateManager.getStringField(paramPruebaSeleccion, jdoInheritedFieldCount + 0, paramPruebaSeleccion.codPrueba);
  }

  private static final void jdoSetcodPrueba(PruebaSeleccion paramPruebaSeleccion, String paramString)
  {
    if (paramPruebaSeleccion.jdoFlags == 0)
    {
      paramPruebaSeleccion.codPrueba = paramString;
      return;
    }
    StateManager localStateManager = paramPruebaSeleccion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaSeleccion.codPrueba = paramString;
      return;
    }
    localStateManager.setStringField(paramPruebaSeleccion, jdoInheritedFieldCount + 0, paramPruebaSeleccion.codPrueba, paramString);
  }

  private static final long jdoGetidPruebaSeleccion(PruebaSeleccion paramPruebaSeleccion)
  {
    return paramPruebaSeleccion.idPruebaSeleccion;
  }

  private static final void jdoSetidPruebaSeleccion(PruebaSeleccion paramPruebaSeleccion, long paramLong)
  {
    StateManager localStateManager = paramPruebaSeleccion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaSeleccion.idPruebaSeleccion = paramLong;
      return;
    }
    localStateManager.setLongField(paramPruebaSeleccion, jdoInheritedFieldCount + 1, paramPruebaSeleccion.idPruebaSeleccion, paramLong);
  }

  private static final String jdoGetnombre(PruebaSeleccion paramPruebaSeleccion)
  {
    if (paramPruebaSeleccion.jdoFlags <= 0)
      return paramPruebaSeleccion.nombre;
    StateManager localStateManager = paramPruebaSeleccion.jdoStateManager;
    if (localStateManager == null)
      return paramPruebaSeleccion.nombre;
    if (localStateManager.isLoaded(paramPruebaSeleccion, jdoInheritedFieldCount + 2))
      return paramPruebaSeleccion.nombre;
    return localStateManager.getStringField(paramPruebaSeleccion, jdoInheritedFieldCount + 2, paramPruebaSeleccion.nombre);
  }

  private static final void jdoSetnombre(PruebaSeleccion paramPruebaSeleccion, String paramString)
  {
    if (paramPruebaSeleccion.jdoFlags == 0)
    {
      paramPruebaSeleccion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramPruebaSeleccion.jdoStateManager;
    if (localStateManager == null)
    {
      paramPruebaSeleccion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPruebaSeleccion, jdoInheritedFieldCount + 2, paramPruebaSeleccion.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}