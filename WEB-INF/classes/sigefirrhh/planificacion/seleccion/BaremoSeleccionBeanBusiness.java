package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class BaremoSeleccionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addBaremoSeleccion(BaremoSeleccion baremoSeleccion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    BaremoSeleccion baremoSeleccionNew = 
      (BaremoSeleccion)BeanUtils.cloneBean(
      baremoSeleccion);

    pm.makePersistent(baremoSeleccionNew);
  }

  public void updateBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception
  {
    BaremoSeleccion baremoSeleccionModify = 
      findBaremoSeleccionById(baremoSeleccion.getIdBaremoSeleccion());

    BeanUtils.copyProperties(baremoSeleccionModify, baremoSeleccion);
  }

  public void deleteBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    BaremoSeleccion baremoSeleccionDelete = 
      findBaremoSeleccionById(baremoSeleccion.getIdBaremoSeleccion());
    pm.deletePersistent(baremoSeleccionDelete);
  }

  public BaremoSeleccion findBaremoSeleccionById(long idBaremoSeleccion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idBaremoSeleccion == pIdBaremoSeleccion";
    Query query = pm.newQuery(BaremoSeleccion.class, filter);

    query.declareParameters("long pIdBaremoSeleccion");

    parameters.put("pIdBaremoSeleccion", new Long(idBaremoSeleccion));

    Collection colBaremoSeleccion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colBaremoSeleccion.iterator();
    return (BaremoSeleccion)iterator.next();
  }

  public Collection findBaremoSeleccionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent baremoSeleccionExtent = pm.getExtent(
      BaremoSeleccion.class, true);
    Query query = pm.newQuery(baremoSeleccionExtent);
    query.setOrdering("codBaremo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodBaremo(String codBaremo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codBaremo == pCodBaremo";

    Query query = pm.newQuery(BaremoSeleccion.class, filter);

    query.declareParameters("java.lang.String pCodBaremo");
    HashMap parameters = new HashMap();

    parameters.put("pCodBaremo", new String(codBaremo));

    query.setOrdering("codBaremo ascending");

    Collection colBaremoSeleccion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBaremoSeleccion);

    return colBaremoSeleccion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(BaremoSeleccion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codBaremo ascending");

    Collection colBaremoSeleccion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBaremoSeleccion);

    return colBaremoSeleccion;
  }
}