package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class BaremoPreseleccionado
  implements Serializable, PersistenceCapable
{
  private long idBaremoPreseleccionado;
  private PostuladoConcurso postuladoConcurso;
  private Varemos varemos;
  private float resultado;
  private String entrevistador;
  private Date fecha;
  private String observaciones;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "entrevistador", "fecha", "idBaremoPreseleccionado", "observaciones", "postuladoConcurso", "resultado", "varemos" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PostuladoConcurso"), Float.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.seleccion.Varemos") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetresultado(this));

    return jdoGetvaremos(this).getNombre() + " - " + a;
  }

  public String getEntrevistador() {
    return jdoGetentrevistador(this);
  }

  public void setEntrevistador(String entrevistador)
  {
    jdoSetentrevistador(this, entrevistador);
  }

  public Date getFecha()
  {
    return jdoGetfecha(this);
  }

  public void setFecha(Date fecha)
  {
    jdoSetfecha(this, fecha);
  }

  public long getIdBaremoPreseleccionado()
  {
    return jdoGetidBaremoPreseleccionado(this);
  }

  public void setIdBaremoPreseleccionado(long idBaremoPreseleccionado)
  {
    jdoSetidBaremoPreseleccionado(this, idBaremoPreseleccionado);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones)
  {
    jdoSetobservaciones(this, observaciones);
  }

  public PostuladoConcurso getPostuladoConcurso()
  {
    return jdoGetpostuladoConcurso(this);
  }

  public void setPostuladoConcurso(PostuladoConcurso postuladoConcurso)
  {
    jdoSetpostuladoConcurso(this, postuladoConcurso);
  }

  public float getResultado()
  {
    return jdoGetresultado(this);
  }

  public void setResultado(float resultado)
  {
    jdoSetresultado(this, resultado);
  }

  public Varemos getVaremos()
  {
    return jdoGetvaremos(this);
  }
  public void setVaremos(Varemos varemos) {
    jdoSetvaremos(this, varemos);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.BaremoPreseleccionado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new BaremoPreseleccionado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    BaremoPreseleccionado localBaremoPreseleccionado = new BaremoPreseleccionado();
    localBaremoPreseleccionado.jdoFlags = 1;
    localBaremoPreseleccionado.jdoStateManager = paramStateManager;
    return localBaremoPreseleccionado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    BaremoPreseleccionado localBaremoPreseleccionado = new BaremoPreseleccionado();
    localBaremoPreseleccionado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localBaremoPreseleccionado.jdoFlags = 1;
    localBaremoPreseleccionado.jdoStateManager = paramStateManager;
    return localBaremoPreseleccionado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.entrevistador);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idBaremoPreseleccionado);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.postuladoConcurso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedFloatField(this, paramInt, this.resultado);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.varemos);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.entrevistador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idBaremoPreseleccionado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.postuladoConcurso = ((PostuladoConcurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultado = localStateManager.replacingFloatField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.varemos = ((Varemos)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(BaremoPreseleccionado paramBaremoPreseleccionado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramBaremoPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.entrevistador = paramBaremoPreseleccionado.entrevistador;
      return;
    case 1:
      if (paramBaremoPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramBaremoPreseleccionado.fecha;
      return;
    case 2:
      if (paramBaremoPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.idBaremoPreseleccionado = paramBaremoPreseleccionado.idBaremoPreseleccionado;
      return;
    case 3:
      if (paramBaremoPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramBaremoPreseleccionado.observaciones;
      return;
    case 4:
      if (paramBaremoPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.postuladoConcurso = paramBaremoPreseleccionado.postuladoConcurso;
      return;
    case 5:
      if (paramBaremoPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.resultado = paramBaremoPreseleccionado.resultado;
      return;
    case 6:
      if (paramBaremoPreseleccionado == null)
        throw new IllegalArgumentException("arg1");
      this.varemos = paramBaremoPreseleccionado.varemos;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof BaremoPreseleccionado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    BaremoPreseleccionado localBaremoPreseleccionado = (BaremoPreseleccionado)paramObject;
    if (localBaremoPreseleccionado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localBaremoPreseleccionado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new BaremoPreseleccionadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new BaremoPreseleccionadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BaremoPreseleccionadoPK))
      throw new IllegalArgumentException("arg1");
    BaremoPreseleccionadoPK localBaremoPreseleccionadoPK = (BaremoPreseleccionadoPK)paramObject;
    localBaremoPreseleccionadoPK.idBaremoPreseleccionado = this.idBaremoPreseleccionado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BaremoPreseleccionadoPK))
      throw new IllegalArgumentException("arg1");
    BaremoPreseleccionadoPK localBaremoPreseleccionadoPK = (BaremoPreseleccionadoPK)paramObject;
    this.idBaremoPreseleccionado = localBaremoPreseleccionadoPK.idBaremoPreseleccionado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BaremoPreseleccionadoPK))
      throw new IllegalArgumentException("arg2");
    BaremoPreseleccionadoPK localBaremoPreseleccionadoPK = (BaremoPreseleccionadoPK)paramObject;
    localBaremoPreseleccionadoPK.idBaremoPreseleccionado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BaremoPreseleccionadoPK))
      throw new IllegalArgumentException("arg2");
    BaremoPreseleccionadoPK localBaremoPreseleccionadoPK = (BaremoPreseleccionadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localBaremoPreseleccionadoPK.idBaremoPreseleccionado);
  }

  private static final String jdoGetentrevistador(BaremoPreseleccionado paramBaremoPreseleccionado)
  {
    if (paramBaremoPreseleccionado.jdoFlags <= 0)
      return paramBaremoPreseleccionado.entrevistador;
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoPreseleccionado.entrevistador;
    if (localStateManager.isLoaded(paramBaremoPreseleccionado, jdoInheritedFieldCount + 0))
      return paramBaremoPreseleccionado.entrevistador;
    return localStateManager.getStringField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 0, paramBaremoPreseleccionado.entrevistador);
  }

  private static final void jdoSetentrevistador(BaremoPreseleccionado paramBaremoPreseleccionado, String paramString)
  {
    if (paramBaremoPreseleccionado.jdoFlags == 0)
    {
      paramBaremoPreseleccionado.entrevistador = paramString;
      return;
    }
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoPreseleccionado.entrevistador = paramString;
      return;
    }
    localStateManager.setStringField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 0, paramBaremoPreseleccionado.entrevistador, paramString);
  }

  private static final Date jdoGetfecha(BaremoPreseleccionado paramBaremoPreseleccionado)
  {
    if (paramBaremoPreseleccionado.jdoFlags <= 0)
      return paramBaremoPreseleccionado.fecha;
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoPreseleccionado.fecha;
    if (localStateManager.isLoaded(paramBaremoPreseleccionado, jdoInheritedFieldCount + 1))
      return paramBaremoPreseleccionado.fecha;
    return (Date)localStateManager.getObjectField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 1, paramBaremoPreseleccionado.fecha);
  }

  private static final void jdoSetfecha(BaremoPreseleccionado paramBaremoPreseleccionado, Date paramDate)
  {
    if (paramBaremoPreseleccionado.jdoFlags == 0)
    {
      paramBaremoPreseleccionado.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoPreseleccionado.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 1, paramBaremoPreseleccionado.fecha, paramDate);
  }

  private static final long jdoGetidBaremoPreseleccionado(BaremoPreseleccionado paramBaremoPreseleccionado)
  {
    return paramBaremoPreseleccionado.idBaremoPreseleccionado;
  }

  private static final void jdoSetidBaremoPreseleccionado(BaremoPreseleccionado paramBaremoPreseleccionado, long paramLong)
  {
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoPreseleccionado.idBaremoPreseleccionado = paramLong;
      return;
    }
    localStateManager.setLongField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 2, paramBaremoPreseleccionado.idBaremoPreseleccionado, paramLong);
  }

  private static final String jdoGetobservaciones(BaremoPreseleccionado paramBaremoPreseleccionado)
  {
    if (paramBaremoPreseleccionado.jdoFlags <= 0)
      return paramBaremoPreseleccionado.observaciones;
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoPreseleccionado.observaciones;
    if (localStateManager.isLoaded(paramBaremoPreseleccionado, jdoInheritedFieldCount + 3))
      return paramBaremoPreseleccionado.observaciones;
    return localStateManager.getStringField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 3, paramBaremoPreseleccionado.observaciones);
  }

  private static final void jdoSetobservaciones(BaremoPreseleccionado paramBaremoPreseleccionado, String paramString)
  {
    if (paramBaremoPreseleccionado.jdoFlags == 0)
    {
      paramBaremoPreseleccionado.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoPreseleccionado.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 3, paramBaremoPreseleccionado.observaciones, paramString);
  }

  private static final PostuladoConcurso jdoGetpostuladoConcurso(BaremoPreseleccionado paramBaremoPreseleccionado)
  {
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoPreseleccionado.postuladoConcurso;
    if (localStateManager.isLoaded(paramBaremoPreseleccionado, jdoInheritedFieldCount + 4))
      return paramBaremoPreseleccionado.postuladoConcurso;
    return (PostuladoConcurso)localStateManager.getObjectField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 4, paramBaremoPreseleccionado.postuladoConcurso);
  }

  private static final void jdoSetpostuladoConcurso(BaremoPreseleccionado paramBaremoPreseleccionado, PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoPreseleccionado.postuladoConcurso = paramPostuladoConcurso;
      return;
    }
    localStateManager.setObjectField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 4, paramBaremoPreseleccionado.postuladoConcurso, paramPostuladoConcurso);
  }

  private static final float jdoGetresultado(BaremoPreseleccionado paramBaremoPreseleccionado)
  {
    if (paramBaremoPreseleccionado.jdoFlags <= 0)
      return paramBaremoPreseleccionado.resultado;
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoPreseleccionado.resultado;
    if (localStateManager.isLoaded(paramBaremoPreseleccionado, jdoInheritedFieldCount + 5))
      return paramBaremoPreseleccionado.resultado;
    return localStateManager.getFloatField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 5, paramBaremoPreseleccionado.resultado);
  }

  private static final void jdoSetresultado(BaremoPreseleccionado paramBaremoPreseleccionado, float paramFloat)
  {
    if (paramBaremoPreseleccionado.jdoFlags == 0)
    {
      paramBaremoPreseleccionado.resultado = paramFloat;
      return;
    }
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoPreseleccionado.resultado = paramFloat;
      return;
    }
    localStateManager.setFloatField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 5, paramBaremoPreseleccionado.resultado, paramFloat);
  }

  private static final Varemos jdoGetvaremos(BaremoPreseleccionado paramBaremoPreseleccionado)
  {
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoPreseleccionado.varemos;
    if (localStateManager.isLoaded(paramBaremoPreseleccionado, jdoInheritedFieldCount + 6))
      return paramBaremoPreseleccionado.varemos;
    return (Varemos)localStateManager.getObjectField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 6, paramBaremoPreseleccionado.varemos);
  }

  private static final void jdoSetvaremos(BaremoPreseleccionado paramBaremoPreseleccionado, Varemos paramVaremos)
  {
    StateManager localStateManager = paramBaremoPreseleccionado.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoPreseleccionado.varemos = paramVaremos;
      return;
    }
    localStateManager.setObjectField(paramBaremoPreseleccionado, jdoInheritedFieldCount + 6, paramBaremoPreseleccionado.varemos, paramVaremos);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}