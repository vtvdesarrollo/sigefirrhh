package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class PostuladoExterno
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTADO_CIVIL;
  protected static final Map LISTA_SEXO;
  protected static final Map LISTA_NACIONALIDAD;
  private long idPostuladoExterno;
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  private String sexo;
  private Date fechaNacimiento;
  private String estadoCivil;
  private String nacionalidad;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cedula", "estadoCivil", "fechaNacimiento", "idPostuladoExterno", "nacionalidad", "primerApellido", "primerNombre", "segundoApellido", "segundoNombre", "sexo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PostuladoExterno"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PostuladoExterno());

    LISTA_ESTADO_CIVIL = 
      new LinkedHashMap();
    LISTA_SEXO = 
      new LinkedHashMap();
    LISTA_NACIONALIDAD = 
      new LinkedHashMap();

    LISTA_ESTADO_CIVIL.put("S", "SOLTERO(A)");
    LISTA_ESTADO_CIVIL.put("C", "CASADO(A)");
    LISTA_ESTADO_CIVIL.put("D", "DIVORCIADO(A)");
    LISTA_ESTADO_CIVIL.put("V", "VIUDO(A)");
    LISTA_ESTADO_CIVIL.put("U", "UNIDO(A)");
    LISTA_ESTADO_CIVIL.put("O", "OTRO");
    LISTA_SEXO.put("F", "FEMENINO");
    LISTA_SEXO.put("M", "MASCULINO");
    LISTA_NACIONALIDAD.put("V", "VENEZOLANO(A)");
    LISTA_NACIONALIDAD.put("E", "EXTRANJERO(A)");
  }

  public PostuladoExterno()
  {
    jdoSetestadoCivil(this, "S");

    jdoSetnacionalidad(this, "V");
  }
  public String toString() {
    return jdoGetprimerApellido(this) + " " + 
      jdoGetsegundoApellido(this) + ", " + 
      jdoGetprimerNombre(this) + " " + 
      jdoGetsegundoNombre(this) + "  -  " + 
      jdoGetcedula(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }
  public String getEstadoCivil() {
    return jdoGetestadoCivil(this);
  }
  public void setEstadoCivil(String estadoCivil) {
    jdoSetestadoCivil(this, estadoCivil);
  }
  public Date getFechaNacimiento() {
    return jdoGetfechaNacimiento(this);
  }
  public void setFechaNacimiento(Date fechaNacimiento) {
    jdoSetfechaNacimiento(this, fechaNacimiento);
  }
  public long getIdPostuladoExterno() {
    return jdoGetidPostuladoExterno(this);
  }
  public void setIdPostuladoExterno(long idPostuladoExterno) {
    jdoSetidPostuladoExterno(this, idPostuladoExterno);
  }
  public String getNacionalidad() {
    return jdoGetnacionalidad(this);
  }
  public void setNacionalidad(String nacionalidad) {
    jdoSetnacionalidad(this, nacionalidad);
  }
  public String getPrimerApellido() {
    return jdoGetprimerApellido(this);
  }
  public void setPrimerApellido(String primerApellido) {
    jdoSetprimerApellido(this, primerApellido);
  }
  public String getPrimerNombre() {
    return jdoGetprimerNombre(this);
  }
  public void setPrimerNombre(String primerNombre) {
    jdoSetprimerNombre(this, primerNombre);
  }
  public String getSegundoApellido() {
    return jdoGetsegundoApellido(this);
  }
  public void setSegundoApellido(String segundoApellido) {
    jdoSetsegundoApellido(this, segundoApellido);
  }
  public String getSegundoNombre() {
    return jdoGetsegundoNombre(this);
  }
  public void setSegundoNombre(String segundoNombre) {
    jdoSetsegundoNombre(this, segundoNombre);
  }
  public String getSexo() {
    return jdoGetsexo(this);
  }
  public void setSexo(String sexo) {
    jdoSetsexo(this, sexo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PostuladoExterno localPostuladoExterno = new PostuladoExterno();
    localPostuladoExterno.jdoFlags = 1;
    localPostuladoExterno.jdoStateManager = paramStateManager;
    return localPostuladoExterno;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PostuladoExterno localPostuladoExterno = new PostuladoExterno();
    localPostuladoExterno.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPostuladoExterno.jdoFlags = 1;
    localPostuladoExterno.jdoStateManager = paramStateManager;
    return localPostuladoExterno;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estadoCivil);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaNacimiento);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPostuladoExterno);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nacionalidad);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.sexo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estadoCivil = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaNacimiento = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPostuladoExterno = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nacionalidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.sexo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PostuladoExterno paramPostuladoExterno, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramPostuladoExterno.cedula;
      return;
    case 1:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.estadoCivil = paramPostuladoExterno.estadoCivil;
      return;
    case 2:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.fechaNacimiento = paramPostuladoExterno.fechaNacimiento;
      return;
    case 3:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.idPostuladoExterno = paramPostuladoExterno.idPostuladoExterno;
      return;
    case 4:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.nacionalidad = paramPostuladoExterno.nacionalidad;
      return;
    case 5:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramPostuladoExterno.primerApellido;
      return;
    case 6:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramPostuladoExterno.primerNombre;
      return;
    case 7:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramPostuladoExterno.segundoApellido;
      return;
    case 8:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramPostuladoExterno.segundoNombre;
      return;
    case 9:
      if (paramPostuladoExterno == null)
        throw new IllegalArgumentException("arg1");
      this.sexo = paramPostuladoExterno.sexo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PostuladoExterno))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PostuladoExterno localPostuladoExterno = (PostuladoExterno)paramObject;
    if (localPostuladoExterno.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPostuladoExterno, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PostuladoExternoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PostuladoExternoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PostuladoExternoPK))
      throw new IllegalArgumentException("arg1");
    PostuladoExternoPK localPostuladoExternoPK = (PostuladoExternoPK)paramObject;
    localPostuladoExternoPK.idPostuladoExterno = this.idPostuladoExterno;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PostuladoExternoPK))
      throw new IllegalArgumentException("arg1");
    PostuladoExternoPK localPostuladoExternoPK = (PostuladoExternoPK)paramObject;
    this.idPostuladoExterno = localPostuladoExternoPK.idPostuladoExterno;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PostuladoExternoPK))
      throw new IllegalArgumentException("arg2");
    PostuladoExternoPK localPostuladoExternoPK = (PostuladoExternoPK)paramObject;
    localPostuladoExternoPK.idPostuladoExterno = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PostuladoExternoPK))
      throw new IllegalArgumentException("arg2");
    PostuladoExternoPK localPostuladoExternoPK = (PostuladoExternoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localPostuladoExternoPK.idPostuladoExterno);
  }

  private static final int jdoGetcedula(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.cedula;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.cedula;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 0))
      return paramPostuladoExterno.cedula;
    return localStateManager.getIntField(paramPostuladoExterno, jdoInheritedFieldCount + 0, paramPostuladoExterno.cedula);
  }

  private static final void jdoSetcedula(PostuladoExterno paramPostuladoExterno, int paramInt)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramPostuladoExterno, jdoInheritedFieldCount + 0, paramPostuladoExterno.cedula, paramInt);
  }

  private static final String jdoGetestadoCivil(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.estadoCivil;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.estadoCivil;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 1))
      return paramPostuladoExterno.estadoCivil;
    return localStateManager.getStringField(paramPostuladoExterno, jdoInheritedFieldCount + 1, paramPostuladoExterno.estadoCivil);
  }

  private static final void jdoSetestadoCivil(PostuladoExterno paramPostuladoExterno, String paramString)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.estadoCivil = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.estadoCivil = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoExterno, jdoInheritedFieldCount + 1, paramPostuladoExterno.estadoCivil, paramString);
  }

  private static final Date jdoGetfechaNacimiento(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.fechaNacimiento;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.fechaNacimiento;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 2))
      return paramPostuladoExterno.fechaNacimiento;
    return (Date)localStateManager.getObjectField(paramPostuladoExterno, jdoInheritedFieldCount + 2, paramPostuladoExterno.fechaNacimiento);
  }

  private static final void jdoSetfechaNacimiento(PostuladoExterno paramPostuladoExterno, Date paramDate)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.fechaNacimiento = paramDate;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.fechaNacimiento = paramDate;
      return;
    }
    localStateManager.setObjectField(paramPostuladoExterno, jdoInheritedFieldCount + 2, paramPostuladoExterno.fechaNacimiento, paramDate);
  }

  private static final long jdoGetidPostuladoExterno(PostuladoExterno paramPostuladoExterno)
  {
    return paramPostuladoExterno.idPostuladoExterno;
  }

  private static final void jdoSetidPostuladoExterno(PostuladoExterno paramPostuladoExterno, long paramLong)
  {
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.idPostuladoExterno = paramLong;
      return;
    }
    localStateManager.setLongField(paramPostuladoExterno, jdoInheritedFieldCount + 3, paramPostuladoExterno.idPostuladoExterno, paramLong);
  }

  private static final String jdoGetnacionalidad(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.nacionalidad;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.nacionalidad;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 4))
      return paramPostuladoExterno.nacionalidad;
    return localStateManager.getStringField(paramPostuladoExterno, jdoInheritedFieldCount + 4, paramPostuladoExterno.nacionalidad);
  }

  private static final void jdoSetnacionalidad(PostuladoExterno paramPostuladoExterno, String paramString)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.nacionalidad = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.nacionalidad = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoExterno, jdoInheritedFieldCount + 4, paramPostuladoExterno.nacionalidad, paramString);
  }

  private static final String jdoGetprimerApellido(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.primerApellido;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.primerApellido;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 5))
      return paramPostuladoExterno.primerApellido;
    return localStateManager.getStringField(paramPostuladoExterno, jdoInheritedFieldCount + 5, paramPostuladoExterno.primerApellido);
  }

  private static final void jdoSetprimerApellido(PostuladoExterno paramPostuladoExterno, String paramString)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoExterno, jdoInheritedFieldCount + 5, paramPostuladoExterno.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.primerNombre;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.primerNombre;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 6))
      return paramPostuladoExterno.primerNombre;
    return localStateManager.getStringField(paramPostuladoExterno, jdoInheritedFieldCount + 6, paramPostuladoExterno.primerNombre);
  }

  private static final void jdoSetprimerNombre(PostuladoExterno paramPostuladoExterno, String paramString)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoExterno, jdoInheritedFieldCount + 6, paramPostuladoExterno.primerNombre, paramString);
  }

  private static final String jdoGetsegundoApellido(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.segundoApellido;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.segundoApellido;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 7))
      return paramPostuladoExterno.segundoApellido;
    return localStateManager.getStringField(paramPostuladoExterno, jdoInheritedFieldCount + 7, paramPostuladoExterno.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(PostuladoExterno paramPostuladoExterno, String paramString)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoExterno, jdoInheritedFieldCount + 7, paramPostuladoExterno.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.segundoNombre;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.segundoNombre;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 8))
      return paramPostuladoExterno.segundoNombre;
    return localStateManager.getStringField(paramPostuladoExterno, jdoInheritedFieldCount + 8, paramPostuladoExterno.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(PostuladoExterno paramPostuladoExterno, String paramString)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoExterno, jdoInheritedFieldCount + 8, paramPostuladoExterno.segundoNombre, paramString);
  }

  private static final String jdoGetsexo(PostuladoExterno paramPostuladoExterno)
  {
    if (paramPostuladoExterno.jdoFlags <= 0)
      return paramPostuladoExterno.sexo;
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoExterno.sexo;
    if (localStateManager.isLoaded(paramPostuladoExterno, jdoInheritedFieldCount + 9))
      return paramPostuladoExterno.sexo;
    return localStateManager.getStringField(paramPostuladoExterno, jdoInheritedFieldCount + 9, paramPostuladoExterno.sexo);
  }

  private static final void jdoSetsexo(PostuladoExterno paramPostuladoExterno, String paramString)
  {
    if (paramPostuladoExterno.jdoFlags == 0)
    {
      paramPostuladoExterno.sexo = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoExterno.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoExterno.sexo = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoExterno, jdoInheritedFieldCount + 9, paramPostuladoExterno.sexo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}