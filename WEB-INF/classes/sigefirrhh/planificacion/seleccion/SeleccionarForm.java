package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.planificacion.elegible.Elegible;
import sigefirrhh.sistema.RegistrarAuditoria;

public class SeleccionarForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeleccionarForm.class.getName());
  private ConcursoCargo concursoCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionNoGenFacade seleccionFacade = new SeleccionNoGenFacade();
  private boolean showConcursoCargoByConcurso;
  private String findSelectConcurso;
  private Collection findColConcurso;
  private Collection colConcurso;
  private Collection colConcursoCargo;
  private Collection colPostuladoConcurso;
  private String selectConcurso;
  private String selectConcursoCargo;
  private Collection findColConcursoCargo;
  private String findSelectConcursoCargo;
  private String selectPostuladoConcurso;
  private int resultado = 0;
  private String observacion;
  private String estatus = "0";

  public boolean isSeleccionado()
  {
    if ((this.concursoCargo != null) && (!this.concursoCargo.getEstatus().equals("1")))
    {
      return true;
    }

    return false;
  }
  public boolean isShowConcursoCargo() {
    return (this.colConcursoCargo != null) && (!this.colConcursoCargo.isEmpty());
  }
  public boolean isShowPostuladoConcurso() {
    return (this.colPostuladoConcurso != null) && (!this.colPostuladoConcurso.isEmpty());
  }
  public boolean isFindShowConcursoCargo() {
    return (this.findColConcursoCargo != null) && (!this.findColConcursoCargo.isEmpty());
  }

  public String getFindSelectConcurso() {
    return this.findSelectConcurso;
  }
  public void setFindSelectConcurso(String valConcurso) {
    this.findSelectConcurso = valConcurso;
  }
  public void findChangeConcursoForConcursoCargo(ValueChangeEvent event) {
    long idConcurso = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColConcursoCargo = null;
      if (idConcurso > 0L)
        this.findColConcursoCargo = 
          this.seleccionFacade.findConcursoCargoByIdConcursoTodos(
          idConcurso);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getFindColConcursoCargo() { Collection col = new ArrayList();
    Iterator iterator = this.findColConcursoCargo.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col; }

  public Collection getFindColConcurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConcurso.iterator();
    Concurso concurso = null;
    while (iterator.hasNext()) {
      concurso = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concurso.getIdConcurso()), 
        concurso.toString()));
    }
    return col;
  }

  public String getSelectConcurso()
  {
    return this.selectConcurso;
  }
  public void setSelectConcurso(String valConcurso) {
    Iterator iterator = this.colConcurso.iterator();
    Concurso concurso = null;
    this.concursoCargo.setConcurso(null);
    while (iterator.hasNext()) {
      concurso = (Concurso)iterator.next();
      if (String.valueOf(concurso.getIdConcurso()).equals(
        valConcurso)) {
        this.concursoCargo.setConcurso(
          concurso);
        break;
      }
    }
    this.selectConcurso = valConcurso;
  }

  public Collection getColConcursoCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcursoCargo.iterator();
    ConcursoCargo concursoCargo = null;
    while (iterator.hasNext()) {
      concursoCargo = (ConcursoCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concursoCargo.getIdConcursoCargo()), 
        concursoCargo.toString()));
    }
    return col;
  }

  public void changeConcurso(ValueChangeEvent event) {
    long idConcurso = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (idConcurso > 0L)
        this.colConcursoCargo = this.seleccionFacade.findConcursoCargoByConcursoTodos(idConcurso);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectPostuladoConcurso = null;
      this.concursoCargo.setPostuladoConcurso(
        null);
    }
  }

  public void changeConcursoCargo(ValueChangeEvent event) { long idConcursoCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (idConcursoCargo > 0L)
      {
        this.colPostuladoConcurso = this.seleccionFacade.findPostuladoConcursoByIdConcursoCargoTodos(idConcursoCargo);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectPostuladoConcurso = null;
      this.concursoCargo.setPostuladoConcurso(
        null);
    } }

  public String getSelectPostuladoConcurso()
  {
    return this.selectPostuladoConcurso;
  }
  public void setSelectPostuladoConcurso(String valPostuladoConcurso) {
    Iterator iterator = this.colPostuladoConcurso.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      String.valueOf(id).equals(
        valPostuladoConcurso);
    }

    this.selectPostuladoConcurso = valPostuladoConcurso;
  }

  public Collection getResult() {
    return this.result;
  }

  public ConcursoCargo getConcursoCargo() {
    if (this.concursoCargo == null) {
      this.concursoCargo = new ConcursoCargo();
    }
    return this.concursoCargo;
  }

  public SeleccionarForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColConcurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcurso.iterator();
    Concurso concurso = null;
    while (iterator.hasNext()) {
      concurso = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concurso.getIdConcurso()), 
        concurso.toString()));
    }
    return col;
  }

  public Collection getColPostuladoConcurso() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPostuladoConcurso.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColConcurso = 
        this.seleccionFacade.findConcursoTodos(this.login.getIdOrganismo());

      this.colConcurso = 
        this.seleccionFacade.findConcursoTodos(this.login.getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPostuladoConcursoByConcursoCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findPostuladoConcursoByConcursoCargo(Long.valueOf(this.findSelectConcursoCargo).longValue());
      this.showConcursoCargoByConcurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConcursoCargoByConcurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectConcurso = null;

    return null;
  }

  public boolean isShowConcursoCargoByConcurso() {
    return this.showConcursoCargoByConcurso;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;

    this.showConcursoCargoByConcurso = false;
    this.colPostuladoConcurso = null;
    this.colConcursoCargo = null;
    this.selectConcursoCargo = null;
    this.selectConcurso = null;
    this.selectPostuladoConcurso = null;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }

    if (this.estatus.equals("0")) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Dede seleccionar la situación final del Postulado ", ""));
      return null;
    }
    try
    {
      if (this.adding) {
        PostuladoConcurso postuladoConcurso = this.seleccionFacade.findPostuladoConcursoById(Long.valueOf(this.selectPostuladoConcurso).longValue());

        this.concursoCargo = this.seleccionFacade.findConcursoCargoById(Long.valueOf(this.selectConcursoCargo).longValue());
        this.concursoCargo.setPostuladoConcurso(postuladoConcurso);

        if ((postuladoConcurso.getEstatus().equals("2")) && (!this.estatus.equals("2"))) {
          this.concursoCargo.setCedula(this.concursoCargo.getPostuladoConcurso().getCedula());
          this.concursoCargo.setEstatus("1");
          this.seleccionFacade.updateConcursoCargo(
            this.concursoCargo);
        }

        postuladoConcurso.setEstatus(this.estatus);
        postuladoConcurso.setResultado(this.resultado);
        postuladoConcurso.setObservacion(this.observacion);

        this.seleccionFacade.updatePostuladoConcurso(postuladoConcurso);

        this.concursoCargo = this.seleccionFacade.findConcursoCargoById(Long.valueOf(this.selectConcursoCargo).longValue());
        this.concursoCargo.setPostuladoConcurso(postuladoConcurso);

        if (this.estatus.equals("2")) {
          this.concursoCargo.setCedula(this.concursoCargo.getPostuladoConcurso().getCedula());
          this.concursoCargo.setEstatus(this.estatus);
          this.seleccionFacade.updateConcursoCargo(
            this.concursoCargo);
        }

        ExpedienteFacade expedienteFacade = new ExpedienteFacade();
        IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
        Personal personal = new Personal();

        if (this.concursoCargo.getPostuladoConcurso().getPersonal() == null) {
          if (this.concursoCargo.getPostuladoConcurso().getElegible() == null) {
            try {
              personal.setCedula(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getCedula());
              personal.setPrimerApellido(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getPrimerApellido());
              personal.setSegundoApellido(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getSegundoApellido());
              personal.setPrimerNombre(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getPrimerNombre());
              personal.setSegundoNombre(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getSegundoNombre());
              personal.setEstadoCivil(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getEstadoCivil());
              personal.setNacionalidad(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getNacionalidad());
              personal.setSexo(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getSexo());
              personal.setFechaNacimiento(this.concursoCargo.getPostuladoConcurso().getPostuladoExterno().getFechaNacimiento());
              personal.setIdPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Personal"));
              expedienteFacade.addPersonal(personal, this.login.getIdOrganismo());
              log.error("grabó el registro en personal");
            } catch (Exception localException1) {
            }
          }
          else {
            personal.setCedula(this.concursoCargo.getPostuladoConcurso().getElegible().getCedula());
            personal.setPrimerApellido(this.concursoCargo.getPostuladoConcurso().getElegible().getPrimerApellido());
            personal.setSegundoApellido(this.concursoCargo.getPostuladoConcurso().getElegible().getSegundoApellido());
            personal.setPrimerNombre(this.concursoCargo.getPostuladoConcurso().getElegible().getPrimerNombre());
            personal.setSegundoNombre(this.concursoCargo.getPostuladoConcurso().getElegible().getSegundoNombre());
            personal.setEstadoCivil(this.concursoCargo.getPostuladoConcurso().getElegible().getEstadoCivil());
            personal.setNacionalidad(this.concursoCargo.getPostuladoConcurso().getElegible().getNacionalidad());
            personal.setSexo(this.concursoCargo.getPostuladoConcurso().getElegible().getSexo());
            personal.setFechaNacimiento(this.concursoCargo.getPostuladoConcurso().getElegible().getFechaNacimiento());
            personal.setIdPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.personal.expediente.Personal"));
            expedienteFacade.addPersonal(personal, this.login.getIdOrganismo());
            log.error("grabó el registro en personal");
          }

        }

        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.concursoCargo);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      }

      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      PostuladoConcurso postuladoConcurso = this.seleccionFacade.findPostuladoConcursoById(Long.valueOf(this.selectPostuladoConcurso).longValue());
      postuladoConcurso.setEstatus("0");
      this.seleccionFacade.updatePostuladoConcurso(postuladoConcurso);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', postuladoConcurso);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.concursoCargo = new ConcursoCargo();

    this.selectPostuladoConcurso = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.concursoCargo.setIdConcursoCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.ConcursoCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.concursoCargo = new ConcursoCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
  public String getSelectConcursoCargo() {
    return this.selectConcursoCargo;
  }
  public void setSelectConcursoCargo(String selectConcursoCargo) {
    this.selectConcursoCargo = selectConcursoCargo;
  }
  public String getFindSelectConcursoCargo() {
    return this.findSelectConcursoCargo;
  }
  public void setFindSelectConcursoCargo(String findSelectConcursoCargo) {
    this.findSelectConcursoCargo = findSelectConcursoCargo;
  }
  public int getResultado() {
    return this.resultado;
  }
  public void setResultado(int resultado) {
    this.resultado = resultado;
  }

  public String getEstatus()
  {
    return this.estatus;
  }
  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }
  public String getObservacion() {
    return this.observacion;
  }
  public void setObservacion(String observacion) {
    this.observacion = observacion;
  }
}