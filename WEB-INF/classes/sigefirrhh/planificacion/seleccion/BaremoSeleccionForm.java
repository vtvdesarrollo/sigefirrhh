package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class BaremoSeleccionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(BaremoSeleccionForm.class.getName());
  private BaremoSeleccion baremoSeleccion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionFacade seleccionFacade = new SeleccionFacade();
  private boolean showBaremoSeleccionByCodBaremo;
  private boolean showBaremoSeleccionByNombre;
  private String findCodBaremo;
  private String findNombre;
  private Object stateResultBaremoSeleccionByCodBaremo = null;

  private Object stateResultBaremoSeleccionByNombre = null;

  public String getFindCodBaremo()
  {
    return this.findCodBaremo;
  }
  public void setFindCodBaremo(String findCodBaremo) {
    this.findCodBaremo = findCodBaremo;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public BaremoSeleccion getBaremoSeleccion() {
    if (this.baremoSeleccion == null) {
      this.baremoSeleccion = new BaremoSeleccion();
    }
    return this.baremoSeleccion;
  }

  public BaremoSeleccionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findBaremoSeleccionByCodBaremo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findBaremoSeleccionByCodBaremo(this.findCodBaremo);
      this.showBaremoSeleccionByCodBaremo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showBaremoSeleccionByCodBaremo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodBaremo = null;
    this.findNombre = null;

    return null;
  }

  public String findBaremoSeleccionByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findBaremoSeleccionByNombre(this.findNombre);
      this.showBaremoSeleccionByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showBaremoSeleccionByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodBaremo = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowBaremoSeleccionByCodBaremo() {
    return this.showBaremoSeleccionByCodBaremo;
  }
  public boolean isShowBaremoSeleccionByNombre() {
    return this.showBaremoSeleccionByNombre;
  }

  public String selectBaremoSeleccion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idBaremoSeleccion = 
      Long.parseLong((String)requestParameterMap.get("idBaremoSeleccion"));
    try
    {
      this.baremoSeleccion = 
        this.seleccionFacade.findBaremoSeleccionById(
        idBaremoSeleccion);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.baremoSeleccion = null;
    this.showBaremoSeleccionByCodBaremo = false;
    this.showBaremoSeleccionByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.seleccionFacade.addBaremoSeleccion(
          this.baremoSeleccion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.seleccionFacade.updateBaremoSeleccion(
          this.baremoSeleccion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deleteBaremoSeleccion(
        this.baremoSeleccion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.baremoSeleccion = new BaremoSeleccion();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.baremoSeleccion.setIdBaremoSeleccion(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.BaremoSeleccion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.baremoSeleccion = new BaremoSeleccion();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}