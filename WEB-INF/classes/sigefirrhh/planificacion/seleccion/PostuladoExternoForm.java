package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class PostuladoExternoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PostuladoExternoForm.class.getName());
  private PostuladoExterno postuladoExterno;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionFacade seleccionFacade = new SeleccionFacade();
  private boolean showPostuladoExternoByCedula;
  private boolean showPostuladoExternoByPrimerApellido;
  private int findCedula;
  private String findPrimerApellido;
  private Object stateResultPostuladoExternoByCedula = null;

  private Object stateResultPostuladoExternoByPrimerApellido = null;

  public int getFindCedula()
  {
    return this.findCedula;
  }
  public void setFindCedula(int findCedula) {
    this.findCedula = findCedula;
  }
  public String getFindPrimerApellido() {
    return this.findPrimerApellido;
  }
  public void setFindPrimerApellido(String findPrimerApellido) {
    this.findPrimerApellido = findPrimerApellido;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public PostuladoExterno getPostuladoExterno() {
    if (this.postuladoExterno == null) {
      this.postuladoExterno = new PostuladoExterno();
    }
    return this.postuladoExterno;
  }

  public PostuladoExternoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListSexo()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PostuladoExterno.LISTA_SEXO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEstadoCivil()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = PostuladoExterno.LISTA_ESTADO_CIVIL.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListNacionalidad() {
    Collection col = new ArrayList();

    Iterator iterEntry = PostuladoExterno.LISTA_NACIONALIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findPostuladoExternoByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findPostuladoExternoByCedula(this.findCedula);
      this.showPostuladoExternoByCedula = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPostuladoExternoByCedula)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCedula = 0;
    this.findPrimerApellido = null;

    return null;
  }

  public String findPostuladoExternoByPrimerApellido()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findPostuladoExternoByPrimerApellido(this.findPrimerApellido);
      this.showPostuladoExternoByPrimerApellido = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPostuladoExternoByPrimerApellido)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCedula = 0;
    this.findPrimerApellido = null;

    return null;
  }

  public boolean isShowPostuladoExternoByCedula() {
    return this.showPostuladoExternoByCedula;
  }
  public boolean isShowPostuladoExternoByPrimerApellido() {
    return this.showPostuladoExternoByPrimerApellido;
  }

  public String selectPostuladoExterno()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPostuladoExterno = 
      Long.parseLong((String)requestParameterMap.get("idPostuladoExterno"));
    try
    {
      this.postuladoExterno = 
        this.seleccionFacade.findPostuladoExternoById(
        idPostuladoExterno);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.postuladoExterno = null;
    this.showPostuladoExternoByCedula = false;
    this.showPostuladoExternoByPrimerApellido = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.postuladoExterno.getFechaNacimiento() != null) && 
      (this.postuladoExterno.getFechaNacimiento().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Nacimiento no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.seleccionFacade.addPostuladoExterno(
          this.postuladoExterno);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.postuladoExterno);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.seleccionFacade.updatePostuladoExterno(
          this.postuladoExterno);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.postuladoExterno);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deletePostuladoExterno(
        this.postuladoExterno);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.postuladoExterno);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.postuladoExterno = new PostuladoExterno();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.postuladoExterno.setIdPostuladoExterno(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.PostuladoExterno"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.postuladoExterno = new PostuladoExterno();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}