package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class VaremosPK
  implements Serializable
{
  public long idVaremos;

  public VaremosPK()
  {
  }

  public VaremosPK(long idVaremos)
  {
    this.idVaremos = idVaremos;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((VaremosPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(VaremosPK thatPK)
  {
    return 
      this.idVaremos == thatPK.idVaremos;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idVaremos)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idVaremos);
  }
}