package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.planificacion.elegible.Elegible;

public class PostuladoConcurso
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_ORIGEN;
  private long idPostuladoConcurso;
  private ConcursoCargo concursoCargo;
  private String origen;
  private String observacion;
  private String estatus;
  private int resultado;
  private Personal personal;
  private Elegible elegible;
  private PostuladoExterno postuladoExterno;
  private int cedula;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cedula", "concursoCargo", "elegible", "estatus", "idPostuladoConcurso", "observacion", "origen", "personal", "postuladoExterno", "resultado" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.seleccion.ConcursoCargo"), sunjdo$classForName$("sigefirrhh.planificacion.elegible.Elegible"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PostuladoExterno"), Integer.TYPE }; private static final byte[] jdoFieldFlags = { 21, 26, 26, 21, 24, 21, 21, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PostuladoConcurso"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PostuladoConcurso());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ORIGEN = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("0", "POSTULADO");
    LISTA_ESTATUS.put("1", "PRESELECCIONADO");
    LISTA_ESTATUS.put("2", "SELECCIONADO");
    LISTA_ESTATUS.put("3", "PRIMER ELEGIBLE");
    LISTA_ESTATUS.put("4", "SEGUNDO ELEGIBLE");
    LISTA_ORIGEN.put("I", "PERSONAL INTERNO");
    LISTA_ORIGEN.put("E", "REGISTRO ELEGIBLE");
    LISTA_ORIGEN.put("X", "PERSONAL EXTERNO");
  }

  public PostuladoConcurso()
  {
    jdoSetorigen(this, "I");

    jdoSetestatus(this, "0");

    jdoSetresultado(this, 0);
  }

  public String getObservacion()
  {
    return jdoGetobservacion(this);
  }
  public void setObservacion(String observacion) {
    jdoSetobservacion(this, observacion);
  }

  public String toString()
  {
    if (jdoGetorigen(this).equals("I"))
      return jdoGetpersonal(this).getCedula() + " - " + 
        jdoGetpersonal(this).getPrimerApellido() + ", " + 
        jdoGetpersonal(this).getPrimerNombre() + " - " + LISTA_ESTATUS.get(jdoGetestatus(this));
    if (jdoGetorigen(this).equals("E")) {
      return jdoGetelegible(this).getCedula() + " - " + 
        jdoGetelegible(this).getPrimerApellido() + ", " + 
        jdoGetelegible(this).getPrimerNombre() + " - " + LISTA_ESTATUS.get(jdoGetestatus(this));
    }
    return jdoGetpostuladoExterno(this).getCedula() + " - " + 
      jdoGetpostuladoExterno(this).getPrimerApellido() + ", " + 
      jdoGetpostuladoExterno(this).getPrimerNombre() + " - " + LISTA_ESTATUS.get(jdoGetestatus(this));
  }

  public ConcursoCargo getConcursoCargo()
  {
    return jdoGetconcursoCargo(this);
  }
  public void setConcursoCargo(ConcursoCargo concursoCargo) {
    jdoSetconcursoCargo(this, concursoCargo);
  }
  public Elegible getElegible() {
    return jdoGetelegible(this);
  }
  public void setElegible(Elegible elegible) {
    jdoSetelegible(this, elegible);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public long getIdPostuladoConcurso() {
    return jdoGetidPostuladoConcurso(this);
  }
  public void setIdPostuladoConcurso(long idPostuladoConcurso) {
    jdoSetidPostuladoConcurso(this, idPostuladoConcurso);
  }
  public String getOrigen() {
    return jdoGetorigen(this);
  }
  public void setOrigen(String origen) {
    jdoSetorigen(this, origen);
  }
  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }
  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }
  public PostuladoExterno getPostuladoExterno() {
    return jdoGetpostuladoExterno(this);
  }
  public void setPostuladoExterno(PostuladoExterno postuladoExterno) {
    jdoSetpostuladoExterno(this, postuladoExterno);
  }
  public int getResultado() {
    return jdoGetresultado(this);
  }
  public void setResultado(int resultado) {
    jdoSetresultado(this, resultado);
  }
  public int getCedula() {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PostuladoConcurso localPostuladoConcurso = new PostuladoConcurso();
    localPostuladoConcurso.jdoFlags = 1;
    localPostuladoConcurso.jdoStateManager = paramStateManager;
    return localPostuladoConcurso;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PostuladoConcurso localPostuladoConcurso = new PostuladoConcurso();
    localPostuladoConcurso.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPostuladoConcurso.jdoFlags = 1;
    localPostuladoConcurso.jdoStateManager = paramStateManager;
    return localPostuladoConcurso;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concursoCargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.elegible);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPostuladoConcurso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observacion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.origen);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.postuladoExterno);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.resultado);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concursoCargo = ((ConcursoCargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.elegible = ((Elegible)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPostuladoConcurso = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.origen = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.postuladoExterno = ((PostuladoExterno)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.resultado = localStateManager.replacingIntField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PostuladoConcurso paramPostuladoConcurso, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramPostuladoConcurso.cedula;
      return;
    case 1:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.concursoCargo = paramPostuladoConcurso.concursoCargo;
      return;
    case 2:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.elegible = paramPostuladoConcurso.elegible;
      return;
    case 3:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramPostuladoConcurso.estatus;
      return;
    case 4:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.idPostuladoConcurso = paramPostuladoConcurso.idPostuladoConcurso;
      return;
    case 5:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.observacion = paramPostuladoConcurso.observacion;
      return;
    case 6:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.origen = paramPostuladoConcurso.origen;
      return;
    case 7:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramPostuladoConcurso.personal;
      return;
    case 8:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.postuladoExterno = paramPostuladoConcurso.postuladoExterno;
      return;
    case 9:
      if (paramPostuladoConcurso == null)
        throw new IllegalArgumentException("arg1");
      this.resultado = paramPostuladoConcurso.resultado;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PostuladoConcurso))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PostuladoConcurso localPostuladoConcurso = (PostuladoConcurso)paramObject;
    if (localPostuladoConcurso.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPostuladoConcurso, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PostuladoConcursoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PostuladoConcursoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PostuladoConcursoPK))
      throw new IllegalArgumentException("arg1");
    PostuladoConcursoPK localPostuladoConcursoPK = (PostuladoConcursoPK)paramObject;
    localPostuladoConcursoPK.idPostuladoConcurso = this.idPostuladoConcurso;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PostuladoConcursoPK))
      throw new IllegalArgumentException("arg1");
    PostuladoConcursoPK localPostuladoConcursoPK = (PostuladoConcursoPK)paramObject;
    this.idPostuladoConcurso = localPostuladoConcursoPK.idPostuladoConcurso;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PostuladoConcursoPK))
      throw new IllegalArgumentException("arg2");
    PostuladoConcursoPK localPostuladoConcursoPK = (PostuladoConcursoPK)paramObject;
    localPostuladoConcursoPK.idPostuladoConcurso = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PostuladoConcursoPK))
      throw new IllegalArgumentException("arg2");
    PostuladoConcursoPK localPostuladoConcursoPK = (PostuladoConcursoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localPostuladoConcursoPK.idPostuladoConcurso);
  }

  private static final int jdoGetcedula(PostuladoConcurso paramPostuladoConcurso)
  {
    if (paramPostuladoConcurso.jdoFlags <= 0)
      return paramPostuladoConcurso.cedula;
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.cedula;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 0))
      return paramPostuladoConcurso.cedula;
    return localStateManager.getIntField(paramPostuladoConcurso, jdoInheritedFieldCount + 0, paramPostuladoConcurso.cedula);
  }

  private static final void jdoSetcedula(PostuladoConcurso paramPostuladoConcurso, int paramInt)
  {
    if (paramPostuladoConcurso.jdoFlags == 0)
    {
      paramPostuladoConcurso.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramPostuladoConcurso, jdoInheritedFieldCount + 0, paramPostuladoConcurso.cedula, paramInt);
  }

  private static final ConcursoCargo jdoGetconcursoCargo(PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.concursoCargo;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 1))
      return paramPostuladoConcurso.concursoCargo;
    return (ConcursoCargo)localStateManager.getObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 1, paramPostuladoConcurso.concursoCargo);
  }

  private static final void jdoSetconcursoCargo(PostuladoConcurso paramPostuladoConcurso, ConcursoCargo paramConcursoCargo)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.concursoCargo = paramConcursoCargo;
      return;
    }
    localStateManager.setObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 1, paramPostuladoConcurso.concursoCargo, paramConcursoCargo);
  }

  private static final Elegible jdoGetelegible(PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.elegible;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 2))
      return paramPostuladoConcurso.elegible;
    return (Elegible)localStateManager.getObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 2, paramPostuladoConcurso.elegible);
  }

  private static final void jdoSetelegible(PostuladoConcurso paramPostuladoConcurso, Elegible paramElegible)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.elegible = paramElegible;
      return;
    }
    localStateManager.setObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 2, paramPostuladoConcurso.elegible, paramElegible);
  }

  private static final String jdoGetestatus(PostuladoConcurso paramPostuladoConcurso)
  {
    if (paramPostuladoConcurso.jdoFlags <= 0)
      return paramPostuladoConcurso.estatus;
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.estatus;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 3))
      return paramPostuladoConcurso.estatus;
    return localStateManager.getStringField(paramPostuladoConcurso, jdoInheritedFieldCount + 3, paramPostuladoConcurso.estatus);
  }

  private static final void jdoSetestatus(PostuladoConcurso paramPostuladoConcurso, String paramString)
  {
    if (paramPostuladoConcurso.jdoFlags == 0)
    {
      paramPostuladoConcurso.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoConcurso, jdoInheritedFieldCount + 3, paramPostuladoConcurso.estatus, paramString);
  }

  private static final long jdoGetidPostuladoConcurso(PostuladoConcurso paramPostuladoConcurso)
  {
    return paramPostuladoConcurso.idPostuladoConcurso;
  }

  private static final void jdoSetidPostuladoConcurso(PostuladoConcurso paramPostuladoConcurso, long paramLong)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.idPostuladoConcurso = paramLong;
      return;
    }
    localStateManager.setLongField(paramPostuladoConcurso, jdoInheritedFieldCount + 4, paramPostuladoConcurso.idPostuladoConcurso, paramLong);
  }

  private static final String jdoGetobservacion(PostuladoConcurso paramPostuladoConcurso)
  {
    if (paramPostuladoConcurso.jdoFlags <= 0)
      return paramPostuladoConcurso.observacion;
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.observacion;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 5))
      return paramPostuladoConcurso.observacion;
    return localStateManager.getStringField(paramPostuladoConcurso, jdoInheritedFieldCount + 5, paramPostuladoConcurso.observacion);
  }

  private static final void jdoSetobservacion(PostuladoConcurso paramPostuladoConcurso, String paramString)
  {
    if (paramPostuladoConcurso.jdoFlags == 0)
    {
      paramPostuladoConcurso.observacion = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.observacion = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoConcurso, jdoInheritedFieldCount + 5, paramPostuladoConcurso.observacion, paramString);
  }

  private static final String jdoGetorigen(PostuladoConcurso paramPostuladoConcurso)
  {
    if (paramPostuladoConcurso.jdoFlags <= 0)
      return paramPostuladoConcurso.origen;
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.origen;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 6))
      return paramPostuladoConcurso.origen;
    return localStateManager.getStringField(paramPostuladoConcurso, jdoInheritedFieldCount + 6, paramPostuladoConcurso.origen);
  }

  private static final void jdoSetorigen(PostuladoConcurso paramPostuladoConcurso, String paramString)
  {
    if (paramPostuladoConcurso.jdoFlags == 0)
    {
      paramPostuladoConcurso.origen = paramString;
      return;
    }
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.origen = paramString;
      return;
    }
    localStateManager.setStringField(paramPostuladoConcurso, jdoInheritedFieldCount + 6, paramPostuladoConcurso.origen, paramString);
  }

  private static final Personal jdoGetpersonal(PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.personal;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 7))
      return paramPostuladoConcurso.personal;
    return (Personal)localStateManager.getObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 7, paramPostuladoConcurso.personal);
  }

  private static final void jdoSetpersonal(PostuladoConcurso paramPostuladoConcurso, Personal paramPersonal)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 7, paramPostuladoConcurso.personal, paramPersonal);
  }

  private static final PostuladoExterno jdoGetpostuladoExterno(PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.postuladoExterno;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 8))
      return paramPostuladoConcurso.postuladoExterno;
    return (PostuladoExterno)localStateManager.getObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 8, paramPostuladoConcurso.postuladoExterno);
  }

  private static final void jdoSetpostuladoExterno(PostuladoConcurso paramPostuladoConcurso, PostuladoExterno paramPostuladoExterno)
  {
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.postuladoExterno = paramPostuladoExterno;
      return;
    }
    localStateManager.setObjectField(paramPostuladoConcurso, jdoInheritedFieldCount + 8, paramPostuladoConcurso.postuladoExterno, paramPostuladoExterno);
  }

  private static final int jdoGetresultado(PostuladoConcurso paramPostuladoConcurso)
  {
    if (paramPostuladoConcurso.jdoFlags <= 0)
      return paramPostuladoConcurso.resultado;
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
      return paramPostuladoConcurso.resultado;
    if (localStateManager.isLoaded(paramPostuladoConcurso, jdoInheritedFieldCount + 9))
      return paramPostuladoConcurso.resultado;
    return localStateManager.getIntField(paramPostuladoConcurso, jdoInheritedFieldCount + 9, paramPostuladoConcurso.resultado);
  }

  private static final void jdoSetresultado(PostuladoConcurso paramPostuladoConcurso, int paramInt)
  {
    if (paramPostuladoConcurso.jdoFlags == 0)
    {
      paramPostuladoConcurso.resultado = paramInt;
      return;
    }
    StateManager localStateManager = paramPostuladoConcurso.jdoStateManager;
    if (localStateManager == null)
    {
      paramPostuladoConcurso.resultado = paramInt;
      return;
    }
    localStateManager.setIntField(paramPostuladoConcurso, jdoInheritedFieldCount + 9, paramPostuladoConcurso.resultado, paramInt);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}