package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class BaremoSeleccion
  implements Serializable, PersistenceCapable
{
  private long idBaremoSeleccion;
  private String codBaremo;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codBaremo", "idBaremoSeleccion", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetnombre(this);
  }

  public String getCodBaremo()
  {
    return jdoGetcodBaremo(this);
  }

  public void setCodBaremo(String codBaremo)
  {
    jdoSetcodBaremo(this, codBaremo);
  }

  public long getIdBaremoSeleccion()
  {
    return jdoGetidBaremoSeleccion(this);
  }

  public void setIdBaremoSeleccion(long idBaremoSeleccion)
  {
    jdoSetidBaremoSeleccion(this, idBaremoSeleccion);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.BaremoSeleccion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new BaremoSeleccion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    BaremoSeleccion localBaremoSeleccion = new BaremoSeleccion();
    localBaremoSeleccion.jdoFlags = 1;
    localBaremoSeleccion.jdoStateManager = paramStateManager;
    return localBaremoSeleccion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    BaremoSeleccion localBaremoSeleccion = new BaremoSeleccion();
    localBaremoSeleccion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localBaremoSeleccion.jdoFlags = 1;
    localBaremoSeleccion.jdoStateManager = paramStateManager;
    return localBaremoSeleccion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codBaremo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idBaremoSeleccion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codBaremo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idBaremoSeleccion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(BaremoSeleccion paramBaremoSeleccion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramBaremoSeleccion == null)
        throw new IllegalArgumentException("arg1");
      this.codBaremo = paramBaremoSeleccion.codBaremo;
      return;
    case 1:
      if (paramBaremoSeleccion == null)
        throw new IllegalArgumentException("arg1");
      this.idBaremoSeleccion = paramBaremoSeleccion.idBaremoSeleccion;
      return;
    case 2:
      if (paramBaremoSeleccion == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramBaremoSeleccion.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof BaremoSeleccion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    BaremoSeleccion localBaremoSeleccion = (BaremoSeleccion)paramObject;
    if (localBaremoSeleccion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localBaremoSeleccion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new BaremoSeleccionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new BaremoSeleccionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BaremoSeleccionPK))
      throw new IllegalArgumentException("arg1");
    BaremoSeleccionPK localBaremoSeleccionPK = (BaremoSeleccionPK)paramObject;
    localBaremoSeleccionPK.idBaremoSeleccion = this.idBaremoSeleccion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BaremoSeleccionPK))
      throw new IllegalArgumentException("arg1");
    BaremoSeleccionPK localBaremoSeleccionPK = (BaremoSeleccionPK)paramObject;
    this.idBaremoSeleccion = localBaremoSeleccionPK.idBaremoSeleccion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BaremoSeleccionPK))
      throw new IllegalArgumentException("arg2");
    BaremoSeleccionPK localBaremoSeleccionPK = (BaremoSeleccionPK)paramObject;
    localBaremoSeleccionPK.idBaremoSeleccion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BaremoSeleccionPK))
      throw new IllegalArgumentException("arg2");
    BaremoSeleccionPK localBaremoSeleccionPK = (BaremoSeleccionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localBaremoSeleccionPK.idBaremoSeleccion);
  }

  private static final String jdoGetcodBaremo(BaremoSeleccion paramBaremoSeleccion)
  {
    if (paramBaremoSeleccion.jdoFlags <= 0)
      return paramBaremoSeleccion.codBaremo;
    StateManager localStateManager = paramBaremoSeleccion.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoSeleccion.codBaremo;
    if (localStateManager.isLoaded(paramBaremoSeleccion, jdoInheritedFieldCount + 0))
      return paramBaremoSeleccion.codBaremo;
    return localStateManager.getStringField(paramBaremoSeleccion, jdoInheritedFieldCount + 0, paramBaremoSeleccion.codBaremo);
  }

  private static final void jdoSetcodBaremo(BaremoSeleccion paramBaremoSeleccion, String paramString)
  {
    if (paramBaremoSeleccion.jdoFlags == 0)
    {
      paramBaremoSeleccion.codBaremo = paramString;
      return;
    }
    StateManager localStateManager = paramBaremoSeleccion.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoSeleccion.codBaremo = paramString;
      return;
    }
    localStateManager.setStringField(paramBaremoSeleccion, jdoInheritedFieldCount + 0, paramBaremoSeleccion.codBaremo, paramString);
  }

  private static final long jdoGetidBaremoSeleccion(BaremoSeleccion paramBaremoSeleccion)
  {
    return paramBaremoSeleccion.idBaremoSeleccion;
  }

  private static final void jdoSetidBaremoSeleccion(BaremoSeleccion paramBaremoSeleccion, long paramLong)
  {
    StateManager localStateManager = paramBaremoSeleccion.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoSeleccion.idBaremoSeleccion = paramLong;
      return;
    }
    localStateManager.setLongField(paramBaremoSeleccion, jdoInheritedFieldCount + 1, paramBaremoSeleccion.idBaremoSeleccion, paramLong);
  }

  private static final String jdoGetnombre(BaremoSeleccion paramBaremoSeleccion)
  {
    if (paramBaremoSeleccion.jdoFlags <= 0)
      return paramBaremoSeleccion.nombre;
    StateManager localStateManager = paramBaremoSeleccion.jdoStateManager;
    if (localStateManager == null)
      return paramBaremoSeleccion.nombre;
    if (localStateManager.isLoaded(paramBaremoSeleccion, jdoInheritedFieldCount + 2))
      return paramBaremoSeleccion.nombre;
    return localStateManager.getStringField(paramBaremoSeleccion, jdoInheritedFieldCount + 2, paramBaremoSeleccion.nombre);
  }

  private static final void jdoSetnombre(BaremoSeleccion paramBaremoSeleccion, String paramString)
  {
    if (paramBaremoSeleccion.jdoFlags == 0)
    {
      paramBaremoSeleccion.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramBaremoSeleccion.jdoStateManager;
    if (localStateManager == null)
    {
      paramBaremoSeleccion.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramBaremoSeleccion, jdoInheritedFieldCount + 2, paramBaremoSeleccion.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}