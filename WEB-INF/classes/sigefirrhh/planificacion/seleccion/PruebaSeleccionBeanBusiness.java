package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PruebaSeleccionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPruebaSeleccion(PruebaSeleccion pruebaSeleccion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PruebaSeleccion pruebaSeleccionNew = 
      (PruebaSeleccion)BeanUtils.cloneBean(
      pruebaSeleccion);

    pm.makePersistent(pruebaSeleccionNew);
  }

  public void updatePruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception
  {
    PruebaSeleccion pruebaSeleccionModify = 
      findPruebaSeleccionById(pruebaSeleccion.getIdPruebaSeleccion());

    BeanUtils.copyProperties(pruebaSeleccionModify, pruebaSeleccion);
  }

  public void deletePruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PruebaSeleccion pruebaSeleccionDelete = 
      findPruebaSeleccionById(pruebaSeleccion.getIdPruebaSeleccion());
    pm.deletePersistent(pruebaSeleccionDelete);
  }

  public PruebaSeleccion findPruebaSeleccionById(long idPruebaSeleccion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPruebaSeleccion == pIdPruebaSeleccion";
    Query query = pm.newQuery(PruebaSeleccion.class, filter);

    query.declareParameters("long pIdPruebaSeleccion");

    parameters.put("pIdPruebaSeleccion", new Long(idPruebaSeleccion));

    Collection colPruebaSeleccion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPruebaSeleccion.iterator();
    return (PruebaSeleccion)iterator.next();
  }

  public Collection findPruebaSeleccionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent pruebaSeleccionExtent = pm.getExtent(
      PruebaSeleccion.class, true);
    Query query = pm.newQuery(pruebaSeleccionExtent);
    query.setOrdering("codPrueba ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodPrueba(String codPrueba)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codPrueba == pCodPrueba";

    Query query = pm.newQuery(PruebaSeleccion.class, filter);

    query.declareParameters("java.lang.String pCodPrueba");
    HashMap parameters = new HashMap();

    parameters.put("pCodPrueba", new String(codPrueba));

    query.setOrdering("codPrueba ascending");

    Collection colPruebaSeleccion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPruebaSeleccion);

    return colPruebaSeleccion;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(PruebaSeleccion.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codPrueba ascending");

    Collection colPruebaSeleccion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPruebaSeleccion);

    return colPruebaSeleccion;
  }
}