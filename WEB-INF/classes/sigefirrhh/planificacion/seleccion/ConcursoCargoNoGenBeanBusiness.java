package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;

public class ConcursoCargoNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(ConcursoCargoNoGenBeanBusiness.class.getName());

  public long validarPostuladoConcurso(int cedula)
  {
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      StringBuffer sql = new StringBuffer();

      sql.append("select cc.id_concurso_cargo");
      sql.append(" from concurso c, concursocargo cc, postuladoconcurso pc");
      sql.append(" where c.id_concurso = cc.id_concurso");
      sql.append(" and cc.id_postulado_concurso = pc.id_postulado_concurso");
      sql.append(" and pc.cedula = ? ");
      sql.append(" and cc.estatus = '2'");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stRegistros.setInt(1, cedula);
      rsRegistros = stRegistros.executeQuery();
      if (rsRegistros.next())
        return rsRegistros.getLong("id_concurso_cargo");
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException4) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException5) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException6)
        {
        }
    }
    if (rsRegistros != null) try {
        rsRegistros.close();
      } catch (Exception localException7) {
      } if (stRegistros != null) try {
        stRegistros.close();
      } catch (Exception localException8) {
      } if (connection != null) try {
        connection.close(); connection = null;
      }
      catch (Exception localException9)
      {
      } return 0L;
  }

  public Collection findByIdConcurso(long idConcurso) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append(" select cc.id_concurso_cargo as id, (c.descripcion_cargo || ' - ' || rc.codigo_nomina)  as descripcion");
      sql.append(" from concursocargo cc, registrocargos rc, cargo c");
      sql.append(" where cc.id_concurso = ?");
      sql.append(" and cc.id_registro_cargos = rc.id_registro_cargos");
      sql.append(" and rc.id_cargo = c.id_cargo");
      sql.append(" and cc.id_concurso_cargo in");
      sql.append(" (select id_concurso_cargo from concursocargo where estatus = '1')");
      sql.append(" order by rc.codigo_nomina");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConcurso);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }

  public Collection findByIdConcursoTodos(long idConcurso)
    throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      sql.append(" select cc.id_concurso_cargo as id, (c.descripcion_cargo || ' - ' || rc.codigo_nomina)  as descripcion");
      sql.append(" from concursocargo cc, registrocargos rc, cargo c");
      sql.append(" where cc.id_concurso = ?");
      sql.append(" and cc.id_registro_cargos = rc.id_registro_cargos");
      sql.append(" and rc.id_cargo = c.id_cargo");
      sql.append(" order by rc.codigo_nomina");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      stRegistros.setLong(1, idConcurso);
      rsRegistros = stRegistros.executeQuery();

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException1) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException2) {  }
 
    }
    return col;
  }

  public Collection findByConcursoNoEstatus(long idConcurso, String estatus)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "concurso.idConcurso == pIdConcurso && estatus == pEstatus";

    Query query = pm.newQuery(ConcursoCargo.class, filter);

    query.declareParameters("long pIdConcurso, String pEstatus");
    HashMap parameters = new HashMap();

    parameters.put("pIdConcurso", new Long(idConcurso));
    parameters.put("pEstatus", estatus);

    query.setOrdering("registroCargos.cargo.descripcionCargo ascending");

    Collection colConcursoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcursoCargo);

    return colConcursoCargo;
  }

  public Collection findByConcursoTodos(long idConcurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "concurso.idConcurso == pIdConcurso ";

    Query query = pm.newQuery(ConcursoCargo.class, filter);

    query.declareParameters("long pIdConcurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdConcurso", new Long(idConcurso));

    query.setOrdering("registroCargos.cargo.descripcionCargo ascending");

    Collection colConcursoCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcursoCargo);

    return colConcursoCargo;
  }
}