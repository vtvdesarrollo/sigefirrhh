package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class VaremosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(VaremosForm.class.getName());
  private Varemos varemos;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionFacade seleccionFacade = new SeleccionFacade();
  private boolean showVaremosByCodVaremos;
  private boolean showVaremosByNombre;
  private String findCodVaremos;
  private String findNombre;
  private Object stateResultVaremosByCodVaremos = null;

  private Object stateResultVaremosByNombre = null;

  public String getFindCodVaremos()
  {
    return this.findCodVaremos;
  }
  public void setFindCodVaremos(String findCodVaremos) {
    this.findCodVaremos = findCodVaremos;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Varemos getVaremos() {
    if (this.varemos == null) {
      this.varemos = new Varemos();
    }
    return this.varemos;
  }

  public VaremosForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findVaremosByCodVaremos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findVaremosByCodVaremos(this.findCodVaremos);
      this.showVaremosByCodVaremos = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showVaremosByCodVaremos)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodVaremos = null;
    this.findNombre = null;

    return null;
  }

  public String findVaremosByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findVaremosByNombre(this.findNombre);
      this.showVaremosByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showVaremosByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodVaremos = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowVaremosByCodVaremos() {
    return this.showVaremosByCodVaremos;
  }
  public boolean isShowVaremosByNombre() {
    return this.showVaremosByNombre;
  }

  public String selectVaremos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idVaremos = 
      Long.parseLong((String)requestParameterMap.get("idVaremos"));
    try
    {
      this.varemos = 
        this.seleccionFacade.findVaremosById(
        idVaremos);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.varemos = null;
    this.showVaremosByCodVaremos = false;
    this.showVaremosByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.seleccionFacade.addVaremos(
          this.varemos);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.seleccionFacade.updateVaremos(
          this.varemos);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deleteVaremos(
        this.varemos);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.varemos = new Varemos();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.varemos.setIdVaremos(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.Varemos"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.varemos = new Varemos();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}