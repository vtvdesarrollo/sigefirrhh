package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConcursoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConcursoForm.class.getName());
  private Concurso concurso;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private SeleccionFacade seleccionFacade = new SeleccionFacade();
  private boolean showConcursoByCodConcurso;
  private boolean showConcursoByNombre;
  private String findCodConcurso;
  private String findNombre;
  private Object stateResultConcursoByCodConcurso = null;

  private Object stateResultConcursoByNombre = null;

  public String getFindCodConcurso()
  {
    return this.findCodConcurso;
  }
  public void setFindCodConcurso(String findCodConcurso) {
    this.findCodConcurso = findCodConcurso;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Concurso getConcurso() {
    if (this.concurso == null) {
      this.concurso = new Concurso();
    }
    return this.concurso;
  }

  public ConcursoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Concurso.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAprobadoMpd() {
    Collection col = new ArrayList();

    Iterator iterEntry = Concurso.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
  }

  public String findConcursoByCodConcurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.seleccionFacade.findConcursoByCodConcurso(this.findCodConcurso, idOrganismo);
      this.showConcursoByCodConcurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConcursoByCodConcurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodConcurso = null;
    this.findNombre = null;

    return null;
  }

  public String findConcursoByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.seleccionFacade.findConcursoByNombre(this.findNombre, idOrganismo);
      this.showConcursoByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConcursoByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodConcurso = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowConcursoByCodConcurso() {
    return this.showConcursoByCodConcurso;
  }
  public boolean isShowConcursoByNombre() {
    return this.showConcursoByNombre;
  }

  public String selectConcurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idConcurso = 
      Long.parseLong((String)requestParameterMap.get("idConcurso"));
    try
    {
      this.concurso = 
        this.seleccionFacade.findConcursoById(
        idConcurso);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.concurso = null;
    this.showConcursoByCodConcurso = false;
    this.showConcursoByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try
    {
      if ((this.concurso.getInicioConvocatoria() != null) && (this.concurso.getFechaApertura() != null) && 
        (this.concurso.getInicioConvocatoria().compareTo(this.concurso.getFechaApertura()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Inicio de Convoctoria no puede ser menor a la fecha de Apertura", ""));
        error = true;
      }

      if ((this.concurso.getFinConvocatoria() != null) && (this.concurso.getInicioConvocatoria() != null) && 
        (this.concurso.getFinConvocatoria().compareTo(this.concurso.getInicioConvocatoria()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin de Convocatoria no puede ser menor a la fecha de Inicio de Convocatoria", ""));
        error = true;
      }

      if ((this.concurso.getFinRetiro() != null) && (this.concurso.getInicioRetiro() != null) && 
        (this.concurso.getFinRetiro().compareTo(this.concurso.getInicioRetiro()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin Retiro Información no puede ser menor a la fecha de Inicio Retiro Información", ""));
        error = true;
      }

      if ((this.concurso.getFinInscripcion() != null) && (this.concurso.getInicioInscripcion() != null) && 
        (this.concurso.getFinInscripcion().compareTo(this.concurso.getInicioInscripcion()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin de Inscripción no puede ser menor a la fecha de Inicio de Inscripción", ""));
        error = true;
      }

      if ((this.concurso.getFinPublicacionInsc() != null) && (this.concurso.getIniPublicacionInsc() != null) && 
        (this.concurso.getFinPublicacionInsc().compareTo(this.concurso.getIniPublicacionInsc()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin de Publicación Resultados de Inscripción no puede ser menor a la fecha Inicio de Publicación Resultados de Inscripción", ""));
        error = true;
      }

      if ((this.concurso.getFinExamenes() != null) && (this.concurso.getIniExamenes() != null) && 
        (this.concurso.getFinExamenes().compareTo(this.concurso.getIniExamenes()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin de Presentación Exámenes no puede ser menor a la fecha Inicio de Presetación de Exámenes", ""));
        error = true;
      }

      if ((this.concurso.getFinPublicacionExam() != null) && (this.concurso.getIniPublicacionExam() != null) && 
        (this.concurso.getFinPublicacionExam().compareTo(this.concurso.getIniPublicacionExam()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin de Publicación Resultados de Exámenes no puede ser menor a la fecha Inicio de Publicación de Resultados de Exámenes", ""));
        error = true;
      }

      if ((this.concurso.getFinPublicacionInsc() != null) && (this.concurso.getIniPublicacionInsc() != null) && 
        (this.concurso.getFinEntrevistas().compareTo(this.concurso.getIniEntrevistas()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin de Enrevistas no puede ser menor a la fecha Inicio de Entrevistas", ""));
        error = true;
      }

      if ((this.concurso.getFinPublicacionEntr() != null) && (this.concurso.getIniPublicacionEntr() != null) && 
        (this.concurso.getFinPublicacionEntr().compareTo(this.concurso.getIniPublicacionEntr()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fin de Publicación Resultado de Entrevistas no puede ser menor a la fecha Inicio de Publicación Resultado de Entrevistas", ""));
        error = true;
      }

      if ((this.concurso.getEntregaInforme() != null) && (this.concurso.getFechaApertura() != null) && 
        (this.concurso.getEntregaInforme().compareTo(this.concurso.getFechaApertura()) < 0)) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Cierre no puede ser menor a la fecha de Apertura", ""));
        error = true;
      }
    }
    catch (Exception e) {
      return null;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.seleccionFacade.addConcurso(
          this.concurso);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.concurso);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.seleccionFacade.updateConcurso(
          this.concurso);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.concurso);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deleteConcurso(
        this.concurso);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.concurso);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.concurso = new Concurso();

    this.concurso.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.concurso.setIdConcurso(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.Concurso"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.concurso = new Concurso();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}