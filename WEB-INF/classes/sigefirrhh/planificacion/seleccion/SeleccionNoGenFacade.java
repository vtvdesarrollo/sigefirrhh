package sigefirrhh.planificacion.seleccion;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;

public class SeleccionNoGenFacade extends SeleccionFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private SeleccionNoGenBusiness seleccionNoGenBusiness = new SeleccionNoGenBusiness();

  public long validarPostuladoConcurso(int cedula)
    throws Exception
  {
    return this.seleccionNoGenBusiness.validarPostuladoConcurso(cedula);
  }

  public Collection findPostuladoConcursoByIdConcursoCargo(long idConcursoCargo)
    throws Exception
  {
    return this.seleccionNoGenBusiness.findPostuladoConcursoByIdConcursoCargo(idConcursoCargo);
  }

  public Collection findPostuladoConcursoByIdConcursoCargo(long idConcursoCargo, String estatus)
    throws Exception
  {
    return this.seleccionNoGenBusiness.findPostuladoConcursoByIdConcursoCargo(idConcursoCargo, estatus);
  }

  public Collection findPostuladoConcursoByIdConcursoCargoTodos(long idConcursoCargo)
    throws Exception
  {
    return this.seleccionNoGenBusiness.findPostuladoConcursoByIdConcursoCargoTodos(idConcursoCargo);
  }

  public Collection findConcursoCargoByIdConcurso(long idConcurso)
    throws Exception
  {
    return this.seleccionNoGenBusiness.findConcursoCargoByIdConcurso(idConcurso);
  }

  public Collection findConcursoCargoByIdConcursoTodos(long idConcurso)
    throws Exception
  {
    return this.seleccionNoGenBusiness.findConcursoCargoByIdConcursoTodos(idConcurso);
  }

  public Collection findConcursoCargoByConcursoNoEstatus(long idConcurso, String estatus)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.seleccionNoGenBusiness.findConcursoCargoByConcursoNoEstatus(idConcurso, estatus);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findConcursoCargoByConcursoTodos(long idConcurso) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionNoGenBusiness.findConcursoCargoByConcursoTodos(idConcurso);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}