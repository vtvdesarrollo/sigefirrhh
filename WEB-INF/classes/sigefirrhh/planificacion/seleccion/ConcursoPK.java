package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class ConcursoPK
  implements Serializable
{
  public long idConcurso;

  public ConcursoPK()
  {
  }

  public ConcursoPK(long idConcurso)
  {
    this.idConcurso = idConcurso;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConcursoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConcursoPK thatPK)
  {
    return 
      this.idConcurso == thatPK.idConcurso;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConcurso)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConcurso);
  }
}