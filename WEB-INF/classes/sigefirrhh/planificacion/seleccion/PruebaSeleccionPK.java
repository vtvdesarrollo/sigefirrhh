package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class PruebaSeleccionPK
  implements Serializable
{
  public long idPruebaSeleccion;

  public PruebaSeleccionPK()
  {
  }

  public PruebaSeleccionPK(long idPruebaSeleccion)
  {
    this.idPruebaSeleccion = idPruebaSeleccion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PruebaSeleccionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PruebaSeleccionPK thatPK)
  {
    return 
      this.idPruebaSeleccion == thatPK.idPruebaSeleccion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPruebaSeleccion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPruebaSeleccion);
  }
}