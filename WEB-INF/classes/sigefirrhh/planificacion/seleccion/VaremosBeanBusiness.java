package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class VaremosBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addVaremos(Varemos varemos)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Varemos varemosNew = 
      (Varemos)BeanUtils.cloneBean(
      varemos);

    pm.makePersistent(varemosNew);
  }

  public void updateVaremos(Varemos varemos) throws Exception
  {
    Varemos varemosModify = 
      findVaremosById(varemos.getIdVaremos());

    BeanUtils.copyProperties(varemosModify, varemos);
  }

  public void deleteVaremos(Varemos varemos) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Varemos varemosDelete = 
      findVaremosById(varemos.getIdVaremos());
    pm.deletePersistent(varemosDelete);
  }

  public Varemos findVaremosById(long idVaremos) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idVaremos == pIdVaremos";
    Query query = pm.newQuery(Varemos.class, filter);

    query.declareParameters("long pIdVaremos");

    parameters.put("pIdVaremos", new Long(idVaremos));

    Collection colVaremos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colVaremos.iterator();
    return (Varemos)iterator.next();
  }

  public Collection findVaremosAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent varemosExtent = pm.getExtent(
      Varemos.class, true);
    Query query = pm.newQuery(varemosExtent);
    query.setOrdering("codVaremos ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodVaremos(String codVaremos)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codVaremos == pCodVaremos";

    Query query = pm.newQuery(Varemos.class, filter);

    query.declareParameters("java.lang.String pCodVaremos");
    HashMap parameters = new HashMap();

    parameters.put("pCodVaremos", new String(codVaremos));

    query.setOrdering("codVaremos ascending");

    Collection colVaremos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVaremos);

    return colVaremos;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Varemos.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    query.setOrdering("codVaremos ascending");

    Collection colVaremos = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colVaremos);

    return colVaremos;
  }
}