package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class PostuladoConcursoPK
  implements Serializable
{
  public long idPostuladoConcurso;

  public PostuladoConcursoPK()
  {
  }

  public PostuladoConcursoPK(long idPostuladoConcurso)
  {
    this.idPostuladoConcurso = idPostuladoConcurso;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PostuladoConcursoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PostuladoConcursoPK thatPK)
  {
    return 
      this.idPostuladoConcurso == thatPK.idPostuladoConcurso;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPostuladoConcurso)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPostuladoConcurso);
  }
}