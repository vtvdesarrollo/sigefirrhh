package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class PruebaSeleccionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PruebaSeleccionForm.class.getName());
  private PruebaSeleccion pruebaSeleccion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionFacade seleccionFacade = new SeleccionFacade();
  private boolean showPruebaSeleccionByCodPrueba;
  private boolean showPruebaSeleccionByNombre;
  private String findCodPrueba;
  private String findNombre;
  private Object stateResultPruebaSeleccionByCodPrueba = null;

  private Object stateResultPruebaSeleccionByNombre = null;

  public String getFindCodPrueba()
  {
    return this.findCodPrueba;
  }
  public void setFindCodPrueba(String findCodPrueba) {
    this.findCodPrueba = findCodPrueba;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public PruebaSeleccion getPruebaSeleccion() {
    if (this.pruebaSeleccion == null) {
      this.pruebaSeleccion = new PruebaSeleccion();
    }
    return this.pruebaSeleccion;
  }

  public PruebaSeleccionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findPruebaSeleccionByCodPrueba()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findPruebaSeleccionByCodPrueba(this.findCodPrueba);
      this.showPruebaSeleccionByCodPrueba = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPruebaSeleccionByCodPrueba)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPrueba = null;
    this.findNombre = null;

    return null;
  }

  public String findPruebaSeleccionByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findPruebaSeleccionByNombre(this.findNombre);
      this.showPruebaSeleccionByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPruebaSeleccionByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodPrueba = null;
    this.findNombre = null;

    return null;
  }

  public boolean isShowPruebaSeleccionByCodPrueba() {
    return this.showPruebaSeleccionByCodPrueba;
  }
  public boolean isShowPruebaSeleccionByNombre() {
    return this.showPruebaSeleccionByNombre;
  }

  public String selectPruebaSeleccion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPruebaSeleccion = 
      Long.parseLong((String)requestParameterMap.get("idPruebaSeleccion"));
    try
    {
      this.pruebaSeleccion = 
        this.seleccionFacade.findPruebaSeleccionById(
        idPruebaSeleccion);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.pruebaSeleccion = null;
    this.showPruebaSeleccionByCodPrueba = false;
    this.showPruebaSeleccionByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.seleccionFacade.addPruebaSeleccion(
          this.pruebaSeleccion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.seleccionFacade.updatePruebaSeleccion(
          this.pruebaSeleccion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deletePruebaSeleccion(
        this.pruebaSeleccion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.pruebaSeleccion = new PruebaSeleccion();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.pruebaSeleccion.setIdPruebaSeleccion(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.PruebaSeleccion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.pruebaSeleccion = new PruebaSeleccion();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}