package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class ConcursoCargoPK
  implements Serializable
{
  public long idConcursoCargo;

  public ConcursoCargoPK()
  {
  }

  public ConcursoCargoPK(long idConcursoCargo)
  {
    this.idConcursoCargo = idConcursoCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ConcursoCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ConcursoCargoPK thatPK)
  {
    return 
      this.idConcursoCargo == thatPK.idConcursoCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idConcursoCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idConcursoCargo);
  }
}