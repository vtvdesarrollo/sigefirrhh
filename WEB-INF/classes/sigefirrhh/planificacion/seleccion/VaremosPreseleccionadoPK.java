package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class VaremosPreseleccionadoPK
  implements Serializable
{
  public long idVaremosPreseleccionado;

  public VaremosPreseleccionadoPK()
  {
  }

  public VaremosPreseleccionadoPK(long idVaremosPreseleccionado)
  {
    this.idVaremosPreseleccionado = idVaremosPreseleccionado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((VaremosPreseleccionadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(VaremosPreseleccionadoPK thatPK)
  {
    return 
      this.idVaremosPreseleccionado == thatPK.idVaremosPreseleccionado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idVaremosPreseleccionado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idVaremosPreseleccionado);
  }
}