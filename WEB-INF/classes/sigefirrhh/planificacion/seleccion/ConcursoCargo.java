package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.personal.registroCargos.RegistroCargos;

public class ConcursoCargo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idConcursoCargo;
  private Concurso concurso;
  private RegistroCargos registroCargos;
  private PostuladoConcurso postuladoConcurso;
  private String estatus;
  private int cedula;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cedula", "concurso", "estatus", "idConcursoCargo", "postuladoConcurso", "registroCargos" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.seleccion.Concurso"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.planificacion.seleccion.PostuladoConcurso"), sunjdo$classForName$("sigefirrhh.personal.registroCargos.RegistroCargos") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 24, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.ConcursoCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ConcursoCargo());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("1", "EN PROCESO DE SELECCION");
    LISTA_ESTATUS.put("2", "SELECCIONADO");
    LISTA_ESTATUS.put("3", "CERRADO");
  }

  public ConcursoCargo()
  {
    jdoSetestatus(this, "1");
  }

  public String toString()
  {
    return jdoGetregistroCargos(this).getCargo().getDescripcionCargo() + " - " + 
      jdoGetregistroCargos(this).getCodigoNomina() + " - " + LISTA_ESTATUS.get(jdoGetestatus(this));
  }

  public Concurso getConcurso() {
    return jdoGetconcurso(this);
  }
  public void setConcurso(Concurso concurso) {
    jdoSetconcurso(this, concurso);
  }
  public String getEstatus() {
    return jdoGetestatus(this);
  }
  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }
  public long getIdConcursoCargo() {
    return jdoGetidConcursoCargo(this);
  }
  public void setIdConcursoCargo(long idConcursoCargo) {
    jdoSetidConcursoCargo(this, idConcursoCargo);
  }
  public PostuladoConcurso getPostuladoConcurso() {
    return jdoGetpostuladoConcurso(this);
  }
  public void setPostuladoConcurso(PostuladoConcurso postuladoConcurso) {
    jdoSetpostuladoConcurso(this, postuladoConcurso);
  }
  public RegistroCargos getRegistroCargos() {
    return jdoGetregistroCargos(this);
  }
  public void setRegistroCargos(RegistroCargos registroCargos) {
    jdoSetregistroCargos(this, registroCargos);
  }
  public int getCedula() {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ConcursoCargo localConcursoCargo = new ConcursoCargo();
    localConcursoCargo.jdoFlags = 1;
    localConcursoCargo.jdoStateManager = paramStateManager;
    return localConcursoCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ConcursoCargo localConcursoCargo = new ConcursoCargo();
    localConcursoCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localConcursoCargo.jdoFlags = 1;
    localConcursoCargo.jdoStateManager = paramStateManager;
    return localConcursoCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concurso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idConcursoCargo);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.postuladoConcurso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.registroCargos);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concurso = ((Concurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idConcursoCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.postuladoConcurso = ((PostuladoConcurso)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.registroCargos = ((RegistroCargos)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ConcursoCargo paramConcursoCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramConcursoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramConcursoCargo.cedula;
      return;
    case 1:
      if (paramConcursoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.concurso = paramConcursoCargo.concurso;
      return;
    case 2:
      if (paramConcursoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramConcursoCargo.estatus;
      return;
    case 3:
      if (paramConcursoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idConcursoCargo = paramConcursoCargo.idConcursoCargo;
      return;
    case 4:
      if (paramConcursoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.postuladoConcurso = paramConcursoCargo.postuladoConcurso;
      return;
    case 5:
      if (paramConcursoCargo == null)
        throw new IllegalArgumentException("arg1");
      this.registroCargos = paramConcursoCargo.registroCargos;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ConcursoCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ConcursoCargo localConcursoCargo = (ConcursoCargo)paramObject;
    if (localConcursoCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localConcursoCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ConcursoCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ConcursoCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConcursoCargoPK))
      throw new IllegalArgumentException("arg1");
    ConcursoCargoPK localConcursoCargoPK = (ConcursoCargoPK)paramObject;
    localConcursoCargoPK.idConcursoCargo = this.idConcursoCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ConcursoCargoPK))
      throw new IllegalArgumentException("arg1");
    ConcursoCargoPK localConcursoCargoPK = (ConcursoCargoPK)paramObject;
    this.idConcursoCargo = localConcursoCargoPK.idConcursoCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConcursoCargoPK))
      throw new IllegalArgumentException("arg2");
    ConcursoCargoPK localConcursoCargoPK = (ConcursoCargoPK)paramObject;
    localConcursoCargoPK.idConcursoCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ConcursoCargoPK))
      throw new IllegalArgumentException("arg2");
    ConcursoCargoPK localConcursoCargoPK = (ConcursoCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localConcursoCargoPK.idConcursoCargo);
  }

  private static final int jdoGetcedula(ConcursoCargo paramConcursoCargo)
  {
    if (paramConcursoCargo.jdoFlags <= 0)
      return paramConcursoCargo.cedula;
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConcursoCargo.cedula;
    if (localStateManager.isLoaded(paramConcursoCargo, jdoInheritedFieldCount + 0))
      return paramConcursoCargo.cedula;
    return localStateManager.getIntField(paramConcursoCargo, jdoInheritedFieldCount + 0, paramConcursoCargo.cedula);
  }

  private static final void jdoSetcedula(ConcursoCargo paramConcursoCargo, int paramInt)
  {
    if (paramConcursoCargo.jdoFlags == 0)
    {
      paramConcursoCargo.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcursoCargo.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramConcursoCargo, jdoInheritedFieldCount + 0, paramConcursoCargo.cedula, paramInt);
  }

  private static final Concurso jdoGetconcurso(ConcursoCargo paramConcursoCargo)
  {
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConcursoCargo.concurso;
    if (localStateManager.isLoaded(paramConcursoCargo, jdoInheritedFieldCount + 1))
      return paramConcursoCargo.concurso;
    return (Concurso)localStateManager.getObjectField(paramConcursoCargo, jdoInheritedFieldCount + 1, paramConcursoCargo.concurso);
  }

  private static final void jdoSetconcurso(ConcursoCargo paramConcursoCargo, Concurso paramConcurso)
  {
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcursoCargo.concurso = paramConcurso;
      return;
    }
    localStateManager.setObjectField(paramConcursoCargo, jdoInheritedFieldCount + 1, paramConcursoCargo.concurso, paramConcurso);
  }

  private static final String jdoGetestatus(ConcursoCargo paramConcursoCargo)
  {
    if (paramConcursoCargo.jdoFlags <= 0)
      return paramConcursoCargo.estatus;
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConcursoCargo.estatus;
    if (localStateManager.isLoaded(paramConcursoCargo, jdoInheritedFieldCount + 2))
      return paramConcursoCargo.estatus;
    return localStateManager.getStringField(paramConcursoCargo, jdoInheritedFieldCount + 2, paramConcursoCargo.estatus);
  }

  private static final void jdoSetestatus(ConcursoCargo paramConcursoCargo, String paramString)
  {
    if (paramConcursoCargo.jdoFlags == 0)
    {
      paramConcursoCargo.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcursoCargo.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramConcursoCargo, jdoInheritedFieldCount + 2, paramConcursoCargo.estatus, paramString);
  }

  private static final long jdoGetidConcursoCargo(ConcursoCargo paramConcursoCargo)
  {
    return paramConcursoCargo.idConcursoCargo;
  }

  private static final void jdoSetidConcursoCargo(ConcursoCargo paramConcursoCargo, long paramLong)
  {
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcursoCargo.idConcursoCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramConcursoCargo, jdoInheritedFieldCount + 3, paramConcursoCargo.idConcursoCargo, paramLong);
  }

  private static final PostuladoConcurso jdoGetpostuladoConcurso(ConcursoCargo paramConcursoCargo)
  {
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConcursoCargo.postuladoConcurso;
    if (localStateManager.isLoaded(paramConcursoCargo, jdoInheritedFieldCount + 4))
      return paramConcursoCargo.postuladoConcurso;
    return (PostuladoConcurso)localStateManager.getObjectField(paramConcursoCargo, jdoInheritedFieldCount + 4, paramConcursoCargo.postuladoConcurso);
  }

  private static final void jdoSetpostuladoConcurso(ConcursoCargo paramConcursoCargo, PostuladoConcurso paramPostuladoConcurso)
  {
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcursoCargo.postuladoConcurso = paramPostuladoConcurso;
      return;
    }
    localStateManager.setObjectField(paramConcursoCargo, jdoInheritedFieldCount + 4, paramConcursoCargo.postuladoConcurso, paramPostuladoConcurso);
  }

  private static final RegistroCargos jdoGetregistroCargos(ConcursoCargo paramConcursoCargo)
  {
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
      return paramConcursoCargo.registroCargos;
    if (localStateManager.isLoaded(paramConcursoCargo, jdoInheritedFieldCount + 5))
      return paramConcursoCargo.registroCargos;
    return (RegistroCargos)localStateManager.getObjectField(paramConcursoCargo, jdoInheritedFieldCount + 5, paramConcursoCargo.registroCargos);
  }

  private static final void jdoSetregistroCargos(ConcursoCargo paramConcursoCargo, RegistroCargos paramRegistroCargos)
  {
    StateManager localStateManager = paramConcursoCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramConcursoCargo.registroCargos = paramRegistroCargos;
      return;
    }
    localStateManager.setObjectField(paramConcursoCargo, jdoInheritedFieldCount + 5, paramConcursoCargo.registroCargos, paramRegistroCargos);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}