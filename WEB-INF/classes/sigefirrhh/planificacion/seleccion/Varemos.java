package sigefirrhh.planificacion.seleccion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Varemos
  implements Serializable, PersistenceCapable
{
  private long idVaremos;
  private String codVaremos;
  private String nombre;
  private double porcentaje;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codVaremos", "idVaremos", "nombre", "porcentaje" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Varemos()
  {
    jdoSetporcentaje(this, 0.0D);
  }
  public String toString() {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetporcentaje(this));
    return jdoGetnombre(this) + " " + a;
  }

  public String getCodVaremo()
  {
    return jdoGetcodVaremos(this);
  }

  public void setCodVaremo(String codVaremos)
  {
    jdoSetcodVaremos(this, codVaremos);
  }

  public long getIdVaremos()
  {
    return jdoGetidVaremos(this);
  }

  public void setIdVaremos(long idVaremos)
  {
    jdoSetidVaremos(this, idVaremos);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre)
  {
    jdoSetnombre(this, nombre);
  }

  public String getCodVaremos()
  {
    return jdoGetcodVaremos(this);
  }

  public void setCodVaremos(String codVaremos)
  {
    jdoSetcodVaremos(this, codVaremos);
  }

  public double getPorcentaje()
  {
    return jdoGetporcentaje(this);
  }

  public void setPorcentaje(double porcentaje)
  {
    jdoSetporcentaje(this, porcentaje);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.planificacion.seleccion.Varemos"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Varemos());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Varemos localVaremos = new Varemos();
    localVaremos.jdoFlags = 1;
    localVaremos.jdoStateManager = paramStateManager;
    return localVaremos;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Varemos localVaremos = new Varemos();
    localVaremos.jdoCopyKeyFieldsFromObjectId(paramObject);
    localVaremos.jdoFlags = 1;
    localVaremos.jdoStateManager = paramStateManager;
    return localVaremos;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codVaremos);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idVaremos);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentaje);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codVaremos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idVaremos = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentaje = localStateManager.replacingDoubleField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Varemos paramVaremos, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramVaremos == null)
        throw new IllegalArgumentException("arg1");
      this.codVaremos = paramVaremos.codVaremos;
      return;
    case 1:
      if (paramVaremos == null)
        throw new IllegalArgumentException("arg1");
      this.idVaremos = paramVaremos.idVaremos;
      return;
    case 2:
      if (paramVaremos == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramVaremos.nombre;
      return;
    case 3:
      if (paramVaremos == null)
        throw new IllegalArgumentException("arg1");
      this.porcentaje = paramVaremos.porcentaje;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Varemos))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Varemos localVaremos = (Varemos)paramObject;
    if (localVaremos.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localVaremos, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new VaremosPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new VaremosPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VaremosPK))
      throw new IllegalArgumentException("arg1");
    VaremosPK localVaremosPK = (VaremosPK)paramObject;
    localVaremosPK.idVaremos = this.idVaremos;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof VaremosPK))
      throw new IllegalArgumentException("arg1");
    VaremosPK localVaremosPK = (VaremosPK)paramObject;
    this.idVaremos = localVaremosPK.idVaremos;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VaremosPK))
      throw new IllegalArgumentException("arg2");
    VaremosPK localVaremosPK = (VaremosPK)paramObject;
    localVaremosPK.idVaremos = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof VaremosPK))
      throw new IllegalArgumentException("arg2");
    VaremosPK localVaremosPK = (VaremosPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localVaremosPK.idVaremos);
  }

  private static final String jdoGetcodVaremos(Varemos paramVaremos)
  {
    if (paramVaremos.jdoFlags <= 0)
      return paramVaremos.codVaremos;
    StateManager localStateManager = paramVaremos.jdoStateManager;
    if (localStateManager == null)
      return paramVaremos.codVaremos;
    if (localStateManager.isLoaded(paramVaremos, jdoInheritedFieldCount + 0))
      return paramVaremos.codVaremos;
    return localStateManager.getStringField(paramVaremos, jdoInheritedFieldCount + 0, paramVaremos.codVaremos);
  }

  private static final void jdoSetcodVaremos(Varemos paramVaremos, String paramString)
  {
    if (paramVaremos.jdoFlags == 0)
    {
      paramVaremos.codVaremos = paramString;
      return;
    }
    StateManager localStateManager = paramVaremos.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremos.codVaremos = paramString;
      return;
    }
    localStateManager.setStringField(paramVaremos, jdoInheritedFieldCount + 0, paramVaremos.codVaremos, paramString);
  }

  private static final long jdoGetidVaremos(Varemos paramVaremos)
  {
    return paramVaremos.idVaremos;
  }

  private static final void jdoSetidVaremos(Varemos paramVaremos, long paramLong)
  {
    StateManager localStateManager = paramVaremos.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremos.idVaremos = paramLong;
      return;
    }
    localStateManager.setLongField(paramVaremos, jdoInheritedFieldCount + 1, paramVaremos.idVaremos, paramLong);
  }

  private static final String jdoGetnombre(Varemos paramVaremos)
  {
    if (paramVaremos.jdoFlags <= 0)
      return paramVaremos.nombre;
    StateManager localStateManager = paramVaremos.jdoStateManager;
    if (localStateManager == null)
      return paramVaremos.nombre;
    if (localStateManager.isLoaded(paramVaremos, jdoInheritedFieldCount + 2))
      return paramVaremos.nombre;
    return localStateManager.getStringField(paramVaremos, jdoInheritedFieldCount + 2, paramVaremos.nombre);
  }

  private static final void jdoSetnombre(Varemos paramVaremos, String paramString)
  {
    if (paramVaremos.jdoFlags == 0)
    {
      paramVaremos.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramVaremos.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremos.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramVaremos, jdoInheritedFieldCount + 2, paramVaremos.nombre, paramString);
  }

  private static final double jdoGetporcentaje(Varemos paramVaremos)
  {
    if (paramVaremos.jdoFlags <= 0)
      return paramVaremos.porcentaje;
    StateManager localStateManager = paramVaremos.jdoStateManager;
    if (localStateManager == null)
      return paramVaremos.porcentaje;
    if (localStateManager.isLoaded(paramVaremos, jdoInheritedFieldCount + 3))
      return paramVaremos.porcentaje;
    return localStateManager.getDoubleField(paramVaremos, jdoInheritedFieldCount + 3, paramVaremos.porcentaje);
  }

  private static final void jdoSetporcentaje(Varemos paramVaremos, double paramDouble)
  {
    if (paramVaremos.jdoFlags == 0)
    {
      paramVaremos.porcentaje = paramDouble;
      return;
    }
    StateManager localStateManager = paramVaremos.jdoStateManager;
    if (localStateManager == null)
    {
      paramVaremos.porcentaje = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramVaremos, jdoInheritedFieldCount + 3, paramVaremos.porcentaje, paramDouble);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}