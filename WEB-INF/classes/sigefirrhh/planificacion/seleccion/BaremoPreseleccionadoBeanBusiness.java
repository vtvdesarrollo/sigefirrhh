package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class BaremoPreseleccionadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    BaremoPreseleccionado baremoPreseleccionadoNew = 
      (BaremoPreseleccionado)BeanUtils.cloneBean(
      baremoPreseleccionado);

    PostuladoConcursoBeanBusiness postuladoConcursoBeanBusiness = new PostuladoConcursoBeanBusiness();

    if (baremoPreseleccionadoNew.getPostuladoConcurso() != null) {
      baremoPreseleccionadoNew.setPostuladoConcurso(
        postuladoConcursoBeanBusiness.findPostuladoConcursoById(
        baremoPreseleccionadoNew.getPostuladoConcurso().getIdPostuladoConcurso()));
    }

    VaremosBeanBusiness varemosBeanBusiness = new VaremosBeanBusiness();

    if (baremoPreseleccionadoNew.getVaremos() != null) {
      baremoPreseleccionadoNew.setVaremos(
        varemosBeanBusiness.findVaremosById(
        baremoPreseleccionadoNew.getVaremos().getIdVaremos()));
    }
    pm.makePersistent(baremoPreseleccionadoNew);
  }

  public void updateBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado) throws Exception
  {
    BaremoPreseleccionado baremoPreseleccionadoModify = 
      findBaremoPreseleccionadoById(baremoPreseleccionado.getIdBaremoPreseleccionado());

    PostuladoConcursoBeanBusiness postuladoConcursoBeanBusiness = new PostuladoConcursoBeanBusiness();

    if (baremoPreseleccionado.getPostuladoConcurso() != null) {
      baremoPreseleccionado.setPostuladoConcurso(
        postuladoConcursoBeanBusiness.findPostuladoConcursoById(
        baremoPreseleccionado.getPostuladoConcurso().getIdPostuladoConcurso()));
    }

    VaremosBeanBusiness varemosBeanBusiness = new VaremosBeanBusiness();

    if (baremoPreseleccionado.getVaremos() != null) {
      baremoPreseleccionado.setVaremos(
        varemosBeanBusiness.findVaremosById(
        baremoPreseleccionado.getVaremos().getIdVaremos()));
    }

    BeanUtils.copyProperties(baremoPreseleccionadoModify, baremoPreseleccionado);
  }

  public void deleteBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    BaremoPreseleccionado baremoPreseleccionadoDelete = 
      findBaremoPreseleccionadoById(baremoPreseleccionado.getIdBaremoPreseleccionado());
    pm.deletePersistent(baremoPreseleccionadoDelete);
  }

  public BaremoPreseleccionado findBaremoPreseleccionadoById(long idBaremoPreseleccionado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idBaremoPreseleccionado == pIdBaremoPreseleccionado";
    Query query = pm.newQuery(BaremoPreseleccionado.class, filter);

    query.declareParameters("long pIdBaremoPreseleccionado");

    parameters.put("pIdBaremoPreseleccionado", new Long(idBaremoPreseleccionado));

    Collection colBaremoPreseleccionado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colBaremoPreseleccionado.iterator();
    return (BaremoPreseleccionado)iterator.next();
  }

  public Collection findBaremoPreseleccionadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent baremoPreseleccionadoExtent = pm.getExtent(
      BaremoPreseleccionado.class, true);
    Query query = pm.newQuery(baremoPreseleccionadoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPostuladoConcurso(long idPostuladoConcurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "postuladoConcurso.idPostuladoConcurso == pIdPostuladoConcurso";

    Query query = pm.newQuery(BaremoPreseleccionado.class, filter);

    query.declareParameters("long pIdPostuladoConcurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdPostuladoConcurso", new Long(idPostuladoConcurso));

    Collection colBaremoPreseleccionado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBaremoPreseleccionado);

    return colBaremoPreseleccionado;
  }
}