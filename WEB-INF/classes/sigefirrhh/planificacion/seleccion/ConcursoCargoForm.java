package sigefirrhh.planificacion.seleccion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.registro.Registro;
import sigefirrhh.base.registro.RegistroFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.registroCargos.RegistroCargos;
import sigefirrhh.personal.registroCargos.RegistroCargosNoGenFacade;
import sigefirrhh.sistema.RegistrarAuditoria;

public class ConcursoCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ConcursoCargoForm.class.getName());
  private ConcursoCargo concursoCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SeleccionNoGenFacade seleccionFacade = new SeleccionNoGenFacade();
  private RegistroCargosNoGenFacade registroCargosFacade = new RegistroCargosNoGenFacade();
  private RegistroFacade registroFacade = new RegistroFacade();
  private boolean showConcursoCargoByConcurso;
  private String findSelectConcurso;
  private Collection findColConcurso;
  private Collection colConcurso;
  private Collection colRegistroForRegistroCargos;
  private Collection colRegistroCargos;
  private Collection colPostuladoConcurso;
  private String selectConcurso;
  private String selectRegistroForRegistroCargos;
  private String selectRegistroCargos;
  private String selectPostuladoConcurso;
  private Object stateResultConcursoCargoByConcurso = null;

  public boolean isSeleccionado()
  {
    if ((this.concursoCargo != null) && (!this.concursoCargo.getEstatus().equals("1")))
    {
      return true;
    }

    return false;
  }

  public String getFindSelectConcurso()
  {
    return this.findSelectConcurso;
  }
  public void setFindSelectConcurso(String valConcurso) {
    this.findSelectConcurso = valConcurso;
  }

  public Collection getFindColConcurso() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColConcurso.iterator();
    Concurso concurso = null;
    while (iterator.hasNext()) {
      concurso = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concurso.getIdConcurso()), 
        concurso.toString()));
    }
    return col;
  }

  public String getSelectConcurso()
  {
    return this.selectConcurso;
  }
  public void setSelectConcurso(String valConcurso) {
    Iterator iterator = this.colConcurso.iterator();
    Concurso concurso = null;
    this.concursoCargo.setConcurso(null);
    while (iterator.hasNext()) {
      concurso = (Concurso)iterator.next();
      if (String.valueOf(concurso.getIdConcurso()).equals(
        valConcurso)) {
        this.concursoCargo.setConcurso(
          concurso);
        break;
      }
    }
    this.selectConcurso = valConcurso;
  }
  public String getSelectRegistroForRegistroCargos() {
    return this.selectRegistroForRegistroCargos;
  }
  public void setSelectRegistroForRegistroCargos(String valRegistroForRegistroCargos) {
    this.selectRegistroForRegistroCargos = valRegistroForRegistroCargos;
  }

  public void changeRegistroForRegistroCargos(ValueChangeEvent event)
  {
    long idRegistro = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colRegistroCargos = null;
      if (idRegistro > 0L) {
        this.colRegistroCargos = 
          this.registroCargosFacade.findRegistroCargosForConcurso(
          idRegistro);
      } else {
        this.selectRegistroCargos = null;
        this.concursoCargo.setRegistroCargos(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectRegistroCargos = null;
      this.concursoCargo.setRegistroCargos(
        null);
    }
  }

  public boolean isShowRegistroForRegistroCargos() { return this.colRegistroForRegistroCargos != null; }

  public String getSelectRegistroCargos() {
    return this.selectRegistroCargos;
  }
  public void setSelectRegistroCargos(String valRegistroCargos) {
    Iterator iterator = this.colRegistroCargos.iterator();
    RegistroCargos registroCargos = null;
    this.concursoCargo.setRegistroCargos(null);

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valRegistroCargos)) {
        try {
          this.concursoCargo.setRegistroCargos(this.registroCargosFacade.findRegistroCargosById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }

    this.selectRegistroCargos = valRegistroCargos;
  }
  public boolean isShowRegistroCargos() {
    return this.colRegistroCargos != null;
  }

  public String getSelectPostuladoConcurso() {
    return this.selectPostuladoConcurso;
  }
  public void setSelectPostuladoConcurso(String valPostuladoConcurso) {
    Iterator iterator = this.colPostuladoConcurso.iterator();
    PostuladoConcurso postuladoConcurso = null;

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valPostuladoConcurso)) {
        try {
          this.concursoCargo.setPostuladoConcurso(this.seleccionFacade.findPostuladoConcursoById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }
    this.selectPostuladoConcurso = valPostuladoConcurso;
  }
  public boolean isShowPostuladoConcurso() {
    return this.colPostuladoConcurso != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ConcursoCargo getConcursoCargo() {
    if (this.concursoCargo == null) {
      this.concursoCargo = new ConcursoCargo();
    }
    return this.concursoCargo;
  }

  public ConcursoCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColConcurso()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colConcurso.iterator();
    Concurso concurso = null;
    while (iterator.hasNext()) {
      concurso = (Concurso)iterator.next();
      col.add(new SelectItem(
        String.valueOf(concurso.getIdConcurso()), 
        concurso.toString()));
    }
    return col;
  }

  public Collection getColRegistroForRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroForRegistroCargos.iterator();
    Registro registroForRegistroCargos = null;
    while (iterator.hasNext()) {
      registroForRegistroCargos = (Registro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(registroForRegistroCargos.getIdRegistro()), 
        registroForRegistroCargos.toString()));
    }
    return col;
  }

  public Collection getColRegistroCargos() {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegistroCargos.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getColPostuladoConcurso() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPostuladoConcurso.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColConcurso = 
        this.seleccionFacade.findConcursoByEstatus("1", this.login.getIdOrganismo());

      this.colConcurso = 
        this.seleccionFacade.findConcursoByEstatus("1", this.login.getIdOrganismo());

      this.colRegistroForRegistroCargos = 
        this.registroFacade.findRegistroByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findConcursoCargoByConcurso()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.seleccionFacade.findConcursoCargoByConcurso(Long.valueOf(this.findSelectConcurso).longValue());
      this.showConcursoCargoByConcurso = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showConcursoCargoByConcurso)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectConcurso = null;

    return null;
  }

  public boolean isShowConcursoCargoByConcurso() {
    return this.showConcursoCargoByConcurso;
  }

  public String selectConcursoCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();
    log.error("paso nuevo 0");

    this.selectConcurso = null;
    this.selectRegistroCargos = null;
    this.selectRegistroForRegistroCargos = null;

    this.selectPostuladoConcurso = null;

    long idConcursoCargo = 
      Long.parseLong((String)requestParameterMap.get("idConcursoCargo"));
    try
    {
      this.concursoCargo = 
        this.seleccionFacade.findConcursoCargoById(
        idConcursoCargo);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.selectPostuladoConcurso = null;
      this.concursoCargo.setPostuladoConcurso(
        null);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.concursoCargo = null;
    this.showConcursoCargoByConcurso = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.seleccionFacade.addConcursoCargo(
          this.concursoCargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.concursoCargo);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      }
      else {
        this.seleccionFacade.updateConcursoCargo(
          this.concursoCargo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.concursoCargo);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.seleccionFacade.deleteConcursoCargo(
        this.concursoCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.concursoCargo = new ConcursoCargo();

    this.selectConcurso = null;
    this.selectRegistroCargos = null;
    this.selectRegistroForRegistroCargos = null;
    this.selectPostuladoConcurso = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.concursoCargo.setIdConcursoCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.planificacion.seleccion.ConcursoCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.concursoCargo = new ConcursoCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}