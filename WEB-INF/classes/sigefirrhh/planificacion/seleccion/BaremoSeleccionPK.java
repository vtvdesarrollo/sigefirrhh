package sigefirrhh.planificacion.seleccion;

import java.io.Serializable;

public class BaremoSeleccionPK
  implements Serializable
{
  public long idBaremoSeleccion;

  public BaremoSeleccionPK()
  {
  }

  public BaremoSeleccionPK(long idBaremoSeleccion)
  {
    this.idBaremoSeleccion = idBaremoSeleccion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((BaremoSeleccionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(BaremoSeleccionPK thatPK)
  {
    return 
      this.idBaremoSeleccion == thatPK.idBaremoSeleccion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idBaremoSeleccion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idBaremoSeleccion);
  }
}