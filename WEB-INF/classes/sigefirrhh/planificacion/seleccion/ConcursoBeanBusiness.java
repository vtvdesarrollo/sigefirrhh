package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class ConcursoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addConcurso(Concurso concurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Concurso concursoNew = 
      (Concurso)BeanUtils.cloneBean(
      concurso);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (concursoNew.getOrganismo() != null) {
      concursoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        concursoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(concursoNew);
  }

  public void updateConcurso(Concurso concurso) throws Exception
  {
    Concurso concursoModify = 
      findConcursoById(concurso.getIdConcurso());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (concurso.getOrganismo() != null) {
      concurso.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        concurso.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(concursoModify, concurso);
  }

  public void deleteConcurso(Concurso concurso) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Concurso concursoDelete = 
      findConcursoById(concurso.getIdConcurso());
    pm.deletePersistent(concursoDelete);
  }

  public Concurso findConcursoById(long idConcurso) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idConcurso == pIdConcurso";
    Query query = pm.newQuery(Concurso.class, filter);

    query.declareParameters("long pIdConcurso");

    parameters.put("pIdConcurso", new Long(idConcurso));

    Collection colConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colConcurso.iterator();
    return (Concurso)iterator.next();
  }

  public Collection findConcursoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent concursoExtent = pm.getExtent(
      Concurso.class, true);
    Query query = pm.newQuery(concursoExtent);
    query.setOrdering("fechaApertura ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodConcurso(String codConcurso, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codConcurso == pCodConcurso &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concurso.class, filter);

    query.declareParameters("java.lang.String pCodConcurso, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodConcurso", new String(codConcurso));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaApertura ascending");

    Collection colConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcurso);

    return colConcurso;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concurso.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaApertura ascending");

    Collection colConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcurso);

    return colConcurso;
  }

  public Collection findByEstatus(String estatus, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "estatus == pEstatus &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concurso.class, filter);

    query.declareParameters("java.lang.String pEstatus, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pEstatus", new String(estatus));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaApertura ascending");

    Collection colConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcurso);

    return colConcurso;
  }

  public Collection findTodos(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = " organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concurso.class, filter);

    query.declareParameters(" long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaApertura ascending");

    Collection colConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcurso);

    return colConcurso;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(Concurso.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("fechaApertura ascending");

    Collection colConcurso = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colConcurso);

    return colConcurso;
  }
}