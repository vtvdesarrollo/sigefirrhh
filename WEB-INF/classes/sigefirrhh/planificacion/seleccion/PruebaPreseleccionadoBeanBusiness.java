package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class PruebaPreseleccionadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PruebaPreseleccionado pruebaPreseleccionadoNew = 
      (PruebaPreseleccionado)BeanUtils.cloneBean(
      pruebaPreseleccionado);

    PostuladoConcursoBeanBusiness postuladoConcursoBeanBusiness = new PostuladoConcursoBeanBusiness();

    if (pruebaPreseleccionadoNew.getPostuladoConcurso() != null) {
      pruebaPreseleccionadoNew.setPostuladoConcurso(
        postuladoConcursoBeanBusiness.findPostuladoConcursoById(
        pruebaPreseleccionadoNew.getPostuladoConcurso().getIdPostuladoConcurso()));
    }

    PruebaSeleccionBeanBusiness pruebaSeleccionBeanBusiness = new PruebaSeleccionBeanBusiness();

    if (pruebaPreseleccionadoNew.getPruebaSeleccion() != null) {
      pruebaPreseleccionadoNew.setPruebaSeleccion(
        pruebaSeleccionBeanBusiness.findPruebaSeleccionById(
        pruebaPreseleccionadoNew.getPruebaSeleccion().getIdPruebaSeleccion()));
    }
    pm.makePersistent(pruebaPreseleccionadoNew);
  }

  public void updatePruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception
  {
    PruebaPreseleccionado pruebaPreseleccionadoModify = 
      findPruebaPreseleccionadoById(pruebaPreseleccionado.getIdPruebaPreseleccionado());

    PostuladoConcursoBeanBusiness postuladoConcursoBeanBusiness = new PostuladoConcursoBeanBusiness();

    if (pruebaPreseleccionado.getPostuladoConcurso() != null) {
      pruebaPreseleccionado.setPostuladoConcurso(
        postuladoConcursoBeanBusiness.findPostuladoConcursoById(
        pruebaPreseleccionado.getPostuladoConcurso().getIdPostuladoConcurso()));
    }

    PruebaSeleccionBeanBusiness pruebaSeleccionBeanBusiness = new PruebaSeleccionBeanBusiness();

    if (pruebaPreseleccionado.getPruebaSeleccion() != null) {
      pruebaPreseleccionado.setPruebaSeleccion(
        pruebaSeleccionBeanBusiness.findPruebaSeleccionById(
        pruebaPreseleccionado.getPruebaSeleccion().getIdPruebaSeleccion()));
    }

    BeanUtils.copyProperties(pruebaPreseleccionadoModify, pruebaPreseleccionado);
  }

  public void deletePruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PruebaPreseleccionado pruebaPreseleccionadoDelete = 
      findPruebaPreseleccionadoById(pruebaPreseleccionado.getIdPruebaPreseleccionado());
    pm.deletePersistent(pruebaPreseleccionadoDelete);
  }

  public PruebaPreseleccionado findPruebaPreseleccionadoById(long idPruebaPreseleccionado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPruebaPreseleccionado == pIdPruebaPreseleccionado";
    Query query = pm.newQuery(PruebaPreseleccionado.class, filter);

    query.declareParameters("long pIdPruebaPreseleccionado");

    parameters.put("pIdPruebaPreseleccionado", new Long(idPruebaPreseleccionado));

    Collection colPruebaPreseleccionado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPruebaPreseleccionado.iterator();
    return (PruebaPreseleccionado)iterator.next();
  }

  public Collection findPruebaPreseleccionadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent pruebaPreseleccionadoExtent = pm.getExtent(
      PruebaPreseleccionado.class, true);
    Query query = pm.newQuery(pruebaPreseleccionadoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPostuladoConcurso(long idPostuladoConcurso)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "postuladoConcurso.idPostuladoConcurso == pIdPostuladoConcurso";

    Query query = pm.newQuery(PruebaPreseleccionado.class, filter);

    query.declareParameters("long pIdPostuladoConcurso");
    HashMap parameters = new HashMap();

    parameters.put("pIdPostuladoConcurso", new Long(idPostuladoConcurso));

    Collection colPruebaPreseleccionado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPruebaPreseleccionado);

    return colPruebaPreseleccionado;
  }
}