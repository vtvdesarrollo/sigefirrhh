package sigefirrhh.planificacion.seleccion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class SeleccionFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  private SeleccionBusiness seleccionBusiness = new SeleccionBusiness();

  public void addConcurso(Concurso concurso)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.seleccionBusiness.addConcurso(concurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updateConcurso(Concurso concurso) throws Exception {
    try {
      this.txn.open();
      this.seleccionBusiness.updateConcurso(concurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deleteConcurso(Concurso concurso) throws Exception {
    try {
      this.txn.open();
      this.seleccionBusiness.deleteConcurso(concurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Concurso findConcursoById(long concursoId) throws Exception {
    try {
      this.txn.open();
      Concurso concurso = this.seleccionBusiness.findConcursoById(concursoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(concurso);
      return concurso;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllConcurso() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllConcurso();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findConcursoByCodConcurso(String codConcurso, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findConcursoByCodConcurso(codConcurso, 
        idOrganismo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findConcursoByNombre(String nombre, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findConcursoByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findConcursoByEstatus(String estatus, long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness
        .findConcursoByEstatus(estatus, idOrganismo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findConcursoTodos(long idOrganismo) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness
        .findConcursoTodos(idOrganismo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findConcursoByOrganismo(long idOrganismo) throws Exception
  {
    try
    {
      this.txn.open();
      return this.seleccionBusiness.findConcursoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addConcursoCargo(ConcursoCargo concursoCargo) throws Exception {
    try {
      this.txn.open();
      this.seleccionBusiness.addConcursoCargo(concursoCargo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updateConcursoCargo(ConcursoCargo concursoCargo) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.updateConcursoCargo(concursoCargo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deleteConcursoCargo(ConcursoCargo concursoCargo) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.deleteConcursoCargo(concursoCargo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public ConcursoCargo findConcursoCargoById(long concursoCargoId) throws Exception
  {
    try {
      this.txn.open();
      ConcursoCargo concursoCargo = this.seleccionBusiness
        .findConcursoCargoById(concursoCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(concursoCargo);
      return concursoCargo;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllConcursoCargo() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllConcursoCargo();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findConcursoCargoByConcurso(long idConcurso) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findConcursoCargoByConcurso(idConcurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addPruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.addPruebaSeleccion(pruebaSeleccion);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.addBaremoSeleccion(baremoSeleccion);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addVaremos(Varemos varemos) throws Exception {
    try {
      this.txn.open();
      this.seleccionBusiness.addVaremos(varemos);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updatePruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.updatePruebaSeleccion(pruebaSeleccion);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updateBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.updateBaremoSeleccion(baremoSeleccion);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updateVaremos(Varemos varemos) throws Exception {
    try {
      this.txn.open();
      this.seleccionBusiness.updateVaremos(varemos);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deletePruebaSeleccion(PruebaSeleccion pruebaSeleccion) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.deletePruebaSeleccion(pruebaSeleccion);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deleteBaremoSeleccion(BaremoSeleccion baremoSeleccion) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.deleteBaremoSeleccion(baremoSeleccion);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deleteVaremos(Varemos varemos) throws Exception {
    try {
      this.txn.open();
      this.seleccionBusiness.deleteVaremos(varemos);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public PruebaSeleccion findPruebaSeleccionById(long pruebaSeleccionId) throws Exception
  {
    try {
      this.txn.open();
      PruebaSeleccion pruebaSeleccion = this.seleccionBusiness
        .findPruebaSeleccionById(pruebaSeleccionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(pruebaSeleccion);
      return pruebaSeleccion;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public BaremoSeleccion findBaremoSeleccionById(long baremoSeleccionId) throws Exception
  {
    try {
      this.txn.open();
      BaremoSeleccion baremoSeleccion = this.seleccionBusiness
        .findBaremoSeleccionById(baremoSeleccionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(baremoSeleccion);
      return baremoSeleccion;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Varemos findVaremosById(long varemosId) throws Exception {
    try {
      this.txn.open();
      Varemos varemos = this.seleccionBusiness.findVaremosById(varemosId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(varemos);
      return varemos;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllPruebaSeleccion() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllPruebaSeleccion();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllBaremoSeleccion() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllBaremoSeleccion();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllVaremos() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllVaremos();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findPruebaSeleccionByCodPrueba(String codPrueba) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findPruebaSeleccionByCodPrueba(codPrueba);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findBaremoSeleccionByCodBaremo(String codBaremo) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findBaremoSeleccionByCodBaremo(codBaremo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findVaremosByCodVaremos(String codVaremos) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findVaremosByCodVaremos(codVaremos);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findPruebaSeleccionByNombre(String nombre) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findPruebaSeleccionByNombre(nombre);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findBaremoSeleccionByNombre(String nombre) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findBaremoSeleccionByNombre(nombre);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findVaremosByNombre(String nombre) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness.findVaremosByNombre(nombre);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addPostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.addPostuladoConcurso(postuladoConcurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updatePostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.updatePostuladoConcurso(postuladoConcurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deletePostuladoConcurso(PostuladoConcurso postuladoConcurso) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.deletePostuladoConcurso(postuladoConcurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public PostuladoConcurso findPostuladoConcursoById(long postuladoConcursoId) throws Exception
  {
    try {
      this.txn.open();
      PostuladoConcurso postuladoConcurso = this.seleccionBusiness
        .findPostuladoConcursoById(postuladoConcursoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(postuladoConcurso);
      return postuladoConcurso;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllPostuladoConcurso() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllPostuladoConcurso();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findPostuladoConcursoByConcursoCargo(long idConcursoCargo) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness
        .findPostuladoConcursoByConcursoCargo(idConcursoCargo);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addPostuladoExterno(PostuladoExterno postuladoExterno) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.addPostuladoExterno(postuladoExterno);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updatePostuladoExterno(PostuladoExterno postuladoExterno) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.updatePostuladoExterno(postuladoExterno);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deletePostuladoExterno(PostuladoExterno postuladoExterno) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.deletePostuladoExterno(postuladoExterno);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public PostuladoExterno findPostuladoExternoById(long postuladoExternoId) throws Exception
  {
    try {
      this.txn.open();
      PostuladoExterno postuladoExterno = this.seleccionBusiness
        .findPostuladoExternoById(postuladoExternoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(postuladoExterno);
      return postuladoExterno;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllPostuladoExterno() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllPostuladoExterno();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findPostuladoExternoByCedula(int cedula) throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findPostuladoExternoByCedula(cedula);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findPostuladoExternoByPrimerApellido(String primerApellido) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness
        .findPostuladoExternoByPrimerApellido(primerApellido);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addPruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness.addPruebaPreseleccionado(pruebaPreseleccionado);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updatePruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness
        .updatePruebaPreseleccionado(pruebaPreseleccionado);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deletePruebaPreseleccionado(PruebaPreseleccionado pruebaPreseleccionado) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness
        .deletePruebaPreseleccionado(pruebaPreseleccionado);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public PruebaPreseleccionado findPruebaPreseleccionadoById(long pruebaPreseleccionadoId) throws Exception
  {
    try {
      this.txn.open();
      PruebaPreseleccionado pruebaPreseleccionado = this.seleccionBusiness
        .findPruebaPreseleccionadoById(pruebaPreseleccionadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(pruebaPreseleccionado);
      return pruebaPreseleccionado;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllPruebaPreseleccionado() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllPruebaPreseleccionado();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findPruebaPreseleccionadoByPostuladoConcurso(long idPostuladoConcurso) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness
        .findPruebaPreseleccionadoByPostuladoConcurso(idPostuladoConcurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void addBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado) throws Exception
  {
    try
    {
      this.txn.open();
      this.seleccionBusiness.addBaremoPreseleccionado(baremoPreseleccionado);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void updateBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness
        .updateBaremoPreseleccionado(baremoPreseleccionado);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public void deleteBaremoPreseleccionado(BaremoPreseleccionado baremoPreseleccionado) throws Exception
  {
    try {
      this.txn.open();
      this.seleccionBusiness
        .deleteBaremoPreseleccionado(baremoPreseleccionado);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public BaremoPreseleccionado findBaremoPreseleccionadoById(long baremoPreseleccionadoId) throws Exception
  {
    try {
      this.txn.open();
      BaremoPreseleccionado baremoPreseleccionado = this.seleccionBusiness
        .findBaremoPreseleccionadoById(baremoPreseleccionadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(baremoPreseleccionado);
      return baremoPreseleccionado;
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findAllBaremoPreseleccionado() throws Exception {
    try {
      this.txn.open();
      return this.seleccionBusiness.findAllBaremoPreseleccionado();
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }

  public Collection findBaremoPreseleccionadoByPostuladoConcurso(long idPostuladoConcurso) throws Exception
  {
    try {
      this.txn.open();
      return this.seleccionBusiness
        .findBaremoPreseleccionadoByPostuladoConcurso(idPostuladoConcurso);
    } finally {
      if (this.txn != null)
        this.txn.close();
    }
  }
}