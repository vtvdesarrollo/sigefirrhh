package sigefirrhh.bienestar.utiles;

import eforserver.business.AbstractFacade;

public class UtilesNoGenFacade extends AbstractFacade
{
  private UtilesNoGenBusiness utilesNoGenBusiness = new UtilesNoGenBusiness();

  public void asignar_utiles(long idTipoPersonal, String proceso) throws Exception {
    this.utilesNoGenBusiness.asignarUtiles(idTipoPersonal, proceso);
  }
}