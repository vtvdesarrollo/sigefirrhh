package sigefirrhh.bienestar.utiles;

import eforserver.presentation.ListUtil;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class AsignarUtilesForm
{
  static Logger log = Logger.getLogger(AsignarUtilesForm.class.getName());
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private UtilesNoGenFacade utilesNoGenFacade;
  private LoginSession login;
  private String proceso = "P";

  public AsignarUtilesForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.utilesNoGenFacade = new UtilesNoGenFacade();
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByPrestaciones(
        this.login.getOrganismo().getIdOrganismo(), this.login.getIdUsuario(), this.login.getAdministrador());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String execute() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.utilesNoGenFacade.asignar_utiles(this.idTipoPersonal, this.proceso);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_process", new FacesMessage("Se asignaron útiles escolares con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public String getProceso() {
    return this.proceso;
  }

  public void setProceso(String proceso) {
    this.proceso = proceso;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }

  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
}