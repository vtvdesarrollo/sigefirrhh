package sigefirrhh.bienestar.guarderia;

import eforserver.business.AbstractBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.apache.log4j.Logger;

public class GuarderiaNoGenBusiness extends AbstractBusiness
{
  Logger log = Logger.getLogger(GuarderiaNoGenBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public void pagarGuarderias(long idTipoPersonal, java.util.Date fecha, String tipoConcepto, String inscripcion) throws Exception {
    Connection connection = null;
    PreparedStatement st = null;

    java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      sql.append("select pagar_guarderias(?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + fecha);
      this.log.error("3- " + tipoConcepto);
      this.log.error("4- " + inscripcion);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setDate(2, fechaSql);
      st.setString(3, tipoConcepto);
      st.setString(4, inscripcion);

      st.executeQuery();

      connection.commit();
      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        }
    }
  }
}