package sigefirrhh.bienestar.guarderia;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.FamiliarBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class GuarderiaFamiliarBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GuarderiaFamiliar guarderiaFamiliarNew = 
      (GuarderiaFamiliar)BeanUtils.cloneBean(
      guarderiaFamiliar);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (guarderiaFamiliarNew.getPersonal() != null) {
      guarderiaFamiliarNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        guarderiaFamiliarNew.getPersonal().getIdPersonal()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (guarderiaFamiliarNew.getFamiliar() != null) {
      guarderiaFamiliarNew.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        guarderiaFamiliarNew.getFamiliar().getIdFamiliar()));
    }

    GuarderiaBeanBusiness guarderiaBeanBusiness = new GuarderiaBeanBusiness();

    if (guarderiaFamiliarNew.getGuarderia() != null) {
      guarderiaFamiliarNew.setGuarderia(
        guarderiaBeanBusiness.findGuarderiaById(
        guarderiaFamiliarNew.getGuarderia().getIdGuarderia()));
    }
    pm.makePersistent(guarderiaFamiliarNew);
  }

  public void updateGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar) throws Exception
  {
    GuarderiaFamiliar guarderiaFamiliarModify = 
      findGuarderiaFamiliarById(guarderiaFamiliar.getIdGuarderiaFamiliar());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (guarderiaFamiliar.getPersonal() != null) {
      guarderiaFamiliar.setPersonal(
        personalBeanBusiness.findPersonalById(
        guarderiaFamiliar.getPersonal().getIdPersonal()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (guarderiaFamiliar.getFamiliar() != null) {
      guarderiaFamiliar.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        guarderiaFamiliar.getFamiliar().getIdFamiliar()));
    }

    GuarderiaBeanBusiness guarderiaBeanBusiness = new GuarderiaBeanBusiness();

    if (guarderiaFamiliar.getGuarderia() != null) {
      guarderiaFamiliar.setGuarderia(
        guarderiaBeanBusiness.findGuarderiaById(
        guarderiaFamiliar.getGuarderia().getIdGuarderia()));
    }

    BeanUtils.copyProperties(guarderiaFamiliarModify, guarderiaFamiliar);
  }

  public void deleteGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GuarderiaFamiliar guarderiaFamiliarDelete = 
      findGuarderiaFamiliarById(guarderiaFamiliar.getIdGuarderiaFamiliar());
    pm.deletePersistent(guarderiaFamiliarDelete);
  }

  public GuarderiaFamiliar findGuarderiaFamiliarById(long idGuarderiaFamiliar) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGuarderiaFamiliar == pIdGuarderiaFamiliar";
    Query query = pm.newQuery(GuarderiaFamiliar.class, filter);

    query.declareParameters("long pIdGuarderiaFamiliar");

    parameters.put("pIdGuarderiaFamiliar", new Long(idGuarderiaFamiliar));

    Collection colGuarderiaFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGuarderiaFamiliar.iterator();
    return (GuarderiaFamiliar)iterator.next();
  }

  public Collection findGuarderiaFamiliarAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent guarderiaFamiliarExtent = pm.getExtent(
      GuarderiaFamiliar.class, true);
    Query query = pm.newQuery(guarderiaFamiliarExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(GuarderiaFamiliar.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colGuarderiaFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGuarderiaFamiliar);

    return colGuarderiaFamiliar;
  }
}