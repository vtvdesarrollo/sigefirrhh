package sigefirrhh.bienestar.guarderia;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class PagarGuarderiaForm
{
  static Logger log = Logger.getLogger(PagarGuarderiaForm.class.getName());
  private String selectTipoPersonal;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private GuarderiaNoGenFacade guarderiaFacade;
  private LoginSession login;
  private Date fecha;
  private String tipoConcepto = "V";
  private String inscripcion = "N";

  public PagarGuarderiaForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.guarderiaFacade = new GuarderiaNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.fecha = Calendar.getInstance().getTime();

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String execute() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.guarderiaFacade.pagarGuarderias(this.idTipoPersonal, this.fecha, this.tipoConcepto, this.inscripcion);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_process", new FacesMessage("Se registró con éxito el pago de guarderías"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
    this.idTipoPersonal = Integer.parseInt(string);
  }

  public Date getFecha() {
    return this.fecha;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }

  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getInscripcion() {
    return this.inscripcion;
  }

  public void setInscripcion(String inscripcion) {
    this.inscripcion = inscripcion;
  }

  public String getTipoConcepto() {
    return this.tipoConcepto;
  }

  public void setTipoConcepto(String tipoConcepto) {
    this.tipoConcepto = tipoConcepto;
  }
}