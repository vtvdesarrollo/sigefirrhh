package sigefirrhh.bienestar.guarderia;

import java.io.Serializable;

public class GuarderiaPK
  implements Serializable
{
  public long idGuarderia;

  public GuarderiaPK()
  {
  }

  public GuarderiaPK(long idGuarderia)
  {
    this.idGuarderia = idGuarderia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GuarderiaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GuarderiaPK thatPK)
  {
    return 
      this.idGuarderia == thatPK.idGuarderia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGuarderia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGuarderia);
  }
}