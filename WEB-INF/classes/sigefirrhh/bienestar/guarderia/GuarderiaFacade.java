package sigefirrhh.bienestar.guarderia;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class GuarderiaFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private GuarderiaBusiness guarderiaBusiness = new GuarderiaBusiness();

  public void addGuarderia(Guarderia guarderia)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.guarderiaBusiness.addGuarderia(guarderia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGuarderia(Guarderia guarderia) throws Exception
  {
    try { this.txn.open();
      this.guarderiaBusiness.updateGuarderia(guarderia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGuarderia(Guarderia guarderia) throws Exception
  {
    try { this.txn.open();
      this.guarderiaBusiness.deleteGuarderia(guarderia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Guarderia findGuarderiaById(long guarderiaId) throws Exception
  {
    try { this.txn.open();
      Guarderia guarderia = 
        this.guarderiaBusiness.findGuarderiaById(guarderiaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(guarderia);
      return guarderia;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGuarderia() throws Exception
  {
    try { this.txn.open();
      return this.guarderiaBusiness.findAllGuarderia();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGuarderiaByCodGuarderia(String codGuarderia)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.guarderiaBusiness.findGuarderiaByCodGuarderia(codGuarderia);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGuarderiaByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.guarderiaBusiness.findGuarderiaByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.guarderiaBusiness.addGuarderiaFamiliar(guarderiaFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar) throws Exception
  {
    try { this.txn.open();
      this.guarderiaBusiness.updateGuarderiaFamiliar(guarderiaFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar) throws Exception
  {
    try { this.txn.open();
      this.guarderiaBusiness.deleteGuarderiaFamiliar(guarderiaFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GuarderiaFamiliar findGuarderiaFamiliarById(long guarderiaFamiliarId) throws Exception
  {
    try { this.txn.open();
      GuarderiaFamiliar guarderiaFamiliar = 
        this.guarderiaBusiness.findGuarderiaFamiliarById(guarderiaFamiliarId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(guarderiaFamiliar);
      return guarderiaFamiliar;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGuarderiaFamiliar() throws Exception
  {
    try { this.txn.open();
      return this.guarderiaBusiness.findAllGuarderiaFamiliar();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGuarderiaFamiliarByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.guarderiaBusiness.findGuarderiaFamiliarByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}