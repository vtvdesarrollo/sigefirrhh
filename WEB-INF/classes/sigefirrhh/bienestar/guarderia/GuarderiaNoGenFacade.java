package sigefirrhh.bienestar.guarderia;

import eforserver.business.AbstractFacade;
import java.util.Date;

public class GuarderiaNoGenFacade extends AbstractFacade
{
  private GuarderiaNoGenBusiness guarderiaNoGenBusiness = new GuarderiaNoGenBusiness();

  public void pagarGuarderias(long idTipoPersonal, Date fecha, String tipoConcepto, String inscripcion) throws Exception {
    this.guarderiaNoGenBusiness.pagarGuarderias(idTipoPersonal, fecha, tipoConcepto, inscripcion);
  }
}