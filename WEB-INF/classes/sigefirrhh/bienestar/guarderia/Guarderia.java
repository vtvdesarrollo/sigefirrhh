package sigefirrhh.bienestar.guarderia;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.ubicacion.Ciudad;

public class Guarderia
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_MES;
  private long idGuarderia;
  private String codGuarderia;
  private String nombre;
  private String direccion;
  private Ciudad ciudad;
  private String telefono1;
  private String telefono2;
  private String personaContacto;
  private String numeroRif;
  private String permisoInam;
  private String beneficiarioCheque;
  private String seguroMedico;
  private double montoInscripcion;
  private double montoMensualidad;
  private String mes;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "beneficiarioCheque", "ciudad", "codGuarderia", "direccion", "idGuarderia", "mes", "montoInscripcion", "montoMensualidad", "nombre", "numeroRif", "permisoInam", "personaContacto", "seguroMedico", "telefono1", "telefono2" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.ubicacion.Ciudad"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.guarderia.Guarderia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Guarderia());

    LISTA_SINO = 
      new LinkedHashMap();
    LISTA_MES = 
      new LinkedHashMap();
    LISTA_MES.put("1", "ENERO");
    LISTA_MES.put("2", "FEBRERO");
    LISTA_MES.put("3", "MARZO");
    LISTA_MES.put("4", "ABRIL");
    LISTA_MES.put("5", "MAYO");
    LISTA_MES.put("6", "JUNIO");
    LISTA_MES.put("7", "JULIO");
    LISTA_MES.put("8", "AGOSTO");
    LISTA_MES.put("9", "SEPTIEMBRE");
    LISTA_MES.put("10", "OCTUBRE");
    LISTA_MES.put("11", "NOVIEMBRE");
    LISTA_MES.put("12", "DICIEMBRE");
    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public Guarderia()
  {
    jdoSetseguroMedico(this, "S");

    jdoSetmontoInscripcion(this, 0.0D);

    jdoSetmontoMensualidad(this, 0.0D);

    jdoSetmes(this, "9");
  }
  public String toString() {
    return jdoGetnombre(this);
  }

  public String getBeneficiarioCheque()
  {
    return jdoGetbeneficiarioCheque(this);
  }

  public Ciudad getCiudad()
  {
    return jdoGetciudad(this);
  }

  public String getCodGuarderia()
  {
    return jdoGetcodGuarderia(this);
  }

  public String getDireccion()
  {
    return jdoGetdireccion(this);
  }

  public long getIdGuarderia()
  {
    return jdoGetidGuarderia(this);
  }

  public double getMontoInscripcion()
  {
    return jdoGetmontoInscripcion(this);
  }

  public double getMontoMensualidad()
  {
    return jdoGetmontoMensualidad(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public String getNumeroRif()
  {
    return jdoGetnumeroRif(this);
  }

  public String getPermisoInam()
  {
    return jdoGetpermisoInam(this);
  }

  public String getPersonaContacto()
  {
    return jdoGetpersonaContacto(this);
  }

  public String getSeguroMedico()
  {
    return jdoGetseguroMedico(this);
  }

  public String getTelefono1()
  {
    return jdoGettelefono1(this);
  }

  public String getTelefono2()
  {
    return jdoGettelefono2(this);
  }

  public void setBeneficiarioCheque(String string)
  {
    jdoSetbeneficiarioCheque(this, string);
  }

  public void setCiudad(Ciudad ciudad)
  {
    jdoSetciudad(this, ciudad);
  }

  public void setCodGuarderia(String string)
  {
    jdoSetcodGuarderia(this, string);
  }

  public void setDireccion(String string)
  {
    jdoSetdireccion(this, string);
  }

  public void setIdGuarderia(long l)
  {
    jdoSetidGuarderia(this, l);
  }

  public void setMontoInscripcion(double d)
  {
    jdoSetmontoInscripcion(this, d);
  }

  public void setMontoMensualidad(double d)
  {
    jdoSetmontoMensualidad(this, d);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public void setNumeroRif(String string)
  {
    jdoSetnumeroRif(this, string);
  }

  public void setPermisoInam(String string)
  {
    jdoSetpermisoInam(this, string);
  }

  public void setPersonaContacto(String string)
  {
    jdoSetpersonaContacto(this, string);
  }

  public void setSeguroMedico(String string)
  {
    jdoSetseguroMedico(this, string);
  }

  public void setTelefono1(String string)
  {
    jdoSettelefono1(this, string);
  }

  public void setTelefono2(String string)
  {
    jdoSettelefono2(this, string);
  }

  public String getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(String string)
  {
    jdoSetmes(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Guarderia localGuarderia = new Guarderia();
    localGuarderia.jdoFlags = 1;
    localGuarderia.jdoStateManager = paramStateManager;
    return localGuarderia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Guarderia localGuarderia = new Guarderia();
    localGuarderia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGuarderia.jdoFlags = 1;
    localGuarderia.jdoStateManager = paramStateManager;
    return localGuarderia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.beneficiarioCheque);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.ciudad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codGuarderia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.direccion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGuarderia);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.mes);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoInscripcion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoMensualidad);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroRif);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.permisoInam);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.personaContacto);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.seguroMedico);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono1);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono2);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.beneficiarioCheque = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ciudad = ((Ciudad)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGuarderia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.direccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGuarderia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoInscripcion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoMensualidad = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroRif = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.permisoInam = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personaContacto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.seguroMedico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono1 = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono2 = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Guarderia paramGuarderia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.beneficiarioCheque = paramGuarderia.beneficiarioCheque;
      return;
    case 1:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.ciudad = paramGuarderia.ciudad;
      return;
    case 2:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.codGuarderia = paramGuarderia.codGuarderia;
      return;
    case 3:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.direccion = paramGuarderia.direccion;
      return;
    case 4:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.idGuarderia = paramGuarderia.idGuarderia;
      return;
    case 5:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramGuarderia.mes;
      return;
    case 6:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.montoInscripcion = paramGuarderia.montoInscripcion;
      return;
    case 7:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.montoMensualidad = paramGuarderia.montoMensualidad;
      return;
    case 8:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGuarderia.nombre;
      return;
    case 9:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.numeroRif = paramGuarderia.numeroRif;
      return;
    case 10:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.permisoInam = paramGuarderia.permisoInam;
      return;
    case 11:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.personaContacto = paramGuarderia.personaContacto;
      return;
    case 12:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.seguroMedico = paramGuarderia.seguroMedico;
      return;
    case 13:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.telefono1 = paramGuarderia.telefono1;
      return;
    case 14:
      if (paramGuarderia == null)
        throw new IllegalArgumentException("arg1");
      this.telefono2 = paramGuarderia.telefono2;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Guarderia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Guarderia localGuarderia = (Guarderia)paramObject;
    if (localGuarderia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGuarderia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GuarderiaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GuarderiaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GuarderiaPK))
      throw new IllegalArgumentException("arg1");
    GuarderiaPK localGuarderiaPK = (GuarderiaPK)paramObject;
    localGuarderiaPK.idGuarderia = this.idGuarderia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GuarderiaPK))
      throw new IllegalArgumentException("arg1");
    GuarderiaPK localGuarderiaPK = (GuarderiaPK)paramObject;
    this.idGuarderia = localGuarderiaPK.idGuarderia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GuarderiaPK))
      throw new IllegalArgumentException("arg2");
    GuarderiaPK localGuarderiaPK = (GuarderiaPK)paramObject;
    localGuarderiaPK.idGuarderia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GuarderiaPK))
      throw new IllegalArgumentException("arg2");
    GuarderiaPK localGuarderiaPK = (GuarderiaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localGuarderiaPK.idGuarderia);
  }

  private static final String jdoGetbeneficiarioCheque(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.beneficiarioCheque;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.beneficiarioCheque;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 0))
      return paramGuarderia.beneficiarioCheque;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 0, paramGuarderia.beneficiarioCheque);
  }

  private static final void jdoSetbeneficiarioCheque(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.beneficiarioCheque = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.beneficiarioCheque = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 0, paramGuarderia.beneficiarioCheque, paramString);
  }

  private static final Ciudad jdoGetciudad(Guarderia paramGuarderia)
  {
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.ciudad;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 1))
      return paramGuarderia.ciudad;
    return (Ciudad)localStateManager.getObjectField(paramGuarderia, jdoInheritedFieldCount + 1, paramGuarderia.ciudad);
  }

  private static final void jdoSetciudad(Guarderia paramGuarderia, Ciudad paramCiudad)
  {
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.ciudad = paramCiudad;
      return;
    }
    localStateManager.setObjectField(paramGuarderia, jdoInheritedFieldCount + 1, paramGuarderia.ciudad, paramCiudad);
  }

  private static final String jdoGetcodGuarderia(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.codGuarderia;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.codGuarderia;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 2))
      return paramGuarderia.codGuarderia;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 2, paramGuarderia.codGuarderia);
  }

  private static final void jdoSetcodGuarderia(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.codGuarderia = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.codGuarderia = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 2, paramGuarderia.codGuarderia, paramString);
  }

  private static final String jdoGetdireccion(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.direccion;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.direccion;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 3))
      return paramGuarderia.direccion;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 3, paramGuarderia.direccion);
  }

  private static final void jdoSetdireccion(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.direccion = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.direccion = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 3, paramGuarderia.direccion, paramString);
  }

  private static final long jdoGetidGuarderia(Guarderia paramGuarderia)
  {
    return paramGuarderia.idGuarderia;
  }

  private static final void jdoSetidGuarderia(Guarderia paramGuarderia, long paramLong)
  {
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.idGuarderia = paramLong;
      return;
    }
    localStateManager.setLongField(paramGuarderia, jdoInheritedFieldCount + 4, paramGuarderia.idGuarderia, paramLong);
  }

  private static final String jdoGetmes(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.mes;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.mes;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 5))
      return paramGuarderia.mes;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 5, paramGuarderia.mes);
  }

  private static final void jdoSetmes(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.mes = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.mes = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 5, paramGuarderia.mes, paramString);
  }

  private static final double jdoGetmontoInscripcion(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.montoInscripcion;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.montoInscripcion;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 6))
      return paramGuarderia.montoInscripcion;
    return localStateManager.getDoubleField(paramGuarderia, jdoInheritedFieldCount + 6, paramGuarderia.montoInscripcion);
  }

  private static final void jdoSetmontoInscripcion(Guarderia paramGuarderia, double paramDouble)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.montoInscripcion = paramDouble;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.montoInscripcion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramGuarderia, jdoInheritedFieldCount + 6, paramGuarderia.montoInscripcion, paramDouble);
  }

  private static final double jdoGetmontoMensualidad(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.montoMensualidad;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.montoMensualidad;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 7))
      return paramGuarderia.montoMensualidad;
    return localStateManager.getDoubleField(paramGuarderia, jdoInheritedFieldCount + 7, paramGuarderia.montoMensualidad);
  }

  private static final void jdoSetmontoMensualidad(Guarderia paramGuarderia, double paramDouble)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.montoMensualidad = paramDouble;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.montoMensualidad = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramGuarderia, jdoInheritedFieldCount + 7, paramGuarderia.montoMensualidad, paramDouble);
  }

  private static final String jdoGetnombre(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.nombre;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.nombre;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 8))
      return paramGuarderia.nombre;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 8, paramGuarderia.nombre);
  }

  private static final void jdoSetnombre(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 8, paramGuarderia.nombre, paramString);
  }

  private static final String jdoGetnumeroRif(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.numeroRif;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.numeroRif;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 9))
      return paramGuarderia.numeroRif;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 9, paramGuarderia.numeroRif);
  }

  private static final void jdoSetnumeroRif(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.numeroRif = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.numeroRif = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 9, paramGuarderia.numeroRif, paramString);
  }

  private static final String jdoGetpermisoInam(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.permisoInam;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.permisoInam;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 10))
      return paramGuarderia.permisoInam;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 10, paramGuarderia.permisoInam);
  }

  private static final void jdoSetpermisoInam(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.permisoInam = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.permisoInam = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 10, paramGuarderia.permisoInam, paramString);
  }

  private static final String jdoGetpersonaContacto(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.personaContacto;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.personaContacto;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 11))
      return paramGuarderia.personaContacto;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 11, paramGuarderia.personaContacto);
  }

  private static final void jdoSetpersonaContacto(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.personaContacto = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.personaContacto = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 11, paramGuarderia.personaContacto, paramString);
  }

  private static final String jdoGetseguroMedico(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.seguroMedico;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.seguroMedico;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 12))
      return paramGuarderia.seguroMedico;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 12, paramGuarderia.seguroMedico);
  }

  private static final void jdoSetseguroMedico(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.seguroMedico = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.seguroMedico = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 12, paramGuarderia.seguroMedico, paramString);
  }

  private static final String jdoGettelefono1(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.telefono1;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.telefono1;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 13))
      return paramGuarderia.telefono1;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 13, paramGuarderia.telefono1);
  }

  private static final void jdoSettelefono1(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.telefono1 = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.telefono1 = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 13, paramGuarderia.telefono1, paramString);
  }

  private static final String jdoGettelefono2(Guarderia paramGuarderia)
  {
    if (paramGuarderia.jdoFlags <= 0)
      return paramGuarderia.telefono2;
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderia.telefono2;
    if (localStateManager.isLoaded(paramGuarderia, jdoInheritedFieldCount + 14))
      return paramGuarderia.telefono2;
    return localStateManager.getStringField(paramGuarderia, jdoInheritedFieldCount + 14, paramGuarderia.telefono2);
  }

  private static final void jdoSettelefono2(Guarderia paramGuarderia, String paramString)
  {
    if (paramGuarderia.jdoFlags == 0)
    {
      paramGuarderia.telefono2 = paramString;
      return;
    }
    StateManager localStateManager = paramGuarderia.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderia.telefono2 = paramString;
      return;
    }
    localStateManager.setStringField(paramGuarderia, jdoInheritedFieldCount + 14, paramGuarderia.telefono2, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}