package sigefirrhh.bienestar.guarderia;

import java.io.Serializable;

public class GuarderiaFamiliarPK
  implements Serializable
{
  public long idGuarderiaFamiliar;

  public GuarderiaFamiliarPK()
  {
  }

  public GuarderiaFamiliarPK(long idGuarderiaFamiliar)
  {
    this.idGuarderiaFamiliar = idGuarderiaFamiliar;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GuarderiaFamiliarPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GuarderiaFamiliarPK thatPK)
  {
    return 
      this.idGuarderiaFamiliar == thatPK.idGuarderiaFamiliar;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGuarderiaFamiliar)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGuarderiaFamiliar);
  }
}