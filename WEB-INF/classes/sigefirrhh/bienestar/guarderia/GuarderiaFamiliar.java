package sigefirrhh.bienestar.guarderia;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class GuarderiaFamiliar
  implements Serializable, PersistenceCapable
{
  private long idGuarderiaFamiliar;
  private Date fechaRegistro;
  private Personal personal;
  private Familiar familiar;
  private Guarderia guarderia;
  private double montoInscripcion;
  private double montoMensualidad;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "familiar", "fechaRegistro", "guarderia", "idGuarderiaFamiliar", "montoInscripcion", "montoMensualidad", "personal" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.bienestar.guarderia.Guarderia"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal") };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public GuarderiaFamiliar()
  {
    jdoSetmontoInscripcion(this, 0.0D);

    jdoSetmontoMensualidad(this, 0.0D);
  }

  public String toString() {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetmontoMensualidad(this));

    return jdoGetfamiliar(this).getPrimerNombre() + ", " + jdoGetfamiliar(this).getPrimerApellido() + " - " + a;
  }

  public Familiar getFamiliar() {
    return jdoGetfamiliar(this);
  }

  public void setFamiliar(Familiar familiar) {
    jdoSetfamiliar(this, familiar);
  }

  public Date getFechaRegistro() {
    return jdoGetfechaRegistro(this);
  }

  public void setFechaRegistro(Date fechaRegistro) {
    jdoSetfechaRegistro(this, fechaRegistro);
  }

  public Guarderia getGuarderia() {
    return jdoGetguarderia(this);
  }

  public void setGuarderia(Guarderia guarderia) {
    jdoSetguarderia(this, guarderia);
  }

  public long getIdGuarderiaFamiliar() {
    return jdoGetidGuarderiaFamiliar(this);
  }

  public void setIdGuarderiaFamiliar(long idGuarderiaFamiliar) {
    jdoSetidGuarderiaFamiliar(this, idGuarderiaFamiliar);
  }

  public double getMontoInscripcion() {
    return jdoGetmontoInscripcion(this);
  }

  public void setMontoInscripcion(double montoInscripcion) {
    jdoSetmontoInscripcion(this, montoInscripcion);
  }

  public double getMontoMensualidad() {
    return jdoGetmontoMensualidad(this);
  }

  public void setMontoMensualidad(double montoMensualidad) {
    jdoSetmontoMensualidad(this, montoMensualidad);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.guarderia.GuarderiaFamiliar"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GuarderiaFamiliar());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GuarderiaFamiliar localGuarderiaFamiliar = new GuarderiaFamiliar();
    localGuarderiaFamiliar.jdoFlags = 1;
    localGuarderiaFamiliar.jdoStateManager = paramStateManager;
    return localGuarderiaFamiliar;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GuarderiaFamiliar localGuarderiaFamiliar = new GuarderiaFamiliar();
    localGuarderiaFamiliar.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGuarderiaFamiliar.jdoFlags = 1;
    localGuarderiaFamiliar.jdoStateManager = paramStateManager;
    return localGuarderiaFamiliar;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRegistro);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.guarderia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGuarderiaFamiliar);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoInscripcion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoMensualidad);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRegistro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.guarderia = ((Guarderia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGuarderiaFamiliar = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoInscripcion = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoMensualidad = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GuarderiaFamiliar paramGuarderiaFamiliar, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGuarderiaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramGuarderiaFamiliar.familiar;
      return;
    case 1:
      if (paramGuarderiaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRegistro = paramGuarderiaFamiliar.fechaRegistro;
      return;
    case 2:
      if (paramGuarderiaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.guarderia = paramGuarderiaFamiliar.guarderia;
      return;
    case 3:
      if (paramGuarderiaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.idGuarderiaFamiliar = paramGuarderiaFamiliar.idGuarderiaFamiliar;
      return;
    case 4:
      if (paramGuarderiaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.montoInscripcion = paramGuarderiaFamiliar.montoInscripcion;
      return;
    case 5:
      if (paramGuarderiaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.montoMensualidad = paramGuarderiaFamiliar.montoMensualidad;
      return;
    case 6:
      if (paramGuarderiaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramGuarderiaFamiliar.personal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GuarderiaFamiliar))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GuarderiaFamiliar localGuarderiaFamiliar = (GuarderiaFamiliar)paramObject;
    if (localGuarderiaFamiliar.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGuarderiaFamiliar, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GuarderiaFamiliarPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GuarderiaFamiliarPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GuarderiaFamiliarPK))
      throw new IllegalArgumentException("arg1");
    GuarderiaFamiliarPK localGuarderiaFamiliarPK = (GuarderiaFamiliarPK)paramObject;
    localGuarderiaFamiliarPK.idGuarderiaFamiliar = this.idGuarderiaFamiliar;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GuarderiaFamiliarPK))
      throw new IllegalArgumentException("arg1");
    GuarderiaFamiliarPK localGuarderiaFamiliarPK = (GuarderiaFamiliarPK)paramObject;
    this.idGuarderiaFamiliar = localGuarderiaFamiliarPK.idGuarderiaFamiliar;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GuarderiaFamiliarPK))
      throw new IllegalArgumentException("arg2");
    GuarderiaFamiliarPK localGuarderiaFamiliarPK = (GuarderiaFamiliarPK)paramObject;
    localGuarderiaFamiliarPK.idGuarderiaFamiliar = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GuarderiaFamiliarPK))
      throw new IllegalArgumentException("arg2");
    GuarderiaFamiliarPK localGuarderiaFamiliarPK = (GuarderiaFamiliarPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localGuarderiaFamiliarPK.idGuarderiaFamiliar);
  }

  private static final Familiar jdoGetfamiliar(GuarderiaFamiliar paramGuarderiaFamiliar)
  {
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderiaFamiliar.familiar;
    if (localStateManager.isLoaded(paramGuarderiaFamiliar, jdoInheritedFieldCount + 0))
      return paramGuarderiaFamiliar.familiar;
    return (Familiar)localStateManager.getObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 0, paramGuarderiaFamiliar.familiar);
  }

  private static final void jdoSetfamiliar(GuarderiaFamiliar paramGuarderiaFamiliar, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderiaFamiliar.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 0, paramGuarderiaFamiliar.familiar, paramFamiliar);
  }

  private static final Date jdoGetfechaRegistro(GuarderiaFamiliar paramGuarderiaFamiliar)
  {
    if (paramGuarderiaFamiliar.jdoFlags <= 0)
      return paramGuarderiaFamiliar.fechaRegistro;
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderiaFamiliar.fechaRegistro;
    if (localStateManager.isLoaded(paramGuarderiaFamiliar, jdoInheritedFieldCount + 1))
      return paramGuarderiaFamiliar.fechaRegistro;
    return (Date)localStateManager.getObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 1, paramGuarderiaFamiliar.fechaRegistro);
  }

  private static final void jdoSetfechaRegistro(GuarderiaFamiliar paramGuarderiaFamiliar, Date paramDate)
  {
    if (paramGuarderiaFamiliar.jdoFlags == 0)
    {
      paramGuarderiaFamiliar.fechaRegistro = paramDate;
      return;
    }
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderiaFamiliar.fechaRegistro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 1, paramGuarderiaFamiliar.fechaRegistro, paramDate);
  }

  private static final Guarderia jdoGetguarderia(GuarderiaFamiliar paramGuarderiaFamiliar)
  {
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderiaFamiliar.guarderia;
    if (localStateManager.isLoaded(paramGuarderiaFamiliar, jdoInheritedFieldCount + 2))
      return paramGuarderiaFamiliar.guarderia;
    return (Guarderia)localStateManager.getObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 2, paramGuarderiaFamiliar.guarderia);
  }

  private static final void jdoSetguarderia(GuarderiaFamiliar paramGuarderiaFamiliar, Guarderia paramGuarderia)
  {
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderiaFamiliar.guarderia = paramGuarderia;
      return;
    }
    localStateManager.setObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 2, paramGuarderiaFamiliar.guarderia, paramGuarderia);
  }

  private static final long jdoGetidGuarderiaFamiliar(GuarderiaFamiliar paramGuarderiaFamiliar)
  {
    return paramGuarderiaFamiliar.idGuarderiaFamiliar;
  }

  private static final void jdoSetidGuarderiaFamiliar(GuarderiaFamiliar paramGuarderiaFamiliar, long paramLong)
  {
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderiaFamiliar.idGuarderiaFamiliar = paramLong;
      return;
    }
    localStateManager.setLongField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 3, paramGuarderiaFamiliar.idGuarderiaFamiliar, paramLong);
  }

  private static final double jdoGetmontoInscripcion(GuarderiaFamiliar paramGuarderiaFamiliar)
  {
    if (paramGuarderiaFamiliar.jdoFlags <= 0)
      return paramGuarderiaFamiliar.montoInscripcion;
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderiaFamiliar.montoInscripcion;
    if (localStateManager.isLoaded(paramGuarderiaFamiliar, jdoInheritedFieldCount + 4))
      return paramGuarderiaFamiliar.montoInscripcion;
    return localStateManager.getDoubleField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 4, paramGuarderiaFamiliar.montoInscripcion);
  }

  private static final void jdoSetmontoInscripcion(GuarderiaFamiliar paramGuarderiaFamiliar, double paramDouble)
  {
    if (paramGuarderiaFamiliar.jdoFlags == 0)
    {
      paramGuarderiaFamiliar.montoInscripcion = paramDouble;
      return;
    }
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderiaFamiliar.montoInscripcion = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 4, paramGuarderiaFamiliar.montoInscripcion, paramDouble);
  }

  private static final double jdoGetmontoMensualidad(GuarderiaFamiliar paramGuarderiaFamiliar)
  {
    if (paramGuarderiaFamiliar.jdoFlags <= 0)
      return paramGuarderiaFamiliar.montoMensualidad;
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderiaFamiliar.montoMensualidad;
    if (localStateManager.isLoaded(paramGuarderiaFamiliar, jdoInheritedFieldCount + 5))
      return paramGuarderiaFamiliar.montoMensualidad;
    return localStateManager.getDoubleField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 5, paramGuarderiaFamiliar.montoMensualidad);
  }

  private static final void jdoSetmontoMensualidad(GuarderiaFamiliar paramGuarderiaFamiliar, double paramDouble)
  {
    if (paramGuarderiaFamiliar.jdoFlags == 0)
    {
      paramGuarderiaFamiliar.montoMensualidad = paramDouble;
      return;
    }
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderiaFamiliar.montoMensualidad = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 5, paramGuarderiaFamiliar.montoMensualidad, paramDouble);
  }

  private static final Personal jdoGetpersonal(GuarderiaFamiliar paramGuarderiaFamiliar)
  {
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramGuarderiaFamiliar.personal;
    if (localStateManager.isLoaded(paramGuarderiaFamiliar, jdoInheritedFieldCount + 6))
      return paramGuarderiaFamiliar.personal;
    return (Personal)localStateManager.getObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 6, paramGuarderiaFamiliar.personal);
  }

  private static final void jdoSetpersonal(GuarderiaFamiliar paramGuarderiaFamiliar, Personal paramPersonal)
  {
    StateManager localStateManager = paramGuarderiaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramGuarderiaFamiliar.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramGuarderiaFamiliar, jdoInheritedFieldCount + 6, paramGuarderiaFamiliar.personal, paramPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}