package sigefirrhh.bienestar.guarderia;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteNoGenFacade;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class GuarderiaFamiliarForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GuarderiaFamiliarForm.class.getName());

  private GuarderiaFamiliar guarderiaFamiliar = null;
  private Guarderia guarderia = null;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteNoGenFacade expedienteFacade = new ExpedienteNoGenFacade();
  private GuarderiaFacade guarderiaFacade = new GuarderiaFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colPersonal;
  private Collection colFamiliar;
  private Collection colGuarderia;
  private String selectPersonal;
  private String selectFamiliar;
  private String selectGuarderia;
  private Object stateResultPersonal = null;

  private Object stateResultGuarderiaFamiliarByPersonal = null;

  public String getSelectPersonal()
  {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.guarderiaFamiliar.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.guarderiaFamiliar.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public String getSelectFamiliar() {
    return this.selectFamiliar;
  }
  public void setSelectFamiliar(String valFamiliar) {
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    this.guarderiaFamiliar.setFamiliar(null);
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      if (String.valueOf(familiar.getIdFamiliar()).equals(
        valFamiliar)) {
        this.guarderiaFamiliar.setFamiliar(
          familiar);
        break;
      }
    }
    this.selectFamiliar = valFamiliar;
  }
  public String getSelectGuarderia() {
    return this.selectGuarderia;
  }

  public void changeGuarderia(ValueChangeEvent event) {
    log.error("En el metodo changeGuarderia");
    Iterator iterator = this.colGuarderia.iterator();
    Guarderia guarderia = null;
    long idGuarderia = Long.valueOf(
      (String)event.getNewValue()).longValue();

    log.error("IdGuarderia: " + idGuarderia);
    if (idGuarderia != 0L) {
      getGuarderiaFamiliar().setGuarderia(null);
      while (iterator.hasNext()) {
        log.error("En el while de la coleccion de guarderias");
        guarderia = (Guarderia)iterator.next();
        if (guarderia.getIdGuarderia() == idGuarderia) {
          log.error("Guarderia encontrada");
          getGuarderiaFamiliar().setGuarderia(guarderia);

          getGuarderiaFamiliar().setMontoInscripcion(guarderia.getMontoInscripcion());
          getGuarderiaFamiliar().setMontoMensualidad(guarderia.getMontoMensualidad());
          log.error("Monto Inscripcion: " + getGuarderiaFamiliar().getMontoInscripcion());
          log.error("Monto Mensualidad: " + getGuarderiaFamiliar().getMontoMensualidad());
          break;
        }
      }
    }
    setGuarderia(guarderia);
    goCancel();
  }

  public String goCancel() {
    return "cancel";
  }

  public void setSelectGuarderia(String valGuarderia) {
    this.selectGuarderia = valGuarderia;
  }
  public Collection getResult() {
    return this.result;
  }

  public GuarderiaFamiliar getGuarderiaFamiliar() {
    if (this.guarderiaFamiliar == null) {
      log.error("GuarderiaFamiliar en nulo");
      setGuarderiaFamiliar(new GuarderiaFamiliar());
    }
    return this.guarderiaFamiliar;
  }

  public GuarderiaFamiliarForm() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public Collection getColFamiliar()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      col.add(new SelectItem(
        String.valueOf(familiar.getIdFamiliar()), 
        familiar.toString()));
    }
    return col;
  }

  public Collection getColGuarderia()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGuarderia.iterator();
    Guarderia guarderia = null;
    while (iterator.hasNext()) {
      guarderia = (Guarderia)iterator.next();
      col.add(new SelectItem(
        String.valueOf(guarderia.getIdGuarderia()), 
        guarderia.toString()));
    }
    return col;
  }

  public void refresh()
  {
    log.error("En el metodo Refresh");
    try
    {
      this.colGuarderia = 
        this.guarderiaFacade.findAllGuarderia();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findGuarderiaFamiliarByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.guarderiaFacade.findGuarderiaFamiliarByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectGuarderiaFamiliar() {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectPersonal = null;
    this.selectFamiliar = null;
    this.selectGuarderia = null;

    long idGuarderiaFamiliar = 
      Long.parseLong((String)requestParameterMap.get("idGuarderiaFamiliar"));
    try
    {
      this.guarderiaFamiliar = 
        this.guarderiaFacade.findGuarderiaFamiliarById(
        idGuarderiaFamiliar);

      if (this.guarderiaFamiliar.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.guarderiaFamiliar.getPersonal().getIdPersonal());
      }
      if (this.guarderiaFamiliar.getFamiliar() != null) {
        this.selectFamiliar = 
          String.valueOf(this.guarderiaFamiliar.getFamiliar().getIdFamiliar());
      }
      if (this.guarderiaFamiliar.getGuarderia() != null)
        this.selectGuarderia = 
          String.valueOf(this.guarderiaFamiliar.getGuarderia().getIdGuarderia());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
      this.colFamiliar = this.expedienteFacade.findFamiliarByPersonalAndParentesco(idPersonal, "H");
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    log.error("En el metodo Save");
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.guarderiaFamiliar.getFechaRegistro() != null) && 
      (this.guarderiaFamiliar.getFechaRegistro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Registro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.guarderiaFamiliar.setPersonal(
          this.personal);
        this.guarderiaFacade.addGuarderiaFamiliar(
          this.guarderiaFamiliar);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.guarderiaFacade.updateGuarderiaFamiliar(
          this.guarderiaFamiliar);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    log.error("En el metodo DeleteOk");
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.guarderiaFacade.deleteGuarderiaFamiliar(
        this.guarderiaFamiliar);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }
  public String add() {
    log.error("En el metodo Add");
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectPersonal = null;

    this.selectFamiliar = null;

    this.selectGuarderia = null;

    this.guarderiaFamiliar = new GuarderiaFamiliar();

    this.guarderia = this.guarderiaFamiliar.getGuarderia();

    this.guarderiaFamiliar.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.guarderiaFamiliar.setIdGuarderiaFamiliar(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.guarderia.GuarderiaFamiliar"));

    return null;
  }

  public String abort()
  {
    log.error("En el metodo abort");
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.guarderiaFamiliar = new GuarderiaFamiliar();
    this.guarderia = this.guarderiaFamiliar.getGuarderia();

    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    log.error("En el metodo abortUpdate");
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.guarderiaFamiliar = new GuarderiaFamiliar();
    this.guarderia = this.guarderiaFamiliar.getGuarderia();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (((this.adding) || (this.editing)) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }
  public void setGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar) {
    this.guarderiaFamiliar = guarderiaFamiliar;
    this.guarderia = this.guarderiaFamiliar.getGuarderia();
  }
  public Guarderia getGuarderia() {
    if (this.guarderia == null) {
      this.guarderia = new Guarderia();
    }
    return this.guarderia;
  }
  public void setGuarderia(Guarderia guarderia) {
    this.guarderia = guarderia;
  }

  public double getMontoInscripcion() {
    return getGuarderiaFamiliar().getMontoInscripcion();
  }
  public void setMontoInscripcion(double newval) {
    getGuarderiaFamiliar().setMontoInscripcion(newval);
  }
  public double getMontoMensualidad() {
    return getGuarderiaFamiliar().getMontoMensualidad();
  }
  public void setMontoMensualidad(double newval) {
    getGuarderiaFamiliar().setMontoMensualidad(newval);
  }
}