package sigefirrhh.bienestar.guarderia;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.ubicacion.Ciudad;
import sigefirrhh.base.ubicacion.CiudadBeanBusiness;

public class GuarderiaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGuarderia(Guarderia guarderia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Guarderia guarderiaNew = 
      (Guarderia)BeanUtils.cloneBean(
      guarderia);

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (guarderiaNew.getCiudad() != null) {
      guarderiaNew.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        guarderiaNew.getCiudad().getIdCiudad()));
    }
    pm.makePersistent(guarderiaNew);
  }

  public void updateGuarderia(Guarderia guarderia) throws Exception
  {
    Guarderia guarderiaModify = 
      findGuarderiaById(guarderia.getIdGuarderia());

    CiudadBeanBusiness ciudadBeanBusiness = new CiudadBeanBusiness();

    if (guarderia.getCiudad() != null) {
      guarderia.setCiudad(
        ciudadBeanBusiness.findCiudadById(
        guarderia.getCiudad().getIdCiudad()));
    }

    BeanUtils.copyProperties(guarderiaModify, guarderia);
  }

  public void deleteGuarderia(Guarderia guarderia) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Guarderia guarderiaDelete = 
      findGuarderiaById(guarderia.getIdGuarderia());
    pm.deletePersistent(guarderiaDelete);
  }

  public Guarderia findGuarderiaById(long idGuarderia) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGuarderia == pIdGuarderia";
    Query query = pm.newQuery(Guarderia.class, filter);

    query.declareParameters("long pIdGuarderia");

    parameters.put("pIdGuarderia", new Long(idGuarderia));

    Collection colGuarderia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGuarderia.iterator();
    return (Guarderia)iterator.next();
  }

  public Collection findGuarderiaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent guarderiaExtent = pm.getExtent(
      Guarderia.class, true);
    Query query = pm.newQuery(guarderiaExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodGuarderia(String codGuarderia)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codGuarderia == pCodGuarderia";

    Query query = pm.newQuery(Guarderia.class, filter);

    query.declareParameters("java.lang.String pCodGuarderia");
    HashMap parameters = new HashMap();

    parameters.put("pCodGuarderia", new String(codGuarderia));

    Collection colGuarderia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGuarderia);

    return colGuarderia;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Guarderia.class, filter);

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colGuarderia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGuarderia);

    return colGuarderia;
  }
}