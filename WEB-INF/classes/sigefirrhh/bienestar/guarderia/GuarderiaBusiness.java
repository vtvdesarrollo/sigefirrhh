package sigefirrhh.bienestar.guarderia;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class GuarderiaBusiness extends AbstractBusiness
  implements Serializable
{
  private GuarderiaBeanBusiness guarderiaBeanBusiness = new GuarderiaBeanBusiness();

  private GuarderiaFamiliarBeanBusiness guarderiaFamiliarBeanBusiness = new GuarderiaFamiliarBeanBusiness();

  public void addGuarderia(Guarderia guarderia)
    throws Exception
  {
    this.guarderiaBeanBusiness.addGuarderia(guarderia);
  }

  public void updateGuarderia(Guarderia guarderia) throws Exception {
    this.guarderiaBeanBusiness.updateGuarderia(guarderia);
  }

  public void deleteGuarderia(Guarderia guarderia) throws Exception {
    this.guarderiaBeanBusiness.deleteGuarderia(guarderia);
  }

  public Guarderia findGuarderiaById(long guarderiaId) throws Exception {
    return this.guarderiaBeanBusiness.findGuarderiaById(guarderiaId);
  }

  public Collection findAllGuarderia() throws Exception {
    return this.guarderiaBeanBusiness.findGuarderiaAll();
  }

  public Collection findGuarderiaByCodGuarderia(String codGuarderia)
    throws Exception
  {
    return this.guarderiaBeanBusiness.findByCodGuarderia(codGuarderia);
  }

  public Collection findGuarderiaByNombre(String nombre)
    throws Exception
  {
    return this.guarderiaBeanBusiness.findByNombre(nombre);
  }

  public void addGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar)
    throws Exception
  {
    this.guarderiaFamiliarBeanBusiness.addGuarderiaFamiliar(guarderiaFamiliar);
  }

  public void updateGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar) throws Exception {
    this.guarderiaFamiliarBeanBusiness.updateGuarderiaFamiliar(guarderiaFamiliar);
  }

  public void deleteGuarderiaFamiliar(GuarderiaFamiliar guarderiaFamiliar) throws Exception {
    this.guarderiaFamiliarBeanBusiness.deleteGuarderiaFamiliar(guarderiaFamiliar);
  }

  public GuarderiaFamiliar findGuarderiaFamiliarById(long guarderiaFamiliarId) throws Exception {
    return this.guarderiaFamiliarBeanBusiness.findGuarderiaFamiliarById(guarderiaFamiliarId);
  }

  public Collection findAllGuarderiaFamiliar() throws Exception {
    return this.guarderiaFamiliarBeanBusiness.findGuarderiaFamiliarAll();
  }

  public Collection findGuarderiaFamiliarByPersonal(long idPersonal)
    throws Exception
  {
    return this.guarderiaFamiliarBeanBusiness.findByPersonal(idPersonal);
  }
}