package sigefirrhh.bienestar.poliza;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportPolizaSiniestrosForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportPolizaSiniestrosForm.class.getName());
  private int reportId;
  private long idTipoPersonal;
  private long idRegion;
  private String formato = "1";
  private String reportName;
  private String tipo = "T";
  private Collection listTipoPersonal;
  private Collection listRegion;
  private String selectPoliza;
  private String selectPlanPoliza;
  private long idPoliza;
  private Collection listPoliza;
  private long idPlanPoliza = 0L;
  private Collection listPlanPoliza;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraNoGenFacade estructuraFacade;
  private BienestarFacade bienestarFacade;
  private LoginSession login;
  private boolean showBoton = false;

  public ReportPolizaSiniestrosForm() { FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraNoGenFacade();
    this.bienestarFacade = new BienestarFacade();
    this.reportName = "titulares";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportPolizaSiniestrosForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listRegion = this.estructuraFacade.findAllRegion();
      this.listPoliza = this.bienestarFacade.findAllPoliza();
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
      this.listRegion = new ArrayList();
      this.listPoliza = new ArrayList();
    }
  }

  public boolean isShowPlanPoliza() { return (this.listPlanPoliza != null) && (!this.listPlanPoliza.isEmpty()); }

  public void changePoliza(ValueChangeEvent event)
  {
    this.idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.listPlanPoliza = null;
      this.idPlanPoliza = 0L;
      if (this.idPoliza != 0L)
        this.listPlanPoliza = this.bienestarFacade.findPlanPolizaByPoliza(this.idPoliza);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changePlanPoliza(ValueChangeEvent event) {
    this.idPlanPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    this.selectPlanPoliza = String.valueOf(this.idPlanPoliza);
    try
    {
      this.showBoton = false;
      if (this.idPlanPoliza != 0L)
      {
        this.showBoton = true;
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getListRegion() { return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region"); }

  public Collection getListPoliza() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listPoliza, "sigefirrhh.base.bienestar.Poliza");
  }
  public Collection getListPlanPoliza() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listPlanPoliza, "sigefirrhh.base.bienestar.PlanPoliza");
  }
  public void cambiarNombreAReporte() {
    try {
      this.reportName = "utiles";
      if (this.idRegion != 0L)
        this.reportName += "reg";
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "juguetes";
      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }
      parameters.put("id_plan_poliza", new Long(this.idPlanPoliza));
      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/bienestar/poliza");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }

  public long getIdRegion()
  {
    return this.idRegion;
  }

  public void setIdRegion(long idRegion)
  {
    this.idRegion = idRegion;
  }

  public long getIdPlanPoliza()
  {
    return this.idPlanPoliza;
  }

  public void setIdPlanPoliza(long idPlanPoliza)
  {
    this.idPlanPoliza = idPlanPoliza;
  }

  public String getSelectPoliza()
  {
    return this.selectPoliza;
  }

  public void setSelectPoliza(String selectPoliza)
  {
    this.selectPoliza = selectPoliza;
  }

  public String getTipo()
  {
    return this.tipo;
  }

  public void setTipo(String tipo)
  {
    this.tipo = tipo;
  }

  public boolean isShowBoton()
  {
    return this.showBoton;
  }

  public void setShowBoton(boolean showBoton)
  {
    this.showBoton = showBoton;
  }

  public String getSelectPlanPoliza()
  {
    return this.selectPlanPoliza;
  }

  public void setSelectPlanPoliza(String selectPlanPoliza)
  {
    this.selectPlanPoliza = selectPlanPoliza;
  }
}