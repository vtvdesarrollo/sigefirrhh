package sigefirrhh.bienestar.poliza;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.Poliza;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class PrimasCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(PrimasCargoForm.class.getName());
  private PrimasCargo primasCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private CargoFacade cargoFacade = new CargoFacade();
  private PolizaFacade polizaFacade = new PolizaFacade();
  private boolean showPrimasCargoByPrimasPlan;
  private String findSelectPolizaForPrimasPlan;
  private String findSelectPlanPolizaForPrimasPlan;
  private String findSelectPrimasPlan;
  private Collection findColPolizaForPrimasPlan;
  private Collection findColPlanPolizaForPrimasPlan;
  private Collection findColPrimasPlan;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colPolizaForPrimasPlan;
  private Collection colPlanPolizaForPrimasPlan;
  private Collection colPrimasPlan;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectPolizaForPrimasPlan;
  private String selectPlanPolizaForPrimasPlan;
  private String selectPrimasPlan;
  private Object stateResultPrimasCargoByPrimasPlan = null;

  public Collection getFindColPolizaForPrimasPlan()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPolizaForPrimasPlan.iterator();
    Poliza polizaForPrimasPlan = null;
    while (iterator.hasNext()) {
      polizaForPrimasPlan = (Poliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(polizaForPrimasPlan.getIdPoliza()), 
        polizaForPrimasPlan.toString()));
    }
    return col;
  }
  public String getFindSelectPolizaForPrimasPlan() {
    return this.findSelectPolizaForPrimasPlan;
  }
  public void setFindSelectPolizaForPrimasPlan(String valPolizaForPrimasPlan) {
    this.findSelectPolizaForPrimasPlan = valPolizaForPrimasPlan;
  }
  public void findChangePolizaForPrimasPlan(ValueChangeEvent event) {
    long idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColPrimasPlan = null;
      this.findColPlanPolizaForPrimasPlan = null;
      if (idPoliza > 0L)
        this.findColPlanPolizaForPrimasPlan = 
          this.bienestarFacade.findPlanPolizaByPoliza(
          idPoliza);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowPolizaForPrimasPlan() { return this.findColPolizaForPrimasPlan != null; }

  public Collection getFindColPlanPolizaForPrimasPlan() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPlanPolizaForPrimasPlan.iterator();
    PlanPoliza planPolizaForPrimasPlan = null;
    while (iterator.hasNext()) {
      planPolizaForPrimasPlan = (PlanPoliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPolizaForPrimasPlan.getIdPlanPoliza()), 
        planPolizaForPrimasPlan.toString()));
    }
    return col;
  }
  public String getFindSelectPlanPolizaForPrimasPlan() {
    return this.findSelectPlanPolizaForPrimasPlan;
  }
  public void setFindSelectPlanPolizaForPrimasPlan(String valPlanPolizaForPrimasPlan) {
    this.findSelectPlanPolizaForPrimasPlan = valPlanPolizaForPrimasPlan;
  }
  public void findChangePlanPolizaForPrimasPlan(ValueChangeEvent event) {
    long idPlanPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColPrimasPlan = null;
      if (idPlanPoliza > 0L)
        this.findColPrimasPlan = 
          this.bienestarFacade.findPrimasPlanByPlanPoliza(
          idPlanPoliza);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowPlanPolizaForPrimasPlan() { return this.findColPlanPolizaForPrimasPlan != null; }

  public String getFindSelectPrimasPlan() {
    return this.findSelectPrimasPlan;
  }
  public void setFindSelectPrimasPlan(String valPrimasPlan) {
    this.findSelectPrimasPlan = valPrimasPlan;
  }

  public Collection getFindColPrimasPlan() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColPrimasPlan.iterator();
    PrimasPlan primasPlan = null;
    while (iterator.hasNext()) {
      primasPlan = (PrimasPlan)iterator.next();
      col.add(new SelectItem(
        String.valueOf(primasPlan.getIdPrimasPlan()), 
        primasPlan.toString()));
    }
    return col;
  }
  public boolean isFindShowPrimasPlan() {
    return this.findColPrimasPlan != null;
  }

  public String getSelectManualCargoForCargo()
  {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.primasCargo.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.primasCargo.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.primasCargo.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.primasCargo.setCargo(
          cargo);
        break;
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectPolizaForPrimasPlan() {
    return this.selectPolizaForPrimasPlan;
  }
  public void setSelectPolizaForPrimasPlan(String valPolizaForPrimasPlan) {
    this.selectPolizaForPrimasPlan = valPolizaForPrimasPlan;
  }
  public void changePolizaForPrimasPlan(ValueChangeEvent event) {
    long idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPrimasPlan = null;
      this.colPlanPolizaForPrimasPlan = null;
      if (idPoliza > 0L) {
        this.colPlanPolizaForPrimasPlan = 
          this.bienestarFacade.findPlanPolizaByPoliza(
          idPoliza);
      } else {
        this.selectPrimasPlan = null;
        this.primasCargo.setPrimasPlan(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectPrimasPlan = null;
      this.primasCargo.setPrimasPlan(
        null);
    }
  }

  public boolean isShowPolizaForPrimasPlan() { return this.colPolizaForPrimasPlan != null; }

  public String getSelectPlanPolizaForPrimasPlan() {
    return this.selectPlanPolizaForPrimasPlan;
  }
  public void setSelectPlanPolizaForPrimasPlan(String valPlanPolizaForPrimasPlan) {
    this.selectPlanPolizaForPrimasPlan = valPlanPolizaForPrimasPlan;
  }
  public void changePlanPolizaForPrimasPlan(ValueChangeEvent event) {
    long idPlanPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPrimasPlan = null;
      if (idPlanPoliza > 0L) {
        this.colPrimasPlan = 
          this.bienestarFacade.findPrimasPlanByPlanPoliza(
          idPlanPoliza);
      } else {
        this.selectPrimasPlan = null;
        this.primasCargo.setPrimasPlan(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectPrimasPlan = null;
      this.primasCargo.setPrimasPlan(
        null);
    }
  }

  public boolean isShowPlanPolizaForPrimasPlan() { return this.colPlanPolizaForPrimasPlan != null; }

  public String getSelectPrimasPlan() {
    return this.selectPrimasPlan;
  }
  public void setSelectPrimasPlan(String valPrimasPlan) {
    Iterator iterator = this.colPrimasPlan.iterator();
    PrimasPlan primasPlan = null;
    this.primasCargo.setPrimasPlan(null);
    while (iterator.hasNext()) {
      primasPlan = (PrimasPlan)iterator.next();
      if (String.valueOf(primasPlan.getIdPrimasPlan()).equals(
        valPrimasPlan)) {
        this.primasCargo.setPrimasPlan(
          primasPlan);
        break;
      }
    }
    this.selectPrimasPlan = valPrimasPlan;
  }
  public boolean isShowPrimasPlan() {
    return this.colPrimasPlan != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public PrimasCargo getPrimasCargo() {
    if (this.primasCargo == null) {
      this.primasCargo = new PrimasCargo();
    }
    return this.primasCargo;
  }

  public PrimasCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColManualCargoForCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColPolizaForPrimasPlan() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPolizaForPrimasPlan.iterator();
    Poliza polizaForPrimasPlan = null;
    while (iterator.hasNext()) {
      polizaForPrimasPlan = (Poliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(polizaForPrimasPlan.getIdPoliza()), 
        polizaForPrimasPlan.toString()));
    }
    return col;
  }

  public Collection getColPlanPolizaForPrimasPlan()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPolizaForPrimasPlan.iterator();
    PlanPoliza planPolizaForPrimasPlan = null;
    while (iterator.hasNext()) {
      planPolizaForPrimasPlan = (PlanPoliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPolizaForPrimasPlan.getIdPlanPoliza()), 
        planPolizaForPrimasPlan.toString()));
    }
    return col;
  }

  public Collection getColPrimasPlan()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPrimasPlan.iterator();
    PrimasPlan primasPlan = null;
    while (iterator.hasNext()) {
      primasPlan = (PrimasPlan)iterator.next();
      col.add(new SelectItem(
        String.valueOf(primasPlan.getIdPrimasPlan()), 
        primasPlan.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColPolizaForPrimasPlan = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colManualCargoForCargo = 
        this.cargoFacade.findManualCargoByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colPolizaForPrimasPlan = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPrimasCargoByPrimasPlan()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.polizaFacade.findPrimasCargoByPrimasPlan(Long.valueOf(this.findSelectPrimasPlan).longValue());
      this.showPrimasCargoByPrimasPlan = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showPrimasCargoByPrimasPlan)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectPolizaForPrimasPlan = null;
    this.findSelectPlanPolizaForPrimasPlan = null;
    this.findSelectPrimasPlan = null;

    return null;
  }

  public boolean isShowPrimasCargoByPrimasPlan() {
    return this.showPrimasCargoByPrimasPlan;
  }

  public String selectPrimasCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectPrimasPlan = null;
    this.selectPolizaForPrimasPlan = null;

    this.selectPlanPolizaForPrimasPlan = null;

    long idPrimasCargo = 
      Long.parseLong((String)requestParameterMap.get("idPrimasCargo"));
    try
    {
      this.primasCargo = 
        this.polizaFacade.findPrimasCargoById(
        idPrimasCargo);
      if (this.primasCargo.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.primasCargo.getCargo().getIdCargo());
      }
      if (this.primasCargo.getPrimasPlan() != null) {
        this.selectPrimasPlan = 
          String.valueOf(this.primasCargo.getPrimasPlan().getIdPrimasPlan());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;
      PrimasPlan primasPlan = null;
      PlanPoliza planPolizaForPrimasPlan = null;
      Poliza polizaForPrimasPlan = null;

      if (this.primasCargo.getCargo() != null) {
        long idCargo = 
          this.primasCargo.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.primasCargo.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
      if (this.primasCargo.getPrimasPlan() != null) {
        long idPrimasPlan = 
          this.primasCargo.getPrimasPlan().getIdPrimasPlan();
        this.selectPrimasPlan = String.valueOf(idPrimasPlan);
        primasPlan = this.bienestarFacade.findPrimasPlanById(
          idPrimasPlan);
        this.colPrimasPlan = this.bienestarFacade.findPrimasPlanByPlanPoliza(
          primasPlan.getPlanPoliza().getIdPlanPoliza());

        long idPlanPolizaForPrimasPlan = 
          this.primasCargo.getPrimasPlan().getPlanPoliza().getIdPlanPoliza();
        this.selectPlanPolizaForPrimasPlan = String.valueOf(idPlanPolizaForPrimasPlan);
        planPolizaForPrimasPlan = 
          this.bienestarFacade.findPlanPolizaById(
          idPlanPolizaForPrimasPlan);
        this.colPlanPolizaForPrimasPlan = 
          this.bienestarFacade.findPlanPolizaByPoliza(
          planPolizaForPrimasPlan.getPoliza().getIdPoliza());

        long idPolizaForPrimasPlan = 
          planPolizaForPrimasPlan.getPoliza().getIdPoliza();
        this.selectPolizaForPrimasPlan = String.valueOf(idPolizaForPrimasPlan);
        polizaForPrimasPlan = 
          this.bienestarFacade.findPolizaById(
          idPolizaForPrimasPlan);
        this.colPolizaForPrimasPlan = 
          this.bienestarFacade.findAllPoliza();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.primasCargo = null;
    this.showPrimasCargoByPrimasPlan = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.polizaFacade.addPrimasCargo(
          this.primasCargo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.polizaFacade.updatePrimasCargo(
          this.primasCargo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.polizaFacade.deletePrimasCargo(
        this.primasCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.primasCargo = new PrimasCargo();

    this.selectCargo = null;

    this.selectManualCargoForCargo = null;

    this.selectPrimasPlan = null;

    this.selectPolizaForPrimasPlan = null;

    this.selectPlanPolizaForPrimasPlan = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.primasCargo.setIdPrimasCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.poliza.PrimasCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.primasCargo = new PrimasCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}