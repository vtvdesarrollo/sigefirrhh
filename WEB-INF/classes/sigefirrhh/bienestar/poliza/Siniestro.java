package sigefirrhh.bienestar.poliza;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.TipoSiniestro;
import sigefirrhh.personal.expediente.Personal;

public class Siniestro
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idSiniestro;
  private TipoSiniestro tipoSiniestro;
  private String numeroSiniestro;
  private Date fechaSiniestro;
  private Date fechaReporte;
  private Date fechaRecepcion;
  private String estatus;
  private double montoReclamo;
  private double montoPagado;
  private Date fechaCierre;
  private String diagnostico;
  private PlanPoliza planPoliza;
  private Beneficiario beneficiario;
  private Titular titular;
  private String observaciones;
  private Personal personal;
  private EstablecimientoSalud establecimientoSalud;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "beneficiario", "diagnostico", "establecimientoSalud", "estatus", "fechaCierre", "fechaRecepcion", "fechaReporte", "fechaSiniestro", "idSiniestro", "montoPagado", "montoReclamo", "numeroSiniestro", "observaciones", "personal", "planPoliza", "tipoSiniestro", "titular" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.bienestar.poliza.Beneficiario"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.EstablecimientoSalud"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza"), sunjdo$classForName$("sigefirrhh.base.bienestar.TipoSiniestro"), sunjdo$classForName$("sigefirrhh.bienestar.poliza.Titular") }; private static final byte[] jdoFieldFlags = { 26, 21, 26, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 26, 26, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.poliza.Siniestro"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Siniestro());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("P", "PENDIENTE RECAUDOS");
    LISTA_ESTATUS.put("E", "EN PROCESO");
    LISTA_ESTATUS.put("R", "RECHAZADO");
    LISTA_ESTATUS.put("G", "PAGADO");
  }

  public Siniestro()
  {
    jdoSetmontoPagado(this, 0.0D);
  }

  public String toString()
  {
    String x = jdoGetplanPoliza(this) + " - " + new SimpleDateFormat("dd/MM/yyyy").format(jdoGetfechaSiniestro(this));
    if (jdoGetbeneficiario(this) != null) {
      x = x + " - " + jdoGetbeneficiario(this).getFamiliar();
    }
    return x;
  }

  public Beneficiario getBeneficiario() {
    return jdoGetbeneficiario(this);
  }

  public void setBeneficiario(Beneficiario beneficiario) {
    jdoSetbeneficiario(this, beneficiario);
  }

  public EstablecimientoSalud getEstablecimientoSalud() {
    return jdoGetestablecimientoSalud(this);
  }

  public void setEstablecimientoSalud(EstablecimientoSalud establecimientoSalud) {
    jdoSetestablecimientoSalud(this, establecimientoSalud);
  }

  public String getEstatus() {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }

  public Date getFechaCierre() {
    return jdoGetfechaCierre(this);
  }

  public void setFechaCierre(Date fechaCierre) {
    jdoSetfechaCierre(this, fechaCierre);
  }

  public Date getFechaRecepcion() {
    return jdoGetfechaRecepcion(this);
  }

  public void setFechaRecepcion(Date fechaRecepcion) {
    jdoSetfechaRecepcion(this, fechaRecepcion);
  }

  public Date getFechaReporte() {
    return jdoGetfechaReporte(this);
  }

  public void setFechaReporte(Date fechaReporte) {
    jdoSetfechaReporte(this, fechaReporte);
  }

  public Date getFechaSiniestro() {
    return jdoGetfechaSiniestro(this);
  }

  public void setFechaSiniestro(Date fechaSiniestro) {
    jdoSetfechaSiniestro(this, fechaSiniestro);
  }

  public long getIdSiniestro() {
    return jdoGetidSiniestro(this);
  }

  public void setIdSiniestro(long idSiniestro) {
    jdoSetidSiniestro(this, idSiniestro);
  }

  public double getMontoPagado() {
    return jdoGetmontoPagado(this);
  }

  public void setMontoPagado(double montoPagado) {
    jdoSetmontoPagado(this, montoPagado);
  }

  public double getMontoReclamo() {
    return jdoGetmontoReclamo(this);
  }

  public void setMontoReclamo(double montoReclamo) {
    jdoSetmontoReclamo(this, montoReclamo);
  }

  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }

  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public PlanPoliza getPlanPoliza() {
    return jdoGetplanPoliza(this);
  }

  public void setPlanPoliza(PlanPoliza planPoliza) {
    jdoSetplanPoliza(this, planPoliza);
  }

  public Titular getTitular() {
    return jdoGettitular(this);
  }

  public void setTitular(Titular titular) {
    jdoSettitular(this, titular);
  }

  public String getDiagnostico() {
    return jdoGetdiagnostico(this);
  }

  public void setDiagnostico(String diagnostico) {
    jdoSetdiagnostico(this, diagnostico);
  }

  public TipoSiniestro getTipoSiniestro() {
    return jdoGettipoSiniestro(this);
  }

  public void setTipoSiniestro(TipoSiniestro tipoSiniestro) {
    jdoSettipoSiniestro(this, tipoSiniestro);
  }

  public String getNumeroSiniestro() {
    return jdoGetnumeroSiniestro(this);
  }

  public void setNumeroSiniestro(String numeroSiniestro) {
    jdoSetnumeroSiniestro(this, numeroSiniestro);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 17;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Siniestro localSiniestro = new Siniestro();
    localSiniestro.jdoFlags = 1;
    localSiniestro.jdoStateManager = paramStateManager;
    return localSiniestro;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Siniestro localSiniestro = new Siniestro();
    localSiniestro.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSiniestro.jdoFlags = 1;
    localSiniestro.jdoStateManager = paramStateManager;
    return localSiniestro;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.beneficiario);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.diagnostico);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.establecimientoSalud);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCierre);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRecepcion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaReporte);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaSiniestro);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSiniestro);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPagado);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoReclamo);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.numeroSiniestro);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoSiniestro);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.titular);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.beneficiario = ((Beneficiario)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.diagnostico = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.establecimientoSalud = ((EstablecimientoSalud)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCierre = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRecepcion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaReporte = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaSiniestro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSiniestro = localStateManager.replacingLongField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPagado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoReclamo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroSiniestro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoSiniestro = ((TipoSiniestro)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.titular = ((Titular)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Siniestro paramSiniestro, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.beneficiario = paramSiniestro.beneficiario;
      return;
    case 1:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.diagnostico = paramSiniestro.diagnostico;
      return;
    case 2:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.establecimientoSalud = paramSiniestro.establecimientoSalud;
      return;
    case 3:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramSiniestro.estatus;
      return;
    case 4:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCierre = paramSiniestro.fechaCierre;
      return;
    case 5:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRecepcion = paramSiniestro.fechaRecepcion;
      return;
    case 6:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.fechaReporte = paramSiniestro.fechaReporte;
      return;
    case 7:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.fechaSiniestro = paramSiniestro.fechaSiniestro;
      return;
    case 8:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.idSiniestro = paramSiniestro.idSiniestro;
      return;
    case 9:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.montoPagado = paramSiniestro.montoPagado;
      return;
    case 10:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.montoReclamo = paramSiniestro.montoReclamo;
      return;
    case 11:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.numeroSiniestro = paramSiniestro.numeroSiniestro;
      return;
    case 12:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramSiniestro.observaciones;
      return;
    case 13:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramSiniestro.personal;
      return;
    case 14:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramSiniestro.planPoliza;
      return;
    case 15:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.tipoSiniestro = paramSiniestro.tipoSiniestro;
      return;
    case 16:
      if (paramSiniestro == null)
        throw new IllegalArgumentException("arg1");
      this.titular = paramSiniestro.titular;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Siniestro))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Siniestro localSiniestro = (Siniestro)paramObject;
    if (localSiniestro.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSiniestro, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SiniestroPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SiniestroPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SiniestroPK))
      throw new IllegalArgumentException("arg1");
    SiniestroPK localSiniestroPK = (SiniestroPK)paramObject;
    localSiniestroPK.idSiniestro = this.idSiniestro;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SiniestroPK))
      throw new IllegalArgumentException("arg1");
    SiniestroPK localSiniestroPK = (SiniestroPK)paramObject;
    this.idSiniestro = localSiniestroPK.idSiniestro;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SiniestroPK))
      throw new IllegalArgumentException("arg2");
    SiniestroPK localSiniestroPK = (SiniestroPK)paramObject;
    localSiniestroPK.idSiniestro = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 8);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SiniestroPK))
      throw new IllegalArgumentException("arg2");
    SiniestroPK localSiniestroPK = (SiniestroPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 8, localSiniestroPK.idSiniestro);
  }

  private static final Beneficiario jdoGetbeneficiario(Siniestro paramSiniestro)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.beneficiario;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 0))
      return paramSiniestro.beneficiario;
    return (Beneficiario)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 0, paramSiniestro.beneficiario);
  }

  private static final void jdoSetbeneficiario(Siniestro paramSiniestro, Beneficiario paramBeneficiario)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.beneficiario = paramBeneficiario;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 0, paramSiniestro.beneficiario, paramBeneficiario);
  }

  private static final String jdoGetdiagnostico(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.diagnostico;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.diagnostico;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 1))
      return paramSiniestro.diagnostico;
    return localStateManager.getStringField(paramSiniestro, jdoInheritedFieldCount + 1, paramSiniestro.diagnostico);
  }

  private static final void jdoSetdiagnostico(Siniestro paramSiniestro, String paramString)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.diagnostico = paramString;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.diagnostico = paramString;
      return;
    }
    localStateManager.setStringField(paramSiniestro, jdoInheritedFieldCount + 1, paramSiniestro.diagnostico, paramString);
  }

  private static final EstablecimientoSalud jdoGetestablecimientoSalud(Siniestro paramSiniestro)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.establecimientoSalud;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 2))
      return paramSiniestro.establecimientoSalud;
    return (EstablecimientoSalud)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 2, paramSiniestro.establecimientoSalud);
  }

  private static final void jdoSetestablecimientoSalud(Siniestro paramSiniestro, EstablecimientoSalud paramEstablecimientoSalud)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.establecimientoSalud = paramEstablecimientoSalud;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 2, paramSiniestro.establecimientoSalud, paramEstablecimientoSalud);
  }

  private static final String jdoGetestatus(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.estatus;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.estatus;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 3))
      return paramSiniestro.estatus;
    return localStateManager.getStringField(paramSiniestro, jdoInheritedFieldCount + 3, paramSiniestro.estatus);
  }

  private static final void jdoSetestatus(Siniestro paramSiniestro, String paramString)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramSiniestro, jdoInheritedFieldCount + 3, paramSiniestro.estatus, paramString);
  }

  private static final Date jdoGetfechaCierre(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.fechaCierre;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.fechaCierre;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 4))
      return paramSiniestro.fechaCierre;
    return (Date)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 4, paramSiniestro.fechaCierre);
  }

  private static final void jdoSetfechaCierre(Siniestro paramSiniestro, Date paramDate)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.fechaCierre = paramDate;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.fechaCierre = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 4, paramSiniestro.fechaCierre, paramDate);
  }

  private static final Date jdoGetfechaRecepcion(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.fechaRecepcion;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.fechaRecepcion;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 5))
      return paramSiniestro.fechaRecepcion;
    return (Date)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 5, paramSiniestro.fechaRecepcion);
  }

  private static final void jdoSetfechaRecepcion(Siniestro paramSiniestro, Date paramDate)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.fechaRecepcion = paramDate;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.fechaRecepcion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 5, paramSiniestro.fechaRecepcion, paramDate);
  }

  private static final Date jdoGetfechaReporte(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.fechaReporte;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.fechaReporte;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 6))
      return paramSiniestro.fechaReporte;
    return (Date)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 6, paramSiniestro.fechaReporte);
  }

  private static final void jdoSetfechaReporte(Siniestro paramSiniestro, Date paramDate)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.fechaReporte = paramDate;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.fechaReporte = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 6, paramSiniestro.fechaReporte, paramDate);
  }

  private static final Date jdoGetfechaSiniestro(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.fechaSiniestro;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.fechaSiniestro;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 7))
      return paramSiniestro.fechaSiniestro;
    return (Date)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 7, paramSiniestro.fechaSiniestro);
  }

  private static final void jdoSetfechaSiniestro(Siniestro paramSiniestro, Date paramDate)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.fechaSiniestro = paramDate;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.fechaSiniestro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 7, paramSiniestro.fechaSiniestro, paramDate);
  }

  private static final long jdoGetidSiniestro(Siniestro paramSiniestro)
  {
    return paramSiniestro.idSiniestro;
  }

  private static final void jdoSetidSiniestro(Siniestro paramSiniestro, long paramLong)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.idSiniestro = paramLong;
      return;
    }
    localStateManager.setLongField(paramSiniestro, jdoInheritedFieldCount + 8, paramSiniestro.idSiniestro, paramLong);
  }

  private static final double jdoGetmontoPagado(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.montoPagado;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.montoPagado;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 9))
      return paramSiniestro.montoPagado;
    return localStateManager.getDoubleField(paramSiniestro, jdoInheritedFieldCount + 9, paramSiniestro.montoPagado);
  }

  private static final void jdoSetmontoPagado(Siniestro paramSiniestro, double paramDouble)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.montoPagado = paramDouble;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.montoPagado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSiniestro, jdoInheritedFieldCount + 9, paramSiniestro.montoPagado, paramDouble);
  }

  private static final double jdoGetmontoReclamo(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.montoReclamo;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.montoReclamo;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 10))
      return paramSiniestro.montoReclamo;
    return localStateManager.getDoubleField(paramSiniestro, jdoInheritedFieldCount + 10, paramSiniestro.montoReclamo);
  }

  private static final void jdoSetmontoReclamo(Siniestro paramSiniestro, double paramDouble)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.montoReclamo = paramDouble;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.montoReclamo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramSiniestro, jdoInheritedFieldCount + 10, paramSiniestro.montoReclamo, paramDouble);
  }

  private static final String jdoGetnumeroSiniestro(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.numeroSiniestro;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.numeroSiniestro;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 11))
      return paramSiniestro.numeroSiniestro;
    return localStateManager.getStringField(paramSiniestro, jdoInheritedFieldCount + 11, paramSiniestro.numeroSiniestro);
  }

  private static final void jdoSetnumeroSiniestro(Siniestro paramSiniestro, String paramString)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.numeroSiniestro = paramString;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.numeroSiniestro = paramString;
      return;
    }
    localStateManager.setStringField(paramSiniestro, jdoInheritedFieldCount + 11, paramSiniestro.numeroSiniestro, paramString);
  }

  private static final String jdoGetobservaciones(Siniestro paramSiniestro)
  {
    if (paramSiniestro.jdoFlags <= 0)
      return paramSiniestro.observaciones;
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.observaciones;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 12))
      return paramSiniestro.observaciones;
    return localStateManager.getStringField(paramSiniestro, jdoInheritedFieldCount + 12, paramSiniestro.observaciones);
  }

  private static final void jdoSetobservaciones(Siniestro paramSiniestro, String paramString)
  {
    if (paramSiniestro.jdoFlags == 0)
    {
      paramSiniestro.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramSiniestro, jdoInheritedFieldCount + 12, paramSiniestro.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(Siniestro paramSiniestro)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.personal;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 13))
      return paramSiniestro.personal;
    return (Personal)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 13, paramSiniestro.personal);
  }

  private static final void jdoSetpersonal(Siniestro paramSiniestro, Personal paramPersonal)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 13, paramSiniestro.personal, paramPersonal);
  }

  private static final PlanPoliza jdoGetplanPoliza(Siniestro paramSiniestro)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.planPoliza;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 14))
      return paramSiniestro.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 14, paramSiniestro.planPoliza);
  }

  private static final void jdoSetplanPoliza(Siniestro paramSiniestro, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 14, paramSiniestro.planPoliza, paramPlanPoliza);
  }

  private static final TipoSiniestro jdoGettipoSiniestro(Siniestro paramSiniestro)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.tipoSiniestro;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 15))
      return paramSiniestro.tipoSiniestro;
    return (TipoSiniestro)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 15, paramSiniestro.tipoSiniestro);
  }

  private static final void jdoSettipoSiniestro(Siniestro paramSiniestro, TipoSiniestro paramTipoSiniestro)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.tipoSiniestro = paramTipoSiniestro;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 15, paramSiniestro.tipoSiniestro, paramTipoSiniestro);
  }

  private static final Titular jdoGettitular(Siniestro paramSiniestro)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
      return paramSiniestro.titular;
    if (localStateManager.isLoaded(paramSiniestro, jdoInheritedFieldCount + 16))
      return paramSiniestro.titular;
    return (Titular)localStateManager.getObjectField(paramSiniestro, jdoInheritedFieldCount + 16, paramSiniestro.titular);
  }

  private static final void jdoSettitular(Siniestro paramSiniestro, Titular paramTitular)
  {
    StateManager localStateManager = paramSiniestro.jdoStateManager;
    if (localStateManager == null)
    {
      paramSiniestro.titular = paramTitular;
      return;
    }
    localStateManager.setObjectField(paramSiniestro, jdoInheritedFieldCount + 16, paramSiniestro.titular, paramTitular);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}