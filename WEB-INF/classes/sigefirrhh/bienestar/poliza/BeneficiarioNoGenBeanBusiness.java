package sigefirrhh.bienestar.poliza;

import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class BeneficiarioNoGenBeanBusiness extends BeneficiarioBeanBusiness
  implements Serializable
{
  public Collection findByPersonalAndPlanPoliza(long idPersonal, long idPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPoliza.idPlanPoliza == pIdPlanPoliza && personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Beneficiario.class, filter);

    query.declareParameters("long pIdPlanPoliza, long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));
    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colBeneficiario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBeneficiario);

    return colBeneficiario;
  }
}