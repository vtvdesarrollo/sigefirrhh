package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Collection;
import org.apache.log4j.Logger;

public class PolizaNoGenBusiness extends AbstractBusiness
{
  Logger log = Logger.getLogger(PolizaNoGenBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  private BeneficiarioNoGenBeanBusiness beneficiarioNoGenBeanBusiness = new BeneficiarioNoGenBeanBusiness();
  private TitularNoGenBeanBusiness titularNoGenBeanBusiness = new TitularNoGenBeanBusiness();

  public Collection findBeneficiarioByPersonalAndPlanPoliza(long idPersonal, long idPlanPoliza)
    throws Exception
  {
    return this.beneficiarioNoGenBeanBusiness.findByPersonalAndPlanPoliza(idPersonal, idPlanPoliza);
  }

  public Collection findTitularByPersonalAndPlanPoliza(long idPersonal, long idPlanPoliza) throws Exception {
    return this.titularNoGenBeanBusiness.findByPersonalAndPlanPoliza(idPersonal, idPlanPoliza);
  }

  public void generarPolizas(long idTipoPersonal, long idPlanPoliza, java.util.Date fecha, String proceso) throws Exception {
    Connection connection = null;
    PreparedStatement st = null;

    java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

    StringBuffer sql = new StringBuffer();
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      if (proceso.equals("T"))
      {
        sql.append("select generar_poliza_titular(?,?,?)");
      }
      else sql.append("select generar_poliza_beneficiario(?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + idPlanPoliza);
      this.log.error("3- " + fecha);
      this.log.error("4- " + proceso);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idPlanPoliza);
      st.setDate(3, fechaSql);

      st.executeQuery();

      connection.commit();
      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        } catch (Exception localException1) {
        } 
    }
  }

  public void suspenderPrimas(long idTipoPersonal, long idPlanPoliza, String tipo) throws Exception { Connection connection = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      sql.append("select suspender_primas(?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + idPlanPoliza);
      this.log.error("3- " + tipo);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idPlanPoliza);
      st.setString(3, tipo);

      st.executeQuery();

      connection.commit();
      connection.close(); connection = null;
      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1) {
        } 
    } } 
  public void generarPrestamoPoliza(long idTipoPersonal, long idPlanPoliza, String tipo, long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, int numeroCuotas) throws Exception {
    Connection connection = null;
    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      sql.append("select generar_prestamos_desde_poliza(?,?,?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + idPlanPoliza);
      this.log.error("3- " + tipo);
      this.log.error("4- " + idConceptoTipoPersonal);
      this.log.error("5- " + idFrecuenciaTipoPersonal);
      this.log.error("6- " + numeroCuotas);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idPlanPoliza);
      st.setString(3, tipo);
      st.setLong(4, idConceptoTipoPersonal);
      st.setLong(5, idFrecuenciaTipoPersonal);
      st.setInt(6, numeroCuotas);

      st.executeQuery();

      connection.commit();

      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        }
    }
  }
}