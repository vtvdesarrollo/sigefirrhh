package sigefirrhh.bienestar.poliza;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class BeneficiarioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(BeneficiarioForm.class.getName());
  private Beneficiario beneficiario;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private PolizaFacade polizaFacade = new PolizaFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colFamiliar;
  private Collection colTipoPersonal;
  private Collection colPersonal;
  private Collection colPrimasPlan;
  private Collection colPlanPoliza;
  private String selectFamiliar;
  private String selectTipoPersonal;
  private String selectPersonal;
  private String selectPrimasPlan;
  private String selectPlanPoliza;
  private Object stateResultPersonal = null;

  private Object stateResultBeneficiarioByPersonal = null;

  public String getSelectFamiliar()
  {
    return this.selectFamiliar;
  }
  public void setSelectFamiliar(String valFamiliar) {
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    this.beneficiario.setFamiliar(null);
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      if (String.valueOf(familiar.getIdFamiliar()).equals(
        valFamiliar)) {
        this.beneficiario.setFamiliar(
          familiar);
        break;
      }
    }
    this.selectFamiliar = valFamiliar;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.beneficiario.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.beneficiario.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.beneficiario.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.beneficiario.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public String getSelectPrimasPlan() {
    return this.selectPrimasPlan;
  }
  public void setSelectPrimasPlan(String valPrimasPlan) {
    Iterator iterator = this.colPrimasPlan.iterator();
    PrimasPlan primasPlan = null;
    this.beneficiario.setPrimasPlan(null);
    while (iterator.hasNext()) {
      primasPlan = (PrimasPlan)iterator.next();
      if (String.valueOf(primasPlan.getIdPrimasPlan()).equals(
        valPrimasPlan)) {
        this.beneficiario.setPrimasPlan(
          primasPlan);
        break;
      }
    }
    this.selectPrimasPlan = valPrimasPlan;
  }
  public String getSelectPlanPoliza() {
    return this.selectPlanPoliza;
  }
  public void setSelectPlanPoliza(String valPlanPoliza) {
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    this.beneficiario.setPlanPoliza(null);
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      if (String.valueOf(planPoliza.getIdPlanPoliza()).equals(
        valPlanPoliza)) {
        this.beneficiario.setPlanPoliza(
          planPoliza);
        break;
      }
    }
    this.selectPlanPoliza = valPlanPoliza;
  }
  public Collection getResult() {
    return this.result;
  }

  public Beneficiario getBeneficiario() {
    if (this.beneficiario == null) {
      this.beneficiario = new Beneficiario();
    }
    return this.beneficiario;
  }

  public BeneficiarioForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Beneficiario.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColFamiliar()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      col.add(new SelectItem(
        String.valueOf(familiar.getIdFamiliar()), 
        familiar.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public Collection getColPrimasPlan()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPrimasPlan.iterator();
    PrimasPlan primasPlan = null;
    while (iterator.hasNext()) {
      primasPlan = (PrimasPlan)iterator.next();
      col.add(new SelectItem(
        String.valueOf(primasPlan.getIdPrimasPlan()), 
        primasPlan.toString()));
    }
    return col;
  }

  public Collection getColPlanPoliza()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPoliza.getIdPlanPoliza()), 
        planPoliza.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colPrimasPlan = 
        this.bienestarFacade.findAllPrimasPlan();
      this.colPlanPoliza = 
        this.bienestarFacade.findAllPlanPoliza();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findBeneficiarioByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.polizaFacade.findBeneficiarioByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectBeneficiario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectFamiliar = null;
    this.selectTipoPersonal = null;
    this.selectPersonal = null;
    this.selectPrimasPlan = null;
    this.selectPlanPoliza = null;

    long idBeneficiario = 
      Long.parseLong((String)requestParameterMap.get("idBeneficiario"));
    try
    {
      this.beneficiario = 
        this.polizaFacade.findBeneficiarioById(
        idBeneficiario);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.beneficiario.getFechaInclusion() != null) && 
      (this.beneficiario.getFechaInclusion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Inclusión no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.beneficiario.getFechaExclusion() != null) && 
      (this.beneficiario.getFechaExclusion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Exclusiòn no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.beneficiario.setPersonal(
          this.personal);
        this.polizaFacade.addBeneficiario(
          this.beneficiario);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.polizaFacade.updateBeneficiario(
          this.beneficiario);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.polizaFacade.deleteBeneficiario(
        this.beneficiario);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectFamiliar = null;

    this.selectTipoPersonal = null;

    this.selectPersonal = null;

    this.selectPrimasPlan = null;

    this.selectPlanPoliza = null;

    this.beneficiario = new Beneficiario();

    this.beneficiario.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.beneficiario.setIdBeneficiario(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.poliza.Beneficiario"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.beneficiario = new Beneficiario();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.beneficiario = new Beneficiario();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }
}