package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.bienestar.EstablecimientoSaludBeanBusiness;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PlanPolizaBeanBusiness;
import sigefirrhh.base.bienestar.TipoSiniestro;
import sigefirrhh.base.bienestar.TipoSiniestroBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class SiniestroBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSiniestro(Siniestro siniestro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Siniestro siniestroNew = 
      (Siniestro)BeanUtils.cloneBean(
      siniestro);

    TipoSiniestroBeanBusiness tipoSiniestroBeanBusiness = new TipoSiniestroBeanBusiness();

    if (siniestroNew.getTipoSiniestro() != null) {
      siniestroNew.setTipoSiniestro(
        tipoSiniestroBeanBusiness.findTipoSiniestroById(
        siniestroNew.getTipoSiniestro().getIdTipoSiniestro()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (siniestroNew.getPlanPoliza() != null) {
      siniestroNew.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        siniestroNew.getPlanPoliza().getIdPlanPoliza()));
    }

    BeneficiarioBeanBusiness beneficiarioBeanBusiness = new BeneficiarioBeanBusiness();

    if (siniestroNew.getBeneficiario() != null) {
      siniestroNew.setBeneficiario(
        beneficiarioBeanBusiness.findBeneficiarioById(
        siniestroNew.getBeneficiario().getIdBeneficiario()));
    }

    TitularBeanBusiness titularBeanBusiness = new TitularBeanBusiness();

    if (siniestroNew.getTitular() != null) {
      siniestroNew.setTitular(
        titularBeanBusiness.findTitularById(
        siniestroNew.getTitular().getIdTitular()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (siniestroNew.getPersonal() != null) {
      siniestroNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        siniestroNew.getPersonal().getIdPersonal()));
    }

    EstablecimientoSaludBeanBusiness establecimientoSaludBeanBusiness = new EstablecimientoSaludBeanBusiness();

    if (siniestroNew.getEstablecimientoSalud() != null) {
      siniestroNew.setEstablecimientoSalud(
        establecimientoSaludBeanBusiness.findEstablecimientoSaludById(
        siniestroNew.getEstablecimientoSalud().getIdEstablecimientoSalud()));
    }
    pm.makePersistent(siniestroNew);
  }

  public void updateSiniestro(Siniestro siniestro) throws Exception
  {
    Siniestro siniestroModify = 
      findSiniestroById(siniestro.getIdSiniestro());

    TipoSiniestroBeanBusiness tipoSiniestroBeanBusiness = new TipoSiniestroBeanBusiness();

    if (siniestro.getTipoSiniestro() != null) {
      siniestro.setTipoSiniestro(
        tipoSiniestroBeanBusiness.findTipoSiniestroById(
        siniestro.getTipoSiniestro().getIdTipoSiniestro()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (siniestro.getPlanPoliza() != null) {
      siniestro.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        siniestro.getPlanPoliza().getIdPlanPoliza()));
    }

    BeneficiarioBeanBusiness beneficiarioBeanBusiness = new BeneficiarioBeanBusiness();

    if (siniestro.getBeneficiario() != null) {
      siniestro.setBeneficiario(
        beneficiarioBeanBusiness.findBeneficiarioById(
        siniestro.getBeneficiario().getIdBeneficiario()));
    }

    TitularBeanBusiness titularBeanBusiness = new TitularBeanBusiness();

    if (siniestro.getTitular() != null) {
      siniestro.setTitular(
        titularBeanBusiness.findTitularById(
        siniestro.getTitular().getIdTitular()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (siniestro.getPersonal() != null) {
      siniestro.setPersonal(
        personalBeanBusiness.findPersonalById(
        siniestro.getPersonal().getIdPersonal()));
    }

    EstablecimientoSaludBeanBusiness establecimientoSaludBeanBusiness = new EstablecimientoSaludBeanBusiness();

    if (siniestro.getEstablecimientoSalud() != null) {
      siniestro.setEstablecimientoSalud(
        establecimientoSaludBeanBusiness.findEstablecimientoSaludById(
        siniestro.getEstablecimientoSalud().getIdEstablecimientoSalud()));
    }

    BeanUtils.copyProperties(siniestroModify, siniestro);
  }

  public void deleteSiniestro(Siniestro siniestro) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Siniestro siniestroDelete = 
      findSiniestroById(siniestro.getIdSiniestro());
    pm.deletePersistent(siniestroDelete);
  }

  public Siniestro findSiniestroById(long idSiniestro) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSiniestro == pIdSiniestro";
    Query query = pm.newQuery(Siniestro.class, filter);

    query.declareParameters("long pIdSiniestro");

    parameters.put("pIdSiniestro", new Long(idSiniestro));

    Collection colSiniestro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSiniestro.iterator();
    return (Siniestro)iterator.next();
  }

  public Collection findSiniestroAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent siniestroExtent = pm.getExtent(
      Siniestro.class, true);
    Query query = pm.newQuery(siniestroExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByBeneficiario(long idBeneficiario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "beneficiario.idBeneficiario == pIdBeneficiario";

    Query query = pm.newQuery(Siniestro.class, filter);

    query.declareParameters("long pIdBeneficiario");
    HashMap parameters = new HashMap();

    parameters.put("pIdBeneficiario", new Long(idBeneficiario));

    Collection colSiniestro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSiniestro);

    return colSiniestro;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Siniestro.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colSiniestro = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSiniestro);

    return colSiniestro;
  }
}