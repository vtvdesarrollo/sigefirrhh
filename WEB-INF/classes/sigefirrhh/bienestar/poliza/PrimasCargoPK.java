package sigefirrhh.bienestar.poliza;

import java.io.Serializable;

public class PrimasCargoPK
  implements Serializable
{
  public long idPrimasCargo;

  public PrimasCargoPK()
  {
  }

  public PrimasCargoPK(long idPrimasCargo)
  {
    this.idPrimasCargo = idPrimasCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((PrimasCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(PrimasCargoPK thatPK)
  {
    return 
      this.idPrimasCargo == thatPK.idPrimasCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idPrimasCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idPrimasCargo);
  }
}