package sigefirrhh.bienestar.poliza;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.expediente.Personal;

public class Titular
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idTitular;
  private double montoPrima;
  private double montoPatron;
  private double montoTrabajador;
  private double cobertura;
  private double coberturaExtra;
  private double primaExtra;
  private Date fechaInclusion;
  private Date fechaExclusion;
  private String estatus;
  private TipoPersonal tipoPersonal;
  private Personal personal;
  private PrimasPlan primasPlan;
  private PlanPoliza planPoliza;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cobertura", "coberturaExtra", "estatus", "fechaExclusion", "fechaInclusion", "idTitular", "montoPatron", "montoPrima", "montoTrabajador", "personal", "planPoliza", "primaExtra", "primasPlan", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.PrimasPlan"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 26, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.poliza.Titular"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Titular());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
  }

  public String toString()
  {
    return jdoGetplanPoliza(this).toString();
  }

  public double getCobertura() {
    return jdoGetcobertura(this);
  }

  public void setCobertura(double cobertura) {
    jdoSetcobertura(this, cobertura);
  }

  public double getCoberturaExtra() {
    return jdoGetcoberturaExtra(this);
  }

  public void setCoberturaExtra(double coberturaExtra) {
    jdoSetcoberturaExtra(this, coberturaExtra);
  }

  public String getEstatus() {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }

  public long getIdTitular() {
    return jdoGetidTitular(this);
  }

  public void setIdTitular(long idTitular) {
    jdoSetidTitular(this, idTitular);
  }

  public double getMontoPatron() {
    return jdoGetmontoPatron(this);
  }

  public void setMontoPatron(double montoPatron) {
    jdoSetmontoPatron(this, montoPatron);
  }

  public double getMontoPrima() {
    return jdoGetmontoPrima(this);
  }

  public void setMontoPrima(double montoPrima) {
    jdoSetmontoPrima(this, montoPrima);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public PlanPoliza getPlanPoliza() {
    return jdoGetplanPoliza(this);
  }

  public void setPlanPoliza(PlanPoliza planPoliza) {
    jdoSetplanPoliza(this, planPoliza);
  }

  public double getPrimaExtra() {
    return jdoGetprimaExtra(this);
  }

  public void setPrimaExtra(double primaExtra) {
    jdoSetprimaExtra(this, primaExtra);
  }

  public PrimasPlan getPrimasPlan() {
    return jdoGetprimasPlan(this);
  }

  public void setPrimasPlan(PrimasPlan primasPlan) {
    jdoSetprimasPlan(this, primasPlan);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public Date getFechaExclusion() {
    return jdoGetfechaExclusion(this);
  }

  public void setFechaExclusion(Date fechaExclusion) {
    jdoSetfechaExclusion(this, fechaExclusion);
  }

  public Date getFechaInclusion() {
    return jdoGetfechaInclusion(this);
  }

  public void setFechaInclusion(Date fechaInclusion) {
    jdoSetfechaInclusion(this, fechaInclusion);
  }

  public double getMontoTrabajador() {
    return jdoGetmontoTrabajador(this);
  }

  public void setMontoTrabajador(double montoTrabajador) {
    jdoSetmontoTrabajador(this, montoTrabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Titular localTitular = new Titular();
    localTitular.jdoFlags = 1;
    localTitular.jdoStateManager = paramStateManager;
    return localTitular;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Titular localTitular = new Titular();
    localTitular.jdoCopyKeyFieldsFromObjectId(paramObject);
    localTitular.jdoFlags = 1;
    localTitular.jdoStateManager = paramStateManager;
    return localTitular;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.cobertura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.coberturaExtra);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaExclusion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInclusion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idTitular);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPatron);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPrima);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoTrabajador);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaExtra);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.primasPlan);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cobertura = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.coberturaExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaExclusion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInclusion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idTitular = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPrima = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasPlan = ((PrimasPlan)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Titular paramTitular, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.cobertura = paramTitular.cobertura;
      return;
    case 1:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.coberturaExtra = paramTitular.coberturaExtra;
      return;
    case 2:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramTitular.estatus;
      return;
    case 3:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.fechaExclusion = paramTitular.fechaExclusion;
      return;
    case 4:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInclusion = paramTitular.fechaInclusion;
      return;
    case 5:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.idTitular = paramTitular.idTitular;
      return;
    case 6:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.montoPatron = paramTitular.montoPatron;
      return;
    case 7:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.montoPrima = paramTitular.montoPrima;
      return;
    case 8:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.montoTrabajador = paramTitular.montoTrabajador;
      return;
    case 9:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramTitular.personal;
      return;
    case 10:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramTitular.planPoliza;
      return;
    case 11:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.primaExtra = paramTitular.primaExtra;
      return;
    case 12:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.primasPlan = paramTitular.primasPlan;
      return;
    case 13:
      if (paramTitular == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramTitular.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Titular))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Titular localTitular = (Titular)paramObject;
    if (localTitular.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localTitular, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new TitularPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new TitularPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TitularPK))
      throw new IllegalArgumentException("arg1");
    TitularPK localTitularPK = (TitularPK)paramObject;
    localTitularPK.idTitular = this.idTitular;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof TitularPK))
      throw new IllegalArgumentException("arg1");
    TitularPK localTitularPK = (TitularPK)paramObject;
    this.idTitular = localTitularPK.idTitular;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TitularPK))
      throw new IllegalArgumentException("arg2");
    TitularPK localTitularPK = (TitularPK)paramObject;
    localTitularPK.idTitular = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof TitularPK))
      throw new IllegalArgumentException("arg2");
    TitularPK localTitularPK = (TitularPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localTitularPK.idTitular);
  }

  private static final double jdoGetcobertura(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.cobertura;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.cobertura;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 0))
      return paramTitular.cobertura;
    return localStateManager.getDoubleField(paramTitular, jdoInheritedFieldCount + 0, paramTitular.cobertura);
  }

  private static final void jdoSetcobertura(Titular paramTitular, double paramDouble)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.cobertura = paramDouble;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.cobertura = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTitular, jdoInheritedFieldCount + 0, paramTitular.cobertura, paramDouble);
  }

  private static final double jdoGetcoberturaExtra(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.coberturaExtra;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.coberturaExtra;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 1))
      return paramTitular.coberturaExtra;
    return localStateManager.getDoubleField(paramTitular, jdoInheritedFieldCount + 1, paramTitular.coberturaExtra);
  }

  private static final void jdoSetcoberturaExtra(Titular paramTitular, double paramDouble)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.coberturaExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.coberturaExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTitular, jdoInheritedFieldCount + 1, paramTitular.coberturaExtra, paramDouble);
  }

  private static final String jdoGetestatus(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.estatus;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.estatus;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 2))
      return paramTitular.estatus;
    return localStateManager.getStringField(paramTitular, jdoInheritedFieldCount + 2, paramTitular.estatus);
  }

  private static final void jdoSetestatus(Titular paramTitular, String paramString)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramTitular, jdoInheritedFieldCount + 2, paramTitular.estatus, paramString);
  }

  private static final Date jdoGetfechaExclusion(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.fechaExclusion;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.fechaExclusion;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 3))
      return paramTitular.fechaExclusion;
    return (Date)localStateManager.getObjectField(paramTitular, jdoInheritedFieldCount + 3, paramTitular.fechaExclusion);
  }

  private static final void jdoSetfechaExclusion(Titular paramTitular, Date paramDate)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.fechaExclusion = paramDate;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.fechaExclusion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTitular, jdoInheritedFieldCount + 3, paramTitular.fechaExclusion, paramDate);
  }

  private static final Date jdoGetfechaInclusion(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.fechaInclusion;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.fechaInclusion;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 4))
      return paramTitular.fechaInclusion;
    return (Date)localStateManager.getObjectField(paramTitular, jdoInheritedFieldCount + 4, paramTitular.fechaInclusion);
  }

  private static final void jdoSetfechaInclusion(Titular paramTitular, Date paramDate)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.fechaInclusion = paramDate;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.fechaInclusion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramTitular, jdoInheritedFieldCount + 4, paramTitular.fechaInclusion, paramDate);
  }

  private static final long jdoGetidTitular(Titular paramTitular)
  {
    return paramTitular.idTitular;
  }

  private static final void jdoSetidTitular(Titular paramTitular, long paramLong)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.idTitular = paramLong;
      return;
    }
    localStateManager.setLongField(paramTitular, jdoInheritedFieldCount + 5, paramTitular.idTitular, paramLong);
  }

  private static final double jdoGetmontoPatron(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.montoPatron;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.montoPatron;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 6))
      return paramTitular.montoPatron;
    return localStateManager.getDoubleField(paramTitular, jdoInheritedFieldCount + 6, paramTitular.montoPatron);
  }

  private static final void jdoSetmontoPatron(Titular paramTitular, double paramDouble)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.montoPatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.montoPatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTitular, jdoInheritedFieldCount + 6, paramTitular.montoPatron, paramDouble);
  }

  private static final double jdoGetmontoPrima(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.montoPrima;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.montoPrima;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 7))
      return paramTitular.montoPrima;
    return localStateManager.getDoubleField(paramTitular, jdoInheritedFieldCount + 7, paramTitular.montoPrima);
  }

  private static final void jdoSetmontoPrima(Titular paramTitular, double paramDouble)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.montoPrima = paramDouble;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.montoPrima = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTitular, jdoInheritedFieldCount + 7, paramTitular.montoPrima, paramDouble);
  }

  private static final double jdoGetmontoTrabajador(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.montoTrabajador;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.montoTrabajador;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 8))
      return paramTitular.montoTrabajador;
    return localStateManager.getDoubleField(paramTitular, jdoInheritedFieldCount + 8, paramTitular.montoTrabajador);
  }

  private static final void jdoSetmontoTrabajador(Titular paramTitular, double paramDouble)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.montoTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.montoTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTitular, jdoInheritedFieldCount + 8, paramTitular.montoTrabajador, paramDouble);
  }

  private static final Personal jdoGetpersonal(Titular paramTitular)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.personal;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 9))
      return paramTitular.personal;
    return (Personal)localStateManager.getObjectField(paramTitular, jdoInheritedFieldCount + 9, paramTitular.personal);
  }

  private static final void jdoSetpersonal(Titular paramTitular, Personal paramPersonal)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramTitular, jdoInheritedFieldCount + 9, paramTitular.personal, paramPersonal);
  }

  private static final PlanPoliza jdoGetplanPoliza(Titular paramTitular)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.planPoliza;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 10))
      return paramTitular.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramTitular, jdoInheritedFieldCount + 10, paramTitular.planPoliza);
  }

  private static final void jdoSetplanPoliza(Titular paramTitular, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramTitular, jdoInheritedFieldCount + 10, paramTitular.planPoliza, paramPlanPoliza);
  }

  private static final double jdoGetprimaExtra(Titular paramTitular)
  {
    if (paramTitular.jdoFlags <= 0)
      return paramTitular.primaExtra;
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.primaExtra;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 11))
      return paramTitular.primaExtra;
    return localStateManager.getDoubleField(paramTitular, jdoInheritedFieldCount + 11, paramTitular.primaExtra);
  }

  private static final void jdoSetprimaExtra(Titular paramTitular, double paramDouble)
  {
    if (paramTitular.jdoFlags == 0)
    {
      paramTitular.primaExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.primaExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramTitular, jdoInheritedFieldCount + 11, paramTitular.primaExtra, paramDouble);
  }

  private static final PrimasPlan jdoGetprimasPlan(Titular paramTitular)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.primasPlan;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 12))
      return paramTitular.primasPlan;
    return (PrimasPlan)localStateManager.getObjectField(paramTitular, jdoInheritedFieldCount + 12, paramTitular.primasPlan);
  }

  private static final void jdoSetprimasPlan(Titular paramTitular, PrimasPlan paramPrimasPlan)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.primasPlan = paramPrimasPlan;
      return;
    }
    localStateManager.setObjectField(paramTitular, jdoInheritedFieldCount + 12, paramTitular.primasPlan, paramPrimasPlan);
  }

  private static final TipoPersonal jdoGettipoPersonal(Titular paramTitular)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
      return paramTitular.tipoPersonal;
    if (localStateManager.isLoaded(paramTitular, jdoInheritedFieldCount + 13))
      return paramTitular.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramTitular, jdoInheritedFieldCount + 13, paramTitular.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Titular paramTitular, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramTitular.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramTitular, jdoInheritedFieldCount + 13, paramTitular.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}