package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class PolizaFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PolizaBusiness polizaBusiness = new PolizaBusiness();

  public void addBeneficiario(Beneficiario beneficiario)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.polizaBusiness.addBeneficiario(beneficiario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateBeneficiario(Beneficiario beneficiario) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.updateBeneficiario(beneficiario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteBeneficiario(Beneficiario beneficiario) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.deleteBeneficiario(beneficiario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Beneficiario findBeneficiarioById(long beneficiarioId) throws Exception
  {
    try { this.txn.open();
      Beneficiario beneficiario = 
        this.polizaBusiness.findBeneficiarioById(beneficiarioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(beneficiario);
      return beneficiario;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllBeneficiario() throws Exception
  {
    try { this.txn.open();
      return this.polizaBusiness.findAllBeneficiario();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBeneficiarioByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findBeneficiarioByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBeneficiarioByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findBeneficiarioByPlanPoliza(idPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSiniestro(Siniestro siniestro)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.polizaBusiness.addSiniestro(siniestro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSiniestro(Siniestro siniestro) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.updateSiniestro(siniestro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSiniestro(Siniestro siniestro) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.deleteSiniestro(siniestro);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Siniestro findSiniestroById(long siniestroId) throws Exception
  {
    try { this.txn.open();
      Siniestro siniestro = 
        this.polizaBusiness.findSiniestroById(siniestroId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(siniestro);
      return siniestro;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSiniestro() throws Exception
  {
    try { this.txn.open();
      return this.polizaBusiness.findAllSiniestro();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSiniestroByBeneficiario(long idBeneficiario)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findSiniestroByBeneficiario(idBeneficiario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSiniestroByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findSiniestroByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addTitular(Titular titular)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.polizaBusiness.addTitular(titular);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateTitular(Titular titular) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.updateTitular(titular);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteTitular(Titular titular) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.deleteTitular(titular);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Titular findTitularById(long titularId) throws Exception
  {
    try { this.txn.open();
      Titular titular = 
        this.polizaBusiness.findTitularById(titularId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(titular);
      return titular;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllTitular() throws Exception
  {
    try { this.txn.open();
      return this.polizaBusiness.findAllTitular();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTitularByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findTitularByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTitularByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findTitularByPlanPoliza(idPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addExcepcionTitular(ExcepcionTitular excepcionTitular)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.polizaBusiness.addExcepcionTitular(excepcionTitular);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateExcepcionTitular(ExcepcionTitular excepcionTitular) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.updateExcepcionTitular(excepcionTitular);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteExcepcionTitular(ExcepcionTitular excepcionTitular) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.deleteExcepcionTitular(excepcionTitular);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ExcepcionTitular findExcepcionTitularById(long excepcionTitularId) throws Exception
  {
    try { this.txn.open();
      ExcepcionTitular excepcionTitular = 
        this.polizaBusiness.findExcepcionTitularById(excepcionTitularId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(excepcionTitular);
      return excepcionTitular;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllExcepcionTitular() throws Exception
  {
    try { this.txn.open();
      return this.polizaBusiness.findAllExcepcionTitular();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExcepcionTitularByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findExcepcionTitularByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExcepcionTitularByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findExcepcionTitularByPlanPoliza(idPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.polizaBusiness.addExcepcionBeneficiario(excepcionBeneficiario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.updateExcepcionBeneficiario(excepcionBeneficiario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.deleteExcepcionBeneficiario(excepcionBeneficiario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ExcepcionBeneficiario findExcepcionBeneficiarioById(long excepcionBeneficiarioId) throws Exception
  {
    try { this.txn.open();
      ExcepcionBeneficiario excepcionBeneficiario = 
        this.polizaBusiness.findExcepcionBeneficiarioById(excepcionBeneficiarioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(excepcionBeneficiario);
      return excepcionBeneficiario;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllExcepcionBeneficiario() throws Exception
  {
    try { this.txn.open();
      return this.polizaBusiness.findAllExcepcionBeneficiario();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExcepcionBeneficiarioByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findExcepcionBeneficiarioByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExcepcionBeneficiarioByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findExcepcionBeneficiarioByPlanPoliza(idPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addPrimasCargo(PrimasCargo primasCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.polizaBusiness.addPrimasCargo(primasCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updatePrimasCargo(PrimasCargo primasCargo) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.updatePrimasCargo(primasCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deletePrimasCargo(PrimasCargo primasCargo) throws Exception
  {
    try { this.txn.open();
      this.polizaBusiness.deletePrimasCargo(primasCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public PrimasCargo findPrimasCargoById(long primasCargoId) throws Exception
  {
    try { this.txn.open();
      PrimasCargo primasCargo = 
        this.polizaBusiness.findPrimasCargoById(primasCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(primasCargo);
      return primasCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllPrimasCargo() throws Exception
  {
    try { this.txn.open();
      return this.polizaBusiness.findAllPrimasCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findPrimasCargoByPrimasPlan(long idPrimasPlan)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.polizaBusiness.findPrimasCargoByPrimasPlan(idPrimasPlan);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}