package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PlanPolizaBeanBusiness;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.bienestar.PrimasPlanBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class TitularBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addTitular(Titular titular)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Titular titularNew = 
      (Titular)BeanUtils.cloneBean(
      titular);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (titularNew.getTipoPersonal() != null) {
      titularNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        titularNew.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (titularNew.getPersonal() != null) {
      titularNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        titularNew.getPersonal().getIdPersonal()));
    }

    PrimasPlanBeanBusiness primasPlanBeanBusiness = new PrimasPlanBeanBusiness();

    if (titularNew.getPrimasPlan() != null) {
      titularNew.setPrimasPlan(
        primasPlanBeanBusiness.findPrimasPlanById(
        titularNew.getPrimasPlan().getIdPrimasPlan()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (titularNew.getPlanPoliza() != null) {
      titularNew.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        titularNew.getPlanPoliza().getIdPlanPoliza()));
    }
    pm.makePersistent(titularNew);
  }

  public void updateTitular(Titular titular) throws Exception
  {
    Titular titularModify = 
      findTitularById(titular.getIdTitular());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (titular.getTipoPersonal() != null) {
      titular.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        titular.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (titular.getPersonal() != null) {
      titular.setPersonal(
        personalBeanBusiness.findPersonalById(
        titular.getPersonal().getIdPersonal()));
    }

    PrimasPlanBeanBusiness primasPlanBeanBusiness = new PrimasPlanBeanBusiness();

    if (titular.getPrimasPlan() != null) {
      titular.setPrimasPlan(
        primasPlanBeanBusiness.findPrimasPlanById(
        titular.getPrimasPlan().getIdPrimasPlan()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (titular.getPlanPoliza() != null) {
      titular.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        titular.getPlanPoliza().getIdPlanPoliza()));
    }

    BeanUtils.copyProperties(titularModify, titular);
  }

  public void deleteTitular(Titular titular) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Titular titularDelete = 
      findTitularById(titular.getIdTitular());
    pm.deletePersistent(titularDelete);
  }

  public Titular findTitularById(long idTitular) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idTitular == pIdTitular";
    Query query = pm.newQuery(Titular.class, filter);

    query.declareParameters("long pIdTitular");

    parameters.put("pIdTitular", new Long(idTitular));

    Collection colTitular = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colTitular.iterator();
    return (Titular)iterator.next();
  }

  public Collection findTitularAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent titularExtent = pm.getExtent(
      Titular.class, true);
    Query query = pm.newQuery(titularExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Titular.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colTitular = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTitular);

    return colTitular;
  }

  public Collection findByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPoliza.idPlanPoliza == pIdPlanPoliza";

    Query query = pm.newQuery(Titular.class, filter);

    query.declareParameters("long pIdPlanPoliza");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));

    Collection colTitular = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTitular);

    return colTitular;
  }
}