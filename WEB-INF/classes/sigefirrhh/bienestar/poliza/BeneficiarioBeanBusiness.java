package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PlanPolizaBeanBusiness;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.bienestar.PrimasPlanBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.FamiliarBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class BeneficiarioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addBeneficiario(Beneficiario beneficiario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Beneficiario beneficiarioNew = 
      (Beneficiario)BeanUtils.cloneBean(
      beneficiario);

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (beneficiarioNew.getFamiliar() != null) {
      beneficiarioNew.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        beneficiarioNew.getFamiliar().getIdFamiliar()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (beneficiarioNew.getTipoPersonal() != null) {
      beneficiarioNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        beneficiarioNew.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (beneficiarioNew.getPersonal() != null) {
      beneficiarioNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        beneficiarioNew.getPersonal().getIdPersonal()));
    }

    PrimasPlanBeanBusiness primasPlanBeanBusiness = new PrimasPlanBeanBusiness();

    if (beneficiarioNew.getPrimasPlan() != null) {
      beneficiarioNew.setPrimasPlan(
        primasPlanBeanBusiness.findPrimasPlanById(
        beneficiarioNew.getPrimasPlan().getIdPrimasPlan()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (beneficiarioNew.getPlanPoliza() != null) {
      beneficiarioNew.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        beneficiarioNew.getPlanPoliza().getIdPlanPoliza()));
    }
    pm.makePersistent(beneficiarioNew);
  }

  public void updateBeneficiario(Beneficiario beneficiario) throws Exception
  {
    Beneficiario beneficiarioModify = 
      findBeneficiarioById(beneficiario.getIdBeneficiario());

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (beneficiario.getFamiliar() != null) {
      beneficiario.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        beneficiario.getFamiliar().getIdFamiliar()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (beneficiario.getTipoPersonal() != null) {
      beneficiario.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        beneficiario.getTipoPersonal().getIdTipoPersonal()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (beneficiario.getPersonal() != null) {
      beneficiario.setPersonal(
        personalBeanBusiness.findPersonalById(
        beneficiario.getPersonal().getIdPersonal()));
    }

    PrimasPlanBeanBusiness primasPlanBeanBusiness = new PrimasPlanBeanBusiness();

    if (beneficiario.getPrimasPlan() != null) {
      beneficiario.setPrimasPlan(
        primasPlanBeanBusiness.findPrimasPlanById(
        beneficiario.getPrimasPlan().getIdPrimasPlan()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (beneficiario.getPlanPoliza() != null) {
      beneficiario.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        beneficiario.getPlanPoliza().getIdPlanPoliza()));
    }

    BeanUtils.copyProperties(beneficiarioModify, beneficiario);
  }

  public void deleteBeneficiario(Beneficiario beneficiario) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Beneficiario beneficiarioDelete = 
      findBeneficiarioById(beneficiario.getIdBeneficiario());
    pm.deletePersistent(beneficiarioDelete);
  }

  public Beneficiario findBeneficiarioById(long idBeneficiario) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idBeneficiario == pIdBeneficiario";
    Query query = pm.newQuery(Beneficiario.class, filter);

    query.declareParameters("long pIdBeneficiario");

    parameters.put("pIdBeneficiario", new Long(idBeneficiario));

    Collection colBeneficiario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colBeneficiario.iterator();
    return (Beneficiario)iterator.next();
  }

  public Collection findBeneficiarioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent beneficiarioExtent = pm.getExtent(
      Beneficiario.class, true);
    Query query = pm.newQuery(beneficiarioExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Beneficiario.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colBeneficiario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBeneficiario);

    return colBeneficiario;
  }

  public Collection findByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPoliza.idPlanPoliza == pIdPlanPoliza";

    Query query = pm.newQuery(Beneficiario.class, filter);

    query.declareParameters("long pIdPlanPoliza");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));

    Collection colBeneficiario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBeneficiario);

    return colBeneficiario;
  }
}