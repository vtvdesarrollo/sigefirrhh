package sigefirrhh.bienestar.poliza;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.personal.expediente.Personal;

public class ExcepcionTitular
  implements Serializable, PersistenceCapable
{
  private long idExcepcionTitular;
  private Personal personal;
  private PlanPoliza planPoliza;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idExcepcionTitular", "personal", "planPoliza" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetplanPoliza(this).toString();
  }

  public long getIdExcepcionTitular() {
    return jdoGetidExcepcionTitular(this);
  }

  public void setIdExcepcionTitular(long idExcepcionTitular) {
    jdoSetidExcepcionTitular(this, idExcepcionTitular);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public PlanPoliza getPlanPoliza() {
    return jdoGetplanPoliza(this);
  }

  public void setPlanPoliza(PlanPoliza planPoliza) {
    jdoSetplanPoliza(this, planPoliza);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.poliza.ExcepcionTitular"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ExcepcionTitular());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ExcepcionTitular localExcepcionTitular = new ExcepcionTitular();
    localExcepcionTitular.jdoFlags = 1;
    localExcepcionTitular.jdoStateManager = paramStateManager;
    return localExcepcionTitular;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ExcepcionTitular localExcepcionTitular = new ExcepcionTitular();
    localExcepcionTitular.jdoCopyKeyFieldsFromObjectId(paramObject);
    localExcepcionTitular.jdoFlags = 1;
    localExcepcionTitular.jdoStateManager = paramStateManager;
    return localExcepcionTitular;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idExcepcionTitular);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idExcepcionTitular = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ExcepcionTitular paramExcepcionTitular, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramExcepcionTitular == null)
        throw new IllegalArgumentException("arg1");
      this.idExcepcionTitular = paramExcepcionTitular.idExcepcionTitular;
      return;
    case 1:
      if (paramExcepcionTitular == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramExcepcionTitular.personal;
      return;
    case 2:
      if (paramExcepcionTitular == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramExcepcionTitular.planPoliza;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ExcepcionTitular))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ExcepcionTitular localExcepcionTitular = (ExcepcionTitular)paramObject;
    if (localExcepcionTitular.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localExcepcionTitular, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ExcepcionTitularPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ExcepcionTitularPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExcepcionTitularPK))
      throw new IllegalArgumentException("arg1");
    ExcepcionTitularPK localExcepcionTitularPK = (ExcepcionTitularPK)paramObject;
    localExcepcionTitularPK.idExcepcionTitular = this.idExcepcionTitular;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExcepcionTitularPK))
      throw new IllegalArgumentException("arg1");
    ExcepcionTitularPK localExcepcionTitularPK = (ExcepcionTitularPK)paramObject;
    this.idExcepcionTitular = localExcepcionTitularPK.idExcepcionTitular;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExcepcionTitularPK))
      throw new IllegalArgumentException("arg2");
    ExcepcionTitularPK localExcepcionTitularPK = (ExcepcionTitularPK)paramObject;
    localExcepcionTitularPK.idExcepcionTitular = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExcepcionTitularPK))
      throw new IllegalArgumentException("arg2");
    ExcepcionTitularPK localExcepcionTitularPK = (ExcepcionTitularPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localExcepcionTitularPK.idExcepcionTitular);
  }

  private static final long jdoGetidExcepcionTitular(ExcepcionTitular paramExcepcionTitular)
  {
    return paramExcepcionTitular.idExcepcionTitular;
  }

  private static final void jdoSetidExcepcionTitular(ExcepcionTitular paramExcepcionTitular, long paramLong)
  {
    StateManager localStateManager = paramExcepcionTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionTitular.idExcepcionTitular = paramLong;
      return;
    }
    localStateManager.setLongField(paramExcepcionTitular, jdoInheritedFieldCount + 0, paramExcepcionTitular.idExcepcionTitular, paramLong);
  }

  private static final Personal jdoGetpersonal(ExcepcionTitular paramExcepcionTitular)
  {
    StateManager localStateManager = paramExcepcionTitular.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionTitular.personal;
    if (localStateManager.isLoaded(paramExcepcionTitular, jdoInheritedFieldCount + 1))
      return paramExcepcionTitular.personal;
    return (Personal)localStateManager.getObjectField(paramExcepcionTitular, jdoInheritedFieldCount + 1, paramExcepcionTitular.personal);
  }

  private static final void jdoSetpersonal(ExcepcionTitular paramExcepcionTitular, Personal paramPersonal)
  {
    StateManager localStateManager = paramExcepcionTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionTitular.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramExcepcionTitular, jdoInheritedFieldCount + 1, paramExcepcionTitular.personal, paramPersonal);
  }

  private static final PlanPoliza jdoGetplanPoliza(ExcepcionTitular paramExcepcionTitular)
  {
    StateManager localStateManager = paramExcepcionTitular.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionTitular.planPoliza;
    if (localStateManager.isLoaded(paramExcepcionTitular, jdoInheritedFieldCount + 2))
      return paramExcepcionTitular.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramExcepcionTitular, jdoInheritedFieldCount + 2, paramExcepcionTitular.planPoliza);
  }

  private static final void jdoSetplanPoliza(ExcepcionTitular paramExcepcionTitular, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramExcepcionTitular.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionTitular.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramExcepcionTitular, jdoInheritedFieldCount + 2, paramExcepcionTitular.planPoliza, paramPlanPoliza);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}