package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PlanPolizaBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class ExcepcionTitularBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addExcepcionTitular(ExcepcionTitular excepcionTitular)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ExcepcionTitular excepcionTitularNew = 
      (ExcepcionTitular)BeanUtils.cloneBean(
      excepcionTitular);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (excepcionTitularNew.getPersonal() != null) {
      excepcionTitularNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        excepcionTitularNew.getPersonal().getIdPersonal()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (excepcionTitularNew.getPlanPoliza() != null) {
      excepcionTitularNew.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        excepcionTitularNew.getPlanPoliza().getIdPlanPoliza()));
    }
    pm.makePersistent(excepcionTitularNew);
  }

  public void updateExcepcionTitular(ExcepcionTitular excepcionTitular) throws Exception
  {
    ExcepcionTitular excepcionTitularModify = 
      findExcepcionTitularById(excepcionTitular.getIdExcepcionTitular());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (excepcionTitular.getPersonal() != null) {
      excepcionTitular.setPersonal(
        personalBeanBusiness.findPersonalById(
        excepcionTitular.getPersonal().getIdPersonal()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (excepcionTitular.getPlanPoliza() != null) {
      excepcionTitular.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        excepcionTitular.getPlanPoliza().getIdPlanPoliza()));
    }

    BeanUtils.copyProperties(excepcionTitularModify, excepcionTitular);
  }

  public void deleteExcepcionTitular(ExcepcionTitular excepcionTitular) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ExcepcionTitular excepcionTitularDelete = 
      findExcepcionTitularById(excepcionTitular.getIdExcepcionTitular());
    pm.deletePersistent(excepcionTitularDelete);
  }

  public ExcepcionTitular findExcepcionTitularById(long idExcepcionTitular) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idExcepcionTitular == pIdExcepcionTitular";
    Query query = pm.newQuery(ExcepcionTitular.class, filter);

    query.declareParameters("long pIdExcepcionTitular");

    parameters.put("pIdExcepcionTitular", new Long(idExcepcionTitular));

    Collection colExcepcionTitular = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colExcepcionTitular.iterator();
    return (ExcepcionTitular)iterator.next();
  }

  public Collection findExcepcionTitularAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent excepcionTitularExtent = pm.getExtent(
      ExcepcionTitular.class, true);
    Query query = pm.newQuery(excepcionTitularExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ExcepcionTitular.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colExcepcionTitular = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExcepcionTitular);

    return colExcepcionTitular;
  }

  public Collection findByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPoliza.idPlanPoliza == pIdPlanPoliza";

    Query query = pm.newQuery(ExcepcionTitular.class, filter);

    query.declareParameters("long pIdPlanPoliza");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));

    Collection colExcepcionTitular = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExcepcionTitular);

    return colExcepcionTitular;
  }
}