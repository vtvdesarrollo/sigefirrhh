package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class TitularNoGenBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public Collection findByPersonalAndPlanPoliza(long idPersonal, long idPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPoliza.idPlanPoliza == pIdPlanPoliza && personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(Titular.class, filter);

    query.declareParameters("long pIdPlanPoliza, long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));
    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colTitular = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colTitular);

    return colTitular;
  }
}