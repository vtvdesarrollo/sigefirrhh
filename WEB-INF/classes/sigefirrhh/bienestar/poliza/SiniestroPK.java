package sigefirrhh.bienestar.poliza;

import java.io.Serializable;

public class SiniestroPK
  implements Serializable
{
  public long idSiniestro;

  public SiniestroPK()
  {
  }

  public SiniestroPK(long idSiniestro)
  {
    this.idSiniestro = idSiniestro;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SiniestroPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SiniestroPK thatPK)
  {
    return 
      this.idSiniestro == thatPK.idSiniestro;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSiniestro)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSiniestro);
  }
}