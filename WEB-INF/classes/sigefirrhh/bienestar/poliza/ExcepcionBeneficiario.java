package sigefirrhh.bienestar.poliza;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class ExcepcionBeneficiario
  implements Serializable, PersistenceCapable
{
  private long idExcepcionBeneficiario;
  private Familiar familiar;
  private Personal personal;
  private PlanPoliza planPoliza;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "familiar", "idExcepcionBeneficiario", "personal", "planPoliza" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), Long.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetfamiliar(this) + " - " + jdoGetplanPoliza(this).toString();
  }

  public Familiar getFamiliar() {
    return jdoGetfamiliar(this);
  }

  public void setFamiliar(Familiar familiar) {
    jdoSetfamiliar(this, familiar);
  }

  public long getIdExcepcionBeneficiario() {
    return jdoGetidExcepcionBeneficiario(this);
  }

  public void setIdExcepcionBeneficiario(long idExcepcionBeneficiario) {
    jdoSetidExcepcionBeneficiario(this, idExcepcionBeneficiario);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public PlanPoliza getPlanPoliza() {
    return jdoGetplanPoliza(this);
  }

  public void setPlanPoliza(PlanPoliza planPoliza) {
    jdoSetplanPoliza(this, planPoliza);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.poliza.ExcepcionBeneficiario"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ExcepcionBeneficiario());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ExcepcionBeneficiario localExcepcionBeneficiario = new ExcepcionBeneficiario();
    localExcepcionBeneficiario.jdoFlags = 1;
    localExcepcionBeneficiario.jdoStateManager = paramStateManager;
    return localExcepcionBeneficiario;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ExcepcionBeneficiario localExcepcionBeneficiario = new ExcepcionBeneficiario();
    localExcepcionBeneficiario.jdoCopyKeyFieldsFromObjectId(paramObject);
    localExcepcionBeneficiario.jdoFlags = 1;
    localExcepcionBeneficiario.jdoStateManager = paramStateManager;
    return localExcepcionBeneficiario;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idExcepcionBeneficiario);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idExcepcionBeneficiario = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ExcepcionBeneficiario paramExcepcionBeneficiario, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramExcepcionBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramExcepcionBeneficiario.familiar;
      return;
    case 1:
      if (paramExcepcionBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.idExcepcionBeneficiario = paramExcepcionBeneficiario.idExcepcionBeneficiario;
      return;
    case 2:
      if (paramExcepcionBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramExcepcionBeneficiario.personal;
      return;
    case 3:
      if (paramExcepcionBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramExcepcionBeneficiario.planPoliza;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ExcepcionBeneficiario))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ExcepcionBeneficiario localExcepcionBeneficiario = (ExcepcionBeneficiario)paramObject;
    if (localExcepcionBeneficiario.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localExcepcionBeneficiario, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ExcepcionBeneficiarioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ExcepcionBeneficiarioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExcepcionBeneficiarioPK))
      throw new IllegalArgumentException("arg1");
    ExcepcionBeneficiarioPK localExcepcionBeneficiarioPK = (ExcepcionBeneficiarioPK)paramObject;
    localExcepcionBeneficiarioPK.idExcepcionBeneficiario = this.idExcepcionBeneficiario;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExcepcionBeneficiarioPK))
      throw new IllegalArgumentException("arg1");
    ExcepcionBeneficiarioPK localExcepcionBeneficiarioPK = (ExcepcionBeneficiarioPK)paramObject;
    this.idExcepcionBeneficiario = localExcepcionBeneficiarioPK.idExcepcionBeneficiario;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExcepcionBeneficiarioPK))
      throw new IllegalArgumentException("arg2");
    ExcepcionBeneficiarioPK localExcepcionBeneficiarioPK = (ExcepcionBeneficiarioPK)paramObject;
    localExcepcionBeneficiarioPK.idExcepcionBeneficiario = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExcepcionBeneficiarioPK))
      throw new IllegalArgumentException("arg2");
    ExcepcionBeneficiarioPK localExcepcionBeneficiarioPK = (ExcepcionBeneficiarioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localExcepcionBeneficiarioPK.idExcepcionBeneficiario);
  }

  private static final Familiar jdoGetfamiliar(ExcepcionBeneficiario paramExcepcionBeneficiario)
  {
    StateManager localStateManager = paramExcepcionBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionBeneficiario.familiar;
    if (localStateManager.isLoaded(paramExcepcionBeneficiario, jdoInheritedFieldCount + 0))
      return paramExcepcionBeneficiario.familiar;
    return (Familiar)localStateManager.getObjectField(paramExcepcionBeneficiario, jdoInheritedFieldCount + 0, paramExcepcionBeneficiario.familiar);
  }

  private static final void jdoSetfamiliar(ExcepcionBeneficiario paramExcepcionBeneficiario, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramExcepcionBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionBeneficiario.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramExcepcionBeneficiario, jdoInheritedFieldCount + 0, paramExcepcionBeneficiario.familiar, paramFamiliar);
  }

  private static final long jdoGetidExcepcionBeneficiario(ExcepcionBeneficiario paramExcepcionBeneficiario)
  {
    return paramExcepcionBeneficiario.idExcepcionBeneficiario;
  }

  private static final void jdoSetidExcepcionBeneficiario(ExcepcionBeneficiario paramExcepcionBeneficiario, long paramLong)
  {
    StateManager localStateManager = paramExcepcionBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionBeneficiario.idExcepcionBeneficiario = paramLong;
      return;
    }
    localStateManager.setLongField(paramExcepcionBeneficiario, jdoInheritedFieldCount + 1, paramExcepcionBeneficiario.idExcepcionBeneficiario, paramLong);
  }

  private static final Personal jdoGetpersonal(ExcepcionBeneficiario paramExcepcionBeneficiario)
  {
    StateManager localStateManager = paramExcepcionBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionBeneficiario.personal;
    if (localStateManager.isLoaded(paramExcepcionBeneficiario, jdoInheritedFieldCount + 2))
      return paramExcepcionBeneficiario.personal;
    return (Personal)localStateManager.getObjectField(paramExcepcionBeneficiario, jdoInheritedFieldCount + 2, paramExcepcionBeneficiario.personal);
  }

  private static final void jdoSetpersonal(ExcepcionBeneficiario paramExcepcionBeneficiario, Personal paramPersonal)
  {
    StateManager localStateManager = paramExcepcionBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionBeneficiario.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramExcepcionBeneficiario, jdoInheritedFieldCount + 2, paramExcepcionBeneficiario.personal, paramPersonal);
  }

  private static final PlanPoliza jdoGetplanPoliza(ExcepcionBeneficiario paramExcepcionBeneficiario)
  {
    StateManager localStateManager = paramExcepcionBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionBeneficiario.planPoliza;
    if (localStateManager.isLoaded(paramExcepcionBeneficiario, jdoInheritedFieldCount + 3))
      return paramExcepcionBeneficiario.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramExcepcionBeneficiario, jdoInheritedFieldCount + 3, paramExcepcionBeneficiario.planPoliza);
  }

  private static final void jdoSetplanPoliza(ExcepcionBeneficiario paramExcepcionBeneficiario, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramExcepcionBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionBeneficiario.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramExcepcionBeneficiario, jdoInheritedFieldCount + 3, paramExcepcionBeneficiario.planPoliza, paramPlanPoliza);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}