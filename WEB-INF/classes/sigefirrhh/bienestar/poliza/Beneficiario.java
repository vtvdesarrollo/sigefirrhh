package sigefirrhh.bienestar.poliza;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class Beneficiario
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_ESTATUS;
  private long idBeneficiario;
  private double montoPrima;
  private double montoPatron;
  private double montoTrabajador;
  private double cobertura;
  private double coberturaExtra;
  private double primaExtra;
  private Date fechaInclusion;
  private Date fechaExclusion;
  private String estatus;
  private Familiar familiar;
  private TipoPersonal tipoPersonal;
  private Personal personal;
  private PrimasPlan primasPlan;
  private PlanPoliza planPoliza;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cobertura", "coberturaExtra", "estatus", "familiar", "fechaExclusion", "fechaInclusion", "idBeneficiario", "montoPatron", "montoPrima", "montoTrabajador", "personal", "planPoliza", "primaExtra", "primasPlan", "tipoPersonal" }; private static final Class[] jdoFieldTypes = { Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.PrimasPlan"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 21, 21, 24, 21, 21, 21, 26, 26, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.poliza.Beneficiario"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Beneficiario());

    LISTA_ESTATUS = 
      new LinkedHashMap();

    LISTA_ESTATUS.put("A", "ACTIVO");
    LISTA_ESTATUS.put("S", "SUSPENDIDO");
  }

  public String toString()
  {
    return jdoGetfamiliar(this) + " - " + jdoGetplanPoliza(this).toString();
  }

  public double getCobertura() {
    return jdoGetcobertura(this);
  }

  public void setCobertura(double cobertura) {
    jdoSetcobertura(this, cobertura);
  }

  public double getCoberturaExtra() {
    return jdoGetcoberturaExtra(this);
  }

  public void setCoberturaExtra(double coberturaExtra) {
    jdoSetcoberturaExtra(this, coberturaExtra);
  }

  public String getEstatus() {
    return jdoGetestatus(this);
  }

  public void setEstatus(String estatus) {
    jdoSetestatus(this, estatus);
  }

  public Familiar getFamiliar() {
    return jdoGetfamiliar(this);
  }

  public void setFamiliar(Familiar familiar) {
    jdoSetfamiliar(this, familiar);
  }

  public Date getFechaExclusion() {
    return jdoGetfechaExclusion(this);
  }

  public void setFechaExclusion(Date fechaExclusion) {
    jdoSetfechaExclusion(this, fechaExclusion);
  }

  public Date getFechaInclusion() {
    return jdoGetfechaInclusion(this);
  }

  public void setFechaInclusion(Date fechaInclusion) {
    jdoSetfechaInclusion(this, fechaInclusion);
  }

  public long getIdBeneficiario() {
    return jdoGetidBeneficiario(this);
  }

  public void setIdBeneficiario(long idBeneficiario) {
    jdoSetidBeneficiario(this, idBeneficiario);
  }

  public double getMontoPatron() {
    return jdoGetmontoPatron(this);
  }

  public void setMontoPatron(double montoPatron) {
    jdoSetmontoPatron(this, montoPatron);
  }

  public double getMontoPrima() {
    return jdoGetmontoPrima(this);
  }

  public void setMontoPrima(double montoPrima) {
    jdoSetmontoPrima(this, montoPrima);
  }

  public double getMontoTrabajador() {
    return jdoGetmontoTrabajador(this);
  }

  public void setMontoTrabajador(double montoTrabajador) {
    jdoSetmontoTrabajador(this, montoTrabajador);
  }

  public Personal getPersonal() {
    return jdoGetpersonal(this);
  }

  public void setPersonal(Personal personal) {
    jdoSetpersonal(this, personal);
  }

  public PlanPoliza getPlanPoliza() {
    return jdoGetplanPoliza(this);
  }

  public void setPlanPoliza(PlanPoliza planPoliza) {
    jdoSetplanPoliza(this, planPoliza);
  }

  public double getPrimaExtra() {
    return jdoGetprimaExtra(this);
  }

  public void setPrimaExtra(double primaExtra) {
    jdoSetprimaExtra(this, primaExtra);
  }

  public PrimasPlan getPrimasPlan() {
    return jdoGetprimasPlan(this);
  }

  public void setPrimasPlan(PrimasPlan primasPlan) {
    jdoSetprimasPlan(this, primasPlan);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 15;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Beneficiario localBeneficiario = new Beneficiario();
    localBeneficiario.jdoFlags = 1;
    localBeneficiario.jdoStateManager = paramStateManager;
    return localBeneficiario;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Beneficiario localBeneficiario = new Beneficiario();
    localBeneficiario.jdoCopyKeyFieldsFromObjectId(paramObject);
    localBeneficiario.jdoFlags = 1;
    localBeneficiario.jdoStateManager = paramStateManager;
    return localBeneficiario;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.cobertura);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.coberturaExtra);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaExclusion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInclusion);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idBeneficiario);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPatron);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPrima);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoTrabajador);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.primaExtra);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.primasPlan);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cobertura = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.coberturaExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaExclusion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInclusion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idBeneficiario = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPrima = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primaExtra = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasPlan = ((PrimasPlan)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Beneficiario paramBeneficiario, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.cobertura = paramBeneficiario.cobertura;
      return;
    case 1:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.coberturaExtra = paramBeneficiario.coberturaExtra;
      return;
    case 2:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramBeneficiario.estatus;
      return;
    case 3:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramBeneficiario.familiar;
      return;
    case 4:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.fechaExclusion = paramBeneficiario.fechaExclusion;
      return;
    case 5:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInclusion = paramBeneficiario.fechaInclusion;
      return;
    case 6:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.idBeneficiario = paramBeneficiario.idBeneficiario;
      return;
    case 7:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.montoPatron = paramBeneficiario.montoPatron;
      return;
    case 8:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.montoPrima = paramBeneficiario.montoPrima;
      return;
    case 9:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.montoTrabajador = paramBeneficiario.montoTrabajador;
      return;
    case 10:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramBeneficiario.personal;
      return;
    case 11:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramBeneficiario.planPoliza;
      return;
    case 12:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.primaExtra = paramBeneficiario.primaExtra;
      return;
    case 13:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.primasPlan = paramBeneficiario.primasPlan;
      return;
    case 14:
      if (paramBeneficiario == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramBeneficiario.tipoPersonal;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Beneficiario))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Beneficiario localBeneficiario = (Beneficiario)paramObject;
    if (localBeneficiario.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localBeneficiario, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new BeneficiarioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new BeneficiarioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BeneficiarioPK))
      throw new IllegalArgumentException("arg1");
    BeneficiarioPK localBeneficiarioPK = (BeneficiarioPK)paramObject;
    localBeneficiarioPK.idBeneficiario = this.idBeneficiario;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BeneficiarioPK))
      throw new IllegalArgumentException("arg1");
    BeneficiarioPK localBeneficiarioPK = (BeneficiarioPK)paramObject;
    this.idBeneficiario = localBeneficiarioPK.idBeneficiario;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BeneficiarioPK))
      throw new IllegalArgumentException("arg2");
    BeneficiarioPK localBeneficiarioPK = (BeneficiarioPK)paramObject;
    localBeneficiarioPK.idBeneficiario = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BeneficiarioPK))
      throw new IllegalArgumentException("arg2");
    BeneficiarioPK localBeneficiarioPK = (BeneficiarioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localBeneficiarioPK.idBeneficiario);
  }

  private static final double jdoGetcobertura(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.cobertura;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.cobertura;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 0))
      return paramBeneficiario.cobertura;
    return localStateManager.getDoubleField(paramBeneficiario, jdoInheritedFieldCount + 0, paramBeneficiario.cobertura);
  }

  private static final void jdoSetcobertura(Beneficiario paramBeneficiario, double paramDouble)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.cobertura = paramDouble;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.cobertura = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBeneficiario, jdoInheritedFieldCount + 0, paramBeneficiario.cobertura, paramDouble);
  }

  private static final double jdoGetcoberturaExtra(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.coberturaExtra;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.coberturaExtra;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 1))
      return paramBeneficiario.coberturaExtra;
    return localStateManager.getDoubleField(paramBeneficiario, jdoInheritedFieldCount + 1, paramBeneficiario.coberturaExtra);
  }

  private static final void jdoSetcoberturaExtra(Beneficiario paramBeneficiario, double paramDouble)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.coberturaExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.coberturaExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBeneficiario, jdoInheritedFieldCount + 1, paramBeneficiario.coberturaExtra, paramDouble);
  }

  private static final String jdoGetestatus(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.estatus;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.estatus;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 2))
      return paramBeneficiario.estatus;
    return localStateManager.getStringField(paramBeneficiario, jdoInheritedFieldCount + 2, paramBeneficiario.estatus);
  }

  private static final void jdoSetestatus(Beneficiario paramBeneficiario, String paramString)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramBeneficiario, jdoInheritedFieldCount + 2, paramBeneficiario.estatus, paramString);
  }

  private static final Familiar jdoGetfamiliar(Beneficiario paramBeneficiario)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.familiar;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 3))
      return paramBeneficiario.familiar;
    return (Familiar)localStateManager.getObjectField(paramBeneficiario, jdoInheritedFieldCount + 3, paramBeneficiario.familiar);
  }

  private static final void jdoSetfamiliar(Beneficiario paramBeneficiario, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramBeneficiario, jdoInheritedFieldCount + 3, paramBeneficiario.familiar, paramFamiliar);
  }

  private static final Date jdoGetfechaExclusion(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.fechaExclusion;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.fechaExclusion;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 4))
      return paramBeneficiario.fechaExclusion;
    return (Date)localStateManager.getObjectField(paramBeneficiario, jdoInheritedFieldCount + 4, paramBeneficiario.fechaExclusion);
  }

  private static final void jdoSetfechaExclusion(Beneficiario paramBeneficiario, Date paramDate)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.fechaExclusion = paramDate;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.fechaExclusion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramBeneficiario, jdoInheritedFieldCount + 4, paramBeneficiario.fechaExclusion, paramDate);
  }

  private static final Date jdoGetfechaInclusion(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.fechaInclusion;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.fechaInclusion;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 5))
      return paramBeneficiario.fechaInclusion;
    return (Date)localStateManager.getObjectField(paramBeneficiario, jdoInheritedFieldCount + 5, paramBeneficiario.fechaInclusion);
  }

  private static final void jdoSetfechaInclusion(Beneficiario paramBeneficiario, Date paramDate)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.fechaInclusion = paramDate;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.fechaInclusion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramBeneficiario, jdoInheritedFieldCount + 5, paramBeneficiario.fechaInclusion, paramDate);
  }

  private static final long jdoGetidBeneficiario(Beneficiario paramBeneficiario)
  {
    return paramBeneficiario.idBeneficiario;
  }

  private static final void jdoSetidBeneficiario(Beneficiario paramBeneficiario, long paramLong)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.idBeneficiario = paramLong;
      return;
    }
    localStateManager.setLongField(paramBeneficiario, jdoInheritedFieldCount + 6, paramBeneficiario.idBeneficiario, paramLong);
  }

  private static final double jdoGetmontoPatron(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.montoPatron;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.montoPatron;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 7))
      return paramBeneficiario.montoPatron;
    return localStateManager.getDoubleField(paramBeneficiario, jdoInheritedFieldCount + 7, paramBeneficiario.montoPatron);
  }

  private static final void jdoSetmontoPatron(Beneficiario paramBeneficiario, double paramDouble)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.montoPatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.montoPatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBeneficiario, jdoInheritedFieldCount + 7, paramBeneficiario.montoPatron, paramDouble);
  }

  private static final double jdoGetmontoPrima(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.montoPrima;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.montoPrima;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 8))
      return paramBeneficiario.montoPrima;
    return localStateManager.getDoubleField(paramBeneficiario, jdoInheritedFieldCount + 8, paramBeneficiario.montoPrima);
  }

  private static final void jdoSetmontoPrima(Beneficiario paramBeneficiario, double paramDouble)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.montoPrima = paramDouble;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.montoPrima = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBeneficiario, jdoInheritedFieldCount + 8, paramBeneficiario.montoPrima, paramDouble);
  }

  private static final double jdoGetmontoTrabajador(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.montoTrabajador;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.montoTrabajador;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 9))
      return paramBeneficiario.montoTrabajador;
    return localStateManager.getDoubleField(paramBeneficiario, jdoInheritedFieldCount + 9, paramBeneficiario.montoTrabajador);
  }

  private static final void jdoSetmontoTrabajador(Beneficiario paramBeneficiario, double paramDouble)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.montoTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.montoTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBeneficiario, jdoInheritedFieldCount + 9, paramBeneficiario.montoTrabajador, paramDouble);
  }

  private static final Personal jdoGetpersonal(Beneficiario paramBeneficiario)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.personal;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 10))
      return paramBeneficiario.personal;
    return (Personal)localStateManager.getObjectField(paramBeneficiario, jdoInheritedFieldCount + 10, paramBeneficiario.personal);
  }

  private static final void jdoSetpersonal(Beneficiario paramBeneficiario, Personal paramPersonal)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramBeneficiario, jdoInheritedFieldCount + 10, paramBeneficiario.personal, paramPersonal);
  }

  private static final PlanPoliza jdoGetplanPoliza(Beneficiario paramBeneficiario)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.planPoliza;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 11))
      return paramBeneficiario.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramBeneficiario, jdoInheritedFieldCount + 11, paramBeneficiario.planPoliza);
  }

  private static final void jdoSetplanPoliza(Beneficiario paramBeneficiario, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramBeneficiario, jdoInheritedFieldCount + 11, paramBeneficiario.planPoliza, paramPlanPoliza);
  }

  private static final double jdoGetprimaExtra(Beneficiario paramBeneficiario)
  {
    if (paramBeneficiario.jdoFlags <= 0)
      return paramBeneficiario.primaExtra;
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.primaExtra;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 12))
      return paramBeneficiario.primaExtra;
    return localStateManager.getDoubleField(paramBeneficiario, jdoInheritedFieldCount + 12, paramBeneficiario.primaExtra);
  }

  private static final void jdoSetprimaExtra(Beneficiario paramBeneficiario, double paramDouble)
  {
    if (paramBeneficiario.jdoFlags == 0)
    {
      paramBeneficiario.primaExtra = paramDouble;
      return;
    }
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.primaExtra = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBeneficiario, jdoInheritedFieldCount + 12, paramBeneficiario.primaExtra, paramDouble);
  }

  private static final PrimasPlan jdoGetprimasPlan(Beneficiario paramBeneficiario)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.primasPlan;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 13))
      return paramBeneficiario.primasPlan;
    return (PrimasPlan)localStateManager.getObjectField(paramBeneficiario, jdoInheritedFieldCount + 13, paramBeneficiario.primasPlan);
  }

  private static final void jdoSetprimasPlan(Beneficiario paramBeneficiario, PrimasPlan paramPrimasPlan)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.primasPlan = paramPrimasPlan;
      return;
    }
    localStateManager.setObjectField(paramBeneficiario, jdoInheritedFieldCount + 13, paramBeneficiario.primasPlan, paramPrimasPlan);
  }

  private static final TipoPersonal jdoGettipoPersonal(Beneficiario paramBeneficiario)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
      return paramBeneficiario.tipoPersonal;
    if (localStateManager.isLoaded(paramBeneficiario, jdoInheritedFieldCount + 14))
      return paramBeneficiario.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramBeneficiario, jdoInheritedFieldCount + 14, paramBeneficiario.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(Beneficiario paramBeneficiario, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramBeneficiario.jdoStateManager;
    if (localStateManager == null)
    {
      paramBeneficiario.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramBeneficiario, jdoInheritedFieldCount + 14, paramBeneficiario.tipoPersonal, paramTipoPersonal);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}