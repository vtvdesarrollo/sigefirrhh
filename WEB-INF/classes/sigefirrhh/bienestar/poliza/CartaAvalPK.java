package sigefirrhh.bienestar.poliza;

import java.io.Serializable;

public class CartaAvalPK
  implements Serializable
{
  public long idCartaAval;

  public CartaAvalPK()
  {
  }

  public CartaAvalPK(long idCartaAval)
  {
    this.idCartaAval = idCartaAval;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((CartaAvalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(CartaAvalPK thatPK)
  {
    return 
      this.idCartaAval == thatPK.idCartaAval;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idCartaAval)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idCartaAval);
  }
}