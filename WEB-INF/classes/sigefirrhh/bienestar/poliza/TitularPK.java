package sigefirrhh.bienestar.poliza;

import java.io.Serializable;

public class TitularPK
  implements Serializable
{
  public long idTitular;

  public TitularPK()
  {
  }

  public TitularPK(long idTitular)
  {
    this.idTitular = idTitular;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((TitularPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(TitularPK thatPK)
  {
    return 
      this.idTitular == thatPK.idTitular;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idTitular)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idTitular);
  }
}