package sigefirrhh.bienestar.poliza;

import java.io.Serializable;

public class ExcepcionTitularPK
  implements Serializable
{
  public long idExcepcionTitular;

  public ExcepcionTitularPK()
  {
  }

  public ExcepcionTitularPK(long idExcepcionTitular)
  {
    this.idExcepcionTitular = idExcepcionTitular;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ExcepcionTitularPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ExcepcionTitularPK thatPK)
  {
    return 
      this.idExcepcionTitular == thatPK.idExcepcionTitular;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idExcepcionTitular)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idExcepcionTitular);
  }
}