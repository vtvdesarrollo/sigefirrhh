package sigefirrhh.bienestar.poliza;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class CartaAval
  implements Serializable, PersistenceCapable
{
  private long idCartaAval;
  private Date fechaSiniestro;
  private Date fechaReporte;
  private Date fechaRecepcion;
  private String estatus;
  private double montoReclamo;
  private double montoPagado;
  private Date fechaCierre;
  private String observaciones;
  private EstablecimientoSalud establecimientoSalud;
  private Personal personal;
  private Familiar familiar;
  private PlanPoliza planPoliza;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "establecimientoSalud", "estatus", "familiar", "fechaCierre", "fechaRecepcion", "fechaReporte", "fechaSiniestro", "idCartaAval", "montoPagado", "montoReclamo", "observaciones", "personal", "planPoliza" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.bienestar.EstablecimientoSalud"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), sunjdo$classForName$("sigefirrhh.base.bienestar.PlanPoliza") };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 21, 21, 21, 21, 24, 21, 21, 21, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public EstablecimientoSalud getEstablecimientoSalud()
  {
    return jdoGetestablecimientoSalud(this);
  }

  public String getEstatus()
  {
    return jdoGetestatus(this);
  }

  public Familiar getFamiliar()
  {
    return jdoGetfamiliar(this);
  }

  public Date getFechaCierre()
  {
    return jdoGetfechaCierre(this);
  }

  public Date getFechaRecepcion()
  {
    return jdoGetfechaRecepcion(this);
  }

  public Date getFechaReporte()
  {
    return jdoGetfechaReporte(this);
  }

  public Date getFechaSiniestro()
  {
    return jdoGetfechaSiniestro(this);
  }

  public long getIdCartaAval()
  {
    return jdoGetidCartaAval(this);
  }

  public double getMontoPagado()
  {
    return jdoGetmontoPagado(this);
  }

  public double getMontoReclamo()
  {
    return jdoGetmontoReclamo(this);
  }

  public String getObservaciones()
  {
    return jdoGetobservaciones(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public PlanPoliza getPlanPoliza()
  {
    return jdoGetplanPoliza(this);
  }

  public void setEstablecimientoSalud(EstablecimientoSalud salud)
  {
    jdoSetestablecimientoSalud(this, salud);
  }

  public void setEstatus(String string)
  {
    jdoSetestatus(this, string);
  }

  public void setFamiliar(Familiar familiar)
  {
    jdoSetfamiliar(this, familiar);
  }

  public void setFechaCierre(Date date)
  {
    jdoSetfechaCierre(this, date);
  }

  public void setFechaRecepcion(Date date)
  {
    jdoSetfechaRecepcion(this, date);
  }

  public void setFechaReporte(Date date)
  {
    jdoSetfechaReporte(this, date);
  }

  public void setFechaSiniestro(Date date)
  {
    jdoSetfechaSiniestro(this, date);
  }

  public void setIdCartaAval(long l)
  {
    jdoSetidCartaAval(this, l);
  }

  public void setMontoPagado(double d)
  {
    jdoSetmontoPagado(this, d);
  }

  public void setMontoReclamo(double d)
  {
    jdoSetmontoReclamo(this, d);
  }

  public void setObservaciones(String string)
  {
    jdoSetobservaciones(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPlanPoliza(PlanPoliza poliza)
  {
    jdoSetplanPoliza(this, poliza);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 13;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.poliza.CartaAval"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new CartaAval());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    CartaAval localCartaAval = new CartaAval();
    localCartaAval.jdoFlags = 1;
    localCartaAval.jdoStateManager = paramStateManager;
    return localCartaAval;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    CartaAval localCartaAval = new CartaAval();
    localCartaAval.jdoCopyKeyFieldsFromObjectId(paramObject);
    localCartaAval.jdoFlags = 1;
    localCartaAval.jdoStateManager = paramStateManager;
    return localCartaAval;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.establecimientoSalud);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatus);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaCierre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaRecepcion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaReporte);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaSiniestro);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idCartaAval);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoPagado);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoReclamo);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.planPoliza);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.establecimientoSalud = ((EstablecimientoSalud)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatus = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaCierre = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaRecepcion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaReporte = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaSiniestro = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idCartaAval = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoPagado = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoReclamo = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.planPoliza = ((PlanPoliza)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(CartaAval paramCartaAval, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.establecimientoSalud = paramCartaAval.establecimientoSalud;
      return;
    case 1:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.estatus = paramCartaAval.estatus;
      return;
    case 2:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramCartaAval.familiar;
      return;
    case 3:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.fechaCierre = paramCartaAval.fechaCierre;
      return;
    case 4:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.fechaRecepcion = paramCartaAval.fechaRecepcion;
      return;
    case 5:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.fechaReporte = paramCartaAval.fechaReporte;
      return;
    case 6:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.fechaSiniestro = paramCartaAval.fechaSiniestro;
      return;
    case 7:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.idCartaAval = paramCartaAval.idCartaAval;
      return;
    case 8:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.montoPagado = paramCartaAval.montoPagado;
      return;
    case 9:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.montoReclamo = paramCartaAval.montoReclamo;
      return;
    case 10:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramCartaAval.observaciones;
      return;
    case 11:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramCartaAval.personal;
      return;
    case 12:
      if (paramCartaAval == null)
        throw new IllegalArgumentException("arg1");
      this.planPoliza = paramCartaAval.planPoliza;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof CartaAval))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    CartaAval localCartaAval = (CartaAval)paramObject;
    if (localCartaAval.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localCartaAval, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new CartaAvalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new CartaAvalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CartaAvalPK))
      throw new IllegalArgumentException("arg1");
    CartaAvalPK localCartaAvalPK = (CartaAvalPK)paramObject;
    localCartaAvalPK.idCartaAval = this.idCartaAval;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof CartaAvalPK))
      throw new IllegalArgumentException("arg1");
    CartaAvalPK localCartaAvalPK = (CartaAvalPK)paramObject;
    this.idCartaAval = localCartaAvalPK.idCartaAval;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CartaAvalPK))
      throw new IllegalArgumentException("arg2");
    CartaAvalPK localCartaAvalPK = (CartaAvalPK)paramObject;
    localCartaAvalPK.idCartaAval = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof CartaAvalPK))
      throw new IllegalArgumentException("arg2");
    CartaAvalPK localCartaAvalPK = (CartaAvalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localCartaAvalPK.idCartaAval);
  }

  private static final EstablecimientoSalud jdoGetestablecimientoSalud(CartaAval paramCartaAval)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.establecimientoSalud;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 0))
      return paramCartaAval.establecimientoSalud;
    return (EstablecimientoSalud)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 0, paramCartaAval.establecimientoSalud);
  }

  private static final void jdoSetestablecimientoSalud(CartaAval paramCartaAval, EstablecimientoSalud paramEstablecimientoSalud)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.establecimientoSalud = paramEstablecimientoSalud;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 0, paramCartaAval.establecimientoSalud, paramEstablecimientoSalud);
  }

  private static final String jdoGetestatus(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.estatus;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.estatus;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 1))
      return paramCartaAval.estatus;
    return localStateManager.getStringField(paramCartaAval, jdoInheritedFieldCount + 1, paramCartaAval.estatus);
  }

  private static final void jdoSetestatus(CartaAval paramCartaAval, String paramString)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.estatus = paramString;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.estatus = paramString;
      return;
    }
    localStateManager.setStringField(paramCartaAval, jdoInheritedFieldCount + 1, paramCartaAval.estatus, paramString);
  }

  private static final Familiar jdoGetfamiliar(CartaAval paramCartaAval)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.familiar;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 2))
      return paramCartaAval.familiar;
    return (Familiar)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 2, paramCartaAval.familiar);
  }

  private static final void jdoSetfamiliar(CartaAval paramCartaAval, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 2, paramCartaAval.familiar, paramFamiliar);
  }

  private static final Date jdoGetfechaCierre(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.fechaCierre;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.fechaCierre;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 3))
      return paramCartaAval.fechaCierre;
    return (Date)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 3, paramCartaAval.fechaCierre);
  }

  private static final void jdoSetfechaCierre(CartaAval paramCartaAval, Date paramDate)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.fechaCierre = paramDate;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.fechaCierre = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 3, paramCartaAval.fechaCierre, paramDate);
  }

  private static final Date jdoGetfechaRecepcion(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.fechaRecepcion;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.fechaRecepcion;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 4))
      return paramCartaAval.fechaRecepcion;
    return (Date)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 4, paramCartaAval.fechaRecepcion);
  }

  private static final void jdoSetfechaRecepcion(CartaAval paramCartaAval, Date paramDate)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.fechaRecepcion = paramDate;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.fechaRecepcion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 4, paramCartaAval.fechaRecepcion, paramDate);
  }

  private static final Date jdoGetfechaReporte(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.fechaReporte;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.fechaReporte;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 5))
      return paramCartaAval.fechaReporte;
    return (Date)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 5, paramCartaAval.fechaReporte);
  }

  private static final void jdoSetfechaReporte(CartaAval paramCartaAval, Date paramDate)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.fechaReporte = paramDate;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.fechaReporte = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 5, paramCartaAval.fechaReporte, paramDate);
  }

  private static final Date jdoGetfechaSiniestro(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.fechaSiniestro;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.fechaSiniestro;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 6))
      return paramCartaAval.fechaSiniestro;
    return (Date)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 6, paramCartaAval.fechaSiniestro);
  }

  private static final void jdoSetfechaSiniestro(CartaAval paramCartaAval, Date paramDate)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.fechaSiniestro = paramDate;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.fechaSiniestro = paramDate;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 6, paramCartaAval.fechaSiniestro, paramDate);
  }

  private static final long jdoGetidCartaAval(CartaAval paramCartaAval)
  {
    return paramCartaAval.idCartaAval;
  }

  private static final void jdoSetidCartaAval(CartaAval paramCartaAval, long paramLong)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.idCartaAval = paramLong;
      return;
    }
    localStateManager.setLongField(paramCartaAval, jdoInheritedFieldCount + 7, paramCartaAval.idCartaAval, paramLong);
  }

  private static final double jdoGetmontoPagado(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.montoPagado;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.montoPagado;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 8))
      return paramCartaAval.montoPagado;
    return localStateManager.getDoubleField(paramCartaAval, jdoInheritedFieldCount + 8, paramCartaAval.montoPagado);
  }

  private static final void jdoSetmontoPagado(CartaAval paramCartaAval, double paramDouble)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.montoPagado = paramDouble;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.montoPagado = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCartaAval, jdoInheritedFieldCount + 8, paramCartaAval.montoPagado, paramDouble);
  }

  private static final double jdoGetmontoReclamo(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.montoReclamo;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.montoReclamo;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 9))
      return paramCartaAval.montoReclamo;
    return localStateManager.getDoubleField(paramCartaAval, jdoInheritedFieldCount + 9, paramCartaAval.montoReclamo);
  }

  private static final void jdoSetmontoReclamo(CartaAval paramCartaAval, double paramDouble)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.montoReclamo = paramDouble;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.montoReclamo = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramCartaAval, jdoInheritedFieldCount + 9, paramCartaAval.montoReclamo, paramDouble);
  }

  private static final String jdoGetobservaciones(CartaAval paramCartaAval)
  {
    if (paramCartaAval.jdoFlags <= 0)
      return paramCartaAval.observaciones;
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.observaciones;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 10))
      return paramCartaAval.observaciones;
    return localStateManager.getStringField(paramCartaAval, jdoInheritedFieldCount + 10, paramCartaAval.observaciones);
  }

  private static final void jdoSetobservaciones(CartaAval paramCartaAval, String paramString)
  {
    if (paramCartaAval.jdoFlags == 0)
    {
      paramCartaAval.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramCartaAval, jdoInheritedFieldCount + 10, paramCartaAval.observaciones, paramString);
  }

  private static final Personal jdoGetpersonal(CartaAval paramCartaAval)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.personal;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 11))
      return paramCartaAval.personal;
    return (Personal)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 11, paramCartaAval.personal);
  }

  private static final void jdoSetpersonal(CartaAval paramCartaAval, Personal paramPersonal)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 11, paramCartaAval.personal, paramPersonal);
  }

  private static final PlanPoliza jdoGetplanPoliza(CartaAval paramCartaAval)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
      return paramCartaAval.planPoliza;
    if (localStateManager.isLoaded(paramCartaAval, jdoInheritedFieldCount + 12))
      return paramCartaAval.planPoliza;
    return (PlanPoliza)localStateManager.getObjectField(paramCartaAval, jdoInheritedFieldCount + 12, paramCartaAval.planPoliza);
  }

  private static final void jdoSetplanPoliza(CartaAval paramCartaAval, PlanPoliza paramPlanPoliza)
  {
    StateManager localStateManager = paramCartaAval.jdoStateManager;
    if (localStateManager == null)
    {
      paramCartaAval.planPoliza = paramPlanPoliza;
      return;
    }
    localStateManager.setObjectField(paramCartaAval, jdoInheritedFieldCount + 12, paramCartaAval.planPoliza, paramPlanPoliza);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}