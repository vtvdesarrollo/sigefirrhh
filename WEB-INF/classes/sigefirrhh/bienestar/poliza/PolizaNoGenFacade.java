package sigefirrhh.bienestar.poliza;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.Collection;
import java.util.Date;

public class PolizaNoGenFacade extends PolizaFacade
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private PolizaNoGenBusiness polizaNoGenBusiness = new PolizaNoGenBusiness();

  public void generarPolizas(long idTipoPersonal, long idPrimasPlan, Date fecha, String proceso) throws Exception {
    this.polizaNoGenBusiness.generarPolizas(idTipoPersonal, idPrimasPlan, fecha, proceso);
  }

  public void suspenderPrimas(long idTipoPersonal, long idPrimasPlan, String proceso) throws Exception {
    this.polizaNoGenBusiness.suspenderPrimas(idTipoPersonal, idPrimasPlan, proceso);
  }

  public void generarPrestamoPoliza(long idTipoPersonal, long idPlanPoliza, String tipo, long idConceptoTipoPersonal, long idFrecuenciaTipoPersonal, int numeroCuotas) throws Exception {
    this.polizaNoGenBusiness.generarPrestamoPoliza(idTipoPersonal, idPlanPoliza, tipo, 
      idConceptoTipoPersonal, idFrecuenciaTipoPersonal, numeroCuotas);
  }

  public Collection findBeneficiarioByPersonalAndPlanPoliza(long idPersonal, long idPlanPoliza) throws Exception {
    try {
      this.txn.open();
      return this.polizaNoGenBusiness.findBeneficiarioByPersonalAndPlanPoliza(idPersonal, idPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findTitularByPersonalAndPlanPoliza(long idPersonal, long idPlanPoliza) throws Exception
  {
    try { this.txn.open();
      return this.polizaNoGenBusiness.findTitularByPersonalAndPlanPoliza(idPersonal, idPlanPoliza);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}