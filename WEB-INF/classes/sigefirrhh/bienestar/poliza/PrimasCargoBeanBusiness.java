package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.bienestar.PrimasPlanBeanBusiness;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;

public class PrimasCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addPrimasCargo(PrimasCargo primasCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    PrimasCargo primasCargoNew = 
      (PrimasCargo)BeanUtils.cloneBean(
      primasCargo);

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (primasCargoNew.getCargo() != null) {
      primasCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        primasCargoNew.getCargo().getIdCargo()));
    }

    PrimasPlanBeanBusiness primasPlanBeanBusiness = new PrimasPlanBeanBusiness();

    if (primasCargoNew.getPrimasPlan() != null) {
      primasCargoNew.setPrimasPlan(
        primasPlanBeanBusiness.findPrimasPlanById(
        primasCargoNew.getPrimasPlan().getIdPrimasPlan()));
    }
    pm.makePersistent(primasCargoNew);
  }

  public void updatePrimasCargo(PrimasCargo primasCargo) throws Exception
  {
    PrimasCargo primasCargoModify = 
      findPrimasCargoById(primasCargo.getIdPrimasCargo());

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (primasCargo.getCargo() != null) {
      primasCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        primasCargo.getCargo().getIdCargo()));
    }

    PrimasPlanBeanBusiness primasPlanBeanBusiness = new PrimasPlanBeanBusiness();

    if (primasCargo.getPrimasPlan() != null) {
      primasCargo.setPrimasPlan(
        primasPlanBeanBusiness.findPrimasPlanById(
        primasCargo.getPrimasPlan().getIdPrimasPlan()));
    }

    BeanUtils.copyProperties(primasCargoModify, primasCargo);
  }

  public void deletePrimasCargo(PrimasCargo primasCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    PrimasCargo primasCargoDelete = 
      findPrimasCargoById(primasCargo.getIdPrimasCargo());
    pm.deletePersistent(primasCargoDelete);
  }

  public PrimasCargo findPrimasCargoById(long idPrimasCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idPrimasCargo == pIdPrimasCargo";
    Query query = pm.newQuery(PrimasCargo.class, filter);

    query.declareParameters("long pIdPrimasCargo");

    parameters.put("pIdPrimasCargo", new Long(idPrimasCargo));

    Collection colPrimasCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colPrimasCargo.iterator();
    return (PrimasCargo)iterator.next();
  }

  public Collection findPrimasCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent primasCargoExtent = pm.getExtent(
      PrimasCargo.class, true);
    Query query = pm.newQuery(primasCargoExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPrimasPlan(long idPrimasPlan)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primasPlan.idPrimasPlan == pIdPrimasPlan";

    Query query = pm.newQuery(PrimasCargo.class, filter);

    query.declareParameters("long pIdPrimasPlan");
    HashMap parameters = new HashMap();

    parameters.put("pIdPrimasPlan", new Long(idPrimasPlan));

    Collection colPrimasCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colPrimasCargo);

    return colPrimasCargo;
  }
}