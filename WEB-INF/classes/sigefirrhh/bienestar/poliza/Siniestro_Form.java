package sigefirrhh.bienestar.poliza;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.EstablecimientoSalud;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.Poliza;
import sigefirrhh.base.bienestar.TipoSiniestro;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Personal;

public class Siniestro_Form extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(Siniestro_Form.class.getName());
  private Siniestro siniestro;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private PolizaFacade polizaFacade = new PolizaFacade();
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colTipoSiniestro;
  private Collection colPolizaForPlanPoliza;
  private Collection colPlanPoliza;
  private Collection colBeneficiario;
  private Collection colTitular;
  private Collection colPersonal;
  private Collection colEstablecimientoSalud;
  private String selectTipoSiniestro;
  private String selectPolizaForPlanPoliza;
  private String selectPlanPoliza;
  private String selectBeneficiario;
  private String selectTitular;
  private String selectPersonal;
  private String selectEstablecimientoSalud;
  private Object stateResultPersonal = null;

  private Object stateResultSiniestroByPersonal = null;

  public String getSelectTipoSiniestro()
  {
    return this.selectTipoSiniestro;
  }
  public void setSelectTipoSiniestro(String valTipoSiniestro) {
    Iterator iterator = this.colTipoSiniestro.iterator();
    TipoSiniestro tipoSiniestro = null;
    this.siniestro.setTipoSiniestro(null);
    while (iterator.hasNext()) {
      tipoSiniestro = (TipoSiniestro)iterator.next();
      if (String.valueOf(tipoSiniestro.getIdTipoSiniestro()).equals(
        valTipoSiniestro)) {
        this.siniestro.setTipoSiniestro(
          tipoSiniestro);
        break;
      }
    }
    this.selectTipoSiniestro = valTipoSiniestro;
  }
  public String getSelectPolizaForPlanPoliza() {
    return this.selectPolizaForPlanPoliza;
  }
  public void setSelectPolizaForPlanPoliza(String valPolizaForPlanPoliza) {
    this.selectPolizaForPlanPoliza = valPolizaForPlanPoliza;
  }
  public void changePolizaForPlanPoliza(ValueChangeEvent event) {
    long idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPlanPoliza = null;
      if (idPoliza > 0L)
        this.colPlanPoliza = 
          this.bienestarFacade.findPlanPolizaByPoliza(
          idPoliza);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowPolizaForPlanPoliza() { return this.colPolizaForPlanPoliza != null; }

  public String getSelectPlanPoliza() {
    return this.selectPlanPoliza;
  }
  public void setSelectPlanPoliza(String valPlanPoliza) {
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    this.siniestro.setPlanPoliza(null);
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      if (String.valueOf(planPoliza.getIdPlanPoliza()).equals(
        valPlanPoliza)) {
        this.siniestro.setPlanPoliza(
          planPoliza);
        break;
      }
    }
    this.selectPlanPoliza = valPlanPoliza;
  }
  public boolean isShowPlanPoliza() {
    return this.colPlanPoliza != null;
  }
  public String getSelectBeneficiario() {
    return this.selectBeneficiario;
  }
  public void setSelectBeneficiario(String valBeneficiario) {
    Iterator iterator = this.colBeneficiario.iterator();
    Beneficiario beneficiario = null;
    this.siniestro.setBeneficiario(null);
    while (iterator.hasNext()) {
      beneficiario = (Beneficiario)iterator.next();
      if (String.valueOf(beneficiario.getIdBeneficiario()).equals(
        valBeneficiario)) {
        this.siniestro.setBeneficiario(
          beneficiario);
        break;
      }
    }
    this.selectBeneficiario = valBeneficiario;
  }
  public String getSelectTitular() {
    return this.selectTitular;
  }
  public void setSelectTitular(String valTitular) {
    Iterator iterator = this.colTitular.iterator();
    Titular titular = null;
    this.siniestro.setTitular(null);
    while (iterator.hasNext()) {
      titular = (Titular)iterator.next();
      if (String.valueOf(titular.getIdTitular()).equals(
        valTitular)) {
        this.siniestro.setTitular(
          titular);
        break;
      }
    }
    this.selectTitular = valTitular;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.siniestro.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.siniestro.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public String getSelectEstablecimientoSalud() {
    return this.selectEstablecimientoSalud;
  }
  public void setSelectEstablecimientoSalud(String valEstablecimientoSalud) {
    Iterator iterator = this.colEstablecimientoSalud.iterator();
    EstablecimientoSalud establecimientoSalud = null;
    this.siniestro.setEstablecimientoSalud(null);
    while (iterator.hasNext()) {
      establecimientoSalud = (EstablecimientoSalud)iterator.next();
      if (String.valueOf(establecimientoSalud.getIdEstablecimientoSalud()).equals(
        valEstablecimientoSalud)) {
        this.siniestro.setEstablecimientoSalud(
          establecimientoSalud);
        break;
      }
    }
    this.selectEstablecimientoSalud = valEstablecimientoSalud;
  }
  public Collection getResult() {
    return this.result;
  }

  public Siniestro getSiniestro() {
    if (this.siniestro == null) {
      this.siniestro = new Siniestro();
    }
    return this.siniestro;
  }

  public Siniestro_Form()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColTipoSiniestro()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoSiniestro.iterator();
    TipoSiniestro tipoSiniestro = null;
    while (iterator.hasNext()) {
      tipoSiniestro = (TipoSiniestro)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoSiniestro.getIdTipoSiniestro()), 
        tipoSiniestro.toString()));
    }
    return col;
  }

  public Collection getListEstatus()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Siniestro.LISTA_ESTATUS.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColPolizaForPlanPoliza()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPolizaForPlanPoliza.iterator();
    Poliza polizaForPlanPoliza = null;
    while (iterator.hasNext()) {
      polizaForPlanPoliza = (Poliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(polizaForPlanPoliza.getIdPoliza()), 
        polizaForPlanPoliza.toString()));
    }
    return col;
  }

  public Collection getColPlanPoliza()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPoliza.getIdPlanPoliza()), 
        planPoliza.toString()));
    }
    return col;
  }

  public Collection getColBeneficiario()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colBeneficiario.iterator();
    Beneficiario beneficiario = null;
    while (iterator.hasNext()) {
      beneficiario = (Beneficiario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(beneficiario.getIdBeneficiario()), 
        beneficiario.toString()));
    }
    return col;
  }

  public Collection getColTitular()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTitular.iterator();
    Titular titular = null;
    while (iterator.hasNext()) {
      titular = (Titular)iterator.next();
      col.add(new SelectItem(
        String.valueOf(titular.getIdTitular()), 
        titular.toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public Collection getColEstablecimientoSalud()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colEstablecimientoSalud.iterator();
    EstablecimientoSalud establecimientoSalud = null;
    while (iterator.hasNext()) {
      establecimientoSalud = (EstablecimientoSalud)iterator.next();
      col.add(new SelectItem(
        String.valueOf(establecimientoSalud.getIdEstablecimientoSalud()), 
        establecimientoSalud.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colTipoSiniestro = 
        this.bienestarFacade.findAllTipoSiniestro();
      this.colPolizaForPlanPoliza = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
      this.colBeneficiario = 
        this.polizaFacade.findAllBeneficiario();
      this.colTitular = 
        this.polizaFacade.findAllTitular();
      this.colEstablecimientoSalud = 
        this.bienestarFacade.findAllEstablecimientoSalud();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findSiniestroByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.polizaFacade.findSiniestroByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectSiniestro()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoSiniestro = null;
    this.selectPlanPoliza = null;
    this.selectPolizaForPlanPoliza = null;

    this.selectBeneficiario = null;
    this.selectTitular = null;
    this.selectPersonal = null;
    this.selectEstablecimientoSalud = null;

    long idSiniestro = 
      Long.parseLong((String)requestParameterMap.get("idSiniestro"));
    try
    {
      this.siniestro = 
        this.polizaFacade.findSiniestroById(
        idSiniestro);

      if (this.siniestro.getTipoSiniestro() != null) {
        this.selectTipoSiniestro = 
          String.valueOf(this.siniestro.getTipoSiniestro().getIdTipoSiniestro());
      }
      if (this.siniestro.getPlanPoliza() != null) {
        this.selectPlanPoliza = 
          String.valueOf(this.siniestro.getPlanPoliza().getIdPlanPoliza());
      }
      if (this.siniestro.getBeneficiario() != null) {
        this.selectBeneficiario = 
          String.valueOf(this.siniestro.getBeneficiario().getIdBeneficiario());
      }
      if (this.siniestro.getTitular() != null) {
        this.selectTitular = 
          String.valueOf(this.siniestro.getTitular().getIdTitular());
      }
      if (this.siniestro.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.siniestro.getPersonal().getIdPersonal());
      }
      if (this.siniestro.getEstablecimientoSalud() != null) {
        this.selectEstablecimientoSalud = 
          String.valueOf(this.siniestro.getEstablecimientoSalud().getIdEstablecimientoSalud());
      }

      PlanPoliza planPoliza = null;
      Poliza polizaForPlanPoliza = null;

      if (this.siniestro.getPlanPoliza() != null) {
        long idPlanPoliza = 
          this.siniestro.getPlanPoliza().getIdPlanPoliza();
        this.selectPlanPoliza = String.valueOf(idPlanPoliza);
        planPoliza = this.bienestarFacade.findPlanPolizaById(
          idPlanPoliza);
        this.colPlanPoliza = this.bienestarFacade.findPlanPolizaByPoliza(
          planPoliza.getPoliza().getIdPoliza());

        long idPolizaForPlanPoliza = 
          this.siniestro.getPlanPoliza().getPoliza().getIdPoliza();
        this.selectPolizaForPlanPoliza = String.valueOf(idPolizaForPlanPoliza);
        polizaForPlanPoliza = 
          this.bienestarFacade.findPolizaById(
          idPolizaForPlanPoliza);
        this.colPolizaForPlanPoliza = 
          this.bienestarFacade.findAllPoliza();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.siniestro.getFechaSiniestro() != null) && 
      (this.siniestro.getFechaSiniestro().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Siniestro no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.siniestro.getFechaReporte() != null) && 
      (this.siniestro.getFechaReporte().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Reporte no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.siniestro.getFechaRecepcion() != null) && 
      (this.siniestro.getFechaRecepcion().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Recepción Recaudo no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if ((this.siniestro.getFechaCierre() != null) && 
      (this.siniestro.getFechaCierre().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Cierre no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.siniestro.setPersonal(
          this.personal);
        this.polizaFacade.addSiniestro(
          this.siniestro);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.polizaFacade.updateSiniestro(
          this.siniestro);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.polizaFacade.deleteSiniestro(
        this.siniestro);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectTipoSiniestro = null;

    this.selectPlanPoliza = null;

    this.selectPolizaForPlanPoliza = null;

    this.selectBeneficiario = null;

    this.selectTitular = null;

    this.selectPersonal = null;

    this.selectEstablecimientoSalud = null;

    this.siniestro = new Siniestro();

    this.siniestro.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.siniestro.setIdSiniestro(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.poliza.Siniestro"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.siniestro = new Siniestro();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.siniestro = new Siniestro();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }
}