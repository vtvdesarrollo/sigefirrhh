package sigefirrhh.bienestar.poliza;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.PrimasPlan;
import sigefirrhh.base.cargo.Cargo;

public class PrimasCargo
  implements Serializable, PersistenceCapable
{
  private long idPrimasCargo;
  private Cargo cargo;
  private PrimasPlan primasPlan;
  private double porcentajePatron;
  private double porcentajeTrabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cargo", "idPrimasCargo", "porcentajePatron", "porcentajeTrabajador", "primasPlan" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Long.TYPE, Double.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.PrimasPlan") };
  private static final byte[] jdoFieldFlags = { 26, 24, 21, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public PrimasCargo()
  {
    jdoSetporcentajePatron(this, 0.0D);

    jdoSetporcentajeTrabajador(this, 0.0D);
  }

  public String toString() {
    return jdoGetprimasPlan(this) + " - " + 
      jdoGetcargo(this);
  }

  public Cargo getCargo()
  {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo)
  {
    jdoSetcargo(this, cargo);
  }

  public long getIdPrimasCargo()
  {
    return jdoGetidPrimasCargo(this);
  }

  public void setIdPrimasCargo(long idPrimasCargo)
  {
    jdoSetidPrimasCargo(this, idPrimasCargo);
  }

  public double getPorcentajePatron()
  {
    return jdoGetporcentajePatron(this);
  }

  public void setPorcentajePatron(double porcentajePatron)
  {
    jdoSetporcentajePatron(this, porcentajePatron);
  }

  public double getPorcentajeTrabajador()
  {
    return jdoGetporcentajeTrabajador(this);
  }

  public void setPorcentajeTrabajador(double porcentajeTrabajador)
  {
    jdoSetporcentajeTrabajador(this, porcentajeTrabajador);
  }

  public PrimasPlan getPrimasPlan()
  {
    return jdoGetprimasPlan(this);
  }

  public void setPrimasPlan(PrimasPlan primasPlan)
  {
    jdoSetprimasPlan(this, primasPlan);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.poliza.PrimasCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new PrimasCargo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    PrimasCargo localPrimasCargo = new PrimasCargo();
    localPrimasCargo.jdoFlags = 1;
    localPrimasCargo.jdoStateManager = paramStateManager;
    return localPrimasCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    PrimasCargo localPrimasCargo = new PrimasCargo();
    localPrimasCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localPrimasCargo.jdoFlags = 1;
    localPrimasCargo.jdoStateManager = paramStateManager;
    return localPrimasCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idPrimasCargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajePatron);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.porcentajeTrabajador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.primasPlan);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idPrimasCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajePatron = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.porcentajeTrabajador = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primasPlan = ((PrimasPlan)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(PrimasCargo paramPrimasCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramPrimasCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramPrimasCargo.cargo;
      return;
    case 1:
      if (paramPrimasCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idPrimasCargo = paramPrimasCargo.idPrimasCargo;
      return;
    case 2:
      if (paramPrimasCargo == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajePatron = paramPrimasCargo.porcentajePatron;
      return;
    case 3:
      if (paramPrimasCargo == null)
        throw new IllegalArgumentException("arg1");
      this.porcentajeTrabajador = paramPrimasCargo.porcentajeTrabajador;
      return;
    case 4:
      if (paramPrimasCargo == null)
        throw new IllegalArgumentException("arg1");
      this.primasPlan = paramPrimasCargo.primasPlan;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof PrimasCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    PrimasCargo localPrimasCargo = (PrimasCargo)paramObject;
    if (localPrimasCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localPrimasCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new PrimasCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new PrimasCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimasCargoPK))
      throw new IllegalArgumentException("arg1");
    PrimasCargoPK localPrimasCargoPK = (PrimasCargoPK)paramObject;
    localPrimasCargoPK.idPrimasCargo = this.idPrimasCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof PrimasCargoPK))
      throw new IllegalArgumentException("arg1");
    PrimasCargoPK localPrimasCargoPK = (PrimasCargoPK)paramObject;
    this.idPrimasCargo = localPrimasCargoPK.idPrimasCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimasCargoPK))
      throw new IllegalArgumentException("arg2");
    PrimasCargoPK localPrimasCargoPK = (PrimasCargoPK)paramObject;
    localPrimasCargoPK.idPrimasCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof PrimasCargoPK))
      throw new IllegalArgumentException("arg2");
    PrimasCargoPK localPrimasCargoPK = (PrimasCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localPrimasCargoPK.idPrimasCargo);
  }

  private static final Cargo jdoGetcargo(PrimasCargo paramPrimasCargo)
  {
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasCargo.cargo;
    if (localStateManager.isLoaded(paramPrimasCargo, jdoInheritedFieldCount + 0))
      return paramPrimasCargo.cargo;
    return (Cargo)localStateManager.getObjectField(paramPrimasCargo, jdoInheritedFieldCount + 0, paramPrimasCargo.cargo);
  }

  private static final void jdoSetcargo(PrimasCargo paramPrimasCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasCargo.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramPrimasCargo, jdoInheritedFieldCount + 0, paramPrimasCargo.cargo, paramCargo);
  }

  private static final long jdoGetidPrimasCargo(PrimasCargo paramPrimasCargo)
  {
    return paramPrimasCargo.idPrimasCargo;
  }

  private static final void jdoSetidPrimasCargo(PrimasCargo paramPrimasCargo, long paramLong)
  {
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasCargo.idPrimasCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramPrimasCargo, jdoInheritedFieldCount + 1, paramPrimasCargo.idPrimasCargo, paramLong);
  }

  private static final double jdoGetporcentajePatron(PrimasCargo paramPrimasCargo)
  {
    if (paramPrimasCargo.jdoFlags <= 0)
      return paramPrimasCargo.porcentajePatron;
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasCargo.porcentajePatron;
    if (localStateManager.isLoaded(paramPrimasCargo, jdoInheritedFieldCount + 2))
      return paramPrimasCargo.porcentajePatron;
    return localStateManager.getDoubleField(paramPrimasCargo, jdoInheritedFieldCount + 2, paramPrimasCargo.porcentajePatron);
  }

  private static final void jdoSetporcentajePatron(PrimasCargo paramPrimasCargo, double paramDouble)
  {
    if (paramPrimasCargo.jdoFlags == 0)
    {
      paramPrimasCargo.porcentajePatron = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasCargo.porcentajePatron = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimasCargo, jdoInheritedFieldCount + 2, paramPrimasCargo.porcentajePatron, paramDouble);
  }

  private static final double jdoGetporcentajeTrabajador(PrimasCargo paramPrimasCargo)
  {
    if (paramPrimasCargo.jdoFlags <= 0)
      return paramPrimasCargo.porcentajeTrabajador;
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasCargo.porcentajeTrabajador;
    if (localStateManager.isLoaded(paramPrimasCargo, jdoInheritedFieldCount + 3))
      return paramPrimasCargo.porcentajeTrabajador;
    return localStateManager.getDoubleField(paramPrimasCargo, jdoInheritedFieldCount + 3, paramPrimasCargo.porcentajeTrabajador);
  }

  private static final void jdoSetporcentajeTrabajador(PrimasCargo paramPrimasCargo, double paramDouble)
  {
    if (paramPrimasCargo.jdoFlags == 0)
    {
      paramPrimasCargo.porcentajeTrabajador = paramDouble;
      return;
    }
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasCargo.porcentajeTrabajador = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramPrimasCargo, jdoInheritedFieldCount + 3, paramPrimasCargo.porcentajeTrabajador, paramDouble);
  }

  private static final PrimasPlan jdoGetprimasPlan(PrimasCargo paramPrimasCargo)
  {
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
      return paramPrimasCargo.primasPlan;
    if (localStateManager.isLoaded(paramPrimasCargo, jdoInheritedFieldCount + 4))
      return paramPrimasCargo.primasPlan;
    return (PrimasPlan)localStateManager.getObjectField(paramPrimasCargo, jdoInheritedFieldCount + 4, paramPrimasCargo.primasPlan);
  }

  private static final void jdoSetprimasPlan(PrimasCargo paramPrimasCargo, PrimasPlan paramPrimasPlan)
  {
    StateManager localStateManager = paramPrimasCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramPrimasCargo.primasPlan = paramPrimasPlan;
      return;
    }
    localStateManager.setObjectField(paramPrimasCargo, jdoInheritedFieldCount + 4, paramPrimasCargo.primasPlan, paramPrimasPlan);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}