package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.PlanPolizaBeanBusiness;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.FamiliarBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class ExcepcionBeneficiarioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ExcepcionBeneficiario excepcionBeneficiarioNew = 
      (ExcepcionBeneficiario)BeanUtils.cloneBean(
      excepcionBeneficiario);

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (excepcionBeneficiarioNew.getFamiliar() != null) {
      excepcionBeneficiarioNew.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        excepcionBeneficiarioNew.getFamiliar().getIdFamiliar()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (excepcionBeneficiarioNew.getPersonal() != null) {
      excepcionBeneficiarioNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        excepcionBeneficiarioNew.getPersonal().getIdPersonal()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (excepcionBeneficiarioNew.getPlanPoliza() != null) {
      excepcionBeneficiarioNew.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        excepcionBeneficiarioNew.getPlanPoliza().getIdPlanPoliza()));
    }
    pm.makePersistent(excepcionBeneficiarioNew);
  }

  public void updateExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario) throws Exception
  {
    ExcepcionBeneficiario excepcionBeneficiarioModify = 
      findExcepcionBeneficiarioById(excepcionBeneficiario.getIdExcepcionBeneficiario());

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (excepcionBeneficiario.getFamiliar() != null) {
      excepcionBeneficiario.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        excepcionBeneficiario.getFamiliar().getIdFamiliar()));
    }

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (excepcionBeneficiario.getPersonal() != null) {
      excepcionBeneficiario.setPersonal(
        personalBeanBusiness.findPersonalById(
        excepcionBeneficiario.getPersonal().getIdPersonal()));
    }

    PlanPolizaBeanBusiness planPolizaBeanBusiness = new PlanPolizaBeanBusiness();

    if (excepcionBeneficiario.getPlanPoliza() != null) {
      excepcionBeneficiario.setPlanPoliza(
        planPolizaBeanBusiness.findPlanPolizaById(
        excepcionBeneficiario.getPlanPoliza().getIdPlanPoliza()));
    }

    BeanUtils.copyProperties(excepcionBeneficiarioModify, excepcionBeneficiario);
  }

  public void deleteExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ExcepcionBeneficiario excepcionBeneficiarioDelete = 
      findExcepcionBeneficiarioById(excepcionBeneficiario.getIdExcepcionBeneficiario());
    pm.deletePersistent(excepcionBeneficiarioDelete);
  }

  public ExcepcionBeneficiario findExcepcionBeneficiarioById(long idExcepcionBeneficiario) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idExcepcionBeneficiario == pIdExcepcionBeneficiario";
    Query query = pm.newQuery(ExcepcionBeneficiario.class, filter);

    query.declareParameters("long pIdExcepcionBeneficiario");

    parameters.put("pIdExcepcionBeneficiario", new Long(idExcepcionBeneficiario));

    Collection colExcepcionBeneficiario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colExcepcionBeneficiario.iterator();
    return (ExcepcionBeneficiario)iterator.next();
  }

  public Collection findExcepcionBeneficiarioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent excepcionBeneficiarioExtent = pm.getExtent(
      ExcepcionBeneficiario.class, true);
    Query query = pm.newQuery(excepcionBeneficiarioExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(ExcepcionBeneficiario.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colExcepcionBeneficiario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExcepcionBeneficiario);

    return colExcepcionBeneficiario;
  }

  public Collection findByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "planPoliza.idPlanPoliza == pIdPlanPoliza";

    Query query = pm.newQuery(ExcepcionBeneficiario.class, filter);

    query.declareParameters("long pIdPlanPoliza");
    HashMap parameters = new HashMap();

    parameters.put("pIdPlanPoliza", new Long(idPlanPoliza));

    Collection colExcepcionBeneficiario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExcepcionBeneficiario);

    return colExcepcionBeneficiario;
  }
}