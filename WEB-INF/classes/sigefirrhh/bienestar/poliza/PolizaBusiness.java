package sigefirrhh.bienestar.poliza;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class PolizaBusiness extends AbstractBusiness
  implements Serializable
{
  private BeneficiarioBeanBusiness beneficiarioBeanBusiness = new BeneficiarioBeanBusiness();

  private SiniestroBeanBusiness siniestroBeanBusiness = new SiniestroBeanBusiness();

  private TitularBeanBusiness titularBeanBusiness = new TitularBeanBusiness();

  private ExcepcionTitularBeanBusiness excepcionTitularBeanBusiness = new ExcepcionTitularBeanBusiness();

  private ExcepcionBeneficiarioBeanBusiness excepcionBeneficiarioBeanBusiness = new ExcepcionBeneficiarioBeanBusiness();

  private PrimasCargoBeanBusiness primasCargoBeanBusiness = new PrimasCargoBeanBusiness();

  public void addBeneficiario(Beneficiario beneficiario)
    throws Exception
  {
    this.beneficiarioBeanBusiness.addBeneficiario(beneficiario);
  }

  public void updateBeneficiario(Beneficiario beneficiario) throws Exception {
    this.beneficiarioBeanBusiness.updateBeneficiario(beneficiario);
  }

  public void deleteBeneficiario(Beneficiario beneficiario) throws Exception {
    this.beneficiarioBeanBusiness.deleteBeneficiario(beneficiario);
  }

  public Beneficiario findBeneficiarioById(long beneficiarioId) throws Exception {
    return this.beneficiarioBeanBusiness.findBeneficiarioById(beneficiarioId);
  }

  public Collection findAllBeneficiario() throws Exception {
    return this.beneficiarioBeanBusiness.findBeneficiarioAll();
  }

  public Collection findBeneficiarioByPersonal(long idPersonal)
    throws Exception
  {
    return this.beneficiarioBeanBusiness.findByPersonal(idPersonal);
  }

  public Collection findBeneficiarioByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    return this.beneficiarioBeanBusiness.findByPlanPoliza(idPlanPoliza);
  }

  public void addSiniestro(Siniestro siniestro)
    throws Exception
  {
    this.siniestroBeanBusiness.addSiniestro(siniestro);
  }

  public void updateSiniestro(Siniestro siniestro) throws Exception {
    this.siniestroBeanBusiness.updateSiniestro(siniestro);
  }

  public void deleteSiniestro(Siniestro siniestro) throws Exception {
    this.siniestroBeanBusiness.deleteSiniestro(siniestro);
  }

  public Siniestro findSiniestroById(long siniestroId) throws Exception {
    return this.siniestroBeanBusiness.findSiniestroById(siniestroId);
  }

  public Collection findAllSiniestro() throws Exception {
    return this.siniestroBeanBusiness.findSiniestroAll();
  }

  public Collection findSiniestroByBeneficiario(long idBeneficiario)
    throws Exception
  {
    return this.siniestroBeanBusiness.findByBeneficiario(idBeneficiario);
  }

  public Collection findSiniestroByPersonal(long idPersonal)
    throws Exception
  {
    return this.siniestroBeanBusiness.findByPersonal(idPersonal);
  }

  public void addTitular(Titular titular)
    throws Exception
  {
    this.titularBeanBusiness.addTitular(titular);
  }

  public void updateTitular(Titular titular) throws Exception {
    this.titularBeanBusiness.updateTitular(titular);
  }

  public void deleteTitular(Titular titular) throws Exception {
    this.titularBeanBusiness.deleteTitular(titular);
  }

  public Titular findTitularById(long titularId) throws Exception {
    return this.titularBeanBusiness.findTitularById(titularId);
  }

  public Collection findAllTitular() throws Exception {
    return this.titularBeanBusiness.findTitularAll();
  }

  public Collection findTitularByPersonal(long idPersonal)
    throws Exception
  {
    return this.titularBeanBusiness.findByPersonal(idPersonal);
  }

  public Collection findTitularByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    return this.titularBeanBusiness.findByPlanPoliza(idPlanPoliza);
  }

  public void addExcepcionTitular(ExcepcionTitular excepcionTitular)
    throws Exception
  {
    this.excepcionTitularBeanBusiness.addExcepcionTitular(excepcionTitular);
  }

  public void updateExcepcionTitular(ExcepcionTitular excepcionTitular) throws Exception {
    this.excepcionTitularBeanBusiness.updateExcepcionTitular(excepcionTitular);
  }

  public void deleteExcepcionTitular(ExcepcionTitular excepcionTitular) throws Exception {
    this.excepcionTitularBeanBusiness.deleteExcepcionTitular(excepcionTitular);
  }

  public ExcepcionTitular findExcepcionTitularById(long excepcionTitularId) throws Exception {
    return this.excepcionTitularBeanBusiness.findExcepcionTitularById(excepcionTitularId);
  }

  public Collection findAllExcepcionTitular() throws Exception {
    return this.excepcionTitularBeanBusiness.findExcepcionTitularAll();
  }

  public Collection findExcepcionTitularByPersonal(long idPersonal)
    throws Exception
  {
    return this.excepcionTitularBeanBusiness.findByPersonal(idPersonal);
  }

  public Collection findExcepcionTitularByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    return this.excepcionTitularBeanBusiness.findByPlanPoliza(idPlanPoliza);
  }

  public void addExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario)
    throws Exception
  {
    this.excepcionBeneficiarioBeanBusiness.addExcepcionBeneficiario(excepcionBeneficiario);
  }

  public void updateExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario) throws Exception {
    this.excepcionBeneficiarioBeanBusiness.updateExcepcionBeneficiario(excepcionBeneficiario);
  }

  public void deleteExcepcionBeneficiario(ExcepcionBeneficiario excepcionBeneficiario) throws Exception {
    this.excepcionBeneficiarioBeanBusiness.deleteExcepcionBeneficiario(excepcionBeneficiario);
  }

  public ExcepcionBeneficiario findExcepcionBeneficiarioById(long excepcionBeneficiarioId) throws Exception {
    return this.excepcionBeneficiarioBeanBusiness.findExcepcionBeneficiarioById(excepcionBeneficiarioId);
  }

  public Collection findAllExcepcionBeneficiario() throws Exception {
    return this.excepcionBeneficiarioBeanBusiness.findExcepcionBeneficiarioAll();
  }

  public Collection findExcepcionBeneficiarioByPersonal(long idPersonal)
    throws Exception
  {
    return this.excepcionBeneficiarioBeanBusiness.findByPersonal(idPersonal);
  }

  public Collection findExcepcionBeneficiarioByPlanPoliza(long idPlanPoliza)
    throws Exception
  {
    return this.excepcionBeneficiarioBeanBusiness.findByPlanPoliza(idPlanPoliza);
  }

  public void addPrimasCargo(PrimasCargo primasCargo)
    throws Exception
  {
    this.primasCargoBeanBusiness.addPrimasCargo(primasCargo);
  }

  public void updatePrimasCargo(PrimasCargo primasCargo) throws Exception {
    this.primasCargoBeanBusiness.updatePrimasCargo(primasCargo);
  }

  public void deletePrimasCargo(PrimasCargo primasCargo) throws Exception {
    this.primasCargoBeanBusiness.deletePrimasCargo(primasCargo);
  }

  public PrimasCargo findPrimasCargoById(long primasCargoId) throws Exception {
    return this.primasCargoBeanBusiness.findPrimasCargoById(primasCargoId);
  }

  public Collection findAllPrimasCargo() throws Exception {
    return this.primasCargoBeanBusiness.findPrimasCargoAll();
  }

  public Collection findPrimasCargoByPrimasPlan(long idPrimasPlan)
    throws Exception
  {
    return this.primasCargoBeanBusiness.findByPrimasPlan(idPrimasPlan);
  }
}