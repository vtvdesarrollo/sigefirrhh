package sigefirrhh.bienestar.poliza;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.PlanPoliza;
import sigefirrhh.base.bienestar.Poliza;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.expediente.ExpedienteFacade;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class ExcepcionBeneficiario_Form extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ExcepcionBeneficiario_Form.class.getName());
  private ExcepcionBeneficiario excepcionBeneficiario;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private ExpedienteFacade expedienteFacade = new ExpedienteFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private PolizaFacade polizaFacade = new PolizaFacade();
  private Collection resultPersonal;
  private Personal personal;
  private boolean selectedPersonal;
  private int findPersonalCedula;
  private String findPersonalPrimerNombre;
  private String findPersonalSegundoNombre;
  private String findPersonalPrimerApellido;
  private String findPersonalSegundoApellido;
  private boolean showResultPersonal;
  private boolean showAddResultPersonal;
  private boolean showResult;
  private String findSelectPersonal;
  private Collection colFamiliar;
  private Collection colPersonal;
  private Collection colPolizaForPlanPoliza;
  private Collection colPlanPoliza;
  private String selectFamiliar;
  private String selectPersonal;
  private String selectPolizaForPlanPoliza;
  private String selectPlanPoliza;
  private Object stateResultPersonal = null;

  private Object stateResultExcepcionBeneficiarioByPersonal = null;

  public String getSelectFamiliar()
  {
    return this.selectFamiliar;
  }
  public void setSelectFamiliar(String valFamiliar) {
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    this.excepcionBeneficiario.setFamiliar(null);
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      if (String.valueOf(familiar.getIdFamiliar()).equals(
        valFamiliar)) {
        this.excepcionBeneficiario.setFamiliar(
          familiar);
        break;
      }
    }
    this.selectFamiliar = valFamiliar;
  }
  public String getSelectPersonal() {
    return this.selectPersonal;
  }
  public void setSelectPersonal(String valPersonal) {
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    this.excepcionBeneficiario.setPersonal(null);
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      if (String.valueOf(personal.getIdPersonal()).equals(
        valPersonal)) {
        this.excepcionBeneficiario.setPersonal(
          personal);
        break;
      }
    }
    this.selectPersonal = valPersonal;
  }
  public String getSelectPolizaForPlanPoliza() {
    return this.selectPolizaForPlanPoliza;
  }
  public void setSelectPolizaForPlanPoliza(String valPolizaForPlanPoliza) {
    this.selectPolizaForPlanPoliza = valPolizaForPlanPoliza;
  }
  public void changePolizaForPlanPoliza(ValueChangeEvent event) {
    long idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colPlanPoliza = null;
      if (idPoliza > 0L)
        this.colPlanPoliza = 
          this.bienestarFacade.findPlanPolizaByPoliza(
          idPoliza);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowPolizaForPlanPoliza() { return this.colPolizaForPlanPoliza != null; }

  public String getSelectPlanPoliza() {
    return this.selectPlanPoliza;
  }
  public void setSelectPlanPoliza(String valPlanPoliza) {
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    this.excepcionBeneficiario.setPlanPoliza(null);
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      if (String.valueOf(planPoliza.getIdPlanPoliza()).equals(
        valPlanPoliza)) {
        this.excepcionBeneficiario.setPlanPoliza(
          planPoliza);
        break;
      }
    }
    this.selectPlanPoliza = valPlanPoliza;
  }
  public boolean isShowPlanPoliza() {
    return this.colPlanPoliza != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public ExcepcionBeneficiario getExcepcionBeneficiario() {
    if (this.excepcionBeneficiario == null) {
      this.excepcionBeneficiario = new ExcepcionBeneficiario();
    }
    return this.excepcionBeneficiario;
  }

  public ExcepcionBeneficiario_Form()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
    if (this.login.isServicioPersonal()) {
      this.findPersonalCedula = Integer.parseInt(this.login.getUsuario());
      findPersonalByCedula();
    }
  }

  public Collection getColFamiliar()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colFamiliar.iterator();
    Familiar familiar = null;
    while (iterator.hasNext()) {
      familiar = (Familiar)iterator.next();
      col.add(new SelectItem(
        String.valueOf(familiar.getIdFamiliar()), 
        familiar.toString()));
    }
    return col;
  }

  public Collection getColPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPersonal.iterator();
    Personal personal = null;
    while (iterator.hasNext()) {
      personal = (Personal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(personal.getIdPersonal()), 
        personal.toString()));
    }
    return col;
  }

  public Collection getColPolizaForPlanPoliza() {
    Collection col = new ArrayList();
    Iterator iterator = this.colPolizaForPlanPoliza.iterator();
    Poliza polizaForPlanPoliza = null;
    while (iterator.hasNext()) {
      polizaForPlanPoliza = (Poliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(polizaForPlanPoliza.getIdPoliza()), 
        polizaForPlanPoliza.toString()));
    }
    return col;
  }

  public Collection getColPlanPoliza()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colPlanPoliza.iterator();
    PlanPoliza planPoliza = null;
    while (iterator.hasNext()) {
      planPoliza = (PlanPoliza)iterator.next();
      col.add(new SelectItem(
        String.valueOf(planPoliza.getIdPlanPoliza()), 
        planPoliza.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colFamiliar = 
        this.expedienteFacade.findAllFamiliar();
      this.colPolizaForPlanPoliza = 
        this.bienestarFacade.findPolizaByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findPersonalByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      this.resultPersonal = 
        this.expedienteFacade.findPersonalByCedula(this.findPersonalCedula, 
        this.login.getOrganismo().getIdOrganismo());
      this.showResultPersonal = 
        ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));

      if (!this.showResultPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;
    return null;
  }

  public String findPersonalByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultPersonal();

      this.result = null;
      this.showResult = false;

      if (((this.findPersonalPrimerNombre == null) || (this.findPersonalPrimerNombre.equals(""))) && 
        ((this.findPersonalSegundoNombre == null) || (this.findPersonalSegundoNombre.equals(""))) && 
        ((this.findPersonalPrimerApellido == null) || (this.findPersonalPrimerApellido.equals(""))) && (
        (this.findPersonalSegundoApellido == null) || (this.findPersonalSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultPersonal = 
          this.expedienteFacade.findPersonalByNombresApellidos(
          this.findPersonalPrimerNombre, 
          this.findPersonalSegundoNombre, 
          this.findPersonalPrimerApellido, 
          this.findPersonalSegundoApellido, 
          this.login.getOrganismo().getIdOrganismo());
        this.showResultPersonal = 
          ((this.resultPersonal != null) && (!this.resultPersonal.isEmpty()));
        if (!this.showResultPersonal)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findPersonalCedula = 0;
    this.findPersonalPrimerNombre = null;
    this.findPersonalSegundoNombre = null;
    this.findPersonalPrimerApellido = null;
    this.findPersonalSegundoApellido = null;

    return null;
  }

  public String findExcepcionBeneficiarioByPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectPersonal();
      if (!this.adding) {
        this.result = 
          this.polizaFacade.findExcepcionBeneficiarioByPersonal(
          this.personal.getIdPersonal());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectExcepcionBeneficiario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectFamiliar = null;
    this.selectPersonal = null;
    this.selectPlanPoliza = null;
    this.selectPolizaForPlanPoliza = null;

    long idExcepcionBeneficiario = 
      Long.parseLong((String)requestParameterMap.get("idExcepcionBeneficiario"));
    try
    {
      this.excepcionBeneficiario = 
        this.polizaFacade.findExcepcionBeneficiarioById(
        idExcepcionBeneficiario);

      if (this.excepcionBeneficiario.getFamiliar() != null) {
        this.selectFamiliar = 
          String.valueOf(this.excepcionBeneficiario.getFamiliar().getIdFamiliar());
      }
      if (this.excepcionBeneficiario.getPersonal() != null) {
        this.selectPersonal = 
          String.valueOf(this.excepcionBeneficiario.getPersonal().getIdPersonal());
      }
      if (this.excepcionBeneficiario.getPlanPoliza() != null) {
        this.selectPlanPoliza = 
          String.valueOf(this.excepcionBeneficiario.getPlanPoliza().getIdPlanPoliza());
      }

      PlanPoliza planPoliza = null;
      Poliza polizaForPlanPoliza = null;

      if (this.excepcionBeneficiario.getPlanPoliza() != null) {
        long idPlanPoliza = 
          this.excepcionBeneficiario.getPlanPoliza().getIdPlanPoliza();
        this.selectPlanPoliza = String.valueOf(idPlanPoliza);
        planPoliza = this.bienestarFacade.findPlanPolizaById(
          idPlanPoliza);
        this.colPlanPoliza = this.bienestarFacade.findPlanPolizaByPoliza(
          planPoliza.getPoliza().getIdPoliza());

        long idPolizaForPlanPoliza = 
          this.excepcionBeneficiario.getPlanPoliza().getPoliza().getIdPoliza();
        this.selectPolizaForPlanPoliza = String.valueOf(idPolizaForPlanPoliza);
        polizaForPlanPoliza = 
          this.bienestarFacade.findPolizaById(
          idPolizaForPlanPoliza);
        this.colPolizaForPlanPoliza = 
          this.bienestarFacade.findAllPoliza();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idPersonal = 
      Long.parseLong((String)requestParameterMap.get("idPersonal"));
    try
    {
      this.personal = 
        this.expedienteFacade.findPersonalById(
        idPersonal);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedPersonal = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultPersonal();
  }

  private void resetResultPersonal() {
    this.resultPersonal = null;
    this.selectedPersonal = false;
    this.personal = null;

    this.showResultPersonal = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.excepcionBeneficiario.setPersonal(
          this.personal);
        this.polizaFacade.addExcepcionBeneficiario(
          this.excepcionBeneficiario);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.polizaFacade.updateExcepcionBeneficiario(
          this.excepcionBeneficiario);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e) {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.polizaFacade.deleteExcepcionBeneficiario(
        this.excepcionBeneficiario);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    } catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedPersonal = true;

    this.selectFamiliar = null;

    this.selectPersonal = null;

    this.selectPlanPoliza = null;

    this.selectPolizaForPlanPoliza = null;

    this.excepcionBeneficiario = new ExcepcionBeneficiario();

    this.excepcionBeneficiario.setPersonal(this.personal);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.excepcionBeneficiario.setIdExcepcionBeneficiario(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.poliza.ExcepcionBeneficiario"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.excepcionBeneficiario = new ExcepcionBeneficiario();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.excepcionBeneficiario = new ExcepcionBeneficiario();
    return "cancel";
  }
  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedPersonal));
  }

  public Collection getResultPersonal() {
    return this.resultPersonal;
  }
  public Personal getPersonal() {
    return this.personal;
  }
  public boolean isSelectedPersonal() {
    return this.selectedPersonal;
  }
  public int getFindPersonalCedula() {
    return this.findPersonalCedula;
  }
  public String getFindPersonalPrimerNombre() {
    return this.findPersonalPrimerNombre;
  }
  public String getFindPersonalSegundoNombre() {
    return this.findPersonalSegundoNombre;
  }
  public String getFindPersonalPrimerApellido() {
    return this.findPersonalPrimerApellido;
  }
  public String getFindPersonalSegundoApellido() {
    return this.findPersonalSegundoApellido;
  }
  public void setFindPersonalCedula(int cedula) {
    this.findPersonalCedula = cedula;
  }
  public void setFindPersonalPrimerNombre(String nombre) {
    this.findPersonalPrimerNombre = nombre;
  }
  public void setFindPersonalSegundoNombre(String nombre) {
    this.findPersonalSegundoNombre = nombre;
  }
  public void setFindPersonalPrimerApellido(String nombre) {
    this.findPersonalPrimerApellido = nombre;
  }
  public void setFindPersonalSegundoApellido(String nombre) {
    this.findPersonalSegundoApellido = nombre;
  }
  public boolean isShowResultPersonal() {
    return this.showResultPersonal;
  }
  public boolean isShowAddResultPersonal() {
    return this.showAddResultPersonal;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedPersonal);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectPersonal() {
    return this.findSelectPersonal;
  }

  public LoginSession getLogin()
  {
    return this.login;
  }
}