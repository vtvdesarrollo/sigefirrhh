package sigefirrhh.bienestar.poliza;

import eforserver.presentation.ListUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.DefinicionesFacadeExtend;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.FrecuenciaTipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class GenerarPrestamoPolizaForm
{
  static Logger log = Logger.getLogger(GenerarPrestamoPolizaForm.class.getName());
  private LoginSession login;
  private long idTipoPersonal;
  private String selectTipoPersonal;
  private long idConceptoTipoPersonal;
  private String selectConceptoTipoPersonal;
  private Collection listTipoPersonal;
  private Collection listConceptoTipoPersonal;
  private String selectPoliza;
  private long idPoliza;
  private Collection listPoliza;
  private long idPlanPoliza;
  private Collection listPlanPoliza;
  private DefinicionesNoGenFacade definicionesFacade;
  private DefinicionesFacadeExtend definicionesFacadeExtend;
  private PolizaNoGenFacade polizaFacade;
  private BienestarFacade bienestarFacade;
  private Date fecha;
  private String tipo = "T";
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private int numeroCuotas = 1;

  public boolean isShowConcepto() {
    return (this.listConceptoTipoPersonal != null) && (!this.listConceptoTipoPersonal.isEmpty());
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    this.idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.listConceptoTipoPersonal = null;
      if (this.idTipoPersonal != 0L) {
        this.listConceptoTipoPersonal = 
          this.definicionesFacadeExtend.findConceptoTipoPersonalByTipoPersonalAndTipoPrestamo(this.idTipoPersonal, "S");
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeConceptoTipoPersonal(ValueChangeEvent event) {
    this.idConceptoTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      if (this.idConceptoTipoPersonal != 0L)
        this.conceptoTipoPersonal = this.definicionesFacade.findConceptoTipoPersonalById(this.idConceptoTipoPersonal);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public GenerarPrestamoPolizaForm() {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.polizaFacade = new PolizaNoGenFacade();
    this.bienestarFacade = new BienestarFacade();
    this.definicionesFacadeExtend = new DefinicionesFacadeExtend();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    this.fecha = Calendar.getInstance().getTime();

    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listPoliza = this.bienestarFacade.findPolizaByVigente("S", this.login.getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
      this.listPoliza = new ArrayList();
    }
  }

  public String execute() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (this.conceptoTipoPersonal == null) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar el Concepto", ""));
        return null;
      }
      this.polizaFacade.generarPrestamoPoliza(this.idTipoPersonal, this.idPlanPoliza, this.tipo, this.conceptoTipoPersonal.getIdConceptoTipoPersonal(), this.conceptoTipoPersonal.getFrecuenciaTipoPersonal().getIdFrecuenciaTipoPersonal(), this.numeroCuotas);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      context.addMessage("success_process", new FacesMessage("Se generaron polizas con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public void changePoliza(ValueChangeEvent event)
  {
    this.idPoliza = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.listPlanPoliza = null;
      this.idPlanPoliza = 0L;
      if (this.idPoliza != 0L)
        this.listPlanPoliza = this.bienestarFacade.findPlanPolizaByPoliza(this.idPoliza);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowPlanPoliza() { return (this.listPlanPoliza != null) && (!this.listPlanPoliza.isEmpty()); }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getListPoliza() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listPoliza, "sigefirrhh.base.bienestar.Poliza");
  }
  public Collection getListConceptoTipoPersonal() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listConceptoTipoPersonal, "sigefirrhh.base.definiciones.ConceptoTipoPersonal");
  }
  public Collection getListPlanPoliza() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listPlanPoliza, "sigefirrhh.base.bienestar.PlanPoliza");
  }

  public Date getFecha() {
    return this.fecha;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public long getIdPlanPoliza() {
    return this.idPlanPoliza;
  }

  public void setIdPlanPoliza(long idPlanPoliza) {
    this.idPlanPoliza = idPlanPoliza;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }

  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getSelectPoliza() {
    return this.selectPoliza;
  }

  public void setSelectPoliza(String selectPoliza) {
    this.selectPoliza = selectPoliza;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  public int getNumeroCuotas() {
    return this.numeroCuotas;
  }
  public void setNumeroCuotas(int numeroCuotas) {
    this.numeroCuotas = numeroCuotas;
  }
  public String getSelectConceptoTipoPersonal() {
    return this.selectConceptoTipoPersonal;
  }
  public void setSelectConceptoTipoPersonal(String selectConceptoTipoPersonal) {
    this.selectConceptoTipoPersonal = selectConceptoTipoPersonal;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String selectTipoPersonal) {
    this.selectTipoPersonal = selectTipoPersonal;
  }
}