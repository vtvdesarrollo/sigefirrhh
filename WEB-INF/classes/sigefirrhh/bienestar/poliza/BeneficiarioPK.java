package sigefirrhh.bienestar.poliza;

import java.io.Serializable;

public class BeneficiarioPK
  implements Serializable
{
  public long idBeneficiario;

  public BeneficiarioPK()
  {
  }

  public BeneficiarioPK(long idBeneficiario)
  {
    this.idBeneficiario = idBeneficiario;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((BeneficiarioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(BeneficiarioPK thatPK)
  {
    return 
      this.idBeneficiario == thatPK.idBeneficiario;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idBeneficiario)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idBeneficiario);
  }
}