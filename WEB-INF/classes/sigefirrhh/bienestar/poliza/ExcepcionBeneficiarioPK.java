package sigefirrhh.bienestar.poliza;

import java.io.Serializable;

public class ExcepcionBeneficiarioPK
  implements Serializable
{
  public long idExcepcionBeneficiario;

  public ExcepcionBeneficiarioPK()
  {
  }

  public ExcepcionBeneficiarioPK(long idExcepcionBeneficiario)
  {
    this.idExcepcionBeneficiario = idExcepcionBeneficiario;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ExcepcionBeneficiarioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ExcepcionBeneficiarioPK thatPK)
  {
    return 
      this.idExcepcionBeneficiario == thatPK.idExcepcionBeneficiario;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idExcepcionBeneficiario)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idExcepcionBeneficiario);
  }
}