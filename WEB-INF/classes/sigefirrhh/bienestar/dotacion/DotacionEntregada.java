package sigefirrhh.bienestar.dotacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.personal.trabajador.Trabajador;

public class DotacionEntregada
  implements Serializable, PersistenceCapable
{
  private long idDotacionEntregada;
  private int anio;
  private int mes;
  private int cantidad;
  private String talla;
  private SubtipoDotacion subTipoDotacion;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "cantidad", "idDotacionEntregada", "mes", "subTipoDotacion", "talla", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.SubtipoDotacion"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetanio(this) + " - " + jdoGetmes(this) + " - " + jdoGetsubTipoDotacion(this).getNombre() + " talla " + 
      jdoGettalla(this) + " - " + jdoGetcantidad(this);
  }

  public int getCantidad()
  {
    return jdoGetcantidad(this);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public long getIdDotacionEntregada() {
    return jdoGetidDotacionEntregada(this);
  }
  public void setIdDotacionEntregada(long idDotacionEntregada) {
    jdoSetidDotacionEntregada(this, idDotacionEntregada);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public SubtipoDotacion getSubTipoDotacion() {
    return jdoGetsubTipoDotacion(this);
  }
  public void setSubTipoDotacion(SubtipoDotacion subTipoDotacion) {
    jdoSetsubTipoDotacion(this, subTipoDotacion);
  }
  public String getTalla() {
    return jdoGettalla(this);
  }
  public void setTalla(String talla) {
    jdoSettalla(this, talla);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }
  public void setCantidad(int cantidad) {
    jdoSetcantidad(this, cantidad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.dotacion.DotacionEntregada"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DotacionEntregada());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DotacionEntregada localDotacionEntregada = new DotacionEntregada();
    localDotacionEntregada.jdoFlags = 1;
    localDotacionEntregada.jdoStateManager = paramStateManager;
    return localDotacionEntregada;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DotacionEntregada localDotacionEntregada = new DotacionEntregada();
    localDotacionEntregada.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDotacionEntregada.jdoFlags = 1;
    localDotacionEntregada.jdoStateManager = paramStateManager;
    return localDotacionEntregada;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidad);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDotacionEntregada);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.subTipoDotacion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.talla);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDotacionEntregada = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subTipoDotacion = ((SubtipoDotacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.talla = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DotacionEntregada paramDotacionEntregada, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDotacionEntregada == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramDotacionEntregada.anio;
      return;
    case 1:
      if (paramDotacionEntregada == null)
        throw new IllegalArgumentException("arg1");
      this.cantidad = paramDotacionEntregada.cantidad;
      return;
    case 2:
      if (paramDotacionEntregada == null)
        throw new IllegalArgumentException("arg1");
      this.idDotacionEntregada = paramDotacionEntregada.idDotacionEntregada;
      return;
    case 3:
      if (paramDotacionEntregada == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramDotacionEntregada.mes;
      return;
    case 4:
      if (paramDotacionEntregada == null)
        throw new IllegalArgumentException("arg1");
      this.subTipoDotacion = paramDotacionEntregada.subTipoDotacion;
      return;
    case 5:
      if (paramDotacionEntregada == null)
        throw new IllegalArgumentException("arg1");
      this.talla = paramDotacionEntregada.talla;
      return;
    case 6:
      if (paramDotacionEntregada == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramDotacionEntregada.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DotacionEntregada))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DotacionEntregada localDotacionEntregada = (DotacionEntregada)paramObject;
    if (localDotacionEntregada.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDotacionEntregada, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DotacionEntregadaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DotacionEntregadaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DotacionEntregadaPK))
      throw new IllegalArgumentException("arg1");
    DotacionEntregadaPK localDotacionEntregadaPK = (DotacionEntregadaPK)paramObject;
    localDotacionEntregadaPK.idDotacionEntregada = this.idDotacionEntregada;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DotacionEntregadaPK))
      throw new IllegalArgumentException("arg1");
    DotacionEntregadaPK localDotacionEntregadaPK = (DotacionEntregadaPK)paramObject;
    this.idDotacionEntregada = localDotacionEntregadaPK.idDotacionEntregada;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DotacionEntregadaPK))
      throw new IllegalArgumentException("arg2");
    DotacionEntregadaPK localDotacionEntregadaPK = (DotacionEntregadaPK)paramObject;
    localDotacionEntregadaPK.idDotacionEntregada = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DotacionEntregadaPK))
      throw new IllegalArgumentException("arg2");
    DotacionEntregadaPK localDotacionEntregadaPK = (DotacionEntregadaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localDotacionEntregadaPK.idDotacionEntregada);
  }

  private static final int jdoGetanio(DotacionEntregada paramDotacionEntregada)
  {
    if (paramDotacionEntregada.jdoFlags <= 0)
      return paramDotacionEntregada.anio;
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionEntregada.anio;
    if (localStateManager.isLoaded(paramDotacionEntregada, jdoInheritedFieldCount + 0))
      return paramDotacionEntregada.anio;
    return localStateManager.getIntField(paramDotacionEntregada, jdoInheritedFieldCount + 0, paramDotacionEntregada.anio);
  }

  private static final void jdoSetanio(DotacionEntregada paramDotacionEntregada, int paramInt)
  {
    if (paramDotacionEntregada.jdoFlags == 0)
    {
      paramDotacionEntregada.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionEntregada.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramDotacionEntregada, jdoInheritedFieldCount + 0, paramDotacionEntregada.anio, paramInt);
  }

  private static final int jdoGetcantidad(DotacionEntregada paramDotacionEntregada)
  {
    if (paramDotacionEntregada.jdoFlags <= 0)
      return paramDotacionEntregada.cantidad;
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionEntregada.cantidad;
    if (localStateManager.isLoaded(paramDotacionEntregada, jdoInheritedFieldCount + 1))
      return paramDotacionEntregada.cantidad;
    return localStateManager.getIntField(paramDotacionEntregada, jdoInheritedFieldCount + 1, paramDotacionEntregada.cantidad);
  }

  private static final void jdoSetcantidad(DotacionEntregada paramDotacionEntregada, int paramInt)
  {
    if (paramDotacionEntregada.jdoFlags == 0)
    {
      paramDotacionEntregada.cantidad = paramInt;
      return;
    }
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionEntregada.cantidad = paramInt;
      return;
    }
    localStateManager.setIntField(paramDotacionEntregada, jdoInheritedFieldCount + 1, paramDotacionEntregada.cantidad, paramInt);
  }

  private static final long jdoGetidDotacionEntregada(DotacionEntregada paramDotacionEntregada)
  {
    return paramDotacionEntregada.idDotacionEntregada;
  }

  private static final void jdoSetidDotacionEntregada(DotacionEntregada paramDotacionEntregada, long paramLong)
  {
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionEntregada.idDotacionEntregada = paramLong;
      return;
    }
    localStateManager.setLongField(paramDotacionEntregada, jdoInheritedFieldCount + 2, paramDotacionEntregada.idDotacionEntregada, paramLong);
  }

  private static final int jdoGetmes(DotacionEntregada paramDotacionEntregada)
  {
    if (paramDotacionEntregada.jdoFlags <= 0)
      return paramDotacionEntregada.mes;
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionEntregada.mes;
    if (localStateManager.isLoaded(paramDotacionEntregada, jdoInheritedFieldCount + 3))
      return paramDotacionEntregada.mes;
    return localStateManager.getIntField(paramDotacionEntregada, jdoInheritedFieldCount + 3, paramDotacionEntregada.mes);
  }

  private static final void jdoSetmes(DotacionEntregada paramDotacionEntregada, int paramInt)
  {
    if (paramDotacionEntregada.jdoFlags == 0)
    {
      paramDotacionEntregada.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionEntregada.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramDotacionEntregada, jdoInheritedFieldCount + 3, paramDotacionEntregada.mes, paramInt);
  }

  private static final SubtipoDotacion jdoGetsubTipoDotacion(DotacionEntregada paramDotacionEntregada)
  {
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionEntregada.subTipoDotacion;
    if (localStateManager.isLoaded(paramDotacionEntregada, jdoInheritedFieldCount + 4))
      return paramDotacionEntregada.subTipoDotacion;
    return (SubtipoDotacion)localStateManager.getObjectField(paramDotacionEntregada, jdoInheritedFieldCount + 4, paramDotacionEntregada.subTipoDotacion);
  }

  private static final void jdoSetsubTipoDotacion(DotacionEntregada paramDotacionEntregada, SubtipoDotacion paramSubtipoDotacion)
  {
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionEntregada.subTipoDotacion = paramSubtipoDotacion;
      return;
    }
    localStateManager.setObjectField(paramDotacionEntregada, jdoInheritedFieldCount + 4, paramDotacionEntregada.subTipoDotacion, paramSubtipoDotacion);
  }

  private static final String jdoGettalla(DotacionEntregada paramDotacionEntregada)
  {
    if (paramDotacionEntregada.jdoFlags <= 0)
      return paramDotacionEntregada.talla;
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionEntregada.talla;
    if (localStateManager.isLoaded(paramDotacionEntregada, jdoInheritedFieldCount + 5))
      return paramDotacionEntregada.talla;
    return localStateManager.getStringField(paramDotacionEntregada, jdoInheritedFieldCount + 5, paramDotacionEntregada.talla);
  }

  private static final void jdoSettalla(DotacionEntregada paramDotacionEntregada, String paramString)
  {
    if (paramDotacionEntregada.jdoFlags == 0)
    {
      paramDotacionEntregada.talla = paramString;
      return;
    }
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionEntregada.talla = paramString;
      return;
    }
    localStateManager.setStringField(paramDotacionEntregada, jdoInheritedFieldCount + 5, paramDotacionEntregada.talla, paramString);
  }

  private static final Trabajador jdoGettrabajador(DotacionEntregada paramDotacionEntregada)
  {
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionEntregada.trabajador;
    if (localStateManager.isLoaded(paramDotacionEntregada, jdoInheritedFieldCount + 6))
      return paramDotacionEntregada.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramDotacionEntregada, jdoInheritedFieldCount + 6, paramDotacionEntregada.trabajador);
  }

  private static final void jdoSettrabajador(DotacionEntregada paramDotacionEntregada, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramDotacionEntregada.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionEntregada.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramDotacionEntregada, jdoInheritedFieldCount + 6, paramDotacionEntregada.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}