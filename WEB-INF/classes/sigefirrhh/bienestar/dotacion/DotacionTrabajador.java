package sigefirrhh.bienestar.dotacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.personal.trabajador.Trabajador;

public class DotacionTrabajador
  implements Serializable, PersistenceCapable
{
  private long idDotacionTrabajador;
  private int cantidad;
  private String talla;
  private SubtipoDotacion subTipoDotacion;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cantidad", "idDotacionTrabajador", "subTipoDotacion", "talla", "trabajador" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.SubtipoDotacion"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") };
  private static final byte[] jdoFieldFlags = { 21, 24, 26, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetsubTipoDotacion(this).getNombre() + " talla " + 
      jdoGettalla(this) + " - " + jdoGetcantidad(this);
  }

  public int getCantidad()
  {
    return jdoGetcantidad(this);
  }

  public long getIdDotacionTrabajador()
  {
    return jdoGetidDotacionTrabajador(this);
  }

  public SubtipoDotacion getSubTipoDotacion()
  {
    return jdoGetsubTipoDotacion(this);
  }

  public String getTalla()
  {
    return jdoGettalla(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setCantidad(int i)
  {
    jdoSetcantidad(this, i);
  }

  public void setIdDotacionTrabajador(long l)
  {
    jdoSetidDotacionTrabajador(this, l);
  }

  public void setSubTipoDotacion(SubtipoDotacion dotacion)
  {
    jdoSetsubTipoDotacion(this, dotacion);
  }

  public void setTalla(String string)
  {
    jdoSettalla(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.dotacion.DotacionTrabajador"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DotacionTrabajador());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DotacionTrabajador localDotacionTrabajador = new DotacionTrabajador();
    localDotacionTrabajador.jdoFlags = 1;
    localDotacionTrabajador.jdoStateManager = paramStateManager;
    return localDotacionTrabajador;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DotacionTrabajador localDotacionTrabajador = new DotacionTrabajador();
    localDotacionTrabajador.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDotacionTrabajador.jdoFlags = 1;
    localDotacionTrabajador.jdoStateManager = paramStateManager;
    return localDotacionTrabajador;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDotacionTrabajador);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.subTipoDotacion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.talla);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDotacionTrabajador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subTipoDotacion = ((SubtipoDotacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.talla = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DotacionTrabajador paramDotacionTrabajador, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDotacionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.cantidad = paramDotacionTrabajador.cantidad;
      return;
    case 1:
      if (paramDotacionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.idDotacionTrabajador = paramDotacionTrabajador.idDotacionTrabajador;
      return;
    case 2:
      if (paramDotacionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.subTipoDotacion = paramDotacionTrabajador.subTipoDotacion;
      return;
    case 3:
      if (paramDotacionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.talla = paramDotacionTrabajador.talla;
      return;
    case 4:
      if (paramDotacionTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramDotacionTrabajador.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DotacionTrabajador))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DotacionTrabajador localDotacionTrabajador = (DotacionTrabajador)paramObject;
    if (localDotacionTrabajador.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDotacionTrabajador, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DotacionTrabajadorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DotacionTrabajadorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DotacionTrabajadorPK))
      throw new IllegalArgumentException("arg1");
    DotacionTrabajadorPK localDotacionTrabajadorPK = (DotacionTrabajadorPK)paramObject;
    localDotacionTrabajadorPK.idDotacionTrabajador = this.idDotacionTrabajador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DotacionTrabajadorPK))
      throw new IllegalArgumentException("arg1");
    DotacionTrabajadorPK localDotacionTrabajadorPK = (DotacionTrabajadorPK)paramObject;
    this.idDotacionTrabajador = localDotacionTrabajadorPK.idDotacionTrabajador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DotacionTrabajadorPK))
      throw new IllegalArgumentException("arg2");
    DotacionTrabajadorPK localDotacionTrabajadorPK = (DotacionTrabajadorPK)paramObject;
    localDotacionTrabajadorPK.idDotacionTrabajador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DotacionTrabajadorPK))
      throw new IllegalArgumentException("arg2");
    DotacionTrabajadorPK localDotacionTrabajadorPK = (DotacionTrabajadorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localDotacionTrabajadorPK.idDotacionTrabajador);
  }

  private static final int jdoGetcantidad(DotacionTrabajador paramDotacionTrabajador)
  {
    if (paramDotacionTrabajador.jdoFlags <= 0)
      return paramDotacionTrabajador.cantidad;
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionTrabajador.cantidad;
    if (localStateManager.isLoaded(paramDotacionTrabajador, jdoInheritedFieldCount + 0))
      return paramDotacionTrabajador.cantidad;
    return localStateManager.getIntField(paramDotacionTrabajador, jdoInheritedFieldCount + 0, paramDotacionTrabajador.cantidad);
  }

  private static final void jdoSetcantidad(DotacionTrabajador paramDotacionTrabajador, int paramInt)
  {
    if (paramDotacionTrabajador.jdoFlags == 0)
    {
      paramDotacionTrabajador.cantidad = paramInt;
      return;
    }
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionTrabajador.cantidad = paramInt;
      return;
    }
    localStateManager.setIntField(paramDotacionTrabajador, jdoInheritedFieldCount + 0, paramDotacionTrabajador.cantidad, paramInt);
  }

  private static final long jdoGetidDotacionTrabajador(DotacionTrabajador paramDotacionTrabajador)
  {
    return paramDotacionTrabajador.idDotacionTrabajador;
  }

  private static final void jdoSetidDotacionTrabajador(DotacionTrabajador paramDotacionTrabajador, long paramLong)
  {
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionTrabajador.idDotacionTrabajador = paramLong;
      return;
    }
    localStateManager.setLongField(paramDotacionTrabajador, jdoInheritedFieldCount + 1, paramDotacionTrabajador.idDotacionTrabajador, paramLong);
  }

  private static final SubtipoDotacion jdoGetsubTipoDotacion(DotacionTrabajador paramDotacionTrabajador)
  {
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionTrabajador.subTipoDotacion;
    if (localStateManager.isLoaded(paramDotacionTrabajador, jdoInheritedFieldCount + 2))
      return paramDotacionTrabajador.subTipoDotacion;
    return (SubtipoDotacion)localStateManager.getObjectField(paramDotacionTrabajador, jdoInheritedFieldCount + 2, paramDotacionTrabajador.subTipoDotacion);
  }

  private static final void jdoSetsubTipoDotacion(DotacionTrabajador paramDotacionTrabajador, SubtipoDotacion paramSubtipoDotacion)
  {
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionTrabajador.subTipoDotacion = paramSubtipoDotacion;
      return;
    }
    localStateManager.setObjectField(paramDotacionTrabajador, jdoInheritedFieldCount + 2, paramDotacionTrabajador.subTipoDotacion, paramSubtipoDotacion);
  }

  private static final String jdoGettalla(DotacionTrabajador paramDotacionTrabajador)
  {
    if (paramDotacionTrabajador.jdoFlags <= 0)
      return paramDotacionTrabajador.talla;
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionTrabajador.talla;
    if (localStateManager.isLoaded(paramDotacionTrabajador, jdoInheritedFieldCount + 3))
      return paramDotacionTrabajador.talla;
    return localStateManager.getStringField(paramDotacionTrabajador, jdoInheritedFieldCount + 3, paramDotacionTrabajador.talla);
  }

  private static final void jdoSettalla(DotacionTrabajador paramDotacionTrabajador, String paramString)
  {
    if (paramDotacionTrabajador.jdoFlags == 0)
    {
      paramDotacionTrabajador.talla = paramString;
      return;
    }
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionTrabajador.talla = paramString;
      return;
    }
    localStateManager.setStringField(paramDotacionTrabajador, jdoInheritedFieldCount + 3, paramDotacionTrabajador.talla, paramString);
  }

  private static final Trabajador jdoGettrabajador(DotacionTrabajador paramDotacionTrabajador)
  {
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionTrabajador.trabajador;
    if (localStateManager.isLoaded(paramDotacionTrabajador, jdoInheritedFieldCount + 4))
      return paramDotacionTrabajador.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramDotacionTrabajador, jdoInheritedFieldCount + 4, paramDotacionTrabajador.trabajador);
  }

  private static final void jdoSettrabajador(DotacionTrabajador paramDotacionTrabajador, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramDotacionTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionTrabajador.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramDotacionTrabajador, jdoInheritedFieldCount + 4, paramDotacionTrabajador.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}