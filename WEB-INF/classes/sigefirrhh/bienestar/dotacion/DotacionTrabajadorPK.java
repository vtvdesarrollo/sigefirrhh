package sigefirrhh.bienestar.dotacion;

import java.io.Serializable;

public class DotacionTrabajadorPK
  implements Serializable
{
  public long idDotacionTrabajador;

  public DotacionTrabajadorPK()
  {
  }

  public DotacionTrabajadorPK(long idDotacionTrabajador)
  {
    this.idDotacionTrabajador = idDotacionTrabajador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DotacionTrabajadorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DotacionTrabajadorPK thatPK)
  {
    return 
      this.idDotacionTrabajador == thatPK.idDotacionTrabajador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDotacionTrabajador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDotacionTrabajador);
  }
}