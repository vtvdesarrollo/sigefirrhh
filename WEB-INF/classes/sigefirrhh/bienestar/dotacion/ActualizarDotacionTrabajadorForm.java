package sigefirrhh.bienestar.dotacion;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ActualizarDotacionTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ActualizarDotacionTrabajadorForm.class.getName());
  private Collection listTipoPersonal;
  private Collection listTipoDotacion;
  private Collection listSubtipoDotacion;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private BienestarFacade bienestarFacade;
  private long idTipoDotacion;
  private long idSubtipoDotacion;
  private long idTipoPersonal;
  private String selectIdTipoPersonal;
  private String selectIdTipoDotacion;
  private int cedula;
  private String talla;
  private int cedulaFinal;
  private String tallaFinal;
  private String nombre;
  private String apellido;
  private Connection connection = Resource.getConnection();
  Statement stExecute = null;
  StringBuffer sql = new StringBuffer();

  private ResultSet rsRegistroTrabajador = null;
  private PreparedStatement stRegistroTrabajador = null;

  public boolean isShowTipoDotacion()
  {
    return (this.listTipoDotacion != null) && (!this.listTipoDotacion.isEmpty());
  }
  public boolean isShowSubtipoDotacion() {
    return (this.listSubtipoDotacion != null) && (!this.listSubtipoDotacion.isEmpty());
  }

  public ActualizarDotacionTrabajadorForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.bienestarFacade = new BienestarFacade();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try
    {
      this.connection.setAutoCommit(true);
      this.connection = Resource.getConnection();

      this.sql = new StringBuffer();
      this.sql.append("select t.id_trabajador, p.primer_nombre, p.primer_apellido, ");
      this.sql.append("dt.id_dotacion_trabajador ");
      this.sql.append(" from trabajador t, personal p, dotaciontrabajador dt");
      this.sql.append(" where t.id_trabajador = dt.id_trabajador");
      this.sql.append(" and t.id_personal = p.id_personal");
      this.sql.append(" and t.estatus = 'A'");
      this.sql.append(" and  t.cedula = ? and t.id_tipo_personal = ?");
      this.sql.append(" and  dt.id_subtipo_dotacion = ? ");

      this.stRegistroTrabajador = this.connection.prepareStatement(
        this.sql.toString(), 
        1003, 
        1007);

      log.error(this.sql.toString());
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      return;
    }

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = 
        this.definicionesNoGenFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), 
        this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), 
        " asignan_dotaciones = 'S' ");

      this.listTipoDotacion = this.bienestarFacade.findAllTipoDotacion();
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
      this.listTipoDotacion = new ArrayList();
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(idTipoPersonal);

    this.nombre = "";
    this.apellido = "";
    this.cedulaFinal = 0;
  }

  public void changeTipoDotacion(ValueChangeEvent event)
  {
    long idTipoDotacion = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoDotacion = String.valueOf(idTipoDotacion);
    this.idSubtipoDotacion = 0L;
    this.listSubtipoDotacion = null;
    try
    {
      if (idTipoDotacion > 0L)
      {
        this.listSubtipoDotacion = 
          this.bienestarFacade.findSubtipoDotacionByTipoDotacion(idTipoDotacion);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    this.nombre = "";
    this.apellido = "";
    this.cedulaFinal = 0;
  }

  public String actualizar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.stRegistroTrabajador.setInt(1, this.cedula);
      this.stRegistroTrabajador.setLong(2, Long.valueOf(this.selectIdTipoPersonal).longValue());
      this.stRegistroTrabajador.setLong(3, this.idSubtipoDotacion);
      this.rsRegistroTrabajador = this.stRegistroTrabajador.executeQuery();

      if (this.rsRegistroTrabajador.next()) {
        this.nombre = this.rsRegistroTrabajador.getString("primer_nombre");
        this.apellido = this.rsRegistroTrabajador.getString("primer_apellido");
      }
      else {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El trabajador no existe o no tiene registrado este tipo de dotacion", ""));
        return null;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras buscaba al trabajador", ""));
      return null;
    }

    this.cedulaFinal = this.cedula;
    this.tallaFinal = this.talla;

    this.sql = new StringBuffer();
    try
    {
      this.stExecute = this.connection.createStatement();

      this.sql.append("update dotaciontrabajador set  talla = '" + this.talla + "' ");
      this.sql.append(" where id_dotacion_trabajador = " + this.rsRegistroTrabajador.getLong("id_dotacion_trabajador"));
      this.stExecute.addBatch(this.sql.toString());

      this.stExecute.executeBatch();

      this.stExecute.close();
    }
    catch (Exception e) {
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error", ""));
      log.error("Excepcion controlada:", e);
      return null;
    }

    this.cedula = 0;
    this.talla = null;
    this.tallaFinal = this.talla;

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListTipoDotacion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoDotacion, "sigefirrhh.base.bienestar.TipoDotacion");
  }

  public Collection getListSubtipoDotacion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listSubtipoDotacion, "sigefirrhh.base.bienestar.SubtipoDotacion");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public int getCedula()
  {
    return this.cedula;
  }

  public void setCedula(int cedula) {
    this.cedula = cedula;
  }

  public int getCedulaFinal() {
    return this.cedulaFinal;
  }
  public void setCedulaFinal(int cedulaFinal) {
    this.cedulaFinal = cedulaFinal;
  }
  public String getApellido() {
    return this.apellido;
  }
  public void setApellido(String apellido) {
    this.apellido = apellido;
  }
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public long getIdSubtipoDotacion() {
    return this.idSubtipoDotacion;
  }
  public void setIdSubtipoDotacion(long idSubtipoDotacion) {
    this.idSubtipoDotacion = idSubtipoDotacion;
  }
  public long getIdTipoDotacion() {
    return this.idTipoDotacion;
  }
  public void setIdTipoDotacion(long idTipoDotacion) {
    this.idTipoDotacion = idTipoDotacion;
  }
  public String getTalla() {
    return this.talla;
  }
  public void setTalla(String talla) {
    this.talla = talla;
  }
  public String getTallaFinal() {
    return this.tallaFinal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }
  public String getSelectIdTipoDotacion() {
    return this.selectIdTipoDotacion;
  }
  public void setSelectIdTipoDotacion(String selectIdTipoDotacion) {
    this.selectIdTipoDotacion = selectIdTipoDotacion;
  }
}