package sigefirrhh.bienestar.dotacion;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.apache.log4j.Logger;

public class DotacionNoGenBusiness extends DotacionBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(DotacionNoGenBusiness.class.getName());

  public void generarDotacionTrabajador(long idTipoPersonal, long idTipoDotacion, long idSubtipoDotacion)
    throws Exception
  {
    Connection connection = null;

    PreparedStatement st = null;

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select generar_dotaciontrabajador(?,?,?)");

      this.log.error("1- tipo peRsonal en SP :" + idTipoPersonal);
      this.log.error("2- tipo dotacion en SP : " + idTipoDotacion);
      this.log.error("3- subtipo dotacion en SP :" + idSubtipoDotacion);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idTipoDotacion);
      st.setLong(3, idSubtipoDotacion);

      st.executeQuery();

      connection.commit();

      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        } 
    }
  }

  public void generarDotacionEntregada(long idTipoPersonal, long idTipoDotacion, long idSubtipoDotacion, int anio, int mes, java.util.Date fecha, String accion) throws Exception {
    Connection connection = null;

    PreparedStatement st = null;

    java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      sql.append("select generar_dotacionentregada(?,?,?,?,?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + idTipoDotacion);
      this.log.error("3- " + idSubtipoDotacion);
      this.log.error("4- " + anio);
      this.log.error("5- " + mes);
      this.log.error("6- " + fecha);
      this.log.error("7- " + accion);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setLong(2, idTipoDotacion);
      st.setLong(3, idSubtipoDotacion);
      st.setInt(4, anio);
      st.setInt(5, mes);
      st.setDate(6, fechaSql);
      st.setString(7, accion);

      st.executeQuery();

      connection.commit();
      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        }
    }
  }
}