package sigefirrhh.bienestar.dotacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.base.bienestar.SubtipoDotacionBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class DotacionEntregadaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDotacionEntregada(DotacionEntregada dotacionEntregada)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DotacionEntregada dotacionEntregadaNew = 
      (DotacionEntregada)BeanUtils.cloneBean(
      dotacionEntregada);

    SubtipoDotacionBeanBusiness subTipoDotacionBeanBusiness = new SubtipoDotacionBeanBusiness();

    if (dotacionEntregadaNew.getSubTipoDotacion() != null) {
      dotacionEntregadaNew.setSubTipoDotacion(
        subTipoDotacionBeanBusiness.findSubtipoDotacionById(
        dotacionEntregadaNew.getSubTipoDotacion().getIdSubtipoDotacion()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (dotacionEntregadaNew.getTrabajador() != null) {
      dotacionEntregadaNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        dotacionEntregadaNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(dotacionEntregadaNew);
  }

  public void updateDotacionEntregada(DotacionEntregada dotacionEntregada) throws Exception
  {
    DotacionEntregada dotacionEntregadaModify = 
      findDotacionEntregadaById(dotacionEntregada.getIdDotacionEntregada());

    SubtipoDotacionBeanBusiness subTipoDotacionBeanBusiness = new SubtipoDotacionBeanBusiness();

    if (dotacionEntregada.getSubTipoDotacion() != null) {
      dotacionEntregada.setSubTipoDotacion(
        subTipoDotacionBeanBusiness.findSubtipoDotacionById(
        dotacionEntregada.getSubTipoDotacion().getIdSubtipoDotacion()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (dotacionEntregada.getTrabajador() != null) {
      dotacionEntregada.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        dotacionEntregada.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(dotacionEntregadaModify, dotacionEntregada);
  }

  public void deleteDotacionEntregada(DotacionEntregada dotacionEntregada) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DotacionEntregada dotacionEntregadaDelete = 
      findDotacionEntregadaById(dotacionEntregada.getIdDotacionEntregada());
    pm.deletePersistent(dotacionEntregadaDelete);
  }

  public DotacionEntregada findDotacionEntregadaById(long idDotacionEntregada) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDotacionEntregada == pIdDotacionEntregada";
    Query query = pm.newQuery(DotacionEntregada.class, filter);

    query.declareParameters("long pIdDotacionEntregada");

    parameters.put("pIdDotacionEntregada", new Long(idDotacionEntregada));

    Collection colDotacionEntregada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDotacionEntregada.iterator();
    return (DotacionEntregada)iterator.next();
  }

  public Collection findDotacionEntregadaAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent dotacionEntregadaExtent = pm.getExtent(
      DotacionEntregada.class, true);
    Query query = pm.newQuery(dotacionEntregadaExtent);
    query.setOrdering("anio descending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(DotacionEntregada.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("anio descending, mes ascending");

    Collection colDotacionEntregada = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDotacionEntregada);

    return colDotacionEntregada;
  }
}