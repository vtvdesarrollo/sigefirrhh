package sigefirrhh.bienestar.dotacion;

import java.io.Serializable;

public class DotacionEntregadaPK
  implements Serializable
{
  public long idDotacionEntregada;

  public DotacionEntregadaPK()
  {
  }

  public DotacionEntregadaPK(long idDotacionEntregada)
  {
    this.idDotacionEntregada = idDotacionEntregada;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DotacionEntregadaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DotacionEntregadaPK thatPK)
  {
    return 
      this.idDotacionEntregada == thatPK.idDotacionEntregada;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDotacionEntregada)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDotacionEntregada);
  }
}