package sigefirrhh.bienestar.dotacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.base.bienestar.TipoDotacion;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoNoGenFacade;
import sigefirrhh.base.cargo.ManualCargo;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class DotacionCargoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DotacionCargoForm.class.getName());
  private DotacionCargo dotacionCargo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private CargoNoGenFacade cargoFacade = new CargoNoGenFacade();
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private DotacionFacade dotacionFacade = new DotacionFacade();
  private boolean showDotacionCargoByTipoPersonal;
  private boolean showDotacionCargoByCargo;
  private String findSelectTipoPersonal;
  private String findSelectManualCargoForCargo;
  private String findSelectCargo;
  private Collection findColTipoPersonal;
  private Collection findColManualCargoForCargo;
  private Collection findColCargo;
  private Collection colTipoPersonal;
  private Collection colManualCargoForCargo;
  private Collection colCargo;
  private Collection colTipoDotacionForSubtipoDotacion;
  private Collection colSubtipoDotacion;
  private String selectTipoPersonal;
  private String selectManualCargoForCargo;
  private String selectCargo;
  private String selectTipoDotacionForSubtipoDotacion;
  private String selectSubtipoDotacion;
  private Object stateResultDotacionCargoByTipoPersonal = null;

  private Object stateResultDotacionCargoByCargo = null;

  public boolean isShowFindColManualCargoForCargo()
  {
    return (this.findColManualCargoForCargo != null) && (!this.findColManualCargoForCargo.isEmpty());
  }
  public boolean isShowColManualCargoForCargo() {
    return (this.colManualCargoForCargo != null) && (!this.colManualCargoForCargo.isEmpty());
  }

  public String getFindSelectTipoPersonal() {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }
  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.colManualCargoForCargo = null;
      this.colCargo = null;
      if (idTipoPersonal > 0L) {
        this.colManualCargoForCargo = 
          this.cargoFacade.findManualCargoByManualPersonal(
          idTipoPersonal);
      } else {
        this.selectManualCargoForCargo = null;
        this.selectCargo = null;
        this.dotacionCargo.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectManualCargoForCargo = null;
      this.selectCargo = null;
      this.dotacionCargo.setCargo(
        null);
    }
  }

  public void changeFindTipoPersonal(ValueChangeEvent event) { long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.findColManualCargoForCargo = null;
      this.findColCargo = null;
      if (idTipoPersonal > 0L) {
        this.findColManualCargoForCargo = 
          this.cargoFacade.findManualCargoByManualPersonal(
          idTipoPersonal);
      } else {
        this.findSelectManualCargoForCargo = null;
        this.findSelectCargo = null;
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.findSelectManualCargoForCargo = null;
      this.findSelectCargo = null;
    } }

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public Collection getFindColManualCargoForCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }
  public String getFindSelectManualCargoForCargo() {
    return this.findSelectManualCargoForCargo;
  }
  public void setFindSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.findSelectManualCargoForCargo = valManualCargoForCargo;
  }
  public void findChangeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.findColCargo = null;
      if (idManualCargo > 0L)
        this.findColCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isFindShowManualCargoForCargo() { return this.findColManualCargoForCargo != null; }

  public String getFindSelectCargo() {
    return this.findSelectCargo;
  }
  public void setFindSelectCargo(String valCargo) {
    this.findSelectCargo = valCargo;
  }

  public Collection getFindColCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }
  public boolean isFindShowCargo() {
    return this.findColCargo != null;
  }

  public String getSelectTipoPersonal()
  {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    this.dotacionCargo.setTipoPersonal(null);

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      iterator.next();
      if (String.valueOf(id).equals(
        valTipoPersonal)) {
        try {
          this.dotacionCargo.setTipoPersonal(this.definicionesFacade.findTipoPersonalById(id.longValue()));
        } catch (Exception e) {
          log.error("Excepcion controlada:", e);
        }
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public String getSelectManualCargoForCargo() {
    return this.selectManualCargoForCargo;
  }
  public void setSelectManualCargoForCargo(String valManualCargoForCargo) {
    this.selectManualCargoForCargo = valManualCargoForCargo;
  }
  public void changeManualCargoForCargo(ValueChangeEvent event) {
    long idManualCargo = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colCargo = null;
      if (idManualCargo > 0L) {
        this.colCargo = 
          this.cargoFacade.findCargoByManualCargo(
          idManualCargo);
      } else {
        this.selectCargo = null;
        this.dotacionCargo.setCargo(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectCargo = null;
      this.dotacionCargo.setCargo(
        null);
    }
  }

  public boolean isShowManualCargoForCargo() { return this.colManualCargoForCargo != null; }

  public String getSelectCargo() {
    return this.selectCargo;
  }
  public void setSelectCargo(String valCargo) {
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    this.dotacionCargo.setCargo(null);
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      if (String.valueOf(cargo.getIdCargo()).equals(
        valCargo)) {
        this.dotacionCargo.setCargo(
          cargo);
        break;
      }
    }
    this.selectCargo = valCargo;
  }
  public boolean isShowCargo() {
    return this.colCargo != null;
  }
  public String getSelectTipoDotacionForSubtipoDotacion() {
    return this.selectTipoDotacionForSubtipoDotacion;
  }
  public void setSelectTipoDotacionForSubtipoDotacion(String valTipoDotacionForSubtipoDotacion) {
    this.selectTipoDotacionForSubtipoDotacion = valTipoDotacionForSubtipoDotacion;
  }
  public void changeTipoDotacionForSubtipoDotacion(ValueChangeEvent event) {
    long idTipoDotacion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colSubtipoDotacion = null;
      if (idTipoDotacion > 0L) {
        this.colSubtipoDotacion = 
          this.bienestarFacade.findSubtipoDotacionByTipoDotacion(
          idTipoDotacion);
      } else {
        this.selectSubtipoDotacion = null;
        this.dotacionCargo.setSubtipoDotacion(
          null);
      }
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      this.selectSubtipoDotacion = null;
      this.dotacionCargo.setSubtipoDotacion(
        null);
    }
  }

  public boolean isShowTipoDotacionForSubtipoDotacion() { return this.colTipoDotacionForSubtipoDotacion != null; }

  public String getSelectSubtipoDotacion() {
    return this.selectSubtipoDotacion;
  }
  public void setSelectSubtipoDotacion(String valSubtipoDotacion) {
    Iterator iterator = this.colSubtipoDotacion.iterator();
    SubtipoDotacion subtipoDotacion = null;
    this.dotacionCargo.setSubtipoDotacion(null);
    while (iterator.hasNext()) {
      subtipoDotacion = (SubtipoDotacion)iterator.next();
      if (String.valueOf(subtipoDotacion.getIdSubtipoDotacion()).equals(
        valSubtipoDotacion)) {
        this.dotacionCargo.setSubtipoDotacion(
          subtipoDotacion);
        break;
      }
    }
    this.selectSubtipoDotacion = valSubtipoDotacion;
  }
  public boolean isShowSubtipoDotacion() {
    return this.colSubtipoDotacion != null;
  }
  public Collection getResult() {
    return this.result;
  }

  public DotacionCargo getDotacionCargo() {
    if (this.dotacionCargo == null) {
      this.dotacionCargo = new DotacionCargo();
    }
    return this.dotacionCargo;
  }

  public DotacionCargoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    refresh();
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getColManualCargoForCargo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colManualCargoForCargo.iterator();
    ManualCargo manualCargoForCargo = null;
    while (iterator.hasNext()) {
      manualCargoForCargo = (ManualCargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(manualCargoForCargo.getIdManualCargo()), 
        manualCargoForCargo.toString()));
    }
    return col;
  }

  public Collection getColCargo()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colCargo.iterator();
    Cargo cargo = null;
    while (iterator.hasNext()) {
      cargo = (Cargo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(cargo.getIdCargo()), 
        cargo.toString()));
    }
    return col;
  }

  public Collection getColTipoDotacionForSubtipoDotacion() {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoDotacionForSubtipoDotacion.iterator();
    TipoDotacion tipoDotacionForSubtipoDotacion = null;
    while (iterator.hasNext()) {
      tipoDotacionForSubtipoDotacion = (TipoDotacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoDotacionForSubtipoDotacion.getIdTipoDotacion()), 
        tipoDotacionForSubtipoDotacion.toString()));
    }
    return col;
  }

  public Collection getColSubtipoDotacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSubtipoDotacion.iterator();
    SubtipoDotacion subtipoDotacion = null;
    while (iterator.hasNext()) {
      subtipoDotacion = (SubtipoDotacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(subtipoDotacion.getIdSubtipoDotacion()), 
        subtipoDotacion.toString()));
    }
    return col;
  }

  public Collection getListUnidad()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = DotacionCargo.LISTA_UNIDAD.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), 
        this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), 
        " asignan_dotaciones = 'S' ");

      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), 
        this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), 
        " asignan_dotaciones = 'S' ");

      this.colTipoDotacionForSubtipoDotacion = 
        this.bienestarFacade.findAllTipoDotacion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findDotacionCargoByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.dotacionFacade.findDotacionCargoByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showDotacionCargoByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDotacionCargoByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;

    return null;
  }

  public String findDotacionCargoByCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.dotacionFacade.findDotacionCargoByCargo(Long.valueOf(this.findSelectCargo).longValue());
      this.showDotacionCargoByCargo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showDotacionCargoByCargo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectTipoPersonal = null;
    this.findSelectManualCargoForCargo = null;
    this.findSelectCargo = null;

    return null;
  }

  public boolean isShowDotacionCargoByTipoPersonal() {
    return this.showDotacionCargoByTipoPersonal;
  }
  public boolean isShowDotacionCargoByCargo() {
    return this.showDotacionCargoByCargo;
  }

  public String selectDotacionCargo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectTipoPersonal = null;
    this.selectCargo = null;
    this.selectManualCargoForCargo = null;

    this.selectSubtipoDotacion = null;
    this.selectTipoDotacionForSubtipoDotacion = null;

    long idDotacionCargo = 
      Long.parseLong((String)requestParameterMap.get("idDotacionCargo"));
    try
    {
      this.dotacionCargo = 
        this.dotacionFacade.findDotacionCargoById(
        idDotacionCargo);
      if (this.dotacionCargo.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.dotacionCargo.getTipoPersonal().getIdTipoPersonal());
      }
      if (this.dotacionCargo.getCargo() != null) {
        this.selectCargo = 
          String.valueOf(this.dotacionCargo.getCargo().getIdCargo());
      }
      if (this.dotacionCargo.getSubtipoDotacion() != null) {
        this.selectSubtipoDotacion = 
          String.valueOf(this.dotacionCargo.getSubtipoDotacion().getIdSubtipoDotacion());
      }

      Cargo cargo = null;
      ManualCargo manualCargoForCargo = null;
      SubtipoDotacion subtipoDotacion = null;
      TipoDotacion tipoDotacionForSubtipoDotacion = null;

      if (this.dotacionCargo.getCargo() != null) {
        long idCargo = 
          this.dotacionCargo.getCargo().getIdCargo();
        this.selectCargo = String.valueOf(idCargo);
        cargo = this.cargoFacade.findCargoById(
          idCargo);
        this.colCargo = this.cargoFacade.findCargoByManualCargo(
          cargo.getManualCargo().getIdManualCargo());

        long idManualCargoForCargo = 
          this.dotacionCargo.getCargo().getManualCargo().getIdManualCargo();
        this.selectManualCargoForCargo = String.valueOf(idManualCargoForCargo);
        manualCargoForCargo = 
          this.cargoFacade.findManualCargoById(
          idManualCargoForCargo);
        this.colManualCargoForCargo = 
          this.cargoFacade.findAllManualCargo();
      }
      if (this.dotacionCargo.getSubtipoDotacion() != null) {
        long idSubtipoDotacion = 
          this.dotacionCargo.getSubtipoDotacion().getIdSubtipoDotacion();
        this.selectSubtipoDotacion = String.valueOf(idSubtipoDotacion);
        subtipoDotacion = this.bienestarFacade.findSubtipoDotacionById(
          idSubtipoDotacion);
        this.colSubtipoDotacion = this.bienestarFacade.findSubtipoDotacionByTipoDotacion(
          subtipoDotacion.getTipoDotacion().getIdTipoDotacion());

        long idTipoDotacionForSubtipoDotacion = 
          this.dotacionCargo.getSubtipoDotacion().getTipoDotacion().getIdTipoDotacion();
        this.selectTipoDotacionForSubtipoDotacion = String.valueOf(idTipoDotacionForSubtipoDotacion);
        tipoDotacionForSubtipoDotacion = 
          this.bienestarFacade.findTipoDotacionById(
          idTipoDotacionForSubtipoDotacion);
        this.colTipoDotacionForSubtipoDotacion = 
          this.bienestarFacade.findAllTipoDotacion();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.dotacionCargo = null;
    this.showDotacionCargoByTipoPersonal = false;
    this.showDotacionCargoByCargo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.dotacionFacade.addDotacionCargo(
          this.dotacionCargo);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.dotacionFacade.updateDotacionCargo(
          this.dotacionCargo);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.dotacionFacade.deleteDotacionCargo(
        this.dotacionCargo);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.dotacionCargo = new DotacionCargo();

    this.selectSubtipoDotacion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.dotacionCargo.setIdDotacionCargo(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.dotacion.DotacionCargo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.dotacionCargo = new DotacionCargo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}