package sigefirrhh.bienestar.dotacion;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportDotacionesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportDotacionesForm.class.getName());
  private int reportId;
  private int anio;
  private int mes;
  private long idTipoPersonal;
  private String tipoReporte = "1";

  private String formato = "1";
  private String reportName;
  private String clausula;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private boolean showOrdenado = true;

  public boolean isShowOrdenado()
  {
    return this.showOrdenado;
  }

  public ReportDotacionesForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.anio = (new Date().getYear() + 1900);
    this.mes = (new Date().getMonth() + 1);

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.reportName = "dotacionesasignadas";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportDotacionesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), 
        this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), 
        " asignan_dotaciones = 'S' ");
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    try
    {
      if (this.tipoReporte.equals("1")) {
        this.reportName = "dotacionesasignadas";
        this.showOrdenado = true;
      } else if (this.tipoReporte.equals("2")) {
        this.reportName = "dotacionesentregadas";
        this.showOrdenado = false;
      } else if (this.tipoReporte.equals("3")) {
        this.reportName = "recibosdeentrega";
        this.showOrdenado = false;
      } else {
        this.reportName = "resumendotaciones";
        this.showOrdenado = false;
      }
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    try
    {
      if (this.tipoReporte.equals("1")) {
        this.reportName = "dotacionesasignadas";
        this.showOrdenado = true;
      } else if (this.tipoReporte.equals("2")) {
        this.reportName = "dotacionesentregadas";
        this.showOrdenado = false;
      } else if (this.tipoReporte.equals("3")) {
        this.reportName = "recibosdeentrega";
        this.showOrdenado = false;
      } else {
        this.reportName = "resumendotaciones";
        this.showOrdenado = false;
      }
      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));
      parameters.put("clausula", getClausula());

      if (this.tipoReporte.equals("1")) {
        parameters.put("path", ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/bienestar/dotacion");
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/bienestar/dotacion");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public String getTipoReporte() {
    return this.tipoReporte;
  }
  public void setTipoReporte(String tipoReporte) {
    this.tipoReporte = tipoReporte;
  }
  public String getClausula() {
    return this.clausula;
  }
  public void setClausula(String clausula) {
    this.clausula = clausula;
  }
}