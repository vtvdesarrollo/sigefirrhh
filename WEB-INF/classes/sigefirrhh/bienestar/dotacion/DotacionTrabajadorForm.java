package sigefirrhh.bienestar.dotacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.base.bienestar.TipoDotacion;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorFacade;

public class DotacionTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(DotacionTrabajadorForm.class.getName());
  private DotacionTrabajador dotacionTrabajador;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private BienestarFacade bienestarFacade = new BienestarFacade();
  private TrabajadorFacade trabajadorFacade = new TrabajadorFacade();
  private DotacionFacade dotacionFacade = new DotacionFacade();
  private DefinicionesNoGenFacade definicionesFacade = new DefinicionesNoGenFacade();
  private Collection resultTrabajador;
  private Trabajador trabajador;
  private boolean selectedTrabajador;
  private int findTrabajadorCedula;
  private String findSelectTrabajadorIdTipoPersonal;
  private int findTrabajadorCodigoNomina;
  private String findTrabajadorPrimerNombre;
  private String findTrabajadorSegundoNombre;
  private String findTrabajadorPrimerApellido;
  private String findTrabajadorSegundoApellido;
  private boolean showResultTrabajador;
  private boolean showAddResultTrabajador;
  private boolean showResult;
  private String findSelectTrabajador;
  private Collection colTipoDotacionForSubTipoDotacion;
  private Collection colSubTipoDotacion;
  private Collection colTrabajador;
  private String selectTipoDotacionForSubTipoDotacion;
  private String selectSubTipoDotacion;
  private String selectTrabajador;
  private Collection findColTipoPersonal;
  private Object stateResultDotacionTrabajadorByTrabajador = null;

  public Collection getFindColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public String getSelectTipoDotacionForSubTipoDotacion()
  {
    return this.selectTipoDotacionForSubTipoDotacion;
  }
  public void setSelectTipoDotacionForSubTipoDotacion(String valTipoDotacionForSubTipoDotacion) {
    this.selectTipoDotacionForSubTipoDotacion = valTipoDotacionForSubTipoDotacion;
  }
  public void changeTipoDotacionForSubTipoDotacion(ValueChangeEvent event) {
    long idTipoDotacion = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try
    {
      this.colSubTipoDotacion = null;
      if (idTipoDotacion > 0L)
        this.colSubTipoDotacion = 
          this.bienestarFacade.findSubtipoDotacionByTipoDotacion(
          idTipoDotacion);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public boolean isShowTipoDotacionForSubTipoDotacion() { return this.colTipoDotacionForSubTipoDotacion != null; }

  public String getSelectSubTipoDotacion() {
    return this.selectSubTipoDotacion;
  }
  public void setSelectSubTipoDotacion(String valSubTipoDotacion) {
    Iterator iterator = this.colSubTipoDotacion.iterator();
    SubtipoDotacion subTipoDotacion = null;
    this.dotacionTrabajador.setSubTipoDotacion(null);
    while (iterator.hasNext()) {
      subTipoDotacion = (SubtipoDotacion)iterator.next();
      if (String.valueOf(subTipoDotacion.getIdSubtipoDotacion()).equals(
        valSubTipoDotacion)) {
        this.dotacionTrabajador.setSubTipoDotacion(
          subTipoDotacion);
        break;
      }
    }
    this.selectSubTipoDotacion = valSubTipoDotacion;
  }
  public boolean isShowSubTipoDotacion() {
    return this.colSubTipoDotacion != null;
  }
  public String getSelectTrabajador() {
    return this.selectTrabajador;
  }
  public void setSelectTrabajador(String valTrabajador) {
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    this.dotacionTrabajador.setTrabajador(null);
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      if (String.valueOf(trabajador.getIdTrabajador()).equals(
        valTrabajador)) {
        this.dotacionTrabajador.setTrabajador(
          trabajador);
        break;
      }
    }
    this.selectTrabajador = valTrabajador;
  }
  public Collection getResult() {
    return this.result;
  }

  public DotacionTrabajador getDotacionTrabajador() {
    if (this.dotacionTrabajador == null) {
      this.dotacionTrabajador = new DotacionTrabajador();
      this.dotacionTrabajador.setTalla("U");
    }
    return this.dotacionTrabajador;
  }

  public DotacionTrabajadorForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColTipoDotacionForSubTipoDotacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoDotacionForSubTipoDotacion.iterator();
    TipoDotacion tipoDotacionForSubTipoDotacion = null;
    while (iterator.hasNext()) {
      tipoDotacionForSubTipoDotacion = (TipoDotacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoDotacionForSubTipoDotacion.getIdTipoDotacion()), 
        tipoDotacionForSubTipoDotacion.toString()));
    }
    return col;
  }

  public Collection getColSubTipoDotacion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colSubTipoDotacion.iterator();
    SubtipoDotacion subTipoDotacion = null;
    while (iterator.hasNext()) {
      subTipoDotacion = (SubtipoDotacion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(subTipoDotacion.getIdSubtipoDotacion()), 
        subTipoDotacion.toString()));
    }
    return col;
  }

  public Collection getColTrabajador()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTrabajador.iterator();
    Trabajador trabajador = null;
    while (iterator.hasNext()) {
      trabajador = (Trabajador)iterator.next();
      col.add(new SelectItem(
        String.valueOf(trabajador.getIdTrabajador()), 
        trabajador.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), 
        this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), 
        " asignan_dotaciones = 'S' ");

      this.colTipoDotacionForSubTipoDotacion = 
        this.bienestarFacade.findAllTipoDotacion();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findTrabajadorByCedula() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCedulaAndIdTipoPersonal(this.findTrabajadorCedula, 
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByCodigoNomina() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      this.resultTrabajador = 
        this.trabajadorFacade.findTrabajadorByCodigoNomina(
        Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue(), 
        this.findTrabajadorCodigoNomina);
      this.showResultTrabajador = 
        ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));

      if (!this.showResultTrabajador)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findTrabajadorByNombresApellidos() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResultTrabajador();

      this.result = null;
      this.showResult = false;

      if (((this.findTrabajadorPrimerNombre == null) || (this.findTrabajadorPrimerNombre.equals(""))) && 
        ((this.findTrabajadorSegundoNombre == null) || (this.findTrabajadorSegundoNombre.equals(""))) && 
        ((this.findTrabajadorPrimerApellido == null) || (this.findTrabajadorPrimerApellido.equals(""))) && (
        (this.findTrabajadorSegundoApellido == null) || (this.findTrabajadorSegundoApellido.equals(""))))
      {
        context.addMessage("error_search", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe introducir al menos un criterio", ""));
      }
      else
      {
        this.resultTrabajador = 
          this.trabajadorFacade.findTrabajadorByNombresApellidosAndIdTipoPersonal(
          this.findTrabajadorPrimerNombre, 
          this.findTrabajadorSegundoNombre, 
          this.findTrabajadorPrimerApellido, 
          this.findTrabajadorSegundoApellido, 
          Long.valueOf(this.findSelectTrabajadorIdTipoPersonal).longValue());

        this.showResultTrabajador = 
          ((this.resultTrabajador != null) && (!this.resultTrabajador.isEmpty()));
        if (!this.showResultTrabajador)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }

    this.findTrabajadorCedula = 0;
    this.findTrabajadorPrimerNombre = null;
    this.findTrabajadorSegundoNombre = null;
    this.findTrabajadorPrimerApellido = null;
    this.findTrabajadorSegundoApellido = null;
    this.findSelectTrabajadorIdTipoPersonal = null;
    this.findTrabajadorCodigoNomina = 0;
    return null;
  }

  public String findDotacionTrabajadorByTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      resetResult();

      selectTrabajador();
      if (!this.adding) {
        this.result = 
          this.dotacionFacade.findDotacionTrabajadorByTrabajador(
          this.trabajador.getIdTrabajador());
        this.showResult = 
          ((this.result != null) && (!this.result.isEmpty()));

        if (!this.showResult)
          context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }

    return null;
  }

  public String selectDotacionTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectSubTipoDotacion = null;
    this.selectTipoDotacionForSubTipoDotacion = null;

    this.selectTrabajador = null;

    long idDotacionTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idDotacionTrabajador"));
    try
    {
      this.dotacionTrabajador = 
        this.dotacionFacade.findDotacionTrabajadorById(
        idDotacionTrabajador);

      if (this.dotacionTrabajador.getSubTipoDotacion() != null) {
        this.selectSubTipoDotacion = 
          String.valueOf(this.dotacionTrabajador.getSubTipoDotacion().getIdSubtipoDotacion());
      }
      if (this.dotacionTrabajador.getTrabajador() != null) {
        this.selectTrabajador = 
          String.valueOf(this.dotacionTrabajador.getTrabajador().getIdTrabajador());
      }

      SubtipoDotacion subTipoDotacion = null;
      TipoDotacion tipoDotacionForSubTipoDotacion = null;

      if (this.dotacionTrabajador.getSubTipoDotacion() != null) {
        long idSubTipoDotacion = 
          this.dotacionTrabajador.getSubTipoDotacion().getIdSubtipoDotacion();
        this.selectSubTipoDotacion = String.valueOf(idSubTipoDotacion);
        subTipoDotacion = this.bienestarFacade.findSubtipoDotacionById(
          idSubTipoDotacion);
        this.colSubTipoDotacion = this.bienestarFacade.findSubtipoDotacionByTipoDotacion(
          subTipoDotacion.getTipoDotacion().getIdTipoDotacion());

        long idTipoDotacionForSubTipoDotacion = 
          this.dotacionTrabajador.getSubTipoDotacion().getTipoDotacion().getIdTipoDotacion();
        this.selectTipoDotacionForSubTipoDotacion = String.valueOf(idTipoDotacionForSubTipoDotacion);
        tipoDotacionForSubTipoDotacion = 
          this.bienestarFacade.findTipoDotacionById(
          idTipoDotacionForSubTipoDotacion);
        this.colTipoDotacionForSubTipoDotacion = 
          this.bienestarFacade.findAllTipoDotacion();
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  public String selectTrabajador()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idTrabajador = 
      Long.parseLong((String)requestParameterMap.get("idTrabajador"));
    try
    {
      this.trabajador = 
        this.trabajadorFacade.findTrabajadorById(
        idTrabajador);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    this.selectedTrabajador = true;

    return null;
  }

  private void resetResult()
  {
    this.result = null;
    this.selected = false;

    this.showResult = false;
    resetResultTrabajador();
  }

  private void resetResultTrabajador() {
    this.resultTrabajador = null;
    this.selectedTrabajador = false;
    this.trabajador = null;

    this.showResultTrabajador = false;
  }

  public String edit() {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error) {
      return null;
    }
    try
    {
      if (this.adding) {
        this.dotacionTrabajador.setTrabajador(
          this.trabajador);
        this.dotacionFacade.addDotacionTrabajador(
          this.dotacionTrabajador);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.dotacionFacade.updateDotacionTrabajador(
          this.dotacionTrabajador);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.result = null;
      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;
    }
    catch (Exception e)
    {
      if (this.adding)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al agregar\n" + e.toString(), ""));
      else {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar\n" + e.toString(), ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }

  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.dotacionFacade.deleteDotacionTrabajador(
        this.dotacionTrabajador);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.result = null;

      this.selected = false;
      this.showResult = false;

      this.adding = false;
      this.editing = false;
      this.selected = false;

      abortUpdate();
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al eliminar\n" + e.toString(), ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.selectedTrabajador = true;

    this.selectSubTipoDotacion = null;

    this.selectTipoDotacionForSubTipoDotacion = null;

    this.selectTrabajador = null;

    this.dotacionTrabajador = new DotacionTrabajador();
    this.dotacionTrabajador.setTalla("U");

    this.dotacionTrabajador.setTrabajador(this.trabajador);

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.dotacionTrabajador.setIdDotacionTrabajador(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.dotacion.DotacionTrabajador"));

    return null;
  }

  public String abort()
  {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.dotacionTrabajador = new DotacionTrabajador();
    resetResult();
    return "cancel";
  }

  public String abortUpdate() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    this.result = null;
    this.showResult = false;
    this.dotacionTrabajador = new DotacionTrabajador();
    return "cancel";
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || ((this.adding) && (this.selectedTrabajador));
  }

  public Collection getResultTrabajador() {
    return this.resultTrabajador;
  }
  public Trabajador getTrabajador() {
    return this.trabajador;
  }
  public boolean isSelectedTrabajador() {
    return this.selectedTrabajador;
  }
  public int getFindTrabajadorCedula() {
    return this.findTrabajadorCedula;
  }
  public String getFindTrabajadorPrimerNombre() {
    return this.findTrabajadorPrimerNombre;
  }
  public String getFindTrabajadorSegundoNombre() {
    return this.findTrabajadorSegundoNombre;
  }
  public String getFindTrabajadorPrimerApellido() {
    return this.findTrabajadorPrimerApellido;
  }
  public String getFindTrabajadorSegundoApellido() {
    return this.findTrabajadorSegundoApellido;
  }
  public void setFindTrabajadorCedula(int cedula) {
    this.findTrabajadorCedula = cedula;
  }
  public void setFindTrabajadorPrimerNombre(String nombre) {
    this.findTrabajadorPrimerNombre = nombre;
  }
  public void setFindTrabajadorSegundoNombre(String nombre) {
    this.findTrabajadorSegundoNombre = nombre;
  }
  public void setFindTrabajadorPrimerApellido(String nombre) {
    this.findTrabajadorPrimerApellido = nombre;
  }
  public void setFindTrabajadorSegundoApellido(String nombre) {
    this.findTrabajadorSegundoApellido = nombre;
  }
  public String getFindSelectTrabajadorIdTipoPersonal() {
    return this.findSelectTrabajadorIdTipoPersonal;
  }
  public void setFindSelectTrabajadorIdTipoPersonal(String idTipoPersonal) {
    this.findSelectTrabajadorIdTipoPersonal = idTipoPersonal;
  }
  public int getFindTrabajadorCodigoNomina() {
    return this.findTrabajadorCodigoNomina;
  }
  public void setFindTrabajadorCodigoNomina(int codigoNomina) {
    this.findTrabajadorCodigoNomina = codigoNomina;
  }
  public boolean isShowResultTrabajador() {
    return this.showResultTrabajador;
  }
  public boolean isShowAddResultTrabajador() {
    return this.showAddResultTrabajador;
  }
  public boolean isShowAdd() {
    return (this.adding) && (!this.selectedTrabajador);
  }
  public boolean isShowResult() {
    return this.showResult;
  }
  public String getFindSelectTrabajador() {
    return this.findSelectTrabajador;
  }

  public LoginSession getLogin() {
    return this.login;
  }
}