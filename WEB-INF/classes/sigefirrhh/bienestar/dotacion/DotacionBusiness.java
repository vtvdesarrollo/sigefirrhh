package sigefirrhh.bienestar.dotacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class DotacionBusiness extends AbstractBusiness
  implements Serializable
{
  private DotacionCargoBeanBusiness dotacionCargoBeanBusiness = new DotacionCargoBeanBusiness();

  private DotacionEntregadaBeanBusiness dotacionEntregadaBeanBusiness = new DotacionEntregadaBeanBusiness();

  private DotacionTrabajadorBeanBusiness dotacionTrabajadorBeanBusiness = new DotacionTrabajadorBeanBusiness();

  public void addDotacionCargo(DotacionCargo dotacionCargo)
    throws Exception
  {
    this.dotacionCargoBeanBusiness.addDotacionCargo(dotacionCargo);
  }

  public void updateDotacionCargo(DotacionCargo dotacionCargo) throws Exception {
    this.dotacionCargoBeanBusiness.updateDotacionCargo(dotacionCargo);
  }

  public void deleteDotacionCargo(DotacionCargo dotacionCargo) throws Exception {
    this.dotacionCargoBeanBusiness.deleteDotacionCargo(dotacionCargo);
  }

  public DotacionCargo findDotacionCargoById(long dotacionCargoId) throws Exception {
    return this.dotacionCargoBeanBusiness.findDotacionCargoById(dotacionCargoId);
  }

  public Collection findAllDotacionCargo() throws Exception {
    return this.dotacionCargoBeanBusiness.findDotacionCargoAll();
  }

  public Collection findDotacionCargoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.dotacionCargoBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findDotacionCargoByCargo(long idCargo)
    throws Exception
  {
    return this.dotacionCargoBeanBusiness.findByCargo(idCargo);
  }

  public void addDotacionEntregada(DotacionEntregada dotacionEntregada)
    throws Exception
  {
    this.dotacionEntregadaBeanBusiness.addDotacionEntregada(dotacionEntregada);
  }

  public void updateDotacionEntregada(DotacionEntregada dotacionEntregada) throws Exception {
    this.dotacionEntregadaBeanBusiness.updateDotacionEntregada(dotacionEntregada);
  }

  public void deleteDotacionEntregada(DotacionEntregada dotacionEntregada) throws Exception {
    this.dotacionEntregadaBeanBusiness.deleteDotacionEntregada(dotacionEntregada);
  }

  public DotacionEntregada findDotacionEntregadaById(long dotacionEntregadaId) throws Exception {
    return this.dotacionEntregadaBeanBusiness.findDotacionEntregadaById(dotacionEntregadaId);
  }

  public Collection findAllDotacionEntregada() throws Exception {
    return this.dotacionEntregadaBeanBusiness.findDotacionEntregadaAll();
  }

  public Collection findDotacionEntregadaByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.dotacionEntregadaBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addDotacionTrabajador(DotacionTrabajador dotacionTrabajador)
    throws Exception
  {
    this.dotacionTrabajadorBeanBusiness.addDotacionTrabajador(dotacionTrabajador);
  }

  public void updateDotacionTrabajador(DotacionTrabajador dotacionTrabajador) throws Exception {
    this.dotacionTrabajadorBeanBusiness.updateDotacionTrabajador(dotacionTrabajador);
  }

  public void deleteDotacionTrabajador(DotacionTrabajador dotacionTrabajador) throws Exception {
    this.dotacionTrabajadorBeanBusiness.deleteDotacionTrabajador(dotacionTrabajador);
  }

  public DotacionTrabajador findDotacionTrabajadorById(long dotacionTrabajadorId) throws Exception {
    return this.dotacionTrabajadorBeanBusiness.findDotacionTrabajadorById(dotacionTrabajadorId);
  }

  public Collection findAllDotacionTrabajador() throws Exception {
    return this.dotacionTrabajadorBeanBusiness.findDotacionTrabajadorAll();
  }

  public Collection findDotacionTrabajadorByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.dotacionTrabajadorBeanBusiness.findByTrabajador(idTrabajador);
  }
}