package sigefirrhh.bienestar.dotacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.definiciones.TipoPersonal;

public class DotacionCargo
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_UNIDAD;
  private long idDotacionCargo;
  private TipoPersonal tipoPersonal;
  private Cargo cargo;
  private SubtipoDotacion subtipoDotacion;
  private int cantidad;
  private String unidad;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "cantidad", "cargo", "idDotacionCargo", "subtipoDotacion", "tipoPersonal", "unidad" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.base.cargo.Cargo"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.SubtipoDotacion"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 26, 24, 26, 26, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.dotacion.DotacionCargo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DotacionCargo());

    LISTA_UNIDAD = 
      new LinkedHashMap();

    LISTA_UNIDAD.put("M", "MENSUAL");
    LISTA_UNIDAD.put("T", "TRIMESTRAL");
    LISTA_UNIDAD.put("S", "SEMESTRAL");
    LISTA_UNIDAD.put("A", "ANUAL");
    LISTA_UNIDAD.put("N", "NO PROGRAMADA");
  }

  public String toString()
  {
    return jdoGetcargo(this).getDescripcionCargo() + " - " + 
      jdoGetsubtipoDotacion(this).getNombre();
  }

  public int getCantidad() {
    return jdoGetcantidad(this);
  }

  public void setCantidad(int cantidad) {
    jdoSetcantidad(this, cantidad);
  }

  public Cargo getCargo() {
    return jdoGetcargo(this);
  }

  public void setCargo(Cargo cargo) {
    jdoSetcargo(this, cargo);
  }

  public long getIdDotacionCargo() {
    return jdoGetidDotacionCargo(this);
  }

  public void setIdDotacionCargo(long idDotacionCargo) {
    jdoSetidDotacionCargo(this, idDotacionCargo);
  }

  public SubtipoDotacion getSubtipoDotacion() {
    return jdoGetsubtipoDotacion(this);
  }

  public void setSubtipoDotacion(SubtipoDotacion subtipoDotacion) {
    jdoSetsubtipoDotacion(this, subtipoDotacion);
  }

  public TipoPersonal getTipoPersonal() {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal) {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public String getUnidad() {
    return jdoGetunidad(this);
  }

  public void setUnidad(String unidad) {
    jdoSetunidad(this, unidad);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DotacionCargo localDotacionCargo = new DotacionCargo();
    localDotacionCargo.jdoFlags = 1;
    localDotacionCargo.jdoStateManager = paramStateManager;
    return localDotacionCargo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DotacionCargo localDotacionCargo = new DotacionCargo();
    localDotacionCargo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDotacionCargo.jdoFlags = 1;
    localDotacionCargo.jdoStateManager = paramStateManager;
    return localDotacionCargo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cargo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDotacionCargo);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.subtipoDotacion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.unidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidad = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cargo = ((Cargo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDotacionCargo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.subtipoDotacion = ((SubtipoDotacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidad = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DotacionCargo paramDotacionCargo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDotacionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cantidad = paramDotacionCargo.cantidad;
      return;
    case 1:
      if (paramDotacionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.cargo = paramDotacionCargo.cargo;
      return;
    case 2:
      if (paramDotacionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.idDotacionCargo = paramDotacionCargo.idDotacionCargo;
      return;
    case 3:
      if (paramDotacionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.subtipoDotacion = paramDotacionCargo.subtipoDotacion;
      return;
    case 4:
      if (paramDotacionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramDotacionCargo.tipoPersonal;
      return;
    case 5:
      if (paramDotacionCargo == null)
        throw new IllegalArgumentException("arg1");
      this.unidad = paramDotacionCargo.unidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DotacionCargo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DotacionCargo localDotacionCargo = (DotacionCargo)paramObject;
    if (localDotacionCargo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDotacionCargo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DotacionCargoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DotacionCargoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DotacionCargoPK))
      throw new IllegalArgumentException("arg1");
    DotacionCargoPK localDotacionCargoPK = (DotacionCargoPK)paramObject;
    localDotacionCargoPK.idDotacionCargo = this.idDotacionCargo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DotacionCargoPK))
      throw new IllegalArgumentException("arg1");
    DotacionCargoPK localDotacionCargoPK = (DotacionCargoPK)paramObject;
    this.idDotacionCargo = localDotacionCargoPK.idDotacionCargo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DotacionCargoPK))
      throw new IllegalArgumentException("arg2");
    DotacionCargoPK localDotacionCargoPK = (DotacionCargoPK)paramObject;
    localDotacionCargoPK.idDotacionCargo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DotacionCargoPK))
      throw new IllegalArgumentException("arg2");
    DotacionCargoPK localDotacionCargoPK = (DotacionCargoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localDotacionCargoPK.idDotacionCargo);
  }

  private static final int jdoGetcantidad(DotacionCargo paramDotacionCargo)
  {
    if (paramDotacionCargo.jdoFlags <= 0)
      return paramDotacionCargo.cantidad;
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionCargo.cantidad;
    if (localStateManager.isLoaded(paramDotacionCargo, jdoInheritedFieldCount + 0))
      return paramDotacionCargo.cantidad;
    return localStateManager.getIntField(paramDotacionCargo, jdoInheritedFieldCount + 0, paramDotacionCargo.cantidad);
  }

  private static final void jdoSetcantidad(DotacionCargo paramDotacionCargo, int paramInt)
  {
    if (paramDotacionCargo.jdoFlags == 0)
    {
      paramDotacionCargo.cantidad = paramInt;
      return;
    }
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionCargo.cantidad = paramInt;
      return;
    }
    localStateManager.setIntField(paramDotacionCargo, jdoInheritedFieldCount + 0, paramDotacionCargo.cantidad, paramInt);
  }

  private static final Cargo jdoGetcargo(DotacionCargo paramDotacionCargo)
  {
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionCargo.cargo;
    if (localStateManager.isLoaded(paramDotacionCargo, jdoInheritedFieldCount + 1))
      return paramDotacionCargo.cargo;
    return (Cargo)localStateManager.getObjectField(paramDotacionCargo, jdoInheritedFieldCount + 1, paramDotacionCargo.cargo);
  }

  private static final void jdoSetcargo(DotacionCargo paramDotacionCargo, Cargo paramCargo)
  {
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionCargo.cargo = paramCargo;
      return;
    }
    localStateManager.setObjectField(paramDotacionCargo, jdoInheritedFieldCount + 1, paramDotacionCargo.cargo, paramCargo);
  }

  private static final long jdoGetidDotacionCargo(DotacionCargo paramDotacionCargo)
  {
    return paramDotacionCargo.idDotacionCargo;
  }

  private static final void jdoSetidDotacionCargo(DotacionCargo paramDotacionCargo, long paramLong)
  {
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionCargo.idDotacionCargo = paramLong;
      return;
    }
    localStateManager.setLongField(paramDotacionCargo, jdoInheritedFieldCount + 2, paramDotacionCargo.idDotacionCargo, paramLong);
  }

  private static final SubtipoDotacion jdoGetsubtipoDotacion(DotacionCargo paramDotacionCargo)
  {
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionCargo.subtipoDotacion;
    if (localStateManager.isLoaded(paramDotacionCargo, jdoInheritedFieldCount + 3))
      return paramDotacionCargo.subtipoDotacion;
    return (SubtipoDotacion)localStateManager.getObjectField(paramDotacionCargo, jdoInheritedFieldCount + 3, paramDotacionCargo.subtipoDotacion);
  }

  private static final void jdoSetsubtipoDotacion(DotacionCargo paramDotacionCargo, SubtipoDotacion paramSubtipoDotacion)
  {
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionCargo.subtipoDotacion = paramSubtipoDotacion;
      return;
    }
    localStateManager.setObjectField(paramDotacionCargo, jdoInheritedFieldCount + 3, paramDotacionCargo.subtipoDotacion, paramSubtipoDotacion);
  }

  private static final TipoPersonal jdoGettipoPersonal(DotacionCargo paramDotacionCargo)
  {
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionCargo.tipoPersonal;
    if (localStateManager.isLoaded(paramDotacionCargo, jdoInheritedFieldCount + 4))
      return paramDotacionCargo.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramDotacionCargo, jdoInheritedFieldCount + 4, paramDotacionCargo.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(DotacionCargo paramDotacionCargo, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionCargo.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramDotacionCargo, jdoInheritedFieldCount + 4, paramDotacionCargo.tipoPersonal, paramTipoPersonal);
  }

  private static final String jdoGetunidad(DotacionCargo paramDotacionCargo)
  {
    if (paramDotacionCargo.jdoFlags <= 0)
      return paramDotacionCargo.unidad;
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
      return paramDotacionCargo.unidad;
    if (localStateManager.isLoaded(paramDotacionCargo, jdoInheritedFieldCount + 5))
      return paramDotacionCargo.unidad;
    return localStateManager.getStringField(paramDotacionCargo, jdoInheritedFieldCount + 5, paramDotacionCargo.unidad);
  }

  private static final void jdoSetunidad(DotacionCargo paramDotacionCargo, String paramString)
  {
    if (paramDotacionCargo.jdoFlags == 0)
    {
      paramDotacionCargo.unidad = paramString;
      return;
    }
    StateManager localStateManager = paramDotacionCargo.jdoStateManager;
    if (localStateManager == null)
    {
      paramDotacionCargo.unidad = paramString;
      return;
    }
    localStateManager.setStringField(paramDotacionCargo, jdoInheritedFieldCount + 5, paramDotacionCargo.unidad, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}