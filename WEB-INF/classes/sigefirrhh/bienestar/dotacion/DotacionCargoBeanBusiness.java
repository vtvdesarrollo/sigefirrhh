package sigefirrhh.bienestar.dotacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.base.bienestar.SubtipoDotacionBeanBusiness;
import sigefirrhh.base.cargo.Cargo;
import sigefirrhh.base.cargo.CargoBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class DotacionCargoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDotacionCargo(DotacionCargo dotacionCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DotacionCargo dotacionCargoNew = 
      (DotacionCargo)BeanUtils.cloneBean(
      dotacionCargo);

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (dotacionCargoNew.getTipoPersonal() != null) {
      dotacionCargoNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        dotacionCargoNew.getTipoPersonal().getIdTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (dotacionCargoNew.getCargo() != null) {
      dotacionCargoNew.setCargo(
        cargoBeanBusiness.findCargoById(
        dotacionCargoNew.getCargo().getIdCargo()));
    }

    SubtipoDotacionBeanBusiness subtipoDotacionBeanBusiness = new SubtipoDotacionBeanBusiness();

    if (dotacionCargoNew.getSubtipoDotacion() != null) {
      dotacionCargoNew.setSubtipoDotacion(
        subtipoDotacionBeanBusiness.findSubtipoDotacionById(
        dotacionCargoNew.getSubtipoDotacion().getIdSubtipoDotacion()));
    }
    pm.makePersistent(dotacionCargoNew);
  }

  public void updateDotacionCargo(DotacionCargo dotacionCargo) throws Exception
  {
    DotacionCargo dotacionCargoModify = 
      findDotacionCargoById(dotacionCargo.getIdDotacionCargo());

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (dotacionCargo.getTipoPersonal() != null) {
      dotacionCargo.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        dotacionCargo.getTipoPersonal().getIdTipoPersonal()));
    }

    CargoBeanBusiness cargoBeanBusiness = new CargoBeanBusiness();

    if (dotacionCargo.getCargo() != null) {
      dotacionCargo.setCargo(
        cargoBeanBusiness.findCargoById(
        dotacionCargo.getCargo().getIdCargo()));
    }

    SubtipoDotacionBeanBusiness subtipoDotacionBeanBusiness = new SubtipoDotacionBeanBusiness();

    if (dotacionCargo.getSubtipoDotacion() != null) {
      dotacionCargo.setSubtipoDotacion(
        subtipoDotacionBeanBusiness.findSubtipoDotacionById(
        dotacionCargo.getSubtipoDotacion().getIdSubtipoDotacion()));
    }

    BeanUtils.copyProperties(dotacionCargoModify, dotacionCargo);
  }

  public void deleteDotacionCargo(DotacionCargo dotacionCargo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DotacionCargo dotacionCargoDelete = 
      findDotacionCargoById(dotacionCargo.getIdDotacionCargo());
    pm.deletePersistent(dotacionCargoDelete);
  }

  public DotacionCargo findDotacionCargoById(long idDotacionCargo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDotacionCargo == pIdDotacionCargo";
    Query query = pm.newQuery(DotacionCargo.class, filter);

    query.declareParameters("long pIdDotacionCargo");

    parameters.put("pIdDotacionCargo", new Long(idDotacionCargo));

    Collection colDotacionCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDotacionCargo.iterator();
    return (DotacionCargo)iterator.next();
  }

  public Collection findDotacionCargoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent dotacionCargoExtent = pm.getExtent(
      DotacionCargo.class, true);
    Query query = pm.newQuery(dotacionCargoExtent);
    query.setOrdering("cargo.descripcionCargo ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(DotacionCargo.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("cargo.descripcionCargo ascending");

    Collection colDotacionCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDotacionCargo);

    return colDotacionCargo;
  }

  public Collection findByCargo(long idCargo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cargo.idCargo == pIdCargo";

    Query query = pm.newQuery(DotacionCargo.class, filter);

    query.declareParameters("long pIdCargo");
    HashMap parameters = new HashMap();

    parameters.put("pIdCargo", new Long(idCargo));

    query.setOrdering("cargo.descripcionCargo ascending");

    Collection colDotacionCargo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDotacionCargo);

    return colDotacionCargo;
  }
}