package sigefirrhh.bienestar.dotacion;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarFacade;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class GenerarDotacionEntregadaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GenerarDotacionEntregadaForm.class.getName());
  private Collection listTipoPersonal;
  private Collection listTipoDotacion;
  private Collection listSubtipoDotacion;
  private LoginSession login;
  private DefinicionesNoGenFacade definicionesNoGenFacade;
  private BienestarFacade bienestarFacade;
  private DotacionNoGenFacade dotacionFacade;
  private long idTipoDotacion;
  private long idSubtipoDotacion;
  private long idTipoPersonal;
  private String selectIdTipoPersonal;
  private String selectIdTipoDotacion;
  private int anio;
  private int mes;
  private Date fecha;
  private String accion = "A";

  public boolean isShowTipoDotacion()
  {
    return (this.listTipoDotacion != null) && (!this.listTipoDotacion.isEmpty());
  }
  public boolean isShowSubtipoDotacion() {
    return (this.listSubtipoDotacion != null) && (!this.listSubtipoDotacion.isEmpty());
  }

  public GenerarDotacionEntregadaForm() {
    FacesContext context = FacesContext.getCurrentInstance();

    this.definicionesNoGenFacade = new DefinicionesNoGenFacade();
    this.bienestarFacade = new BienestarFacade();
    this.dotacionFacade = new DotacionNoGenFacade();

    this.fecha = Calendar.getInstance().getTime();
    this.anio = Calendar.getInstance().get(1);

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try {
      this.listTipoPersonal = 
        this.definicionesNoGenFacade.findTipoPersonalWithSeguridadAndCriterio(this.login.getIdUsuario(), 
        this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador(), 
        " asignan_dotaciones = 'S' ");
      this.listTipoDotacion = this.bienestarFacade.findAllTipoDotacion();
    }
    catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
      this.listTipoDotacion = new ArrayList();
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoPersonal = String.valueOf(idTipoPersonal);
  }

  public void changeTipoDotacion(ValueChangeEvent event)
  {
    long idTipoDotacion = Long.valueOf(
      (String)event.getNewValue()).longValue();

    this.selectIdTipoDotacion = String.valueOf(idTipoDotacion);
    this.idSubtipoDotacion = 0L;
    this.listSubtipoDotacion = null;
    try
    {
      if (idTipoDotacion > 0L)
      {
        this.listSubtipoDotacion = 
          this.bienestarFacade.findSubtipoDotacionByTipoDotacion(idTipoDotacion);
      }
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String ejecutar()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      if (Long.valueOf(this.selectIdTipoPersonal).longValue() == 0L) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar el tipo de personal", ""));
        return null;
      }
      if (Long.valueOf(this.selectIdTipoDotacion).longValue() == 0L) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar el tipo de dotacion", ""));
        return null;
      }

      if ((this.mes == 0) || (this.mes > 12)) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El mes es incorrecto", ""));
        return null;
      }

      if (this.anio == 0) {
        context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "El año es incorrecto", ""));
        return null;
      }
      this.dotacionFacade.generarDotacionEntregada(Long.valueOf(
        this.selectIdTipoPersonal).longValue(), 
        Long.valueOf(this.selectIdTipoDotacion).longValue(), 
        this.idSubtipoDotacion, this.anio, this.mes, this.fecha, this.accion);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocurrió un error mientras buscaba al trabajador", ""));
      return null;
    }

    context.addMessage("success_add", new FacesMessage("Se realizó con éxito"));
    return null;
  }

  public Collection getListTipoDotacion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoDotacion, "sigefirrhh.base.bienestar.TipoDotacion");
  }

  public Collection getListSubtipoDotacion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listSubtipoDotacion, "sigefirrhh.base.bienestar.SubtipoDotacion");
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }
  public String getIdTipoPersonal() {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public long getIdSubtipoDotacion() {
    return this.idSubtipoDotacion;
  }
  public void setIdSubtipoDotacion(long idSubtipoDotacion) {
    this.idSubtipoDotacion = idSubtipoDotacion;
  }
  public long getIdTipoDotacion() {
    return this.idTipoDotacion;
  }
  public void setIdTipoDotacion(long idTipoDotacion) {
    this.idTipoDotacion = idTipoDotacion;
  }

  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getSelectIdTipoPersonal() {
    return this.selectIdTipoPersonal;
  }
  public void setSelectIdTipoPersonal(String selectIdTipoPersonal) {
    this.selectIdTipoPersonal = selectIdTipoPersonal;
  }
  public String getSelectIdTipoDotacion() {
    return this.selectIdTipoDotacion;
  }
  public void setSelectIdTipoDotacion(String selectIdTipoDotacion) {
    this.selectIdTipoDotacion = selectIdTipoDotacion;
  }
  public String getAccion() {
    return this.accion;
  }
  public void setAccion(String accion) {
    this.accion = accion;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public Date getFecha() {
    return this.fecha;
  }
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }
  public int getMes() {
    return this.mes;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
}