package sigefirrhh.bienestar.dotacion;

import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Date;

public class DotacionNoGenFacade extends DotacionFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DotacionNoGenBusiness dotacionNoGenBusiness = new DotacionNoGenBusiness();

  public void generarDotacionTrabajador(long idTipoPersonal, long idTipoDotacion, long idSubtipoDotacion)
    throws Exception
  {
    this.dotacionNoGenBusiness.generarDotacionTrabajador(idTipoPersonal, idTipoDotacion, idSubtipoDotacion);
  }

  public void generarDotacionEntregada(long idTipoPersonal, long idTipoDotacion, long idSubtipoDotacion, int anio, int mes, Date fecha, String accion)
    throws Exception
  {
    this.dotacionNoGenBusiness.generarDotacionEntregada(idTipoPersonal, idTipoDotacion, 
      idSubtipoDotacion, anio, mes, fecha, accion);
  }
}