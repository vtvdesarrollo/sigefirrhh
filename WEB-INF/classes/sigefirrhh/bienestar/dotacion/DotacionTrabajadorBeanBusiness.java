package sigefirrhh.bienestar.dotacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.SubtipoDotacion;
import sigefirrhh.base.bienestar.SubtipoDotacionBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class DotacionTrabajadorBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDotacionTrabajador(DotacionTrabajador dotacionTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DotacionTrabajador dotacionTrabajadorNew = 
      (DotacionTrabajador)BeanUtils.cloneBean(
      dotacionTrabajador);

    SubtipoDotacionBeanBusiness subTipoDotacionBeanBusiness = new SubtipoDotacionBeanBusiness();

    if (dotacionTrabajadorNew.getSubTipoDotacion() != null) {
      dotacionTrabajadorNew.setSubTipoDotacion(
        subTipoDotacionBeanBusiness.findSubtipoDotacionById(
        dotacionTrabajadorNew.getSubTipoDotacion().getIdSubtipoDotacion()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (dotacionTrabajadorNew.getTrabajador() != null) {
      dotacionTrabajadorNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        dotacionTrabajadorNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(dotacionTrabajadorNew);
  }

  public void updateDotacionTrabajador(DotacionTrabajador dotacionTrabajador) throws Exception
  {
    DotacionTrabajador dotacionTrabajadorModify = 
      findDotacionTrabajadorById(dotacionTrabajador.getIdDotacionTrabajador());

    SubtipoDotacionBeanBusiness subTipoDotacionBeanBusiness = new SubtipoDotacionBeanBusiness();

    if (dotacionTrabajador.getSubTipoDotacion() != null) {
      dotacionTrabajador.setSubTipoDotacion(
        subTipoDotacionBeanBusiness.findSubtipoDotacionById(
        dotacionTrabajador.getSubTipoDotacion().getIdSubtipoDotacion()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (dotacionTrabajador.getTrabajador() != null) {
      dotacionTrabajador.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        dotacionTrabajador.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(dotacionTrabajadorModify, dotacionTrabajador);
  }

  public void deleteDotacionTrabajador(DotacionTrabajador dotacionTrabajador) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DotacionTrabajador dotacionTrabajadorDelete = 
      findDotacionTrabajadorById(dotacionTrabajador.getIdDotacionTrabajador());
    pm.deletePersistent(dotacionTrabajadorDelete);
  }

  public DotacionTrabajador findDotacionTrabajadorById(long idDotacionTrabajador) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDotacionTrabajador == pIdDotacionTrabajador";
    Query query = pm.newQuery(DotacionTrabajador.class, filter);

    query.declareParameters("long pIdDotacionTrabajador");

    parameters.put("pIdDotacionTrabajador", new Long(idDotacionTrabajador));

    Collection colDotacionTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDotacionTrabajador.iterator();
    return (DotacionTrabajador)iterator.next();
  }

  public Collection findDotacionTrabajadorAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent dotacionTrabajadorExtent = pm.getExtent(
      DotacionTrabajador.class, true);
    Query query = pm.newQuery(dotacionTrabajadorExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(DotacionTrabajador.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colDotacionTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDotacionTrabajador);

    return colDotacionTrabajador;
  }
}