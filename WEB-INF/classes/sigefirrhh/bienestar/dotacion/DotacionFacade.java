package sigefirrhh.bienestar.dotacion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class DotacionFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private DotacionBusiness dotacionBusiness = new DotacionBusiness();

  public void addDotacionCargo(DotacionCargo dotacionCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.dotacionBusiness.addDotacionCargo(dotacionCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDotacionCargo(DotacionCargo dotacionCargo) throws Exception
  {
    try { this.txn.open();
      this.dotacionBusiness.updateDotacionCargo(dotacionCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDotacionCargo(DotacionCargo dotacionCargo) throws Exception
  {
    try { this.txn.open();
      this.dotacionBusiness.deleteDotacionCargo(dotacionCargo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DotacionCargo findDotacionCargoById(long dotacionCargoId) throws Exception
  {
    try { this.txn.open();
      DotacionCargo dotacionCargo = 
        this.dotacionBusiness.findDotacionCargoById(dotacionCargoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(dotacionCargo);
      return dotacionCargo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDotacionCargo() throws Exception
  {
    try { this.txn.open();
      return this.dotacionBusiness.findAllDotacionCargo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDotacionCargoByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.dotacionBusiness.findDotacionCargoByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDotacionCargoByCargo(long idCargo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.dotacionBusiness.findDotacionCargoByCargo(idCargo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDotacionEntregada(DotacionEntregada dotacionEntregada)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.dotacionBusiness.addDotacionEntregada(dotacionEntregada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDotacionEntregada(DotacionEntregada dotacionEntregada) throws Exception
  {
    try { this.txn.open();
      this.dotacionBusiness.updateDotacionEntregada(dotacionEntregada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDotacionEntregada(DotacionEntregada dotacionEntregada) throws Exception
  {
    try { this.txn.open();
      this.dotacionBusiness.deleteDotacionEntregada(dotacionEntregada);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DotacionEntregada findDotacionEntregadaById(long dotacionEntregadaId) throws Exception
  {
    try { this.txn.open();
      DotacionEntregada dotacionEntregada = 
        this.dotacionBusiness.findDotacionEntregadaById(dotacionEntregadaId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(dotacionEntregada);
      return dotacionEntregada;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDotacionEntregada() throws Exception
  {
    try { this.txn.open();
      return this.dotacionBusiness.findAllDotacionEntregada();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDotacionEntregadaByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.dotacionBusiness.findDotacionEntregadaByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDotacionTrabajador(DotacionTrabajador dotacionTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.dotacionBusiness.addDotacionTrabajador(dotacionTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDotacionTrabajador(DotacionTrabajador dotacionTrabajador) throws Exception
  {
    try { this.txn.open();
      this.dotacionBusiness.updateDotacionTrabajador(dotacionTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDotacionTrabajador(DotacionTrabajador dotacionTrabajador) throws Exception
  {
    try { this.txn.open();
      this.dotacionBusiness.deleteDotacionTrabajador(dotacionTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DotacionTrabajador findDotacionTrabajadorById(long dotacionTrabajadorId) throws Exception
  {
    try { this.txn.open();
      DotacionTrabajador dotacionTrabajador = 
        this.dotacionBusiness.findDotacionTrabajadorById(dotacionTrabajadorId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(dotacionTrabajador);
      return dotacionTrabajador;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDotacionTrabajador() throws Exception
  {
    try { this.txn.open();
      return this.dotacionBusiness.findAllDotacionTrabajador();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDotacionTrabajadorByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.dotacionBusiness.findDotacionTrabajadorByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}