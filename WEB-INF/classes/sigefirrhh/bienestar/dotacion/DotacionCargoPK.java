package sigefirrhh.bienestar.dotacion;

import java.io.Serializable;

public class DotacionCargoPK
  implements Serializable
{
  public long idDotacionCargo;

  public DotacionCargoPK()
  {
  }

  public DotacionCargoPK(long idDotacionCargo)
  {
    this.idDotacionCargo = idDotacionCargo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DotacionCargoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DotacionCargoPK thatPK)
  {
    return 
      this.idDotacionCargo == thatPK.idDotacionCargo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDotacionCargo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDotacionCargo);
  }
}