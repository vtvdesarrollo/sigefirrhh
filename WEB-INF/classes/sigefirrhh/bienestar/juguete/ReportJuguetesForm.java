package sigefirrhh.bienestar.juguete;

import eforserver.presentation.Form;
import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.EstructuraNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportJuguetesForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportJuguetesForm.class.getName());
  private int reportId;
  private long idTipoPersonal;
  private long idRegion;
  private String formato = "1";
  private String reportName;
  private Collection listTipoPersonal;
  private Collection listRegion;
  private DefinicionesNoGenFacade definicionesFacade;
  private EstructuraNoGenFacade estructuraFacade;
  private LoginSession login;
  private String tipo = "det";

  public ReportJuguetesForm() { FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.estructuraFacade = new EstructuraNoGenFacade();
    this.reportName = "juguetesdet";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportJuguetesForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
      this.listRegion = this.estructuraFacade.findAllRegion();
    }
    catch (Exception e)
    {
      this.listTipoPersonal = new ArrayList();
      this.listRegion = new ArrayList();
    }
  }

  public Collection getListRegion() {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listRegion, "sigefirrhh.base.estructura.Region");
  }
  public void cambiarNombreAReporte() {
    try {
      this.reportName = "juguetes";
      this.reportName += this.tipo;
      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }
      if (this.formato.equals("2"))
        this.reportName = ("a_" + this.reportName);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "juguetes";
      this.reportName += this.tipo;
      if (this.idRegion != 0L) {
        this.reportName += "reg";
      }
      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }

      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_tipo_personal", new Long(this.idTipoPersonal));
      if (this.idRegion != 0L) {
        parameters.put("id_region", new Long(this.idRegion));
      }

      JasperForWeb report = new JasperForWeb();
      if (this.formato.equals("2")) {
        report.setType(3);
      }
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/bienestar/juguete");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
  public long getIdTipoPersonal() {
    return this.idTipoPersonal;
  }
  public void setIdTipoPersonal(long idTipoPersonal) {
    this.idTipoPersonal = idTipoPersonal;
  }
  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }
  public long getIdRegion() {
    return this.idRegion;
  }
  public void setIdRegion(long idRegion) {
    this.idRegion = idRegion;
  }
  public String getTipo() {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
}