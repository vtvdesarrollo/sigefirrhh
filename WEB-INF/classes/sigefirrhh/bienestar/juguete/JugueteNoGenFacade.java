package sigefirrhh.bienestar.juguete;

import eforserver.business.AbstractFacade;
import java.util.Date;

public class JugueteNoGenFacade extends AbstractFacade
{
  private JugueteNoGenBusiness jugueteNoGenBusiness = new JugueteNoGenBusiness();

  public void asignarJuguetes(long idTipoPersonal, Date fecha, String proceso) throws Exception {
    this.jugueteNoGenBusiness.asignarJuguetes(idTipoPersonal, fecha, proceso);
  }
}