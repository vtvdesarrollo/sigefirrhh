package sigefirrhh.bienestar.juguete;

import eforserver.business.AbstractBusiness;
import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.apache.log4j.Logger;

public class JugueteNoGenBusiness extends AbstractBusiness
{
  Logger log = Logger.getLogger(JugueteNoGenBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public void asignarJuguetes(long idTipoPersonal, java.util.Date fecha, String proceso) throws Exception
  {
    Connection connection = null;
    PreparedStatement st = null;

    java.sql.Date fechaSql = new java.sql.Date(fecha.getYear(), fecha.getMonth(), fecha.getDate());

    StringBuffer sql = new StringBuffer();
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      sql.append("select asignar_juguetes(?,?,?)");

      this.log.error("1- " + idTipoPersonal);
      this.log.error("2- " + fecha);
      this.log.error("3- " + proceso);

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idTipoPersonal);
      st.setDate(2, fechaSql);
      st.setString(3, proceso);

      st.executeQuery();

      connection.commit();

      this.log.error("ejecutó el proceso");
    } finally {
      if (st != null) try {
          st.close();
        } catch (Exception localException) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException1)
        {
        }
    }
  }
}