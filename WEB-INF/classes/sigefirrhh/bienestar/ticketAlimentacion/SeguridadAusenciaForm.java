package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.common.Resource;
import eforserver.presentation.Form;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.login.LoginSession;

public class SeguridadAusenciaForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeguridadAusenciaForm.class.getName());
  private String selectGrupoTicket;
  private long idGrupoTicket;
  private Collection listGrupoTicket;
  private SeguridadAusencia seguridadAusencia = new SeguridadAusencia();
  private long idTipoPersonal;
  private LoginSession login;
  private int anio = 0;
  private int mes = 0;
  private int proceso = 0;
  private boolean auxShow = false;
  private DefinicionesNoGenFacade definicionesFacade;
  private TicketAlimentacionFacadeExtend ticketAlimentacion = new TicketAlimentacionFacadeExtend();
  private TicketAlimentacionFacadeExtend ticketAlimentacionFacade = new TicketAlimentacionFacadeExtend();
  private boolean showSeguridadAusenciaByGrupoTicket;

  public SeguridadAusenciaForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();

    this.selectGrupoTicket = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    refresh();
  }

  public void refresh()
  {
    try
    {
      this.listGrupoTicket = 
        this.ticketAlimentacion.findGrupoTicketWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void generar() {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      Connection connection = Resource.getConnection();
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      Statement stActualizar = null;
      stActualizar = connection.createStatement();
      if (this.proceso == 2) {
        if (this.seguridadAusencia.getMes() == 1) {
          this.anio -= 1;
          this.mes = 12;
        } else {
          this.mes = (this.seguridadAusencia.getMes() - 1);
        }
      }
      stActualizar.execute("update seguridadausencia set mes = " + this.mes + ", anio = " + this.anio + " where id_grupo_ticket = " + this.idGrupoTicket);
      context.addMessage("success_process", new FacesMessage("Se realizó con éxito"));

      refresh();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
  }

  public long getIdTipoPersonal() { return this.idTipoPersonal; }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }

  public int getAnio() {
    return this.anio;
  }

  public void setAnio(int anio)
  {
    this.anio = anio;
  }

  public int getMes() {
    return this.mes;
  }

  public void setMes(int mes) {
    this.mes = mes;
  }

  public int getProceso()
  {
    return this.proceso;
  }

  public void setProceso(int proceso)
  {
    this.proceso = proceso;
  }
  public void changeGrupoTicket(ValueChangeEvent event) {
    this.auxShow = false;
    this.idGrupoTicket = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.seguridadAusencia = this.ticketAlimentacionFacade.findLastSeguridadAusencia(this.idGrupoTicket);
      if (this.seguridadAusencia != null) {
        if (this.proceso == 1) {
          if (this.seguridadAusencia.getMes() == 12) {
            this.anio = (this.seguridadAusencia.getAnio() + 1);
            this.mes = 1;
          } else {
            this.mes = (this.seguridadAusencia.getMes() + 1);
            this.anio = this.seguridadAusencia.getAnio();
          }
        }
        if (this.proceso == 2) {
          this.mes = this.seguridadAusencia.getMes();
          this.anio = this.seguridadAusencia.getAnio();
        }
        this.auxShow = true;
      }
      else {
        this.auxShow = false;
      }
    }
    catch (Exception e) {
      this.auxShow = false;
      log.error("Excepcion controlada:", e);
    }
  }

  public Collection getListGrupoTicket() { Collection col = new ArrayList();
    Iterator iterator = this.listGrupoTicket.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col; }

  public String getSelectGrupoTicket()
  {
    return this.selectGrupoTicket;
  }

  public void setSelectGrupoTicket(String string) {
    this.selectGrupoTicket = string;
    this.idGrupoTicket = Integer.parseInt(string);
  }
  public boolean isShow() {
    return this.auxShow;
  }
}