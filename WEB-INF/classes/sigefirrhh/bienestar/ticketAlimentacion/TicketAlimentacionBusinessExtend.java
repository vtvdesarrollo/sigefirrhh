package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.BienestarBusiness;

public class TicketAlimentacionBusinessExtend extends TicketAlimentacionBusiness
{
  Logger log = Logger.getLogger(TicketAlimentacionBusinessExtend.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private BienestarBusiness bienestarBusiness = new BienestarBusiness();

  public void setSeguridadAusencia() {
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      StringBuffer sql = new StringBuffer();

      sql.append("select anio, mes  ");
      sql.append(" from seguridadausencia");

      stRegistros = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      rsRegistros = stRegistros.executeQuery();
    }
    catch (Exception e)
    {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException1) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException2) {
        } if (connection != null) try {
          connection.close(); connection = null; } catch (Exception localException3) {
        } 
    }
  }

  public void asignarTickets(long idGrupoTicket, int anio, int mes, int proceso, String especial, String usuario) { Connection connection = null;
    ResultSet rs = null;
    PreparedStatement st = null;
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);

      java.sql.Date fechaSql = new java.sql.Date(new java.util.Date().getYear(), new java.util.Date().getMonth(), new java.util.Date().getDate());

      this.log.error("1-idGrupoTicket " + idGrupoTicket);
      this.log.error("2-mes " + mes);
      this.log.error("3-anio " + anio);
      this.log.error("4-proceso " + proceso);
      this.log.error("5-especial " + especial);
      this.log.error("5-fecha " + fechaSql);
      this.log.error("6-usuario " + usuario);

      StringBuffer sql = new StringBuffer();
      sql.append("select asignar_cestaticket(?,?,?,?,?,?,?)");

      st = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);
      st.setLong(1, idGrupoTicket);
      st.setInt(2, mes);
      st.setInt(3, anio);
      st.setInt(4, proceso);
      st.setString(5, especial);
      st.setDate(6, fechaSql);
      st.setString(7, usuario);

      rs = st.executeQuery();
      connection.commit();
      this.log.error("ejecutó SP aisgnar_cestaticket");
    }
    catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
    }
    finally {
      if (rs != null) try {
          rs.close();
        } catch (Exception localException1) {
        } if (st != null) try {
          st.close();
        } catch (Exception localException2) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException3)
        {
        } 
    } } 
  public Collection findGrupoTicketWithSeguridad(long idUsuario, long idOrganismo, String administrador) throws Exception
  {
    Collection col = new ArrayList();
    Connection connection = null;
    ResultSet rsRegistros = null;
    PreparedStatement stRegistros = null;
    StringBuffer sql = new StringBuffer();

    this.log.error("id_usuario" + idUsuario);
    this.log.error("id_organismo" + idOrganismo);
    this.log.error("administrador" + administrador);
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);

      if (administrador.equals("N")) {
        sql.append("select gt.id_grupo_ticket as id, (gt.nombre || ' ' || gt.cod_grupo_ticket)  as descripcion  ");
        sql.append(" from grupoticket gt");
        sql.append(" where gt.id_grupo_ticket in (");
        sql.append(" select tp.id_grupo_ticket ");
        sql.append(" from tipopersonal tp, usuariotipopersonal utp");
        sql.append(" where utp.id_usuario = ?");
        sql.append(" and tp.id_organismo = ?");
        sql.append(" and utp.id_tipo_personal = tp.id_tipo_personal)");
        sql.append(" order by gt.cod_grupo_ticket");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idUsuario);
        stRegistros.setLong(2, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      } else {
        sql.append("select gt.id_grupo_ticket as id, (gt.nombre || ' ' || gt.cod_grupo_ticket)  as descripcion  ");
        sql.append(" from grupoticket gt");
        sql.append(" where ");
        sql.append(" gt.id_organismo = ?");
        sql.append(" order by gt.cod_grupo_ticket");
        stRegistros = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stRegistros.setLong(1, idOrganismo);
        rsRegistros = stRegistros.executeQuery();
      }

      while (rsRegistros.next())
      {
        col.add(Long.valueOf(String.valueOf(rsRegistros.getLong("id"))));
        col.add(rsRegistros.getString("descripcion"));
      }

      return col;
    } finally {
      if (rsRegistros != null) try {
          rsRegistros.close();
        } catch (Exception localException3) {
        } if (stRegistros != null) try {
          stRegistros.close();
        } catch (Exception localException4) {
        } if (connection != null) try {
          connection.close(); connection = null;
        }
        catch (Exception localException5) {
        } 
    }
  }

  public SeguridadTicket findLastSeguridadTicket(long idGrupoTicket) throws Exception { PersistenceManager pm = PMThread.getPM();

    String filter = "grupoTicket.idGrupoTicket == pIdGrupoTicket";

    Query query = pm.newQuery(SeguridadTicket.class, filter);

    query.declareParameters("long pIdGrupoTicket");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoTicket", new Long(idGrupoTicket));

    query.setOrdering("anio descending, mes descending");

    Collection colSeguridad = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridad);
    Iterator iterSeguridad = colSeguridad.iterator();
    if (iterSeguridad.hasNext())
      return (SeguridadTicket)iterSeguridad.next();
    return null;
  }

  public SeguridadAusencia findLastSeguridadAusencia(long idGrupoTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoTicket.idGrupoTicket == pIdGrupoTicket";

    Query query = pm.newQuery(SeguridadAusencia.class, filter);

    query.declareParameters("long pIdGrupoTicket");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoTicket", new Long(idGrupoTicket));

    query.setOrdering("anio descending, mes descending");

    Collection colSeguridadAusencia = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadAusencia);
    Iterator iterSeguridadAusencia = colSeguridadAusencia.iterator();
    if (iterSeguridadAusencia.hasNext())
      return (SeguridadAusencia)iterSeguridadAusencia.next();
    this.log.error("Advertencia: No se encontro SeguridadAusencia para idGrupoTicket = " + idGrupoTicket);
    return null;
  }

  public Collection findRetroactivoByTrabajadorAndPagado(long idTrabajador, String pagado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && pagado == pPagado";

    Query query = pm.newQuery(RetroactivoTicket.class, filter);

    query.declareParameters("long pIdTrabajador, String pPagado");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pPagado", pagado);

    query.setOrdering("mes ascending");

    Collection colRetroactivoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRetroactivoTicket);

    return colRetroactivoTicket;
  }

  public Collection findDescuentoTicketByTrabajadorAndPagado(long idTrabajador, String pagado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador && pagado == pPagado";

    Query query = pm.newQuery(DescuentoTicket.class, filter);

    query.declareParameters("long pIdTrabajador, String pPagado");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));
    parameters.put("pPagado", pagado);

    query.setOrdering("mes ascending");

    Collection colDescuentoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDescuentoTicket);

    return colDescuentoTicket;
  }
}