package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class ProveedorRegionPK
  implements Serializable
{
  public long idProveedorRegion;

  public ProveedorRegionPK()
  {
  }

  public ProveedorRegionPK(long idProveedorRegion)
  {
    this.idProveedorRegion = idProveedorRegion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProveedorRegionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProveedorRegionPK thatPK)
  {
    return 
      this.idProveedorRegion == thatPK.idProveedorRegion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProveedorRegion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProveedorRegion);
  }
}