package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class TicketAlimentacionBusiness extends AbstractBusiness
  implements Serializable
{
  private ExcepcionTicketBeanBusiness excepcionTicketBeanBusiness = new ExcepcionTicketBeanBusiness();

  private HistoricoTicketBeanBusiness historicoTicketBeanBusiness = new HistoricoTicketBeanBusiness();

  private DescuentoTicketBeanBusiness descuentoTicketBeanBusiness = new DescuentoTicketBeanBusiness();

  private SeguridadTicketBeanBusiness seguridadTicketBeanBusiness = new SeguridadTicketBeanBusiness();

  private RetroactivoTicketBeanBusiness retroactivoTicketBeanBusiness = new RetroactivoTicketBeanBusiness();

  private GrupoTicketBeanBusiness grupoTicketBeanBusiness = new GrupoTicketBeanBusiness();

  public void addExcepcionTicket(ExcepcionTicket excepcionTicket)
    throws Exception
  {
    this.excepcionTicketBeanBusiness.addExcepcionTicket(excepcionTicket);
  }

  public void updateExcepcionTicket(ExcepcionTicket excepcionTicket) throws Exception {
    this.excepcionTicketBeanBusiness.updateExcepcionTicket(excepcionTicket);
  }

  public void deleteExcepcionTicket(ExcepcionTicket excepcionTicket) throws Exception {
    this.excepcionTicketBeanBusiness.deleteExcepcionTicket(excepcionTicket);
  }

  public ExcepcionTicket findExcepcionTicketById(long excepcionTicketId) throws Exception {
    return this.excepcionTicketBeanBusiness.findExcepcionTicketById(excepcionTicketId);
  }

  public Collection findAllExcepcionTicket() throws Exception {
    return this.excepcionTicketBeanBusiness.findExcepcionTicketAll();
  }

  public Collection findExcepcionTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.excepcionTicketBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addHistoricoTicket(HistoricoTicket historicoTicket)
    throws Exception
  {
    this.historicoTicketBeanBusiness.addHistoricoTicket(historicoTicket);
  }

  public void updateHistoricoTicket(HistoricoTicket historicoTicket) throws Exception {
    this.historicoTicketBeanBusiness.updateHistoricoTicket(historicoTicket);
  }

  public void deleteHistoricoTicket(HistoricoTicket historicoTicket) throws Exception {
    this.historicoTicketBeanBusiness.deleteHistoricoTicket(historicoTicket);
  }

  public HistoricoTicket findHistoricoTicketById(long historicoTicketId) throws Exception {
    return this.historicoTicketBeanBusiness.findHistoricoTicketById(historicoTicketId);
  }

  public Collection findAllHistoricoTicket() throws Exception {
    return this.historicoTicketBeanBusiness.findHistoricoTicketAll();
  }

  public Collection findHistoricoTicketByAnio(int anio)
    throws Exception
  {
    return this.historicoTicketBeanBusiness.findByAnio(anio);
  }

  public Collection findHistoricoTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.historicoTicketBeanBusiness.findByTrabajador(idTrabajador);
  }

  public Collection findHistoricoTicketByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.historicoTicketBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public void addDescuentoTicket(DescuentoTicket descuentoTicket)
    throws Exception
  {
    this.descuentoTicketBeanBusiness.addDescuentoTicket(descuentoTicket);
  }

  public void updateDescuentoTicket(DescuentoTicket descuentoTicket) throws Exception {
    this.descuentoTicketBeanBusiness.updateDescuentoTicket(descuentoTicket);
  }

  public void deleteDescuentoTicket(DescuentoTicket descuentoTicket) throws Exception {
    this.descuentoTicketBeanBusiness.deleteDescuentoTicket(descuentoTicket);
  }

  public DescuentoTicket findDescuentoTicketById(long descuentoTicketId) throws Exception {
    return this.descuentoTicketBeanBusiness.findDescuentoTicketById(descuentoTicketId);
  }

  public Collection findAllDescuentoTicket() throws Exception {
    return this.descuentoTicketBeanBusiness.findDescuentoTicketAll();
  }

  public Collection findDescuentoTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.descuentoTicketBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addSeguridadTicket(SeguridadTicket seguridadTicket)
    throws Exception
  {
    this.seguridadTicketBeanBusiness.addSeguridadTicket(seguridadTicket);
  }

  public void updateSeguridadTicket(SeguridadTicket seguridadTicket) throws Exception {
    this.seguridadTicketBeanBusiness.updateSeguridadTicket(seguridadTicket);
  }

  public void deleteSeguridadTicket(SeguridadTicket seguridadTicket) throws Exception {
    this.seguridadTicketBeanBusiness.deleteSeguridadTicket(seguridadTicket);
  }

  public SeguridadTicket findSeguridadTicketById(long seguridadTicketId) throws Exception {
    return this.seguridadTicketBeanBusiness.findSeguridadTicketById(seguridadTicketId);
  }

  public Collection findAllSeguridadTicket() throws Exception {
    return this.seguridadTicketBeanBusiness.findSeguridadTicketAll();
  }

  public Collection findSeguridadTicketByGrupoTicket(long idGrupoTicket)
    throws Exception
  {
    return this.seguridadTicketBeanBusiness.findByGrupoTicket(idGrupoTicket);
  }

  public void addRetroactivoTicket(RetroactivoTicket retroactivoTicket)
    throws Exception
  {
    this.retroactivoTicketBeanBusiness.addRetroactivoTicket(retroactivoTicket);
  }

  public void updateRetroactivoTicket(RetroactivoTicket retroactivoTicket) throws Exception {
    this.retroactivoTicketBeanBusiness.updateRetroactivoTicket(retroactivoTicket);
  }

  public void deleteRetroactivoTicket(RetroactivoTicket retroactivoTicket) throws Exception {
    this.retroactivoTicketBeanBusiness.deleteRetroactivoTicket(retroactivoTicket);
  }

  public RetroactivoTicket findRetroactivoTicketById(long retroactivoTicketId) throws Exception {
    return this.retroactivoTicketBeanBusiness.findRetroactivoTicketById(retroactivoTicketId);
  }

  public Collection findAllRetroactivoTicket() throws Exception {
    return this.retroactivoTicketBeanBusiness.findRetroactivoTicketAll();
  }

  public Collection findRetroactivoTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    return this.retroactivoTicketBeanBusiness.findByTrabajador(idTrabajador);
  }

  public void addGrupoTicket(GrupoTicket grupoTicket)
    throws Exception
  {
    this.grupoTicketBeanBusiness.addGrupoTicket(grupoTicket);
  }

  public void updateGrupoTicket(GrupoTicket grupoTicket) throws Exception {
    this.grupoTicketBeanBusiness.updateGrupoTicket(grupoTicket);
  }

  public void deleteGrupoTicket(GrupoTicket grupoTicket) throws Exception {
    this.grupoTicketBeanBusiness.deleteGrupoTicket(grupoTicket);
  }

  public GrupoTicket findGrupoTicketById(long grupoTicketId) throws Exception {
    return this.grupoTicketBeanBusiness.findGrupoTicketById(grupoTicketId);
  }

  public Collection findAllGrupoTicket() throws Exception {
    return this.grupoTicketBeanBusiness.findGrupoTicketAll();
  }

  public Collection findGrupoTicketByCodGrupoTicket(int codGrupoTicket, long idOrganismo)
    throws Exception
  {
    return this.grupoTicketBeanBusiness.findByCodGrupoTicket(codGrupoTicket, idOrganismo);
  }

  public Collection findGrupoTicketByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    return this.grupoTicketBeanBusiness.findByNombre(nombre, idOrganismo);
  }

  public Collection findGrupoTicketByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.grupoTicketBeanBusiness.findByOrganismo(idOrganismo);
  }
}