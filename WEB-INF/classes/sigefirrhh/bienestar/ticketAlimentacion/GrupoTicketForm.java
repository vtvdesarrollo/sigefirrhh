package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class GrupoTicketForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(GrupoTicketForm.class.getName());
  private GrupoTicket grupoTicket;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private int reportId;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private TicketAlimentacionFacade ticketAlimentacionFacade = new TicketAlimentacionFacade();
  private boolean showGrupoTicketByCodGrupoTicket;
  private boolean showGrupoTicketByNombre;
  private int findCodGrupoTicket;
  private String findNombre;
  private Object stateResultGrupoTicketByCodGrupoTicket = null;

  private Object stateResultGrupoTicketByNombre = null;

  public int getFindCodGrupoTicket()
  {
    return this.findCodGrupoTicket;
  }
  public void setFindCodGrupoTicket(int findCodGrupoTicket) {
    this.findCodGrupoTicket = findCodGrupoTicket;
  }
  public String getFindNombre() {
    return this.findNombre;
  }
  public void setFindNombre(String findNombre) {
    this.findNombre = findNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public GrupoTicket getGrupoTicket() {
    if (this.grupoTicket == null) {
      this.grupoTicket = new GrupoTicket();
    }
    return this.grupoTicket;
  }

  public GrupoTicketForm() throws Exception
  {
    newReportId();

    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findGrupoTicketByCodGrupoTicket()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.ticketAlimentacionFacade.findGrupoTicketByCodGrupoTicket(this.findCodGrupoTicket, idOrganismo);
      this.showGrupoTicketByCodGrupoTicket = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoTicketByCodGrupoTicket)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoTicket = 0;
    this.findNombre = null;

    return null;
  }

  public String findGrupoTicketByNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.ticketAlimentacionFacade.findGrupoTicketByNombre(this.findNombre, idOrganismo);
      this.showGrupoTicketByNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showGrupoTicketByNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCodGrupoTicket = 0;
    this.findNombre = null;

    return null;
  }

  public boolean isShowGrupoTicketByCodGrupoTicket() {
    return this.showGrupoTicketByCodGrupoTicket;
  }
  public boolean isShowGrupoTicketByNombre() {
    return this.showGrupoTicketByNombre;
  }

  public String selectGrupoTicket()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idGrupoTicket = 
      Long.parseLong((String)requestParameterMap.get("idGrupoTicket"));
    try
    {
      this.grupoTicket = 
        this.ticketAlimentacionFacade.findGrupoTicketById(
        idGrupoTicket);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.grupoTicket = null;
    this.showGrupoTicketByCodGrupoTicket = false;
    this.showGrupoTicketByNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ticketAlimentacionFacade.addGrupoTicket(
          this.grupoTicket);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ticketAlimentacionFacade.updateGrupoTicket(
          this.grupoTicket);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ticketAlimentacionFacade.deleteGrupoTicket(
        this.grupoTicket);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.grupoTicket = new GrupoTicket();

    this.grupoTicket.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.grupoTicket.setIdGrupoTicket(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.grupoTicket = new GrupoTicket();
    return "cancel";
  }

  public String runReport()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", 
      this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", 
      ((ServletContext)context.getExternalContext().getContext()).getRealPath(
      this.login.getURLLogo()));
    parameters.put("id_organismo", new Long(this.login.getOrganismo().getIdOrganismo()));
    JasperForWeb report = new JasperForWeb();
    report.setParameters(parameters);

    report.setReportName("GrupoTicket");
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + 
      "/reports/sigefirrhh/bienestar/ticketAlimentacion");
    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(
      "GrupoTicket" + this.reportId, report);
    newReportId();
    return null;
  }

  private void newReportId() {
    int id = 0;
    Random r = new Random();
    do
      id = Math.abs(r.nextInt(1000));
    while (id == this.reportId);
    this.reportId = id;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }

  public boolean isAdding() {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}