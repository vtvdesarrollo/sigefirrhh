package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class SeguridadTicketPK
  implements Serializable
{
  public long idSeguridadTicket;

  public SeguridadTicketPK()
  {
  }

  public SeguridadTicketPK(long idSeguridadTicket)
  {
    this.idSeguridadTicket = idSeguridadTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadTicketPK thatPK)
  {
    return 
      this.idSeguridadTicket == thatPK.idSeguridadTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadTicket);
  }
}