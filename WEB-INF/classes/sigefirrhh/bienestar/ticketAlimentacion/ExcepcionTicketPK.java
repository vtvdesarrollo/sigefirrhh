package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class ExcepcionTicketPK
  implements Serializable
{
  public long idExcepcionTicket;

  public ExcepcionTicketPK()
  {
  }

  public ExcepcionTicketPK(long idExcepcionTicket)
  {
    this.idExcepcionTicket = idExcepcionTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ExcepcionTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ExcepcionTicketPK thatPK)
  {
    return 
      this.idExcepcionTicket == thatPK.idExcepcionTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idExcepcionTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idExcepcionTicket);
  }
}