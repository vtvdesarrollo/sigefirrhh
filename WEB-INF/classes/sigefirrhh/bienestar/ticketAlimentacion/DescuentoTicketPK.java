package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class DescuentoTicketPK
  implements Serializable
{
  public long idDescuentoTicket;

  public DescuentoTicketPK()
  {
  }

  public DescuentoTicketPK(long idDescuentoTicket)
  {
    this.idDescuentoTicket = idDescuentoTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((DescuentoTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(DescuentoTicketPK thatPK)
  {
    return 
      this.idDescuentoTicket == thatPK.idDescuentoTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idDescuentoTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idDescuentoTicket);
  }
}