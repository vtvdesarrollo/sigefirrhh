package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.ProveedorTicket;
import sigefirrhh.base.bienestar.ProveedorTicketBeanBusiness;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.base.estructura.DependenciaBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class HistoricoTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addHistoricoTicket(HistoricoTicket historicoTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    HistoricoTicket historicoTicketNew = 
      (HistoricoTicket)BeanUtils.cloneBean(
      historicoTicket);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoTicketNew.getTrabajador() != null) {
      historicoTicketNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoTicketNew.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoTicketNew.getTipoPersonal() != null) {
      historicoTicketNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoTicketNew.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (historicoTicketNew.getConceptoTipoPersonal() != null) {
      historicoTicketNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        historicoTicketNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    ProveedorTicketBeanBusiness proveedorTicketBeanBusiness = new ProveedorTicketBeanBusiness();

    if (historicoTicketNew.getProveedorTicket() != null) {
      historicoTicketNew.setProveedorTicket(
        proveedorTicketBeanBusiness.findProveedorTicketById(
        historicoTicketNew.getProveedorTicket().getIdProveedorTicket()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (historicoTicketNew.getDependencia() != null) {
      historicoTicketNew.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        historicoTicketNew.getDependencia().getIdDependencia()));
    }
    pm.makePersistent(historicoTicketNew);
  }

  public void updateHistoricoTicket(HistoricoTicket historicoTicket) throws Exception
  {
    HistoricoTicket historicoTicketModify = 
      findHistoricoTicketById(historicoTicket.getIdHistoricoTicket());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (historicoTicket.getTrabajador() != null) {
      historicoTicket.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        historicoTicket.getTrabajador().getIdTrabajador()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (historicoTicket.getTipoPersonal() != null) {
      historicoTicket.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        historicoTicket.getTipoPersonal().getIdTipoPersonal()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (historicoTicket.getConceptoTipoPersonal() != null) {
      historicoTicket.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        historicoTicket.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    ProveedorTicketBeanBusiness proveedorTicketBeanBusiness = new ProveedorTicketBeanBusiness();

    if (historicoTicket.getProveedorTicket() != null) {
      historicoTicket.setProveedorTicket(
        proveedorTicketBeanBusiness.findProveedorTicketById(
        historicoTicket.getProveedorTicket().getIdProveedorTicket()));
    }

    DependenciaBeanBusiness dependenciaBeanBusiness = new DependenciaBeanBusiness();

    if (historicoTicket.getDependencia() != null) {
      historicoTicket.setDependencia(
        dependenciaBeanBusiness.findDependenciaById(
        historicoTicket.getDependencia().getIdDependencia()));
    }

    BeanUtils.copyProperties(historicoTicketModify, historicoTicket);
  }

  public void deleteHistoricoTicket(HistoricoTicket historicoTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    HistoricoTicket historicoTicketDelete = 
      findHistoricoTicketById(historicoTicket.getIdHistoricoTicket());
    pm.deletePersistent(historicoTicketDelete);
  }

  public HistoricoTicket findHistoricoTicketById(long idHistoricoTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idHistoricoTicket == pIdHistoricoTicket";
    Query query = pm.newQuery(HistoricoTicket.class, filter);

    query.declareParameters("long pIdHistoricoTicket");

    parameters.put("pIdHistoricoTicket", new Long(idHistoricoTicket));

    Collection colHistoricoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colHistoricoTicket.iterator();
    return (HistoricoTicket)iterator.next();
  }

  public Collection findHistoricoTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent historicoTicketExtent = pm.getExtent(
      HistoricoTicket.class, true);
    Query query = pm.newQuery(historicoTicketExtent);
    query.setOrdering("anio ascending, mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByAnio(int anio)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "anio == pAnio";

    Query query = pm.newQuery(HistoricoTicket.class, filter);

    query.declareParameters("int pAnio");
    HashMap parameters = new HashMap();

    parameters.put("pAnio", new Integer(anio));

    query.setOrdering("anio ascending, mes ascending");

    Collection colHistoricoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoTicket);

    return colHistoricoTicket;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(HistoricoTicket.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("anio ascending, mes ascending");

    Collection colHistoricoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoTicket);

    return colHistoricoTicket;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(HistoricoTicket.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("anio ascending, mes ascending");

    Collection colHistoricoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colHistoricoTicket);

    return colHistoricoTicket;
  }
}