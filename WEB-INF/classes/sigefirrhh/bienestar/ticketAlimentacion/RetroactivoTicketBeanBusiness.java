package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class RetroactivoTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRetroactivoTicket(RetroactivoTicket retroactivoTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RetroactivoTicket retroactivoTicketNew = 
      (RetroactivoTicket)BeanUtils.cloneBean(
      retroactivoTicket);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (retroactivoTicketNew.getTrabajador() != null) {
      retroactivoTicketNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        retroactivoTicketNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(retroactivoTicketNew);
  }

  public void updateRetroactivoTicket(RetroactivoTicket retroactivoTicket) throws Exception
  {
    RetroactivoTicket retroactivoTicketModify = 
      findRetroactivoTicketById(retroactivoTicket.getIdRetroactivoTicket());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (retroactivoTicket.getTrabajador() != null) {
      retroactivoTicket.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        retroactivoTicket.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(retroactivoTicketModify, retroactivoTicket);
  }

  public void deleteRetroactivoTicket(RetroactivoTicket retroactivoTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RetroactivoTicket retroactivoTicketDelete = 
      findRetroactivoTicketById(retroactivoTicket.getIdRetroactivoTicket());
    pm.deletePersistent(retroactivoTicketDelete);
  }

  public RetroactivoTicket findRetroactivoTicketById(long idRetroactivoTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRetroactivoTicket == pIdRetroactivoTicket";
    Query query = pm.newQuery(RetroactivoTicket.class, filter);

    query.declareParameters("long pIdRetroactivoTicket");

    parameters.put("pIdRetroactivoTicket", new Long(idRetroactivoTicket));

    Collection colRetroactivoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRetroactivoTicket.iterator();
    return (RetroactivoTicket)iterator.next();
  }

  public Collection findRetroactivoTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent retroactivoTicketExtent = pm.getExtent(
      RetroactivoTicket.class, true);
    Query query = pm.newQuery(retroactivoTicketExtent);
    query.setOrdering("mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(RetroactivoTicket.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("mes ascending");

    Collection colRetroactivoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRetroactivoTicket);

    return colRetroactivoTicket;
  }
}