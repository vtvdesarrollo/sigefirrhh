package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportHistoricoTicketsForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportHistoricoTicketsForm.class.getName());
  private int reportId;
  private long idGrupoTicket;
  private String agrupado = "1";

  private String formato = "1";
  private String reportName = null;
  private Collection listGrupoTicket;
  private TicketAlimentacionFacadeExtend ticketAlimentacionFacade;
  private LoginSession login;
  private int anio = 0;
  private int mes = 0;

  public ReportHistoricoTicketsForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));
    this.ticketAlimentacionFacade = new TicketAlimentacionFacadeExtend();

    this.reportId = JasperForWeb.newReportId(this.reportId);
    this.anio = Calendar.getInstance().get(1);
    this.mes = Calendar.getInstance().get(2);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event) {
        ReportHistoricoTicketsForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    if (this.agrupado.trim().equals("1"))
      this.reportName = "calculoticketlphis";
    else if (this.agrupado.trim().equals("2"))
      this.reportName = "calculoticketuelhis";
    else if (this.agrupado.trim().equals("3"))
      this.reportName = "calculoticketcathis";
    else if (this.agrupado.trim().equals("4"))
      this.reportName = "calculoticketcthis";
  }

  public void refresh()
  {
    try
    {
      this.listGrupoTicket = this.ticketAlimentacionFacade.findGrupoTicketWithSeguridad(this.login.getIdUsuario(), 
        this.login.getIdOrganismo(), this.login.getAdministrador());

      this.reportName = null;
    }
    catch (Exception e) {
      this.listGrupoTicket = new ArrayList();
    }
  }

  public String runReport() {
    JasperForWeb report = null;
    FacesContext context = FacesContext.getCurrentInstance();
    Map parameters = new Hashtable();

    log.error("Agrupado: " + this.agrupado);
    try {
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_grupo_ticket", new Long(this.idGrupoTicket));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));

      cambiarNombreAReporte();

      report = new JasperForWeb();

      if (this.formato.equals("2")) {
        report.setType(3);
      }
      log.error("ReportName de nuevo : " + this.reportName);
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/bienestar/ticketAlimentacion");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
      parameters = null;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListGrupoTicket()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.listGrupoTicket.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getFormato() {
    return this.formato;
  }
  public void setFormato(String formato) {
    this.formato = formato;
  }

  public int getAnio() {
    return this.anio;
  }

  public void setAnio(int anio) {
    this.anio = anio;
  }

  public int getMes() {
    return this.mes;
  }

  public void setMes(int mes) {
    this.mes = mes;
  }

  public String getAgrupado()
  {
    return this.agrupado;
  }

  public void setAgrupado(String agrupado)
  {
    this.agrupado = agrupado;
  }

  public long getIdGrupoTicket()
  {
    return this.idGrupoTicket;
  }

  public void setIdGrupoTicket(long idGrupoTicket)
  {
    this.idGrupoTicket = idGrupoTicket;
  }
}