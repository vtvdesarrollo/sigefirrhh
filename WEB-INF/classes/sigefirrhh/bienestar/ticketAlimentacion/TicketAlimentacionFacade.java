package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class TicketAlimentacionFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private TicketAlimentacionBusiness ticketAlimentacionBusiness = new TicketAlimentacionBusiness();

  public void addExcepcionTicket(ExcepcionTicket excepcionTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ticketAlimentacionBusiness.addExcepcionTicket(excepcionTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateExcepcionTicket(ExcepcionTicket excepcionTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.updateExcepcionTicket(excepcionTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteExcepcionTicket(ExcepcionTicket excepcionTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.deleteExcepcionTicket(excepcionTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public ExcepcionTicket findExcepcionTicketById(long excepcionTicketId) throws Exception
  {
    try { this.txn.open();
      ExcepcionTicket excepcionTicket = 
        this.ticketAlimentacionBusiness.findExcepcionTicketById(excepcionTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(excepcionTicket);
      return excepcionTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllExcepcionTicket() throws Exception
  {
    try { this.txn.open();
      return this.ticketAlimentacionBusiness.findAllExcepcionTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findExcepcionTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findExcepcionTicketByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addHistoricoTicket(HistoricoTicket historicoTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ticketAlimentacionBusiness.addHistoricoTicket(historicoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateHistoricoTicket(HistoricoTicket historicoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.updateHistoricoTicket(historicoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteHistoricoTicket(HistoricoTicket historicoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.deleteHistoricoTicket(historicoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public HistoricoTicket findHistoricoTicketById(long historicoTicketId) throws Exception
  {
    try { this.txn.open();
      HistoricoTicket historicoTicket = 
        this.ticketAlimentacionBusiness.findHistoricoTicketById(historicoTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(historicoTicket);
      return historicoTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllHistoricoTicket() throws Exception
  {
    try { this.txn.open();
      return this.ticketAlimentacionBusiness.findAllHistoricoTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoTicketByAnio(int anio)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findHistoricoTicketByAnio(anio);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findHistoricoTicketByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findHistoricoTicketByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findHistoricoTicketByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addDescuentoTicket(DescuentoTicket descuentoTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ticketAlimentacionBusiness.addDescuentoTicket(descuentoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateDescuentoTicket(DescuentoTicket descuentoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.updateDescuentoTicket(descuentoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteDescuentoTicket(DescuentoTicket descuentoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.deleteDescuentoTicket(descuentoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public DescuentoTicket findDescuentoTicketById(long descuentoTicketId) throws Exception
  {
    try { this.txn.open();
      DescuentoTicket descuentoTicket = 
        this.ticketAlimentacionBusiness.findDescuentoTicketById(descuentoTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(descuentoTicket);
      return descuentoTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllDescuentoTicket() throws Exception
  {
    try { this.txn.open();
      return this.ticketAlimentacionBusiness.findAllDescuentoTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDescuentoTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findDescuentoTicketByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addSeguridadTicket(SeguridadTicket seguridadTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ticketAlimentacionBusiness.addSeguridadTicket(seguridadTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateSeguridadTicket(SeguridadTicket seguridadTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.updateSeguridadTicket(seguridadTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteSeguridadTicket(SeguridadTicket seguridadTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.deleteSeguridadTicket(seguridadTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadTicket findSeguridadTicketById(long seguridadTicketId) throws Exception
  {
    try { this.txn.open();
      SeguridadTicket seguridadTicket = 
        this.ticketAlimentacionBusiness.findSeguridadTicketById(seguridadTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadTicket);
      return seguridadTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllSeguridadTicket() throws Exception
  {
    try { this.txn.open();
      return this.ticketAlimentacionBusiness.findAllSeguridadTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findSeguridadTicketByGrupoTicket(long idGrupoTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findSeguridadTicketByGrupoTicket(idGrupoTicket);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findSeguridadAusenciaByIdGrupoTicket(long idGrupoTicket)
    throws Exception
  {
    return null;
  }

  public void addRetroactivoTicket(RetroactivoTicket retroactivoTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ticketAlimentacionBusiness.addRetroactivoTicket(retroactivoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRetroactivoTicket(RetroactivoTicket retroactivoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.updateRetroactivoTicket(retroactivoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRetroactivoTicket(RetroactivoTicket retroactivoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.deleteRetroactivoTicket(retroactivoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RetroactivoTicket findRetroactivoTicketById(long retroactivoTicketId) throws Exception
  {
    try { this.txn.open();
      RetroactivoTicket retroactivoTicket = 
        this.ticketAlimentacionBusiness.findRetroactivoTicketById(retroactivoTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(retroactivoTicket);
      return retroactivoTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRetroactivoTicket() throws Exception
  {
    try { this.txn.open();
      return this.ticketAlimentacionBusiness.findAllRetroactivoTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRetroactivoTicketByTrabajador(long idTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findRetroactivoTicketByTrabajador(idTrabajador);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addGrupoTicket(GrupoTicket grupoTicket)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.ticketAlimentacionBusiness.addGrupoTicket(grupoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateGrupoTicket(GrupoTicket grupoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.updateGrupoTicket(grupoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteGrupoTicket(GrupoTicket grupoTicket) throws Exception
  {
    try { this.txn.open();
      this.ticketAlimentacionBusiness.deleteGrupoTicket(grupoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public GrupoTicket findGrupoTicketById(long grupoTicketId) throws Exception
  {
    try { this.txn.open();
      GrupoTicket grupoTicket = 
        this.ticketAlimentacionBusiness.findGrupoTicketById(grupoTicketId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(grupoTicket);
      return grupoTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllGrupoTicket() throws Exception
  {
    try { this.txn.open();
      return this.ticketAlimentacionBusiness.findAllGrupoTicket();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoTicketByCodGrupoTicket(int codGrupoTicket, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findGrupoTicketByCodGrupoTicket(codGrupoTicket, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoTicketByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findGrupoTicketByNombre(nombre, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoTicketByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findGrupoTicketByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}