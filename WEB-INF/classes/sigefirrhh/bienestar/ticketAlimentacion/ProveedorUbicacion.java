package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.ProveedorTicket;

public class ProveedorUbicacion
  implements Serializable, PersistenceCapable
{
  private long idProveedorUbicacion;
  private ProveedorTicket proveedorTicket;
  private String codigoUbicacion;
  private String observaciones;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codigoUbicacion", "idProveedorUbicacion", "observaciones", "proveedorTicket" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.ProveedorTicket") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetproveedorTicket(this).toString() + "  -  " + 
      jdoGetcodigoUbicacion(this);
  }

  public String getCodigoUbicacion() {
    return jdoGetcodigoUbicacion(this);
  }
  public void setCodigoUbicacion(String codigoUbicacion) {
    jdoSetcodigoUbicacion(this, codigoUbicacion);
  }
  public long getIdProveedorUbicacion() {
    return jdoGetidProveedorUbicacion(this);
  }
  public void setIdProveedorUbicacion(long idProveedorUbicacion) {
    jdoSetidProveedorUbicacion(this, idProveedorUbicacion);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public ProveedorTicket getProveedorTicket() {
    return jdoGetproveedorTicket(this);
  }
  public void setProveedorTicket(ProveedorTicket proveedorTicket) {
    jdoSetproveedorTicket(this, proveedorTicket);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.ProveedorUbicacion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ProveedorUbicacion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ProveedorUbicacion localProveedorUbicacion = new ProveedorUbicacion();
    localProveedorUbicacion.jdoFlags = 1;
    localProveedorUbicacion.jdoStateManager = paramStateManager;
    return localProveedorUbicacion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ProveedorUbicacion localProveedorUbicacion = new ProveedorUbicacion();
    localProveedorUbicacion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProveedorUbicacion.jdoFlags = 1;
    localProveedorUbicacion.jdoStateManager = paramStateManager;
    return localProveedorUbicacion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoUbicacion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProveedorUbicacion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proveedorTicket);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoUbicacion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProveedorUbicacion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proveedorTicket = ((ProveedorTicket)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ProveedorUbicacion paramProveedorUbicacion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProveedorUbicacion == null)
        throw new IllegalArgumentException("arg1");
      this.codigoUbicacion = paramProveedorUbicacion.codigoUbicacion;
      return;
    case 1:
      if (paramProveedorUbicacion == null)
        throw new IllegalArgumentException("arg1");
      this.idProveedorUbicacion = paramProveedorUbicacion.idProveedorUbicacion;
      return;
    case 2:
      if (paramProveedorUbicacion == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramProveedorUbicacion.observaciones;
      return;
    case 3:
      if (paramProveedorUbicacion == null)
        throw new IllegalArgumentException("arg1");
      this.proveedorTicket = paramProveedorUbicacion.proveedorTicket;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ProveedorUbicacion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ProveedorUbicacion localProveedorUbicacion = (ProveedorUbicacion)paramObject;
    if (localProveedorUbicacion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProveedorUbicacion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProveedorUbicacionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProveedorUbicacionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProveedorUbicacionPK))
      throw new IllegalArgumentException("arg1");
    ProveedorUbicacionPK localProveedorUbicacionPK = (ProveedorUbicacionPK)paramObject;
    localProveedorUbicacionPK.idProveedorUbicacion = this.idProveedorUbicacion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProveedorUbicacionPK))
      throw new IllegalArgumentException("arg1");
    ProveedorUbicacionPK localProveedorUbicacionPK = (ProveedorUbicacionPK)paramObject;
    this.idProveedorUbicacion = localProveedorUbicacionPK.idProveedorUbicacion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProveedorUbicacionPK))
      throw new IllegalArgumentException("arg2");
    ProveedorUbicacionPK localProveedorUbicacionPK = (ProveedorUbicacionPK)paramObject;
    localProveedorUbicacionPK.idProveedorUbicacion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProveedorUbicacionPK))
      throw new IllegalArgumentException("arg2");
    ProveedorUbicacionPK localProveedorUbicacionPK = (ProveedorUbicacionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localProveedorUbicacionPK.idProveedorUbicacion);
  }

  private static final String jdoGetcodigoUbicacion(ProveedorUbicacion paramProveedorUbicacion)
  {
    if (paramProveedorUbicacion.jdoFlags <= 0)
      return paramProveedorUbicacion.codigoUbicacion;
    StateManager localStateManager = paramProveedorUbicacion.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorUbicacion.codigoUbicacion;
    if (localStateManager.isLoaded(paramProveedorUbicacion, jdoInheritedFieldCount + 0))
      return paramProveedorUbicacion.codigoUbicacion;
    return localStateManager.getStringField(paramProveedorUbicacion, jdoInheritedFieldCount + 0, paramProveedorUbicacion.codigoUbicacion);
  }

  private static final void jdoSetcodigoUbicacion(ProveedorUbicacion paramProveedorUbicacion, String paramString)
  {
    if (paramProveedorUbicacion.jdoFlags == 0)
    {
      paramProveedorUbicacion.codigoUbicacion = paramString;
      return;
    }
    StateManager localStateManager = paramProveedorUbicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorUbicacion.codigoUbicacion = paramString;
      return;
    }
    localStateManager.setStringField(paramProveedorUbicacion, jdoInheritedFieldCount + 0, paramProveedorUbicacion.codigoUbicacion, paramString);
  }

  private static final long jdoGetidProveedorUbicacion(ProveedorUbicacion paramProveedorUbicacion)
  {
    return paramProveedorUbicacion.idProveedorUbicacion;
  }

  private static final void jdoSetidProveedorUbicacion(ProveedorUbicacion paramProveedorUbicacion, long paramLong)
  {
    StateManager localStateManager = paramProveedorUbicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorUbicacion.idProveedorUbicacion = paramLong;
      return;
    }
    localStateManager.setLongField(paramProveedorUbicacion, jdoInheritedFieldCount + 1, paramProveedorUbicacion.idProveedorUbicacion, paramLong);
  }

  private static final String jdoGetobservaciones(ProveedorUbicacion paramProveedorUbicacion)
  {
    if (paramProveedorUbicacion.jdoFlags <= 0)
      return paramProveedorUbicacion.observaciones;
    StateManager localStateManager = paramProveedorUbicacion.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorUbicacion.observaciones;
    if (localStateManager.isLoaded(paramProveedorUbicacion, jdoInheritedFieldCount + 2))
      return paramProveedorUbicacion.observaciones;
    return localStateManager.getStringField(paramProveedorUbicacion, jdoInheritedFieldCount + 2, paramProveedorUbicacion.observaciones);
  }

  private static final void jdoSetobservaciones(ProveedorUbicacion paramProveedorUbicacion, String paramString)
  {
    if (paramProveedorUbicacion.jdoFlags == 0)
    {
      paramProveedorUbicacion.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramProveedorUbicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorUbicacion.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramProveedorUbicacion, jdoInheritedFieldCount + 2, paramProveedorUbicacion.observaciones, paramString);
  }

  private static final ProveedorTicket jdoGetproveedorTicket(ProveedorUbicacion paramProveedorUbicacion)
  {
    StateManager localStateManager = paramProveedorUbicacion.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorUbicacion.proveedorTicket;
    if (localStateManager.isLoaded(paramProveedorUbicacion, jdoInheritedFieldCount + 3))
      return paramProveedorUbicacion.proveedorTicket;
    return (ProveedorTicket)localStateManager.getObjectField(paramProveedorUbicacion, jdoInheritedFieldCount + 3, paramProveedorUbicacion.proveedorTicket);
  }

  private static final void jdoSetproveedorTicket(ProveedorUbicacion paramProveedorUbicacion, ProveedorTicket paramProveedorTicket)
  {
    StateManager localStateManager = paramProveedorUbicacion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorUbicacion.proveedorTicket = paramProveedorTicket;
      return;
    }
    localStateManager.setObjectField(paramProveedorUbicacion, jdoInheritedFieldCount + 3, paramProveedorUbicacion.proveedorTicket, paramProveedorTicket);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}