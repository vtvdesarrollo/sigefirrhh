package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class TicketAlimentacionFacadeExtend extends TicketAlimentacionFacade
{
  private TicketAlimentacionBusinessExtend ticketAlimentacionBusiness = new TicketAlimentacionBusinessExtend();
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public void asignarTickets(long idGrupoTicket, int anio, int mes, int proceso, String especial, String usuario) {
    this.ticketAlimentacionBusiness.asignarTickets(idGrupoTicket, anio, mes, proceso, especial, usuario);
  }

  public SeguridadTicket findLastSeguridadTicket(long idGrupoTicket) throws Exception {
    try {
      this.txn.open();
      SeguridadTicket seguridadTicket = 
        this.ticketAlimentacionBusiness.findLastSeguridadTicket(idGrupoTicket);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(seguridadTicket);
      return seguridadTicket;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public SeguridadAusencia findLastSeguridadAusencia(long idGrupoTicket) throws Exception
  {
    try {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findLastSeguridadAusencia(idGrupoTicket);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findGrupoTicketWithSeguridad(long idUsuario, long idOrganismo, String administrador)
    throws Exception
  {
    return this.ticketAlimentacionBusiness.findGrupoTicketWithSeguridad(idUsuario, idOrganismo, administrador);
  }
  public Collection findRetroactivoTicketByTrabajadorAndPagado(long idTrabajador, String pagado) throws Exception {
    try {
      this.txn.open();
      return this.ticketAlimentacionBusiness.findRetroactivoByTrabajadorAndPagado(idTrabajador, pagado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findDescuentoTicketByTrabajadorAndPagado(long idTrabajador, String pagado) throws Exception {
    try { this.txn.open();
      return this.ticketAlimentacionBusiness.findDescuentoTicketByTrabajadorAndPagado(idTrabajador, pagado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void setSeguridadAusencia()
  {
    this.ticketAlimentacionBusiness.setSeguridadAusencia();
  }
}