package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.report.JasperForWeb;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class AsignarTicketsForm
{
  static Logger log = Logger.getLogger(AsignarTicketsForm.class.getName());
  private String selectGrupoTicket;
  private long idGrupoTicket;
  private Collection listGrupoTicket;
  private int reportId;
  private String reportName;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private TicketAlimentacionFacadeExtend ticketAlimentacionFacade = new TicketAlimentacionFacadeExtend();
  private SeguridadTicket seguridadTicket = new SeguridadTicket();
  private int anio;
  private int mes;
  private boolean auxShow = false;
  private int proceso;
  private boolean showExec = false;

  public boolean isShow() {
    return this.auxShow;
  }
  public AsignarTicketsForm() {
    this.reportName = "calculoticketlpgen";
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectGrupoTicket = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listGrupoTicket = 
        this.ticketAlimentacionFacade.findGrupoTicketWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String execute()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.ticketAlimentacionFacade.asignarTickets(this.idGrupoTicket, this.anio, this.mes, this.proceso, "N", this.login.getUsuario());

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      if (getProceso() == 2) {
        setShowExec(false);
      }
      context.addMessage("success_process", new FacesMessage("Se asignaron tickets con éxito"));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String runReport()
  {
    Map parameters = new Hashtable();
    try
    {
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
      parameters.put("id_grupo_ticket", new Long(this.idGrupoTicket));
      parameters.put("anio", new Integer(this.anio));
      parameters.put("mes", new Integer(this.mes));
      JasperForWeb report = new JasperForWeb();
      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/bienestar/ticketAlimentacion");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      log.error("PASO 3");
    }
    return null;
  }

  public void changeGrupoTicket(ValueChangeEvent event)
  {
    this.idGrupoTicket = Long.valueOf(
      (String)event.getNewValue()).longValue();
    if (this.idGrupoTicket != 0L) {
      try {
        this.seguridadTicket = this.ticketAlimentacionFacade.findLastSeguridadTicket(this.idGrupoTicket);
        if (this.seguridadTicket != null) {
          if (this.seguridadTicket.getMes() == 12) {
            this.anio = (this.seguridadTicket.getAnio() + 1);
            this.mes = 1;
          } else {
            this.mes = (this.seguridadTicket.getMes() + 1);
            this.anio = this.seguridadTicket.getAnio();
          }

          this.auxShow = true;
          setShowExec(true);
        } else {
          this.auxShow = false;
          setShowExec(false);
        }
      }
      catch (Exception e) {
        this.auxShow = false;
        setShowExec(false);
        log.error("Excepcion controlada:", e);
      }
    } else {
      this.auxShow = false;
      setShowExec(false);
    }
  }

  public Collection getListGrupoTicket() { Collection col = new ArrayList();
    Iterator iterator = this.listGrupoTicket.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col; }

  public String getSelectGrupoTicket()
  {
    return this.selectGrupoTicket;
  }

  public void setSelectGrupoTicket(String string) {
    this.selectGrupoTicket = string;
    this.idGrupoTicket = Integer.parseInt(string);
  }

  public int getAnio() {
    return this.anio;
  }
  public int getMes() {
    return this.mes;
  }
  public int getProceso() {
    return this.proceso;
  }
  public void setProceso(int proceso) {
    this.proceso = proceso;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public void setMes(int mes) {
    this.mes = mes;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int reportId) {
    this.reportId = reportId;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String reportName) {
    this.reportName = reportName;
  }
  public boolean isShowExec() {
    return this.showExec;
  }
  public void setShowExec(boolean showExec) {
    this.showExec = showExec;
  }
}