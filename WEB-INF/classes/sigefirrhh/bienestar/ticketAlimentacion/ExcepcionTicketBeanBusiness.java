package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class ExcepcionTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addExcepcionTicket(ExcepcionTicket excepcionTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    ExcepcionTicket excepcionTicketNew = 
      (ExcepcionTicket)BeanUtils.cloneBean(
      excepcionTicket);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (excepcionTicketNew.getTrabajador() != null) {
      excepcionTicketNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        excepcionTicketNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(excepcionTicketNew);
  }

  public void updateExcepcionTicket(ExcepcionTicket excepcionTicket) throws Exception
  {
    ExcepcionTicket excepcionTicketModify = 
      findExcepcionTicketById(excepcionTicket.getIdExcepcionTicket());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (excepcionTicket.getTrabajador() != null) {
      excepcionTicket.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        excepcionTicket.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(excepcionTicketModify, excepcionTicket);
  }

  public void deleteExcepcionTicket(ExcepcionTicket excepcionTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    ExcepcionTicket excepcionTicketDelete = 
      findExcepcionTicketById(excepcionTicket.getIdExcepcionTicket());
    pm.deletePersistent(excepcionTicketDelete);
  }

  public ExcepcionTicket findExcepcionTicketById(long idExcepcionTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idExcepcionTicket == pIdExcepcionTicket";
    Query query = pm.newQuery(ExcepcionTicket.class, filter);

    query.declareParameters("long pIdExcepcionTicket");

    parameters.put("pIdExcepcionTicket", new Long(idExcepcionTicket));

    Collection colExcepcionTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colExcepcionTicket.iterator();
    return (ExcepcionTicket)iterator.next();
  }

  public Collection findExcepcionTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent excepcionTicketExtent = pm.getExtent(
      ExcepcionTicket.class, true);
    Query query = pm.newQuery(excepcionTicketExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(ExcepcionTicket.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    Collection colExcepcionTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colExcepcionTicket);

    return colExcepcionTicket;
  }
}