package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class SeguridadAusenciaPK
  implements Serializable
{
  public long idSeguridadAusencia;

  public SeguridadAusenciaPK()
  {
  }

  public SeguridadAusenciaPK(long idSeguridadAusencia)
  {
    this.idSeguridadAusencia = idSeguridadAusencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((SeguridadAusenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(SeguridadAusenciaPK thatPK)
  {
    return 
      this.idSeguridadAusencia == thatPK.idSeguridadAusencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idSeguridadAusencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idSeguridadAusencia);
  }
}