package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class UbicacionDependenciaPK
  implements Serializable
{
  public long idUbicacionDependencia;

  public UbicacionDependenciaPK()
  {
  }

  public UbicacionDependenciaPK(long idUbicacionDependencia)
  {
    this.idUbicacionDependencia = idUbicacionDependencia;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UbicacionDependenciaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UbicacionDependenciaPK thatPK)
  {
    return 
      this.idUbicacionDependencia == thatPK.idUbicacionDependencia;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUbicacionDependencia)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUbicacionDependencia);
  }
}