package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class GrupoTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_PERIODICIDAD;
  protected static final Map LISTA_SI_NO;
  private long idGrupoTicket;
  private int codGrupoTicket;
  private String nombre;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codGrupoTicket", "idGrupoTicket", "nombre", "organismo" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Organismo") }; private static final byte[] jdoFieldFlags = { 21, 24, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new GrupoTicket());

    LISTA_PERIODICIDAD = 
      new LinkedHashMap();
    LISTA_SI_NO = 
      new LinkedHashMap();
    LISTA_PERIODICIDAD.put("Q", "QUINCENAL");
    LISTA_PERIODICIDAD.put("S", "SEMANAL");
    LISTA_PERIODICIDAD.put("M", "MENSUAL");
    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public String toString()
  {
    return jdoGetnombre(this) + "  -  " + 
      jdoGetcodGrupoTicket(this);
  }
  public int getCodGrupoTicket() {
    return jdoGetcodGrupoTicket(this);
  }

  public void setCodGrupoTicket(int codGrupoTicket) {
    jdoSetcodGrupoTicket(this, codGrupoTicket);
  }

  public long getIdGrupoTicket() {
    return jdoGetidGrupoTicket(this);
  }

  public void setIdGrupoTicket(long idGrupoTicket) {
    jdoSetidGrupoTicket(this, idGrupoTicket);
  }

  public String getNombre() {
    return jdoGetnombre(this);
  }

  public void setNombre(String nombre) {
    jdoSetnombre(this, nombre);
  }

  public Organismo getOrganismo() {
    return jdoGetorganismo(this);
  }

  public void setOrganismo(Organismo organismo) {
    jdoSetorganismo(this, organismo);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    GrupoTicket localGrupoTicket = new GrupoTicket();
    localGrupoTicket.jdoFlags = 1;
    localGrupoTicket.jdoStateManager = paramStateManager;
    return localGrupoTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    GrupoTicket localGrupoTicket = new GrupoTicket();
    localGrupoTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localGrupoTicket.jdoFlags = 1;
    localGrupoTicket.jdoStateManager = paramStateManager;
    return localGrupoTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.codGrupoTicket);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idGrupoTicket);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codGrupoTicket = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idGrupoTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(GrupoTicket paramGrupoTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramGrupoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.codGrupoTicket = paramGrupoTicket.codGrupoTicket;
      return;
    case 1:
      if (paramGrupoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idGrupoTicket = paramGrupoTicket.idGrupoTicket;
      return;
    case 2:
      if (paramGrupoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramGrupoTicket.nombre;
      return;
    case 3:
      if (paramGrupoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramGrupoTicket.organismo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof GrupoTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    GrupoTicket localGrupoTicket = (GrupoTicket)paramObject;
    if (localGrupoTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localGrupoTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new GrupoTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new GrupoTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoTicketPK))
      throw new IllegalArgumentException("arg1");
    GrupoTicketPK localGrupoTicketPK = (GrupoTicketPK)paramObject;
    localGrupoTicketPK.idGrupoTicket = this.idGrupoTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof GrupoTicketPK))
      throw new IllegalArgumentException("arg1");
    GrupoTicketPK localGrupoTicketPK = (GrupoTicketPK)paramObject;
    this.idGrupoTicket = localGrupoTicketPK.idGrupoTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoTicketPK))
      throw new IllegalArgumentException("arg2");
    GrupoTicketPK localGrupoTicketPK = (GrupoTicketPK)paramObject;
    localGrupoTicketPK.idGrupoTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof GrupoTicketPK))
      throw new IllegalArgumentException("arg2");
    GrupoTicketPK localGrupoTicketPK = (GrupoTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localGrupoTicketPK.idGrupoTicket);
  }

  private static final int jdoGetcodGrupoTicket(GrupoTicket paramGrupoTicket)
  {
    if (paramGrupoTicket.jdoFlags <= 0)
      return paramGrupoTicket.codGrupoTicket;
    StateManager localStateManager = paramGrupoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoTicket.codGrupoTicket;
    if (localStateManager.isLoaded(paramGrupoTicket, jdoInheritedFieldCount + 0))
      return paramGrupoTicket.codGrupoTicket;
    return localStateManager.getIntField(paramGrupoTicket, jdoInheritedFieldCount + 0, paramGrupoTicket.codGrupoTicket);
  }

  private static final void jdoSetcodGrupoTicket(GrupoTicket paramGrupoTicket, int paramInt)
  {
    if (paramGrupoTicket.jdoFlags == 0)
    {
      paramGrupoTicket.codGrupoTicket = paramInt;
      return;
    }
    StateManager localStateManager = paramGrupoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoTicket.codGrupoTicket = paramInt;
      return;
    }
    localStateManager.setIntField(paramGrupoTicket, jdoInheritedFieldCount + 0, paramGrupoTicket.codGrupoTicket, paramInt);
  }

  private static final long jdoGetidGrupoTicket(GrupoTicket paramGrupoTicket)
  {
    return paramGrupoTicket.idGrupoTicket;
  }

  private static final void jdoSetidGrupoTicket(GrupoTicket paramGrupoTicket, long paramLong)
  {
    StateManager localStateManager = paramGrupoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoTicket.idGrupoTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramGrupoTicket, jdoInheritedFieldCount + 1, paramGrupoTicket.idGrupoTicket, paramLong);
  }

  private static final String jdoGetnombre(GrupoTicket paramGrupoTicket)
  {
    if (paramGrupoTicket.jdoFlags <= 0)
      return paramGrupoTicket.nombre;
    StateManager localStateManager = paramGrupoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoTicket.nombre;
    if (localStateManager.isLoaded(paramGrupoTicket, jdoInheritedFieldCount + 2))
      return paramGrupoTicket.nombre;
    return localStateManager.getStringField(paramGrupoTicket, jdoInheritedFieldCount + 2, paramGrupoTicket.nombre);
  }

  private static final void jdoSetnombre(GrupoTicket paramGrupoTicket, String paramString)
  {
    if (paramGrupoTicket.jdoFlags == 0)
    {
      paramGrupoTicket.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramGrupoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoTicket.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramGrupoTicket, jdoInheritedFieldCount + 2, paramGrupoTicket.nombre, paramString);
  }

  private static final Organismo jdoGetorganismo(GrupoTicket paramGrupoTicket)
  {
    StateManager localStateManager = paramGrupoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramGrupoTicket.organismo;
    if (localStateManager.isLoaded(paramGrupoTicket, jdoInheritedFieldCount + 3))
      return paramGrupoTicket.organismo;
    return (Organismo)localStateManager.getObjectField(paramGrupoTicket, jdoInheritedFieldCount + 3, paramGrupoTicket.organismo);
  }

  private static final void jdoSetorganismo(GrupoTicket paramGrupoTicket, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramGrupoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramGrupoTicket.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramGrupoTicket, jdoInheritedFieldCount + 3, paramGrupoTicket.organismo, paramOrganismo);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}