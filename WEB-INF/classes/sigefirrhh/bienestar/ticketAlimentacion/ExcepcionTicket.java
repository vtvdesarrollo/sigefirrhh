package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class ExcepcionTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idExcepcionTicket;
  private int numeroTickets;
  private String observaciones;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "idExcepcionTicket", "numeroTickets", "observaciones", "trabajador" }; private static final Class[] jdoFieldTypes = { Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 24, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.ExcepcionTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ExcepcionTicket());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public ExcepcionTicket()
  {
    jdoSetnumeroTickets(this, 0);
  }

  public String toString()
  {
    return 
      String.valueOf(jdoGetnumeroTickets(this)) + ' ' + jdoGetobservaciones(this);
  }

  public long getIdExcepcionTicket()
  {
    return jdoGetidExcepcionTicket(this);
  }
  public void setIdExcepcionTicket(long idExcepcionTicket) {
    jdoSetidExcepcionTicket(this, idExcepcionTicket);
  }
  public int getNumeroTickets() {
    return jdoGetnumeroTickets(this);
  }
  public void setNumeroTickets(int numeroTickets) {
    jdoSetnumeroTickets(this, numeroTickets);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ExcepcionTicket localExcepcionTicket = new ExcepcionTicket();
    localExcepcionTicket.jdoFlags = 1;
    localExcepcionTicket.jdoStateManager = paramStateManager;
    return localExcepcionTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ExcepcionTicket localExcepcionTicket = new ExcepcionTicket();
    localExcepcionTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localExcepcionTicket.jdoFlags = 1;
    localExcepcionTicket.jdoStateManager = paramStateManager;
    return localExcepcionTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idExcepcionTicket);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroTickets);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idExcepcionTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroTickets = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ExcepcionTicket paramExcepcionTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramExcepcionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idExcepcionTicket = paramExcepcionTicket.idExcepcionTicket;
      return;
    case 1:
      if (paramExcepcionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.numeroTickets = paramExcepcionTicket.numeroTickets;
      return;
    case 2:
      if (paramExcepcionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramExcepcionTicket.observaciones;
      return;
    case 3:
      if (paramExcepcionTicket == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramExcepcionTicket.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ExcepcionTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ExcepcionTicket localExcepcionTicket = (ExcepcionTicket)paramObject;
    if (localExcepcionTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localExcepcionTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ExcepcionTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ExcepcionTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExcepcionTicketPK))
      throw new IllegalArgumentException("arg1");
    ExcepcionTicketPK localExcepcionTicketPK = (ExcepcionTicketPK)paramObject;
    localExcepcionTicketPK.idExcepcionTicket = this.idExcepcionTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ExcepcionTicketPK))
      throw new IllegalArgumentException("arg1");
    ExcepcionTicketPK localExcepcionTicketPK = (ExcepcionTicketPK)paramObject;
    this.idExcepcionTicket = localExcepcionTicketPK.idExcepcionTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExcepcionTicketPK))
      throw new IllegalArgumentException("arg2");
    ExcepcionTicketPK localExcepcionTicketPK = (ExcepcionTicketPK)paramObject;
    localExcepcionTicketPK.idExcepcionTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ExcepcionTicketPK))
      throw new IllegalArgumentException("arg2");
    ExcepcionTicketPK localExcepcionTicketPK = (ExcepcionTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localExcepcionTicketPK.idExcepcionTicket);
  }

  private static final long jdoGetidExcepcionTicket(ExcepcionTicket paramExcepcionTicket)
  {
    return paramExcepcionTicket.idExcepcionTicket;
  }

  private static final void jdoSetidExcepcionTicket(ExcepcionTicket paramExcepcionTicket, long paramLong)
  {
    StateManager localStateManager = paramExcepcionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionTicket.idExcepcionTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramExcepcionTicket, jdoInheritedFieldCount + 0, paramExcepcionTicket.idExcepcionTicket, paramLong);
  }

  private static final int jdoGetnumeroTickets(ExcepcionTicket paramExcepcionTicket)
  {
    if (paramExcepcionTicket.jdoFlags <= 0)
      return paramExcepcionTicket.numeroTickets;
    StateManager localStateManager = paramExcepcionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionTicket.numeroTickets;
    if (localStateManager.isLoaded(paramExcepcionTicket, jdoInheritedFieldCount + 1))
      return paramExcepcionTicket.numeroTickets;
    return localStateManager.getIntField(paramExcepcionTicket, jdoInheritedFieldCount + 1, paramExcepcionTicket.numeroTickets);
  }

  private static final void jdoSetnumeroTickets(ExcepcionTicket paramExcepcionTicket, int paramInt)
  {
    if (paramExcepcionTicket.jdoFlags == 0)
    {
      paramExcepcionTicket.numeroTickets = paramInt;
      return;
    }
    StateManager localStateManager = paramExcepcionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionTicket.numeroTickets = paramInt;
      return;
    }
    localStateManager.setIntField(paramExcepcionTicket, jdoInheritedFieldCount + 1, paramExcepcionTicket.numeroTickets, paramInt);
  }

  private static final String jdoGetobservaciones(ExcepcionTicket paramExcepcionTicket)
  {
    if (paramExcepcionTicket.jdoFlags <= 0)
      return paramExcepcionTicket.observaciones;
    StateManager localStateManager = paramExcepcionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionTicket.observaciones;
    if (localStateManager.isLoaded(paramExcepcionTicket, jdoInheritedFieldCount + 2))
      return paramExcepcionTicket.observaciones;
    return localStateManager.getStringField(paramExcepcionTicket, jdoInheritedFieldCount + 2, paramExcepcionTicket.observaciones);
  }

  private static final void jdoSetobservaciones(ExcepcionTicket paramExcepcionTicket, String paramString)
  {
    if (paramExcepcionTicket.jdoFlags == 0)
    {
      paramExcepcionTicket.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramExcepcionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionTicket.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramExcepcionTicket, jdoInheritedFieldCount + 2, paramExcepcionTicket.observaciones, paramString);
  }

  private static final Trabajador jdoGettrabajador(ExcepcionTicket paramExcepcionTicket)
  {
    StateManager localStateManager = paramExcepcionTicket.jdoStateManager;
    if (localStateManager == null)
      return paramExcepcionTicket.trabajador;
    if (localStateManager.isLoaded(paramExcepcionTicket, jdoInheritedFieldCount + 3))
      return paramExcepcionTicket.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramExcepcionTicket, jdoInheritedFieldCount + 3, paramExcepcionTicket.trabajador);
  }

  private static final void jdoSettrabajador(ExcepcionTicket paramExcepcionTicket, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramExcepcionTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramExcepcionTicket.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramExcepcionTicket, jdoInheritedFieldCount + 3, paramExcepcionTicket.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}