package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Dependencia;

public class UbicacionDependencia
  implements Serializable, PersistenceCapable
{
  private long idUbicacionDependencia;
  private ProveedorUbicacion proveedorUbicacion;
  private Dependencia dependencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "dependencia", "idUbicacionDependencia", "proveedorUbicacion" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), Long.TYPE, sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.ProveedorUbicacion") };
  private static final byte[] jdoFieldFlags = { 26, 24, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetproveedorUbicacion(this).toString() + " - " + jdoGetdependencia(this).toString();
  }

  public Dependencia getDependencia()
  {
    return jdoGetdependencia(this);
  }
  public void setDependencia(Dependencia dependencia) {
    jdoSetdependencia(this, dependencia);
  }
  public long getIdUbicacionDependencia() {
    return jdoGetidUbicacionDependencia(this);
  }
  public void setIdUbicacionDependencia(long idUbicacionDependencia) {
    jdoSetidUbicacionDependencia(this, idUbicacionDependencia);
  }
  public ProveedorUbicacion getProveedorUbicacion() {
    return jdoGetproveedorUbicacion(this);
  }
  public void setProveedorUbicacion(ProveedorUbicacion proveedorUbicacion) {
    jdoSetproveedorUbicacion(this, proveedorUbicacion);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.UbicacionDependencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UbicacionDependencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UbicacionDependencia localUbicacionDependencia = new UbicacionDependencia();
    localUbicacionDependencia.jdoFlags = 1;
    localUbicacionDependencia.jdoStateManager = paramStateManager;
    return localUbicacionDependencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UbicacionDependencia localUbicacionDependencia = new UbicacionDependencia();
    localUbicacionDependencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUbicacionDependencia.jdoFlags = 1;
    localUbicacionDependencia.jdoStateManager = paramStateManager;
    return localUbicacionDependencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUbicacionDependencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proveedorUbicacion);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUbicacionDependencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proveedorUbicacion = ((ProveedorUbicacion)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UbicacionDependencia paramUbicacionDependencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUbicacionDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramUbicacionDependencia.dependencia;
      return;
    case 1:
      if (paramUbicacionDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.idUbicacionDependencia = paramUbicacionDependencia.idUbicacionDependencia;
      return;
    case 2:
      if (paramUbicacionDependencia == null)
        throw new IllegalArgumentException("arg1");
      this.proveedorUbicacion = paramUbicacionDependencia.proveedorUbicacion;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UbicacionDependencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UbicacionDependencia localUbicacionDependencia = (UbicacionDependencia)paramObject;
    if (localUbicacionDependencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUbicacionDependencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UbicacionDependenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UbicacionDependenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UbicacionDependenciaPK))
      throw new IllegalArgumentException("arg1");
    UbicacionDependenciaPK localUbicacionDependenciaPK = (UbicacionDependenciaPK)paramObject;
    localUbicacionDependenciaPK.idUbicacionDependencia = this.idUbicacionDependencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UbicacionDependenciaPK))
      throw new IllegalArgumentException("arg1");
    UbicacionDependenciaPK localUbicacionDependenciaPK = (UbicacionDependenciaPK)paramObject;
    this.idUbicacionDependencia = localUbicacionDependenciaPK.idUbicacionDependencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UbicacionDependenciaPK))
      throw new IllegalArgumentException("arg2");
    UbicacionDependenciaPK localUbicacionDependenciaPK = (UbicacionDependenciaPK)paramObject;
    localUbicacionDependenciaPK.idUbicacionDependencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UbicacionDependenciaPK))
      throw new IllegalArgumentException("arg2");
    UbicacionDependenciaPK localUbicacionDependenciaPK = (UbicacionDependenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localUbicacionDependenciaPK.idUbicacionDependencia);
  }

  private static final Dependencia jdoGetdependencia(UbicacionDependencia paramUbicacionDependencia)
  {
    StateManager localStateManager = paramUbicacionDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramUbicacionDependencia.dependencia;
    if (localStateManager.isLoaded(paramUbicacionDependencia, jdoInheritedFieldCount + 0))
      return paramUbicacionDependencia.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramUbicacionDependencia, jdoInheritedFieldCount + 0, paramUbicacionDependencia.dependencia);
  }

  private static final void jdoSetdependencia(UbicacionDependencia paramUbicacionDependencia, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramUbicacionDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramUbicacionDependencia.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramUbicacionDependencia, jdoInheritedFieldCount + 0, paramUbicacionDependencia.dependencia, paramDependencia);
  }

  private static final long jdoGetidUbicacionDependencia(UbicacionDependencia paramUbicacionDependencia)
  {
    return paramUbicacionDependencia.idUbicacionDependencia;
  }

  private static final void jdoSetidUbicacionDependencia(UbicacionDependencia paramUbicacionDependencia, long paramLong)
  {
    StateManager localStateManager = paramUbicacionDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramUbicacionDependencia.idUbicacionDependencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramUbicacionDependencia, jdoInheritedFieldCount + 1, paramUbicacionDependencia.idUbicacionDependencia, paramLong);
  }

  private static final ProveedorUbicacion jdoGetproveedorUbicacion(UbicacionDependencia paramUbicacionDependencia)
  {
    StateManager localStateManager = paramUbicacionDependencia.jdoStateManager;
    if (localStateManager == null)
      return paramUbicacionDependencia.proveedorUbicacion;
    if (localStateManager.isLoaded(paramUbicacionDependencia, jdoInheritedFieldCount + 2))
      return paramUbicacionDependencia.proveedorUbicacion;
    return (ProveedorUbicacion)localStateManager.getObjectField(paramUbicacionDependencia, jdoInheritedFieldCount + 2, paramUbicacionDependencia.proveedorUbicacion);
  }

  private static final void jdoSetproveedorUbicacion(UbicacionDependencia paramUbicacionDependencia, ProveedorUbicacion paramProveedorUbicacion)
  {
    StateManager localStateManager = paramUbicacionDependencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramUbicacionDependencia.proveedorUbicacion = paramProveedorUbicacion;
      return;
    }
    localStateManager.setObjectField(paramUbicacionDependencia, jdoInheritedFieldCount + 2, paramUbicacionDependencia.proveedorUbicacion, paramProveedorUbicacion);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}