package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class GrupoTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addGrupoTicket(GrupoTicket grupoTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    GrupoTicket grupoTicketNew = 
      (GrupoTicket)BeanUtils.cloneBean(
      grupoTicket);

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (grupoTicketNew.getOrganismo() != null) {
      grupoTicketNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        grupoTicketNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(grupoTicketNew);
  }

  public void updateGrupoTicket(GrupoTicket grupoTicket) throws Exception
  {
    GrupoTicket grupoTicketModify = 
      findGrupoTicketById(grupoTicket.getIdGrupoTicket());

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (grupoTicket.getOrganismo() != null) {
      grupoTicket.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        grupoTicket.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(grupoTicketModify, grupoTicket);
  }

  public void deleteGrupoTicket(GrupoTicket grupoTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    GrupoTicket grupoTicketDelete = 
      findGrupoTicketById(grupoTicket.getIdGrupoTicket());
    pm.deletePersistent(grupoTicketDelete);
  }

  public GrupoTicket findGrupoTicketById(long idGrupoTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idGrupoTicket == pIdGrupoTicket";
    Query query = pm.newQuery(GrupoTicket.class, filter);

    query.declareParameters("long pIdGrupoTicket");

    parameters.put("pIdGrupoTicket", new Long(idGrupoTicket));

    Collection colGrupoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colGrupoTicket.iterator();
    return (GrupoTicket)iterator.next();
  }

  public Collection findGrupoTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent grupoTicketExtent = pm.getExtent(
      GrupoTicket.class, true);
    Query query = pm.newQuery(grupoTicketExtent);
    query.setOrdering("codGrupoTicket ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodGrupoTicket(int codGrupoTicket, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codGrupoTicket == pCodGrupoTicket &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoTicket.class, filter);

    query.declareParameters("int pCodGrupoTicket, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pCodGrupoTicket", new Integer(codGrupoTicket));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codGrupoTicket ascending");

    Collection colGrupoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoTicket);

    return colGrupoTicket;
  }

  public Collection findByNombre(String nombre, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre) &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoTicket.class, filter);

    query.declareParameters("java.lang.String pNombre, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codGrupoTicket ascending");

    Collection colGrupoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoTicket);

    return colGrupoTicket;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(GrupoTicket.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("codGrupoTicket ascending");

    Collection colGrupoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colGrupoTicket);

    return colGrupoTicket;
  }
}