package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class SeguridadTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addSeguridadTicket(SeguridadTicket seguridadTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    SeguridadTicket seguridadTicketNew = 
      (SeguridadTicket)BeanUtils.cloneBean(
      seguridadTicket);

    GrupoTicketBeanBusiness grupoTicketBeanBusiness = new GrupoTicketBeanBusiness();

    if (seguridadTicketNew.getGrupoTicket() != null) {
      seguridadTicketNew.setGrupoTicket(
        grupoTicketBeanBusiness.findGrupoTicketById(
        seguridadTicketNew.getGrupoTicket().getIdGrupoTicket()));
    }
    pm.makePersistent(seguridadTicketNew);
  }

  public void updateSeguridadTicket(SeguridadTicket seguridadTicket) throws Exception
  {
    SeguridadTicket seguridadTicketModify = 
      findSeguridadTicketById(seguridadTicket.getIdSeguridadTicket());

    GrupoTicketBeanBusiness grupoTicketBeanBusiness = new GrupoTicketBeanBusiness();

    if (seguridadTicket.getGrupoTicket() != null) {
      seguridadTicket.setGrupoTicket(
        grupoTicketBeanBusiness.findGrupoTicketById(
        seguridadTicket.getGrupoTicket().getIdGrupoTicket()));
    }

    BeanUtils.copyProperties(seguridadTicketModify, seguridadTicket);
  }

  public void deleteSeguridadTicket(SeguridadTicket seguridadTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    SeguridadTicket seguridadTicketDelete = 
      findSeguridadTicketById(seguridadTicket.getIdSeguridadTicket());
    pm.deletePersistent(seguridadTicketDelete);
  }

  public SeguridadTicket findSeguridadTicketById(long idSeguridadTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idSeguridadTicket == pIdSeguridadTicket";
    Query query = pm.newQuery(SeguridadTicket.class, filter);

    query.declareParameters("long pIdSeguridadTicket");

    parameters.put("pIdSeguridadTicket", new Long(idSeguridadTicket));

    Collection colSeguridadTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colSeguridadTicket.iterator();
    return (SeguridadTicket)iterator.next();
  }

  public Collection findSeguridadTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent seguridadTicketExtent = pm.getExtent(
      SeguridadTicket.class, true);
    Query query = pm.newQuery(seguridadTicketExtent);
    query.setOrdering("anio ascending, mes ascending, fechaProceso ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByGrupoTicket(long idGrupoTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "grupoTicket.idGrupoTicket == pIdGrupoTicket";

    Query query = pm.newQuery(SeguridadTicket.class, filter);

    query.declareParameters("long pIdGrupoTicket");
    HashMap parameters = new HashMap();

    parameters.put("pIdGrupoTicket", new Long(idGrupoTicket));

    query.setOrdering("anio ascending, mes ascending, fechaProceso ascending");

    Collection colSeguridadTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colSeguridadTicket);

    return colSeguridadTicket;
  }
}