package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class RetroactivoTicketPK
  implements Serializable
{
  public long idRetroactivoTicket;

  public RetroactivoTicketPK()
  {
  }

  public RetroactivoTicketPK(long idRetroactivoTicket)
  {
    this.idRetroactivoTicket = idRetroactivoTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RetroactivoTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RetroactivoTicketPK thatPK)
  {
    return 
      this.idRetroactivoTicket == thatPK.idRetroactivoTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRetroactivoTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRetroactivoTicket);
  }
}