package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class DescuentoTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idDescuentoTicket;
  private int anio;
  private int mes;
  private int referenciaTicket;
  private int referenciaEmbargo;
  private int referenciaAusencia;
  private int descuentoAusencia;
  private int descuentoReposo;
  private int descuentoPermiso;
  private int descuentoVacaciones;
  private int descuentoOtros;
  private String observaciones;
  private String pagado;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "descuentoAusencia", "descuentoOtros", "descuentoPermiso", "descuentoReposo", "descuentoVacaciones", "idDescuentoTicket", "mes", "observaciones", "pagado", "referenciaAusencia", "referenciaEmbargo", "referenciaTicket", "trabajador" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.DescuentoTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new DescuentoTicket());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public DescuentoTicket()
  {
    jdoSetpagado(this, "N");
  }

  public String toString()
  {
    return jdoGetanio(this) + " - " + jdoGetmes(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public int getDescuentoAusencia() {
    return jdoGetdescuentoAusencia(this);
  }
  public void setDescuentoAusencia(int descuentoAusencia) {
    jdoSetdescuentoAusencia(this, descuentoAusencia);
  }
  public int getDescuentoOtros() {
    return jdoGetdescuentoOtros(this);
  }
  public void setDescuentoOtros(int descuentoOtros) {
    jdoSetdescuentoOtros(this, descuentoOtros);
  }
  public int getDescuentoPermiso() {
    return jdoGetdescuentoPermiso(this);
  }
  public void setDescuentoPermiso(int descuentoPermiso) {
    jdoSetdescuentoPermiso(this, descuentoPermiso);
  }
  public int getDescuentoReposo() {
    return jdoGetdescuentoReposo(this);
  }
  public void setDescuentoReposo(int descuentoReposo) {
    jdoSetdescuentoReposo(this, descuentoReposo);
  }
  public int getDescuentoVacaciones() {
    return jdoGetdescuentoVacaciones(this);
  }
  public void setDescuentoVacaciones(int descuentoVacaciones) {
    jdoSetdescuentoVacaciones(this, descuentoVacaciones);
  }
  public long getIdDescuentoTicket() {
    return jdoGetidDescuentoTicket(this);
  }
  public void setIdDescuentoTicket(long idDescuentoTicket) {
    jdoSetidDescuentoTicket(this, idDescuentoTicket);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public String getPagado() {
    return jdoGetpagado(this);
  }
  public void setPagado(String pagado) {
    jdoSetpagado(this, pagado);
  }
  public int getReferenciaEmbargo() {
    return jdoGetreferenciaEmbargo(this);
  }
  public void setReferenciaEmbargo(int referenciaEmbargo) {
    jdoSetreferenciaEmbargo(this, referenciaEmbargo);
  }
  public int getReferenciaTicket() {
    return jdoGetreferenciaTicket(this);
  }
  public void setReferenciaTicket(int referenciaTicket) {
    jdoSetreferenciaTicket(this, referenciaTicket);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }
  public int getReferenciaAusencia() {
    return jdoGetreferenciaAusencia(this);
  }
  public void setReferenciaAusencia(int referenciaAusencia) {
    jdoSetreferenciaAusencia(this, referenciaAusencia);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    DescuentoTicket localDescuentoTicket = new DescuentoTicket();
    localDescuentoTicket.jdoFlags = 1;
    localDescuentoTicket.jdoStateManager = paramStateManager;
    return localDescuentoTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    DescuentoTicket localDescuentoTicket = new DescuentoTicket();
    localDescuentoTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localDescuentoTicket.jdoFlags = 1;
    localDescuentoTicket.jdoStateManager = paramStateManager;
    return localDescuentoTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.descuentoAusencia);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.descuentoOtros);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.descuentoPermiso);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.descuentoReposo);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.descuentoVacaciones);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idDescuentoTicket);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagado);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.referenciaAusencia);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.referenciaEmbargo);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.referenciaTicket);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descuentoAusencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descuentoOtros = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descuentoPermiso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descuentoReposo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descuentoVacaciones = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idDescuentoTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.referenciaAusencia = localStateManager.replacingIntField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.referenciaEmbargo = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.referenciaTicket = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramDescuentoTicket.anio;
      return;
    case 1:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.descuentoAusencia = paramDescuentoTicket.descuentoAusencia;
      return;
    case 2:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.descuentoOtros = paramDescuentoTicket.descuentoOtros;
      return;
    case 3:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.descuentoPermiso = paramDescuentoTicket.descuentoPermiso;
      return;
    case 4:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.descuentoReposo = paramDescuentoTicket.descuentoReposo;
      return;
    case 5:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.descuentoVacaciones = paramDescuentoTicket.descuentoVacaciones;
      return;
    case 6:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idDescuentoTicket = paramDescuentoTicket.idDescuentoTicket;
      return;
    case 7:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramDescuentoTicket.mes;
      return;
    case 8:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramDescuentoTicket.observaciones;
      return;
    case 9:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.pagado = paramDescuentoTicket.pagado;
      return;
    case 10:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.referenciaAusencia = paramDescuentoTicket.referenciaAusencia;
      return;
    case 11:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.referenciaEmbargo = paramDescuentoTicket.referenciaEmbargo;
      return;
    case 12:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.referenciaTicket = paramDescuentoTicket.referenciaTicket;
      return;
    case 13:
      if (paramDescuentoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramDescuentoTicket.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof DescuentoTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    DescuentoTicket localDescuentoTicket = (DescuentoTicket)paramObject;
    if (localDescuentoTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localDescuentoTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new DescuentoTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new DescuentoTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DescuentoTicketPK))
      throw new IllegalArgumentException("arg1");
    DescuentoTicketPK localDescuentoTicketPK = (DescuentoTicketPK)paramObject;
    localDescuentoTicketPK.idDescuentoTicket = this.idDescuentoTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof DescuentoTicketPK))
      throw new IllegalArgumentException("arg1");
    DescuentoTicketPK localDescuentoTicketPK = (DescuentoTicketPK)paramObject;
    this.idDescuentoTicket = localDescuentoTicketPK.idDescuentoTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DescuentoTicketPK))
      throw new IllegalArgumentException("arg2");
    DescuentoTicketPK localDescuentoTicketPK = (DescuentoTicketPK)paramObject;
    localDescuentoTicketPK.idDescuentoTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 6);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof DescuentoTicketPK))
      throw new IllegalArgumentException("arg2");
    DescuentoTicketPK localDescuentoTicketPK = (DescuentoTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 6, localDescuentoTicketPK.idDescuentoTicket);
  }

  private static final int jdoGetanio(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.anio;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.anio;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 0))
      return paramDescuentoTicket.anio;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 0, paramDescuentoTicket.anio);
  }

  private static final void jdoSetanio(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 0, paramDescuentoTicket.anio, paramInt);
  }

  private static final int jdoGetdescuentoAusencia(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.descuentoAusencia;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.descuentoAusencia;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 1))
      return paramDescuentoTicket.descuentoAusencia;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 1, paramDescuentoTicket.descuentoAusencia);
  }

  private static final void jdoSetdescuentoAusencia(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.descuentoAusencia = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.descuentoAusencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 1, paramDescuentoTicket.descuentoAusencia, paramInt);
  }

  private static final int jdoGetdescuentoOtros(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.descuentoOtros;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.descuentoOtros;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 2))
      return paramDescuentoTicket.descuentoOtros;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 2, paramDescuentoTicket.descuentoOtros);
  }

  private static final void jdoSetdescuentoOtros(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.descuentoOtros = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.descuentoOtros = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 2, paramDescuentoTicket.descuentoOtros, paramInt);
  }

  private static final int jdoGetdescuentoPermiso(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.descuentoPermiso;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.descuentoPermiso;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 3))
      return paramDescuentoTicket.descuentoPermiso;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 3, paramDescuentoTicket.descuentoPermiso);
  }

  private static final void jdoSetdescuentoPermiso(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.descuentoPermiso = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.descuentoPermiso = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 3, paramDescuentoTicket.descuentoPermiso, paramInt);
  }

  private static final int jdoGetdescuentoReposo(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.descuentoReposo;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.descuentoReposo;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 4))
      return paramDescuentoTicket.descuentoReposo;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 4, paramDescuentoTicket.descuentoReposo);
  }

  private static final void jdoSetdescuentoReposo(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.descuentoReposo = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.descuentoReposo = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 4, paramDescuentoTicket.descuentoReposo, paramInt);
  }

  private static final int jdoGetdescuentoVacaciones(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.descuentoVacaciones;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.descuentoVacaciones;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 5))
      return paramDescuentoTicket.descuentoVacaciones;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 5, paramDescuentoTicket.descuentoVacaciones);
  }

  private static final void jdoSetdescuentoVacaciones(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.descuentoVacaciones = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.descuentoVacaciones = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 5, paramDescuentoTicket.descuentoVacaciones, paramInt);
  }

  private static final long jdoGetidDescuentoTicket(DescuentoTicket paramDescuentoTicket)
  {
    return paramDescuentoTicket.idDescuentoTicket;
  }

  private static final void jdoSetidDescuentoTicket(DescuentoTicket paramDescuentoTicket, long paramLong)
  {
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.idDescuentoTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramDescuentoTicket, jdoInheritedFieldCount + 6, paramDescuentoTicket.idDescuentoTicket, paramLong);
  }

  private static final int jdoGetmes(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.mes;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.mes;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 7))
      return paramDescuentoTicket.mes;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 7, paramDescuentoTicket.mes);
  }

  private static final void jdoSetmes(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 7, paramDescuentoTicket.mes, paramInt);
  }

  private static final String jdoGetobservaciones(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.observaciones;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.observaciones;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 8))
      return paramDescuentoTicket.observaciones;
    return localStateManager.getStringField(paramDescuentoTicket, jdoInheritedFieldCount + 8, paramDescuentoTicket.observaciones);
  }

  private static final void jdoSetobservaciones(DescuentoTicket paramDescuentoTicket, String paramString)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramDescuentoTicket, jdoInheritedFieldCount + 8, paramDescuentoTicket.observaciones, paramString);
  }

  private static final String jdoGetpagado(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.pagado;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.pagado;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 9))
      return paramDescuentoTicket.pagado;
    return localStateManager.getStringField(paramDescuentoTicket, jdoInheritedFieldCount + 9, paramDescuentoTicket.pagado);
  }

  private static final void jdoSetpagado(DescuentoTicket paramDescuentoTicket, String paramString)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.pagado = paramString;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.pagado = paramString;
      return;
    }
    localStateManager.setStringField(paramDescuentoTicket, jdoInheritedFieldCount + 9, paramDescuentoTicket.pagado, paramString);
  }

  private static final int jdoGetreferenciaAusencia(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.referenciaAusencia;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.referenciaAusencia;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 10))
      return paramDescuentoTicket.referenciaAusencia;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 10, paramDescuentoTicket.referenciaAusencia);
  }

  private static final void jdoSetreferenciaAusencia(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.referenciaAusencia = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.referenciaAusencia = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 10, paramDescuentoTicket.referenciaAusencia, paramInt);
  }

  private static final int jdoGetreferenciaEmbargo(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.referenciaEmbargo;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.referenciaEmbargo;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 11))
      return paramDescuentoTicket.referenciaEmbargo;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 11, paramDescuentoTicket.referenciaEmbargo);
  }

  private static final void jdoSetreferenciaEmbargo(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.referenciaEmbargo = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.referenciaEmbargo = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 11, paramDescuentoTicket.referenciaEmbargo, paramInt);
  }

  private static final int jdoGetreferenciaTicket(DescuentoTicket paramDescuentoTicket)
  {
    if (paramDescuentoTicket.jdoFlags <= 0)
      return paramDescuentoTicket.referenciaTicket;
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.referenciaTicket;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 12))
      return paramDescuentoTicket.referenciaTicket;
    return localStateManager.getIntField(paramDescuentoTicket, jdoInheritedFieldCount + 12, paramDescuentoTicket.referenciaTicket);
  }

  private static final void jdoSetreferenciaTicket(DescuentoTicket paramDescuentoTicket, int paramInt)
  {
    if (paramDescuentoTicket.jdoFlags == 0)
    {
      paramDescuentoTicket.referenciaTicket = paramInt;
      return;
    }
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.referenciaTicket = paramInt;
      return;
    }
    localStateManager.setIntField(paramDescuentoTicket, jdoInheritedFieldCount + 12, paramDescuentoTicket.referenciaTicket, paramInt);
  }

  private static final Trabajador jdoGettrabajador(DescuentoTicket paramDescuentoTicket)
  {
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramDescuentoTicket.trabajador;
    if (localStateManager.isLoaded(paramDescuentoTicket, jdoInheritedFieldCount + 13))
      return paramDescuentoTicket.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramDescuentoTicket, jdoInheritedFieldCount + 13, paramDescuentoTicket.trabajador);
  }

  private static final void jdoSettrabajador(DescuentoTicket paramDescuentoTicket, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramDescuentoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramDescuentoTicket.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramDescuentoTicket, jdoInheritedFieldCount + 13, paramDescuentoTicket.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}