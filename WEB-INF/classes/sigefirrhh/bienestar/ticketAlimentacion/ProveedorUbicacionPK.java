package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class ProveedorUbicacionPK
  implements Serializable
{
  public long idProveedorUbicacion;

  public ProveedorUbicacionPK()
  {
  }

  public ProveedorUbicacionPK(long idProveedorUbicacion)
  {
    this.idProveedorUbicacion = idProveedorUbicacion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ProveedorUbicacionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ProveedorUbicacionPK thatPK)
  {
    return 
      this.idProveedorUbicacion == thatPK.idProveedorUbicacion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idProveedorUbicacion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idProveedorUbicacion);
  }
}