package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class GrupoTicketPK
  implements Serializable
{
  public long idGrupoTicket;

  public GrupoTicketPK()
  {
  }

  public GrupoTicketPK(long idGrupoTicket)
  {
    this.idGrupoTicket = idGrupoTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((GrupoTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(GrupoTicketPK thatPK)
  {
    return 
      this.idGrupoTicket == thatPK.idGrupoTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idGrupoTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idGrupoTicket);
  }
}