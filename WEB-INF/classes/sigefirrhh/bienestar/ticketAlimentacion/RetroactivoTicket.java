package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.personal.trabajador.Trabajador;

public class RetroactivoTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idRetroactivoTicket;
  private int anio;
  private int mes;
  private int mesProceso;
  private int anioProceso;
  private int numeroTickets;
  private String observaciones;
  private String pagado;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "anioProceso", "idRetroactivoTicket", "mes", "mesProceso", "numeroTickets", "observaciones", "pagado", "trabajador" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21, 21, 21, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.RetroactivoTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RetroactivoTicket());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public RetroactivoTicket()
  {
    jdoSetnumeroTickets(this, 0);

    jdoSetpagado(this, "N");
  }

  public String toString()
  {
    return jdoGetanio(this) + " - " + jdoGetmes(this) + "  -  " + 
      jdoGetnumeroTickets(this);
  }

  public int getAnio()
  {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public int getAnioProceso() {
    return jdoGetanioProceso(this);
  }
  public void setAnioProceso(int anioProceso) {
    jdoSetanioProceso(this, anioProceso);
  }
  public long getIdRetroactivoTicket() {
    return jdoGetidRetroactivoTicket(this);
  }
  public void setIdRetroactivoTicket(long idRetroactivoTicket) {
    jdoSetidRetroactivoTicket(this, idRetroactivoTicket);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }
  public int getMesProceso() {
    return jdoGetmesProceso(this);
  }
  public void setMesProceso(int mesProceso) {
    jdoSetmesProceso(this, mesProceso);
  }
  public int getNumeroTickets() {
    return jdoGetnumeroTickets(this);
  }
  public void setNumeroTickets(int numeroTickets) {
    jdoSetnumeroTickets(this, numeroTickets);
  }
  public String getObservaciones() {
    return jdoGetobservaciones(this);
  }
  public void setObservaciones(String observaciones) {
    jdoSetobservaciones(this, observaciones);
  }
  public String getPagado() {
    return jdoGetpagado(this);
  }
  public void setPagado(String pagado) {
    jdoSetpagado(this, pagado);
  }
  public Trabajador getTrabajador() {
    return jdoGettrabajador(this);
  }
  public void setTrabajador(Trabajador trabajador) {
    jdoSettrabajador(this, trabajador);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 9;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RetroactivoTicket localRetroactivoTicket = new RetroactivoTicket();
    localRetroactivoTicket.jdoFlags = 1;
    localRetroactivoTicket.jdoStateManager = paramStateManager;
    return localRetroactivoTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RetroactivoTicket localRetroactivoTicket = new RetroactivoTicket();
    localRetroactivoTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRetroactivoTicket.jdoFlags = 1;
    localRetroactivoTicket.jdoStateManager = paramStateManager;
    return localRetroactivoTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioProceso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRetroactivoTicket);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesProceso);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.numeroTickets);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.observaciones);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagado);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioProceso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRetroactivoTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesProceso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.numeroTickets = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.observaciones = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RetroactivoTicket paramRetroactivoTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramRetroactivoTicket.anio;
      return;
    case 1:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.anioProceso = paramRetroactivoTicket.anioProceso;
      return;
    case 2:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idRetroactivoTicket = paramRetroactivoTicket.idRetroactivoTicket;
      return;
    case 3:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramRetroactivoTicket.mes;
      return;
    case 4:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.mesProceso = paramRetroactivoTicket.mesProceso;
      return;
    case 5:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.numeroTickets = paramRetroactivoTicket.numeroTickets;
      return;
    case 6:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.observaciones = paramRetroactivoTicket.observaciones;
      return;
    case 7:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.pagado = paramRetroactivoTicket.pagado;
      return;
    case 8:
      if (paramRetroactivoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramRetroactivoTicket.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RetroactivoTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RetroactivoTicket localRetroactivoTicket = (RetroactivoTicket)paramObject;
    if (localRetroactivoTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRetroactivoTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RetroactivoTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RetroactivoTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RetroactivoTicketPK))
      throw new IllegalArgumentException("arg1");
    RetroactivoTicketPK localRetroactivoTicketPK = (RetroactivoTicketPK)paramObject;
    localRetroactivoTicketPK.idRetroactivoTicket = this.idRetroactivoTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RetroactivoTicketPK))
      throw new IllegalArgumentException("arg1");
    RetroactivoTicketPK localRetroactivoTicketPK = (RetroactivoTicketPK)paramObject;
    this.idRetroactivoTicket = localRetroactivoTicketPK.idRetroactivoTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RetroactivoTicketPK))
      throw new IllegalArgumentException("arg2");
    RetroactivoTicketPK localRetroactivoTicketPK = (RetroactivoTicketPK)paramObject;
    localRetroactivoTicketPK.idRetroactivoTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RetroactivoTicketPK))
      throw new IllegalArgumentException("arg2");
    RetroactivoTicketPK localRetroactivoTicketPK = (RetroactivoTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localRetroactivoTicketPK.idRetroactivoTicket);
  }

  private static final int jdoGetanio(RetroactivoTicket paramRetroactivoTicket)
  {
    if (paramRetroactivoTicket.jdoFlags <= 0)
      return paramRetroactivoTicket.anio;
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.anio;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 0))
      return paramRetroactivoTicket.anio;
    return localStateManager.getIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 0, paramRetroactivoTicket.anio);
  }

  private static final void jdoSetanio(RetroactivoTicket paramRetroactivoTicket, int paramInt)
  {
    if (paramRetroactivoTicket.jdoFlags == 0)
    {
      paramRetroactivoTicket.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 0, paramRetroactivoTicket.anio, paramInt);
  }

  private static final int jdoGetanioProceso(RetroactivoTicket paramRetroactivoTicket)
  {
    if (paramRetroactivoTicket.jdoFlags <= 0)
      return paramRetroactivoTicket.anioProceso;
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.anioProceso;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 1))
      return paramRetroactivoTicket.anioProceso;
    return localStateManager.getIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 1, paramRetroactivoTicket.anioProceso);
  }

  private static final void jdoSetanioProceso(RetroactivoTicket paramRetroactivoTicket, int paramInt)
  {
    if (paramRetroactivoTicket.jdoFlags == 0)
    {
      paramRetroactivoTicket.anioProceso = paramInt;
      return;
    }
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.anioProceso = paramInt;
      return;
    }
    localStateManager.setIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 1, paramRetroactivoTicket.anioProceso, paramInt);
  }

  private static final long jdoGetidRetroactivoTicket(RetroactivoTicket paramRetroactivoTicket)
  {
    return paramRetroactivoTicket.idRetroactivoTicket;
  }

  private static final void jdoSetidRetroactivoTicket(RetroactivoTicket paramRetroactivoTicket, long paramLong)
  {
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.idRetroactivoTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramRetroactivoTicket, jdoInheritedFieldCount + 2, paramRetroactivoTicket.idRetroactivoTicket, paramLong);
  }

  private static final int jdoGetmes(RetroactivoTicket paramRetroactivoTicket)
  {
    if (paramRetroactivoTicket.jdoFlags <= 0)
      return paramRetroactivoTicket.mes;
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.mes;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 3))
      return paramRetroactivoTicket.mes;
    return localStateManager.getIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 3, paramRetroactivoTicket.mes);
  }

  private static final void jdoSetmes(RetroactivoTicket paramRetroactivoTicket, int paramInt)
  {
    if (paramRetroactivoTicket.jdoFlags == 0)
    {
      paramRetroactivoTicket.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 3, paramRetroactivoTicket.mes, paramInt);
  }

  private static final int jdoGetmesProceso(RetroactivoTicket paramRetroactivoTicket)
  {
    if (paramRetroactivoTicket.jdoFlags <= 0)
      return paramRetroactivoTicket.mesProceso;
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.mesProceso;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 4))
      return paramRetroactivoTicket.mesProceso;
    return localStateManager.getIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 4, paramRetroactivoTicket.mesProceso);
  }

  private static final void jdoSetmesProceso(RetroactivoTicket paramRetroactivoTicket, int paramInt)
  {
    if (paramRetroactivoTicket.jdoFlags == 0)
    {
      paramRetroactivoTicket.mesProceso = paramInt;
      return;
    }
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.mesProceso = paramInt;
      return;
    }
    localStateManager.setIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 4, paramRetroactivoTicket.mesProceso, paramInt);
  }

  private static final int jdoGetnumeroTickets(RetroactivoTicket paramRetroactivoTicket)
  {
    if (paramRetroactivoTicket.jdoFlags <= 0)
      return paramRetroactivoTicket.numeroTickets;
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.numeroTickets;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 5))
      return paramRetroactivoTicket.numeroTickets;
    return localStateManager.getIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 5, paramRetroactivoTicket.numeroTickets);
  }

  private static final void jdoSetnumeroTickets(RetroactivoTicket paramRetroactivoTicket, int paramInt)
  {
    if (paramRetroactivoTicket.jdoFlags == 0)
    {
      paramRetroactivoTicket.numeroTickets = paramInt;
      return;
    }
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.numeroTickets = paramInt;
      return;
    }
    localStateManager.setIntField(paramRetroactivoTicket, jdoInheritedFieldCount + 5, paramRetroactivoTicket.numeroTickets, paramInt);
  }

  private static final String jdoGetobservaciones(RetroactivoTicket paramRetroactivoTicket)
  {
    if (paramRetroactivoTicket.jdoFlags <= 0)
      return paramRetroactivoTicket.observaciones;
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.observaciones;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 6))
      return paramRetroactivoTicket.observaciones;
    return localStateManager.getStringField(paramRetroactivoTicket, jdoInheritedFieldCount + 6, paramRetroactivoTicket.observaciones);
  }

  private static final void jdoSetobservaciones(RetroactivoTicket paramRetroactivoTicket, String paramString)
  {
    if (paramRetroactivoTicket.jdoFlags == 0)
    {
      paramRetroactivoTicket.observaciones = paramString;
      return;
    }
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.observaciones = paramString;
      return;
    }
    localStateManager.setStringField(paramRetroactivoTicket, jdoInheritedFieldCount + 6, paramRetroactivoTicket.observaciones, paramString);
  }

  private static final String jdoGetpagado(RetroactivoTicket paramRetroactivoTicket)
  {
    if (paramRetroactivoTicket.jdoFlags <= 0)
      return paramRetroactivoTicket.pagado;
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.pagado;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 7))
      return paramRetroactivoTicket.pagado;
    return localStateManager.getStringField(paramRetroactivoTicket, jdoInheritedFieldCount + 7, paramRetroactivoTicket.pagado);
  }

  private static final void jdoSetpagado(RetroactivoTicket paramRetroactivoTicket, String paramString)
  {
    if (paramRetroactivoTicket.jdoFlags == 0)
    {
      paramRetroactivoTicket.pagado = paramString;
      return;
    }
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.pagado = paramString;
      return;
    }
    localStateManager.setStringField(paramRetroactivoTicket, jdoInheritedFieldCount + 7, paramRetroactivoTicket.pagado, paramString);
  }

  private static final Trabajador jdoGettrabajador(RetroactivoTicket paramRetroactivoTicket)
  {
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramRetroactivoTicket.trabajador;
    if (localStateManager.isLoaded(paramRetroactivoTicket, jdoInheritedFieldCount + 8))
      return paramRetroactivoTicket.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramRetroactivoTicket, jdoInheritedFieldCount + 8, paramRetroactivoTicket.trabajador);
  }

  private static final void jdoSettrabajador(RetroactivoTicket paramRetroactivoTicket, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramRetroactivoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramRetroactivoTicket.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramRetroactivoTicket, jdoInheritedFieldCount + 8, paramRetroactivoTicket.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}