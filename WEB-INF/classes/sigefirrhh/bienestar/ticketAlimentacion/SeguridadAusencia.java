package sigefirrhh.bienestar.ticketAlimentacion;

import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SeguridadAusencia
  implements PersistenceCapable
{
  private long idSeguridadAusencia;
  private int anio;
  private int mes;
  private GrupoTicket grupoTicket;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "anio", "grupoTicket", "idSeguridadAusencia", "mes" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket"), Long.TYPE, Integer.TYPE };
  private static final byte[] jdoFieldFlags = { 21, 26, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public SeguridadAusencia()
  {
    jdoSetanio(this, 0);
    jdoSetmes(this, 0);
  }

  public GrupoTicket getGrupoTicket()
  {
    return jdoGetgrupoTicket(this);
  }
  public void setGrupoTicket(GrupoTicket grupoTicket) {
    jdoSetgrupoTicket(this, grupoTicket);
  }
  public int getAnio() {
    return jdoGetanio(this);
  }
  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }
  public int getMes() {
    return jdoGetmes(this);
  }
  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }

  public long getIdSeguridadAusencia()
  {
    return jdoGetidSeguridadAusencia(this);
  }
  public void setIdSeguridadAusencia(long idSeguridadAusencia) {
    jdoSetidSeguridadAusencia(this, idSeguridadAusencia);
  }
  public String toString() {
    return jdoGetidSeguridadAusencia(this) + "-" + 
      jdoGetanio(this) + "/" + jdoGetmes(this);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.SeguridadAusencia"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadAusencia());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadAusencia localSeguridadAusencia = new SeguridadAusencia();
    localSeguridadAusencia.jdoFlags = 1;
    localSeguridadAusencia.jdoStateManager = paramStateManager;
    return localSeguridadAusencia;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadAusencia localSeguridadAusencia = new SeguridadAusencia();
    localSeguridadAusencia.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadAusencia.jdoFlags = 1;
    localSeguridadAusencia.jdoStateManager = paramStateManager;
    return localSeguridadAusencia;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoTicket);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadAusencia);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoTicket = ((GrupoTicket)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadAusencia = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadAusencia paramSeguridadAusencia, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadAusencia.anio;
      return;
    case 1:
      if (paramSeguridadAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.grupoTicket = paramSeguridadAusencia.grupoTicket;
      return;
    case 2:
      if (paramSeguridadAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadAusencia = paramSeguridadAusencia.idSeguridadAusencia;
      return;
    case 3:
      if (paramSeguridadAusencia == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadAusencia.mes;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadAusencia))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadAusencia localSeguridadAusencia = (SeguridadAusencia)paramObject;
    if (localSeguridadAusencia.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadAusencia, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadAusenciaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadAusenciaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadAusenciaPK))
      throw new IllegalArgumentException("arg1");
    SeguridadAusenciaPK localSeguridadAusenciaPK = (SeguridadAusenciaPK)paramObject;
    localSeguridadAusenciaPK.idSeguridadAusencia = this.idSeguridadAusencia;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadAusenciaPK))
      throw new IllegalArgumentException("arg1");
    SeguridadAusenciaPK localSeguridadAusenciaPK = (SeguridadAusenciaPK)paramObject;
    this.idSeguridadAusencia = localSeguridadAusenciaPK.idSeguridadAusencia;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadAusenciaPK))
      throw new IllegalArgumentException("arg2");
    SeguridadAusenciaPK localSeguridadAusenciaPK = (SeguridadAusenciaPK)paramObject;
    localSeguridadAusenciaPK.idSeguridadAusencia = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadAusenciaPK))
      throw new IllegalArgumentException("arg2");
    SeguridadAusenciaPK localSeguridadAusenciaPK = (SeguridadAusenciaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localSeguridadAusenciaPK.idSeguridadAusencia);
  }

  private static final int jdoGetanio(SeguridadAusencia paramSeguridadAusencia)
  {
    if (paramSeguridadAusencia.jdoFlags <= 0)
      return paramSeguridadAusencia.anio;
    StateManager localStateManager = paramSeguridadAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAusencia.anio;
    if (localStateManager.isLoaded(paramSeguridadAusencia, jdoInheritedFieldCount + 0))
      return paramSeguridadAusencia.anio;
    return localStateManager.getIntField(paramSeguridadAusencia, jdoInheritedFieldCount + 0, paramSeguridadAusencia.anio);
  }

  private static final void jdoSetanio(SeguridadAusencia paramSeguridadAusencia, int paramInt)
  {
    if (paramSeguridadAusencia.jdoFlags == 0)
    {
      paramSeguridadAusencia.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAusencia.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadAusencia, jdoInheritedFieldCount + 0, paramSeguridadAusencia.anio, paramInt);
  }

  private static final GrupoTicket jdoGetgrupoTicket(SeguridadAusencia paramSeguridadAusencia)
  {
    StateManager localStateManager = paramSeguridadAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAusencia.grupoTicket;
    if (localStateManager.isLoaded(paramSeguridadAusencia, jdoInheritedFieldCount + 1))
      return paramSeguridadAusencia.grupoTicket;
    return (GrupoTicket)localStateManager.getObjectField(paramSeguridadAusencia, jdoInheritedFieldCount + 1, paramSeguridadAusencia.grupoTicket);
  }

  private static final void jdoSetgrupoTicket(SeguridadAusencia paramSeguridadAusencia, GrupoTicket paramGrupoTicket)
  {
    StateManager localStateManager = paramSeguridadAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAusencia.grupoTicket = paramGrupoTicket;
      return;
    }
    localStateManager.setObjectField(paramSeguridadAusencia, jdoInheritedFieldCount + 1, paramSeguridadAusencia.grupoTicket, paramGrupoTicket);
  }

  private static final long jdoGetidSeguridadAusencia(SeguridadAusencia paramSeguridadAusencia)
  {
    return paramSeguridadAusencia.idSeguridadAusencia;
  }

  private static final void jdoSetidSeguridadAusencia(SeguridadAusencia paramSeguridadAusencia, long paramLong)
  {
    StateManager localStateManager = paramSeguridadAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAusencia.idSeguridadAusencia = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadAusencia, jdoInheritedFieldCount + 2, paramSeguridadAusencia.idSeguridadAusencia, paramLong);
  }

  private static final int jdoGetmes(SeguridadAusencia paramSeguridadAusencia)
  {
    if (paramSeguridadAusencia.jdoFlags <= 0)
      return paramSeguridadAusencia.mes;
    StateManager localStateManager = paramSeguridadAusencia.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadAusencia.mes;
    if (localStateManager.isLoaded(paramSeguridadAusencia, jdoInheritedFieldCount + 3))
      return paramSeguridadAusencia.mes;
    return localStateManager.getIntField(paramSeguridadAusencia, jdoInheritedFieldCount + 3, paramSeguridadAusencia.mes);
  }

  private static final void jdoSetmes(SeguridadAusencia paramSeguridadAusencia, int paramInt)
  {
    if (paramSeguridadAusencia.jdoFlags == 0)
    {
      paramSeguridadAusencia.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadAusencia.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadAusencia.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadAusencia, jdoInheritedFieldCount + 3, paramSeguridadAusencia.mes, paramInt);
  }
}