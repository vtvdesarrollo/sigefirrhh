package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class DescuentoTicketBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addDescuentoTicket(DescuentoTicket descuentoTicket)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    DescuentoTicket descuentoTicketNew = 
      (DescuentoTicket)BeanUtils.cloneBean(
      descuentoTicket);

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (descuentoTicketNew.getTrabajador() != null) {
      descuentoTicketNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        descuentoTicketNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(descuentoTicketNew);
  }

  public void updateDescuentoTicket(DescuentoTicket descuentoTicket) throws Exception
  {
    DescuentoTicket descuentoTicketModify = 
      findDescuentoTicketById(descuentoTicket.getIdDescuentoTicket());

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (descuentoTicket.getTrabajador() != null) {
      descuentoTicket.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        descuentoTicket.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(descuentoTicketModify, descuentoTicket);
  }

  public void deleteDescuentoTicket(DescuentoTicket descuentoTicket) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    DescuentoTicket descuentoTicketDelete = 
      findDescuentoTicketById(descuentoTicket.getIdDescuentoTicket());
    pm.deletePersistent(descuentoTicketDelete);
  }

  public DescuentoTicket findDescuentoTicketById(long idDescuentoTicket) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idDescuentoTicket == pIdDescuentoTicket";
    Query query = pm.newQuery(DescuentoTicket.class, filter);

    query.declareParameters("long pIdDescuentoTicket");

    parameters.put("pIdDescuentoTicket", new Long(idDescuentoTicket));

    Collection colDescuentoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colDescuentoTicket.iterator();
    return (DescuentoTicket)iterator.next();
  }

  public Collection findDescuentoTicketAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent descuentoTicketExtent = pm.getExtent(
      DescuentoTicket.class, true);
    Query query = pm.newQuery(descuentoTicketExtent);
    query.setOrdering("mes ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByTrabajador(long idTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "trabajador.idTrabajador == pIdTrabajador";

    Query query = pm.newQuery(DescuentoTicket.class, filter);

    query.declareParameters("long pIdTrabajador");
    HashMap parameters = new HashMap();

    parameters.put("pIdTrabajador", new Long(idTrabajador));

    query.setOrdering("mes ascending");

    Collection colDescuentoTicket = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colDescuentoTicket);

    return colDescuentoTicket;
  }
}