package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.Serializable;

public class HistoricoTicketPK
  implements Serializable
{
  public long idHistoricoTicket;

  public HistoricoTicketPK()
  {
  }

  public HistoricoTicketPK(long idHistoricoTicket)
  {
    this.idHistoricoTicket = idHistoricoTicket;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((HistoricoTicketPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(HistoricoTicketPK thatPK)
  {
    return 
      this.idHistoricoTicket == thatPK.idHistoricoTicket;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idHistoricoTicket)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idHistoricoTicket);
  }
}