package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class SeguridadTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idSeguridadTicket;
  private int anio;
  private int mes;
  private String especial;
  private Date fechaProceso;
  private GrupoTicket grupoTicket;
  private String usuario;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "especial", "fechaProceso", "grupoTicket", "idSeguridadTicket", "mes", "usuario" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.GrupoTicket"), Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 26, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.SeguridadTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new SeguridadTicket());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public SeguridadTicket()
  {
    jdoSetespecial(this, "N");
  }

  public String toString()
  {
    return jdoGetgrupoTicket(this).toString();
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio) {
    jdoSetanio(this, anio);
  }

  public String getEspecial() {
    return jdoGetespecial(this);
  }

  public void setEspecial(String especial) {
    jdoSetespecial(this, especial);
  }

  public Date getFechaProceso() {
    return jdoGetfechaProceso(this);
  }

  public void setFechaProceso(Date fechaProceso) {
    jdoSetfechaProceso(this, fechaProceso);
  }

  public long getIdSeguridadTicket() {
    return jdoGetidSeguridadTicket(this);
  }

  public void setIdSeguridadTicket(long idSeguridadTicket) {
    jdoSetidSeguridadTicket(this, idSeguridadTicket);
  }

  public int getMes() {
    return jdoGetmes(this);
  }

  public void setMes(int mes) {
    jdoSetmes(this, mes);
  }

  public String getUsuario() {
    return jdoGetusuario(this);
  }

  public void setUsuario(String usuario) {
    jdoSetusuario(this, usuario);
  }

  public GrupoTicket getGrupoTicket() {
    return jdoGetgrupoTicket(this);
  }

  public void setGrupoTicket(GrupoTicket grupoTicket) {
    jdoSetgrupoTicket(this, grupoTicket);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 7;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    SeguridadTicket localSeguridadTicket = new SeguridadTicket();
    localSeguridadTicket.jdoFlags = 1;
    localSeguridadTicket.jdoStateManager = paramStateManager;
    return localSeguridadTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    SeguridadTicket localSeguridadTicket = new SeguridadTicket();
    localSeguridadTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localSeguridadTicket.jdoFlags = 1;
    localSeguridadTicket.jdoStateManager = paramStateManager;
    return localSeguridadTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.especial);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.grupoTicket);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idSeguridadTicket);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.especial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grupoTicket = ((GrupoTicket)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idSeguridadTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(SeguridadTicket paramSeguridadTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramSeguridadTicket == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramSeguridadTicket.anio;
      return;
    case 1:
      if (paramSeguridadTicket == null)
        throw new IllegalArgumentException("arg1");
      this.especial = paramSeguridadTicket.especial;
      return;
    case 2:
      if (paramSeguridadTicket == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramSeguridadTicket.fechaProceso;
      return;
    case 3:
      if (paramSeguridadTicket == null)
        throw new IllegalArgumentException("arg1");
      this.grupoTicket = paramSeguridadTicket.grupoTicket;
      return;
    case 4:
      if (paramSeguridadTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idSeguridadTicket = paramSeguridadTicket.idSeguridadTicket;
      return;
    case 5:
      if (paramSeguridadTicket == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramSeguridadTicket.mes;
      return;
    case 6:
      if (paramSeguridadTicket == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramSeguridadTicket.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof SeguridadTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    SeguridadTicket localSeguridadTicket = (SeguridadTicket)paramObject;
    if (localSeguridadTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localSeguridadTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new SeguridadTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new SeguridadTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadTicketPK))
      throw new IllegalArgumentException("arg1");
    SeguridadTicketPK localSeguridadTicketPK = (SeguridadTicketPK)paramObject;
    localSeguridadTicketPK.idSeguridadTicket = this.idSeguridadTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof SeguridadTicketPK))
      throw new IllegalArgumentException("arg1");
    SeguridadTicketPK localSeguridadTicketPK = (SeguridadTicketPK)paramObject;
    this.idSeguridadTicket = localSeguridadTicketPK.idSeguridadTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadTicketPK))
      throw new IllegalArgumentException("arg2");
    SeguridadTicketPK localSeguridadTicketPK = (SeguridadTicketPK)paramObject;
    localSeguridadTicketPK.idSeguridadTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof SeguridadTicketPK))
      throw new IllegalArgumentException("arg2");
    SeguridadTicketPK localSeguridadTicketPK = (SeguridadTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localSeguridadTicketPK.idSeguridadTicket);
  }

  private static final int jdoGetanio(SeguridadTicket paramSeguridadTicket)
  {
    if (paramSeguridadTicket.jdoFlags <= 0)
      return paramSeguridadTicket.anio;
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadTicket.anio;
    if (localStateManager.isLoaded(paramSeguridadTicket, jdoInheritedFieldCount + 0))
      return paramSeguridadTicket.anio;
    return localStateManager.getIntField(paramSeguridadTicket, jdoInheritedFieldCount + 0, paramSeguridadTicket.anio);
  }

  private static final void jdoSetanio(SeguridadTicket paramSeguridadTicket, int paramInt)
  {
    if (paramSeguridadTicket.jdoFlags == 0)
    {
      paramSeguridadTicket.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadTicket.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadTicket, jdoInheritedFieldCount + 0, paramSeguridadTicket.anio, paramInt);
  }

  private static final String jdoGetespecial(SeguridadTicket paramSeguridadTicket)
  {
    if (paramSeguridadTicket.jdoFlags <= 0)
      return paramSeguridadTicket.especial;
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadTicket.especial;
    if (localStateManager.isLoaded(paramSeguridadTicket, jdoInheritedFieldCount + 1))
      return paramSeguridadTicket.especial;
    return localStateManager.getStringField(paramSeguridadTicket, jdoInheritedFieldCount + 1, paramSeguridadTicket.especial);
  }

  private static final void jdoSetespecial(SeguridadTicket paramSeguridadTicket, String paramString)
  {
    if (paramSeguridadTicket.jdoFlags == 0)
    {
      paramSeguridadTicket.especial = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadTicket.especial = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadTicket, jdoInheritedFieldCount + 1, paramSeguridadTicket.especial, paramString);
  }

  private static final Date jdoGetfechaProceso(SeguridadTicket paramSeguridadTicket)
  {
    if (paramSeguridadTicket.jdoFlags <= 0)
      return paramSeguridadTicket.fechaProceso;
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadTicket.fechaProceso;
    if (localStateManager.isLoaded(paramSeguridadTicket, jdoInheritedFieldCount + 2))
      return paramSeguridadTicket.fechaProceso;
    return (Date)localStateManager.getObjectField(paramSeguridadTicket, jdoInheritedFieldCount + 2, paramSeguridadTicket.fechaProceso);
  }

  private static final void jdoSetfechaProceso(SeguridadTicket paramSeguridadTicket, Date paramDate)
  {
    if (paramSeguridadTicket.jdoFlags == 0)
    {
      paramSeguridadTicket.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadTicket.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramSeguridadTicket, jdoInheritedFieldCount + 2, paramSeguridadTicket.fechaProceso, paramDate);
  }

  private static final GrupoTicket jdoGetgrupoTicket(SeguridadTicket paramSeguridadTicket)
  {
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadTicket.grupoTicket;
    if (localStateManager.isLoaded(paramSeguridadTicket, jdoInheritedFieldCount + 3))
      return paramSeguridadTicket.grupoTicket;
    return (GrupoTicket)localStateManager.getObjectField(paramSeguridadTicket, jdoInheritedFieldCount + 3, paramSeguridadTicket.grupoTicket);
  }

  private static final void jdoSetgrupoTicket(SeguridadTicket paramSeguridadTicket, GrupoTicket paramGrupoTicket)
  {
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadTicket.grupoTicket = paramGrupoTicket;
      return;
    }
    localStateManager.setObjectField(paramSeguridadTicket, jdoInheritedFieldCount + 3, paramSeguridadTicket.grupoTicket, paramGrupoTicket);
  }

  private static final long jdoGetidSeguridadTicket(SeguridadTicket paramSeguridadTicket)
  {
    return paramSeguridadTicket.idSeguridadTicket;
  }

  private static final void jdoSetidSeguridadTicket(SeguridadTicket paramSeguridadTicket, long paramLong)
  {
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadTicket.idSeguridadTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramSeguridadTicket, jdoInheritedFieldCount + 4, paramSeguridadTicket.idSeguridadTicket, paramLong);
  }

  private static final int jdoGetmes(SeguridadTicket paramSeguridadTicket)
  {
    if (paramSeguridadTicket.jdoFlags <= 0)
      return paramSeguridadTicket.mes;
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadTicket.mes;
    if (localStateManager.isLoaded(paramSeguridadTicket, jdoInheritedFieldCount + 5))
      return paramSeguridadTicket.mes;
    return localStateManager.getIntField(paramSeguridadTicket, jdoInheritedFieldCount + 5, paramSeguridadTicket.mes);
  }

  private static final void jdoSetmes(SeguridadTicket paramSeguridadTicket, int paramInt)
  {
    if (paramSeguridadTicket.jdoFlags == 0)
    {
      paramSeguridadTicket.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadTicket.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramSeguridadTicket, jdoInheritedFieldCount + 5, paramSeguridadTicket.mes, paramInt);
  }

  private static final String jdoGetusuario(SeguridadTicket paramSeguridadTicket)
  {
    if (paramSeguridadTicket.jdoFlags <= 0)
      return paramSeguridadTicket.usuario;
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
      return paramSeguridadTicket.usuario;
    if (localStateManager.isLoaded(paramSeguridadTicket, jdoInheritedFieldCount + 6))
      return paramSeguridadTicket.usuario;
    return localStateManager.getStringField(paramSeguridadTicket, jdoInheritedFieldCount + 6, paramSeguridadTicket.usuario);
  }

  private static final void jdoSetusuario(SeguridadTicket paramSeguridadTicket, String paramString)
  {
    if (paramSeguridadTicket.jdoFlags == 0)
    {
      paramSeguridadTicket.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramSeguridadTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramSeguridadTicket.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramSeguridadTicket, jdoInheritedFieldCount + 6, paramSeguridadTicket.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}