package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.ProveedorTicket;
import sigefirrhh.base.estructura.Region;

public class ProveedorRegion
  implements Serializable, PersistenceCapable
{
  private long idProveedorRegion;
  private ProveedorTicket proveedorTicket;
  private Region region;
  private String codTicket;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codTicket", "idProveedorRegion", "proveedorTicket", "region" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.ProveedorTicket"), sunjdo$classForName$("sigefirrhh.base.estructura.Region") };
  private static final byte[] jdoFieldFlags = { 21, 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetregion(this).getNombre() + "  -  " + 
      jdoGetproveedorTicket(this).getNombre();
  }

  public String getCodTicket()
  {
    return jdoGetcodTicket(this);
  }

  public void setCodTicket(String codTicket)
  {
    jdoSetcodTicket(this, codTicket);
  }

  public long getIdProveedorRegion()
  {
    return jdoGetidProveedorRegion(this);
  }

  public void setIdProveedorRegion(long idProveedorRegion)
  {
    jdoSetidProveedorRegion(this, idProveedorRegion);
  }

  public ProveedorTicket getProveedorTicket()
  {
    return jdoGetproveedorTicket(this);
  }

  public void setProveedorTicket(ProveedorTicket proveedorTicket)
  {
    jdoSetproveedorTicket(this, proveedorTicket);
  }

  public Region getRegion()
  {
    return jdoGetregion(this);
  }

  public void setRegion(Region region)
  {
    jdoSetregion(this, region);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.ProveedorRegion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ProveedorRegion());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ProveedorRegion localProveedorRegion = new ProveedorRegion();
    localProveedorRegion.jdoFlags = 1;
    localProveedorRegion.jdoStateManager = paramStateManager;
    return localProveedorRegion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ProveedorRegion localProveedorRegion = new ProveedorRegion();
    localProveedorRegion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localProveedorRegion.jdoFlags = 1;
    localProveedorRegion.jdoStateManager = paramStateManager;
    return localProveedorRegion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTicket);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idProveedorRegion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proveedorTicket);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.region);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTicket = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idProveedorRegion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proveedorTicket = ((ProveedorTicket)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.region = ((Region)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ProveedorRegion paramProveedorRegion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramProveedorRegion == null)
        throw new IllegalArgumentException("arg1");
      this.codTicket = paramProveedorRegion.codTicket;
      return;
    case 1:
      if (paramProveedorRegion == null)
        throw new IllegalArgumentException("arg1");
      this.idProveedorRegion = paramProveedorRegion.idProveedorRegion;
      return;
    case 2:
      if (paramProveedorRegion == null)
        throw new IllegalArgumentException("arg1");
      this.proveedorTicket = paramProveedorRegion.proveedorTicket;
      return;
    case 3:
      if (paramProveedorRegion == null)
        throw new IllegalArgumentException("arg1");
      this.region = paramProveedorRegion.region;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ProveedorRegion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ProveedorRegion localProveedorRegion = (ProveedorRegion)paramObject;
    if (localProveedorRegion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localProveedorRegion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ProveedorRegionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ProveedorRegionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProveedorRegionPK))
      throw new IllegalArgumentException("arg1");
    ProveedorRegionPK localProveedorRegionPK = (ProveedorRegionPK)paramObject;
    localProveedorRegionPK.idProveedorRegion = this.idProveedorRegion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ProveedorRegionPK))
      throw new IllegalArgumentException("arg1");
    ProveedorRegionPK localProveedorRegionPK = (ProveedorRegionPK)paramObject;
    this.idProveedorRegion = localProveedorRegionPK.idProveedorRegion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProveedorRegionPK))
      throw new IllegalArgumentException("arg2");
    ProveedorRegionPK localProveedorRegionPK = (ProveedorRegionPK)paramObject;
    localProveedorRegionPK.idProveedorRegion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ProveedorRegionPK))
      throw new IllegalArgumentException("arg2");
    ProveedorRegionPK localProveedorRegionPK = (ProveedorRegionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localProveedorRegionPK.idProveedorRegion);
  }

  private static final String jdoGetcodTicket(ProveedorRegion paramProveedorRegion)
  {
    if (paramProveedorRegion.jdoFlags <= 0)
      return paramProveedorRegion.codTicket;
    StateManager localStateManager = paramProveedorRegion.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorRegion.codTicket;
    if (localStateManager.isLoaded(paramProveedorRegion, jdoInheritedFieldCount + 0))
      return paramProveedorRegion.codTicket;
    return localStateManager.getStringField(paramProveedorRegion, jdoInheritedFieldCount + 0, paramProveedorRegion.codTicket);
  }

  private static final void jdoSetcodTicket(ProveedorRegion paramProveedorRegion, String paramString)
  {
    if (paramProveedorRegion.jdoFlags == 0)
    {
      paramProveedorRegion.codTicket = paramString;
      return;
    }
    StateManager localStateManager = paramProveedorRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorRegion.codTicket = paramString;
      return;
    }
    localStateManager.setStringField(paramProveedorRegion, jdoInheritedFieldCount + 0, paramProveedorRegion.codTicket, paramString);
  }

  private static final long jdoGetidProveedorRegion(ProveedorRegion paramProveedorRegion)
  {
    return paramProveedorRegion.idProveedorRegion;
  }

  private static final void jdoSetidProveedorRegion(ProveedorRegion paramProveedorRegion, long paramLong)
  {
    StateManager localStateManager = paramProveedorRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorRegion.idProveedorRegion = paramLong;
      return;
    }
    localStateManager.setLongField(paramProveedorRegion, jdoInheritedFieldCount + 1, paramProveedorRegion.idProveedorRegion, paramLong);
  }

  private static final ProveedorTicket jdoGetproveedorTicket(ProveedorRegion paramProveedorRegion)
  {
    StateManager localStateManager = paramProveedorRegion.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorRegion.proveedorTicket;
    if (localStateManager.isLoaded(paramProveedorRegion, jdoInheritedFieldCount + 2))
      return paramProveedorRegion.proveedorTicket;
    return (ProveedorTicket)localStateManager.getObjectField(paramProveedorRegion, jdoInheritedFieldCount + 2, paramProveedorRegion.proveedorTicket);
  }

  private static final void jdoSetproveedorTicket(ProveedorRegion paramProveedorRegion, ProveedorTicket paramProveedorTicket)
  {
    StateManager localStateManager = paramProveedorRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorRegion.proveedorTicket = paramProveedorTicket;
      return;
    }
    localStateManager.setObjectField(paramProveedorRegion, jdoInheritedFieldCount + 2, paramProveedorRegion.proveedorTicket, paramProveedorTicket);
  }

  private static final Region jdoGetregion(ProveedorRegion paramProveedorRegion)
  {
    StateManager localStateManager = paramProveedorRegion.jdoStateManager;
    if (localStateManager == null)
      return paramProveedorRegion.region;
    if (localStateManager.isLoaded(paramProveedorRegion, jdoInheritedFieldCount + 3))
      return paramProveedorRegion.region;
    return (Region)localStateManager.getObjectField(paramProveedorRegion, jdoInheritedFieldCount + 3, paramProveedorRegion.region);
  }

  private static final void jdoSetregion(ProveedorRegion paramProveedorRegion, Region paramRegion)
  {
    StateManager localStateManager = paramProveedorRegion.jdoStateManager;
    if (localStateManager == null)
    {
      paramProveedorRegion.region = paramRegion;
      return;
    }
    localStateManager.setObjectField(paramProveedorRegion, jdoInheritedFieldCount + 3, paramProveedorRegion.region, paramRegion);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}